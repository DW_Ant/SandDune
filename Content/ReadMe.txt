The Content directory is intended to store assets for final release where the files are not meant to be edited by humans.

The files here can easily be replaced using the editors that convert DevAssets into Content assets.

The purpose behind this separation is to get the best of both worlds.

Advantages behind DevAssets:
1. Human readable makes it easy to diff and merge when it comes to collaboration.
2. Plain text makes assets resilient to engine changes where information loss is not likely despite having engine changes. This would also make it easier to convert DevAssets from one version to another.
3. Plain text also works well with source control. P4 would be able to save diffs rather than copies for each revision. Git would be able to store and merge diffs.

Advantages behind Content assets:
1. Machine format (binary format) makes assets fast to stream in runtime.
2. Binary format makes it difficult (not impossible) for the end user to modify. For projects that encourages mods, delivering the DevAssets and editors are always an option.
3. Editors are able to convert DevAssets in a format that cater to the platform they're used for. For example, an asset can be made to support 32 bit platforms, platforms where only certain compressions are supported, etc...

The Content directory assets are not intended to be submitted to source control. Instead it's only intended to be delivered to the final product.