uniform sampler2D SDBaseTexture;

void main (void)
{
	//Sets text to red
	gl_FragColor = texture2D(SDBaseTexture, gl_TexCoord[0].xy) * vec4(1,0,0,1);
}