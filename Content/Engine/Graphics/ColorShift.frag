uniform sampler2D SDBaseTexture;
uniform vec4 ColorMultiplier;

//Simple shader that multiplies each pixel of the texture by the ColorMultiplier.
void main (void)
{
	vec4 curColor = texture2D(SDBaseTexture, gl_TexCoord[0].xy);
	
	gl_FragColor = curColor * ColorMultiplier;
}