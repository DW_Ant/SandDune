The DevAsset directory is intended to store assets for source control where files are easily editable.

For image and sound files, these are the photoshop/gimp/audacity files where layers are preserved and uncompressed.

For binary data files (like maps and particles), these are not saved in source control. Instead these files are saved in plain text in order to be diff'd, mergable, and human readable.

These files are not intended to be submitted in the final product due to conversion, performance, and machine readable purposes. Instead, use the editors to convert these files into release form. The release files are saved in the content directory (which is not stored in source control).