Sand Dune is a C++ modular 2D game engine designed from the ground up. This engine leverages a hybrid between an Entity Component System and Object Oriented Programming (OOP).

What makes it an Entity Component System?
The bulk of the functionality is handled through components. Entities can own their own component tree in order to define their behavior. For example, a single Entity may contain multiple Tick Components,
Render Components, Network Components, Physics Components, and Transformation Components. Even the components, themselves, can have their own components. For example, a GuiComponent may have a Text Component and
a Frame Component. And the Frame Component can have their own Input Component and Render Component.

What makes it OOP?
Sand Dune breaks up its functionality in classes. In an ideal situation, each class is responsible for a collection of related functions. In addition to this, Sand Dune implements its own class reflection system
where each object can have self awareness of its class. This system also allows systems to pass in class references to handle generically instantiating objects without directly referencing the class.
Inheritance is a powerful tool used for extending functionality of existing classes without making root classes over complicated to the point where it loses its meaning (commonly referred to as a "god" class in OOP).
An example of a class hierarchy in Sand Dune is the GuiComponent. The GuiComponent is the parent class of other GuiComponents such as FrameComponent, ScrollbarComponent, ListBoxComponent, and LabelComponent. And LabelComponent
is the parent class of the TextFieldComponent, which adds input handling that allows the user to edit the LabelComponent.

Sand Dune uses the right tool for the job. Combining both of these systems allows the engine to not only take advantage the readability and convenience of OOP, but also allows for robust object creation by mixing and matching
components for unique behavior while minimizing inner dependencies.


====================================================
Sand Dune Strengths
====================================================
* Sand Dune defines a host of essential modules majority of game applications need.
	- Core: Defines data types that interface with data buffers and string utilities. Defines the main architecture for the entity component system and engine loop. Defines a collection of other useful utilities such as the DPointer, ticking, and object reflection systems.
	- Editor: Contains utility functions assisting developers to build their editors.
	- File: Defines utility functions and data collections related to the operating system's file system. In addition to that, this module defines configuration file utilities.
	- Graphics: Constructs the architecture responsible for the render pipeline. This module is also responsible for handling 2D and 3D transformations. This module only renders 2D objects.
	- Gui: Defines a collection of Entities for various UI Components. In addition to that, this module is responsible for the GuiTheme to assist with making UI appearances within an application consistent.
	- Input: This module is responsible for processing input events from the window handle and propagating the event to its registered components in a defined manner.
	- Localization: This module is responsible for replacing hard-coded strings with data driven text that varies based on the user selected language.
	- Multi-Thread: Defines an infrastructure that allows multiple engine instances on separate threads to communicate with each other.
	- Network: Defines an architecture that allows this process to communicate with other processes. This module relies on TCP communication to replicate variables, invoke RPCs, and propagate objects. In addition, this module builds the framework allowing developers to configure security parameters.
	- Physics: A primitive module that is responsible for changing Entity positions over time. This module only handles collision. The Box2D module is added for a more mature and robust physics solution.
* Sand Dune is modularly designed, allowing developers to configure which engine components to build and use. This also allow developers to easily insert new functionality to the engine behaviors. Most importantly,
the code is far easier to read when it's broken up into relevant chunks.
* Sand Dune uses the SFML library (http://www.sfml-dev.org/) to interface with low level operations without a dependency in the operating system.
* Sand Dune follows a strict coding convention to improve readability.
* The engine comes with in-source documenting comments, documentation, and examples over complex systems.
* Unit tests are provided to support the integrity of the functionality.


====================================================
Limitations
====================================================
* Only 2D graphics are supported. For scope purposes, there are no 3D shapes that can be rendered to a viewport.
* Sand Dune is missing some essential modules most games need. The following systems are not yet implemented:
	-AI
	-Animation
	-Gameplay
	-Lighting
	-Map Editor
	-Music
	-Particles
	-Sound
	-World
* This engine is still going through rapid development. Systems are subject to change.
* Sand Dune is not yet stable. Bugs and issues are inevitable. Unit tests and examples are provided to identify and address these issues early, but the engine will not become "battle hardened" until it matures over time.
* Only Windows is supported at this time.
* Network module only supports TCP communication. Eventually UDP will be added.
* At this time, the Physics module does not calculate impact points and normals. In addition, it's missing ray tracing features.


====================================================
Prerequisites
====================================================
C++20 compiler (only the MSVC compiler has been tested)
CMake version 3.12.4


====================================================
Setup
====================================================
1. To Generate the solution, execute the RunCMake.bat file found in the <root>/Source directory.
2. The batch script will prompt you to choose a 32 bit or a 64 bit project.
3. The solution file will be found at <root>/Source/ProjectFiles/<platform>/SandDune.sln
4. Compiling the solution will generate executable binaries and DLLs in the <root>/Binaries/<platform>/ directory.
5. After compiling, run <root>/Binaries/<platform>/AssetExporter program to convert plain text assets from DevAssets directory to binary assets in the Content directory.


====================================================
Licenses
====================================================
Sand Dune's source code is released under a commercial license. Please see LICENSE.txt for complete licensing information.
SFML libraries and source code are distributed under the zlib/png license. Please see http://www.sfml-dev.org/license.php for license details.
Box 2D libraries and source code are released under the MIT license.
The UTF-8 third party source code copyright Nemanja Trifunovic is publicly available. Details for this license can be found in the utf8.h file.
Assets in the DevAsset directory have credits and licenses documented in each asset.