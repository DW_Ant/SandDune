/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Plugin.h

  Contains important file includes and definitions for the Plugin module.

  The Plugin module allows the engine to dynamically load shared libraries
  during startup time. With this, the user could enable/disable plugins at a per
  project per thread basis.

  Plugins typically register their own engine component(s) in order to extend
  the engine's functionality.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef PLUGIN_EXPORT
		#define PLUGIN_API __declspec(dllexport)
	#else
		#define PLUGIN_API __declspec(dllimport)
	#endif
#else
	#define PLUGIN_API
#endif

//Platform-specific definitions
#ifdef PLATFORM_WINDOWS
	#ifdef PLATFORM_32BIT
		#define PLUGIN_FUNCTION_DECORATION
	#else
		#define PLUGIN_FUNCTION_DECORATION __stdcall
	#endif
	#define LIBRARY_EXTENSION ".dll"
#else
	#define PLUGIN_FUNCTION_DECORATION
	#define LIBRARY_EXTENSION ".so"
#endif

/**
 * typedefs used for the plugin interface functions.
 */
typedef int (PLUGIN_FUNCTION_DECORATION *Function_StartPlugin)(size_t);
typedef bool (PLUGIN_FUNCTION_DECORATION *Function_DelayedStartPlugin)(size_t, float);
typedef void (PLUGIN_FUNCTION_DECORATION *Function_UpdatePlugin)(float);
typedef void (PLUGIN_FUNCTION_DECORATION *Function_ShutdownPlugin)();

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsPlugin.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacPlugin.h"
#endif

SD_BEGIN
extern PLUGIN_API LogCategory PluginLog;
SD_END