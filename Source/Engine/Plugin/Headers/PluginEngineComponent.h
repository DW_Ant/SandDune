/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PluginEngineComponent.h
  An EngineComponent that manages all dynamically loaded libraries.
=====================================================================
*/

#pragma once

#include "Plugin.h"

SD_BEGIN
class PLUGIN_API PluginEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(PluginEngineComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EPluginState
	{
		PS_NotLoaded, //Library did not initialize (most likely because it failed to initialize).
		PS_Pending, //Library is loaded, but it's currently waiting for its dependencies to load up (eg: an engine component is waiting for the engine to initialize another component).
		PS_Ready, //Library is loaded, initialized, and ready
		PS_Unloaded //Library was loaded, but it was shutdown
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPlugin
	{
		/* Name of the plugin (this is equal to the file name without extension). */
		DString Name;

		EPluginState State;

		/* The library handle that is used to interface with the OS. */
		LIBRARY_HANDLE LibraryHandle;

		/* Essential function pointers to interface with the plugin. */
		Function_StartPlugin StartPluginFunction;
		Function_DelayedStartPlugin DelayedStartFunction;
		Function_UpdatePlugin UpdateFunction;
		Function_ShutdownPlugin ShutdownFunction;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all loaded plugins within the same thread as this engine component. */
	std::vector<SPlugin> Plugins;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PluginEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeComponent () override;
	virtual void PreTick (Float deltaSec) override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through the plugin list and returns the state of the plugin with matching name.
	 * If the name doesn't exist, it'll return PS_NotLoaded.
	 */
	EPluginState GetPluginState (const DString& pluginName) const;
};
SD_END