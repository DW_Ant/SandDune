/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PluginClasses.h
  Contains all header includes for the Plugin module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the PluginClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_PLUGIN
#include "PlatformMacPlugin.h"
#include "PlatformWindowsPlugin.h"
#include "Plugin.h"
#include "PluginClasses.h"
#include "PluginEngineComponent.h"

#endif
