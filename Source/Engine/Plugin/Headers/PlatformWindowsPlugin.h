/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsPlugin.h

  Contains an assembly of Windows-specific functions related to the plugin module.
=====================================================================
*/

#pragma once

#include "Plugin.h"

#ifdef PLATFORM_WINDOWS
#define LIBRARY_HANDLE HMODULE

SD_BEGIN
/**
 * Attempts to dynamically load the given DLL or shared library and returns a handle of
 * the loaded library.
 * The out parameters are assigned to function pointers within the DLL if found. If this function returns nullptr,
 * these parameters are not guaranteed to be assigned.
 * The library must define the following exposed functions in global space:
 *
 * ----------------------------------------------------
 * This function is responsible for instantiating anything it needs to initialize.
 * A typical plugin would instantiate its own EngineComponent and register that component
 * to the Engine associated with the given index.
 * This function returns a negative number should it fail to load the library, and the library will be
 * dropped. The library is responsible for clean up since it will not receive a ShutdownPlugin call if it returns a negative number.
 *
 * This function returns zero if it successfully loaded, but it's currently waiting for a dependency to
 * initialize. Returning zero would cause this plugin to receive DelayedStartPlugin calls.
 *
 * This function returns a postive number if it successfully loaded, and is ready for use.
 * int StartPlugin (size_t engineIdx);
 *
 * ----------------------------------------------------
 * This function assumes that the plugin hasn't completely initialized, and it's waiting at least one dependency
 * before it finishes initializing. This is called every frame until it finally returns true.
 * Return true implies that all dependencies are loaded in, and this plugin now finished initializing.
 * bool DelayedStartPlugin (size_t engineIdx, float deltaSec);
 *
 * ----------------------------------------------------
 * This function is responsible for updating the library. This is called every frame while the plugin is ready.
 * void UpdatePlugin (float deltaSec);
 *
 * ----------------------------------------------------
 * This function is responsible for cleaning up the library. This function should delete
 * any resources the plugin allocated. A typical plugin would shutdown its engine component.
 * void ShutdownPlugin ();
 */
PLUGIN_API LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library, Function_StartPlugin& outStartPlugin, Function_DelayedStartPlugin& outDelayedStart, Function_UpdatePlugin& outUpdate, Function_ShutdownPlugin& outShutdown);

/**
 * Attempts to load the given library without looking into any of its processes.
 * Returns nullptr if it failed to load.
 */
PLUGIN_API LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library);

/**
 * Attempts to unload the given shared library. Returns true on success.
 * Unloading a library should not affect other instances that are using this library (eg: There are multiple engine components each with their own library handles).
 */
PLUGIN_API bool OS_UnloadLibrary (LIBRARY_HANDLE library);
SD_END
#endif