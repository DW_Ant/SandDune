/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsPlugin.cpp
=====================================================================
*/

#include "PlatformWindowsPlugin.h"

#ifdef PLATFORM_WINDOWS
SD_BEGIN
LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library, Function_StartPlugin& outStartPlugin, Function_DelayedStartPlugin& outDelayedStart, Function_UpdatePlugin& outUpdate, Function_ShutdownPlugin& outShutdown)
{
	LIBRARY_HANDLE dllHandle = OS_DynamicallyLoadLibrary(library);
	if (dllHandle == nullptr)
	{
		return nullptr;
	}

	//Ensure the dll has all required functions
	outStartPlugin = (Function_StartPlugin)GetProcAddress(dllHandle, "StartPlugin");
	if (outStartPlugin == nullptr)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to dynamically load library. Unable to find the process address of StartPlugin within %s. Error code: %s"), library.GetName(false, true), DString::MakeString(GetLastError()));
		OS_UnloadLibrary(dllHandle);
		return nullptr;
	}

	outDelayedStart = (Function_DelayedStartPlugin)GetProcAddress(dllHandle, "DelayedStartPlugin");
	if (outDelayedStart == nullptr)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to dynamically load library. Unable to find the process address of DelayedStartPlugin within %s. Error code: %s"), library.GetName(false, true), DString::MakeString(GetLastError()));
		OS_UnloadLibrary(dllHandle);
		return nullptr;
	}

	outUpdate = (Function_UpdatePlugin)GetProcAddress(dllHandle, "UpdatePlugin");
	if (outUpdate == nullptr)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to dynamically load library. Unable to find the process address of UpdatePlugin within %s. Error code: %s"), library.GetName(false, true), DString::MakeString(GetLastError()));
		OS_UnloadLibrary(dllHandle);
		return nullptr;
	}

	outShutdown = (Function_ShutdownPlugin)GetProcAddress(dllHandle, "ShutdownPlugin");
	if (outShutdown == nullptr)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to dynamically load library. Unable to find the process address of ShutdownPlugin within %s. Error code: %s"), library.GetName(false, true), DString::MakeString(GetLastError()));
		OS_UnloadLibrary(dllHandle);
		return nullptr;
	}

	PluginLog.Log(LogCategory::LL_Log, TXT("Successfully loaded dynamic library: %s"), library.GetName(false, true));
	return dllHandle;
}

LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library)
{
	LIBRARY_HANDLE dllHandle = LoadLibrary(StrToLPCSTR(library.GetName(true, true)));
	if (dllHandle == nullptr)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to dynamically load library. Could not load dll: %s"), library.GetName(true, true));
		return nullptr;
	}

	return dllHandle;
}

bool OS_UnloadLibrary (LIBRARY_HANDLE library)
{
	bool success = FreeLibrary(library);
	if (!success)
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to unload dynamic library. %s"), DString::MakeString(GetLastError()));
	}

	return success;
}
SD_END
#endif