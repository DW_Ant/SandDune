/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PluginEngineComponent.cpp
=====================================================================
*/

#include "PluginEngineComponent.h"

IMPLEMENT_ENGINE_COMPONENT(SD::PluginEngineComponent)
SD_BEGIN

PluginEngineComponent::PluginEngineComponent () : EngineComponent()
{
	bTickingComponent = true;
}

void PluginEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	ConfigWriter* config = ConfigWriter::CreateObject();
	CHECK(config != nullptr)

	DString configName(TXT("Plugins.ini"));
	if (!config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, configName), false))
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("The PluginEngineComponent is unable to load any plugins since it's unable to open the %s."), configName);
		config->Destroy();
		return;
	}

	Engine* owningEngine = GetOwningEngine();
	CHECK(owningEngine != nullptr)

	DString sectionName = DString::CreateFormattedString(TXT("%s_%s"), ProjectName, DString::MakeString(owningEngine->GetEngineIndex()));
	if (!config->ContainsSection(sectionName))
	{
		PluginLog.Log(LogCategory::LL_Warning, TXT("The %s file does not contain the %s section. The PluginEngineComponent will assume not to load any plugins for the application %s in engine instance %s."), configName, sectionName, ProjectName, DString::MakeString(owningEngine->GetEngineIndex()));
		config->Destroy();
		return;
	}

	std::vector<DString> pluginsToLoad;
	config->GetArrayValues(sectionName, TXT("EnabledPlugin"), OUT pluginsToLoad);
	config->Destroy();
	if (ContainerUtils::IsEmpty(pluginsToLoad))
	{
		PluginLog.Log(LogCategory::LL_Log, TXT("None of the plugins are enabled for %s."), ProjectName);
		return;
	}

	//Attempt to load each plugin
	for (const DString& pluginName : pluginsToLoad)
	{
		PluginLog.Log(LogCategory::LL_Verbose, TXT("Attempting to load plugin: %s"), pluginName);
		FileAttributes library(Directory::BINARY_DIRECTORY, pluginName + Utils::GetBinaryConfigSuffix() + LIBRARY_EXTENSION);
		if (!OS_CheckIfFileExists(library))
		{
			PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to load plugin %s. Plugin does not exist. Searched in %s."), pluginName, library.ReadPath());
			continue;
		}

		SPlugin newPlugin;
		newPlugin.LibraryHandle = OS_DynamicallyLoadLibrary(library, OUT newPlugin.StartPluginFunction, OUT newPlugin.DelayedStartFunction, OUT newPlugin.UpdateFunction, OUT newPlugin.ShutdownFunction);
		if (newPlugin.LibraryHandle == nullptr)
		{
			PluginLog.Log(LogCategory::LL_Warning, TXT("Failed to load plugin %s. See log for details."), library.GetName(false, true));
			continue;
		}
		newPlugin.Name = library.GetName(false, false);

		Plugins.push_back(newPlugin);
	}

	//Invoke each plugin to launch
	for (SPlugin& plugin : Plugins)
	{
		int state = plugin.StartPluginFunction(owningEngine->GetEngineIndex());
		if (state < 0)
		{
			plugin.State = PS_NotLoaded;
		}
		else if (state == 0)
		{
			plugin.State = PS_Pending;
		}
		else
		{
			PluginLog.Log(LogCategory::LL_Verbose, TXT("Plugin %s is ready!"), plugin.Name);
			plugin.State = PS_Ready;
		}
	}
}

void PluginEngineComponent::PreTick (Float deltaSec)
{
	Super::PreTick(deltaSec);

	for (SPlugin& plugin : Plugins)
	{
		if (plugin.State == PS_Pending)
		{
			Engine* owningEngine = GetOwningEngine();
			CHECK(owningEngine != nullptr)

			if (plugin.DelayedStartFunction(owningEngine->GetEngineIndex(), deltaSec.Value))
			{
				PluginLog.Log(LogCategory::LL_Verbose, TXT("Plugin %s is ready!"), plugin.Name);
				plugin.State = PS_Ready;
			}
		}
		else if (plugin.State == PS_Ready)
		{
			plugin.UpdateFunction(deltaSec.Value);
		}
	}
}

void PluginEngineComponent::ShutdownComponent ()
{
	for (SPlugin& plugin : Plugins)
	{
		PluginLog.Log(LogCategory::LL_Verbose, TXT("Shutting down plugin %s"), plugin.Name);
		if (plugin.State == PS_Pending || plugin.State == PS_Ready)
		{
			plugin.ShutdownFunction();
		}

		if (!OS_UnloadLibrary(plugin.LibraryHandle))
		{
			PluginLog.Log(LogCategory::LL_Warning, TXT("PluginEngineComponent failed to unload plugin %s. See the log for more details."), plugin.Name);
		}

		plugin.State = PS_Unloaded;
	}

	Super::ShutdownComponent();
}

PluginEngineComponent::EPluginState PluginEngineComponent::GetPluginState (const DString& pluginName) const
{
	for (const SPlugin& plugin : Plugins)
	{
		//Typically file systems are case insensitive.
		if (plugin.Name.Compare(pluginName, DString::CC_IgnoreCase) == 0)
		{
			return plugin.State;
		}
	}

	return PS_Unloaded;
}
SD_END