/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacPlugin.cpp
=====================================================================
*/

#include "PlatformMacPlugin.h"

#ifdef PLATFORM_MAC
SD_BEGIN
LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library)
{
#error Please implement OS_DynamicallyLoadLibrary for your platform.
}

LIBRARY_HANDLE OS_DynamicallyLoadLibrary (const FileAttributes& library)
{
#error Please implement OS_DynamicallyLoadLibrary for your platform.
}

bool OS_UnloadLibrary (LIBRARY_HANDLE library)
{
#error Please implement OS_UnloadLibrary for your platform.
}
SD_END
#endif