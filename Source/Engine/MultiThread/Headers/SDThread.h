/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SDThread.h
  An object that represents a single thread.  This object combines functionality from WinAPI and
  pthreads to help make the engine platform independent.  Properties defined here are ignored
  if the current operating system does not support them.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

#ifdef PLATFORM_WINDOWS
	/* HANDLE is typedef'd as void* */
	#define THREAD_OBJECT void
#elif defined(PLATFORM_LINUX)
	#define THREAD_OBJECT pthread_t
#else
	#error Please define THREAD_OBJECT for your platform.
#endif

// Macro that constructs a function signature that aligns with the current platform's CreateThread's start routine parameter.
#ifdef PLATFORM_WINDOWS
	#define SD_THREAD_FUNCTION(functionName) \
	DWORD WINAPI functionName (LPVOID params)

	#define SD_EXIT_THREAD return 0;
#elif define(PLATFORM_LINUX)
	#define SD_THREAD_FUNCTION(functionName) \
	static void* thread_start (void* params)

	#define SD_EXIT_THREAD return nullptr;
#else
	#error Please define SD_THREAD_FUNCTION for your platform.
	#error Please define SD_EXIT_THREAD for your platform.
#endif


SD_BEGIN
class MULTI_THREAD_API SDThread
{


	/*
	=====================
	  Data type
	=====================
	*/

public:
	struct MULTI_THREAD_API SThreadInitData
	{
	public:
		/* Alias applied to the thread (used for debugging).  If left empty, then it'll use the OS default thread name. */
		DString ThreadName;

		SThreadInitData ();
		SThreadInitData (const SThreadInitData& copyObject);
		virtual ~SThreadInitData ();
	};

#ifdef PLATFORM_WINDOWS
	#define THREAD_INIT_TYPE SD::SDThread::SWinThreadInitData

	/**
	 * Struct defining attributes used to initialize WinAPI threads.
	 */
	struct MULTI_THREAD_API SWinThreadInitData : public SThreadInitData
	{
	public:
		LPSECURITY_ATTRIBUTES SecurityAttributes;
		size_t StackSize;
		DWORD CreationFlags;

		SWinThreadInitData ();
		SWinThreadInitData (const SWinThreadInitData& copyObject);
		virtual ~SWinThreadInitData ();
	};

	//Struct specifically used to raise an exception to allow windows debuggers to give threads an alias.  See OS_CreateThread.
	//See:  https://docs.microsoft.com/en-us/visualstudio/debugger/how-to-set-a-thread-name-in-native-code
	struct MULTI_THREAD_API SWinThreadNameData
	{
	public:
		DWORD Type = 0x1000; //Must be 0x1000.

		/* Pointer to name (in user addr space). */
		LPCSTR ThreadName;

		/* Thread ID (-1=caller thread). */
		DWORD ThreadId = -1;

		/* Reserved for future use, must be zero. */
		DWORD Flags = 0;

		//Note:  Cannot define a constructor for this struct due to error C2712:
		//Cannot use __try in functions that require object unwinding
	};
#endif

#ifdef PLATFORM_LINUX
	#define THREAD_INIT_TYPE SD::SDThread::SpThreadInitData

	/**
	 * Struct defining attributes used to initialize pthreads.
	 */
	struct MULTI_THREAD_API SpThreadInitData : public SThreadInitData
	{
	public:
		pthread_attr_t InitAttributes;

		SpThreadInitData ();
		SpThreadInitData (const SpThreadInitData& copyObject);
		virtual ~SpThreadInitData ();
	};
#endif


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Object used to uniquely identify this thread from others. */
	THREAD_OBJECT* Thread;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SDThread ();
	SDThread (THREAD_OBJECT* inThread);
    SDThread (const SDThread& copyObject);
	virtual ~SDThread ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	inline bool operator== (const SDThread& other) const
	{
		return (Thread == other.Thread);
	}

	inline operator bool () const
	{
		return IsValid();
	}

	inline bool operator! () const
	{
		return !IsValid();
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	inline bool IsValid () const
	{
		return (Thread != nullptr);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const THREAD_OBJECT* GetThread () const
	{
		return Thread;
	}
};
SD_END