/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MultiThread.h
  Contains important file includes and definitions for the MultiThread module.

  The MultiThread module defines a functionality that can lock/freeze engines that
  allows safe communication across thread boundaries.  This module also defines
  a ThreadedComponent that grants any Entity the capability of freezing engines.

  Lastly, this module defines utility functions and objects used for generic multi-
  threading functionality such as creating/naming a thread, and establishing a
  platform agnostic thread object.

  This module depends on the following modules:
  - Core
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef MULTI_THREAD_EXPORT
		#define MULTI_THREAD_API __declspec(dllexport)
	#else
		#define MULTI_THREAD_API __declspec(dllimport)
	#endif
#else
	#define MULTI_THREAD_API
#endif

#include <thread>

SD_BEGIN
extern LogCategory MULTI_THREAD_API MultiThreadLog;
SD_END