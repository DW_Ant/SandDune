/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadManager.h
  The ThreadManager listens for requests from any ThreadedComponent within the same thread.
  Should there be sufficient amount of requests for data transfer, then the manager will
  halt the current thread, and notify the external engine's ThreadManager to stop their thread.

  When both Engines are frozen, then all pending ThreadedComponents may transfer their data.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

SD_BEGIN
class MultiThreadEngineComponent;
class ThreadedComponent;

class MULTI_THREAD_API ThreadManager
{


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/**
	 * Struct containing data related for requests to execute data transfers.
	 */
	struct SPendingRequestData
	{
	public:
		/* The Engine Index of Engine::EngineInstances that will freeze before copying data. */
		size_t ExternalEngineIdx;

		/* List of ThreadedComponents that are waiting for both Engines to freeze before transfering data. */
		std::vector<ThreadedComponent*> PendingComponents;

		/* If true, then the manager will freeze both Engines as soon as it can. */
		bool bFreezingEngines;

		/* Maximum number of time remaining before transfering data.  When negative, then this is disabled. */
		Float TimeRemaining;

		SPendingRequestData ()
		{
			ExternalEngineIdx = UINT_INDEX_NONE;
			bFreezingEngines = false;
			TimeRemaining = -1.f;
		}

		/**
		 * Iterates through each PendingComponent to invoke their CopyMethods.
		 * Clears pending activity after execution.
		 */
		void FlushPendingComponents ();

		/**
		 * Returns true if the conditions are sufficient enough to lock both threads.
		 * This method will decrement time remaining if enabled.
		 *
		 * @param manager The local manager that owns this thread request.
		 * @param deltaSec Time elapsed since last check if it should lock this thread.
		 */
		bool ShouldLockThreads (ThreadManager* manager, Float deltaSec);
	};

	/**
	 * Struct containing data related to a ThreadedComponent instantiation request.
	 */
	struct SComponentInstantiationData
	{
		/* External component the newly instanced local component will link to. */
		ThreadedComponent* ExternalRequester;

		/* Method used to instantiate the local component.  The function returns the newly instantiated object. */
		std::function<ThreadedComponent*()> InstantiationMethod;

		SComponentInstantiationData ()
		{
			ExternalRequester = nullptr;
			InstantiationMethod = nullptr;
		}

		SComponentInstantiationData (ThreadedComponent* inExternalRequester, std::function<ThreadedComponent*()> inInstantiationMethod)
		{
			ExternalRequester = inExternalRequester;
			InstantiationMethod = inInstantiationMethod;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of ThreadedComponents that needs a notification that they have new data available in their data buffer.
	They are invoked immediately after the local engine unfreezes and it's no longer blocking other threads. */
	std::vector<ThreadedComponent*> NewDataNotifyComponents;

protected:
	/* Determines how often does the ThreadManager check the NumFreezeRequests variable while
	waiting for other threads to transfer data.  This is in milliseconds. */
	static const Int FreezeCheckFrequency;

	/* If positive, the ThreadManager will sleep for this many seconds. While sleeping, other engines are able to transfer data to this thread (not the other way around).
	Often times, other threads do not need to run at a high update rate as the main thread. Might as well process thread requests while this manager is sleeping and
	the external thread manager happened to check on this thread manager while it's sleeping. That way, thread requests can be processed without requiring to freeze both threads.
	If not positive, then the only way to transfer data between threads is when both engines are in agreement to freeze and copy data. */
	Float GracePeriod;

	/* Maximum number of thread requests for a single Engine that are allowed to queue up before
	the ThreadManager forces the engine lock.
	Requests from external Engines to lock the local engine are excluded.
	If not positive, then the ThreadManager will not automatically flush requests based on number of requests. */
	Int MaxNumRequests;

	/* List of requests to instantiate ThreadedComponents on this thread.
	A single request will force an engine freeze as soon as possible.
	This vector is referenced in multiple threads that maybe outside the freezing framework.  Lock its associated mutex when accessing. */
	std::vector<SComponentInstantiationData> ComponentInstantiationRequests;

	/* Mutex for accessing ComponentInstantiationRequests. */
	std::mutex ComponentInstantiationRequestMutex;

	/* List of Requests of ThreadedComponents waiting for an external Engine to freeze.
	Each struct must point to an external Engine.  Each ExternalEngineIdx must be unique. */
	std::vector<SPendingRequestData> PendingRequestData;

	/* Increments everytime a ThreadManager from an external thread requests the local Engine to freeze.
	The local ThreadManager will then halt the current thread to allow the external manager to copy data.  The
	external manager will then decrement this value when it's finished.  The local thread manager will
	resume local thread execution when this value reaches zero. */
	Int NumExternalFreezeRequests;

	/* Mutex used to access NumExternalFreezeRequest variable. */
	std::mutex FreezeRequestMutex;

	/* Becomes true if the local thread is frozen, and it's safe to copy data to this thread. */
	bool bThreadFrozen;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ThreadManager ();
	virtual ~ThreadManager ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds a callback to be invoked on the external thread where the function will instantiate
	 * a threaded component object that links to the specified ThreadedComponent.
	 * The ThreadManager will wait until both engines are locked before instantiation.
	 * After instantiation, the manager will link the localRequester with the newly instantiated ThreadedComponent.
	 */
	virtual void InstantiateExternalThreadComponent (ThreadedComponent* localRequester, std::function<ThreadedComponent*()> instantiationMethod);

	/**
	 * Checks its lists of PendingRequestData.  This method will halt the current thread should it need
	 * to copy data across thread boundaries.

	 * @param deltaSec Number of seconds that elapsed since last frame.
	 * @return true if the ThreadManager halted this thread, and at least one of the external engines transferred data to thread components residing in this thread.
	 */
	virtual bool EvaluateLockRequests (Float deltaSec);

	/**
	 * Adds a request to the data transfer queue.  The requester and ThreadManager must reside on the same thread.
	 *
	 * @param localRequester - The ThreadComponent that would like to transfer data from this thread to the external thread.
	 * @param bForeLock - If true, then this request is urgent.  The ThreadManager will lock both Engines as soon as possible.
	 * @param maxWaitTime - The maximum wait time before the manager locks both threads.  The thread manager may flush the
	 * requests before the max wait time (based on urgency and amount of other requests).  If nonpositive, then the request
	 * may wait indefinitely (no time limit).
	 */
	virtual void AddThreadRequest (ThreadedComponent* localRequester, bool bForceLock, Float maxWaitTime);

	/**
	 * Removes all thread component requests that were instigated from the specified requester.
	 */
	virtual void CancelComponentInstantiation (ThreadedComponent* externalRequester);

	/**
	 * Removes the specified ThreadedComponent from the pending data list.
	 */
	virtual void RemoveThreadRequest (ThreadedComponent* localThreadComp, size_t engineIdx);

	virtual void SetMaxNumRequests (Int newMaxNumRequests);

	/**
	 * Iterates through the list of ThreadedComponents that are pending notification that they have new data available in their data buffer.
	 * This will then clear the list after execution. This is executed synchronously.
	 */
	virtual void NotifyNewDataComponents ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetGracePeriod (Float newGracePeriod);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetGracePeriod () const
	{
		return GracePeriod;
	}

	inline Int GetMaxNumRequests () const
	{
		return MaxNumRequests;
	}

	inline bool IsThreadFrozen () const
	{
		return bThreadFrozen;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Finds the MultiThreadEngineComponent that's attached to the specified engine.
	 *
	 * @param engineIdx Target engine to retrieve found in the index of Engine::EngineInstances
	 * @return If valid, it'll return the threaded engine component attached to the engine.
	 * It'll return nullptr if the engine is not found or is shutting down.
	 */
	virtual MultiThreadEngineComponent* GetThreadedEngine (size_t engineIdx) const;

	/**
	 * Retrieves the engine's friendly name (if any).  Otherwise, it'll return the given engine index.
	 *
	 * @param engineIdx The target engine to retrieve found in the index of Engine::Instances.
	 */
	virtual DString GetEngineName (size_t engineIdx) const;

	/**
	 * Retrieves the local engine's friendly name (if any).  Otherwise, it'll return the index of the local engine.
	 */
	virtual DString GetEngineName () const;

	/**
	 * Iterates through the pending thread component instantiation list, and links up the instantiated component
	 * with the requester.  Only components matching engines that are currently lock are processed.
	 * This function assumes that the local engine is frozen.
	 */
	virtual void ProcessComponentInstantationRequests ();

	/**
	 * Notifies the external engine to freeze when its time is right.  This function will not return
	 * until the external engine is frozen and all pending components transfered their data to the external thread.
	 *
	 * @param outPendingRequestData struct that's waiting for the external engine to freeze.
	 */
	virtual void TransferDataToExternalThread (SPendingRequestData& outPendingRequestData);

	/**
	 * Notifies this ThreadManager to freeze its local Engine to allow another ThreadManager to transfer data.
	 * The ThreadManager will halt the current thread when the Engine permits it to.
	 */
	virtual void ExternalFreezeRequest ();

	/**
	 * Notifies this ThreadManager that the data transfer has finished.  This decrements the NumFreezeRequests.
	 */
	virtual void ExternalUnfreezeRequest ();
};
SD_END