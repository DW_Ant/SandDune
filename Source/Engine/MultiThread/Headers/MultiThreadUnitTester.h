/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MultiThreadUnitTester.h
  Unit tester that'll launch multi-threaded tests.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

#ifdef DEBUG_MODE

SD_BEGIN
class MULTI_THREAD_API MultiThreadUnitTester : public UnitTester
{
	DECLARE_CLASS(MultiThreadUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes the environment for the ThreadTester.  Creates a Debug Engine instance.
	 * Kicks off the debug engine thread.
	 * Instantiates and kicks off the thread tester in local thread.  Returns true if the test launched.
	 */
	virtual bool LaunchThreadTester (EUnitTestFlags testFlags) const;
};

SD_END

#endif //debug_mode