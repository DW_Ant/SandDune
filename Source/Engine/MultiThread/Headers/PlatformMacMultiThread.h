/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacMultiThread.h
  Utility functions containing Mac-specific multi threaded operations.
=====================================================================
*/

#pragma once

#include "MultiThread.h"
#include "SDThread.h"

#ifdef PLATFORM_MAC

SD_BEGIN

/*
=====================
  Methods
=====================
*/

/**
 * Creates a new thread, and invokes the specified function on that thread.
 * @param outThread The thread object that'll uniquely identify the newly created thread.
 * @param startRoutine The function address that will be invoked first on the new thread.  This parameter varies based on the platform.
 * @param routineArgs The parameters passed into startRoutine.
 * @param initData A struct containing various optional parameters used to initialize the thread.
 * @return Returns an error code should an issue happen with thread initialization.  Returns 0 if there aren't any errors.
 */
int MULTI_THREAD_API OS_CreateThread (SDThread& outThread, void** startRoutine, void* routineArgs, SDThread::SThreadInitData* initData);
SD_END

#endif