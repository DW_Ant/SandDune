/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DebugEngineTestWatcher.h

  Object that tracks various debugging objects.  Whenever all objects are destroyed, this
  object will shutdown the local (Debugging) engine.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

#ifdef DEBUG_MODE

SD_BEGIN
class MULTI_THREAD_API DebugEngineTestWatcher : public Object
{
	DECLARE_CLASS(DebugEngineTestWatcher)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of objects this object is watching.  Whenever this list clears during garbage collection, this object will shutdown the engine. */
	std::vector<Object*> WatchedObjects;

private:
	/* Becomes true when a watch object was added to the watch list. */
	bool bIsWatching;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Searches through the local Engine's object list to find this instance, and returns the
	 * DebugEngineTestWatcher instance if found.
	 */
	static DebugEngineTestWatcher* FindTestWatcher ();

	/**
	 * Adds the given object to the watch list.  This object will not shutdown the engine until
	 * the given object and all other objects in watch list is destroyed.
	 */
	virtual void AddWatchObject (Object* newWatchObject);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END

#endif