/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadedComponent.h
  ThreadedComponent may generate a request to halt at least 2 Enignes to allow safe data
  transfer across thread boundaries.  It acts like the messenger that can send and receive data across
  threads, and their owning Entities are able to read that data when the engines are unlocked.

  This framework constructs an ordered structure for thread-safe data transfers for easier debugging
  and to simplify complicated multi-threaded architectures.

  The ThreadedComponents are able to instantiate another ThreadedComponent on the external thread.  After
  instantiation, the two ThreadedComponents are linked together.  A ThreadedComponent can link at most one ThreadedComponent.

  Connected ThreadedComponents are able to generate requests to their local ThreadManagers.  ThreadManagers
  periodically freeze two engines.  When frozen, the ThreadManagers will notify all pending ThreadedComponents
  that it's safe to transfer data to the other thread.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

SD_BEGIN
class ThreadManager;

class MULTI_THREAD_API ThreadedComponent : public EntityComponent
{
	DECLARE_CLASS(ThreadedComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then the ExternalData buffer will automatically empty when OnNewData is called.
	When false, then the Entity that reads the data buffer must empty it. Setting it to false
	allows for data accumulation, and it allows entities to read it on their own terms (such as tick callbacks). */
	bool bAutoClearDataBuffer;

protected:
	/* Index of the external Engine this ThreadedComponent will copy data to. */
	UINT_TYPE ExternalEngineIdx;

	/* Index of the Engine this component resides in. */
	UINT_TYPE InternalEngineIdx;

	/* Component in the other thread this component may transfer data to and receive data from.
	The external component must reside in thread that's different from this component's thread.
	This variable is not a DPointer to prevent the pointer linked list from pointing to objects in different threads.
	*/
	ThreadedComponent* ExternalComponent;

	/* Mutex used for accessing ExternalComponent. */
	std::mutex ExternalComponentMutex;

	/* Function to invoke to transfer data across thread boundaries.  This method is executed on this component's thread
	while the external thread is locked (in a spin loop).  The CopyMethod should populate the data buffer which the external component has access to. */
	SDFunction<void, DataBuffer& /*outExternalBuffer*/ > CopyMethod;

	/* Delegate executed on this component's local thread. This is called when the ExternalData databuffer has information in it, and
	the external engine is no longer frozen. The delegate handler can safely access anything in the local thread.
	The parameter of OnNewData is a reference to this component's ExternalData. The handler should read data from this buffer before it empties. */
	SDFunction<void, const DataBuffer& /*incomingData*/> OnNewData;

	/* Delegate executed whenever this component lost its connection to the ExternalComponent. This is used primarily for cleanup purposes.
	For example, if a stubbed Entity Owner was instantiated, this is a good place to clean up that Entity.
	By the time this handler is called, the external thread is not frozen. */
	SDFunction<void, ThreadedComponent* /*delegateOwner*/> OnDisconnect;

	/* DataBuffer that can be populated by the threaded component in the external thread. */
	DataBuffer ExternalData;

private:
	/* ThreadManager this component contacts to generate requests for data transfers. */
	ThreadManager* LocalThreadManager;

	/* Becomes true if this component is or was linked to an external ThreadedComponent. */
	bool bEstablishedConnection;

	/* Becomes true when the external component disconnected from this component. This flag indicates that this component needs to broadcast its OnDisconnect callback. */
	bool bPendingOnDisconnect;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this component's connection to the external thread is terminated.
	 * This returns false if the connection is not yet established or if there is currently a connection.
	 */
	virtual bool IsConnectionTerminated () const;

	/**
	 * Notifies the local Thread Manager that this component would like to send data to an external thread.
	 * When the ThreadManager permits it (when local and external engines are locked), this component may then
	 * execute its copy method.
	 * @param bForced If true, then the thread manager will lock both engines as soon as possible.  Otherwise
	 * the request may wait indefinitely before the transfer, which is based on number of requests and wait time.
	 */
	virtual void RequestDataTransfer (bool bForced);

	/**
	 * Notifies the local Thread Manager that this component would like to send data to an external thread.
	 * When the ThreadManager permits it (when local and external engines are locked), this component may then
	 * execute its copy method.
	 * @param maxWaitTime Maximum time to wait before the thread manager locks both engines.  If nonpositive, then
	 * the request may wait indefinitely (no time limit).
	 */
	virtual void RequestDataTransfer (Float maxWaitTime);

	/**
	 * Notifies the local thread manager that this component would like to instantiate a
	 * ThreadedComponent on the external thread.
	 * When the manager processes the request, this component's ExternalComponent is assigned.
	 * A ThreadedComponent is allowed to instantiate at most one component on the external thread.
	 *
	 * @param inExternalEngineIdx The index of the engine (in Engine::EngineInstances) that'll be hosting the new ThreadedComponent.
	 * @param instantiationMethod Callback to invoke that'll instantiate and initialize the component.  The engine is not locked at the time this is called.
	 * This function should instantiate a ThreadedComponent and return that instantiated component to pair up this component with its external counterpart.
	 */
	virtual void InstantiateExternalComp (UINT_TYPE inExternalEngineIdx, std::function<ThreadedComponent*()> instantiationMethod);

	/**
	 * Returns true if this ThreadedComponent's CopyMethod is bound to a callback.
	 * Typically, ThreadedComponents that are only reading data will not have this callback set.
	 */
	virtual bool IsCopyMethodBounded () const;

	/**
	 * Executes the OnNewData delegate. This function is called immediately after the local engine unfreezes,
	 * and it's safe to access other elements in the current thread.
	 * If bAutoClearDataBuffer is true, then the ExternalData is cleared after invoking the delegate.
	 */
	virtual void ReceiveNewData ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCopyMethod (SDFunction<void, DataBuffer&> newCopyMethod);
	virtual void SetOnNewData (SDFunction<void, const DataBuffer&> newOnNewData);
	virtual void SetOnDisconnect (const SDFunction<void, ThreadedComponent*>& newOnDisconnect);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline UINT_TYPE GetExternalEngineIdx () const
	{
		return ExternalEngineIdx;
	}

	inline UINT_TYPE GetInternalEngineIdx () const
	{
		return InternalEngineIdx;
	}

	/**
	 * Returns a reference to the ExternalData vector.
	 * This is primarily used to extract data from this component.
	 * It's safe to read from it at any time since other engines cannot write to this data buffer
	 * while the local thread is unlocked.
	 */
	inline DataBuffer& EditExternalData ()
	{
		return ExternalData;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Invokes the copy method to run the data transfer.
	 * Make sure the local Engine and the external Engine are frozen to avoid race conditions.
	 */
	virtual void ExecuteCopyMethod ();

	/**
	 * Detaches this component from the external threaded component, and it
	 * disconnects the external component's reference to this component.
	 */
	virtual void DetachThreadedConnection ();

	virtual void SetExternalComponent (ThreadedComponent* newExternalComponent);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCheckDisconnect (Float deltaSec, TickComponent* tick);

	friend class ThreadManager;
};
SD_END