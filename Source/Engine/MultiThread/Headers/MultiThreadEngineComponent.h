/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MultiThreadEngineComponent.h
  This component is responsible maintaining and ticking the ThreadManager.
  An Engine with this component assumes that it may freeze occasionally to enable cross engine communication.
=====================================================================
*/

#pragma once

#include "MultiThread.h"
#include "ThreadManager.h"

SD_BEGIN
class MULTI_THREAD_API MultiThreadEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(MultiThreadEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* ThreadManager that may freeze this engine to allow external engines to transfer data across thread boundaries. */
	ThreadManager ThreadController;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	MultiThreadEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PostTick (Float deltaSec) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual ThreadManager* GetThreadManager ();
};
SD_END