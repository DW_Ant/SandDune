/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsMultiThread.h
  Utility functions containing Windows-specific multi threaded operations.
=====================================================================
*/

#pragma once

#include "MultiThread.h"
#include "SDThread.h"

#ifdef PLATFORM_WINDOWS


SD_BEGIN


/*
=====================
  Methods
=====================
*/

/**
 * Creates a new thread, and invokes the specified function on that thread.
 * @param outThread The thread object that'll uniquely identify the newly created thread.
 * @param startRoutine The function address that will be invoked first on the new thread.  This parameter varies based on the platform.
 * @param routineArgs The parameters passed into startRoutine.
 * @param initData A struct containing various optional parameters used to initialize the thread.  If nullptr, then default settings are used.
 * @return Returns an error code should an issue happen with thread initialization.  Returns 0 if there aren't any errors.
 * If you need specific details on windows errors.  Call the GetLastError Win API method.
 */
int MULTI_THREAD_API OS_CreateThread (SDThread& outThread, LPTHREAD_START_ROUTINE startRoutine, void* routineArgs, SDThread::SThreadInitData* initData);

SD_END

#endif //PLATFORM_WINDOWS