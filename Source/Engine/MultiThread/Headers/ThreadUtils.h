/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadUtils.h
  A small utility class that defines a few thread-related helper functions.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

SD_BEGIN
class MULTI_THREAD_API ThreadUtils : public BaseUtils
{
	DECLARE_CLASS(ThreadUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to lock the mutex (only if it's available).  If locked, then this function will
	 * execute the function that's passed in.  The mutex is automatically unlocked after the function's
	 * execution.  This function will not block the current thread if the mutex is already locked.
	 * Behavior is undefined if this function is called while the current thread already locked the mutex.
	 *
	 * @param outMutex The object containing the semaphore.  This mutex will be locked during the execution of the lambda.
	 * @param codeToExecute The method to execute if the local thread obtained ownership over the mutex.
	 * @return Returns true if the mutex was locked, and the codeToExecute parameter was executed.
	 */
	static bool TryLock (std::mutex& outMutex, std::function<void()> codeToExecute);
};
SD_END