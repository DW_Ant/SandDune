/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadManager.cpp
=====================================================================
*/

#include "MultiThreadEngineComponent.h"
#include "ThreadedComponent.h"
#include "ThreadManager.h"
#include "ThreadUtils.h"

SD_BEGIN
const Int ThreadManager::FreezeCheckFrequency = 2;

void ThreadManager::SPendingRequestData::FlushPendingComponents ()
{
	for (ThreadedComponent* threadComp : PendingComponents)
	{
		threadComp->ExecuteCopyMethod();
	}

	//Components are no longer pending.  Reset data.
	ContainerUtils::Empty(OUT PendingComponents);
	bFreezingEngines = false;
	TimeRemaining = -1.f;
}

bool ThreadManager::SPendingRequestData::ShouldLockThreads (ThreadManager* manager, Float deltaSec)
{
	if (bFreezingEngines)
	{
		return true;
	}

	//No requests waiting or all pending threaded components were destroyed
	if (ContainerUtils::IsEmpty(PendingComponents))
	{
		return false;
	}

	if (manager->GetMaxNumRequests() > 0 && PendingComponents.size() >= manager->GetMaxNumRequests().ToUnsignedInt())
	{
		return true;
	}

	if (TimeRemaining >= 0)
	{
		TimeRemaining -= deltaSec;
		return (TimeRemaining <= 0);
	}

	return false;
}

ThreadManager::ThreadManager () :
	GracePeriod(-1.f),
	MaxNumRequests(0),
	NumExternalFreezeRequests(0),
	bThreadFrozen(false)
{
	//Noop
}

ThreadManager::~ThreadManager ()
{
	//Noop
}

void ThreadManager::InstantiateExternalThreadComponent (ThreadedComponent* localRequester, std::function<ThreadedComponent*()> instantiationMethod)
{
	std::lock_guard<std::mutex> guard(ComponentInstantiationRequestMutex);
#ifdef DEBUG_MODE
	//Ensure the threaded component does not have a request pending already
	for (size_t i = 0; i < ComponentInstantiationRequests.size(); ++i)
	{
		if (ComponentInstantiationRequests.at(i).ExternalRequester == localRequester)
		{
			MultiThreadLog.Log(LogCategory::LL_Warning, TXT("The threaded component (%s) already has a component instantiation request.  A ThreadedComponent can only link to one external ThreadedComponent."), localRequester->ToString());
		}
	}
#endif

	if (localRequester->ExternalComponent != nullptr)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Cannot instantiate an external threaded component since %s is already linked to an external threaded component."), localRequester->ToString());
		return;
	}

	ComponentInstantiationRequests.push_back(SComponentInstantiationData(localRequester, instantiationMethod));
}

bool ThreadManager::EvaluateLockRequests (Float deltaSec)
{
	//Instantiate Listeners that other threads may be looking for
	ProcessComponentInstantationRequests();

	bool bHaltedThread = false;

	bool bHasRequest = (NumExternalFreezeRequests > 0);
	if (bHasRequest)
	{
		bHaltedThread = true;

		/*
		An External ThreadManager has requested to lock this thread.
		That means an external thread is already locked.  Flush all local PendingRequests
		that are already waiting for that locked Engine before notifying the external thread.
		*/
		size_t i = 0;
		while (i < PendingRequestData.size())
		{
			MultiThreadEngineComponent* externalEngine = GetThreadedEngine(PendingRequestData.at(i).ExternalEngineIdx);
			if (externalEngine == nullptr || externalEngine->GetThreadManager() == nullptr)
			{
				//The other Engine is shutting down, or has already been destroyed.  Remove ThreadedComponents.
				for (ThreadedComponent* threadComp : PendingRequestData.at(i).PendingComponents)
				{
					threadComp->Destroy();
				}

				PendingRequestData.erase(PendingRequestData.begin() + i);
				continue;
			}
			else if (externalEngine->GetThreadManager()->bThreadFrozen)
			{
				//External thread is already locked.  Copy data while both threads (local and external) are locked.
				PendingRequestData.at(i).FlushPendingComponents();
			}

			++i;
		}

		//Notify other ThreadManagers that it's safe to transfer data to this thread.
		bThreadFrozen = true;
		MultiThreadLog.Log(LogCategory::LL_Verbose, TXT("Locking %s to receive external data."), GetEngineName());

		//Now is a safe time to lock current thread.  Freeze current thread to allow external thread to transfer data.
		while (bHasRequest)
		{
			OS_Sleep(FreezeCheckFrequency);

			//Freeze if something is accessing to NumExternalRequests or if num requests greater than 0.
			bHasRequest = true;
			ThreadUtils::TryLock(FreezeRequestMutex, [&]()
			{
				bHasRequest = (NumExternalFreezeRequests > 0);
			});
		}
		MultiThreadLog.Log(LogCategory::LL_Verbose, TXT("Unlocking %s."), GetEngineName());

		//At this point, no other manager is waiting on this thread.  Unlock the thread.
		bThreadFrozen = false;
	}
	else
	{
		//Send data to external thread managers that are sleeping.
		for (size_t i = 0; i < PendingRequestData.size(); ++i)
		{
			if (ContainerUtils::IsEmpty(PendingRequestData.at(i).PendingComponents))
			{
				continue;
			}

			MultiThreadEngineComponent* externalEngine = GetThreadedEngine(PendingRequestData.at(i).ExternalEngineIdx);
			if (externalEngine == nullptr)
			{
				continue;
			}

			ThreadManager* externalThreadManager = externalEngine->GetThreadManager();
			if (externalThreadManager != nullptr && externalThreadManager->bThreadFrozen)
			{
				TransferDataToExternalThread(OUT PendingRequestData.at(i));
				//Don't freeze current engine since this engine is not going to wait for the external thread manager to wake up.
			}
		}
	}


	if (!bHaltedThread && GracePeriod > 0.f)
	{
		//Let other thread managers transfer data to this thread if they happen to check on this during this sleep period.
		bThreadFrozen = true;
		OS_Sleep((GracePeriod * 1000.f).ToInt());

		while (NumExternalFreezeRequests > 0)
		{
			//Noop - An external engine is currently writing to this instance. Wait for that external engine to finish. 
		}

		bThreadFrozen = false;

		//If an external component wrote to at least one threaded component while this manager was frozen, set bHaltedThread to true to indicate that there is an update.
		bHaltedThread = !ContainerUtils::IsEmpty(NewDataNotifyComponents);
	}

	//Check if it needs to lock other Engines
	for (size_t i = 0; i < PendingRequestData.size(); ++i)
	{
		if (PendingRequestData.at(i).ShouldLockThreads(this, deltaSec))
		{
			//This will lock the current thread until the other engine becomes available.
			TransferDataToExternalThread(PendingRequestData.at(i));
			bHaltedThread = true;
		}
	}

	return bHaltedThread;
}

void ThreadManager::AddThreadRequest (ThreadedComponent* requester, bool bForceLock, Float maxWaitTime)
{
	size_t externalEngineIdx = requester->GetExternalEngineIdx();
	if (externalEngineIdx == UINT_INDEX_NONE)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to add thread request since the instigating ThreadedComponent's ExternalEngineIdx is not initialized."));
		return;
	}

	size_t requestIdx = UINT_INDEX_NONE;

	//Check if the engine already has a list
	for (size_t i = 0; i < PendingRequestData.size(); ++i)
	{
		if (PendingRequestData.at(i).ExternalEngineIdx == externalEngineIdx)
		{
			requestIdx = i;
			break;
		}
	}

	if (requestIdx == UINT_INDEX_NONE)
	{
		requestIdx = PendingRequestData.size();
		PendingRequestData.push_back(SPendingRequestData());
		PendingRequestData.at(requestIdx).ExternalEngineIdx = externalEngineIdx;
	}
	else
	{
		//Find the requester
		for (ThreadedComponent* comp : PendingRequestData.at(requestIdx).PendingComponents)
		{
			if (comp == requester)
			{
				//This component is already pending.  Simply update parameters
				PendingRequestData.at(requestIdx).bFreezingEngines |= bForceLock;

				if (maxWaitTime > 0.f)
				{
					PendingRequestData.at(requestIdx).TimeRemaining = Utils::Clamp<Float>(PendingRequestData.at(requestIdx).TimeRemaining, 0.f, maxWaitTime);
				}

				return;
			}
		}
	}

	//Add the new requester to the queue
	PendingRequestData.at(requestIdx).PendingComponents.push_back(requester);
	PendingRequestData.at(requestIdx).bFreezingEngines = bForceLock;
	PendingRequestData.at(requestIdx).TimeRemaining = maxWaitTime;
}

void ThreadManager::CancelComponentInstantiation (ThreadedComponent* externalRequester)
{
	std::lock_guard<std::mutex> guard(ComponentInstantiationRequestMutex);
	size_t i = 0;
	while (i < ComponentInstantiationRequests.size())
	{
		if (ComponentInstantiationRequests.at(i).ExternalRequester == externalRequester)
		{
			ComponentInstantiationRequests.erase(ComponentInstantiationRequests.begin() + i);
			continue;
		}

		++i;
	}
}

void ThreadManager::RemoveThreadRequest (ThreadedComponent* localThreadComp, size_t engineIdx)
{
	for (size_t requestIdx = 0; requestIdx < PendingRequestData.size(); ++requestIdx)
	{
		if (PendingRequestData.at(requestIdx).ExternalEngineIdx == engineIdx)
		{
			for (size_t compIdx = 0; compIdx < PendingRequestData.at(requestIdx).PendingComponents.size(); ++compIdx)
			{
				if (localThreadComp == PendingRequestData.at(requestIdx).PendingComponents.at(compIdx))
				{
					PendingRequestData.at(requestIdx).PendingComponents.erase(PendingRequestData.at(requestIdx).PendingComponents.begin() + compIdx);
					return;
				}
			}

			return;
		}
	}
}

void ThreadManager::SetMaxNumRequests (Int newMaxNumRequests)
{
	MaxNumRequests = newMaxNumRequests;
}

void ThreadManager::NotifyNewDataComponents ()
{
	for (ThreadedComponent* comp : NewDataNotifyComponents)
	{
		comp->ReceiveNewData();
	}

	ContainerUtils::Empty(OUT NewDataNotifyComponents);
}

void ThreadManager::SetGracePeriod (Float newGracePeriod)
{
	GracePeriod = newGracePeriod;
}

MultiThreadEngineComponent* ThreadManager::GetThreadedEngine (size_t engineIdx) const
{
	Engine* engine = Engine::GetEngine(engineIdx);
	if (engine == nullptr || engine->IsShuttingDown())
	{
		return nullptr;
	}

	return MultiThreadEngineComponent::Find(engine->GetThreadID());
}

DString ThreadManager::GetEngineName (size_t engineIdx) const
{
#ifdef DEBUG_MODE
	Engine* engine = Engine::GetEngine(engineIdx);
	if (engine == nullptr)
	{
		return DString::CreateFormattedString(TXT("Unknown Engine at index %s"), DString::MakeString(engineIdx));
	}

	return engine->GetDebugName();
#else
	return DString::CreateFormattedString(TXT("Engine at index %s"), DString::MakeString(engineIdx));
#endif
}

DString ThreadManager::GetEngineName () const
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	return GetEngineName(localEngine->GetEngineIndex());
}

void ThreadManager::ProcessComponentInstantationRequests ()
{
	ThreadUtils::TryLock(ComponentInstantiationRequestMutex, [&]
	{
		while (ComponentInstantiationRequests.size() > 0)
		{
			if (ComponentInstantiationRequests.at(0).ExternalRequester->ExternalComponent == nullptr) //Protect against memory leaks.  Ensure there's only one connection per component.
			{
				ThreadedComponent* newComponent = ComponentInstantiationRequests.at(0).InstantiationMethod();
				if (newComponent == nullptr)
				{
					MultiThreadLog.Log(LogCategory::LL_Warning, TXT("An event handler failed to instantiate a threaded component.  The requesting thread component cannot pass data across thread boundaries."));
				}
				else
				{
					//Link up the two components
					ComponentInstantiationRequests.at(0).ExternalRequester->SetExternalComponent(newComponent);
					newComponent->SetExternalComponent(ComponentInstantiationRequests.at(0).ExternalRequester);

					newComponent->ExternalEngineIdx = ComponentInstantiationRequests.at(0).ExternalRequester->GetInternalEngineIdx();
				}
			}
			else
			{
				MultiThreadLog.Log(LogCategory::LL_Warning, TXT("A ThreadedComponent is not allowed to instantiate multiple thread listeners on the external thread."));
			}

			ComponentInstantiationRequests.erase(ComponentInstantiationRequests.begin());
		}
	});
}

void ThreadManager::TransferDataToExternalThread (SPendingRequestData& outPendingRequestData)
{
	MultiThreadEngineComponent* externalEngine = GetThreadedEngine(outPendingRequestData.ExternalEngineIdx);
	if (externalEngine == nullptr)
	{
		return;
	}

	ThreadManager* externalThreadManager = externalEngine->GetThreadManager();
	CHECK(externalThreadManager != nullptr && externalThreadManager != this)

	externalThreadManager->ExternalFreezeRequest();

	MultiThreadLog.Log(LogCategory::LL_Verbose, TXT("Locking %s to process local data requests."), GetEngineName());

	bThreadFrozen = true;

	//Wait for the external thread manager to lock before transfering data.
	while (!externalThreadManager->bThreadFrozen)
	{
		OS_Sleep(FreezeCheckFrequency);
	}
	bThreadFrozen = false;

	//Transfer data to the frozen thread.
	outPendingRequestData.FlushPendingComponents();
	externalThreadManager->ExternalUnfreezeRequest();

	MultiThreadLog.Log(LogCategory::LL_Verbose, TXT("Unlocking %s."), GetEngineName());
}

void ThreadManager::ExternalFreezeRequest ()
{
	std::lock_guard<std::mutex> guard(FreezeRequestMutex);
	NumExternalFreezeRequests++;
}

void ThreadManager::ExternalUnfreezeRequest ()
{
	std::lock_guard<std::mutex> guard(FreezeRequestMutex);
	NumExternalFreezeRequests--;
}
SD_END