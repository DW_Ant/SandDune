/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadUtils.cpp
=====================================================================
*/

#include "ThreadUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::ThreadUtils, SD::BaseUtils);
SD_BEGIN

bool ThreadUtils::TryLock (std::mutex& outMutex, std::function<void()> codeToExecute)
{
	if (outMutex.try_lock())
	{
		codeToExecute();
		outMutex.unlock();
		return true;
	}

	return false;
}
SD_END