/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacMultiThread.cpp
=====================================================================
*/

#include "PlatformMacMultiThread.h"

#ifdef PLATFORM_MAC

SD_BEGIN
int OS_CreateThread (SDThread& outThread, void** startRoutine, void* routineArgs, SDThread::SThreadInitData* initData)
{
#error Implement this function 'OS_CreateThread'
	return -1;
}
SD_END

#endif