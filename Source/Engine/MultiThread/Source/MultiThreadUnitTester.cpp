/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MultiThreadUnitTester.cpp
=====================================================================
*/

#include "MultiThreadClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::MultiThreadUnitTester, SD::UnitTester)

SD_BEGIN
/**
 * Creates and initializes a debug Engine object.  This function will then continuously loop until the engine shuts down.
 * This function will not return until the Debug Engine completely shuts down.
 */
SD_THREAD_FUNCTION(InitializeDebugEngine);

bool MultiThreadUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bPassed = true;

	if ((testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		//Note:  bPassed becomes true when the test was kicked off.  The async test may fail moments later.  Based on testFlags, it may throw an assertion if it fails.
		bPassed = LaunchThreadTester(testFlags);
	}

	return bPassed;
}

bool MultiThreadUnitTester::LaunchThreadTester (EUnitTestFlags testFlags) const
{
	//Create a thread that launches the Debug Engine
	SDThread newThread;
	THREAD_INIT_TYPE threadInitData;
	threadInitData.ThreadName = TXT("Debug Thread");
	int exitCode = OS_CreateThread(newThread, InitializeDebugEngine, nullptr, &threadInitData);
	if (exitCode != 0)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread tester.  Could not kick off a new thread that'll host the debug Engine.  Exit code:  %s"), DString::MakeString(exitCode));
		return false;
	}

	ThreadTester* oneWayTester = ThreadTester::CreateObject();
	if (oneWayTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread Tester.  Could not instantiate the one-way thread test object."));
		return false;
	}

	ThreadTester* twoWayTester = ThreadTester::CreateObject();
	if (twoWayTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread Tester.  Could not instantiate the two-way thread test object."));
		return false;
	}

	//In milliseconds
	Int timeoutTime = 5000;
	Int totalWaitTime = 0;
	Int sleepInterval = 500;
	//Wait for the DebugEngine to finish initializing before kicking off the ThreadTester test
	while (Engine::GetEngine(Engine::DEBUG_ENGINE_IDX) == nullptr || !Engine::GetEngine(Engine::DEBUG_ENGINE_IDX)->IsInitialized())
	{
		totalWaitTime += sleepInterval;
		if (totalWaitTime >= timeoutTime)
		{
			UnitTestError(testFlags, TXT("Failed to launch thread tester since the DebugEngine in the other thread was not instantiated.  The wait loop timed-out."));
			oneWayTester->Destroy();
			twoWayTester->Destroy();
			return false;
		}

		OS_Sleep(sleepInterval);
	}

	//The thread testers will destroy themselves automatically after their test completed.
	oneWayTester->BeginOneWayTest(testFlags);
	TestLog(testFlags, TXT("One-way thread tester test has begun."));
	twoWayTester->BeginTwoWayTest(testFlags);
	TestLog(testFlags, TXT("Two-way thread tester test has begun."));
	return true;
}

/**
 * Entry function the debug thread will start in.
 * This function simply sets up the DebugEngine instance with a MultiThreadEngineComponent.
 */
SD_THREAD_FUNCTION(InitializeDebugEngine)
{
	Engine* debugEngine = new Engine();
	CHECK(debugEngine != nullptr)

	MultiThreadEngineComponent* multiThreadEngine = new MultiThreadEngineComponent();

	std::vector<EngineComponent*> engineComponents; //This Engine will only have the MultiThreadEngineComponent
	engineComponents.push_back(multiThreadEngine);
	debugEngine->InitializeEngine(Engine::DEBUG_ENGINE_IDX, engineComponents);
	debugEngine->SetDebugName(TXT("Debug Engine"));

	if (ThreadManager* manager = multiThreadEngine->GetThreadManager())
	{
		//Have the debug thread manager pause for a 10th of a second during idle time.
		manager->SetGracePeriod(0.1f);
	}

	//Instantiate the watcher that'll determine when it's time to shutdown the DebugEngine.
	DebugEngineTestWatcher* testWatcher = DebugEngineTestWatcher::CreateObject();

	while (debugEngine != nullptr)
	{
		debugEngine->Tick();

		if (debugEngine->IsShuttingDown())
		{
			delete debugEngine;
			debugEngine = nullptr;
		}
	}

	SD_EXIT_THREAD
}
SD_END

#endif