/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DebugEngineTestWatcher.cpp
=====================================================================
*/

#include "DebugEngineTestWatcher.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::DebugEngineTestWatcher, SD::Object)
SD_BEGIN

void DebugEngineTestWatcher::InitProps ()
{
	Super::InitProps();

	bIsWatching = false;
}

void DebugEngineTestWatcher::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, DebugEngineTestWatcher, HandleGarbageCollection, void));
	}
}

void DebugEngineTestWatcher::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, DebugEngineTestWatcher, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

DebugEngineTestWatcher* DebugEngineTestWatcher::FindTestWatcher ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for (ObjectIterator iter(localEngine->GetObjectHashNumber(), false); iter.SelectedObject.IsValid(); iter++)
	{
		DebugEngineTestWatcher* watcher = dynamic_cast<DebugEngineTestWatcher*>(iter.SelectedObject.Get());
		if (watcher != nullptr)
		{
			return watcher;
		}
	}

	MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Unable to find an instance of the DebugEngineTestWatcher."));
	return nullptr;
}

void DebugEngineTestWatcher::AddWatchObject (Object* newWatchObject)
{
	//Ensure the object isn't already added
	for (Object* watchedObj : WatchedObjects)
	{
		if (watchedObj == newWatchObject)
		{
			MultiThreadLog.Log(LogCategory::LL_Warning, TXT("%s refused to add %s to the watch list since that object is already in the watch list."), ToString(), newWatchObject->ToString());
			return;
		}
	}

	WatchedObjects.push_back(newWatchObject);
	bIsWatching = true;
}

void DebugEngineTestWatcher::HandleGarbageCollection ()
{
	if (!bIsWatching)
	{
		return;
	}

	UINT_TYPE i = 0;
	while (i < WatchedObjects.size())
	{
		if (WatchedObjects.at(i)->GetPendingDelete())
		{
			WatchedObjects.erase(WatchedObjects.begin() + i);
			continue;
		}

		++i;
	}

	if (ContainerUtils::IsEmpty(WatchedObjects))
	{
		MultiThreadLog.Log(LogCategory::LL_Debug, TXT("Shutting down debug engine since all objects registered in the watcher were destroyed."));

		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
		Destroy();
	}
}
SD_END

#endif