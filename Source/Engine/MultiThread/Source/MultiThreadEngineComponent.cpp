/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MultiThreadEngineComponent.cpp
=====================================================================
*/

#include "MultiThreadEngineComponent.h"

IMPLEMENT_ENGINE_COMPONENT(SD::MultiThreadEngineComponent)
SD_BEGIN

MultiThreadEngineComponent::MultiThreadEngineComponent () : Super()
{
	bTickingComponent = true;
}

void MultiThreadEngineComponent::PostTick (Float deltaSec)
{
	Super::PostTick(deltaSec);

	if (ThreadController.EvaluateLockRequests(deltaSec))
	{
		ThreadController.NotifyNewDataComponents();
	}
}

ThreadManager* MultiThreadEngineComponent::GetThreadManager ()
{
	return &ThreadController;
}
SD_END