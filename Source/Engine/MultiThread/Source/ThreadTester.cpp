/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadTester.cpp
=====================================================================
*/

#include "DebugEngineTestWatcher.h"
#include "ThreadedComponent.h"
#include "ThreadTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::ThreadTester, SD::Entity)
SD_BEGIN

const std::vector<Int> ThreadTester::ExpectedData
{
	//Hard-code a series of 'random' numbers to test against timing/unpredictable race conditions.
	1683, 1292, 1828, 1327, 1344, 1620, 1819, 1039, 1441, 1990,
	-195, -118, -132, -179, -164, -117, -150, -181, -196, -125,
	2048, 2081, 2145, 2193, 2948, 2567, 2447, 2100, 2200, 2283,
	-294, -298, -281, -219, -281, -291, -200, -283, -256, -247,
	3921, 3948, 3628, 3184, 3478, 3184, 3648, 3170, 3084, 3942,
	-382, -310, -394, -391, -356, -346, -354, -358, -353, -341,
	4813, 4587, 4125, 4631, 4826, 4913, 4739, 4086, 4168, 4825,
	-486, -426, -407, -417, -482, -493, -465, -428, -486, -445,
	5826, 5284, 5590, 5570, 5501, 5530, 5040, 5194, 5379, 5482,
	-551, -553, -557, -559, -556, -554, -582, -528, -591, -537,
	6132, 6804, 6545, 6963, 6365, 6842, 6257, 6158, 6723, 6485,
	-639, -695, -624, -621, -685, -674, -618, -652, -691, -638,
	7432, 7415, 7418, 7862, 7895, 7843, 7651, 7086, 7913, 7824,
	-753, -741, -789, -748, -761, -742, -735, -761, -764, -705,
	8521, 8426, 8451, 8731, 8539, 8965, 8726, 8435, 8064, 8942,
	-843, -876, -871, -879, -836, -804, -894, -834, -891, -894,
	9761, 9714, 9784, 9852, 9856, 9864, 9531, 9641, 9082, 9431,
	-946, -963, -987, -953, -957, -958, -954, -956, -952, -950
};

const Range<Float> ThreadTester::DataIntervalRange(0.01f, 0.5f);

void ThreadTester::InitProps ()
{
	Super::InitProps();

	bIsOnMainThread = false;
	bIsTwoWayTest = false;
	DataWriteTime = DataIntervalRange.Min;
	LastDataWriteTime = 0.f;

	ThreadComp = nullptr;
	Tick = nullptr;

	DataReadTime = DataIntervalRange.Max; //DataCopyTime is going to follow inversely to DataTransferTime
	LastDataReadTime = 0.f;
	TimeReceiveTimeout = 10.f;
	LastReceivedTime = 0.f;

	SentDataIndex = UINT_INDEX_NONE;
	bSentInitialData = false;
	bStartedTest = false;
}

void ThreadTester::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	bIsOnMainThread = localEngine->IsMainEngine();

	Tick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, ThreadTester, HandleTick, void, Float));
		Tick->SetTicking(false); //wait for test to begin before ticking.
	}
}

void ThreadTester::Destroyed ()
{
	if (bStartedTest)
	{
		//Generate a UnitTest error if there was an error.
		EvaluateReceivedData();
	}

	Super::Destroyed();
}

bool ThreadTester::BeginOneWayTest (UnitTester::EUnitTestFlags testFlags)
{
	return ExecuteUnitTest(testFlags, false);
}

bool ThreadTester::BeginTwoWayTest (UnitTester::EUnitTestFlags testFlags)
{
	return ExecuteUnitTest(testFlags, true);
}

bool ThreadTester::ExecuteUnitTest (UnitTester::EUnitTestFlags testFlags, bool bInTwoWayTest)
{
	TestFlags = testFlags;
	bIsTwoWayTest = bInTwoWayTest;
	bStartedTest = true;

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		LastReceivedTime = localEngine->GetElapsedTime();
	}

	//Only instantiate a ThreadedComponent on the MainThread since the ThreadedComponent on the debug thread
	//will be instantiated through the ThreadManager's instantiation request (through the lambda specified below).
	if (bIsOnMainThread)
	{
		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->InstantiateExternalComp(Engine::DEBUG_ENGINE_IDX, [=]()
			{
				//Create a ThreadTester and ThreadedComponent on external (debug) thread
				ThreadTester* tester = ThreadTester::CreateObject();
				tester->ThreadComp = ThreadedComponent::CreateObject();
				if (tester->AddComponent(tester->ThreadComp))
				{
					bool bTestStarted = tester->ExecuteUnitTest(testFlags, bIsTwoWayTest);
					if (!bTestStarted)
					{
						UnitTester::UnitTestError_Static(testFlags, TXT("ThreadTester on the external thread failed to initialize its own test process."));
					}

					//The thread component will read data from the OnNewData callback instead of reading from tick. It'll also rely on auto clearing the data buffer.
					tester->ThreadComp->bAutoClearDataBuffer = true;
					tester->ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(tester, ThreadTester, HandleNewData, void, const DataBuffer&));

					//For the two-way test, the ThreadedComponent will need to know how to send data back to main thread.
					if (bIsTwoWayTest)
					{
						tester->ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(tester, ThreadTester, HandleDataTransfer, void, DataBuffer&));
					}

					//Add this component to the watch list so the DebugEngine doesn't terminate before the test finished.
					DebugEngineTestWatcher* localWatcher = DebugEngineTestWatcher::FindTestWatcher();
					CHECK(localWatcher != nullptr) //Make sure the watcher was instantiated on DebugEngine.
					localWatcher->AddWatchObject(tester);
				}

				return tester->ThreadComp.Get();
			});
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, ThreadTester, HandleDataTransfer, void, DataBuffer&));

			//The thread component in the main thread will not rely on auto clear data buffer, and it will read from the tick loop.
			ThreadComp->bAutoClearDataBuffer = false;
		}
		else
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("Failed to intialize ThreadTester test since it could not attach ThreadedComponent to tester."));
			return false;
		}
	}

	CHECK(Tick.IsValid())
	Tick->SetTicking(true);

	return true;
}

void ThreadTester::EvaluateReceivedData () const
{
	//For the one-way test, the ThreadTester on the main thread is not expecting any data
	if (!bIsTwoWayTest && bIsOnMainThread)
	{
		if (ReceivedData.size() != 0)
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("The ThreadTester test failed since the ThreadTester on the main thread received data during the one-way communication test."));
		}

		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	DString engineName = localEngine->GetDebugName();

	if (localEngine->IsShuttingDown())
	{
		//Don't evaluate data. If the main engine is shutting down (from user closing the window early), the data may have been interrupted (since the main engine also shuts down the Debug engine).
		return;
	}

	if (ReceivedData.size() != ExpectedData.size())
	{
		UnitTester::UnitTestError_Static(TestFlags, TXT("The ThreadTester test failed since the ThreadTester on the %s thread received %s message(s) when it should have received %s."), engineName, DString::MakeString(ReceivedData.size()), DString::MakeString(ExpectedData.size()));
		return;
	}

	for (UINT_TYPE i = 0; i < ReceivedData.size(); ++i)
	{
		if (ReceivedData.at(i) != ExpectedData.at(i))
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("The ThreadTester test failed since there was a data mismatch the ThreadTester on the %s thread received.  For the number index %s, the ThreadTester received %s instead of %s."), engineName, DString::MakeString(i), ReceivedData.at(i), ExpectedData.at(i));
			return;
		}
	}
}

bool ThreadTester::ShouldTerminateTest () const
{
	//Check if the connection was terminated (Only the ThreadTesters on the DebugThread should terminate this way).
	if (ThreadComp.IsNullptr() || ThreadComp->IsConnectionTerminated())
	{
		if (bIsOnMainThread)
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("ThreadTester test failed since the threaded component's connection terminated prematurely.  It's expected to have the ThreadedComponent on the main thread to terminate the connection rather than the ThreadedComponents on the Debug thread terminating the connection."));
		}

		UnitTester::TestLog(TestFlags, TXT("The ThreadTester is terminating since its ThreadedComponent is detached from the external thread."));
		return true;
	}

	//ThreadTesters on the debug thread will not terminate the test since we need to test the ThreadComponents detecting a disconnection.
	if (!bIsOnMainThread)
	{
		return false;
	}

	//For two way tests, the ThreadTester on the main thread should only terminate when the tester received enough data
	//since it must wait for DebugEngine to send data back.  The ThreadTester on DebugEngine would terminate after the one on the MainThread terminates connection.
	if (bIsTwoWayTest)
	{
		//Check if it received data recently
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		if (localEngine->GetElapsedTime() - LastReceivedTime >= TimeReceiveTimeout)
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("The two way test failed since the ThreadTester on the main thread did not receive data recently."));
			return true;
		}

		if (ReceivedData.size() == ExpectedData.size() && //Received enough data from external thread
			SentDataIndex != UINT_INDEX_NONE && SentDataIndex >= ExpectedData.size() - 1) //Sent enough data to external thread
		{
			UnitTester::TestLog(TestFlags, TXT("The ThreadTester on the main thread two way test terminated since it received enough data from the debug thread."));
			return true;
		}
	}
	else //OneWayTest
	{
		//For the one way test, the ThreadTester on main thread should terminate after sending the last number.
		if (SentDataIndex != UINT_INDEX_NONE && SentDataIndex >= ExpectedData.size() - 1)
		{
			UnitTester::TestLog(TestFlags, TXT("The ThreadTester on the main thread one way test terminated since it finished sending data to the debug thread."));
			return true;
		}
	}

	return false;
}

void ThreadTester::SendDataRequest ()
{
	bSentInitialData = true;

	//Notify the ThreadManager to lock both engines as soon as possible.
	ThreadComp->RequestDataTransfer(true);
}

void ThreadTester::ReadReceivedData ()
{
	if (ThreadComp.IsValid())
	{
		/*
		Normally, a DataBuffer contains multiple variables, and we would simply implement something like this.
		newData >> VarA;
		newData >> VarB;
		newData >> VarC;

		However, since we need to record history to validate the test, this data buffer is structured where
		numbers are always appended at the end.  Therefore, we copy each number in the ReceivedData vector.
		*/
		DataBuffer& newData = ThreadComp->EditExternalData();
		newData.JumpToBeginning();
		while (!newData.IsReaderAtEnd())
		{
			Int newNumber;
			newData >> newNumber;
			ReceivedData.push_back(newNumber);

			//UnitTester::TestLog(TestFlags, TXT("ThreadTester read %s from its ThreadedComponent's DataBuffer."), newNumber);
		}

		//Refresh the last received timestamp.
		if (newData.GetNumBytes() > 0)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)
			LastReceivedTime = localEngine->GetElapsedTime();
		}

		if (bIsOnMainThread)
		{
			//The ThreadTester on the debug thread has bAutoClearBuffer to true. Although it wouldn't hurt clearing an empty buffer, this unit test will check that that flag works as expected.
			//The ThreadTester on the main thread however does not have that flag set to true. This means it must clear the data buffer to prevent data accumulation.
			newData.EmptyBuffer();
		}
	}
}

void ThreadTester::HandleDataTransfer (DataBuffer& outExternalData)
{
	if (SentDataIndex == UINT_INDEX_NONE)
	{
		SentDataIndex = 0;
	}
	else
	{
		SentDataIndex++;
	}

	CHECK(SentDataIndex < ExpectedData.size())

	//Normally we would simply clear the buffer before pushing new data, but this unit test needs to track the history of data
	//pushed to the other thread to test if all requests were pushed in right order.
	//Therefore the data is appended rather than replacing existing data.
	outExternalData << ExpectedData.at(SentDataIndex);

	//UnitTester::TestLog(TestFlags, TXT("ThreadTester's ThreadComponent sent %s."), ExpectedData.at(SentDataIndex));
}

void ThreadTester::HandleNewData (const DataBuffer& outNewData)
{
	ReadReceivedData();
}

void ThreadTester::HandleTick (Float deltaSec)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	//See if it needs to read data
	if (bIsOnMainThread && localEngine->GetElapsedTime() - LastDataReadTime >= DataReadTime)
	{
		ReadReceivedData();

		//Change the read time frequency to simulate various work load.
		//Reverse the min/max bounds to make the interval travel in descending rates.
		DataReadTime = Utils::Lerp(Float::MakeFloat(ReceivedData.size()) / Float::MakeFloat(ExpectedData.size()), DataIntervalRange.Max, DataIntervalRange.Min);
		LastDataReadTime = localEngine->GetElapsedTime();
	}

	//If it can send data
	if (ThreadComp.IsValid() && ThreadComp->IsCopyMethodBounded())
	{
		//If it has data to send
		if (SentDataIndex == UINT_INDEX_NONE || SentDataIndex < ExpectedData.size() - 1)
		{
			//Has enough time elapsed to send data?
			if (localEngine->GetElapsedTime() - LastDataWriteTime >= DataWriteTime)
			{
				SendDataRequest();

				//Change the write time frequency to simulate various work load.
				DataWriteTime = (SentDataIndex == UINT_INDEX_NONE) ? DataIntervalRange.Min : Utils::Lerp(Float::MakeFloat(SentDataIndex) / Float::MakeFloat(ExpectedData.size()), DataIntervalRange.Min, DataIntervalRange.Max);
				LastDataWriteTime = localEngine->GetElapsedTime();
			}
		}
	}

	if (ShouldTerminateTest())
	{
		ReadReceivedData(); //Read data from buffer one last time (in case new data just transfered, but it didn't copy due to read interval).
		Destroy();
	}
}
SD_END

#endif