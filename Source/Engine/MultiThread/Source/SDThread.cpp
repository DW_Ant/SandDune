/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SDThread.cpp
=====================================================================
*/

#include "SDThread.h"

SD_BEGIN
SDThread::SThreadInitData::SThreadInitData ()
{
	ThreadName = DString::EmptyString;
}

SDThread::SThreadInitData::SThreadInitData (const SThreadInitData& copyObject)
{
	ThreadName = copyObject.ThreadName;
}

SDThread::SThreadInitData::~SThreadInitData ()
{
	//Noop
}

#ifdef PLATFORM_WINDOWS
SDThread::SWinThreadInitData::SWinThreadInitData () : SThreadInitData()
{
	SecurityAttributes = nullptr;
	StackSize = 0;
	CreationFlags = 0;
}

SDThread::SWinThreadInitData::SWinThreadInitData (const SWinThreadInitData& copyObject) : SThreadInitData()
{
	SecurityAttributes = copyObject.SecurityAttributes;
	StackSize = copyObject.StackSize;
	CreationFlags = copyObject.CreationFlags;
}

SDThread::SWinThreadInitData::~SWinThreadInitData ()
{
	//Noop
}
#endif

#ifdef PLATFORM_LINUX
SDThread::SpThreadInitData::SpThreadInitData () : SThreadInitData()
{
	int error = pthread_attr_init(&InitAttributes);
	CHECK(error == 0) //POSIX.1-2001 documents a rare error.  This error shouldn't happen unless there's a compatibility issue.
}

SDThread::SpThreadInitData::SpThreadInitData (const SpThreadInitData& copyObject) : SThreadInitData()
{
	InitAttributes = copyObject.InitAttributes;
}

SDThread::SpThreadInitData::~SpThreadInitData ()
{
	//Noop
}
#endif

SDThread::SDThread ()
{
	Thread = nullptr;
}

SDThread::SDThread (THREAD_OBJECT* inThread)
{
	Thread = inThread;
}

SDThread::SDThread (const SDThread& copyObject)
{
	Thread = copyObject.Thread;
}

SDThread::~SDThread ()
{
	//Noop
}
SD_END