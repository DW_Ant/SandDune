/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadedComponent.cpp
=====================================================================
*/

#include "MultiThreadEngineComponent.h"
#include "ThreadedComponent.h"
#include "ThreadManager.h"

IMPLEMENT_CLASS(SD::ThreadedComponent, SD::EntityComponent)
SD_BEGIN

void ThreadedComponent::InitProps ()
{
	Super::InitProps();

	bAutoClearDataBuffer = true;

	ExternalEngineIdx = UINT_INDEX_NONE;
	InternalEngineIdx = UINT_INDEX_NONE;

	{
		std::lock_guard<std::mutex> guard(ExternalComponentMutex);
		ExternalComponent = nullptr;
	}

	CopyMethod.ClearFunction();

	LocalThreadManager = nullptr;
	bEstablishedConnection = false;
	bPendingOnDisconnect = false;
}

void ThreadedComponent::BeginObject ()
{
	Super::BeginObject();

	InternalEngineIdx = Engine::FindEngine()->GetEngineIndex();

	MultiThreadEngineComponent* localThreadEngine = MultiThreadEngineComponent::Find();
	CHECK(localThreadEngine != nullptr);

	LocalThreadManager = localThreadEngine->GetThreadManager();
	CHECK(LocalThreadManager != nullptr);
}

void ThreadedComponent::Destroyed ()
{
	//Cancel any pending actions
	if (LocalThreadManager != nullptr)
	{
		if (ExternalEngineIdx != UINT_INDEX_NONE)
		{
			LocalThreadManager->RemoveThreadRequest(this, ExternalEngineIdx);
		}

		LocalThreadManager->CancelComponentInstantiation(this);
		LocalThreadManager = nullptr;
	}

	if (!IsConnectionTerminated())
	{
		DetachThreadedConnection();
	}

	Super::Destroyed();
}

bool ThreadedComponent::IsConnectionTerminated () const
{
	return (ExternalComponent == nullptr && bEstablishedConnection);
}

void ThreadedComponent::RequestDataTransfer (bool bForced)
{
	CHECK(LocalThreadManager != nullptr)
	if (!IsConnectionTerminated())
	{
		LocalThreadManager->AddThreadRequest(this, bForced, -1.f);
	}
}

void ThreadedComponent::RequestDataTransfer (Float maxWaitTime)
{
	//Ensure the developer has invoked InstantiateListener before making requests
	CHECK(ExternalEngineIdx != UINT_INDEX_NONE)

	CHECK(LocalThreadManager != nullptr)
	if (!IsConnectionTerminated())
	{
		LocalThreadManager->AddThreadRequest(this, false, maxWaitTime);
	}
}

void ThreadedComponent::InstantiateExternalComp (UINT_TYPE inExternalEngineIdx, std::function<ThreadedComponent*()> instantiationMethod)
{
	if (inExternalEngineIdx > Engine::GetMaxNumPossibleEngines() - 1)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the given external engine index (%s) is not a valid index.  Valid indices range from 0-%s."), Int(inExternalEngineIdx), Engine::GetMaxNumPossibleEngines());
		return;
	}

	//The external engine index must be an external engine
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	if (localEngine->GetEngineIndex() == inExternalEngineIdx)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the given external engine index (%s) is the index of the engine that's in the same thread.  The index must be an index to an engine in a separate thread."), Int(inExternalEngineIdx));
		return;
	}

	ExternalEngineIdx = inExternalEngineIdx;

	//Find the external thread ID through the external engine
	Engine* externalEngine = Engine::GetEngine(ExternalEngineIdx);
	CHECK(externalEngine != nullptr)
	MultiThreadEngineComponent* externalEngineComp = MultiThreadEngineComponent::Find(externalEngine->GetThreadID());
	if (externalEngineComp == nullptr)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the specified engine (of index %s) does not contain a MultiThreadEngineComponent."), Int(ExternalEngineIdx));
		return;
	}

	ThreadManager* externalThreadManager = externalEngineComp->GetThreadManager();
	CHECK(externalThreadManager != nullptr)

	//The external ThreadedComponent must reside in other thread.  Notify external thread manager about the instantiation request.
	externalThreadManager->InstantiateExternalThreadComponent(this, instantiationMethod);
}

bool ThreadedComponent::IsCopyMethodBounded () const
{
	return CopyMethod.IsBounded();
}

void ThreadedComponent::ReceiveNewData ()
{
	if (ExternalData.GetNumBytes() > 0)
	{
		if (OnNewData.IsBounded())
		{
			while (!ExternalData.IsReaderAtEnd())
			{
				OnNewData(OUT ExternalData);
			}
		}

		if (bAutoClearDataBuffer)
		{
			ExternalData.EmptyBuffer();
		}
	}
}

void ThreadedComponent::SetCopyMethod (SDFunction<void, DataBuffer&> newCopyMethod)
{
	CopyMethod = newCopyMethod;
}

void ThreadedComponent::SetOnNewData (SDFunction<void, const DataBuffer&> newOnNewData)
{
	OnNewData = newOnNewData;
}

void ThreadedComponent::SetOnDisconnect (const SDFunction<void, ThreadedComponent*>& newOnDisconnect)
{
	SDFunction<void, Float, TickComponent*> tickCallback = SDFUNCTION_2PARAM(this, ThreadedComponent, HandleCheckDisconnect, void, Float, TickComponent*);

	//Remove the check delegate tick if there is one.
	if (OnDisconnect.IsBounded())
	{
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (TickComponent* tick = dynamic_cast<TickComponent*>(iter.GetSelectedComponent()))
			{
				if (tick->ReadTickExHandler() == tickCallback)
				{
					tick->Destroy();
					break;
				}
			}
		}
	}

	OnDisconnect = newOnDisconnect;
	if (OnDisconnect.IsBounded())
	{
		//Create ticking component responsible for checking if the external component disconnected from this component.
		TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_MISC);
		if (AddComponent(tick))
		{
			tick->SetTickExHandler(tickCallback);
			tick->SetTickInterval(1.f);
		}
	}
}

void ThreadedComponent::ExecuteCopyMethod ()
{
	CHECK(CopyMethod.IsBounded())
	if (ExternalComponent != nullptr)
	{
#ifdef DEBUG_MODE
		//Ensure the other thread is frozen
		CHECK(ExternalComponent->LocalThreadManager->IsThreadFrozen())
#endif

		//Execute method that'll populate the data buffer.
		CopyMethod(OUT ExternalComponent->ExternalData);

		CHECK(ExternalComponent->LocalThreadManager != nullptr)
		ExternalComponent->LocalThreadManager->NewDataNotifyComponents.push_back(ExternalComponent);
	}
}

void ThreadedComponent::DetachThreadedConnection ()
{
	bool invokeCallback = false;
	{
		//Sever connection between the two threaded components
		std::lock_guard<std::mutex> guard(ExternalComponentMutex);
		if (ExternalComponent != nullptr)
		{
			ExternalComponent->bPendingOnDisconnect = true; //Ensure the OnDisconnect callback is executed on the correct thread.
			ExternalComponent->ExternalComponent = nullptr;
			ExternalComponent = nullptr;
			invokeCallback = true;
		}
	}

	if (invokeCallback && OnDisconnect.IsBounded())
	{
		OnDisconnect(this);
	}
}

void ThreadedComponent::SetExternalComponent (ThreadedComponent* newExternalComponent)
{
	std::lock_guard<std::mutex> guard(ExternalComponentMutex);
	CHECK(ExternalComponent == nullptr)
	ExternalComponent = newExternalComponent;
	bEstablishedConnection = true;
}

void ThreadedComponent::HandleCheckDisconnect (Float deltaSec, TickComponent* tick)
{
	if (bPendingOnDisconnect)
	{
		if (OnDisconnect.IsBounded())
		{
			OnDisconnect(this);
		}

		bPendingOnDisconnect = false;
		tick->Destroy();
	}
}
SD_END