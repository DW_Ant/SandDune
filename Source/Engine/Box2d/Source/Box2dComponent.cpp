/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dComponent.cpp
=====================================================================
*/

#include "Box2dComponent.h"
#include "Box2dEngineComponent.h"
#include "Box2dUtils.h"

IMPLEMENT_CLASS(SD::Box2dComponent, SD::EntityComponent)
SD_BEGIN

b2BodyDef Box2dComponent::BodyInit;
const unsigned int Box2dComponent::COLLISION_FLAG_ENTITY =			0x00000001;

void Box2dComponent::InitProps ()
{
	Super::InitProps();

	OverwritePosition = false;
	OverwriteVelocity = false;
	OverwriteRotation = false;

	PhysicsTick = nullptr;
	PhysicsBody = nullptr;

	Acceleration = Vector3::ZERO_VECTOR;
	Velocity = Vector3::ZERO_VECTOR;

	PrevPosition = Vector3::ZERO_VECTOR;
	PrevVelocity = Velocity;
	PrevRotation = Rotator::ZERO_ROTATOR;
}

void Box2dComponent::BeginObject ()
{
	Super::BeginObject();

	Box2dEngineComponent* boxEngine = Box2dEngineComponent::Find();
	CHECK(boxEngine != nullptr)

	boxEngine->OnPrePhysics.RegisterHandler(SDFUNCTION_1PARAM(this, Box2dComponent, HandlePrePhysics, void, Float));
	boxEngine->OnPostPhysics.RegisterHandler(SDFUNCTION_1PARAM(this, Box2dComponent, HandlePostPhysics, void, Float));
}

void Box2dComponent::Destroyed ()
{
	Box2dEngineComponent* boxEngine = Box2dEngineComponent::Find();
	if (boxEngine != nullptr) //If the engine component is not yet destroyed
	{
		boxEngine->OnPrePhysics.UnregisterHandler(SDFUNCTION_1PARAM(this, Box2dComponent, HandlePrePhysics, void, Float));
		boxEngine->OnPostPhysics.UnregisterHandler(SDFUNCTION_1PARAM(this, Box2dComponent, HandlePostPhysics, void, Float));

		if (PhysicsBody != nullptr)
		{
			boxEngine->DestroyPhysicsBody(PhysicsBody);
		}
	}

	Super::Destroyed();
}

bool Box2dComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (dynamic_cast<SceneTransform*>(ownerCandidate) != nullptr)
	{
		return Super::CanBeAttachedTo(ownerCandidate);
	}

	Box2dLog.Log(LogCategory::LL_Warning, TXT("Box 2D Components can only attach to Scene Transformable Entities."));
	return false;
}

void Box2dComponent::PostEngineInitialize () const
{
	Super::PostEngineInitialize();

	//Initialize of BodyInit's member variables that will not change.
	BodyInit.enabled = true;
	BodyInit.awake = true;
	BodyInit.allowSleep = true;

	BodyInit.angularVelocity = 0.f;
	BodyInit.gravityScale = 1.f;
}

void Box2dComponent::InitializePhysics (const SPhysicsInitData& initData)
{
	if (PhysicsBody != nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot reinitialize physics after the Box2dComponent already has an associated physics body."));
		return;
	}

	Box2dEngineComponent* boxEngine = Box2dEngineComponent::Find();
	CHECK(boxEngine != nullptr)

	SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(GetOwner());
	if (ownerTransform == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize Box2dComponent since it is not yet attached to a Scene Transform."));
		return;
	}

	b2World& world = boxEngine->EditWorldPhysics();

	switch (initData.PhysicsType)
	{
		case(PT_Static):
			BodyInit.type = b2_staticBody;
			break;

		case (PT_Kinematic):
			BodyInit.type = b2_kinematicBody;
			break;

		case (PT_Dynamic):
			BodyInit.type = b2_dynamicBody;
			break;
	}

	PrevPosition = ownerTransform->GetTranslation();
	PrevVelocity = Velocity;
	PrevRotation = ownerTransform->GetRotation();

	BodyInit.position = Box2dUtils::ToBox2dCoordinates(ownerTransform->ReadTranslation());
	BodyInit.linearVelocity = Box2dUtils::ToBox2dCoordinates(Velocity);
	BodyInit.angle = Box2dUtils::ToBox2dRotation(ownerTransform->GetRotation());

	BodyInit.bullet = initData.EnableContinuousCollision;
	BodyInit.fixedRotation = initData.IsFixedRotation;

	BodyInit.angularDamping = initData.AngularDamping.Value;
	BodyInit.linearDamping = initData.LinearDamping.Value;
	BodyInit.userData = this;

	PhysicsBody = world.CreateBody(&BodyInit);
	CHECK(PhysicsBody != nullptr)
}

void Box2dComponent::SetCollisionCategories (uint16 collisionCategory)
{
	if (PhysicsBody == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a Box2dComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.categoryBits = collisionCategory;
		fixture->SetFilterData(collisionFilter);
	}
}

void Box2dComponent::SetCollisionMasks (uint16 collisionMasks)
{
	if (PhysicsBody == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a Box2dComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.maskBits = collisionMasks;
		fixture->SetFilterData(collisionFilter);
	}
}

void Box2dComponent::SetCollisionGroup (int16 collisionGroup)
{
	if (PhysicsBody == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a Box2dComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.groupIndex = collisionGroup;
		fixture->SetFilterData(collisionFilter);
	}
}

void Box2dComponent::SetCollisionData (uint16 collisionCategories, uint16 collisionMasks, int16 collisionGroup)
{
	if (PhysicsBody == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a Box2dComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();

		collisionFilter.categoryBits = collisionCategories;
		collisionFilter.maskBits = collisionMasks;
		collisionFilter.groupIndex = collisionGroup;

		fixture->SetFilterData(collisionFilter);
	}
}

void Box2dComponent::SetAcceleration (const Vector3& newAcceleration)
{
	Acceleration = newAcceleration;
}

void Box2dComponent::SetVelocity (const Vector3& newVelocity)
{
	Velocity = newVelocity;
}

bool Box2dComponent::CanBlockComponent (Box2dComponent* other, b2Contact* contact, const b2Manifold* oldManifold) const
{
	return true;
}

bool Box2dComponent::CanBlockComponent (Box2dComponent* other) const
{
	return true;
}

void Box2dComponent::HandlePrePhysics (Float deltaSec)
{
	if (PhysicsBody == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("A Box2dComponent is still registered to the Box2dEngineComponent's Physics callback without having a PhysicsBody."));
		return;
	}

	SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(GetOwner());
	if (ownerTransform == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("A Box2dComponent was detached from its owner SceneTransform. It cannot simulate physics without a transformation."));
		return;
	}

	//If the game overwrote this component's velocity or position, notify the physics engine before it runs its cycle.
	if (PrevPosition != ownerTransform->GetTranslation() || PrevRotation != ownerTransform->GetRotation())
	{
		b2Vec2 box2Pos = Box2dUtils::ToBox2dCoordinates(ownerTransform->GetTranslation());
		float box2Rot = Box2dUtils::ToBox2dRotation(ownerTransform->GetRotation());
		PhysicsBody->SetTransform(box2Pos, box2Rot);
	}

	//Change velocity when a change is detected or when there's continuous acceleration.
	if (PrevVelocity != Velocity || !Acceleration.IsEmpty())
	{
		b2Vec2 newVelocity = Box2dUtils::ToBox2dCoordinates(Velocity + (Acceleration * deltaSec));
		PhysicsBody->SetLinearVelocity(newVelocity);
	}
}

void Box2dComponent::HandlePostPhysics (Float deltaSec)
{
	if (PhysicsBody != nullptr)
	{
		PrevPosition = Box2dUtils::To3dSdCoordinates(PhysicsBody->GetPosition());
		PrevVelocity = Box2dUtils::To3dSdCoordinates(PhysicsBody->GetLinearVelocity());
		PrevRotation = Box2dUtils::ToSdRotation(PhysicsBody->GetAngle());

		if (!OverwriteVelocity)
		{
			Velocity.X = PrevVelocity.X;
			Velocity.Y = PrevVelocity.Y;
		}

		if (SceneTransform* owner = dynamic_cast<SceneTransform*>(GetOwner()))
		{
			if (!OverwritePosition)
			{
				owner->EditTranslation().X = PrevPosition.X;
				owner->EditTranslation().Y = PrevPosition.Y;
			}

			if (!OverwriteRotation)
			{
				owner->SetRotation(PrevRotation);
			}
		}

		OverwritePosition = false;
		OverwriteVelocity = false;
		OverwriteRotation = false;
	}
}

void Box2dComponent::HandleBeginContact (Box2dComponent* otherComp, b2Contact* contactData)
{
	if (OnBeginContact.IsBounded())
	{
		OnBeginContact(otherComp, contactData);
	}
}

void Box2dComponent::HandleEndContact (Box2dComponent* otherComp, b2Contact* contactData)
{
	if (OnEndContact.IsBounded())
	{
		OnEndContact(otherComp, contactData);
	}
}

void Box2dComponent::HandleCollision (Box2dComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse)
{
	if (OnCollision.IsBounded())
	{
		OnCollision(otherComp, contactData, impulse);
	}
}
SD_END