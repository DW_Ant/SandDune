/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dUnitTester.cpp
=====================================================================
*/

#include "Box2dUnitTester.h"
#include "Box2dUtils.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::Box2dUnitTester, SD::UnitTester)
SD_BEGIN

bool Box2dUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool success = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		success = TestConversions(testFlags);
	}

	return success;
}

bool Box2dUnitTester::TestConversions (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Box 2D Conversions"));

	//This test essentially validates that the Box2D <-> Sand Dune conversion functions are symmetrical.
	Float originalDist = 15.2f;
	Vector2 originalPos(-20.4f, 55.6f);
	Rotator originalRot(125.f, 0.f, 0.f, Rotator::RU_Degrees);

	float boxDist = Box2dUtils::ToBox2dDist(originalDist);
	b2Vec2 boxPos = Box2dUtils::ToBox2dCoordinates(originalPos);
	float boxRot = Box2dUtils::ToBox2dRotation(originalRot);

	//Convert it back to see if they yield the same results as the original.
	Float convertedDist = Box2dUtils::ToSdDist(boxDist);
	Vector2 convertedPos = Box2dUtils::ToSdCoordinates(boxPos);
	Rotator convertedRot = Box2dUtils::ToSdRotation(boxRot);

	//Compare the values
	if (originalDist != convertedDist)
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original distance value (%s) does not equal to the converted distance value (%s)."), originalDist, convertedDist);
		return false;
	}

	if (originalPos != convertedPos)
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original coordinates (%s) do not equal to the converted coordinates (%s)."), originalPos, convertedPos);
		return false;
	}

	Int deltaYaw = Int(originalRot.Yaw) - Int(convertedRot.Yaw);
	deltaYaw.AbsInline();
	if (deltaYaw > 1) //1 to handle some degree of tolerance for floating point precision conversion errors.
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original rotation (%s) does not equal to the converted rotation (%s)."), originalRot, convertedRot);
		return false;
	}

	ExecuteSuccessSequence(testFlags, TXT("Box 2D Conversions"));
	return true;
}
SD_END

#endif