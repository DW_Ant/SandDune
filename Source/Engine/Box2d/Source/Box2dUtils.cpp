/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dUtils.cpp
=====================================================================
*/

#include "Box2dUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::Box2dUtils, SD::BaseUtils)
SD_BEGIN

//Sand Dune uses centimeters. Box2D uses meters.
float Box2dUtils::ToBox2dDist (Float sdDist)
{
	return sdDist.Value * 0.01f;
}

Float Box2dUtils::ToSdDist (float box2dDist)
{
	return box2dDist * 100.f;
}

//Sand Dune uses a left handed coordinate system. X right, Y backwards, Z up.
//Box 2D coordinate system is X right, Y forward.
b2Vec2 Box2dUtils::ToBox2dCoordinates (const Vector2& sdVect)
{
	return b2Vec2(ToBox2dDist(sdVect.X), -ToBox2dDist(sdVect.Y));
}

b2Vec2 Box2dUtils::ToBox2dCoordinates (const Vector3& sdVect)
{
	return b2Vec2(ToBox2dDist(sdVect.X), -ToBox2dDist(sdVect.Y));
}

Vector2 Box2dUtils::ToSdCoordinates (const b2Vec2& b2Vect)
{
	return Vector2(ToSdDist(b2Vect.x), -ToSdDist(b2Vect.y));
}

Vector3 Box2dUtils::To3dSdCoordinates (const b2Vec2& b2Vect)
{
	return Vector3(ToSdDist(b2Vect.x), -ToSdDist(b2Vect.y), 0.f);
}

float Box2dUtils::ToBox2dRotation (Rotator sdRotation)
{
	//Only yaw values are considered being a 2D coordinate system.
	return sdRotation.GetYaw(Rotator::RU_Radians).Value;
}

Rotator Box2dUtils::ToSdRotation (float box2dRotation)
{
	return Rotator(box2dRotation, 0.f, 0.f, Rotator::RU_Radians);
}

b2PolygonShape Box2dUtils::GetBox2dRectangle (const Rectangle& dimensions)
{
	b2PolygonShape results;

	//SetAsBox takes in half height and half width
	results.SetAsBox(ToBox2dDist(dimensions.Width * 0.5f), ToBox2dDist(dimensions.Height * 0.5f));

	return results;
}

SD_END