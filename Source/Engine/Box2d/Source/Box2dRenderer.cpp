/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dRenderer.cpp
=====================================================================
*/

#include "Box2dRenderer.h"
#include "Box2dComponent.h"
#include "Box2dUtils.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::Box2dRenderer, SD::RenderComponent)
SD_BEGIN

void Box2dRenderer::InitProps ()
{
	Super::InitProps();

	FillColor = Color(0, 0, 0, 0); //invisible
	OutlineColor = sf::Color::White;
}

void Box2dRenderer::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const Box2dRenderer* copyObj = dynamic_cast<const Box2dRenderer*>(objTemplate);
	if (copyObj != nullptr)
	{
		FillColor = copyObj->FillColor;
		OutlineColor = copyObj->OutlineColor;

		for (size_t i = 0; i < copyObj->DrawData.size(); ++i)
		{
			SDrawData newDrawData;
			newDrawData.ShapeType = copyObj->DrawData.at(i).ShapeType;
			switch(newDrawData.ShapeType)
			{
				case(ST_Polygon) :
				{
					ContainerUtils::Empty(newDrawData.PolygonData.Vertices);
					newDrawData.PolygonData.Vertices.resize(copyObj->DrawData.at(i).PolygonData.Vertices.size());
					for (UINT_TYPE vertexIdx = 0; vertexIdx < copyObj->DrawData.at(i).PolygonData.Vertices.size(); ++vertexIdx)
					{
						newDrawData.PolygonData.Vertices.at(vertexIdx) = copyObj->DrawData.at(i).PolygonData.Vertices.at(vertexIdx);
					}
					break;
				}
				case(ST_Circle) :
					newDrawData.CircleData.Radius = copyObj->DrawData.at(i).CircleData.Radius;
					break;
			}

			DrawData.push_back(newDrawData);
		}
	}
}

void Box2dRenderer::Render (RenderTarget* renderTarget, const Camera* camera)
{
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	if (ContainerUtils::IsEmpty(DrawData))
	{
		Box2dComponent* physComp = FindPhysicsComponent();
		CHECK(physComp != nullptr)

		GetDrawShapesDataFrom(physComp);
	}

	for (const SDrawData& drawData : DrawData)
	{
		switch (drawData.ShapeType)
		{
			case(ST_Unknown) :
				Box2dLog.Log(LogCategory::LL_Warning, TXT("Attempted to draw physics renderer without a shape type defined."));
				break;

			case(ST_Polygon) :
			{
				sf::ConvexShape renderedShape(drawData.PolygonData.Vertices.size());

				renderedShape.setPosition(projectionData.Position);
				renderedShape.setScale(projectionData.Scale);
				renderedShape.setRotation(projectionData.Rotation);

				for (UINT_TYPE i = 0; i < drawData.PolygonData.Vertices.size(); ++i)
				{
					renderedShape.setPoint(i, drawData.PolygonData.Vertices.at(i));
				}

				renderedShape.setOutlineColor(OutlineColor.Source);
				renderedShape.setFillColor(FillColor.Source);
				renderTarget->Draw(this, camera, renderedShape);
				break;
			}

			case(ST_Circle) :
			{
				sf::CircleShape renderedShape;

				renderedShape.setPosition(projectionData.Position);
				renderedShape.setScale(projectionData.Scale);
				renderedShape.setRotation(projectionData.Rotation);
				renderedShape.setOrigin((sf::Vector2f(1.f, 1.f) * drawData.CircleData.Radius.Value)); //box2d circles are relative to the center.
				renderedShape.setRadius(drawData.CircleData.Radius.Value);

				renderedShape.setOutlineColor(OutlineColor.Source);
				renderedShape.setFillColor(FillColor.Source);
				renderTarget->Draw(this, camera, renderedShape);
				break;
			}
		}
	}
}

Aabb Box2dRenderer::GetAabb () const
{
	Float leftMost = MAX_FLOAT;
	Float forwardMost = -MAX_FLOAT;
	Float rightMost = -MAX_FLOAT;
	Float backwardMost = MAX_FLOAT;

	for (const SDrawData& drawData : DrawData)
	{
		switch(drawData.ShapeType)
		{
			case(ST_Circle):
				leftMost = Utils::Min(leftMost, -drawData.CircleData.Radius);
				forwardMost = Utils::Max(forwardMost, drawData.CircleData.Radius);
				rightMost = Utils::Max(rightMost, drawData.CircleData.Radius);
				backwardMost = Utils::Min(backwardMost, -drawData.CircleData.Radius);
				break;

			case(ST_Polygon):
			{
				for (sf::Vector2f vertex : drawData.PolygonData.Vertices)
				{
					leftMost = Utils::Min<float>(leftMost.Value, vertex.y);
					forwardMost = Utils::Max<float>(forwardMost.Value, vertex.x);
					rightMost = Utils::Max<float>(rightMost.Value, vertex.y);
					backwardMost = Utils::Min<float>(backwardMost.Value, vertex.x);
				}

				break;
			}
		}
	}

	return Aabb(forwardMost, backwardMost, rightMost, leftMost, 0.f, 0.f);
}

void Box2dRenderer::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	Box2dComponent* physComp = FindPhysicsComponent();
	CHECK(physComp != nullptr)

	GetDrawShapesDataFrom(physComp);
}

void Box2dRenderer::ComponentDetached ()
{
	ContainerUtils::Empty(OUT DrawData);

	Super::ComponentDetached();
}

Box2dComponent* Box2dRenderer::FindPhysicsComponent ()
{
	CHECK(GetOwner() != nullptr);

	return dynamic_cast<Box2dComponent*>(GetOwner()->FindComponent(Box2dComponent::SStaticClass(), false));
}

void Box2dRenderer::GetDrawShapesDataFrom (Box2dComponent* physComp)
{
	CHECK(physComp != nullptr);

	b2Body* physBody = physComp->GetPhysicsBody();
	if (physBody == nullptr)
	{
		//PhyscsComponent has not initialized its physics data. Try again later.
		ContainerUtils::Empty(DrawData);
		return;
	}

	for (b2Fixture* fixture = physBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		SDrawData newDrawData;
		if (b2CircleShape* circleShape = dynamic_cast<b2CircleShape*>(fixture->GetShape()))
		{
			newDrawData.ShapeType = ST_Circle;
			newDrawData.CircleData.Radius = Box2dUtils::ToSdDist(circleShape->m_radius);
		}
		else if (b2PolygonShape* polyShape = dynamic_cast<b2PolygonShape*>(fixture->GetShape()))
		{
			newDrawData.ShapeType = ST_Polygon;

			ContainerUtils::Empty(newDrawData.PolygonData.Vertices);
			newDrawData.PolygonData.Vertices.resize(polyShape->m_count);
			for (UINT_TYPE i = 0; i < newDrawData.PolygonData.Vertices.size(); ++i)
			{
				newDrawData.PolygonData.Vertices.at(i) = Vector2::SDtoSFML(Box2dUtils::ToSdCoordinates(polyShape->m_vertices[i]));
			}
		}
		else
		{
			Box2dLog.Log(LogCategory::LL_Warning, TXT("Box2dRenderer is unable to render %s since its shape is not implemented."), physComp->ToString());
		}

		DrawData.push_back(newDrawData);
	}
}

SD_END
#endif