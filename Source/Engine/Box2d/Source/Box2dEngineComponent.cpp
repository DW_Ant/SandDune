/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dEngineComponent.cpp
=====================================================================
*/

#include "Box2dComponent.h"
#include "Box2dEngineComponent.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD::Box2dEngineComponent)

Box2dEngineComponent::Box2dEngineComponent () : EngineComponent(),
	DefaultGravity(Vector3::ZERO_VECTOR),
	WorldPhysics(b2Vec2(0.f, 0.f)),
	PhysicsTicker(nullptr),
	NumPhysIterationsPerFrame(5)
{
	//Noop
}

Box2dEngineComponent::~Box2dEngineComponent ()
{
	//Noop
}

void Box2dEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	WorldPhysics.SetContactListener(this);
	WorldPhysics.SetContactFilter(this);

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_BOX2D, TICK_GROUP_PRIORITY_BOX2D);

#ifdef DEBUG_MODE
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, Box2dEngineComponent, HandlePreGarbageCollection, void));
#endif
}

void Box2dEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	PhysicsTicker = Entity::CreateObject();
	CHECK(PhysicsTicker != nullptr)

	TickComponent* comp = TickComponent::CreateObject(TICK_GROUP_BOX2D);
	if (PhysicsTicker->AddComponent(comp))
	{
		comp->SetTickHandler(SDFUNCTION_1PARAM(this, Box2dEngineComponent, HandleTickPhysics, void, Float));
	}
}

void Box2dEngineComponent::ShutdownComponent ()
{
	if (PhysicsTicker != nullptr)
	{
		PhysicsTicker->Destroy();
		PhysicsTicker = nullptr;
	}

#ifdef DEBUG_MODE
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, Box2dEngineComponent, HandlePreGarbageCollection, void));
#endif

	Super::ShutdownComponent();
}

void Box2dEngineComponent::BeginContact (b2Contact* contact)
{
	CHECK(contact != nullptr)
	Box2dComponent* physA = nullptr;
	Box2dComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleBeginContact(physB, contact);
	physB->HandleBeginContact(physA, contact);
}

void Box2dEngineComponent::EndContact (b2Contact* contact)
{
	CHECK(contact != nullptr)
	Box2dComponent* physA = nullptr;
	Box2dComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleEndContact(physB, contact);
	physB->HandleEndContact(physA, contact);
}

void Box2dEngineComponent::PreSolve (b2Contact* contact, const b2Manifold* oldManifold)
{
	CHECK(contact != nullptr)
	Box2dComponent* physA = nullptr;
	Box2dComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	contact->SetEnabled((physA->CanBlockComponent(physB, contact, oldManifold) && physB->CanBlockComponent(physA, contact, oldManifold)));
}

void Box2dEngineComponent::PostSolve (b2Contact* contact, const b2ContactImpulse* impulse)
{
	CHECK(contact != nullptr)
	Box2dComponent* physA = nullptr;
	Box2dComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleCollision(physB, contact, impulse);
	physB->HandleCollision(physA, contact, impulse);
}

bool Box2dEngineComponent::ShouldCollide (b2Fixture* fixtureA, b2Fixture* fixtureB)
{
	//First apply default behavior from Box2d. If box 2d does not permit collision, then return early.
	if (!b2ContactFilter::ShouldCollide(fixtureA, fixtureB))
	{
		return false;
	}

	CHECK(fixtureA != nullptr && fixtureB != nullptr)

	//Get PhysicsComponents from fixtures.
	b2Body* bodyA = fixtureA->GetBody();
	b2Body* bodyB = fixtureB->GetBody();
	if (bodyA == nullptr || bodyB == nullptr)
	{
		return false;
	}

	Box2dComponent* physA = reinterpret_cast<Box2dComponent*>(bodyA->GetUserData());
	Box2dComponent* physB = reinterpret_cast<Box2dComponent*>(bodyB->GetUserData());

	return (physA->CanBlockComponent(physB) && physB->CanBlockComponent(physA));
}

void Box2dEngineComponent::DestroyPhysicsBody (b2Body* body)
{
#if ENABLE_COMPLEX_CHECKING
	CHECK(ContainerUtils::FindInVector(PendingBodyDestruction, body) == UINT_INDEX_NONE)
#endif

	PendingBodyDestruction.push_back(body);
}

void Box2dEngineComponent::SetPhysicsUpdateRate (Float newUpdateRate, Int numStepsPerUpdate)
{
	NumPhysIterationsPerFrame = numStepsPerUpdate;

	TickComponent* physTickComp = dynamic_cast<TickComponent*>(PhysicsTicker->FindComponent(TickComponent::SStaticClass(), false));
	if (physTickComp == nullptr)
	{
		Box2dLog.Log(LogCategory::LL_Warning, TXT("Unable to update the physics rate to %s since the Box2dEngineComponent is unable to find the Tick Component responsible for updating the Box2D engine!"), newUpdateRate);
		return;
	}

	physTickComp->SetTickInterval(newUpdateRate);
}

void Box2dEngineComponent::SetDefaultGravity (const Vector3& newDefaultGravity)
{
	DefaultGravity = newDefaultGravity;
}

bool Box2dEngineComponent::GetPhysicsComponentsFromContact (b2Contact* contactData, Box2dComponent*& outPhysCompA, Box2dComponent*& outPhysCompB)
{
	b2Fixture* fixtureA = contactData->GetFixtureA();
	b2Fixture* fixtureB = contactData->GetFixtureB();
	if (fixtureA == nullptr || fixtureB == nullptr)
	{
		return false;
	}

	const b2Body* bodyA = fixtureA->GetBody();
	const b2Body* bodyB = fixtureB->GetBody();
	if (bodyA == nullptr || bodyB == nullptr)
	{
		return false;
	}

	if (bodyA->GetUserData() == nullptr || bodyB->GetUserData() == nullptr)
	{
		return false;
	}

	outPhysCompA = reinterpret_cast<Box2dComponent*>(bodyA->GetUserData());
	outPhysCompB = reinterpret_cast<Box2dComponent*>(bodyB->GetUserData());
	return true;
}

void Box2dEngineComponent::HandleTickPhysics (Float deltaSec)
{
	//World Physics should not be locked at this time. The simulation should start and finish within a tick cycle.
	CHECK(!WorldPhysics.IsLocked())

	//Remove all pending physics bodies before simulating physics.
	while (!ContainerUtils::IsEmpty(PendingBodyDestruction))
	{
		b2Body* lastBody = ContainerUtils::GetLast(PendingBodyDestruction);

		//Need to clear the user data from the PhysicsBody since it may return a dangling pointer due to ContactList callback.
		lastBody->SetUserData(nullptr);

		//WorldPhysics::DestroyBody will free the memory associated with the physics body.
		WorldPhysics.DestroyBody(ContainerUtils::GetLast(PendingBodyDestruction));
		PendingBodyDestruction.pop_back();
	}

	//Notify all callbacks that a physics cycle is about to start.
	OnPrePhysics.Broadcast(deltaSec);

	WorldPhysics.Step(deltaSec.Value, NumPhysIterationsPerFrame.ToInt32(), NumPhysIterationsPerFrame.ToInt32());

	OnPostPhysics.Broadcast(deltaSec);
}

void Box2dEngineComponent::HandlePreGarbageCollection ()
{
#ifdef DEBUG_MODE
	//Ensure that a garbage collection event is not happening in a middle of the Box 2d cycle.
	//Otherwise there's a risk that Box2dComponents could be removed, causing dangling pointers in Box2D
	CHECK_INFO(!WorldPhysics.IsLocked(), "Garbage Collection should not run in the middle of a Box2d timestep!")
#endif
}
SD_END