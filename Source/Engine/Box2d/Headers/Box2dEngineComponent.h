/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dEngineComponent.h
  The Box 2D Engine Component manages the Box 2D world instance, and processes Box 2D simulation cycle.
=====================================================================
*/

#pragma once

#include "Box2d.h"

#ifdef PLATFORM_WINDOWS
	#pragma warning(push)
	//Disable warning since this shouldn't be an issue when there's no Box2d lib to compile. Box2d is compiled when World module is compiled.
	#pragma warning(disable:4275) //non dll-interface class used as base for dll-interface class.
#endif

SD_BEGIN
class BOX2D_API Box2dEngineComponent : public EngineComponent, protected b2ContactListener, protected b2ContactFilter
{
	DECLARE_ENGINE_COMPONENT(Box2dEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of registered callbacks to invoke just before a Box2D physics simulation cycle begins.
	The parameter deltaSec is the time elapsed since last tick. */
	MulticastDelegate<Float /*deltaSec*/> OnPrePhysics;

	/* List of registered callbacks to invoke after a Box2D physics simulation cycle.
	The parameter deltaSec is the time elapsed since last tick. */
	MulticastDelegate<Float /*deltaSec*/> OnPostPhysics;

protected:
	/* Default constant force applied to all Entities with physics components. */
	Vector3 DefaultGravity;

	/* Box 2D's world that defines the base physics properties, and manages all physics objects. */
	b2World WorldPhysics;

	/* A lone Entity that's responsible for kicking off the Box 2D simulation every frame. Using an external Entity
	to integrate the Box 2D simulation within the Physics tick group. */
	Entity* PhysicsTicker;

	/* The number of times the Box 2D Physics Engine will simulate each frame. Higher values yield greater position but of course,
	more costly. Don't sacrifice frame rates in exhange for higher iterations. It's more efficient to run at 60 fps with 10 iterations,
	rather than running at 30 fps with 20 iterations. */
	Int NumPhysIterationsPerFrame;

	/* List of physics bodies that are pending destruction. This list is accumulated until it's an appropriate time
	to destroy them. This is prevent the situation where physics bodies are destroyed in the middle of a simulation. */
	std::vector<b2Body*> PendingBodyDestruction;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Box2dEngineComponent ();
	virtual ~Box2dEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	/* Contact listener interface */
	virtual void BeginContact (b2Contact* contact) override;
	virtual void EndContact (b2Contact* contact) override;
	virtual void PreSolve (b2Contact* contact, const b2Manifold* oldManifold) override;
	virtual void PostSolve (b2Contact* contact, const b2ContactImpulse* impulse) override;

	/* Contact Filter interface */
	virtual bool ShouldCollide (b2Fixture* fixtureA, b2Fixture* fixtureB) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Marks the given body for destruction. This function will not immediately destroy the object
	 * until it is safe to do so. It is safe to clear the pointer after invoking this method.
	 * This will also destroy all fixtures associated with the given body.
	 */
	virtual void DestroyPhysicsBody (b2Body* body);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Specifies how frequently the Physics Engine will simulate (in seconds).
	 * If newUpdateRate is negative, it'll update as fast as the local Engine's update rate.
	 * numStepsPerUpdate configures how many simulation iterations the engine will run per frame.
	 */
	virtual void SetPhysicsUpdateRate (Float newUpdateRate, Int numStepsPerUpdate);

	void SetDefaultGravity (const Vector3& newDefaultGravity);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector3 GetDefaultGravity () const
	{
		return DefaultGravity;
	}

	inline const b2World& ReadWorldPhysics () const
	{
		return WorldPhysics;
	}

	inline b2World& EditWorldPhysics ()
	{
		return WorldPhysics;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads from the contact data to assign the Box2dComponent parameters.
	 * Returns true if this function successfully found both components.
	 */
	static bool GetPhysicsComponentsFromContact (b2Contact* contactData, Box2dComponent*& outPhysCompA, Box2dComponent*& outPhysCompB);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTickPhysics (Float deltaSec);
	virtual void HandlePreGarbageCollection ();
};
SD_END

#ifdef PLATFORM_WINDOWS
	#pragma warning(pop)
#endif