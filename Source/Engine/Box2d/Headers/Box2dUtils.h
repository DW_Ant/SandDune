/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dUtils.h
  Contains utilities functions to help interface with Box 2D library.
=====================================================================
*/

#pragma once

#include "Box2d.h"

SD_BEGIN
class BOX2D_API Box2dUtils : public BaseUtils
{
	DECLARE_CLASS(Box2dUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Converts the given distance unit from Sand Dune units to Box 2D units.
	 */
	static float ToBox2dDist (Float sdDist);
	static Float ToSdDist (float box2dDist);

	/**
	 * Converts a 2D vector from Sand Dune's coordinate system to Box 2D coordinate system.
	 * And vice versa.
	 */
	static b2Vec2 ToBox2dCoordinates (const Vector2& sdVect);
	static b2Vec2 ToBox2dCoordinates (const Vector3& sdVect);
	static Vector2 ToSdCoordinates (const b2Vec2& b2Vect);
	static Vector3 To3dSdCoordinates (const b2Vec2& b2Vect);

	/**
	 * Converts Sand Dune's Rotators to Box 2D rotation units.
	 */
	static float ToBox2dRotation (Rotator sdRotation);
	static Rotator ToSdRotation (float box2dRotation);

	/**
	 * Creates a box 2d rectangular shape on the stack using the given rectangular dimensions.
	 * The parameter are in SandDune units, and this function converts them to box 2d coordinates.
	 *
	 * @param dimensions The rectangle that ONLY describes the width and height. This function does not create position data for this shape.
	 */
	static b2PolygonShape GetBox2dRectangle (const Rectangle& dimensions);
};
SD_END