/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dComponent.h

  The Box2d Component influences the owner entity's position over time. Each frame, it'll run this component
  through Box 2D's physics cycle, and the end result will update the owning Entitiy's transformation.

  These components interfaces with the 2D Box library, and they could only be attached to SceneTransform entities.
=====================================================================
*/

#pragma once

#include "Box2d.h"

SD_BEGIN
class BOX2D_API Box2dComponent : public EntityComponent
{
	DECLARE_CLASS(Box2dComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ePhysicsType
	{
		/* Static Bodies are nonsimulated stationary bodies (no velocity) that does not collide with
		other static or kinematic bodies. Their position can still be set manually. */
		PT_Static,

		/* A partially simulated body that moves in accordance to its velocity. This body does not collide
		against other kinematic or static bodies. Their position can still be set manually. */
		PT_Kinematic,

		/* A fully simulated body that moves in accordance to its velocity and forces. This body can
		collide against dynamic, kinematic, and static bodies. Their position can be set manually.
		Dynamic bodies must have a positive noninfinite mass. */
		PT_Dynamic
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SPhysicsInitData
	{
		/* Determines the type of body at initialization. It's most efficient to set the physics type
		at spawn. Setting physics type after initialization is costly. */
		ePhysicsType PhysicsType;

		/* If true, then the physics body will not rotate under simulation. */
		bool IsFixedRotation;

		/* Only applicable for PT_Dynamic physics types. If true, this will allow continuous collision detection.
		Although it will be more costly, it'll prevent fast objects from passing through thin solid objects.
		For example, think of the bullet through a paper problem.
		Refer to Box2D's Bullet property. */
		bool EnableContinuousCollision;

		/* Damping properties gradually reduces the momentum of an object over time. 0 implies,
		the object will drift forever (as if in space), and higher values increases deceleration.
		Values can range from 0 to infinity. Recommended to stick between 0 to 1.
		See Box2D manual for more details. */
		Float LinearDamping;
		Float AngularDamping;

		SPhysicsInitData () :
			PhysicsType(PT_Static),
			IsFixedRotation(false),
			EnableContinuousCollision(true),
			LinearDamping(0.f),
			AngularDamping(0.f)
		{
			//Noop
		}
	};

	/**
	 * A collection of properties that are used to create simplified physics bodies.
	 * This struct is primarily used for the CreateAndInitializeComponent function.
	 * For more customization options (such as having multiple fixtures for one component), use the InitializePhysics function then directly manipulate
	 * the Box2d objects.
	 */
	struct SComponentInitData
	{
		/* Data used to create and initialize the box 2d body. */
		SPhysicsInitData BodyInit;

		/* Assigns this physics component's velocity/acceleration in cms per second. */
		Vector3 StarterVelocity;
		Vector3 StarterAcceleration;

		uint16 CollisionCategories;
		uint16 CollisionMasks;
		int16 CollisionGroup;

		/* Defines the geometric shape and dimensions in Box2d units. */
		const b2Shape* CollisionShape;

		/* The friction coefficient for the fixture, usually in the range [0,1] where 0 makes the fixture frictionless (on ice). */
		float Friction;

		/* The restitution (elasticity or bounciness) for the fixture, usually in the range [0,1] where 0 causes the fixture
		to not bounce at all, and 1 implies all momentum is conserved and reflected upon collision. */
		float Restitution;

		/* The density for the fixture, in kg/m^2. This is used to compute the fixture's weight based on its size.
		The higher the object's mass, the more stable it becomes yet more energy is required to move it. */
		float Density;

		/* If false, then this component will only detect overlapping events, but will never block others. */
		bool IsBlockingComponent;

		SComponentInitData () :
			StarterVelocity(Vector3::ZERO_VECTOR),
			StarterAcceleration(Vector3::ZERO_VECTOR),
			CollisionCategories(COLLISION_FLAG_ENTITY),
			CollisionMasks(COLLISION_FLAG_ENTITY),
			Friction(0.f),
			Restitution(0.5f),
			Density(1.f),
			IsBlockingComponent(true)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Collision flags */
	static const unsigned int COLLISION_FLAG_ENTITY; //Default collision flag. Typically encapsulates as everything.

	/* Invoked whenever this PhysicsComponent begins/ends contact with the other PhysicsComponent.
	Do not hold references to b2Contact as their lifespan is short-lived.
	Do NOT force the garbage collection in the middle of these callbacks! */
	SDFunction<void, Box2dComponent* /*otherComp*/, b2Contact* /*contactData*/> OnBeginContact;
	SDFunction<void, Box2dComponent* /*otherComp*/, b2Contact* /*contactData*/> OnEndContact;

	/* Invoked immediately after a collision is resolved. See Box2d PostSolve for details.
	Do NOT force the garbage collection in the middle of these callbacks! */
	SDFunction<void, Box2dComponent* /*otherComp*/, b2Contact* /*contactData*/, const b2ContactImpulse* /*impulse*/> OnCollision;

	/* Set to true when the Position, Velocity, or Rotation are adjusted in the middle of the Physics cycle (eg: overwriting during collision callback).
	When true, this prevents the PhysicsComponent from snapping the component back to the original position, velocity, or rotation.
	These variables reset to false after each Physics cycle. */
	bool OverwritePosition : 2;
	bool OverwriteVelocity : 2;
	bool OverwriteRotation : 4;

protected:
	TickComponent* PhysicsTick;

	/* Box 2D object that'll be simulating the physics. */
	b2Body* PhysicsBody;

	/* Changes the velocity property by this many units per second. */
	Vector3 Acceleration;

	/* Updates this component's translation unit by this many units per second. */
	Vector3 Velocity;

private:
	/* Shared Box 2D object used to initialize physics Bodies. Being a static object, this object is reused
	to save from having to assign all of its member variables every time a PhysicsComponent initializes. */
	static b2BodyDef BodyInit;

	/* Cached position and velocity to detect if the position/velocity should override the physics simulation. */
	Vector3 PrevPosition;
	Vector3 PrevVelocity;
	Rotator PrevRotation;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Destroyed () override;

	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void PostEngineInitialize () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes a Box 2D PhysicsBody that'll define this object's
	 * physics properties. This should be invoked after this component's position is
	 * set to avoid performance issues when initializing tons of overlapping physics objects
	 * over the origin.
	 */
	virtual void InitializePhysics (const SPhysicsInitData& initData);

	/**
	 * Defines this PhysicsComponent's collision flags. This will define what collision categories
	 * this component possess. Use bitwise flags to classify this component in multiple categories.
	 *
	 * Physics components can only collide against other Physics components when both of their
	 * collision masks permit colliding against each other.
	 *
	 * This component's physics must be initialized and its collision fixtures must be instantiated
	 * before its collision properties can be set.
	 *
	 * See Box 2d collision filter's category bits for more information.
	 */
	virtual void SetCollisionCategories (uint16 collisionCategories);

	/**
	 * Defines this PhysicsComponent's collision mask. A collision mask defines which categories this
	 * component may collide against.
	 *
	 * This component's physics must be initialized and its collision fixtures must be instantiated
	 * before its collision properties can be set.
	 *
	 * See Box 2d collision filter's mask bits for more information.
	 */
	virtual void SetCollisionMasks (uint16 collisionMasks);

	/**
	 * Defines this PhysicsComponet's collision index. This is primarily used for Entities that have
	 * unique collision behavior against other Entities that are in the same group.
	 *
	 * This component's physics must be initialized and its collision fixtures must be instantiated
	 * before its collision properties can be set.
	 *
	 * See Box 2d collision filter's group index for more information.
	 *
	 * From the Box2d tutorial, the collision group properties are used as followed:
	 * - If either Entity has a group of zero, use Collision Categories/Collision Mask rules
     * - If both group values are non-zero but different, use Collision Categories/Collision Mask rules
     * - If both group values are the same and positive, collide
     * - If both group values are the same and negative, don't collide
	 */
	virtual void SetCollisionGroup (int16 collisionGroup);

	/**
	 * Small shortcut if there's a need to overhaul all collision data.
	 */
	virtual void SetCollisionData (uint16 collisionCategories, uint16 collisionMasks, int16 collisionGroup);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAcceleration (const Vector3& newAcceleration);
	virtual void SetVelocity (const Vector3& newVelocity);

	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Accessor to the Box2D physics body instance.
	 * Being that it's not a DPointer instance, it's imperative not to delete this object,
	 * otherwise it would lead to a dangling pointer.
	 */
	inline b2Body* GetPhysicsBody ()
	{
		return PhysicsBody;
	}

	inline Vector3 GetAcceleration () const
	{
		return Acceleration;
	}

	inline const Vector3& ReadAcceleration () const
	{
		return Acceleration;
	}

	inline Vector3 GetVelocity () const
	{
		return Velocity;
	}

	inline const Vector3& ReadVelocity () const
	{
		return Velocity;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * This is called upon detecting collision. If this function returns true, it'll block the other PhysicsComponent.
	 * This is called every physics time-step. Override this method for custom blocking behavior.
	 * Both CanBlockComponent function overloads must return true for the components to block the other.
	 * Both (this and other) components must return true for them to block each other.
	 *
	 * Overload with the contact and manifold parameters if the collision detection is based on that data (such as the direction
	 * this component approaches). Otherwise, override the function with only the PhysicsComponent parameter for performance
	 * reasons since that function is called during AABB overlap.
	 *
	 * Read about Box2d PreSolve and b2CallbackFilter callback for more information.
	 */
	virtual bool CanBlockComponent (Box2dComponent* other, b2Contact* contact, const b2Manifold* oldManifold) const;
	virtual bool CanBlockComponent (Box2dComponent* other) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/* Events provoked at the start and end of each physics simulation (once per frame). */
	virtual void HandlePrePhysics (Float deltaSec);
	virtual void HandlePostPhysics (Float deltaSec);

	/* Events provoked whenever this component started/or not longer is contacting the other PhysicsComponent. */
	virtual void HandleBeginContact (Box2dComponent* otherComp, b2Contact* contactData);
	virtual void HandleEndContact (Box2dComponent* otherComp, b2Contact* contactData);
	virtual void HandleCollision (Box2dComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Convenient function to create and initialize a Physics Component that'll be attached to the given owner.
	 * This function will create a simple PhysicsComponent that only has one shape.
	 *
	 * Template argument T is the PhysicsComponent class to instantiate and attach to the componentOwner.
	 * This function returns a valid pointer to the created PhysicsComponent if the function is successful.
	 */
	template <class T>
	static T* CreateAndInitializeComponent (Entity* componentOwner, const SComponentInitData& initData)
	{
		if (dynamic_cast<SceneTransform*>(componentOwner) == nullptr)
		{
			Box2dLog.Log(LogCategory::LL_Warning, TXT("Box2DComponents can only attach to Entities that implement the SceneTransformation interface."));
			return nullptr;
		}

		T* results = T::CreateObject();
		if (!componentOwner->AddComponent(results))
		{
			return nullptr;
		}

		results->InitializePhysics(initData.BodyInit);
		results->SetVelocity(initData.StarterVelocity);
		results->SetAcceleration(initData.StarterAcceleration);

		b2FixtureDef fixtureInit;
		fixtureInit.shape = initData.CollisionShape;
		fixtureInit.friction = initData.Friction;
		fixtureInit.restitution = initData.Restitution;
		fixtureInit.density = initData.Density;
		fixtureInit.isSensor = !initData.IsBlockingComponent;
		fixtureInit.filter.categoryBits = initData.CollisionCategories;
		fixtureInit.filter.maskBits = initData.CollisionMasks;
		fixtureInit.filter.groupIndex = initData.CollisionGroup;
		b2Fixture* createdFixture = results->GetPhysicsBody()->CreateFixture(&fixtureInit);
		if (createdFixture == nullptr)
		{
			Box2dLog.Log(LogCategory::LL_Warning, TXT("Failed to construct box 2d fixture for a Box2D component."));
			OS_BreakExecution();
		}

		return results;
	}

	friend class Box2dEngineComponent;
};
SD_END