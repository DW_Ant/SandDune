/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dUnitTester.h
  Tests various aspects of the Box 2D Module.
=====================================================================
*/

#pragma once

#include "Box2d.h"

#ifdef DEBUG_MODE
SD_BEGIN
class BOX2D_API Box2dUnitTester : public UnitTester
{
	DECLARE_CLASS(Box2dUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestConversions (EUnitTestFlags testFlags) const;
};
SD_END

#endif