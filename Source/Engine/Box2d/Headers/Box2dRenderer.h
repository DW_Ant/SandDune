/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dRenderer.h

  A debugging render component that is used to visualize a Box2dComponent.

  The shape is dictated by the owner's shape.

  This class is only available in debug mode.
=====================================================================
*/

#pragma once

#include "Box2d.h"

SD_BEGIN

#ifdef DEBUG_MODE
class Box2dComponent;

class BOX2D_API Box2dRenderer : public RenderComponent
{
	DECLARE_CLASS(Box2dRenderer)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	/**
	 * A series of properties used to determine how this component should view in scene.
	 */
	enum eShapeType
	{
		ST_Unknown,
		ST_Polygon,
		ST_Circle
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct PolygonDrawData
	{
		/* Series of vertices that are listed in counter clockwise order. SFML doesn't support rendering concave polygons.
		The vertices are in SD units and uses SD coordinate system. */
		std::vector<sf::Vector2f> Vertices;
	};

	struct CircleDrawData
	{
		Float Radius;
	};

	struct SDrawData
	{
		eShapeType ShapeType;

		PolygonDrawData PolygonData;
		CircleDrawData CircleData;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Color FillColor;
	Color OutlineColor;

protected:
	std::vector<SDrawData> DrawData;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Searches through the Owner's component list and returns its PhysicsComponent if found.
	 */
	virtual Box2dComponent* FindPhysicsComponent ();

	/**
	 * Populates the draw data based on the condition of the given PhysicsComponent.
	 */
	virtual void GetDrawShapesDataFrom (Box2dComponent* physComp);
};
#endif
SD_END