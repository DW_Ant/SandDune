/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dClasses.h
  Contains all header includes for the Box2d module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the Box2dClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_BOX2D
#include "Box2d.h"
#include "Box2dComponent.h"
#include "Box2dEngineComponent.h"
#include "Box2dRenderer.h"
#include "Box2dUnitTester.h"
#include "Box2dUtils.h"

#endif
