/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2d.h
  The Box2d module interfaces with the Box2D library and integrates it with the Sand Dune framework.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"

#include <Box2D\Box2D.h>

#ifdef PLATFORM_WINDOWS
	#ifdef BOX2D_EXPORT
		#define BOX2D_API __declspec(dllexport)
	#else
		#define BOX2D_API __declspec(dllimport)
	#endif
#else
	#define BOX2D_API
#endif

#define TICK_GROUP_BOX2D "Box2D"
#define TICK_GROUP_PRIORITY_BOX2D 590 //Update physics before rendering, but immediately after SD's physics (even if it doesn't make sense to run SD's physics with Box2D's physics).

SD_BEGIN
extern LogCategory BOX2D_API Box2dLog;
SD_END