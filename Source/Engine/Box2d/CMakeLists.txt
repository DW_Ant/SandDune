#########################################################################
# Establish directories
#########################################################################
include_directories ("Headers"
	"${ExternalDirectory}/SFML/Include"
	"${ExternalDirectory}/Box2D/include")
	
link_directories(${ExternalDirectory}/Box2D/lib/${ArchitectureFolderName})
link_directories(${ExternalDirectory}/SFML/lib/${ArchitectureFolderName})

SET (Box2dDirectory "${SourceDirectory}/Engine/Box2d")
SET (WorkingDirectory ${Box2dDirectory})


#########################################################################
# Establish source files
#########################################################################

SET (HeaderFiles
	"Headers/Box2d.h"
	"Headers/Box2dComponent.h"
	"Headers/Box2dEngineComponent.h"
	"Headers/Box2dRenderer.h"
	"Headers/Box2dUnitTester.h"
	"Headers/Box2dUtils.h"
)

SET (SourceFiles
	"Source/Box2d.cpp"
	"Source/Box2dComponent.cpp"
	"Source/Box2dEngineComponent.cpp"
	"Source/Box2dRenderer.cpp"
	"Source/Box2dUnitTester.cpp"
	"Source/Box2dUtils.cpp"
)


#########################################################################
# Auto Generate Include File
#########################################################################

#Convert header list to C++ include directives
SET (IncludeList ${HeaderFiles})
GenerateIncludeList(IncludeList)

#Set definitions used in generated header file.
SET (PUBLIC_INCLUDE_LIST ${IncludeList})
SET (MODULE_NAME "Box2d")
SET (PREPROCESSOR_MODULE_NAME "BOX2D")

#Configure the header file to pass some of the CMake settings to the source code.
configure_file (
	"${WorkingDirectory}/CMakeConfiguration/${MODULE_NAME}Classes.h.in"
	"${WorkingDirectory}/Headers/${MODULE_NAME}Classes.h"
)

#Add the generated header file to the file list
list (APPEND HeaderFiles "Headers/${MODULE_NAME}Classes.h")

	
#########################################################################
# Build the module
#########################################################################

SET (AllFiles ${HeaderFiles} ${SourceFiles})

#Generate project filters that align with the source file's directory location.
ConfigureProjectFilters("${AllFiles}")

add_compile_definitions(${PREPROCESSOR_MODULE_NAME}_EXPORT)

add_library(SD-${MODULE_NAME} SHARED ${AllFiles})
ApplyTargetConfiguration(SD-${MODULE_NAME})

set_target_properties (SD-${MODULE_NAME} PROPERTIES FOLDER "Engine")

AddSandDuneLibDependency(${MODULE_NAME} Core)
AddSandDuneLibDependency(${MODULE_NAME} Graphics)

target_link_libraries(SD-${MODULE_NAME}
	PRIVATE optimized Box2D
	PRIVATE debug Box2D-d
	PRIVATE optimized Box2D
	PRIVATE debug Box2D-d
	PRIVATE optimized sfml-graphics
	PRIVATE debug sfml-graphics-d)