/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Subroutine.h
  A Subroutine is an instance that can execute a series of commands.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptCommand.h"
#include "ScriptVariable.h"

SD_BEGIN
class ClmStack;
class ContentClass;
class ContentObject;
class ScriptScope;

class CONTENT_LOGIC_API Subroutine
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EFunctionFlags
	{
		FF_None = 0,
		FF_Static = 1, //This function does not have access to any object instance. It can still access global variables and functions in scope.
		FF_Global = 2, //This function resides in the ClmEngineComponent instead of a scope. Because of this, this function only has access to local variables.
		FF_Native = 4, //This function is implemented in C++ (Called from script, implemented in C++)
		FF_Event = 8, //This function is created in C++ (Called from native, implemented in script)
		FF_Const = 16, //This function does not have write access to owning object or any global variable. Editors can interpret this as a functional subroutine that can be directly inserted to an input parameter.
		FF_Protected = 32, //If this flag is provided, then this function can only be called by instances of the same or child classes. This isn't enforced during runtime. It's up to the editor to check this.
		FF_Final = 64 //If true, then subclasses cannot override this function.
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	HashedString FunctionName;

	EFunctionFlags Flags;

	/* For member subroutines, this is a reference to the class that contains this Subroutine. */
	const ContentClass* FunctionOwner;

	/* List of parameters this function is expecting in the params. The function doesn't write to these params during execution. Instead these are only used to describe the function's signature.
	During execution time, the CommandSteps will read from the stack instead where the order of the stack must be equal to the param stack. These params are read for default values during execution time. */
	std::vector<ScriptVariable> Params;

	/* List of return values this function may produce. If the script doesn't write to them, then it returns their default values. The function doesn't write to these params during execution. Instead these are only used to describe the function's signature. */
	std::vector<ScriptVariable> Output;

	/* List of commands this subroutine will execute when processed. The Subroutine will take ownership over these commands and will delete these instances when this routine is deleted.
	It's a pointer for polymorphism purposes. */
	std::vector<ScriptCommand*> CommandSteps;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Subroutine (const HashedString& inFunctionName, EFunctionFlags inFlags, const ContentClass* inFunctionOwner);
	virtual ~Subroutine ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the ContentObject reference this Subroutine is currently being executed from.
	 * The stack expects the following format:  OutParams, InParams, ObjReferenceThisIsExecutedOn.
	 * Returns nullptr, if the end of the stack does not contain an object reference.
	 */
	static ContentObject* GetExecuteOn (ScriptScope* context, ClmStack& outStack);

	virtual ScriptVariable& AddParameter (const HashedString& paramName);
	virtual ScriptVariable& AddOutput (const HashedString& outputName);

	/**
	 * Adds a new command step to this subroutine. This function will take ownership over the command instance, and it's safe for the caller to forget about the pointer.
	 */
	virtual void AddCommand (ScriptCommand* newCommand);

	/**
	 * Kicks off the subroutine on the specified object. If executeOn is a nullptr, then it assumes it's a static or global function.
	 * The outStack is populated with the subroutine's parameters and local variables.
	 */
	virtual void ExecuteFunction (ScriptScope* scope, std::vector<DString>& outCallstack, ClmStack& outStack) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const HashedString& ReadFunctionName () const
	{
		return FunctionName;
	}

	inline EFunctionFlags GetFlags () const
	{
		return Flags;
	}

	inline const ContentClass* GetFunctionOwner () const
	{
		return FunctionOwner;
	}

	inline const std::vector<ScriptVariable>& ReadParams () const
	{
		return Params;
	}

	inline const std::vector<ScriptVariable>& ReadOutput () const
	{
		return Output;
	}

	inline const std::vector<ScriptCommand*>& ReadCommandSteps () const
	{
		return CommandSteps;
	}
};

DEFINE_ENUM_FUNCTIONS(Subroutine::EFunctionFlags)
SD_END