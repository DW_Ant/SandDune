/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandEndSection.h
  The command that marks the end of a section.

  This is used to evaluate the command above the section beginning to
  figure out the next step in the sequence.

  For example: an end section for a IfCondition would check the IfCondition
  above the BeginSection. If the IfCondition has an else clause, that command
  will inform the flow of control to skip over that else clause if it already
  went through the if section.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandEndSection : public ScriptCommand
{


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandEndSection ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END