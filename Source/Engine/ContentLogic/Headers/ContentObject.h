/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentObject.h
  An object that is an instance from a ContentClass. It's an object
  that interfaces with the ContentLogic module.

  Design Decisions
  Decided against using component design and went with a simplified OOP for the
  following reasons:
  * Simplicity purposes
  * The ContentLogic Module (CLM) is not intended to replace native C++. Instead
  the CLM is intended to add primitive flow of control for various assets. Anything
  extensive or beyond that should be in source instead. Source is faster, easier to
  debug, interfaces with IDEs and source control, and it's easier to read.
  * Reduce the amount of duplication between SandDune's component architecture.
  If the CLM has components, the next logical step would to implement component
  iterators, the components. Eventually this may lead to duplicate classes
  (eg: RenderComponent vs RenderComponent in CLM).
  * Even if it's shared (eg: RenderComponent has an associated CLM component),
  the CLM should not have visibility to everything in engine source for that would
  be overstepping the purpose of the CLM. For example if a RenderComponent was registered
  through CLM, it may be difficult to track down where it was registered when debugging
  in source. Instead RenderComponents should be restricted to be registered via source.

  Decided in favor of using an explicit ContentObject class for the following reasons:
  * Grants the developer the ability to expand capabilities to an extent. Should they see
  a need to create custom CLM classes, they can. The limitation is that they don't have
  visibility to SD classes unless the SD classes expose their functionality to CLM.
  * Grants OOP benefits to CLM: object persistence (eg: states), variable/member
  function scoping, instantiation, and polymorphism.
  * The ContentObject can interface with other SD classes should there be a need to
  expose certain functionalities to the CLM.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptVariable.h"

SD_BEGIN
class ContentClass;
class ScriptComponent;
class ScriptScope;
class ScriptVariableRef;
class Subroutine;

class CONTENT_LOGIC_API ContentObject : public Entity
{
	DECLARE_CLASS(ContentObject)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Class that defines inheritance and available member subroutines. */
	const ContentClass* Class;

	/* The component instance that owns this content object. If nullptr, then it's presumed that this object is created outside of a component.
	This is primarily used to obtain native functions. */
	ScriptComponent* OwningComponent;

	/* A unique identifier used to figure out where this object resides on the heap. */
	size_t Id;

	/* List of object IDs this object is a owner of. If this object is destroyed, all objects of matching IDs will also be destroyed. */
	std::vector<size_t> OwnedObjects;

	/* The ID of the object that owns this instance. If zero, then nothing owns this object. */
	size_t OwnerId;

	/* Map of all member variables including variables found in the parent class. The order of the variables are defined from parent class to child. */
	std::vector<ScriptVariable> MemberVariables;

	DPointer<TickComponent> ScriptTick;

private:
	/* Reference to the scope this object resides in. */
	ScriptScope* Scope;

	/* The Subroutine that defines the script object's Tick function. Cached to avoid finding the function every frame. */
	const Subroutine* ScriptTickFunction;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString ToString () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Looks at the class to figure out the index position of the member variable that matches the given name.
	 * If there is a member variable that matches the given name, it'll return a pointer to it. In addition to that, it'll return the index position of that variable.
	 * The complexity of this function is linear. It's recommended to reference member variables by index if possible.
	 */
	virtual ScriptVariableRef FindMemberVariable (const HashedString& varName, size_t& outVarIdx);
	virtual ScriptVariableRef GetMemberVariable (size_t varIdx);

	/**
	 * Sets up this object and kicks off the initialization sequence.
	 * The class is used to populate this object's member variable list.
	 * This also generates an ID for this object to be placed in the scope's heap.
	 * This function also registers this object to the specified scope.
	 * If a ScriptComponent is specified, it'll pair this object with that component. If it's nullptr, then it assumes this object was created from outside a component (eg: ContentObject creating another ContentObject).
	 * If an owner is specified, then this object will automatically be destroyed whenever the owner is destroyed.
	 * After everything is set, it'll invoke the script's BeginObject Subroutine.
	 */
	virtual void BeginContentObject (const ContentClass* inClass, ScriptScope* context, ScriptComponent* inOwningComponent, ContentObject* owner);
	virtual void DestroyContentObject ();

	/**
	 * Returns true if this object owns the specified object.
	 */
	virtual bool IsOwnerOf (ContentObject* otherObj) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwner (ContentObject* newOwner);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const ContentClass* GetClass () const
	{
		return Class;
	}

	inline ScriptComponent* GetOwningComponent () const
	{
		return OwningComponent;
	}

	inline size_t GetId () const
	{
		return Id;
	}

	virtual ContentObject* GetOwner () const;

	inline const std::vector<ScriptVariable>& ReadMemberVariables () const
	{
		return MemberVariables;
	}

	inline std::vector<ScriptVariable>& EditMemberVariables ()
	{
		return MemberVariables;
	}

	inline TickComponent* GetScriptTick () const
	{
		return ScriptTick.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Looks at the class assigned to this object instance, and initializes this instance's MemberVariable vector based on that class.
	 */
	virtual void InitMemberVariables ();

	virtual void InitId ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleScriptTick (Float deltaSec);
};
SD_END