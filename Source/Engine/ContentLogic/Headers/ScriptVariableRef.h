/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptVariableRef.h
  A wrapper that contains an index value that points to a specific
  variable in a container. The container can be the stack or an object
  member variable vector.

  This wrapper exist because the container can resize.
  If it resizes, the system may reposition the vector in a different place
  in memory. Doing this comes at a risk of leaving any cached ScriptVariable
  references to the container to dangle (causing undefined behavior).

  The ScriptVariableRef works around this by holding an index value and a reference
  to the container, itself. That way even if the stack changes size, this wrapper is able to
  locate the variable.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptVariable.h"

SD_BEGIN
class CONTENT_LOGIC_API ScriptVariableRef
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to the vector container that contains the ScriptVariable. This can either reside on the stack or an object member variable vector. */
	std::vector<ScriptVariable>* Container;

	/* The index to the vector that points to a specific variable. */
	size_t VarIdx;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ScriptVariableRef (std::vector<ScriptVariable>* inContainer, size_t inVarIdx);
	ScriptVariableRef (const ScriptVariableRef& cpyObj);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	bool operator== (const ScriptVariableRef& other) const;
	bool operator!= (const ScriptVariableRef& other) const;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	const ScriptVariable& ReadVar () const;
	ScriptVariable& EditVar ();

	/**
	 * Returns true if this reference is pointing to an actual script variable.
	 */
	bool IsValid () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline size_t GetVarIdx () const
	{
		return VarIdx;
	}
};
SD_END