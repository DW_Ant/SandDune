/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptVariable.h
  A wrapper to a DProperty that a Subroutine can read and write to.
  The main purpose of this class is to act like a generic wrapper for all
  types so that it may be placed in a stack in continuous memory, and also
  allow for runtime interpretation of function signatures (eg: a function can
  be dynamically generated without running the compiler when changing types).

  This class meets the following requirements:
  - Used as a generic return type for all Subroutines.
  - Can translate back to a C++ type.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"

SD_BEGIN
class ClmStack;
class ContentClass;
class ContentObject;
class ScriptScope;
class ScriptVariableRef;
class Subroutine;

class CONTENT_LOGIC_API ScriptVariable
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EVarType : char
	{
		VT_Unknown = 0,
		VT_Bool = 1,
		VT_Int = 2,
		VT_Float = 3,
		VT_String = 4,
		VT_Function = 5, //Pointer that's pointing to a Subroutine (eg: delegates).
		VT_Obj = 6, //Pointer that's pointing to a ContentObject on the Heap.
		VT_Class = 7, //Reference to a ContentClass (referenced by HashedString).
		VT_Pointer = 8, //Pointer that's pointing to another ScriptVariable.
		VT_Max = VT_Pointer
	};

	enum EPointerFlag : char
	{
		PF_Stack = 0,
		PF_ObjMemVar = 1,
		PF_ScopeGlobal = 2
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SScriptDelegate
	{
		/* The object instance that owns the delegate handler. */
		ContentObject* FunctionOwner;

		/* Pointer to the Subroutine to execute when invoked. */
		const Subroutine* Function;

		SScriptDelegate ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	EVarType VarType;

	/* If true, then the VarType is rigid, and it cannot change. It'll refuse to write data in this variable if the type does not match. */
	bool bFixedType;

	/* If true, then this is a valid variable. An invalid variable would be one that lingers in a container that didn't resize yet (such as a variable beyond the top index in a stack). */
	bool bIsValid;

	/* DataBuffer that contains the data of the variable. */
	DataBuffer Data;

	/* Name of the variable. An empty string implies that it's an unnamed variable (eg: local variable). */
	HashedString VariableName;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ScriptVariable ();
	ScriptVariable (const HashedString& inVariableName);
	ScriptVariable (const ScriptVariable& cpyObj);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const ScriptVariable& cpyObj);

	//Only returns true if the variable type and its data matches.
	bool operator== (const ScriptVariable& other) const;
	bool operator!= (const ScriptVariable& other) const;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Locks this variable to be this specific type.
	 */
	virtual void SetFixedType (EVarType fixedType);

	/**
	 * Resets this variable's attributes (eg: useful for variables that are reused such as the ClmStack).
	 * This also empties the data buffer, causing the variable to return the type's defaults (eg: false or 0).
	 */
	virtual void Reset ();

	/**
	 * Same as Reset but only resets its type back to Unknown. Useful for editors that change the variable types without resetting its name and other attributes.
	 */
	virtual void ResetType ();

	/**
	 * Writes to the data buffer based on its variable type.
	 * Due to ScriptComponent's IMPLEMENT_SCRIPT_FUNCTION macro, these function names must match the DProperty type.
	 */
	virtual void WriteBool (Bool flag);
	virtual void WriteInt (Int value);
	virtual void WriteFloat (Float value);
	virtual void WriteDString (const DString& string);
	virtual void WriteFunction (ContentObject* functionOwner, const Subroutine* function);
	virtual void WriteObjPointer (ContentObject* pointAt);
	virtual void WriteClass (const ContentClass* contentClass);

	/**
	 * Write a pointer that points at another ScriptVariable on a stack.
	 * stackIdx is the index position where the pointed variable resides.
	 * If stackIdx is equal to INDEX_NONE, then this assumes the pointer is nullptr.
	 */
	virtual void WritePointer (size_t stackIdx);

	/**
	 * Write a pointer that points at a ScriptVariable in the Heap.
	 * memVarIdx is the variable's index position in owningObj. The memVarIdx also includes positions of the object's parent class member variables.
	 * If the memVarIdx is unknown, reference ContentOject's FindMemVarIdx to find a variable by its name.
	 * If owningObj is nullptr or memVarIdx is INDEX_NONE, then this pointer will be assigned to nullptr.
	 */
	virtual void WritePointer (ContentObject* owningObj, size_t memVarIdx);

	/**
	 * Write a pointer that points at a ScriptVariable in the scope global variables.
	 * The varIdx is the index position in the scope's GlobalVariables vector. If the index position is equal to INDEX_NONE, then this assumes the pointer is nullptr.
	 */
	virtual void WriteScopePointer (ScriptScope* scope, size_t varIdx);

	/**
	 * Reads from the data buffer to translate it to a particular type.
	 * Due to ScriptComponent's IMPLEMENT_SCRIPT_FUNCTION macro, these function names must match the DProperty type.
	 */
	virtual Bool ToBool () const;
	virtual Int ToInt () const;
	virtual Float ToFloat () const;
	virtual DString ToDString () const;
	virtual SScriptDelegate ToFunction (ScriptScope* context) const;
	virtual ContentObject* ToObjPointer (ScriptScope* context) const;
	virtual const ContentClass* ToClass () const;

	/**
	 * Finds the immediate ScriptVariable residing either on the Heap or Stack.
	 * Returns nullptr if the data is unable to find a ScriptVariable.
	 */
	virtual ScriptVariableRef GetPointer (ScriptScope* scope, ClmStack& stack) const;
	
	/**
	 * Recursively navigates the pointers until it finds a ScriptVariable that is not a pointer to another ScriptVariable.
	 */
	virtual ScriptVariableRef GetPointerRecursive (ScriptScope* scope, ClmStack& stack) const;

	/**
	 * Interprets the contents of this variable as a string regardless of its type.
	 */
	virtual DString InterpretValueAsString (ScriptScope* scope, ClmStack& stack) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetIsValid (bool newIsValid);
	virtual void SetVariableName (const HashedString& newVariableName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EVarType GetVarType () const
	{
		return VarType;
	}

	inline bool IsValid () const
	{
		return bIsValid;
	}

	inline const HashedString& ReadVariableName () const
	{
		return VariableName;
	}

	inline HashedString& EditVariableName ()
	{
		return VariableName;
	}
};
SD_END