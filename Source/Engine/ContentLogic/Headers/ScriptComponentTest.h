/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptComponentTest.h
  A ScriptComponent with functions used to test the ContentLogic module.

  This doesn't have any utility other than to run unit tests.
=====================================================================
*/

#pragma once

#include "ScriptComponent.h"

#ifdef DEBUG_MODE
SD_BEGIN
class CONTENT_LOGIC_API ScriptComponentTest : public ScriptComponent
{
	DECLARE_CLASS(ScriptComponentTest)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Invokes the ProcessScriptEvent_Implementation (defined by the SCRIPT_FUNCTION macro). This invokes a Subroutine defined by the ScriptObj.
	 * The ScriptObj is expected to populate the out parameters to the following values:
	 * outInt - Whatever CallableConvertInt function returns.
	 * outFloat - Whatever CallableConvertFloat function returns.
	 * outString - Whatever CallableConvertString function returns.
	 */
	virtual void ProcessScriptFunction (Int inInt, Float inFloat, const DString& inString, Int& outInt, Float& outFloat, DString& outString);

	/**
	 * Sets outInt to the inverse of the input (int * -1).
	 */
	virtual void CallableConvertInt (Int inInt, Int& outInt) const;

	/**
	 * Sets the outFloat to the double of input.
	 */
	virtual void CallableConvertFloat (Float inFloat, Float& outFloat) const;

	/**
	 * Sets the outString to the upper case version of the input.
	 */
	virtual void CallableConvertString (const DString& inString, DString& outString) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	DECLARE_SCRIPT_FUNCTION_3PARAM(ProcessScriptFunction, Int, inInt, Float, inFloat, DString, inString);
};
SD_END
#endif