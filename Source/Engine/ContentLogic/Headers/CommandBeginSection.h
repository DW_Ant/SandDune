/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandBeginSection.h
  A command that marks the beginning of a section block.

  This is used in conjunction of an EndSection to indicate the size of this block.
  This is typically used for flow of control sections such as loops and branches.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandBeginSection : public ScriptCommand
{


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandBeginSection ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END