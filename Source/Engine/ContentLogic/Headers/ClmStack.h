/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClmStack.h
  A container of ScriptVariables that can be added/removed at the end.

  This is used for callstack information and variables pushed to the stack.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptVariable.h"

SD_BEGIN
class ContentClass;
class ContentObject;
class ScriptScope;
class ScriptVariableRef;

class CONTENT_LOGIC_API ClmStack
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of variables pushed to the stack. */
	std::vector<ScriptVariable> Stack;

	/* Index in the stack that points to the element on the top of the stack. */
	size_t TopIdx;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ClmStack ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Appends the given variable to the top of the stack. If the TopIdx is not at the end of the Stack, this function will overwrite the contents of the next element in the stack.
	 */
	virtual ScriptVariableRef PushVariable (const HashedString& varName);
	virtual ScriptVariableRef PushVariable ();

	/**
	 * Decrements the variable in the stack. This doesn't remove the actual contents until the stack shrinks.
	 */
	virtual void PopVariable ();

	/**
	 * Collapses the stack to the current TopIdx.
	 */
	virtual void Shrink ();

	/**
	 * Retrieves the variable relative to the top of the stack. If offset is 1, it'll retrieve the variable that's on the second highest position of the stack.
	 */
	virtual ScriptVariableRef EditVariable (size_t offset);

	/**
	 * Returns the number of valid variables in the stack (does not include remaining variables beyond the top index position).
	 */
	virtual size_t GetStackSize () const;
	inline bool IsEmpty () const
	{
		return (TopIdx == INDEX_NONE);
	}

	/**
	 * Pops all variables from the stack. Doesn't shrink it though.
	 */
	inline void Empty ()
	{
		TopIdx = INDEX_NONE;
	}

	/**
	 * Utility to retrieve a parameter at the end of the stack. If the parameter is a pointer, it'll dereference it.
	 * The stack is expected to have the output (if any) before the parameters (if any). The parameters are expected to be at the end of the stack.
	 * If bContainsThis is true, then this assumes the end of the stack contains an object reference that owns the function being executed.
	 */
	virtual Bool GetBoolParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual Int GetIntParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual Float GetFloatParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual DString GetStringParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual ScriptVariable::SScriptDelegate GetFunctionParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual ContentObject* GetObjParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;
	virtual const ContentClass* GetClassParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const;

	/**
	 * Utility to write a variable to an output variable on the stack. If the variable is a pointer, it'll dereference it.
	 * The stack is expected to have the output before the parameters (if any). The parameters are expected to be at the end of the stack.
	 */
	virtual void WriteBoolOutput (Bool value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteIntOutput (Int value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteFloatOutput (Float value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteStringOutput (const DString& value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteFunctionOutput (ContentObject* functionOwner, const Subroutine* function, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteObjOutput (ContentObject* obj, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);
	virtual void WriteClassOutput (const ContentClass* contentClass, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<ScriptVariable>& ReadStack () const
	{
		return Stack;
	}

	inline std::vector<ScriptVariable>& EditStack ()
	{
		return Stack;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick accessors to a specific variable in the stack.
	 * Protected since it's not safe to access these variables if the stack resizes.
	 */
	inline const ScriptVariable& ReadVar (size_t offset) const
	{
		return Stack.at(TopIdx - offset);
	}

	inline ScriptVariable& EditVar (size_t offset)
	{
		return Stack.at(TopIdx - offset);
	}
};
SD_END