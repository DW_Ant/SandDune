/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeDelegate.h
  A command that executes a delegate.
  This command will take a function variable, and attempt to execute that function
  if it's bounded. This command will do nothing if nothing is bound to that delegate.

  The end of the stack will be passed into as the delegate's parameters. If the handler
  is a member function, a reference to 'this' will be appended to the end of the stack before
  invoking it.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandInvokeDelegate : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The stack offset from the top that contains the delegate to execute. If it's a pointer variable, this command will automatically dereference it. */
	size_t DelegateOffset;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandInvokeDelegate (size_t inDelegateOffset);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Looks at the given Subroutine and compares its parameters to the variables at the end of the stack. If all types match, then it'll return true.
	 */
	virtual bool ValidateParameters (ScriptScope* scope, const Subroutine* targetFunction, ClmStack& outStack) const;
};
SD_END