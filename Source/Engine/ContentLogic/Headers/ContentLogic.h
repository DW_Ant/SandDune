/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentLogic.h
  Contains important file includes and definitions for the ContentLogic module.

  The ContentLogic Module (CLM) defines the framework that allows runtime execution
  from developer-defined environments. For example, the editor may have a
  node-based graph that connects functions and variables together.

  The ContentLogic sets up that graph's environment (such as variables and the stack),
  and executes those function bindings. There is no compiler for this. The functions,
  variables, and bindings are setup during runtime. It's also evaluated during runtime as well.
  Therefore, the CLM is incredibly slower compared to native. The main advantage CLM
  has is that it allows allows scripts to be generated, and also dynamically load from files.

  This module is intended to be used during runtime and development time. However, this
  module will not have dependencies to modules such as Gui, Input, and Graphics.
  Therefore, this module will not have editor capabilities.

  This module depends on the following modules:
  - Core
  - File
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef CONTENT_LOGIC_EXPORT
		#define CONTENT_LOGIC_API __declspec(dllexport)
	#else
		#define CONTENT_LOGIC_API __declspec(dllimport)
	#endif
#else
	#define CONTENT_LOGIC_API
#endif

#ifdef GetClassName
//Remove intrusive MS preprocessor definition
#undef GetClassName
#endif

#define TICK_GROUP_CONTENT_LOGIC "ContentLogic"
#define TICK_GROUP_PRIORITY_CONTENT_LOGIC 4000


SD_BEGIN
extern LogCategory CONTENT_LOGIC_API ContentLogicLog;
SD_END