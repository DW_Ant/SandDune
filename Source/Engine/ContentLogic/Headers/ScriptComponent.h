/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptComponent.h
  A component that allows a native SD class to interface with a runtime
  environment in the ContentLogic module. This component is able to kick
  off subroutines.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"

#define DECLARE_SCRIPT_FUNCTION(functionName) \
private: \
	virtual bool functionName##_Implementation (std::vector<ScriptVariable>& outParams); \
public:

#define DECLARE_SCRIPT_FUNCTION_1PARAM(functionName, paramType1, paramName1) \
private: \
	virtual bool functionName##_Implementation (##paramType1 paramName1##, std::vector<ScriptVariable>& outParams); \
public:

#define DECLARE_SCRIPT_FUNCTION_2PARAM(functionName, paramType1, paramName1, paramType2, paramName2) \
private: \
	virtual bool functionName##_Implementation (##paramType1 paramName1##, paramType2 paramName2##, std::vector<ScriptVariable>& outParams); \
public:

#define DECLARE_SCRIPT_FUNCTION_3PARAM(functionName, paramType1, paramName1, paramType2, paramName2, paramType3, paramName3) \
private: \
	virtual bool functionName##_Implementation (##paramType1 paramName1##, paramType2 paramName2##, paramType3 paramName3##, std::vector<ScriptVariable>& outParams); \
public:



#define IMPLEMENT_SCRIPT_FUNCTION(className, functionName) \
bool className##::##functionName##_Implementation (std::vector<ScriptVariable>& outParams) \
{ \
	CHECK(ScriptClass != nullptr && ScriptObj != nullptr) \
	size_t functionHash = SD::HashedString( #functionName ).GetHash(); \
	if (const SD::Subroutine* function = ScriptClass->FindFunction(Scope.Get(), functionHash, ScriptObj.Get())) \
	{ \
		std::vector<SD::DString> callstack; \
		callstack.emplace_back(TXT( #functionName )); \
		SD::ClmStack stack; \
		for (const ScriptVariable& output : function->ReadOutput()) \
		{ \
			ScriptVariableRef localOutput = stack.PushVariable(output.ReadVariableName()); \
			localOutput.EditVar().SetFixedType(output.GetVarType()); \
		} \
		\
		if ((function->GetFlags() & (Subroutine::FF_Static | Subroutine::FF_Global)) == 0) \
		{ \
			SD::ScriptVariableRef execOn = stack.PushVariable(SD::HashedString("this")); \
			execOn.EditVar().WriteObjPointer(ScriptObj.Get()); \
		} \
		\
		function->ExecuteFunction(Scope.Get(), OUT callstack, OUT stack); \
		outParams.resize(function->ReadOutput().size()); \
		PopulateOutParams(OUT stack, 0, OUT outParams); \
		return true; \
	} \
	\
	return false; \
}

#define IMPLEMENT_SCRIPT_FUNCTION_1PARAM(className, functionName, paramType1, paramName1) \
bool className##::##functionName##_Implementation(##paramType1 paramName1##, std::vector<ScriptVariable>& outParams) \
{ \
	CHECK(ScriptClass != nullptr && ScriptObj != nullptr) \
	size_t functionHash = SD::HashedString( #functionName ).GetHash(); \
	if (const SD::Subroutine* function = ScriptClass->FindFunction(Scope.Get(), functionHash, ScriptObj.Get())) \
	{ \
		std::vector<SD::DString> callstack; \
		callstack.emplace_back(TXT( #functionName )); \
		SD::ClmStack stack; \
		for (const ScriptVariable& output : function->ReadOutput()) \
		{ \
			ScriptVariableRef localOutput = stack.PushVariable(output.ReadVariableName()); \
			localOutput.EditVar().SetFixedType(output.GetVarType()); \
		} \
		SD::ScriptVariableRef param1 = stack.PushVariable(); \
		\
		param1.EditVar().Write##paramType1##(##paramName1##); \
		if ((function->GetFlags() & (Subroutine::FF_Static | Subroutine::FF_Global)) == 0) \
		{ \
			SD::ScriptVariableRef execOn = stack.PushVariable(SD::HashedString("this")); \
			execOn.EditVar().WriteObjPointer(ScriptObj.Get()); \
		} \
		\
		function->ExecuteFunction(Scope.Get(), OUT callstack, OUT stack); \
		outParams.resize(function->ReadOutput().size()); \
		PopulateOutParams(OUT stack, 1, OUT outParams); \
		return true; \
	} \
	\
	return false; \
}

#define IMPLEMENT_SCRIPT_FUNCTION_2PARAM(className, functionName, paramType1, paramName1, paramType2, paramName2) \
bool className##::##functionName##_Implementation(##paramType1 paramName1##, paramType2 paramName2##, std::vector<ScriptVariable>& outParams) \
{ \
	CHECK(ScriptClass != nullptr && ScriptObj != nullptr) \
	size_t functionHash = SD::HashedString( #functionName ).GetHash(); \
	if (const SD::Subroutine* function = ScriptClass->FindFunction(Scope.Get(), functionHash, ScriptObj.Get())) \
	{ \
		std::vector<SD::DString> callstack; \
		callstack.emplace_back(TXT( #functionName )); \
		SD::ClmStack stack; \
		for (const ScriptVariable& output : function->ReadOutput()) \
		{ \
			ScriptVariableRef localOutput = stack.PushVariable(output.ReadVariableName()); \
			localOutput.EditVar().SetFixedType(output.GetVarType()); \
		} \
		SD::ScriptVariableRef param1 = stack.PushVariable(); \
		SD::ScriptVariableRef param2 = stack.PushVariable(); \
		\
		param1.EditVar().Write##paramType1##(##paramName1##); \
		param2.EditVar().Write##paramType2##(##paramName2##); \
		if ((function->GetFlags() & (Subroutine::FF_Static | Subroutine::FF_Global)) == 0) \
		{ \
			SD::ScriptVariableRef execOn = stack.PushVariable(SD::HashedString("this")); \
			execOn.EditVar().WriteObjPointer(ScriptObj.Get()); \
		} \
		\
		function->ExecuteFunction(Scope.Get(), OUT callstack, OUT stack); \
		outParams.resize(function->ReadOutput().size()); \
		PopulateOutParams(OUT stack, 2, OUT outParams); \
		return true; \
	} \
	\
	return false; \
}

#define IMPLEMENT_SCRIPT_FUNCTION_3PARAM(className, functionName, paramType1, paramName1, paramType2, paramName2, paramType3, paramName3) \
bool className##::##functionName##_Implementation(##paramType1 paramName1##, paramType2 paramName2##, paramType3 paramName3##, std::vector<ScriptVariable>& outParams) \
{ \
	CHECK(ScriptClass != nullptr && ScriptObj != nullptr) \
	size_t functionHash = SD::HashedString( #functionName ).GetHash(); \
	if (const SD::Subroutine* function = ScriptClass->FindFunction(Scope.Get(), functionHash, ScriptObj.Get())) \
	{ \
		std::vector<SD::DString> callstack; \
		callstack.emplace_back(TXT( #functionName )); \
		SD::ClmStack stack; \
		for (const ScriptVariable& output : function->ReadOutput()) \
		{ \
			ScriptVariableRef localOutput = stack.PushVariable(output.ReadVariableName()); \
			localOutput.EditVar().SetFixedType(output.GetVarType()); \
		} \
		SD::ScriptVariableRef param1 = stack.PushVariable(); \
		SD::ScriptVariableRef param2 = stack.PushVariable(); \
		SD::ScriptVariableRef param3 = stack.PushVariable(); \
		\
		param1.EditVar().Write##paramType1##(##paramName1##); \
		param2.EditVar().Write##paramType2##(##paramName2##); \
		param3.EditVar().Write##paramType3##(##paramName3##); \
		if ((function->GetFlags() & (Subroutine::FF_Static | Subroutine::FF_Global)) == 0) \
		{ \
			SD::ScriptVariableRef execOn = stack.PushVariable(SD::HashedString("this")); \
			execOn.EditVar().WriteObjPointer(ScriptObj.Get()); \
		} \
		\
		function->ExecuteFunction(Scope.Get(), OUT callstack, OUT stack); \
		outParams.resize(function->ReadOutput().size()); \
		PopulateOutParams(OUT stack, 3, OUT outParams); \
		return true; \
	} \
	\
	return false; \
}

SD_BEGIN
class ClmStack;
class ContentClass;
class ContentObject;
class ScriptScope;
class ScriptVariable;

class CONTENT_LOGIC_API ScriptComponent : public EntityComponent
{
	DECLARE_CLASS(ScriptComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<ScriptScope> Scope;

	/* The name of the class to use. This must be assigned before BeginObject since the ScriptComponent uses this to set up its ScriptObj.
	If empty, then it assumes that an external instance will invoke SetScriptClass after instantiation. */
	HashedString ScriptClassName;

	/* List of subroutines SD classes can invoke. */
	const ContentClass* ScriptClass;

	DPointer<ContentObject> ScriptObj;

	/* List of available precompiled functions a subroutine can invoke. */
	std::unordered_map<HashedString, std::function<void(ClmStack& /*outStack*/)>> CallableNativeFunctions;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual bool RegisterCallableNativeFunction (const HashedString& functionName, const std::function<void(ClmStack&)>& lambda);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetScope (ScriptScope* newScope);
	virtual void SetScriptClass (const ContentClass* newScriptClass);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ScriptScope* GetScope () const
	{
		return Scope.Get();
	}

	inline const ContentClass* GetScriptClass () const
	{
		return ScriptClass;
	}

	inline ContentObject* GetScriptObj () const
	{
		return ScriptObj.Get();
	}

	inline const std::unordered_map<HashedString, std::function<void(ClmStack&)>>& ReadCallableNativeFunctions () const
	{
		return CallableNativeFunctions;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Assuming that the Scope and Class are established, this function creates the ContentObject associated with this component.
	 */
	virtual void CreateScriptObj ();

	/**
	 * Utility that populates the output parameters from the given stack. This function is under the assumption the stack ends with output before the input parameters.
	 * The size of the outParams determines how many output variables this function expects.
	 */
	virtual void PopulateOutParams (ClmStack& outStack, size_t numParams, std::vector<ScriptVariable>& outParams) const;
};
SD_END