/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandElseClause.h
  A command that instructs the corresponding IfCondition to skip over
  the section after this command if that IfCondition is true.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandElseClause : public ScriptCommand
{


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandElseClause ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END