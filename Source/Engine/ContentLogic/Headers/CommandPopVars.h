/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandPopVars.h
  A command that pops one or more variables from the stack.

  For performance reasons, there are no safe guards when popping too many
  variables. It's the interpreter's responsibility to assemble the
  function commands correctly.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandPopVars : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The number of variables to remove from the stack. */
	size_t NumVars;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandPopVars (size_t inNumVars);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END