/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandPushVar.h
  Creates a local unnamed variable and pushes it at the end of the stack.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"
#include "ScriptVariable.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandPushVar : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	ScriptVariable::EVarType Type;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandPushVar (ScriptVariable::EVarType inType);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END