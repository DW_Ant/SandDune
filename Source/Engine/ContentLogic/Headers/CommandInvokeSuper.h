/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeSubroutine.h
  A command the executes the Subroutine's super function.

  This is only applicable for member functions with the same name
  as a owning object's parent's member function.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandInvokeSuper : public ScriptCommand
{


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandInvokeSuper ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END