/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandAssignVar.h
  Copies the contents from source to destination.

  If either variable is a pointer and autoDereference is true, then
  it'll recursively navigate the pointer so that the assignment edits the
  variable it's pointing to.

  The specialized assignment commands allows for direct editing without
  creating an extra local variable. These are generally used for
  hard coded/inlined expressions.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"
#include "ScriptVariable.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandAssignVar : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true and if the source is a pointer, then this command will automatically dereference the source before copying its contents over to destination.
	Set this variable to false if this command should assign the variable to a pointer. */
	bool bAutoDereferenceSrc;

	/* If true and if the destination is a pointer, then this command will automatically dereference the destination before performing the assignment operation.
	Set this variable to false if this command should assign a pointer instead. */
	bool bAutoDereferenceDest;

	/* Offset on the stack where the variable is found. To handle variables on the heap, the Subroutine, should create a local variable on the stack that points to that variable. */
	size_t SourceOffset;
	size_t DestinationOffset;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignVar (bool inAutoDereferenceSrc, bool inAutoDereferenceDest, size_t inSourceOffset, size_t inDestinationOffset);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};

class CONTENT_LOGIC_API CommandAssignBool : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	size_t DestinationOffset;
	Bool NewValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignBool (size_t inDestinationOffset, Bool inNewValue);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};

class CONTENT_LOGIC_API CommandAssignInt : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	size_t DestinationOffset;
	Int NewValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignInt (size_t inDestinationOffset, Int inNewValue);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};

class CONTENT_LOGIC_API CommandAssignFloat : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	size_t DestinationOffset;
	Float NewValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignFloat (size_t inDestinationOffset, Float inNewValue);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};

class CONTENT_LOGIC_API CommandAssignString : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	size_t DestinationOffset;
	DString NewValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignString (size_t inDestinationOffset, const DString& inNewValue);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};

class CONTENT_LOGIC_API CommandAssignRef : public ScriptCommand
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	//Simple enumerator used to disambiguate the constructors.
	enum EScopeVar
	{
		SV_VarIsOnScope
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The stack position relative to the top index that contains the pointer to assign. */
	size_t PointerStackOffset;

	/* If the variable resides on the stack, this the position offset relative from the top of the stack. If the variable is pointing to an object member variable, then
	this variable is index value to the object's member variable vector. */
	size_t PointAtOffset;

	/* If it's not INDEX_NONE, then this is the stack offset position that points to an object pointer. This object pointer contains the member variable table that this pointer will point to. */
	size_t ObjStackPosition;

	/* If true, then this variable reference will point at a scope's global variable. */
	bool bIsScopeVar;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandAssignRef (size_t inPointerStackOffset, size_t pointAtOffsetOnStack);
	CommandAssignRef (size_t inPointerStackOffset, size_t objPosInStack, size_t memVarIdxPos);
	CommandAssignRef (size_t inPointerStackOffset, EScopeVar scopeVar, size_t varIdxOnScope);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END