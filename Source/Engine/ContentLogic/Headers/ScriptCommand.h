/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptCommand.h
  The parent class that represents a single command a Subroutine can execute.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"

SD_BEGIN
class ClmStack;
class CommandEndSection;
class ContentObject;
class ScriptScope;
class Subroutine;

class CONTENT_LOGIC_API ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The step number this command resides in the owning Subroutine. */
	size_t StepIdx;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ScriptCommand ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Executes the actual command.
	 * Returns the delta step command where 1 implies proceed to the next step in the callingFunction, and -1 implies stepping backwards by one command.
	 */
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const = 0;

	/**
	 * Invoked whenever an EndSection is evaluating the command above the BeginSection. This function is called to identify the next step in the flow of control.
	 */
	virtual Int SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetStepIdx (size_t newStepIdx);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline size_t GetStepIdx () const
	{
		return StepIdx;
	}
};
SD_END