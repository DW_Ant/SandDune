/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentLogicUnitTester.h
  Tests various aspects of the Content Logic Module.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"

#ifdef DEBUG_MODE
SD_BEGIN
class CONTENT_LOGIC_API ContentLogicUnitTester : public UnitTester
{
	DECLARE_CLASS(ContentLogicUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestStack (EUnitTestFlags testFlags) const;
	virtual bool TestScriptVariables (EUnitTestFlags testFlags) const;
	virtual bool TestContentObjects (EUnitTestFlags testFlags) const;
	virtual bool TestCommands (EUnitTestFlags testFlags) const;
	virtual bool TestPolymorphism (EUnitTestFlags testFlags) const;
	virtual bool TestScriptComponents (EUnitTestFlags testFlags) const;
	virtual bool TestScriptFunctions (EUnitTestFlags testFlags) const;
};
SD_END
#endif