/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandNative.h
  A command that executes a C++ lambda function.

  This command allows the ContentLogic module to invoke C++ functions
  to leverage its speed and access to the Sand Dune architecture.

  Since creating instances of these commands require the compiler to run,
  the editors can only invoke callable native functions in ScriptComponents
  rather than creating custom lambdas.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandNative : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Lambda to execute when this command processes. These lambdas are used for general C++ functions. If nullptr, then this command will attempt to execute the
	callable function with matching FunctionName within the owning ScriptComponent. */
	std::function<void(ScriptScope* scope, ClmStack& outStack)> OnExecution;

	/* If OnExecution is nullptr, this HashedString is used to find a CallableNativeFunction within the owning ScriptComponent. If one is found, it'll execute that
	function instead. This is used for user exposed native functions with context of the ScriptComponent. */
	HashedString FunctionName;



	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandNative (const std::function<void(ScriptScope* scope, ClmStack& outStack)>& inOnExecution);
	CommandNative (const HashedString& inFunctionName);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END