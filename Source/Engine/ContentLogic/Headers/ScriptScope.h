/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptScope.h
  This object defines the scope of a CLM environment. This houses global
  variables, global functions, and object instances.

  A scope defines the boundaries of the runtime environment. CLM objects
  cannot interact with other objects or variables in a different scope. This
  boundary also defines the limits for name clashes. One variable name will
  not clash with another variable of the same name if it resides in a different
  scope even if both variables are global.

  Some examples of scopes:
  * The dynamic audio asset can only interact with objects within the asset.
  * The map scripts can only interact with objects related to the same map.

  Multiple ScriptComponents can reference the same ScriptScope.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptVariable.h"
#include "Subroutine.h"

SD_BEGIN
class ContentClass;
class ContentObject;
class ScriptVariableRef;

class CONTENT_LOGIC_API ScriptScope : public Object
{
	DECLARE_CLASS(ScriptScope)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Collection of instances defined within the bounds of this scope. GlobalVariables is a vector in order to interface with ScriptVariableRef. */
	std::vector<ScriptVariable> GlobalVariables;
	std::unordered_map<size_t, Subroutine*> StaticFunctions;

	/* List of all allocated ContentObjects created within this Scope. This includes objects created within and outside of ScriptComponents.
	This isn't a true Heap, but it acts like one since only object pointers reside here. */
	std::unordered_map<size_t, ContentObject*> ObjHeap;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual ScriptVariable& CreateGlobalVariable (const HashedString& varName);
	virtual Subroutine& CreateStaticFunction (const HashedString& functionName, bool bNativeFunction, bool bConstFunction);
	virtual void RegisterContentObject (ContentObject* newObj);
	virtual void UnregisterContentObject (ContentObject* target);

	/**
	 * Iterates through each global variable until it finds one with the matching variable name.
	 * It'll return a variable reference that points at that variable if found.
	 */
	virtual ScriptVariableRef EditGlobalVariable (const HashedString& varName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<ScriptVariable>& ReadGlobalVariables () const
	{
		return GlobalVariables;
	}

	inline std::vector<ScriptVariable>& EditGlobalVariables ()
	{
		return GlobalVariables;
	}

	inline const std::unordered_map<size_t, Subroutine*>& ReadStaticFunctions () const
	{
		return StaticFunctions;
	}

	inline const std::unordered_map<size_t, ContentObject*>& ReadHeap () const
	{
		return ObjHeap;
	}
};
SD_END