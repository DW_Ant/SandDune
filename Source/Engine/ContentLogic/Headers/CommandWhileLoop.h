/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandWhileLoop.h
  A command that causes the flow of control to loop through the section until
  the associated condition is false.

  The Subroutine that uses this command is expected to update the associated bool
  to 'refresh' the condition every time this loop is executed.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandWhileLoop : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Offset in the stack that contains the ScriptVariable to evaluate. If it's a pointer type, it'll navigate to the ScriptVariable it's pointing to. */
	size_t BoolOffset;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandWhileLoop (size_t inBoolOffset);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
	virtual Int SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END