/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClmEngineComponent.h
  An EngineComponent that houses all global defined objects in the ContentLogic
  system.

  Global objects such as the ContentClasses, global variables, and global
  functions reside here.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"

SD_BEGIN
class ContentClass;
class Subroutine;

class CONTENT_LOGIC_API ClmEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(ClmEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of reserved words that are not permitted for variable or function names. Using these words may lead to unexpected behavior with the interpreter when it
	handles inlined expressions. The interpreter is case sensitive. It is up to the editor to check against these reserved words. */
	static const std::vector<DString> RESERVED_WORDS;

protected:
	/* List of all classes instantiated and ready. This component will take ownership over these classes. */
	std::unordered_map<size_t, ContentClass*> ScriptClasses;

	/* List of ContentClasses that do haven't loaded their parent class.
	Classes that are not expecting a parent class are added directly to ScriptClasses map instead of this vector. */
	std::vector<ContentClass*> OrphanedClasses;

	/* List of all static global functions that doesn't have context to a script class or scope (eg: shared functions between scopes). These subroutines will not have access to any variables other than local variables. */
	std::unordered_map<size_t, Subroutine*> GlobalFunctions;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a new ContentClass instance with the given name and parent class.
	 * Returns a reference to the class to allow the caller to initialize it.
	 * If the class already exists, it'll return nullptr.
	 */
	virtual ContentClass* CreateScriptClass (const HashedString& className, const HashedString& parentClass);

	/**
	 * Destroys the specified class.
	 * Be careful when destroying classes since any ContentObjects inheriting from those classes may still linger even though their associated class is destroyed.
	 * It's best to call this when unloading packages or when cleaning up a subsystem.
	 * @param bRecursiveDestroy If true, then it'll also destroy all classes that inherit from the class that was destroyed. Otherwise, it'll only set their parentClass reference to nullptr.
	 */
	virtual void DestroyScriptClass (ContentClass* classToDestroy, bool bRecursiveDestroy);

	virtual Subroutine& CreateGlobalFunction (const HashedString& functionName, bool bNativeFunction);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::unordered_map<size_t, ContentClass*>& ReadScriptClasses () const
	{
		return ScriptClasses;
	}

	inline const std::unordered_map<size_t, Subroutine*>& ReadGlobalFunctions () const
	{
		return GlobalFunctions;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeScriptClasses ();
	virtual void CreateComparisonFunctions ();
	virtual void CreateBoolOperations ();
	virtual void CreateConversionFunctions ();
	virtual void CreateMathFunctions ();
	virtual void CreateObjHandlingFunctions ();
};
SD_END