/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentClass.h
  An object that defines the meta data for a ContentObject. Each ContentObject
  instance of the same class references the same class instance.
=====================================================================
*/

#pragma once

#include "ContentLogic.h"
#include "ScriptVariable.h"
#include "Subroutine.h"

SD_BEGIN
class ContentObject;
class ScriptScope;

class CONTENT_LOGIC_API ContentClass
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EClassFlags
	{
		CF_None = 0x00,
		CF_Abstract = 0x01, //This class cannot instantiate objects of this type.
		CF_HideFromDropdown = 0x02 //This class will not appear as a class dropdown option, preventing other classes from deriving from it or referencing it as a class variable.
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Class this instance is inheriting from. */
	ContentClass* ParentClass;

	/* List of classes that inherit from this class. */
	std::vector<ContentClass*> ChildClasses;

	HashedString ClassName;

	/* Name of the parent class. Used for to handle cases where the child class is initialized before the parent. If empty, then it assumes that this class will not have a parent. */
	HashedString ParentClassName;

	/* List of all member functions visible in this class. This does not include functions defined in the ParentClass. */
	std::unordered_map<size_t, Subroutine*> MemberFunctions;

	/* List of all member variables in this class excluding the ones listed in the parent class. All ContentObjects created from this class will default their variables to these variables.
	This is a vector instead of a map since the order of variables matter since most member variables are accessed via index instead of hash. */
	std::vector<ScriptVariable> MemberVariables;

	/* List of default overrides for each possible member variable. Each member variable is expected to be defined in one of the parent classes. The variables are paired via VariableNames. */
	std::vector<ScriptVariable> DefaultOverrides;

	EClassFlags ClassFlags;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ContentClass (const HashedString& inClassName, const HashedString& inParentClassName);
	virtual ~ContentClass ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual Subroutine& CreateMemberFunction (const HashedString& functionName, Subroutine::EFunctionFlags functionFlags);
	virtual ScriptVariable& CreateMemberVariable (const HashedString& varName);

	/**
	 * Searches for the function in the class hierarchy and returns a reference to it if found.
	 * Order of operations ranging from highest priority to lowest priority:
	 * Find function in the child-most layer.
	 * Find function in parent class.
	 * Find global function in Scope.
	 * Find global static function in EngineComponent.
	 */
	virtual const Subroutine* FindFunction (ScriptScope* scope, size_t functionHash, ContentObject* objContext) const;

	/**
	 * Returns true if this class inherits from the specified class (or is exactly that specified class).
	 */
	virtual bool IsA (const ContentClass* possibleParent) const;

	/**
	 * Returns true if the given class inherits from this class.
	 */
	virtual bool IsParentOf (const ContentClass* possibleChild) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetParentClass (ContentClass* newParentClass);
	virtual void SetIsAbstract (bool isAbstract);
	virtual void SetHideFromDropdown (bool newHideFromDropdown);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ContentClass* GetParentClass () const
	{
		return ParentClass;
	}

	inline std::vector<ContentClass*> GetChildClasses () const
	{
		return ChildClasses;
	}

	inline const std::vector<ContentClass*>& ReadChildClasses () const
	{
		return ChildClasses;
	}

	inline const HashedString& ReadClassName () const
	{
		return ClassName;
	}

	inline const HashedString& ReadParentClassName () const
	{
		return ParentClassName;
	}

	inline const std::unordered_map<size_t, Subroutine*>& ReadMemberFunctions () const
	{
		return MemberFunctions;
	}

	inline std::unordered_map<size_t, Subroutine*>& EditMemberFunctions ()
	{
		return MemberFunctions;
	}

	inline const std::vector<ScriptVariable>& ReadMemberVariables () const
	{
		return MemberVariables;
	}

	inline std::vector<ScriptVariable>& EditMemberVariables ()
	{
		return MemberVariables;
	}

	inline const std::vector<ScriptVariable>& ReadDefaultOverrides () const
	{
		return DefaultOverrides;
	}

	inline std::vector<ScriptVariable>& EditDefaultOverrides ()
	{
		return DefaultOverrides;
	}

	inline bool IsAbstract () const
	{
		return ((ClassFlags & CF_Abstract) > 0);
	}

	inline bool IsHiddenFromDropdown () const
	{
		return ((ClassFlags & CF_HideFromDropdown) > 0);
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Recursively climbs up the class chain until it finds the function it's looking for.
	 */
	virtual const Subroutine* FindFunctionRecursive (size_t functionHash) const;

	/**
	 * Creates member functions that each class instance should always have.
	 * Not a virtual function since this is invoked from a constructor.
	 */
	void CreateCoreFunctions ();
};

DEFINE_ENUM_FUNCTIONS(ContentClass::EClassFlags)
SD_END