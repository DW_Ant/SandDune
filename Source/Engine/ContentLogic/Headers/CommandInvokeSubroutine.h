/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeSubroutine.h
  A command that executes a subroutine.
  Note: A pointer to the Subroutine should not be cached for the following reasons:
  -The Subroutine can vary based on the class of the ContentObject passed in.
  -A Subroutine can be removed whenever a package is unloaded.

  This command expects the following at the end of the callstack in the following order:
  -The output parameters received after calling the function.
  -The input parameters sent to the function.
  -Pointer to the object that contains the member function. This variable can be
  omitted if the Flags is either Global or Static.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"
#include "Subroutine.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandInvokeSubroutine : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Flags that describe the function. Only the Global and Static flags are used.
	Recemmended to improve performance and also disambiguate function signatures. For example if a static function ends with an object pointer parameter, this command
	would interpret that variable as an object to execute the function on instead of treating that object as a parameter. If the global or static flag was set, it would
	not misinterpret that variable. */
	Subroutine::EFunctionFlags Flags;
	size_t FunctionHash;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandInvokeSubroutine (Subroutine::EFunctionFlags inFlags, const HashedString& inFunctionName);
	CommandInvokeSubroutine (Subroutine::EFunctionFlags inFlags, size_t inFunctionHash);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END