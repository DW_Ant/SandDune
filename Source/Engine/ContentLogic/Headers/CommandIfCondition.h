/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandIfCondition.h
  A command that evaluates a bool to identify which command step
  to jump to.
=====================================================================
*/

#pragma once

#include "ScriptCommand.h"

SD_BEGIN
class CONTENT_LOGIC_API CommandIfCondition : public ScriptCommand
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Offset in the stack that contains the ScriptVariable to evaluate. If it's a pointer type, it'll navigate to the ScriptVariable it's pointing to. */
	size_t BoolOffset;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CommandIfCondition (size_t inBoolOffset);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual Int ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
	virtual Int SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const override;
};
SD_END