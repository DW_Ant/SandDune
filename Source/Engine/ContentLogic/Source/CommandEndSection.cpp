/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandEndSection.cpp
=====================================================================
*/

#include "CommandBeginSection.h"
#include "CommandEndSection.h"
#include "Subroutine.h"

SD_BEGIN
CommandEndSection::CommandEndSection () : ScriptCommand()
{
	//Noop
}

Int CommandEndSection::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	//Search for the BeginSection associated with this section.
	if (callingFunction != nullptr && StepIdx != INDEX_NONE && StepIdx > 1)
	{
		Int sectionLayer = 0;
		size_t i = StepIdx - 1;
		while (true)
		{
			if (i == 0)
			{
				//At the beginning of the command step list. Even if it's a begin section, skip it since there isn't a command to check against (checks the command before the begin section).
				return 1; //Move to the next command step following this end section.
			}

			if (dynamic_cast<CommandEndSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
			{
				++sectionLayer;
			}
			else if (dynamic_cast<CommandBeginSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
			{
				if (sectionLayer <= 0)
				{
					Int nextStep = callingFunction->ReadCommandSteps().at(i - 1)->SectionEvaluation(this, scope, callingFunction, OUT outCallstack, OUT outStack);
					CHECK(nextStep != 0);
					return nextStep;
				}
				
				--sectionLayer;
			}

			--i;
		}
	}

	return 1;
}
SD_END