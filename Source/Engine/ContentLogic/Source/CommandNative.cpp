/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandNative.cpp
=====================================================================
*/

#include "CommandNative.h"
#include "ContentObject.h"
#include "ScriptComponent.h"
#include "Subroutine.h"

SD_BEGIN
CommandNative::CommandNative (const std::function<void(ScriptScope*, ClmStack&)>& inOnExecution) : ScriptCommand(),
	OnExecution(inOnExecution),
	FunctionName()
{
	CHECK(OnExecution != nullptr)
}

CommandNative::CommandNative (const HashedString& inFunctionName) : ScriptCommand(),
	OnExecution(nullptr),
	FunctionName(inFunctionName)
{
	//Noop
}

Int CommandNative::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	if (OnExecution != nullptr)
	{
		OnExecution(scope, OUT outStack);
	}
	else if (FunctionName.GetHash() != 0)
	{
		if (ContentObject* executeOn = Subroutine::GetExecuteOn(scope, OUT outStack))
		{
			ScriptComponent* owningComp = executeOn->GetOwningComponent();
			if (owningComp != nullptr && owningComp->ReadCallableNativeFunctions().contains(FunctionName))
			{
				owningComp->ReadCallableNativeFunctions().at(FunctionName)(OUT outStack);
			}
		}
	}

	return 1;
}
SD_END