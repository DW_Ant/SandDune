/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentLogic.cpp
=====================================================================
*/

#include "ContentLogic.h"

SD_BEGIN
LogCategory ContentLogicLog(TXT("ContentLogic"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_STANDARD_OUTPUT |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OUTPUT_WINDOW);
SD_END