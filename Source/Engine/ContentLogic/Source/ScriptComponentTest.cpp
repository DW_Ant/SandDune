/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptComponentTest.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptComponentTest.h"
#include "Subroutine.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::ScriptComponentTest, SD::ScriptComponent)
SD_BEGIN

void ScriptComponentTest::InitProps ()
{
	Super::InitProps();

	ScriptClassName = HashedString("UnitTestScriptComp");
}

void ScriptComponentTest::BeginObject ()
{
	Super::BeginObject();

	RegisterCallableNativeFunction(HashedString("CallableConvertInt"), [&](ClmStack& outStack)
	{
		Int inInt = outStack.GetIntParam(Scope.Get(), 0, 1, true);
		Int outInt;
		CallableConvertInt(inInt, OUT outInt);
		outStack.WriteIntOutput(outInt, Scope.Get(), 0, 1, 1, true);
	});

	RegisterCallableNativeFunction(HashedString("CallableConvertFloat"), [&](ClmStack& outStack)
	{
		Float inFloat = outStack.GetFloatParam(Scope.Get(), 0, 1, true);
		Float outFloat;
		CallableConvertFloat(inFloat, OUT outFloat);
		outStack.WriteFloatOutput(outFloat, Scope.Get(), 0, 1, 1, true);
	});

	RegisterCallableNativeFunction(HashedString("CallableConvertString"), [&](ClmStack& outStack)
	{
		DString inString = outStack.GetStringParam(Scope.Get(), 0, 1, true);
		DString outString;
		CallableConvertString(inString, OUT outString);
		outStack.WriteStringOutput(outString, Scope.Get(), 0, 1, 1, true);
	});
}

void ScriptComponentTest::ProcessScriptFunction (Int inInt, Float inFloat, const DString& inString, Int& outInt, Float& outFloat, DString& outString)
{
	std::vector<ScriptVariable> output;
	if (ProcessScriptFunction_Implementation(inInt, inFloat, inString, OUT output))
	{
		CHECK(output.size() >= 3)
		outInt = output.at(0).ToInt();
		outFloat = output.at(1).ToFloat();
		outString = output.at(2).ToDString();
	}
}

void ScriptComponentTest::CallableConvertInt (Int inInt, Int& outInt) const
{
	outInt = inInt * -1;
}

void ScriptComponentTest::CallableConvertFloat (Float inFloat, Float& outFloat) const
{
	outFloat = inFloat * 2.f;
}

void ScriptComponentTest::CallableConvertString (const DString& inString, DString& outString) const
{
	outString = inString;
	outString.ToUpper();
}

IMPLEMENT_SCRIPT_FUNCTION_3PARAM(ScriptComponentTest, ProcessScriptFunction, Int, inInt, Float, inFloat, DString, inString);
SD_END
#endif