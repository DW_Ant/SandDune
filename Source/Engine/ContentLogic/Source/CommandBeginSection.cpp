/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandBeginSection.cpp
=====================================================================
*/

#include "CommandBeginSection.h"

SD_BEGIN
CommandBeginSection::CommandBeginSection () : ScriptCommand()
{
	//Noop
}

Int CommandBeginSection::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	return 1;
}
SD_END