/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandElseClause.cpp
=====================================================================
*/

#include "CommandElseClause.h"

SD_BEGIN
CommandElseClause::CommandElseClause () : ScriptCommand()
{
	//Noop
}

Int CommandElseClause::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	//IfCondition handles EndSection evaluation. If this command is executed, simply proceed inside to the next section.
	return 1;
}
SD_END