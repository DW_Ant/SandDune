/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentClass.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "CommandNative.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

SD_BEGIN
ContentClass::ContentClass (const HashedString& inClassName, const HashedString& inParentClassName) :
	ParentClass(nullptr),
	ClassName(inClassName),
	ParentClassName(inParentClassName),
	ClassFlags(CF_None)
{
	if (ParentClassName.GetHash() == 0) //Only root classes will implement these functions since subclasses will inherit them instead.
	{
		CreateCoreFunctions();
	}
}

ContentClass::~ContentClass ()
{
	std::vector<Subroutine*> pendingDelete;
	for (auto& memberFunc : MemberFunctions)
	{
		pendingDelete.push_back(memberFunc.second);
	}
	MemberFunctions.clear();

	for (Subroutine* memberFunc : pendingDelete)
	{
		delete memberFunc;
	}
}

Subroutine& ContentClass::CreateMemberFunction (const HashedString& functionName, Subroutine::EFunctionFlags functionFlags)
{
	if (MemberFunctions.contains(functionName.GetHash()))
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to create member function %s in %s since that function name is already in use."), functionName, ClassName);
		return *MemberFunctions.at(functionName.GetHash());
	}

	if ((functionFlags & Subroutine::FF_Global) > 0)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("A member function within a ContentClass cannot have the global flag. Those functions are registered to the ClmEngineComponent."));
		functionFlags ^= Subroutine::FF_Global;
	}

	MemberFunctions.insert({functionName.GetHash(), new Subroutine(functionName, functionFlags, this)});
	return *MemberFunctions.at(functionName.GetHash());
}

ScriptVariable& ContentClass::CreateMemberVariable (const HashedString& varName)
{
	for (ScriptVariable& var : MemberVariables)
	{
		if (var.ReadVariableName() == varName)
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot create member variable %s in %s since that variable name is already in use."), varName, ClassName);
			return var;
		}
	}

	MemberVariables.emplace_back(varName);
	return MemberVariables.at(MemberVariables.size() - 1);
}

const Subroutine* ContentClass::FindFunction (ScriptScope* scope, size_t functionHash, ContentObject* objContext) const
{
	if (objContext != nullptr)
	{
		//Downcast to the child-most class reference to the instance.
		const ContentClass* childClass = objContext->GetClass();
		const Subroutine* memberFunction = childClass->FindFunctionRecursive(functionHash);
		if (memberFunction != nullptr)
		{
			return memberFunction;
		}
	}

	if (scope != nullptr && scope->ReadStaticFunctions().contains(functionHash))
	{
		return scope->ReadStaticFunctions().at(functionHash);
	}

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)
	if (clmEngine->ReadGlobalFunctions().contains(functionHash))
	{
		return clmEngine->ReadGlobalFunctions().at(functionHash);
	}

	return nullptr;
}

bool ContentClass::IsA (const ContentClass* possibleParent) const
{
	const ContentClass* curParent = this;
	while (curParent != nullptr)
	{
		if (curParent == possibleParent)
		{
			return true;
		}

		curParent = curParent->ParentClass;
	}

	return false;
}

bool ContentClass::IsParentOf (const ContentClass* possibleChild) const
{
	return (possibleChild->IsA(this) && possibleChild != this);
}

void ContentClass::SetParentClass (ContentClass* newParentClass)
{
	if (ParentClass != nullptr)
	{
		ContainerUtils::RemoveItem(ParentClass->ChildClasses, this);
	}

	ParentClass = newParentClass;

	if (ParentClass != nullptr)
	{
		ParentClass->ChildClasses.push_back(this);
	}
}

void ContentClass::SetIsAbstract (bool isAbstract)
{
	if (isAbstract)
	{
		ClassFlags |= CF_Abstract;
	}
	else
	{
		ClassFlags &= ~CF_Abstract;
	}
}

void ContentClass::SetHideFromDropdown (bool newHideFromDropdown)
{
	if (newHideFromDropdown)
	{
		ClassFlags |= CF_HideFromDropdown;
	}
	else
	{
		ClassFlags &= ~CF_HideFromDropdown;
	}
}

const Subroutine* ContentClass::FindFunctionRecursive (size_t functionHash) const
{
	if (MemberFunctions.contains(functionHash))
	{
		return MemberFunctions.at(functionHash);
	}

	if (ParentClass != nullptr)
	{
		return ParentClass->FindFunctionRecursive(functionHash);
	}

	return nullptr;
}

void ContentClass::CreateCoreFunctions ()
{
	Subroutine& setTicking = CreateMemberFunction(HashedString("SetTicking"), Subroutine::FF_Native);
	{
		ScriptVariable& isTicking = setTicking.AddParameter(HashedString("isTicking"));
		isTicking.SetFixedType(ScriptVariable::VT_Bool);

		setTicking.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			ScriptVariableRef execOnVar = outStack.EditVariable(0);
			ContentObject* execOn = execOnVar.EditVar().ToObjPointer(scope);
			CHECK(execOn != nullptr)
			if (TickComponent* tick = execOn->GetScriptTick())
			{
				Bool isTicking = outStack.GetBoolParam(scope, 0, 1, true);
				tick->SetTicking(isTicking);
			}
		}));
	}

	Subroutine& setTickInterval = CreateMemberFunction(HashedString("SetTickInterval"), Subroutine::FF_Native);
	{
		ScriptVariable& tickInterval = setTickInterval.AddParameter(HashedString("tickInterval"));
		tickInterval.SetFixedType(ScriptVariable::VT_Float);
		tickInterval.WriteFloat(-1.f);

		setTickInterval.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			ScriptVariableRef execOnVar = outStack.EditVariable(0);
			ContentObject* execOn = execOnVar.EditVar().ToObjPointer(scope);
			CHECK(execOn != nullptr)
			if (TickComponent* tick = execOn->GetScriptTick())
			{
				Float tickInterval = outStack.GetFloatParam(scope, 0, 1, true);
				tick->SetTickInterval(tickInterval);
			}
		}));
	}

	Subroutine& destroyFunction = CreateMemberFunction(HashedString("Destroy"), Subroutine::FF_Native);
	{
		destroyFunction.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			ScriptVariableRef var = outStack.EditVariable(0);
			if (!var.IsValid() || var.EditVar().GetVarType() != ScriptVariable::VT_Obj)
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Destroy must be called in relation to an object reference. The end of the stack doesn't have a reference to 'this'."));
				return;
			}

			ContentObject* objToDestroy = var.EditVar().ToObjPointer(scope);
			if (objToDestroy != nullptr)
			{
				objToDestroy->Destroy();
			}
		}));
	}
}
SD_END