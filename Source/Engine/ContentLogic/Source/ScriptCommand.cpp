/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptCommand.cpp
=====================================================================
*/

#include "ScriptCommand.h"

SD_BEGIN
ScriptCommand::ScriptCommand () :
	StepIdx(INDEX_NONE)
{
	//Noop
}

Int ScriptCommand::SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	//By default, a command would simply move onto the next command following the end section.
	return 1;
}

void ScriptCommand::SetStepIdx (size_t newStepIdx)
{
	StepIdx = newStepIdx;
}
SD_END