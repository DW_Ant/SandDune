/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeSubroutine.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "CommandInvokeSubroutine.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

SD_BEGIN
CommandInvokeSubroutine::CommandInvokeSubroutine (Subroutine::EFunctionFlags inFlags, const HashedString& inFunctionName) : ScriptCommand(),
	Flags(inFlags)
{
	FunctionHash = inFunctionName.GetHash();
}

CommandInvokeSubroutine::CommandInvokeSubroutine (Subroutine::EFunctionFlags inFlags, size_t inFunctionHash) : ScriptCommand(),
	Flags(inFlags),
	FunctionHash(inFunctionHash)
{
	//Noop
}

Int CommandInvokeSubroutine::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	const Subroutine* functionToInvoke = nullptr;

	//Check for member functions
	if ((Flags & Subroutine::FF_Static) == 0 && (Flags & Subroutine::FF_Global) == 0)
	{
		ContentObject* functionOwner = Subroutine::GetExecuteOn(scope, OUT outStack);
		if (functionOwner != nullptr)
		{
			if (const ContentClass* objClass = functionOwner->GetClass())
			{
				functionToInvoke = objClass->FindFunction(scope, FunctionHash, functionOwner);
			}
		}
	}

	//Check for static functions
	if (functionToInvoke == nullptr && (Flags & Subroutine::FF_Global) == 0)
	{
		if (scope->ReadStaticFunctions().contains(FunctionHash))
		{
			functionToInvoke = scope->ReadStaticFunctions().at(FunctionHash);
		}
	}

	//Check for global functions
	if (functionToInvoke == nullptr)
	{
		ClmEngineComponent* localClmEngine = ClmEngineComponent::Find();
		if (localClmEngine != nullptr && localClmEngine->ReadGlobalFunctions().contains(FunctionHash))
		{
			functionToInvoke = localClmEngine->ReadGlobalFunctions().at(FunctionHash);
		}
	}

	if (functionToInvoke != nullptr)
	{
		functionToInvoke->ExecuteFunction(scope, OUT outCallstack, OUT outStack);
	}

	return 1;
}
SD_END