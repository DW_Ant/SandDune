/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Subroutine.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "ContentClass.h"
#include "ScriptCommand.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

SD_BEGIN
Subroutine::Subroutine (const HashedString& inFunctionName, EFunctionFlags inFlags, const ContentClass* inFunctionOwner) :
	FunctionName(inFunctionName),
	Flags(inFlags),
	FunctionOwner(inFunctionOwner)
{
	//Noop
}

Subroutine::~Subroutine ()
{
	for (size_t i = 0; i < CommandSteps.size(); ++i)
	{
		delete CommandSteps.at(i);
	}
	ContainerUtils::Empty(OUT CommandSteps);
}

ContentObject* Subroutine::GetExecuteOn (ScriptScope* context, ClmStack& outStack)
{
	ScriptVariableRef var = outStack.EditVariable(0);
	if (!var.IsValid())
	{
		return nullptr;
	}

	if (var.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		var = var.EditVar().GetPointerRecursive(context, outStack);
	}

	if (var.EditVar().GetVarType() != ScriptVariable::VT_Obj)
	{
		return nullptr;
	}

	return var.EditVar().ToObjPointer(context);
}

ScriptVariable& Subroutine::AddParameter (const HashedString& paramName)
{
	return Params.emplace_back(paramName);
}

ScriptVariable& Subroutine::AddOutput (const HashedString& outputName)
{
	return Output.emplace_back(outputName);
}

void Subroutine::AddCommand (ScriptCommand* newCommand)
{
	newCommand->SetStepIdx(CommandSteps.size());
	CommandSteps.push_back(newCommand);
}

void Subroutine::ExecuteFunction (ScriptScope* scope, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	if (outCallstack.size() >= 1000)
	{
		//Failsafe check in case there's an infinite recursion. This module isn't intended for excessive calls.
		ContentLogicLog.Log(LogCategory::LL_Critical, TXT("Stack size limit reached (%s) in %s. The ContentLogic module is significantly slower compared to native. Consider reducing the number of nested function calls or moving some of them inside C++ instead."), Int(outCallstack.size()), FunctionName);
		return;
	}

	DString newCallstack = DString::EmptyString;
	if (FunctionOwner != nullptr)
	{
		newCallstack = FunctionOwner->ReadClassName().ToString() + TXT("::");
	}
	newCallstack += FunctionName.GetString();
	outCallstack.push_back(newCallstack);

	Int step = 0;
	while (step < CommandSteps.size())
	{
		Int deltaStep = CommandSteps.at(step.ToUnsignedInt())->ExecuteCommand(scope, this, OUT outCallstack, OUT outStack);
		if (deltaStep == 0)
		{
			ContentLogicLog.Log(LogCategory::LL_Critical, TXT("Infinite recursion detected. The %s executes a function that does not advance the command step. Forcing delta step to the next command."), FunctionName);
			deltaStep = 1;
		}

		step += deltaStep;
		if (step < 0)
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Negative step detected (%s). The Subroutine cannot execute a negative command step. Aborting. . ."), step);
			break;
		}
	}

	outCallstack.pop_back();
}
SD_END