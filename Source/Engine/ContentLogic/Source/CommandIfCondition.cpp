/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandIfCondition.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandBeginSection.h"
#include "CommandElseClause.h"
#include "CommandEndSection.h"
#include "CommandIfCondition.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

SD_BEGIN
CommandIfCondition::CommandIfCondition (size_t inBoolOffset) : ScriptCommand(),
	BoolOffset(inBoolOffset)
{
	//Noop
}

Int CommandIfCondition::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef condition = outStack.EditVariable(BoolOffset);
	if (condition.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		condition = condition.ReadVar().GetPointerRecursive(scope, outStack);
	}

	if (condition.ReadVar().ToBool())
	{
		//Simple case, jump to the next step.
		return 1;
	}

	CHECK(callingFunction != nullptr)

	Int sectionLayer = 0;

	//Find the end of the section and jump to there.
	for (size_t i = StepIdx; i < callingFunction->ReadCommandSteps().size(); ++i)
	{
		if (dynamic_cast<CommandBeginSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
		{
			sectionLayer++;
			continue;
		}

		if (dynamic_cast<CommandEndSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
		{
			sectionLayer--;
			if (sectionLayer <= 0)
			{
				Int skipAmount = (i - StepIdx);
				//Check the next step. If it's an else clause, skip to the next step.
				if (i+1 < callingFunction->ReadCommandSteps().size() && dynamic_cast<CommandElseClause*>(callingFunction->ReadCommandSteps().at(i+1)) != nullptr)
				{
					skipAmount++; //Skip over the else clause to go straight to the BeginSection after it.
				}

				return skipAmount;
			}
		}
	}

	//Missing end section associated with this if condition's begin section. Jump to the end of the subroutine
	return callingFunction->ReadCommandSteps().size();
}

Int CommandIfCondition::SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	//Assuming this is jumping back from an if condition that passed. This function needs to jump to either the end of the else clause
	CHECK(callingFunction != nullptr)
	if (endSection->GetStepIdx() + 1 < callingFunction->ReadCommandSteps().size() && dynamic_cast<CommandElseClause*>(callingFunction->ReadCommandSteps().at(endSection->GetStepIdx() + 1)) != nullptr)
	{
		//This if condition has an else clause. Need to jump ahead to the end section of that else clause.
		Int sectionLayer = 0;
		for (size_t i = endSection->GetStepIdx() + 2 /*1 = ElseClause, 2 = BeginSection*/ ; i < callingFunction->ReadCommandSteps().size(); ++i)
		{
			if (dynamic_cast<CommandBeginSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
			{
				sectionLayer++;
				continue;
			}

			if (dynamic_cast<CommandEndSection*>(callingFunction->ReadCommandSteps().at(i)) != nullptr)
			{
				sectionLayer--;
				if (sectionLayer <= 0)
				{
					return (i - endSection->GetStepIdx()); //skip over the end section
				}
			}
		}

		//Not enough end sections found. Jump to the end to assume that the section covers the rest of the steps.
		return callingFunction->ReadCommandSteps().size();
	}

	//This if condition does not have an else clause. Simply advance to the next step after the end section.
	return 1;
}
SD_END