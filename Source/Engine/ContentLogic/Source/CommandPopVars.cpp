/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandPopVars.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandPopVars.h"

SD_BEGIN
CommandPopVars::CommandPopVars (size_t inNumVars) : ScriptCommand(),
	NumVars(inNumVars)
{
	//Noop
}

Int CommandPopVars::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	for (size_t i = 0; i < NumVars; ++i)
	{
		outStack.PopVariable();
	}

	return 1;
}
SD_END