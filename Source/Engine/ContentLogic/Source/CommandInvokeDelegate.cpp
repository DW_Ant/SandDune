/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeDelegate.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandInvokeDelegate.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

SD_BEGIN
CommandInvokeDelegate::CommandInvokeDelegate (size_t inDelegateOffset) : ScriptCommand(),
	DelegateOffset(inDelegateOffset)
{
	//Noop
}

Int CommandInvokeDelegate::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef var = outStack.EditVariable(DelegateOffset);
	if (var.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		var = var.EditVar().GetPointerRecursive(scope, outStack);
	}

	if (var.EditVar().GetVarType() != ScriptVariable::VT_Function)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to invoke delegate since the variable %s was found at stack position %s. That is not a delegate variable."), var.EditVar().ToDString(), Int(DelegateOffset));
		return 1;
	}

	ScriptVariable::SScriptDelegate scriptDelegate = var.EditVar().ToFunction(scope);
	if (scriptDelegate.Function == nullptr)
	{
		//Nothing is bound to this delegate.
		return 1;
	}

#ifdef DEBUG_MODE
	if (!ValidateParameters(scope, scriptDelegate.Function, outStack))
	{
		return 1;
	}
#endif

	if (scriptDelegate.FunctionOwner != nullptr)
	{
		//The function handler must be a member function. Add a 'this' variable to the end of the stack.
		ScriptVariableRef thisVar = outStack.PushVariable();
		thisVar.EditVar().WriteObjPointer(scriptDelegate.FunctionOwner);
	}

	scriptDelegate.Function->ExecuteFunction(scope, OUT outCallstack, OUT outStack);

	if (scriptDelegate.FunctionOwner != nullptr)
	{
		//Pop this
		outStack.PopVariable();
	}

	return 1;
}

bool CommandInvokeDelegate::ValidateParameters (ScriptScope* scope, const Subroutine* targetFunction, ClmStack& outStack) const
{
	for (size_t stackOffset = 0; stackOffset < targetFunction->ReadParams().size(); ++stackOffset)
	{
		ScriptVariableRef stackVar = outStack.EditVariable(stackOffset);
		if (stackVar.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
		{
			stackVar = stackVar.EditVar().GetPointerRecursive(scope, outStack);
		}

		size_t paramIdx = (targetFunction->ReadParams().size() - 1) - stackOffset;
		if (stackVar.EditVar().GetVarType() != targetFunction->ReadParams().at(paramIdx).GetVarType())
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to invoke delegate %s since there's a parameter mismatch at param index %s. The stack contains var type %s but the parameter expects type %s."), targetFunction->ReadFunctionName(), Int(paramIdx), Int(stackVar.EditVar().GetVarType()), Int(targetFunction->ReadParams().at(paramIdx).GetVarType()));
			return false;
		}
	}

	//Check output vars
	for (size_t outIdx = 0; outIdx < targetFunction->ReadOutput().size(); ++outIdx)
	{
		size_t stackOffset = (targetFunction->ReadOutput().size() - 1) - outIdx;
		stackOffset += targetFunction->ReadParams().size();

		ScriptVariableRef stackVar = outStack.EditVariable(stackOffset);
		if (stackVar.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
		{
			stackVar = stackVar.EditVar().GetPointerRecursive(scope, outStack);
		}

		if (stackVar.EditVar().GetVarType() != targetFunction->ReadOutput().at(outIdx).GetVarType())
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to invoke delegate %s since there's a output variable mismatch at output index %s. The stack contains var type %s but the output expects type %s."), targetFunction->ReadFunctionName(), Int(outIdx), Int(stackVar.EditVar().GetVarType()), Int(targetFunction->ReadOutput().at(outIdx).GetVarType()));
			return false;
		}
	}

	return true;
}
SD_END