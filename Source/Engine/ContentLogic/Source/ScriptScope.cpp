/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptScope.cpp
=====================================================================
*/

#include "ContentObject.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

IMPLEMENT_CLASS(SD::ScriptScope, SD::Object)
SD_BEGIN

void ScriptScope::Destroyed ()
{
	while (ObjHeap.size() > 0)
	{
		size_t failsafeSize = ObjHeap.size();
		ContentObject* obj = ObjHeap.begin()->second;
		obj->Destroy();

		if (failsafeSize == ObjHeap.size())
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("%s was registered to the script heap, but it didn't unregister itself during the Destroy call."), ObjHeap.begin()->second->ToString());
			ObjHeap.erase(ObjHeap.begin());
		}
	}

	//Delete static functions
	std::vector<Subroutine*> pendingDelete;
	for (auto& staticFunc : StaticFunctions)
	{
		pendingDelete.push_back(staticFunc.second);
	}
	StaticFunctions.clear();

	for (Subroutine* staticFunc : pendingDelete)
	{
		delete staticFunc;
	}

	Super::Destroyed();
}

ScriptVariable& ScriptScope::CreateGlobalVariable (const HashedString& varName)
{
	//Ensure the variable name doesn't exist
	for (size_t i = 0; i < GlobalVariables.size(); ++i)
	{
		if (GlobalVariables.at(i).ReadVariableName() == varName)
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot create global variable %s since that variable already exists."), varName);
			return GlobalVariables.at(i);
		}
	}

	GlobalVariables.push_back(varName);
	return GlobalVariables.at(GlobalVariables.size() - 1);
}

Subroutine& ScriptScope::CreateStaticFunction (const HashedString& functionName, bool bNativeFunction, bool bConstFunction)
{
	if (!StaticFunctions.contains(functionName.GetHash()))
	{
		Subroutine::EFunctionFlags flags = Subroutine::FF_Static;
		if (bNativeFunction)
		{
			flags |= Subroutine::FF_Native;
		}

		if (bConstFunction)
		{
			flags |= Subroutine::FF_Const;
		}

		StaticFunctions.insert({functionName.GetHash(), new Subroutine(functionName, flags, nullptr)});
	}

	return *StaticFunctions.at(functionName.GetHash());
}

void ScriptScope::RegisterContentObject (ContentObject* newObj)
{
	if (newObj == nullptr)
	{
		return;
	}

	if (ObjHeap.contains(newObj->GetId()))
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot register %s to the scope's heap since the ID %s is already taken."), newObj->ToString(), Int(newObj->GetId()));
		return;
	}

	ObjHeap.insert({newObj->GetId(), newObj});
}

void ScriptScope::UnregisterContentObject (ContentObject* target)
{
	if (target != nullptr && ObjHeap.contains(target->GetId()))
	{
		ObjHeap.erase(target->GetId());
	}
}

ScriptVariableRef ScriptScope::EditGlobalVariable (const HashedString& varName)
{
	for (size_t i = 0; i < GlobalVariables.size(); ++i)
	{
		if (GlobalVariables.at(i).ReadVariableName() == varName)
		{
			return ScriptVariableRef(&GlobalVariables, i);
		}
	}

	return ScriptVariableRef(nullptr, INDEX_NONE);
}
SD_END