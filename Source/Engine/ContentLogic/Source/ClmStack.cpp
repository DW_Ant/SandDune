/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClmStack.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

SD_BEGIN
ClmStack::ClmStack () :
	TopIdx(INDEX_NONE)
{
	//Noop
}

ScriptVariableRef ClmStack::PushVariable (const HashedString& varName)
{
	TopIdx = (TopIdx != INDEX_NONE) ? TopIdx+1 : 0;

	if (TopIdx < Stack.size())
	{
		Stack.at(TopIdx).Reset();
		Stack.at(TopIdx).EditVariableName() = varName;
		Stack.at(TopIdx).SetIsValid(true);
	}
	else
	{
		Stack.emplace_back(varName);
	}

	return ScriptVariableRef(&Stack, TopIdx);
}

ScriptVariableRef ClmStack::PushVariable ()
{
	return PushVariable(HashedString());
}

void ClmStack::PopVariable ()
{
	CHECK_INFO(TopIdx != INDEX_NONE, "The ClmStack is already empty.")
	if (TopIdx == INDEX_NONE)
	{
		return;
	}

	Stack.at(TopIdx).SetIsValid(false);
	if (TopIdx == 0)
	{
		TopIdx = INDEX_NONE;
	}
	else
	{
		TopIdx--;
	}
}

void ClmStack::Shrink ()
{
	if (TopIdx == INDEX_NONE)
	{
		ContainerUtils::Empty(OUT Stack);
	}
	else if (TopIdx < Stack.size() - 1)
	{
		Stack.resize(TopIdx + 1);
	}
}

ScriptVariableRef ClmStack::EditVariable (size_t offset)
{
	CHECK(TopIdx >= offset && TopIdx != INDEX_NONE)
	return ScriptVariableRef(&Stack, TopIdx - offset);
}

size_t ClmStack::GetStackSize () const
{
	if (TopIdx == INDEX_NONE)
	{
		return 0;
	}

	return TopIdx + 1;
}

Bool ClmStack::GetBoolParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToBool();
		}
	}

	return param.ToBool();
}

Int ClmStack::GetIntParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToInt();
		}
	}

	return param.ToInt();
}

Float ClmStack::GetFloatParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToFloat();
		}
	}

	return param.ToFloat();
}

DString ClmStack::GetStringParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToDString();
		}
	}

	return param.ToDString();
}

ScriptVariable::SScriptDelegate ClmStack::GetFunctionParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToFunction(scope);
		}
	}

	return param.ToFunction(scope);
}

ContentObject* ClmStack::GetObjParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToObjPointer(scope);
		}
	}

	return param.ToObjPointer(scope);
}

const ContentClass* ClmStack::GetClassParam (ScriptScope* scope, size_t paramIdx, size_t numParams, bool bContainsThis) const
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(paramIdx < numParams)

	const ScriptVariable& param = ReadVar((numParams-1) - paramIdx);
	if (param.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = param.GetPointerRecursive(scope, *const_cast<ClmStack*>(this));
		if (pointedAt.IsValid())
		{
			return pointedAt.ReadVar().ToClass();
		}
	}

	return param.ToClass();
}

void ClmStack::WriteBoolOutput (Bool value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteBool(value);
		}
	}
	else
	{
		var.WriteBool(value);
	}
}

void ClmStack::WriteIntOutput (Int value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteInt(value);
		}
	}
	else
	{
		var.WriteInt(value);
	}
}

void ClmStack::WriteFloatOutput (Float value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteFloat(value);
		}
	}
	else
	{
		var.WriteFloat(value);
	}
}
void ClmStack::WriteStringOutput (const DString& value, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteDString(value);
		}
	}
	else
	{
		var.WriteDString(value);
	}
}

void ClmStack::WriteFunctionOutput (ContentObject* functionOwner, const Subroutine* function, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteFunction(functionOwner, function);
		}
	}
	else
	{
		var.WriteFunction(functionOwner, function);
	}
}

void ClmStack::WriteObjOutput (ContentObject* obj, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteObjPointer(obj);
		}
	}
	else
	{
		var.WriteObjPointer(obj);
	}
}

void ClmStack::WriteClassOutput (const ContentClass* contentClass, ScriptScope* scope, size_t outParamIdx, size_t numOutParam, size_t numParams, bool bContainsThis)
{
	if (bContainsThis)
	{
		numParams++;
	}

	CHECK(outParamIdx < numOutParam)

	ScriptVariable& var = EditVar(((numOutParam-1) - outParamIdx) + numParams);
	if (var.GetVarType() == ScriptVariable::VT_Pointer)
	{
		ScriptVariableRef pointedAt = var.GetPointerRecursive(scope, *this);
		if (pointedAt.IsValid())
		{
			pointedAt.EditVar().WriteClass(contentClass);
		}
	}
	else
	{
		var.WriteClass(contentClass);
	}
}
SD_END