/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentObject.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptComponent.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

IMPLEMENT_CLASS(SD::ContentObject, SD::Entity)
SD_BEGIN
void ContentObject::InitProps ()
{
	Super::InitProps();

	Class = nullptr;
	OwningComponent = nullptr;
	Id = 0;
	OwnerId = 0;
	Scope = nullptr;
	ScriptTickFunction = nullptr;
}

DString ContentObject::ToString () const
{
	if (IsDefaultObject()
#ifdef DEBUG_MODE
		|| !DebugName.IsEmpty()
#endif
	)
	{
		//Display either Default_X or whatever DebugName was set to it.
		return Super::ToString();
	}

	//Otherwise display this object's associated script class with an ID.
	if (Class != nullptr)
	{
		return Class->ReadClassName().ToString() + TXT("_") + DString::MakeString(Id);
	}
	
	return TXT("UnknownClass_") + DString::MakeString(Id);
}

void ContentObject::Destroyed ()
{
	DestroyContentObject();

	Super::Destroyed();
}

ScriptVariableRef ContentObject::FindMemberVariable (const HashedString& varName, size_t& outVarIdx)
{
	for (size_t i = 0; i < MemberVariables.size(); ++i)
	{
		if (MemberVariables.at(i).ReadVariableName() == varName)
		{
			outVarIdx = i;
			return ScriptVariableRef(&MemberVariables, i);
		}
	}

	outVarIdx = INDEX_NONE;
	return ScriptVariableRef(&MemberVariables, INDEX_NONE);
}

ScriptVariableRef ContentObject::GetMemberVariable (size_t varIdx)
{
	return ScriptVariableRef(&MemberVariables, varIdx);
}

void ContentObject::BeginContentObject (const ContentClass* inClass, ScriptScope* context, ScriptComponent* inOwningComponent, ContentObject* owner)
{
	if (Id != 0)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot begin %s since that object instance is already initialized."), ToString());
		return;
	}

	Class = inClass;
	if (Class != nullptr)
	{
		InitMemberVariables();

		//If this class defines a Tick function, then create a TickComponent that'll invoke that function.
		//Don't use Class::FindFunction since this shouldn't find static or global functions.
		HashedString tickFunctionName("Tick");
		for (const ContentClass* curClass = Class; curClass != nullptr; curClass = curClass->GetParentClass())
		{
			if (curClass->ReadMemberFunctions().contains(tickFunctionName.GetHash()))
			{
				ScriptTickFunction = curClass->ReadMemberFunctions().at(tickFunctionName.GetHash());
				break;
			}
		}

		if (ScriptTickFunction != nullptr)
		{
			ScriptTick = TickComponent::CreateObject(TICK_GROUP_CONTENT_LOGIC);
			if (AddComponent(ScriptTick))
			{
				ScriptTick->SetTickHandler(SDFUNCTION_1PARAM(this, ContentObject, HandleScriptTick, void, Float));
			}
		}
	}

	InitId();

	if (context != nullptr)
	{
		context->RegisterContentObject(this);
		Scope = context;
	}

	OwningComponent = inOwningComponent;
	SetOwner(owner);

	//Notify script to call BeginObject
	if (Class != nullptr)
	{
		const Subroutine* beginFunction = Class->FindFunction(Scope, HashedString("BeginObject").GetHash(), this);
		if (beginFunction != nullptr)
		{
			//Invoke the script's BeginFunction
			std::vector<DString> callstack;
			ClmStack stack;
			beginFunction->ExecuteFunction(Scope, OUT callstack, OUT stack);
		}
	}
}

void ContentObject::DestroyContentObject ()
{
	//Destroy all objects this instance owns before this object is destroyed
	if (Scope != nullptr)
	{
		//Cache the IDs since the destroyed objects would write to this object's OwnedObjects vector while it's looping.
		std::vector<size_t> cachedOwnedObjs = OwnedObjects;
		for (size_t childId : cachedOwnedObjs)
		{
			if (Scope->ReadHeap().contains(childId))
			{
				if (ContentObject* ownedObj = Scope->ReadHeap().at(childId))
				{
					ownedObj->Destroy();
				}
			}
		}
	}

	if (Class != nullptr && Scope != nullptr)
	{
		const Subroutine* destroyedFunction = Class->FindFunction(Scope, HashedString("Destroyed").GetHash(), this);
		if (destroyedFunction != nullptr)
		{
			//Invoke the script's Destroyed function before this instance is removed.
			std::vector<DString> callstack;
			ClmStack stack;
			destroyedFunction->ExecuteFunction(Scope, OUT callstack, OUT stack);
		}
	}

	if (ContentObject* owner = GetOwner())
	{
		ContainerUtils::RemoveItem(OUT owner->OwnedObjects, OUT GetId());
	}

	if (Scope != nullptr)
	{
		Scope->UnregisterContentObject(this); //This may be necessary if this object was deleted before the scope was destroyed, and if this object doesn't reside within a ScriptComponent.
		Scope = nullptr;
	}
}

bool ContentObject::IsOwnerOf (ContentObject* otherObj) const
{
	if (otherObj == nullptr)
	{
		return false;
	}

	for (ContentObject* curObj = otherObj->GetOwner(); curObj != nullptr; curObj = curObj->GetOwner())
	{
		if (curObj == this)
		{
			return true;
		}
	}

	return false;
}

void ContentObject::SetOwner (ContentObject* newOwner)
{
	if (ContentObject* oldOwner = GetOwner())
	{
		ContainerUtils::RemoveItem(OUT oldOwner->OwnedObjects, GetId());
	}

	OwnerId = 0;
	if (newOwner != nullptr)
	{
		//Ensure there isn't a circular ownership
		if (newOwner->IsOwnerOf(this))
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot set %s to own %s due to circular ownership. %s currently owns %s."), ToString(), newOwner->ToString(), newOwner->ToString(), ToString());
			return;
		}

		ContainerUtils::AddUnique(OUT newOwner->OwnedObjects, GetId());
		OwnerId = newOwner->GetId();
	}
}

ContentObject* ContentObject::GetOwner () const
{
	if (OwnerId != 0 && Scope != nullptr && Scope->ReadHeap().contains(OwnerId))
	{
		return Scope->ReadHeap().at(OwnerId);
	}

	return nullptr;
}

void ContentObject::InitMemberVariables ()
{
	CHECK(Class != nullptr)
	ContainerUtils::Empty(OUT MemberVariables);

	std::vector<const ContentClass*> classChain;
	for (const ContentClass* curClass = Class; curClass != nullptr; curClass = curClass->GetParentClass())
	{
		classChain.push_back(curClass);
	}

	if (ContainerUtils::IsEmpty(classChain))
	{
		return; //This class has no member variables
	}

	//Navigate in reverse order since the MemberVariables need to be ordered from parent to child.
	size_t classIdx = classChain.size() - 1;
	while (true)
	{
		for (size_t varIdx = 0; varIdx < classChain.at(classIdx)->ReadMemberVariables().size(); ++varIdx)
		{
			//Create a copy of the variable. This also copies over the class defaults
			MemberVariables.push_back(classChain.at(classIdx)->ReadMemberVariables().at(varIdx));
		}

		if (classIdx == 0)
		{
			break;
		}

		--classIdx;
	}

	//Apply class default overrides (eg: subclasses overriding the member variable defaults).
	if (classChain.size() > 1)
	{
		classIdx = classChain.size() - 2; //Skip root class since the root cannot override its own variables.
		while (true)
		{
			for (const ScriptVariable& classDefault : classChain.at(classIdx)->ReadDefaultOverrides())
			{
				bool bFoundVar = false;
				for (ScriptVariable& memVar : MemberVariables)
				{
					if (memVar.ReadVariableName() == classDefault.ReadVariableName())
					{
						memVar = classDefault;
						bFoundVar = true;
						break; //Break MemberVariables
					}
				}

				if (!bFoundVar)
				{
					ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to apply variable default since the variable named %s is not found in a %s instance."), classDefault.ReadVariableName(), Class->ReadClassName());
				}
			}

			if (classIdx == 0)
			{
				break;
			}

			--classIdx;
		}
	}
}

void ContentObject::InitId ()
{
	static size_t nextId = 0;
	++nextId;
	Id = nextId;
}

void ContentObject::HandleScriptTick (Float deltaSec)
{
	CHECK(ScriptTickFunction != nullptr)

	std::vector<DString> callstack;
	ClmStack stack;
	ScriptVariableRef tickParam = stack.PushVariable();
	tickParam.EditVar().WriteFloat(deltaSec);

	ScriptVariableRef thisParam = stack.PushVariable();
	thisParam.EditVar().WriteObjPointer(this);

	ScriptTickFunction->ExecuteFunction(Scope, OUT callstack, OUT stack);
}
SD_END