/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandAssignVar.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandAssignVar.h"
#include "ScriptScope.h"
#include "ScriptVariableRef.h"

SD_BEGIN
CommandAssignVar::CommandAssignVar (bool inAutoDereferenceSrc, bool inAutoDereferenceDest, size_t inSourceOffset, size_t inDestinationOffset) : ScriptCommand(),
	bAutoDereferenceSrc(inAutoDereferenceSrc),
	bAutoDereferenceDest(inAutoDereferenceDest),
	SourceOffset(inSourceOffset),
	DestinationOffset(inDestinationOffset)
{
	//Noop
}

Int CommandAssignVar::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef source = outStack.EditVariable(SourceOffset);
	if (bAutoDereferenceSrc)
	{
		if (source.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
		{
			source = source.ReadVar().GetPointerRecursive(scope, outStack);
		}
	}

	ScriptVariableRef destination = outStack.EditVariable(DestinationOffset);
	if (bAutoDereferenceDest)
	{
		if (destination.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
		{
			destination = destination.ReadVar().GetPointerRecursive(scope, outStack);
		}
	}

	destination.EditVar() = source.ReadVar();
	return 1;
}

CommandAssignBool::CommandAssignBool (size_t inDestinationOffset, Bool inNewValue) : ScriptCommand(),
	DestinationOffset(inDestinationOffset),
	NewValue(inNewValue)
{
	//Noop
}

Int CommandAssignBool::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef destination = outStack.EditVariable(DestinationOffset);
	if (destination.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		destination = destination.ReadVar().GetPointerRecursive(scope, outStack);
	}

	destination.EditVar().WriteBool(NewValue);
	return 1;
}

CommandAssignInt::CommandAssignInt (size_t inDestinationOffset, Int inNewValue) : ScriptCommand(),
	DestinationOffset(inDestinationOffset),
	NewValue(inNewValue)
{
	//Noop
}

Int CommandAssignInt::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef destination = outStack.EditVariable(DestinationOffset);
	if (destination.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		destination = destination.ReadVar().GetPointerRecursive(scope, outStack);
	}

	destination.EditVar().WriteInt(NewValue);
	return 1;
}

CommandAssignFloat::CommandAssignFloat (size_t inDestinationOffset, Float inNewValue) : ScriptCommand(),
	DestinationOffset(inDestinationOffset),
	NewValue(inNewValue)
{
	//Noop
}

Int CommandAssignFloat::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef destination = outStack.EditVariable(DestinationOffset);
	if (destination.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		destination = destination.ReadVar().GetPointerRecursive(scope, outStack);
	}

	destination.EditVar().WriteFloat(NewValue);
	return 1;
}

CommandAssignString::CommandAssignString (size_t inDestinationOffset, const DString& inNewValue) : ScriptCommand(),
	DestinationOffset(inDestinationOffset),
	NewValue(inNewValue)
{
	//Noop
}

Int CommandAssignString::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef destination = outStack.EditVariable(DestinationOffset);
	if (destination.ReadVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		destination = destination.ReadVar().GetPointerRecursive(scope, outStack);
	}

	destination.EditVar().WriteDString(NewValue);
	return 1;
}

CommandAssignRef::CommandAssignRef (size_t inPointerStackOffset, size_t pointAtOffsetOnStack) : ScriptCommand(),
	PointerStackOffset(inPointerStackOffset),
	PointAtOffset(pointAtOffsetOnStack),
	ObjStackPosition(INDEX_NONE),
	bIsScopeVar(false)
{
	//Noop
}

CommandAssignRef::CommandAssignRef (size_t inPointerStackOffset, size_t objPosInStack, size_t memVarIdxPos) : ScriptCommand(),
	PointerStackOffset(inPointerStackOffset),
	PointAtOffset(memVarIdxPos),
	ObjStackPosition(objPosInStack),
	bIsScopeVar(false)
{
	//Noop
}

CommandAssignRef::CommandAssignRef (size_t inPointerStackOffset, EScopeVar scopeVar, size_t varIdxOnScope) : ScriptCommand(),
	PointerStackOffset(inPointerStackOffset),
	PointAtOffset(varIdxOnScope),
	ObjStackPosition(INDEX_NONE),
	bIsScopeVar(true)
{
	//Noop
}

Int CommandAssignRef::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef dest = outStack.EditVariable(PointerStackOffset);
	if (bIsScopeVar)
	{
		CHECK(PointAtOffset < scope->ReadGlobalVariables().size())
		dest.EditVar().WriteScopePointer(scope, PointAtOffset);
	}
	else if (ObjStackPosition == INDEX_NONE)
	{
		CHECK(PointAtOffset < outStack.GetStackSize())

		//Create a pointer to a variable on the stack.
		dest.EditVar().WritePointer((outStack.GetStackSize() - 1) - PointAtOffset);
	}
	else
	{
		ScriptVariableRef objVar = outStack.EditVariable(ObjStackPosition);
		CHECK(objVar.IsValid())
		if (objVar.EditVar().GetVarType() != ScriptVariable::VT_Obj)
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to assign variable reference to point at a member variable since %s is not an object pointer variable."), objVar.EditVar().InterpretValueAsString(scope, outStack));
		}
		else
		{
			ContentObject* obj = objVar.EditVar().ToObjPointer(scope);
			dest.EditVar().WritePointer(obj, PointAtOffset);
		}
	}

	return 1;
}
SD_END