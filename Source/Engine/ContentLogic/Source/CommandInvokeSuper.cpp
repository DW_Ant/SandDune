/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandInvokeSuper.cpp
=====================================================================
*/

#include "CommandInvokeSuper.h"
#include "ContentClass.h"
#include "Subroutine.h"

SD_BEGIN
CommandInvokeSuper::CommandInvokeSuper () : ScriptCommand()
{
	//Noop
}

Int CommandInvokeSuper::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	CHECK(callingFunction != nullptr)
	if ((callingFunction->GetFlags() & (Subroutine::FF_Global | Subroutine::FF_Static)) > 0)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot invoke Super for %s. There are no parent classes for global or static functions."), callingFunction->ReadFunctionName());
		return 1;
	}

	const ContentClass* curClass = callingFunction->GetFunctionOwner();
	CHECK(curClass != nullptr)

	//Skipping curClass to start with parent class is intentional
	for (curClass = curClass->GetParentClass(); curClass != nullptr; curClass = curClass->GetParentClass())
	{
		if (curClass->ReadMemberFunctions().contains(callingFunction->ReadFunctionName().GetHash()))
		{
			const Subroutine* superFunction = curClass->ReadMemberFunctions().at(callingFunction->ReadFunctionName().GetHash());
			CHECK(superFunction != nullptr)
			superFunction->ExecuteFunction(scope, OUT outCallstack, OUT outStack);
			break;
		}
	}

	return 1;
}
SD_END