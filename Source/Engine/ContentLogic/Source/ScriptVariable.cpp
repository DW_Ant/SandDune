/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptVariable.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

SD_BEGIN
ScriptVariable::SScriptDelegate::SScriptDelegate () :
	FunctionOwner(nullptr),
	Function(nullptr)
{
	//Noop
}

ScriptVariable::ScriptVariable () :
	VarType(VT_Unknown),
	bFixedType(false),
	bIsValid(true),
	VariableName()
{
	//Noop
}

ScriptVariable::ScriptVariable (const HashedString& inVariableName) :
	VarType(VT_Unknown),
	bFixedType(false),
	bIsValid(true),
	VariableName(inVariableName)
{
	//Noop
}

ScriptVariable::ScriptVariable (const ScriptVariable& cpyObj) :
	VarType(cpyObj.VarType),
	bFixedType(cpyObj.bFixedType),
	bIsValid(cpyObj.bIsValid),
	VariableName(cpyObj.VariableName)
{
	cpyObj.Data.CopyBufferTo(OUT Data);
}

void ScriptVariable::operator= (const ScriptVariable& cpyObj)
{
	VarType = cpyObj.VarType;
	bFixedType = cpyObj.bFixedType;
	bIsValid = cpyObj.bIsValid;
	cpyObj.Data.CopyBufferTo(OUT Data);
	VariableName = cpyObj.VariableName;
}

bool ScriptVariable::operator== (const ScriptVariable& other) const
{
	if (VarType != other.VarType || Data.GetNumBytes() != other.Data.GetNumBytes())
	{
		return false;
	}

	for (size_t i = 0; i < Data.GetNumBytes(); ++i)
	{
		if (Data.ReadRawData().at(i) != other.Data.ReadRawData().at(i))
		{
			return false;
		}
	}

	return true;
}

bool ScriptVariable::operator!= (const ScriptVariable& other) const
{
	return !(*this == other);
}

void ScriptVariable::SetFixedType (EVarType fixedType)
{
	if (!bFixedType && fixedType != VT_Unknown)
	{
		VarType = fixedType;
		bFixedType = true;
	}
}

void ScriptVariable::Reset ()
{
	ResetType();
	VariableName.SetString(DString::EmptyString);
	Data.EmptyBuffer();
}

void ScriptVariable::ResetType ()
{
	bFixedType = false;
	VarType = VT_Unknown;
}

void ScriptVariable::WriteBool (Bool flag)
{
	if (bFixedType && VarType != VT_Bool)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a bool type."), VariableName);
		return;
	}

	VarType = VT_Bool;
	Data.EmptyBuffer();
	Data << flag;
}

void ScriptVariable::WriteInt (Int value)
{
	if (bFixedType && VarType != VT_Int)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from am int type."), VariableName);
		return;
	}

	VarType = VT_Int;
	Data.EmptyBuffer();
	Data << value;
}

void ScriptVariable::WriteFloat (Float value)
{
	if (bFixedType && VarType != VT_Float)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a float type."), VariableName);
		return;
	}

	VarType = VT_Float;
	Data.EmptyBuffer();
	Data << value;
}

void ScriptVariable::WriteDString (const DString& string)
{
	if (bFixedType && VarType != VT_String)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a string type."), VariableName);
		return;
	}

	VarType = VT_String;
	Data.EmptyBuffer();
	Data << string;
}

void ScriptVariable::WriteFunction (ContentObject* functionOwner, const Subroutine* function)
{
	if (bFixedType && VarType != VT_Function)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a function."), VariableName);
		return;
	}

	VarType = VT_Function;
	Data.EmptyBuffer();

	if (function != nullptr)
	{
		if (functionOwner != nullptr)
		{
			Data << functionOwner->GetId();
		}
		else
		{
			size_t invalidObj = 0;
			Data << invalidObj;
		}

		Data << function->ReadFunctionName().GetHash();
	}
}

void ScriptVariable::WriteObjPointer (ContentObject* pointAt)
{
	if (bFixedType && VarType != VT_Obj)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from an object pointer."), VariableName);
		return;
	}

	VarType = VT_Obj;
	Data.EmptyBuffer();

	if (pointAt != nullptr)
	{
		Data << pointAt->GetId();
	}
}

void ScriptVariable::WriteClass (const ContentClass* contentClass)
{
	if (bFixedType && VarType != VT_Class)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a class."), VariableName);
		return;
	}

	VarType = VT_Class;
	Data.EmptyBuffer();

	if (contentClass != nullptr)
	{
		Data << contentClass->ReadClassName().GetHash();
	}
}

void ScriptVariable::WritePointer (size_t stackIdx)
{
	if (bFixedType && VarType != VT_Pointer)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a pointer type."), VariableName);
		return;
	}

	VarType = VT_Pointer;
	Data.EmptyBuffer();

	if (stackIdx != INDEX_NONE)
	{
		char header = PF_Stack;
		Data.AppendBytes(&header, 1);
		Data << stackIdx;
	}
}

void ScriptVariable::WritePointer (ContentObject* owningObj, size_t memVarIdx)
{
	if (bFixedType && VarType != VT_Pointer)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a pointer type."), VariableName);
		return;
	}

	VarType = VT_Pointer;
	Data.EmptyBuffer();

	if (owningObj != nullptr && memVarIdx != INDEX_NONE)
	{
		char header = PF_ObjMemVar;
		Data.AppendBytes(&header, 1);

		Data << owningObj->GetId();
		Data << memVarIdx;
	}
}

void ScriptVariable::WriteScopePointer (ScriptScope* scope, size_t varIdx)
{
	if (bFixedType && VarType != VT_Pointer)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to change the fixed ScriptVariable %s to a type different from a pointer type."), VariableName);
		return;
	}

	VarType = VT_Pointer;
	Data.EmptyBuffer();

	if (scope != nullptr && varIdx != INDEX_NONE)
	{
		char header = PF_ScopeGlobal;
		Data.AppendBytes(&header, 1);
		Data << varIdx;
	}
}

Bool ScriptVariable::ToBool () const
{
	Bool result = false;
	if (VarType != VT_Bool)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a Bool ScriptVariable %s since it's not a bool type."), VariableName);
		return result;
	}

	if (Data.IsEmpty())
	{
		return result;
	}

	Data.JumpToBeginning();
	Data >> result;
	return result;
}

Int ScriptVariable::ToInt () const
{
	Int result = 0;
	if (VarType != VT_Int)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read an Int ScriptVariable %s since it's not an int type."), VariableName);
		return result;
	}

	if (Data.IsEmpty())
	{
		return result;
	}

	Data.JumpToBeginning();
	Data >> result;
	return result;
}

Float ScriptVariable::ToFloat () const
{
	Float result = 0.f;
	if (VarType != VT_Float)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a Float ScriptVariable %s since it's not a float type."), VariableName);
		return result;
	}

	if (Data.IsEmpty())
	{
		return result;
	}

	Data.JumpToBeginning();
	Data >> result;
	return result;
}

DString ScriptVariable::ToDString () const
{
	DString result = DString::EmptyString;
	if (VarType != VT_String)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a DString ScriptVariable %s since it's not a string type."), VariableName);
		return result;
	}

	if (Data.IsEmpty())
	{
		return result;
	}

	Data.JumpToBeginning();
	Data >> result;
	return result;
}

ScriptVariable::SScriptDelegate ScriptVariable::ToFunction (ScriptScope* context) const
{
	SScriptDelegate result;
	if (VarType != VT_Function)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a function delegate ScriptVariable %s since it's not a function type."), VariableName);
		return result;
	}

	if (Data.IsEmpty() || context == nullptr)
	{
		return result;
	}

	Data.JumpToBeginning();
	size_t objId;
	size_t functionHash;
	Data >> objId >> functionHash;
	
	if (Data.HasReadError())
	{
		return result;
	}

	if (objId != 0)
	{
		//Find the object
		if (!context->ReadHeap().contains(objId))
		{
			return result;
		}

		ContentObject* obj = context->ReadHeap().at(objId);
		CHECK(obj != nullptr && obj->GetClass() != nullptr)
		const ContentClass* objClass = obj->GetClass();
		const Subroutine* func = objClass->FindFunction(context, functionHash, obj);

		result.FunctionOwner = obj;
		result.Function = func;
	}
	else if (context->ReadStaticFunctions().contains(functionHash))
	{
		//Function must be a static function residing on the scope.
		result.Function = context->ReadStaticFunctions().at(functionHash);
	}

	return result;
}

ContentObject* ScriptVariable::ToObjPointer (ScriptScope* context) const
{
	ContentObject* result = nullptr;
	if (VarType != VT_Obj)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read an object pointer ScriptVariable %s since it's not an object pointer type."), VariableName);
		return result;
	}

	if (Data.IsEmpty() || context == nullptr)
	{
		return result;
	}

	Data.JumpToBeginning();
	size_t id;
	if (!(Data >> id).HasReadError())
	{
		if (context->ReadHeap().contains(id))
		{
			result = context->ReadHeap().at(id);
		}
	}

	return result;
}

const ContentClass* ScriptVariable::ToClass () const
{
	const ContentClass* result = nullptr;
	if (VarType != VT_Class)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a class from ScriptVariable %s since it's not a class type."), VariableName);
		return result;
	}

	if (Data.IsEmpty())
	{
		return result;
	}

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)

	Data.JumpToBeginning();
	size_t classHash;
	if (!(Data >> classHash).HasReadError())
	{
		if (clmEngine->ReadScriptClasses().contains(classHash))
		{
			result = clmEngine->ReadScriptClasses().at(classHash);
		}
	}

	return result;
}

ScriptVariableRef ScriptVariable::GetPointer (ScriptScope* scope, ClmStack& stack) const
{
	if (scope == nullptr)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot obtain what %s is pointing at without specifying the Scope context it resides in."), VariableName);
		return ScriptVariableRef(nullptr, INDEX_NONE);
	}

	if (VarType != VT_Pointer)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read a pointer ScriptVariable %s since it's not a pointer type."), VariableName);
		return ScriptVariableRef(nullptr, INDEX_NONE);
	}

	if (Data.IsEmpty() || !IsValid())
	{
		return ScriptVariableRef(nullptr, INDEX_NONE);
	}

	Data.JumpToBeginning();
	char header;
	Data.ReadBytes(&header, 1);

	switch(header)
	{
		case(PF_Stack):
		{
			//Handle simple case. Pointing at a variable on the stack.
			size_t stackIdx;
			if ((Data >> stackIdx).HasReadError())
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read stack offset data from %s."), VariableName);
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			if (stackIdx == INDEX_NONE || stackIdx >= stack.GetStackSize())
			{
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			return ScriptVariableRef(&stack.EditStack(), stackIdx);
		}

		case(PF_ObjMemVar):
		{
			size_t heapId;
			if ((Data >> heapId).HasReadError())
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read Heap ID data from %s."), VariableName);
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			if (!scope->ReadHeap().contains(heapId))
			{
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			ContentObject* obj = scope->ReadHeap().at(heapId);
			if (obj == nullptr)
			{
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			size_t memVarIdx;
			if ((Data >> memVarIdx).HasReadError())
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read member variable offset data from %s."), VariableName);
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			if (memVarIdx == INDEX_NONE || memVarIdx >= obj->ReadMemberVariables().size())
			{
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			return ScriptVariableRef(&obj->EditMemberVariables(), memVarIdx);
		}

		case(PF_ScopeGlobal):
		{
			size_t varIdx;
			if ((Data >> varIdx).HasReadError())
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read scope variable index data from %s."), VariableName);
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			if (varIdx == INDEX_NONE)
			{
				return ScriptVariableRef(nullptr, INDEX_NONE);
			}

			return ScriptVariableRef(&scope->EditGlobalVariables(), varIdx);
		}
	}

	ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot read pointer reference from %s. Header type is unknown %s."), VariableName, Int(header));
	return ScriptVariableRef(nullptr, INDEX_NONE);
}

ScriptVariableRef ScriptVariable::GetPointerRecursive (ScriptScope* scope, ClmStack& stack) const
{
	ScriptVariableRef result(nullptr, INDEX_NONE);
	ScriptVariable* curVar = const_cast<ScriptVariable*>(this);
	while (curVar != nullptr)
	{
		if (curVar->VarType != VT_Pointer)
		{
			break;
		}

		result = curVar->GetPointer(scope, stack);
		if (!result.IsValid())
		{
			//It's pointing to nullptr
			break;
		}

		curVar = &result.EditVar();
	}

	return result;
}

DString ScriptVariable::InterpretValueAsString (ScriptScope* scope, ClmStack& stack) const
{
	switch(VarType)
	{
		case(VT_Unknown):
			return TXT("Undefined Variable");

		case(VT_Bool):
			return ToBool().ToString();

		case(VT_Int):
			return ToInt().ToString();

		case(VT_Float):
			return ToFloat().ToString();

		case(VT_String):
			return ToDString();

		case(VT_Function):
		{
			SScriptDelegate delegateData = ToFunction(scope);
			if (delegateData.Function == nullptr)
			{
				return TXT("nullptr");
			}

			DString prefix;
			if (delegateData.FunctionOwner != nullptr)
			{
				prefix = delegateData.FunctionOwner->ToString() + TXT("::");
			}

			return prefix + delegateData.Function->ReadFunctionName().ToString();
		}

		case(VT_Obj):
		{
			if (ContentObject* obj = ToObjPointer(scope))
			{
				return obj->ToString();
			}

			return TXT("nullptr");
		}

		case(VT_Class):
		{
			if (const ContentClass* classInstance = ToClass())
			{
				return classInstance->ReadClassName().ToString();
			}

			return TXT("nullptr");
		}

		case(VT_Pointer):
		{
			ScriptVariableRef var = GetPointer(scope, stack);
			if (var.IsValid())
			{
				return TXT("PointAt_") + var.EditVar().InterpretValueAsString(scope, stack);
			}

			return TXT("nullptr");
		}
	}

	return TXT("Undefined Variable");
}

void ScriptVariable::SetIsValid (bool newIsValid)
{
	bIsValid = newIsValid;
}

void ScriptVariable::SetVariableName (const HashedString& newVariableName)
{
	VariableName = newVariableName;
}
SD_END