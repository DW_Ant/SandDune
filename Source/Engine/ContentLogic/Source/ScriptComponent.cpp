/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptComponent.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptComponent.h"
#include "Subroutine.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"

IMPLEMENT_CLASS(SD::ScriptComponent, SD::EntityComponent)
SD_BEGIN

void ScriptComponent::InitProps ()
{
	Super::InitProps();

	ScriptClass = nullptr;
}

void ScriptComponent::BeginObject ()
{
	Super::BeginObject();

	if (ScriptClassName.GetHash() != 0) //If a subclass specifies a ScriptClassName
	{
		ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
		CHECK(clmEngine != nullptr)
		if (!clmEngine->ReadScriptClasses().contains(ScriptClassName.GetHash()))
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to setup script object for %s since the class name %s is not found."), ToString(), ScriptClassName);
		}
		else
		{
			const ContentClass* scriptClass = clmEngine->ReadScriptClasses().at(ScriptClassName.GetHash());
			CHECK(scriptClass != nullptr)
			SetScriptClass(scriptClass);
		}
	}
}

void ScriptComponent::Destroyed ()
{
	if (ScriptObj != nullptr)
	{
		ScriptObj->Destroy();
	}

	Super::Destroyed();
}

bool ScriptComponent::RegisterCallableNativeFunction (const HashedString& functionName, const std::function<void(ClmStack&)>& lambda)
{
	if (CallableNativeFunctions.contains(functionName))
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to register CallableNativeFunction in %s since the function name %s is already registered."), ToString(), functionName);
		return false;
	}

	CallableNativeFunctions.insert({functionName, lambda});
	return true;
}

void ScriptComponent::SetScope (ScriptScope* newScope)
{
	if (ScriptObj != nullptr)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot set a ScriptComponent's scope after it established an object. This is because a ContentObject cannot change scopes."));
		return;
	}

	Scope = newScope;
	if (ScriptClass != nullptr && Scope != nullptr && !ScriptClass->IsAbstract())
	{
		CreateScriptObj();
	}
}

void ScriptComponent::SetScriptClass (const ContentClass* newScriptClass)
{
	if (ScriptObj != nullptr)
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot set a ScriptComponent's class after it established an object since a ContentObject cannot change classes."));
		return;
	}

	ScriptClass = newScriptClass;
	if (ScriptClass != nullptr && Scope != nullptr && !ScriptClass->IsAbstract())
	{
		CreateScriptObj();
	}
}

void ScriptComponent::CreateScriptObj ()
{
	CHECK(ScriptClass != nullptr && Scope != nullptr && !ScriptClass->IsAbstract())

	ScriptObj = ContentObject::CreateObject();
	ScriptObj->BeginContentObject(ScriptClass, Scope.Get(), this, nullptr);
}

void ScriptComponent::PopulateOutParams (ClmStack& outStack, size_t numParams, std::vector<ScriptVariable>& outParams) const
{
	if (outParams.size() + numParams + 1 > outStack.GetStackSize())
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Unable to populate out params for %s since the stack contains %s elements when it's expected to contain %s elements in it. Number of params = %s, num output params = %s"), ToString(), Int(outStack.GetStackSize()), Int(numParams + outParams.size()), Int(numParams), Int(outParams.size()));
		return;
	}

	for (size_t i = 0; i < outParams.size(); ++i)
	{
		//NOTE: The top of the stack contains a reference to the object being executed. Everything is displaced by 1 index.
		//Also since the outParams are index-based, we would normally subtract .size() by 1. The +1 and -1 cancels out.
		size_t stackOffset = numParams + (outParams.size() - i);

		//Copy the contents from stack to outParams
		outParams.at(i) = outStack.EditVariable(stackOffset).EditVar();
	}
}
SD_END