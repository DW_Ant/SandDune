/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScriptVariableRef.cpp
=====================================================================
*/

#include "ScriptVariableRef.h"

SD_BEGIN
ScriptVariableRef::ScriptVariableRef (std::vector<ScriptVariable>* inContainer, size_t inVarIdx) :
	Container(inContainer),
	VarIdx(inVarIdx)
{
	//Noop
}

ScriptVariableRef::ScriptVariableRef (const ScriptVariableRef& cpyObj) :
	Container(cpyObj.Container),
	VarIdx(cpyObj.VarIdx)
{
	//Noop
}

bool ScriptVariableRef::operator== (const ScriptVariableRef& other) const
{
	return (Container == other.Container && VarIdx == other.VarIdx);
}

bool ScriptVariableRef::operator!= (const ScriptVariableRef& other) const
{
	return !(*this == other);
}

const ScriptVariable& ScriptVariableRef::ReadVar () const
{
#ifdef DEBUG_MODE
	CHECK(IsValid())
#endif

	return Container->at(VarIdx);
}

ScriptVariable& ScriptVariableRef::EditVar ()
{
#ifdef DEBUG_MODE
	CHECK(IsValid())
#endif

	return Container->at(VarIdx);
}

bool ScriptVariableRef::IsValid () const
{
	return (Container != nullptr && VarIdx < Container->size() && Container->at(VarIdx).IsValid());
}
SD_END