/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClmEngineComponent.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "CommandAssignVar.h"
#include "CommandBeginSection.h"
#include "CommandElseClause.h"
#include "CommandEndSection.h"
#include "CommandIfCondition.h"
#include "CommandInvokeSubroutine.h"
#include "CommandNative.h"
#include "CommandPopVars.h"
#include "CommandPushVar.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "Subroutine.h"

IMPLEMENT_ENGINE_COMPONENT(SD::ClmEngineComponent)
SD_BEGIN

const std::vector<DString> ClmEngineComponent::RESERVED_WORDS
{
	TXT("true"),
	TXT("false"),
	TXT("if"),
	TXT("else"),
	TXT("Bool"),
	TXT("Int"),
	TXT("Float"),
	TXT("DString")
};

void ClmEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	InitializeScriptClasses();
	CreateComparisonFunctions();
	CreateBoolOperations();
	CreateConversionFunctions();
	CreateMathFunctions();
	CreateObjHandlingFunctions();
}

void ClmEngineComponent::ShutdownComponent ()
{
	for (size_t i = 0; i < OrphanedClasses.size(); ++i)
	{
		delete OrphanedClasses.at(i);
	}
	ContainerUtils::Empty(OUT OrphanedClasses);

	for (std::pair<size_t, ContentClass*> contentClass : ScriptClasses)
	{
		delete contentClass.second;
	}
	ScriptClasses.clear();

	//Delete global functions
	std::vector<Subroutine*> pendingDelete;
	for (auto& globalFunc : GlobalFunctions)
	{
		pendingDelete.push_back(globalFunc.second);
	}
	GlobalFunctions.clear();

	for (Subroutine* globalFunc : pendingDelete)
	{
		delete globalFunc;
	}

	Super::ShutdownComponent();
}

ContentClass* ClmEngineComponent::CreateScriptClass (const HashedString& className, const HashedString& parentClass)
{
	if (ScriptClasses.contains(className.GetHash()))
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot create %s since that class already exists."), className);
		return nullptr;
	}

	for (ContentClass* otherClass : OrphanedClasses)
	{
		if (otherClass->ReadClassName() == className)
		{
			ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot create %s since that class already exists."), className);
			return nullptr;
		}
	}

	ContentClass* newClass = new ContentClass(className, parentClass);

	//Check if there are any orphans expecting this class.
	size_t i = 0;
	while (i < OrphanedClasses.size())
	{
		if (OrphanedClasses.at(i)->ReadParentClassName() == className)
		{
			ContentClass* movedClass = OrphanedClasses.at(i);
			movedClass->SetParentClass(newClass);
			ScriptClasses.insert({movedClass->ReadClassName().GetHash(), movedClass});
			OrphanedClasses.erase(OrphanedClasses.begin() + i);
			continue;
		}

		--i;
	}

	if (parentClass.GetHash() != 0)
	{
		//Check if the parent already exists
		if (!ScriptClasses.contains(parentClass.GetHash()))
		{
			OrphanedClasses.push_back(newClass); //Parent hasn't loaded yet
			return newClass;
		}

		newClass->SetParentClass(ScriptClasses.at(parentClass.GetHash()));
	}

	ScriptClasses.insert({className.GetHash(), newClass});
	return newClass;
}

void ClmEngineComponent::DestroyScriptClass (ContentClass* classToDestroy, bool bRecursiveDestroy)
{
	CHECK(classToDestroy != nullptr)
	if (!ScriptClasses.contains(classToDestroy->ReadClassName().GetHash()))
	{
		//Check orphans
		bool bFoundClass = false;
		for (size_t i = 0; i < OrphanedClasses.size(); ++i)
		{
			if (OrphanedClasses.at(i) == classToDestroy)
			{
				bFoundClass = true;
				OrphanedClasses.erase(OrphanedClasses.begin() + i);
				break;
			}
		}

		if (!bFoundClass)
		{
			//Nothing to destroy
			return;
		}
	}
	else
	{
		ScriptClasses.erase(classToDestroy->ReadClassName().GetHash());
	}

	if (!bRecursiveDestroy)
	{
		//Clear reference to ParentClass
		for (std::pair<size_t, ContentClass*> scriptClass : ScriptClasses)
		{
			if (scriptClass.second->GetParentClass() == classToDestroy)
			{
				scriptClass.second->SetParentClass(nullptr);
			}
		}
	}
	else //Delete all classes that inherit from this class including nested children.
	{
		std::vector<size_t> pendingRemoveKeys;
		for (auto it = ScriptClasses.begin(); it != ScriptClasses.end(); ++it)
		{
			if (it->second->IsA(classToDestroy) && it->second != classToDestroy)
			{
				pendingRemoveKeys.push_back(it->first);
			}
		}
		
		for (size_t i = 0; i < pendingRemoveKeys.size(); ++i)
		{
			delete ScriptClasses.at(pendingRemoveKeys.at(i));
			ScriptClasses.erase(pendingRemoveKeys.at(i));
		}
	}

	delete classToDestroy;
}

Subroutine& ClmEngineComponent::CreateGlobalFunction (const HashedString& functionName, bool bNativeFunction)
{
	if (GlobalFunctions.contains(functionName.GetHash()))
	{
		ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot create a global function instance since the function name %s already exists."), functionName);
		return *GlobalFunctions.at(functionName.GetHash());
	}

	Subroutine::EFunctionFlags flags = Subroutine::FF_Static | Subroutine::FF_Global | Subroutine::FF_Const;
	if (bNativeFunction)
	{
		flags |= Subroutine::FF_Native;
	}

	GlobalFunctions.insert({functionName.GetHash(), new Subroutine(functionName, flags, nullptr)});
	return *GlobalFunctions.at(functionName.GetHash());
}

void ClmEngineComponent::InitializeScriptClasses ()
{
#ifdef DEBUG_MODE
	//Typically script classes are auto generated based on the package. This is written manually for testing purposes.
	{
		ContentClass* unitTestClass = CreateScriptClass(HashedString("UnitTestScriptComp"), HashedString());
		CHECK(unitTestClass)

		unitTestClass->SetHideFromDropdown(true); //Prevent editors from viewing this class.

		ScriptVariable& intVar = unitTestClass->CreateMemberVariable(HashedString("IntVar"));
		intVar.SetFixedType(ScriptVariable::VT_Int);
		intVar.WriteInt(10);

		ScriptVariable& floatVar = unitTestClass->CreateMemberVariable(HashedString("FloatVar"));
		floatVar.SetFixedType(ScriptVariable::VT_Float);
		floatVar.WriteFloat(-100.f);

		ScriptVariable& stringVar = unitTestClass->CreateMemberVariable(HashedString("StringVar"));
		stringVar.SetFixedType(ScriptVariable::VT_String);
		stringVar.WriteDString(TXT("DefaultText"));

		Subroutine& scriptEvent = unitTestClass->CreateMemberFunction(HashedString("ProcessScriptFunction"), Subroutine::FF_Event | Subroutine::FF_Const);
		{
			ScriptVariable& outInt = scriptEvent.AddOutput(HashedString("outInt"));
			outInt.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& outFloat = scriptEvent.AddOutput(HashedString("outFloat"));
			outFloat.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& outString = scriptEvent.AddOutput(HashedString("outString"));
			outString.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& inInt = scriptEvent.AddParameter(HashedString("inInt"));
			inInt.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& inFloat = scriptEvent.AddParameter(HashedString("inFloat"));
			inFloat.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& inString = scriptEvent.AddParameter(HashedString("inString"));
			inString.SetFixedType(ScriptVariable::VT_String);
			
			//Stack: outInt, outFloat, outString, inInt, inFloat, inString, this, Pointer[0]->IntVar, Pointer[1]->FloatVar, Pointer[2]->StringVar
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));

			//Have local variables point at member variables.
			scriptEvent.AddCommand(new CommandAssignRef(2, 3, 0));
			scriptEvent.AddCommand(new CommandAssignRef(1, 3, 1));
			scriptEvent.AddCommand(new CommandAssignRef(0, 3, 2));

			//Copy the parameters to the member variables.
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 6, 2));
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 5, 1));
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 4, 0));

			//Call the exposed functions on the object.
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Obj));
			scriptEvent.AddCommand(new CommandAssignRef(2, 12)); //Point at outInt
			scriptEvent.AddCommand(new CommandAssignRef(1, 9)); //Point at inInt
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 6, 0)); //Copy this to the end of the stack
			scriptEvent.AddCommand(new CommandNative(HashedString("CallableConvertInt")));
			scriptEvent.AddCommand(new CommandPopVars(3));

			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Obj));
			scriptEvent.AddCommand(new CommandAssignRef(2, 11)); //Point at outFloat
			scriptEvent.AddCommand(new CommandAssignRef(1, 8)); //Point at inFloat
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 6, 0)); //Copy this to the end of the stack
			scriptEvent.AddCommand(new CommandNative(HashedString("CallableConvertFloat")));
			scriptEvent.AddCommand(new CommandPopVars(3));

			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			scriptEvent.AddCommand(new CommandPushVar(ScriptVariable::VT_Obj));
			scriptEvent.AddCommand(new CommandAssignRef(2, 10)); //Point at outString
			scriptEvent.AddCommand(new CommandAssignRef(1, 7)); //Point at inString
			scriptEvent.AddCommand(new CommandAssignVar(true, true, 6, 0)); //Copy this to the end of the stack
			scriptEvent.AddCommand(new CommandNative(HashedString("CallableConvertString")));
			scriptEvent.AddCommand(new CommandPopVars(3));

			//Remove the local pointers
			scriptEvent.AddCommand(new CommandPopVars(3));
		}
	}
#endif
}

void ClmEngineComponent::CreateComparisonFunctions ()
{
	//Create == operations
	{
		Subroutine& equalsBool = CreateGlobalFunction(HashedString("Bool==Bool"), true);
		{
			ScriptVariable& result = equalsBool.AddOutput(HashedString("isEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = equalsBool.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param2 = equalsBool.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Bool);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Bool a = outStack.GetBoolParam(scope, 0, 2, false);
				Bool b = outStack.GetBoolParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a == b), scope, 0, 1, 2, false);
			});
			equalsBool.AddCommand(native);
		}

		Subroutine& equalsInt = CreateGlobalFunction(HashedString("Int==Int"), true);
		{
			ScriptVariable& result = equalsInt.AddOutput(HashedString("isEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = equalsInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = equalsInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a == b), scope, 0, 1, 2, false);
			});
			equalsInt.AddCommand(native);
		}

		Subroutine& equalsFloat = CreateGlobalFunction(HashedString("Float==Float"), true);
		{
			ScriptVariable& result = equalsFloat.AddOutput(HashedString("isEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = equalsFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = equalsFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput(a.IsCloseTo(b), scope, 0, 1, 2, false);
			});
			equalsFloat.AddCommand(native);
		}

		Subroutine& equalsString = CreateGlobalFunction(HashedString("String==String"), true);
		{
			ScriptVariable& result = equalsString.AddOutput(HashedString("isEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = equalsString.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& param2 = equalsString.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& isSensitive = equalsString.AddParameter(HashedString("isSensitive"));
			isSensitive.SetFixedType(ScriptVariable::VT_Bool);
			isSensitive.WriteBool(false);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString a = outStack.GetStringParam(scope, 0, 3, false);
				DString b = outStack.GetStringParam(scope, 1, 3, false);
				Bool isSensitive = outStack.GetBoolParam(scope, 2, 3, false);
				DString::ECaseComparison caseComparison = (isSensitive) ? DString::CC_CaseSensitive : DString::CC_IgnoreCase;
				outStack.WriteBoolOutput((a.Compare(b, caseComparison) == 0), scope, 0, 1, 3, false);
			});
			equalsString.AddCommand(native);
		}

		Subroutine& equalsObj = CreateGlobalFunction(HashedString("Object==Object"), true);
		{
			ScriptVariable& result = equalsObj.AddOutput(HashedString("isEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = equalsObj.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Obj);

			ScriptVariable& param2 = equalsObj.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Obj);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				ContentObject* a = outStack.GetObjParam(scope, 0, 2, false);
				ContentObject* b = outStack.GetObjParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a == b), scope, 0, 1, 2, false);
			});
			equalsObj.AddCommand(native);
		}
	}

	//Create != operations
	{
		Subroutine& notEqualsBool = CreateGlobalFunction(HashedString("Bool!=Bool"), true);
		{
			ScriptVariable& result = notEqualsBool.AddOutput(HashedString("isNotEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = notEqualsBool.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param2 = notEqualsBool.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Bool);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Bool a = outStack.GetBoolParam(scope, 0, 2, false);
				Bool b = outStack.GetBoolParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a != b), scope, 0, 1, 2, false);
			});
			notEqualsBool.AddCommand(native);
		}

		Subroutine& notEqualsInt = CreateGlobalFunction(HashedString("Int!=Int"), true);
		{
			ScriptVariable& result = notEqualsInt.AddOutput(HashedString("isNotEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = notEqualsInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = notEqualsInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a != b), scope, 0, 1, 2, false);
			});
			notEqualsInt.AddCommand(native);
		}

		Subroutine& notEqualsFloat = CreateGlobalFunction(HashedString("Float!=Float"), true);
		{
			ScriptVariable& result = notEqualsFloat.AddOutput(HashedString("isNotEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = notEqualsFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = notEqualsFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput(!a.IsCloseTo(b), scope, 0, 1, 2, false);
			});
			notEqualsFloat.AddCommand(native);
		}

		Subroutine& notEqualsString = CreateGlobalFunction(HashedString("String!=String"), true);
		{
			ScriptVariable& result = notEqualsString.AddOutput(HashedString("isNotEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = notEqualsString.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& param2 = notEqualsString.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& isSensitive = notEqualsString.AddParameter(HashedString("isSensitive"));
			isSensitive.SetFixedType(ScriptVariable::VT_Bool);
			isSensitive.WriteBool(false);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString a = outStack.GetStringParam(scope, 0, 3, false);
				DString b = outStack.GetStringParam(scope, 1, 3, false);
				Bool isSensitive = outStack.GetBoolParam(scope, 2, 3, false);
				DString::ECaseComparison caseComparison = (isSensitive) ? DString::CC_CaseSensitive : DString::CC_IgnoreCase;
				outStack.WriteBoolOutput((a.Compare(b, caseComparison) != 0), scope, 0, 1, 3, false);
			});
			notEqualsString.AddCommand(native);
		}

		Subroutine& notEqualsObj = CreateGlobalFunction(HashedString("Object!=Object"), true);
		{
			ScriptVariable& result = notEqualsObj.AddOutput(HashedString("isNotEqual"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = notEqualsObj.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Obj);

			ScriptVariable& param2 = notEqualsObj.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Obj);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				ContentObject* a = outStack.GetObjParam(scope, 0, 2, false);
				ContentObject* b = outStack.GetObjParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a != b), scope, 0, 1, 2, false);
			});
			notEqualsObj.AddCommand(native);
		}
	}

	//Create > operations
	{
		Subroutine& greaterInt = CreateGlobalFunction(HashedString("Int>Int"), true);
		{
			ScriptVariable& result = greaterInt.AddOutput(HashedString("isGreaterThan"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = greaterInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = greaterInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a > b), scope, 0, 1, 2, false);
			});
			greaterInt.AddCommand(native);
		}

		Subroutine& greaterFloat = CreateGlobalFunction(HashedString("Float>Float"), true);
		{
			ScriptVariable& result = greaterFloat.AddOutput(HashedString("isGreaterThan"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = greaterFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = greaterFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a > b), scope, 0, 1, 2, false);
			});
			greaterFloat.AddCommand(native);
		}
	}

	//Create >= operations
	{
		Subroutine& greaterInt = CreateGlobalFunction(HashedString("Int>=Int"), true);
		{
			ScriptVariable& result = greaterInt.AddOutput(HashedString("isGreaterThanOrEqualTo"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = greaterInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = greaterInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a >= b), scope, 0, 1, 2, false);
			});
			greaterInt.AddCommand(native);
		}

		Subroutine& greaterFloat = CreateGlobalFunction(HashedString("Float>=Float"), true);
		{
			ScriptVariable& result = greaterFloat.AddOutput(HashedString("isGreaterThanOrEqualTo"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = greaterFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = greaterFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a >= b), scope, 0, 1, 2, false);
			});
			greaterFloat.AddCommand(native);
		}
	}

	//Create < operations
	{
		Subroutine& lesserInt = CreateGlobalFunction(HashedString("Int<Int"), true);
		{
			ScriptVariable& result = lesserInt.AddOutput(HashedString("isLessThan"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = lesserInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = lesserInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a < b), scope, 0, 1, 2, false);
			});
			lesserInt.AddCommand(native);
		}

		Subroutine& lesserFloat = CreateGlobalFunction(HashedString("Float<Float"), true);
		{
			ScriptVariable& result = lesserFloat.AddOutput(HashedString("isLessThan"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = lesserFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = lesserFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a < b), scope, 0, 1, 2, false);
			});
			lesserFloat.AddCommand(native);
		}
	}

	//Create <= operations
	{
		Subroutine& lesserInt = CreateGlobalFunction(HashedString("Int<=Int"), true);
		{
			ScriptVariable& result = lesserInt.AddOutput(HashedString("isLessThanOrEqualTo"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = lesserInt.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = lesserInt.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a <= b), scope, 0, 1, 2, false);
			});
			lesserInt.AddCommand(native);
		}

		Subroutine& lesserFloat = CreateGlobalFunction(HashedString("Float<=Float"), true);
		{
			ScriptVariable& result = lesserFloat.AddOutput(HashedString("isLessThanOrEqualTo"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = lesserFloat.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param2 = lesserFloat.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteBoolOutput((a <= b), scope, 0, 1, 2, false);
			});
			lesserFloat.AddCommand(native);
		}
	}

	//Create pointer operations
	{
		Subroutine& isValid = CreateGlobalFunction(HashedString("IsObjectValid"), true);
		{
			ScriptVariable& result = isValid.AddOutput(HashedString("isValid"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = isValid.AddParameter(HashedString("pointer"));
			param1.SetFixedType(ScriptVariable::VT_Obj);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				ContentObject* obj = outStack.GetObjParam(scope, 0, 1, false);
				outStack.WriteBoolOutput((obj != nullptr), scope, 0, 1, 1, false);
			});
			isValid.AddCommand(native);
		}

		Subroutine& isNull = CreateGlobalFunction(HashedString("IsObjectNull"), true);
		{
			ScriptVariable& result = isNull.AddOutput(HashedString("isNull"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = isNull.AddParameter(HashedString("pointer"));
			param1.SetFixedType(ScriptVariable::VT_Obj);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				ContentObject* obj = outStack.GetObjParam(scope, 0, 1, false);
				outStack.WriteBoolOutput((obj == nullptr), scope, 0, 1, 1, false);
			});
			isNull.AddCommand(native);
		}
	}
}

void ClmEngineComponent::CreateBoolOperations ()
{
	Subroutine& andOperation = CreateGlobalFunction(HashedString("&&"), true);
	{
		ScriptVariable& result = andOperation.AddOutput(HashedString("result"));
		result.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param1 = andOperation.AddParameter(HashedString("a"));
		param1.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param2 = andOperation.AddParameter(HashedString("b"));
		param2.SetFixedType(ScriptVariable::VT_Bool);

		CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			Bool a = outStack.GetBoolParam(scope, 0, 2, false);
			Bool b = outStack.GetBoolParam(scope, 1, 2, false);
			outStack.WriteBoolOutput((a && b), scope, 0, 1, 2, false);
		});
		andOperation.AddCommand(native);
	}

	Subroutine& orOperation = CreateGlobalFunction(HashedString("||"), true);
	{
		ScriptVariable& result = orOperation.AddOutput(HashedString("result"));
		result.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param1 = orOperation.AddParameter(HashedString("a"));
		param1.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param2 = orOperation.AddParameter(HashedString("b"));
		param2.SetFixedType(ScriptVariable::VT_Bool);

		CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			Bool a = outStack.GetBoolParam(scope, 0, 2, false);
			Bool b = outStack.GetBoolParam(scope, 1, 2, false);
			outStack.WriteBoolOutput((a || b), scope, 0, 1, 2, false);
		});
		orOperation.AddCommand(native);
	}

	Subroutine& xorOperation = CreateGlobalFunction(HashedString("XOR"), true);
	{
		ScriptVariable& result = xorOperation .AddOutput(HashedString("result"));
		result.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param1 = xorOperation .AddParameter(HashedString("a"));
		param1.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param2 = xorOperation .AddParameter(HashedString("b"));
		param2.SetFixedType(ScriptVariable::VT_Bool);

		CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			Bool a = outStack.GetBoolParam(scope, 0, 2, false);
			Bool b = outStack.GetBoolParam(scope, 1, 2, false);
			outStack.WriteBoolOutput((a ^ b), scope, 0, 1, 2, false);
		});
		xorOperation.AddCommand(native);
	}

	Subroutine& notOperation = CreateGlobalFunction(HashedString("NOT"), true);
	{
		ScriptVariable& result = notOperation .AddOutput(HashedString("result"));
		result.SetFixedType(ScriptVariable::VT_Bool);

		ScriptVariable& param1 = notOperation .AddParameter(HashedString("a"));
		param1.SetFixedType(ScriptVariable::VT_Bool);

		CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			Bool a = outStack.GetBoolParam(scope, 0, 1, false);
			outStack.WriteBoolOutput(!a, scope, 0, 1, 1, false);
		});
		notOperation.AddCommand(native);
	}
}

void ClmEngineComponent::CreateConversionFunctions ()
{
	//Create functions that converts bools
	//Note: It's important to keep the naming convension strictly "TypeToType" since the Editor's VariablePort uses that to find the right conversion method.
	//If there's a need to change these function names, be sure to update VariablePort.
	{
		Subroutine& boolToInt = CreateGlobalFunction(HashedString("BoolToInt"), false);
		{
			ScriptVariable& out = boolToInt.AddOutput(HashedString("int"));
			out.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param1 = boolToInt.AddParameter(HashedString("bool"));
			param1.SetFixedType(ScriptVariable::VT_Bool);

			CommandIfCondition* ifStep = new CommandIfCondition(0);
			boolToInt.AddCommand(ifStep);
			{
				CommandBeginSection* begin = new CommandBeginSection();
				boolToInt.AddCommand(begin);

				CommandAssignInt* assignment = new CommandAssignInt(1, 1);
				boolToInt.AddCommand(assignment);

				CommandEndSection* end = new CommandEndSection();
				boolToInt.AddCommand(end);
			}
			CommandElseClause* elseStep = new CommandElseClause();
			boolToInt.AddCommand(elseStep);
			{
				CommandBeginSection* begin = new CommandBeginSection();
				boolToInt.AddCommand(begin);

				CommandAssignInt* assignment = new CommandAssignInt(1, 0);
				boolToInt.AddCommand(assignment);

				CommandEndSection* end = new CommandEndSection();
				boolToInt.AddCommand(end);
			}
		}

		Subroutine& boolToFloat = CreateGlobalFunction(HashedString("BoolToFloat"), false);
		{
			ScriptVariable& out = boolToFloat.AddOutput(HashedString("float"));
			out.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param1 = boolToFloat.AddParameter(HashedString("bool"));
			param1.SetFixedType(ScriptVariable::VT_Bool);

			CommandIfCondition* ifStep = new CommandIfCondition(0);
			boolToFloat.AddCommand(ifStep);
			{
				CommandBeginSection* begin = new CommandBeginSection();
				boolToFloat.AddCommand(begin);

				CommandAssignFloat* assignment = new CommandAssignFloat(1, 1.f);
				boolToFloat.AddCommand(assignment);

				CommandEndSection* end = new CommandEndSection();
				boolToFloat.AddCommand(end);
			}
			CommandElseClause* elseStep = new CommandElseClause();
			boolToFloat.AddCommand(elseStep);
			{
				CommandBeginSection* begin = new CommandBeginSection();
				boolToFloat.AddCommand(begin);

				CommandAssignFloat* assignment = new CommandAssignFloat(1, 0.f);
				boolToFloat.AddCommand(assignment);

				CommandEndSection* end = new CommandEndSection();
				boolToFloat.AddCommand(end);
			}
		}

		Subroutine& boolToString = CreateGlobalFunction(HashedString("BoolToString"), true);
		{
			ScriptVariable& out = boolToString.AddOutput(HashedString("string"));
			out.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& param1 = boolToString.AddParameter(HashedString("bool"));
			param1.SetFixedType(ScriptVariable::VT_Bool);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Bool input = outStack.GetBoolParam(scope, 0, 1, false);
				DString convertedString = input.ToString();
				outStack.WriteStringOutput(convertedString, scope, 0, 1, 1, false);
			});
			boolToString.AddCommand(native);
		}
	}

	//Int conversions
	{
		Subroutine& intToBool = CreateGlobalFunction(HashedString("IntToBool"), false);
		{
			ScriptVariable& out = intToBool.AddOutput(HashedString("bool"));
			out.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = intToBool.AddParameter(HashedString("int"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			CommandPushVar* intB = new CommandPushVar(ScriptVariable::VT_Int);
			intToBool.AddCommand(intB);

			CommandAssignInt* bAssign = new CommandAssignInt(0, 0); //Assign intB to zero
			intToBool.AddCommand(bAssign);

			//If int is not equal to zero, then it'll write true. Otherwise it's going to write false.
			CommandInvokeSubroutine* conversion = new CommandInvokeSubroutine(Subroutine::FF_Global, HashedString("Int!=Int"));
			intToBool.AddCommand(conversion);

			CommandPopVars* popB = new CommandPopVars(1);
			intToBool.AddCommand(popB);
		}

		Subroutine& intToFloat = CreateGlobalFunction(HashedString("IntToFloat"), true);
		{
			ScriptVariable& out = intToFloat.AddOutput(HashedString("float"));
			out.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& param1 = intToFloat.AddParameter(HashedString("int"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int input = outStack.GetIntParam(scope, 0, 1, false);
				outStack.WriteFloatOutput(input.ToFloat(), scope, 0, 1, 1, false);
			});
			intToFloat.AddCommand(native);
		}

		Subroutine& intToString = CreateGlobalFunction(HashedString("IntToString"), true);
		{
			ScriptVariable& out = intToString.AddOutput(HashedString("string"));
			out.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& param1 = intToString.AddParameter(HashedString("int"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int input = outStack.GetIntParam(scope, 0, 1, false);
				outStack.WriteStringOutput(input.ToString(), scope, 0, 1, 1, false);
			});
			intToString.AddCommand(native);
		}
	}

	//Float conversions
	{
		Subroutine& floatToBool = CreateGlobalFunction(HashedString("FloatToBool"), false);
		{
			ScriptVariable& out = floatToBool.AddOutput(HashedString("bool"));
			out.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& param1 = floatToBool.AddParameter(HashedString("float"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			CommandPushVar* floatB = new CommandPushVar(ScriptVariable::VT_Float);
			floatToBool.AddCommand(floatB);

			CommandAssignFloat* bAssign = new CommandAssignFloat(0, 0.f); //Assign floatB to zero
			floatToBool.AddCommand(bAssign);

			//If float is not equal to zero, then it'll write true. Otherwise it's going to write false.
			CommandInvokeSubroutine* conversion = new CommandInvokeSubroutine(Subroutine::FF_Global, HashedString("Float!=Float"));
			floatToBool.AddCommand(conversion);

			CommandPopVars* popB = new CommandPopVars(1);
			floatToBool.AddCommand(popB);
		}

		Subroutine& floatToInt = CreateGlobalFunction(HashedString("FloatToInt"), true);
		{
			ScriptVariable& out = floatToInt.AddOutput(HashedString("int"));
			out.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param1 = floatToInt.AddParameter(HashedString("float"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float input = outStack.GetFloatParam(scope, 0, 1, false);
				outStack.WriteIntOutput(input.ToInt(), scope, 0, 1, 1, false);
			});
			floatToInt.AddCommand(native);
		}

		Subroutine& floatToString = CreateGlobalFunction(HashedString("FloatToString"), true);
		{
			ScriptVariable& out = floatToString.AddOutput(HashedString("string"));
			out.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& param1 = floatToString.AddParameter(HashedString("float"));
			param1.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float input = outStack.GetFloatParam(scope, 0, 1, false);
				outStack.WriteStringOutput(input.ToString(), scope, 0, 1, 1, false);
			});
			floatToString.AddCommand(native);
		}
	}

	//String conversions
	{
		Subroutine& stringToBool = CreateGlobalFunction(HashedString("StringToBool"), true);
		{
			ScriptVariable& param1 = stringToBool.AddParameter(HashedString("string"));
			param1.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& out = stringToBool.AddOutput(HashedString("bool"));
			out.SetFixedType(ScriptVariable::VT_Bool);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString input = outStack.GetStringParam(scope, 0, 1, false);
				outStack.WriteBoolOutput(input.ToBool(), scope, 0, 1, 1, false);
			});
			stringToBool.AddCommand(native);
		}

		Subroutine& stringToInt = CreateGlobalFunction(HashedString("StringToInt"), true);
		{
			ScriptVariable& param1 = stringToInt.AddParameter(HashedString("string"));
			param1.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& out = stringToInt.AddOutput(HashedString("int"));
			out.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString input = outStack.GetStringParam(scope, 0, 1, false);
				outStack.WriteIntOutput(input.Atoi(), scope, 0, 1, 1, false);
			});
			stringToInt.AddCommand(native);
		}

		Subroutine& stringToFloat = CreateGlobalFunction(HashedString("StringToFloat"), true);
		{
			ScriptVariable& param1 = stringToFloat.AddParameter(HashedString("string"));
			param1.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& out = stringToFloat.AddOutput(HashedString("float"));
			out.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString input = outStack.GetStringParam(scope, 0, 1, false);
				outStack.WriteFloatOutput(input.Stof(), scope, 0, 1, 1, false);
			});
			stringToFloat.AddCommand(native);
		}
	}
}

void ClmEngineComponent::CreateMathFunctions ()
{
	//Create add operations
	{
		Subroutine& intAdd = CreateGlobalFunction(HashedString("Int+Int"), true);
		{
			ScriptVariable& sum = intAdd.AddOutput(HashedString("sum"));
			sum.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& a = intAdd.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& b = intAdd.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteIntOutput(a+b, scope, 0, 1, 2, false);
			});
			intAdd.AddCommand(native);
		}

		Subroutine& floatAdd = CreateGlobalFunction(HashedString("Float+Float"), true);
		{
			ScriptVariable& sum = floatAdd.AddOutput(HashedString("sum"));
			sum.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& a = floatAdd.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& b = floatAdd.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteFloatOutput(a+b, scope, 0, 1, 2, false);
			});
			floatAdd.AddCommand(native);
		}

		//String concatenation 
		Subroutine& stringConcat = CreateGlobalFunction(HashedString("String+String"), true);
		{
			ScriptVariable& combined = stringConcat.AddOutput(HashedString("combined"));
			combined.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& a = stringConcat.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& b = stringConcat.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_String);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString a = outStack.GetStringParam(scope, 0, 2, false);
				DString b = outStack.GetStringParam(scope, 1, 2, false);
				outStack.WriteStringOutput(a+b, scope, 0, 1, 2, false);
			});
			stringConcat.AddCommand(native);
		}

		Subroutine& increment = CreateGlobalFunction(HashedString("Int++"), true);
		{
			ScriptVariable& sum = increment.AddOutput(HashedString("result"));
			sum.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& input = increment.AddParameter(HashedString("input"));
			input.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int input = outStack.GetIntParam(scope, 0, 1, false);
				++input;
				outStack.WriteIntOutput(input, scope, 0, 1, 1, false);
			});
			increment.AddCommand(native);
		}
	}

	//Create subtract operations
	{
		Subroutine& intSubtract = CreateGlobalFunction(HashedString("Int-Int"), true);
		{
			ScriptVariable& difference = intSubtract.AddOutput(HashedString("difference"));
			difference.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& a = intSubtract.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& b = intSubtract.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteIntOutput(a-b, scope, 0, 1, 2, false);
			});
			intSubtract.AddCommand(native);
		}

		Subroutine& floatSubtract = CreateGlobalFunction(HashedString("Float-Float"), true);
		{
			ScriptVariable& difference = floatSubtract.AddOutput(HashedString("difference"));
			difference.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& a = floatSubtract.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& b = floatSubtract.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteFloatOutput(a-b, scope, 0, 1, 2, false);
			});
			floatSubtract.AddCommand(native);
		}

		Subroutine& decrement = CreateGlobalFunction(HashedString("Int--"), true);
		{
			ScriptVariable& sum = decrement.AddOutput(HashedString("result"));
			sum.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& input = decrement.AddParameter(HashedString("input"));
			input.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int input = outStack.GetIntParam(scope, 0, 1, false);
				--input;
				outStack.WriteIntOutput(input, scope, 0, 1, 1, false);
			});
			decrement.AddCommand(native);
		}
	}

	//Create multiplication operations
	{
		Subroutine& intMultiply = CreateGlobalFunction(HashedString("Int*Int"), true);
		{
			ScriptVariable& product = intMultiply.AddOutput(HashedString("product"));
			product.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& a = intMultiply.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& b = intMultiply.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteIntOutput(a*b, scope, 0, 1, 2, false);
			});
			intMultiply.AddCommand(native);
		}

		Subroutine& floatMultiply = CreateGlobalFunction(HashedString("Float*Float"), true);
		{
			ScriptVariable& product = floatMultiply.AddOutput(HashedString("product"));
			product.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& a = floatMultiply.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& b = floatMultiply.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				outStack.WriteFloatOutput(a*b, scope, 0, 1, 2, false);
			});
			floatMultiply.AddCommand(native);
		}
	}

	//Create division operations
	{
		Subroutine& intDivide = CreateGlobalFunction(HashedString("Int/Int"), true);
		{
			ScriptVariable& quotient = intDivide.AddOutput(HashedString("quotient"));
			quotient.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& a = intDivide.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& b = intDivide.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				if (b == 0)
				{
					ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide %s by zero."), a);
					b = 1;
				}

				outStack.WriteIntOutput(a/b, scope, 0, 1, 2, false);
			});
			intDivide.AddCommand(native);
		}

		Subroutine& floatDivide = CreateGlobalFunction(HashedString("Float/Float"), true);
		{
			ScriptVariable& quotient = floatDivide.AddOutput(HashedString("quotient"));
			quotient.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& a = floatDivide.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& b = floatDivide.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				if (b == 0.f)
				{
					ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide %s by zero."), a);
					b = 1.f;
				}

				outStack.WriteFloatOutput(a/b, scope, 0, 1, 2, false);
			});
			floatDivide.AddCommand(native);
		}

		Subroutine& intMod = CreateGlobalFunction(HashedString("Int%Int"), true);
		{
			ScriptVariable& remainder = intMod.AddOutput(HashedString("remainder"));
			remainder.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& a = intMod.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& b = intMod.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Int);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int a = outStack.GetIntParam(scope, 0, 2, false);
				Int b = outStack.GetIntParam(scope, 1, 2, false);
				if (b == 0)
				{
					ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to modulo %s by zero."), a);
					b = 1;
				}

				outStack.WriteIntOutput(a%b, scope, 0, 1, 2, false);
			});
			intMod.AddCommand(native);
		}

		Subroutine& floatMod = CreateGlobalFunction(HashedString("Float%Float"), true);
		{
			ScriptVariable& remainder = floatMod.AddOutput(HashedString("remainder"));
			remainder.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& a = floatMod.AddParameter(HashedString("a"));
			a.SetFixedType(ScriptVariable::VT_Float);

			ScriptVariable& b = floatMod.AddParameter(HashedString("b"));
			b.SetFixedType(ScriptVariable::VT_Float);

			CommandNative* native = new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Float a = outStack.GetFloatParam(scope, 0, 2, false);
				Float b = outStack.GetFloatParam(scope, 1, 2, false);
				if (b == 0.f)
				{
					ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Attempted to modulo %s by zero."), a);
					b = 1.f;
				}

				outStack.WriteFloatOutput(a%b, scope, 0, 1, 2, false);
			});
			floatMod.AddCommand(native);
		}
	}
}

void ClmEngineComponent::CreateObjHandlingFunctions ()
{
	Subroutine& createObject = CreateGlobalFunction(HashedString("CreateObject"), true);
	{
		ScriptVariable& newObj = createObject.AddOutput(HashedString("newObj"));
		newObj.SetFixedType(ScriptVariable::VT_Obj);

		ScriptVariable& objClass = createObject.AddParameter(HashedString("objClass"));
		objClass.SetFixedType(ScriptVariable::VT_Class);

		ScriptVariable& owner = createObject.AddParameter(HashedString("owner"));
		owner.SetFixedType(ScriptVariable::VT_Obj);

		createObject.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
		{
			const ContentClass* objClass = outStack.GetClassParam(scope, 0, 2, false);
			if (objClass == nullptr)
			{
				outStack.WriteObjOutput(nullptr, scope, 0, 1, 2, false);
				return;
			}

			if (objClass->IsAbstract())
			{
				ContentLogicLog.Log(LogCategory::LL_Warning, TXT("Cannot instantiate %s since that class is an abstract class."), objClass->ReadClassName());
				outStack.WriteObjOutput(nullptr, scope, 0, 1, 2, false);
				return;
			}

			ContentObject* newObj = ContentObject::CreateObject();
			ContentObject* objOwner = outStack.GetObjParam(scope, 1, 2, false);
			newObj->BeginContentObject(objClass, scope, nullptr, objOwner); //Registers to scope. Invokes BeginObject.
			outStack.WriteObjOutput(newObj, scope, 0, 1, 2, false);
		}));
	}
}
SD_END