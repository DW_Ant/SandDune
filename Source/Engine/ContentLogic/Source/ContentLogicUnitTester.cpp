/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContentLogicUnitTester.cpp
=====================================================================
*/

#include "ClmEngineComponent.h"
#include "ClmStack.h"
#include "ContentLogicUnitTester.h"
#include "ContentClass.h"
#include "ContentObject.h"
#include "CommandAssignVar.h"
#include "CommandBeginSection.h"
#include "CommandElseClause.h"
#include "CommandEndSection.h"
#include "CommandIfCondition.h"
#include "CommandInvokeDelegate.h"
#include "CommandInvokeSubroutine.h"
#include "CommandInvokeSuper.h"
#include "CommandNative.h"
#include "CommandPopVars.h"
#include "CommandPushVar.h"
#include "CommandWhileLoop.h"
#include "ScriptComponent.h"
#include "ScriptComponentTest.h"
#include "ScriptScope.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::ContentLogicUnitTester, SD::UnitTester)
SD_BEGIN

bool ContentLogicUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool passed = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		passed &= TestStack(testFlags) && TestScriptVariables(testFlags) && TestContentObjects(testFlags) && TestCommands(testFlags) &&
			TestPolymorphism(testFlags) && TestScriptComponents(testFlags) && TestScriptFunctions(testFlags);
	}

	return passed;
}

bool ContentLogicUnitTester::TestStack (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Stack"));

	ClmStack stack;
	ScriptVariableRef a(nullptr, INDEX_NONE);
	ScriptVariableRef b(nullptr, INDEX_NONE);
	ScriptVariableRef c(nullptr, INDEX_NONE);
	ScriptVariableRef d(nullptr, INDEX_NONE);
	ScriptVariableRef e(nullptr, INDEX_NONE);

	SetTestCategory(testFlags, TXT("Pushing Variables"));
	{
		a = stack.PushVariable();
		if (!a.IsValid())
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After pushing a variable, the reference to that variable is still invalid."));
			return false;
		}

		b = stack.PushVariable();
		c = stack.PushVariable();
		d = stack.PushVariable();

		if (!b.IsValid() || !c.IsValid() || !d.IsValid())
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After pushing multiple variables, some of the references to that variable are still invalid."));
			return false;
		}

		if (a == b || a == c || a == d || b == c || b == d || c == d)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After pushing multiple variables, some of the variable references are equal to each other when all of them should have been unique."));
			return false;
		}

		size_t expectedSize = 4;
		if (stack.GetStackSize() != expectedSize)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After pushing multiple variables, the stack size is expected to be %s. Instead it's %s."), Int(expectedSize), Int(stack.GetStackSize()));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Accessing Variables"));
	{
		ScriptVariableRef ref = stack.EditVariable(0);
		if (ref != d)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. Unable to obtain a reference to the variable at the top of the stack."));
			return false;
		}

		ref = stack.EditVariable(1);
		if (ref != c)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. Unable to obtain a reference to the third variable on the stack."));
			return false;
		}

		e = stack.PushVariable();
		if (ref != c)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After pushing a new variable to the stack, the original reference is displaced since it's no longer pointing to its original variable before the push."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Popping Variables"));
	{
		stack.PopVariable();
		stack.PopVariable();
		if (e.IsValid() || d.IsValid())
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping a few variables, one of the variable references to the removed variable is still considered valid."));
			return false;
		}

		if (!c.IsValid())
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping a few variables, some of the references pointing to valid variables have been unexpectedly invalidated."));
			return false;
		}

		ScriptVariableRef ref = stack.EditVariable(0);
		if (ref != c)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping a few variables, obtaining the variable at the top of the stack does not match the expected variable."));
			return false;
		}

		size_t expected = 3;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping a few variables, the expected stack size is %s. Instead it's %s."), Int(expected), Int(stack.GetStackSize()));
			return false;
		}

		//The vector size should be the same since popping variables only moves the index.
		expected = 5;
		if (stack.ReadStack().size() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping a few variables, the size of the vector should still be %s since Shrink isn't invoked. Instead it's %s."), Int(expected), Int(stack.ReadStack().size()));
			return false;
		}

		stack.Shrink();
		expected = stack.GetStackSize();
		if (stack.ReadStack().size() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After shrinking the stack, the vector size (%s) should be equal to the stack size (%s)."), Int(stack.ReadStack().size()), Int(expected));
			return false;
		}

		//Pop two more variables, then push one variable. The stack idx should move back two then advance one.
		stack.PopVariable();
		stack.PopVariable();
		if (b.IsValid())
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping two more variables, one of the variable references to the popped variables should have been invalidated."));
			return false;
		}

		ref = stack.EditVariable(0);
		if (ref != a)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping two more variables, the top of the stack does not match the expected variable."));
			return false;
		}

		b = stack.PushVariable();
		ref = stack.EditVariable(0);
		if (!b.IsValid() || ref != b)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping two more variables, then pushing one, the variable reference to the top of the stack does not match the expected variable."));
			return false;
		}

		expected = 2;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping two more variables, then pushing one, the expected stack size is %s. Instead it's %s."), Int(expected), Int(stack.GetStackSize()));
			return false;
		}

		expected = 3;
		if (stack.ReadStack().size() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping two more variables, then pushing one, the expected vector size in the stack should still be %s. Instead it's %s."), Int(expected), Int(stack.ReadStack().size()));
			return false;
		}

		//Remove the last of the variables
		stack.PopVariable();
		stack.PopVariable();
		expected = 0;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Stack unit test failed. After popping the rest of the variables, the size of the stack should have been %s. Instead it's %s."), Int(expected), Int(stack.GetStackSize()));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Stack"));
	return true;
}

bool ContentLogicUnitTester::TestScriptVariables (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("ScriptVariables"));

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		ScriptVariable a(HashedString("a"));
		ScriptVariable b(HashedString("b"));

		a.WriteBool(true);
		b.WriteBool(true);
		if (a != b)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered not equal despite writing true to both of them."));
			return false;
		}

		b.WriteBool(false);
		if (a == b)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered equal despite writing true to one and false to the other."));
			return false;
		}

		b.WriteInt(1);
		if (a == b)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered equal despite one being an Int and the other being a Bool."));
			return false;
		}

		a.WriteFloat(5.f);
		ScriptVariable c(a);
		if (a != c)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered not equal despite one was created using a copy constructor."));
			return false;
		}

		DString testStr = TXT("ContentLogicModule");
		a.WriteDString(testStr);
		b.WriteDString(testStr);
		if (a != b)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered not equal despite writing %s to both of them."), testStr);
			return false;
		}

		b.WriteDString(TXT("Something else"));
		if (a == b)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered equal despite writing different strings to them."));
			return false;
		}

		c = a;
		if (a != c)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. ScriptVariables are considered not equal despite assigning one to the other."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Write Test"));
	{
		ScriptVariable var;
		
		Bool writeBool(true);
		var.WriteBool(writeBool);
		Bool readBool = var.ToBool();
		if (writeBool != readBool)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After writing %s to a ScriptVariable, it was able to read a %s from it instead."), writeBool, readBool);
			return false;
		}

		Int writeInt(-16);
		var.WriteInt(writeInt);
		Int readInt = var.ToInt();
		if (writeInt != readInt)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After writing %s to a ScriptVariable, it was able to read a %s from it instead."), writeInt, readInt);
			return false;
		}

		Float writeFloat(-3.14f);
		var.WriteFloat(writeFloat);
		Float readFloat = var.ToFloat();
		if (writeFloat != readFloat)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After writing %s to a ScriptVariable, it was able to read a %s from it instead."), writeFloat, readFloat);
			return false;
		}

		DString writeString(TXT("Write Test"));
		var.WriteDString(writeString);
		DString readString = var.ToDString();
		if (writeString.Compare(readString, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After writing \"%s\" to a ScriptVariable, it was able to read a \"%s\" from it instead."), writeString, readString);
			return false;
		}

		var.Reset();
		var.SetFixedType(ScriptVariable::VT_String); //Force type to string to treat this variable as a string. This will suppress the log warning when attempting to read a variable incorrectly. The data should still be clear though.
		readString = var.ToDString();
		if (!readString.IsEmpty())
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After resetting a ScriptVariable, it was still able to read a \"%s\" from it."), readString);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Pointers"));
	{
		ScriptScope* scope = ScriptScope::CreateObject();
		ClmStack stack;

		ContentClass testClass(HashedString("ScriptVariableTestClass"), HashedString());
		ScriptVariable& classVar1 = testClass.CreateMemberVariable(HashedString("HeapTestVar[0]"));
		ScriptVariable& classVar2 = testClass.CreateMemberVariable(HashedString("HeapTestVar[1]"));
		ContentObject* dummyObj = ContentObject::CreateObject();
		dummyObj->BeginContentObject(&testClass, scope, nullptr, nullptr);

		std::function<void()> cleanupTest([&]()
		{
			//Must unregister the ContentObject from the ScriptScope since the ScriptScope would automatically delete all registered objects when the Scope is deleted.
			dummyObj->Destroy();
			scope->Destroy();
		});

		Int stackInt(81);
		ScriptVariableRef stackVar = stack.PushVariable(HashedString("StackInt"));
		stackVar.EditVar().WriteInt(stackInt);

		ScriptVariableRef buffer = stack.PushVariable(HashedString("UnusedBufferVar")); //Not really used. But just to add some space between stackVar and pointA on the stack.

		Float heapFloat0(-18.5f);
		ScriptVariableRef heapVar0 = dummyObj->GetMemberVariable(0);
		heapVar0.EditVar().WriteFloat(heapFloat0);

		Int heapInt1(-35);
		ScriptVariableRef heapVar1 = dummyObj->GetMemberVariable(1);
		heapVar1.EditVar().WriteInt(heapInt1);

		ScriptVariableRef pointA = stack.PushVariable(HashedString("PointerA"));
		pointA.EditVar().WritePointer(0); //Points at stackVar


		ScriptVariableRef pointB = stack.PushVariable(HashedString("PointerB"));
		pointB.EditVar().WritePointer(2); //Points at A

		ScriptVariableRef pointedA = pointB.ReadVar().GetPointer(scope, stack);
		if (pointedA != pointA)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to obtain a reference to a pointer from a different pointer."));
			cleanupTest();
			return false;
		}

		ScriptVariableRef pointedAt = pointA.ReadVar().GetPointer(scope, stack);
		if (pointedAt != stackVar)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to obtain a reference to the original stack variable from a pointer."));
			cleanupTest();
			return false;
		}

		Int readStackInt = pointedAt.ReadVar().ToInt();
		if (readStackInt != stackInt)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to obtain the original int through a variable reference. Original int = %s. Int read from reference = %s."), stackInt, readStackInt);
			cleanupTest();
			return false;
		}

		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt != stackVar)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to obtain the original stack variable when recursively navigating through variable references."));
			cleanupTest();
			return false;
		}

		//Create a variable at the end of the stack. It should not displace the pointers.
		ScriptVariableRef extraVar = stack.PushVariable(HashedString("TrailingVariable"));
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt != stackVar)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After pushing a new variable to the end of the stack, a pointer pointing at a variable on the stack is displaced since it's no longer pointing to the same object it once was."));
			cleanupTest();
			return false;
		}


		//Have pointA point at heapVar0.
		pointA.EditVar().WritePointer(dummyObj, 0);

		//Verify pointA can retrieve heapVar0.
		pointedAt = pointA.EditVar().GetPointer(scope, stack);
		if (pointedAt != heapVar0)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to obtain the first variable on the heap."));
			cleanupTest();
			return false;
		}

		Float readFloat = pointedAt.ReadVar().ToFloat();
		if (readFloat != heapFloat0)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to retrieve the value of the original float. Original float = %s. Float read in = %s."), heapFloat0, readFloat);
			cleanupTest();
			return false;
		}

		//pointB should automatically point at the heap
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt != heapVar0)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Failed to retrieve a reference to the original heap variable by recursively navigating through the pointers."));
			cleanupTest();
			return false;
		}

		//Write to the heap through the reference.
		Float newFloat(150.2f);
		pointedAt.EditVar().WriteFloat(newFloat);
		readFloat = heapVar0.ReadVar().ToFloat();
		if (readFloat != newFloat)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After editing a variable through a reference to %s, failed to retrieve that value through the original variable. It read %s instead."), newFloat, readFloat);
			cleanupTest();
			return false;
		}

		//See if the second heap variable can be obtained.
		pointB.EditVar().WritePointer(dummyObj, 1);
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt != heapVar1)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Unable to obtain the second heap variable through a reference."));
			cleanupTest();
			return false;
		}

		//Test object pointers
		ScriptVariable objPointer;
		objPointer.WriteObjPointer(dummyObj);

		ContentObject* objReference = objPointer.ToObjPointer(scope);
		if (objReference != dummyObj)
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. Unable to obtain a reference to an object."));
			cleanupTest();
			return false;
		}

		//Ensure variables can be assigned to nullptr.
		pointA.EditVar().WritePointer(dummyObj, INDEX_NONE);
		pointB.EditVar().WritePointer(2); //point at A
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt.IsValid())
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After assigning a pointer to INDEX_NONE, the ScriptVariableRef still thinks it's a valid reference."));
			cleanupTest();
			return false;
		}

		pointA.EditVar().WritePointer(nullptr, 0);
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt.IsValid())
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After assigning a pointer to nullptr, the ScriptVariableRef still thinks its a valid reference."));
			cleanupTest();
			return false;
		}

		pointA.EditVar().WritePointer(INDEX_NONE);
		pointedAt = pointB.ReadVar().GetPointerRecursive(scope, stack);
		if (pointedAt.IsValid())
		{
			UnitTestError(testFlags, TXT("ScriptVariable test failed. After assigning a pointer to INDEX_NONE via stack offset, the ScriptVariableRef still thinks it's a valid reference."));
			cleanupTest();
			return false;
		}

		cleanupTest();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Content Classes"));
	{
		ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
		CHECK(clmEngine != nullptr)

		ContentClass* testClassRoot = clmEngine->CreateScriptClass(HashedString("UnitTestScriptVarRoot"), HashedString());
		ContentClass* testClassSibling = clmEngine->CreateScriptClass(HashedString("UnitTestScriptVarSibling"), HashedString());
		ContentClass* testClassChild = clmEngine->CreateScriptClass(HashedString("UnitTestScriptVarChild"), HashedString("UnitTestScriptVarRoot"));
		std::function<void()> cleanupTest([&]()
		{
			clmEngine->DestroyScriptClass(testClassRoot, true); //child is also removed
			clmEngine->DestroyScriptClass(testClassSibling, true);
		});

		ScriptVariable scriptVar;
		ContentClass* curClass;
		std::function<bool()> testVar([&]()
		{
			scriptVar.WriteClass(curClass);
			const ContentClass* readClass = scriptVar.ToClass();
			if (readClass != curClass)
			{
				DString className = (curClass != nullptr) ? curClass->ReadClassName().ToString() : TXT("nullptr");
				UnitTestError(testFlags, TXT("Content Class test failed. After writing %s to the script variable, it's expected for the variable to be able to recall that class."), className);
				cleanupTest();
				return false;
			}

			return true;
		});

		curClass = testClassRoot;
		if (!testVar())
		{
			return false;
		}

		curClass = testClassSibling;
		if (!testVar())
		{
			return false;
		}

		curClass = testClassChild;
		if (!testVar())
		{
			return false;
		}

		curClass = nullptr;
		if (!testVar())
		{
			return false;
		}

		cleanupTest();
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("ScriptVariables"));
	return true;
}

bool ContentLogicUnitTester::TestContentObjects (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Content Objects"));

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)

	ContentClass* parentClass = clmEngine->CreateScriptClass(HashedString("UnitTestParentClass"), HashedString());
	ContentClass* childClass = clmEngine->CreateScriptClass(HashedString("UnitTestChildClass"), HashedString("UnitTestMidClass")); //Create child before parent. The engine component should pair the classes when the parent is initialized.
	ContentClass* midClass = clmEngine->CreateScriptClass(HashedString("UnitTestMidClass"), HashedString("UnitTestParentClass")); //Create child after parent. The engine component should still pair this class with the parent.
	ScriptScope* scope = ScriptScope::CreateObject();

	std::function cleanupTest([&]()
	{
		clmEngine->DestroyScriptClass(parentClass, true);
		scope->Destroy();
	});

	SetTestCategory(testFlags, TXT("Init Content Classes"));
	{
		if (midClass->GetParentClass() != parentClass)
		{
			DString actualParent = (midClass->GetParentClass() != nullptr) ? midClass->GetParentClass()->ReadClassName().ToString() : TXT("nullptr");
			UnitTestError(testFlags, TXT("Content Object unit test failed. The engine component is unable to assemble the class structure correctly. The mid class is suppose to inherit from %s. Instead its parent is %s."), parentClass->ReadClassName(), actualParent);
			cleanupTest();
			return false;
		}

		if (childClass->GetParentClass() != midClass)
		{
			DString actualParent = (childClass->GetParentClass() != nullptr) ? childClass->GetParentClass()->ReadClassName().ToString() : TXT("nullptr");
			UnitTestError(testFlags, TXT("Content Object test failed. The engine component is unable to assemble the class structure correctly. The child class is suppose to inherit from %s. Instead its parent is %s."), midClass->ReadClassName(), actualParent);
			cleanupTest();
			return false;
		}

		{
			ContentClass* child;
			ContentClass* parent;
			bool expected;
			std::function<bool()> testIsA([&]()
			{
				bool actual = child->IsA(parent);
				if (expected != actual)
				{
					DString conditional = (expected) ? DString::EmptyString : TXT(" NOT ");
					UnitTestError(testFlags, TXT("Content Class test failed. %s is %s suppose to be a child or equal to %s. ContentClass::IsA suggests otherwise."), child->ReadClassName(), conditional, parent->ReadClassName());
					cleanupTest();
					return false;
				}

				return true;
			});

			child = childClass;
			parent = childClass;
			expected = true;
			if (!testIsA())
			{
				return false;
			}

			parent = midClass;
			if (!testIsA())
			{
				return false;
			}

			parent = parentClass;
			if (!testIsA())
			{
				return false;
			}

			child = midClass;
			parent = childClass;
			expected = false;
			if (!testIsA())
			{
				return false;
			}

			parent = midClass;
			expected = true;
			if (!testIsA())
			{
				return false;
			}

			parent = parentClass;
			if (!testIsA())
			{
				return false;
			}

			child = parentClass;
			parent = childClass;
			expected = false;
			if (!testIsA())
			{
				return false;
			}

			parent = midClass;
			if (!testIsA())
			{
				return false;
			}

			parent = parentClass;
			expected = true;
			if (!testIsA())
			{
				return false;
			}
		}

		{
			//Test IsParentOf
			ContentClass* parent;
			ContentClass* child;
			bool expected;
			std::function<bool()> testIsParentOf([&]()
			{
				bool actual = parent->IsParentOf(child);
				if (actual != expected)
				{
					DString conditional = expected ? TXT("NOT") : DString::EmptyString;
					UnitTestError(testFlags, TXT("Content Class test failed. The %s is %s suppose to be considered a parent of %s. ContentClass::IsParentOf suggests otherwise."), parent->ReadClassName(), conditional, child->ReadClassName());
					cleanupTest();
					return false;
				}

				return true;
			});

			parent = parentClass;
			child = parentClass;
			expected = false;
			if (!testIsParentOf())
			{
				return false;
			}

			child = midClass;
			expected = true;
			if (!testIsParentOf())
			{
				return false;
			}

			child = childClass;
			if (!testIsParentOf())
			{
				return false;
			}

			parent = midClass;
			child = parentClass;
			expected = false;
			if (!testIsParentOf())
			{
				return false;
			}

			child = midClass;
			if (!testIsParentOf())
			{
				return false;
			}

			child = childClass;
			expected = true;
			if (!testIsParentOf())
			{
				return false;
			}

			parent = childClass;
			child = parentClass;
			expected = false;
			if (!testIsParentOf())
			{
				return false;
			}

			child = midClass;
			if (!testIsParentOf())
			{
				return false;
			}

			child = childClass;
			if (!testIsParentOf())
			{
				return false;
			}
		}

		//Create member variables and their defaults for each class.
		ScriptVariable& parentInt = parentClass->CreateMemberVariable(HashedString("ParentInt"));
		parentInt.SetFixedType(ScriptVariable::VT_Int);
		parentInt.WriteInt(16);

		ScriptVariable& parentFloat = parentClass->CreateMemberVariable(HashedString("ParentFloat"));
		parentFloat.SetFixedType(ScriptVariable::VT_Float);
		parentFloat.WriteFloat(-2.5f);

		ScriptVariable& midBool = midClass->CreateMemberVariable(HashedString("MidBool"));
		midBool.SetFixedType(ScriptVariable::VT_Bool);
		midBool.WriteBool(true);

		ScriptVariable& midInt = midClass->CreateMemberVariable(HashedString("MidInt"));
		midInt.SetFixedType(ScriptVariable::VT_Int);
		midInt.WriteInt(-5);

		ScriptVariable& childString = childClass->CreateMemberVariable(HashedString("ChildString"));
		childString.SetFixedType(ScriptVariable::VT_String);
		childString.WriteDString(TXT("DefaultString"));

		ScriptVariable& childObj = childClass->CreateMemberVariable(HashedString("ChildObj"));
		childObj.SetFixedType(ScriptVariable::VT_Obj);
		childObj.WriteObjPointer(nullptr);
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Object Instantiation"));
	{
		std::vector<ContentObject*> objsToDestroy;
		std::function<void()> cleanupCategory([&]()
		{
			for (ContentObject* obj : objsToDestroy)
			{
				obj->Destroy();
			}
			ContainerUtils::Empty(OUT objsToDestroy);
		});

		ContentObject* testObj;
		size_t expectedIdx;
		HashedString varName;
		std::function<bool()> testFindVar([&]()
		{
			size_t actualIdx;
			ScriptVariableRef varRef = testObj->FindMemberVariable(varName, OUT actualIdx);
			if (actualIdx != expectedIdx)
			{
				UnitTestError(testFlags, TXT("Content Object test failed. When searching for the member variable %s, there's a variable index mismatch. Expected index = %s. Actual index = %s."), varName, Int(expectedIdx), Int(actualIdx));
				cleanupCategory();
				cleanupTest();
				return false;
			}

			if (actualIdx == INDEX_NONE && varRef.IsValid())
			{
				UnitTestError(testFlags, TXT("Content Object test failed. When searching for the member variable %s, the variable index value is INDEX_NONE, but the returned variable reference is valid."), varName);
				cleanupCategory();
				cleanupTest();
				return false;
			}

			return true;
		});

		ContentObject* parentObj = ContentObject::CreateObject();
		parentObj->BeginContentObject(parentClass, nullptr, nullptr, nullptr);
		objsToDestroy.push_back(parentObj);
		testObj = parentObj;

		varName = HashedString("ParentInt");
		expectedIdx = 0;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("ParentFloat");
		expectedIdx = 1;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("MidBool");
		expectedIdx = INDEX_NONE;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("ChildObj");
		if (!testFindVar())
		{
			return false;
		}

		ContentObject* midObj = ContentObject::CreateObject();
		midObj->BeginContentObject(midClass, nullptr, nullptr, nullptr);
		objsToDestroy.push_back(midObj);
		testObj = midObj;

		//The midObj should have all member variables found in the parent class, too.
		varName = HashedString("ParentInt");
		expectedIdx = 0;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("MidInt");
		expectedIdx = 3;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("ChildString");
		expectedIdx = INDEX_NONE;
		if (!testFindVar())
		{
			return false;
		}

		ContentObject* childObj = ContentObject::CreateObject();
		childObj->BeginContentObject(childClass, nullptr, nullptr, nullptr);
		objsToDestroy.push_back(childObj);
		testObj = childObj;

		varName = HashedString("ParentFloat");
		expectedIdx = 1;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("MidBool");
		expectedIdx = 2;
		if (!testFindVar())
		{
			return false;
		}

		varName = HashedString("ChildObj");
		expectedIdx = 5;
		if (!testFindVar())
		{
			return false;
		}

		cleanupCategory();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Editing Objects"));
	{
		ContentObject* childObj = ContentObject::CreateObject();
		childObj->BeginContentObject(childClass, nullptr, nullptr, nullptr);
		ContentObject* childObj2 = ContentObject::CreateObject();
		childObj2->BeginContentObject(childClass, nullptr, nullptr, nullptr);
		std::function<void()> cleanupCategory([&]()
		{
			childObj->Destroy();
			childObj2->Destroy();
		});

		HashedString varName("ChildString");
		ScriptVariableRef classRef(nullptr, INDEX_NONE);
		for (size_t i = 0; i < childClass->ReadMemberVariables().size(); ++i)
		{
			if (childClass->ReadMemberVariables().at(i).ReadVariableName() == varName)
			{
				classRef = ScriptVariableRef(&childClass->EditMemberVariables(), i);
				break;
			}
		}

		if (!classRef.IsValid())
		{
			UnitTestError(testFlags, TXT("Content Object test failed. Unable to find variable named %s from class."), varName);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		size_t varIdx;
		ScriptVariableRef childString = childObj->FindMemberVariable(varName, OUT varIdx);
		if (!childString.IsValid())
		{
			UnitTestError(testFlags, TXT("Content Object test failed. Unable to find variable named %s from instantiated object."), varName);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		ScriptVariable childString2 = childObj2->EditMemberVariables().at(varIdx);

		//Ensure the object instances inherited string values from the class.
		DString classValue = classRef.EditVar().ToDString();
		DString childValue = childString.EditVar().ToDString();
		DString child2Value = childString2.ToDString();

		DString expectedStr = TXT("DefaultString");
		if (classValue.Compare(expectedStr, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Content Object test failed. The string value from the class should have been \"%s\". Instead it's \"%s\"."), expectedStr, classValue);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		if (classValue.Compare(childValue, DString::CC_CaseSensitive) != 0 || classValue.Compare(child2Value, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Content Object test failed. The default string value from each instantiated instance should be inherited from the class. Instead the child string values are \"%s\" and \"%s\". The class default is \"%s\"."), childValue, child2Value, classValue);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		DString writtenStr = TXT("My Edited String");
		childString.EditVar().WriteDString(writtenStr);

		//Refresh strings to check their values
		classValue = classRef.EditVar().ToDString();
		childValue = childString.EditVar().ToDString();
		child2Value = childString2.ToDString();
		if (classValue.Compare(childValue, DString::CC_CaseSensitive) == 0 || classValue.Compare(child2Value, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Content Object test failed. After writing \"%s\" to one of the objects, only one of the instances are expected to contain that value. Instead the object instances contain \"%s\" and \"%s\". The class default reads \"%s\"."), writtenStr, childValue, child2Value, classValue);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		writtenStr = TXT("My Other Edited Text");
		childString2.WriteDString(writtenStr);

		//Refresh strings to check their values
		classValue = classRef.EditVar().ToDString();
		childValue = childString.EditVar().ToDString();
		child2Value = childString2.ToDString();

		if (child2Value.Compare(writtenStr, DString::CC_CaseSensitive) != 0 || childValue.Compare(child2Value, DString::CC_CaseSensitive) == 0 || classValue.Compare(child2Value, DString::CC_CaseSensitive) == 0)
		{
			UnitTestError(testFlags, TXT("Content Object test failed. After writing \"%s\" to the other object, the string values are expected to be unique from each other. Instead the string values are \"%s\" and \"%s\". The class default is \"%s\"."), writtenStr, childValue, child2Value, classValue);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		cleanupCategory();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Object Ownership"));
	{
		//Use pointers since this test will be testing object deletion from the heap.
		ContentObject* ownerObj = ContentObject::CreateObject();
		ownerObj->BeginContentObject(midClass, scope, nullptr, nullptr);

		ContentObject* childA = ContentObject::CreateObject();
		childA->BeginContentObject(midClass, scope, nullptr, ownerObj);

		ContentObject* childB = ContentObject::CreateObject();
		childB->BeginContentObject(childClass, scope, nullptr, ownerObj);

		ContentObject* leaf = ContentObject::CreateObject();
		leaf->BeginContentObject(parentClass, scope, nullptr, childA);

		std::function<void()> cleanupCategory([&]()
		{
			//Don't call UnregisterObject directly since the ContentObject's destructor will unregister themselves from the scope.
			if (ownerObj != nullptr)
			{
				ownerObj->Destroy();
				ownerObj = nullptr;
			}

			if (childA != nullptr)
			{
				childA->Destroy();
				childA = nullptr;
			}

			if (childB != nullptr)
			{
				childB->Destroy();
				childB = nullptr;
			}

			if (leaf != nullptr)
			{
				leaf->Destroy();
				leaf = nullptr;
			}
		});

		ContentObject* testOwner;
		ContentObject* testOwned;
		bool expected;
		std::function<bool()> testOwnership([&]()
		{
			bool actual = testOwner->IsOwnerOf(testOwned);
			if (actual != expected)
			{
				DString conditionalStr = expected ? TXT("") : TXT(" NOT ");
				UnitTestError(testFlags, TXT("Object Ownership test failed. %s is %s expected to be an owner of %s."), testOwner->ToString(), conditionalStr, testOwned->ToString());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			return true;
		});

		testOwner = ownerObj;
		testOwned = childA;
		expected = true;
		if (!testOwnership())
		{
			return false;
		}

		testOwned = leaf;
		if (!testOwnership())
		{
			return false;
		}

		testOwner = childA;
		if (!testOwnership())
		{
			return false;
		}

		testOwner = childA;
		testOwned = ownerObj;
		expected = false;
		if (!testOwnership())
		{
			return false;
		}

		testOwner = leaf;
		if (!testOwnership())
		{
			return false;
		}

		testOwner = childA;
		testOwned = childB;
		if (!testOwnership())
		{
			return false;
		}

		//Destroying childA should also destroy leaf. Owner and childB should remain.
		size_t ownerId = ownerObj->GetId();
		size_t childAId = childA->GetId();
		size_t childBId = childB->GetId();
		size_t leafId = leaf->GetId();
		childA->Destroy(); //deletes leaf
		leaf = nullptr;
		childA = nullptr;
		if (scope->ReadHeap().contains(childAId))
		{
			UnitTestError(testFlags, TXT("Object Ownership test failed. After deleting childA, the scope still has a reference to it on the Heap."));
			cleanupCategory();
			cleanupTest();
			return false;
		}

		if (scope->ReadHeap().contains(leafId))
		{
			UnitTestError(testFlags, TXT("Object Ownership test failed. After deleting childA, which is an owner of leaf object, the scope still has a reference to the leaf on the Heap."));
			cleanupCategory();
			cleanupTest();
			return false;
		}

		if (!scope->ReadHeap().contains(childBId) || !scope->ReadHeap().contains(ownerId))
		{
			UnitTestError(testFlags, TXT("Object Ownership test failed. After deleting childA, the scope lost references to objects that shouldn't be affected."));
			cleanupCategory();
			cleanupTest();
			return false;
		}

		//Deleting parent should delete childB. ChildA and leaf shouldn't be affected since those objects are already destroyed.
		ownerObj->Destroy(); //deletes childB
		childB = nullptr;
		ownerObj = nullptr;
		if (scope->ReadHeap().contains(ownerId))
		{
			UnitTestError(testFlags, TXT("Object Ownership test failed. After deleting the owner, the scope still has a reference to it on the Heap."));
			cleanupCategory();
			cleanupTest();
			return false;
		}

		if (scope->ReadHeap().contains(childBId))
		{
			UnitTestError(testFlags, TXT("Object Ownership test failed. After deleting the owner, the scope still has a reference to childB even though the child should have been deleted when the owner was deleted."));
			cleanupCategory();
			cleanupTest();
			return false;
		}

		cleanupCategory();
	}
	CompleteTestCategory(testFlags);

	cleanupTest();
	ExecuteSuccessSequence(testFlags, TXT("Content Objects"));
	return true;
}

bool ContentLogicUnitTester::TestCommands (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Commands"));

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)

	ScriptScope* scope = ScriptScope::CreateObject();
	HashedString functionCounterName("FunctionCounter");
	{
		ScriptVariable& functionCounterVar = scope->CreateGlobalVariable(functionCounterName);
		functionCounterVar.SetFixedType(ScriptVariable::VT_Int);
		functionCounterVar.WriteInt(0);
	}

	ClmStack stack;
	std::vector<DString> callstack;

	ContentClass* testClass = clmEngine->CreateScriptClass(HashedString("UnitTestClass"), HashedString());
	{
		ScriptVariable& leadingInt = testClass->CreateMemberVariable(HashedString("LeadingInt"));
		leadingInt.SetFixedType(ScriptVariable::VT_Int);
		
		ScriptVariable& trailingFloat = testClass->CreateMemberVariable(HashedString("TrailingFloat"));
		trailingFloat.SetFixedType(ScriptVariable::VT_Float);
	}
	
	ContentObject* testObj = ContentObject::CreateObject();
	testObj->BeginContentObject(testClass, scope, nullptr, nullptr);

	std::function<void()> cleanupTest([&]()
	{
		testObj->Destroy();
		clmEngine->DestroyScriptClass(testClass, true);
		scope->Destroy();
	});

	SetTestCategory(testFlags, TXT("Push Command"));
	{
		Subroutine& testPushCommand = scope->CreateStaticFunction(HashedString("TestPushCommand"), false, true);
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_Bool));
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_Int));
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_Float));
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_String));
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_Obj));
		testPushCommand.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
		testPushCommand.ExecuteFunction(scope, OUT callstack, OUT stack);

		size_t expected = 6;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Push command test failed. After executing a function with %s push commands, the stack size should have %s variables in it. Instead the stack size is %s."), Int(expected), Int(expected), Int(stack.GetStackSize()));
			cleanupTest();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Assign Variables"));
	{
		Bool testBool = true;
		Int testInt = 35;
		Float testFloat = -45.25f;
		DString testStr = TXT("Doodle The Cat");
		//Don't write one for obj pointer. That's not a valid test case. Wouldn't make sense to hard code a memory address.
		
		Subroutine& testAssignCmd = scope->CreateStaticFunction(HashedString("TestAssignCmd"), false, true);
		testAssignCmd.AddCommand(new CommandAssignBool(5, testBool));
		testAssignCmd.AddCommand(new CommandAssignInt(4, testInt));
		testAssignCmd.AddCommand(new CommandAssignFloat(3, testFloat));
		testAssignCmd.AddCommand(new CommandAssignString(2, testStr));
		testAssignCmd.ExecuteFunction(scope, OUT callstack, OUT stack);

		ScriptVariableRef var = stack.EditVariable(5);
		if (var.EditVar().ToBool() != testBool)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that writes %s to a bool, reading from it returned %s instead."), testBool, var.EditVar().ToBool());
			cleanupTest();
			return false;
		}

		var = stack.EditVariable(4);
		if (var.EditVar().ToInt() != testInt)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that writes %s to an int, reading from it returned %s instead."), testInt, var.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		var = stack.EditVariable(3);
		if (var.EditVar().ToFloat() != testFloat)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that writes %s to a float, reading from it returned %s instead."), testFloat, var.EditVar().ToFloat());
			cleanupTest();
			return false;
		}

		var = stack.EditVariable(2);
		if (var.EditVar().ToDString().Compare(testStr, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that writes \"%s\" to a string, reading from it returned \"%s\" instead."), testStr, var.EditVar().ToDString());
			cleanupTest();
			return false;
		}

		testInt = 115;
		Subroutine& testRefAssign = scope->CreateStaticFunction(HashedString("TestRefAssign"), false, true);
		testRefAssign.AddCommand(new CommandAssignInt(4, testInt));
		testRefAssign.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
		//Stack now looks like this:  Bool, Int, Float, String, Obj, Pointer[0], Pointer[1]
		testRefAssign.AddCommand(new CommandAssignRef(1, 5)); //Assign pointer to point at Int
		testRefAssign.ExecuteFunction(scope, OUT callstack, OUT stack);

		var = stack.EditVariable(1); //Pointer[0]
		if (!var.IsValid() || var.EditVar().GetVarType() != ScriptVariable::VT_Pointer)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that assigns a variable pointer, reading from that pointer should have resulted in a pointer type."));
			cleanupTest();
			return false;
		}

		ScriptVariableRef pointedAt = var.EditVar().GetPointer(scope, OUT stack);
		if (!pointedAt.IsValid() || pointedAt != stack.EditVariable(5)) //Should be pointing at the int
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that assigns a variable pointer, reading from that pointer should have resulted in pointing at the Int. It's pointing at %s instead."), pointedAt.EditVar().InterpretValueAsString(scope, stack));
			cleanupTest();
			return false;
		}

		if (pointedAt.EditVar().ToInt() != testInt)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a command that assigns a variable pointer, reading from that pointer should have resulted in %s. Instead it's %s."), testInt, pointedAt.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		Subroutine& testNestedRef = scope->CreateStaticFunction(HashedString("NestedReferences"), false, true);
		testNestedRef.AddCommand(new CommandAssignRef(0, 1)); //Have Pointer[1] point at Pointer[0]
		testNestedRef.AddCommand(new CommandAssignRef(1, 4)); //Have Pointer[0] point at the float
		testNestedRef.ExecuteFunction(scope, OUT callstack, OUT stack);

		if (stack.EditVariable(1).EditVar().GetVarType() != ScriptVariable::VT_Pointer || stack.EditVariable(0).EditVar().GetVarType() != ScriptVariable::VT_Pointer)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a nested reference command, reading from both pointers should be a pointer type."));
			cleanupTest();
			return false;
		}

		if (stack.EditVariable(0).EditVar().GetPointer(scope, stack) != stack.EditVariable(1))
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a nested reference command, reading from the outer pointer should point at the inner pointer."));
			cleanupTest();
			return false;
		}

		pointedAt = stack.EditVariable(0).EditVar().GetPointerRecursive(scope, stack);
		if (!pointedAt.IsValid() || pointedAt.EditVar().ToFloat() != testFloat)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After running a nested reference command, recursively dereferencing the outer pointer should result in %s. Instead it's %s."), testFloat, pointedAt.EditVar().ToFloat());
			cleanupTest();
			return false;
		}

		Subroutine& pointAtMemberVar = scope->CreateStaticFunction(HashedString("PointAtMemberVar"), false, true);
		{
			ScriptVariable& objPointer = pointAtMemberVar.AddParameter(HashedString("ObjPointer")); //Need to add a parameter since the object pointer will be generated at runtime instead of design time.
			objPointer.SetFixedType(ScriptVariable::VT_Obj);

			pointAtMemberVar.AddCommand(new CommandAssignRef(2, 0, 1)); //Pointer[0] point at object's TrailingFloat member variable.
		}

		ScriptVariableRef param = stack.PushVariable(HashedString("ObjPointerParam")); //Before calling the subroutine, must push params onto the stack that the subroutine will read from.
		param.EditVar().WriteObjPointer(testObj);
		pointAtMemberVar.ExecuteFunction(scope, OUT callstack, OUT stack);
		stack.PopVariable(); //Remove the ObjPointerParameter

		var = stack.EditVariable(1);
		if (!var.IsValid())
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. Failed to assign a pointer to an object member variable."));
			cleanupTest();
			return false;
		}

		pointedAt = var.EditVar().GetPointer(scope, stack);
		if (!pointedAt.IsValid() || pointedAt.EditVar().GetVarType() != ScriptVariable::VT_Float)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. Failed to assign a pointer to point at a float member variable of an object."));
			cleanupTest();
			return false;
		}

		//Edit the object's float to see if it's reflected in the pointer.
		CHECK(testObj->EditMemberVariables().size() >= 2)
		testFloat = 40.2f;
		testObj->EditMemberVariables().at(1).WriteFloat(testFloat);
		if (pointedAt.EditVar().ToFloat() != testFloat)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After assigning the test object's member variable to %s, it's expected that the pointer will reflect those changes. Instead the pointer points at %s."), testFloat, pointedAt.EditVar().ToFloat());
			cleanupTest();
			return false;
		}

		//Ensure Pointer[1] -> Pointer[0] -> Obj->Float reflects the new changes.
		var = stack.EditVariable(0);
		pointedAt = var.EditVar().GetPointerRecursive(scope, stack);
		if (!pointedAt.IsValid() || pointedAt.EditVar().GetVarType() != ScriptVariable::VT_Float)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. After assigning a pointer to point at a float member variable, recursively dereferencing a pointer should have returned that variable."));
			cleanupTest();
			return false;
		}

		if (pointedAt.EditVar().ToFloat() != testFloat)
		{
			UnitTestError(testFlags, TXT("Assign Variable test failed. The pointer should be pointing at a float member variable. Dereferencing that pointer should have resulted in %s. Instead it's %s."), testFloat, pointedAt.EditVar().ToFloat());
			cleanupTest();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Pop Variables"));
	{
		//At this point, the stack should look like this:
		//Bool, Int, Float, String, Obj, Pointer[0], Pointer[1]
		size_t originalSize = 7;
		if (stack.GetStackSize() != originalSize)
		{
			UnitTestError(testFlags, TXT("Pop Variable test failed. The stack is expected to have %s items in it before running the test. Instead the stack contains %s items."), Int(originalSize), Int(stack.GetStackSize()));
			cleanupTest();
			return false;
		}

		Subroutine& popThreeVars = scope->CreateStaticFunction(HashedString("PopThreeVars"), false, true);
		popThreeVars.AddCommand(new CommandPopVars(3));
		popThreeVars.ExecuteFunction(scope, OUT callstack, OUT stack);

		size_t expected = 4;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Pop Variable test failed. After popping some variables, the size of the stack should be %s. Instead it's %s."), Int(expected), Int(stack.GetStackSize()));
			cleanupTest();
			return false;
		}

		//The variable at the top of the stack should be a string.
		ScriptVariableRef var = stack.EditVariable(0);
		if (!var.IsValid() || var.EditVar().GetVarType() != ScriptVariable::VT_String)
		{
			UnitTestError(testFlags, TXT("Pop Variable test failed. After popping some variables, the variable at the top of the stack should now be a string."));
			cleanupTest();
			return false;
		}

		//Run the command again
		popThreeVars.ExecuteFunction(scope, OUT callstack, OUT stack);
		expected = 1;
		if (stack.GetStackSize() != expected)
		{
			UnitTestError(testFlags, TXT("Pop Variable test failed. After popping some variables, the size of the stack should be %s. Instead it's %s."), Int(expected), Int(stack.GetStackSize()));
			cleanupTest();
			return false;
		}

		//Remove the last of the variables
		while (!stack.IsEmpty())
		{
			stack.PopVariable();
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Invoke Function"));
	{
		HashedString nativeSumName("NativeSum");
		Subroutine& nativeSum = scope->CreateStaticFunction(nativeSumName, true, true);
		{
			//This function simply takes the sum of two parameters and return the sum of them.
			ScriptVariable& param1 = nativeSum.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = nativeSum.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& output = nativeSum.AddOutput(HashedString("sum"));
			output.SetFixedType(ScriptVariable::VT_Int);

			nativeSum.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int param1 = outStack.GetIntParam(scope, 0, 2, false);
				Int param2 = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteIntOutput(param1 + param2, scope, 0, 1, 2, false);
			}));
		}

		HashedString incrementName("Increment");
		Subroutine& increment = scope->CreateStaticFunction(incrementName, false, false);
		{
			increment.AddCommand(new CommandPushVar(ScriptVariable::VT_Int)); //Output
			increment.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer)); //param1
			increment.AddCommand(new CommandPushVar(ScriptVariable::VT_Int)); //param2

			increment.AddCommand(new CommandAssignRef(1, CommandAssignRef::SV_VarIsOnScope, 0)); //Assign param1 to point at scope's FunctionCounter. Note that Stack::GetIntParam will automatically dereference the variable.
			increment.AddCommand(new CommandAssignInt(0, 1)); //Assign param2 to 1
			increment.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, nativeSumName.GetHash()));

			//Copy the contents of the output (which contains the results from nativeSum) to the scope's FunctionCounter
			increment.AddCommand(new CommandAssignVar(false, true, 2, 1));

			//Remove local variables
			increment.AddCommand(new CommandPopVars(3));
		}

		HashedString increment3xName("Increment3x");
		Subroutine& increment3x = scope->CreateStaticFunction(increment3xName, false, false);
		{
			//This function simply calls Increment 3 times.
			increment3x.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, incrementName));
			increment3x.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, incrementName));
			increment3x.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, incrementName));
		}

		{
			Int a = 13;
			Int b = -4;
			ScriptVariableRef outParam = stack.PushVariable();
			ScriptVariableRef param1 = stack.PushVariable();
			param1.EditVar().WriteInt(a);

			ScriptVariableRef param2 = stack.PushVariable();
			param2.EditVar().WriteInt(b);

			nativeSum.ExecuteFunction(scope, OUT callstack, OUT stack);

			if (outParam.EditVar().ToInt() != (a + b))
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. After invoking a native function that takes the sum of %s and %s, the expected output is %s. Instead it returned %s."), a, b, (a+b), outParam.EditVar().ToInt());
				cleanupTest();
				return false;
			}

			stack.PopVariable();
			stack.PopVariable();
			stack.PopVariable();
		}

		{
			ScriptVariableRef var = scope->EditGlobalVariable(functionCounterName);
			if (!var.IsValid())
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. The scope is expected to contain the global variable named %s."), functionCounterName);
				cleanupTest();
				return false;
			}

			if (var.EditVar().ToInt() != 0)
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. The %s variable is expected to initialize with 0. Instead it's initialized at %s."), functionCounterName, var.EditVar().ToInt());
				cleanupTest();
				return false;
			}

			increment.ExecuteFunction(scope, OUT callstack, OUT stack);
			Int expected = 1;
			if (var.EditVar().ToInt() != expected)
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. After running the increment function, the %s variable is expected to be equal to %s. Instead it's %s."), functionCounterName, expected, var.EditVar().ToInt());
				cleanupTest();
				return false;
			}

			if (!ContainerUtils::IsEmpty(callstack))
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. After running the increment function, the callstack is expected to be empty. All Subroutines are expected to pop their calls from the callstack. Instead the callstack contains %s elements."), Int(callstack.size()));
				cleanupTest();
				return false;
			}

			//Ensure nested Subroutines work
			increment3x.ExecuteFunction(scope, OUT callstack, OUT stack);
			expected += 3;
			if (var.EditVar().ToInt() != expected)
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. After running the 3x increment function, the %s variable is expected to be equal to %s. Instead it's %s."), functionCounterName, expected, var.EditVar().ToInt());
				cleanupTest();
				return false;
			}

			if (!ContainerUtils::IsEmpty(callstack) || stack.GetStackSize() != 0)
			{
				UnitTestError(testFlags, TXT("Invoke Function test failed. After running the 3x increment function, the callstack AND stack are expected to be empty by the time the function finishes execution. The callstack size is %s. The stack size is %s."), Int(callstack.size()), Int(stack.GetStackSize()));
				cleanupTest();
				return false;
			}
		}

		//Test a Subroutine with 2 params and 3 output
		Subroutine& uberFunction = scope->CreateStaticFunction(HashedString("UberFunction"), false, true);
		{
			ScriptVariable& param1 = uberFunction.AddParameter(HashedString("a"));
			param1.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& param2 = uberFunction.AddParameter(HashedString("b"));
			param2.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& sum = uberFunction.AddOutput(HashedString("sum"));
			sum.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& doubledSum = uberFunction.AddOutput(HashedString("doubledSum"));
			doubledSum.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& sumPlus100 = uberFunction.AddOutput(HashedString("sumPlus100"));
			sumPlus100.SetFixedType(ScriptVariable::VT_Int);

			uberFunction.AddCommand(new CommandPushVar(ScriptVariable::VT_Int)); //results for native sum
			uberFunction.AddCommand(new CommandPushVar(ScriptVariable::VT_Int)); //a
			uberFunction.AddCommand(new CommandPushVar(ScriptVariable::VT_Int)); //b

			//Stack now appears: sum, doubleSum, sumPlus100, param1, param2, resultsNative, a, b
			uberFunction.AddCommand(new CommandAssignVar(false, false, 4, 1)); //Copy param1 to a
			uberFunction.AddCommand(new CommandAssignVar(false, false, 3, 0)); //Copy param2 to b
			uberFunction.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("NativeSum")));

			//Copy nativeResults back to sum
			uberFunction.AddCommand(new CommandAssignVar(false, false, 2, 7));

			//Copy nativeResults to a AND b since we're about to run the sum function again to calculate doubling the sum
			uberFunction.AddCommand(new CommandAssignVar(false, false, 2, 1));
			uberFunction.AddCommand(new CommandAssignVar(false, false, 2, 0));

			//Run native sum again. This will combine a and b, and write the results to resultsNative.
			uberFunction.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("NativeSum")));

			//Copy the results to doubleSum
			uberFunction.AddCommand(new CommandAssignVar(false, false, 2, 6));

			//Copy the original sum back to a
			uberFunction.AddCommand(new CommandAssignVar(false, false, 7, 1));
			
			//Assign b to 100
			uberFunction.AddCommand(new CommandAssignInt(0, 100));

			//Run native sum to calculate (sum+100)
			uberFunction.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("NativeSum")));

			//Copy resultsNative to sumPlus100
			uberFunction.AddCommand(new CommandAssignVar(false, false, 2, 5));

			//Remove the 3 local variables this subroutine generated
			uberFunction.AddCommand(new CommandPopVars(3));
		}

		//Test the large function. It should be able to read 2 input parameters and write to 3 output params.
		//Output is always listed before Input in the stack.
		ScriptVariableRef sum = stack.PushVariable(HashedString("sum"));
		ScriptVariableRef doubledSum = stack.PushVariable(HashedString("doubledSum"));
		ScriptVariableRef sumPlus100 = stack.PushVariable(HashedString("sumPlus100"));

		//Assemble the input parameters
		Int inputA = 35;
		Int inputB = -14;
		ScriptVariableRef a = stack.PushVariable(HashedString("a"));
		a.EditVar().WriteInt(inputA);

		ScriptVariableRef b = stack.PushVariable(HashedString("b"));
		b.EditVar().WriteInt(inputB);

		uberFunction.ExecuteFunction(scope, OUT callstack, OUT stack);

		if (sum.EditVar().ToInt() != inputA + inputB)
		{
			UnitTestError(testFlags, TXT("Invoke Function test failed. After running a function that suppose to calculate the sum of %s and %s, the expected output is %s. Instead it generated %s."), inputA, inputB, (inputA+inputB), sum.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		Int expected = (inputA + inputB) * 2;
		if (doubledSum.EditVar().ToInt() != expected)
		{
			UnitTestError(testFlags, TXT("Invoke Function test failed. After running a function that suppose to calculate the doubled sum of %s and %s, the expected output is %s. Instead it generated %s."), inputA, inputB, expected, doubledSum.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		expected = (inputA + inputB + 100);
		if (sumPlus100.EditVar().ToInt() != expected)
		{
			UnitTestError(testFlags, TXT("Invoke Function test failed. After running a function that suppose to calculate the 100 above the sum of %s and %s, the expected output is %s. Instead it generated %s."), inputA, inputB, expected, sumPlus100.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		//Remove the five local variables this test generated
		size_t numLocalVars = 5;
		for (size_t i = 0; i < numLocalVars; ++i)
		{
			stack.PopVariable();
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Delegates"));
	{
		HashedString delegateClassName("UnitTestDelegateTester");
		ContentClass* delegateClassTester = clmEngine->CreateScriptClass(delegateClassName, HashedString());
		{
			CHECK(delegateClassTester != nullptr)
			ScriptVariable& onDelegate = delegateClassTester->CreateMemberVariable(HashedString("OnDelegate"));
			onDelegate.SetFixedType(ScriptVariable::VT_Function);
			//Although this class should define which function signature is allowed to bind to this function, this is only enforced in the editor. During runtime, the stack will be validated before invoking delegate.
			//The delegate signature is only configured through the editor. The ContentLogic module would allow any function pointers to be assigned to this. The editor's compilers are responsible for enforcing valid bindings.

			ScriptVariable& delegateCounter = delegateClassTester->CreateMemberVariable(HashedString("DelegateCounter"));
			delegateCounter.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& delegateInverseCount = delegateClassTester->CreateMemberVariable(HashedString("DelegateInverseCount"));
			delegateInverseCount.SetFixedType(ScriptVariable::VT_Int);
		
			Subroutine& handleDelegate = delegateClassTester->CreateMemberFunction(HashedString("HandleDelegate"), Subroutine::FF_Native | Subroutine::FF_Protected);
			{
				//Test output that returns the negative of countBy
				ScriptVariable& inverseCount = handleDelegate.AddOutput(HashedString("inverseCount"));
				inverseCount.SetFixedType(ScriptVariable::VT_Int);

				ScriptVariable& countBy = handleDelegate.AddParameter(HashedString("countBy"));
				countBy.SetFixedType(ScriptVariable::VT_Int);

				handleDelegate.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
				{
					ScriptVariableRef execOnVar = outStack.EditVariable(0);
					ContentObject* execOn = execOnVar.EditVar().ToObjPointer(scope);
					CHECK(execOn != nullptr && execOn->ReadMemberVariables().size() >= 3)

					Int countBy = outStack.GetIntParam(scope, 0, 1, true);
					ScriptVariableRef counter = execOn->GetMemberVariable(1);
					CHECK(counter.EditVar().GetVarType() == ScriptVariable::VT_Int)
					counter.EditVar().WriteInt(counter.EditVar().ToInt() + countBy);

					//Return the negative of countBy
					outStack.WriteIntOutput(-countBy, scope, 0, 1, 1, true);
				}));
			}

			Subroutine& execDelegate = delegateClassTester->CreateMemberFunction(HashedString("ExecDelegate"), Subroutine::FF_Protected);
			{
				ScriptVariable& countBy = execDelegate.AddParameter(HashedString("countBy"));
				countBy.SetFixedType(ScriptVariable::VT_Int);

				execDelegate.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
				execDelegate.AddCommand(new CommandAssignRef(0, 1, 0)); //Point at OnDelegate

				execDelegate.AddCommand(new CommandPushVar(ScriptVariable::VT_Int));
				execDelegate.AddCommand(new CommandPushVar(ScriptVariable::VT_Int));

				//Stack: countBy, this (delegateOwner), pointToOnDelegate, inverseCount, countBy
				execDelegate.AddCommand(new CommandAssignVar(true, true, 4, 0)); //Copy countBy to countBy.

				execDelegate.AddCommand(new CommandInvokeDelegate(2));

				execDelegate.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
				execDelegate.AddCommand(new CommandAssignRef(0, 4, 2)); //Point at delegateOwner->DelegateInverseCount
				execDelegate.AddCommand(new CommandAssignVar(true, true, 2, 0)); //Copy the output to delegate owner's DelegateInverseCount

				//Pop all local variables
				execDelegate.AddCommand(new CommandPopVars(4));
			}
		}

		ContentObject* delegateOwner = ContentObject::CreateObject();
		delegateOwner->BeginContentObject(delegateClassTester, scope, nullptr, nullptr);

		ContentObject* delegateHandler = ContentObject::CreateObject();
		delegateHandler->BeginContentObject(delegateClassTester, scope, nullptr, nullptr);

		std::function<void()> cleanupCategory([&]()
		{
			delegateOwner->Destroy();
			delegateHandler->Destroy();
			clmEngine->DestroyScriptClass(delegateClassTester, true);
		});

		//Bind the delegate
		{
			ScriptVariableRef onDelegate = delegateOwner->GetMemberVariable(0);
			if (onDelegate.EditVar().GetVarType() != ScriptVariable::VT_Function)
			{
				UnitTestError(testFlags, TXT("Delegate test failed. The %s object is expected to contain a delegate member variable at index 0."), delegateOwner->ToString());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			HashedString handlerName("HandleDelegate");
			const Subroutine* handlerFunction = delegateClassTester->FindFunction(scope, handlerName.GetHash(), delegateHandler);
			if (handlerFunction == nullptr)
			{
				UnitTestError(testFlags, TXT("Delegate test failed. Unable to find a function named %s on object instance %s."), handlerName, delegateHandler->ToString());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			onDelegate.EditVar().WriteFunction(delegateHandler, handlerFunction);
		}

		HashedString execDelegateName("ExecDelegate");
		const Subroutine* execDelegate = delegateClassTester->FindFunction(scope, execDelegateName.GetHash(), delegateOwner);
		if (execDelegate == nullptr)
		{
			UnitTestError(testFlags, TXT("Delegate test failed. Unable to find a function named %s on object instance %s."), execDelegateName, delegateOwner->ToString());
			cleanupCategory();
			cleanupTest();
			return false;
		}

		ScriptVariableRef ownerCounter = delegateOwner->GetMemberVariable(1);
		ScriptVariableRef ownerInverse = delegateOwner->GetMemberVariable(2);
		ScriptVariableRef handlerCounter = delegateHandler->GetMemberVariable(1);
		ScriptVariableRef handlerInverse = delegateHandler->GetMemberVariable(2);

		Int expectedCounter;
		Int expectedInverse;
		std::function<bool()> runTest([&]()
		{
			execDelegate->ExecuteFunction(scope, OUT callstack, OUT stack);

			//The delegate shouldn't touch these variables
			if (ownerCounter.EditVar().ToInt() != 0 || handlerInverse.EditVar().ToInt() != 0)
			{
				UnitTestError(testFlags, TXT("Delegate test failed. The %s function shouldn't have affected the owner's counter (%s) or the handler's inverse counter (%s). Both of them should have remain zero."), execDelegateName, ownerCounter.EditVar().ToInt(), handlerInverse.EditVar().ToInt());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			if (handlerCounter.EditVar().ToInt() != expectedCounter)
			{
				UnitTestError(testFlags, TXT("Delegate test failed. After invoking %s, the delegate handler's counter variable should have been %s. Instead it's %s."), execDelegateName, expectedCounter, handlerCounter.EditVar().ToInt());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			if (ownerInverse.EditVar().ToInt() != expectedInverse)
			{
				UnitTestError(testFlags, TXT("Delegate test failed. After invoking %s, the delegate owner's inverse variable should have been %s. Instead it's %s."), execDelegateName, expectedInverse, ownerInverse.EditVar().ToInt());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			return true;
		});

		//Invoke delegate with countBy = 1.
		ScriptVariableRef countByVar = stack.PushVariable(HashedString("countBy"));
		countByVar.EditVar().WriteInt(1);

		ScriptVariableRef thisVar = stack.PushVariable(HashedString("delegateOwner"));
		thisVar.EditVar().WriteObjPointer(delegateOwner);

		//Expect delegateHandler's DelegateCounter=1, and delegateOwner's DelegateInverseCount=-1
		expectedCounter = 1;
		expectedInverse = -1;
		if (!runTest())
		{
			return false;
		}

		countByVar.EditVar().WriteInt(3);
		expectedCounter = 4;
		expectedInverse = -3;
		if (!runTest())
		{
			return false;
		}

		countByVar.EditVar().WriteInt(50);
		expectedCounter = 54;
		expectedInverse = -50;
		if (!runTest())
		{
			return false;
		}

		stack.PopVariable();
		stack.PopVariable();
		cleanupCategory();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("If Conditions"));
	{
		Subroutine& ifCondition = scope->CreateStaticFunction(HashedString("IfCondition"), false, true);
		{
			ScriptVariable& innerSection = ifCondition.AddOutput(HashedString("innerSection"));
			innerSection.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& outerSection = ifCondition.AddOutput(HashedString("outerSection"));
			outerSection.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& isTrue = ifCondition.AddOutput(HashedString("isTrue"));
			isTrue.SetFixedType(ScriptVariable::VT_Bool);

			//Initialize output to 0
			ifCondition.AddCommand(new CommandAssignInt(2, 0));
			ifCondition.AddCommand(new CommandAssignInt(1, 0));
			ifCondition.AddCommand(new CommandIfCondition(0));

			ifCondition.AddCommand(new CommandBeginSection());
			{
				//Assign innerSection output to 1
				ifCondition.AddCommand(new CommandAssignInt(2, 1));
			}
			ifCondition.AddCommand(new CommandEndSection());

			//Assign outerSection output to 1
			ifCondition.AddCommand(new CommandAssignInt(1, 1));
		}

		Subroutine& ifElseCondition = scope->CreateStaticFunction(HashedString("IfElseCondition"), false, true);
		{
			ScriptVariable& ifSection = ifElseCondition.AddOutput(HashedString("ifSection"));
			ifSection.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& elseSection = ifElseCondition.AddOutput(HashedString("elseSection"));
			elseSection.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& outerSection = ifElseCondition.AddOutput(HashedString("outerSection"));
			outerSection.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& isTrue = ifElseCondition.AddOutput(HashedString("isTrue"));
			isTrue.SetFixedType(ScriptVariable::VT_Bool);

			//Initialize output to 0
			ifElseCondition.AddCommand(new CommandAssignInt(3, 0));
			ifElseCondition.AddCommand(new CommandAssignInt(2, 0));
			ifElseCondition.AddCommand(new CommandAssignInt(1, 0));
			ifElseCondition.AddCommand(new CommandIfCondition(0));

			ifElseCondition.AddCommand(new CommandBeginSection());
			{
				//Assign ifSection output to 1
				ifElseCondition.AddCommand(new CommandAssignInt(3, 1));
			}
			ifElseCondition.AddCommand(new CommandEndSection());
			ifElseCondition.AddCommand(new CommandElseClause());
			ifElseCondition.AddCommand(new CommandBeginSection());
			{
				//Assign elseSection output to 1
				ifElseCondition.AddCommand(new CommandAssignInt(2, 1));
			}
			ifElseCondition.AddCommand(new CommandEndSection());

			//Assign outerSection output to 1
			ifElseCondition.AddCommand(new CommandAssignInt(1, 1));
		}

		/*
		void NestedIfCondition (bool condition, bool innerIfCondition, bool innerElseCondition, int& ifIf, int& ifElse, int& elseif, int& elseelse)
		{
			ifif = 0;
			ifelse = 0;
			elseif = 0;
			elseelse = 0;
			if (condition)
			{
				if (innerIfCondition)
				{
					ifif = 1;
				}
				else
				{
					ifelse = 1;
				}
			}
			else
			{
				if (innerElseCondition)
				{
					elseif = 1;
				}
				else
				{
					elseelse = 1;
				}
			}
		}
		*/
		Subroutine& nestedIfCondition = scope->CreateStaticFunction(HashedString("NestedIfCondition"), false, true);
		{
			//output
			ScriptVariable& ifif = nestedIfCondition.AddOutput(HashedString("ifif"));
			ifif.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& ifelse = nestedIfCondition.AddOutput(HashedString("ifelse"));
			ifelse.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& elseif = nestedIfCondition.AddOutput(HashedString("elseif"));
			elseif.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& elseelse = nestedIfCondition.AddOutput(HashedString("elseelse"));
			elseelse.SetFixedType(ScriptVariable::VT_Int);

			//params
			ScriptVariable& condition = nestedIfCondition.AddParameter(HashedString("condition"));
			condition.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& innerIf = nestedIfCondition.AddParameter(HashedString("innerIf"));
			innerIf.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& innerElse = nestedIfCondition.AddParameter(HashedString("innerElse"));
			innerElse.SetFixedType(ScriptVariable::VT_Bool);

			//Stack:  ifif, ifelse, elseif, elseelse, condition, innerIf, innerElse
			//Init output to 0
			for (size_t i = 0; i < nestedIfCondition.ReadOutput().size(); ++i)
			{
				nestedIfCondition.AddCommand(new CommandAssignInt(i + nestedIfCondition.ReadParams().size(), 0));
			}

			nestedIfCondition.AddCommand(new CommandIfCondition(2));
			nestedIfCondition.AddCommand(new CommandBeginSection());
			{
				nestedIfCondition.AddCommand(new CommandIfCondition(1));
				nestedIfCondition.AddCommand(new CommandBeginSection());
				{
					//Assign ifif
					nestedIfCondition.AddCommand(new CommandAssignInt(6, 1));
				}
				nestedIfCondition.AddCommand(new CommandEndSection());
				nestedIfCondition.AddCommand(new CommandElseClause());
				nestedIfCondition.AddCommand(new CommandBeginSection());
				{
					//Assign ifelse
					nestedIfCondition.AddCommand(new CommandAssignInt(5, 1));
				}
				nestedIfCondition.AddCommand(new CommandEndSection());
			}
			nestedIfCondition.AddCommand(new CommandEndSection());
			nestedIfCondition.AddCommand(new CommandElseClause());
			nestedIfCondition.AddCommand(new CommandBeginSection());
			{
				nestedIfCondition.AddCommand(new CommandIfCondition(0));
				nestedIfCondition.AddCommand(new CommandBeginSection());
				{
					//Assign elseif
					nestedIfCondition.AddCommand(new CommandAssignInt(4, 1));
				}
				nestedIfCondition.AddCommand(new CommandEndSection());
				nestedIfCondition.AddCommand(new CommandElseClause());
				nestedIfCondition.AddCommand(new CommandBeginSection());
				{
					//Assign elseelse
					nestedIfCondition.AddCommand(new CommandAssignInt(3, 1));
				}
				nestedIfCondition.AddCommand(new CommandEndSection());
			}
			nestedIfCondition.AddCommand(new CommandEndSection());
		}

		ScriptVariableRef ifSection = stack.PushVariable(HashedString("ifSection"));
		ifSection.EditVar().SetFixedType(ScriptVariable::VT_Int);

		ScriptVariableRef outerSection = stack.PushVariable(HashedString("outerSection"));
		outerSection.EditVar().SetFixedType(ScriptVariable::VT_Int);

		ScriptVariableRef isTrue = stack.PushVariable(HashedString("isTrue"));
		isTrue.EditVar().SetFixedType(ScriptVariable::VT_Bool);

		isTrue.EditVar().WriteBool(false);
		Int expectedIfSection = 0;
		Int expectedOuterSection = 1;

		ifCondition.ExecuteFunction(scope, OUT callstack, OUT stack);
		if (ifSection.EditVar().ToInt() != expectedIfSection || outerSection.EditVar().ToInt() != expectedOuterSection)
		{
			UnitTestError(testFlags, TXT("If Conditions unit test failed. After invoking a subroutine with the condition equal to %s, the if section is expected to be %s (it's %s instead), and the outer section is expected to be %s (it's %s instead)."), isTrue.EditVar().ToBool(), expectedIfSection, ifSection.EditVar().ToInt(), expectedOuterSection, outerSection.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		isTrue.EditVar().WriteBool(true);
		expectedIfSection = 1;
		expectedOuterSection = 1;

		ifCondition.ExecuteFunction(scope, OUT callstack, OUT stack);
		if (ifSection.EditVar().ToInt() != expectedIfSection || outerSection.EditVar().ToInt() != expectedOuterSection)
		{
			UnitTestError(testFlags, TXT("If Conditions unit test failed. After invoking a subroutine with the condition equal to %s, the if section is expected to be %s (it's %s instead), and the outer section is expected to be %s (it's %s instead)."), isTrue.EditVar().ToBool(), expectedIfSection, ifSection.EditVar().ToInt(), expectedOuterSection, outerSection.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		//Need to insert an else section in the middle of the stack for the next test.
		stack.PopVariable(); //isTrue param
		stack.PopVariable(); //isOuter section

		ScriptVariableRef elseSection = stack.PushVariable(HashedString("elseSection"));
		elseSection.EditVar().SetFixedType(ScriptVariable::VT_Int);

		outerSection = stack.PushVariable(HashedString("outerSection"));
		outerSection.EditVar().SetFixedType(ScriptVariable::VT_Int);

		isTrue = stack.PushVariable(HashedString("isTrue"));
		isTrue.EditVar().SetFixedType(ScriptVariable::VT_Bool);

		isTrue.EditVar().WriteBool(false);
		expectedIfSection = 0;
		Int expectedElseSection = 1;
		expectedOuterSection = 1;

		ifElseCondition.ExecuteFunction(scope, OUT callstack, OUT stack);
		if (ifSection.EditVar().ToInt() != expectedIfSection || elseSection.EditVar().ToInt() != expectedElseSection || outerSection.EditVar().ToInt() != expectedOuterSection)
		{
			UnitTestError(testFlags, TXT("If Conditions unit test failed. After invoking a subroutine with the condition equal to %s, the if section is expected to be %s (it's %s instead), the else section is expected to be %s (it's %s instead), and the outer section is expected to be %s (it's %s instead)."), isTrue.EditVar().ToBool(), expectedIfSection, ifSection.EditVar().ToInt(), expectedElseSection, elseSection.EditVar().ToInt(), expectedOuterSection, outerSection.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		isTrue.EditVar().WriteBool(true);
		expectedIfSection = 1;
		expectedElseSection = 0;

		ifElseCondition.ExecuteFunction(scope, OUT callstack, OUT stack);
		if (ifSection.EditVar().ToInt() != expectedIfSection || elseSection.EditVar().ToInt() != expectedElseSection || outerSection.EditVar().ToInt() != expectedOuterSection)
		{
			UnitTestError(testFlags, TXT("If Conditions unit test failed. After invoking a subroutine with the condition equal to %s, the if section is expected to be %s (it's %s instead), the else section is expected to be %s (it's %s instead), and the outer section is expected to be %s (it's %s instead)."), isTrue.EditVar().ToBool(), expectedIfSection, ifSection.EditVar().ToInt(), expectedElseSection, elseSection.EditVar().ToInt(), expectedOuterSection, outerSection.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		while (!stack.IsEmpty())
		{
			stack.PopVariable();
		}

		//Run nested condition tests
		{
			ScriptVariableRef ifIf = stack.PushVariable(HashedString("ifIf"));
			ScriptVariableRef ifElse = stack.PushVariable(HashedString("ifElse"));
			ScriptVariableRef elseIf = stack.PushVariable(HashedString("elseIf"));
			ScriptVariableRef elseElse = stack.PushVariable(HashedString("elseElse"));
			ScriptVariableRef conditional = stack.PushVariable();
			ScriptVariableRef innerIf = stack.PushVariable();
			ScriptVariableRef innerElse = stack.PushVariable();

			ifIf.EditVar().SetFixedType(ScriptVariable::VT_Int);
			ifElse.EditVar().SetFixedType(ScriptVariable::VT_Int);
			elseIf.EditVar().SetFixedType(ScriptVariable::VT_Int);
			elseElse.EditVar().SetFixedType(ScriptVariable::VT_Int);

			conditional.EditVar().SetFixedType(ScriptVariable::VT_Bool);
			innerIf.EditVar().SetFixedType(ScriptVariable::VT_Bool);
			innerElse.EditVar().SetFixedType(ScriptVariable::VT_Bool);

			Int expectedIfIf;
			Int expectedIfElse;
			Int expectedElseIf;
			Int expectedElseElse;

			std::function<DString()> paramToString([&]()
			{
				return DString::CreateFormattedString(TXT("conditional=%s, innerIf=%s, innerElse=%s"), conditional.EditVar().ToBool(), innerIf.EditVar().ToBool(), innerElse.EditVar().ToBool());
			});

			std::function<bool()> testNestedConditions([&]()
			{
				nestedIfCondition.ExecuteFunction(scope, OUT callstack, OUT stack);

				const DString errorMsg = TXT("Nested If Conditions unit test failed. The nested conditional results with params (%s) does not meet the actual. The %s output should have been %s. Instead it's %s.");

				Int actual = ifIf.EditVar().ToInt();
				if (actual != expectedIfIf)
				{
					UnitTestError(testFlags, errorMsg, paramToString(), ifIf.EditVar().ReadVariableName(), expectedIfIf, actual);
					cleanupTest();
					return false;
				}

				actual = ifElse.EditVar().ToInt();
				if (actual != expectedIfElse)
				{
					UnitTestError(testFlags, errorMsg, paramToString(), ifElse.EditVar().ReadVariableName(), expectedIfElse, actual);
					cleanupTest();
					return false;
				}

				actual = elseIf.EditVar().ToInt();
				if (actual != expectedElseIf)
				{
					UnitTestError(testFlags, errorMsg, paramToString(), elseIf.EditVar().ReadVariableName(), expectedElseIf, actual);
					cleanupTest();
					return false;
				}

				actual = elseElse.EditVar().ToInt();
				if (actual != expectedElseElse)
				{
					UnitTestError(testFlags, errorMsg, paramToString(), elseElse.EditVar().ReadVariableName(), expectedElseElse, actual);
					cleanupTest();
					return false;
				}

				return true;
			});

			conditional.EditVar().WriteBool(false);
			innerIf.EditVar().WriteBool(false);
			innerElse.EditVar().WriteBool(false);
			expectedIfIf = 0;
			expectedIfElse = 0;
			expectedElseIf = 0;
			expectedElseElse = 1;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(true);
			innerIf.EditVar().WriteBool(false);
			innerElse.EditVar().WriteBool(false);
			expectedIfIf = 0;
			expectedIfElse = 1;
			expectedElseIf = 0;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(false);
			innerIf.EditVar().WriteBool(true);
			innerElse.EditVar().WriteBool(false);
			expectedIfIf = 0;
			expectedIfElse = 0;
			expectedElseIf = 0;
			expectedElseElse = 1;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(true);
			innerIf.EditVar().WriteBool(true);
			innerElse.EditVar().WriteBool(false);
			expectedIfIf = 1;
			expectedIfElse = 0;
			expectedElseIf = 0;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(false);
			innerIf.EditVar().WriteBool(false);
			innerElse.EditVar().WriteBool(true);
			expectedIfIf = 0;
			expectedIfElse = 0;
			expectedElseIf = 1;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(true);
			innerIf.EditVar().WriteBool(false);
			innerElse.EditVar().WriteBool(true);
			expectedIfIf = 0;
			expectedIfElse = 1;
			expectedElseIf = 0;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(false);
			innerIf.EditVar().WriteBool(true);
			innerElse.EditVar().WriteBool(true);
			expectedIfIf = 0;
			expectedIfElse = 0;
			expectedElseIf = 1;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			conditional.EditVar().WriteBool(true);
			innerIf.EditVar().WriteBool(true);
			innerElse.EditVar().WriteBool(true);
			expectedIfIf = 1;
			expectedIfElse = 0;
			expectedElseIf = 0;
			expectedElseElse = 0;
			if (!testNestedConditions())
			{
				return false;
			}

			while (!stack.IsEmpty())
			{
				stack.PopVariable();
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("While Loops"));
	{
		//Returns true if the given int is beyond the string's num characters.
		Subroutine& isAtEndOfStr = scope->CreateStaticFunction(HashedString("IsAtEndOfStr"), true, true);
		{
			ScriptVariable& result = isAtEndOfStr.AddOutput(HashedString("result"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& str = isAtEndOfStr.AddParameter(HashedString("str"));
			str.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& idx = isAtEndOfStr.AddParameter(HashedString("idx"));
			idx.SetFixedType(ScriptVariable::VT_Int);

			isAtEndOfStr.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString str = outStack.GetStringParam(scope, 0, 2, false);
				Int idx = outStack.GetIntParam(scope, 1, 2, false);
				outStack.WriteBoolOutput(idx < str.Length(), scope, 0, 1, 2, false);
			}));
		}

		//Returns true if the character at the specified index is a vowel.
		Subroutine& isVowel = scope->CreateStaticFunction(HashedString("IsVowel"), true, true);
		{
			ScriptVariable& result = isVowel.AddOutput(HashedString("result"));
			result.SetFixedType(ScriptVariable::VT_Bool);

			ScriptVariable& str = isVowel.AddParameter(HashedString("str"));
			str.SetFixedType(ScriptVariable::VT_String);

			ScriptVariable& idx = isVowel.AddParameter(HashedString("idx"));
			idx.SetFixedType(ScriptVariable::VT_Int);

			isVowel.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				DString str = outStack.GetStringParam(scope, 0, 2, false);
				Int idx = outStack.GetIntParam(scope, 1, 2, false);
				CHECK(idx < str.Length())

				str.ToUpper();
				TStringChar testChar = str.At(idx);
				bool isVowel = (testChar == 'A' || testChar == 'E' || testChar == 'I' || testChar == 'O' || testChar == 'U' || testChar == 'Y');

				outStack.WriteBoolOutput(isVowel, scope, 0, 1, 2, false);
			}));
		}

		//Increments the specified int reference. Not calling Int+Int subroutine here since that function isn't tested yet.
		Subroutine& incrementInt = scope->CreateStaticFunction(HashedString("IncrementInt"), true, true);
		{
			ScriptVariable& output = incrementInt.AddParameter(HashedString("output"));
			output.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& intParam = incrementInt.AddParameter(HashedString("intParam"));
			intParam.SetFixedType(ScriptVariable::VT_Int);

			incrementInt.AddCommand(new CommandNative([](ScriptScope* scope, ClmStack& outStack)
			{
				Int input = outStack.GetIntParam(scope, 0, 1, false);
				++input;
				outStack.WriteIntOutput(input, scope, 0, 1, 1, false);
			}));
		}

		Subroutine& countNumVowels = scope->CreateStaticFunction(HashedString("CountNumVowels"), false, true);
		{
			ScriptVariable& numVowels = countNumVowels.AddOutput(HashedString("numVowels"));
			numVowels.SetFixedType(ScriptVariable::VT_Int);

			ScriptVariable& str = countNumVowels.AddParameter(HashedString("string"));
			str.SetFixedType(ScriptVariable::VT_String);

			//Initialize output to zero
			countNumVowels.AddCommand(new CommandAssignInt(1, 0));

			//Stack: numVowels, string, bool (whileCondition), string (pointAtSring), int (idx)
			countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Bool));
			countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			countNumVowels.AddCommand(new CommandAssignRef(0, 2));
			countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Int));
			countNumVowels.AddCommand(new CommandAssignInt(0, 0));
			countNumVowels.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("IsAtEndOfStr")));
			countNumVowels.AddCommand(new CommandWhileLoop(2));
			countNumVowels.AddCommand(new CommandBeginSection());
			{
				//Stack: numVowels, string, bool (whileCondition), string (pointAtString), int (idx), bool (isVowel), string (pointAtString), int (pointAtIdx)
				countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Bool));
				countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
				countNumVowels.AddCommand(new CommandAssignRef(0, 3));
				countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
				countNumVowels.AddCommand(new CommandAssignRef(0, 3));
				countNumVowels.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("IsVowel")));
				countNumVowels.AddCommand(new CommandPopVars(2));
				countNumVowels.AddCommand(new CommandIfCondition(0));
				countNumVowels.AddCommand(new CommandBeginSection());
				{
					//Stack: numVowels, string, bool (whileCondition), string (pointAtString), int (idx), bool (isVowel), int (pointAt numVowels), int (pointAt numVowels)
					countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
					countNumVowels.AddCommand(new CommandAssignRef(0, 6));
					countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
					countNumVowels.AddCommand(new CommandAssignRef(0, 7));

					//Increment numVowels
					countNumVowels.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("IncrementInt")));
					countNumVowels.AddCommand(new CommandPopVars(2));
				}
				countNumVowels.AddCommand(new CommandEndSection());
				countNumVowels.AddCommand(new CommandPopVars(1)); //Remove if condition bool (isVowel)

				//Increment idx. Idx is already at the end of the stack. Simply add one more int for the input, pointing at output.
				countNumVowels.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
				countNumVowels.AddCommand(new CommandAssignRef(0, 1));
				countNumVowels.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("IncrementInt")));
				countNumVowels.AddCommand(new CommandPopVars(1));

				//Update whileCondition. The stack already ends with bool, string, int
				countNumVowels.AddCommand(new CommandInvokeSubroutine(Subroutine::FF_Static, HashedString("IsAtEndOfStr")));
			}
			countNumVowels.AddCommand(new CommandEndSection());
			countNumVowels.AddCommand(new CommandPopVars(3));
		}

		DString testStr;
		Int expected;
		std::function<bool()> testWhileLoop([&]()
		{
			ScriptVariableRef output = stack.PushVariable(HashedString("output"));
			ScriptVariableRef input = stack.PushVariable(HashedString("input"));
			input.EditVar().WriteDString(testStr);
			countNumVowels.ExecuteFunction(scope, OUT callstack, OUT stack);

			Int actual = output.EditVar().ToInt();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("While Loop test failed. The function that counts the number of vowels in \"%s\" returned %s. It should have returned %s instead."), testStr, actual, expected);
				cleanupTest();
				return false;
			}

			stack.PopVariable();
			stack.PopVariable();

			if (!stack.IsEmpty())
			{
				//The countNumVowels function has a few pop variable commands at the end after the while loop. If this hits, it's very possible that aboring the while loop skipped a few commands.
				UnitTestError(testFlags, TXT("While Loop test failed. The function that counts the number of vowels in \"%s\" did not clear the stack. It's possible that the function did not terminate correctly after leaving the while loop."), testStr);
				cleanupTest();
				return false;
			}

			return true;
		});

		testStr = TXT("ContentLogic");
		expected = 4;
		if (!testWhileLoop())
		{
			return false;
		}

		testStr = TXT("pfff");
		expected = 0;
		if (!testWhileLoop())
		{
			return false;
		}

		testStr = TXT("Sand Dune");
		expected = 3;
		if (!testWhileLoop())
		{
			return false;
		}

		testStr = DString::EmptyString;
		expected = 0;
		if (!testWhileLoop())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	cleanupTest();
	ExecuteSuccessSequence(testFlags, TXT("Commands"));
	return true;
}

bool ContentLogicUnitTester::TestPolymorphism (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Polymorphism"));

	ScriptScope* scope = ScriptScope::CreateObject();

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)

	/*
	Create the following class tree:
	Animal
		Dog
			Corgi
		Bird
			Penguin
		Cow
	*/
	ContentClass* animalClass = clmEngine->CreateScriptClass(HashedString("UnitTestAnimal"), HashedString());
	{
		ScriptVariable& name = animalClass->CreateMemberVariable(HashedString("Name"));
		name.SetFixedType(ScriptVariable::VT_String);
		name.WriteDString(TXT("Animal"));

		ScriptVariable& movementSpeed = animalClass->CreateMemberVariable(HashedString("MovementSpeed"));
		movementSpeed.SetFixedType(ScriptVariable::VT_Float);

		ScriptVariable& isInitialized = animalClass->CreateMemberVariable(HashedString("IsInitialized"));
		isInitialized.SetFixedType(ScriptVariable::VT_Bool);
		isInitialized.WriteBool(false);

		Subroutine& makeNoise = animalClass->CreateMemberFunction(HashedString("MakeNoise"), Subroutine::FF_Const);
		{
			ScriptVariable& noise = makeNoise.AddOutput(HashedString("noise"));
			noise.SetFixedType(ScriptVariable::VT_String);

			makeNoise.AddCommand(new CommandAssignString(1, TXT("<silent>")));
		}

		Subroutine& initialize = animalClass->CreateMemberFunction(HashedString("Initialize"), Subroutine::FF_None);
		{
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			initialize.AddCommand(new CommandAssignRef(0, 1, 1)); //Assign pointer to point at this->MovementSpeed
			initialize.AddCommand(new CommandAssignFloat(0, 100.f)); //Set MovementSpeed to 100
			initialize.AddCommand(new CommandAssignRef(0, 1, 2)); //Assign pointer to point at this->IsInitialized
			initialize.AddCommand(new CommandAssignBool(0, true));
			initialize.AddCommand(new CommandPopVars(1));
		}
	}

	ContentClass* dogClass = clmEngine->CreateScriptClass(HashedString("UnitTestDog"), HashedString("UnitTestAnimal"));
	{
		ScriptVariable& nameVar = dogClass->EditDefaultOverrides().emplace_back(HashedString("Name"));
		nameVar.WriteDString(TXT("Dog"));

		ScriptVariable& favoriteTreat = dogClass->CreateMemberVariable(HashedString("FavoriteTreat"));
		favoriteTreat.SetFixedType(ScriptVariable::VT_String);
		favoriteTreat.WriteDString(TXT("Biscuits"));

		Subroutine& makeNoise = dogClass->CreateMemberFunction(HashedString("MakeNoise"), Subroutine::FF_Const);
		{
			ScriptVariable& noise = makeNoise.AddOutput(HashedString("noise"));
			noise.SetFixedType(ScriptVariable::VT_String);

			makeNoise.AddCommand(new CommandAssignString(1, TXT("Woof")));
		}

		Subroutine& initialize = dogClass->CreateMemberFunction(HashedString("Initialize"), Subroutine::FF_None);
		{
			initialize.AddCommand(new CommandInvokeSuper());
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			initialize.AddCommand(new CommandAssignRef(0, 1, 1)); //Assign pointer to point at this->MovementSpeed
			initialize.AddCommand(new CommandAssignFloat(0, 250.f)); //Set MovementSpeed to 250
			initialize.AddCommand(new CommandPopVars(1));
		}
	}

	ContentClass* corgiClass = clmEngine->CreateScriptClass(HashedString("UnitTestCorgi"), HashedString("UnitTestDog"));
	{
		ScriptVariable& nameVar = corgiClass->EditDefaultOverrides().emplace_back(HashedString("Name"));
		nameVar.WriteDString(TXT("Corgi"));

		Subroutine& initialize = corgiClass->CreateMemberFunction(HashedString("Initialize"), Subroutine::FF_None);
		{
			initialize.AddCommand(new CommandInvokeSuper());
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			initialize.AddCommand(new CommandAssignRef(0, 1, 1)); //Assign pointer to point at this->MovementSpeed
			initialize.AddCommand(new CommandAssignFloat(0, 125.f)); //Set MovementSpeed to 125
			initialize.AddCommand(new CommandPopVars(1));
		}
	}

	ContentClass* birdClass = clmEngine->CreateScriptClass(HashedString("UnitTestBird"), HashedString("UnitTestAnimal"));
	{
		ScriptVariable& nameVar = birdClass->EditDefaultOverrides().emplace_back(HashedString("Name"));
		nameVar.WriteDString(TXT("Bird"));

		ScriptVariable& canFly = birdClass->CreateMemberVariable(HashedString("CanFly"));
		canFly.SetFixedType(ScriptVariable::VT_Bool);
		canFly.WriteBool(true);

		Subroutine& initialize = birdClass->CreateMemberFunction(HashedString("Initialize"), Subroutine::FF_None);
		{
			initialize.AddCommand(new CommandInvokeSuper());
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));

			//Stack should look like this: this, Pointer[Movement], Pointer[CanFly]
			//Object variable list should look like this: Name, MovementSpeed, IsInitialized, CanFly
			initialize.AddCommand(new CommandAssignRef(1, 2, 1)); //Assign pointer to point at this->MovementSpeed
			initialize.AddCommand(new CommandAssignRef(0, 2, 3)); //Assign pointer to point at this->CanFly
			initialize.AddCommand(new CommandIfCondition(0));
			{
				initialize.AddCommand(new CommandBeginSection());
				initialize.AddCommand(new CommandAssignFloat(1, 5000.f));
				initialize.AddCommand(new CommandEndSection());
			}
			initialize.AddCommand(new CommandElseClause());
			{
				initialize.AddCommand(new CommandBeginSection());
				initialize.AddCommand(new CommandAssignFloat(1, 200.f));
				initialize.AddCommand(new CommandEndSection());
			}

			initialize.AddCommand(new CommandPopVars(2));
		}

		Subroutine& makeNoise = birdClass->CreateMemberFunction(HashedString("MakeNoise"), Subroutine::FF_Const);
		{
			ScriptVariable& noise = makeNoise.AddOutput(HashedString("noise"));
			noise.SetFixedType(ScriptVariable::VT_String);

			makeNoise.AddCommand(new CommandAssignString(1, TXT("Tweet")));
		}
	}

	ContentClass* penguinClass = clmEngine->CreateScriptClass(HashedString("UnitTestPenguin"), HashedString("UnitTestBird"));
	{
		ScriptVariable& nameVar = penguinClass->EditDefaultOverrides().emplace_back(HashedString("Name"));
		nameVar.WriteDString(TXT("Penguin"));

		ScriptVariable& canFlyVar = penguinClass->EditDefaultOverrides().emplace_back(HashedString("CanFly"));
		canFlyVar.WriteBool(false);

		Subroutine& makeNoise = penguinClass->CreateMemberFunction(HashedString("MakeNoise"), Subroutine::FF_Const);
		{
			ScriptVariable& noise = makeNoise.AddOutput(HashedString("noise"));
			noise.SetFixedType(ScriptVariable::VT_String);

			makeNoise.AddCommand(new CommandAssignString(1, TXT("eeeeheheh")));
		}
	}

	ContentClass* cowClass = clmEngine->CreateScriptClass(HashedString("UnitTestCow"), HashedString("UnitTestAnimal"));
	{
		ScriptVariable& nameVar = cowClass->EditDefaultOverrides().emplace_back(HashedString("Name"));
		nameVar.WriteDString(TXT("Cow"));

		Subroutine& initialize = cowClass->CreateMemberFunction(HashedString("Initialize"), Subroutine::FF_None);
		{
			initialize.AddCommand(new CommandInvokeSuper());
			initialize.AddCommand(new CommandPushVar(ScriptVariable::VT_Pointer));
			initialize.AddCommand(new CommandAssignRef(0, 1, 1)); //Assign pointer to point at this->MovementSpeed
			initialize.AddCommand(new CommandAssignFloat(0, 50.f)); //Set MovementSpeed to 50
			initialize.AddCommand(new CommandPopVars(1));
		}

		Subroutine& makeNoise = cowClass->CreateMemberFunction(HashedString("MakeNoise"), Subroutine::FF_Const);
		{
			ScriptVariable& noise = makeNoise.AddOutput(HashedString("noise"));
			noise.SetFixedType(ScriptVariable::VT_String);

			makeNoise.AddCommand(new CommandAssignString(1, TXT("Moo")));
		}
	}

	std::vector<DString> callstack;
	ClmStack stack;

	ContentObject* animal = ContentObject::CreateObject();
	animal->BeginContentObject(animalClass, scope, nullptr, nullptr);
	ContentObject* doggo = ContentObject::CreateObject();
	doggo->BeginContentObject(dogClass, scope, nullptr, nullptr);
	ContentObject* corgi = ContentObject::CreateObject();
	corgi->BeginContentObject(corgiClass, scope, nullptr, nullptr);
	ContentObject* finch = ContentObject::CreateObject();
	{
		finch->BeginContentObject(birdClass, scope, nullptr, nullptr);
		ScriptVariableRef name = finch->GetMemberVariable(0);
		name.EditVar().WriteDString(TXT("Finch"));
	}

	ContentObject* skipper = ContentObject::CreateObject();
	{
		skipper->BeginContentObject(penguinClass, scope, nullptr, nullptr);
		ScriptVariableRef name = skipper->GetMemberVariable(0);
		name.EditVar().WriteDString(TXT("Skipper"));
	}

	ContentObject* kowalski = ContentObject::CreateObject();
	{
		kowalski->BeginContentObject(penguinClass, scope, nullptr, nullptr);
		ScriptVariableRef name = kowalski->GetMemberVariable(0);
		name.EditVar().WriteDString(TXT("Kowalski"));
	}
	
	ContentObject* rico = ContentObject::CreateObject();
	{
		rico->BeginContentObject(penguinClass, scope, nullptr, nullptr);
		ScriptVariableRef name = rico->GetMemberVariable(0);
		name.EditVar().WriteDString(TXT("Rico"));
	}
	
	ContentObject* cow = ContentObject::CreateObject();
	cow->BeginContentObject(cowClass, scope, nullptr, nullptr);

	std::function<void()> cleanupTest([&]()
	{
		animal->Destroy();
		doggo->Destroy();
		corgi->Destroy();
		finch->Destroy();
		skipper->Destroy();
		kowalski->Destroy();
		rico->Destroy();
		cow->Destroy();
		clmEngine->DestroyScriptClass(animalClass, true);
		scope->Destroy();
	});

	SetTestCategory(testFlags, TXT("Object Defaults"));
	{
		DString expected;
		ContentObject* testObj;

		std::function<bool()> testName([&]()
		{
			size_t memVarIdx = 0;
			ScriptVariableRef var = testObj->GetMemberVariable(memVarIdx);
			if (!var.IsValid() || var.EditVar().GetVarType() != ScriptVariable::VT_String)
			{
				UnitTestError(testFlags, TXT("Polymorphism test failed. The object is expected to have a name variable at index %s."), Int(memVarIdx));
				cleanupTest();
				return false;
			}

			DString actualName = var.EditVar().ToDString();
			if (actualName.Compare(expected, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("Polymorphism test failed. The object is expected to have a name variable at index %s is expected to be equal to \"%s\". Instead it's \"%s\"."), Int(memVarIdx), expected, actualName);
				cleanupTest();
				return false;
			}

			return true;
		});
		
		testObj = animal;
		expected = TXT("Animal");
		if (!testName())
		{
			return false;
		}

		testObj = doggo;
		expected = TXT("Dog");
		if (!testName())
		{
			return false;
		}

		testObj = corgi;
		expected = TXT("Corgi");
		if (!testName())
		{
			return false;
		}

		testObj = finch;
		expected = TXT("Finch");
		if (!testName())
		{
			return false;
		}

		testObj = skipper;
		expected = TXT("Skipper");
		if (!testName())
		{
			return false;
		}

		testObj = kowalski;
		expected = TXT("Kowalski");
		if (!testName())
		{
			return false;
		}

		testObj = rico;
		expected = TXT("Rico");
		if (!testName())
		{
			return false;
		}

		testObj = cow;
		expected = TXT("Cow");
		if (!testName())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	std::function<DString(ContentObject*)> objToString([](ContentObject* obj) -> DString
	{
		CHECK(obj != nullptr)
		ScriptVariableRef var = obj->GetMemberVariable(0);
		if (var.IsValid() && var.EditVar().GetVarType() == ScriptVariable::VT_String)
		{
			return var.EditVar().ToDString();
		}

		return TXT("<Unknown Object>");
	});

	SetTestCategory(testFlags, TXT("Virtual Functions"));
	{
		ContentObject* testObj;
		HashedString functionName("Initialize");
		Float expectedMovement;
		std::function<bool()> testInit([&]()
		{
			//Ensure the object is not already initialized
			size_t memVarIdx = 2;
			ScriptVariableRef initVar = testObj->GetMemberVariable(memVarIdx);
			if (!initVar.IsValid() || initVar.EditVar().GetVarType() != ScriptVariable::VT_Bool)
			{
				DString varName = initVar.IsValid() ? initVar.EditVar().ReadVariableName().ToString() : TXT("NULL");
				UnitTestError(testFlags, TXT("Virtual Function test failed. The %s is expected to have an init member variable at index %s. Instead %s is found."), objToString(testObj), Int(memVarIdx), varName);
				cleanupTest();
				return false;
			}

			if (initVar.EditVar().ToBool())
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. The %s is expected to initialize %s to false."), objToString(testObj), initVar.EditVar().ReadVariableName());
				cleanupTest();
				return false;
			}

			const Subroutine* function = animalClass->FindFunction(scope, functionName.GetHash(), testObj);
			if (function == nullptr)
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. The %s is unable to find the %s function."), objToString(testObj), functionName);
				cleanupTest();
				return false;
			}

			//Push testObj at the end of the stack to act like "ExecuteOn"
			ScriptVariableRef thisVar = stack.PushVariable();
			thisVar.EditVar().SetFixedType(ScriptVariable::VT_Obj);
			thisVar.EditVar().WriteObjPointer(testObj);

			function->ExecuteFunction(scope, OUT callstack, OUT stack);

			stack.PopVariable();

			if (!initVar.EditVar().ToBool())
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. %s is expected to set its %s variable to true after calling %s."), objToString(testObj), initVar.EditVar().ReadVariableName(), functionName);
				cleanupTest();
				return false;
			}

			//Verify Movement speed

			memVarIdx = 1;
			ScriptVariableRef movementVar = testObj->GetMemberVariable(memVarIdx);
			if (!movementVar.IsValid() || movementVar.EditVar().GetVarType() != ScriptVariable::VT_Float)
			{
				DString varName = movementVar.IsValid() ? movementVar.EditVar().ReadVariableName().ToString() : TXT("NULL");
				UnitTestError(testFlags, TXT("Virtual Function test failed. %s is expected to contain a MovementSpeed variable at index %s. Instead it found %s."), objToString(testObj), Int(memVarIdx), varName);
				cleanupTest();
				return false;
			}

			if (!movementVar.EditVar().ToFloat().IsCloseTo(expectedMovement))
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. %s is expected to initialize %s to %s after invoking %s. Instead it initialized to %s."), objToString(testObj), movementVar.EditVar().ReadVariableName(), expectedMovement, functionName, movementVar.EditVar().ToFloat());
				cleanupTest();
				return false;
			}

			return true;
		});

		testObj = animal;
		expectedMovement = 100.f;
		if (!testInit())
		{
			return false;
		}

		testObj = doggo;
		expectedMovement = 250.f;
		if (!testInit())
		{
			return false;
		}

		testObj = corgi;
		expectedMovement = 125.f;
		if (!testInit())
		{
			return false;
		}

		testObj = finch;
		expectedMovement = 5000.f;
		if (!testInit())
		{
			return false;
		}

		testObj = skipper;
		expectedMovement = 200.f;
		if (!testInit())
		{
			return false;
		}

		testObj = kowalski;
		if (!testInit())
		{
			return false;
		}

		testObj = rico;
		if (!testInit())
		{
			return false;
		}

		testObj = cow;
		expectedMovement = 50.f;
		if (!testInit())
		{
			return false;
		}

		DString expectedNoise;
		std::function<bool()> testNoise([&]()
		{
			const ContentClass* curClass = testObj->GetClass();
			CHECK(curClass != nullptr)
			HashedString noiseFunctionName("MakeNoise");
			const Subroutine* noiseFunction = curClass->FindFunction(scope, noiseFunctionName.GetHash(), testObj);
			if (noiseFunction == nullptr)
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. The %s object is expected to contain a %s function."), objToString(testObj), noiseFunctionName);
				cleanupTest();
				return false;
			}

			ScriptVariableRef noiseVar = stack.PushVariable();
			noiseVar.EditVar().SetFixedType(ScriptVariable::VT_String);

			ScriptVariableRef thisVar = stack.PushVariable();
			thisVar.EditVar().WriteObjPointer(testObj);

			noiseFunction->ExecuteFunction(scope, OUT callstack, OUT stack);

			DString actualNoise = noiseVar.EditVar().ToDString();
			stack.PopVariable();
			stack.PopVariable();

			if (actualNoise.Compare(expectedNoise, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("Virtual Function test failed. %s is expected to make a \"%s\" noise. Instead it made a \"%s\" noise."), objToString(testObj), actualNoise, expectedNoise);
				cleanupTest();
				return false;
			}

			return true;
		});

		testObj = doggo;
		expectedNoise = TXT("Woof");
		if (!testNoise())
		{
			return false;
		}

		testObj = corgi;
		if (!testNoise())
		{
			return false;
		}

		testObj = finch;
		expectedNoise = TXT("Tweet");
		if (!testNoise())
		{
			return false;
		}

		testObj = cow;
		expectedNoise = TXT("Moo");
		if (!testNoise())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Member Variables"));
	{
		//General member variable instances are already tested with Name. This test will test against member variables residing in subclasses.

		size_t treatIdx = 3;
		ScriptVariableRef dogTreat = doggo->GetMemberVariable(treatIdx);
		ScriptVariableRef corgiTreat = corgi->GetMemberVariable(treatIdx);
		if (!dogTreat.IsValid() || !corgiTreat.IsValid())
		{
			UnitTestError(testFlags, TXT("Member Variables test failed. The %s and %s are expected to contain a treat member variable at index %s."), objToString(doggo), objToString(corgi), Int(treatIdx));
			cleanupTest();
			return false;
		}

		if (dogTreat.EditVar().GetVarType() != ScriptVariable::VT_String || corgiTreat.EditVar().GetVarType() != ScriptVariable::VT_String)
		{
			UnitTestError(testFlags, TXT("Member Variables test failed. The %s and %s %s variables are expected to be a string type."), objToString(doggo), objToString(corgi), dogTreat.EditVar().ReadVariableName());
			cleanupTest();
			return false;
		}

		//Assign the variables. They should be independent from each other. Assigning in one instance should not affect the other.
		DString expectedDogTreat = TXT("Beef");
		DString expectedCorgiTreat = TXT("Peanut Butter");
		dogTreat.EditVar().WriteDString(expectedDogTreat);
		corgiTreat.EditVar().WriteDString(expectedCorgiTreat);
		if (dogTreat.EditVar().ToDString().Compare(expectedDogTreat, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Member Variables test failed. Failed to write \"%s\" in the %s's member variable %s. Instead the variable is \"%s\"."), expectedDogTreat, objToString(doggo), dogTreat.EditVar().ReadVariableName(), dogTreat.EditVar().ToDString());
			cleanupTest();
			return false;
		}

		if (corgiTreat.EditVar().ToDString().Compare(expectedCorgiTreat, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Member Variables test failed. Failed to write \"%s\" in the %s's member variable %s. Instead the variable is \"%s\"."), expectedCorgiTreat, objToString(corgi), corgiTreat.EditVar().ReadVariableName(), corgiTreat.EditVar().ToDString());
			cleanupTest();
			return false;
		}

		//The treat variable should not exist for cow.
		ScriptVariableRef cowTreat = cow->GetMemberVariable(treatIdx);
		if (cowTreat.IsValid())
		{
			UnitTestError(testFlags, TXT("Member Variables test failed. The %s is not expected to have a treat variable. Instead it found %s at variable idx %s."), objToString(cow), cowTreat.EditVar().ReadVariableName(), Int(treatIdx));
			cleanupTest();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	cleanupTest();
	ExecuteSuccessSequence(testFlags, TXT("Polymorphism"));
	return true;
}

bool ContentLogicUnitTester::TestScriptComponents (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Script Components"));

	ScriptScope* scope = ScriptScope::CreateObject();
	Entity* testEntity = Entity::CreateObject();
	ScriptComponentTest* scriptComp = ScriptComponentTest::CreateObject();
	if (testEntity->AddComponent(scriptComp))
	{
		scriptComp->SetScope(scope);
	}

	std::function<void()> cleanupTest([&]()
	{
		testEntity->Destroy();
		scope->Destroy();
	});

	//Test if the ScriptComponent created the correct content object instance
	SetTestCategory(testFlags, TXT("Object Instantiation"));
	{
		ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
		CHECK(clmEngine != nullptr)
		HashedString expectedClassName("UnitTestScriptComp");
		if (!clmEngine->ReadScriptClasses().contains(expectedClassName.GetHash()))
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The ClmEngineComponent is expected to contain a script class named %s."), expectedClassName);
			cleanupTest();
			return false;
		}

		const ContentClass* expectedClass = clmEngine->ReadScriptClasses().at(expectedClassName.GetHash());
		if (expectedClass == nullptr)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The class %s doesn't correspond to a class instance."), expectedClassName);
			cleanupTest();
			return false;
		}
		
		if (scriptComp->GetScriptClass() != expectedClass)
		{
			DString scriptClassName = (scriptComp->GetScriptClass() != nullptr) ? scriptComp->GetScriptClass()->ReadClassName().ToString() : TXT("nullptr");
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The script component is expected to use a %s class. Instead it's using %s."), expectedClassName, scriptClassName);
			cleanupTest();
			return false;
		}

		ContentObject* scriptObj = scriptComp->GetScriptObj();
		if (scriptObj == nullptr)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The script component is expected to spawn a %s object."), expectedClassName);
			cleanupTest();
			return false;
		}

		if (scriptObj->GetClass() != expectedClass)
		{
			DString className = (scriptObj->GetClass() != nullptr) ? scriptObj->GetClass()->ReadClassName().ToString() : TXT("nullptr");
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The script component's script object is expected to be a %s class. Instead it's %s."), expectedClass->ReadClassName(), className);
			cleanupTest();
			return false;
		}

		//Check object defaults
		ScriptVariableRef varInt = scriptObj->GetMemberVariable(0);
		if (!varInt.IsValid() || varInt.EditVar().GetVarType() != ScriptVariable::VT_Int)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s instance is expected to define an int member variable at index 0."), expectedClassName);
			cleanupTest();
			return false;
		}

		ScriptVariableRef varFloat = scriptObj->GetMemberVariable(1);
		if (!varFloat.IsValid() || varFloat.EditVar().GetVarType() != ScriptVariable::VT_Float)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s instance is expected to define a float member variable at index 1."), expectedClassName);
			cleanupTest();
			return false;
		}

		ScriptVariableRef varString = scriptObj->GetMemberVariable(2);
		if (!varString.IsValid() || varString.EditVar().GetVarType() != ScriptVariable::VT_String)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s instance is expected to define a string member variable at index 2."), expectedClassName);
			cleanupTest();
			return false;
		}

		//The ClmEngineComponent specifies the default values when creating the class.
		Int expectedInt = 10;
		if (varInt.EditVar().ToInt() != expectedInt)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s object's int member variable is expected to default to %s. Instead it's %s."), expectedClassName, expectedInt, varInt.EditVar().ToInt());
			cleanupTest();
			return false;
		}

		Float expectedFloat = -100.f;
		if (varFloat.EditVar().ToFloat() != expectedFloat)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s object's float member variable is expected to default to %s. Instead it's %s."), expectedClassName, expectedFloat, varFloat.EditVar().ToFloat());
			cleanupTest();
			return false;
		}

		DString expectedString = TXT("DefaultText");
		if (varString.EditVar().ToDString().Compare(expectedString, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Object Instantiation test failed. The %s object's string member variable is expected to default to \"%s\". Instead it's \"%s\"."), expectedClassName, expectedString, varString.EditVar().ToDString());
			cleanupTest();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Function Handling"));
	{
		Int intInput = 55;
		Float floatInput = 115.f;
		DString stringInput = TXT("My New Text");

		Int intOutput;
		Float floatOutput;
		DString stringOutput;

		//Invoking ProcessScriptFunction tests the following:
		//To test Native -> Script handling:
		//The function, itself, invokes a script function. That script function is expected to record the input variables to the obj's member variables.
		//To test Script -> Native:
		//The script function also invokes the script component's ConvertVar functions. The results from these functions are written as output in ProcessScriptFunction.
		scriptComp->ProcessScriptFunction(intInput, floatInput, stringInput, OUT intOutput, OUT floatOutput, OUT stringOutput);

		ContentObject* scriptObj = scriptComp->GetScriptObj();
		CHECK(scriptObj != nullptr)

		//Check the script object's member variables to see if it recorded the input parameters.
		ScriptVariableRef var = scriptObj->GetMemberVariable(0);
		CHECK(var.IsValid()) //Variables should have already been checked
		if (var.EditVar().ToInt() != intInput)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The %s is expected to record the input parameters. The %s variable is %s when it should have been %s instead."), scriptObj->ToString(), var.EditVar().ReadVariableName(), var.EditVar().ToInt(), intInput);
			cleanupTest();
			return false;
		}

		var = scriptObj->GetMemberVariable(1);
		CHECK(var.IsValid())
		if (var.EditVar().ToFloat() != floatInput)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The %s is expected to record the input parameters. The %s variable is %s when it should have been %s instead."), scriptObj->ToString(), var.EditVar().ReadVariableName(), var.EditVar().ToFloat(), floatInput);
			cleanupTest();
			return false;
		}

		var = scriptObj->GetMemberVariable(2);
		CHECK(var.IsValid())
		if (var.EditVar().ToDString().Compare(stringInput, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The %s is expected to record the input parameters. The %s variable is \"%s\" when it should have been \"%s\" instead."), scriptObj->ToString(), var.EditVar().ReadVariableName(), var.EditVar().ToDString(), stringInput);
			cleanupTest();
			return false;
		}

		//Test the output. They are expected to be the results from the conversion functions since the script function should invoke those exposed native methods.
		Int expectedIntOutput;
		scriptComp->CallableConvertInt(intInput, OUT expectedIntOutput);
		if (intOutput != expectedIntOutput)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The intOutput is expected to be %s. Instead it's %s."), expectedIntOutput, intOutput);
			cleanupTest();
			return false;
		}

		Float expectedFloatOutput;
		scriptComp->CallableConvertFloat(floatInput, OUT expectedFloatOutput);
		if (floatOutput != expectedFloatOutput)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The floatOutput is expected to be %s. Instead it's %s."), expectedFloatOutput, floatOutput);
			cleanupTest();
			return false;
		}

		DString expectedStringOutput;
		scriptComp->CallableConvertString(stringInput, OUT expectedStringOutput);
		if (stringOutput.Compare(expectedStringOutput, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Function Handling test failed. The stringOutput is expected to be \"%s\". Instead it's \"%s\"."), expectedStringOutput, stringOutput);
			cleanupTest();
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	
	cleanupTest();
	ExecuteSuccessSequence(testFlags, TXT("Script Components"));
	return true;
}

bool ContentLogicUnitTester::TestScriptFunctions (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Script Functions"));

	ScriptScope* scope = ScriptScope::CreateObject();
	std::vector<DString> callstack;
	ClmStack stack;

	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	CHECK(clmEngine != nullptr)

	ContentClass* testClass = clmEngine->CreateScriptClass(HashedString("UnitTestTestClass"), HashedString());
	ContentObject* testObjA = ContentObject::CreateObject();
	testObjA->BeginContentObject(testClass, scope, nullptr, nullptr);

	ContentObject* testObjB = ContentObject::CreateObject();
	testObjB->BeginContentObject(testClass, scope, nullptr, nullptr);

	std::function<void()> cleanupTest([&]()
	{
		testObjA->Destroy();
		testObjB->Destroy();

		clmEngine->DestroyScriptClass(testClass, true);

		scope->Destroy();
	});

	SetTestCategory(testFlags, TXT("Comparison Functions"));
	{
		Bool expected;
		HashedString functionName;
		ScriptVariableRef result = stack.PushVariable();
		ScriptVariableRef a = stack.PushVariable();
		ScriptVariableRef b = stack.PushVariable();

		std::function<bool()> testFunction([&]()
		{
			if (!clmEngine->ReadGlobalFunctions().contains(functionName.GetHash()))
			{
				UnitTestError(testFlags, TXT("Comparison Function test failed. The ClmEngineComponent does not contain a function named %s."), functionName);
				cleanupTest();
				return false;
			}

			const Subroutine* function = clmEngine->ReadGlobalFunctions().at(functionName.GetHash());
			CHECK(function != nullptr)
			function->ExecuteFunction(scope, OUT callstack, OUT stack);
			if (result.EditVar().ToBool() != expected)
			{
				UnitTestError(testFlags, TXT("Comparison Function test failed. The %s function is expected to return %s. Instead it returned %s."), functionName, expected, result.EditVar().ToBool());
				cleanupTest();
				return false;
			}

			return true;
		});

		functionName = HashedString("Bool==Bool");
		a.EditVar().WriteBool(true);
		b.EditVar().WriteBool(true);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteBool(false);
		b.EditVar().WriteBool(false);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteBool(true);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int==Int");
		a.EditVar().WriteInt(5);
		b.EditVar().WriteInt(5);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(7);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float==Float");
		a.EditVar().WriteFloat(-2.5f);
		b.EditVar().WriteFloat(-2.5f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-2.f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(2.5f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("String==String");
		a.EditVar().WriteDString(TXT("ContentLogic"));
		b.EditVar().WriteDString(TXT("ContentLogic"));
		ScriptVariableRef isSensitive = stack.PushVariable();
		isSensitive.EditVar().WriteBool(true);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteDString(TXT("CONTENTLOGIC"));
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		isSensitive.EditVar().WriteBool(false);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteDString(TXT("And now. Something completely different."));
		expected = false;
		if (!testFunction())
		{
			return false;
		}
		stack.PopVariable(); //Remove isSensitive from stack

		functionName = HashedString("Object==Object");
		a.EditVar().WriteObjPointer(testObjA);
		b.EditVar().WriteObjPointer(testObjB);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteObjPointer(nullptr);
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteObjPointer(testObjA);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteObjPointer(nullptr);
		b.EditVar().WriteObjPointer(nullptr);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Bool!=Bool");
		a.EditVar().WriteBool(true);
		b.EditVar().WriteBool(true);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteBool(false);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteBool(false);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int!=Int");
		a.EditVar().WriteInt(22);
		b.EditVar().WriteInt(22);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(26);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float!=Float");
		a.EditVar().WriteFloat(100.f);
		b.EditVar().WriteFloat(100.f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-100.f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(111.f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("String!=String");
		a.EditVar().WriteDString(TXT("ContentLogic"));
		b.EditVar().WriteDString(TXT("ContentLogic"));
		isSensitive = stack.PushVariable();
		isSensitive.EditVar().WriteBool(true);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteDString(TXT("CONTENTLOGIC"));
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		isSensitive.EditVar().WriteBool(false);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteDString(TXT("Something different"));
		expected = true;
		if (!testFunction())
		{
			return false;
		}
		stack.PopVariable(); //Remove isSensitive

		functionName = HashedString("Object!=Object");
		a.EditVar().WriteObjPointer(testObjA);
		b.EditVar().WriteObjPointer(testObjB);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteObjPointer(nullptr);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteObjPointer(nullptr);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteObjPointer(testObjB);
		b.EditVar().WriteObjPointer(testObjB);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int>Int");
		a.EditVar().WriteInt(10);
		b.EditVar().WriteInt(5);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(10);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(11);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float>Float");
		a.EditVar().WriteFloat(20.f);
		b.EditVar().WriteFloat(10.f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(20.f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(21.f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int>=Int");
		a.EditVar().WriteInt(5);
		b.EditVar().WriteInt(4);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(5);
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(6);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float>=Float");
		a.EditVar().WriteFloat(-3.5f);
		b.EditVar().WriteFloat(-4.f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-3.5f);
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-2.5f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int<Int");
		a.EditVar().WriteInt(12);
		b.EditVar().WriteInt(10);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(12);
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(14);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float<Float");
		a.EditVar().WriteFloat(2.25f);
		b.EditVar().WriteFloat(1.75f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(2.25f);
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(3.1f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int<=Int");
		a.EditVar().WriteInt(-101);
		b.EditVar().WriteInt(-102);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(-101);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteInt(-100);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float<=Float");
		a.EditVar().WriteFloat(-20.5f);
		b.EditVar().WriteFloat(-21.2f);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-20.5f);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		b.EditVar().WriteFloat(-19.9f);
		if (!testFunction())
		{
			return false;
		}

		//The next comparison functions only accept one parameter. Refill the stack with only one param, one output.
		stack.Empty();
		result = stack.PushVariable();
		a = stack.PushVariable();

		functionName = HashedString("IsObjectValid");
		a.EditVar().WriteObjPointer(testObjA);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteObjPointer(nullptr);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("IsObjectNull");
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteObjPointer(testObjB);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		stack.Empty();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Bool Operations"));
	{
		HashedString functionName;
		ScriptVariableRef result = stack.PushVariable();
		Bool expected;

		std::function<bool()> testFunction([&]()
		{
			if (!clmEngine->ReadGlobalFunctions().contains(functionName.GetHash()))
			{
				UnitTestError(testFlags, TXT("Bool Operations test failed. The ClmEngineComponent doesn't contain a function named %s."), functionName);
				cleanupTest();
				return false;
			}

			const Subroutine* function = clmEngine->ReadGlobalFunctions().at(functionName.GetHash());
			CHECK(function != nullptr)
			function->ExecuteFunction(scope, OUT callstack, OUT stack);

			if (result.EditVar().ToBool() != expected)
			{
				UnitTestError(testFlags, TXT("Bool Operations test failed. The %s is expected to return %s. Instead it returned %s."), functionName, expected, result.EditVar().ToBool());
				cleanupTest();
				return false;
			}

			return true;
		});

		ScriptVariableRef varA = stack.PushVariable();
		ScriptVariableRef varB = stack.PushVariable();

		functionName = HashedString("&&");
		varA.EditVar().WriteBool(true);
		varB.EditVar().WriteBool(true);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		varA.EditVar().WriteBool(false);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		varB.EditVar().WriteBool(false);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("||");
		varA.EditVar().WriteBool(true);
		varB.EditVar().WriteBool(true);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		varA.EditVar().WriteBool(false);
		if (!testFunction())
		{
			return false;
		}

		varB.EditVar().WriteBool(false);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("XOR");
		varA.EditVar().WriteBool(true);
		varB.EditVar().WriteBool(true);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		varA.EditVar().WriteBool(false);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		varB.EditVar().WriteBool(false);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		//NOT only accepts one parameter. Remove varB
		stack.PopVariable();

		functionName = HashedString("NOT");
		varA.EditVar().WriteBool(true);
		expected = false;
		if (!testFunction())
		{
			return false;
		}

		varA.EditVar().WriteBool(false);
		expected = true;
		if (!testFunction())
		{
			return false;
		}

		stack.Empty();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Conversion Functions"));
	{
		HashedString functionName;
		ScriptVariableRef output = stack.PushVariable();
		ScriptVariableRef input = stack.PushVariable();
		ScriptVariable expected;
		std::function<bool()> testFunction([&]()
		{
			if (!clmEngine->ReadGlobalFunctions().contains(functionName.GetHash()))
			{
				UnitTestError(testFlags, TXT("Conversion Function test failed. The function named %s isn't found."), functionName);
				cleanupTest();
				return false;
			}

			const Subroutine* function = clmEngine->ReadGlobalFunctions().at(functionName.GetHash());
			CHECK(function != nullptr)
			function->ExecuteFunction(scope, OUT callstack, OUT stack);
			if (output.EditVar().GetVarType() != expected.GetVarType())
			{
				UnitTestError(testFlags, TXT("Conversion Function test failed. The output of %s is expected to be a type %s. Instead it's a type %s."), functionName, Int(expected.GetVarType()), Int(output.EditVar().GetVarType()));
				cleanupTest();
				return false;
			}

			if (output.EditVar() != expected)
			{
				UnitTestError(testFlags, TXT("Conversion Function test failed. The output of %s is expected to be equal to %s. Instead it's %s."), functionName, expected.InterpretValueAsString(scope, stack), output.EditVar().InterpretValueAsString(scope, stack));
				cleanupTest();
				return false;
			}

			return true;
		});

		functionName = HashedString("BoolToInt");
		Bool inFlag = true;
		input.EditVar().WriteBool(inFlag);
		expected.WriteInt(inFlag.ToInt());
		if (!testFunction())
		{
			return false;
		}

		inFlag = false;
		input.EditVar().WriteBool(inFlag);
		expected.WriteInt(inFlag.ToInt());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("BoolToFloat");
		inFlag = true;
		input.EditVar().WriteBool(inFlag);
		expected.WriteFloat(inFlag.ToFloat());
		if (!testFunction())
		{
			return false;
		}

		inFlag = false;
		input.EditVar().WriteBool(inFlag);
		expected.WriteFloat(inFlag.ToFloat());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("BoolToString");
		inFlag = true;
		input.EditVar().WriteBool(inFlag);
		expected.WriteDString(inFlag.ToString());
		if (!testFunction())
		{
			return false;
		}

		inFlag = false;
		input.EditVar().WriteBool(inFlag);
		expected.WriteDString(inFlag.ToString());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("IntToBool");
		input.EditVar().WriteInt(5);
		expected.WriteBool(true);
		if (!testFunction())
		{
			return false;
		}

		input.EditVar().WriteInt(-1);
		if (!testFunction())
		{
			return false;
		}

		input.EditVar().WriteInt(0);
		expected.WriteBool(false);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("IntToFloat");
		Int origInt = 15;
		input.EditVar().WriteInt(origInt);
		expected.WriteFloat(origInt.ToFloat());
		if (!testFunction())
		{
			return false;
		}

		origInt = -27;
		input.EditVar().WriteInt(origInt);
		expected.WriteFloat(origInt.ToFloat());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("IntToString");
		expected.WriteDString(origInt.ToString());
		if (!testFunction())
		{
			return false;
		}

		origInt = 1103;
		input.EditVar().WriteInt(origInt);
		expected.WriteDString(origInt.ToString());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("FloatToBool");
		input.EditVar().WriteFloat(13.2f);
		expected.WriteBool(true);
		if (!testFunction())
		{
			return false;
		}

		input.EditVar().WriteFloat(0.f);
		expected.WriteBool(false);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("FloatToInt");
		Float origFloat = 1002.4f;
		input.EditVar().WriteFloat(origFloat);
		expected.WriteInt(origFloat.ToInt());
		if (!testFunction())
		{
			return false;
		}

		origFloat = -55.75f;
		input.EditVar().WriteFloat(origFloat);
		expected.WriteInt(origFloat.ToInt());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("FloatToString");
		expected.WriteDString(origFloat.ToString());
		if (!testFunction())
		{
			return false;
		}

		origFloat = 1020.68f;
		input.EditVar().WriteFloat(origFloat);
		expected.WriteDString(origFloat.ToString());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("StringToBool");
		DString str(TXT("ContentLogic"));
		input.EditVar().WriteDString(str);
		expected.WriteBool(str.ToBool());
		if (!testFunction())
		{
			return false;
		}

		str = TXT("true");
		input.EditVar().WriteDString(str);
		expected.WriteBool(str.ToBool());
		if (!testFunction())
		{
			return false;
		}

		str = TXT("1");
		input.EditVar().WriteDString(str);
		expected.WriteBool(str.ToBool());
		if (!testFunction())
		{
			return false;
		}

		str = TXT("false");
		input.EditVar().WriteDString(str);
		expected.WriteBool(str.ToBool());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("StringToInt");
		str = TXT("293");
		input.EditVar().WriteDString(str);
		expected.WriteInt(str.Atoi());
		if (!testFunction())
		{
			return false;
		}

		str = TXT("-19");
		input.EditVar().WriteDString(str);
		expected.WriteInt(str.Atoi());
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("StringToFloat");
		str = TXT("52.94");
		input.EditVar().WriteDString(str);
		expected.WriteFloat(str.Stof());
		if (!testFunction())
		{
			return false;
		}

		str = TXT("-298.52");
		input.EditVar().WriteDString(str);
		expected.WriteFloat(str.Stof());
		if (!testFunction())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		HashedString functionName;
		ScriptVariableRef output = stack.PushVariable();
		ScriptVariableRef a = stack.PushVariable();
		ScriptVariableRef b = stack.PushVariable();
		ScriptVariable expected;
		std::function<bool()> testFunction([&]()
		{
			if (!clmEngine->ReadGlobalFunctions().contains(functionName.GetHash()))
			{
				UnitTestError(testFlags, TXT("Arithmetic test failed. The function name %s was not found."), functionName);
				cleanupTest();
				return false;
			}

			const Subroutine* function = clmEngine->ReadGlobalFunctions().at(functionName.GetHash());
			CHECK(function != nullptr)
			function->ExecuteFunction(scope, OUT callstack, OUT stack);
			if (output.EditVar().GetVarType() != expected.GetVarType())
			{
				UnitTestError(testFlags, TXT("Arithmetic test failed. The function %s is expected to return a %s type. Instead it's %s."), functionName, Int(expected.GetVarType()), Int(output.EditVar().GetVarType()));
				cleanupTest();
				return false;
			}

			//Handle float cases since they may be an approximation.
			bool isEqual;
			if (expected.GetVarType() == ScriptVariable::VT_Float)
			{
				isEqual = (output.EditVar().ToFloat().IsCloseTo(expected.ToFloat()));
			}
			else
			{
				isEqual = output.EditVar() == expected; //Strings and Ints
			}

			if (!isEqual)
			{
				UnitTestError(testFlags, TXT("Arithmetic test failed. The function %s is expected to return %s. Instead it returned %s."), functionName, expected.InterpretValueAsString(scope, stack), output.EditVar().InterpretValueAsString(scope, stack));
				cleanupTest();
				return false;
			}

			return true;
		});

		functionName = HashedString("Int+Int");
		a.EditVar().WriteInt(13);
		b.EditVar().WriteInt(-2);
		expected.WriteInt(11);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(298);
		b.EditVar().WriteInt(146);
		expected.WriteInt(444);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float+Float");
		a.EditVar().WriteFloat(10.5f);
		b.EditVar().WriteFloat(6.75f);
		expected.WriteFloat(17.25f);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteFloat(22.25f);
		b.EditVar().WriteFloat(-35.75f);
		expected.WriteFloat(-13.5f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("String+String");
		a.EditVar().WriteDString(TXT("Content"));
		b.EditVar().WriteDString(TXT("Logic"));
		expected.WriteDString(TXT("ContentLogic"));
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteDString(TXT("Dyllus "));
		b.EditVar().WriteDString(TXT("Duskstorm"));
		expected.WriteDString(TXT("Dyllus Duskstorm"));
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int-Int");
		a.EditVar().WriteInt(20);
		b.EditVar().WriteInt(8);
		expected.WriteInt(12);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(-25);
		b.EditVar().WriteInt(2);
		expected.WriteInt(-27);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float-Float");
		a.EditVar().WriteFloat(-13.2f);
		b.EditVar().WriteFloat(-6.1f);
		expected.WriteFloat(-7.1f);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteFloat(2.4f);
		b.EditVar().WriteFloat(1.7f);
		expected.WriteFloat(0.7f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int*Int");
		a.EditVar().WriteInt(4);
		b.EditVar().WriteInt(8);
		expected.WriteInt(32);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(2);
		b.EditVar().WriteInt(-10);
		expected.WriteInt(-20);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float*Float");
		a.EditVar().WriteFloat(12.3f);
		b.EditVar().WriteFloat(6.1f);
		expected.WriteFloat(75.03f);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteFloat(5.5f);
		b.EditVar().WriteFloat(-3.9f);
		expected.WriteFloat(-21.45f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int/Int");
		a.EditVar().WriteInt(10);
		b.EditVar().WriteInt(2);
		expected.WriteInt(5);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(-20);
		b.EditVar().WriteInt(10);
		expected.WriteInt(-2);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float/Float");
		a.EditVar().WriteFloat(12.5f);
		b.EditVar().WriteFloat(3.2f);
		expected.WriteFloat(3.90625f);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteFloat(4.5f);
		b.EditVar().WriteFloat(-0.25f);
		expected.WriteFloat(-18.f);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int%Int");
		a.EditVar().WriteInt(6);
		b.EditVar().WriteInt(5);
		expected.WriteInt(1);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(-12);
		b.EditVar().WriteInt(5);
		expected.WriteInt(-2);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Float%Float");
		a.EditVar().WriteFloat(102.f);
		b.EditVar().WriteFloat(33.f);
		expected.WriteFloat(3.f);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteFloat(-40.f);
		b.EditVar().WriteFloat(31.f);
		expected.WriteFloat(-9.f);
		if (!testFunction())
		{
			return false;
		}

		//Increment/Decrement only needs one parameter. Remove b
		stack.PopVariable();

		functionName = HashedString("Int++");
		a.EditVar().WriteInt(17);
		expected.WriteInt(18);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(-22);
		expected.WriteInt(-21);
		if (!testFunction())
		{
			return false;
		}

		functionName = HashedString("Int--");
		a.EditVar().WriteInt(106);
		expected.WriteInt(105);
		if (!testFunction())
		{
			return false;
		}

		a.EditVar().WriteInt(-33);
		expected.WriteInt(-34);
		if (!testFunction())
		{
			return false;
		}

		stack.Empty();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Object Handling"));
	{
		ContentClass* rootClass = clmEngine->CreateScriptClass(HashedString("UnitTestRootClass"), HashedString());
		ContentClass* leafClass = clmEngine->CreateScriptClass(HashedString("UnitTestLeafClass"), HashedString("UnitTestRootClass"));
		ContentClass* otherClass = clmEngine->CreateScriptClass(HashedString("UnitTestOtherClass"), HashedString());

		std::vector<ContentObject*> createdObjects;

		std::function<void()> cleanupCategory([&]()
		{
			for (ContentObject* obj : createdObjects)
			{
				delete obj;
			}
			ContainerUtils::Empty(OUT createdObjects);

			clmEngine->DestroyScriptClass(rootClass, true); //destroys leaf
			clmEngine->DestroyScriptClass(otherClass, true);
		});
		
		HashedString createFunctionName("CreateObject");
		if (!clmEngine->ReadGlobalFunctions().contains(createFunctionName.GetHash()))
		{
			UnitTestError(testFlags, TXT("Object Handling test failed. The function %s was not found."), createFunctionName);
			cleanupCategory();
			cleanupTest();
			return false;
		}

		const Subroutine* createFunction = clmEngine->ReadGlobalFunctions().at(createFunctionName.GetHash());
		CHECK(createFunction != nullptr)
		
		//Output
		ScriptVariableRef createdObj = stack.PushVariable();
		createdObj.EditVar().SetFixedType(ScriptVariable::VT_Obj);

		//Input
		ScriptVariableRef objClass = stack.PushVariable();
		objClass.EditVar().SetFixedType(ScriptVariable::VT_Class);

		ScriptVariableRef objOwner = stack.PushVariable();
		objOwner.EditVar().WriteObjPointer(nullptr);

		//Test fails if it returns nullptr
		std::function<ContentObject*()> runCreateTests([&]() -> ContentObject*
		{
			const ContentClass* targetClass = objClass.EditVar().ToClass();
			if (targetClass == nullptr)
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. The class is expected to be specified in the parameters. Instead it's nullptr."));
				cleanupCategory();
				cleanupTest();
				return nullptr;
			}

			createFunction->ExecuteFunction(scope, OUT callstack, OUT stack);

			ContentObject* result = createdObj.EditVar().ToObjPointer(scope);
			if (result == nullptr)
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. %s failed to instantiate an object from a %s"), createFunctionName, targetClass->ReadClassName());
				cleanupCategory();
				cleanupTest();
				return nullptr;
			}

			createdObjects.push_back(result);
			if (result->GetClass() != targetClass)
			{
				DString actualClassName = (result->GetClass() != nullptr) ? result->GetClass()->ReadClassName().ToString() : TXT("nullptr");
				UnitTestError(testFlags, TXT("Object Handling test failed. The object instantiated from %s should have been a %s type. Instead it's a %s."), createFunctionName, targetClass->ReadClassName(), actualClassName);
				delete result;
				cleanupCategory();
				cleanupTest();
				return nullptr;
			}

			//Ensure the object resides in the scope.
			if (!scope->ReadHeap().contains(result->GetId()))
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. The scope should contain a reference to %s, but it's not found anywhere on the heap."), result->ToString());
				delete result;
				cleanupCategory();
				cleanupTest();
				return nullptr;
			}

			return result;
		});

		//Create three instances. One from each class.
		objClass.EditVar().WriteClass(rootClass);
		ContentObject* rootObj = runCreateTests();
		if (rootObj == nullptr)
		{
			return false;
		}

		objClass.EditVar().WriteClass(leafClass);
		ContentObject* leafObj = runCreateTests();
		if (leafObj == nullptr)
		{
			return false;
		}

		objClass.EditVar().WriteClass(otherClass);
		ContentObject* otherObj = runCreateTests();
		if (otherObj == nullptr)
		{
			return false;
		}

		//Destroyed only requires reference to 'this'. That is populated within testDestroyed
		stack.PopVariable(); //objOwner
		stack.PopVariable(); //objClass
		stack.PopVariable(); //createdObj

		ContentObject* targetObj;
		std::function<bool()> testDestroyed([&]()
		{
			const ContentClass* targetClass = targetObj->GetClass();
			if (targetClass == nullptr)
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. The %s object is not associated with a class."), targetObj->ToString());
				cleanupCategory();
				cleanupTest();
				return false;
			}

			HashedString functionName("Destroy");
			const Subroutine* destroyedFunction = targetClass->FindFunction(scope, functionName.GetHash(), targetObj);
			if (destroyedFunction == nullptr)
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. The %s doesn't have a function named %s."), targetObj->ToString(), functionName);
				cleanupCategory();
				cleanupTest();
				return false;
			}

			size_t objId = targetObj->GetId();
			ScriptVariableRef thisVar = stack.PushVariable();
			thisVar.EditVar().WriteObjPointer(targetObj);
			destroyedFunction->ExecuteFunction(scope, OUT callstack, OUT stack);
			stack.PopVariable();

			if (scope->ReadHeap().contains(objId))
			{
				UnitTestError(testFlags, TXT("Object Handling test failed. After calling %s on %s, its ID (%s) still exists on the scope's heap."), functionName, targetObj->ToString(), Int(objId));
				cleanupCategory();
				cleanupTest();
				return false;
			}

			ContainerUtils::RemoveItem(OUT createdObjects, targetObj);
			return true;
		});

		targetObj = rootObj;
		if (!testDestroyed())
		{
			return false;
		}

		targetObj = leafObj;
		if (!testDestroyed())
		{
			return false;
		}

		targetObj = otherObj;
		if (!testDestroyed())
		{
			return false;
		}

		cleanupCategory();
	}
	CompleteTestCategory(testFlags);

	cleanupTest();
	ExecuteSuccessSequence(testFlags, TXT("Script Functions"));
	return true;
}
SD_END
#endif