/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandPushVar.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandPushVar.h"
#include "ScriptVariableRef.h"

SD_BEGIN
CommandPushVar::CommandPushVar (ScriptVariable::EVarType inType) : ScriptCommand(),
	Type(inType)
{
	//Noop
}

Int CommandPushVar::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef newVar = outStack.PushVariable();
	newVar.EditVar().SetFixedType(Type);
	return 1;
}
SD_END