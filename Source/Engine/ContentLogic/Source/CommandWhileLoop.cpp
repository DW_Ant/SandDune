/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CommandWhileLoop.cpp
=====================================================================
*/

#include "ClmStack.h"
#include "CommandWhileLoop.h"
#include "CommandBeginSection.h"
#include "CommandEndSection.h"
#include "ScriptVariable.h"
#include "ScriptVariableRef.h"
#include "Subroutine.h"

SD_BEGIN
CommandWhileLoop::CommandWhileLoop (size_t inBoolOffset) :
	BoolOffset(inBoolOffset)
{
	//Noop
}

Int CommandWhileLoop::ExecuteCommand (ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef var = outStack.EditVariable(BoolOffset);
	if (var.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		var = var.EditVar().GetPointerRecursive(scope, outStack);
	}

	CHECK(var.EditVar().GetVarType() == ScriptVariable::VT_Bool)
	if (var.EditVar().ToBool())
	{
		//Proceed inside the loop
		return 1;
	}

	//Find the end section corresponding to this loop.
	Int sectionLayer = 0;

	//+1 to skip over this command
	for (size_t curStep = StepIdx + 1; curStep < callingFunction->ReadCommandSteps().size(); ++curStep)
	{
		if (dynamic_cast<CommandBeginSection*>(callingFunction->ReadCommandSteps().at(curStep)) != nullptr)
		{
			++sectionLayer;
			continue;
		}

		if (dynamic_cast<CommandEndSection*>(callingFunction->ReadCommandSteps().at(curStep)) != nullptr)
		{
			--sectionLayer;
			if (sectionLayer <= 0)
			{
				return (curStep - StepIdx) + 1; //Jump to the step after the end section
			}
		}
	}

	//No end section found. Jump to the end of the function.
	return callingFunction->ReadCommandSteps().size();
}

Int CommandWhileLoop::SectionEvaluation (const CommandEndSection* endSection, ScriptScope* scope, const Subroutine* callingFunction, std::vector<DString>& outCallstack, ClmStack& outStack) const
{
	ScriptVariableRef var = outStack.EditVariable(BoolOffset);
	if (var.EditVar().GetVarType() == ScriptVariable::VT_Pointer)
	{
		var = var.EditVar().GetPointerRecursive(scope, outStack);
	}

	CHECK(var.EditVar().GetVarType() == ScriptVariable::VT_Bool)
	if (!var.EditVar().ToBool())
	{
		//Exit the loop
		return 1;
	}

	//Jump backwards to the start of the loop.
	Int numSteps = endSection->GetStepIdx() - StepIdx;
	numSteps *= -1; //navigate backwards
	numSteps += 2; //Don't evaluate the while loop and the BeginSection. Head to the step after the BeginSection instead.
	return numSteps;
}
SD_END