/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RandomEngineComponent.h
  Simple engine component responsible for managing the random seed.
=====================================================================
*/

#pragma once

#include "Random.h"

SD_BEGIN
class RANDOM_API RandomEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(RandomEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Latest seed used on the number generator. */
	unsigned int Seed;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetSeed (unsigned int newSeed);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline unsigned int GetSeed () const
	{
		return Seed;
	}
};
SD_END