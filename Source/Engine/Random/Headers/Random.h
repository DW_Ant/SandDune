/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Random.h
  Contains important file includes and definitions for the Random module.

  The Random module is a utility module containing several functions orientated on randomization.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef RANDOM_EXPORT
		#define RANDOM_API __declspec(dllexport)
	#else
		#define RANDOM_API __declspec(dllimport)
	#endif
#else
	#define RANDOM_API
#endif

#include <random>

SD_BEGIN
extern LogCategory RANDOM_API RandomLog;
SD_END