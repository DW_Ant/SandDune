/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RandomUnitTester.h
  Unit tester that'll be validating various random related utility functions.
=====================================================================
*/

#pragma once

#include "Random.h"

#ifdef DEBUG_MODE

SD_BEGIN
class RANDOM_API RandomUnitTester : public UnitTester
{
	DECLARE_CLASS(RandomUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestUtilityFunctions (EUnitTestFlags testFlags) const;
	virtual bool TestRandomEngine (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode