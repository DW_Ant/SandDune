/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RandomUtils.h
  Contains various/misc functions that yields different results based on fRand()
=====================================================================
*/

#pragma once

#include "Random.h"

SD_BEGIN
class RANDOM_API RandomUtils : public BaseUtils
{
	DECLARE_CLASS(RandomUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a random number ranging from [0,1].
	 */
	static Float fRand ();

	/**
	 * Returns a random value ranging from [min, max]
	 */
	static Float RandRange (Float min, Float max);

	/**
	 * Returns a random value from [0, max)
	 */
	static Int Rand (Int max);
	static size_t Rand (size_t max);

	/**
	 * Returns a random 2D point within the circle of the given radius.
	 * Positive X is right of center; negative Y is below center.
	 */
	static Vector2 RandPointWithinCircle (Float maxRadius);
	static Vector2 RandPointWithinCircle (Float minRadius, Float maxRadius);


	/*
	=====================
	  Templates
	=====================
	*/

	template <class T>
	static void ShuffleVector (std::vector<T>& outDataList)
	{
		RandomEngineComponent* randEngine = RandomEngineComponent::Find();
		CHECK(randEngine != nullptr)

		std::mt19937 twisterEngine;
		twisterEngine.seed(randEngine->GetSeed());
		std::shuffle(std::begin(outDataList), std::end(outDataList), twisterEngine);
	}

	/**
	 * Returns a pointer to a random element in the given vector.
	 * The given vector must contain at least one element!
	 */
	template <class T>
	static const T& GetRandElement (const std::vector<T>& data)
	{
		CHECK(!ContainerUtils::IsEmpty(data))
		return data.at(Rand(data.size() - 1));
	}
};
SD_END