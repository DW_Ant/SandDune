/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RandomEngineComponent.cpp
=====================================================================
*/

#include "RandomClasses.h"

IMPLEMENT_ENGINE_COMPONENT(SD::RandomEngineComponent)
SD_BEGIN

void RandomEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	SetSeed(static_cast<unsigned int>(time(nullptr)));
}

void RandomEngineComponent::SetSeed (unsigned int newSeed)
{
	RandomLog.Log(LogCategory::LL_Log, TXT("Random number generator seed is now %s."), Int(newSeed));

	Seed = newSeed;
	srand(Seed);
}
SD_END