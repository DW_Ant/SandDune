/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RandomUtils.cpp
=====================================================================
*/

#include "RandomClasses.h"

IMPLEMENT_ABSTRACT_CLASS(SD::RandomUtils, SD::BaseUtils)
SD_BEGIN

Float RandomUtils::fRand ()
{
	int randResult = std::rand();

	return Float(static_cast<float>(randResult) / RAND_MAX);
}

Float RandomUtils::RandRange (Float min, Float max)
{
	return (fRand() * (max - min)) + min;
}

Int RandomUtils::Rand (Int max)
{
	//Reason for subtract 1.0e-6, covers the case where fRand returns 1.  Return value should always be less than max.
	return Int(max.ToFloat() * (fRand() - 1.0e-6f));
}

size_t RandomUtils::Rand (size_t max)
{
	return Rand(Int(max)).ToUnsignedInt();
}

Vector2 RandomUtils::RandPointWithinCircle (Float maxRadius)
{
	Float selectedX = RandRange(-1.f, 1.f);

	Float maxY = std::cos((selectedX * PI_FLOAT * 0.5f).Value);
	Float selectedY = RandRange(-maxY, maxY);

	Vector2 result(selectedX, selectedY);
	result *= maxRadius;

#ifdef DEBUG_MODE
	CHECK(result.VSize() <= maxRadius);
#endif

	return result;
}

Vector2 RandomUtils::RandPointWithinCircle (Float minRadius, Float maxRadius)
{
	//Safety check
	if (minRadius > maxRadius)
	{
		//Display debugging message.  Swapping parameters wont cause harm to the application other than a minor performance hit.
		RandomLog.Log(LogCategory::LL_Warning, TXT("Bad parameters were given to RandPointWithinCircles(%s, %s).  The minRadius is greater than maxRadius.  Reversing parameters."), minRadius, maxRadius);

		Float oldMinRadius = minRadius;
		minRadius = maxRadius;
		maxRadius = oldMinRadius;
	}

	Vector2 randPoint = RandPointWithinCircle(maxRadius - minRadius);
	if (randPoint.VSize() == 0)
	{
		//Random point ended up being in center of circle.  Pick a random direction to fulfill minRadius.
		Float selectedTheta = RandRange(-PI_FLOAT, PI_FLOAT);
		Float selectedX = cos(selectedTheta.Value) * minRadius;
		Float selectedY = sin(selectedTheta.Value) * minRadius;
		return Vector2(selectedX, selectedY);
	}

	//Increase selected point by minRadius.  Solve using similar right triangles.
	Float curRadius = sqrt(pow(randPoint.X.Value, 2) + pow(randPoint.Y.Value, 2));
	Float deltaX = (randPoint.X * minRadius)/curRadius;
	Float deltaY = (randPoint.Y * minRadius)/curRadius;

	return Vector2(randPoint.X + deltaX, randPoint.Y + deltaY);
}
SD_END