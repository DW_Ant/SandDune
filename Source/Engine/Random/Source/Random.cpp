/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Random.cpp
=====================================================================
*/

#include "RandomClasses.h"

SD_BEGIN
LogCategory RandomLog(TXT("Random"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);
SD_END