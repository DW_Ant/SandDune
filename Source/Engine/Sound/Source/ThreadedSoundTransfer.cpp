/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadSoundTransfer.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioBufferModifier.h"
#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"
#include "PlayerListenerComponent.h"
#include "ThreadedDynamicAudioInstance.h"
#include "ThreadedSoundTransfer.h"

#ifdef WITH_MULTI_THREAD

IMPLEMENT_CLASS(SD::ThreadedSoundTransfer, SD::Entity)
SD_BEGIN
const Int ThreadedSoundTransfer::FLAG_TERMINATE_ENGINE	(0x00000001);
const Int ThreadedSoundTransfer::FLAG_LOG_DATA			(0x00000002);
const Int ThreadedSoundTransfer::FLAG_AUDIO_IMPORT		(0x00000004);
const Int ThreadedSoundTransfer::FLAG_AUDIO_PROPS		(0x00000008);
const Int ThreadedSoundTransfer::FLAG_AUDIO_DESTROY		(0x00000010);
const Int ThreadedSoundTransfer::FLAG_SPEED_OF_SOUND	(0x00000020);
const Int ThreadedSoundTransfer::FLAG_MASTER_VOLUME		(0x00000040);
const Int ThreadedSoundTransfer::FLAG_CHANNEL_VOLUME	(0x00000080);
const Int ThreadedSoundTransfer::FLAG_CHANNEL_DILATION	(0x00000100);
const Int ThreadedSoundTransfer::FLAG_DUCK_VOLUME		(0x00000200);
const Int ThreadedSoundTransfer::FLAG_DUCK_LERP			(0x00000400);
const Int ThreadedSoundTransfer::FLAG_PLAY_STATIC		(0x00000800);
const Int ThreadedSoundTransfer::FLAG_STOP_STATIC		(0x00001000);
const Int ThreadedSoundTransfer::FLAG_STOP_DYNAMIC		(0x00002000);
const Int ThreadedSoundTransfer::FLAG_STOP_CHANNEL		(0x00004000);
const Int ThreadedSoundTransfer::FLAG_STOP_ALL			(0x00008000);
const Int ThreadedSoundTransfer::FLAG_SOUND_BEGIN		(0x00010000);
const Int ThreadedSoundTransfer::FLAG_SOUND_ENDED		(0x00020000);

ThreadedSoundTransfer::SPendingLog::SPendingLog (LogCategory::ELogLevel inLogLevel, const DString& inMsg) :
	LogLevel(inLogLevel),
	Msg(inMsg)
{
	//Noop
}

ThreadedSoundTransfer::SPendingAudioImport::SPendingAudioImport (const FileAttributes& inFile, const DString& inAudioName) :
	File(inFile),
	AudioName(inAudioName)
{
	//Noop
}

ThreadedSoundTransfer::SPendingChannelVolume::SPendingChannelVolume (size_t inChannelHash, Float inNewVolume) :
	ChannelHash(inChannelHash),
	NewVolume(inNewVolume)
{
	//Noop
}

ThreadedSoundTransfer::SPendingChannelDilation::SPendingChannelDilation (size_t inChannelHash, Float inNewDilation) :
	ChannelHash(inChannelHash),
	NewDilation(inNewDilation)
{
	//Noop
}

ThreadedSoundTransfer::SPendingStaticSound::SPendingStaticSound (size_t inAssetHash, Bool inDynamicAsset, Float inVolume, Float inPitch) :
	AssetHash(inAssetHash),
	DynamicAsset(inDynamicAsset),
	Volume(inVolume),
	Pitch(inPitch)
{
	//Noop
}

ThreadedSoundTransfer::SPendingStaticSound::SPendingStaticSound (const SPendingStaticSound& cpyObj) :
	AssetHash(cpyObj.AssetHash),
	DynamicAsset(cpyObj.DynamicAsset),
	Volume(cpyObj.Volume),
	Pitch(cpyObj.Pitch)
{
	cpyObj.Parameters.CopyBufferTo(OUT Parameters);
}

void ThreadedSoundTransfer::SPendingStaticSound::operator= (const SPendingStaticSound& cpyObj)
{
	AssetHash = cpyObj.AssetHash;
	DynamicAsset = cpyObj.DynamicAsset;
	Volume = cpyObj.Volume;
	Pitch = cpyObj.Pitch;
	cpyObj.Parameters.CopyBufferTo(OUT Parameters);
}

ThreadedSoundTransfer::SPendingSoundBegin::SPendingSoundBegin (size_t inAssetHash, Int inAudioCompId, Int inListenerCompId) :
	AssetHash(inAssetHash),
	AudioCompId(inAudioCompId),
	ListenerCompId(inListenerCompId)
{
	//Noop
}

ThreadedSoundTransfer::SPendingSoundEnded::SPendingSoundEnded (size_t inAssetHash, Int inAudioCompId, Int inListenerCompId) :
	AssetHash(inAssetHash),
	AudioCompId(inAudioCompId),
	ListenerCompId(inListenerCompId)
{
	//Noop
}

void ThreadedSoundTransfer::InitProps ()
{
	Super::InitProps();

	bDestroyingExternal = false;
	MaxWaitTime = 0.33f; //Three updates per second
	ThreadComp = nullptr;

	PendingSpeedOfSound = -1.f;
	PendingMasterVolume = -1.f;
	PendingDuckVolume = -1.f;
	PendingDuckLerp = -1.f;
	bPendingStopAll = false;
}

void ThreadedSoundTransfer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	if (localAudio != nullptr && localAudio->IsRunningMultiThreadMode() && localEngine->GetEngineIndex() != MULTI_THREAD_SOUND_IDX)
	{
		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, ThreadedSoundTransfer, HandleCopyData, void, DataBuffer&));
			ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(this, ThreadedSoundTransfer, HandleReceiveData, void, const DataBuffer&));

			//Instantiate a ThreadedSoundTransfer on the sound thread
			ThreadComp->InstantiateExternalComp(MULTI_THREAD_SOUND_IDX, []
			{
				//This method is executed on the sound thread
				AudioEngineComponent* mainAudioEngine = AudioEngineComponent::Find();
				CHECK_INFO(mainAudioEngine != nullptr, "An AudioEngineComponent doesn't exist on the sound thread")

				ThreadedSoundTransfer* otherTransfer = ThreadedSoundTransfer::CreateObject();
				mainAudioEngine->ThreadTransfer = otherTransfer;
				
				otherTransfer->ThreadComp = ThreadedComponent::CreateObject();
				if (otherTransfer->AddComponent(otherTransfer->ThreadComp))
				{
					otherTransfer->ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(otherTransfer, ThreadedSoundTransfer, HandleCopyData, void, DataBuffer&));
					otherTransfer->ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(otherTransfer, ThreadedSoundTransfer, HandleReceiveData, void, const DataBuffer&));
				}

				return otherTransfer->ThreadComp;
			});
		}
	}
}

void ThreadedSoundTransfer::TerminateOtherEngine ()
{
	bDestroyingExternal = true;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::TransferLog (LogCategory::ELogLevel logLevel, const DString& logMsg)
{
	PendingLogs.emplace_back(SPendingLog(logLevel, logMsg));
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::ImportAudio (const FileAttributes& audioFile, const DString& audioName)
{
	PendingImports.emplace_back(SPendingAudioImport(audioFile, audioName));
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::CopyAudioProps (size_t audioHash)
{
	ContainerUtils::AddUnique(OUT PendingAudioProps, audioHash);
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::DestroyAudio (size_t audioHash)
{
	PendingAudioDestroy.push_back(audioHash);
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetSpeedOfSound (Float newSpeedOfSound)
{
	PendingSpeedOfSound = newSpeedOfSound;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetMasterVolume (Float newMasterVolume)
{
	PendingMasterVolume = newMasterVolume;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetChannelVolume (size_t channelHash, Float newVolume)
{
	PendingChannelVolumes.emplace_back(SPendingChannelVolume(channelHash, newVolume));
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetChannelDilation (size_t channelHash, Float newDilation)
{
	PendingChannelDilations.emplace_back(SPendingChannelDilation(channelHash, newDilation));
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetDuckVolume (Float newDuckVolume)
{
	PendingDuckVolume = newDuckVolume;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::SetDuckLerp (Float newDuckLerp)
{
	PendingDuckLerp = newDuckLerp;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::PlayStaticSound (const AudioAsset* asset, Float volume, Float pitch)
{
	const ThreadedDynamicAudioInstance* dynAsset = dynamic_cast<const ThreadedDynamicAudioInstance*>(asset);

	SPendingStaticSound& soundEvnt = PendingStaticSounds.emplace_back(asset->GetAudioHash(), (dynAsset != nullptr), volume, pitch);
	if (dynAsset != nullptr)
	{
		dynAsset->ReadParamRecords().CopyBufferTo(OUT soundEvnt.Parameters);
	}

	if (ThreadComp != nullptr)
	{
		//Playing sounds are often time sensitive. Force a data transfer as soon as possible.
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::StopStaticSound (size_t assetHash)
{
	PendingStopStatic.push_back(assetHash);
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::StopDynamicSound (size_t assetHash)
{
	PendingStopDynamic.push_back(assetHash);
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::StopChannel (size_t channelHash)
{
	PendingStopChannel.push_back(channelHash);
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::StopAllSounds ()
{
	bPendingStopAll = true;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
}

void ThreadedSoundTransfer::BroadcastSoundBegin (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp)
{
	size_t audioHash = 0;
	if (asset != nullptr)
	{
		audioHash = asset->GetAudioHash();
	}

	Int audioId = 0;
	if (audioComp != nullptr)
	{
		audioId = audioComp->GetAudioCompId();
	}

	Int listenId = 0;
	if (listenComp != nullptr)
	{
		listenId = listenComp->GetListenerId();
	}

	PendingSoundBeginList.emplace_back(SPendingSoundBegin(audioHash, audioId, listenId));
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void ThreadedSoundTransfer::BroadcastSoundEnded (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp)
{
	size_t audioHash = 0;
	if (asset != nullptr)
	{
		audioHash = asset->GetAudioHash();
	}

	Int audioId = 0;
	if (audioComp != nullptr)
	{
		audioId = audioComp->GetAudioCompId();
	}

	Int listenId = 0;
	if (listenComp != nullptr)
	{
		listenId = listenComp->GetListenerId();
	}

	PendingSoundEndedList.emplace_back(SPendingSoundEnded(audioHash, audioId, listenId));

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

Int ThreadedSoundTransfer::WriteBufferHeader (DataBuffer& outExternalData)
{
	Int dataFlags = 0;
	if (bDestroyingExternal)
	{
		dataFlags |= FLAG_TERMINATE_ENGINE;
	}

	if (!ContainerUtils::IsEmpty(PendingLogs))
	{
		dataFlags |= FLAG_LOG_DATA;
	}

	if (!ContainerUtils::IsEmpty(PendingImports))
	{
		dataFlags |= FLAG_AUDIO_IMPORT;
	}

	if (!ContainerUtils::IsEmpty(PendingAudioProps))
	{
		dataFlags |= FLAG_AUDIO_PROPS;
	}

	if (!ContainerUtils::IsEmpty(PendingAudioDestroy))
	{
		dataFlags |= FLAG_AUDIO_DESTROY;
	}

	if (PendingSpeedOfSound >= 0.f)
	{
		dataFlags |= FLAG_SPEED_OF_SOUND;
	}

	if (PendingMasterVolume >= 0.f)
	{
		dataFlags |= FLAG_MASTER_VOLUME;
	}

	if (!ContainerUtils::IsEmpty(PendingChannelVolumes))
	{
		dataFlags |= FLAG_CHANNEL_VOLUME;
	}

	if (!ContainerUtils::IsEmpty(PendingChannelDilations))
	{
		dataFlags |= FLAG_CHANNEL_DILATION;
	}

	if (PendingDuckVolume >= 0.f)
	{
		dataFlags |= FLAG_DUCK_VOLUME;
	}

	if (PendingDuckLerp >= 0.f)
	{
		dataFlags |= FLAG_DUCK_LERP;
	}

	if (!ContainerUtils::IsEmpty(PendingStaticSounds))
	{
		dataFlags |= FLAG_PLAY_STATIC;
	}

	if (!ContainerUtils::IsEmpty(PendingStopStatic))
	{
		dataFlags |= FLAG_STOP_STATIC;
	}

	if (!ContainerUtils::IsEmpty(PendingStopDynamic))
	{
		dataFlags |= FLAG_STOP_DYNAMIC;
	}

	if (!ContainerUtils::IsEmpty(PendingStopChannel))
	{
		dataFlags |= FLAG_STOP_CHANNEL;
	}

	if (bPendingStopAll)
	{
		dataFlags |= FLAG_STOP_ALL;
	}

	if (!ContainerUtils::IsEmpty(PendingSoundBeginList))
	{
		dataFlags |= FLAG_SOUND_BEGIN;
	}

	if (!ContainerUtils::IsEmpty(PendingSoundEndedList))
	{
		dataFlags |= FLAG_SOUND_ENDED;
	}

	outExternalData << dataFlags;
	return dataFlags;
}

void ThreadedSoundTransfer::HandleCopyData (DataBuffer& outExternalData)
{
	Int dataFlags = WriteBufferHeader(OUT outExternalData);

	if ((dataFlags & FLAG_LOG_DATA) > 0)
	{
		outExternalData << Int(PendingLogs.size());
		for (size_t i = 0; i < PendingLogs.size(); ++i)
		{
			Int logLevel = PendingLogs.at(i).LogLevel;
			outExternalData << logLevel;
			outExternalData << PendingLogs.at(i).Msg;
		}
		ContainerUtils::Empty(OUT PendingLogs);
	}

	if ((dataFlags & FLAG_AUDIO_IMPORT) > 0)
	{
		outExternalData << Int(PendingImports.size());
		for (size_t i = 0; i < PendingImports.size(); ++i)
		{
			outExternalData << PendingImports.at(i).File;
			outExternalData << PendingImports.at(i).AudioName;
		}
		ContainerUtils::Empty(OUT PendingImports);
	}

	if ((dataFlags & FLAG_AUDIO_PROPS) > 0)
	{
		outExternalData << Int(PendingAudioProps.size());

		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		for (size_t audioHash : PendingAudioProps)
		{
			const AudioAsset* asset = localAudioPool->GetAudio(audioHash);
			if (asset == nullptr)
			{
				//Add asset zero to indicate there isn't an audio asset, and to skip its properties
				size_t invalidHash = 0;
				outExternalData << invalidHash;
				continue;
			}

			outExternalData << audioHash;
			asset->CopyPropsTo(OUT outExternalData);
		}
		ContainerUtils::Empty(OUT PendingAudioProps);
	}

	if ((dataFlags & FLAG_AUDIO_DESTROY) > 0)
	{
		outExternalData << Int(PendingAudioDestroy.size());
		for (size_t i = 0; i < PendingAudioDestroy.size(); ++i)
		{
			outExternalData << PendingAudioDestroy.at(i);
		}
		ContainerUtils::Empty(OUT PendingAudioDestroy);
	}

	if ((dataFlags & FLAG_SPEED_OF_SOUND) > 0)
	{
		outExternalData << PendingSpeedOfSound;
		PendingSpeedOfSound = -1.f;
	}

	if ((dataFlags & FLAG_MASTER_VOLUME) > 0)
	{
		outExternalData << PendingMasterVolume;
		PendingMasterVolume = -1.f;
	}

	if ((dataFlags & FLAG_CHANNEL_VOLUME) > 0)
	{
		Int numChannels = Int(PendingChannelVolumes.size());
		outExternalData << numChannels;

		for (const SPendingChannelVolume& channelVol : PendingChannelVolumes)
		{
			outExternalData << channelVol.ChannelHash;
			outExternalData << channelVol.NewVolume;
		}
		ContainerUtils::Empty(OUT PendingChannelVolumes);
	}

	if ((dataFlags & FLAG_CHANNEL_DILATION) > 0)
	{
		Int numChannels = Int(PendingChannelDilations.size());
		outExternalData << numChannels;

		for (const SPendingChannelDilation& channelDil : PendingChannelDilations)
		{
			outExternalData << channelDil.ChannelHash;
			outExternalData << channelDil.NewDilation;
		}
		ContainerUtils::Empty(OUT PendingChannelDilations);
	}

	if ((dataFlags & FLAG_DUCK_VOLUME) > 0)
	{
		outExternalData << PendingDuckVolume;
		PendingDuckVolume = -1.f;
	}

	if ((dataFlags & FLAG_DUCK_LERP) > 0)
	{
		outExternalData << PendingDuckLerp;
		PendingDuckLerp = -1.f;
	}

	if ((dataFlags & FLAG_PLAY_STATIC) > 0)
	{
		Int numSounds = Int(PendingStaticSounds.size());
		outExternalData << numSounds;

		for (const SPendingStaticSound& staticSound : PendingStaticSounds)
		{
			outExternalData << staticSound.AssetHash;
			outExternalData << staticSound.DynamicAsset;
			outExternalData << staticSound.Volume;
			outExternalData << staticSound.Pitch;

			if (staticSound.DynamicAsset)
			{
				outExternalData << staticSound.Parameters;
			}
		}
		ContainerUtils::Empty(OUT PendingStaticSounds);
	}

	if ((dataFlags & FLAG_STOP_STATIC) > 0)
	{
		Int numStop = Int(PendingStopStatic.size());
		outExternalData << numStop;

		for (size_t audioHash : PendingStopStatic)
		{
			outExternalData << audioHash;
		}
		ContainerUtils::Empty(OUT PendingStopStatic);
	}

	if ((dataFlags & FLAG_STOP_DYNAMIC) > 0)
	{
		Int numStop = Int(PendingStopDynamic.size());
		outExternalData << numStop;

		for (size_t audioHash : PendingStopDynamic)
		{
			outExternalData << audioHash;
		}
		ContainerUtils::Empty(OUT PendingStopDynamic);
	}

	if ((dataFlags & FLAG_STOP_CHANNEL) > 0)
	{
		Int numStop = Int(PendingStopChannel.size());
		outExternalData << numStop;

		for (size_t channelHash : PendingStopChannel)
		{
			outExternalData << channelHash;
		}
		ContainerUtils::Empty(OUT PendingStopChannel);
	}

	if ((dataFlags & FLAG_STOP_ALL) > 0)
	{
		bPendingStopAll = false;
	}

	if ((dataFlags & FLAG_SOUND_BEGIN) > 0)
	{
		Int numEvents = Int(PendingSoundBeginList.size());
		outExternalData << numEvents;

		for (size_t i = 0; i < PendingSoundBeginList.size(); ++i)
		{
			outExternalData << PendingSoundBeginList.at(i).AssetHash;
			outExternalData << PendingSoundBeginList.at(i).AudioCompId;
			outExternalData << PendingSoundBeginList.at(i).ListenerCompId;
		}
		ContainerUtils::Empty(OUT PendingSoundBeginList);
	}

	if ((dataFlags & FLAG_SOUND_ENDED) > 0)
	{
		Int numEvents = Int(PendingSoundEndedList.size());
		outExternalData << numEvents;

		for (size_t i = 0; i < PendingSoundEndedList.size(); ++i)
		{
			outExternalData << PendingSoundEndedList.at(i).AssetHash;
			outExternalData << PendingSoundEndedList.at(i).AudioCompId;
			outExternalData << PendingSoundEndedList.at(i).ListenerCompId;
		}
		ContainerUtils::Empty(OUT PendingSoundEndedList);
	}
}

void ThreadedSoundTransfer::HandleReceiveData (const DataBuffer& incomingData)
{
	Int dataFlags;
	incomingData >> dataFlags;

	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	if ((dataFlags & FLAG_TERMINATE_ENGINE) > 0)
	{
		Engine* localEngine = Engine::FindEngine();
		localEngine->Shutdown();
	}

	if ((dataFlags & FLAG_LOG_DATA) > 0)
	{
		Int numLogs;
		incomingData >> numLogs;
		for (Int i = 0; i < numLogs; ++i)
		{
			Int intLogLevel;
			incomingData >> intLogLevel;

			LogCategory::ELogLevel logLevel = static_cast<LogCategory::ELogLevel>(intLogLevel.Value);

			DString msg;
			incomingData >> msg;
			SoundLog.Log(logLevel, msg);
		}
	}

	if ((dataFlags & FLAG_AUDIO_IMPORT) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		localAudioPool->SetNotifyExternalPool(false); //Prevent circular importing

		Int numAudio;
		incomingData >> numAudio;
		for (Int i = 0; i < numAudio; ++i)
		{
			FileAttributes file;
			incomingData >> file;

			DString audioName;
			incomingData >> audioName;

			if (localAudioPool->IsOnSoundThread())
			{
				localAudioPool->CreateAndImportAudio(file, HashedString(audioName));
			}
			else
			{
				localAudioPool->CreateBlankAudio(HashedString(audioName));
			}
		}

		localAudioPool->SetNotifyExternalPool(true);
	}

	if ((dataFlags & FLAG_AUDIO_PROPS) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		Int numAssets;
		incomingData >> numAssets;
		for (Int i = 0; i < numAssets; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;
			if (audioHash == 0)
			{
				//This asset was removed from the other thread, this entry wouldn't have data.
				continue;
			}

			AudioAsset* asset = localAudioPool->EditAudio(audioHash);
			CHECK(asset != nullptr)

			asset->CopyPropsFrom(incomingData);
		}
	}

	if ((dataFlags & FLAG_AUDIO_DESTROY) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		localAudioPool->SetNotifyExternalPool(false);

		Int numDestroy;
		incomingData >> numDestroy;
		for (Int i = 0; i < numDestroy; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;
			localAudioPool->DestroyAudio(audioHash);
		}

		localAudioPool->SetNotifyExternalPool(true);
	}

	if ((dataFlags & FLAG_SPEED_OF_SOUND) > 0)
	{
		Float newSpeed;
		incomingData >> newSpeed;
		localAudio->SetSpeedOfSound(newSpeed);
	}

	if ((dataFlags & FLAG_MASTER_VOLUME) > 0)
	{
		Float newVol;
		incomingData >> newVol;
		localAudio->SetMasterVolume(newVol);
	}

	if ((dataFlags & FLAG_CHANNEL_VOLUME) > 0)
	{
		Int numChannels;
		incomingData >> numChannels;
		for (Int i = 0; i < numChannels; ++i)
		{
			size_t channelHash;
			incomingData >> channelHash;

			Float channelVolume;
			incomingData >> channelVolume;

			localAudio->SetChannelVolume(channelHash, channelVolume);
		}
	}

	if ((dataFlags & FLAG_CHANNEL_DILATION) > 0)
	{
		Int numChannels;
		incomingData >> numChannels;
		for (Int i = 0; i < numChannels; ++i)
		{
			size_t channelHash;
			incomingData >> channelHash;

			Float newDilation;
			incomingData >> newDilation;

			localAudio->SetChannelDilation(channelHash, newDilation);
		}
	}

	if ((dataFlags & FLAG_DUCK_VOLUME) > 0)
	{
		Float newDuckVolume;
		incomingData >> newDuckVolume;
		localAudio->SetDuckVolumeMultiplier(newDuckVolume);
	}

	if ((dataFlags & FLAG_DUCK_LERP) > 0)
	{
		Float newDuckLerp;
		incomingData >> newDuckLerp;
		localAudio->SetDuckLerpDuration(newDuckLerp);
	}

	if ((dataFlags & FLAG_PLAY_STATIC) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		Int numSounds;
		incomingData >> numSounds;
		for (Int i = 0; i < numSounds; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			Bool dynamicAsset;
			incomingData >> dynamicAsset;

			Float volume;
			incomingData >> volume;

			Float pitch;
			incomingData >> pitch;

			if (dynamicAsset)
			{
				//Read the parameters early even if the dynamic asset will not create an instance. This is to move the data buffer's reader pointer.
				DataBuffer params;
				incomingData >> params;

				if (const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(localAudioPool->GetAudio(audioHash)))
				{
					if (DynamicAudioInstance* instance = localAudio->SetupDynamicAsset(dynAsset, false, params))
					{
						localAudio->PlayStaticSound(instance, volume, pitch);
						continue;
					}

					//Fallthrough is intended. If the instance is not created, treat it as a normal audio asset.
				}
			}

			if (const AudioAsset* asset = localAudioPool->GetAudio(audioHash))
			{
				localAudio->PlayStaticSound(asset, volume, pitch);
			}
		}
	}

	if ((dataFlags & FLAG_STOP_STATIC) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		Int numStop;
		incomingData >> numStop;
		for (Int i = 0; i < numStop; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			if (const AudioAsset* asset = localAudioPool->GetAudio(audioHash))
			{
				localAudio->StopStaticSounds(asset);
			}
		}
	}

	if ((dataFlags & FLAG_STOP_DYNAMIC) > 0)
	{
		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		Int numStop;
		incomingData >> numStop;
		for (Int i = 0; i < numStop; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			if (const AudioAsset* asset = localAudioPool->GetAudio(audioHash))
			{
				localAudio->StopDynamicSounds(asset);
			}
		}
	}

	if ((dataFlags & FLAG_STOP_CHANNEL) > 0)
	{
		Int numStop;
		incomingData >> numStop;
		for (Int i = 0; i < numStop; ++i)
		{
			size_t channelHash;
			incomingData >> channelHash;
			localAudio->StopSoundsByChannel(channelHash);
		}
	}

	if ((dataFlags & FLAG_STOP_ALL) > 0)
	{
		localAudio->StopAllSounds();
	}

	if ((dataFlags & FLAG_SOUND_BEGIN) > 0)
	{
		AudioAssetPool* audioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(audioPool != nullptr)

		Int numEvents;
		incomingData >> numEvents;

		for (Int i = 0; i < numEvents; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			Int audioId;
			incomingData >> audioId;

			Int listenId;
			incomingData >> listenId;

			if (const AudioAsset* asset = audioPool->GetAudio(audioHash))
			{
				AudioComponent* sourceComp = nullptr;
				if (audioId != 0 && localAudio->ReadAudioComponents().contains(audioId))
				{
					sourceComp = localAudio->ReadAudioComponents().at(audioId);
				}

				PlayerListenerComponent* listenComp = nullptr;
				if (listenId != 0 && localAudio->ReadListenerComponents().contains(listenId))
				{
					listenComp = dynamic_cast<PlayerListenerComponent*>(localAudio->ReadListenerComponents().at(listenId));
				}

				localAudio->ProcessExternalSoundBegin(asset, sourceComp, listenComp);
			}
		}
	}

	if ((dataFlags & FLAG_SOUND_ENDED) > 0)
	{
		AudioAssetPool* audioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(audioPool != nullptr)

		Int numEvents;
		incomingData >> numEvents;

		for (Int i = 0; i < numEvents; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			Int audioId;
			incomingData >> audioId;

			Int listenId;
			incomingData >> listenId;

			if (const AudioAsset* asset = audioPool->GetAudio(audioHash))
			{
				AudioComponent* audioComp = nullptr;
				if (audioId != 0 && localAudio->ReadAudioComponents().contains(audioId))
				{
					audioComp = localAudio->ReadAudioComponents().at(audioId);
				}

				PlayerListenerComponent* listenComp = nullptr;
				if (listenId != 0 && localAudio->ReadListenerComponents().contains(listenId))
				{
					listenComp = dynamic_cast<PlayerListenerComponent*>(localAudio->ReadListenerComponents().at(listenId));
				}

				localAudio->ProcessExternalSoundEnd(asset, audioComp, listenComp);
			}
		}
	}
}

SD_END
#endif