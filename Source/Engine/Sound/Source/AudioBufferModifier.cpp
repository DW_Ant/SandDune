/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferModifier.cpp
=====================================================================
*/

#include "AudioBufferModifier.h"

IMPLEMENT_ABSTRACT_CLASS(SD::AudioBufferModifier, SD::Entity)
SD_BEGIN

void AudioBufferModifier::ResetAudioParameters ()
{
	//Noop
}

bool AudioBufferModifier::HasParamName (const HashedString& paramName) const
{
	return false;
}

bool AudioBufferModifier::SetSoundParameter (const HashedString& paramName, bool paramValue)
{
	return false;
}

bool AudioBufferModifier::SetSoundParameter (const HashedString& paramName, const DString& paramValue)
{
	return false;
}

bool AudioBufferModifier::SetSoundParameter (const HashedString& paramName, Int paramValue)
{
	return false;
}

bool AudioBufferModifier::SetSoundParameter (const HashedString& paramName, Float paramValue)
{
	return false;
}

void AudioBufferModifier::RecursiveExecute (const std::function<void(AudioBufferModifier*)>& lambda)
{
	lambda(this);

	for (AudioBufferModifier* modifier : NextModifiers)
	{
		if (modifier != nullptr)
		{
			modifier->RecursiveExecute(lambda);
		}
	}
}
SD_END