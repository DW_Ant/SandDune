/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioAssetPool.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioEngineComponent.h"
#ifdef WITH_MULTI_THREAD
#include "ThreadedSoundTransfer.h"
#endif

IMPLEMENT_CLASS(SD::AudioAssetPool, SD::ResourcePool)
SD_BEGIN
IMPLEMENT_RESOURCE_POOL(AudioAsset)

void AudioAssetPool::InitProps ()
{
	Super::InitProps();

	bIsOnSoundThread = true;
	bNotifyExternalPool = true;
}

void AudioAssetPool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();
}

void AudioAssetPool::ReleaseResources ()
{
	for (std::pair<size_t, AudioAsset*> asset : ImportedAudio)
	{
		asset.second->Destroy();
	}
	ImportedAudio.clear();
}

void AudioAssetPool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

AudioAsset* AudioAssetPool::CreateAndImportAudio (const FileAttributes& file, const HashedString& audioAssetName)
{
	if (ImportedAudio.contains(audioAssetName.GetHash()))
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Unable to import audio asset from file \"%s\" since the asset name \"%s\" is already in use."), file.GetName(true, true), audioAssetName.ReadString());
		return nullptr;
	}

#ifdef WITH_MULTI_THREAD
	if (bNotifyExternalPool)
	{
		AudioEngineComponent* localAudio = AudioEngineComponent::Find();
		CHECK(localAudio != nullptr)
		if (ThreadedSoundTransfer* transfer = localAudio->GetThreadTransfer())
		{
			transfer->ImportAudio(file, audioAssetName.ReadString());
		}
	}

	if (!bIsOnSoundThread)
	{
		return CreateBlankAudio(audioAssetName);
	}
#endif

	AudioAsset* newAsset = AudioAsset::CreateObject();
	CHECK(newAsset->GetBuffer() != nullptr)

	{
		SfmlOutputStream sfmlOutput;
		if (!newAsset->EditBuffer()->loadFromFile(file.GetName(true, true).ToCString()))
		{
			GetSoundLog().Log(LogCategory::LL_Warning, TXT("Unable to create and import audio. Failed to load from file %s. %s."), file.GetName(true, true), sfmlOutput.ReadOutput());
			newAsset->Destroy();
			return nullptr;
		}
	}

	newAsset->SetAudioName(audioAssetName.ReadString());
	newAsset->SetAudioHash(audioAssetName.GetHash());
	ImportedAudio.insert({newAsset->GetAudioHash(), newAsset});
	return newAsset;
}

AudioAsset* AudioAssetPool::CreateBlankAudio (const HashedString& audioAssetName)
{
	if (ImportedAudio.contains(audioAssetName.GetHash()))
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Unable to create a blank audio since the asset name \"%s\" is already in use."), audioAssetName.ReadString());
		return nullptr;
	}

	AudioAsset* newAsset = AudioAsset::CreateObject();
	newAsset->SetAudioName(audioAssetName.ReadString());
	newAsset->SetAudioHash(audioAssetName.GetHash());
	ImportedAudio.insert({newAsset->GetAudioHash(), newAsset});
	return newAsset;
}

void AudioAssetPool::GetAllAudio (std::vector<AudioAsset*>& outAllAudio)
{
	for (std::pair<size_t, AudioAsset*> asset : ImportedAudio)
	{
		outAllAudio.push_back(asset.second);
	}
}

const AudioAsset* AudioAssetPool::GetAudio (const HashedString& audioName, bool bLogOnMissing) const
{
	const AudioAsset* result = GetAudio(audioName.GetHash());
	if (result == nullptr && bLogOnMissing)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("The Audio Asset \"%s\" does not exist in the AudioAssetPool."), audioName.ReadString());
	}
	
	return result;
}

const AudioAsset* AudioAssetPool::GetAudio (size_t audioHash) const
{
	if (ImportedAudio.contains(audioHash))
	{
		return ImportedAudio.at(audioHash);
	}

	return nullptr;
}

AudioAsset* AudioAssetPool::EditAudio (const HashedString& audioName, bool bLogOnMissing)
{
	AudioAsset* result = EditAudio(audioName.GetHash());
	if (result == nullptr && bLogOnMissing)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("The Audio Asset \"%s\" does not exist in the AudioAssetPool."), audioName.ReadString());
	}
	
	return result;
}

AudioAsset* AudioAssetPool::EditAudio (size_t audioHash)
{
	if (ImportedAudio.contains(audioHash))
	{
		return ImportedAudio.at(audioHash);
	}

	return nullptr;
}

void AudioAssetPool::DestroyAudio (const HashedString& audioName)
{
	DestroyAudio(audioName.GetHash());
}

void AudioAssetPool::DestroyAudio (size_t audioHash)
{
	if (ImportedAudio.contains(audioHash))
	{
#ifdef WITH_MULTI_THREAD
		if (bNotifyExternalPool)
		{
			AudioEngineComponent* localAudio = AudioEngineComponent::Find();
			CHECK(localAudio != nullptr)
			if (ThreadedSoundTransfer* transfer = localAudio->GetThreadTransfer())
			{
				transfer->DestroyAudio(audioHash);
			}
		}
#endif

		ImportedAudio.at(audioHash)->Destroy();
		ImportedAudio.erase(audioHash);
	}
}

void AudioAssetPool::SetIsOnSoundThread (bool newIsOnSoundThread)
{
	bIsOnSoundThread = newIsOnSoundThread;
}

void AudioAssetPool::SetNotifyExternalPool (bool newNotifyExternalPool)
{
	bNotifyExternalPool = newNotifyExternalPool;
}
SD_END