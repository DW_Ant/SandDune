/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundListenerComponent.cpp
=====================================================================
*/

#include "AudioEngineComponent.h"
#include "SoundListenerComponent.h"

IMPLEMENT_ABSTRACT_CLASS(SD::SoundListenerComponent, SD::EntityComponent)
SD_BEGIN

void SoundListenerComponent::InitProps ()
{
	Super::InitProps();

	ListenerId = 0;
	OwnerTransform = nullptr;
}

void SoundListenerComponent::BeginObject ()
{
	Super::BeginObject();

	if (ShouldGenerateId())
	{
		GenerateId();

		AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
		CHECK(localAudioEngine != nullptr)
		localAudioEngine->RegisterListenerComponent(this);
	}
}

bool SoundListenerComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (FindSceneTransform(ownerCandidate) != nullptr);
}

void SoundListenerComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwnerTransform = FindSceneTransform(newOwner);

	Entity* transformEntity = dynamic_cast<Entity*>(OwnerTransform);
	CHECK(transformEntity != nullptr) //FindSceneTransform should only find a SceneTransform from either an Entity or EntityComponents

	//Register callbacks to each component between owner transform and this component in case any of those components changed owners
	for (Entity* owner = newOwner; owner != nullptr; /* Noop */ )
	{
		if (owner == transformEntity)
		{
			//Found the transform Entity. No need to register callbacks to it and its owner.
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(owner))
		{
			comp->OnOwnerChanged.RegisterHandler(SDFUNCTION_2PARAM(this, SoundListenerComponent, HandleOwnerChanged, void, EntityComponent*, Entity*));
			owner = comp->GetOwner();
			continue;
		}

		CHECK(false) //Should never happen. transformEntity should be somewhere in the component chain. If we reached the Entity without finding the transformEntity, then FindSceneTransform found some Entity that doesn't exist in the component chain.
	}

	OwnerTransformChanged();
}

void SoundListenerComponent::ComponentDetached ()
{
	if (OwnerTransform != nullptr)
	{
		//Unregister its callback from the old component chain
		Entity* transformEntity = dynamic_cast<Entity*>(OwnerTransform);
		if (transformEntity != nullptr)
		{
			for (Entity* owner = GetOwner(); owner != nullptr; /* Noop */ )
			{
				if (owner == transformEntity)
				{
					break;
				}

				if (EntityComponent* comp = dynamic_cast<EntityComponent*>(owner))
				{
					comp->OnOwnerChanged.UnregisterHandler(SDFUNCTION_2PARAM(this, SoundListenerComponent, HandleOwnerChanged, void, EntityComponent*, Entity*));
					owner = comp->GetOwner();
					continue;
				}

				break;
			}
		}

		OwnerTransform = nullptr;
		OwnerTransformChanged();
	}

	Super::ComponentDetached();
}

void SoundListenerComponent::Destroyed ()
{
	if (AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find())
	{
		localAudioEngine->UnregisterListenerComponent(this);
	}

	Super::Destroyed();
}

bool SoundListenerComponent::ShouldGenerateId () const
{
	return true;
}

void SoundListenerComponent::GenerateId ()
{
	static Int nextId = 1;
	ListenerId = nextId;
	++nextId;
}

SceneTransform* SoundListenerComponent::FindSceneTransform (Entity* startFrom) const
{
	//Check the component chain
	for (Entity* owner = startFrom; owner != nullptr; /* Noop */ )
	{
		if (SceneTransform* transform = dynamic_cast<SceneTransform*>(owner))
		{
			return transform;
		}

		EntityComponent* comp = dynamic_cast<EntityComponent*>(owner);
		if (comp == nullptr)
		{
			return nullptr; //End of the chain
		}

		owner = comp->GetOwner();
	}

	return nullptr;
}

void SoundListenerComponent::OwnerTransformChanged ()
{
	//Noop
}

void SoundListenerComponent::HandleOwnerChanged (EntityComponent* componentThatChanged, Entity* prevOwner)
{
	CHECK(componentThatChanged != nullptr)

	//Unregister all callbacks between prevOwner and the TransformOwner
	for (Entity* curEntity = prevOwner; curEntity != nullptr; /* Noop */ )
	{
		if (dynamic_cast<SceneTransform*>(curEntity) != nullptr)
		{
			//Found a scene transform. This Entity and anything above it will not have the callback.
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(curEntity))
		{
			comp->OnOwnerChanged.UnregisterHandler(SDFUNCTION_2PARAM(this, SoundListenerComponent, HandleOwnerChanged, void, EntityComponent*, Entity*));
			curEntity = comp->GetOwner();
			continue;
		}

		break;
	}

	SceneTransform* oldTransform = OwnerTransform;
	OwnerTransform = FindSceneTransform(componentThatChanged);

	if (Entity* transformEntity = dynamic_cast<Entity*>(OwnerTransform)) //This can be null if the componentThatChanged detached
	{
		//Register callbacks between OwnerTransform and the componentThatChanged
		for (Entity* owner = componentThatChanged->GetOwner(); owner != nullptr; /* Noop */ )
		{
			if (owner == transformEntity)
			{
				break;
			}

			if (EntityComponent* comp = dynamic_cast<EntityComponent*>(owner))
			{
				comp->OnOwnerChanged.RegisterHandler(SDFUNCTION_2PARAM(this, SoundListenerComponent, HandleOwnerChanged, void, EntityComponent*, Entity*));
				owner = comp->GetOwner();
				continue;
			}

			CHECK(false) //Should never happen since the transformEntity should exist somewhere in the component chain. Otherwise the FindSceneTransform found a SceneTransform that's not in the component chain.
		}
	}

	if (oldTransform != OwnerTransform)
	{
		OwnerTransformChanged();
	}
}
SD_END