/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundMode.cpp
=====================================================================
*/

#include "AudioEngineComponent.h"
#include "SoundMode.h"

IMPLEMENT_CLASS(SD::SoundMode, SD::Entity)
SD_BEGIN

const Int SoundMode::FLAG_LEVEL_ADJUST	(0x00000001);
const Int SoundMode::FLAG_ACTIVATE		(0x00000002);
const Int SoundMode::FLAG_DEACTIVATE	(0x00000004);

SoundMode::SPendingLevelAdjust::SPendingLevelAdjust (size_t inChannelHash, Float inVolume) :
	ChannelHash(inChannelHash),
	Volume(inVolume)
{
	//Noop
}

void SoundMode::InitProps ()
{
	Super::InitProps();

	TransitionTick = nullptr;
	Strength = -1.f;
#ifdef WITH_MULTI_THREAD
	ThreadComp = nullptr;
	bInSoundThread = false;
	MaxWaitTime = 0.5f;
	PendingFlags = 0;
	PendingActivationTime = -1.f;
	PendingDeactivationTime = -1.f;
#endif

	StartTransitionTime = -1.f;
	TransitionDuration = -1.f;
	StrengthSource = -1.f;
	StrengthDestination = -1.f;
}

void SoundMode::BeginObject ()
{
	Super::BeginObject();

	TransitionTick = TickComponent::CreateObject(TICK_GROUP_SOUND);
	if (AddComponent(TransitionTick))
	{
		TransitionTick->SetTickHandler(SDFUNCTION_1PARAM(this, SoundMode, HandleTransitionTick, void, Float));
		TransitionTick->SetTicking(false);
	}

#ifdef WITH_MULTI_THREAD
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)
	bInSoundThread = localAudioEngine->IsMainAudioEngine();

	if (!bInSoundThread)
	{
		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, SoundMode, HandleCopyData, void, DataBuffer&));
			ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(this, SoundMode, HandleReceiveData, void, const DataBuffer&));
			ThreadComp->InstantiateExternalComp(MULTI_THREAD_SOUND_IDX, []() -> ThreadedComponent*
			{
				SoundMode* otherSoundMode = SoundMode::CreateObject();

				ThreadedComponent* result = ThreadedComponent::CreateObject();
				if (!otherSoundMode->AddComponent(result))
				{
					otherSoundMode->Destroy();
					return nullptr;
				}

				result->SetCopyMethod(SDFUNCTION_1PARAM(otherSoundMode, SoundMode, HandleCopyData, void, DataBuffer&));
				result->SetOnNewData(SDFUNCTION_1PARAM(otherSoundMode, SoundMode, HandleReceiveData, void, const DataBuffer&));
				result->SetOnDisconnect(std::function<void(ThreadedComponent*)>([](ThreadedComponent* owningComp)
				{
					Entity* rootEntity = owningComp->GetRootEntity();
					if (rootEntity != nullptr)
					{
						rootEntity->Destroy();
					}
				}));

				return result;
			});
		}
	}
#endif
}

void SoundMode::Destroyed ()
{
	if (Strength > 0.f)
	{
		RemoveSoundMode();
	}

	Super::Destroyed();
}

void SoundMode::SetLevelAdjust (const DString& channelName, Float channelVolume)
{
	SetLevelAdjust(channelName.GenerateHash(), channelVolume);
}

void SoundMode::SetLevelAdjust (size_t channelHash, Float channelVolume)
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread && ThreadComp != nullptr)
	{
		PendingFlags |= FLAG_LEVEL_ADJUST;
		PendingLevelAdjust.emplace_back(SPendingLevelAdjust(channelHash, channelVolume));
		ThreadComp->RequestDataTransfer(MaxWaitTime);
		return;
	}
#endif

	if (ChannelVolumes.contains(channelHash))
	{
		ChannelVolumes.at(channelHash) = channelVolume;
	}
	else
	{
		ChannelVolumes.insert({channelHash, channelVolume});
	}

	if (Strength > 0.f)
	{
		AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
		CHECK(localAudioEngine != nullptr)
		localAudioEngine->RefreshSoundModeVolumes();
	}
}

void SoundMode::Activate (Float transitionTime)
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread && ThreadComp != nullptr)
	{
		PendingFlags |= FLAG_ACTIVATE;
		PendingActivationTime = transitionTime;
		ThreadComp->RequestDataTransfer(MaxWaitTime);
		return;
	}
#endif

	if (Strength >= 1.f)
	{
		return;
	}

	if (Strength <= 0.f)
	{
		AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
		localAudioEngine->RegisterSoundMode(this);
	}

	StartTransition(transitionTime, 1.f);
}

void SoundMode::Deactivate (Float transitionTime)
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread && ThreadComp != nullptr)
	{
		PendingFlags |= FLAG_DEACTIVATE;
		PendingDeactivationTime = transitionTime;
		ThreadComp->RequestDataTransfer(MaxWaitTime);
		return;
	}
#endif

	if (Strength > 0.f)
	{
		StartTransition(transitionTime, 0.f);
	}
}

#ifdef WITH_MULTI_THREAD
void SoundMode::SetMaxWaitTime (Float newMaxWaitTime)
{
	MaxWaitTime = newMaxWaitTime;
}
#endif

void SoundMode::StartTransition (Float transitionTime, Float desiredStrength)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	StartTransitionTime = localEngine->GetElapsedTime();
	TransitionDuration = transitionTime;
	StrengthSource = Strength;
	StrengthDestination = desiredStrength;

	if (transitionTime <= 0.f)
	{
		AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
		CHECK(localAudioEngine != nullptr)
		Strength = desiredStrength;
		if (Strength <= 0.f)
		{
			localAudioEngine->UnregisterSoundMode(this);
		}

		localAudioEngine->RefreshSoundModeVolumes();
		TransitionTick->SetTicking(false);
	}
	else if (TransitionTick != nullptr)
	{
		TransitionTick->SetTicking(true);
	}
}

void SoundMode::RemoveSoundMode ()
{
	if (AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find())
	{
		localAudioEngine->UnregisterSoundMode(this);
	}
}

void SoundMode::HandleTransitionTick (Float deltaSec)
{
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	if (localAudio == nullptr || localAudio->GetOwningEngine() == nullptr)
	{
		//AudioEngine destroyed
		Strength = 0.f;
		TransitionTick->SetTicking(false);
		return;
	}

	Float curTime = localAudio->GetOwningEngine()->GetElapsedTime();
	Float ratio = (curTime - StartTransitionTime) / TransitionDuration;
	if (ratio >= 1.f)
	{
		Strength = StrengthDestination;
	}
	else
	{
		Strength = Utils::Lerp(ratio, StrengthSource, StrengthDestination);
	}

	localAudio->RefreshSoundModeVolumes();

	if (ratio >= 1.f)
	{
		TransitionTick->SetTicking(false);
		if (Strength <= 0.f)
		{
			RemoveSoundMode();
		}
	}
}

#ifdef WITH_MULTI_THREAD
void SoundMode::HandleCopyData (DataBuffer& outData)
{
	outData << PendingFlags;

	if ((PendingFlags & FLAG_LEVEL_ADJUST) > 0)
	{
		outData << Int(PendingLevelAdjust.size());

		for (const SPendingLevelAdjust& adjust : PendingLevelAdjust)
		{
			outData << adjust.ChannelHash;
			outData << adjust.Volume;
		}
		ContainerUtils::Empty(OUT PendingLevelAdjust);
	}

	if ((PendingFlags & FLAG_ACTIVATE) > 0)
	{
		outData << PendingActivationTime;
	}

	if ((PendingFlags & FLAG_DEACTIVATE) > 0)
	{
		outData << PendingDeactivationTime;
	}

	PendingFlags = 0;
}

void SoundMode::HandleReceiveData (const DataBuffer& incomingData)
{
	Int dataFlags;
	incomingData >> dataFlags;

	if ((dataFlags & FLAG_LEVEL_ADJUST) > 0)
	{
		Int numLevels;
		incomingData >> numLevels;

		for (Int i = 0; i < numLevels; ++i)
		{
			size_t channelHash;
			incomingData >> channelHash;

			Float volume;
			incomingData >> volume;

			SetLevelAdjust(channelHash, volume);
		}
	}

	if ((dataFlags & FLAG_ACTIVATE) > 0)
	{
		Float transitionTime;
		incomingData >> transitionTime;
		Activate(transitionTime);
	}

	if ((dataFlags & FLAG_DEACTIVATE) > 0)
	{
		Float transitionTime;
		incomingData >> transitionTime;
		Deactivate(transitionTime);
	}
}
#endif
SD_END