/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DopplerEffect.cpp
=====================================================================
*/

#include "AudioComponent.h"
#include "DopplerEffect.h"
#include "PlayerListenerComponent.h"

SD_BEGIN
Float DopplerEffect::SpeedOfSound(34029.f);
Range<Float> DopplerEffect::PitchRange(0.25f, 4.f);

DopplerEffect::DopplerEffect () :
	Source(nullptr),
	Listener(nullptr),
	LastDopplerMultiplier(1.f),
	DopplerDeltaPos(0.f)
{
	//Noop
}

Float DopplerEffect::CalcEffect (Float deltaSec)
{
	if (Source != nullptr && Listener != nullptr)
	{
		CHECK(Source->GetOwnerTransform() != nullptr && Listener->GetOwnerTransform() != nullptr)
		Float newDist = (Source->GetOwnerTransform()->ReadAbsTranslation() - Listener->GetOwnerTransform()->ReadAbsTranslation()).VSize();
				
		//Update pitch if all three elements have doppler enabled
		if (Source->IsDopplerEnabled() && Listener->IsDopplerEnabled() && deltaSec != 0.f)
		{
			Float deltaSize = (DopplerDeltaPos - newDist) / deltaSec;

			CHECK(SpeedOfSound != 0.f)
			//Want ratio to range between -SpeedOfSound to +SpeedOfSound:  If deltaSize is at -SpeedOfSound, the ratio is 0. If deltaSize is 0, the ratio is 0.5. If deltaSize is +SpeedOfSound, the ratio is 1.
			Float ratio = (SpeedOfSound + deltaSize) / (SpeedOfSound * 2.f);

			if (ratio > 0.f)
			{
				//Formula: 4x^2.  Key factors: When the ratio is 0.5, the pitch must be 1 since that's the halfway point. When the ratio is 0, the pitch must be a really low number (it's clamped). When the ratio is 1, the pitch is set to 4 (some arbitrary really high pitch).
				LastDopplerMultiplier = (4.f * ratio * ratio);

				//Clamp multiplier in case the object is traveling faster than the speed of sound.
				LastDopplerMultiplier = PitchRange.GetClampedValue(LastDopplerMultiplier);
			}
			else
			{
				LastDopplerMultiplier = PitchRange.Min;
			}
		}
				
		//Still update DopplerDeltaPos even if doppler is disabled. This prevents sudden pitch shift from vast position changes.
		DopplerDeltaPos = newDist;
		Source->DecrementDopplerCounter();
		Listener->DecrementDopplerCounter();
	}

	return LastDopplerMultiplier;
}

void DopplerEffect::SetSource (AudioComponent* newSource)
{
	Source = newSource;
	LastDopplerMultiplier = 1.f;

	//Update DopplerDeltaPos
	CalcEffect(0.f);
}

void DopplerEffect::SetListener (PlayerListenerComponent* newListener)
{
	Listener = newListener;
	LastDopplerMultiplier = 1.f;

	//Update DopplerDeltaPos
	CalcEffect(0.f);
}
SD_END