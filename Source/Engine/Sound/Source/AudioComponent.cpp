/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioComponent.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"
#include "DynamicSound.h"
#ifdef WITH_MULTI_THREAD
#include "GradualTranslator.h"
#endif
#include "PlayerListenerComponent.h"
#include "SoundEvent.h"
#include "SoundPlaybackInterface.h"
#ifdef WITH_MULTI_THREAD
#include "ThreadedDynamicAudioInstance.h"
#endif

IMPLEMENT_CLASS(SD::AudioComponent, SD::EntityComponent)
SD_BEGIN

#ifdef WITH_MULTI_THREAD
const Int AudioComponent::FLAG_COPY_ID				(0x00000001);
const Int AudioComponent::FLAG_COPY_PROPS			(0x00000002);
const Int AudioComponent::FLAG_COPY_POSITION		(0x00000004);
const Int AudioComponent::FLAG_TELEPORT_POS			(0x00000008);
const Int AudioComponent::FLAG_PLAY_SOUND			(0x00000010);
const Int AudioComponent::FLAG_DETACH_SOUND			(0x00000020);
const Int AudioComponent::FLAG_INTERRUPT_SOUND		(0x00000040);
const Int AudioComponent::FLAG_INTERRUPT_ASSET		(0x00000080);
const Int AudioComponent::FLAG_INTERRUPT_CHANNEL	(0x00000100);
#endif

AudioComponent::SPendingSound::SPendingSound (size_t inAssetHash, Bool inIsDynamicAsset, Float inVolume, Bool inDetachedSound) :
	AssetHash(inAssetHash),
	IsDynamicAsset(inIsDynamicAsset),
	Volume(inVolume),
	bDetachedSound(inDetachedSound)
{
	//Noop
}

AudioComponent::SPendingSound::SPendingSound (const SPendingSound& cpyObj) :
	AssetHash(cpyObj.AssetHash),
	IsDynamicAsset(cpyObj.IsDynamicAsset),
	Volume(cpyObj.Volume),
	bDetachedSound(cpyObj.bDetachedSound)
{
	cpyObj.AudioParams.CopyBufferTo(OUT AudioParams);
}

void AudioComponent::SPendingSound::operator= (const SPendingSound& cpyObj)
{
	AssetHash = cpyObj.AssetHash;
	IsDynamicAsset = cpyObj.IsDynamicAsset;
	Volume = cpyObj.Volume;
	bDetachedSound = cpyObj.bDetachedSound;
	cpyObj.AudioParams.CopyBufferTo(OUT AudioParams);
}

void AudioComponent::InitProps ()
{
	Super::InitProps();

	AudioCompId = 0;
	//Mirror the defaults from the default SoundEvent.
	InnerRadius = 1000.f; //10 meters
	OuterRadius = 10000.f; //100 meters
	VolumeMultiplier = 1.f;
	PitchMultiplier = 1.f;
	bEnableDoppler = true;
	TeleportDopplerCount = 0;
	OwnerTransform = nullptr;
	bPlayingDetachedSound = false;

#ifdef WITH_MULTI_THREAD
	ThreadComp = nullptr;
	bInSoundThread = true;
	MaxWaitTime = 0.33f;
	PendingFlags = 0;
#endif
}

void AudioComponent::BeginObject ()
{
	Super::BeginObject();

#ifdef WITH_MULTI_THREAD
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	if (!localAudio->IsMainAudioEngine())
	{
		bInSoundThread = false;

		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, AudioComponent, HandleCopyData, void, DataBuffer&));
			ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(this, AudioComponent, HandleReceiveData, void, const DataBuffer&));

			//Instantiate a ThreadedSoundTransfer on the sound thread
			ThreadComp->InstantiateExternalComp(MULTI_THREAD_SOUND_IDX, []
			{
				//This method is executed on the sound thread
				GradualTranslator* translator = GradualTranslator::CreateObject();
				AudioComponent* otherAudio = AudioComponent::CreateObject();
				ThreadedComponent* otherThreadComp = nullptr;
				if (translator->AddComponent(otherAudio))
				{
					translator->SetMoveTime(otherAudio->MaxWaitTime);
					otherAudio->bInSoundThread = true;

					otherThreadComp = ThreadedComponent::CreateObject();
					if (otherAudio->AddComponent(otherThreadComp))
					{
						otherThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(otherAudio, AudioComponent, HandleCopyData, void, DataBuffer&));
						otherThreadComp->SetOnNewData(SDFUNCTION_1PARAM(otherAudio, AudioComponent, HandleReceiveData, void, const DataBuffer&));

						//The dummyTransform Entity should be destroyed whenever the ThreadComponents are disconnected.
						otherThreadComp->SetOnDisconnect(std::function<void(ThreadedComponent*)>([](ThreadedComponent* delegateOwner)
						{
							Entity* rootEntity = delegateOwner->GetRootEntity();
							if (rootEntity != nullptr)
							{
								//This would destroy this ThreadedComponent and the AudioComponent between the root Entity and this.
								rootEntity->Destroy();
							}
						}));
					}
				}

				return otherThreadComp;
			});
		}

		//Generate an ID on the main thread, copy its ID to the other thread component on the sound thread.
		GenerateAudioCompId();
	}
#endif

	//In multi threaded mode, the ID is only generated if it's in the main thread. In single threaded mode, always generate an ID.
	if (!localAudio->IsRunningMultiThreadMode())
	{
		GenerateAudioCompId();
	}
}

bool AudioComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<SceneTransform*>(ownerCandidate) != nullptr);
}

void AudioComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwnerTransform = dynamic_cast<SceneTransform*>(newOwner);
	CHECK(OwnerTransform != nullptr)

#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread)
	{
		OwnerTransform->OnTransformChanged.RegisterHandler(SDFUNCTION(this, AudioComponent, HandleAbsTransformChanged, void));
		PendingFlags |= FLAG_COPY_POSITION;
		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(true);
		}
	}
#endif
}

void AudioComponent::ComponentDetached ()
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread)
	{
		SDFunction<void> transChangedCallback = SDFUNCTION(this, AudioComponent, HandleAbsTransformChanged, void);
		if (OwnerTransform != nullptr && OwnerTransform->OnTransformChanged.IsRegistered(transChangedCallback, true))
		{
			OwnerTransform->OnTransformChanged.UnregisterHandler(transChangedCallback);
		}
	}
#endif

	DetachFromActiveSounds();
	OwnerTransform = nullptr;

	Super::ComponentDetached();
}

void AudioComponent::Destroyed ()
{
	InterruptSounds();

	if (AudioEngineComponent* localAudio = AudioEngineComponent::Find())
	{
#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			//Notify PlayerListeners to remove this component since this is about to be destroyed (don't wait for the thread callback since the garbage collector may run before the thread callback happens).
			for (const std::pair<Int, SoundListenerComponent*>& listener : localAudio->ReadListenerComponents())
			{
				if (PlayerListenerComponent* playerListen = dynamic_cast<PlayerListenerComponent*>(listener.second))
				{
					playerListen->RemoveSourceFromDoppler(this);
				}
			}
		}
#endif

		localAudio->UnregisterAudioComponent(this);
	}

	Super::Destroyed();
}

void AudioComponent::PlaySound (const AudioAsset* asset, Float volumeMultiplier)
{
#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread && asset != nullptr)
	{
		PendingFlags |= FLAG_PLAY_SOUND;
		const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(asset);
		SPendingSound& soundEvent = PendingSounds.emplace_back(asset->GetAudioHash(), (dynAsset != nullptr), volumeMultiplier, bPlayingDetachedSound);
		if (dynAsset != nullptr)
		{
			ThreadedDynamicAudioInstance* threadAsset = ThreadedDynamicAudioInstance::CreateObject();
			threadAsset->SetOwningAsset(dynAsset);

			AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
			CHECK(localAudioEngine != nullptr)

			//Populate the param record
			for (SoundPlaybackInterface* playback : localAudioEngine->ReadPlaybackInterfaces())
			{
				playback->SetAudioParameters(threadAsset);
			}

			threadAsset->ReadParamRecords().CopyBufferTo(OUT soundEvent.AudioParams);
			threadAsset->Destroy();
		}

		ThreadComp->RequestDataTransfer(true);
		//Fallthrough is intentional. Although the sound will be played in the sound thread, the HearSound should be handled in both threads since the SoundListeners related to AI is probably not in the sound thread.
	}
#endif

	SoundEvent newSound(asset, this, InnerRadius, OuterRadius, VolumeMultiplier * volumeMultiplier, PitchMultiplier);
	newSound.BroadcastSoundEvent();
}

void AudioComponent::TeleportAudioComp ()
{
#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread)
	{
		//Force the copy position, too, since doppler calculations relies on an updated position.
		PendingFlags |= (FLAG_COPY_POSITION | FLAG_TELEPORT_POS);
		ThreadComp->RequestDataTransfer(true);
		return;
	}
#endif

	//Figure out how many active sounds this component is responsible for.
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	TeleportDopplerCount = 0;
	for (DynamicSound* dynSound : localAudio->ReadDynamicSounds())
	{
		if (dynSound->IsActive() && dynSound->ReadEvent().GetSource() == this)
		{
			TeleportDopplerCount++;
		}
	}
}

void AudioComponent::PlayDetachedSound (const AudioAsset* asset, Float volumeMultiplier)
{
	bPlayingDetachedSound = true;
	PlaySound(asset, volumeMultiplier);
	bPlayingDetachedSound = false;
}

void AudioComponent::DetachFromActiveSounds ()
{
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread)
	{
		//Notify PlayerListeners about this call since they are calculating the DopplerEffect on this thread.
		for (const std::pair<Int, SoundListenerComponent*>& listener : localAudioEngine->ReadListenerComponents())
		{
			if (PlayerListenerComponent* playerListen = dynamic_cast<PlayerListenerComponent*>(listener.second))
			{
				playerListen->RemoveSourceFromDoppler(this);
			}
		}

		PendingFlags |= FLAG_DETACH_SOUND;
		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(true);
		}
		
		return;
	}
#endif

	localAudioEngine->DetachSoundsFromSource(this);
}

void AudioComponent::InterruptSounds ()
{
#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread)
	{
		PendingFlags |= FLAG_INTERRUPT_SOUND;
		ThreadComp->RequestDataTransfer(true);
		return;
	}
#endif

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	if (localAudioEngine != nullptr)
	{
		localAudioEngine->StopDynamicSounds(this);
	}
}

void AudioComponent::InterruptSounds (const AudioAsset* targetAsset)
{
	if (targetAsset == nullptr)
	{
		InterruptSounds(); //No asset specified? Stop all sounds produced from this component.
		return;
	}

#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread)
	{
		PendingFlags |= FLAG_INTERRUPT_ASSET;
		PendingInterruptions.push_back(targetAsset->GetAudioHash());
		ThreadComp->RequestDataTransfer(true);
		return;
	}
#endif

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	if (localAudioEngine != nullptr)
	{
		localAudioEngine->StopDynamicSounds(targetAsset, this);
	}
}

void AudioComponent::InterruptSoundsByChannel (const DString& channelName)
{
	size_t channelHash = channelName.GenerateHash();
	InterruptSoundsByChannel(channelHash);
}

void AudioComponent::InterruptSoundsByChannel (size_t channelHash)
{
#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread)
	{
		PendingFlags |= FLAG_INTERRUPT_CHANNEL;
		PendingInterruptChannels.push_back(channelHash);
		ThreadComp->RequestDataTransfer(true);
		return;
	}
#endif

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	if (localAudioEngine != nullptr)
	{
		localAudioEngine->StopDynamicSounds(this, channelHash);
	}
}

void AudioComponent::SetInnerRadius (Float newInnerRadius)
{
	InnerRadius = newInnerRadius;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

void AudioComponent::SetOuterRadius (Float newOuterRadius)
{
	OuterRadius = newOuterRadius;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

void AudioComponent::SetVolumeMultiplier (Float newVolumeMultiplier)
{
	VolumeMultiplier = newVolumeMultiplier;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

void AudioComponent::SetPitchMultiplier (Float newPitchMultiplier)
{
	PitchMultiplier = newPitchMultiplier;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

void AudioComponent::SetEnableDoppler (bool newEnableDoppler)
{
	bEnableDoppler = newEnableDoppler;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

#ifdef WITH_MULTI_THREAD
void AudioComponent::SetMaxWaitTime (Float newMaxWaitTime)
{
	MaxWaitTime = newMaxWaitTime;

	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}
#endif

void AudioComponent::GenerateAudioCompId ()
{
	//Should be thread safe since only one of the threads will run this function.
	static Int nextId = 0;
	++nextId;
	AudioCompId = nextId;

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)
	localAudioEngine->RegisterAudioComponent(this);

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_ID;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(true);
	}
#endif
}

void AudioComponent::DecrementDopplerCounter ()
{
	if (--TeleportDopplerCount < 0)
	{
		TeleportDopplerCount = 0;
	}
}

#ifdef WITH_MULTI_THREAD
void AudioComponent::HandleAbsTransformChanged ()
{
	PendingFlags |= FLAG_COPY_POSITION;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void AudioComponent::HandleCopyData (DataBuffer& outExternalData)
{
	if (OwnerTransform == nullptr)
	{
		//Remove the update position flag in case a position was updated before it was severed.
		PendingFlags &= (~FLAG_COPY_POSITION);
	}

	outExternalData << PendingFlags;

	if ((PendingFlags & FLAG_COPY_ID) > 0)
	{
		outExternalData << AudioCompId;
	}

	if ((PendingFlags & FLAG_COPY_PROPS) > 0)
	{
		outExternalData << InnerRadius;
		outExternalData << OuterRadius;
		outExternalData << VolumeMultiplier;
		outExternalData << PitchMultiplier;
		outExternalData << Bool(bEnableDoppler);
		outExternalData << MaxWaitTime;
	}

	if ((PendingFlags & FLAG_COPY_POSITION) > 0)
	{
		CHECK(OwnerTransform != nullptr)
		outExternalData << OwnerTransform->ReadAbsTranslation();
	}

	if ((PendingFlags & FLAG_PLAY_SOUND) > 0)
	{
		Int numSounds = Int(PendingSounds.size());
		outExternalData << numSounds;

		for (const SPendingSound& sound : PendingSounds)
		{
			outExternalData << sound.AssetHash;
			outExternalData << sound.IsDynamicAsset;
			outExternalData << sound.Volume;
			outExternalData << sound.bDetachedSound;

			if (sound.IsDynamicAsset)
			{
				outExternalData << sound.AudioParams;
			}
		}
		ContainerUtils::Empty(OUT PendingSounds);
	}

	if ((PendingFlags & FLAG_INTERRUPT_ASSET) > 0)
	{
		Int numAssets = Int(PendingInterruptions.size());
		outExternalData << numAssets;

		for (size_t i = 0; i < PendingInterruptions.size(); ++i)
		{
			outExternalData << PendingInterruptions.at(i);
		}
		ContainerUtils::Empty(OUT PendingInterruptions);
	}

	if ((PendingFlags & FLAG_INTERRUPT_CHANNEL) > 0)
	{
		Int numChannels = Int(PendingInterruptChannels.size());
		outExternalData << numChannels;

		for (size_t i = 0; i < PendingInterruptChannels.size(); ++i)
		{
			outExternalData << PendingInterruptChannels.at(i);
		}
		ContainerUtils::Empty(OUT PendingInterruptChannels);
	}

	PendingFlags = 0;
}

void AudioComponent::HandleReceiveData (const DataBuffer& incomingData)
{
	Int dataFlags;
	incomingData >> dataFlags;

	if ((dataFlags & FLAG_COPY_ID) > 0)
	{
		AudioEngineComponent* localAudio = AudioEngineComponent::Find();
		CHECK(localAudio != nullptr)

		localAudio->UnregisterAudioComponent(this);
		incomingData >> AudioCompId;
		localAudio->RegisterAudioComponent(this);
	}

	if ((dataFlags & FLAG_COPY_PROPS) > 0)
	{
		incomingData >> InnerRadius;
		incomingData >> OuterRadius;
		incomingData >> VolumeMultiplier;
		incomingData >> PitchMultiplier;

		Bool dopplerFlag;
		incomingData >> dopplerFlag;
		bEnableDoppler = dopplerFlag;

		Float newMaxWaitTime;
		incomingData >> newMaxWaitTime;
		if (newMaxWaitTime != MaxWaitTime)
		{
			MaxWaitTime = newMaxWaitTime;
			if (GradualTranslator* translator = dynamic_cast<GradualTranslator*>(GetRootEntity()))
			{
				translator->SetMoveTime(MaxWaitTime);
			}
		}
	}

	if ((dataFlags & FLAG_COPY_POSITION) > 0)
	{
		Vector3 newAbsPos;
		incomingData >> newAbsPos;

		if (GradualTranslator* translator = dynamic_cast<GradualTranslator*>(OwnerTransform))
		{
			//The dummy scene Entity shouldn't be relative to anything so setting this to the absPos should be okay.
			translator->SetDestination(newAbsPos);
		}
	}

	if ((dataFlags & FLAG_TELEPORT_POS) > 0)
	{
		TeleportAudioComp();
	}

	if ((dataFlags & FLAG_PLAY_SOUND) > 0)
	{
		Int numSounds;
		incomingData >> numSounds;

		AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudioPool != nullptr)

		for (Int i = 0; i < numSounds; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			Bool isDynAsset;
			incomingData >> isDynAsset;

			Float volume;
			incomingData >> volume;

			Bool bDetachedSound;
			incomingData >> bDetachedSound;

			if (isDynAsset)
			{
				DataBuffer params;
				incomingData >> params;

				if (const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(localAudioPool->GetAudio(audioHash)))
				{
					AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
					CHECK(localAudioEngine != nullptr)
					if (DynamicAudioInstance* dynInstance = localAudioEngine->SetupDynamicAsset(dynAsset, false, params))
					{
						if (bDetachedSound)
						{
							PlayDetachedSound(dynInstance, volume);
						}
						else
						{
							PlaySound(dynInstance, volume);
						}

						continue;
					}

					//Fallthrough is intended. Treat the dynamic audio asset as a normal asset.
				}
			}

			if (const AudioAsset* asset = localAudioPool->GetAudio(audioHash))
			{
				if (bDetachedSound)
				{
					PlayDetachedSound(asset, volume);
				}
				else
				{
					PlaySound(asset, volume);
				}
			}
		}
	}

	if ((dataFlags & FLAG_DETACH_SOUND) > 0)
	{
		DetachFromActiveSounds();
	}

	if ((dataFlags & FLAG_INTERRUPT_SOUND) > 0)
	{
		InterruptSounds();
	}

	if ((dataFlags & FLAG_INTERRUPT_ASSET) > 0)
	{
		Int numSounds;
		incomingData >> numSounds;

		AudioAssetPool* localAudio = AudioAssetPool::FindAudioAssetPool();
		CHECK(localAudio != nullptr)

		for (Int i = 0; i < numSounds; ++i)
		{
			size_t audioHash;
			incomingData >> audioHash;

			if (const AudioAsset* asset = localAudio->GetAudio(audioHash))
			{
				InterruptSounds(asset);
			}
		}
	}

	if ((dataFlags & FLAG_INTERRUPT_CHANNEL) > 0)
	{
		Int numChannels;
		incomingData >> numChannels;

		for (Int i = 0; i < numChannels; ++i)
		{
			size_t channelHash;
			incomingData >> channelHash;
			InterruptSoundsByChannel(channelHash);
		}
	}
}
#endif
SD_END