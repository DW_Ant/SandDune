/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Sound.cpp
=====================================================================
*/

#include "Sound.h"

SD_BEGIN
LogCategory SoundLog(TXT("Sound"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);

LogCategory ThreadedSoundLog(TXT("Sound"), LogCategory::VERBOSITY_DEFAULT,
#if !WITH_MULTI_THREAD
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT
#else
	//For multi threads, the ThreadedSoundTransfer will copy all logs from the external to local thread. To avoid log duplication in standard output and windows, the ThreadedSoundLog will not record anything.
	0
#endif
	);

LogCategory& GetSoundLog ()
{
	Engine* localEngine = Engine::FindEngine();
	return (localEngine == nullptr || localEngine->IsMainEngine()) ? SoundLog : ThreadedSoundLog;
}
SD_END