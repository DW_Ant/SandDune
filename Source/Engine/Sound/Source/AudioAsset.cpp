/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioAsset.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioEngineComponent.h"
#ifdef WITH_MULTI_THREAD
#include "ThreadedSoundTransfer.h"
#endif

IMPLEMENT_CLASS(SD::AudioAsset, SD::Object)
SD_BEGIN

void AudioAsset::InitProps ()
{
	Super::InitProps();

	Buffer = nullptr;
	AudioName = TXT("Unknown Audio");
	OwningAssetPool = nullptr;
	ChannelName = DString::EmptyString;
	ChannelHash = 0;
	bLooping = false;
	bRandomStart = true;
	Subtitles = DString::EmptyString;
	bEnableDoppler = true;
	DuckPriority = 0;
	ConflictHandling = CH_None;
}

void AudioAsset::BeginObject ()
{
	Super::BeginObject();

	if (Buffer == nullptr)
	{
		Buffer = new sf::SoundBuffer();
	}
}

DString AudioAsset::GetFriendlyName () const
{
	if (!AudioName.IsEmpty())
	{
		return TXT("Audio:  ") + AudioName;
	}

	return Super::GetFriendlyName();
}

void AudioAsset::Destroyed ()
{
	if (AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find())
	{
		localAudioEngine->StopStaticSounds(this);
		localAudioEngine->StopDynamicSounds(this);
	}

	UnregisterTags();

	if (Buffer != nullptr)
	{
		delete Buffer;
		Buffer = nullptr;
	}

	Super::Destroyed();
}

Float AudioAsset::GetDuration () const
{
	if (Buffer != nullptr)
	{
		return Buffer->getDuration().asSeconds();
	}

	return -1.f;
}

Int AudioAsset::GetSampleRate () const
{
	if (Buffer != nullptr)
	{
		return Buffer->getSampleRate();
	}

	return -1;
}

size_t AudioAsset::GetNumSamples () const
{
	if (Buffer != nullptr)
	{
		return Buffer->getSampleCount();
	}

	return 0;
}

size_t AudioAsset::GetBitDepth () const
{
	//SFML uses an Int16 for its samples.
	return sizeof(sf::Int16);
}

unsigned int AudioAsset::GetNumChannels () const
{
	if (Buffer != nullptr)
	{
		return Buffer->getChannelCount();
	}

	return 0;
}

const sf::Int16* AudioAsset::GetRawData () const
{
	if (Buffer != nullptr)
	{
		return Buffer->getSamples();
	}

	return nullptr;
}

#ifdef WITH_MULTI_THREAD
void AudioAsset::CopyPropsToOtherThread ()
{
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)
	if (ThreadedSoundTransfer* transfer = localAudio->GetThreadTransfer())
	{
		transfer->CopyAudioProps(AudioHash);
	}
}

void AudioAsset::CopyPropsTo (DataBuffer& outData) const
{
	outData << ReadAudioName();
	outData << ReadChannelName();
	outData << Bool(IsLooping());
	outData << Bool(IsRandomStart());
	outData << Bool(IsDopplerEnabled());
	outData << GetDuckPriority();
	outData << Int(GetConflictHandling());
}

void AudioAsset::CopyPropsFrom (const DataBuffer& incomingData)
{
	DString audioName;
	incomingData >> audioName;
	SetAudioName(audioName);

	DString channelName;
	incomingData >> channelName;
	SetChannelName(channelName);

	Bool isLooping;
	incomingData >> isLooping;
	SetLooping(isLooping);

	Bool randStart;
	incomingData >> randStart;
	SetRandomStart(randStart);

	Bool dopplerEnabled;
	incomingData >> dopplerEnabled;
	SetEnableDoppler(dopplerEnabled);

	Int duckPriority;
	incomingData >> duckPriority;
	SetDuckPriority(duckPriority);

	Int conflictHandling;
	incomingData >> conflictHandling;
	SetConflictHandling(static_cast<AudioAsset::EConflictHandling>(conflictHandling.Value));
}
#endif

void AudioAsset::SetAudioName (const DString& newAudioName)
{
	AudioName = newAudioName;
}

void AudioAsset::SetAudioHash (size_t newAudioHash)
{
	AudioHash = newAudioHash;
}

void AudioAsset::SetChannelName (const DString& newChannelName)
{
	//Remove the tag associated with the previous name.
	if (!ChannelName.IsEmpty() && OwningAssetPool != nullptr)
	{
		HashedString oldChannelTag(ChannelName);
		ResourceTag::RemoveTag(oldChannelTag, OUT OwningAssetPool->Tags);
		ContainerUtils::RemoveItem(OUT Tags, oldChannelTag);
	}

	ChannelName = newChannelName;
	ChannelHash = ChannelName.GenerateHash();

	//Automatically register the channel name as a tag
	if (!ChannelName.IsEmpty() && OwningAssetPool != nullptr)
	{
		const HashedString& newChannelTag = Tags.emplace_back(ChannelName);
		ResourceTag::AddTag(newChannelTag, OUT OwningAssetPool->Tags);
	}
}

void AudioAsset::SetOwningAssetPool (AudioAssetPool* newOwningAssetPool)
{
	UnregisterTags(); //Remove tags from previous resource pool

	OwningAssetPool = newOwningAssetPool;

	//Automatically register the channel name as a tag
	if (!ChannelName.IsEmpty() && OwningAssetPool != nullptr)
	{
		const HashedString& newChannelTag = Tags.emplace_back(ChannelName);
		ResourceTag::AddTag(newChannelTag, OUT OwningAssetPool->Tags);
	}
}

void AudioAsset::UnregisterTags ()
{
	if (OwningAssetPool != nullptr)
	{
		for (const HashedString& tag : Tags)
		{
			ResourceTag::RemoveTag(tag, OUT OwningAssetPool->Tags);
		}
		ContainerUtils::Empty(OUT Tags);
	}
}
SD_END