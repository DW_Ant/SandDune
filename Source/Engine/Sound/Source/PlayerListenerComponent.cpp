/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlayerListenerComponent.cpp
=====================================================================
*/

#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "DynamicSound.h"
#ifdef WITH_MULTI_THREAD
#include "GradualTranslator.h"
#endif
#include "PlayerListenerComponent.h"

IMPLEMENT_CLASS(SD::PlayerListenerComponent, SD::SoundListenerComponent)
SD_BEGIN

#ifdef WITH_MULTI_THREAD
const Int PlayerListenerComponent::FLAG_COPY_ID			(0x00000001);
const Int PlayerListenerComponent::FLAG_COPY_PROPS		(0x00000002);
const Int PlayerListenerComponent::FLAG_UPDATE_POS		(0x00000004);
const Int PlayerListenerComponent::FLAG_TELEPORT_POS	(0x00000008);
const Int PlayerListenerComponent::FLAG_UPDATE_PITCH	(0x00000010);
const Int PlayerListenerComponent::FLAG_SEVER_SOUNDS	(0x00000020);
#endif

PlayerListenerComponent::SDoppler::SDoppler () :
	NumSoundsFromSource(1)
{
	//Noop
}

void PlayerListenerComponent::InitProps ()
{
	Super::InitProps();

	bEnableDoppler = true;

#ifdef WITH_MULTI_THREAD
	ThreadComp = nullptr;
	bInSoundThread = true;
	PendingFlags = 0;

	//Probably would want VERY fast response times since all dynamic sounds are relative to this component.
	MaxWaitTime = 0.2f;
	DopplerTick = nullptr;
#endif
}

void PlayerListenerComponent::BeginObject ()
{
	Super::BeginObject();

#ifdef WITH_MULTI_THREAD
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	if (!localAudio->IsMainAudioEngine())
	{
		bInSoundThread = false;

		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, PlayerListenerComponent, HandleCopyData, void, DataBuffer&));
			ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(this, PlayerListenerComponent, HandleReceiveData, void, const DataBuffer&));

			//Instantiate a ThreadedSoundTransfer on the sound thread
			ThreadComp->InstantiateExternalComp(MULTI_THREAD_SOUND_IDX, []
			{
				//This method is executed on the sound thread
				GradualTranslator* translator = GradualTranslator::CreateObject();
				PlayerListenerComponent* otherListener = PlayerListenerComponent::CreateObject();
				ThreadedComponent* otherThreadComp = nullptr;
				if (translator->AddComponent(otherListener))
				{
					translator->SetMoveTime(otherListener->MaxWaitTime);
					otherListener->bInSoundThread = true;

					otherThreadComp = ThreadedComponent::CreateObject();
					if (otherListener->AddComponent(otherThreadComp))
					{
						otherThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(otherListener, PlayerListenerComponent, HandleCopyData, void, DataBuffer&));
						otherThreadComp->SetOnNewData(SDFUNCTION_1PARAM(otherListener, PlayerListenerComponent, HandleReceiveData, void, const DataBuffer&));

						//The dummyTransform Entity should be destroyed whenever the ThreadComponents are disconnected.
						otherThreadComp->SetOnDisconnect(std::function<void(ThreadedComponent*)>([](ThreadedComponent* delegateOwner)
						{
							Entity* rootEntity = delegateOwner->GetRootEntity();
							if (rootEntity != nullptr)
							{
								//This would destroy this ThreadedComponent and the AudioComponent between the root Entity and this.
								rootEntity->Destroy();
							}
						}));
					}
				}

				return otherThreadComp;
			});

			//If it generated an ID, notify the other ThreadComponent what its ID will be.
			if (ListenerId != 0)
			{
				PendingFlags |= FLAG_COPY_ID;
				ThreadComp->RequestDataTransfer(MaxWaitTime);
			}
		}

		DopplerTick = TickComponent::CreateObject(TICK_GROUP_SOUND);
		if (AddComponent(DopplerTick))
		{
			DopplerTick->SetTickHandler(SDFUNCTION_1PARAM(this, PlayerListenerComponent, HandleDopplerTick, void, Float));
			DopplerTick->SetTicking(false);
		}

		localAudio->OnSoundEnded.RegisterHandler(SDFUNCTION_3PARAM(this, PlayerListenerComponent, HandleThreadedSoundEnded, void, const AudioAsset*, AudioComponent*, PlayerListenerComponent*));
	}
#endif
}

void PlayerListenerComponent::HearSoundEvent (const SoundEvent& newSoundEvent)
{
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	if (localAudioEngine->IsMainAudioEngine())
	{
		if (AudioComponent* broadcaster = newSoundEvent.GetSource())
		{
			if (broadcaster->IsPlayingDetachedSound())
			{
				localAudioEngine->PlayDetachedSoundEvent(newSoundEvent, this);
				return;
			}
		}

		localAudioEngine->PlaySoundEvent(newSoundEvent, this);
	}
#ifdef WITH_MULTI_THREAD
	else if (newSoundEvent.GetSource() != nullptr && !newSoundEvent.GetSource()->IsPlayingDetachedSound())
	{
		//Add a doppler affect locally.
		//Check if there is one associated with the source already.
		bool bHasDoppler = false;
		for (SDoppler& doppler : Dopplers)
		{
			if (doppler.Doppler.GetSource() == newSoundEvent.GetSource())
			{
				doppler.NumSoundsFromSource++;
				bHasDoppler = true;
				break;
			}
		}

		if (!bHasDoppler)
		{
			SDoppler& doppler = Dopplers.emplace_back();
			doppler.Doppler.SetSource(newSoundEvent.GetSource());
			doppler.Doppler.SetListener(this);
		}

		if (DopplerTick != nullptr && !DopplerTick->IsTicking())
		{
			DopplerTick->SetTicking(true);
		}
	}
#endif
}

void PlayerListenerComponent::OwnerTransformChanged ()
{
	Super::OwnerTransformChanged();

	SeverFromActiveSounds();
}

void PlayerListenerComponent::AttachTo (Entity* newOwner)
{
#ifdef WITH_MULTI_THREAD
	SDFunction<void> transformChangeCallback = SDFUNCTION(this, PlayerListenerComponent, HandleAbsTransformChanged, void);
	if (!bInSoundThread && OwnerTransform != nullptr && OwnerTransform->OnTransformChanged.IsRegistered(transformChangeCallback, true))
	{
		OwnerTransform->OnTransformChanged.UnregisterHandler(transformChangeCallback);
	}
#endif

	Super::AttachTo(newOwner);

#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread && OwnerTransform != nullptr)
	{
		OwnerTransform->OnTransformChanged.RegisterHandler(transformChangeCallback);
		PendingFlags |= FLAG_UPDATE_POS;
		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(true);
		}
	}
#endif
}

void PlayerListenerComponent::ComponentDetached ()
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread)
	{
		SDFunction<void> transformChangeCallback = SDFUNCTION(this, PlayerListenerComponent, HandleAbsTransformChanged, void);
		if (OwnerTransform != nullptr && OwnerTransform->OnTransformChanged.IsRegistered(transformChangeCallback, true))
		{
			OwnerTransform->OnTransformChanged.UnregisterHandler(transformChangeCallback);
		}
	}
#endif

	Super::ComponentDetached();
}

bool PlayerListenerComponent::ShouldGenerateId () const
{
	if (!Super::ShouldGenerateId())
	{
		return false;
	}

	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	return (!localAudio->IsRunningMultiThreadMode() || !localAudio->IsMainAudioEngine());
}

void PlayerListenerComponent::Destroyed ()
{
	SeverFromActiveSounds();

	if (AudioEngineComponent* localAudio = AudioEngineComponent::Find())
	{
#ifdef WITH_MULTI_THREAD
		SDFunction<void, const AudioAsset*, AudioComponent*, PlayerListenerComponent*> handler = SDFUNCTION_3PARAM(this, PlayerListenerComponent, HandleThreadedSoundEnded, void, const AudioAsset*, AudioComponent*, PlayerListenerComponent*);
		if (localAudio->OnSoundEnded.IsRegistered(handler, true))
		{
			localAudio->OnSoundEnded.UnregisterHandler(handler);
		}
#endif

		localAudio->UnregisterListenerComponent(this);
	}

	Super::Destroyed();
}

void PlayerListenerComponent::SeverFromActiveSounds ()
{
#ifdef WITH_MULTI_THREAD
	if (!bInSoundThread)
	{
		PendingFlags |= FLAG_SEVER_SOUNDS;
		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(true);
		}

		return;
	}

	//Stop updating pitches for all sounds heard from this component.
	ContainerUtils::Empty(OUT Dopplers);
	if (DopplerTick != nullptr)
	{
		DopplerTick->SetTicking(false);
	}
#endif

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	if (localAudioEngine != nullptr)
	{
		localAudioEngine->DetachSoundsFromListener(this);
	}
}

void PlayerListenerComponent::TeleportListenerComp ()
{
#ifdef WITH_MULTI_THREAD
	if (ThreadComp != nullptr && !bInSoundThread)
	{
		//Force the copy position, too, since doppler calculations relies on an updated position.
		PendingFlags |= (FLAG_UPDATE_POS | FLAG_TELEPORT_POS);
		ThreadComp->RequestDataTransfer(true);
		return;
	}
#endif

	//Figure out how many active sounds this component is responsible for.
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr)

	TeleportDopplerCount = 0;
	for (DynamicSound* dynSound : localAudio->ReadDynamicSounds())
	{
		if (dynSound->IsActive() && dynSound->GetListener() == this)
		{
			TeleportDopplerCount++;
		}
	}
}

#ifdef WITH_MULTI_THREAD
void PlayerListenerComponent::RemoveSourceFromDoppler (AudioComponent* source)
{
	for (size_t i = 0; i < Dopplers.size(); ++i)
	{
		if (Dopplers.at(i).Doppler.GetSource() == source)
		{
			Dopplers.erase(Dopplers.begin() + i);
			if (Dopplers.size() <= 0 && DopplerTick != nullptr)
			{
				DopplerTick->SetTicking(false);
			}

			return;
		}
	}
}
#endif

void PlayerListenerComponent::SetEnableDoppler (bool newEnableDoppler)
{
	bEnableDoppler = newEnableDoppler;

#ifdef WITH_MULTI_THREAD
	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
#endif
}

#ifdef WITH_MULTI_THREAD
void PlayerListenerComponent::SetMaxWaitTime (Float newMaxWaitTime)
{
	MaxWaitTime = newMaxWaitTime;

	PendingFlags |= FLAG_COPY_PROPS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}
#endif

void PlayerListenerComponent::DecrementDopplerCounter ()
{
	if (--TeleportDopplerCount < 0)
	{
		TeleportDopplerCount = 0;
	}
}

#ifdef WITH_MULTI_THREAD
void PlayerListenerComponent::HandleDopplerTick (Float deltaSec)
{
	for (SDoppler& doppler : Dopplers)
	{
		Float oldPitch = doppler.Doppler.GetLastDopplerMultiplier();
		Float newPitch = doppler.Doppler.CalcEffect(deltaSec);
		if (oldPitch != newPitch && doppler.Doppler.GetSource() != nullptr)
		{
			if (!PendingPitchMultipliers.contains(doppler.Doppler.GetSource()->GetAudioCompId()))
			{
				PendingPitchMultipliers.insert({doppler.Doppler.GetSource()->GetAudioCompId(), newPitch});
			}
			else
			{
				PendingPitchMultipliers.at(doppler.Doppler.GetSource()->GetAudioCompId()) = newPitch;
			}
		}
	}

	if (ThreadComp != nullptr && PendingPitchMultipliers.size() > 0)
	{
		PendingFlags |= FLAG_UPDATE_PITCH;
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void PlayerListenerComponent::HandleThreadedSoundEnded (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp)
{
	if (listenComp != this || audioComp == nullptr)
	{
		return;
	}

	for (size_t i = 0; i < Dopplers.size(); ++i)
	{
		if (Dopplers.at(i).Doppler.GetSource() == audioComp)
		{
			Dopplers.at(i).NumSoundsFromSource--;

			if (Dopplers.at(i).NumSoundsFromSource <= 0)
			{
				Dopplers.erase(Dopplers.begin() + i);
				if (ContainerUtils::IsEmpty(Dopplers) && DopplerTick != nullptr)
				{
					DopplerTick->SetTicking(false);
				}
			}

			return;
		}
	}
}

void PlayerListenerComponent::HandleAbsTransformChanged ()
{
	CHECK(!bInSoundThread)
	PendingFlags |= FLAG_UPDATE_POS;
	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void PlayerListenerComponent::HandleCopyData (DataBuffer& outExternalData)
{
	if (OwnerTransform == nullptr)
	{
		//Remove the update position flag in case a position was updated before it was severed.
		PendingFlags &= (~FLAG_UPDATE_POS);
	}

	outExternalData << PendingFlags;

	if ((PendingFlags & FLAG_COPY_ID) > 0)
	{
		outExternalData << ListenerId;
	}

	if ((PendingFlags & FLAG_COPY_PROPS) > 0)
	{
		outExternalData << Bool(bEnableDoppler);
		outExternalData << MaxWaitTime;
	}

	if ((PendingFlags & FLAG_UPDATE_POS) > 0)
	{
		CHECK(OwnerTransform != nullptr)
		outExternalData << OwnerTransform->ReadAbsTranslation();
	}

	if ((PendingFlags & FLAG_UPDATE_PITCH) > 0)
	{
		Int numUpdates = Int(PendingPitchMultipliers.size());
		outExternalData << numUpdates;

		for (const std::pair<Int, Float>& pendingPitch : PendingPitchMultipliers)
		{
			outExternalData << pendingPitch.first;
			outExternalData << pendingPitch.second;
		}
		PendingPitchMultipliers.clear();
	}

	PendingFlags = 0;
}

void PlayerListenerComponent::HandleReceiveData (const DataBuffer& incomingData)
{
	Int dataFlags;
	incomingData >> dataFlags;

	if ((dataFlags & FLAG_COPY_ID) > 0)
	{
		AudioEngineComponent* localAudio = AudioEngineComponent::Find();
		CHECK(localAudio != nullptr)
		localAudio->UnregisterListenerComponent(this);

		incomingData >> ListenerId;

		localAudio->RegisterListenerComponent(this);
	}

	if ((dataFlags & FLAG_COPY_PROPS) > 0)
	{
		Bool dopplerFlag;
		incomingData >> dopplerFlag;
		bEnableDoppler = dopplerFlag;

		Float newMaxWaitTime;
		incomingData >> newMaxWaitTime;
		if (MaxWaitTime != newMaxWaitTime)
		{
			MaxWaitTime = newMaxWaitTime;
			if (GradualTranslator* translator = dynamic_cast<GradualTranslator*>(GetRootEntity()))
			{
				translator->SetMoveTime(MaxWaitTime);
			}
		}
	}

	if ((dataFlags & FLAG_UPDATE_POS) > 0)
	{
		Vector3 newPos;
		incomingData >> newPos;

		if (GradualTranslator* translator = dynamic_cast<GradualTranslator*>(OwnerTransform))
		{
			translator->SetDestination(newPos);
		}
	}

	if ((dataFlags & FLAG_TELEPORT_POS) > 0)
	{
		TeleportListenerComp();
	}

	if ((dataFlags & FLAG_UPDATE_PITCH) > 0)
	{
		Int numUpdates;
		incomingData >> numUpdates;

		AudioEngineComponent* localAudio = AudioEngineComponent::Find();
		CHECK(localAudio != nullptr)

		for (Int i = 0; i < numUpdates; ++i)
		{
			Int audioCompId;
			incomingData >> audioCompId;

			Float newDopplerPitch;
			incomingData >> newDopplerPitch;

			//Update any dynamic sounds that matches this listener and source.
			for (DynamicSound* dynSnd : localAudio->ReadDynamicSounds())
			{
				if (dynSnd->IsActive() && dynSnd->ReadEvent().GetSource() != nullptr && dynSnd->ReadEvent().GetSource()->GetAudioCompId() == audioCompId && dynSnd->GetListener() == this)
				{
					dynSnd->SetDopplerPitchOverride(newDopplerPitch);
				}
			}
		}
	}

	if ((dataFlags & FLAG_SEVER_SOUNDS) > 0)
	{
		SeverFromActiveSounds();
	}
}
#endif
SD_END