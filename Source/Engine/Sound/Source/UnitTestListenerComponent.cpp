/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTestListenerComponent.cpp
=====================================================================
*/

#include "UnitTestListenerComponent.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::UnitTestListenerComponent, SD::SoundListenerComponent)
SD_BEGIN

void UnitTestListenerComponent::InitProps ()
{
	Super::InitProps();

	NumEventsReceived = 0;
}

void UnitTestListenerComponent::HearSoundEvent (const SoundEvent& newSoundEvent)
{
	++NumEventsReceived;
}

void UnitTestListenerComponent::ResetTest ()
{
	NumEventsReceived = 0;
}
SD_END
#endif