/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundEvent.cpp
=====================================================================
*/

#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "SoundEvent.h"
#include "SoundListenerComponent.h"

SD_BEGIN
SoundEvent::SoundEvent () :
	Asset(nullptr),
	Source(nullptr),
	InnerRadius(-1.f),
	OuterRadius(-1.f),
	VolumeMultiplier(1.f),
	PitchMultiplier(1.f),
	bHasSource(nullptr)
{
	//Noop
}

SoundEvent::SoundEvent (const AudioAsset* inAsset, AudioComponent* inSource) :
	Asset(inAsset),
	Source(inSource),
	InnerRadius(1000.f), //10 meters
	OuterRadius(10000.f), //100 meters
	VolumeMultiplier(1.f),
	PitchMultiplier(1.f),
	bHasSource(Source != nullptr)
{
	CHECK(Asset != nullptr)
}

SoundEvent::SoundEvent (const AudioAsset* inAsset, AudioComponent* inSource, Float inInnerRadius, Float inOuterRadius, Float inVolumeMultiplier, Float inPitchMultiplier) :
	Asset(inAsset),
	Source(inSource),
	InnerRadius(inInnerRadius),
	OuterRadius(inOuterRadius),
	VolumeMultiplier(inVolumeMultiplier),
	PitchMultiplier(inPitchMultiplier),
	bHasSource(Source != nullptr)
{
	CHECK(Asset != nullptr)
}

SoundEvent::SoundEvent (const SoundEvent& other) :
	Asset(other.Asset),
	Source(other.Source),
	InnerRadius(other.InnerRadius),
	OuterRadius(other.OuterRadius),
	VolumeMultiplier(other.VolumeMultiplier),
	PitchMultiplier(other.PitchMultiplier),
	bHasSource(other.bHasSource),
	LastKnownLocation(other.LastKnownLocation)
{
	//Noop
}

void SoundEvent::BroadcastSoundEvent ()
{
	if (Source == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot broadcast a sound event without specifying the source to broadcast it from."));
		return;
	}

	if (Source->GetOwnerTransform() == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot broadcast a sound event from an AudioComponent that is not associated with a SceneTransform."));
		return;
	}

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	Float maxDistSquared = OuterRadius * OuterRadius;
	for (const std::pair<Int, SoundListenerComponent*>& registeredListener : localAudioEngine->ReadListenerComponents())
	{
		SoundListenerComponent* comp = registeredListener.second;
		if (comp->GetOwnerTransform() != nullptr && (comp->GetOwnerTransform()->ReadAbsTranslation() - Source->GetOwnerTransform()->ReadAbsTranslation()).CalcDistSquared() <= maxDistSquared)
		{
			comp->HearSoundEvent(*this);
		}
	}
}

void SoundEvent::SeverSource ()
{
	if (Source != nullptr && Source->GetOwnerTransform() != nullptr)
	{
		LastKnownLocation = Source->GetOwnerTransform()->GetAbsTranslation();
	}

	Source = nullptr;
}

void SoundEvent::ClearReferences ()
{
	Asset = nullptr;
	Source = nullptr;
}

Vector3 SoundEvent::GetSourceLocation () const
{
	if (Source != nullptr && Source->GetOwnerTransform() != nullptr)
	{
		return Source->GetOwnerTransform()->GetAbsTranslation();
	}

	return LastKnownLocation;
}

void SoundEvent::SetAsset (const AudioAsset* newAsset)
{
	Asset = newAsset;
}
SD_END