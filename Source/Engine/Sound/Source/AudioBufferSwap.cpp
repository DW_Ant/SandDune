/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferSwap.cpp
=====================================================================
*/

#include "AudioBufferSwap.h"
#include "DynamicAudioInstance.h"

IMPLEMENT_CLASS(SD::AudioBufferSwap, SD::AudioBufferModifier)
SD_BEGIN

void AudioBufferSwap::InitProps ()
{
	Super::InitProps();

	RawAudioData = nullptr;
	SampleCount = 0;
	NumChannels = 0;
	SampleRate = 0;
	SubtitleOverride = DString::EmptyString;
}

AudioBufferModifier* AudioBufferSwap::DoEffect (DynamicAudioInstance* asset)
{
	CHECK(asset != nullptr)
	if (RawAudioData != nullptr)
	{
		if (sf::SoundBuffer* buffer = asset->EditBuffer())
		{
			if (!buffer->loadFromSamples(RawAudioData, SampleCount, NumChannels.ToUnsignedInt32(), SampleRate.ToUnsignedInt32()))
			{
				SoundLog.Log(LogCategory::LL_Warning, TXT("Unable to swap audio buffers to %s. Failed to load buffer from raw audio data."), asset->GetFriendlyName());
			}
		}
	}

	if (!SubtitleOverride.IsEmpty())
	{
		asset->SetSubtitles(SubtitleOverride);
	}

	if (NextModifiers.size() > 0)
	{
		return NextModifiers.at(0);
	}

	return nullptr;
}

void AudioBufferSwap::Destroyed ()
{
	if (SampleCount > 0 && RawAudioData != nullptr)
	{
		delete[] RawAudioData;
		RawAudioData = nullptr;
		SampleCount = 0;
	}

	Super::Destroyed();
}

sf::Int16*& AudioBufferSwap::UpdateRawData (size_t newSampleCount, Int newNumChannels, Int newSampleRate)
{
	if (RawAudioData != nullptr)
	{
		delete[] RawAudioData;
		RawAudioData = nullptr;
	}

	SampleCount = newSampleCount;
	NumChannels = newNumChannels;
	SampleRate = newSampleRate;

	if (SampleCount > 0)
	{
		RawAudioData = new sf::Int16[SampleCount];
	}

	return RawAudioData;
}

void AudioBufferSwap::SetSubtitleOverride (const DString& newSubtitleOverride)
{
	SubtitleOverride = newSubtitleOverride;
}
SD_END