/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadedDynamicAudioInstance.cpp
=====================================================================
*/

#include "ThreadedDynamicAudioInstance.h"

#ifdef WITH_MULTI_THREAD
IMPLEMENT_CLASS(SD::ThreadedDynamicAudioInstance, SD::DynamicAudioInstance)
SD_BEGIN

void ThreadedDynamicAudioInstance::SetAudioStringParam (const HashedString& paramName, const DString& paramValue)
{
	ParamRecords << Int(PT_String);
	ParamRecords << paramName;
	ParamRecords << paramValue;
}

void ThreadedDynamicAudioInstance::SetAudioIntParam (const HashedString& paramName, Int paramValue)
{
	ParamRecords << Int(PT_Int);
	ParamRecords << paramName;
	ParamRecords << paramValue;
}

void ThreadedDynamicAudioInstance::SetAudioFloatParam (const HashedString& paramName, Float paramValue)
{
	ParamRecords << Int(PT_Float);
	ParamRecords << paramName;
	ParamRecords << paramValue;
}

void ThreadedDynamicAudioInstance::SetAudioBoolParam (const HashedString& paramName, bool paramValue)
{
	ParamRecords << Int(PT_Bool);
	ParamRecords << paramName;
	ParamRecords << Bool(paramValue);
}

void ThreadedDynamicAudioInstance::ApplyModifiers ()
{
	//Noop
}

bool ThreadedDynamicAudioInstance::ShouldRegisterAudioCallback () const
{
	return false;
}
SD_END
#endif