/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicAudioInstance.cpp
=====================================================================
*/

#include "AudioBufferModifier.h"
#include "AudioEngineComponent.h"
#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"

IMPLEMENT_CLASS(SD::DynamicAudioInstance, SD::AudioAsset)
SD_BEGIN

void DynamicAudioInstance::InitProps ()
{
	Super::InitProps();

	OwningAsset = nullptr;
}

void DynamicAudioInstance::BeginObject ()
{
	Super::BeginObject();

	if (ShouldRegisterAudioCallback())
	{
		if (AudioEngineComponent* localAudio = AudioEngineComponent::Find())
		{
			localAudio->OnSoundEnded.RegisterHandler(SDFUNCTION_3PARAM(this, DynamicAudioInstance, HandleSoundEnded, void, const AudioAsset*, AudioComponent*, PlayerListenerComponent*));
		}
	}
}

DString DynamicAudioInstance::GetFriendlyName () const
{
	if (OwningAsset != nullptr)
	{
		return OwningAsset->GetFriendlyName() + TXT("_Instance");
	}

	return TXT("UnknownDynamicAudioInstance");
}

DString DynamicAudioInstance::GetAudioName () const
{
	return (OwningAsset->GetAudioName());
}

const DString& DynamicAudioInstance::ReadAudioName () const
{
	return (OwningAsset->ReadAudioName());
}

size_t DynamicAudioInstance::GetAudioHash () const
{
	return (OwningAsset->GetAudioHash());
}

DString DynamicAudioInstance::GetChannelName () const
{
	return (OwningAsset->GetChannelName());
}

const DString& DynamicAudioInstance::ReadChannelName () const
{
	return (OwningAsset->ReadChannelName());
}

size_t DynamicAudioInstance::GetChannelHash () const
{
	return (OwningAsset->GetChannelHash());
}

bool DynamicAudioInstance::IsLooping () const
{
	return (OwningAsset->IsLooping());
}

bool DynamicAudioInstance::IsRandomStart () const
{
	return (OwningAsset->IsRandomStart());
}

DString DynamicAudioInstance::GetSubtitles () const
{
	if (!Subtitles.IsEmpty())
	{
		return Subtitles;
	}

	return (OwningAsset->GetSubtitles());
}

const DString& DynamicAudioInstance::ReadSubtitles () const
{
	if (!Subtitles.IsEmpty())
	{
		return Subtitles;
	}

	return (OwningAsset->ReadSubtitles());
}

bool DynamicAudioInstance::IsDopplerEnabled () const
{
	return (OwningAsset->IsDopplerEnabled());
}

Int DynamicAudioInstance::GetDuckPriority () const
{
	return (OwningAsset->GetDuckPriority());
}

AudioAsset::EConflictHandling DynamicAudioInstance::GetConflictHandling () const
{
	return (OwningAsset->GetConflictHandling());
}

void DynamicAudioInstance::Destroyed ()
{
	if (ShouldRegisterAudioCallback())
	{
		if (AudioEngineComponent* localAudio = AudioEngineComponent::Find())
		{
			localAudio->OnSoundEnded.UnregisterHandler(SDFUNCTION_3PARAM(this, DynamicAudioInstance, HandleSoundEnded, void, const AudioAsset*, AudioComponent*, PlayerListenerComponent*));
		}
	}

	Super::Destroyed();
}

void DynamicAudioInstance::SetAudioStringParam (const HashedString& paramName, const DString& paramValue)
{
	CHECK(OwningAsset != nullptr)
	AudioBufferModifier* match = FindModifier(paramName, OwningAsset->GetFirstModifier());
	if (match != nullptr)
	{
		match->SetSoundParameter(paramName, paramValue);
	}
}

void DynamicAudioInstance::SetAudioIntParam (const HashedString& paramName, Int paramValue)
{
	CHECK(OwningAsset != nullptr)
	AudioBufferModifier* match = FindModifier(paramName, OwningAsset->GetFirstModifier());
	if (match != nullptr)
	{
		match->SetSoundParameter(paramName, paramValue);
	}
}

void DynamicAudioInstance::SetAudioFloatParam (const HashedString& paramName, Float paramValue)
{
	CHECK(OwningAsset != nullptr)
	AudioBufferModifier* match = FindModifier(paramName, OwningAsset->GetFirstModifier());
	if (match != nullptr)
	{
		match->SetSoundParameter(paramName, paramValue);
	}
}

void DynamicAudioInstance::SetAudioBoolParam (const HashedString& paramName, bool paramValue)
{
	CHECK(OwningAsset != nullptr)
	AudioBufferModifier* match = FindModifier(paramName, OwningAsset->GetFirstModifier());
	if (match != nullptr)
	{
		match->SetSoundParameter(paramName, paramValue);
	}
}

void DynamicAudioInstance::SetParamsFromBuffer (const DataBuffer& incomingData)
{
	while (!incomingData.IsReaderAtEnd())
	{
		Int paramType;
		incomingData >> paramType;

		HashedString paramName;
		incomingData >> paramName;

		switch(paramType.Value)
		{
			case(PT_String):
			{
				DString paramValue;
				incomingData >> paramValue;
				SetAudioStringParam(paramName, paramValue);
				break;
			}

			case(PT_Int):
			{
				Int paramValue;
				incomingData >> paramValue;
				SetAudioIntParam(paramName, paramValue);
				break;
			}

			case(PT_Float):
			{
				Float paramValue;
				incomingData >> paramValue;
				SetAudioFloatParam(paramName, paramValue);
				break;
			}

			case (PT_Bool):
			{
				Bool paramValue;
				incomingData >> paramValue;
				SetAudioBoolParam(paramName, paramValue);
				break;
			}
		}
	}
}

void DynamicAudioInstance::ApplyModifiers ()
{
	CHECK(OwningAsset != nullptr)

	AudioBufferModifier* modifier = OwningAsset->GetFirstModifier();
	Int counter = 0;
	Int maxIterations = 1000;
	while (modifier != nullptr && counter < maxIterations)
	{
		modifier = modifier->DoEffect(this);
		++counter;
	}

	if (counter >= maxIterations)
	{
		SoundLog.Log(LogCategory::LL_Critical, TXT("Infinite recursion detected when applying buffer modifiers to %s. Check the modifier chain to ensure there aren't any infinite loops. Iterated %s times."), GetFriendlyName(), counter);
	}
}

void DynamicAudioInstance::SetOwningAsset (const DynamicAudioAsset* newOwningAsset)
{
	OwningAsset = newOwningAsset;

	//Avoid copying properties to save on cpu cycles. Instead the instance will redirect to the OwningAsset when referencing the channel name and such.
}

bool DynamicAudioInstance::ShouldRegisterAudioCallback () const
{
	return true;
}

AudioBufferModifier* DynamicAudioInstance::FindModifier (const HashedString& paramName, AudioBufferModifier* searchFrom) const
{
	if (searchFrom == nullptr)
	{
		return nullptr;
	}

	if (searchFrom->HasParamName(paramName))
	{
		return searchFrom;
	}

	for (AudioBufferModifier* modifier : searchFrom->ReadNextModifiers())
	{
		if (modifier == nullptr)
		{
			continue;
		}

		//Recursively search within the modifier's out param
		AudioBufferModifier* match = FindModifier(paramName, modifier);
		if (match != nullptr)
		{
			return match;
		}
	}

	return nullptr;
}

void DynamicAudioInstance::HandleSoundEnded (const AudioAsset* asset, AudioComponent* src, PlayerListenerComponent* listener)
{
	if (asset == this)
	{
		//Asset is intended to be played only once.
		Destroy();
	}
}
SD_END