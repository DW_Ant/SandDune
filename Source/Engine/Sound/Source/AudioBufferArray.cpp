/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferArray.cpp
=====================================================================
*/

#include "AudioBufferArray.h"

IMPLEMENT_CLASS(SD::AudioBufferArray, SD::AudioBufferModifier)
SD_BEGIN

void AudioBufferArray::InitProps ()
{
	Super::InitProps();

	bRandomIndex = true;
	LastPlayedIdx = SD_MAXINT; //It'll reset to zero when the owning asset is played for the first time.
	SelectedIdx = nullptr;
}

AudioBufferModifier* AudioBufferArray::DoEffect (DynamicAudioInstance* asset)
{
	if (ContainerUtils::IsEmpty(NextModifiers))
	{
		return nullptr;
	}

	if (SelectedIdx != nullptr && ContainerUtils::IsValidIndex(NextModifiers, SelectedIdx->ParamValue))
	{
		LastPlayedIdx = SelectedIdx->ParamValue;
		return NextModifiers.at(SelectedIdx->ParamValue.ToUnsignedInt());
	}

#ifdef WITH_RANDOM
	if (bRandomIndex)
	{
		LastPlayedIdx = RandomUtils::Rand(NextModifiers.size());
		return NextModifiers.at(LastPlayedIdx.ToUnsignedInt());
	}
#endif

	if (++LastPlayedIdx >= NextModifiers.size())
	{
		LastPlayedIdx = 0;
	}

	return NextModifiers.at(LastPlayedIdx.ToUnsignedInt());
}

void AudioBufferArray::ResetAudioParameters ()
{
	Super::ResetAudioParameters();

	if (SelectedIdx != nullptr)
	{
		SelectedIdx->ResetToDefault();
	}
}

bool AudioBufferArray::HasParamName (const HashedString& paramName) const
{
	if (SelectedIdx != nullptr && SelectedIdx->ReadParamName() == paramName)
	{
		return true;
	}

	return Super::HasParamName(paramName);
}

bool AudioBufferArray::SetSoundParameter (const HashedString& paramName, Int paramValue)
{
	if (SelectedIdx != nullptr && SelectedIdx->ReadParamName() == paramName)
	{
		SelectedIdx->ParamValue = paramValue;
		return true;
	}

	return Super::SetSoundParameter(paramName, paramValue);
}

void AudioBufferArray::Destroyed ()
{
	if (SelectedIdx != nullptr)
	{
		delete SelectedIdx;
		SelectedIdx = nullptr;
	}

	Super::Destroyed();
}

void AudioBufferArray::SetRandomIndex (bool bNewRandomIndex)
{
	bRandomIndex = bNewRandomIndex;
}

void AudioBufferArray::SetSelectedIdx (SoundProperty<Int>* newSelectedIdx)
{
	if (SelectedIdx != nullptr)
	{
		delete SelectedIdx;
		SelectedIdx = nullptr;
	}

	SelectedIdx = newSelectedIdx;
}
SD_END