/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GradualTranslator.cpp
=====================================================================
*/

#include "GradualTranslator.h"

#ifdef WITH_MULTI_THREAD

IMPLEMENT_CLASS(SD::GradualTranslator, SD::SceneEntity)
SD_BEGIN

//In case the MainThread sent a forced update between MaxWaitTimes. The 1.5x ensures that this Entity will continue to move even if a position update was missed one time.
const Float GradualTranslator::MoveTimeMultiplier(1.5f);

void GradualTranslator::InitProps ()
{
	Super::InitProps();

	MoveTime = 0.5f;
	LastUpdateTime = -1.f;
	MoveFrom = Vector3::ZERO_VECTOR;
	MoveTo = Vector3::ZERO_VECTOR;
	Tick = nullptr;
}

void GradualTranslator::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_SOUND);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, GradualTranslator, HandleTick, void, Float));
		Tick->SetTicking(false);
	}
}

void GradualTranslator::SetDestination (const Vector3& newMoveTo)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	LastUpdateTime = localEngine->GetElapsedTime();
	MoveFrom = GetAbsTranslation();
	MoveTo = newMoveTo;

	if (Tick != nullptr)
	{
		Tick->SetTicking(true);
	}
}

void GradualTranslator::SetMoveTime (Float newMoveTime)
{
	MoveTime = newMoveTime * MoveTimeMultiplier;
}

void GradualTranslator::HandleTick (Float deltaSec)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	Float ratio = (localEngine->GetElapsedTime() - LastUpdateTime) / MoveTime;
	if (ratio >= 1.f)
	{
		SetTranslation(MoveTo);
		CalculateAbsoluteTransform();

		if (Tick != nullptr)
		{
			Tick->SetTicking(false);
		}
	}
	else
	{
		Vector3 newTranslation = Vector3::Lerp(ratio, MoveFrom, MoveTo);
		SetTranslation(newTranslation);
		CalculateAbsoluteTransform();
	}
}
SD_END
#endif