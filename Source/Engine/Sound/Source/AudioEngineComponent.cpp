/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioEngineComponent.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioBufferModifier.h"
#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "DopplerEffect.h"
#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"
#include "DynamicSound.h"
#include "PlayerListenerComponent.h"
#include "SoundListenerComponent.h"
#include "SoundMode.h"
#include "SoundPlaybackInterface.h"
#ifdef WITH_MULTI_THREAD
#include "ThreadedDynamicAudioInstance.h"
#include "ThreadedSoundTransfer.h"
#endif

IMPLEMENT_ENGINE_COMPONENT(SD::AudioEngineComponent)
SD_BEGIN

#ifdef WITH_MULTI_THREAD
/**
 * Creates and initializes an Engine object responsible for launching and running the main AudioEngineComponent.
 */
SD_THREAD_FUNCTION(RunSoundEngine);
#endif

const DString AudioEngineComponent::AUDIO_CHANNEL_GENERAL(TXT("General"));
const DString AudioEngineComponent::AUDIO_CHANNEL_DIALOGUE(TXT("Dialogue"));
const DString AudioEngineComponent::AUDIO_CHANNEL_UI(TXT("Gui"));
const DString AudioEngineComponent::AUDIO_CHANNEL_MUSIC(TXT("Music"));

AudioEngineComponent::SAudioChannel::SAudioChannel (const DString& inChannelName, Float inVolume) :
	ChannelName(inChannelName),
	Volume(inVolume),
	SoundModeVolume(1.f),
	TimeDilation(1.f)
{
	CHECK(!ChannelName.IsEmpty())
}

AudioEngineComponent::SActiveStaticSound::SActiveStaticSound (const AudioAsset* inAsset, Float inVolume) :
	Asset(inAsset),
	Volume(inVolume)
{
	CHECK(Asset != nullptr)
}

Float AudioEngineComponent::SActiveStaticSound::CalcTimeRemaining () const
{
	Float playPos = SoundPlayer.getPlayingOffset().asSeconds();

	return (Asset->GetDuration() - playPos) * SoundPlayer.getPitch();
}

AudioEngineComponent::AudioEngineComponent (bool multiThreadMode) : Super(),
	SpeedOfSound(34029.f),
	MasterVolume(1.f),
	DuckVolumeMultiplier(0.67f),
	DuckLerpDuration(0.5f),
	bMultiThreadedMode(multiThreadMode),
#ifdef WITH_MULTI_THREAD
	bThreadedSlave(false),
#endif
	NextStaticEndTime(-1.f),
	bIsPlayingFiniteStaticSound(false),
	HighestDuckPriority(0),
	bIsDucking(false),
	DuckingTimestamp(-1.f)
{
	bTickingComponent = true;

#ifndef WITH_MULTI_THREAD
	bMultiThreadedMode = false;
#endif
}

void AudioEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

#ifdef WITH_MULTI_THREAD
	if (bMultiThreadedMode)
	{
		InitMultiThread();
	}
#endif

	Engine* localEngine = GetOwningEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_SOUND, TICK_GROUP_PRIORITY_SOUND);

	if (IsMainAudioEngine())
	{
		//Dynamic sounds are only played on the main audio engine.
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, AudioEngineComponent, HandleCollectGarbage, void));
	}

	//Create a handful of common audio channels.
	CreateAudioChannel(AUDIO_CHANNEL_GENERAL, 1.f); //Misc channel
	CreateAudioChannel(AUDIO_CHANNEL_DIALOGUE, 1.f);
	CreateAudioChannel(AUDIO_CHANNEL_UI, 1.f);
	CreateAudioChannel(AUDIO_CHANNEL_MUSIC, 1.f);

	if (AudioAssetPool::FindAudioAssetPool() == nullptr)
	{
		AudioAssetPool* localAssetPool = AudioAssetPool::CreateObject();
		localAssetPool->SetIsOnSoundThread(IsMainAudioEngine());
	}

	//Set the SFML's global listener direction to face along positive X to indicate forward direction (aligns with Rotator::ZERO_ROTATOR). 
	sf::Listener::setDirection(0.f, 0.f, -1.f);

	//Set SFML's global listener up direction to face along positive Z. Positive Y in SFML coordinates is equal to positive Z in SD coordinates.
	sf::Listener::setUpVector(0.f, 1.f, 0.f);

	//The listener will always be at the origin. To support multiple viewports, the DynamicSounds' position will be relative to this origin.
	sf::Listener::setPosition(0.f, 0.f, 0.f);

	if (IsMainAudioEngine())
	{
		DopplerEffect::SpeedOfSound = SpeedOfSound;

		//Allocate space for DynamicSounds
		DynamicSounds.resize(8);
		for (size_t i = 0; i < DynamicSounds.size(); ++i)
		{
			DynamicSounds.at(i) = DynamicSound::CreateObject();
			DynamicSounds.at(i)->OnFinished = SDFUNCTION_1PARAM(this, AudioEngineComponent, HandleDynamicSoundFinished, void, DynamicSound*);
		}
	}
}

void AudioEngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{
	Super::ProcessLog(category, logLevel, formattedMsg);

#ifdef WITH_MULTI_THREAD
	if (category == &ThreadedSoundLog)
	{
		CHECK_INFO(IsMainAudioEngine(), "ThreadedSoundLog can only be called on the sound thread.")
		if (ThreadTransfer != nullptr)
		{
			//Copy the logs to the sound thread transfer so it can be recorded in the main thread since that has access to the file logs.
			ThreadTransfer->TransferLog(logLevel, formattedMsg);
		}
	}
#endif
}

void AudioEngineComponent::PreTick (Float deltaSec)
{
	Super::PreTick(deltaSec);

	if (IsMainAudioEngine())
	{
		if (bIsPlayingFiniteStaticSound)
		{
			NextStaticEndTime -= deltaSec;
			if (NextStaticEndTime <= 0.f)
			{
				FlushStoppedSounds();
			}
		}

		ProcessDucking();
	}
}

void AudioEngineComponent::PostTick (Float deltaSec)
{
	Super::PostTick(deltaSec);

	for (size_t i = 0; i < PendingSoundEndEvents.size(); ++i)
	{
		const AudioAsset* asset = std::get<0>(PendingSoundEndEvents.at(i));
		if (asset == nullptr || asset->GetPendingDelete())
		{
			//A sound event without specifying an asset isn't that useful.
			continue;
		}

		AudioComponent* source = std::get<1>(PendingSoundEndEvents.at(i));
		if (source != nullptr && source->GetPendingDelete())
		{
			//The audio component was destroyed, but the garbage collector did not delete it yet. Act like it's destroyed by setting the pointer to nullptr.
			source = nullptr;
		}

		PlayerListenerComponent* listener = std::get<2>(PendingSoundEndEvents.at(i));
		if (listener != nullptr && listener->GetPendingDelete())
		{
			listener = nullptr;
		}

		OnSoundEnded.Broadcast(asset, source, listener);
		for (SoundPlaybackInterface* playbackInterface : PlaybackInterfaces)
		{
			playbackInterface->ProcessStoppedSound(asset, source, listener);
		}

#ifdef WITH_MULTI_THREAD
		if (ThreadTransfer != nullptr)
		{
			ThreadTransfer->BroadcastSoundEnded(asset, source, listener);
		}
#endif
	}
	ContainerUtils::Empty(OUT PendingSoundEndEvents);
}

void AudioEngineComponent::ShutdownComponent ()
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->TerminateOtherEngine();
	}

	if (ThreadTransfer != nullptr)
	{
		ThreadTransfer->Destroy();
		ThreadTransfer = nullptr;
	}
#endif

	if (IsMainAudioEngine())
	{
		StopAllSounds();
	}

	for (size_t i = 0; i < DynamicSounds.size(); ++i)
	{
		DynamicSounds.at(i)->OnFinished.ClearFunction(); //Unbind callbacks to save on processing
		DynamicSounds.at(i)->Destroy();
	}
	ContainerUtils::Empty(OUT DynamicSounds);

	if (AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool())
	{
		localAudioPool->Destroy();
	}

	if (OwningEngine != nullptr && IsMainAudioEngine())
	{
		OwningEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, AudioEngineComponent, HandleCollectGarbage, void));
	}

	Super::ShutdownComponent();
}

#ifdef WITH_MULTI_THREAD
void AudioEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	//If the Engine contains a MultiThreadEngineComponent, then this engine component will depend on this component to run in multi-thread mode.
	if (OwningEngine != nullptr)
	{
		for (EngineComponent* engComp : OwningEngine->ReadEngineComponents())
		{
			if (dynamic_cast<MultiThreadEngineComponent*>(engComp) != nullptr)
			{
				outDependencies.push_back(MultiThreadEngineComponent::SStaticClass());
				break;
			}
		}
	}
}
#endif

bool AudioEngineComponent::IsMainAudioEngine () const
{
#ifdef WITH_MULTI_THREAD
	return !bThreadedSlave;
#else
	return true;
#endif
}

bool AudioEngineComponent::IsRunningMultiThreadMode () const
{
#ifdef WITH_MULTI_THREAD
	return bMultiThreadedMode;
#else
	return false;
#endif
}

bool AudioEngineComponent::CreateAudioChannel (const DString& channelName, Float channelVolume)
{
	if (channelName.IsEmpty())
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot create an audio channel with an empty name."));
		return false;
	}

	size_t channelHash = channelName.GenerateHash();
	if (AudioChannels.contains(channelHash))
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot create audio channel %s since that channel already exists."), channelName);
		return false;
	}

	AudioChannels.insert({channelHash, SAudioChannel(channelName, channelVolume)});
	return true;
}

bool AudioEngineComponent::SetChannelVolume (const DString& channelName, Float newVolume)
{
	size_t channelHash = channelName.GenerateHash();
	bool success = SetChannelVolume(channelHash, newVolume);
	if (!success)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot update the volume for audio channel %s since that channel doesn't exist."), channelName);
	}

	return success;
}

bool AudioEngineComponent::SetChannelVolume (size_t channelHash, Float newVolume)
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetChannelVolume(channelHash, newVolume);
	}
#endif

	if (AudioChannels.contains(channelHash))
	{
		AudioChannels.at(channelHash).Volume = newVolume;
		UpdateChannelVolume(channelHash);
		return true;
	}
	
	return false;
}

void AudioEngineComponent::RefreshSoundModeVolumes ()
{
	for (auto channel = AudioChannels.begin(); channel != AudioChannels.end(); ++channel)
	{
		channel->second.SoundModeVolume = 1.f;

		for (SoundMode* mode : SoundModes)
		{
			if (mode->GetStrength() > 0.f && mode->ReadChannelVolumes().contains(channel->first))
			{
				channel->second.SoundModeVolume *= (1.f - (mode->ReadChannelVolumes().at(channel->first) * mode->GetStrength()));
			}
		}

		UpdateChannelVolume(channel->first);
	}
}

bool AudioEngineComponent::SetChannelDilation (const DString& channelName, Float newDilation)
{
	size_t channelHash = channelName.GenerateHash();
	bool success = SetChannelDilation(channelHash, newDilation);
	if (!success)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot update the dilation for the audio channel %s since that channel doesn't exist."), channelName);
	}

	return success;
}

bool AudioEngineComponent::SetChannelDilation (size_t channelHash, Float newDilation)
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetChannelDilation(channelHash, newDilation);
	}
#endif

	if (AudioChannels.contains(channelHash))
	{
		AudioChannels.at(channelHash).TimeDilation = newDilation;
		UpdateChannelDilation(channelHash, newDilation);
		return true;
	}

	return false;
}

DynamicAudioInstance* AudioEngineComponent::SetupDynamicAsset (const DynamicAudioAsset* dynamicAsset) const
{
	DataBuffer emptyBuffer;
	return SetupDynamicAsset(dynamicAsset, true, emptyBuffer);
}

DynamicAudioInstance* AudioEngineComponent::SetupDynamicAsset (const DynamicAudioAsset* dynamicAsset, bool notifyPlaybackForParams, const DataBuffer& appendedParams) const
{
	if (dynamicAsset->GetFirstModifier() == nullptr)
	{
		//The asset doesn't need to create an instance since there aren't any modifiers.
		return nullptr;
	}

	DynamicAudioInstance* instance = dynamicAsset->CreateAudioInstance();
	if (instance == nullptr)
	{
		return nullptr;
	}

	//Reset the parameters to defaults for the next instance.
	dynamicAsset->GetFirstModifier()->RecursiveExecute([](AudioBufferModifier* modifier)
	{
		CHECK(modifier != nullptr)
		modifier->ResetAudioParameters();
	});

	//Allow playback interfaces to set parameters
	if (notifyPlaybackForParams)
	{
		for (SoundPlaybackInterface* playback : PlaybackInterfaces)
		{
			playback->SetAudioParameters(instance);
		}
	}

	if (appendedParams.GetNumBytes() > 0)
	{
		instance->SetParamsFromBuffer(appendedParams);
	}

	if (IsMainAudioEngine())
	{
		//Applying modifiers can be expensive since they may iterate or replace the audio buffers. Only apply on the audio engine instance that is going to play it.
		instance->ApplyModifiers();
	}

	return instance;
}

void AudioEngineComponent::PlayStaticSound (const AudioAsset* sound, Float volumeMultiplier, Float pitchMultiplier)
{
	if (sound == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot play a local sound without specifying a sound asset."));
		return;
	}

#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		if (const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(sound))
		{
			ThreadedDynamicAudioInstance* threadInstance = ThreadedDynamicAudioInstance::CreateObject();
			threadInstance->SetOwningAsset(dynAsset);

			//Record the parameters in this local thread. The data will be recorded and sent off to the sound thread.
			for (SoundPlaybackInterface* playbackInterface : PlaybackInterfaces)
			{
				playbackInterface->SetAudioParameters(threadInstance);
			}

			ThreadTransfer->PlayStaticSound(threadInstance, volumeMultiplier, pitchMultiplier);
			threadInstance->Destroy();
		}
		else
		{
			ThreadTransfer->PlayStaticSound(sound, volumeMultiplier, pitchMultiplier);
		}
		return;
	}
#endif

	if (const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(sound))
	{
		if (DynamicAudioInstance* instance = SetupDynamicAsset(dynAsset))
		{
			//Play the instance instead of the dynamic asset
			PlayStaticSound(instance, volumeMultiplier, pitchMultiplier);
			return;
		}

		//Treat the dynamic asset as a normal audio asset since it didn't create an instance.
	}

	if (sound->GetConflictHandling() == AudioAsset::CH_MustMatchChannel)
	{
		StopSoundsByChannel(sound->GetChannelHash());
	}

	Float channelVolume = 1.f;
	Float pitch = pitchMultiplier;
	if (!sound->ReadChannelName().IsEmpty() && AudioChannels.contains(sound->GetChannelHash()))
	{
		const SAudioChannel& channel = AudioChannels.at(sound->GetChannelHash());
		channelVolume = channel.Volume * channel.SoundModeVolume;
		pitch *= channel.TimeDilation;
	}

	Float duckVolume = 1.f;
	if (bIsDucking && sound->GetDuckPriority() < HighestDuckPriority && sound->GetDuckPriority() >= 0)
	{
		duckVolume = DuckVolumeMultiplier;
	}

	SActiveStaticSound& newSound = ActiveStaticSounds.emplace_back(SActiveStaticSound(sound, volumeMultiplier));
	newSound.SoundPlayer.setBuffer(*sound->GetBuffer());
	newSound.SoundPlayer.setVolume((volumeMultiplier * channelVolume * MasterVolume * duckVolume * 100.f).Value);
	newSound.SoundPlayer.setPitch(pitch.Value);
	newSound.SoundPlayer.setLoop(sound->IsLooping());
	newSound.SoundPlayer.play();

	if (sound->IsLooping() && sound->IsRandomStart())
	{
		//setPlayingOffset must be set when it's either playing or paused.
		Float randStart = RandomUtils::RandRange(0.f, sound->GetDuration());
		newSound.SoundPlayer.setPlayingOffset(sf::Time(sf::seconds(randStart.Value)));
	}

	if (sound->GetDuckPriority() > HighestDuckPriority)
	{
		CHECK(OwningEngine != nullptr)
		DuckingTimestamp = OwningEngine->GetElapsedTime();
		HighestDuckPriority = sound->GetDuckPriority();
		bIsDucking = true;
	}

	if (!sound->IsLooping())
	{
		Float duration = newSound.CalcTimeRemaining();
		if (!bIsPlayingFiniteStaticSound || duration < NextStaticEndTime)
		{
			NextStaticEndTime = duration;
			bIsPlayingFiniteStaticSound = true;
		}
	}

	OnSoundPlayed.Broadcast(sound, nullptr, nullptr);
	for (SoundPlaybackInterface* playback : PlaybackInterfaces)
	{
		playback->ProcessPlayedSound(sound, nullptr, nullptr);
	}

#ifdef WITH_MULTI_THREAD
	if (ThreadTransfer != nullptr)
	{
		ThreadTransfer->BroadcastSoundBegin(sound, nullptr, nullptr);
	}
#endif
}

size_t AudioEngineComponent::PlaySoundEvent (const SoundEvent& soundEvent, PlayerListenerComponent* listener)
{
	if (soundEvent.GetAsset() == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot play a sound event without specifying a sound asset."));
		return UINT_INDEX_NONE;
	}

	if (soundEvent.GetSource() == nullptr || listener == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot play a sound with spacial awareness without specifying the source of the sound and the listener component to hear it. To play sounds without spacial awareness, use PlayStaticSound instead."));
		return UINT_INDEX_NONE;
	}

	if (!IsMainAudioEngine())
	{
		//AudioComponents should have a ThreadedComponent that should execute this code in the correct thread. If this is called directly, log a warning.
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Playing dynamic sound outside from the main audio engine. Instead try playing sounds through AudioComponents to process the events."));
		return UINT_INDEX_NONE;
	}

	if (const DynamicAudioAsset* dynAsset = dynamic_cast<const DynamicAudioAsset*>(soundEvent.GetAsset()))
	{
		if (DynamicAudioInstance* instance = SetupDynamicAsset(dynAsset))
		{
			//Play the instance instead of the dynamic asset
			SoundEvent dynamicSoundEvent(soundEvent);
			dynamicSoundEvent.SetAsset(instance);
			return PlaySoundEvent(dynamicSoundEvent, listener);
		}

		//Treat the dynamic asset as a normal audio asset since it didn't create an instance.
	}

	//Process conflict handling
	switch (soundEvent.GetAsset()->GetConflictHandling())
	{
		case(AudioAsset::CH_None):
			break;

		case(AudioAsset::CH_MustMatchSrc):
			StopDynamicSounds(soundEvent.GetSource());
			break;

		case(AudioAsset::CH_MustMatchChannel):
			StopSoundsByChannel(soundEvent.GetAsset()->GetChannelHash());
			break;

		case(AudioAsset::CH_MustMatchSrcAndChannel):
			StopDynamicSounds(soundEvent.GetSource(), soundEvent.GetAsset()->GetChannelHash());
			break;
	}

	Float channelVolume = 1.f;
	Float soundModeVolume = 1.f;
	Float channelDilation = 1.f;
	if (!soundEvent.GetAsset()->ReadChannelName().IsEmpty() && AudioChannels.contains(soundEvent.GetAsset()->GetChannelHash()))
	{
		const SAudioChannel& channel = AudioChannels.at(soundEvent.GetAsset()->GetChannelHash());
		channelVolume = channel.Volume;
		soundModeVolume = channel.SoundModeVolume;
		channelDilation = channel.TimeDilation;
	}

	size_t availableIdx = UINT_INDEX_NONE;
	for (size_t i = 0; i < DynamicSounds.size(); ++i)
	{
		if (!DynamicSounds.at(i)->IsActive())
		{
			availableIdx = i;
			break;
		}
	}

	if (availableIdx == UINT_INDEX_NONE)
	{
		//All sound slots are active. Double the size of the vector.
		availableIdx = DynamicSounds.size();
		DynamicSounds.resize(DynamicSounds.size() * 2);
		for (size_t i = availableIdx; i < DynamicSounds.size(); ++i)
		{
			DynamicSounds.at(i) = DynamicSound::CreateObject();
			DynamicSounds.at(i)->OnFinished = SDFUNCTION_1PARAM(this, AudioEngineComponent, HandleDynamicSoundFinished, void, DynamicSound*);
		}
	}

	DynamicSounds.at(availableIdx)->SetChannelVolume(channelVolume);
	DynamicSounds.at(availableIdx)->SetSoundModeVolume(soundModeVolume);
	DynamicSounds.at(availableIdx)->SetChannelDilation(channelDilation);

	if (bIsDucking && soundEvent.GetAsset()->GetDuckPriority() < HighestDuckPriority && soundEvent.GetAsset()->GetDuckPriority() >= 0)
	{
		DynamicSounds.at(availableIdx)->SetDuckVolume(DuckVolumeMultiplier);
	}
	else
	{
		DynamicSounds.at(availableIdx)->SetDuckVolume(1.f);
	}

	DynamicSounds.at(availableIdx)->ActivateSound(soundEvent, listener);

	if (soundEvent.GetAsset()->GetDuckPriority() > HighestDuckPriority)
	{
		CHECK(OwningEngine != nullptr)
		DuckingTimestamp = OwningEngine->GetElapsedTime();
		HighestDuckPriority = soundEvent.GetAsset()->GetDuckPriority();
		bIsDucking = true;
	}

	OnSoundPlayed.Broadcast(soundEvent.GetAsset(), soundEvent.GetSource(), listener);
	for (SoundPlaybackInterface* playback : PlaybackInterfaces)
	{
		playback->ProcessPlayedSound(soundEvent.GetAsset(), soundEvent.GetSource(), listener);
	}

#ifdef WITH_MULTI_THREAD
	if (ThreadTransfer != nullptr)
	{
		ThreadTransfer->BroadcastSoundBegin(soundEvent.GetAsset(), soundEvent.GetSource(), listener);
	}
#endif

	return availableIdx;
}

size_t AudioEngineComponent::PlayDetachedSoundEvent (const SoundEvent& soundEvent, PlayerListenerComponent* listener)
{
	size_t idx = PlaySoundEvent(soundEvent, listener);
	if (idx != UINT_INDEX_NONE)
	{
		DynamicSounds.at(idx)->SeverSource();
	}

	return idx;
}

void AudioEngineComponent::DetachSoundsFromSource (AudioComponent* source)
{
	if (source != nullptr)
	{
		if (!IsMainAudioEngine())
		{
			//Rely on the AudioComponent to handle the thread transfer
			source->DetachFromActiveSounds();
			return;
		}

		for (DynamicSound* snd : DynamicSounds)
		{
			if (snd->IsActive() && snd->ReadEvent().GetSource() == source)
			{
				snd->SeverSource();
			}
		}
	}
}

void AudioEngineComponent::DetachSoundsFromListener (PlayerListenerComponent* listener)
{
	if (listener != nullptr)
	{
		if (!IsMainAudioEngine())
		{
			//Rely on the ListenerComponent to handle the thread transfer
			listener->SeverFromActiveSounds();
			return;
		}

		for (DynamicSound* snd : DynamicSounds)
		{
			if (snd->IsActive() && snd->GetListener() == listener)
			{
				snd->SeverListener();
			}
		}
	}
}

void AudioEngineComponent::StopStaticSounds (const AudioAsset* asset)
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr && asset != nullptr)
	{
		ThreadTransfer->StopStaticSound(asset->GetAudioHash());
		return;
	}
#endif

	size_t i = 0;
	while (i < ActiveStaticSounds.size())
	{
		if (ActiveStaticSounds.at(i).Asset == asset)
		{
			ProcessSoundEnd(asset, nullptr, nullptr);
			ActiveStaticSounds.at(i).SoundPlayer.stop();
			ActiveStaticSounds.erase(ActiveStaticSounds.begin() + i);
			continue;
		}

		++i;
	}

	//Don't bother checking current static sounds to update NextStaticEndTime. That variable is updated on Tick anyways, and it'll just call an extra FlushStoppedSounds, which should be harmless since this function already removed that static sound from the list.
}

void AudioEngineComponent::StopDynamicSounds (AudioComponent* source)
{
	if (!IsMainAudioEngine())
	{
		//Rely on the AudioComponent to handle the thread transfer
		source->InterruptSounds();
		return;
	}

	for (DynamicSound* snd : DynamicSounds)
	{
		if (snd->IsActive() && snd->ReadEvent().GetSource() == source)
		{
			snd->Stop();
		}
	}
}

void AudioEngineComponent::StopDynamicSounds (const AudioAsset* asset)
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr && asset != nullptr)
	{
		ThreadTransfer->StopDynamicSound(asset->GetAudioHash());
		return;
	}
#endif

	for (DynamicSound* snd : DynamicSounds)
	{
		if (snd->IsActive() && snd->ReadEvent().GetAsset() == asset)
		{
			snd->Stop();
		}
	}
}

void AudioEngineComponent::StopDynamicSounds (const AudioAsset* asset, AudioComponent* source)
{
	if (!IsMainAudioEngine())
	{
		//Rely on the AudioComponent to handle the thread transfer
		source->InterruptSounds(asset);
		return;
	}

	for (DynamicSound* snd : DynamicSounds)
	{
		if (snd->IsActive() && snd->ReadEvent().GetAsset() == asset && snd->ReadEvent().GetSource() == source)
		{
			snd->Stop();
		}
	}
}

void AudioEngineComponent::StopDynamicSounds (AudioComponent* source, const DString& audioChannel)
{
	size_t channelHash = audioChannel.GenerateHash();
	StopDynamicSounds(source, channelHash);
}

void AudioEngineComponent::StopDynamicSounds (AudioComponent* source, size_t audioChannelHash)
{
	if (!IsMainAudioEngine())
	{
		//Rely on the AudioComponent to handle the thread transfer
		source->InterruptSoundsByChannel(audioChannelHash);
		return;
	}

	for (DynamicSound* snd : DynamicSounds)
	{
		if (snd->IsActive() && snd->ReadEvent().GetSource() == source && snd->ReadEvent().GetAsset()->GetChannelHash() == audioChannelHash)
		{
			snd->Stop();
		}
	}
}

void AudioEngineComponent::StopSoundsByChannel (const DString& audioChannel)
{
	size_t channelHash = audioChannel.GenerateHash();
	StopSoundsByChannel(channelHash);
}

void AudioEngineComponent::StopSoundsByChannel (size_t audioChannelHash)
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->StopChannel(audioChannelHash);
		return;
	}
#endif

	size_t i = 0;
	while (i < ActiveStaticSounds.size())
	{
		CHECK(ActiveStaticSounds.at(i).Asset != nullptr)
		if (ActiveStaticSounds.at(i).Asset->GetChannelHash() == audioChannelHash)
		{
			ProcessSoundEnd(ActiveStaticSounds.at(i).Asset, nullptr, nullptr);
			ActiveStaticSounds.at(i).SoundPlayer.stop();
			ActiveStaticSounds.erase(ActiveStaticSounds.begin() + i);
			continue;
		}

		++i;
	}

	//Don't bother checking current static sounds to update NextStaticEndTime. That variable is updated on Tick anyways, and it'll just call an extra FlushStoppedSounds, which should be harmless since this function already removed that static sound from the list.

	for (DynamicSound* dynSound : DynamicSounds)
	{
		if (dynSound->IsActive() && dynSound->ReadEvent().GetAsset()->GetChannelHash() == audioChannelHash)
		{
			dynSound->Stop();
		}
	}
}

void AudioEngineComponent::StopAllSounds ()
{
#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->StopAllSounds();
		return;
	}
#endif

	for (SActiveStaticSound& sound : ActiveStaticSounds)
	{
		ProcessSoundEnd(sound.Asset, nullptr, nullptr);
		sound.SoundPlayer.stop();
	}
	ContainerUtils::Empty(OUT ActiveStaticSounds);
	bIsPlayingFiniteStaticSound = false;
	NextStaticEndTime = -1.f;

	for (DynamicSound* sound : DynamicSounds)
	{
		sound->Stop();
	}
}

void AudioEngineComponent::RegisterSoundMode (SoundMode* newSoundMode)
{
	if (newSoundMode != nullptr)
	{
		ContainerUtils::AddUnique(OUT SoundModes, newSoundMode);
	}
}

void AudioEngineComponent::UnregisterSoundMode (SoundMode* target)
{
	ContainerUtils::RemoveItem(OUT SoundModes, target);
}

void AudioEngineComponent::RegisterAudioComponent (AudioComponent* newComp)
{
	if (newComp == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot register a null AudioComponent to the AudioEngineComponent."));
		return;
	}

	if (AudioComponents.contains(newComp->GetAudioCompId()))
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("The AudioComponent with ID %s is already registered to the AudioEngineComponent."), newComp->GetAudioCompId());
		return;
	}

	AudioComponents.insert({newComp->GetAudioCompId(), newComp});
}

void AudioEngineComponent::UnregisterAudioComponent (AudioComponent* target)
{
	if (target != nullptr && AudioComponents.contains(target->GetAudioCompId()))
	{
		AudioComponents.erase(target->GetAudioCompId());
	}
}

void AudioEngineComponent::RegisterListenerComponent (SoundListenerComponent* newComp)
{
	if (newComp == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot register a null SoundListenerComponent to the AudioEngineComponent."));
		return;
	}

	if (ListenerComponents.contains(newComp->GetListenerId()))
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("The SoundListenerComponent with ID %s is already registered to the AudioEngineComponent."), newComp->GetListenerId());
		return;
	}

	ListenerComponents.insert({newComp->GetListenerId(), newComp});
}

void AudioEngineComponent::UnregisterListenerComponent (SoundListenerComponent* target)
{
	if (target != nullptr && ListenerComponents.contains(target->GetListenerId()))
	{
		ListenerComponents.erase(target->GetListenerId());
	}

	//Remove this listener reference from the dynamic sounds.
	for (DynamicSound* snd : DynamicSounds)
	{
		if (snd->GetListener() == target)
		{
			snd->SeverListener();
		}
	}
}

void AudioEngineComponent::RegisterPlaybackInterface (SoundPlaybackInterface* newInterface)
{
	ContainerUtils::AddUnique(OUT PlaybackInterfaces, newInterface);
}

void AudioEngineComponent::UnregisterPlaybackInterface (SoundPlaybackInterface* target)
{
	ContainerUtils::RemoveItem(OUT PlaybackInterfaces, target);
}

void AudioEngineComponent::SetSpeedOfSound (Float newSpeedOfSound)
{
	SpeedOfSound = newSpeedOfSound;

	if (IsMainAudioEngine())
	{
		DopplerEffect::SpeedOfSound = SpeedOfSound;
	}
#ifdef WITH_MULTI_THREAD
	else if (ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetSpeedOfSound(SpeedOfSound);
	}
#endif
}

void AudioEngineComponent::SetMasterVolume (Float newMasterVolume)
{
	MasterVolume = newMasterVolume;

	if (IsMainAudioEngine())
	{
		//Update all active sounds
		for (SActiveStaticSound& staticSound : ActiveStaticSounds)
		{
			if (staticSound.SoundPlayer.getStatus() == sf::SoundSource::Stopped)
			{
				continue;
			}

			//Find the channel associated with this asset
			Float channelVolume = 1.f;
			if (!staticSound.Asset->ReadChannelName().IsEmpty() && AudioChannels.contains(staticSound.Asset->GetChannelHash()))
			{
				const SAudioChannel& channel = AudioChannels.at(staticSound.Asset->GetChannelHash());
				channelVolume = channel.Volume * channel.SoundModeVolume;
			}

			Float duckVolume = 1.f;
			if (bIsDucking && staticSound.Asset->GetDuckPriority() < HighestDuckPriority && staticSound.Asset->GetDuckPriority() >= 0)
			{
				duckVolume = DuckVolumeMultiplier;
			}

			staticSound.SoundPlayer.setVolume((staticSound.Volume * channelVolume * MasterVolume * duckVolume * 100.f).Value);
		}

		DynamicSound::SetMasterVolume(MasterVolume);
	}

#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetMasterVolume(MasterVolume);
	}
#endif
}

void AudioEngineComponent::SetDuckVolumeMultiplier (Float newDuckVolumeMultiplier)
{
	DuckVolumeMultiplier = newDuckVolumeMultiplier;

#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetDuckVolume(DuckVolumeMultiplier);
	}
#endif
}

void AudioEngineComponent::SetDuckLerpDuration (Float newDuckLerpDuration)
{
	DuckLerpDuration = newDuckLerpDuration;

#ifdef WITH_MULTI_THREAD
	if (!IsMainAudioEngine() && ThreadTransfer != nullptr)
	{
		ThreadTransfer->SetDuckLerp(DuckLerpDuration);
	}
#endif
}

#ifdef WITH_MULTI_THREAD
void AudioEngineComponent::InitMultiThread ()
{
	CHECK(OwningEngine != nullptr)

	//If the OwningEngine doesn't have a MultiThreadEngineComponent, assume that the AudioEngineComponent will run single threaded.
	const std::vector<EngineComponent*>& engineComps = OwningEngine->ReadEngineComponents();
	bool bHasMultiThreadEngine = false;
	for (EngineComponent* engineComp : engineComps)
	{
		if (dynamic_cast<MultiThreadEngineComponent*>(engineComp) != nullptr)
		{
			bHasMultiThreadEngine = true;
			break;
		}
	}

	if (!bHasMultiThreadEngine)
	{
		//The engine is not running with a multi threaded component. Run the main AudioEngineComponent in this thread.
		bThreadedSlave = false;
		bMultiThreadedMode = false;
		return;
	}

	bThreadedSlave = (OwningEngine->GetEngineIndex() != MULTI_THREAD_SOUND_IDX);
	if (!bThreadedSlave)
	{
		//The main audio engine component doesn't need to do anything more. The external AudioEngineComponent will instantiate its ThreadedSoundTransfer object.
		return;
	}

	//Assume only the AudioEngineComponent on the main thread will launch the sound thread.
	if (OwningEngine->IsMainThread())
	{
		SDThread thread;
		THREAD_INIT_TYPE threadInit;
		threadInit.ThreadName = TXT("Sound Thread");
		int exitCode = OS_CreateThread(thread, RunSoundEngine, nullptr, &threadInit);
		if (exitCode != 0)
		{
			GetSoundLog().Log(LogCategory::LL_Critical, TXT("Failed to launch the sound thread. Exit code:  %s"), Int(exitCode));
			bMultiThreadedMode = false;
			bThreadedSlave = false;
			return;
		}
	}

	Int timeoutTime = 30000; //30 seconds
	Int totalWaitTime = 0;
	Int sleepInterval = 500; //half second

	//Wait for the sound engine to finish initializing
	while (Engine::GetEngine(MULTI_THREAD_SOUND_IDX) == nullptr || !Engine::GetEngine(MULTI_THREAD_SOUND_IDX)->IsInitialized())
	{
		totalWaitTime += sleepInterval;
		if (totalWaitTime >= timeoutTime)
		{
			GetSoundLog().Log(LogCategory::LL_Critical, TXT("Failed to launch the sound thread. Application timed out after waiting for %s ms for the sound engine to initialize."), timeoutTime);
			bMultiThreadedMode = false;
			bThreadedSlave = false;
			return;
		}

		OS_Sleep(sleepInterval);
	}

	if (OwningEngine->IsMainEngine())
	{
		GetSoundLog().Log(LogCategory::LL_Log, TXT("Sound thread initialized and ready! The AudioEngineComponent is now running in multi threaded mode."));
	}

	ThreadTransfer = ThreadedSoundTransfer::CreateObject();
}
#endif

void AudioEngineComponent::UpdateChannelVolume (size_t channelHash)
{
	CHECK(AudioChannels.contains(channelHash))
	const SAudioChannel& channel = AudioChannels.at(channelHash);

	for (SActiveStaticSound& staticSound : ActiveStaticSounds)
	{
		if (staticSound.Asset->GetChannelHash() == channelHash && staticSound.SoundPlayer.getStatus() != sf::SoundSource::Stopped)
		{
			Float duckVolume = 1.f;
			if (bIsDucking && staticSound.Asset->GetDuckPriority() < HighestDuckPriority && staticSound.Asset->GetDuckPriority() >= 0)
			{
				duckVolume = DuckVolumeMultiplier;
			}

			staticSound.SoundPlayer.setVolume((staticSound.Volume * channel.Volume * channel.SoundModeVolume * MasterVolume * duckVolume * 100.f).Value);
		}
	}

	for (DynamicSound* dynamicSound : DynamicSounds)
	{
		CHECK(dynamicSound != nullptr)
		if (dynamicSound->IsActive() && dynamicSound->ReadEvent().GetAsset()->GetChannelHash() == channelHash)
		{
			dynamicSound->SetChannelVolume(channel.Volume);
			dynamicSound->SetSoundModeVolume(channel.SoundModeVolume);
		}
	}
}

void AudioEngineComponent::UpdateChannelDilation (size_t channelHash, Float newDilation)
{
	for (SActiveStaticSound& staticSound : ActiveStaticSounds)
	{
		if (staticSound.Asset->GetChannelHash() == channelHash && staticSound.SoundPlayer.getStatus() != sf::SoundSource::Stopped)
		{
			staticSound.SoundPlayer.setPitch(newDilation.Value);
		}
	}

	//Update static end time
	if (bIsPlayingFiniteStaticSound)
	{
		NextStaticEndTime = -1.f;

		//Must iterate all static sounds regardless of the channel since the affected sounds could have been pushed out from the 'lead'.
		for (const SActiveStaticSound& staticSound : ActiveStaticSounds)
		{
			if (!staticSound.Asset->IsLooping() && staticSound.SoundPlayer.getStatus() != sf::SoundSource::Stopped)
			{
				Float duration = staticSound.CalcTimeRemaining();
				if (NextStaticEndTime <= 0.f || duration < NextStaticEndTime)
				{
					NextStaticEndTime = duration;
				}
			}
		}
	}

	for (DynamicSound* dynamicSound : DynamicSounds)
	{
		if (dynamicSound->IsActive() && dynamicSound->ReadEvent().GetAsset()->GetChannelHash() == channelHash)
		{
			dynamicSound->SetChannelDilation(newDilation);
		}
	}
}

void AudioEngineComponent::ProcessSoundEnd (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener)
{
	if (asset == nullptr)
	{
		return;
	}

	//This function should only be called on the thread that's playing sounds.
	CHECK(IsMainAudioEngine())

	if (bIsDucking && asset->GetDuckPriority() >= HighestDuckPriority)
	{
		//The sound with highest priority finished playing. Find the next player with the highest priority.
		Int newPriority = 0;
		for (const SActiveStaticSound& staticSound : ActiveStaticSounds)
		{
			if (staticSound.SoundPlayer.getStatus() == sf::Sound::Stopped)
			{
				continue;
			}

			CHECK(staticSound.Asset != nullptr)
			if (staticSound.Asset->GetDuckPriority() > newPriority)
			{
				newPriority = staticSound.Asset->GetDuckPriority();
			}
		}

		for (DynamicSound* dynSound : DynamicSounds)
		{
			if (!dynSound->IsActive())
			{
				continue;
			}

			const AudioAsset* asset = dynSound->ReadEvent().GetAsset();
			CHECK(asset != nullptr)
			if (asset->GetDuckPriority() > newPriority)
			{
				newPriority = asset->GetDuckPriority();
			}
		}

		if (newPriority < HighestDuckPriority)
		{
			CHECK(OwningEngine != nullptr)
			DuckingTimestamp = OwningEngine->GetElapsedTime();
			HighestDuckPriority = newPriority;
		}
	}

	//Add to the pending list to be broadcasted at the end of Tick. Can't do it here to prevent event handlers from playing new sounds while the AudioEngineComponent is accessing static/dynamic sounds.
	PendingSoundEndEvents.push_back({asset, source, listener});
}

void AudioEngineComponent::FlushStoppedSounds ()
{
	//This function should only be called on the engine instance that's playing sounds.
	CHECK(IsMainAudioEngine())

	bIsPlayingFiniteStaticSound = false;

	size_t i = 0;
	while (i < ActiveStaticSounds.size())
	{
		if (ActiveStaticSounds.at(i).SoundPlayer.getStatus() == sf::SoundSource::Stopped)
		{
			const AudioAsset* asset = ActiveStaticSounds.at(i).Asset;
			ActiveStaticSounds.erase(ActiveStaticSounds.begin() + i);
			ProcessSoundEnd(asset, nullptr, nullptr);
			continue;
		}

		if (!ActiveStaticSounds.at(i).Asset->IsLooping())
		{
			Float timeRemaining = ActiveStaticSounds.at(i).CalcTimeRemaining();
			if (timeRemaining > 0.f)
			{
				if (NextStaticEndTime <= 0.f || timeRemaining < NextStaticEndTime)
				{
					NextStaticEndTime = timeRemaining;
					bIsPlayingFiniteStaticSound = true;
				}
			}
		}

		++i;
	}
}

void AudioEngineComponent::ProcessDucking ()
{
	//Should only be called on the engine instance that's playing sounds.
	CHECK(IsMainAudioEngine())

	if (!bIsDucking)
	{
		//Not currently playing any sounds with higher priority than defaults.
		return;
	}

	Float ratio = Utils::Min<Float>((OwningEngine->GetElapsedTime() - DuckingTimestamp) / DuckLerpDuration, 1.f);
	Float quietVol = Utils::Lerp<Float>(ratio, 1.f, DuckVolumeMultiplier); //Lerping direction when a sound needs to duck.
	Float loudVol = Utils::Lerp<Float>(ratio, DuckVolumeMultiplier, 1.f); //Lerping direction when a sound needs to transition out from ducking.

	//Update static sounds
	for (SActiveStaticSound& staticSound : ActiveStaticSounds)
	{
		if (staticSound.SoundPlayer.getStatus() == sf::SoundSource::Stopped)
		{
			continue;
		}

		if (staticSound.Asset->GetDuckPriority() < 0)
		{
			//Assets with negative duck priority are immune to ducking.
			continue;
		}

		//Find the channel associated with this asset
		Float channelVolume = 1.f;
		if (!staticSound.Asset->ReadChannelName().IsEmpty() && AudioChannels.contains(staticSound.Asset->GetChannelHash()))
		{
			const SAudioChannel& channel = AudioChannels.at(staticSound.Asset->GetChannelHash());
			channelVolume = channel.Volume * channel.SoundModeVolume;
		}

		bool bGoingQuiet = (staticSound.Asset->GetDuckPriority() < HighestDuckPriority);
		if (bGoingQuiet)
		{
			Float expectedVol = quietVol * staticSound.Volume * channelVolume * MasterVolume * 100.f; 
			if (expectedVol > staticSound.SoundPlayer.getVolume()) //Condition is needed in case this player is quieter than expected. This can happen if this player got a head start from a previous ducking event.
			{
				staticSound.SoundPlayer.setVolume(expectedVol.Value);
			}
		}
		else
		{
			Float expectedVol = loudVol * staticSound.Volume * channelVolume * MasterVolume * 100.f;
			if (expectedVol < staticSound.SoundPlayer.getVolume()) //In case this player is already louder than the current ducking transition. This can happen if the player got a head start from a previous ducking event.
			{
				staticSound.SoundPlayer.setVolume(expectedVol.Value);
			}
		}
	}

	//Update dynamic sounds
	for (DynamicSound* dynamicSound : DynamicSounds)
	{
		if (!dynamicSound->IsActive())
		{
			continue;
		}

		CHECK(dynamicSound->ReadEvent().GetAsset() != nullptr)
		if (dynamicSound->ReadEvent().GetAsset()->GetDuckPriority() < 0)
		{
			//Assets with negative duck priority are immune to ducking.
			continue;
		}

		bool bGoingQuiet = (dynamicSound->EditEvent().GetAsset()->GetDuckPriority() < HighestDuckPriority);
		if (bGoingQuiet)
		{
			if (quietVol > dynamicSound->GetDuckVolume())
			{
				dynamicSound->SetDuckVolume(quietVol);
			}
		}
		else if (loudVol < dynamicSound->GetDuckVolume())
		{
			dynamicSound->SetDuckVolume(loudVol);
		}
	}

	if (ratio >= 1.f && HighestDuckPriority == 0)
	{
		bIsDucking = false;
	}
}

#ifdef WITH_MULTI_THREAD
void AudioEngineComponent::ProcessExternalSoundBegin (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener)
{
	OnSoundPlayed.Broadcast(asset, source, listener);
	for (SoundPlaybackInterface* playbackInterface : PlaybackInterfaces)
	{
		playbackInterface->ProcessPlayedSound(asset, source, listener);
	}
}

void AudioEngineComponent::ProcessExternalSoundEnd (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener)
{
	OnSoundEnded.Broadcast(asset, source, listener);
	for (SoundPlaybackInterface* playbackInterface : PlaybackInterfaces)
	{
		playbackInterface->ProcessStoppedSound(asset, source, listener);
	}
}
#endif

void AudioEngineComponent::HandleDynamicSoundFinished (DynamicSound* finishedPlayer)
{
	CHECK(finishedPlayer != nullptr)
	ProcessSoundEnd(finishedPlayer->ReadEvent().GetAsset(), finishedPlayer->ReadEvent().GetSource(), finishedPlayer->GetListener());
}

void AudioEngineComponent::HandleCollectGarbage ()
{
	//Clear references before the garbage collector deletes them.
	for (size_t i = 0; i < PendingSoundEndEvents.size(); ++i)
	{
		if (std::get<0>(PendingSoundEndEvents.at(i))->GetPendingDelete())
		{
			std::get<0>(PendingSoundEndEvents.at(i)) = nullptr;
		}

		if (std::get<1>(PendingSoundEndEvents.at(i)) != nullptr && std::get<1>(PendingSoundEndEvents.at(i))->GetPendingDelete())
		{
			std::get<1>(PendingSoundEndEvents.at(i)) = nullptr;
		}

		if (std::get<2>(PendingSoundEndEvents.at(i)) != nullptr && std::get<2>(PendingSoundEndEvents.at(i))->GetPendingDelete())
		{
			std::get<2>(PendingSoundEndEvents.at(i)) = nullptr;
		}
	}
}

#ifdef WITH_MULTI_THREAD
SD_THREAD_FUNCTION(RunSoundEngine)
{
	Engine* soundEngine = new Engine();
	CHECK(soundEngine != nullptr)

	Float soundThreadUpdateRate = 0.05f; //20 fps
	soundEngine->SetMinDeltaTime(soundThreadUpdateRate);

	MultiThreadEngineComponent* multiThreadEngine = new MultiThreadEngineComponent();

	std::vector<EngineComponent*> engineComponents;
	engineComponents.push_back(multiThreadEngine);
	engineComponents.push_back(new AudioEngineComponent(true));

	soundEngine->InitializeEngine(MULTI_THREAD_SOUND_IDX, engineComponents);
#ifdef DEBUG_MODE
	soundEngine->SetDebugName(TXT("Sound Engine"));
#endif

	if (ThreadManager* manager = multiThreadEngine->GetThreadManager())
	{
		//Set a grace period on the sound thread to allow the main thread to send data to the sound thread without freezing the main engine.
		manager->SetGracePeriod(soundThreadUpdateRate);
	}

	while (true)
	{
		soundEngine->Tick();
		if (soundEngine->IsShuttingDown())
		{
			break;
		}
	}

	delete soundEngine;
	soundEngine = nullptr;

	SD_EXIT_THREAD
}
#endif
SD_END