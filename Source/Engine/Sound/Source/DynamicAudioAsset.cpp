/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicAudioAsset.cpp
=====================================================================
*/

#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"

IMPLEMENT_CLASS(SD::DynamicAudioAsset, SD::AudioAsset)
SD_BEGIN

void DynamicAudioAsset::InitProps ()
{
	Super::InitProps();

	FirstModifier = nullptr;
}

DynamicAudioInstance* DynamicAudioAsset::CreateAudioInstance () const
{
	DynamicAudioInstance* result = DynamicAudioInstance::CreateObject();
	result->SetOwningAsset(this);

	return result;
}
SD_END