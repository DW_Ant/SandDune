/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundUnitTest.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "SoundUnitTest.h"
#include "UnitTestListenerComponent.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::SoundUnitTest, SD::UnitTester)
SD_BEGIN

bool SoundUnitTest::RunTests (EUnitTestFlags testFlags) const
{
	bool bPassed = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bPassed &= TestSoundEvents(testFlags);
	}

	return bPassed;
}

bool SoundUnitTest::TestSoundEvents (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Sound Events"));

	SetTestCategory(testFlags, TXT("Listener Transforms"));
	{
		SceneEntity* entity = SceneEntity::CreateObject();
		std::function<void()> terminateTest([&]()
		{
			entity->Destroy();
			entity = nullptr;
		});

		/*
		Create the following component tree
		entity
			SceneTransform (sceneA)
				TickComponent
					TickComponent (leafTickA)
			SceneTransform (sceneB)
				SceneTransform (sceneC)
					TickComponent
						TickComponent (leafTickB)
			TickComponent
				SceneTransform (sceneD)
					TickComponent
						SceneTransform (sceneE)
							TickComponent
								TickComponent
									TickComponent (leafTickC)
		*/

		SceneTransformComponent* sceneA = nullptr;
		SceneTransformComponent* sceneB = nullptr;
		SceneTransformComponent* sceneC = nullptr;
		SceneTransformComponent* sceneD = nullptr;
		SceneTransformComponent* sceneE = nullptr;
		TickComponent* leafTickA = nullptr;
		TickComponent* leafTickB = nullptr;
		TickComponent* leafTickC = nullptr;

		sceneA = SceneTransformComponent::CreateObject();
		if (entity->AddComponent(sceneA))
		{
			TickComponent* tickA = TickComponent::CreateObject(TICK_GROUP_DEBUG);
			if (sceneA->AddComponent(tickA))
			{
				leafTickA = TickComponent::CreateObject(TICK_GROUP_DEBUG);
				if (tickA->AddComponent(leafTickA))
				{
					//Noop
				}
			}
		}

		sceneB = SceneTransformComponent::CreateObject();
		if (entity->AddComponent(sceneB))
		{
			sceneC = SceneTransformComponent::CreateObject();
			if (sceneB->AddComponent(sceneC))
			{
				TickComponent* tickA = TickComponent::CreateObject(TICK_GROUP_DEBUG);
				if (sceneC->AddComponent(tickA))
				{
					leafTickB = TickComponent::CreateObject(TICK_GROUP_DEBUG);
					if (tickA->AddComponent(leafTickB))
					{
						//Noop
					}
				}
			}
		}

		TickComponent* tickA = TickComponent::CreateObject(TICK_GROUP_DEBUG);
		if (entity->AddComponent(tickA))
		{
			sceneD = SceneTransformComponent::CreateObject();
			if (tickA->AddComponent(sceneD))
			{
				TickComponent* tickB = TickComponent::CreateObject(TICK_GROUP_DEBUG);
				if (sceneD->AddComponent(tickB))
				{
					sceneE = SceneTransformComponent::CreateObject();
					if (tickB->AddComponent(sceneE))
					{
						TickComponent* tickC = TickComponent::CreateObject(TICK_GROUP_DEBUG);
						if (sceneE->AddComponent(tickC))
						{
							TickComponent* tickE = TickComponent::CreateObject(TICK_GROUP_DEBUG);
							if (tickC->AddComponent(tickE))
							{
								leafTickC = TickComponent::CreateObject(TICK_GROUP_DEBUG);
								if (tickE->AddComponent(leafTickC))
								{
									//Noop
								}
							}
						}
					}
				}
			}
		}

		UnitTestListenerComponent* testListener = UnitTestListenerComponent::CreateObject();
		SceneTransform* expected = nullptr;
		std::function<bool(Entity*)> testTransform([&](Entity* attachTo)
		{
			//If attachTo is nullptr, then assume the test listener is already attached to the correct component.
			if (attachTo != nullptr)
			{
				testListener->DetachSelfFromOwner();
				if (!attachTo->AddComponent(testListener))
				{
					UnitTestError(testFlags, TXT("Listener Transform test failed. Unable to attach a listener component to a %s."), attachTo->ToString());
					terminateTest();
					return false;
				}
			}

			SceneTransform* actual = testListener->GetOwnerTransform();
			if (expected != actual)
			{
				UnitTestError(testFlags, TXT("Listener Transform test failed. After attaching the listener to a %s, the listener's transform reference does not match the expected transform reference."), testListener->GetOwner()->ToString());
				terminateTest();
				return false;
			}

			std::vector<EntityComponent*> expectedComps;

			//All components between listener and the scene transform should have a callback registered on owner changed.
			for (Entity* owner = testListener->GetOwner(); owner != nullptr; /* Noop */ )
			{
				if (EntityComponent* comp = dynamic_cast<EntityComponent*>(owner))
				{
					if (dynamic_cast<SceneTransform*>(comp) != nullptr)
					{
						//The closest scene transform is found. They are not expected to have a callback registered.
						break;
					}

					expectedComps.push_back(comp);
					owner = comp->GetOwner();
					continue;
				}

				break;
			}

			for (ComponentIterator iter(entity, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				bool expectCallback = (ContainerUtils::FindInVector(expectedComps, iter.GetSelectedComponent()) != UINT_INDEX_NONE);
				bool hasCallback = iter.GetSelectedComponent()->OnOwnerChanged.IsRegistered(SDFUNCTION_2PARAM(testListener, SoundListenerComponent, HandleOwnerChanged, void, EntityComponent*, Entity*), true);

				if (expectCallback != hasCallback)
				{
					DString expectedStr = (expectCallback) ? DString::EmptyString : TXT(" NOT ");
					DString actualStr = (hasCallback) ? TXT("does") : TXT("doesn't");
					UnitTestError(testFlags, TXT("Listener Transform test failed. The component %s is %s expected to have an HandleOwnerChanged callback. But it %s have a callback."), iter.GetSelectedComponent()->ToString(), expectedStr, actualStr);
					terminateTest();
					return false;
				}
			}

			return true;
		});

		expected = entity;
		if (!testTransform(entity))
		{
			return false;
		}

		expected = sceneA;
		if (!testTransform(sceneA))
		{
			return false;
		}

		if (!testTransform(leafTickA))
		{
			return false;
		}

		expected = sceneC;
		if (!testTransform(leafTickB))
		{
			return false;
		}

		expected = sceneB;
		if (!testTransform(sceneB))
		{
			return false;
		}
		
		expected = sceneE;
		if (!testTransform(leafTickC))
		{
			return false;
		}

		//Moving sceneD shouldn't affect what the transform references
		sceneD->DetachSelfFromOwner();
		if (leafTickB->AddComponent(sceneD))
		{
			//Noop
		}

		if (!testTransform(nullptr))
		{
			return false;
		}

		//Move leafTickC to leafTickA. This should automatically update the transform reference.
		leafTickC->DetachSelfFromOwner();
		if (leafTickA->AddComponent(leafTickC))
		{
			//Noop
		}

		expected = sceneA;
		if (!testTransform(nullptr))
		{
			return false;
		}

		terminateTest();
	}
	CompleteTestCategory(testFlags);

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	if (localAudioEngine == nullptr || !localAudioEngine->IsMainAudioEngine())
	{
		TestLog(testFlags, TXT("The Broadcasting Events tests require an AudioEngineComponent instance in the sound thread (for multi threaded applications), or running in single threaded mode for this synchronous test to run. Skipping tests..."));
	}
	else
	{
		SetTestCategory(testFlags, TXT("Broadcasting Events"));
		{
			AudioAsset* dummyAsset = AudioAsset::CreateObject();
			SceneEntity* broadcaster = SceneEntity::CreateObject();
			SceneEntity* listener = SceneEntity::CreateObject();

			std::function<void()> terminateTest([&]()
			{
				dummyAsset->Destroy();
				dummyAsset = nullptr;

				broadcaster->Destroy();
				broadcaster = nullptr;

				listener->Destroy();
				listener = nullptr;
			});

			AudioComponent* audioComp = AudioComponent::CreateObject();
			if (!broadcaster->AddComponent(audioComp))
			{
				UnitTestError(testFlags, TXT("Broadcasting Events test failed. Unable to attach an audio component to the broadcaster."));
				terminateTest();
				return false;
			}

			UnitTestListenerComponent* listenerComp = UnitTestListenerComponent::CreateObject();
			if (!listener->AddComponent(listenerComp))
			{
				UnitTestError(testFlags, TXT("Broadcasting Events test failed. Unable to attach a listener component to a SceneEntity."));
				terminateTest();
				return false;
			}

			Int expected;
			std::function<bool()> testEvent([&]()
			{
				listenerComp->ResetTest();
				broadcaster->CalculateAbsoluteTransform();
				listener->CalculateAbsoluteTransform();
				audioComp->PlaySound(dummyAsset, 1.f);
				Int actual = listenerComp->GetNumEventsReceived();
				if (actual != expected)
				{
					UnitTestError(testFlags, TXT("Broadcasting Events test failed. After broadcasting an event from position %s with radius %s, the listener component from position %s was able to receive the event %s time(s). It's expected to receive the event %s time(s)."), broadcaster->ReadAbsTranslation(), audioComp->GetOuterRadius(), listener->ReadAbsTranslation(), actual, expected);
					terminateTest();
					return false;
				}

				return true;
			});

			broadcaster->SetTranslation(Vector3::ZERO_VECTOR);
			listener->SetTranslation(Vector3::ZERO_VECTOR);
			audioComp->SetOuterRadius(1000.f);
			expected = 1;
			if (!testEvent())
			{
				return false;
			}

			listener->SetTranslation(Vector3(-990.f, 0.f, 0.f));
			if (!testEvent())
			{
				return false;
			}

			listener->SetTranslation(Vector3(-1010.f, 0.f, 0.f));
			expected = 0;
			if (!testEvent())
			{
				return false;
			}

			audioComp->SetOuterRadius(5000.f);
			expected = 1;
			if (!testEvent())
			{
				return false;
			}

			listener->SetTranslation(Vector3(4950.f, 0.f, -4950.f));
			expected = 0;
			if (!testEvent())
			{
				return false;
			}

			terminateTest();
		}
		CompleteTestCategory(testFlags);
	}

	ExecuteSuccessSequence(testFlags, TXT("Sound Events"));
	return true;
}
SD_END
#endif