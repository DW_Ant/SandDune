/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicSound.cpp
=====================================================================
*/

#include "AudioAsset.h"
#include "AudioComponent.h"
#include "DynamicSound.h"
#include "PlayerListenerComponent.h"

IMPLEMENT_CLASS(SD::DynamicSound, SD::Entity)
SD_BEGIN

Float DynamicSound::MasterVolume(1.f);
Vector3 DynamicSound::DirectionMultiplier(1.f, 1.f, 1.f);
Float DynamicSound::DirectionSoundThreshold(100.f); //1 decimeter (it's distance squared)

void DynamicSound::InitProps ()
{
	Super::InitProps();

	SoundPlayer = nullptr;
	ChannelVolume = 1.f;
	SoundModeVolume = 1.f;
	ChannelDilation = 1.f;
	DuckVolume = 1.f;
	Tick = nullptr;
	DormantTickInterval = 0.2f;
	CalcDirectionInterval = 0.1f; //10 times a second
	DopplerPitchOverride = -1.f;

	CalcDirectionTime = -1.f;
	bIsExecutingOnFinished = false;
}

void DynamicSound::BeginObject ()
{
	Super::BeginObject();

	if (SoundPlayer == nullptr)
	{
		SoundPlayer = new sf::Sound();
	}

	Tick = TickComponent::CreateObject(TICK_GROUP_SOUND);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, DynamicSound, HandleTick, void, Float));
		Tick->SetTicking(false);
	}
}

void DynamicSound::Destroyed ()
{
	Stop();

	if (SoundPlayer != nullptr)
	{
		delete SoundPlayer;
		SoundPlayer = nullptr;
	}

	Super::Destroyed();
}

void DynamicSound::ActivateSound (const SoundEvent& inEvent, PlayerListenerComponent* inListener)
{
	CHECK(SoundPlayer != nullptr)

	if (IsActive())
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot activate a DynamicSound Entity that is already playing or paused. It must be stopped first."));
		return;
	}

	if (inListener == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot activate a sound without a listener."));
		return;
	}

	if (inEvent.GetAsset() == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot activate a sound with an event that's missing a reference to an audio asset."));
		return;
	}

	if (inEvent.GetSource() == nullptr)
	{
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot activate a sound with an event that's missing a reference to a source."));
		return;
	}

	if (bIsExecutingOnFinished)
	{
		//It's recommended not to play new sounds during a finished sound event.
		GetSoundLog().Log(LogCategory::LL_Warning, TXT("Cannot activate a sound on a sound player while it's executing its OnFinished delegate. The DynamicSound is about to clear its references to its SoundEvent, which would caues problems to the new sound event."));
		return;
	}

	Listener = inListener;
	Event = inEvent;
	Doppler.SetListener(Listener.Get());
	Doppler.SetSource(Event.GetSource());
	SoundPlayer->setBuffer(*Event.GetAsset()->GetBuffer());
	SoundPlayer->setVolume((Event.GetVolumeMultiplier() * ChannelVolume * SoundModeVolume * MasterVolume * DuckVolume * 100.f).Value);
	SoundPlayer->setLoop(Event.GetAsset()->IsLooping());
	SoundPlayer->setPosition(0.f, 0.f, 0.f);
	DopplerPitchOverride = -1.f;
	UpdatePitch(0.f); //Update the DopplerDeltaPos and apply the event's pitch multiplier.
	SoundPlayer->play();

	if (Event.GetAsset()->IsLooping() && Event.GetAsset()->IsRandomStart())
	{
		//setPlayingOffset must be set when it's either playing or paused.
		Float randStart = RandomUtils::RandRange(0.f, Event.GetAsset()->GetDuration());
		SoundPlayer->setPlayingOffset(sf::Time(sf::seconds(randStart.Value)));
	}

	Tick->SetTicking(true);
	Tick->SetTickInterval(-1.f);
	Tick->ResetAccumulatedTickTime(); //Reset accumulated time to ensure the delta time is accurate for this sound reset.
	CalcDirectionTime = -1.f; //Ensure direction is calculated immediately.
}

bool DynamicSound::IsActive () const
{
	return (SoundPlayer != nullptr && SoundPlayer->getStatus() != sf::SoundSource::Stopped);
}

void DynamicSound::SeverSource ()
{
	Event.SeverSource();
	Doppler.SetSource(nullptr);
}

void DynamicSound::SeverListener ()
{
	if (Listener != nullptr && Listener->GetOwnerTransform() != nullptr)
	{
		LastKnownListenerLocation = Listener->GetOwnerTransform()->GetAbsTranslation();
	}

	Listener = nullptr;
	Doppler.SetListener(nullptr);
}

void DynamicSound::Stop ()
{
	bool wasActive = IsActive(); //Avoid calling OnFinished if Stop is called on an inactive DynamicSound.

	CHECK(Tick != nullptr)
	Tick->SetTicking(false);

	if (SoundPlayer != nullptr)
	{
		SoundPlayer->stop();
	}

	//Broadcast OnFinished before this Entity loses references to the listener and sound event. Must be called after stop since the event handler may check if this player IsActive().
	if (wasActive && OnFinished.IsBounded())
	{
		bIsExecutingOnFinished = true;
		OnFinished(this);
		bIsExecutingOnFinished = false;
	}

	Event.ClearReferences();
	Listener = nullptr;
	Doppler.SetSource(nullptr);
	Doppler.SetListener(nullptr);
}

Vector3 DynamicSound::GetListenerLocation () const
{
	if (Listener != nullptr && Listener->GetOwnerTransform() != nullptr)
	{
		return Listener->GetOwnerTransform()->GetAbsTranslation();
	}

	return LastKnownListenerLocation;
}

void DynamicSound::SetMasterVolume (Float newMasterVolume)
{
	MasterVolume = newMasterVolume;
}

void DynamicSound::SetDirectionMultiplier (const Vector3& newDirectionMultiplier)
{
	DirectionMultiplier = newDirectionMultiplier;
}

void DynamicSound::SetChannelVolume (Float newChannelVolume)
{
	ChannelVolume = newChannelVolume;
}

void DynamicSound::SetSoundModeVolume (Float newSoundModeVolume)
{
	SoundModeVolume = newSoundModeVolume;
}

void DynamicSound::SetChannelDilation (Float newChannelDilation)
{
	ChannelDilation = newChannelDilation;
	UpdatePitch(0.f);
}

void DynamicSound::SetDuckVolume (Float newDuckVolume)
{
	DuckVolume = newDuckVolume;
}

void DynamicSound::SetListener (PlayerListenerComponent* newListener)
{
	if (newListener == nullptr)
	{
		SeverListener();
		return;
	}

	Listener = newListener;
}

void DynamicSound::SetCalcDirectionInterval (Float newCalcDirectionInterval)
{
	CalcDirectionInterval = newCalcDirectionInterval;
}

void DynamicSound::SetDopplerPitchOverride (Float newDopplerPitchOverride)
{
	DopplerPitchOverride = newDopplerPitchOverride;
}

Float DynamicSound::CalcAttenuation (const Vector3& srcPos, const Vector3& listenerPos) const
{
	Float dist = (srcPos - listenerPos).VSize();
	if (dist <= Event.GetInnerRadius())
	{
		return 1.f;
	}
	else if (dist >= Event.GetOuterRadius())
	{
		return 0.f;
	}

	Float ratio = (dist - Event.GetInnerRadius()) / (Event.GetOuterRadius() - Event.GetInnerRadius());
	Float result = Utils::Lerp<Float>(ratio, 1.f, 0.f);
	return result;
}

bool DynamicSound::CalcDirection (const Vector3& srcPos, Vector3& outSoundPos) const
{
	CHECK(Listener != nullptr && Listener->GetOwnerTransform())

	Vector3 relativePos = srcPos - Listener->GetOwnerTransform()->ReadAbsTranslation();

	//Rotate relativePos around the origin based on the rotation of the Listener.
	//If the relativePos is in front of the listener's rotation, then it needs to rotate about the origin so that the end vector will be (dist, 0, 0). Positive X is forward direction.
	//If the relativePos is to the right of the listener's rotation, then it needs to rotate about the origin so that the end vector will be (0, dist, 0).
	TransformMatrix transform;
	transform.Translate(relativePos);

	const Rotator& absDir = Listener->GetOwnerTransform()->ReadAbsRotation();
	transform.Rotate(absDir);

	outSoundPos = transform.GetTranslation();

	return true;
}

void DynamicSound::UpdatePitch (Float deltaSec)
{
	if (SoundPlayer != nullptr)
	{
		Float dopplerScale = 1.f;
		if (DopplerPitchOverride >= 0.f)
		{
			dopplerScale = DopplerPitchOverride;
		}
		else if (ReadEvent().GetAsset() != nullptr && ReadEvent().GetAsset()->IsDopplerEnabled())
		{
			dopplerScale = Doppler.CalcEffect(deltaSec);
		}

		SoundPlayer->setPitch((ChannelDilation * Event.GetPitchMultiplier() * dopplerScale).Value);
	}
}

void DynamicSound::HandleTick (Float deltaSec)
{
	if (!IsActive())
	{
		//Sound player naturally stopped because it finished playing the sound.
		Tick->SetTicking(false);
		if (OnFinished.IsBounded())
		{
			bIsExecutingOnFinished = true;
			OnFinished(this);
			bIsExecutingOnFinished = false;
		}

		return;
	}

	CHECK(SoundPlayer != nullptr) //IsActive should have checked this
	Vector3 srcPos = Event.GetSourceLocation();
	Vector3 listenerPos = GetListenerLocation();

	Float attenuationMultiplier = CalcAttenuation(srcPos, listenerPos);
	if (attenuationMultiplier <= 0.f)
	{
		Tick->SetTickInterval(DormantTickInterval);
		SoundPlayer->setVolume(0.f);
		if (Event.GetAsset()->IsLooping())
		{
			SoundPlayer->pause();
		}

		return;
	}
	else
	{
		Tick->SetTickInterval(-1.f);
		if (SoundPlayer->getStatus() == sf::SoundSource::Paused && Event.GetAsset()->IsLooping())
		{
			SoundPlayer->play();
		}
	}

	SoundPlayer->setVolume((attenuationMultiplier * Event.GetVolumeMultiplier() * ChannelVolume * SoundModeVolume * MasterVolume * DuckVolume * 100.f).Value);

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	if ((localEngine->GetElapsedTime() - CalcDirectionTime) >= CalcDirectionInterval)
	{
		CalcDirectionTime = localEngine->GetElapsedTime();

		//Update direction
		if (Event.GetAsset()->GetNumChannels() == 1 && Listener != nullptr && Listener->GetOwnerTransform() != nullptr)
		{
			Vector3 relativePos;
			if (CalcDirection(srcPos, OUT relativePos) && relativePos.CalcDistSquared() > DirectionSoundThreshold)
			{
				relativePos *= DirectionMultiplier;
				relativePos.NormalizeInline();

				//Convert the Vector from SD to SFML coordinates (SDx = SFx, SDy = SFz, SDz = SFy).
				SoundPlayer->setPosition(sf::Vector3f(relativePos.X.Value, relativePos.Z.Value, relativePos.Y.Value));
			}
			else
			{
				SoundPlayer->setPosition(sf::Vector3f(0.f, 0.f, 0.f));
			}
		}
	}

	if (Event.GetAsset()->IsDopplerEnabled())
	{
		UpdatePitch(deltaSec);
	}
}
SD_END