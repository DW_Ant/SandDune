/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundMode.h
  An object that influences the channel volume levels to apply a certain
  feel to the application. All volume levels are compounded with other systems that
  also influence the levels such as other SoundModes, user preferences, and
  master volume.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class SOUND_API SoundMode : public Entity
{
	DECLARE_CLASS(SoundMode)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPendingLevelAdjust
	{
		size_t ChannelHash;
		Float Volume;

		SPendingLevelAdjust (size_t inChannelHash, Float inVolume);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	static const Int FLAG_LEVEL_ADJUST;
	static const Int FLAG_ACTIVATE;
	static const Int FLAG_DEACTIVATE;

	/* The key of the map is equal to the channel's hash. */
	std::unordered_map<size_t, Float> ChannelVolumes;

	TickComponent* TransitionTick;

	/* Determines how activate this sound mode is. 0 implies inactive, 0-1 implies partial strength, 1 implies full strength. */
	Float Strength;

#ifdef WITH_MULTI_THREAD
	ThreadedComponent* ThreadComp;

	/* Becomes true if this SoundMode resides in the AudioThread. */
	bool bInSoundThread;

	/* Maximum time this SoundMode would wait when there are pending data requests. */
	Float MaxWaitTime;

	/* Flags describing what kind of data is waiting to be sent across threads. */
	Int PendingFlags;

	std::vector<SPendingLevelAdjust> PendingLevelAdjust;
	Float PendingActivationTime;
	Float PendingDeactivationTime;
#endif

private:
	/* Engine timestamp when this sound mode first started activating/deactivating. */
	Float StartTransitionTime;

	/* Time it takes for the strength to reach its destination (in seconds). */
	Float TransitionDuration;

	/* The strength value the transition is moving from. */
	Float StrengthSource;

	/* The desired strength value the transition is moving towards. */
	Float StrengthDestination;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Determines what the specified channel's volume will be while this sound mode is active.
	 */
	virtual void SetLevelAdjust (const DString& channelName, Float channelVolume);
	virtual void SetLevelAdjust (size_t channelHash, Float channelVolume);

	/**
	 * Starts the process of applying this sound mode's channel volumes over time.
	 * The transition time determines how long it takes to completely transition the volume levels (in seconds).
	 */
	virtual void Activate (Float transitionTime);
	virtual void Deactivate (Float transitionTime);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
#ifdef WITH_MULTI_THREAD
	virtual void SetMaxWaitTime (Float newMaxWaitTime);
#endif


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::unordered_map<size_t, Float>& ReadChannelVolumes () const
	{
		return ChannelVolumes;
	}

	inline Float GetStrength () const
	{
		return Strength;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sets up the procedure in starting a transition.
	 */
	virtual void StartTransition (Float transitionTime, Float desiredStrength);

	/**
	 * Immediately removes this SoundMode from the local AudioEngineComponent without waiting for the transition time.
	 */
	virtual void RemoveSoundMode ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTransitionTick (Float deltaSec);
#ifdef WITH_MULTI_THREAD
	virtual void HandleCopyData (DataBuffer& outData);
	virtual void HandleReceiveData (const DataBuffer& incomingData);
#endif
};
SD_END