/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioEngineComponent.h
  An engine component that acts like the manager for the audio engine.

  It contains the following responsibilities:
  - The capability to play sounds on the output device.
  - Maintain a list of all active sounds.

  The AudioEngineComponent can run in single threaded or multi threaded mode.

  Single Threaded Mode:
  - Use single threaded mode for simple applications or applications that requires
  frame perfect updates (eg: rhythm games).
  -Pros: Sounds are played with "frame-perfect" up to date information such as position
  awareness. There is also no delay between calling interrupt/sever sounds and when it's executed
  on the sound thread. The delay can be in milliseconds. It's also simpler to debug.

  Multi Threaded Mode:
  - Use Multi Threaded mode for complex applications that needs the extra CPU cycles to
  improve frame rate for other systems on the main thread.
  - The Sound's work load for calculating expensive effects are handled on the sound thread.
  This prevents any hitching if the game needs to calculate the buffer data when applying an
  effect (such as reverb and occlusion) for a large asset.
  - The MultiThreadEngineComponent is required to exist for the AudioEngineComponent to
  enter multi threaded mode.
=====================================================================
*/

#pragma once

#include "Sound.h"
#include "SoundEvent.h"

SD_BEGIN
class AudioAsset;
class AudioComponent;
class DynamicAudioAsset;
class DynamicAudioInstance;
class DynamicSound;
class PlayerListenerComponent;
class SoundListenerComponent;
class SoundMode;
class SoundPlaybackInterface;

#ifdef WITH_MULTI_THREAD
class ThreadedSoundTransfer;
#endif

class SOUND_API AudioEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(AudioEngineComponent)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SAudioChannel
	{
		/* The name of this channel that uniquely distinguishes itself from other channels. */
		DString ChannelName;

		/* Volume multiplier to apply to all sounds using this channel. This is meant for the user configuration to customize the audio balance. */
		Float Volume;

		/* Volume multipliers applied from all active sound modes. */
		Float SoundModeVolume;

		/* The speed modification to apply to all sounds using this channel. 0.5 means all sounds in this channel will play at half speed. NOTE: Changing speed also affects the sounds' pitch. */
		Float TimeDilation;

		SAudioChannel (const DString& inChannelName, Float inVolume);
	};

protected:
	//Simple struct containing a sound player. This struct assumes no spacial awareness.
	struct SActiveStaticSound
	{
		/* The SFML object that handles playing the sound. */
		sf::Sound SoundPlayer;

		/* The sound asset used to populate the SoundPlayer's buffer. */
		const AudioAsset* Asset;

		/* The default volume multiplier when this sound is played. This factor does not consider volume settings. */
		Float Volume;

		SActiveStaticSound (const AudioAsset* inAsset, Float inVolume);

		/* Returns the number of seconds remaining on this static sound. This considers the sound's current play position and pitch adjustments. */
		Float CalcTimeRemaining () const;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString AUDIO_CHANNEL_GENERAL;
	static const DString AUDIO_CHANNEL_DIALOGUE;
	static const DString AUDIO_CHANNEL_UI;
	static const DString AUDIO_CHANNEL_MUSIC;

	/* Delegate broadcasted whenever a static or dynamic sound is playing on the speakers. */
	MulticastDelegate<const AudioAsset* /*asset*/, AudioComponent* /*src*/, PlayerListenerComponent* /*listener*/> OnSoundPlayed;

	/* Delegate broadcasted whenever a sound finished or stopped. The source or listener may be null if it's a static sound that finished, a dynamic sound with a severed source, or the source was destroyed. */
	MulticastDelegate<const AudioAsset* /*asset*/, AudioComponent* /*src*/, PlayerListenerComponent* /*listener*/> OnSoundEnded;

protected:
	/* Determines how quickly sounds can travel across air (in cms per second). Adjust this value to exaggerate Doppler affects and play sound delays. */
	Float SpeedOfSound;

	/* List of all channels allocated from this application. The key of the audio channel is the hash of the audio channel's name. */
	std::unordered_map<size_t, SAudioChannel> AudioChannels;

	/* List of all sound modes that are adjusting the channel volumes. Sound Modes are expected to invoke refresh whenever their strength values changed. */
	std::vector<SoundMode*> SoundModes;

	/* List of all static sounds that are currently playing on this computer's speakers. */
	std::vector<SActiveStaticSound> ActiveStaticSounds;

	/* List of all sounds with spacial awareness that can play on this computer's speakers.
	Dormant instances can be repurposed to play other sounds.
	The AudioEngine takes ownership over these Entities, and will destroy them on shutdown. */
	std::vector<DynamicSound*> DynamicSounds;

	/* The global volume multiplier for all sounds that the user configured. */
	Float MasterVolume;

	/* Volume multiplier to apply to played sounds while it's ducking. */
	Float DuckVolumeMultiplier;

	/* Number of seconds it takes to lerp a ducking sounds in and out (in seconds). */
	Float DuckLerpDuration;

	/* List of all AudioComponents created in this thread. The engine component will not take ownership over these components. The key of this map is the AudioComponent's ID. */
	std::unordered_map<Int, AudioComponent*, IntKeyHash, IntKeyEqual> AudioComponents;

	/* List of all SoundListenerComponents created in this thread. The engine component will not take ownership over these components. The key of this map is the ListenerComponent's ID. */
	std::unordered_map<Int, SoundListenerComponent*, IntKeyHash, IntKeyEqual> ListenerComponents;

	/* List of all playback interfaces registered to this engine component. */
	std::vector<SoundPlaybackInterface*> PlaybackInterfaces;

	/* If true, then there is another AudioEngineComponent instance in a separate thread. */
	bool bMultiThreadedMode;

#ifdef WITH_MULTI_THREAD
	/* If true, then this engine component is a mirror to the actual engine component. The slave engine component is responsible for notifying the master audio engine of updates invoked from an external thread. */
	bool bThreadedSlave;

	/* Object responsible for transferring data between AudioEngineComponents across threads. */
	DPointer<ThreadedSoundTransfer> ThreadTransfer;
#endif

private:
	/* Number of seconds remaining that is predicted when at least one static sound is going to terminate. */
	Float NextStaticEndTime;

	/* Becomes true if this engine component is playing at least one static sound that is not looping. Used for determine if NextStaticEndTime needs to be evaluated. */
	bool bIsPlayingFiniteStaticSound;

	/* The highest sound priority currently being played in Static and Dynamic sounds. */
	Int HighestDuckPriority;

	/* Becomes true if this engine is currently ducking sounds or transitioning to or from ducking sounds. */
	bool bIsDucking;

	/* Timestamp when the ducking sound started to apply (in seconds). */
	Float DuckingTimestamp;

	/* List of assets with their sources that are pending to be broadcasted in an OnSoundEnded delegate. This must be executed separate from the actual end event
	to prevent event handlers from playing new sounds/writing to the AudioEngineComponent while it's processing a sound end event. */
	std::vector<std::tuple<const AudioAsset*, AudioComponent*, PlayerListenerComponent*>> PendingSoundEndEvents;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	AudioEngineComponent (bool multiThreadMode = false);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg) override;
	virtual void PreTick (Float deltaSec) override;
	virtual void PostTick (Float deltaSec) override;
	virtual void ShutdownComponent () override;

protected:
#ifdef WITH_MULTI_THREAD
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;
#endif


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this AudioEngineComponent is responsible for playing sounds.
	 * For single threaded applications, this is always true.
	 * For multi threaded applications, this is only true if it's the AudioEngineComponent instance residing on the sound thread.
	 */
	virtual bool IsMainAudioEngine () const;

	/**
	 * Returns true if there are more than one AudioEngineComponent instances on separate threads.
	 */
	virtual bool IsRunningMultiThreadMode () const;

	/**
	 * If the channel name is unique, a new audio channel will be created and registered to this engine component.
	 * Returns true if created and registered.
	 */
	virtual bool CreateAudioChannel (const DString& channelName, Float channelVolume);

	/**
	 * Change the volume of the audio channel that matches the given name.
	 * This also updates the audio balance of all active playing sounds to reflect the change.
	 * Returns true if the specified channel was found and updated.
	 */
	virtual bool SetChannelVolume (const DString& channelName, Float newVolume);
	virtual bool SetChannelVolume (size_t channelHash, Float newVolume);

	/**
	 * Updates the static and dynamic sounds based on the active sound modes.
	 */
	virtual void RefreshSoundModeVolumes ();

	/**
	 * Changes the time dilation of the audio channel that matches the given name.
	 * This also updates the dilation of all active playing sounds to reflect the change.
	 * Returns true if the specified channel was found and updated.
	 */
	virtual bool SetChannelDilation (const DString& channelName, Float newDilation);
	virtual bool SetChannelDilation (size_t channelHash, Float newDilation);

	/**
	 * Sets up a DynamicAudioInstance from the given asset. Returns the instance intended to be played.
	 * Returns nullptr if the dynamic asset didn't or doesn't need to create an instance (eg: A dynamic audio asset without any modifiers).
	 * The resulting instance expects that it'll be played in this engine component so it will know when to auto destroy itself. Otherwise, the caller of this function is expected to take ownership of this object.
	 */
	virtual DynamicAudioInstance* SetupDynamicAsset (const DynamicAudioAsset* dynamicAsset) const;
	virtual DynamicAudioInstance* SetupDynamicAsset (const DynamicAudioAsset* dynamicAsset, bool notifyPlaybackForParams, const DataBuffer& appendedParams) const;

	/**
	 * The simplest method to directly play a sound without going through AudioComponents or SoundEvents.
	 * @param sound The audio asset that contains the buffer data to play.
	 * @param volumeMultiplier amplifies the sound volume by this value. For normalization purposes, it's best to keep this between 0 and 1.
	 * @param pitchMultiplier Multiplies the pitch where higher values increases the frequency (high pitched). 1 leaves the pitch to defaults.
	 */
	virtual void PlayStaticSound (const AudioAsset* sound, Float volumeMultiplier, Float pitchMultiplier);

	/**
	 * Plays a sound on the computer's speakers using the given sound event to determine the behavior.
	 * Returns the index of the DynamicSound vector that is played. Returns UINT_INDEX_NONE if it's not playing.
	 */
	virtual size_t PlaySoundEvent (const SoundEvent& soundEvent, PlayerListenerComponent* listener);

	virtual size_t PlayDetachedSoundEvent (const SoundEvent& soundEvent, PlayerListenerComponent* listener);

	/**
	 * Iterates through each active sound, and if the sound is broadcasted from the given source, it'll sever the connection between the audio component and the sound.
	 */
	virtual void DetachSoundsFromSource (AudioComponent* source);

	/**
	 * Iterates through each active sound, and if the sound is received from the given listener, it'll sever the connection between the listener and the sound event.
	 */
	virtual void DetachSoundsFromListener (PlayerListenerComponent* listener);

	/**
	 * Iterates through each static sound, and stops any sound playing the specified asset.
	 */
	virtual void StopStaticSounds (const AudioAsset* asset);

	/**
	 * Iterates through each dynamic sound, and stops any sound being played by the specified AudioComponent.
	 */
	virtual void StopDynamicSounds (AudioComponent* source);

	/**
	 * Iterates through each dynamic sound, and stops any sound playing the specified asset.
	 */
	virtual void StopDynamicSounds (const AudioAsset* asset);

	/**
	 * Iterates through each dynamic sound, and stops any sound playing the specified asset broadcasted from the given AudioComponent.
	 */
	virtual void StopDynamicSounds (const AudioAsset* asset, AudioComponent* source);

	/**
	 * Iterates through each dynamic sound, and stops any sound playing from the specified AudioComponent that also happens to play an asset with matching audio channel.
	 */
	virtual void StopDynamicSounds (AudioComponent* source, const DString& audioChannel);
	virtual void StopDynamicSounds (AudioComponent* source, size_t audioChannelHash);

	/**
	 * Iterates through all static and dynamic sounds. Any of them that are playing an asset with the specified channel, are stopped.
	 */
	virtual void StopSoundsByChannel (const DString& audioChannel);
	virtual void StopSoundsByChannel (size_t audioChannelHash);

	/**
	 * Stops all active sounds being played on the speakers.
	 */
	virtual void StopAllSounds ();

	virtual void RegisterSoundMode (SoundMode* newSoundMode);
	virtual void UnregisterSoundMode (SoundMode* target);
	virtual void RegisterAudioComponent (AudioComponent* newComp);
	virtual void UnregisterAudioComponent (AudioComponent* target);
	virtual void RegisterListenerComponent (SoundListenerComponent* newComp);
	virtual void UnregisterListenerComponent (SoundListenerComponent* target);
	virtual void RegisterPlaybackInterface (SoundPlaybackInterface* newInterface);
	virtual void UnregisterPlaybackInterface (SoundPlaybackInterface* target);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSpeedOfSound (Float newSpeedOfSound);
	virtual void SetMasterVolume (Float newMasterVolume);
	virtual void SetDuckVolumeMultiplier (Float newDuckVolumeMultiplier);
	virtual void SetDuckLerpDuration (Float newDuckLerpDuration);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetSpeedOfSound () const
	{
		return SpeedOfSound;
	}

	inline std::unordered_map<size_t, SAudioChannel> GetAudioChannels () const
	{
		return AudioChannels;
	}

	inline const std::unordered_map<size_t, SAudioChannel>& ReadAudioChannels () const
	{
		return AudioChannels;
	}

	inline const std::vector<DynamicSound*>& ReadDynamicSounds () const
	{
		return DynamicSounds;
	}

	inline Float GetMasterVolume () const
	{
		return MasterVolume;
	}

	inline Float GetDuckVolumeMultiplier () const
	{
		return DuckVolumeMultiplier;
	}

	inline Float GetDuckLerpDuration () const
	{
		return DuckLerpDuration;
	}

	inline const std::unordered_map<Int, AudioComponent*, IntKeyHash, IntKeyEqual>& ReadAudioComponents () const
	{
		return AudioComponents;
	}

	inline const std::unordered_map<Int, SoundListenerComponent*, IntKeyHash, IntKeyEqual>& ReadListenerComponents () const
	{
		return ListenerComponents;
	}

	inline const std::vector<SoundPlaybackInterface*>& ReadPlaybackInterfaces () const
	{
		return PlaybackInterfaces;
	}

#ifdef WITH_MULTI_THREAD
	inline ThreadedSoundTransfer* GetThreadTransfer () const
	{
		return ThreadTransfer.Get();
	}
#endif


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
#ifdef WITH_MULTI_THREAD
	/**
	 * Creates and initializes this component to handle multi thread.
	 * If this is executed in a thread external to the sound thread, it'll check if it exists or not. If it doesn't exist, it'll initialize the sound thread.
	 * In addition to that, it'll initialize the thread related objects.
	 */
	virtual void InitMultiThread ();
#endif
	/**
	 * Iterates through all static and dynamic sounds, and if they're playing a sound of an asset matching the given channel,
	 * this function will adjust its volume to reflect its new channel volume and the current sound modes' compounded volumes.
	 */
	virtual void UpdateChannelVolume (size_t channelHash);

	/**
	 * Iterates through all static and dynamic sounds, and if they're playing a sound of an asset matching the given channel,
	 * this function will adjust its speed to reflect the new change.
	 */
	virtual void UpdateChannelDilation (size_t channelHash, Float newDilation);

	/**
	 * A sound player (Dynamic or Static) finished playing their sound. This function processes what happens when a sound finishes.
	 */
	virtual void ProcessSoundEnd (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener);

	/**
	 * Iterates through each static sound and flushes them from the vector to clear space.
	 */
	virtual void FlushStoppedSounds ();

	/**
	 * Iterates through each static and dynamic sounds, and applies ducking volume multipliers to each sound.
	 */
	virtual void ProcessDucking ();

#ifdef WITH_MULTI_THREAD
	/**
	 * Function called from the ThreadedSoundTransfer. This function is executed whenever a sound is played from the other engine instance.
	 */
	virtual void ProcessExternalSoundBegin (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener);

	/**
	 * Function called from the ThreadedSoundTransfer. This function is executed whenever a played sound ended from the other engine instance.
	 */
	virtual void ProcessExternalSoundEnd (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener);
#endif


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleDynamicSoundFinished (DynamicSound* finishedPlayer);
	virtual void HandleCollectGarbage ();

#ifdef WITH_MULTI_THREAD
friend class ThreadedSoundTransfer;
#endif
};
SD_END