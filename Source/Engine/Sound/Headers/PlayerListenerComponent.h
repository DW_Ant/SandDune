/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlayerListenerComponent.h
  A listener component that will play a sound on the computer's speakers from
  each sound event it receives.

  This component is typically attached to the camera the player is viewing from.
=====================================================================
*/

#pragma once

#include "DopplerEffect.h"
#include "SoundListenerComponent.h"

SD_BEGIN
class AudioComponent;

class SOUND_API PlayerListenerComponent : public SoundListenerComponent
{
	DECLARE_CLASS(PlayerListenerComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SDoppler
	{
		/* The class responsible for calculating the doppler effect. */
		DopplerEffect Doppler;

		/* The number of active sounds being played from the doppler's source. This is used to determine when the listener should remove this doppler instance. */
		Int NumSoundsFromSource;

		SDoppler ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
#ifdef WITH_MULTI_THREAD
	static const Int FLAG_COPY_ID;
	static const Int FLAG_COPY_PROPS;
	static const Int FLAG_UPDATE_POS;
	static const Int FLAG_TELEPORT_POS;
	static const Int FLAG_UPDATE_PITCH;
	static const Int FLAG_SEVER_SOUNDS;
#endif

	/* If true, then all sounds this component hear are potentially able to apply the Doppler effect. The Doppler effect only applies if the source, the asset, and this component enables it.*/
	bool bEnableDoppler;

	/* Prevents changing the pitch from the doppler affect until all dynamic sounds are processed once. If there are 5 active sounds, each of them will decrement this
	value by one. Doppler affect is enabled when this reaches zero.	This is typically set when the owner is teleporting to avoid sudden pitch shifts for a vast change in positions. */
	Int TeleportDopplerCount;

#ifdef WITH_MULTI_THREAD
	ThreadedComponent* ThreadComp;

	/* Quick flag used to quickly check if this component resides in the sound thread or not. */
	bool bInSoundThread;

	/* Flags that defines what kind of data needs to be transferred across threads. */
	Int PendingFlags;

	/* Maximum number of seconds to wait before transferring nonessential data across threads. */
	Float MaxWaitTime;

	/* Doppler Effect used to calculate pitches for each source. This is calculated on the MainThread instead of the SoundThread primarily because thread locking is inconsistent.
	Changing the pitch is very sensitive to the subtle changes in movement. It's calculated in this thread since this thread has more continuous and up-to-date movement.
	The ListenerComponent will then send the resulting doppler's pitch multiplier to the sound thread. */
	std::vector<SDoppler> Dopplers;
	TickComponent* DopplerTick;

	/* Map of pending doppler pitches waiting to be sent across threads. The key to this map is the AudioComponent's ID. */
	std::unordered_map<Int, Float, IntKeyHash, IntKeyEqual> PendingPitchMultipliers;
#endif


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void HearSoundEvent (const SoundEvent& newSoundEvent) override;

protected:
	virtual void OwnerTransformChanged () override;
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	virtual bool ShouldGenerateId () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Detaches this listener component from any active playing sound associated with this component.
	 * This allows the sound to continue to play from this component's last known location.
	 */
	virtual void SeverFromActiveSounds ();

	/**
	 * Temporarily disables the doppler pitch adjustments until a frame later.
	 */
	virtual void TeleportListenerComp ();

#ifdef WITH_MULTI_THREAD
	/**
	 * Iterates through each DopplerEffect, and if any elements matches the specified AudioComponent will be removed from the list.
	 */
	virtual void RemoveSourceFromDoppler (AudioComponent* source);
#endif


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetEnableDoppler (bool newEnableDoppler);

#ifdef WITH_MULTI_THREAD
	virtual void SetMaxWaitTime (Float newMaxWaitTime);
#endif


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsDopplerEnabled () const
	{
		return (bEnableDoppler && TeleportDopplerCount <= 0);
	}

#ifdef WITH_MULTI_THREAD
	inline Float GetMaxWaitTime () const
	{
		return MaxWaitTime;
	}
#endif


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Decrements the doppler teleport counter. If it's down to zero, all dynamic sounds can then update their pitches based on the doppler affect.
	 */
	virtual void DecrementDopplerCounter ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
#ifdef WITH_MULTI_THREAD
	virtual void HandleDopplerTick (Float deltaSec);
	virtual void HandleThreadedSoundEnded (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp);
	virtual void HandleAbsTransformChanged ();
	virtual void HandleCopyData (DataBuffer& outExternalData);
	virtual void HandleReceiveData (const DataBuffer& incomingData);
#endif

friend class DopplerEffect;
};
SD_END