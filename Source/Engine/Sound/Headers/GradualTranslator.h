/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GradualTranslator.h
  This Entity is responsible for lerping positions over time.

  When running with a multi-threaded AudioEngineComponent, the owner Entities of
  AudioComponents and PlayerListenerComponents may receive position updates at a
  low frequency to minimize thread locking. The problem with this frequency is that
  updates for DynamicSounds would sound jagged rather than continuous.

  That is the purpose of this Entity. This Entity is the owner of AudioComponents and
  PlayerListenerComponents in the sound thread. Whenever it receives position updates,
  instead of snapping to that position, it'll gradually translate itself to that new position.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
#ifdef WITH_MULTI_THREAD
class SOUND_API GradualTranslator : public SceneEntity
{
	DECLARE_CLASS(GradualTranslator)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The multiplier that determines the GradualTranslator's MoveTime in relation to the AudioComponent's update rate. */
	static const Float MoveTimeMultiplier;

	/* The number of seconds it takes for this Entity to reach their destination. */
	Float MoveTime;

	/* The timestamp when a position update was last sent. */
	Float LastUpdateTime;

	/* The position of this Entity when an update was last received. */
	Vector3 MoveFrom;

	/* The destination this Entity is moving towards. */
	Vector3 MoveTo;

	TickComponent* Tick;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetDestination (const Vector3& newMoveTo);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMoveTime (Float newMoveTime);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetMoveTime () const
	{
		return MoveTime;
	}


	/*
	=====================
	  Event
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
#endif
SD_END