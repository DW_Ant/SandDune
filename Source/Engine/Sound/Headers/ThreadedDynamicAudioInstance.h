/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadedDynamicAudioInstance.h
  Same as a DynamicAudioInstance, but this asset is not intended to
  reference audio buffers, nor is it intended to be played on the speakers.

  Whenever a parameter is set, the ThreadedDynamicAudioInstance will record
  the data to a general data buffer so it can be passed to the sound thread
  in order to be replicated to the real DynamicAudioInstance.
=====================================================================
*/

#pragma once

#include "DynamicAudioInstance.h"

#ifdef WITH_MULTI_THREAD
SD_BEGIN
class SOUND_API ThreadedDynamicAudioInstance : public DynamicAudioInstance
{
	DECLARE_CLASS(ThreadedDynamicAudioInstance)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* DataBuffer that records each parameter assignment. */
	DataBuffer ParamRecords;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void SetAudioStringParam (const HashedString& paramName, const DString& paramValue) override;
	virtual void SetAudioIntParam (const HashedString& paramName, Int paramValue) override;
	virtual void SetAudioFloatParam (const HashedString& paramName, Float paramValue) override;
	virtual void SetAudioBoolParam (const HashedString& paramName, bool paramValue) override;
	virtual void ApplyModifiers () override;

protected:
	virtual bool ShouldRegisterAudioCallback () const override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const DataBuffer& ReadParamRecords () const
	{
		return ParamRecords;
	}

	inline DataBuffer& EditParamRecords ()
	{
		return ParamRecords;
	}
};
SD_END
#endif