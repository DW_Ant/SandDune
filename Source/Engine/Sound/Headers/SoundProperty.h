/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundProperty.h
  A simple class that maps a string to a variable.

  This class also interfaces with the editor.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
template<class T>
class SOUND_API SoundProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The actual data of this property. */
	T ParamValue;

protected:
	/* The name of the property used to identify this instance from others. */
	HashedString ParamName;

	/* The value of parameter before anything was assigned to it. */
	T DefaultValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SoundProperty (const HashedString& inParamName, T inParamValue) :
		ParamValue(inParamValue),
		ParamName(inParamName),
		DefaultValue(inParamValue)
	{
		//Noop
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	inline void ResetToDefault ()
	{
		ParamValue = DefaultValue;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const HashedString& ReadParamName () const
	{
		return ParamName;
	}

	inline T GetDefaultValue () const
	{
		return DefaultValue;
	}
};
SD_END