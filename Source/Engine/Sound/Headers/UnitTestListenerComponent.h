/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTestListenerComponent.h
  A listener comonent that simply increments a number every time
  it receives an event.

  This is used in a unit test to verify broadcasted sound events.
=====================================================================
*/

#pragma once

#include "SoundListenerComponent.h"

#ifdef DEBUG_MODE
SD_BEGIN
class SOUND_API UnitTestListenerComponent : public SoundListenerComponent
{
	DECLARE_CLASS(UnitTestListenerComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Int NumEventsReceived;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void HearSoundEvent (const SoundEvent& newSoundEvent) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Resets the num events received back to zero.
	 */
	virtual void ResetTest ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetNumEventsReceived () const
	{
		return NumEventsReceived;
	}
};
SD_END
#endif