/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioAssetPool.h
  AudioAssetPool is responsible for maintaining all imported AudioAssets.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioAsset;

class SOUND_API AudioAssetPool : public ResourcePool
{
	DECLARE_CLASS(AudioAssetPool)
	DECLARE_RESOURCE_POOL(AudioAsset)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of tags each resource could reference to make it easier to find when searching.
	Only manipulate this vector through ResourceTag::AddTag and ResourceTag::RemoveTag since audio assets reference this vector by index.
	Directly manipulating this vector will cause the audio asset indices to be displaced. */
	std::vector<ResourceTag> Tags;

protected:
	/* List of all audio assets imported in this pool. The key to this map is the hash of the texture name. */
	std::unordered_map<size_t, AudioAsset*> ImportedAudio;

	/* If true, then this AudioAsset resides on the sound thread. The sound thread can be on the main thread for single threaded applications.
	Generally the AudioAssetPool that's not on the sound thread would have stubbed imported audio assets (blank instances without data). */
	bool bIsOnSoundThread;

	/* If true, then this pool will attempt to import audio assets in the external thread. Set this to false when importing to prevent reimporting
	audio assets back to the original pool. */
	bool bNotifyExternalPool;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ReleaseResources () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates an Audio Asset object, imports the specified file for its data, and registers the asset to this pool.
	 */
	virtual AudioAsset* CreateAndImportAudio (const FileAttributes& file, const HashedString& audioAssetName);

	/**
	 * Creates a blank Audio Asset object that doesn't have any data, and reserves that instance to the ImportedAudio map.
	 * These blank instances are typically useful for referencing assets in a different thread.
	 */
	virtual AudioAsset* CreateBlankAudio (const HashedString& audioAssetName);

	/**
	 * Populates the given vector with all imported audio assets with no order in particular.
	 */
	virtual void GetAllAudio (std::vector<AudioAsset*>& outAllAudio);

	/**
	 * Retrieves the audio asset instance associated with the given string.
	 */
	virtual const AudioAsset* GetAudio (const HashedString& audioName, bool bLogOnMissing = true) const;
	virtual const AudioAsset* GetAudio (size_t audioHash) const;
	virtual AudioAsset* EditAudio (const HashedString& audioName, bool bLogOnMissing = true);
	virtual AudioAsset* EditAudio (size_t audioHash);

	/**
	 * Destroys and removes the audio asset instance associated with the given string.
	 */
	virtual void DestroyAudio (const HashedString& audioName);
	virtual void DestroyAudio (size_t audioHash);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetIsOnSoundThread (bool newIsOnSoundThread);
	virtual void SetNotifyExternalPool (bool newNotifyExternalPool);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsOnSoundThread () const
	{
		return bIsOnSoundThread;
	}
};
SD_END