/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioComponent.h
  An EntityComponent that allows the owner to broadcast sound events.

  An Entity must own a SoundListenerComponent to be able to receive these
  sound events. Both of these components are expected to attach to a SceneTransform.

  In addition to broadcasting sound events, the AudioComponent can obtain a reference
  to the SoundStream while it plays, allowing the component to manipulate it over time.

  Lastly, external systems can quickly access the sound event instigator through these components.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioAsset;

class SOUND_API AudioComponent : public EntityComponent
{
	DECLARE_CLASS(AudioComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPendingSound
	{
		size_t AssetHash;
		Bool IsDynamicAsset;
		Float Volume;
		Bool bDetachedSound;

		/* Only applicable if the asset is a dynamic asset. This buffer contains a list of parameters to apply on the other thread. */
		DataBuffer AudioParams;

		SPendingSound (size_t inAssetHash, Bool inIsDynamicAsset, Float inVolume, Bool inDetachedSound);
		SPendingSound (const SPendingSound& cpyObj);
		void operator= (const SPendingSound& cpyObj);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
#ifdef WITH_MULTI_THREAD
	static const Int FLAG_COPY_ID;
	static const Int FLAG_COPY_PROPS;
	static const Int FLAG_COPY_POSITION;
	static const Int FLAG_TELEPORT_POS;
	static const Int FLAG_PLAY_SOUND;
	static const Int FLAG_DETACH_SOUND;
	static const Int FLAG_INTERRUPT_SOUND;
	static const Int FLAG_INTERRUPT_ASSET;
	static const Int FLAG_INTERRUPT_CHANNEL;
#endif

	/* ID that identifies this component. This ID must match the corresponding AudioComponent in the external thread if there is one. */
	Int AudioCompId;

	/* Maximum distance a listener component can be and still receive the maximum volume from sounds produced from this component.
	The sound volume gradually fades to 0 whenever the relative distance approaches OuterRadius and beyond. */
	Float InnerRadius;

	/* Distance that determines how far sounds produced from this component will travel. Any listener components beyond this radius will not receive this event. */
	Float OuterRadius;

	/* The multiplier for the sound volume. Components within the inner radius will receive sounds from this component at this volume.
	Otherwise, the sound volume would range between this value and zero depending on their relative distance in the radius spectrum. */
	Float VolumeMultiplier;

	/* The multiplier for the pitch/speed of the sound it'll play. This is compounded with the audio channel's time dilation and the doppler effect. */
	Float PitchMultiplier;

	/* If true, then any active sounds played from this component can apply a Doppler effect. This is evaluated every frame for each active sound. */
	bool bEnableDoppler;

	/* Prevents changing the pitch from the doppler affect until all dynamic sounds are processed once. If there are 5 active sounds, each of them will decrement this
	value by one. Doppler affect is enabled when this reaches zero.
	This is typically set when the owner is teleporting to avoid sudden pitch shifts for a vast change in positions. */
	Int TeleportDopplerCount;

	/* Reference to the SceneTransform of the owner this component is attached to. */
	SceneTransform* OwnerTransform;

	/* Returns true if this audio component is currently playing a detached sound. */
	bool bPlayingDetachedSound;

#ifdef WITH_MULTI_THREAD
	ThreadedComponent* ThreadComp;

	/* Quick flag used to quickly check if this component resides in the sound thread or not. */
	bool bInSoundThread;

	/* Maximum time to wait before the thread component will transfer data across threads (in seconds).
	This does not apply to broadcasting or stopping sound events since that forces a data request ASAP.
	This should be set to a lower interval for fast moving/important sounds. This should be a higher interval for slow, stationary, or nonimportant objects. */
	Float MaxWaitTime;

	/* List of all flags used to indicate which data should be transfered over across threads. */
	Int PendingFlags;

	std::vector<SPendingSound> PendingSounds;

	/* List of audio assets with matching hash to interrupt on the sound thread. */
	std::vector<size_t> PendingInterruptions;

	/* List of channel hashes to interrupt on the sound thread. */
	std::vector<size_t> PendingInterruptChannels;
#endif


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a sound event using the given asset as the meta data for the sound.
	 * The volumeMultiplier parameter is compounded with this audio component's VolumeMultiplier.
	 */
	virtual void PlaySound (const AudioAsset* asset, Float volumeMultiplier);

	/**
	 * Temporarily disables the doppler pitch adjustments until a frame later.
	 */
	virtual void TeleportAudioComp ();

	/**
	 * Creates a sound event using the given asset as the meta data for the sound.
	 * The sound is not attached to this component, meaning it'll only play at the AudioComponent's position from where the sound event started.
	 */
	virtual void PlayDetachedSound (const AudioAsset* asset, Float volumeMultiplier);

	/**
	 * Any active sound produced from this component severs its association with this AudioComponent.
	 * The sound will continue playing, but AudioComponent's location is copied to the sound source.
	 * This will allow the SoundEvent to continue playing at this component's last location until the sound naturally terminates.
	 */
	virtual void DetachFromActiveSounds ();

	/**
	 * Immediately stops all active sounds produced from this component.
	 */
	virtual void InterruptSounds ();

	/**
	 * Immediately stops all active sounds that are playing the given asset produced from this component.
	 */
	virtual void InterruptSounds (const AudioAsset* targetAsset);

	/**
	 * Immediately stops all active sounds produced from this component that are playing a particular audio channel.
	 */
	virtual void InterruptSoundsByChannel (const DString& channelName);
	virtual void InterruptSoundsByChannel (size_t channelHash);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetInnerRadius (Float newInnerRadius);
	virtual void SetOuterRadius (Float newOuterRadius);
	virtual void SetVolumeMultiplier (Float newVolumeMultiplier);
	virtual void SetPitchMultiplier (Float newPitchMultiplier);
	virtual void SetEnableDoppler (bool newEnableDoppler);

#ifdef WITH_MULTI_THREAD
	virtual void SetMaxWaitTime (Float newMaxWaitTime);
#endif


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetAudioCompId () const
	{
		return AudioCompId;
	}

	inline Float GetInnerRadius () const
	{
		return InnerRadius;
	}

	inline Float GetOuterRadius () const
	{
		return OuterRadius;
	}

	inline Float GetVolumeMultiplier () const
	{
		return VolumeMultiplier;
	}

	inline Float GetPitchMultiplier () const
	{
		return PitchMultiplier;
	}

	inline bool IsDopplerEnabled () const
	{
		return (bEnableDoppler && TeleportDopplerCount <= 0);
	}

	inline SceneTransform* GetOwnerTransform () const
	{
		return OwnerTransform;
	}

	inline bool IsPlayingDetachedSound () const
	{
		return bPlayingDetachedSound;
	}

#ifdef WITH_MULTI_THREAD
	inline Float GetMaxWaitTime () const
	{
		return MaxWaitTime;
	}
#endif


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Generates and assigns an ID unique to this component and the component in the other thread.
	 */
	virtual void GenerateAudioCompId ();

	/**
	 * Decrements the doppler teleport counter. If it's down to zero, all dynamic sounds can then update their pitches based on the doppler affect.
	 */
	virtual void DecrementDopplerCounter ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
#ifdef WITH_MULTI_THREAD
	virtual void HandleAbsTransformChanged ();
	virtual void HandleCopyData (DataBuffer& outExternalData);
	virtual void HandleReceiveData (const DataBuffer& incomingData);
#endif

friend class DopplerEffect;
};
SD_END