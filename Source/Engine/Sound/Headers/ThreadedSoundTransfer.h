/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ThreadedSoundTransfer.h
  An Entity responsible for transferring data between the two AudioEngineComponent
  instances across threads.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
#ifdef WITH_MULTI_THREAD
class SOUND_API ThreadedSoundTransfer : public Entity
{
	DECLARE_CLASS(ThreadedSoundTransfer)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPendingLog
	{
		LogCategory::ELogLevel LogLevel;
		DString Msg;

		SPendingLog (LogCategory::ELogLevel inLogLevel, const DString& inMsg);
	};

	struct SPendingAudioImport
	{
		FileAttributes File;
		DString AudioName;

		SPendingAudioImport (const FileAttributes& inFile, const DString& inAudioName);
	};

	struct SPendingChannelVolume
	{
		size_t ChannelHash;
		Float NewVolume;
		
		SPendingChannelVolume (size_t inChannelHash, Float inNewVolume);
	};

	struct SPendingChannelDilation
	{
		size_t ChannelHash;
		Float NewDilation;

		SPendingChannelDilation (size_t inChannelHash, Float inNewDilation);
	};

	struct SPendingStaticSound
	{
		size_t AssetHash;

		/* Becomes true if this asset is a DynamicAudioInstance. */
		Bool DynamicAsset;

		Float Volume;
		Float Pitch;

		/* Only applicable for DynamicAudioAssets, this data buffer contains information about the asset parameters and their values. */
		DataBuffer Parameters;

		SPendingStaticSound (size_t inAssetHash, Bool inDynamicAsset, Float inVolume, Float inPitch);
		SPendingStaticSound (const SPendingStaticSound& cpyObj);
		void operator= (const SPendingStaticSound& cpyObj);
	};

	struct SPendingSoundBegin
	{
		size_t AssetHash;
		Int AudioCompId;
		Int ListenerCompId;

		SPendingSoundBegin (size_t inAssetHash, Int inAudioCompId, Int inListenerCompId);
	};

	struct SPendingSoundEnded
	{
		size_t AssetHash;
		Int AudioCompId;
		Int ListenerCompId;

		SPendingSoundEnded (size_t inAssetHash, Int inAudioCompId, Int inListenerCompId);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various flags used to determine what kind of data resides in the data buffers. */
	static const Int FLAG_TERMINATE_ENGINE;
	static const Int FLAG_LOG_DATA;
	static const Int FLAG_AUDIO_IMPORT;
	static const Int FLAG_AUDIO_PROPS;
	static const Int FLAG_AUDIO_DESTROY;
	//Flags that adjust sound settings
	static const Int FLAG_SPEED_OF_SOUND;
	static const Int FLAG_MASTER_VOLUME;
	static const Int FLAG_CHANNEL_VOLUME;
	static const Int FLAG_CHANNEL_DILATION;
	static const Int FLAG_DUCK_VOLUME;
	static const Int FLAG_DUCK_LERP;
	//Flags that play and stop sounds
	static const Int FLAG_PLAY_STATIC;
	static const Int FLAG_STOP_STATIC;
	static const Int FLAG_STOP_DYNAMIC;
	static const Int FLAG_STOP_CHANNEL;
	static const Int FLAG_STOP_ALL;
	static const Int FLAG_SOUND_BEGIN;
	static const Int FLAG_SOUND_ENDED;

	/* If true, then this Entity is attempting to terminate the Engine instance in the external thread. */
	bool bDestroyingExternal;

	/* Determines the maximum wait time for nonessential updates across threads. */
	Float MaxWaitTime;

	ThreadedComponent* ThreadComp;

	/* List of logs waiting to be logged on the main thread. */
	std::vector<SPendingLog> PendingLogs;

	/* List of audio assets to import. This can be on the sound thread (if imported from main thread), or main thread (if imported from the sound thread). */
	std::vector<SPendingAudioImport> PendingImports;

	/* List of audio asset hashes to copy properties from. */
	std::vector<size_t> PendingAudioProps;

	/* List of audio assets to destroy/remove from the AudioAssetPool. The contents of the vector element is the audio asset hash. */
	std::vector<size_t> PendingAudioDestroy;

	Float PendingSpeedOfSound;
	Float PendingMasterVolume;
	std::vector<SPendingChannelVolume> PendingChannelVolumes;
	std::vector<SPendingChannelDilation> PendingChannelDilations;
	Float PendingDuckVolume;
	Float PendingDuckLerp;
	std::vector<SPendingStaticSound> PendingStaticSounds;
	std::vector<size_t> PendingStopStatic;
	std::vector<size_t> PendingStopDynamic;
	std::vector<size_t> PendingStopChannel;
	bool bPendingStopAll;
	std::vector<SPendingSoundBegin> PendingSoundBeginList;
	std::vector<SPendingSoundEnded> PendingSoundEndedList;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Invoke this method to terminate the Engine instance residing in the other thread.
	 */
	virtual void TerminateOtherEngine ();

	/**
	 * Appends the specified log message to the queue to be pushed to the main thread.
	 */
	virtual void TransferLog (LogCategory::ELogLevel logLevel, const DString& logMsg);

	/**
	 * Appends the audio asset data to the PendingImports queue to be pushed to the other thread.
	 */
	virtual void ImportAudio (const FileAttributes& audioFile, const DString& audioName);

	/**
	 * Appends the audio asset hash to copy its properties to the asset on the other thread.
	 */
	virtual void CopyAudioProps (size_t audioHash);

	/**
	 * Appends the audio asset hash to the PendingAudioDestroy to destroy the asset on the other thread.
	 */
	virtual void DestroyAudio (size_t audioHash);

	virtual void SetSpeedOfSound (Float newSpeedOfSound);
	virtual void SetMasterVolume (Float newMasterVolume);
	virtual void SetChannelVolume (size_t channelHash, Float newVolume);
	virtual void SetChannelDilation (size_t channelHash, Float newDilation);
	virtual void SetDuckVolume (Float newDuckVolume);
	virtual void SetDuckLerp (Float newDuckLerp);
	virtual void PlayStaticSound (const AudioAsset* asset, Float volume, Float pitch);
	virtual void StopStaticSound (size_t assetHash);
	virtual void StopDynamicSound (size_t assetHash);
	virtual void StopChannel (size_t channelHash);
	virtual void StopAllSounds ();
	virtual void BroadcastSoundBegin (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp);
	virtual void BroadcastSoundEnded (const AudioAsset* asset, AudioComponent* audioComp, PlayerListenerComponent* listenComp);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Prepares the header information that determines what kind of information resides in the data buffer.
	 */
	virtual Int WriteBufferHeader (DataBuffer& outExternalData);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCopyData (DataBuffer& outExternalData);
	virtual void HandleReceiveData (const DataBuffer& incomingData);
};
#endif
SD_END