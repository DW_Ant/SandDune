/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundClasses.h
  Contains all header includes for the Sound module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the SoundClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_SOUND
#include "AudioAsset.h"
#include "AudioAssetPool.h"
#include "AudioBufferArray.h"
#include "AudioBufferModifier.h"
#include "AudioBufferSwap.h"
#include "AudioComponent.h"
#include "AudioEngineComponent.h"
#include "DopplerEffect.h"
#include "DynamicAudioAsset.h"
#include "DynamicAudioInstance.h"
#include "DynamicSound.h"
#include "GradualTranslator.h"
#include "PlayerListenerComponent.h"
#include "Sound.h"
#include "SoundEvent.h"
#include "SoundListenerComponent.h"
#include "SoundMode.h"
#include "SoundPlaybackInterface.h"
#include "SoundProperty.h"
#include "SoundUnitTest.h"
#include "ThreadedDynamicAudioInstance.h"
#include "ThreadedSoundTransfer.h"
#include "UnitTestListenerComponent.h"

#endif
