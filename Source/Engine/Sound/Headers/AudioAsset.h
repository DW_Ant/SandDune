/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioAsset.h
  A resource that contains raw audio data and meta data information about it
  such as duration and tags.

  This class is mostly a wrapper to SFML's SoundBuffer, but this class also
  interfaces with other Sand Dune classes such as the associated resource pool.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioAssetPool;

class SOUND_API AudioAsset : public Object
{
	DECLARE_CLASS(AudioAsset)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EConflictHandling
	{
		CH_None = 0, //Playing this asset will not interrupt any sounds.
		CH_MustMatchSrc = 1, //If an audio component plays this asset, it'll stop all other active sounds from this same component before playing this asset.
		CH_MustMatchChannel = 2, //Before playing this asset, all other active sounds (static and dynamic) with matching channels are interrupted.
		CH_MustMatchSrcAndChannel = CH_MustMatchSrc | CH_MustMatchChannel //If an audio component plays this asset, all sounds from this component that also are in the same channel are interrupted. Common example: Prevent a NPC from speaking two voice lines at the same time.
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The raw byte data of the sound. This is set to null to avoid stalling the application from static initialization (hangs when trying to create an audio device before main is called). */
	sf::SoundBuffer* Buffer;

	DString AudioName;

	/* The unique hash number from the AudioName. This is used to find the audio asset in an AudioAssetPool when referenced from across threads. */
	size_t AudioHash;

	/* Reference to the audio pool this asset is registered to. */
	AudioAssetPool* OwningAssetPool;

	/* List of tags this audio is categorized. */
	std::vector<HashedString> Tags;

	/* Name of the audio channel this asset belongs to. If empty, then this asset isn't associated with a channel.
	Assets within a channel can collectively adjust their volume and pitch.
	A typical case for adjusting a channel volume is when the user is adjusting the audio balance in the settings menu.
	A typical case for adjusting a channel pitch is to determine which sounds should play/pause. For example, the pause menu should pause game sounds, but Gui sounds should play normally. */
	DString ChannelName;

	/* Hash of the audio channel name for fast comparisons. */
	size_t ChannelHash;

	/* If true, then any sound events playing this asset will loop. */
	bool bLooping;

	/* If true, then the sound will start playing at a random point in the buffer. Only applicable for looping assets. */
	bool bRandomStart;

	/* Subtitles associated with this asset. This can either be the actual spoken text or a localization key. It's up to the GUI interpreting this value. */
	DString Subtitles;

	/* If true, then dynamic sounds playing this asset may apply the Doppler Effect. */
	bool bEnableDoppler;

	/* Any sounds that are playing with lesser priority than this asset will play at a reduced volume. For example: If an important speaker is talking,
	other sound assets with	lesser priority will reduce their volume.
	Negative priority values renders the asset immune to ducking. For example prevent music from reducing volume even if an important sound is playing. */
	Int DuckPriority;

	/* Behavior that determines if playing this asset will interrupt other active sounds. */
	EConflictHandling ConflictHandling;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the number of seconds it takes to play this audio asset without modifications.
	 */
	virtual Float GetDuration () const;

	/**
	 * Returns the sample rate of sound.
	 */
	virtual Int GetSampleRate () const;

	virtual size_t GetNumSamples () const;

	/**
	 * Returns the bit depth of this audio (how many possible amplitude values per sample).
	 */
	virtual size_t GetBitDepth () const; //SFML uses 16-bit ints

	/**
	 * Returns the number of channels of this sound. 1 = mono, 2 = stereo.
	 */
	virtual unsigned int GetNumChannels () const;

	/**
	 * Retrieves read only access to the buffer's raw sample data.
	 */
	virtual const sf::Int16* GetRawData () const;

#ifdef WITH_MULTI_THREAD
	/**
	 * Copies most properties from the given asset to the associated asset in the other thread.
	 * This does not include the audio buffer data.
	 */
	virtual void CopyPropsToOtherThread ();

	/**
	 * Writes this asset's properties to the given data buffer.
	 */
	virtual void CopyPropsTo (DataBuffer& outData) const;

	/**
	 * Reads data for this asset from the given data buffer. This is to work in conjunction with CopyPropsTo.
	 */
	virtual void CopyPropsFrom (const DataBuffer& incomingData);
#endif


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAudioName (const DString& newAudioName);
	virtual void SetAudioHash (size_t newAudioHash);
	virtual void SetChannelName (const DString& newChannelName);
	virtual void SetOwningAssetPool (AudioAssetPool* newOwningAssetPool);

	virtual void SetLooping (bool newLooping)
	{
		bLooping = newLooping;
	}

	virtual void SetRandomStart (bool newRandomStart)
	{
		bRandomStart = newRandomStart;
	}

	virtual void SetSubtitles (const DString& newSubtitles)
	{
		Subtitles = newSubtitles;
	}

	virtual void SetEnableDoppler (bool newEnableDoppler)
	{
		bEnableDoppler = newEnableDoppler;
	}

	virtual void SetDuckPriority (Int newDuckPriority)
	{
		DuckPriority = newDuckPriority;
	}

	virtual void SetConflictHandling (EConflictHandling newConflictHandling)
	{
		ConflictHandling = newConflictHandling;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const sf::SoundBuffer* GetBuffer () const
	{
		return Buffer;
	}

	inline sf::SoundBuffer* EditBuffer ()
	{
		return Buffer;
	}

	virtual inline DString GetAudioName () const
	{
		return AudioName;
	}

	virtual inline const DString& ReadAudioName () const
	{
		return AudioName;
	}

	virtual inline size_t GetAudioHash () const
	{
		return AudioHash;
	}

	inline AudioAssetPool* GetOwningAssetPool () const
	{
		return OwningAssetPool;
	}

	inline const std::vector<HashedString>& ReadTags () const
	{
		return Tags;
	}

	inline std::vector<HashedString>& EditTags ()
	{
		return Tags;
	}

	virtual inline DString GetChannelName () const
	{
		return ChannelName;
	}

	virtual inline const DString& ReadChannelName () const
	{
		return ChannelName;
	}

	virtual inline size_t GetChannelHash () const
	{
		return ChannelHash;
	}

	virtual inline bool IsLooping () const
	{
		return bLooping;
	}

	virtual inline bool IsRandomStart () const
	{
		return bRandomStart;
	}

	virtual inline DString GetSubtitles () const
	{
		return Subtitles;
	}

	virtual inline const DString& ReadSubtitles () const
	{
		return Subtitles;
	}

	virtual inline bool IsDopplerEnabled () const
	{
		return bEnableDoppler;
	}

	virtual inline Int GetDuckPriority () const
	{
		return DuckPriority;
	}

	virtual inline EConflictHandling GetConflictHandling () const
	{
		return ConflictHandling;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Unregisters all tags from the owning asset pool.
	 */
	virtual void UnregisterTags ();
};

DEFINE_ENUM_FUNCTIONS(AudioAsset::EConflictHandling)
SD_END