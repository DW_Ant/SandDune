/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicAudioAsset.h
  An audio asset that is able to create DynamicAudioInstances where each
  instance could change its audio buffer based on conditions.
=====================================================================
*/

#pragma once

#include "AudioAsset.h"

SD_BEGIN
class AudioBufferModifier;
class DynamicAudioInstance;

class SOUND_API DynamicAudioAsset : public AudioAsset
{
	DECLARE_CLASS(DynamicAudioAsset)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The start of the buffer modifier linked list that are able to determine how this asset is played. This only points the beginning of the linked list.
	BufferModifiers are able to point to one or more different modifiers based on conditions (eg: branching nodes).
	NOTE: When DynamicAudioInstances are setting parameters, they are directly writing to these instances. This is to enable persistence across multiple playbacks
	(for example: the list one would play sequentially to the next item next time it's played). In addition, this prevents an unecessary copy for each time this
	asset is played. Make sure the modifiers apply their effect immediately after setting parameters before the next dynamic instance adjusts its parameters. */
	AudioBufferModifier* FirstModifier;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a DynamicAudioInstance from this asset. This instance is intended to be played for a sound a single time (gets destroyed automatically when the sound ends).
	 * If it's not intended to be played, then it's expected that that the caller should destroy this instance to prevent memory leak.
	 */
	virtual DynamicAudioInstance* CreateAudioInstance () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline AudioBufferModifier* GetFirstModifier () const
	{
		return FirstModifier;
	}
};
SD_END