/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundPlaybackInterface.h
  An interface that allows subclasses to modify and react to sounds being
  played on the speakers.

  Difference between SoundListenerComponent and SoundPlaybackInterface:

  SoundListenerComponent reacts to broadcasted sound events. These components
  include listeners even if the listener do not play sounds on the speakers.
  For example, an AI component can have a listener component to react to sound events.

  SoundPlaybackInterface reacts to all sounds played on the speakers. This happens
  after a listener component is playing a sound event. This interface can also react
  to static sounds. A common example of an interface is a GuiComponent listening to played
  sounds to identify if it needs to display subtitles. Another purpose of the playback
  interface is when something needs to modify the DynamicAudioAsset parameters.
  If the played sound is a DynamicAudioAsset, interfaces will receive a SetAudioParameters callback,
  allowing them to adjust any parameters before they're played on the speakers. An example of this
  is a TeamComponent that changes all friendly footsteps to a less threatening sound.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioComponent;
class DynamicAudioInstance;
class PlayerListenerComponent;

class SOUND_API SoundPlaybackInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Hook to allow the interface to set any instance parameters before the audio is played.
	 */
	virtual void SetAudioParameters (DynamicAudioInstance* audioInstance)
	{
		//Noop
	}

	/**
	 * Called whenever the given asset is playing. If it's a static sound event, the source and listener references will be nullptr.
	 */
	virtual void ProcessPlayedSound (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener)
	{
		//Noop
	}

	/**
	 * Called whenever an instance of the given asset stopped playing. If it's a static sound event, the source and listener references will be nullptr.
	 */
	virtual void ProcessStoppedSound (const AudioAsset* asset, AudioComponent* source, PlayerListenerComponent* listener)
	{
		//Noop
	}
};
SD_END