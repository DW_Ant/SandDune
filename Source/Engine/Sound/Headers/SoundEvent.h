/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundEvent.h
  An object that represents a sound that is traveling around.

  A sound event doesn't always correspond to playing sounds through the user's
  speakers. A SoundEvent exists whenever an AudioComponent broadcasts an event.
  It's up to the SoundListenerComponents to receive the event to react to
  it (eg: AI reacting to sound).
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioAsset;
class AudioComponent;

class SOUND_API SoundEvent
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The sound asset containing information about the sound, itself. */
	const AudioAsset* Asset;

	/* The component that broadcasted this sound. */
	AudioComponent* Source;

	/* Maximum distance a listener component can be and still receive the maximum volume from this sound event. The sound volume gradually fades to 0
	whenever the relative distance approaches OuterRadius and beyond. */
	Float InnerRadius;

	/* Distance that determines how far this sound will travel from the source. Any listener components beyond this radius will not receive this event. */
	Float OuterRadius;

	/* The multiplier for the sound volume. Components within the inner radius will receive this sound event at this volume. Otherwise, the sound volume would range
	between this value and zero depending on their relative distance in the radius spectrum. */
	Float VolumeMultiplier;

	/* The multiplier for the pitch/speed of the sound it'll play. This is compounded with the audio channel's time dilation and the doppler effect. */
	Float PitchMultiplier;

private:
	/* Becomes true if this SoundEvent has or had an AudioComponent source. This remains true even after the AudioComponent was severed from this event. */
	bool bHasSource;

	/* Used whenever there was a sound source, but then it was later severed from this sound event (eg: AudioComponent was destroyed before this sound finished). 
	When a sound event is severed from a source, this vector is used to record the source's location to maintain directional sounds.
	This vector is not initialized (undefined) until the source is severed. */
	Vector3 LastKnownLocation;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SoundEvent (); //Constructs an invalid sound event
	SoundEvent (const AudioAsset* inAsset, AudioComponent* inSource);
	SoundEvent (const AudioAsset* inAsset, AudioComponent* inSource, Float inInnerRadius, Float inOuterRadius, Float inVolumeMultiplier, Float inPitchMultiplier);
	SoundEvent (const SoundEvent& other);


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Notifies every nearby local SoundListenerComponent near the source about this sound event to allow them to react to this event.
	 */
	virtual void BroadcastSoundEvent ();

	/**
	 * Caches the Source's location before clearing the reference to the AudioComponent that broadcasted this event.
	 */
	virtual void SeverSource ();

	/**
	 * Clears any references this event may have to any external objects.
	 */
	virtual void ClearReferences ();

	/**
	 * Returns the location of the source or the last known location of the source if the AudioComponent is severed from this event.
	 * This method returns an undefined vector if HasSource is false.
	 */
	virtual Vector3 GetSourceLocation () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAsset (const AudioAsset* newAsset);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const AudioAsset* GetAsset () const
	{
		return Asset;
	}

	inline AudioComponent* GetSource () const
	{
		return Source;
	}

	inline Float GetInnerRadius () const
	{
		return InnerRadius;
	}

	inline Float GetOuterRadius () const
	{
		return OuterRadius;
	}

	inline Float GetVolumeMultiplier () const
	{
		return VolumeMultiplier;
	}

	inline Float GetPitchMultiplier () const
	{
		return PitchMultiplier;
	}

	inline bool HasSource () const
	{
		return bHasSource;
	}
};
SD_END