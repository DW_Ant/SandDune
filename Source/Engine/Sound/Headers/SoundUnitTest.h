/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundUnitTest.h
  A unit test that tests the Sound module.
=====================================================================
*/

#pragma once

#include "Sound.h"

#ifdef DEBUG_MODE
SD_BEGIN
class SOUND_API SoundUnitTest : public UnitTester
{
	DECLARE_CLASS(SoundUnitTest)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestSoundEvents (EUnitTestFlags testFlags) const;
};
SD_END
#endif