/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicSound.h
  An Entity that represents an active sound being played on the speakers.

  Contrasts to AudioEngineComponent's static sounds, the DynamicSound can change how
  the sound is played as it's played. It can perform the following:

  * Directional sounds based on the source's location relative to the listener's location.
  * Attenuation - Soften the volume the further away the source is from the listener.
  * Sound Obstruction - Muffle any sounds obscured between source and listener.
  * Pause looping sounds when listener is too far away from source.
  * Doppler Effect - Change pitch based on relative movement between source and listener.
=====================================================================
*/

#pragma once

#include "DopplerEffect.h"
#include "Sound.h"
#include "SoundEvent.h"

SD_BEGIN
class PlayerListenerComponent;

class SOUND_API DynamicSound : public Entity
{
	DECLARE_CLASS(DynamicSound)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate to broadcast whenever this sound player finished or interrupted its current sound. */
	SDFunction<void, DynamicSound*> OnFinished;

protected:
	/* The user configured master volume that globally multiplies the volume by this value. */
	static Float MasterVolume;

	/* Vector used to multiply all directional vectors when computing for positional sounds. This is used for configurable settings. For example, if the user swapped their left and right speakers,
	this variable can be configured to (-1, 1, 1) to horizontally reverse the directions of all sounds. */
	static Vector3 DirectionMultiplier;

	/* When calculating the directional vector, this is the maxium squared distance the sound can be from the source and listener. If it's less than this threshold, the sound will not have any directional data.
	Otherwise it'll use a normalized vector towards that direction. */
	static Float DirectionSoundThreshold;

	/* The SFML object that handles playing the sound. Must be a pointer to prevent stalling on static init (cannot be created before main). */
	sf::Sound* SoundPlayer;

	/* The event responsible for instigating this sound. */
	SoundEvent Event;

	/* The object responsible for calculating the Doppler Effect. */
	DopplerEffect Doppler;

	/* Current user configuration of the asset's channel's volume. */
	Float ChannelVolume;

	/* Current volume multipliers applied from active SoundModes. */
	Float SoundModeVolume;

	/* Current speed of the asset's channel's time dilation. */
	Float ChannelDilation;

	/* Volume multiplier to apply due to ducking. */
	Float DuckVolume;

	/* The ListenerComponent that received this sound event. If nullptr, then it assumes the listener location is at the LastKnownListenerLocation. */
	DPointer<PlayerListenerComponent> Listener;

	TickComponent* Tick;

	/* Determines the tick interval whenever this sound is dormant (when the listener is too far away to hear this sound). */
	Float DormantTickInterval;

	/* Determines the frequence the sound relative position/direction is calculated (in seconds). */
	Float CalcDirectionInterval;

	/* For multi threaded sound applications, DynamicSounds are not expected to compute the pitch on the sound thread since position updates would be inconsistent
	(causing fluctuations in pitch shifts). Therefore, DynamicSounds are expected to receive pitch updates from the main thread.
	This is disabled if this value is negative. */
	Float DopplerPitchOverride;

private:
	/* Location of the ListenerComponent just before it was severed from this sound event. */
	Vector3 LastKnownListenerLocation;

	/* The time stamp when the direction was last calculated (in seconds). */
	Float CalcDirectionTime;

	/* Becomes true if this sound player is in a middle of broadcasting its OnFinished delegate. This prevents the dynamic sound from playing new sounds since it's about to clear its
	references from the previous sound event. This is a failsafe check to prevent OnFinished handlers from playing new sounds. */
	bool bIsExecutingOnFinished;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes the SoundPlayer to play the given SoundEvent.
	 * A DynamicSound can be activated only if it's currently not active (can't activate it while it's currently playing or paused a sound event).
	 */
	virtual void ActivateSound (const SoundEvent& inEvent, PlayerListenerComponent* inListener);

	/**
	 * Returns true if this Entity is contains a SoundEvent that it's playing through or is temporarily paused.
	 */
	virtual bool IsActive () const;

	/**
	 * Detaches the source from this Entity. When detaching, the source's location is saved to the event before clearing its references.
	 * This allows the sound to continue playing and assume the source will linger at that last known location.
	 */
	virtual void SeverSource ();

	/**
	 * Detaches the SoundListener from this Entity. When detaching, the current listener's location is saved to the LastKnownListenerLocation before clearing its reference.
	 * This allows the sound to continue playing and assume the listener will linger at that last known location.
	 */
	virtual void SeverListener ();

	/**
	 * Interrupts any sound playback and stops any processing of this event. After calling this function, the DynamicSound can only play again through another ActiveSound event.
	 */
	virtual void Stop ();

	/**
	 * Returns the Listener's current location if any. If the listener is severed from this sound, then it'll return its last known location.
	 */
	Vector3 GetListenerLocation () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	static void SetMasterVolume (Float newMasterVolume);
	static void SetDirectionMultiplier (const Vector3& newDirectionMultiplier);
	virtual void SetChannelVolume (Float newChannelVolume);
	virtual void SetSoundModeVolume (Float newSoundModeVolume);
	virtual void SetChannelDilation (Float newChannelDilation);
	virtual void SetDuckVolume (Float newDuckVolume);
	virtual void SetListener (PlayerListenerComponent* newListener);
	virtual void SetCalcDirectionInterval (Float newCalcDirectionInterval);
	virtual void SetDopplerPitchOverride (Float newDopplerPitchOverride);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static inline Float GetMasterVolume ()
	{
		return MasterVolume;
	}

	static inline Vector3 GetDirectionMultiplier ()
	{
		return DirectionMultiplier;
	}

	static inline const Vector3& ReadDirectionMultiplier ()
	{
		return DirectionMultiplier;
	}

	inline const SoundEvent& ReadEvent () const
	{
		return Event;
	}

	inline SoundEvent& EditEvent ()
	{
		return Event;
	}

	inline Float GetChannelVolume () const
	{
		return ChannelVolume;
	}

	inline Float GetSoundModeVolume () const
	{
		return SoundModeVolume;
	}

	inline Float GetChannelDilation () const
	{
		return ChannelDilation;
	}

	inline Float GetDuckVolume () const
	{
		return DuckVolume;
	}

	inline PlayerListenerComponent* GetListener () const
	{
		return Listener.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns the volume multiplier based on the relative distance to apply attenuation.
	 * This is executed every frame when active so it should be a fast algorithm.
	 * This should return a range between 0 and 1 where 0 would mute it, and 1 is at full volume.
	 */
	virtual Float CalcAttenuation (const Vector3& srcPos, const Vector3& listenerPos) const;

	/**
	 * Calculates the relative location the sound should emit. This is used for directional sound purposes.
	 * This is only called every frame when this is active, listener within range, playing a mono asset, and if the Listener is a valid pointer.
	 * Returns true if the vector is calculated and it should update the sound's position with this vector.
	 */
	virtual bool CalcDirection (const Vector3& srcPos, Vector3& outSoundPos) const;

	/**
	 * Updates the pitch of the sound player based on the time dilation and the doppler effect.
	 * The Doppler effect pitch multiplier does not change if the delta time is zero.
	 */
	virtual void UpdatePitch (Float deltaSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END