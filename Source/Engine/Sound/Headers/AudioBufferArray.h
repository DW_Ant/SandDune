/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferArray.h
  A buffer modifier that can branch to multiple other buffer modifiers either
  randomly or sequentially.
=====================================================================
*/

#pragma once

#include "AudioBufferModifier.h"
#include "SoundProperty.h"

SD_BEGIN
class SOUND_API AudioBufferArray : public AudioBufferModifier
{
	DECLARE_CLASS(AudioBufferArray)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then a random index is selected every time this sound is played. This is only available if the Random module is compiled. */
	bool bRandomIndex;

	/* Indicates the previous index that was selected last time this sound was played. If it's not random and the SelectedIdx is not used, this modifier
	simply increments this index by one every time this asset is played, causing the asset to iterate through this array sequentially. */
	Int LastPlayedIdx;

	/* If positive, then this will overwrite the selected array index to this value. The BufferArray instance will take ownership over this sound property reference. */
	SoundProperty<Int>* SelectedIdx;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual AudioBufferModifier* DoEffect (DynamicAudioInstance* asset) override;
	virtual void ResetAudioParameters () override;
	virtual bool HasParamName (const HashedString& paramName) const override;
	virtual bool SetSoundParameter (const HashedString& paramName, Int paramValue) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetRandomIndex (bool bNewRandomIndex);
	virtual void SetSelectedIdx (SoundProperty<Int>* newSelectedIdx);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsRandomIndex () const
	{
		return bRandomIndex;
	}
};
SD_END