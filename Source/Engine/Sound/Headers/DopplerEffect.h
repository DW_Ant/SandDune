/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DopplerEffect.h
  A simple object that specializes in computing the pitch multiplier
  for the doppler effect between two SceneTransforms.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class AudioComponent;
class PlayerListenerComponent;

class SOUND_API DopplerEffect
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	//Although these static public variables are not thread safe, only one of the threads would be calculating the DopplerEffect.
	/* Copy of the speed of sound from the AudioEngineComponent. */
	static Float SpeedOfSound;

	/* The minimum and maximum acceptable pitch multiplier this effect may calculate. */
	static Range<Float> PitchRange;

protected:
	AudioComponent* Source;
	PlayerListenerComponent* Listener;

	/* Last remembered doppler pitch multiplier. Used whenever either source or listener is severed from this sound. */
	Float LastDopplerMultiplier;

	/* Float used to calculate the doppler affect. This float is the most recent calculated distance between source and listener. */
	Float DopplerDeltaPos;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DopplerEffect ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the DopplerPitchMultiplier based on the change of relative positions between source and listener.
	 * If deltaSec is zero, then the effect doesn't change the pitch multiplier, but it will still decrement the broadcaster/source doppler counters
	 * and update the last DopplerDeltaPos.
	 */
	virtual Float CalcEffect (Float deltaSec);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSource (AudioComponent* newSource);
	virtual void SetListener (PlayerListenerComponent* newListener);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline AudioComponent* GetSource () const
	{
		return Source;
	}

	inline PlayerListenerComponent* GetListener () const
	{
		return Listener;
	}

	inline Float GetLastDopplerMultiplier () const
	{
		return LastDopplerMultiplier;
	}
};
SD_END