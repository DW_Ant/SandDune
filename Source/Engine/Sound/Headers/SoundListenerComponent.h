/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SoundListenerComponent.h
  An EntityComponent that can receive and react to broadcasted sound events.

  The base class doesn't do anything to sound events. It's up to the subclasses
  to implement reactionary behaviors (such as notifying the AI about sound events).

  To quickly iterate through each SoundListenerComponent, each component is registered
  to the audio engine component in the current thread.

  For spacial awareness purposes, these components are required to attach to a
  component tree where somewhere in the component hierarchy is a SceneTransform.
=====================================================================
*/

#pragma once

#include "Sound.h"

SD_BEGIN
class SoundEvent;

#ifdef DEBUG_MODE
class SoundUnitTest;
#endif

class SOUND_API SoundListenerComponent : public EntityComponent
{
	DECLARE_CLASS(SoundListenerComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The ID of this ListenerComponent. If this component is shared across threads, their ID's should pair up. */
	Int ListenerId;

	/* Reference to the SceneTransform found in the component chain this component is attached to.
	This doesn't always have to be the immediate owner, but rather the inner-most transform found in the attachment chain. */
	SceneTransform* OwnerTransform;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Called whenever this listener component is able to hear a new sound event.
	 */
	virtual void HearSoundEvent (const SoundEvent& newSoundEvent) = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetListenerId () const
	{
		return ListenerId;
	}

	inline SceneTransform* GetOwnerTransform () const
	{
		return OwnerTransform;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if this instance should generate a unique ID.
	 */
	virtual bool ShouldGenerateId () const;
	virtual void GenerateId ();

	/**
	 * Climbs through the component chain of the given Entity to find the first component or entity that is also a SceneTransform.
	 */
	virtual SceneTransform* FindSceneTransform (Entity* startFrom) const;

	/**
	 * Called whenever the OwnerTransform changed references.
	 */
	virtual void OwnerTransformChanged ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleOwnerChanged (EntityComponent* componentThatChanged, Entity* prevOwner);

#ifdef DEBUG_MODE
	//Used to verify HandleOwnerChanged
	friend class SoundUnitTest;
#endif
};
SD_END