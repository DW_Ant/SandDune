/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferSwap.h
  A simple buffer modifier that replaces the asset's audio buffer.
=====================================================================
*/

#pragma once

#include "AudioBufferModifier.h"

SD_BEGIN
class SOUND_API AudioBufferSwap : public AudioBufferModifier
{
	DECLARE_CLASS(AudioBufferSwap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The start of the raw audio buffer to place into the asset instance. */
	sf::Int16* RawAudioData;

	/* Number of samples found in the RawAudioData. */
	size_t SampleCount;

	/* Mono vs stereo. */
	Int NumChannels;

	/* Determines how many samples a second is played. */
	Int SampleRate;

	/* If it's not empty, it'll replace the audio instance's subtitles with this value. */
	DString SubtitleOverride;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual AudioBufferModifier* DoEffect (DynamicAudioInstance* asset) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Clears out the raw audio data, and returns the editable memory address of the newly allocated array of size SampleCount.
	 */
	virtual sf::Int16*& UpdateRawData (size_t newSampleCount, Int newNumChannels, Int newSampleRate);

	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSubtitleOverride (const DString& newSubtitleOverride);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const DString& ReadSubtitleOverride () const
	{
		return SubtitleOverride;
	}
};
SD_END