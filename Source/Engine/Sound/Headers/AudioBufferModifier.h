/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AudioBufferModifier.h
  An object that is able to adjust the raw audio buffer data of a DynamicAudioAsset.

  This class also interfaces with the AudioEditor to allow developers
  to configure its input and output parameters.

  In addition to that, the information from each buffer modifier instance is
  saved to the DynamicAudioAsset.
=====================================================================
*/

#pragma once

#include "Sound.h"
#include "SoundProperty.h"

SD_BEGIN
class DynamicAudioInstance;

class SOUND_API AudioBufferModifier : public Entity
{
	DECLARE_CLASS(AudioBufferModifier)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all possible outcomes this audio buffer can move to. For branching nodes, this is a list of all possible outcomes. For single output, this contains a single element.
	If empty, then it's assumed that there are no more audio buffers modifiers after this modifier. */
	std::vector<AudioBufferModifier*> NextModifiers;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Called every time the given asset is about to be played. This function can adjust the instance's audio buffer.
	 * This also returns a reference to one of the next modifiers to progress the chain. If this returns nullptr, then the DynamicAudioInstance assumes there are no more
	 * modifications and will proceed to play the resulting modified sound buffer.
	 */
	virtual AudioBufferModifier* DoEffect (DynamicAudioInstance* asset) = 0;

	/**
	 * Called every time whenever a DynamicAudioAsset is about to modify its parameters. This is to reset any audio parameters from the previous
	 * playback. It's worth noting that a single BufferModifier is used for multiple DynamicAudioInstances.
	 */
	virtual void ResetAudioParameters ();

	/**
	 * Returns true if this modifier contains a parameter with the given name.
	 */
	virtual bool HasParamName (const HashedString& paramName) const;

	/**
	 * Searches for the sound property with the give parameter name. If one is found, it'll set its parameter to the give value.
	 * Note: Can't use a template function since virtual templates are not supported in C++. So overloads are used instead.
	 * Returns true if a parameter is found and updated.
	 */
	virtual bool SetSoundParameter (const HashedString& paramName, bool paramValue);
	virtual bool SetSoundParameter (const HashedString& paramName, const DString& paramValue);
	virtual bool SetSoundParameter (const HashedString& paramName, Int paramValue);
	virtual bool SetSoundParameter (const HashedString& paramName, Float paramValue);

	/**
	 * Recursively iterates through each NextModifier and executes the given lambda with that modifier in the parameter.
	 * The lambda is also executed on this modifier instance.
	 */
	virtual void RecursiveExecute (const std::function<void(AudioBufferModifier*)>& lambda);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<AudioBufferModifier*>& ReadNextModifiers () const
	{
		return NextModifiers;
	}
};
SD_END