/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Sound.h
  Contains important file includes and definitions for the Sound module.

  The Sound module allows the application to modify and play sounds. It
  defines a robust framework that allows to statically and dynamically
  play sounds, and instigate events causing external systems to react
  to sound events.

  Main classes:
  AudioAsset - Contains raw data of a single source file.

  DynamicAudioAsset - Performs an audio mix of one or more AudioAssets
  (eg: Swapping sound based on language, randomization, effects).

  AudioComponent - Allows component owner to play sounds.

  SoundListenerComponent - Allows component owner to react to sound events.

  AudioEngineComponent - Plays sounds through the output devices and
  manages active sounds.

  SoundStream - While a sound is playing, this manipulates the sound
  over time (eg: directional sound and attenuation).
  
  Audio Mode - Temporarily adjusts the audio volume balances by channel
  to apply a certain mood.

  Sound vs Audio
  These words are often use interchangeably. But going by their definitions,
  Sand Dune will use the 'Audio' word for classes and functions responsible
  for replicating a sound event (such as components and assets). Classes and
  functions will use 'Sound' for played audio instances such as an active sound event.

  This module depends on the following modules:
  - Core
  - File
  - Graphics
  - Random

  The following modules are optional:
  - MultiThread
  - Random
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"

#ifdef WITH_MULTI_THREAD
//If MultiThreaded is defined, then the AudioEngine will reside in a different thread.
#include "Engine\MultiThread\Headers\MultiThreadClasses.h"
#endif

#ifdef WITH_RANDOM
//If Random is defined, then the random utilities are enabled (such as the AudioBufferArray).
#include "Engine\Random\Headers\RandomClasses.h"
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef SOUND_EXPORT
		#define SOUND_API __declspec(dllexport)
	#else
		#define SOUND_API __declspec(dllimport)
	#endif
#else
	#define SOUND_API
#endif

#include <SFML/Audio.hpp>

#ifdef PlaySound
//Remove intrusive MS preprocessor definition
#undef PlaySound
#endif

#define TICK_GROUP_SOUND "Sound"
#define TICK_GROUP_PRIORITY_SOUND 490 //Update after the transforms are updated and after rendering

#ifdef WITH_MULTI_THREAD
//The Engine index the sound thread resides in will be at index 1.
#define MULTI_THREAD_SOUND_IDX 1
#endif

SD_BEGIN
extern LogCategory SOUND_API SoundLog;

//Sound log used on the thread that plays the sounds. For single threaded applications, this is in the main thread.
extern LogCategory ThreadedSoundLog;

/**
 * Retrieves the correct sound log instance based on the current thread.
 */
extern SOUND_API LogCategory& GetSoundLog ();
SD_END