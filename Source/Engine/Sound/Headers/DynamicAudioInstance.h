/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicAudioInstance.h
  An asset that is a copy of a DynamicAudioAsset. This instance
  allows for external Entities to set its parameters that may determine
  how this asset is played.

  A DynamicAudioInstance is intended to be only played once. Afterwards, it'll
  automatically destroy itself.
=====================================================================
*/

#pragma once

#include "AudioAsset.h"

SD_BEGIN
class AudioBufferModifier;
class AudioComponent;
class DynamicAudioAsset;
class PlayerListenerComponent;

class SOUND_API DynamicAudioInstance : public AudioAsset
{
	DECLARE_CLASS(DynamicAudioInstance)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EParamType
	{
		PT_String,
		PT_Int,
		PT_Float,
		PT_Bool
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to the DynamicAudioAsset this instance was created from. */
	const DynamicAudioAsset* OwningAsset;



	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual DString GetFriendlyName () const override;

	virtual DString GetAudioName () const override;
	virtual const DString& ReadAudioName () const override;
	virtual size_t GetAudioHash () const override;
	virtual DString GetChannelName () const override;
	virtual const DString& ReadChannelName () const override;
	virtual size_t GetChannelHash () const override;
	virtual bool IsLooping () const override;
	virtual bool IsRandomStart () const override;
	virtual DString GetSubtitles () const override;
	virtual const DString& ReadSubtitles () const override;
	virtual bool IsDopplerEnabled () const override;
	virtual Int GetDuckPriority () const override;
	virtual EConflictHandling GetConflictHandling () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetAudioStringParam (const HashedString& paramName, const DString& paramValue);
	virtual void SetAudioIntParam (const HashedString& paramName, Int paramValue);
	virtual void SetAudioFloatParam (const HashedString& paramName, Float paramValue);
	virtual void SetAudioBoolParam (const HashedString& paramName, bool paramValue);
	virtual void SetParamsFromBuffer (const DataBuffer& incomingData);

	/**
	 * Conditionally iterates through the buffer modifiers to adjust this asset's audio buffer.
	 * At this point, it's presumed that all parameters are set, and this asset is about to be played.
	 */
	virtual void ApplyModifiers ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningAsset (const DynamicAudioAsset* newOwningAsset);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const DynamicAudioAsset* GetOwningAsset () const
	{
		return OwningAsset;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if this instance should register to the local audio engine component's OnSoundEnded callback.
	 */
	virtual bool ShouldRegisterAudioCallback () const;

	/**
	 * Recursively searches through each buffer modifier until it finds the instance that matches the parameter name.
	 */
	virtual AudioBufferModifier* FindModifier (const HashedString& paramName, AudioBufferModifier* searchFrom) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSoundEnded (const AudioAsset* asset, AudioComponent* src, PlayerListenerComponent* listener);
};
SD_END