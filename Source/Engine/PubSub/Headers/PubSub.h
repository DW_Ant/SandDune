/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSub.h

  Contains important file includes and definitions for the Pub Sub module.

  The Pub Sub module defines the framework where entities may subscribe to
  event messages, and any entity may invoke a particular event message to publish
  (or broadcast) an event to notify all callbacks subscribed to that event.

  This is a fancy way of using multicast delegates where two separate
  Entities can subscribe without ever needing to reference each other.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef PUB_SUB_EXPORT
		#define PUB_SUB_API __declspec(dllexport)
	#else
		#define PUB_SUB_API __declspec(dllimport)
	#endif
#else
	#define PUB_SUB_API
#endif

SD_BEGIN
extern PUB_SUB_API LogCategory PubSubLog;
SD_END