/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSubMacros.h

  A series of macros to make it easy to create consistent EventMessage subclasses.
=====================================================================
*/

#pragma once

#include "PubSub.h"

/**
 * Declares the utility functions and variables for the given Event's key.
 */
#define DECLARE_EVENT_MESSAGE(className) \
	protected: \
		/* Key used to quickly find the event in the local Engine's event map. This is based on the hash of the eventName. */ \
		static size_t className##Key; \
		\
	public: \
		static size_t GetKey (); \
		virtual SD::DString ToString () const override; \
	private: /* Restore class default accessor. */

/**
 * Defines the variables and functions that were declared from DECLARE_EVENT_MESSAGE.
 */
#define IMPLEMENT_EVENT_MESSAGE(className) \
	size_t className##::##className##Key = 0; \
	\
	size_t className##::GetKey () \
	{ \
		if (##className##Key == 0) \
		{ \
			SD::DString str(#className); \
			ScrambleString(OUT str); \
			className##Key = str.GenerateHash(); \
		} \
		\
		return className##Key; \
	} \
	\
	SD::DString className##::ToString () const \
	{ \
		return #className; \
	}