/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSubUnitTest.h

  A unit test that tests the PubSub module.
=====================================================================
*/

#pragma once

#include "PubSub.h"

#ifdef DEBUG_MODE

SD_BEGIN
class PUB_SUB_API PubSubUnitTest : public UnitTester
{
	DECLARE_CLASS(PubSubUnitTest)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool RunEventMessageTest (EUnitTestFlags testFlags) const;
};
SD_END
#endif