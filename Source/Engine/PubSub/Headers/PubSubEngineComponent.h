/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSubEngineComponent.h
  An EngineComponent that houses all event messages for this thread.
=====================================================================
*/

#pragma once

#include "PubSub.h"

SD_BEGIN
class EventMessage;

class PUB_SUB_API PubSubEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(PubSubEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all EventMessages created in this local thread. */
	std::map<size_t, EventMessage*> Events;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Inserts the new message instance to the map. Returns false if the given key is already occupied.
	 * If the key is already occupied, then the newEvent is not inserted in the map.
	 */
	virtual bool RegisterEvent (size_t eventKey, EventMessage* newEvent);

	/**
	 * Searches through the Events to find the instance associated with the given key.
	 */
	virtual EventMessage* FindEvent (size_t eventKey);

	/**
	 * Searches for the event associated with the given key, and if it's found, it'll remove it from the map and
	 * delete the event.
	 * Returns true if the event is found and deleted.
	 */
	virtual bool DeleteEvent (size_t eventKey);
};
SD_END