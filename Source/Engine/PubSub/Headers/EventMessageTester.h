/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EventMessageTester.h

  An Entity created during a unit test. This Entity will test registering events, broadcasting events,
  and making sure the events are received.
=====================================================================
*/

#pragma once

#include "EventMessage.h"
#include "PubSubMacros.h"

#ifdef DEBUG_MODE

SD_BEGIN
class PUB_SUB_API Event_EmptyEvent : public EventMessage
{
	DECLARE_EVENT_MESSAGE(Event_EmptyEvent);

public:
	MulticastDelegate<> Event;
};

class PUB_SUB_API Event_SimpleEvent : public EventMessage
{
	DECLARE_EVENT_MESSAGE(Event_SimpleEvent);

public:
	MulticastDelegate<Int, const DString&, Float> Event;
};

class PUB_SUB_API Event_ComplicatedEvent : public EventMessage
{
	DECLARE_EVENT_MESSAGE(Event_ComplicatedEvent);

public:
	struct SEventData
	{
		Int a = 0;
		Int b = 0;
		DString strA = DString::EmptyString;
		DString strB = DString::EmptyString;
		Vector3 vecA = Vector3::ZERO_VECTOR;
		Rotator rotA = Rotator::ZERO_ROTATOR;

		DString ToString () const
		{
			return DString::CreateFormattedString(TXT("a=%s, b=%s, strA=%s, strB=%s, vecA=%s, rotA=%s"), a, b, strA, strB, vecA, rotA);
		}

		bool operator== (const SEventData& other) const
		{
			return (a == other.a && b == other.b &&
				vecA == other.vecA && rotA == other.rotA &&
				strA.Compare(other.strA, DString::CC_CaseSensitive) == 0 && strB.Compare(other.strB, DString::CC_CaseSensitive) == 0);
		}

		bool operator!= (const SEventData& other) const
		{
			return !(operator==(other));
		}
	};

	MulticastDelegate<const SEventData&> Event;
};

class PUB_SUB_API EventMessageTester : public Object
{
	DECLARE_CLASS(EventMessageTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Number of times the empty event was received. */
	Int EmptyEventCounter;
	bool bEmptyEventRegistered;

	/* Variables associated with the simple test event. */
	Int SimpleEventCounter;
	Int SimpleEventTotalA;
	DString SimpleEventCombinedB;
	Float SimpleEventTotalC;
	bool bSimpleEventRegistered;

	/* Variables associated with the complicated test event. */
	Event_ComplicatedEvent::SEventData ComplicatedEventReceivedStruct;
	bool bComplicatedEventRegistered;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Runs the series of tests synchronously, and it returns true if all tests passed.
	 */
	virtual bool RunTests (UnitTester::EUnitTestFlags testFlags, const UnitTester* tester);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Unregisters any event this object subscribed to.
	 */
	virtual void UnsubscribeEvents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEmptyEvent ();
	virtual void HandleSimpleEvent (Int a, const DString& b, Float c);
	virtual void HandleComplicatedEvent (const Event_ComplicatedEvent::SEventData& evnt);
};
SD_END
#endif