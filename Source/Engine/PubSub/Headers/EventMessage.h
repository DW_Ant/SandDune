/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EventMessage.h
  An object that maintains a list of subscribers and registers itself to the local
  engine component for accessing purposes.

  Note: Although it would be simpler to access EventMessages via static accessors, it's instead
  accessed in a map within PubSubEngineComponent to make contain it in the local thread; events
  only broadcast messages to subscribers in the same thread.
=====================================================================
*/

#pragma once

#include "PubSub.h"
#include "PubSubEngineComponent.h"

SD_BEGIN
class PUB_SUB_API EventMessage
{


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EventMessage ();
	virtual ~EventMessage ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Events use a string to generate a hash value. Unfortunately, the MSVC only considers the first few characters when generating a hash: https://stackoverflow.com/a/7969544
	 * This function attempts to diminish the likelihood of generating hash collisions.
	 * Under the assumption that other systems will use prefixes for their event names, this function will scramble the characters in a deterministic way so that
	 * event names with long prefixes will not cause collisions for MSVC builds.
	 */
	static void ScrambleString (DString& outStr);

	/**
	 * Returns a string representation of this event. Primarily used for debugging and logging purposes.
	 */
	virtual DString ToString () const = 0;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Obtains the EventMessage instance of matching class from the local PubSubEngineComponent.
	 * If the EventMessage does not exist, it would create one.
	 */
	template <class T>
	static T* FindEvent ()
	{
		PubSubEngineComponent* localPsEngine = PubSubEngineComponent::Find();
		if (localPsEngine == nullptr)
		{
			PubSubLog.Log(LogCategory::LL_Warning, TXT("Unable to find event since there isn't a PubSubEngineComponent instance in this thread."));
			return nullptr;
		}

		size_t eventKey = T::GetKey();
		EventMessage* evnt = localPsEngine->FindEvent(eventKey);
		if (evnt == nullptr)
		{
			//Not registered, create a new message instance
			T* newEvent = new T();
			if (!localPsEngine->RegisterEvent(eventKey, newEvent))
			{
				delete newEvent;
				return nullptr;
			}

			return newEvent;
		}

		T* result = dynamic_cast<T*>(evnt);
		if (result == nullptr)
		{
			PubSubLog.Log(LogCategory::LL_Warning, TXT("Key conflict found in the PubSubEngineComponent. The key %s is found in the engine component, but it returned the incorrect event type. It found %s instead."), Int(eventKey), evnt->ToString());
			return nullptr;
		}

		return result;
	}
};
SD_END