/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EventMessage.cpp
=====================================================================
*/

#include "EventMessage.h"

SD_BEGIN
EventMessage::EventMessage ()
{
	//Noop
}

EventMessage::~EventMessage ()
{
	//Noop
}

void EventMessage::ScrambleString (DString& outStr)
{
	//Swap the first 3 characters with the last 3 characters to make the end of the string relevant for long event names.
	//Converts MyLongEvent_105 to 501ongEvent_LyM
	for (Int i = 0; i < 3 && i < outStr.Length(); ++i)
	{
		std::swap(outStr[i.ToUnsignedInt()], outStr[(outStr.Length() - (i+1)).ToUnsignedInt()]);
	}
}
SD_END