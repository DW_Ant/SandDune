/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSub.cpp
=====================================================================
*/

#include "PubSub.h"

SD_BEGIN
LogCategory PubSubLog(TXT("PubSub"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);
SD_END