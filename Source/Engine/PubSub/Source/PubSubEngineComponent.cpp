/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSubEngineComponent.cpp
=====================================================================
*/

#include "PubSubEngineComponent.h"
#include "EventMessage.h"

IMPLEMENT_ENGINE_COMPONENT(SD::PubSubEngineComponent)
SD_BEGIN

void PubSubEngineComponent::ShutdownComponent ()
{
	for (auto mapIter = Events.begin(); mapIter != Events.end(); ++mapIter)
	{
		delete mapIter->second;
	}

	Events.clear();

	Super::ShutdownComponent();
}

bool PubSubEngineComponent::RegisterEvent (size_t eventKey, EventMessage* newEvent)
{
	auto results = Events.emplace(eventKey, newEvent);
	if (!results.second)
	{
		PubSubLog.Log(LogCategory::LL_Warning, TXT("Failed to register event %s since the given key %s is already occupied."), newEvent->ToString(), Int(eventKey));
	}

	return results.second;
}

EventMessage* PubSubEngineComponent::FindEvent (size_t eventKey)
{
	auto results = Events.find(eventKey);
	if (results == Events.end())
	{
		return nullptr;
	}

	return results->second;
}

bool PubSubEngineComponent::DeleteEvent (size_t eventKey)
{
	auto evnt = Events.find(eventKey);
	if (evnt != Events.end())
	{
		delete evnt->second;
		Events.erase(evnt);
		return true;
	}

	return false;
}
SD_END