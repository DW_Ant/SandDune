/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PubSubUnitTest.cpp
=====================================================================
*/

#include "PubSubUnitTest.h"
#include "EventMessageTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::PubSubUnitTest, SD::UnitTester)
SD_BEGIN

bool PubSubUnitTest::RunTests (EUnitTestFlags testFlags) const
{
	bool result = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		result &= RunEventMessageTest(testFlags);
	}

	return result;
}

bool PubSubUnitTest::RunEventMessageTest (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Event Message"));

	//Redirect the test to the EventMessageTester since that will be handling delegate handling.
	EventMessageTester* tester = EventMessageTester::CreateObject();
	if (!tester->RunTests(testFlags, this))
	{
		tester->Destroy();
		return false;
	}

	tester->Destroy();
	ExecuteSuccessSequence(testFlags, TXT("Event Message"));
	return true;
}
SD_END
#endif