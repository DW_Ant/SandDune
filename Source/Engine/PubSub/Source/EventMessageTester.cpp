/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EventMessageTester.cpp
=====================================================================
*/

#include "EventMessageTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::EventMessageTester, SD::Object)
SD_BEGIN
IMPLEMENT_EVENT_MESSAGE(Event_EmptyEvent)
IMPLEMENT_EVENT_MESSAGE(Event_SimpleEvent)
IMPLEMENT_EVENT_MESSAGE(Event_ComplicatedEvent)

void EventMessageTester::InitProps ()
{
	Super::InitProps();

	EmptyEventCounter = 0;
	bEmptyEventRegistered = false;

	SimpleEventCounter = 0;
	SimpleEventTotalA = 0;
	SimpleEventCombinedB = DString::EmptyString;
	SimpleEventTotalC = 0.f;
	bSimpleEventRegistered = false;

	bComplicatedEventRegistered = false;
}

void EventMessageTester::Destroyed ()
{
	UnsubscribeEvents();

	Super::Destroyed();
}

bool EventMessageTester::RunTests (UnitTester::EUnitTestFlags testFlags, const UnitTester* tester)
{
	//Reset to defaults
	const EventMessageTester* cdo = dynamic_cast<const EventMessageTester*>(GetDefaultObject());
	if (cdo != nullptr)
	{
		EmptyEventCounter = cdo->EmptyEventCounter;
		SimpleEventCounter = cdo->SimpleEventCounter;
		SimpleEventTotalA = cdo->SimpleEventTotalA;
		SimpleEventCombinedB = cdo->SimpleEventCombinedB;
		SimpleEventTotalC = cdo->SimpleEventTotalC;
		ComplicatedEventReceivedStruct = cdo->ComplicatedEventReceivedStruct;
	}

	tester->SetTestCategory(testFlags, TXT("Key Generation"));
	{
		//Ensure all three keys are unique.
		if (Event_EmptyEvent::GetKey() == Event_SimpleEvent::GetKey())
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. The generated keys from Empty Event and Simple Event should have been unique. Instead both of them generated %s."), Int(Event_EmptyEvent::GetKey()));
			return false;
		}

		if (Event_EmptyEvent::GetKey() == Event_ComplicatedEvent::GetKey())
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. The generated keys from Empty Event and Complicated Event should have been unique. Instead both of them generated %s."), Int(Event_EmptyEvent::GetKey()));
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->SetTestCategory(testFlags, TXT("Find Events"));
	{
		tester->TestLog(testFlags, TXT("Note: Searching for an event message for the first time should automatically register the event to the local engine component."));
		const Event_EmptyEvent* emptyEvnt = EventMessage::FindEvent<Event_EmptyEvent>();
		if (emptyEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to register empty event to the local engine component."));
			return false;
		}

		const Event_SimpleEvent* simpleEvnt = EventMessage::FindEvent<Event_SimpleEvent>();
		if (simpleEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to register simple event to the local engine component."));
			return false;
		}

		const Event_ComplicatedEvent* complicatedEvnt = EventMessage::FindEvent<Event_ComplicatedEvent>();
		if (complicatedEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to register complicated event to the local engine component."));
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->SetTestCategory(testFlags, TXT("Broadcasting Events"));
	{
		//Although we found the events in the previous test category, search for them again to test if we can find previous existing events.
		Event_EmptyEvent* emptyEvnt = EventMessage::FindEvent<Event_EmptyEvent>();
		if (emptyEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to find empty event in the local engine component."));
			return false;
		}

		Event_SimpleEvent* simpleEvnt = EventMessage::FindEvent<Event_SimpleEvent>();
		if (simpleEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to find simple event in the local engine component."));
			return false;
		}

		Event_ComplicatedEvent* complicatedEvnt = EventMessage::FindEvent<Event_ComplicatedEvent>();
		if (complicatedEvnt == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. Unable to find complicated event in the local engine component."));
			return false;
		}

		emptyEvnt->Event.RegisterHandler(SDFUNCTION(this, EventMessageTester, HandleEmptyEvent, void));
		simpleEvnt->Event.RegisterHandler(SDFUNCTION_3PARAM(this, EventMessageTester, HandleSimpleEvent, void, Int, const DString&, Float));
		complicatedEvnt->Event.RegisterHandler(SDFUNCTION_1PARAM(this, EventMessageTester, HandleComplicatedEvent, void, const Event_ComplicatedEvent::SEventData&));
		bEmptyEventRegistered = true;
		bSimpleEventRegistered = true;
		bComplicatedEventRegistered = true;

		Int expectedEmptyCounter = 0;
		for (int i = 0; i < 3; ++i)
		{
			emptyEvnt->Event.Broadcast();
			expectedEmptyCounter++;
			if (EmptyEventCounter != expectedEmptyCounter)
			{
				tester->UnitTestError(testFlags, TXT("Event Message test failed. The empty event counter should be equal to %s. Instead it's %s."), expectedEmptyCounter, EmptyEventCounter);
				return false;
			}
		}

		Int expectedSimpleCounter = 0;
		Int expectedInt = 5;
		DString expectedStr = TXT("Yohkuj");
		Float expectedFloat = 3.14f;
		std::function<bool()> testSimpleEvent([&]()
		{
			if (SimpleEventCounter != expectedSimpleCounter)
			{
				tester->UnitTestError(testFlags, TXT("Event Message test failed. Expected the simple event to broadcast %s times. Instead it only broadcasted %s times."), expectedSimpleCounter, SimpleEventCounter);
				return false;
			}

			if (SimpleEventTotalA != expectedInt)
			{
				tester->UnitTestError(testFlags, TXT("Event Message test failed. The simple event's sum of ints should have been %s. Instead it's %s."), expectedInt, SimpleEventTotalA);
				return false;
			}

			if (expectedStr.Compare(SimpleEventCombinedB, DString::CC_CaseSensitive) != 0)
			{
				tester->UnitTestError(testFlags, TXT("Event Message test failed. The combined string from the simple event's string param should have been \"%s\". Instead it's \"%s\"."), expectedStr, SimpleEventCombinedB);
				return false;
			}

			if (expectedFloat != SimpleEventTotalC)
			{
				tester->UnitTestError(testFlags, TXT("Event Message test failed. The simple event's sum of floats should have been %s. Instead it's %s."), expectedFloat, SimpleEventTotalC);
				return false;
			}

			return true;
		});

		simpleEvnt->Event.Broadcast(expectedInt, expectedStr, expectedFloat);
		expectedSimpleCounter++;
		if (!testSimpleEvent())
		{
			return false;
		}

		Int addInt = 3;
		DString appendStr = TXT("Lykkos");
		Float addFloat = 4.1f;
		expectedInt += addInt;
		expectedStr += appendStr;
		expectedFloat += addFloat;
		simpleEvnt->Event.Broadcast(addInt, appendStr, addFloat);
		expectedSimpleCounter++;
		if (!testSimpleEvent())
		{
			return false;
		}

		addInt = 9;
		appendStr = TXT("Vanites");
		addFloat = -2.5f;
		expectedInt += addInt;
		expectedStr += appendStr;
		expectedFloat += addFloat;
		simpleEvnt->Event.Broadcast(addInt, appendStr, addFloat);
		expectedSimpleCounter++;
		if (!testSimpleEvent())
		{
			return false;
		}

		addInt = 52;
		appendStr = TXT("Kytmaw");
		addFloat = -128.f;
		expectedInt += addInt;
		expectedStr += appendStr;
		expectedFloat += addFloat;
		simpleEvnt->Event.Broadcast(addInt, appendStr, addFloat);
		expectedSimpleCounter++;
		if (!testSimpleEvent())
		{
			return false;
		}

		Event_ComplicatedEvent::SEventData expectedData;
		expectedData.a = 8;
		expectedData.b = -4;
		expectedData.strA = TXT("Kjas");
		expectedData.strB = TXT("Aldasa");
		expectedData.vecA = Vector3(84.f, -21.f, 8.6f);
		expectedData.rotA = Rotator(Rotator::HALF_REVOLUTION, Rotator::QUARTER_REVOLUTION, 0);
		complicatedEvnt->Event.Broadcast(expectedData);
		if (expectedData != ComplicatedEventReceivedStruct)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. The expected data [%s] does not match what was obtained [%s]."), expectedData, ComplicatedEventReceivedStruct);
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->SetTestCategory(testFlags, TXT("Remove Events"));
	{
		Event_EmptyEvent* emptyEvnt = EventMessage::FindEvent<Event_EmptyEvent>();
		Event_SimpleEvent* simpleEvnt = EventMessage::FindEvent<Event_SimpleEvent>();
		Event_ComplicatedEvent* complicatedEvnt = EventMessage::FindEvent<Event_ComplicatedEvent>();
		CHECK(emptyEvnt != nullptr && simpleEvnt != nullptr && complicatedEvnt != nullptr)

		emptyEvnt->Event.UnregisterHandler(SDFUNCTION(this, EventMessageTester, HandleEmptyEvent, void));
		simpleEvnt->Event.UnregisterHandler(SDFUNCTION_3PARAM(this, EventMessageTester, HandleSimpleEvent, void, Int, const DString&, Float));
		complicatedEvnt->Event.UnregisterHandler(SDFUNCTION_1PARAM(this, EventMessageTester, HandleComplicatedEvent, void, const Event_ComplicatedEvent::SEventData&));
		bEmptyEventRegistered = false;
		bSimpleEventRegistered = false;
		bComplicatedEventRegistered = false;

		Int oldCounter = EmptyEventCounter;

		//Ensure the event is actually unregistered
		emptyEvnt->Event.Broadcast();
		if (oldCounter != EmptyEventCounter)
		{
			tester->UnitTestError(testFlags, TXT("Event Message test failed. The EventMessageTester is still subscribed to the empty event after attempting to unregister it."));
			return false;
		}

		//Although it's fine to leave events in the engine component, this test will verify that it's possible to remove events from the component.
		PubSubEngineComponent* localPsEngine = PubSubEngineComponent::Find();
		CHECK(localPsEngine != nullptr)

		std::function<bool(EventMessage*, size_t)> deleteEvnt([&](EventMessage* evnt, size_t evntKey)
		{
			if (!localPsEngine->DeleteEvent(evntKey))
			{
				tester->UnitTestError(testFlags, TXT("Event Messagetest failed. Could not delete %s from the PubSubEngineComponent."), evnt->ToString());
				return false;
			}

			return true;
		});

		if (!deleteEvnt(emptyEvnt, emptyEvnt->GetKey()) || !deleteEvnt(simpleEvnt, simpleEvnt->GetKey()) || !deleteEvnt(complicatedEvnt, complicatedEvnt->GetKey()))
		{
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	return true;
}

void EventMessageTester::UnsubscribeEvents ()
{
	if (bEmptyEventRegistered)
	{
		Event_EmptyEvent* evnt = EventMessage::FindEvent<Event_EmptyEvent>();
		CHECK(evnt != nullptr)
		evnt->Event.UnregisterHandler(SDFUNCTION(this, EventMessageTester, HandleEmptyEvent, void));
		bEmptyEventRegistered = false;
	}

	if (bSimpleEventRegistered)
	{
		Event_SimpleEvent* evnt = EventMessage::FindEvent<Event_SimpleEvent>();
		CHECK(evnt != nullptr)
		evnt->Event.UnregisterHandler(SDFUNCTION_3PARAM(this, EventMessageTester, HandleSimpleEvent, void, Int, const DString&, Float));
		bSimpleEventRegistered = false;
	}

	if (bComplicatedEventRegistered)
	{
		Event_ComplicatedEvent* evnt = EventMessage::FindEvent<Event_ComplicatedEvent>();
		CHECK(evnt != nullptr)
		evnt->Event.UnregisterHandler(SDFUNCTION_1PARAM(this, EventMessageTester, HandleComplicatedEvent, void, const Event_ComplicatedEvent::SEventData&));
		bComplicatedEventRegistered = false;
	}
}

void EventMessageTester::HandleEmptyEvent ()
{
	EmptyEventCounter++;
}

void EventMessageTester::HandleSimpleEvent (Int a, const DString& b, Float c)
{
	SimpleEventCounter++;
	SimpleEventTotalA += a;
	SimpleEventCombinedB += b;
	SimpleEventTotalC += c;
}

void EventMessageTester::HandleComplicatedEvent (const Event_ComplicatedEvent::SEventData& evnt)
{
	ComplicatedEventReceivedStruct = evnt;
}
SD_END
#endif