/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LatentTimerTester.cpp
=====================================================================
*/

#include "LatentTimerTester.h"
#include "TimerEngineComponent.h"
#include "TimerManager.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::LatentTimerTester, SD::Entity)
SD_BEGIN

void LatentTimerTester::InitProps ()
{
	Super::InitProps();

	OwningUnitTester = nullptr;
	NumEventsTriggered = 0;
	MaxNumRepeatedEvents = 3;
	EventInterval = 1.f;
	StartTime = 0.f;
	bFailedTest = false;
	UnitTestStopwatch = nullptr;
}

void LatentTimerTester::Destroyed ()
{
	if (!bFailedTest)
	{
		UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  Unit test is destroyed properly because its LifeSpanComponent timed out."));
		UnitTester::TestLog(TestFlags, TXT("End timer test."));
		UnitTester::TestLog(TestFlags, TXT(""));
	}

	RemoveStopwatch();

	Super::Destroyed();
}

void LatentTimerTester::LaunchTest (UnitTester::EUnitTestFlags testFlags, const UnitTester* owningUnitTester)
{
	TestFlags = testFlags;
	OwningUnitTester = owningUnitTester;
	UnitTester::TestLog(TestFlags, TXT("Begin timer unit test. . ."));
	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  This test should trigger one standard event and %s repeated events within %s second(s)."), MaxNumRepeatedEvents, (MaxNumRepeatedEvents.ToFloat() * EventInterval));

	StartTime = Engine::FindEngine()->GetElapsedTime();

	RemoveStopwatch(); //Replace stopwatch if it already existed.
	UnitTestStopwatch = new Stopwatch(TXT("Timer unit test stopwatch"));

	TickComponent* ticker = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (ticker == nullptr)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Failed to initialize LatentTimerTester since it was unable to instantiate a tick component!"));
		bFailedTest = true;
		Destroy();
		return;
	}

	ticker->SetTickHandler(SDFUNCTION_1PARAM(this, LatentTimerTester, HandleTick, void, Float));
	AddComponent(ticker);

	TimerEngineComponent* timerEngine = TimerEngineComponent::Find();
	CHECK(timerEngine != nullptr)
	TimerManager* timeManager = timerEngine->GetTimeManager();
	CHECK(timeManager != nullptr)

	timeManager->SetTimer(this, EventInterval, false, SDFUNCTION(this, LatentTimerTester, HandleSingleEvent, void));
	timeManager->SetTimer(this, EventInterval, true, SDFUNCTION(this, LatentTimerTester, HandleRepeatedEvent, void));
	timeManager->SetTimer(this, EventInterval, false, SDFUNCTION(this, LatentTimerTester, HandleFalseEvent, void));
	timeManager->RemoveTimer(SDFUNCTION(this, LatentTimerTester, HandleFalseEvent, void));

	LifeSpanComponent* lifeSpan = LifeSpanComponent::CreateObject();
	if (lifeSpan == nullptr)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Failed to initialize LatentTimerTester since it was unable to instantiate a life span component!"));
		bFailedTest = true;
		Destroy();
		return;
	}

	//Ensure that this unit tester expires one second after the last repeated event
	lifeSpan->LifeSpan = Float(round((MaxNumRepeatedEvents.ToFloat() * EventInterval).Value / 1.f)) + 1.f;
	AddComponent(lifeSpan);
}

void LatentTimerTester::VerifyEvents ()
{
	//Should be at most one per repeated event plus one from single event
	if (NumEventsTriggered > MaxNumRepeatedEvents + 1)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  Too many events were triggered. %s > %s"), NumEventsTriggered, (MaxNumRepeatedEvents + 1));
		bFailedTest = true;
		Destroy();
	}
}

void LatentTimerTester::RemoveStopwatch ()
{
	if (UnitTestStopwatch != nullptr)
	{
		delete UnitTestStopwatch;
		UnitTestStopwatch = nullptr;
	}
}

void LatentTimerTester::HandleSingleEvent ()
{
	NumEventsTriggered++;
	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  single timer event was called.  Total number of events is now:  %s"), NumEventsTriggered.ToString());
	VerifyEvents();
}

void LatentTimerTester::HandleRepeatedEvent ()
{
	static Int repeatedCounter = 0;

	repeatedCounter++;
	NumEventsTriggered++;

	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  repeated timer event was called (%s).  Total number number of events is now:  %s"), repeatedCounter, NumEventsTriggered);
	VerifyEvents();

	if (repeatedCounter >= MaxNumRepeatedEvents)
	{
		UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  MaxNumRepeatedEvents (%s) reached; stopping repeated timer."), MaxNumRepeatedEvents);

		TimerEngineComponent* timerEngine = TimerEngineComponent::Find();
		CHECK(timerEngine != nullptr)

		TimerManager* timeManager = timerEngine->GetTimeManager();
		CHECK(timeManager != nullptr)

		timeManager->RemoveTimer(SDFUNCTION(this, LatentTimerTester, HandleRepeatedEvent, void));
	}
}

void LatentTimerTester::HandleFalseEvent ()
{
	RemoveStopwatch();
	OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  The false event was still triggered when it should have been removed from the Timer Manager before triggering this event."));
	bFailedTest = true;
	Destroy();
}

void LatentTimerTester::HandleTick (Float deltaSec)
{
	// The time elapsed condition must fail twice in a row to handle cases such as (Very large delta times, or the tick component was ticked before timer manager)
	static bool bExpiredPreviously = false;

	//Check if too much time elapsed
	if (bExpiredPreviously && Engine::FindEngine()->GetElapsedTime() - StartTime > (EventInterval * MaxNumRepeatedEvents.ToFloat()) + 2) //add 2 seconds to time limit to ensure it's considered after the LifeSpanComponent's expiration time
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  Not enough events (%s) were triggered within the time limit.\nThis test should have finished in %s seconds."), NumEventsTriggered, (EventInterval * MaxNumRepeatedEvents.ToFloat()));
		bFailedTest = true;
		Destroy();
	}

	bExpiredPreviously = (Engine::FindEngine()->GetElapsedTime() - StartTime > (EventInterval * MaxNumRepeatedEvents.ToFloat()) + 2);
}
SD_END

#endif