/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Time.cpp
=====================================================================
*/

#include "SdTime.h"

SD_BEGIN
LogCategory TimeLog(TXT("Time"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);
SD_END