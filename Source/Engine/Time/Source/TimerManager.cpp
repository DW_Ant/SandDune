/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimerManager.cpp
=====================================================================
*/

#include "TimerManager.h"

IMPLEMENT_CLASS(SD::TimerManager, SD::Entity)
SD_BEGIN

void TimerManager::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, TimerManager, HandleGarbageCollection, void));

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (tick == nullptr)
	{
		//this should never happen
		localEngine->FatalError(TXT("Unable to create a tick component for ") + ToString());
		return;
	}

	tick->SetTickHandler(SDFUNCTION_1PARAM(this, TimerManager, HandleTick, void, Float));
	AddComponent(tick);
}

void TimerManager::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, TimerManager, HandleGarbageCollection, void));

	Super::Destroyed();
}

bool TimerManager::SetTimer (const Object* targetObject, Float timeDelay, bool bRepeat, const SDFunction<void>& functionCallback)
{
	if (!functionCallback.IsBounded())
	{
		return false;
	}

	bRepeat = (bRepeat && timeDelay > 0);

	for (size_t i = 0; i < Timers.size(); i++)
	{
		if (Timers.at(i).EventHandler == functionCallback)
		{
			Timers.at(i).TimeRemaining = timeDelay;
			Timers.at(i).TimeInterval = (bRepeat) ? timeDelay : 0;
			Timers.at(i).bPendingDelete = false; //Revert pending delete if it was about to be deleted

			return true;
		}
	}

	STimerData& newData = Timers.emplace_back();
	newData.OwningObject = targetObject;
	newData.EventHandler = functionCallback;
	newData.TimeInterval = (bRepeat) ? timeDelay : 0.f;
	newData.TimeRemaining = timeDelay;

	return false;
}

bool TimerManager::RemoveTimer (const SDFunction<void>& targetCallback)
{
	for (size_t i = 0; i < Timers.size(); i++)
	{
		if (Timers.at(i).EventHandler == targetCallback)
		{
			//Add this entry to the list of entries to delete next tick (since this could be called within tick).
			Timers.at(i).bPendingDelete = true;
			return true;
		}
	}

	return false;
}

Float TimerManager::GetTimeRemaining (const SDFunction<void>& targetCallback) const
{
	for (size_t i = 0; i < Timers.size(); i++)
	{
		if (Timers.at(i).EventHandler == targetCallback)
		{
			return Timers.at(i).TimeRemaining;
		}
	}

	return 0;
}

void TimerManager::EvaluateTimerObjects ()
{
	for (size_t i = 0; i < Timers.size(); i++)
	{
		if (Timers.at(i).bPendingDelete)
		{
			continue; //already marked.  Skip check
		}

		if (!IsValidTimerData(i))
		{
			Timers.at(i).bPendingDelete = true;
		}
	}
}

bool TimerManager::IsValidTimerData (UINT_TYPE timerIdx) const
{
	return (VALID_OBJECT(Timers.at(timerIdx).OwningObject));
}

void TimerManager::ApplyPendingDelete ()
{
	size_t i = 0;
	while (i < Timers.size())
	{
		if (Timers.at(i).bPendingDelete)
		{
			Timers.erase(Timers.begin() + i);
			continue;
		}

		++i;
	}
}

void TimerManager::HandleTick (Float deltaSec)
{
	EvaluateTimerObjects(); //Check if any timers should be marked for deletion.
	ApplyPendingDelete(); //Remove all PendingDelete timers

	for (size_t i = 0; i < Timers.size(); i++)
	{
		Timers.at(i).TimeRemaining -= deltaSec;

		//In case the timer was invoked multiple times this tick due to short interval or long deltaSec
		while(Timers.at(i).TimeRemaining <= 0 && !Timers.at(i).bPendingDelete)
		{
			if (Timers.at(i).OwningObject->GetPendingDelete())
			{
				//OwningObject->Destroy was called recently.
				Timers.at(i).bPendingDelete = true;
				break;
			}

			Timers.at(i).EventHandler();

			if (Timers.at(i).bPendingDelete || !VALID_OBJECT(Timers.at(i).OwningObject))
			{
				//Either RemoveTimer was called within EventHandler or the owning object was destroyed.
				Timers.at(i).bPendingDelete = true; //Notify parent loop to skip
				break;
			}

			//check time remaining one more time in case the EventHandler called SetTimer to itself
			if (Timers.at(i).TimeRemaining > 0)
			{
				break;
			}

			if (Timers.at(i).TimeInterval <= 0)
			{
				//Not repeating... remove timer
				Timers.at(i).bPendingDelete = true;
				break;
			}

			Timers.at(i).TimeRemaining += Timers.at(i).TimeInterval;
		} //End while (TimeRemaining < 0 && !bPendingDelete)
	} //End for (i < Timers.size())
} //End HandleTick

void TimerManager::HandleGarbageCollection ()
{
	//Check the timer list to see if any of the objects were destroyed
	for (size_t i = 0; i < Timers.size(); i++)
	{
		if (Timers.at(i).OwningObject == nullptr || Timers.at(i).OwningObject->GetPendingDelete())
		{
			Timers.at(i).OwningObject = nullptr;
			Timers.at(i).bPendingDelete = true;
		}
	}
}
SD_END