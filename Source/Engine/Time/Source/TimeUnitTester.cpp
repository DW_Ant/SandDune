/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimeUnitTester.cpp
=====================================================================
*/

#include "DateTime.h"
#include "LatentTimerTester.h"
#include "TimeUnitTester.h"
#include "TimeUtils.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::TimeUnitTester, SD::UnitTester)
SD_BEGIN

bool TimeUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult = (TestTimeUtils(testFlags) && TestDateTime(testFlags));
	}

	if (bResult && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		bResult = (LaunchLatentTest(testFlags));
	}

	return bResult;
}

bool TimeUnitTester::TestTimeUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Time Utilities"));

	TestLog(testFlags, TXT("Testing number of days within month."));
	TimeUtils::SMonthInfo month;

	//Test days within month (ignoring leap years)
	if (TimeUtils::NumDaysWithinMonth(0) != 31)
	{
		TimeUtils::GetMonthInfo(0, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(1) != 28)
	{
		TimeUtils::GetMonthInfo(1, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  28 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(2) != 31)
	{
		TimeUtils::GetMonthInfo(2, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(3) != 30)
	{
		TimeUtils::GetMonthInfo(3, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  30 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(4) != 31)
	{
		TimeUtils::GetMonthInfo(4, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(5) != 30)
	{
		TimeUtils::GetMonthInfo(5, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  30 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(6) != 31)
	{
		TimeUtils::GetMonthInfo(6, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(7) != 31)
	{
		TimeUtils::GetMonthInfo(7, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(8) != 30)
	{
		TimeUtils::GetMonthInfo(8, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  30 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(9) != 31)
	{
		TimeUtils::GetMonthInfo(9, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(10) != 30)
	{
		TimeUtils::GetMonthInfo(10, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  30 days are expected to be within %s."), month.FullMonthName);
		return false;
	}

	if (TimeUtils::NumDaysWithinMonth(11) != 31)
	{
		TimeUtils::GetMonthInfo(11, month);
		UnitTestError(testFlags, TXT("Time Utils test failed.  31 days are expected to be within %s."), month.FullMonthName);
		return false;
	}
	TestLog(testFlags, TXT("Number of days within month test passed!"));

	ExecuteSuccessSequence(testFlags, TXT("Time Utilities"));
	return true;
}

bool TimeUnitTester::TestDateTime (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Date Time"));

	DateTime blankTime;
	DateTime monsterEvoRelease;
	monsterEvoRelease.Second = 0;
	monsterEvoRelease.Minute = 11;
	monsterEvoRelease.Hour = 23;
	monsterEvoRelease.Day = 11;
	monsterEvoRelease.Month = 11;
	monsterEvoRelease.Year = 2011;
	DateTime evoReleaseCopy = monsterEvoRelease;

	SetTestCategory(testFlags, TXT("Comparisons"));
	{
		if (blankTime == monsterEvoRelease)
		{
			UnitTestError(testFlags, TXT("Comparison test failed.  %s should not equal to %s."), blankTime.ToIsoFormat(), monsterEvoRelease.ToIsoFormat());
			return false;
		}

		if (monsterEvoRelease != evoReleaseCopy)
		{
			UnitTestError(testFlags, TXT("Comparison test failed.  %s should be equal to %s."), monsterEvoRelease.ToIsoFormat(), evoReleaseCopy.ToIsoFormat());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		DateTime halfer;
		halfer.Second = 30;
		halfer.Minute = 30;
		halfer.Hour = 12;
		halfer.Day = 15;
		halfer.Month = 6;
		halfer.Year = 1000;

		DateTime tester = monsterEvoRelease + halfer;
		DateTime expected;
		expected.Second = 30;
		expected.Minute = 41;
		expected.Hour = 35;
		expected.Day = 26;
		expected.Month = 17;
		expected.Year = 3011;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Comparison test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		DateTime multiplier;
		multiplier.Second = 2;
		multiplier.Minute = 2;
		multiplier.Hour = 2;
		multiplier.Day = 2;
		multiplier.Month = 2;
		multiplier.Year = 2;

		tester *= multiplier;
		expected.Second = 60;
		expected.Minute = 82;
		expected.Hour = 70;
		expected.Day = 52;
		expected.Month = 34;
		expected.Year = 6022;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Comparison test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Wrap"));
	{
		DateTime tester = blankTime;
		tester.Second = 90;
		tester.WrapSeconds();

		DateTime expected = blankTime;
		expected.Second = 30;
		expected.Minute = 1;

		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Second wrap test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		tester = blankTime;
		expected = blankTime;
		tester.Minute = 200;
		tester.WrapMinutes();
		expected.Minute = 20;
		expected.Hour = 3;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Minute wrap test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		tester = blankTime;
		expected = blankTime;
		tester.Hour = 30;
		tester.WrapHours();
		expected.Hour = 6;
		expected.Day = 1;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Hour wrap test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		tester = blankTime;
		expected = blankTime;
		tester.Day = 35;
		tester.WrapDays();
		expected.Day = 4;
		expected.Month = 1;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Day wrap test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		tester = blankTime;
		expected = blankTime;
		tester.Month = 100;
		tester.WrapMonths();
		expected.Month = 4;
		expected.Year = 8;
		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Month wrap test failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}

		tester = blankTime;
		expected = blankTime;
		tester.Second = 100;
		tester.Minute = 100;
		tester.Hour = 100;
		tester.Day = 100;
		tester.Month = 100;
		tester.Year = 105;
		tester.WrapDateTime();
		expected.Second = 40; //wrap to 1 minute
		expected.Minute = 41; //wrap to 1 hour
		expected.Hour = 5; //wrap to 4 days

		//Wrap months first to figure out current month
		expected.Month = 4; //wrap to 8 years
		expected.Year = 113;

		//Current month is May... 31 days
		expected.Day = 73; //don't forget that 4 days were added from wrapping hours
		expected.Month++;

		//Current month is June... 30 days
		expected.Day = 43;
		expected.Month++;

		//Current month is July... 31 days
		expected.Day = 12;
		expected.Month++;

		if (tester != expected)
		{
			UnitTestError(testFlags, TXT("Date time wrap failed.  %s should be equal to %s."), tester.ToIsoFormat(), expected.ToIsoFormat());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculation Sum"));
	{
		DateTime tester = blankTime;
		tester.Second = 15;
		tester.Minute = 30;
		tester.Hour = 2;
		if (tester.CalculateTotalSeconds() != 9015)
		{
			UnitTestError(testFlags, TXT("Calculate total seconds failed.  Total seconds for %s should be equal to 9015.  Instead it returned %s."), tester.ToIsoFormat(), tester.CalculateTotalSeconds());
			return false;
		}

		if (abs((tester.CalculateTotalHours() - 2.504167f).Value) > 0.00001)
		{
			UnitTestError(testFlags, TXT("Calculate total hours failed.  Total hours for %s should be about 2.504167.  Instead it returned %s."), tester.ToIsoFormat(), tester.CalculateTotalHours());
			return false;
		}

		tester = blankTime;
		tester.Hour = 12;
		tester.Day = 10;
		tester.Month = 4;
		tester.Year = 3;

		//Year 0 is leap year.
		//(366) + (365 * 2) +  (31 + 28 + 31 + 30) + 10 + 0.5
		if (tester.CalculateTotalDays() != 1226.5)
		{
			UnitTestError(testFlags, TXT("Calculate total days failed.  Total days for %s should be equal to 1225.5.  Instead it returned %s"), tester.ToIsoFormat(), tester.CalculateTotalDays());
			return false;
		}

		tester.Minute = 60;
		tester.Hour = 11;
		tester.Day = 90;
		tester.Month = 10;
		tester.Year = 104; //Currently a leap year

		//There were 25 leap years within 104 years
		//(366 * 25) + (365 * 79) + (31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31) + 90 + 0.5
		if (tester.CalculateTotalDays() != 38380.5)
		{
			UnitTestError(testFlags, TXT("Calculate total days failed.  Total days for %s should be equal to 38380.5.  Instead it returned %s."), tester.ToIsoFormat(), tester.CalculateTotalDays());
			return false;
		}

		tester.Second = 45;
		tester.Minute = 11;
		tester.Hour = 39;
		tester.Day = 4928;
		tester.Month = 29;
		tester.Year = 1000;

		//There are 365 days within current year.  Ideally tester should be wrapped to consider the leap years buried within the excessive days.
		//1000 + (29/12) + (4928/365) + (39/[24*365]) + (11/[1440*365]) + (45/[86400*365])
		//1000 + 2.416667 + 13.501370 + 0.004452 + 0.000021 + 0.000001
		if (abs((tester.CalculateTotalYears() - 1015.922511f).Value) > 0.000001)
		{
			UnitTestError(testFlags, TXT("Calculate total years failed.  Total years for %s should be about 1015.922511.  Instead it returned %s."), tester.ToIsoFormat(), tester.CalculateTotalYears());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		DateTime now;
		DateTime arbitraryTime(2011, 11, 19, 8, 15, 49);
		now.SetToCurrentTime();
		DateTime readNow;
		DateTime readArbitraryTime;
		DataBuffer dateTimeBuffer;

		dateTimeBuffer << now;
		dateTimeBuffer << arbitraryTime;
		dateTimeBuffer >> readNow;
		dateTimeBuffer >> readArbitraryTime;

		if (now != readNow)
		{
			UnitTestError(testFlags, TXT("DateTime streaming test failed.  %s was pushed to a data buffer.  %s was pulled from that data buffer."), now.ToIsoFormat(), readNow.ToIsoFormat());
			return false;
		}

		if (arbitraryTime != readArbitraryTime)
		{
			UnitTestError(testFlags, TXT("DateTime streaming test failed.  The 2nd DateTime %s was pushed to a data buffer.  %s was pulled from that data buffer."), arbitraryTime.ToIsoFormat(), readArbitraryTime.ToIsoFormat());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		DateTime expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			DateTime actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("DateTime test failed. Parsing the string \"%s\" should have resulted in %s. Instead it's %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = DateTime(2015, 10, 25, 13, 6, 47);
		testStr = TXT("(Year = 2015, Month = 10, Day = 25, Hour = 13, Minute = 6, Second = 47)");
		if (!testParsing())
		{
			return false;
		}

		expected = DateTime(2021, 2, 12, 23, 52, 10); //The timestamp when this unit test was written.
		testStr = TXT("(Minute=52, Second=10, Day=12, Year=2021, Hour=23, Month=2)");
		if (!testParsing())
		{
			return false;
		}

		expected.SetToCurrentTime();
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	DateTime tester;
	tester.SetToCurrentTime();
	TestLog(testFlags, TXT("The local time when this unit test was conducted is:  %s."), tester);

	ExecuteSuccessSequence(testFlags, TXT("Date Time"));
	return true;
}

bool TimeUnitTester::LaunchLatentTest (EUnitTestFlags testFlags) const
{
	TestLog(testFlags, TXT("Launching latent test.  The entity will report errors later if one of its conditions fail."));

	//No need to hold a reference to the latent tester.  The entity will destroy itself.
	LatentTimerTester* tester = LatentTimerTester::CreateObject();
	tester->LaunchTest(testFlags, this);

	return true;
}
SD_END

#endif