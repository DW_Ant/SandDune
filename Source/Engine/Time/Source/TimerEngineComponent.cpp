/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimerEngineComponent.cpp
=====================================================================
*/

#include "TimerEngineComponent.h"
#include "TimerManager.h"

IMPLEMENT_ENGINE_COMPONENT(SD::TimerEngineComponent)
SD_BEGIN

void TimerEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	TimeManager = TimerManager::CreateObject();
}

void TimerEngineComponent::ShutdownComponent ()
{
	TimeManager->Destroy();

	Super::ShutdownComponent();
}
SD_END