/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DateTime.cpp
=====================================================================
*/

#include "DateTime.h"
#include "TimeUtils.h"

SD_BEGIN
DateTime::DateTime ()
{
	Year = 0;
	Month = 0;
	Day = 0;
	Hour = 0;
	Minute = 0;
	Second = 0;
}

DateTime::DateTime (const Int& inYear, const Int& inMonth, const Int& inDay, const Int& inHour, const Int& inMinute, const Int& inSecond)
{
	Year = inYear;
	Month = inMonth;
	Day = inDay;
	Hour = inHour;
	Minute = inMinute;
	Second = inSecond;
}

DateTime::DateTime (const DateTime& otherDateTime)
{
	Year = otherDateTime.Year;
	Month = otherDateTime.Month;
	Day = otherDateTime.Day;
	Hour = otherDateTime.Hour;
	Minute = otherDateTime.Minute;
	Second = otherDateTime.Second;
}

DateTime::~DateTime ()
{

}

void DateTime::operator= (const DateTime& copyDateTime)
{
	Year = copyDateTime.Year;
	Month = copyDateTime.Month;
	Day = copyDateTime.Day;
	Hour = copyDateTime.Hour;
	Minute = copyDateTime.Minute;
	Second = copyDateTime.Second;
}

void DateTime::ResetToDefaults ()
{
	Year = 0;
	Month = 0;
	Day = 0;
	Hour = 0;
	Minute = 0;
	Second = 0;
}

DString DateTime::ToString () const
{
	return DString::CreateFormattedString(TXT("(Year=%s, Month=%s, Day=%s, Hour=%s, Minute=%s, Second=%s)"), Year, Month, Day, Hour, Minute, Second);
}

void DateTime::ParseString (const DString& str)
{
	DString varData;
	std::function<bool(const DString&, Int&)> setVar([&](const DString& varName, Int& outVar)
	{
		varData = ParseVariable(str, varName, CommonPropertyRegex::Int);
		if (varData.IsEmpty())
		{
			*this = DateTime();
			return false;
		}

		outVar = Int(varData);
		return true;
	});

	if (!setVar(TXT("Year"), OUT Year))
	{
		return;
	}

	if (!setVar(TXT("Month"), OUT Month))
	{
		return;
	}

	if (!setVar(TXT("Day"), OUT Day))
	{
		return;
	}

	if (!setVar(TXT("Hour"), OUT Hour))
	{
		return;
	}

	if (!setVar(TXT("Minute"), OUT Minute))
	{
		return;
	}

	setVar(TXT("Second"), OUT Second);
}

size_t DateTime::GetMinBytes () const
{
	//Year, Month, Day, Hour, Minute, Second
	return (Int::SGetMinBytes() * 6);
}

void DateTime::Serialize (DataBuffer& outData) const
{
	outData << Year << Month << Day << Hour << Minute << Second;
}

bool DateTime::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Year >> Month >> Day >> Hour >> Minute >> Second;
	return !dataBuffer.HasReadError();
}

void DateTime::SetToCurrentTime ()
{
	time_t rawTime = time(0); //Get time from system
	struct tm timeInfo;
	localtime_s(&timeInfo, &rawTime);

	Year = timeInfo.tm_year + 1900; //tm_year starts since 1900
	Month = timeInfo.tm_mon; //Months range from 0-11, no need for an offset here
	Day = timeInfo.tm_mday - 1; //days range from 1-31, offset days by its range offset
	Hour = timeInfo.tm_hour;
	Minute = timeInfo.tm_min;
	Second = timeInfo.tm_sec;
}

DString DateTime::ToIsoFormat () const
{
	return FormatStringPadded(TXT("%year-%month-%day %hour:%minute:%second"));
}

DString DateTime::FormatString (const DString& format) const
{
	DString result = format;

	result.ReplaceInline(TXT("%year"), Year.ToString(), DString::CC_IgnoreCase);

	//When formatting date, we start from 1.  January is displayed as 1 (not 0).
	result.ReplaceInline(TXT("%month"), (Month + 1).ToString(), DString::CC_IgnoreCase);

	//When formatting time, we start from 0.  Midnight is displayed as 0 (not 1).
	result.ReplaceInline(TXT("%day"), (Day + 1).ToString(), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%hour"), Hour.ToString(), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%minute"), Minute.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%second"), Second.ToFormattedString(2), DString::CC_IgnoreCase);

	return result;
}

DString DateTime::FormatStringPadded (const DString& format) const
{
	//When formatting date, we start from 1.  January is displayed as 1 (not 0).
	//When formatting time, we start from 0.  Midnight is displayed as 0 (not 1).
	Int dayDisplay = Day + 1;
	Int monthDisplay = Month + 1;
	Int yearDisplay = Year;
	DString result = format;

	result.ReplaceInline(TXT("%year"), yearDisplay.ToFormattedString(4), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%month"), monthDisplay.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%day"), dayDisplay.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%hour"), Hour.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%minute"), Minute.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%second"), Second.ToFormattedString(2), DString::CC_IgnoreCase);

	return result;
}

void DateTime::WrapSeconds ()
{
	Int numMin = Second/60;

	Minute += numMin;
	Second -= numMin * 60;
}

void DateTime::WrapMinutes ()
{
	Int numHours = Minute/60;

	Hour += numHours;
	Minute -= numHours * 60;
}

void DateTime::WrapHours ()
{
	Int numDays = Hour/24;

	Day += numDays;
	Hour -= numDays * 24;
}

void DateTime::WrapDays ()
{
	if (Month < 0 || Month > 11)
	{
		//Must wrap months first to avoid infinite loop (NumDaysWithinMonths expects months to range from 0-11).
		WrapMonths();
	}

	while (true)
	{
		Int daysWithinMonth = TimeUtils::NumDaysWithinMonth(Month, Year);
		if (daysWithinMonth > Day)
		{
			break;
		}

		Day -= daysWithinMonth;
		if (++Month > 11)
		{
			//Wrap months since NumDaysWithinMonth expects Month to range from 0-11
			Month = 0;
			Year++;
		}
	}
}

void DateTime::WrapMonths ()
{
	Int numYears = Month/12;

	Year += numYears;
	Month -= numYears * 12;
}

void DateTime::WrapDateTime ()
{
	WrapSeconds();
	WrapMinutes();
	WrapHours();
	WrapDays();
	WrapMonths();
}

Float DateTime::CalculateTotalSeconds () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	Float totalDays = daysOnly.CalculateTotalDays();

	return Second.ToFloat() + (Minute.ToFloat() * 60) + (Hour.ToFloat() * 3600) + (totalDays * 86400);
}

Float DateTime::CalculateTotalMinutes () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	Float totalDays = daysOnly.CalculateTotalDays();

	return (Second.ToFloat()/60.f) + Minute.ToFloat() + (Hour.ToFloat() * 60.f) + (totalDays * 1440.f);
}

Float DateTime::CalculateTotalHours () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	Float totalDays = daysOnly.CalculateTotalDays();

	return (Second.ToFloat()/3600.f) + (Minute.ToFloat()/60.f) + Hour.ToFloat() + (totalDays * 24.f);
}

Float DateTime::CalculateTotalDays () const
{
	//Unwrap years to handle leap years
	Int yearsRemaining = Year;
	Int totalDays = Day;
	while (yearsRemaining > 0)
	{
		/*
		Note:  When calculating total days, only consider years prior to current year.
		If current year is 2015, only count years from 0-2014.
		*/
		if (TimeUtils::IsLeapYear(yearsRemaining - 1))
		{
			totalDays += 366;
		}
		else
		{
			totalDays += 365;
		}

		yearsRemaining--;
	}

	Int monthsRemaining = Month;
	while (monthsRemaining > 0)
	{
		/*
		note:  When calculating total days, only add months prior to the current month.
		If current Month is March, only add January and February.
		*/
		totalDays += TimeUtils::NumDaysWithinMonth(monthsRemaining - 1, Year);
		monthsRemaining--;
	}

	return (Second.ToFloat()/86400.f) + (Minute.ToFloat()/1440.f) + (Hour.ToFloat()/24.f) + totalDays.ToFloat();
}

Float DateTime::CalculateTotalMonths () const
{
	Float daysWithinCurMonth = TimeUtils::NumDaysWithinMonth(Month, Year).ToFloat();

	Float result = Second.ToFloat()/(86400.f * daysWithinCurMonth);
	result += Minute.ToFloat()/(1440.f * daysWithinCurMonth);
	result += Hour.ToFloat()/(24.f * daysWithinCurMonth);
	result += Day.ToFloat()/daysWithinCurMonth;
	result += Month.ToFloat();
	result += Year.ToFloat() * 12.f;

	return result;
}

Float DateTime::CalculateTotalYears () const
{
	Float daysWithinCurYear = (TimeUtils::IsLeapYear(Year)) ? 366.f : 365.f;

	Float result = Second.ToFloat()/(86400.f * daysWithinCurYear);
	result += Minute.ToFloat()/(1440.f * daysWithinCurYear);
	result += Hour.ToFloat()/(24.f * daysWithinCurYear);
	result += Day.ToFloat() / daysWithinCurYear;
	result += (Month.ToFloat()/12.f);
	result += Year.ToFloat();

	return result;
}

#pragma region "External Operators"
bool operator== (const DateTime& left, const DateTime& right)
{
	return (left.Second == right.Second && left.Minute == right.Minute && left.Hour == right.Hour &&
			left.Day == right.Day && left.Month == right.Month && left.Year == right.Year);
}

bool operator!= (const DateTime& left, const DateTime& right)
{
	return !(left == right);
}

DateTime operator+ (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year + right.Year, left.Month + right.Month, left.Day + right.Day, left.Hour + right.Hour, left.Minute + right.Minute, left.Second + right.Second);
}

DateTime& operator+= (DateTime& left, const DateTime& right)
{
	left.Year += right.Year;
	left.Month += right.Month;
	left.Day += right.Day;
	left.Hour += right.Hour;
	left.Minute += right.Minute;
	left.Second += right.Second;
	return left;
}

DateTime operator- (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year - right.Year, left.Month - right.Month, left.Day - right.Day, left.Hour - right.Hour, left.Minute - right.Minute, left.Second - right.Second);
}

DateTime& operator-= (DateTime& left, const DateTime& right)
{
	left.Year -= right.Year;
	left.Month -= right.Month;
	left.Day -= right.Day;
	left.Hour -= right.Hour;
	left.Minute -= right.Minute;
	left.Second -= right.Second;
	return left;
}

DateTime operator* (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year * right.Year, left.Month * right.Year, left.Day * right.Day, left.Hour * right.Hour, left.Minute * right.Minute, left.Second * right.Second);
}

DateTime& operator*= (DateTime& left, const DateTime& right)
{
	left.Year *= right.Year;
	left.Month *= right.Month;
	left.Day *= right.Day;
	left.Hour *= right.Hour;
	left.Minute *= right.Minute;
	left.Second *= right.Second;
	return left;
}

DateTime operator/ (const DateTime& left, const DateTime& right)
{
	DateTime result(left);
	result /= right;
	return result;
}

DateTime& operator/= (DateTime& left, const DateTime& right)
{
	if (right.Year != 0)
	{
		left.Year /= right.Year;
	}

	if (right.Hour != 0)
	{
		left.Hour /= right.Hour;
	}

	if (right.Day != 0)
	{
		left.Day /= right.Day;
	}

	if (right.Hour != 0)
	{
		left.Hour /= right.Hour;
	}

	if (right.Minute != 0)
	{
		left.Minute /= right.Minute;
	}

	if (right.Second != 0)
	{
		left.Second /= right.Second;
	}

	return left;
}
#pragma endregion
SD_END