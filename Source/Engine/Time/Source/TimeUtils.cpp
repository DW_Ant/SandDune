/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimeUtils.cpp
=====================================================================
*/

#include "TimeUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::TimeUtils, SD::BaseUtils)
SD_BEGIN

std::vector<TimeUtils::SMonthInfo> TimeUtils::MonthInfo = TimeUtils::PopulateMonthInfo();

Int TimeUtils::NumDaysWithinMonth (Int month, Int year)
{
	if (month < 0 || month > 11)
	{
		TimeLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid month.  There are only 12 months within a year.  Expected values range from 0 - 11."), month);
		return 0;
	}

	if (month == 1 && year > 0 && IsLeapYear(year))
	{
		return MonthInfo.at(month.Value).NumDays + 1;
	}

	return MonthInfo.at(month.Value).NumDays;
}

bool TimeUtils::IsLeapYear (Int year)
{
	//A leap year is divisable by 4 except for 100.  If divisable by 400, then it's a leap year regardless.
	if ((year % 100) == 0)
	{
		return ((year % 400) == 0);
	}

	return ((year % 4) == 0);
}

void TimeUtils::GetMonthInfo (Int monthNumber, SMonthInfo& outMonthInfo)
{
	if (monthNumber < 0 || monthNumber > 11)
	{
		TimeLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid month.  There are only 12 months within a year.  Expected values range from 0 - 11."), monthNumber);
		return;
	}

	UINT_TYPE index = monthNumber.ToUnsignedInt();
	outMonthInfo.NumDays = MonthInfo.at(index).NumDays;
	outMonthInfo.FullMonthName = MonthInfo.at(index).FullMonthName;
	outMonthInfo.MonthNameAbbreviated = MonthInfo.at(index).MonthNameAbbreviated;
}

std::vector<TimeUtils::SMonthInfo> TimeUtils::PopulateMonthInfo ()
{
	std::vector<SMonthInfo> results;
	SMonthInfo newMonthInfo;

	newMonthInfo.FullMonthName = TXT("January");
	newMonthInfo.MonthNameAbbreviated = TXT("Jan");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("Febuary");
	newMonthInfo.MonthNameAbbreviated = TXT("Feb");
	newMonthInfo.NumDays = 28;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("March");
	newMonthInfo.MonthNameAbbreviated = TXT("March");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("April");
	newMonthInfo.MonthNameAbbreviated = TXT("Apr");
	newMonthInfo.NumDays = 30;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("May");
	newMonthInfo.MonthNameAbbreviated = TXT("May");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("June");
	newMonthInfo.MonthNameAbbreviated = TXT("June");
	newMonthInfo.NumDays = 30;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("July");
	newMonthInfo.MonthNameAbbreviated = TXT("Jul");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("August");
	newMonthInfo.MonthNameAbbreviated = TXT("Aug");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("September");
	newMonthInfo.MonthNameAbbreviated = TXT("Sept");
	newMonthInfo.NumDays = 30;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("October");
	newMonthInfo.MonthNameAbbreviated = TXT("Oct");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("November");
	newMonthInfo.MonthNameAbbreviated = TXT("Nov");
	newMonthInfo.NumDays = 30;
	results.push_back(newMonthInfo);

	newMonthInfo.FullMonthName = TXT("December");
	newMonthInfo.MonthNameAbbreviated = TXT("Dec");
	newMonthInfo.NumDays = 31;
	results.push_back(newMonthInfo);

	return results;
}
SD_END