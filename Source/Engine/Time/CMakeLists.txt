#########################################################################
# Establish directories
#########################################################################
include_directories ("Headers"
	"${ExternalDirectory}/SFML/Include")

SET (TimeDirectory "${SourceDirectory}/Engine/Time")
SET (WorkingDirectory ${TimeDirectory})


#########################################################################
# Establish source files
#########################################################################

SET (HeaderFiles
	"Headers/DateTime.h"
	"Headers/LatentTimerTester.h"
	"Headers/SdTime.h"
	"Headers/TimerEngineComponent.h"
	"Headers/TimerManager.h"
	"Headers/TimeUnitTester.h"
	"Headers/TimeUtils.h"
)

SET (SourceFiles
	"Source/DateTime.cpp"
	"Source/LatentTimerTester.cpp"
	"Source/Time.cpp"
	"Source/TimerEngineComponent.cpp"
	"Source/TimerManager.cpp"
	"Source/TimeUnitTester.cpp"
	"Source/TimeUtils.cpp"
)


#########################################################################
# Auto Generate Include File
#########################################################################

#Convert header list to C++ include directives
SET (IncludeList ${HeaderFiles})
GenerateIncludeList(IncludeList)

#Set definitions used in generated header file.
SET (PUBLIC_INCLUDE_LIST ${IncludeList})
SET (MODULE_NAME "Time")
SET (PREPROCESSOR_MODULE_NAME "TIME")

#Configure the header file to pass some of the CMake settings to the source code.
configure_file (
	"${WorkingDirectory}/CMakeConfiguration/${MODULE_NAME}Classes.h.in"
	"${WorkingDirectory}/Headers/${MODULE_NAME}Classes.h"
)

#Add the generated header file to the file list
list (APPEND HeaderFiles "Headers/${MODULE_NAME}Classes.h")

	
#########################################################################
# Build the module
#########################################################################

SET (AllFiles ${HeaderFiles} ${SourceFiles})

#Generate project filters that align with the source file's directory location.
ConfigureProjectFilters("${AllFiles}")

add_compile_definitions(${PREPROCESSOR_MODULE_NAME}_EXPORT)

add_library(SD-${MODULE_NAME} SHARED ${AllFiles})
ApplyTargetConfiguration(SD-${MODULE_NAME})

set_target_properties (SD-${MODULE_NAME} PROPERTIES FOLDER "Engine")

AddSandDuneLibDependency(${MODULE_NAME} Core)