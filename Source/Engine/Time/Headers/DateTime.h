/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DateTime.h
  Data type responsible for storing time and the date.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TIME_API DateTime : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Year 0 is 0AD. */
	Int Year;

	/* The actual value range from 0-11 (when wrapped), but it's displayed as Month + 1 from FormatString. */
	Int Month;

	/* The actual value range from 0-30 (when wrapped), but it's displayed as Day + 1 from FormatString. */
	Int Day;
	Int Hour;
	Int Minute;
	Int Second;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DateTime ();
	DateTime (const Int& inYear, const Int& inMonth, const Int& inDay, const Int& inHour, const Int& inMinute, const Int& inSecond);
	DateTime (const DateTime& otherDateTime);
	virtual ~DateTime ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const DateTime& copyDateTime);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	/**
	 * Converts data type to string that'll display all values (used mostly for logging).
	 * See FormatString to customize appearance.
	 */
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets attributes to current system time.
	 */
	virtual void SetToCurrentTime ();

	/**
	 * Returns a string of this date time in iso format.
	 */
	virtual DString ToIsoFormat () const;

	/**
	 * Returns a formatted string where string macros are replaced with values.
	 * %year, %month, %day, %hour, %minute, %second
	 */
	virtual DString FormatString (const DString& format) const;

	/**
	 * Same as FormatString, but it pads the values with leading zeroes if the values don't have
	 * enough digits.  Years=4 digits, Months=2, Days=2, Hours=2, Minutes=2, Seconds=2.
	 */
	virtual DString FormatStringPadded (const DString& format) const;

	/**
	 * Increments 1 minute for every 60 seconds.
	 */
	virtual void WrapSeconds ();

	/**
	 * Increments 1 hour for every 60 minutes.
	 */
	virtual void WrapMinutes ();

	/**
	 * Increments 1 day for every 24 hours.
	 */
	virtual void WrapHours ();

	/**
	 * Increments 1 month for every 28-31 days (based on the year and current month).
	 */
	virtual void WrapDays ();

	/**
	 * Increments 1 year for every 12 months.
	 */
	virtual void WrapMonths ();

	/**
	 * Wraps seconds, minutes, hours, days, and months.
	 */
	virtual void WrapDateTime ();

	virtual Float CalculateTotalSeconds () const;
	virtual Float CalculateTotalMinutes () const;
	virtual Float CalculateTotalHours () const;
	virtual Float CalculateTotalDays () const;
	virtual Float CalculateTotalMonths () const;
	virtual Float CalculateTotalYears () const;
};

#pragma region "External Operators"
TIME_API bool operator== (const DateTime& left, const DateTime& right);
TIME_API bool operator!= (const DateTime& left, const DateTime& right);
TIME_API DateTime operator+ (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator+= (DateTime& left, const DateTime& right);
TIME_API DateTime operator- (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator-= (DateTime& left, const DateTime& right);
TIME_API DateTime operator* (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator*= (DateTime& left, const DateTime& right);
TIME_API DateTime operator/ (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator/= (DateTime& left, const DateTime& right);
#pragma endregion
SD_END