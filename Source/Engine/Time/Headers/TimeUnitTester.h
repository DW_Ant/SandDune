/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimeUnitTester.h
  Tests DateTime and time-related utilities.
=====================================================================
*/

#pragma once

#include "SdTime.h"

#ifdef DEBUG_MODE

SD_BEGIN
class TIME_API TimeUnitTester : public UnitTester
{
	DECLARE_CLASS(TimeUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestTimeUtils (EUnitTestFlags testFlags) const;
	virtual bool TestDateTime (EUnitTestFlags testFlags) const;

	/**
	 * Initializes the Latent Timer Tester entity to test latent functionality such
	 * as SetTimer and Tick Components.
	 */
	virtual bool LaunchLatentTest (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode