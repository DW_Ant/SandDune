/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimeUtils.h
  Abstract class containing generic utility functions related to date
  and time.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TIME_API TimeUtils : public BaseUtils
{
	DECLARE_CLASS(TimeUtils)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SMonthInfo
	{
		/* Number of days for this month (ignoring leap year). */
		Int NumDays;

		/* Short-hand version to write the month. */
		DString MonthNameAbbreviated;

		DString FullMonthName;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	static std::vector<SMonthInfo> MonthInfo;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns number of days within the given month.
	 * If a year is positive, then it considers if February has an additional day or not.
	 */
	static Int NumDaysWithinMonth (Int month, Int year = -1);

	/**
	 * Returns true if the given year is a leap year.
	 */
	static bool IsLeapYear (Int year);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static void GetMonthInfo (Int monthNumber, SMonthInfo& outMonthInfo);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Populates the month info vector with data.
	 */
	static std::vector<SMonthInfo> PopulateMonthInfo ();
};
SD_END