/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimerManager.h
  This class is responsible for tracking a list of timer functions,
  and invoke their events when their time expired.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TIME_API TimerManager : public Entity
{
	DECLARE_CLASS(TimerManager)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct STimerData
	{
		/* The object that'll handle the event.  This variable is used to check if it's safe to call the function without causing access violations. */
		const Object* OwningObject = nullptr;

		/* Callback to call when enough time elapsed. */
		SDFunction<void> EventHandler;

		/* If greater than 0, then this timer struct will reset its counter
		to this value every time this event handler was called. */
		Float TimeInterval = 0;

		Float TimeRemaining = 0;

		/* If true, then this Timer struct will be deleted at the beginning of next Tick. */
		bool bPendingDelete = false;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all registered timers waiting for a callback. */
	std::vector<STimerData> Timers;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers new timer instance to the vector of timers.
	 * Returns true if it replaced an existing entry (when functionCallback matches with another).
	 */
	virtual bool SetTimer (const Object* targetObject, Float timeDelay, bool bRepeat, const SDFunction<void>& functionCallback);

	/**
	 * Removes the function callback from the vector of timers.
	 * Returns true if the function was found and removed.
	 */
	virtual bool RemoveTimer (const SDFunction<void>& targetCallback);

	/**
	 * Returns time remaining of the registered targetCallback.
	 * This will return 0, if the targetCallback was not found.
	 */
	virtual Float GetTimeRemaining (const SDFunction<void>& targetCallback) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each timer and checks if they still hold valid references to OwningObject.
	 * If not, then the Timer is marked for deletion.
	 */
	virtual void EvaluateTimerObjects ();

	/**
	 * Returns true if the given Timer struct is valid.  Return false if it should be marked for deletion.
	 * Parameter is the index value of the Timers vector.
	 */
	virtual bool IsValidTimerData (UINT_TYPE timerIdx) const;

	/**
	 * Flushes PendingDeleteIndices to clean up the Timers vector.
	 */
	virtual void ApplyPendingDelete ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
	virtual void HandleGarbageCollection ();
};
SD_END