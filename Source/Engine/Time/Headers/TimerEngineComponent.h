/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimerEngineComponent.h
  This component doesn't do much other than owning the TimerManager.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TimerManager;

class TIME_API TimerEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(TimerEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<TimerManager> TimeManager;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	TimerManager* GetTimeManager () const
	{
		return TimeManager.Get();
	}
};
SD_END