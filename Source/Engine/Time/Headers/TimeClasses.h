/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TimeClasses.h
  Contains all header includes for the Time module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the TimeClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_TIME
#include "DateTime.h"
#include "LatentTimerTester.h"
#include "SdTime.h"
#include "TimerEngineComponent.h"
#include "TimerManager.h"
#include "TimeUnitTester.h"
#include "TimeUtils.h"

#endif
