/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SdTime.h
  Contains important file includes and definitions for the SD_Time module.

  The Time module contains utilities and data types related to time and
  date.

  This file was renamed to SdTime.h to avoid name clash with MS's time.h
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#include <time.h>

#ifdef PLATFORM_WINDOWS
	#ifdef TIME_EXPORT
		#define TIME_API __declspec(dllexport)
	#else
		#define TIME_API __declspec(dllimport)
	#endif
#else
	#define TIME_API
#endif

SD_BEGIN
extern TIME_API LogCategory TimeLog;
SD_END