/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LatentTimerTester.h
  This class is responsible for conducting unit tests various timer-
  related functionality such as the timer manager.
=====================================================================
*/

#pragma once

#include "SdTime.h"

#ifdef DEBUG_MODE

SD_BEGIN
class TIME_API LatentTimerTester : public Entity
{
	DECLARE_CLASS(LatentTimerTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	UnitTester::EUnitTestFlags TestFlags;
	DPointer<const UnitTester> OwningUnitTester;

	/* Counter incrementing every time an event is triggered. */
	Int NumEventsTriggered;

	/* How many times the repeated event should be triggered. */
	Int MaxNumRepeatedEvents;

	/* Interval at which the timer events are triggered. */
	Float EventInterval;

	/* Time at which this test began. */
	Float StartTime;

	/* Becomes true if this unit tester is about to be destroyed due to a failed test. */
	bool bFailedTest;

	/* Stopwatch to record how long this timer unit tester was running. */
	Stopwatch* UnitTestStopwatch;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void LaunchTest (UnitTester::EUnitTestFlags testFlags, const UnitTester* unitTesterOwner);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the test results to ensure that the events weren't triggered too many times.
	 */
	virtual void VerifyEvents ();

	virtual void RemoveStopwatch ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSingleEvent ();
	virtual void HandleRepeatedEvent ();

	/**
	 * Test event that should never be triggered (testing if the timer manager is able to abort callbacks).
	 */
	virtual void HandleFalseEvent ();

	/**
	 * Checks if the events are not taking too long to trigger.
	 */
	virtual void HandleTick (Float deltaSec);
};
SD_END

#endif //debug_mode