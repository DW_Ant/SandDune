/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Vector2.cpp
=====================================================================
*/

#include "CoreDatatypes.h"
#include "Vector2.h"

SD_BEGIN
const Vector2 Vector2::ZERO_VECTOR(0.0f, 0.0f);

Vector2::Vector2 ()
{

}

Vector2::Vector2 (const float x, const float y)
{
	X = x;
	Y = y;
}

Vector2::Vector2 (const Float x, const Float y)
{
	X = x;
	Y = y;
}

Vector2::Vector2 (const Vector2& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
}

void Vector2::operator= (const Vector2& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
}

Vector2 Vector2::operator- () const
{
	return Vector2(-X, -Y);
}

Float& Vector2::operator[] (UINT_TYPE axisIdx)
{
	CHECK(axisIdx <= 1)

	switch (axisIdx)
	{
		default:
		case(0): return X;
		case(1): return Y;
	}
}

const Float& Vector2::operator[] (UINT_TYPE axisIdx) const
{
	CHECK(axisIdx <= 1)

	switch (axisIdx)
	{
		default:
		case(0): return X;
		case(1): return Y;
	}
}

void Vector2::ResetToDefaults ()
{
	X = 0.f;
	Y = 0.f;
}

DString Vector2::ToString () const
{
	return (TXT("(X=") + X.ToString() + TXT(", Y=") + Y.ToString() + TXT(")"));
}

void Vector2::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("X"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Vector2::ZERO_VECTOR;
		return;
	}
	X = Float(varData);

	varData = ParseVariable(str, TXT("Y"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Vector2::ZERO_VECTOR;
		return;
	}
	Y = Float(varData);
}

size_t Vector2::GetMinBytes () const
{
	return Vector2::SGetMinBytes();
}

void Vector2::Serialize (DataBuffer& outData) const
{
	outData << X;
	outData << Y;
}

bool Vector2::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> X >> Y;
	return !dataBuffer.HasReadError();
}

Vector2 Vector2::SFMLtoSD (const sf::Vector2f sfVector)
{
	return Vector2(sfVector.x, sfVector.y);
}

sf::Vector2f Vector2::SDtoSFML (const Vector2 sdVector)
{
	return sf::Vector2f(sdVector.X.Value, sdVector.Y.Value);
}

size_t Vector2::SGetMinBytes ()
{
	return (sizeof(Float::Value) * 2);
}

bool Vector2::IsNearlyEqual (const Vector2& compareTo, Float tolerance) const
{
	return (Float::Abs(X - compareTo.X) <= tolerance && Float::Abs(Y - compareTo.Y) <= tolerance);
}

Float Vector2::VSize () const
{
	return (std::sqrtf(std::powf(X.Value, 2.f) + std::powf(Y.Value, 2.f)));
}

Float Vector2::CalcDistSquared () const
{
	return (std::powf(X.Value, 2.f) + std::powf(Y.Value, 2.f));
}

Float Vector2::CalcDistBetween (const Vector2& ptA, const Vector2& ptB)
{
	return std::sqrtf(CalcSquaredDistBetween(ptA, ptB).Value);
}

Float Vector2::CalcSquaredDistBetween (const Vector2& ptA, const Vector2& ptB)
{
	return (std::powf((ptA.X - ptB.X).Value, 2.f) + std::powf((ptA.Y - ptB.Y).Value, 2.f));
}

void Vector2::SetLengthTo (Float targetLength)
{
	NormalizeInline();

	X *= targetLength;
	Y *= targetLength;
}

Vector2 Vector2::Min (Vector2 input, const Vector2& min)
{
	input.MinInline(min);
	return input;
}

void Vector2::MinInline (const Vector2& min)
{
	X = Utils::Min(X, min.X);
	Y = Utils::Min(Y, min.Y);
}

Vector2 Vector2::Max (Vector2 input, const Vector2& max)
{
	input.MaxInline(max);
	return input;
}

void Vector2::MaxInline (const Vector2& max)
{
	X = Utils::Max(X, max.X);
	Y = Utils::Max(Y, max.Y);
}

Vector2 Vector2::Clamp (Vector2 input, const Vector2& min, const Vector2& max)
{
	input.ClampInline(min, max);
	return input;
}

void Vector2::ClampInline (const Vector2& min, const Vector2& max)
{
	X = Utils::Clamp(X, min.X, max.X);
	Y = Utils::Clamp(Y, min.Y, max.Y);
}

Float Vector2::Dot (const Vector2& otherVector) const
{
	return ((X * otherVector.X) + (Y * otherVector.Y)).Value;
}

Vector2 Vector2::CalcReflection (const Vector2& normal) const
{
	Vector2 invert(*this);
	invert *= -1.f;
	return (invert - (2.f * invert.Dot(normal) * normal));
}

void Vector2::NormalizeInline ()
{
	Float magnitude = CalcDistSquared();

	if (magnitude == 0.f)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to normalize a zero 2D vector!"));
#endif
	}
	else if (magnitude != 1.f) //If not already normalized
	{
		magnitude.PowInline(0.5f); //square root

		X /= magnitude;
		Y /= magnitude;
	}
}

Vector2 Vector2::Normalize (Vector2 vect)
{
	vect.NormalizeInline();
	return vect;
}

bool Vector2::IsUniformed () const
{
	return (X == Y);
}

bool Vector2::IsEmpty () const
{
	return (X == 0.f && Y == 0.f);
}

Vector3 Vector2::ToVector3 () const
{
	return Vector3(X, Y, 0);
}

#pragma region "External Operators"
bool operator== (const Vector2& left, const Vector2& right)
{
	return (left.X == right.X && left.Y == right.Y);
}

bool operator!= (const Vector2& left, const Vector2& right)
{
	return !(left == right);
}

Vector2 operator+ (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X + right.X, left.Y + right.Y);
}

Vector2 operator+ (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X + right.x, left.Y + right.y);
}

sf::Vector2f operator+ (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x + right.X.Value, left.y + right.Y.Value);
}

Vector2& operator+= (Vector2& left, const Vector2& right)
{
	left.X += right.X;
	left.Y += right.Y;
	return left;
}

Vector2& operator+= (Vector2& left, const sf::Vector2f& right)
{
	left.X += right.x;
	left.Y += right.y;
	return left;
}

sf::Vector2f& operator+= (sf::Vector2f& left, const Vector2& right)
{
	left.x += right.X.Value;
	left.y += right.Y.Value;
	return left;
}

Vector2 operator- (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X - right.X, left.Y - right.Y);
}

Vector2 operator- (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X - right.x, left.Y - right.y);
}

sf::Vector2f operator- (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x - right.X.Value, left.y - right.Y.Value);
}

Vector2& operator-= (Vector2& left, const Vector2& right)
{
	left.X -= right.X;
	left.Y -= right.Y;
	return left;
}

Vector2& operator-= (Vector2& left, const sf::Vector2f& right)
{
	left.X -= right.x;
	left.Y -= right.y;
	return left;
}

sf::Vector2f& operator-= (sf::Vector2f& left, const Vector2& right)
{
	left.x -= right.X.Value;
	left.y -= right.Y.Value;
	return left;
}

Vector2 operator* (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X * right.X, left.Y * right.Y);
}

Vector2 operator* (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X * right.x, left.Y * right.y);
}

sf::Vector2f operator* (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x * right.X.Value, left.y * right.Y.Value);
}

Vector2& operator*= (Vector2& left, const Vector2& right)
{
	left.X *= right.X;
	left.Y *= right.Y;
	return left;
}

Vector2& operator*= (Vector2& left, const sf::Vector2f& right)
{
	left.X *= right.x;
	left.Y *= right.y;
	return left;
}

sf::Vector2f& operator*= (sf::Vector2f& left, const Vector2& right)
{
	left.x *= right.X.Value;
	left.y *= right.Y.Value;
	return left;
}

Vector2 operator/ (const Vector2& left, const Vector2& right)
{
	Vector2 result(left);
	result /= right;
	return result;
}

Vector2 operator/ (const Vector2& left, const sf::Vector2f& right)
{
	Vector2 result(left);
	result /= right;
	return result;
}

sf::Vector2f operator/ (const sf::Vector2f& left, const Vector2& right)
{
	sf::Vector2f result(left);
	result /= right;
	return left;
}

Vector2& operator/= (Vector2& left, const Vector2& right)
{
	if (right.X != 0.f)
	{
		left.X /= right.X;
	}

	if (right.Y != 0.f)
	{
		left.Y /= right.Y;
	}

	return left;
}

Vector2& operator/= (Vector2& left, const sf::Vector2f& right)
{
	if (right.x != 0.f)
	{
		left.X /= right.x;
	}

	if (right.y != 0.f)
	{
		left.Y /= right.y;
	}

	return left;
}

sf::Vector2f& operator/= (sf::Vector2f& left, const Vector2& right)
{
	if (right.X != 0.f)
	{
		left.x /= right.X.Value;
	}

	if (right.Y != 0.f)
	{
		left.y /= right.Y.Value;
	}

	return left;
}
#pragma endregion
SD_END