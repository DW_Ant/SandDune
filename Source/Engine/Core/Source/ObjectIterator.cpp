/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjectIterator.cpp
=====================================================================
*/

#include "Engine.h"
#include "ObjectIterator.h"
#include "Utils.h"

SD_BEGIN
ObjectIterator::ObjectIterator ()
{
	LocalEngine = Engine::FindEngine();
	CHECK(LocalEngine != nullptr)

	TargetedHash = LocalEngine->GetObjectHashNumber();
	bMultiHashIDs = true; //Typically specifying searching through objects typically means that one wants to find everything.
	FindFirstObject();
}

ObjectIterator::ObjectIterator (unsigned int targetedHash, bool bUseMultiHashIDs)
{
	LocalEngine = Engine::FindEngine();
	CHECK(LocalEngine != nullptr)

	if (targetedHash != 0 && !Utils::IsPowerOf2(targetedHash))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Invalid object hash value (%s).  Object hash values must be in powers of 2.  Defaulting iterator to 0."), DString::MakeString(targetedHash));
		TargetedHash = 0;
	}
	else
	{
		TargetedHash = targetedHash;
	}

	bMultiHashIDs = bUseMultiHashIDs;
	FindFirstObject();
}

ObjectIterator::~ObjectIterator ()
{

}

void ObjectIterator::operator++ ()
{
	FindNextObject();
}

void ObjectIterator::operator++ (int)
{
	FindNextObject();
}

unsigned int ObjectIterator::GetTargetedHash () const
{
	return TargetedHash;
}

void ObjectIterator::FindFirstObject ()
{
	ObjTableIdx = CalcTableIdx();
	if (ObjTableIdx >= LocalEngine->ObjectHashTable.size())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Created an ObjectIterator with an invalid TargetedHash (%s).  Computing the table index returned %s.  The Engine's hash table size is %s"), DString::MakeString(TargetedHash), DString::MakeString(ObjTableIdx), DString::MakeString(LocalEngine->ObjectHashTable.size()));
		SelectedObject = nullptr;
	}
	else
	{
		SelectedObject = LocalEngine->ObjectHashTable.at(ObjTableIdx).LeadingObject;

		if (bMultiHashIDs)
		{
			while (SelectedObject.IsNullptr() && ++ObjTableIdx < LocalEngine->ObjectHashTable.size())
			{
				SelectedObject = LocalEngine->ObjectHashTable.at(ObjTableIdx).LeadingObject;
			}
		}
	}

	//Don't select invalid objects.  Find next valid object
	if (SelectedObject.IsValid() && SelectedObject->GetPendingDelete())
	{
		FindNextObject(); //Either select next available object in linked list or select nothing.
	}
}

unsigned int ObjectIterator::CalcTableIdx () const
{
	double result = log2(TargetedHash);
	return static_cast<unsigned int>(trunc(result));
}

void ObjectIterator::FindNextObject ()
{
	//Need to use a raw pointer since DPointers will not select bPendingDeleted Objects
	Object* nextObj = SelectedObject.Get();

	do
	{
		if (nextObj == nullptr)
		{
			break; //This iterator is done
		}

		nextObj = nextObj->NextObject; //Caution:  NextObject could have bPendingDelete to true.
		if (bMultiHashIDs)
		{
			//Reached the end of this table.  Iterate down the hash table
			while (nextObj == nullptr && ++ObjTableIdx < LocalEngine->ObjectHashTable.size())
			{
				nextObj = LocalEngine->ObjectHashTable.at(ObjTableIdx).LeadingObject;
			}
		}
	}
	while (!VALID_OBJECT(nextObj)); //Don't return pending delete objects.  Keep iterating until we ran out of objects or when we find an available object

	SelectedObject = nextObj;
}
SD_END