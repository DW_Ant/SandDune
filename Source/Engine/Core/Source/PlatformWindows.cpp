/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindows.cpp
=====================================================================
*/


#include "Core.h"
#include "Engine.h"
#include "LogCategory.h"
#include "PlatformWindows.h"

#ifdef PLATFORM_WINDOWS
#include <roapi.h>

SD_BEGIN
void InitializePlatform (bool bMainThread)
{
	//Initialize Windows Runtime on this current thread.
	HRESULT result = RoInitialize(RO_INIT_MULTITHREADED);
	switch (result)
	{
		case(S_FALSE):
			CoreLog.Log(LogCategory::LL_Verbose, TXT("RoInitialize is already called on this thread."));
			break;

		case(E_INVALIDARG):
			CoreLog.Log(LogCategory::LL_Critical, TXT("RO_INIT_MULTITHREADED is not a valid argument. Please correct the source."));
			break;

		case(E_OUTOFMEMORY):
			CoreLog.Log(LogCategory::LL_Critical, TXT("Not enough memory to initialize Windows Runtime."));
			break;
	}
}

void ShutdownPlatform (bool bMainThread)
{
	RoUninitialize();
}

void PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle)
{
#if USE_WIDE_STRINGS
	MessageBox(NULL, StrToLPCWSTR(windowMsg), StrToLPCWSTR(windowTitle), MB_ICONERROR);
#else
	MessageBox(NULL, StrToLPCSTR(windowMsg), StrToLPCSTR(windowTitle), MB_ICONERROR);
#endif
}

void OS_Sleep (Int milliseconds)
{
	CHECK(milliseconds > 0)
	Sleep(static_cast<DWORD>(milliseconds.Value));
}

bool OS_CopyToClipboard (const DString& copyContent)
{
	//This code is based on MSDN documentation about copying text to OS's clipboard
	if (!OpenClipboard(0))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to copy \"%s\" to clipboard since application was unable to open clipboard.  Error code:  %s"), copyContent, DString::MakeString(GetLastError()));
		return false;
	}

	EmptyClipboard();

	if (copyContent.IsEmpty())
	{
		//Copying nothing to the clipboard will do nothing more than clearing the clipboard
		CloseClipboard();
		return true;
	}

	size_t numBytes = copyContent.ReadString().size() + 1;
	TCHAR* cStr = new TCHAR[numBytes];
#if USE_WIDE_STRINGS
	wcscpy_s(cStr, numBytes, copyContent.ToCString());
#else
	errno_t errorNum = strcpy_s(cStr, numBytes, copyContent.ToCString());
	if (errorNum != 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to copy \"%s\" to clipboard. Error code: %s"), copyContent, Int(errorNum));
		delete[] cStr;
		CloseClipboard();
		return false;
	}
#endif

	size_t strSize = copyContent.ReadString().capacity() + sizeof(TCHAR); //include null terminator

	//Allocate global memory for the object
	HGLOBAL globalCopy = GlobalAlloc(GMEM_MOVEABLE, strSize);
	if (!globalCopy)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to copy \"%s\" to clipboard since application was unable to allocate global memory."), copyContent);
		delete[] cStr;
		CloseClipboard();
		return false;
	}

	//Lock the handle and copy the text to the buffer
	LPVOID lock = GlobalLock(globalCopy);
	memcpy(lock, cStr, strSize);
	GlobalUnlock(globalCopy);

	SetClipboardData(CF_TEXT, globalCopy);
	CloseClipboard();
	delete[] cStr;

	return true;
}

DString OS_PasteFromClipboard ()
{
	//This code is based on MSDN's documentation about retrieving text from OS's clipboard
	if (!IsClipboardFormatAvailable(CF_TEXT))
	{
		//Currently the clipboard does not contain plain text
		return TXT("");
	}

	if (!OpenClipboard(0))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to get content from clipboard since application was unable to open clipboard.  Error code:  %s"), DString::MakeString(GetLastError()));
		return TXT("");
	}

	HGLOBAL globalBuffer = GetClipboardData(CF_TEXT);
	if (!globalBuffer)
	{
		CloseClipboard();
		return TXT("");
	}

	TCHAR* text = static_cast<TCHAR*>(GlobalLock(globalBuffer));
	if (!text)
	{
		CloseClipboard();
		return TXT("");
	}

	DString result(text);

	GlobalUnlock(globalBuffer);
	CloseClipboard();

	return result;
}

void OS_BreakExecution ()
{
	if (IsDebuggerPresent())
	{
		DebugBreak();
	}
}
SD_END

#endif