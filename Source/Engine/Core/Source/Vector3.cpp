/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Vector3.cpp
=====================================================================
*/

#include "CoreDatatypes.h"
#include "Vector3.h"

SD_BEGIN
const Vector3 Vector3::ZERO_VECTOR(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::Right(1.f, 0.f, 0.f);
const Vector3 Vector3::Up(0.f, 1.f, 0.f);
const Vector3 Vector3::Forward(0.f, 0.f, 1.f);

Vector3::Vector3 ()
{

}

Vector3::Vector3 (const float x, const float y, const float z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3::Vector3 (const Float x, const Float y, const Float z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3::Vector3 (const Vector3& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
	Z = copyVector.Z;
}

void Vector3::operator= (const Vector3& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
	Z = copyVector.Z;
}

Vector3 Vector3::operator- () const
{
	return Vector3(-X, -Y, -Z);
}

Float& Vector3::operator[] (UINT_TYPE axisIdx)
{
	CHECK(axisIdx <= 2)

	switch (axisIdx)
	{
		default:
		case(0): return X;
		case(1): return Y;
		case(2): return Z;
	}
}

const Float& Vector3::operator[] (UINT_TYPE axisIdx) const
{
	CHECK(axisIdx <= 2)

	switch (axisIdx)
	{
		default:
		case(0): return X;
		case(1): return Y;
		case(2): return Z;
	}
}

void Vector3::ResetToDefaults ()
{
	X = 0.f;
	Y = 0.f;
	Z = 0.f;
}

DString Vector3::ToString () const
{
	return (TXT("(X=") + X.ToString() + TXT(", Y=") + Y.ToString() + TXT(", Z=") + Z.ToString() + TXT(")"));
}

void Vector3::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("X"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Vector3::ZERO_VECTOR;
		return;
	}
	X = Float(varData);

	varData = ParseVariable(str, TXT("Y"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Vector3::ZERO_VECTOR;
		return;
	}
	Y = Float(varData);

	varData = ParseVariable(str, TXT("Z"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Vector3::ZERO_VECTOR;
		return;
	}
	Z = Float(varData);
}

size_t Vector3::GetMinBytes () const
{
	return Vector3::SGetMinBytes();
}

void Vector3::Serialize (DataBuffer& outData) const
{
	outData << X;
	outData << Y;
	outData << Z;
}

bool Vector3::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> X >> Y >> Z;
	return !dataBuffer.HasReadError();
}

Vector3 Vector3::SFMLtoSD (const sf::Vector3f sfVector)
{
	return Vector3(sfVector.x, sfVector.y, sfVector.z);
}

sf::Vector3f Vector3::SDtoSFML (const Vector3 sdVector)
{
	return sf::Vector3f(sdVector.X.Value, sdVector.Y.Value, sdVector.Z.Value);
}

size_t Vector3::SGetMinBytes ()
{
	return (sizeof(Float::Value) * 3);
}

bool Vector3::IsNearlyEqual (const Vector3& compareTo, Float tolerance) const
{
	return (Float::Abs(X - compareTo.X) <= tolerance &&
		Float::Abs(Y - compareTo.Y) <= tolerance &&
		Float::Abs(Z - compareTo.Z) <= tolerance);
}

Float Vector3::VSize (bool bIncludeZ) const
{
	if (bIncludeZ)
	{
		return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2) + pow(Z.Value, 2)));
	}
	else
	{
		return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2)));
	}
}

Float Vector3::CalcDistSquared (bool bIncludeZ) const
{
	if (bIncludeZ)
	{
		return pow(X.Value, 2) + pow(Y.Value, 2) + pow(Z.Value, 2);
	}
	else
	{
		return pow(X.Value, 2) + pow(Y.Value, 2);
	}
}

void Vector3::SetLengthTo (Float targetLength)
{
	NormalizeInline();

	X *= targetLength;
	Y *= targetLength;
	Z *= targetLength;
}

Vector3 Vector3::Min (Vector3 input, const Vector3& min)
{
	input.MinInline(min);
	return input;
}

void Vector3::MinInline (const Vector3& min)
{
	X = Utils::Min(X, min.X);
	Y = Utils::Min(Y, min.Y);
	Z = Utils::Min(Z, min.Z);
}

Vector3 Vector3::Max (Vector3 input, const Vector3& max)
{
	input.MaxInline(max);
	return input;
}

void Vector3::MaxInline (const Vector3& max)
{
	X = Utils::Max(X, max.X);
	Y = Utils::Max(Y, max.Y);
	Z = Utils::Max(Z, max.Z);
}

Vector3 Vector3::Clamp (Vector3 input, const Vector3& min, const Vector3& max)
{
	input.ClampInline(min, max);
	return input;
}

void Vector3::ClampInline (const Vector3& min, const Vector3& max)
{
	X = Utils::Clamp(X, min.X, max.X);
	Y = Utils::Clamp(Y, min.Y, max.Y);
	Z = Utils::Clamp(Z, min.Z, max.Z);
}

Float Vector3::Dot (const Vector3& otherVector) const
{
	return ((X * otherVector.X) + (Y * otherVector.Y) + (Z * otherVector.Z)).Value;
}

Vector3 Vector3::CrossProduct (const Vector3& otherVector) const
{
	Vector3 result;

	result.X = (Y * otherVector.Z) - (Z * otherVector.Y);
	result.Y = (Z * otherVector.X) - (X * otherVector.Z);
	result.Z = (X * otherVector.Y) - (Y * otherVector.X);

	return result;
}

Vector3 Vector3::CalcReflection (const Vector3& normal) const
{
	Vector3 invert(*this);
	invert *= -1.f;
	return (invert - (2.f * invert.Dot(normal) * normal));
}

void Vector3::NormalizeInline ()
{
	Float magnitude = CalcDistSquared();

	if (magnitude == 0.f)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to normalize a zero 3D vector!"));
#endif
	}
	else if (magnitude != 1.f) //If not already normalized
	{
		magnitude.PowInline(0.5f); //square root

		X /= magnitude;
		Y /= magnitude;
		Z /= magnitude;
	}
}

Vector3 Vector3::Normalize (Vector3 vect)
{
	vect.NormalizeInline();
	return vect;
}

bool Vector3::IsUniformed () const
{
	return (X == Y && X == Z);
}

bool Vector3::IsEmpty () const
{
	return (X == 0.f && Y == 0.f && Z == 0.f);
}

Vector2 Vector3::ToVector2 () const
{
	return Vector2(X, Y);
}

#pragma region "External Operators"
bool operator== (const Vector3& left, const Vector3& right)
{
	return (left.X == right.X && left.Y == right.Y && left.Z == right.Z);
}

bool operator!= (const Vector3& left, const Vector3& right)
{
	return !(left == right);
}

Vector3 operator+ (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
}

Vector3& operator+= (Vector3& left, const Vector3& right)
{
	left.X += right.X;
	left.Y += right.Y;
	left.Z += right.Z;
	return left;
}

Vector3 operator- (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
}

Vector3& operator-= (Vector3& left, const Vector3& right)
{
	left.X -= right.X;
	left.Y -= right.Y;
	left.Z -= right.Z;
	return left;
}

Vector3 operator* (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X * right.X, left.Y * right.Y, left.Z * right.Z);
}

Vector3& operator*= (Vector3& left, const Vector3& right)
{
	left.X *= right.X;
	left.Y *= right.Y;
	left.Z *= right.Z;
	return left;
}

Vector3 operator/ (const Vector3& left, const Vector3& right)
{
	Vector3 result(left);
	result /= right;
	return result;
}

Vector3& operator/= (Vector3& left, const Vector3& right)
{
	if (right.X != 0.f)
	{
		left.X /= right.X;
	}

	if (right.Y != 0.f)
	{
		left.Y /= right.Y;
	}

	if (right.Z != 0.f)
	{
		left.Z /= right.Z;
	}

	return left;
}
#pragma endregion
SD_END