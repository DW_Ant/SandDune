/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Engine.cpp
=====================================================================
*/

#include "DClassAssembler.h"
#include "ClassIterator.h"
#include "ContainerUtils.h"
#include "Engine.h"
#include "EngineComponent.h"
#include "ObjectIterator.h"
#include "TickComponent.h"
#include "TickGroup.h"
#include "StringIterator.h"
#include "Utils.h"

SD_BEGIN
const size_t Engine::MAIN_ENGINE_IDX = 0;
//Index 1 is reserved for the Sound Thread.
//Index 3 is reserved for the Editor Worker Thread.
//NOTE: If your application is not using the engine component that uses those indices, then it's safe to use those indices for your application.
const size_t Engine::DEBUG_ENGINE_IDX = (Engine::GetMaxNumPossibleEngines() - 1).ToUnsignedInt();
Engine* Engine::EngineInstances[8];
std::vector<DString> Engine::TickGroupNames;

Engine::Engine ()
{
	ThreadID = std::this_thread::get_id();
	CommandLineArgs = DString::EmptyString;

#ifdef DEBUG_MODE
	DebugName = TXT("Engine");
#endif

	PrevTickTime = clock();
	StartTime = clock();
	MaxDeltaTime = 0.2f; //Minimum 5 frames per second.
	//MinDeltaTime = -1.f; //Disable max frame rate.
	//MinDeltaTime = 0.0333333f; //~30 fps
	MinDeltaTime = 0.015f; //~60 fps
	ElapsedTime = 0.f;
	FrameCounter = 0;
	GarbageCollectInterval = 30.f;
	bFinishedInit = false;
	PreviousGarbageCollectTime = 0.f;

	ObjectHashNumber = 0;
	EntityHashNumber = 0;
	ComponentHashNumber = 0;
	TickComponentHashNumber = 0;

	bShuttingDown = false;
}

Engine::~Engine ()
{
	//Everything should have been cleaned up via ShutdownSandDune()
}

bool Engine::IsMainThread ()
{
	//Ensure this method isn't called before the Main Engine is initialized.
	CHECK(EngineInstances[0] != nullptr)

	return (std::this_thread::get_id() == EngineInstances[0]->GetThreadID());
}

void Engine::InitializeEngine (size_t engineIdx, const std::vector<EngineComponent*>& engineComponents)
{
	//Ensure this engine wasn't initialized yet
	if (EngineInstances[engineIdx] == this)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempted to initialize an engine that's already initialized."));
		return;
	}

	//Ensure the EngineInstance slot is available.
	if (EngineInstances[engineIdx] != nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Conflict in engine indices.  The index %s is already in use."), Int(engineIdx));
		return;
	}

	EngineInstances[engineIdx] = this;
	EngineIndex = engineIdx;

#ifdef DEBUG_MODE
	if (IsMainEngine())
	{
		DebugName = TXT("Main Engine");
	}
#endif

	InitializePlatform(IsMainEngine()); //Initialize platform-specific functionality

	for (EngineComponent* curComp : engineComponents)
	{
		RegisterEngineComponent(curComp);
	}

	ObjectHashNumber = RegisterObjectHash(TXT("Object"));
	EntityHashNumber = RegisterObjectHash(TXT("Entity"));
	ComponentHashNumber = RegisterObjectHash(TXT("Entity Component"));
	TickComponentHashNumber = RegisterObjectHash(TXT("Tick Component"));

	//Give engine components an opportunity to reserve partitions of the object hash table for object groupings.
	for (size_t i = 0; i < EngineComponents.size() && ObjectHashTable.size() < 31; i++)
	{
		EngineComponents.at(i)->RegisterObjectHash();
	}

	//Initialize Core Tick Groups
	CreateTickGroup(TICK_GROUP_SYSTEM, TICK_GROUP_PRIORITY_SYSTEM);
	CreateTickGroup(TICK_GROUP_MISC, TICK_GROUP_PRIORITY_MISC);
#ifdef DEBUG_MODE
	CreateTickGroup(TICK_GROUP_DEBUG, TICK_GROUP_PRIORITY_DEBUG);
#endif

	InitializeEngineComponents();

	if (IsMainEngine())
	{
		//Invoke PostEngineInitialize upon all objects
		for (ClassIterator iter(Object::SStaticClass()); iter.SelectedClass; iter++)
		{
			const Object* curObject = static_cast<const Object*>(iter.SelectedClass->GetDefaultObject());

			if (curObject != nullptr)
			{
				curObject->PostEngineInitialize();
			}
		}
	}

	bFinishedInit = true;
}

void Engine::SetCommandLineArgs (const DString& newCommandLineArgs)
{
	CommandLineArgs = newCommandLineArgs;

	ContainerUtils::Empty(OUT ParsedCmdLineArgs);

	//Split command line into spaces with the exception of those inside quotes.
	bool bInsideQuotes = false;
	DString parsedString(DString::EmptyString);
	for (StringIterator iter(&CommandLineArgs, true); !iter.IsAtEnd(); ++iter)
	{
		DString selectedStr = iter.GetString();
		if (selectedStr == TXT(" ") && !bInsideQuotes)
		{
			ParsedCmdLineArgs.push_back(parsedString);
			parsedString = DString::EmptyString;
		}
		else if (selectedStr == TXT("\""))
		{
			bInsideQuotes = !bInsideQuotes;
		}
		else if (bInsideQuotes || selectedStr != TXT(" ")) //Ignore spaces not inside quotes
		{
			parsedString += selectedStr;
		}
	}

	if (!parsedString.IsEmpty())
	{
		ParsedCmdLineArgs.push_back(parsedString);
	}
}

bool Engine::HasCmdLineSwitch (const DString& cmdArgSwitch, DString::ECaseComparison caseComparison) const
{
	for (const DString& str : ParsedCmdLineArgs)
	{
		if (str.FindAt(0) == TXT("-"))
		{
			if (str.Compare(cmdArgSwitch, caseComparison) == 0)
			{
				return true;
			}
		}
	}

	return false;
}

DString Engine::GetCmdLineValue (const DString& cmdArgKey, DString::ECaseComparison caseComparison) const
{
	for (const DString& str : ParsedCmdLineArgs)
	{
		Int equalPos = str.Find(TXT("="), 1, DString::CC_CaseSensitive);
		if (equalPos > 1 && equalPos < str.Length() - 1) //The equal character must be inside the string instead of the edges of it.
		{
			DString key = str.SubString(0, equalPos - 1);
			if (key.Compare(cmdArgKey, caseComparison) == 0)
			{
				return str.SubString(equalPos + 1);
			}
		}
	}

	return DString::EmptyString;
}

unsigned int Engine::RegisterObjectHash (const DString& friendlyName)
{
	if (ObjectHashTable.size() >= 31)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to register a portion of the object hash table for %s since the max size for the table is 31."), friendlyName);
		return UINT32_INDEX_NONE;
	}

	SHashTableMeta newEntry;
	newEntry.FriendlyName = friendlyName;
	newEntry.HashNumber = 1 << ObjectHashTable.size();
	ObjectHashTable.push_back(newEntry);

	return newEntry.HashNumber;
}

bool Engine::IsMainEngine () const
{
	return (EngineInstances[0] == this);
}

void Engine::FatalError (const DString& errorMsg)
{
	CoreLog.Log(LogCategory::LL_Fatal, errorMsg);

	OS_BreakExecution();

	DString dialogMsg = GenerateFatalErrorMsg(errorMsg);
	PlatformOpenWindow(dialogMsg, TXT("Fatal Error!"));
	ShutdownMainEngine();
}

void Engine::RegisterPreGarbageCollectEvent (SDFunction<void> newHandler)
{
	PreGarbageCollectionEvents.push_back(newHandler);
}

void Engine::RemovePreGarbageCollectEvent (SDFunction<void> oldHandler)
{
	for (size_t i = 0; i < PreGarbageCollectionEvents.size(); i++)
	{
		if (oldHandler == PreGarbageCollectionEvents.at(i))
		{
			PreGarbageCollectionEvents.erase(PreGarbageCollectionEvents.begin() + i);
			return;
		}
	}
}

void Engine::CollectGarbage ()
{
	if (!IsShuttingDown())
	{
		CoreLog.Log(LogCategory::LL_Verbose, TXT("Running garbage collector."));
	}

	Int numObjectsDeleted = 0;

	for (size_t i = 0; i < PreGarbageCollectionEvents.size(); i++)
	{
		PreGarbageCollectionEvents.at(i).Execute();
	}

	PreviousGarbageCollectTime = ElapsedTime;
	for (size_t i = 0; i < ObjectHashTable.size(); i++)
	{
		Object* prevObject = nullptr;
		Object* curObject = ObjectHashTable.at(i).LeadingObject;

		while (curObject != nullptr)
		{
			Object* nextObject = curObject->NextObject;
			if (curObject->bPendingDelete)
			{
				//Preserve the object linked list
				if (ObjectHashTable.at(i).LeadingObject == curObject) //current object is leading the hash table
				{
					ObjectHashTable.at(i).LeadingObject = nullptr; //failsafe incase no objects are available

					//find the next available object to fill in the object hash table's initial slot
					for (Object* newHashLeader = curObject->NextObject; newHashLeader != nullptr; newHashLeader = newHashLeader->NextObject)
					{
						if (!newHashLeader->bPendingDelete)
						{
							//Found an available object!
							ObjectHashTable.at(i).LeadingObject = newHashLeader;
							break;
						}
					}
				}
				else if (prevObject != nullptr) //Object in middle of linked list
				{
					prevObject->NextObject = nextObject;
				}

				++numObjectsDeleted;
				curObject->NextObject = nullptr; //Indicate that this object was successfully removed from the linked list. Otherwise the destructor would throw an assertion.
				delete curObject;
			}
			else //current object is not PendingDelete
			{
				prevObject = curObject;
			}

			curObject = nextObject;
		}
	}

	if (!IsShuttingDown())
	{
		CoreLog.Log(LogCategory::LL_Verbose, TXT("Garbage collector deleted %s objects."), numObjectsDeleted);
	}
}

void Engine::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& msg)
{
	DString formattedMsg = msg;
	if (formattedMsg.Length() <= 0 || formattedMsg.At(formattedMsg.Length() - 1) != TEXT('\n'))
	{
		formattedMsg += '\n';
	}

	for (EngineComponent* component : EngineComponents)
	{
		component->FormatLog(category, logLevel, formattedMsg);
	}

	for (EngineComponent* component : EngineComponents)
	{
		component->ProcessLog(category, logLevel, formattedMsg);
	}

	if ((category->GetUsageFlags() & LogCategory::FLAG_STANDARD_OUTPUT) > 0)
	{
		//output message to standard output stream
		std::cout << formattedMsg.ToCString();
	}

	//OS-specific output
#ifdef PLATFORM_WINDOWS
	if ((category->GetUsageFlags() & LogCategory::FLAG_OS_OUTPUT) > 0)
	{
		// Instruct Visual Studio to print out message and
		// any external program listening to this message such as DebugView will print this, too.
		OutputDebugStringW(formattedMsg.ToWideStringInUTF16().c_str());
	}
#endif //windows
}

#ifdef DEBUG_MODE
void Engine::LogObjectHashTable ()
{
	Int objectCounter = 0;

	CoreLog.Log(LogCategory::LL_Debug, TXT("-=== Logging Hash Table ===-"));
	for (size_t i = 0; i < ObjectHashTable.size(); i++)
	{
		Int hashCounter = 0;
		CoreLog.Log(LogCategory::LL_Debug, TXT("Hash Entry[%s]:  %s (%s)"), Int(i), ObjectHashTable.at(i).FriendlyName, DString::MakeString(ObjectHashTable.at(i).HashNumber));
		for (ObjectIterator iter((1 << i)); iter.SelectedObject; iter++)
		{
			objectCounter++;
			hashCounter++;
			CoreLog.Log(LogCategory::LL_Debug, TXT("    %s.  %s"), hashCounter, iter.SelectedObject->ToString());
		}

		CoreLog.Log(LogCategory::LL_Debug, TXT(""));
	}

	CoreLog.Log(LogCategory::LL_Debug, TXT("Size of ObjectHashTable:  %s"), DString::MakeString(ObjectHashTable.size()));
	CoreLog.Log(LogCategory::LL_Debug, TXT("Total number of Objects in hash table:  %s"), objectCounter);
	CoreLog.Log(LogCategory::LL_Debug, TXT("-=== End Hash Table Logs ===-"));
}
#endif

void Engine::CreateTickGroup (const DString& tickGroupName, Int tickPriority)
{
	size_t tickGroupIdx = TickGroups.size();
	for (size_t i = 0; i < TickGroups.size(); ++i)
	{
		if (TickGroups.at(i)->GetGroupName().Compare(tickGroupName, DString::CC_CaseSensitive) == 0)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Tick Group \"%s\" is already created in Engine's tick cycle.  Rejecting duplicate Tick Group instantiation request."));
			return;
		}

		if (TickGroups.at(i)->GetTickPriority() < tickPriority)
		{
			tickGroupIdx = i;
			break;
		}
	}

	TickGroups.insert(TickGroups.begin() + tickGroupIdx, new TickGroup(tickGroupName, tickPriority));
}

void Engine::Tick ()
{
	//calculate deltaTime since last tick
	clock_t curTime = clock();
	Float deltaSec = static_cast<float>(curTime - PrevTickTime)/CLOCKS_PER_SEC;
	if (MinDeltaTime > 0 && deltaSec < MinDeltaTime)
	{
		//Pause a bit to meet minimum delta time.
		Float sleepTime = ((MinDeltaTime - deltaSec));
		Int sleepTimeMillisec = (sleepTime * 1000.f).ToInt();
		if (sleepTimeMillisec > 0)
		{
			OS_Sleep(sleepTimeMillisec);
			deltaSec += sleepTime;
		}
	}

	PrevTickTime = clock();
	if (MaxDeltaTime > 0)
	{
		deltaSec = Utils::Min(deltaSec, MaxDeltaTime);
	}

	ElapsedTime += deltaSec;
	FrameCounter++;

	for (size_t i = 0; i < TickingEngineComponents.size(); i++)
	{
		TickingEngineComponents.at(i)->PreTick(deltaSec);
	}

	//Update all TickGroups and their registered TickComponents
	for (TickGroup* tickGroup : TickGroups)
	{
		tickGroup->TickRegisteredComponents(deltaSec);
	}

#ifdef DEBUG_MODE
	for (ObjectIterator iter(EntityHashNumber, true); iter.SelectedObject; iter++)
	{
		if (Entity* curEntity = dynamic_cast<Entity*>(iter.SelectedObject.Get()))
		{
			curEntity->DebugTick(deltaSec);
		}
	}
#endif

	for (size_t i = 0; i < TickingEngineComponents.size(); i++)
	{
		TickingEngineComponents.at(i)->PostTick(deltaSec);
	}

	//Check if the garbage collector needs to run
	if (ElapsedTime - PreviousGarbageCollectTime > GarbageCollectInterval)
	{
		CollectGarbage();
	}

	if (IsShuttingDown())
	{
		ShutdownSandDune();
	}
}

TickGroup* Engine::FindTickGroup (const DString& groupName) const
{
	for (TickGroup* group : TickGroups)
	{
		if (group->ReadGroupName().Compare(groupName, DString::CC_CaseSensitive) == 0)
		{
			return group;
		}
	}

	return nullptr;
}

#ifdef DEBUG_MODE
void Engine::SetDebugName (const DString& newDebugName)
{
	DebugName = newDebugName;
}
#endif

void Engine::SetMinDeltaTime (Float newMinDeltaTime)
{
	MinDeltaTime = newMinDeltaTime;
}

void Engine::SetMaxDeltaTime (Float newMaxDeltaTime)
{
	MaxDeltaTime = newMaxDeltaTime;
}

bool Engine::AddEngineComponent (EngineComponent* newComponent)
{
	if (newComponent == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot add a null engine component!"));
		return false;
	}

	//First ensure the component doesn't exist already
	for (size_t i = 0; i < EngineComponents.size(); ++i)
	{
		if (EngineComponents.at(i)->StaticClass() == newComponent->StaticClass())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to add engine component %s since the component is already registered to the Engine."), newComponent->GetName());
			return false;
		}
	}

	//Execute the initialization sequence synchronously. This function assumes all dependencies are already set.
	RegisterEngineComponent(newComponent);
	newComponent->RegisterObjectHash();
	newComponent->PreInitializeComponent();
	newComponent->InitializeComponent();
	newComponent->PostInitializeComponent();
	return true;
}

void Engine::RemoveEngineComponent (const DClass* engineClass)
{
	for (size_t i = 0; i < EngineComponents.size(); i++)
	{
		if (EngineComponents.at(i)->StaticClass() == engineClass)
		{
			EngineComponent* targetComponent = EngineComponents.at(i);
			if (targetComponent->GetTickingComponent())
			{
				//Find and remove this component from the TickingComponents vector
				for (size_t j = 0; j < TickingEngineComponents.size(); j++)
				{
					if (TickingEngineComponents.at(j) == targetComponent)
					{
						TickingEngineComponents.erase(TickingEngineComponents.begin() + j);
						break;
					}
				}
			}

			EngineComponents.at(i)->ShutdownComponent();
			delete EngineComponents.at(i);
			EngineComponents.erase(EngineComponents.begin() + i);

			return;
		}
	}
}

void Engine::Shutdown ()
{
	bShuttingDown = true;
}

void Engine::ShutdownMainEngine ()
{
	Engine::GetEngine(MAIN_ENGINE_IDX)->Shutdown();
}

bool Engine::IsShuttingDown () const
{
	return bShuttingDown;
}

Engine* Engine::GetEngine (size_t engineIdx)
{
	CHECK(engineIdx >= 0 && engineIdx < 8)

	return EngineInstances[engineIdx];
}

Engine* Engine::FindEngine ()
{
	std::thread::id localThreadId = std::this_thread::get_id();
	return FindEngine(localThreadId);
}

Engine* Engine::FindEngine (std::thread::id threadID)
{
	for (Engine* engine : EngineInstances)
	{
		if (engine != nullptr && engine->ThreadID == threadID)
		{
			return engine;
		}
	}

	return nullptr;
}

std::thread::id Engine::GetThreadID () const
{
	return ThreadID;
}

Float Engine::GetElapsedTime () const
{
	return ElapsedTime;
}

const clock_t& Engine::GetStartTime () const
{
	return StartTime;
}

size_t Engine::GetFrameCounter () const
{
	return FrameCounter;
}

Float Engine::GetMinDeltaTime () const
{
	return MinDeltaTime;
}

Float Engine::GetMaxDeltaTime () const
{
	return MaxDeltaTime;
}

unsigned int Engine::GetObjectHashNumber () const
{
	return ObjectHashNumber;
}

unsigned int Engine::GetEntityHashNumber () const
{
	return EntityHashNumber;
}

unsigned int Engine::GetComponentHashNumber () const
{
	return ComponentHashNumber;
}

unsigned int Engine::GetTickComponentHashNumber () const
{
	return TickComponentHashNumber;
}

void Engine::RegisterEngineComponent (EngineComponent* newComponent)
{
	EngineComponents.push_back(newComponent);
	newComponent->SetOwningEngine(this);

	if (newComponent->GetTickingComponent())
	{
		TickingEngineComponents.push_back(newComponent);
	}
}

void Engine::InitializeEngineComponents ()
{
	std::vector<const DClass*> executedEngineComponents;
	std::vector<EngineComponent*> componentsToExecute = EngineComponents;

	//PreInitializeComponents
	while (componentsToExecute.size() > 0)
	{
		size_t oldCompSize = componentsToExecute.size();
		size_t i = 0;
		while (i < componentsToExecute.size())
		{
			if (componentsToExecute.at(i)->CanRunPreInitializeComponent(executedEngineComponents))
			{
				componentsToExecute.at(i)->PreInitializeComponent();
				executedEngineComponents.push_back(componentsToExecute.at(i)->StaticClass());
				componentsToExecute.erase(componentsToExecute.begin() + i);
				continue;
			}

			i++;
		}

		//Each iteration must execute at least one engine component, otherwise it's assumed that there's a circular dependency.
		if (componentsToExecute.size() == oldCompSize)
		{
			CoreLog.Log(LogCategory::LL_Fatal, TXT("Failed to pre initialize engine components.  A circular dependency was detected amongst the following engine components."));
			for (size_t i = 0; i < componentsToExecute.size(); i++)
			{
				CoreLog.Log(LogCategory::LL_Fatal, TXT("    %s"), componentsToExecute.at(i)->StaticClass()->GetDuneClassName());
			}
			FatalError(TXT("There's a circular dependency amongst the registered engine components' PreInitializeComponent.  See the logs for details."));
			return;
		}
	}
	//Reset for InitializeComponent
	componentsToExecute = EngineComponents;
	executedEngineComponents.clear();

	//InitializeComponents
	while (componentsToExecute.size() > 0)
	{
		size_t oldCompSize = componentsToExecute.size();
		size_t i = 0;
		while (i < componentsToExecute.size())
		{
			if (componentsToExecute.at(i)->CanRunInitializeComponent(executedEngineComponents))
			{
				componentsToExecute.at(i)->InitializeComponent();
				executedEngineComponents.push_back(componentsToExecute.at(i)->StaticClass());
				componentsToExecute.erase(componentsToExecute.begin() + i);
				continue;
			}

			i++;
		}

		//Each iteration must execute at least one engine component, otherwise it's assumed that there's a circular dependency.
		if (componentsToExecute.size() == oldCompSize)
		{
			CoreLog.Log(LogCategory::LL_Fatal, TXT("Failed to initialize engine components.  A circular dependency was detected amongst the following engine components."));
			for (size_t i = 0; i < componentsToExecute.size(); i++)
			{
				CoreLog.Log(LogCategory::LL_Fatal, TXT("    %s"), componentsToExecute.at(i)->StaticClass()->GetDuneClassName());
			}
			FatalError(TXT("There's a circular dependency amongst the registered engine components' InitializeComponent.  See the logs for details."));
			return;
		}
	}
	//Reset for PostInitializeComponent
	componentsToExecute = EngineComponents;
	executedEngineComponents.clear();

	//PostInitializeComponents
	while (componentsToExecute.size() > 0)
	{
		size_t oldCompSize = componentsToExecute.size();
		size_t i = 0;
		while (i < componentsToExecute.size())
		{
			if (componentsToExecute.at(i)->CanRunPostInitializeComponent(executedEngineComponents))
			{
				componentsToExecute.at(i)->PostInitializeComponent();
				executedEngineComponents.push_back(componentsToExecute.at(i)->StaticClass());
				componentsToExecute.erase(componentsToExecute.begin() + i);
				continue;
			}

			i++;
		}

		//Each iteration must execute at least one engine component, otherwise it's assumed that there's a circular dependency.
		if (componentsToExecute.size() == oldCompSize)
		{
			CoreLog.Log(LogCategory::LL_Fatal, TXT("Failed to post initialize engine components.  A circular dependency was detected amongst the following engine components."));
			for (size_t i = 0; i < componentsToExecute.size(); i++)
			{
				CoreLog.Log(LogCategory::LL_Fatal, TXT("    %s"), componentsToExecute.at(i)->StaticClass()->GetDuneClassName());
			}
			FatalError(TXT("There's a circular dependency amongst the registered engine components' PostInitializeComponent.  See the logs for details."));
			return;
		}
	}
}

void Engine::RegisterObject (Object* newObject)
{
	unsigned int objHash = newObject->GetObjectHash();

	if (objHash != 0 && !Utils::IsPowerOf2(objHash))
	{
		CoreLog.Log(LogCategory::LL_Critical, TXT("Unable to register %s to hash table.  %s must either be 0 or a number in powers of 2."), newObject->GetName(), DString::MakeString(objHash));
		return;
	}

	unsigned int tableIdx = static_cast<unsigned int>(trunc(log2(objHash)));
	if (tableIdx >= ObjectHashTable.size())
	{
		CoreLog.Log(LogCategory::LL_Critical, TXT("Unable to register %s to hash table.  The table index (%s) is larger than the object hash table size (%s)."), newObject->GetName(), DString::MakeString(tableIdx), DString::MakeString(ObjectHashTable.size()));
		return;
	}

	if (ObjectHashTable.at(tableIdx).LeadingObject != nullptr)
	{
		//Maintain linked list
		newObject->NextObject = ObjectHashTable.at(tableIdx).LeadingObject;
	}

	//Push new object to the front of the link list
	ObjectHashTable.at(tableIdx).LeadingObject = newObject;
}

DString Engine::GenerateFatalErrorMsg (const DString& specificError)
{
	return (TXT("FATAL ERROR!\n") + specificError);
}

void Engine::ShutdownSandDune ()
{
	//Shutdown all other Engines before shutting down the main engine
	if (IsMainEngine())
	{
		while (true)
		{
			Int numEnginesRemaining = 0;

			for (Engine* engine : EngineInstances)
			{
				if (engine == this)
				{
					continue;
				}

				if (engine != nullptr)
				{
					numEnginesRemaining++;
					if (!engine->IsShuttingDown())
					{
						engine->Shutdown();
					}
				}
			}

			if (numEnginesRemaining == 0)
			{
				//All other engines shutdown successfully
				break;
			}

			OS_Sleep(15);
		}
	}

	//Notify components that the engine is about to shutdown
	for (size_t i = 0; i < EngineComponents.size(); i++)
	{
		EngineComponents.at(i)->ShutdownComponent();
	}

	//Destroy all objects
	for (ObjectIterator iter(ObjectHashNumber, true); iter.SelectedObject; iter++)
	{
		if (iter.SelectedObject.IsValid())
		{
			iter.SelectedObject->Destroy();
		}
	}
	CollectGarbage();

	//Remove TickGroups
	for (TickGroup* group : TickGroups)
	{
		delete group;
	}
	ContainerUtils::Empty(OUT TickGroups);

	for (size_t i = 0; i < EngineComponents.size(); i++)
	{
		delete EngineComponents.at(i);
	}
	ContainerUtils::Empty(OUT EngineComponents);

	if (IsMainEngine())
	{
		//Clean up DClasses and their DefaultObjects (DefaultObjects are cleared when the DClasses are deleted).
		DClassAssembler::DisassembleDClasses();
	}

	ShutdownPlatform(IsMainEngine()); //Uninitialize any OS functionality for this thread.
	EngineInstances[EngineIndex] = nullptr;
}
SD_END