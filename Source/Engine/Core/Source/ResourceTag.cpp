/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourceTag.cpp
=====================================================================
*/

#include "ContainerUtils.h"
#include "ResourceTag.h"

SD_BEGIN
ResourceTag::ResourceTag () :
	TagName(DString::EmptyString),
	References(0)
{
	//Noop
}

size_t ResourceTag::AddTag (const HashedString& tagName, std::vector<ResourceTag>& outTagList)
{
	for (size_t i = 0; i < outTagList.size(); ++i)
	{
		if (tagName == outTagList.at(i).TagName)
		{
			outTagList.at(i).AddReference();
			return i;
		}
	}

	//Tag is not found. Look for an empty slot
	for (size_t i = 0; i < outTagList.size(); ++i)
	{
		if (outTagList.at(i).TagName.GetHash() == 0)
		{
			outTagList.at(i).TagName = tagName;
			outTagList.at(i).AddReference();
			return i;
		}
	}

	//Add to end instead
	size_t result = outTagList.size();
	ResourceTag newTag;
	newTag.TagName = tagName;
	newTag.AddReference();
	outTagList.emplace_back(newTag);

	return result;
}

bool ResourceTag::RemoveTag (const HashedString& tagName, std::vector<ResourceTag>& outTagList)
{
	if (tagName.GetHash() == 0)
	{
		return false;
	}

	for (size_t i = 0; i < outTagList.size(); ++i)
	{
		if (outTagList.at(i).TagName == tagName)
		{
			if (outTagList.at(i).RemoveReference())
			{
				outTagList.erase(outTagList.begin() + i);
			}

			return true;
		}
	}

	return false;
}

void ResourceTag::AddReference ()
{
	++References;
}

bool ResourceTag::RemoveReference ()
{
	--References;
	return (References <= 0);
}
SD_END