/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DClass.cpp
=====================================================================
*/

#include "DClass.h"
#include "DClassAssembler.h"
#include "EngineComponent.h"
#include "Object.h"

SD_BEGIN
DClass::DClass (const DString& className) : CDO{nullptr}
{
	ClassType = CT_None;

	DuneClassName = className;
	ParentClass = nullptr;
	IsAbstractClass = true;
	ClassHash = 0;
}

DClass::~DClass ()
{
	switch (ClassType)
	{
		case(CT_Object):
			if (CDO.DefaultObject != nullptr)
			{
				delete CDO.DefaultObject;
				CDO.DefaultObject = nullptr;
			}
			break;

		case(CT_EngineComponent):
			if (CDO.DefaultEngineComponent != nullptr)
			{
				delete CDO.DefaultEngineComponent;
				CDO.DefaultEngineComponent = nullptr;
			}
			break;
	}
}

const DClass* DClass::FindDClass (const DString& className)
{
	return DClassAssembler::FindDClass(className);
}

const DClass* DClass::FindDClass (size_t classHash)
{
	return DClassAssembler::FindDClass(classHash);
}

void DClass::SetCDO (Object* newObject)
{
	CHECK_INFO(CDO.DefaultObject == nullptr, "Only 1 CDO can be assigned to a DClass instance.")
	ClassType = CT_Object;
	CDO.DefaultObject = newObject;
	IsAbstractClass = false;
}

void DClass::SetCDO (EngineComponent* newCDO)
{
	CHECK_INFO(CDO.DefaultEngineComponent == nullptr, "Only 1 CDO can be assigned to a DClass instance.")
	ClassType = CT_EngineComponent;
	CDO.DefaultEngineComponent = newCDO;
	IsAbstractClass = false;
}

bool DClass::IsChildOf (const DClass* targetParentClass) const
{
	if (targetParentClass == this)
	{
		return true;
	}
	else if (ParentClass == nullptr)
	{
		return false;
	}

	return (ParentClass->IsChildOf(targetParentClass));
}

bool DClass::IsParentOf (const DClass* targetChildClass) const
{
	//It's faster to do it this way since we don't have to iterate through all children.
	return (targetChildClass->IsChildOf(this));
}

DString DClass::GetClassNameWithoutNamespace () const
{
	Int lastColonIdx = DuneClassName.Find(TXT(":"), 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	if (lastColonIdx <= 0)
	{
		//Namespace is not specified in the class name. Return whole string.
		return DuneClassName;
	}

	if (lastColonIdx >= (DuneClassName.Length() - 1))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Invalid DClass \"%s\". When trying to obtain the class name without namespace, it determined that the class name ended with a colon."), DuneClassName);
		return DuneClassName;
	}

	return DuneClassName.SubString(lastColonIdx + 1);
}

DString DClass::ToString () const
{
	return DuneClassName;
}

const DClass* DClass::GetSuperClass () const
{
	return ParentClass;
}

const Object* DClass::GetDefaultObject () const
{
	if (ClassType == CT_Object)
	{
		return CDO.DefaultObject;
	}

	return nullptr;
}

const EngineComponent* DClass::GetDefaultEngineComponent () const
{
	if (ClassType == CT_EngineComponent)
	{
		return CDO.DefaultEngineComponent;
	}

	return nullptr;
}

void DClass::AddChild (DClass* child)
{
	size_t index;

	//Added alphabetically for organizational purposes
	for (index = 0; index < Children.size(); index++)
	{
		if (Children.at(index)->GetDuneClassName().Compare(child->GetDuneClassName(), DString::CC_CaseSensitive) > 0)
		{
			break;
		}
	}

	Children.insert(Children.begin() + index, child);
}
SD_END