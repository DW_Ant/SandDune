/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickGroup.cpp
=====================================================================
*/

#include "SDFunction.h"
#include "TickComponent.h"
#include "TickGroup.h"

SD_BEGIN
TickGroup::TickGroup (const DString& inGroupName, Int inTickPriority) :
	GroupName(inGroupName),
	TickPriority(inTickPriority),
	TimeDilation(1.f),
	bTicking(true)
{
	//Noop
}

TickGroup::~TickGroup ()
{
	//Noop
}

void TickGroup::RegisterTick (TickComponent* newTick)
{
#ifdef DEBUG_MODE
	CHECK_INFO(!OnTick.IsRegistered(SDFUNCTION_1PARAM(newTick, TickComponent, Tick, void, Float), true), "The TickComponent is already registered to " + GroupName);
#endif

	OnTick.RegisterHandler(SDFUNCTION_1PARAM(newTick, TickComponent, Tick, void, Float));
}

void TickGroup::RemoveTick (TickComponent* targetTick)
{
#ifdef DEBUG_MODE
	CHECK_INFO(OnTick.IsRegistered(SDFUNCTION_1PARAM(targetTick, TickComponent, Tick, void, Float), true), "The TickComponent is already removed from " + GroupName);
#endif

	OnTick.UnregisterHandler(SDFUNCTION_1PARAM(targetTick, TickComponent, Tick, void, Float));
}

void TickGroup::SetTicking (bool bNewTicking)
{
	bTicking = bNewTicking;
}

void TickGroup::TickRegisteredComponents (Float deltaSec)
{
	if (IsTicking())
	{
		OnTick.Broadcast(deltaSec * TimeDilation);
	}
}

void TickGroup::SetTimeDilation (Float newTimeDilation)
{
	TimeDilation = newTimeDilation;
}
SD_END