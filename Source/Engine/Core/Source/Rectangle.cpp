/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Rectangle.cpp
=====================================================================
*/

#include "LogCategory.h"
#include "Rectangle.h"
#include "Utils.h"

SD_BEGIN
Rectangle::Rectangle () :
	Width(0.f),
	Height(0.f),
	Center(Vector2::ZERO_VECTOR)
{
	//Noop
}

Rectangle::Rectangle (Float inWidth, Float inHeight, const Vector2& inCenter) :
	Width(inWidth),
	Height(inHeight),
	Center(inCenter)
{
	//Noop
}

Rectangle::Rectangle (const Rectangle& copyRectangle) :
	Width(copyRectangle.Width),
	Height(copyRectangle.Height),
	Center(copyRectangle.Center)
{
	//Noop
}

void Rectangle::operator= (const Rectangle& copyRectangle)
{
	Width = copyRectangle.Width;
	Height = copyRectangle.Height;
	Center = copyRectangle.Center;
}

bool Rectangle::operator== (const Rectangle& otherRect) const
{
	return (Width == otherRect.Width && Height == otherRect.Height && Center == otherRect.Center);
}

bool Rectangle::operator!= (const Rectangle& otherRect) const
{
	return !(*this == otherRect);
}

void Rectangle::ResetToDefaults ()
{
	Width = 0.f;
	Height = 0.f;
	Center = Vector2::ZERO_VECTOR;
}

DString Rectangle::ToString () const
{
	return (TXT("(Width=") + Width.ToString() + TXT(", Height=") + Height.ToString() + TXT(", Center=") + Center.ToString() + TXT(")"));
}

void Rectangle::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("Width"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Rectangle();
		return;
	}
	Width = Float(varData);

	varData = ParseVariable(str, TXT("Height"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Rectangle();
		return;
	}
	Height = Float(varData);

	varData = ParseStruct(str, TXT("Center"));
	if (varData.IsEmpty())
	{
		*this = Rectangle();
		return;
	}
	Center.ParseString(varData);
}

size_t Rectangle::GetMinBytes () const
{
	return (Width.GetMinBytes() + Height.GetMinBytes() + Center.GetMinBytes());
}

void Rectangle::Serialize (DataBuffer& outData) const
{
	outData << Width;
	outData << Height;
	outData << Center;
}

bool Rectangle::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Width >> Height >> Center;
	return !dataBuffer.HasReadError();
}

Rectangle Rectangle::GetOverlappingRectangle (const Rectangle& otherRect) const
{
	if (!Overlaps(otherRect))
	{
		return Rectangle();
	}

	Float top;
	Float right;
	Float bottom;
	Float left;

	top = Utils::Min(GetGreaterHBorder(), otherRect.GetGreaterHBorder());
	right = Utils::Min(GetGreaterVBorder(), otherRect.GetGreaterVBorder());
	bottom = Utils::Max(GetLesserHBorder(), otherRect.GetLesserHBorder());
	left = Utils::Max(GetLesserVBorder(), otherRect.GetLesserVBorder());

	Float width = (right - left);
	Float height = (top - bottom);
	Vector2 center;
	center.X = (right + left) * 0.5f;
	center.Y = (top + bottom) * 0.5f;
	return Rectangle(width, height, center);
}

bool Rectangle::Overlaps (const Rectangle& otherRect) const
{
	//The distances between the Center points' X and Y coordinates.
	Vector2 centerDist = (Center - otherRect.Center);
	centerDist.X.AbsInline();
	centerDist.Y.AbsInline();

	if (centerDist.Y > (Height * 0.5f) + (otherRect.Height * 0.5f))
	{
		//This rectangle is either below/above of the other rectangle.
		return false;
	}

	if (centerDist.X > (Width * 0.5f) + (otherRect.Width * 0.5f))
	{
		//This rectangle is either left/right of the other rectangle.
		return false;
	}

	return true;
}

bool Rectangle::EncompassesPoint (const Vector2& targetPoint) const
{
	Vector2 pointDistFromCenter(targetPoint - Center);
	pointDistFromCenter.X.AbsInline();
	pointDistFromCenter.Y.AbsInline();

	return (pointDistFromCenter.X <= Width * 0.5f && pointDistFromCenter.Y <= Height * 0.5f);
}

#pragma region "External Operators"
Rectangle operator* (const Rectangle& left, Float right)
{
	return Rectangle(left.Width * right, left.Height * right, left.Center * right);
}

Rectangle operator* (Float left, const Rectangle& right)
{
	return Rectangle(left * right.Width, left * right.Height, left * right.Center);
}

Rectangle operator* (const Rectangle& left, const Rectangle& right)
{
	return Rectangle(left.Width * right.Width, left.Height * right.Height, left.Center * right.Height);
}

Rectangle& operator*= (Rectangle& left, Float right)
{
	left.Width *= right;
	left.Height *= right;
	left.Center *= right;

	return left;
}

Rectangle& operator*= (Rectangle& left, const Rectangle& right)
{
	left.Width *= right.Width;
	left.Height *= right.Height;
	left.Center *= right.Center;

	return left;
}

Rectangle operator/ (const Rectangle& left, Float right)
{
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide Rectangle [%s] by zero."), left);
		return left;
	}

	return Rectangle(left.Width / right, left.Height / right, left.Center / right);
}

Rectangle operator/ (const Rectangle& left, const Rectangle& right)
{
	return Rectangle(left.Width / right.Width, left.Height / right.Height, left.Center / right.Center);
}

Rectangle& operator/= (Rectangle& left, Float right)
{
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide Rectangle [%s] by zero."), left);
		return left;
	}

	left.Width /= right;
	left.Height /= right;
	left.Center /= right;

	return left;
}

Rectangle& operator/= (Rectangle& left, const Rectangle& right)
{
	left.Width /= right.Width;
	left.Height /= right.Height;
	left.Center /= right.Center;

	return left;
}
#pragma endregion
SD_END