/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Stopwatch.cpp
=====================================================================
*/

#include "Stopwatch.h"

SD_BEGIN
Stopwatch::Stopwatch () :
	Name(TXT("Stopwatch")),
	AutoLog(true),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const DString& stopwatchName) :
	Name(stopwatchName),
	AutoLog(true),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const DString& stopwatchName, bool inAutoLog) :
	Name(stopwatchName),
	AutoLog(inAutoLog),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const Stopwatch& cpyObj) :
	Name(cpyObj.Name),
	AutoLog(cpyObj.AutoLog),
	StartTime(cpyObj.StartTime),
	RecordedTime(cpyObj.RecordedTime)
{
	//Noop
}

Stopwatch::~Stopwatch ()
{
	DestroyStopwatch();
}

void Stopwatch::PauseTime ()
{
	RecordedTime = GetElapsedTime();
	StartTime = -1.f; //pauses timer
}

void Stopwatch::ResumeTime ()
{
	StartTime = GetSystemTime();
}

void Stopwatch::ResetTime ()
{
	RecordedTime = 0.f;

	if (StartTime >= 0)
	{
		StartTime = GetSystemTime();
	}
}

Float Stopwatch::GetElapsedTime () const
{
	Float result = RecordedTime;

	if (StartTime >= 0)
	{
		result += GetSystemTime() - StartTime;
	}

	return result;
}

Float Stopwatch::GetSystemTime () const
{
	clock_t curTime = clock();

	return static_cast<float>(curTime);
}

void Stopwatch::DestroyStopwatch ()
{
	if (AutoLog)
	{
		CoreLog.Log(LogCategory::LL_Log, TXT("%s is destroyed.  The recorded time of this stopwatch was:  %s milliseconds."), Name, GetElapsedTime());
	}
}
SD_END