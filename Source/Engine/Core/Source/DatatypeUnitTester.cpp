/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DatatypeUnitTester.cpp
=====================================================================
*/

#include "CoreClasses.h"
#include "utf8.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::DatatypeUnitTester, SD::UnitTester)
SD_BEGIN

bool DatatypeUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestDString(testFlags) && TestDataBuffer(testFlags) && TestStringIterator(testFlags) && TestCharacterEncoding(testFlags) && TestBool(testFlags) && TestInt(testFlags) && TestFloat(testFlags) &&
			TestHashedString(testFlags) && TestDPointer(testFlags) && TestRange(testFlags) && TestVector2(testFlags) && TestVector3(testFlags) && TestRotator(testFlags) && TestRectangle(testFlags) &&
			TestSDFunction(testFlags) && TestResourceTree(testFlags) && TestMatrix(testFlags));
	}

	return true;
}

bool DatatypeUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	//This test should run before any other SD test.
	return true;
}

//It's important to run the DString test first since all of the other tests depends on DStrings to work (for logging).
bool DatatypeUnitTester::TestDString (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("DString"));

	DString blankString;
	DString initString(TXT("Initialized string"));
	DString copiedString(initString);

	//Need to test formatted string early since the logs depend on that functionality to work
	DString formattedString = DString::CreateFormattedString(TXT("Formatted string (%s)"), copiedString);
	DString superFormattedString = DString::CreateFormattedString(TXT("Super Formatted string (%s), (%s), (%s), (%s)"), DString(TXT("CStringA")), DString(TXT("CStringB")), DString(TXT("CStringC")), DString(TXT("CStringD")));

	DString formatParam1 = TXT("Some Value");
	DString formatParam2 = TXT("Insert a string with '%s' macro within");
	DString formatParam3 = TXT("");
	DString formatParam4 = TXT("(Final Value with number 7)");
	DString otherFormatString = DString::CreateFormattedString(TXT("Testing other Format String function with %s where we %s another string.  Empty String=\"%s\". %s.  -=End=-"), formatParam1, formatParam2, formatParam3, formatParam4);

	TestLog(testFlags, TXT("Constructor tests.  Note that these logs may appear incorrectly since the FormatString and ToCString was not yet tested."));
	TestLog(testFlags, TXT("    blankString=%s"), blankString);
	TestLog(testFlags, TXT("    initString=%s"), initString);
	TestLog(testFlags, TXT("    copiedString=%s"), copiedString);
	TestLog(testFlags, TXT("    formattedString=%s"), formattedString);
	TestLog(testFlags, TXT("    superFormattedString=%s"), superFormattedString);
	TestLog(testFlags, TXT("    otherFormatString=%s"), otherFormatString);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		if (initString != TXT("Initialized string"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), initString);
			TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		if (formattedString != TXT("Formatted string (Initialized string)"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The formattedString should have been equal to \"Formatted string (Initialized string)\".  Instead it's \"%s\""), formattedString);
			TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		DString superFormatResult = TXT("Super Formatted string (CStringA), (CStringB), (CStringC), (CStringD)");
		if (superFormattedString != superFormatResult)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The superFormattedString should have been equal to \"%s\".  Instead it's \"%s\""), superFormatResult, superFormattedString);
			TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		DString otherFormatStringResult = TXT("Testing other Format String function with Some Value where we Insert a string with '%s' macro within another string.  Empty String=\"\". (Final Value with number 7).  -=End=-");
		if (otherFormatString != otherFormatStringResult)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The otherFormatString should have been equal to \"%s\".  Instead it's \"%s\""), otherFormatStringResult, otherFormatString);
			TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		//compare CStrings
		if (strcmp(initString.ToCString(), TXT("Initialized string")) != 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The CString of initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), initString);
			TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since ToCString was not yet tested."));
			return false;
		}

		DString compareString = initString;
		compareString.ToLower();
		if (compareString != TXT("initialized string"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The lower case of initialized string should have been equal to \"initialized string\".  Instead it's \"%s\""), compareString);
			return false;
		}

		compareString.ToUpper();
		if (compareString != TXT("INITIALIZED STRING"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The upper case of initialized string should have been equal to \"INITIALIZED STRING\".  Instead it's \"%s\""), compareString);
			return false;
		}

		if (initString.Compare(compareString, DString::CC_IgnoreCase) != 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The case insensitive compare of \"%s\" and \"%s\" should have returned true."), initString, compareString);
			return false;
		}

		if (!blankString.IsEmpty())
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The blank string should be considered empty.  Instead IsEmpty returned false.  Value of blankString is:  \"%s\""), blankString);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		std::function<bool(const DString&)> testParsing([&](const DString& testStr)
		{
			//Even though it's the same data type, essentially test ToString -> ParseString returns the same value.
			DString toStr = testStr.ToString();
			if (testStr.Compare(testStr, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("DString tests failed. The conversion to string did not produce the same value. Original string \"%s\". Converted string \"%s\"."), testStr, toStr);
				return false;
			}

			DString fromStr;
			fromStr.ParseString(toStr);
			if (fromStr.Compare(testStr, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("DString tests failed. The parsing a string \"%s\" did not produce the same value. Instead it produced \"%s\"."), testStr, fromStr);
				return false;
			}

			return true;
		});

		if (!testParsing(TXT("ToString test")))
		{
			return false;
		}

		if (!testParsing(TXT("A complicated string: 1234567890)(*&^%$#@! -> End string.")))
		{
			return false;
		}

		if (!testParsing(TXT("String with \"quotes\" and single \'quotes\'")))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("DString utilities"));
	{
		DString main = TXT("main");
		TestLog(testFlags, TXT("Adding \" appended text\" to \"%s\""), main);
		main += TXT(" appended text");

		if (main != TXT("main appended text"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The resulting appended string should have been \"main appended text\".  Instead it's \"%s\""), main);
			return false;
		}

		if (main.At(3) != TEXT('n'))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Index 3 of \"%s\" should have been \'n\'.  Instead it returned:  \"%s\""), main, DString(main.At(3)));
			return false;
		}

		TestLog(testFlags, TXT("Finding \"appended\" within \"%s\".  The subtext is found at index %s"), main, main.Find(TXT("appended"), 0, DString::CC_IgnoreCase));
		if (main.Find(TXT("appended"), 0, DString::CC_IgnoreCase) != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"appended\" appears within \"%s\" should have been 5.  Instead it returned:  \"%s\""), main, main.Find(TXT("appended"), 0, DString::CC_IgnoreCase));
			return false;
		}

		if (main.Find(TXT("APPENDED"), 0, DString::CC_CaseSensitive) >= 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case sensitive) should have been negative.  Instead it returned:  %s"), main, main.Find(TXT("APPENDED"), 0, DString::CC_CaseSensitive));
			return false;
		}

		if (main.Find(TXT("APPENDED"), 0, DString::CC_IgnoreCase) != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case insensitive) should have been 5.  Instead it returned:  %s"), main, main.Find(TXT("APPENDED"), 0, DString::CC_IgnoreCase));
			return false;
		}

		if (main.Find(TXT("appended"), 7, DString::CC_IgnoreCase) >= 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"appended\" within \"%s\" starting from index 7 should have returned negative results.  Instead it returned:  %s"), main, main.Find(TXT("appended"), 7, DString::CC_IgnoreCase));
			return false;
		}

		DString closeMatchStr = TXT("NeedNEEDLENeedNeedle In HayStack");
		Int matchIdx = closeMatchStr.Find(TXT("NEEDLE"), 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		if (matchIdx != 4)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"Needle\" within \"%s\" (case sensitive from the right) should have returned 4.  Instead it returned %s."), closeMatchStr, matchIdx);
			return false;
		}

		matchIdx = closeMatchStr.Find(TXT("NEEDLE"), 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		if (matchIdx != 14)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"Needlen\" within \"%s\" (case insensitive from the right) should have returned 14.  Instead it returned %s."), closeMatchStr, matchIdx);
			return false;
		}

		DString wrappingTestStr = TXT("Some :string: value with colons.");
		Bool bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 2);
		TestLog(testFlags, TXT("Is char index 2 'm' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
		if (bIsWrapped)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The character at index 2 'm' should not be wrapped in colons in string:  \"%s\""), wrappingTestStr);
			return false;
		}

		bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 7);
		TestLog(testFlags, TXT("Is char index 7 't' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
		if (!bIsWrapped)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The character at index 7 't' should be wrapped in colons in string:  \"%s\""), wrappingTestStr);
			return false;
		}

		bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 25);
		TestLog(testFlags, TXT("Is char index 25 'c' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
		if (bIsWrapped)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The character at index 25 'c' should not be wrapped in colons in string:  \"%s\""), wrappingTestStr);
			return false;
		}

		bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 12);
		TestLog(testFlags, TXT("Is char index 12 ':' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
		if (bIsWrapped)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The character at index 12 ':' should not be wrapped in colons since the colons, themselves, define the wrapping boundaries."));
			return false;
		}

		main = TXT("Sand Dune project:  Transcendence");
		DString lastWord = main.SubString(20);
		TestLog(testFlags, TXT("Retrieving the last word from \"%s\" returns \"%s\""), main, lastWord);
		if (lastWord != TXT("Transcendence"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The last word of \"%s\" should have been \"Transcendence\".  Instead it returned \"%s\""), main, lastWord);
			return false;
		}

		DString secondWord = main.SubString(5, 8);
		TestLog(testFlags, TXT("Retrieving the second word from \"%s\" returns \"%s\""), main, secondWord);
		if (secondWord != TXT("Dune"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The second word of \"%s\" should have been \"Dune\".  Instead it returned \"%s\""), main, secondWord);
			return false;
		}

		DString duneProject = main.SubStringCount(5, 12);
		TestLog(testFlags, TXT("Retrieving \"Dune Project\" from \"%s\" using SubStringCount returns \"%s\""), main, duneProject);
		if (duneProject != TXT("Dune project"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The 2nd and 3rd word of \"%s\" should have been \"Dune project\".  Instead it returned \"%s\""), main, duneProject);
			return false;
		}

		main = TXT("Segment1|Segment2");
		Int splitIndex = 8; //at '|' character
		TestLog(testFlags, TXT("Splitting \"%s\" at | character into two other strings."), main);
		DString firstSegment;
		DString secondSegment;
		main.SplitString(splitIndex, firstSegment, secondSegment);
		if (firstSegment != TXT("Segment1") || secondSegment != TXT("Segment2"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Splitting \"%s\" at the | character (index %s) should have constructed strings:  \"Segment1\" and \"Segment2\".  Instead \"%s\" and \"%s\" was constructed."), main, splitIndex, firstSegment, secondSegment);
			return false;
		}

		//Testing ParseString
		{
			main = TXT("2,5,2,29,918,-192,29,38,61,,44,"); //Double comma will be testing empty strings.
			TCHAR delimiter = ',';
			std::vector<DString> actual;
			std::vector<DString> expected = {TXT("2"), TXT("5"), TXT("2"), TXT("29"), TXT("918"), TXT("-192"), TXT("29"), TXT("38"), TXT("61"), TXT(""), TXT("44"), TXT("")};
			bool removeEmpty = false;
			std::function<bool()> testParseString([&]()
			{
				main.ParseString(delimiter, OUT actual, removeEmpty);
				if (actual.size() != expected.size())
				{
					UnitTestError(testFlags, TXT("DString tests failed.  The total number of elements of the number vector constructed from parsing \"%s\" should have been %s.  Instead the vector size is %s"), main, DString::MakeString(expected.size()), DString::MakeString(actual.size()));
					return false;
				}

				//validate each entry
				for (size_t i = 0; i < expected.size(); i++)
				{
					if (expected.at(i) != actual.at(i))
					{
						UnitTestError(testFlags, TXT("DString tests failed.  The numbers vector at index %s does not match the expected value:  %s.  Instead it's %s"), DString::MakeString(i), expected.at(i), actual.at(i));
						return false;
					}
				}

				return true;
			});

			if (!testParseString())
			{
				return false;
			}

			//Remove the empty string from the expected list.
			ContainerUtils::RemoveItems(OUT expected, DString::EmptyString);
			removeEmpty = true;
			if (!testParseString())
			{
				return false;
			}

			main = TXT("1 Testing Single Elements  in   a   list 2");
			expected = {TXT("1"), TXT("Testing"), TXT("Single"), TXT("Elements"), TXT("in"), TXT("a"), TXT("list"), TXT("2")};
			delimiter = ' ';
			removeEmpty = true;
			if (!testParseString())
			{
				return false;
			}
		}

		main = TXT("Alpha _ Omega");
		DString edgeString = main.Left(5);
		TestLog(testFlags, TXT("The left part of \"%s\" is \"%s\""), main, edgeString);
		if (edgeString != TXT("Alpha"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The left part of \"%s\" should have returned \"Alpha\".  Instead it returned \"%s\"."), main, edgeString);
			return false;
		}

		edgeString = main.Right(5);
		TestLog(testFlags, TXT("The right part of \"%s\" is \"%s\""), main, edgeString);
		if (edgeString != TXT("Omega"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The right part of \"%s\" should have returned \"Omega\".  Instead it returned \"%s\"."), main, edgeString);
			return false;
		}

		TestLog(testFlags, TXT("The length of \"%s\" is %s"), main, main.Length());
		if (main.Length() != 13)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The length of \"%s\" should have returned 13. Instead it returned %s"), main, main.Length());
			return false;
		}

		TestLog(testFlags, TXT("Inserting \" _ Beta\" after alpha in \"%s\""), main);
		main.Insert(5, TXT(" _ Beta"));
		if (main != TXT("Alpha _ Beta _ Omega"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of inserting \" _ Beta\" after \"Alpha\" within \"Alpha _ Omega\" should have resulted in \"Alpha _ Beta _ Omega\".  Instead it's:  \"%s\"."), main);
			return false;
		}

		TestLog(testFlags, TXT("Removing \" _ Omega\" from \"%s\""), main);
		main.Remove(12, 8);
		if (main != TXT("Alpha _ Beta"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of removing \" _ Omega\" from \"Alpha _ Beta _ Omega\" should have resulted in \"Alpha _ Beta\".  Instead it's:  \"%s\"."), main);
			return false;
		}

		TestLog(testFlags, TXT("Replacing spaces with \"[ ]\" in \"%s\""), main);
		main.ReplaceInline(TXT(" "), TXT("[ ]"), DString::CC_CaseSensitive);
		if (main != TXT("Alpha[ ]_[ ]Beta"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing \" \" with \"[ ]\" should have resulted in \"Alpha[ ]_[ ]Beta\".  Instead it's \"%s\"."), main);
			return false;
		}

		TestLog(testFlags, TXT("Replacing all a's with @'s in \"%s\" (case insensitive)."), main);
		main.ReplaceInline(TXT("a"), TXT("aAa"), DString::CC_IgnoreCase);
		if (main != TXT("aAalphaAa[ ]_[ ]BetaAa"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing a's (ignore case) with \"aAa\" should have resulted in \"aAalphaAa[ ]_[ ]BetaAa\".  Instead it's \"%s\"."), main);
			return false;
		}

		TestLog(testFlags, TXT("Replacing all lower-case a's with nothing in \"%s\""), main);
		main.ReplaceInline(TXT("a"), TXT(""), DString::CC_CaseSensitive);
		if (main != TXT("AlphA[ ]_[ ]BetA"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing all lower case a's (case sensitive) with nothing should have resulted in \"AlphA[ ]_[ ]BetA\".  Instead it's \"%s\"."), main);
			return false;
		}

		DString searchIn = TXT("Needle in haystack.");
		DString replaceResults = DString::Replace(searchIn, TXT("Needle"), TXT("Pitchfork"), DString::CC_CaseSensitive);
		if (replaceResults != TXT("Pitchfork in haystack."))
		{
			UnitTestError(testFlags, TXT("DString tests failed. The result of replacing \"Needle\" with \"Pitchfork\" in \"%s\" should have resulted in \"Pitchfork in Haystack.\".  Instead it's \"%s\"."), searchIn, replaceResults);
			return false;
		}

		TestLog(testFlags, TXT("Clearing \"%s\""), main);
		main.Clear();
		if (main != TXT(""))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Clearing a string should have resulted in \"\".  Instead it's:  \"%s\"."), main);
			return false;
		}

		TestLog(testFlags, TXT("Testing search functions for DString. . ."));
		DString searchString = TXT("Sand Dune");
		DString baseString = TXT("Sand Pit");
		Int mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 5.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		baseString = TXT("SaNd DuNe");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("The first mismatching index when searching %s (case sensitive) within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 2)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s (case sensitive) within %s should be 2.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("The first mismatching index (case insensitive) when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != INT_INDEX_NONE)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index (case insensitive) when searching %s within %s should be " STRINGIFY(INT_INDEX_NONE) ".  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		searchString = TXT("Entity");
		baseString = TXT("EntityComponent");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 6)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 6.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		searchString = TXT("Something ");
		baseString = TXT("Something Something Dark Side");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 20)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 20.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		searchString = TXT(" ");
		baseString = TXT("Trailing Spaces  ");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("The last mismatching index when searching \"%s\" within \"%s\" is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 14)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The last mismatching index when searching \"%s\" within \"%s\" should be 14.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		searchString = TXT("YO");
		baseString = TXT("Yoyo");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("The first mismatching (case insensitive) index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != INT_INDEX_NONE)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching (case insensitive) index when searching %s within %s should be negative.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		searchString = TXT("na");
		baseString = TXT("banana");
		mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("The last mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
		if (mismatchIdx != 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The last mismatching index when searching %s within %s should be 0.  Instead it returned %s."), searchString, baseString, mismatchIdx);
			return false;
		}

		DString fatString = TXT("            My custom string.        ");
		main = fatString;
		TestLog(testFlags, TXT("Trimming spaces on the ends of \"%s\""), main);
		main.TrimSpaces();
		if (main != TXT("My custom string."))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Trimming spaces on the end of \"%s\" should have resulted in \"My custom string.\".  Instead it's:  \"%s\"."), fatString, main);
			return false;
		}

		//Test Hashing
		{
			std::vector<DString> generatedHashes;
			std::function<bool(const DString&)> testHash([&](const DString& testString)
			{
				size_t firstHash = testString.GenerateHash();
				size_t secondHash = testString.GenerateHash(); //Ensure that it generates the same hash.
				if (firstHash != secondHash)
				{
					UnitTestError(testFlags, TXT("DString tests failed. Generating the hash from \"%s\" should have produced the same hash. Instead it generated %s and %s."), testString, DString::MakeString(firstHash), DString::MakeString(secondHash));
					return false;
				}

				//Ensure it's generating different different hashes. Although it's possible for a collision, the hard coded examples for this unit test should not collide.
				for (size_t i = 0; i < generatedHashes.size(); ++i)
				{
					if (firstHash == generatedHashes.at(i))
					{
						UnitTestError(testFlags, TXT("DString tests failed. Generating the hash from \"%s\" should have produced a different hash from the other test cases. Instead %s was generated twice."), testString, DString::MakeString(firstHash));
						return false;
					}
				}

				generatedHashes.push_back(testString);
				return true;
			});

			if (!testHash(TXT("Cheapshot")))
			{
				return false;
			}

			if (!testHash(TXT("KarmaKat")))
			{
				return false;
			}

			if (!testHash(TXT("Mobius")))
			{
				return false;
			}

			if (!testHash(TXT("Spooky")))
			{
				return false;
			}

			if (!testHash(TXT("TempestSmiteclaw")))
			{
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String to Int"));
	{
		std::function<bool(const DString&, Int)> testAtoi([&](const DString& input, Int expectedOutput)
		{
			Int actualOutput = input.Atoi();
			if (actualOutput != expectedOutput)
			{
				UnitTestError(testFlags, TXT("DString tests failed. The Atoi function from \"%s\" should have returned %s. Instead it returned %s."), input, expectedOutput, actualOutput);
			}

			return (actualOutput == expectedOutput);
		});

		if (!testAtoi(TXT("45"), 45))
		{
			return false;
		}

		if (!testAtoi(TXT("27361"), 27361))
		{
			return false;
		}

		if (!testAtoi(TXT("-1337"), -1337))
		{
			return false;
		}

		if (!testAtoi(DString::EmptyString, 0))
		{
			return false;
		}

		if (!testAtoi(TXT("JunkInput"), 0))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String to Float"));
	{
		std::function<bool(const DString&, Float)> testStof([&](const DString& str, Float expected)
		{
			Float actual = str.Stof();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("DString tests failed. The stof function from \"%s\" should have returned %s. Instead it returned %s."), str, expected, actual);
			}

			return (actual == expected);
		});

		if (!testStof(TXT("60"), 60.f))
		{
			return false;
		}

		if (!testStof(TXT("45.25"), 45.25f))
		{
			return false;
		}

		if (!testStof(TXT("297.2987"), 297.2987f))
		{
			return false;
		}

		if (!testStof(TXT("-38.2"), -38.2f))
		{
			return false;
		}

		if (!testStof(DString::EmptyString, 0.f))
		{
			return false;
		}

		if (!testStof(TXT("JunkValue"), 0.f))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Regex Utils"));
	{
		TestLog(testFlags, TXT("Testing DString::HasRegexMatch"));
		DString noNumString(TXT("Apples"));
		DString numString(TXT("Apples_1"));
		DString regexPattern(TXT("[0-9]"));

		if (noNumString.HasRegexMatch(regexPattern))
		{
			UnitTestError(testFlags, TXT("DString test failed. There shouldn't be a match when searching \"%s\" in \"%s\"."), regexPattern, noNumString);
			return false;
		}

		if (!numString.HasRegexMatch(regexPattern))
		{
			UnitTestError(testFlags, TXT("DString test failed. There should have been a match when searching \"%s\" in \"%s\"."), regexPattern, numString);
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("DString"));

	return true;
}

bool DatatypeUnitTester::TestDataBuffer (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Data Buffer"));

	SetTestCategory(testFlags, TXT("Endian Tests"));
	{
		bool bIsLittleEndian = DataBuffer::IsSystemLittleEndian();
		if (bIsLittleEndian)
		{
			TestLog(testFlags, TXT("Byte order is little endian."));
		}
		else
		{
			TestLog(testFlags, TXT("Byte order is big endian."));
		}
		const int numCharsInMsg = 21; //include null terminator
		char testMsg[] = "Original Byte order.";
		char origTestMsg[numCharsInMsg];
		strcpy_s(origTestMsg, numCharsInMsg, testMsg);

		TestLog(testFlags, TXT("The original message before byte swap is:  %s"), DString(testMsg));
		int numChars = 20;
		DataBuffer::SwapByteOrder(testMsg, numChars);
		TestLog(testFlags, TXT("The swapped byte order of original message is:  %s"), DString(testMsg));

		for (int i = 0; i < numChars; ++i)
		{
			if (testMsg[i] != origTestMsg[numChars - i - 1])
			{
				UnitTestError(testFlags, TXT("Failed to swap the byte order of a char array.  There's a byte mismatch with the original message and the reversed message at char index %s of \"%s\".  Reversed message is \"%s\"."), DString::MakeString(i), DString(origTestMsg), DString(testMsg));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Write Tests"));
	{
		DataBuffer writeTestBuffer;
		DataBuffer reservedBuffer(16);
		if (writeTestBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
		{
			UnitTestError(testFlags, TXT("An empty buffer's endianness should default to the machine's endianness."));
			return false;
		}

		const int numCharsInBasicData = 20; //include null terminator
		char basicData[] = "Hello Data Buffers!";
		const int numBytesInInt = sizeof(int);
		int someNumber = 392;
		const int numCharsInSuffixData = 16; //include null terminator
		char suffixData[] = "Ending Message.";

		std::vector<int> numbers = {83, 918, 732, 298}; //4 32bit numbers will fill up the 16 byte buffer.

		//Write the data to the buffers
		TestLog(testFlags, TXT("Writing \"%s\" to data buffer."), DString(basicData));
		writeTestBuffer.AppendBytes(basicData, numCharsInBasicData);

		//Convert int to char array
		TestLog(testFlags, TXT("Writing the number %s to data buffer."), Int(someNumber));
		char someNumberChar[numBytesInInt + 1];
		memcpy(someNumberChar, &someNumber, numBytesInInt);
		//someNumberChar[numBytesInInt] = '\0';
		writeTestBuffer.AppendBytes(someNumberChar, numBytesInInt);

		TestLog(testFlags, TXT("Writing \"%s\" to data buffer."), DString(suffixData));
		writeTestBuffer.AppendBytes(suffixData, numCharsInSuffixData);

		//Write numbers to the other data buffer
		char numberCharList[(numBytesInInt * 4) + 1];
		//std::copy(numbers.begin(), numbers.end(), numberCharList);
		for (size_t numIdx = 0; numIdx < numbers.size(); ++numIdx)
		{
			memcpy(numberCharList + (numBytesInInt * numIdx), &numbers.at(numIdx), numBytesInInt);
		}

		//Format vector<int> to a string list for test log
		DString numList = DString::EmptyString;
		bool bFirstNum = true;
		for (int num : numbers)
		{
			if (!bFirstNum)
			{
				numList += TXT(", ");
			}

			numList += DString::MakeString(num);
			bFirstNum = false;
		}
		TestLog(testFlags, TXT("Writing the following numbers to the second data buffer:  %s"), numList);
		reservedBuffer.AppendBytes(numberCharList, numBytesInInt * 4);
		CompleteTestCategory(testFlags);

		//=====================

		SetTestCategory(testFlags, TXT("Read Tests"));
		//Extract data from emptyBuffer
		char readBasicData[numCharsInBasicData];
		int readSomeNumber = -1;
		char readSuffixData[numCharsInSuffixData];
		writeTestBuffer.JumpToBeginning();
		writeTestBuffer.ReadBytes(readBasicData, numCharsInBasicData);
		TestLog(testFlags, TXT("Read the following char array from data buffer:  %s"), DString(readBasicData));
		for (int charIdx = 0; charIdx < numCharsInBasicData; ++charIdx)
		{
			if (readBasicData[charIdx] != basicData[charIdx])
			{
				UnitTestError(testFlags, TXT("Failed to extract character array from data buffer.  There's a character mismatch at index %s.  The original data was \"%s\".  It extracted \"%s\" instead."), DString::MakeString(charIdx), DString(basicData), DString(readBasicData));
				return false;
			}
		}

		writeTestBuffer.ReadBytes(someNumberChar, numBytesInInt);
		memcpy(&readSomeNumber, someNumberChar, numBytesInInt);
		TestLog(testFlags, TXT("Read the number %s from data buffer."), Int(readSomeNumber));
		if (someNumber != readSomeNumber)
		{
			UnitTestError(testFlags, TXT("Failed to extract integer from data buffer.  The original number was %s.  It extracted %s instead."), DString::MakeString(someNumber), DString::MakeString(readSomeNumber));
			return false;
		}

		writeTestBuffer.ReadBytes(readSuffixData, numCharsInSuffixData);
		TestLog(testFlags, TXT("Read the following char array from data buffer:  %s"), DString(readSuffixData));
		for (int charIdx = 0; charIdx < numCharsInSuffixData; ++charIdx)
		{
			if (suffixData[charIdx] != readSuffixData[charIdx])
			{
				UnitTestError(testFlags, TXT("Failed to extract character array from data buffer.  There's a character mismatch at index %s.  The original data was \"%s\".  It extracted \"%s\" instead."), DString::MakeString(charIdx), DString(suffixData), DString(readSuffixData));
				return false;
			}
		}

		if (writeTestBuffer.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Data Buffer test failed. The test buffer should not be considered empty when it has %s bytes in it."), Int(writeTestBuffer.GetNumBytes()));
			return false;
		}

		writeTestBuffer.EmptyBuffer();
		Int numBytesInBuffer = writeTestBuffer.GetNumBytes();
		if (numBytesInBuffer != 0)
		{
			UnitTestError(testFlags, TXT("Failed to clear data buffer.  There is still %s bytes in the data buffer after the clear command."), numBytesInBuffer);
			return false;
		}

		if (!writeTestBuffer.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Data Buffer test failed. After clearing a data buffer, it should be considered empty."));
			return false;
		}

		std::vector<int> readNumbers;
		reservedBuffer.JumpToBeginning();
		for (UINT_TYPE i = 0; i < numbers.size(); ++i)
		{
			int extractedNum;
			char extractedData[numBytesInInt];
			reservedBuffer.ReadBytes(extractedData, numBytesInInt);
			memcpy(&extractedNum, extractedData, numBytesInInt);
			readNumbers.push_back(extractedNum);
			TestLog(testFlags, TXT("Extracted the number %s from data buffer."), Int(extractedNum));
		}

		CHECK(readNumbers.size() == numbers.size()) //This should never happen due to for loop above
		for (UINT_TYPE i = 0; i < readNumbers.size(); ++i)
		{
			if (readNumbers.at(i) != numbers.at(i))
			{
				UnitTestError(testFlags, TXT("Failed to extract numbers from data buffer.  There's a number mismatch at index %s of number list.  It extracted %s instead of the expected %s."), DString::MakeString(i), DString::MakeString(readNumbers.at(i)), DString::MakeString(numbers.at(i)));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Streaming"));
	{
		//Run string data buffer test here since the string test runs before the data buffer test, and the string test does not test string streaming.
		DString msg1 = TXT("First string message.");
		DString msg2 = TXT("That other string message.");
		DString msg3 = TXT("The last string message.");
		DataBuffer stringBuffer;

		stringBuffer << msg1;
		stringBuffer << msg2;
		stringBuffer << msg3;

		DString readMsg1;
		DString readMsg2;
		DString readMsg3;
		stringBuffer >> readMsg1;
		stringBuffer >> readMsg2;
		stringBuffer >> readMsg3;

		if (msg1 != readMsg1)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The first message reads:  \"%s\".  Expected message:  \"%s\""), readMsg1, msg1);
			return false;
		}

		if (msg2 != readMsg2)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The second message reads:  \"%s\".  Expected message:  \"%s\""), readMsg2, msg2);
			return false;
		}

		if (msg3 != readMsg3)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The first message reads:  \"%s\".  Expected message:  \"%s\""), readMsg3, msg3);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Array Streaming"));
	{
		std::vector<DString> animals{TXT("Cat"), TXT("Dog"), TXT("Monkey"), TXT("Snake"), TXT("Tegu"), TXT("Whale"), TXT("Mole")};
		std::vector<DString> colors{TXT("Red"), TXT("Orange"), TXT("Yellow"), TXT("Green"), TXT("Blue"), TXT("Indigo"), TXT("Blue")};
		DataBuffer vectBuffer;
		vectBuffer.SerializeArray(animals);
		vectBuffer.SerializeArray(colors);

		std::vector<DString> readMsg;
		vectBuffer.DeserializeArray(OUT readMsg);

		if (readMsg.size() != animals.size())
		{
			UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original array is size is %s, but the streamed array's size is %s."), Int(animals.size()), Int(readMsg.size()));
			return false;
		}

		for (size_t i = 0; i < animals.size(); ++i)
		{
			if (readMsg.at(i) != animals.at(i))
			{
				UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original array at index %s is \"%s\". It read \"%s\" instead."), Int(i), animals.at(i), readMsg.at(i));
				return false;
			}
		}

		ContainerUtils::Empty(OUT readMsg);
		vectBuffer.DeserializeArray(OUT readMsg);
		if (readMsg.size() != colors.size())
		{
			UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original array is size is %s, but the streamed array's size is %s."), Int(colors.size()), Int(readMsg.size()));
			return false;
		}

		for (size_t i = 0; i < colors.size(); ++i)
		{
			if (readMsg.at(i) != colors.at(i))
			{
				UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original array at index %s is \"%s\". It read \"%s\" instead."), Int(i), colors.at(i), readMsg.at(i));
				return false;
			}
		}

		std::vector<Int> nums{4, 8, 10, -4, -1, 0, 15, 9, 100, 4, 4, 4, 4, 4, 4, 16};
		vectBuffer.EmptyBuffer();
		vectBuffer << nums;

		std::vector<Int> readNums;
		vectBuffer >> readNums;

		if (nums.size() != readNums.size())
		{
			UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original vector size is %s. Read %s elements from the data buffer."), DString::MakeString(nums.size()), DString::MakeString(readNums.size()));
			return false;
		}

		for (size_t i = 0; i < nums.size(); ++i)
		{
			if (nums.at(i) != readNums.at(i))
			{
				UnitTestError(testFlags, TXT("Failed to stream array to data buffer. The original vector at index %s was %s. It read %s from that index instead."), DString::MakeString(i), nums.at(i), readNums.at(i));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("DataBuffer Streaming"));
	{
		DString bufferAStrA = TXT("Apples");
		DString bufferAStrB = TXT("Strawberries");
		DString bufferBStrA = TXT("Red");
		DString bufferBStrB = TXT("Blue");

		DataBuffer a;
		a << bufferAStrA;
		a << bufferAStrB;

		DString b = TXT("Doodle");

		DataBuffer c;
		c << bufferBStrA;
		c << bufferBStrB;

		DString d = TXT("Cat!");

		DataBuffer testBuffer;
		testBuffer << a;
		testBuffer << b;
		testBuffer << c;
		testBuffer << d;

		DataBuffer copyA;
		DString copyB;
		DataBuffer copyC;
		DString copyD;

		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;
		testBuffer >> copyD;

		DString dataBufferContents = DString::CreateFormattedString(TXT("a DataBuffer containing (\"%s\" and \"%s\"), \"%s\", another DataBuffer containing (\"%s\" and \"%s\"), and finally a string \"%s\""), bufferAStrA, bufferAStrB, b, bufferBStrA, bufferBStrB, d);

		if (b != copyB || d != copyD)
		{
			UnitTestError(testFlags, TXT("Failed to stream DataBuffer to another DataBuffer. After creating %s, extracting strings did not match the original. Original strings: \"%s\" and \"%s\". Strings in buffer: \"%s\" and \"%s\"."), dataBufferContents, b, d, copyB, copyD);
			return false;
		}

		if (a.ReadRawData().size() != copyA.ReadRawData().size() || c.ReadRawData().size() != copyC.ReadRawData().size())
		{
			UnitTestError(testFlags, TXT("Failed to stream DataBuffer to another DataBuffer. After creating %s, the size of the DataBuffers extracted do not match. Original buffer sizes: %s, %s. Extracted buffer sizes: %s, %s."), dataBufferContents, Int(a.ReadRawData().size()), Int(c.ReadRawData().size()), Int(copyA.ReadRawData().size()), Int(copyC.ReadRawData().size()));
			return false;
		}

		for (size_t i = 0; i < a.ReadRawData().size(); ++i)
		{
			if (a.ReadRawData().at(i) != copyA.ReadRawData().at(i))
			{
				UnitTestError(testFlags, TXT("Failed to stream DataBuffer to another DataBuffer. After reading the first data buffer, there's a mismatch in data at index %s. Original byte: %s. Read byte: %s."), Int(i), DString(a.ReadRawData().at(i)), DString(copyA.ReadRawData().at(i)));
				return false;
			}
		}

		for (size_t i = 0; i < c.ReadRawData().size(); ++i)
		{
			if (c.ReadRawData().at(i) != copyC.ReadRawData().at(i))
			{
				UnitTestError(testFlags, TXT("Failed to stream DataBuffer to another DataBuffer. After reading the second data buffer, there's a mismatch in data at index %s. Original byte: %s. Read byte: %s."), Int(i), DString(c.ReadRawData().at(i)), DString(copyC.ReadRawData().at(i)));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Copying Data Buffers"));
	{
		DString originalStrA = TXT("My Eternal");
		DString originalStrB = TXT("Weaver of Hearts");
		DataBuffer originalBuffer;

		originalBuffer << originalStrA;
		originalBuffer << originalStrB;

		DataBuffer copyBuffer;
		originalBuffer.CopyBufferTo(OUT copyBuffer);

		DString cpyStrA;
		DString cpyStrB;
		copyBuffer >> cpyStrA;
		if (originalStrA.Compare(cpyStrA, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Failed to copy data buffer to another. The original string is \"%s\". Instead the copy has \"%s\""), originalStrA, cpyStrA);
			return false;
		}

		copyBuffer >> cpyStrB;
		if (originalStrB.Compare(cpyStrB, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Failed to copy data buffer to another. The original string is \"%s\". Instead the copy has \"%s\""), originalStrB, cpyStrB);
			return false;
		}

		if (!copyBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Failed to copy data buffer to another. The data buffer copy has extra data in its buffer. The reader index is not at the end after reading its expected variables."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Specialization Streaming")); //Testing serializing specific template instances that streams nonDProperties.
	{
		size_t originalA = 824;
		size_t originalB = 9821;

		DataBuffer testBuffer;
		testBuffer << originalA;
		testBuffer << originalB;

		size_t copyA;
		size_t copyB;

		testBuffer >> copyA;
		testBuffer >> copyB;

		if (copyA != originalA || copyB != originalB)
		{
			UnitTestError(testFlags, TXT("Failed to stream size_t to data buffer. The original values were %s and %s. Instead it pulled %s and %s from the data buffer."), Int(originalA), Int(originalB), Int(copyA), Int(copyB));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Append Bytes"));
	{
		//No need to test raw bytes since that was tested in the write tests. Only need to test against appending another data buffer.
		size_t a = 1324;
		DataBuffer testBuffer;
		testBuffer << a;

		size_t b = 3425;
		size_t c = 8571;
		size_t d = 285;
		DataBuffer extraBuffer;
		extraBuffer << b;
		extraBuffer << c;
		extraBuffer << d;

		testBuffer.AppendBytes(extraBuffer);

		size_t copyA;
		size_t copyB;
		size_t copyC;
		size_t copyD;

		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;
		testBuffer >> copyD;

		if (a != copyA || b != copyB || c != copyC || d != copyD)
		{
			UnitTestError(testFlags, TXT("Append Bytes data buffer test failed. After appending one data buffer to another, it's expected to contain the following variables %s, %s, %s, %s. Instead it read %s, %s, %s, %s."), Int(a), Int(b), Int(c), Int(d), Int(copyA), Int(copyB), Int(copyC), Int(copyD));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Append Bytes data buffer test failed. After appending one data buffer to another, it's expected to contain 4 variables. Since the reader is not at the end of the data buffer, it suggests that the AppendBytes function inserted extra data."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Editing Data Buffers"));
	{
		size_t a = 15;
		size_t b = 1;
		size_t originalB = b;
		size_t c = 25;

		DataBuffer testBuffer;
		testBuffer << a;
		testBuffer << b;
		testBuffer << c;

		b = 40;
		const int numBytes = sizeof(b);
		char charArray[numBytes];
		memcpy(charArray, &b, numBytes);

		//This should overwrite b
		size_t writeIdx = sizeof(a);
		testBuffer.EditBytes(writeIdx, charArray, numBytes);

		size_t copyA;
		size_t copyB;
		size_t copyC;
		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;
		if (a != copyA || b != copyB || c != copyC)
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After writing the following variables to a data buffer (%s, %s, %s), and after editing the second variable to %s. Reading from that data buffer didn't return those variables. Instead it returned (%s, %s, %s)."), Int(a), Int(originalB), Int(c), Int(b), Int(copyA), Int(copyB), Int(copyC));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After editing an existing variable and attempting to read all variables from the data buffer, the read is not at the end of the data buffer. This suggests that the test inserted extra bytes in the buffer."));
			return false;
		}

		//Attempt to overwrite c and add d to the buffer
		size_t cd[2];
		cd[0] = 100;
		cd[1] = 250;
		const int arrayNumBytes = sizeof(cd);
		char arrayBuffer[arrayNumBytes];
		memcpy(arrayBuffer, cd, arrayNumBytes);
		writeIdx = sizeof(a) + sizeof(b);
		testBuffer.EditBytes(writeIdx, arrayBuffer, arrayNumBytes); //This should overwrite c and append d to the end.
		testBuffer.JumpToBeginning();

		size_t copyD;
		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;
		testBuffer >> copyD;
		if (a != copyA || b != copyB || cd[0] != copyC || cd[1] != copyD)
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After writing the following variables to a data buffer (%s, %s, %s), and after editing the third variable to %s and append %s to the end, reading from that data buffer didn't return those variables. Instead it returned (%s, %s, %s, %s)."), Int(a), Int(b), Int(c), Int(cd[0]), Int(cd[1]), Int(copyA), Int(copyB), Int(copyC), Int(copyD));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After editing an existing variable and attempting to read all variables from the data buffer, the read is not at the end of the data buffer. This suggests that the test inserted extra bytes in the buffer."));
			return false;
		}

		size_t newB = 1538;
		size_t newC = 5821;
		size_t newD = 5815;
		size_t newE = 3916;
		DataBuffer insertBuffer;
		insertBuffer << newB;
		insertBuffer << newC;
		insertBuffer << newD;
		insertBuffer << newE;

		testBuffer.EditBytes(sizeof(a), insertBuffer);

		size_t copyE;
		testBuffer.JumpToBeginning();
		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;
		testBuffer >> copyD;
		testBuffer >> copyE;

		if (copyA != a || copyB != newB || copyC != newC || copyD != newD || copyE != newE)
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After editing this data buffer from another, the resulting buffer should have been %s, %s, %s, %s, %s instead of %s, %s, %s, %s, %s"), Int(a), Int(newB), Int(newC), Int(newD), Int(newE), Int(copyA), Int(copyB), Int(copyC), Int(copyD), Int(copyE));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Edit Data Buffer test failed. After editing this data buffer from another and after reading its contents, the data buffer's reader is not at the end suggesting that the edit added extra bytes."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Insert Data"));
	{
		size_t a = 9216;
		size_t b = 5175;
		size_t c = 1298;

		DataBuffer testBuffer;
		testBuffer << a;
		testBuffer << c;

		const size_t numBytes = sizeof(b);
		char buffer[numBytes];
		memcpy(buffer, &b, numBytes);
		size_t insertIdx = sizeof(a);
		testBuffer.InsertBytes(insertIdx, buffer, numBytes);

		size_t copyA;
		size_t copyB;
		size_t copyC;
		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;

		if (a != copyA || b != copyB || c != copyC)
		{
			UnitTestError(testFlags, TXT("Insert Data Buffer test failed. After writing %s and %s to a data buffer, and attempting to insert %s after the first number, reading from the data buffer should have produced %s, %s, %s. Instead it produced %s, %s, %s."), Int(a), Int(c), Int(b), Int(a), Int(b), Int(c), Int(copyA), Int(copyB), Int(copyC));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Insert Data Buffer test failed. After writing 2 variables, inserting 1 variable to a data buffer, then after reading 3 variables from that same data buffer, the reader index should be at the end. Instead it's somewhere in the middle suggesting extra bytes were inserted to the buffer."));
			return false;
		}

		DataBuffer extraBuffer;
		size_t preC = 8912;
		size_t preD = 7250;
		extraBuffer << preC;
		extraBuffer << preD;

		insertIdx = sizeof(a) + sizeof(b);
		testBuffer.InsertBytes(insertIdx, extraBuffer);

		size_t copyPreC;
		size_t copyPreD;

		testBuffer.JumpToBeginning();
		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyPreC;
		testBuffer >> copyPreD;
		testBuffer >> copyC;

		if (a != copyA || b != copyB || preC != copyPreC || preD != copyPreD || c != copyC)
		{
			UnitTestError(testFlags, TXT("Insert Data Buffer test failed. After inserting one data buffer into another, the resulting data buffer should have produced %s, %s, %s, %s, %s. Instead it produced %s, %s, %s, %s, %s."), Int(a), Int(b), Int(preC), Int(preD), Int(c), Int(copyA), Int(copyB), Int(copyPreC), Int(copyPreD), Int(copyC));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Insert Data Buffer test failed. After inserting one data buffer to another, the resulting data buffer should have a total of 5 variables. After reading those variables, the reader index should be at the end. Since it's not the end, the insert function could have inserted extra bytes."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Removing Data"));
	{
		size_t a = 9182;
		size_t b = 2982;
		size_t c = 2789;
		size_t d = 1324;

		DataBuffer testBuffer;
		testBuffer << a;
		testBuffer << b;
		testBuffer << c;
		testBuffer << d;

		//Remove b
		size_t removeIdx = sizeof(a);
		testBuffer.RemoveBytes(removeIdx, sizeof(b));

		size_t copyA;
		size_t copyC;
		size_t copyD;
		testBuffer >> copyA;
		testBuffer >> copyC;
		testBuffer >> copyD;

		if (a != copyA || c != copyC || d != copyD)
		{
			UnitTestError(testFlags, TXT("Remove Data Buffer test failed. After writing %s, %s, %s, %s to a data buffer and removing the second variable, reading from that buffer should have produced %s, %s, %s. Instead it produced %s, %s, %s."), Int(a), Int(b), Int(c), Int(d), Int(a), Int(c), Int(d), Int(copyA), Int(copyC), Int(copyD));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Remove Data Buffer test failed. After reading variables from the data buffer, the reader is not at the end of the buffer suggesting that the function didn't remove enough bytes."));
			return false;
		}

		//Attempt to erase way too many bytes. The function should only delete d due to removeIdx.
		removeIdx = sizeof(a) + sizeof(c);
		testBuffer.RemoveBytes(removeIdx, 1000);
		testBuffer.JumpToBeginning();
		testBuffer >> copyA;
		testBuffer >> copyC;

		if (a != copyA || c != copyC)
		{
			UnitTestError(testFlags, TXT("Remove Data Buffer test failed. After removing variable from the data buffer, reading from that buffer should have produced variables %s, %s instead of %s, %s."), Int(a), Int(c), Int(copyA), Int(copyC));
			return false;
		}

		if (!testBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Remove Data Buffer test failed. After deleting multiple variables from the data buffer, the buffer is expected to only hold 2 variables. Since the reader is not at the end, it suggests that the RemoveBytes function did not remove enough data."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Data Buffer"));

	return true;
}

bool DatatypeUnitTester::TestStringIterator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("String Iterator"));

	DString testString = TXT("DStringIterator test string!");
	StringIterator iter(&testString);
	std::vector<DString> expectedChars;
	for (Int i = 0; i < testString.Length(); i++)
	{
		expectedChars.push_back(DString(testString.At(i)));
	}

	Int idx = 0;
	SetTestCategory(testFlags, TXT("Incrementing Iterator"));
	{
		TestLog(testFlags, TXT("Using an iterator to increment through this string:  \"%s\""), testString);

		for ( /* Noop */ ; !iter.IsAtEnd(); iter++)
		{
			DString curCharacter = iter.GetString();
			TestLog(testFlags, TXT("At Idx %s, the iterator points at \"%s\""), Int(idx), curCharacter);
			if (curCharacter != expectedChars.at(idx.ToUnsignedInt()))
			{
				UnitTestError(testFlags, TXT("The iterator did not find the expected character (\"%s\") at idx %s.  Instead the iterator is returning \"%s\""), expectedChars.at(idx.ToUnsignedInt()), Int(idx), curCharacter);
				return false;
			}

			idx++;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Decrementing Iterator"));
	{
		StringIterator backwardsIterator(&testString, false); //Point to last character
		for (idx = expectedChars.size() - 1; idx >= 0; idx--)
		{
			DString curCharacter = backwardsIterator.GetString();
			TestLog(testFlags, TXT("At Idx %s, the iterator points at \"%s\""), Int(idx), curCharacter);
			if (curCharacter != expectedChars.at(idx.ToUnsignedInt()))
			{
				UnitTestError(testFlags, TXT("The iterator did not find the expected character (\"%s\") at idx %s.  Instead the iterator is returning \"%s\""), expectedChars.at(idx.ToUnsignedInt()), Int(idx), curCharacter);
				return false;
			}

			if (!backwardsIterator.IsAtBeginning())
			{
				backwardsIterator--;
			}
		}

		if (!backwardsIterator.IsAtBeginning())
		{
			UnitTestError(testFlags, TXT("Despite reaching the beginning of the expected character array, the iterator does not claim that it's at the beginning of the string."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Jumping Iterator"));
	{
		testString = TXT("Easy. No Stress.");
		DString expectedChar;
		StringIterator jumpIterator(&testString);
		bool bJumpForward;
		Int jumpAmount;

		std::function<bool()> testJumping([&]()
		{
			if (bJumpForward)
			{
				jumpIterator += jumpAmount;
			}
			else
			{
				jumpIterator -= jumpAmount;
			}

			if (jumpIterator.GetString().Compare(expectedChar, DString::CC_CaseSensitive) != 0)
			{
				DString forwardText = (bJumpForward) ? TXT("forward") : TXT("backward");
				UnitTestError(testFlags, TXT("Jump String Iterator test failed. After jumping %s %s many times, the expected selected character in the string \"%s\" should have been \"%s\". Instead it's \"%s\"."), forwardText, jumpAmount, testString, expectedChar, jumpIterator.GetString());
				return false;
			}

			return true;
		});

		bJumpForward = true;
		jumpAmount = 3;
		expectedChar = TXT("y");
		if (!testJumping())
		{
			return false;
		}

		jumpAmount = 7;
		expectedChar = TXT("t");
		if (!testJumping())
		{
			return false;
		}

		jumpAmount = 100;
		expectedChar = DString::EmptyString;
		if (!testJumping())
		{
			return false;
		}

		bJumpForward = false;
		jumpAmount = 4;
		expectedChar = TXT("e");
		if (!testJumping())
		{
			return false;
		}

		jumpAmount = 8;
		expectedChar = TXT(".");
		if (!testJumping())
		{
			return false;
		}

		jumpAmount = 100;
		expectedChar = TXT("E");
		if (!testJumping())
		{
			return false;
		}

		jumpIterator.JumpToEnd();
		if (jumpIterator.GetString().Compare(TXT("."), DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Jump String Iterator test failed. Jumping the iterator to the end of \"%s\" should have resulted in \".\". Instead it's \"%s\"."), testString, jumpIterator.GetString());
			return false;
		}

		jumpIterator.JumpToBeginning();
		if (jumpIterator.GetString().Compare(TXT("E"), DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Jump String Iterator test failed. Jumping the iterator to the beginning of \"%s\" should have resulted in \"E\". Instead it's \"%s\"."), testString, jumpIterator.GetString());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("String Iterator"));

	return true;
}

bool DatatypeUnitTester::TestCharacterEncoding (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Character Encoding"));

	bool bTestUnicode = true;
#if USE_UTF32
	TestLog(testFlags, TXT("Character encoding is UTF-32"));
#elif USE_UTF16
	TestLog(testFlags, TXT("Character encoding is UTF-16"));
#elif USE_UTF8
	TestLog(testFlags, TXT("Character encoding is UTF-8"));
#else
	TestLog(testFlags, TXT("Not using character encoding.  DStrings are ANSI."));
	bTestUnicode = false;
#endif

	if (!bTestUnicode)
	{
		TestLog(testFlags, TXT("Skipping character encoding test."));
		ExecuteSuccessSequence(testFlags, TXT("Character Encoding"));
		return true;
	}

	SetTestCategory(testFlags, TXT("Built in Conversions"));
	{
		//Strings copied from: https://en.cppreference.com/w/cpp/locale/wstring_convert/to_bytes
		std::wstring wstr = L"z\u00df\u6c34";
		DString resultingStr;
		resultingStr.FromWideString(wstr);
		DString expected = TXT("z\u00df\u6c34");
		if (resultingStr != expected)
		{
			UnitTestError(testFlags, TXT("Built in conversions test failed. Failed to convert wstring to the expected string \"%s\""), expected);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Unicode String Iterator"));

	//Note:  When debugging the string values in Visual Studio, you may have to append ",s8" in the watch window's variable names to view the char string in UTF-8.
	//to view UTF-16, enter ',s' at the end of variable name.  And ',s32' for UTF-32 strings.
	DString testString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = cosmos"); //Greek word for kosme

	//It's important to test the string iterator since many string utilities (such as FindAt and Find) depends on the string iterator.
	TestLog(testFlags, TXT("Using a string iterator it iterate through %s characters."), DString(STRING_CONFIG_NAME));
	StringIterator testIter(&testString);
	std::vector<DString> expectedCharacters;
	expectedCharacters.push_back(TXT("\u03ba"));
	expectedCharacters.push_back(TXT("\u1f79"));
	expectedCharacters.push_back(TXT("\u03c3"));
	expectedCharacters.push_back(TXT("\u03bc"));
	expectedCharacters.push_back(TXT("\u03b5"));
	expectedCharacters.push_back(TXT(" "));
	expectedCharacters.push_back(TXT("="));
	expectedCharacters.push_back(TXT(" "));
	expectedCharacters.push_back(TXT("c"));
	expectedCharacters.push_back(TXT("o"));
	expectedCharacters.push_back(TXT("s"));
	expectedCharacters.push_back(TXT("m"));
	expectedCharacters.push_back(TXT("o"));
	expectedCharacters.push_back(TXT("s"));
	for (UINT_TYPE charIdx = 0; charIdx < expectedCharacters.size(); charIdx++)
	{
		if (testIter.IsAtEnd())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("String iterator failed to iterate through %s characters at idx %s since it reached to the end of the string early."), DString(STRING_CONFIG_NAME), DString::MakeString(charIdx));
			return false;
		}

		if (testIter.GetString() != expectedCharacters.at(charIdx))
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("String iterator failed to retrieve the expected character at idx %s."), DString::MakeString(charIdx));
			return false;
		}
		++testIter;
	}

	//test backwards iterator
	StringIterator reverseTestIter(&testString, false);
	for (Int charIdx = expectedCharacters.size() - 1; charIdx >= 0; charIdx--)
	{
		if (charIdx > 0 && reverseTestIter.IsAtBeginning())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator failed to iterate through %s characters at idx %s since it reached to the beginning of the string early."), DString(STRING_CONFIG_NAME), charIdx);
			return false;
		}
		else if (charIdx == 0 && !reverseTestIter.IsAtBeginning())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator test failed since the string iterator is not returning that it's at the beginning of the string despite the character index being equal to %s."), charIdx);
			return false;
		}

		if (reverseTestIter.GetString() != expectedCharacters.at(charIdx.ToUnsignedInt()))
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator failed to retrieve the expected character at idx %s."), charIdx);
			return false;
		}

		if (charIdx > 0)
		{
			--reverseTestIter;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encoding Validation"));
	DString validString = testString;
	if (!validString.IsValidEncoding())
	{
		UnitTestError(testFlags, TXT("The valid " STRINGIFY(STRING_CONFIG_NAME) " string is considered not valid."));
		return false;
	}

	std::vector<DString> invalidStrings;
#if USE_UTF32
	#error Please populate the invalidStrings vector for UTF-32.
#elif USE_UTF16
	#error Please populate the invalidStrings vector for UTF-16.
#elif USE_UTF8
	//These strings are copied from:  http://stackoverflow.com/questions/1301402/example-invalid-utf8-string
	invalidStrings.push_back("\xc3\x28");
	invalidStrings.push_back("\xa0\xa1");
	invalidStrings.push_back("\xe2\x28\xa1");
	invalidStrings.push_back("\xe2\x82\x28");
	invalidStrings.push_back("\xf0\x28\x8c\xbc");
	invalidStrings.push_back("\xf0\x90\x28\xbc");
	invalidStrings.push_back("\xf0\x28\x8c\x28");
#endif
	for (UINT_TYPE i = 0; i < invalidStrings.size(); i++)
	{
		if (invalidStrings.at(i).IsValidEncoding())
		{
			UnitTestError(testFlags, TXT("An invalid string at idx %s is considered to be a valid " STRINGIFY(STRING_CONFIG_NAME) " string."), DString::MakeString(i));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encoding Conversions"));
	StringUTF8 utf8 = testString.ToUTF8();
	StringUTF16 utf16 = testString.ToUTF16();
	StringUTF32 utf32 = testString.ToUTF32();
	TestLog(testFlags, TXT("The value of the test string is a Greek word for kosme (cosmos).  \"%s\""), testString);
	if (!testString.IsValidEncoding())
	{
		UnitTestError(testFlags, TXT("Failed to construct a UTF-8 string:  %s"), testString);
		return false;
	}

	//Length function should return the expected value
	TestLog(testFlags, TXT("The length of \"%s\" is:  %s"), testString, testString.Length());
	if (testString.Length() != 14)
	{
		UnitTestError(testFlags, TXT("The length of \"%s\" is not the expected value of 14.  Instead it's reporting that the length of that string is %s"), testString, testString.Length());
		return false;
	}

	//The converted strings should report the same lengths as the original string
	if (testString.Length() != DString::UTF32Length(utf32))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-32 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), testString.Length(), DString::UTF32Length(utf32));
		return false;
	}

	if (testString.Length() != DString::UTF16Length(utf16))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-16 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), testString.Length(), DString::UTF16Length(utf16));
		return false;
	}

	if (testString.Length() != DString::UTF8Length(utf8))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-8 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), testString.Length(), DString::UTF16Length(utf16));
		return false;
	}

	//When converting from UTF-x back to its original encoding, it should be equal to the same value as original string.
	DString convertedFromUTF32(utf32);
	if (convertedFromUTF32 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-32 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-32 and back to original."));

	DString convertedFromUTF16(utf16);
	if (convertedFromUTF16 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-16 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-16 and back to original."));

	DString convertedFromUTF8(utf8);
	if (convertedFromUTF8 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-8 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-8 and back to original."));

	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Utilities"));
	{
		std::function<bool(const DString&)> isValidString([&](const DString& testString)
		{
			bool result = testString.IsValidEncoding();
			if (!result)
			{
				UnitTestError(testFlags, TXT("The string \"%s\" contains invalid characters."), testString);
			}

			return result;
		});

		DString expectedResult = TXT("\u1f79");
		DString result = testString.FindAt(1);
		if (!isValidString(result))
		{
			return false;
		}

		TestLog(testFlags, TXT("The character of \"%s\" at index 1 is:  \"%s\""), testString, result);
		if (result != expectedResult)
		{
			UnitTestError(testFlags, TXT("Failed to obtain the 2nd character from \"%s\".  It returned \"%s\" instead."), testString, result);
			return false;
		}

		expectedResult = TXT("c");
		result = testString.FindAt(8);
		if (!isValidString(result))
		{
			return false;
		}

		TestLog(testFlags, TXT("The character of \"%s\" at index 8 is:  \"%s\""), testString, result);
		if (result != expectedResult)
		{
			UnitTestError(testFlags, TXT("Failed to obtain the 9th character from \"%s\".  It returned \"%s\" instead."), testString, result);
			return false;
		}

		//Test to upper (Find test depends on case insensitive comparison)
		DString toUpperString = testString;
		toUpperString.ToUpper();
		if (!isValidString(toUpperString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Converting \"%s\" to upper case is \"%s\""), testString, toUpperString);
		DString expectedString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = COSMOS");
		if (toUpperString != expectedString)
		{
			UnitTestError(testFlags, TXT("Failed to convert \"%s\" to upper case since it resulted in \"%s\"."), testString, toUpperString);
			return false;
		}

		DString toLowerString = toUpperString;
		toLowerString.ToLower();
		if (!isValidString(toLowerString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Converting \"%s\" to lower case is \"%s\""), toUpperString, toLowerString);
		expectedString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = cosmos");
		if (toLowerString != expectedString)
		{
			UnitTestError(testFlags, TXT("Failed to convert \"%s\" to lower case since it resulted in \"%s\"."), toUpperString, toLowerString);
			return false;
		}

		if (toLowerString.Compare(toUpperString, DString::CC_IgnoreCase) != 0)
		{
			UnitTestError(testFlags, TXT("Failed to compare strings:  \"%s\" and \"%s\".  Comparing those two (case ignored), this function should have returned 0."), toUpperString, toLowerString);
			return false;
		}

		if (testString.Compare(toUpperString, DString::CC_CaseSensitive) == 0)
		{
			UnitTestError(testFlags, TXT("Failed to compare strings:  \"%s\" and \"%s\".  Comparing those two (case sensitive), this function should have returned non-zero."), testString, toUpperString);
			return false;
		}

		if (testString == toUpperString)
		{
			UnitTestError(testFlags, TXT("The DString's == operator should be case sensitive.  However, comparing \"%s\" to \"%s\" returned true instead of false."), testString, toUpperString);
			return false;
		}

		DString searchString = TXT("cosmos");
		Int foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 8)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 8."), searchString, testString, foundIdx);
			return false;
		}

		if (!testString.Contains(searchString, DString::CC_CaseSensitive))
		{
			UnitTestError(testFlags, TXT("Contains test failed. Although \"%s\" was found in \"%s\", Contains returned false."), searchString, testString);
			return false;
		}

		TStringChar searchChar = 'o';
		foundIdx = testString.Find(searchChar, 2, DString::SD_LeftToRight);
		if (foundIdx != 9)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index. When searching for the character '%s' within \"%s\", Find returned %s instead of 9."), DString(searchChar), testString, foundIdx);
			return false;
		}

		foundIdx = testString.Find(searchChar, 0, DString::SD_RightToLeft);
		if (foundIdx != 12)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index. When searching for the character '%s' within \"%s\" from right to left, Find returned %s instead of 12."), DString(searchChar), testString, foundIdx);
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 8)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 8."), searchString, testString, foundIdx);
			return false;
		}

		if (!testString.Contains(searchString, DString::CC_IgnoreCase))
		{
			UnitTestError(testFlags, TXT("Contains test failed. Although \"%s\" was found in \"%s\", Contains returned false."), searchString, testString);
			return false;
		}

		searchString = TXT("\u03bc");
		foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 3)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 3."), searchString, testString, foundIdx);
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 3)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 3."), searchString, testString, foundIdx);
			return false;
		}

		searchString = TXT("\u1f79\u03c3\u03bc\u03b5");
		foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 1)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 1."), searchString, testString, foundIdx);
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 1)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 1."), searchString, testString, foundIdx);
			return false;
		}

		searchString = TXT("o");
		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 12)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 12."), searchString, testString, foundIdx);
			return false;
		}

		foundIdx = testString.Find(searchString, 2, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right starting from position 2) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 9)
		{
			UnitTestError(testFlags, TXT("Find failed to find the expected index.  When searching for \"%s\" within \"%s\", Find returned %s instead of 9."), searchString, testString, foundIdx);
			return false;
		}

		searchString = TXT("DOESN'T_EXIST");
		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase);
		if (foundIdx != INT_INDEX_NONE)
		{
			UnitTestError(testFlags, TXT("Find test failed. It returned index %s when searching for \"%s\" within \"%s\"."), foundIdx, searchString, testString);
			return false;
		}

		if (testString.Contains(searchString, DString::CC_IgnoreCase))
		{
			UnitTestError(testFlags, TXT("Contains test failed. \"%s\" was considered to be contained in \"%s\" when it shouldn't have."), searchString, testString);
			return false;
		}

		//Testing StartsWith and EndsWith
		{
			DString baseString = TXT("I am the knife in the darkness and you are my light. Trust in me like how I trust you. Doing all of this...and you being at my side when I do work is what makes me happy."); //-Yohkuj
			DString searchPrefix = TXT("I am the knife in the darkness and you are my light.");
			if (!baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should have started with \"%s\"."), baseString, searchPrefix);
				return false;
			}

			searchPrefix = TXT("You are the knife");
			if (baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" does not start with \"%s\"."), baseString, searchPrefix);
				return false;
			}

			DString searchSuffix = TXT("you being at my side when I do work is what makes me happy.");
			if (!baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" should have ended with \"%s\"."), baseString, searchSuffix);
				return false;
			}

			searchSuffix = TXT("work makes me happy.");
			if (baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" does not end with \"%s\"."), baseString, searchSuffix);
				return false;
			}

			if (!baseString.StartsWith('I'))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should start with 'I'."), baseString);
				return false;
			}

			if (!baseString.EndsWith('.'))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" should have ended with '.'."), baseString);
				return false;
			}

			//Test case where the string is longer than the base string.
			baseString = TXT("Weaver of Hearts");
			searchPrefix = TXT("Weaver of Hearts!");
			if (baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should not start with \"%s\"."), baseString, searchPrefix);
				return false;
			}

			searchSuffix = TXT("The Weaver of Hearts");
			if (baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" does not end with \"%s\"."), baseString, searchSuffix);
				return false;
			}
		}

		//Testing popback
		{
			const DString originalStr = TXT("Artorias Mistwalker");
			DString popBackStr(originalStr);
			const std::vector<DString> expectedStrs
			{
				TXT("Artorias Mistwalker"),
				TXT("Artorias Mistwalke"),
				TXT("Artorias Mistwalk"),
				TXT("Artorias Mistwal"),
				TXT("Artorias Mistwa"),
				TXT("Artorias Mistw"),
				TXT("Artorias Mist"),
				TXT("Artorias Mis"),
				TXT("Artorias Mi"),
				TXT("Artorias M"),
				TXT("Artorias "),
				TXT("Artorias"),
				TXT("Artoria"),
				TXT("Artori"),
				TXT("Artor"),
				TXT("Arto"),
				TXT("Art"),
				TXT("Ar"),
				TXT("A")
			};

			Int popCounter = 0;
			for (size_t i = 0; i < expectedStrs.size(); ++i)
			{
				if (popBackStr.Compare(expectedStrs.at(i), DString::CC_CaseSensitive) != 0)
				{
					UnitTestError(testFlags, TXT("\"%s\" does not match \"%s\" after popping \"%s\" %s times."), popBackStr, expectedStrs.at(i), originalStr, popCounter);
					return false;
				}

				popBackStr.PopBack();
				++popCounter;
			}

			if (!popBackStr.IsEmpty())
			{
				UnitTestError(testFlags, TXT("Popping \"%s\" %s times did not end with an empty string. There are still \"%s\" remaining."), originalStr, popCounter, popBackStr);
				return false;
			}
		}

		//Test Remove since Format string depends on it.
		DString removeTest = testString;
		removeTest.Remove(8, 3);
		if (!isValidString(removeTest))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = mos");
		TestLog(testFlags, TXT("Removing \"cos\" from \"%s\" results in \"%s\"."), testString, removeTest);
		if (removeTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Removing characters from string test failed.  Removing 3 characters from index 8 from string \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), testString, expectedResult, removeTest);
			return false;
		}

		removeTest = testString;
		removeTest.Remove(0, 4);
		if (!isValidString(removeTest))
		{
			return false;
		}

		expectedResult = TXT("\u03b5 = cosmos");
		TestLog(testFlags, TXT("Removing \"\u03ba\u1f79\u03c3\u03bc\" from \"%s\" results in \"%s\"."), testString, removeTest);
		if (removeTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Removing characters from string test failed.  Removing 4 characters from index 0 from string \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), testString, expectedResult, removeTest);
			return false;
		}

		removeTest.Remove(7, -1); //Remove the 'mos' at the end. Negative count means it'll remove all characters following from start position.
		expectedResult = TXT("\u03b5 = cos");
		if (removeTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Removing characters from string test failed. Removing the rest of the characters starting from index 6 from string \"%s\" should have resulted in \"%s\". Instead it resulted in \"%s\"."), testString, expectedResult, removeTest);
			return false;
		}

		//Test Insert since Format string depends on it.
		DString baseString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 cosmos");
		DString insertTest = baseString;
		DString insertingString = TXT("is equal to ");
		insertTest.Insert(6, insertingString);
		if (!isValidString(insertTest))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 is equal to cosmos");
		TestLog(testFlags, TXT("Inserting \"%s\" at index 6 of string \"%s\" results in \"%s\"."), insertingString, baseString, insertTest);
		if (insertTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Inserting the string \"%s\" at index 6 of \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), insertingString, baseString, expectedResult, insertTest);
			return false;
		}

		baseString = TXT("\u03ba = cosmos");
		insertTest = baseString;
		insertingString = TXT("\u1f79\u03c3\u03bc\u03b5");
		insertTest.Insert(1, insertingString);
		if (!isValidString(insertTest))
		{
			return false;
		}

		expectedResult = testString;
		TestLog(testFlags, TXT("Inserting \"%s\" at index 1 of string \"%s\" results in \"%s\"."), insertingString, baseString, insertTest);
		if (insertTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Inserting the string \"%s\" at index 1 of \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), insertingString, baseString, expectedResult, insertTest);
			return false;
		}

		//Test Format string since logs depends on it.
		DString ansiReplaceString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = %s%s%s%s%s%s");
		DString formattedString = DString::CreateFormattedString(ansiReplaceString, DString(TXT("c")), DString(TXT("o")), DString(TXT("s")), DString(TXT("m")), DString(TXT("o")), DString(TXT("s")));
		if (!isValidString(formattedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing s macros with the word cosmos in string \"%s\" resulted in \"%s\""), ansiReplaceString, formattedString);
		if (formattedString != testString)
		{
			UnitTestError(testFlags, TXT("Format string test failed since it returned a string that doesn't match the expected string \"%s\".  Instead it returned \"%s\"."), testString, formattedString);
			return false;
		}

		DString unicodeReplaceString = TXT("%s%s%s%s%s = cosmos");
		formattedString = DString::CreateFormattedString(unicodeReplaceString, DString(TXT("\u03ba")), DString(TXT("\u1f79")), DString(TXT("\u03c3")), DString(TXT("\u03bc")), DString(TXT("\u03b5")));
		if (!isValidString(formattedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing s macros with the word \u03ba\u1f79\u03c3\u03bc\u03b5 in string \"%s\" resulted in \"%s\""), unicodeReplaceString, formattedString);
		if (formattedString != testString)
		{
			UnitTestError(testFlags, TXT("Format string test failed since it returned a string that doesn't match the expected string \"%s\".  Instead it returned \"%s\"."), testString, formattedString);
			return false;
		}

		DString subString = testString.SubString(8);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("The Substring of \"%s\" at index 8 to the end is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string test failed.  The sub string of \"%s\" from index 8 to end should have resulted in \"%s\".  Instead it resulted in \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		subString = testString.SubString(1, 8);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u1f79\u03c3\u03bc\u03b5 = c");
		TestLog(testFlags, TXT("The Substring of \"%s\" from indices 1-8 is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string test failed.  The sub string of \"%s\" from indices 1-8 should have resulted in \"%s\".  Instead it resulted in \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		subString = testString.SubStringCount(8, -1);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("The SubstringCount of \"%s\" from index 8 to the end is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string count test failed.  The sub string of \"%s\" from index 8 to the end should have resulted in \"%s\".  Instead it resulted in \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		subString = testString.SubStringCount(0, 5);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		TestLog(testFlags, TXT("Using SubStringCount, the first 5 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string count test failed.  The first 5 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		//Test Left and Right
		subString = testString.Left(5);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		TestLog(testFlags, TXT("Using Left, the first 5 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("The first 5 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		subString = testString.Right(6);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("Using Right, the last 6 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("The last 6 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), testString, expectedResult, subString);
			return false;
		}

		//Test Split String
		DString segment1;
		DString segment2;
		DString expectedSeg1 = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 ");
		DString expectedSeg2 = TXT(" cosmos");
		testString.SplitString(6, segment1, segment2);
		if (!isValidString(segment1) || !isValidString(segment2))
		{
			return false;
		}

		TestLog(testFlags, TXT("Splitting the string \"%s\" at index 6 resulted in two the segments:  \"%s\" and \"%s\"."), testString, segment1, segment2);
		if (segment1 != expectedSeg1 || segment2 != expectedSeg2)
		{
			UnitTestError(testFlags, TXT("Split String test failed.  When splitting the string \"%s\" at index 6, the resulting segments should have been \"%s\" and \"%s\".  Instead, the resulting segments are \"%s\" and \"%s\"."), testString, expectedSeg1, expectedSeg2, segment1, segment2);
			return false;
		}

		//Test parsing string
		std::vector<DString> testStringParts;
		testString.ParseString(' ', testStringParts, true);
		std::vector<DString> expectedParts;
		expectedParts.push_back(TXT("\u03ba\u1f79\u03c3\u03bc\u03b5"));
		expectedParts.push_back(TXT("="));
		expectedParts.push_back(TXT("cosmos"));
		TestLog(testFlags, TXT("Splitting \"%s\" so that each segment between spaces is its own vector resulted in this vector:  %s."), testString, DString::ListToString(testStringParts));
		if (testStringParts.size() != expectedParts.size())
		{
			UnitTestError(testFlags, TXT("Parse String test failed.  Splitting \"%s\" by the space character should have resulted in a vector of size %s.  Instead the resulting vector size is %s."), testString, DString::MakeString(expectedParts.size()), DString::MakeString(testStringParts.size()));
			return false;
		}

		for (UINT_TYPE i = 0; i < expectedParts.size(); i++)
		{
			if (expectedParts.at(i) != testStringParts.at(i))
			{
				UnitTestError(testFlags, TXT("Parse String test failed.  After splitting \"%s\" by the space character, the expected vector at index %s should have resulted in \"%s\".  Instead the vector at index %s is actually \"%s\"."), testString, DString::MakeString(i), expectedParts.at(i), DString::MakeString(i), testStringParts.at(i));
				return false;
			}
		}

		//Test IsWrappedByChar
		Bool bIsWrapped = testString.IsWrappedByChar('o', 13); //Test the last s after the second o.
		TestLog(testFlags, TXT("The last 's' in \"%s\" surrounded by 'o'?  %s"), testString, bIsWrapped);
		if (bIsWrapped)
		{
			UnitTestError(testFlags, TXT("IsWrappedByChar test failed.  The last s in \"%s\" should not be considered that it's surrounded by o's."), testString);
			return false;
		}

		bIsWrapped = testString.IsWrappedByChar('o', 10); //Test the s after the first o.
		TestLog(testFlags, TXT("The 's' after the first 'o' in \"%s\" surrounded by 'o'?  %s"), testString, bIsWrapped);
		if (!bIsWrapped)
		{
			UnitTestError(testFlags, TXT("IsWrappedByChar test failed.  The s after the first o in \"%s\" should be considered that it's surrounded by o's."), testString);
			return false;
		}

		//Test ReplaceInline
		DString replacedString = testString;
		DString greekCosmos = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		DString englishCosmos = TXT("cosmos");
		replacedString.ReplaceInline(greekCosmos, englishCosmos, DString::CC_CaseSensitive);
		expectedResult = TXT("cosmos = cosmos");
		if (!isValidString(replacedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing \"%s\" in \"%s\" with \"%s\" resulted in \"%s\"."), greekCosmos, testString, englishCosmos, replacedString);
		if (replacedString != expectedResult)
		{
			UnitTestError(testFlags, TXT("String replace test failed.  Replacing \"%s\" in \"%s\" with \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), greekCosmos, testString, englishCosmos, expectedResult, replacedString);
			return false;
		}

		replacedString = testString;
		replacedString.ReplaceInline(englishCosmos, greekCosmos, DString::CC_CaseSensitive);
		expectedResult = greekCosmos + TXT(" = ") + greekCosmos;
		if (!isValidString(replacedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing \"%s\" in \"%s\" with \"%s\" resulted in \"%s\"."), englishCosmos, testString, greekCosmos, replacedString);
		if (replacedString != expectedResult)
		{
			UnitTestError(testFlags, TXT("String replace test failed.  Replacing \"%s\" in \"%s\" with \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), englishCosmos, testString, greekCosmos, expectedResult, replacedString);
			return false;
		}

		//Test FindFirstMismatchingIdx
		DString testMismatchIdxString = TXT("|||") + testString + TXT("|||||");
		searchString = TXT("|||");
		Int mismatchIdx = testMismatchIdxString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_LeftToRight);
		TestLog(testFlags, TXT("Searching for the first mismatching character when searching \"%s\" within \"%s\" resulted in %s."), searchString, testMismatchIdxString, mismatchIdx);
		if (mismatchIdx != 3)
		{
			UnitTestError(testFlags, TXT("Failed to find the first mismatching character.  When searching for the first the first character that does not match \"%s\" within \"%s\", it should have returned 3.  Instead it returned %s."), searchString, testMismatchIdxString, mismatchIdx);
			return false;
		}

		mismatchIdx = testMismatchIdxString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching from the right for the first mismatching character when searching \"%s\" within \"%s\" resulted in %s."), searchString, testMismatchIdxString, mismatchIdx);
		if (mismatchIdx != 16)
		{
			UnitTestError(testFlags, TXT("Failed to find the first mismatching character when searching from the right.  When searching from the right for the first the first character that does not match \"%s\" within \"%s\", it should have returned 16.  Instead it returned %s."), searchString, testMismatchIdxString, mismatchIdx);
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Character Encoding"));

	return true;
}

bool DatatypeUnitTester::TestBool (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Bool"));

	TestLog(testFlags, TXT("NOTE:  booleans may be displayed incorrectly since the localization module was not yet tested."));

	Bool blankBool(false);
	Bool initBool(true);
	Bool copiedBool(initBool);
	Bool stringBool(TXT("True"));

	TestLog(testFlags, TXT("Constructor tests:  BlankBool=%s"), blankBool);
	TestLog(testFlags, TXT("    initializedBool(true)=%s"), initBool);
	TestLog(testFlags, TXT("    copiedBool from initializedBool=%s"), copiedBool);
	TestLog(testFlags, TXT("    stringBool(\"true\")=%s"), stringBool);

	SetTestCategory(testFlags, TXT("Comparison"));
	Bool trueBool = true;
	Bool falseBool = false;
	TestLog(testFlags, TXT("True is returning %s"), trueBool);
	if (!trueBool || falseBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  Expected bool conditions (true) and (false) did not return the right conditions.  They returned  %s and %s."), trueBool, falseBool);
		return false;
	}

	TestLog(testFlags, TXT("Testing bool comparison (true == false)?"), Bool(trueBool == falseBool));
	if (trueBool == falseBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should not equal to false."));
		return false;
	}

	if (trueBool != true)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
		return false;
	}

	if (true != trueBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Parse String"));
	{
		DString testStr;
		Bool expected;
		std::function<bool()> testParseString([&]()
		{
			Bool convertedBool;
			convertedBool.ParseString(testStr);
			if (convertedBool != expected)
			{
				UnitTestError(testFlags, TXT("Bool test failed. Parsing the string \"%s\" to a Bool should have generated %s. Instead it generated %s."), testStr, expected, convertedBool);
				return false;
			}

			return true;
		});

		testStr = TXT("True");
		expected = true;
		if (!testParseString())
		{
			return false;
		}

		testStr = TXT("true");
		if (!testParseString())
		{
			return false;
		}

		testStr = TXT("TRUE");
		if (!testParseString())
		{
			return false;
		}

		testStr = TXT("False");
		expected = false;
		if (!testParseString())
		{
			return false;
		}

		testStr = TXT("false");
		if (!testParseString())
		{
			return false;
		}

		testStr = TXT("FALSE");
		if (!testParseString())
		{
			return false;
		}

		expected = true;
		testStr = expected.ToString();
		if (!testParseString())
		{
			return false;
		}

		expected = false;
		testStr = expected.ToString();
		if (!testParseString())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		std::vector<Bool> bools = {true, false, false, true, true, false, true, false};
		std::vector<Bool> readBools;
		DataBuffer boolBuffer;

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			boolBuffer << bools.at(i);
		}

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			Bool newBool;
			boolBuffer >> newBool;
			readBools.push_back(newBool);
		}

		if (bools.size() != readBools.size())
		{
			UnitTestError(testFlags, TXT("Bool streaming test failed.  After pushing %s bools to a data buffer.  %s bools were pulled from that data buffer."), DString::MakeString(bools.size()), DString::MakeString(readBools.size()));
			return false;
		}

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			if (bools.at(i) != readBools.at(i))
			{
				UnitTestError(testFlags, TXT("Bool streaming test failed.  There's a bool mismatch at index %s.  The bool %s was pushed to that data buffer, but a %s was pulled from there instead."), DString::MakeString(i), bools.at(i), readBools.at(i));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Bool"));

	return true;
}

bool DatatypeUnitTester::TestInt (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Int"));

	//Ensure the INDEX_NONE macro has negative values for signed ints, and for unsigned int, equal to max possible value.
	Int signedNullIndex = INDEX_NONE;
	if (signedNullIndex >= 0)
	{
		UnitTestError(testFlags, TXT("The INDEX_NONE macro should be a negative value for signed integers.  Instead it's equal to:  %s"), signedNullIndex);
		return false;
	}

	size_t unsignedNullIndex = INDEX_NONE;
	if (unsignedNullIndex != SIZE_MAX)
	{
		UnitTestError(testFlags, TXT("The INDEX_NONE macro should be the highest possible value for unsigned integers.  The macro is equal to %s instead of %s"), DString::MakeString(unsignedNullIndex), DString::MakeString(SIZE_MAX));
		return false;
	}

	Int blankInt(0);
	Int initializedInt(15);
	Int copiedInt(initializedInt);

	TestLog(testFlags, TXT("Constructor tests:  BlankInt=") + blankInt.ToString());
	TestLog(testFlags, TXT("    initializedInt(15)= ") + initializedInt.ToString());
	TestLog(testFlags, TXT("    copiedInt from initializedInt= ") + copiedInt.ToString());

#ifdef PLATFORM_64BIT
	if (sizeof(blankInt.Value) != 8)
	{
		UnitTestError(testFlags, TXT("Ints are not 64 bit."));
		return false;
	}
#else
	if (sizeof(blankInt.Value) != 4)
	{
		UnitTestError(testFlags, TXT("Ints are not 32 bit."));
		return false;
	}
#endif

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		TestLog(testFlags, TXT("blankInt != 0?  %s"), Bool(blankInt != 0));
		if (blankInt != 0)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  blankInt is not zero.  Instead it's:  ") + blankInt.ToString());
			return false;
		}

		TestLog(testFlags, TXT("initializedInt(%s) != copiedInt(%s) ? %s"), initializedInt, copiedInt, Bool(initializedInt != copiedInt));
		if (initializedInt != copiedInt)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedInt != copiedInt.  Values are:  %s and %s"), initializedInt, copiedInt);
			return false;
		}

		Int testInt = 5;
		TestLog(testFlags, TXT("5 < 6 ? %s"), Bool(testInt < 6));
		if (!(testInt < 6))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than 6."), testInt);
			return false;
		}

		TestLog(testFlags, TXT("5 <= 5 ? %s"), Bool(testInt <= 5));
		if (!(testInt <= 5))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than or equal to 5."), testInt);
			return false;
		}

		TestLog(testFlags, TXT("5 > 3 ? %s"), Bool(testInt > 3));
		if (!(testInt > 3))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not greater than 3."), testInt);
			return false;
		}

		TestLog(testFlags, TXT("5 >= 7 ? %s"), Bool(testInt >= 7));
		if (testInt >= 7)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is greater than or equal to 7."), testInt);
			return false;
		}

		TestLog(testFlags, TXT("5 >= 3 && 5 < 7 && 5 == 5 && (5 == 10 || 5 != 9) ? %s"), Bool(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)));
		if (!(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testInt is:  %s"), testInt);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		Int testInt = 10 + 8;
		TestLog(testFlags, TXT("10 + 8 = %s"), testInt);
		if (testInt != 18)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 18.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 10 + initializedInt;
		TestLog(testFlags, TXT("10 + 15 = %s"), testInt);
		if (testInt != 25)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), testInt);
			return false;
		}

		testInt++;
		TestLog(testFlags, TXT("Increment from 25 leads to:  %s"), testInt);
		if (testInt != 26)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 26.  It's currently:  %s"), testInt);
			return false;
		}

		--testInt;
		TestLog(testFlags, TXT("Decrement from 26 leads to:  %s"), testInt);
		if (testInt != 25)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = initializedInt - 5;
		TestLog(testFlags, TXT("15 - 5 = %s"), testInt);
		if (testInt != 10)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"),  testInt);
			return false;
		}

		testInt += 13;
		TestLog(testFlags, TXT("10 += 13 ---> %s"), testInt);
		if (testInt != 23)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = initializedInt * 2;
		TestLog(testFlags, TXT("15 * 2 = %s"), testInt);
		if (testInt != 30)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 30.  It's currently:  %s"), testInt);
			return false;
		}

		testInt /= 3;
		TestLog(testFlags, TXT("30 /= 3 ---> %s"), testInt);
		if (testInt != 10)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), testInt);
			return false;
		}

		testInt += ((20 - initializedInt) * 4) + 3;
		TestLog(testFlags, TXT("10 += ((20 - 15) * 4) + 3 results in:  %s"), testInt);
		if (testInt != 33)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 33.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 20;
		testInt %= 3;
		TestLog(testFlags, TXT("20 % 3 = %s"), testInt);
		if (testInt != 2)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 2.  It's currently:  %s"), testInt);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Bitwise Operation"));
	{
		Int testInt = 14;
		testInt = testInt >> 2;
		TestLog(testFlags, TXT("14 >> 2 = %s"), testInt);
		if (testInt != 3)
		{
			UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 3.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 9;
		testInt = testInt << 2;
		TestLog(testFlags, TXT("9 << 2 = %s"), testInt);
		if (testInt != 36)
		{
			UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 36.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 13;
		testInt &= 11;
		TestLog(testFlags, TXT("13 & 11 = %s"), testInt);
		if (testInt != 9)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 9.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 13;
		testInt = testInt | 3;
		TestLog(testFlags, TXT("13 | 3 = %s"), testInt);
		if (testInt != 15)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 15.  It's currently:  %s"), testInt);
			return false;
		}

		testInt = 13;
		testInt ^= 7;
		TestLog(testFlags, TXT("13 ^ 3 = %s"), testInt);
		if (testInt != 10)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 10.  It's currently:  %s"), testInt);
			return false;
		}

		//Test bit inversion
		Int originalInt = 10;
		testInt = ~originalInt;
		Int expected = -11;
		if (testInt != expected)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed. The expected value of ~%s should have been %s. Instead it's %s."), originalInt, expected, testInt);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Typecasting"));
	{
		TestLog(testFlags, TXT("Int to unsigned int check. . ."));
		Int testInt = -5;
		unsigned int testUnsignedInt = testInt.ToUnsignedInt32();
		TestLog(testFlags, TXT("Int(-5) converted to unsigned int is:  %s"), DString::MakeString(testUnsignedInt));
		if (testUnsignedInt != 0)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting unsigned int is:  0"));
			return false;
		}

		testInt = 9001;
		testUnsignedInt = testInt.ToUnsignedInt32();
		TestLog(testFlags, TXT("Int(9001) converted to unsigned int is:  %s"),DString::MakeString(testUnsignedInt));
		if (testUnsignedInt != 9001)
		{
			UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting unsigned int is:  9001"));
			return false;
		}

		TestLog(testFlags, TXT("Unsigned int to Int check. . ."));
	#ifdef PLATFORM_64BIT
		unsigned long long testOverflow = 15000111222333444555;
	#else
		unsigned int testOverflow = 3456789012;
	#endif
		testInt = testOverflow;
		if (testInt != SD_MAXINT)
		{
			UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting Int is:  %s. Instead it is %s."), Int(SD_MAXINT), testInt);
			return false;
		}

		testUnsignedInt = 1337;
		testInt = testUnsignedInt;
		TestLog(testFlags, TXT("UnsignedInt(1337) to Int is:  %s"), testInt);
		if (testInt != 1337)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting Int is:  1337"));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Int Streaming"));
	{
		//Test against a lot of numbers to ensure the compression works well.
		std::vector<Int> testInts({0, 1, 2, 3, 4, 5, 7, 8, 9, -1, -2, -3, -4, -5, -7, -8, -9, 31, 32, 33, 4090, 4096, 4097, -4090, -4096, -4097, SD_MAXINT, -SD_MAXINT});

		DataBuffer data;
		for (size_t i = 0; i < testInts.size(); ++i)
		{
			data << testInts.at(i);
		}

		for (size_t i = 0; i < testInts.size(); ++i)
		{
			Int readInt;
			data >> readInt;
			if (readInt != testInts.at(i))
			{
				UnitTestError(testFlags, TXT("Int streaming failed. After %s to data buffer, it recovered %s from it at index %s."), testInts.at(i), readInt, Int(i));
				return false;
			}
		}

		if (!data.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Int streaming failed. After reading Ints from the data buffer, the reader should be at the end of the buffer."));
			return false;
		}

		//Pushing ints in other endian format, should also be readable
		DataBuffer reverseBuffer(0, !DataBuffer::IsSystemLittleEndian());
		for (size_t i = 0; i < testInts.size(); ++i)
		{
			reverseBuffer << testInts.at(i);
		}

		for (size_t i = 0; i < testInts.size(); ++i)
		{
			Int readInt;
			reverseBuffer >> readInt;
			if (readInt != testInts.at(i))
			{
				UnitTestError(testFlags, TXT("Int streaming failed. After reversing the data buffer endianness and pushing %s Ints to the buffer, it recovered %s from index %s instead of %s."), Int(testInts.size()), readInt, Int(i), testInts.at(i));
				return false;
			}
		}

		if (!reverseBuffer.IsReaderAtEnd())
		{
			UnitTestError(testFlags, TXT("Int streaming failed. After reading Ints from the reverse data buffer, the reader should be at the end of the buffer."));
			return false;
		}

#ifdef PLATFORM_64BIT
		//Test streaming an Int with flags
		Int flaggedInt(0x0F0000FF00FF00FF);
		data << flaggedInt;

		Int recoveredFlaggedInt;
		data >> recoveredFlaggedInt;
		if (recoveredFlaggedInt != flaggedInt)
		{
			UnitTestError(testFlags, TXT("Int streaming failed. After writing %s to the buffer, it recovered %s from it."), flaggedInt, recoveredFlaggedInt);
			return false;
		}
#endif
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Int expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Int actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Int test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = 0;
		testStr = TXT("0");
		if (!testParsing())
		{
			return false;
		}

		expected = 8;
		testStr = TXT("8");
		if (!testParsing())
		{
			return false;
		}

		expected = 5;
		testStr = TXT("000005");
		if (!testParsing())
		{
			return false;
		}

		expected = 12;
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}

		testStr = TXT("-1298");
		expected = -1298;
		if (!testParsing())
		{
			return false;
		}

		expected = -12678;
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Int Utils"));
	{
		Int testInt = 4;
		TestLog(testFlags, TXT("Is %s even?  %s"), testInt, Bool(testInt.IsEven()));
		if (!testInt.IsEven() || testInt.IsOdd())
		{
			UnitTestError(testFlags, TXT("Int utils test failed.  %s should be considered an even number."), testInt);
			return false;
		}

		testInt++;
		TestLog(testFlags, TXT("Is %s odd?  %s"), testInt, Bool(testInt.IsOdd()));
		if (!testInt.IsOdd() || testInt.IsEven())
		{
			UnitTestError(testFlags, TXT("Int utils test failed.  %s should be considered an odd number."), testInt);
			return false;
		}

		//Ensure Ints can be used in unordered_maps
		std::unordered_map<Int, DString, IntKeyHash, IntKeyEqual> testMap
		{
			{4, TXT("Sand")},
			{8, TXT("Dune")},
			{10, TXT("Engine")}
		};

		if (testMap.at(4) != TXT("Sand") || testMap.at(8) != TXT("Dune") || testMap.at(10) != TXT("Engine"))
		{
			UnitTestError(testFlags, TXT("Int utils test failed. Retrieving Int keys for an undered map is returning the incorrect values."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Int numDigits"));
	{
		std::function<bool(Int, Int)> testNumDigits([&](Int testInt, Int expectedNumDigits)
		{
			Int actualNumDigits = testInt.NumDigits();
			bool passed = (actualNumDigits == expectedNumDigits);
			if (!passed)
			{
				UnitTestError(testFlags, TXT("Int's NumDigits test failed since the number of digits for %s should have been %s. Instead it's %s."), testInt, expectedNumDigits, actualNumDigits);
			}

			return passed;
		});

		if (!testNumDigits(4, 1))
		{
			return false;
		}

		if (!testNumDigits(10, 2))
		{
			return false;
		}

		if (!testNumDigits(40, 2))
		{
			return false;
		}

		if (!testNumDigits(128, 3))
		{
			return false;
		}

		if (!testNumDigits(0, 1))
		{
			return false;
		}

		if (!testNumDigits(-53982, 5))
		{
			return false;
		}

		if (!testNumDigits(-100000, 6))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Formatted String"));
	{
		std::function<bool(Int, Int, const DString&)> testFormattedString([&](Int testInt, Int numDigits, const DString& expectedString)
		{
			DString actualString = testInt.ToFormattedString(numDigits);
			bool passed = (actualString.Compare(expectedString, DString::CC_CaseSensitive) == 0);
			if (!passed)
			{
				UnitTestError(testFlags, TXT("Int's Formatted String test failed since the formatted string of a %s with %s number of digits should have returned \"%s\". Instead it returned \"%s\"."), testInt, numDigits, expectedString, actualString);
			}

			return passed;
		});

		if (!testFormattedString(5, 1, TXT("5")))
		{
			return false;
		}

		if (!testFormattedString(5, 3, TXT("005")))
		{
			return false;
		}

		if (!testFormattedString(5, 16, TXT("0000000000000005")))
		{
			return false;
		}

		if (!testFormattedString(100, 2, TXT("100")))
		{
			return false;
		}

		if (!testFormattedString(100, 4, TXT("0100")))
		{
			return false;
		}

		if (!testFormattedString(0, 3, TXT("000")))
		{
			return false;
		}

		if (!testFormattedString(-16, 2, TXT("-16")))
		{
			return false;
		}

		if (!testFormattedString(-16, 4, TXT("-0016")))
		{
			return false;
		}

		if (!testFormattedString(-256, 1, TXT("-256")))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Int"));

	return true;
}

bool DatatypeUnitTester::TestFloat (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Float"));

	Float blankFloat(0.f);
	Float initializedFloat(14.5f);
	Float copiedFloat(initializedFloat);

	TestLog(testFlags, TXT("Constructor tests:  blankFloat = %s"), blankFloat);
	TestLog(testFlags, TXT("    initializedFloat(14.5) = %s"), initializedFloat);
	TestLog(testFlags, TXT("    copiedFloat from initializedInt = %s"), copiedFloat);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		TestLog(testFlags, TXT("blankFloat != 0?  %s"), Bool(blankFloat != 0));
		if (blankFloat != 0)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  blankFloat is not zero.  Instead it's:  %s"), blankFloat);
			return false;
		}

		TestLog(testFlags, TXT("initializedFloat(%s) != copiedFloat(%s) ? %s"), initializedFloat, copiedFloat, Bool(initializedFloat != copiedFloat));
		if (initializedFloat != copiedFloat)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedFloat != copiedInt.  Values are:  %s and %s"), initializedFloat, copiedFloat);
			return false;
		}

		Float testFloat = 5.95f;
		TestLog(testFlags, TXT("5.95 < 6 ? %s"), Bool(testFloat < 6));
		if (!(testFloat < 6))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than 6."), testFloat);
			return false;
		}

		TestLog(testFlags, TXT("5.95 <= 5.95 ? %s"), Bool(testFloat <= 5.95f));
		if (!(testFloat <= 5.95f))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than or equal to 5.95."), testFloat);
			return false;
		}

		TestLog(testFlags, TXT("5.95 > 3 ? %s"), Bool(testFloat > 3));
		if (!(testFloat > 3))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not greater than 3."), testFloat);
			return false;
		}

		TestLog(testFlags, TXT("5.95 >= 7.2 ? %s"), Bool(testFloat >= 7.2f));
		if (testFloat >= 7.2f)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is greater than or equal to 7.2."), testFloat);
			return false;
		}

		TestLog(testFlags, TXT("5.95 >= 3.14 && 5.95 < 7.8 && 5.95 == 5.95 && (5.95 == 10.05 || 5.95 != 9) ? %s"), Bool(testFloat >= 3.14f && testFloat < 7.8f && testFloat == Float(5.95f) && (testFloat == 10.05f || testFloat != Int(9).ToFloat())));
		if (!(testFloat >= 3.14f && testFloat < 7.8f && testFloat == Float(5.95f) && (testFloat == 10.05f || testFloat != Int(9).ToFloat())))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testFloat is:  %s"), testFloat);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		Float testFloat = 10.2 + 8.8;
		TestLog(testFlags, TXT("10.2 + 8.8 = %s"), testFloat);
		if (testFloat != 19)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 19.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat = 10.f + initializedFloat;
		TestLog(testFlags, TXT("10 + 14.5 = %s"), testFloat);
		if (testFloat != 24.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat++;
		TestLog(testFlags, TXT("Increment from 24.5 leads to:  %s"), testFloat);
		if (testFloat != 25.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 25.5.  It's currently:  %s"), testFloat);
			return false;
		}

		--testFloat;
		TestLog(testFlags, TXT("Decrement from 25.5 leads to:  %s"), testFloat);
		if (testFloat != 24.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat = initializedFloat - 1.11115f;
		TestLog(testFlags, TXT("14.5 - 1.11115 = %s"), testFloat);
		if (testFloat != 13.38885f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 13.38885.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat += 13.794f;
		TestLog(testFlags, TXT("13.38885 += 13.794  ---> %s"), testFloat);
		if (testFloat != 27.18285f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 27.18285.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat = initializedFloat * 2;
		TestLog(testFlags, TXT("14.5 * 2 = %s"), testFloat);
		if (testFloat != 29)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 29.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat /= 3;
		TestLog(testFlags, TXT("29 /=3  ---> %s"), testFloat);
		if (abs(testFloat.Value - 9.666667) > 0.000001)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is ~9.666667.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat = 10;
		testFloat += ((20.25f - 14.5f) * 4.f) + 3.1f;
		TestLog(testFlags, TXT("10 += ((20.25 - 14.5) * 4) + 3.1 results in:  %s"), testFloat);
		if (testFloat != 36.1f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 36.1.  It's currently:  %s"), testFloat);
			return false;
		}

		testFloat = 20.5;
		testFloat %= 3;
		TestLog(testFlags, TXT("20.5 % 3 = %s"), testFloat);
		if (testFloat != 2.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 2.5.  It's currently:  %s"), testFloat);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("IsCloseTo"));
	{
		std::function<bool(Float, Float, Float, bool)> testIsCloseTo([&](Float left, Float right, Float tolerance, bool expectedIsCloseTo)
		{
			bool actual = left.IsCloseTo(right, tolerance);
			if (actual != expectedIsCloseTo)
			{
				if (expectedIsCloseTo)
				{
					UnitTestError(testFlags, TXT("IsCloseTo test failed. %s is expected to be close to %s within %s tolerance."), left, right, tolerance);
				}
				else
				{
					UnitTestError(testFlags, TXT("IsCloseTo test failed. %s is NOT expected to be close to %s within %s tolerance."), left, right, tolerance);
				}
			}

			return (actual == expectedIsCloseTo);
		});

		Float a(5.f);
		Float b(5.f);

		if (!testIsCloseTo(a, b, FLOAT_TOLERANCE, true))
		{
			return false;
		}

		b = 5.5f;
		if (!testIsCloseTo(a, b, 0.001f, false))
		{
			return false;
		}

		if (!testIsCloseTo(b, a, 0.001f, false))
		{
			return false;
		}

		if (!testIsCloseTo(a, b, 1.f, true))
		{
			return false;
		}

		a *= -1.f;
		if (!testIsCloseTo(a, b, 1.f, false))
		{
			return false;
		}

		a = -10.f;
		b = -10.1f;
		if (!testIsCloseTo(a, b, 0.11f, true))
		{
			return false;
		}

		if (!testIsCloseTo(a, b, 0.09f, false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Typcasting"));
	{
		Float testFloat = -5.84f;
		Int testInt = testFloat.ToInt();
		TestLog(testFlags, TXT("Float(-5.84) converted to Int is:  %s"), testInt);
		if (testInt != -5)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting Int is:  -5"));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Float Streaming"));
	{
		Float testFloat = 5942.184f;
		Float secondFloat = -548.487f;
		Float readTestFloat = 0.f;
		Float readSecondFloat = 0.f;
		DataBuffer data;
		data << testFloat;
		data << secondFloat;
		data >> readTestFloat;
		data >> readSecondFloat;
		if (readTestFloat != testFloat)
		{
			UnitTestError(testFlags, TXT("Float streaming failed.  After writing %s to data buffer, it recovered %s from it."), testFloat, readTestFloat);
			return false;
		}

		if (readSecondFloat != secondFloat)
		{
			UnitTestError(testFlags, TXT("Float streaming failed.  After writing a second float (%s) to data buffer, it recovered %s from it."), secondFloat, readSecondFloat);
			return false;
		}

		DataBuffer reversedBuffer(0, !DataBuffer::IsSystemLittleEndian());
		//Pushing ints in other endian format, should also be readable
		readTestFloat = 0.f;
		readSecondFloat = 0.f;
		reversedBuffer << testFloat;
		reversedBuffer << secondFloat;
		reversedBuffer >> readTestFloat;
		reversedBuffer >> readSecondFloat;
		if (readTestFloat != testFloat)
		{
			UnitTestError(testFlags, TXT("Float streaming failed.  After writing %s to a reversed data buffer, it recovered %s from it."), testFloat, readTestFloat);
			return false;
		}

		if (readSecondFloat != secondFloat)
		{
			UnitTestError(testFlags, TXT("Float streaming failed.  After writing a second float (%s) to a reversed data buffer, it recovered %s from it."), secondFloat, readSecondFloat);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Float expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Float actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Float test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = 0.f;
		testStr = TXT("0");
		if (!testParsing())
		{
			return false;
		}

		expected = 4.45f;
		testStr = TXT("4.45");
		if (!testParsing())
		{
			return false;
		}

		expected = 50.f;
		testStr = TXT("50");
		if (!testParsing())
		{
			return false;
		}

		expected = 1024.f;
		testStr = TXT("1024");
		if (!testParsing())
		{
			return false;
		}

		expected = -33.2f;
		testStr = TXT("-33.2");
		if (!testParsing())
		{
			return false;
		}

		expected = -98.317f;
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Float Utils"));
	{
		Float minBounds = 0.f;
		Float maxBounds = 10.f;
		Float testFloat = 0.5f;
		TestLog(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
		if (Utils::Lerp(testFloat, minBounds, maxBounds) != 5.f)
		{
			UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 5.  Instead it returned %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
			return false;
		}

		minBounds = 10.f;
		maxBounds = 1010.f;
		testFloat = 0.75f;
		TestLog(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
		if (Utils::Lerp(testFloat, minBounds, maxBounds) != 760.f)
		{
			UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 760.  Instead it returned %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
			return false;
		}

		testFloat = 10.75f;
		TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, Float::Round(testFloat));
		if (Float::Round(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), testFloat, Float::Round(testFloat));
			return false;
		}

		testFloat = 10.5f;
		TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, Float::Round(testFloat));
		if (Float::Round(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), testFloat, Float::Round(testFloat));
			return false;
		}

		testFloat = 10.25f;
		TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, Float::Round(testFloat));
		if (Float::Round(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), testFloat, Float::Round(testFloat));
			return false;
		}

		testFloat = 10.75f;
		TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, Float::RoundUp(testFloat));
		if (Float::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), testFloat, Float::RoundUp(testFloat));
			return false;
		}

		testFloat = 10.5f;
		TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, Float::RoundUp(testFloat));
		if (Float::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), testFloat, Float::RoundUp(testFloat));
			return false;
		}

		testFloat = 10.25f;
		TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, Float::RoundUp(testFloat));
		if (Float::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), testFloat, Float::RoundUp(testFloat));
			return false;
		}

		testFloat = 10.75f;
		TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, Float::RoundDown(testFloat));
		if (Float::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), testFloat, Float::RoundDown(testFloat));
			return false;
		}

		testFloat = 10.5f;
		TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, Float::RoundDown(testFloat));
		if (Float::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), testFloat, Float::RoundDown(testFloat));
			return false;
		}

		testFloat = 10.25f;
		TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, Float::RoundDown(testFloat));
		if (Float::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), testFloat, Float::RoundDown(testFloat));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Float Power"));
	{
		Float base;
		Float exponent;
		Float expected;
		std::function<bool()> testPow([&]()
		{
			Float actual(base);

			Float result = Float::Pow(actual, exponent);
			if (!result.IsCloseTo(expected))
			{
				UnitTestError(testFlags, TXT("Float Power test failed. The %s to the %s power should have resulted in %s. Instead it computed %s."), base, exponent, expected, result);
				return false;
			}

			//Ensure the inlined variation produces the same result.
			actual.PowInline(exponent);
			if (!actual.IsCloseTo(expected))
			{
				UnitTestError(testFlags, TXT("Float Power inlined test failed. The %s to the %s power should have resulted in %s. Instead it computed %s."), base, exponent, expected, actual);
				return false;
			}

			return true;
		});

		base = 3.f;
		exponent = 2.f;
		expected = 9.f;
		if (!testPow())
		{
			return false;
		}

		base = 10.f;
		exponent = 4.f;
		expected = 10000.f;
		if (!testPow())
		{
			return false;
		}

		base = 81.f;
		exponent = 0.5f;
		expected = 9.f;
		if (!testPow())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Formatted String"));
	{
		const std::function<bool(Float, Int, Int, Float::eTruncateMethod, const DString&)> testFormatString([&](Float originalFloat, Int minDigits, Int numDecimals, Float::eTruncateMethod rounding, const DString& expectedString)
		{
			const DString formattedStr = originalFloat.ToFormattedString(minDigits, numDecimals, rounding);
			if (formattedStr.Compare(expectedString, DString::CC_CaseSensitive) != 0)
			{
				DString truncateMethodStr;
				switch (rounding)
				{
					case(Float::TM_RoundDown):
						truncateMethodStr = TXT("Round Down");
						break;

					case(Float::TM_Round):
						truncateMethodStr = TXT("Round");
						break;

					case(Float::TM_RoundUp):
						truncateMethodStr = TXT("Round Up");
						break;
				};

				UnitTestError(testFlags, TXT("Float formatted string test failed. The formatted string of a %s (where minDigits=%s, numDecimals=%s and truncateMethod=%s) is expected to be %s. Instead it's %s."), originalFloat, minDigits, numDecimals, truncateMethodStr, expectedString, formattedStr);
				return false;
			}

			return true;
		});

		Float originalFloat = 1.753f;
		if (!testFormatString(originalFloat, 1, 2, Float::TM_Round, TXT("1.75")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 2, Float::TM_RoundUp, TXT("1.76")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, Float::TM_Round, TXT("1.8")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, Float::TM_RoundDown, TXT("1.7")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 6, Float::TM_Round, TXT("1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 6, 6, Float::TM_Round, TXT("1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 10, 6, Float::TM_Round, TXT("0001.753000")))
		{
			return false;
		}

		originalFloat *= -1.f;
		if (!testFormatString(originalFloat, 10, 6, Float::TM_Round, TXT("-0001.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 5, 2, Float::TM_Round, TXT("-001.75")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 2, 6, Float::TM_RoundUp, TXT("-1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, Float::TM_RoundDown, TXT("-1.7")))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Float"));

	return true;
}

bool DatatypeUnitTester::TestHashedString (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Hashed String"));

	SetTestCategory(testFlags, TXT("Construction"));
	{
		DString aStr(TXT("UnitTest"));
		DString bStr(TXT("OtherUnitTest"));

		HashedString a(aStr);
		HashedString b(bStr);
		HashedString c(a);

		if (a.ReadString().Compare(aStr, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After constructing a hashed string from %s, ReadString returned %s instead of %s."), aStr, a.ReadString(), aStr);
			return false;
		}

		size_t bHash = bStr.GenerateHash();
		if (b.GetHash() != bHash)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After constructing a hashed string from %s, its hash (%s) is different from the original string's hash (%s)."), bStr, DString::MakeString(b.GetHash()), DString::MakeString(bHash));
			return false;
		}

		if (a == b)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After constructing the hashed strings %s and %s. The two Hashed Strings are considered equal despite being constructed from two different strings."), aStr, bStr);
			return false;
		}

		if (a != c)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After constructing another HashedString from %s. The copied HashedString is considered different from what it's copied from."), aStr);
			return false;
		}

		a.ParseString(bStr.ReadString());
		if (a != b)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After parsing the string %s, the resulting hashed string %s should have been equal to %s. Instead it's not."), b.ReadString(), a, b);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		HashedString a(TXT("Datatype Unit Tester"));
		HashedString b(TXT("Hashed String test"));
		HashedString c(TXT("Some other Hashed String"));

		DataBuffer testBuffer;
		testBuffer << a;
		testBuffer << b;
		testBuffer << c;

		HashedString copyA;
		HashedString copyB;
		HashedString copyC;

		testBuffer >> copyA;
		testBuffer >> copyB;
		testBuffer >> copyC;

		if (a != copyA || b != copyB || c != copyC)
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After streaming the following hashed strings %s, %s, %s to a data stream, it pulled %s, %s, %s from that same data stream."), a, b, c, copyA, copyB, copyC);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Hash Map Verification"));
	{
		//NOTE: This isn't thread safe. Other threads may access the HashMap while this unit test is running.
		DString testStrA = TXT("HashMapTest");
		DString testStrB = TXT("HashStringTest");

		size_t aHash = testStrA.GenerateHash();
		size_t bHash = testStrB.GenerateHash();

		{
			if (HashedString::HashMap.contains(aHash) || HashedString::HashMap.contains(bHash))
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. The HashMap already contains hashes for %s or %s before HashStrings are constructed."), testStrA, testStrB);
				return false;
			}

			HashedString a(testStrA);
			if (!HashedString::HashMap.contains(aHash))
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. The HashMap does not contain an entry for %s despite creating a HashString with that string."), testStrA);
				return false;
			}

			if (HashedString::HashMap.at(aHash).ReferenceCounter != 1)
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. The HashMap should initialize an entry with a reference counter of 1. Instead it's %s."), HashedString::HashMap.at(aHash).ReferenceCounter);
				return false;
			}

			HashedString b(testStrA);
			if (HashedString::HashMap.at(aHash).ReferenceCounter != 2)
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. After constructing two HashedStrings from %s, the reference counter for that entry should have been 2. Instead it's %s."), testStrA, HashedString::HashMap.at(aHash).ReferenceCounter);
				return false;
			}

			//Move the hash strings to a string B
			b.SetString(testStrB);
			if (HashedString::HashMap.at(aHash).ReferenceCounter != 1)
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. After moving one of the HashedStrings to %s, the reference counter to the original entry for string %s should have been 1. Instead it's %s."), testStrB, testStrA, HashedString::HashMap.at(aHash).ReferenceCounter);
				return false;
			}

			if (!HashedString::HashMap.contains(bHash))
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. After moving one of the HashedStrings to %s, the HashMap should have an entry in the map, but it doesn't exist."), testStrB);
				return false;
			}

			a.SetString(testStrB);
			if (HashedString::HashMap.contains(aHash))
			{
				UnitTestError(testFlags, TXT("Hashed String test failed. After moving all hashed strings away from %s, it's expected that the HashMap to remove that entry. %s still exists in the HashMap."), testStrA, testStrA);
				return false;
			}
		} //HashedStrings went out of scope. testStrB entry should be removed from the hash map.

		if (HashedString::HashMap.contains(bHash))
		{
			UnitTestError(testFlags, TXT("Hashed String test failed. After the HashedStrings went out of scope, their entries in the HashMap should have been removed. Instead %s still exists in the map."), testStrB);
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Hashed String"));

	return true;
}

bool DatatypeUnitTester::TestDPointer (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("DPointer"));

	DPointer<DPointerTester> pointer = DPointerTester::CreateObject();
	DPointer<DPointerTester> pointed = DPointerTester::CreateObject();

	SetTestCategory(testFlags, TXT("DPointer Comparison"));
	{
		pointer->OtherTester = pointed;

		if (pointed->OtherTester.IsValid())
		{
			UnitTestError(testFlags, TXT("Failed to initialize DPointerTester.  The pointer variable should have initialized to nullptr."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		if (pointer->OtherTester.IsNullptr() || pointer->OtherTester != pointed)
		{
			UnitTestError(testFlags, TXT("Failed to initialize DPointerTester.  The pointer variable should be equal to the pointed object."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		DPointer<DPointerTester> accessor = pointer;
		if (accessor->OtherTester != pointed)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  Could not receive a copy of the pointer."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		DPointer<PrimitivePointerTester> primTester = new PrimitivePointerTester();
		primTester->OtherTester = pointed;
		PrimitivePointerTester& testerByRef = *primTester;
		if (testerByRef != *primTester.Get()) //Ensure that both are pointing to same memory address
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  Comparing DPointerTester by reference should have been the equal to the original pointed object."));
			pointer->Destroy();
			pointed->Destroy();
			delete primTester.Get();
			return false;
		}
		delete primTester.Get();

		DPointerTester* rawPointer = pointer.Get();
		if (rawPointer->OtherTester != pointer->OtherTester)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  Comparing the DPointerTester by raw pointer should have been equal to the original pointed object."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		//Compare DPointer to a raw pointer
		if (pointer != rawPointer || rawPointer != pointer)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed. Comparing a raw pointer to a DPointer that point to the same object should have returned true. Instead the condition suggests they're different."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		if (pointed == rawPointer || rawPointer == pointed)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed. Comparing a raw pointer to a DPointer that points to a different object should have returned false. Instead the condition suggests they're the same."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		Object* objPointer = rawPointer;
		if (pointer != objPointer || objPointer != pointer)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed. Comparing a raw pointer of a different type against a DPointer of the same object should have returned true. Instead the condition suggests they're different."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		if (pointed == objPointer || objPointer == pointed)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed. Comparing a raw pointer of a different type against a DPointer of the same object should have returned false. Instead the condition suggests they're the same."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	DPointer<DPointerTester> pointer1 = pointed;
	DPointer<DPointerTester> pointer2 = pointed;
	DPointer<DPointerTester> pointer3 = pointed;

	//Need to test copying and assigning DPointers around to ensure that their linked lists are maintained.
	SetTestCategory(testFlags, TXT("Pointer Assignment"));
	{
		std::vector<DPointerBase*> expectedPointers;
		expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
		expectedPointers.push_back(pointer1.GetPointerAddress());
		expectedPointers.push_back(pointer2.GetPointerAddress());
		expectedPointers.push_back(pointer3.GetPointerAddress());

		for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
		{
			int numMatchingPointers = 0;

			//Each pointer in the object's pointer list should be in list exactly once
			for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
			{
				if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
				{
					numMatchingPointers++;
				}
			}

			if (numMatchingPointers != 1)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), DString::MakeString(numMatchingPointers));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}

		//shuffle pointers around
		pointer1 = nullptr;
		pointer2 = pointer3; //Point to pointed through pointer3
		pointer3 = pointer; //Point from pointed to pointer
		expectedPointers.clear();
		expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
		expectedPointers.push_back(pointer2.GetPointerAddress());

		std::vector<DPointerBase*> unexpectedPointers;
		unexpectedPointers.push_back(pointer1.GetPointerAddress());
		unexpectedPointers.push_back(pointer3.GetPointerAddress());
		unexpectedPointers.push_back(pointed->OtherTester.GetPointerAddress());

		//Validate Pointed's internal pointer linked list
		for (size_t i = 0; i < expectedPointers.size(); ++i)
		{
			int numMatchingPointers = 0;
			for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
			{
				if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
				{
					numMatchingPointers++;
				}
			}

			if (numMatchingPointers != 1)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), DString::MakeString(numMatchingPointers));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}

		for (size_t i = 0; i < unexpectedPointers.size(); ++i)
		{
			int numMatchingPointers = 0;
			for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
			{
				if (curPointer == unexpectedPointers.at(i))
				{
					numMatchingPointers++;
				}
			}

			if (numMatchingPointers != 0)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  The pointer within the object's linked list should have been removed after the pointer was assigned to some other Object.  It was found %s times."), DString::MakeString(numMatchingPointers));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}

		//Validate Pointer's internal pointer linked list
		expectedPointers.clear();
		expectedPointers.push_back(pointer3.GetPointerAddress());

		unexpectedPointers.clear();
		unexpectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
		unexpectedPointers.push_back(pointer1.GetPointerAddress());
		unexpectedPointers.push_back(pointer2.GetPointerAddress());
		unexpectedPointers.push_back(pointed->OtherTester.GetPointerAddress());

		for (size_t i = 0; i < expectedPointers.size(); ++i)
		{
			int numMatchingPointers = 0;
			for (DPointerBase* curPointer = pointer->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
			{
				if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
				{
					numMatchingPointers++;
				}
			}

			if (numMatchingPointers != 1)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), DString::MakeString(numMatchingPointers));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}

		for (size_t i = 0; i < unexpectedPointers.size(); ++i)
		{
			int numMatchingPointers = 0;
			for (DPointerBase* curPointer = pointer->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
			{
				if (curPointer->GetPointerAddress() == unexpectedPointers.at(i)->GetPointerAddress())
				{
					numMatchingPointers++;
				}
			}

			if (numMatchingPointers != 0)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  The pointer within the object's linked list should have been removed after the pointer was assigned to some other Object.  It was found %s times."), DString::MakeString(numMatchingPointers));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Clearing Pointers"));
	{
		//Test assigning DPointers to null
		DPointer<DPointerTester> clearPointerTester = pointed;
		DPointer<DPointerTester> nullPointerTester = pointed;
		DPointerBase* pointerAddress = &clearPointerTester;

		clearPointerTester.ClearPointer();
		if (clearPointerTester.IsValid())
		{
			UnitTestError(testFlags, TXT("DPointerTest failed after clearing a pointer.  After clearing a pointer, the reference to its pointed object should have been cleared, however the pointer should still exist."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		//Verify that pointed lost its reference to clearPointerTester
		for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer == pointerAddress)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed.  After clearing the pointer, the reference to that pointer should have been removed from the Object's linked list."));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}

		pointerAddress = &nullPointerTester;
		nullPointerTester = nullptr;
		if (nullPointerTester.IsValid())
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  The pointer is not nullptr after assigning it to nullptr."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}

		//Verify that pointed lost its reference to nullPointerTester
		for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer == pointerAddress)
			{
				UnitTestError(testFlags, TXT("DPointerTest failed.  After assigning the pointer to null, the reference to that pointer should have been removed from the Object's linked list."));
				pointer->Destroy();
				pointed->Destroy();
				return false;
			}
		}


		pointer->OtherTester = pointed;
		pointed->OtherTester = pointer; //test circular dependency
		pointer1 = pointed;
		pointer2 = pointer;
		pointer3 = pointer;

		//expected pointers to clear
		std::vector<DPointerBase*> expectedPointers;
		expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
		expectedPointers.push_back(pointer1.GetPointerAddress());

		//pointers expected to still point to something
		std::vector<DPointerBase*> unexpectedPointers;
		unexpectedPointers.push_back(pointer2.GetPointerAddress());
		unexpectedPointers.push_back(pointer3.GetPointerAddress());

		pointed->Destroy(); //should set all pointers that are pointing at this object to nullptr

		for (size_t i = 0; i < expectedPointers.size(); ++i)
		{
			if ((*expectedPointers.at(i)))
			{
				UnitTestError(testFlags, TXT("DPointer test failed.  After destroying an object, all pointers pointing at that object should have been set to nullptr."));
				pointer->Destroy();
				return false;
			}
		}

		for (size_t i = 0; i < unexpectedPointers.size(); ++i)
		{
			if (!(*unexpectedPointers.at(i)))
			{
				UnitTestError(testFlags, TXT("DPointer test failed.  After destroying an object, all pointers pointing to a different object should still retain their values."));
				pointer->Destroy();
				return false;
			}
		}

		expectedPointers.clear();
		expectedPointers.push_back(pointer1.GetPointerAddress());
		expectedPointers.push_back(pointer2.GetPointerAddress());
		expectedPointers.push_back(pointer3.GetPointerAddress());

		//clear remaining pointers
		pointer->Destroy();

		for (size_t i = 0; i < expectedPointers.size(); ++i)
		{
			if ((*expectedPointers.at(i)))
			{
				UnitTestError(testFlags, TXT("DPointer test failed.  After destroying the second object, the remaining pointers should have been set to nullptr."));
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("DPointer"));

	return true;
}

bool DatatypeUnitTester::TestRange (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Range"));

	Range<Int> blankRange;
	Range<Int> initializedRange(0, 100);
	Range<Int> copiedRange(initializedRange);

	TestLog(testFlags, TXT("Constructor tests:  blankRange= %s"), blankRange);
	TestLog(testFlags, TXT("    initializedRange(0,100)= %s"), initializedRange);
	TestLog(testFlags, TXT("    copiedRange from initializedRange= %s"), copiedRange);

	SetTestCategory(testFlags, TXT("Comparison"));
	Range<Int> comparisonRange(25, 100);
	TestLog(testFlags, TXT("initializedRange(%s) == comparisonRange(%s)?  %s"), initializedRange, comparisonRange, Bool(initializedRange == comparisonRange));
	if (initializedRange == comparisonRange)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s should not be equal to %s."), initializedRange, comparisonRange);
		return false;
	}

	TestLog(testFlags, TXT("initializedRange(%s) == copiedRange(%s) ? %s"), initializedRange, copiedRange, Bool(initializedRange == copiedRange));
	if (initializedRange != copiedRange)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s != copiedRange %s."), initializedRange, copiedRange);
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Range Streaming"));
	{
		Range<Int> streamIntRange(4, 15);
		Range<Float> streamFloatRange(-48.3f, 81.6f);
		Range<Int> readStreamIntRange;
		Range<Float> readStreamFloatRange;
		DataBuffer rangeBuffer;

		rangeBuffer << streamIntRange;
		rangeBuffer << streamFloatRange;
		rangeBuffer >> readStreamIntRange;
		rangeBuffer >> readStreamFloatRange;

		if (streamIntRange != readStreamIntRange)
		{
			UnitTestError(testFlags, TXT("Streaming int range test failed.  The range %s was pushed to data buffer.  %s was pulled from data buffer."), streamIntRange, readStreamIntRange);
			return false;
		}

		if (streamFloatRange != readStreamFloatRange)
		{
			UnitTestError(testFlags, TXT("Streaming float range test failed.  The range %s was pushed to data buffer.  %s was pulled from data buffer."), streamFloatRange, readStreamFloatRange);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Range<Int> expectedIntRange;
		DString testStr;
		std::function<bool()> testIntParsing([&]()
		{
			Range<Int> actual;
			actual.ParseString(testStr);
			if (actual != expectedIntRange)
			{
				UnitTestError(testFlags, TXT("Range test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expectedIntRange, actual);
				return false;
			}

			return true;
		});

		expectedIntRange = Range<Int>(0, 5);
		testStr = TXT("(Min=0, Max = 5)");
		if (!testIntParsing())
		{
			return false;
		}

		expectedIntRange = Range<Int>(-10, 32);
		testStr = TXT("(Min = -10 , Max=32)"); //messing with spaces is intended
		if (!testIntParsing())
		{
			return false;
		}

		expectedIntRange = Range<Int>(-1000, 2048);
		testStr = TXT("(Min=-1000,Max=2048)");
		if (!testIntParsing())
		{
			return false;
		}

		expectedIntRange = Range<Int>(-523, 373);
		testStr = expectedIntRange.ToString();
		if (!testIntParsing())
		{
			return false;
		}

		Range<Float> expectedFloatRange;
		std::function<bool()> testFloatParsing([&]()
		{
			Range<Float> actual;
			actual.ParseString(testStr);
			if (actual != expectedFloatRange)
			{
				UnitTestError(testFlags, TXT("Range test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expectedFloatRange, actual);
				return false;
			}

			return true;
		});

		expectedFloatRange = Range<Float>(4.f, 10.f);
		testStr = TXT("(Min = 4, Max=10)");
		if (!testFloatParsing())
		{
			return false;
		}

		expectedFloatRange = Range<Float>(-15.f, 0.f);
		testStr = TXT("(Min=-15,Max=0)");
		if (!testFloatParsing())
		{
			return false;
		}

		expectedFloatRange = Range<Float>(-32.5f, 512.25f);
		testStr = TXT("( Min = -32.5000 , Max = 00512.25 )");
		if (!testFloatParsing())
		{
			return false;
		}

		expectedFloatRange = Range<Float>(-79.24f, 50.24f);
		testStr = expectedFloatRange.ToString();
		if (!testFloatParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	{
		Range<Int> testRange(0, 100);
		Range<Int> otherRange(50, 125);
		TestLog(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, Bool(testRange.Intersects(otherRange)));
		if (!testRange.Intersects(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be intersecting %s."), testRange, otherRange);
			return false;
		}

		otherRange.Min = 125;
		otherRange.Max = 200;
		TestLog(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, Bool(testRange.Intersects(otherRange)));
		if (testRange.Intersects(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be intersecting %s."), testRange, otherRange);
			return false;
		}

		otherRange.Min = 50;
		otherRange.Max = 200;
		Range<Int> intersection = testRange.GetIntersectionFrom(otherRange);
		TestLog(testFlags, TXT("The intersection of %s and %s is %s"), testRange, otherRange, intersection);
		if (intersection != Range<Int>(50, 100))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The intersection of %s and %s should be 50-100.  Instead it returned %s."), testRange, otherRange, intersection);
			return false;
		}

		TestLog(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, Bool(testRange.IsWithin(otherRange)));
		if (testRange.IsWithin(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be completely within %s."), testRange, otherRange);
			return false;
		}

		otherRange.Min = -100;
		otherRange.Max = 200;
		TestLog(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, Bool(testRange.IsWithin(otherRange)));
		if (!testRange.IsWithin(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be completely within %s."), testRange, otherRange);
			return false;
		}

		otherRange.Min = 100;
		otherRange.Max = 0;
		TestLog(testFlags, TXT("Is %s inverted?  %s"), otherRange, Bool(otherRange.IsInverted()));
		if (!otherRange.IsInverted())
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be considered inverted."), otherRange);
			return false;
		}

		TestLog(testFlags, TXT("...Fixing %s inversion..."), otherRange);
		otherRange.FixRange(); //This should fix inverted ranges
		TestLog(testFlags, TXT("Is %s inverted?  %s"), otherRange, Bool(otherRange.IsInverted()));
		if (otherRange.IsInverted())
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be inverted."), otherRange);
			return false;
		}

		TestLog(testFlags, TXT("Does %s contain 25? %s"), testRange, Bool(testRange.ContainsValue(25)));
		if (!testRange.ContainsValue(25))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be containing 25"), testRange);
			return false;
		}

		TestLog(testFlags, TXT("Does %s contain 125?  %s"), testRange, Bool(testRange.ContainsValue(125)));
		if (testRange.ContainsValue(125))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be containing 125"), testRange);
			return false;
		}

		TestLog(testFlags, TXT("The difference of %s is:  %s"), testRange, testRange.Difference());
		if (testRange.Difference() != 100)
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the difference of %s is 100."), testRange);
			return false;
		}

		TestLog(testFlags, TXT("The center of %s is:  %s"), testRange, testRange.Center());
		if (testRange.Center() != 50)
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the center of %s is 50."), testRange);
			return false;
		}

		testRange.Min = 20;
		testRange.Max = 80;
		if (testRange.Center() != 50)
		{
			UnitTestError(testFlags, TXT("Utility functions failed. The expected center of the range %s should have been 50. Instead it returned %s."), testRange, testRange.Center());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("CalcAlpha"));
	{
		Range<Float> testRange;
		Float input;
		Float expected;
		std::function<bool()> testCalcAlpha([&]()
		{
			Float actual = testRange.CalcAlpha(input);
			if (!actual.IsCloseTo(expected))
			{
				UnitTestError(testFlags, TXT("CalcAlpha test failed. When passing in %s to the range %s, the expected alpha should have been %s. Instead it returned %s."), input, testRange, expected, actual);
				return false;
			}

			return true;
		});

		testRange = Range<Float>(0.f, 100.f);
		input = 25.f;
		expected = 0.25f;
		if (!testCalcAlpha())
		{
			return false;
		}

		input = 75.f;
		expected = 0.75f;
		if (!testCalcAlpha())
		{
			return false;
		}

		input = 110.f;
		expected = 1.1f;
		if (!testCalcAlpha())
		{
			return false;
		}

		testRange.Min = 50.f;
		input = 25.f;
		expected = -0.5f;
		if (!testCalcAlpha())
		{
			return false;
		}

		input = 90.f;
		expected = 0.8f;
		if (!testCalcAlpha())
		{
			return false;
		}

		testRange = Range<Float>(10.f, 30.f);
		input = 20.f;
		expected = 0.5f;
		if (!testCalcAlpha())
		{
			return false;
		}

		input = 15.f;
		expected = 0.25f;
		if (!testCalcAlpha())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Range"));

	return true;
}

bool DatatypeUnitTester::TestVector2 (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Vector2"));

	Vector2 blankVector = Vector2::ZERO_VECTOR;
	Vector2 initializedVector(1, 2);
	Vector2 copiedVector(initializedVector);
	Vector2 typecastedVector(Vector3(10,20,30).ToVector2());

	TestLog(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
	TestLog(testFlags, TXT("    initializedVector(1,2)= %s"), initializedVector);
	TestLog(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
	TestLog(testFlags, TXT("    typecastedVector from Vector3(10,20,30)= %s"), typecastedVector);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		TestLog(testFlags, TXT("Checking initializedVector axis. . ."));
		if (initializedVector[0] != 1.f || initializedVector[1] != 2.f)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  Failed to retrieve individual axis of initializedVector(%s).  It returned %s and %s instead of 1 and 2."), initializedVector, initializedVector[0], initializedVector[1]);
			return false;
		}

		TestLog(testFlags, TXT("initializedVector != copiedVector ? %s"), Bool(initializedVector != copiedVector));
		if (initializedVector != copiedVector)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), initializedVector, copiedVector);
			return false;
		}

		Vector2 testVector(5, 10);
		TestLog(testFlags, TXT("<5,10> != <1,2> && (<5,10> == <99,99> || <5,10> == <5,10>) ? %s"), Bool(testVector != copiedVector && (testVector == Vector2(99,99) || testVector == Vector2(5,10))));
		if (!(testVector != copiedVector && (testVector != Vector2(99,99) || testVector == Vector2(5,10))))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), testVector);
			return false;
		}

		TestLog(testFlags, TXT("<5, 10> == <5, 15> ? %s"), Bool(testVector == Vector2(5, 15)));
		if (testVector == Vector2(5, 15))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
			return false;
		}

		TestLog(testFlags, TXT("<5, 10> != <5.025, 10> ? %s"), Bool(testVector == Vector2(5.025f, 10)));
		if (testVector == Vector2(5.025f, 10))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		Vector2 testVector(1.f, 5.f);
		if (-testVector != Vector2(-1.f, -5.f))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed. The expected inverted vector of %s should have been <-1,-5>. Instead it returned %s."), testVector, (-testVector));
			return false;
		}

		testVector = Vector2(1,2) + Vector2(5,9);
		TestLog(testFlags, TXT("<1,2> + <5,9> = %s"), testVector);
		if (testVector != Vector2(6,11))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector = copiedVector - Vector2(4, 10);
		TestLog(testFlags, TXT("<1,2> - <4,10> = %s"), testVector);
		if (testVector != Vector2(-3,-8))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector += Vector2(4,6);
		TestLog(testFlags, TXT("<-3,-8> += <4,6>  ---> %s"), testVector);
		if (testVector != Vector2(1,-2))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector = Vector2(5.f, 8.f) * 2.f;
		TestLog(testFlags, TXT("<5,8> * 2 = %s"), testVector);
		if (testVector != Vector2(10,16))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector /= 8.f;
		TestLog(testFlags, TXT("<10,16> /=8  ---> %s"), testVector);
		if (testVector != Vector2(1.25, 2))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector *= 10.f;
		TestLog(testFlags, TXT("<1.25,2> *=10  ---> %s"), testVector);
		if (testVector != Vector2(12.5, 20))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector += ((Vector2(5.f,5.f) - copiedVector) * 4.f) + Vector2(-10.f,-15.f);
		TestLog(testFlags, TXT("<12.5,20> += ((<5,5> - <1,2>) * 4) + <-10,-15>    results in:  %s"), testVector);
		if (testVector != Vector2(18.5, 17))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17>.  It's currently:  %s"), testVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Vector2 firstVector(2.4f, -48.15f);
		Vector2 secondVector(-51.9f, 104.7f);
		Vector2 readFirstVector;
		Vector2 readSecondVector;
		DataBuffer vectorBuffer;

		vectorBuffer << firstVector;
		vectorBuffer << secondVector;
		vectorBuffer >> readFirstVector;
		vectorBuffer >> readSecondVector;

		if (firstVector != readFirstVector)
		{
			UnitTestError(testFlags, TXT("Vector streaming test failed.  The vector %s was pushed to data buffer.  The vector %s was pulled from that data buffer."), firstVector, readFirstVector);
			return false;
		}

		if (secondVector != readSecondVector)
		{
			UnitTestError(testFlags, TXT("Vector streaming test failed.  The 2nd vector %s was pushed to data buffer.  The 2nd vector %s was pulled from that data buffer."), secondVector, readSecondVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Vector2 expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Vector2 actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Vector 2 test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		testStr = TXT("(X = 0, Y = 0)");
		expected = Vector2::ZERO_VECTOR;
		if (!testParsing())
		{
			return false;
		}

		testStr = TXT("(X = 35, Y = 6)");
		expected = Vector2(35.f, 6.f);
		if (!testParsing())
		{
			return false;
		}

		testStr = TXT("(X=16.52,Y=98.25)");
		expected = Vector2(16.52f, 98.25f);
		if (!testParsing())
		{
			return false;
		}

		testStr = TXT("( X   =  -25.77 ,Y=-88.44 )");
		expected = Vector2(-25.77f, -88.44f);
		if (!testParsing())
		{
			return false;
		}

		expected = Vector2(-55.24f, 46.1f);
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	
	SetTestCategory(testFlags, TXT("Reflection"));
	{
		Vector2 testVector;
		Vector2 normal;
		Vector2 expected;

		std::function<bool()> testReflection([&]()
		{
			normal.NormalizeInline();
			Vector2 actual = testVector.CalcReflection(normal);
			if (!actual.IsNearlyEqual(expected))
			{
				UnitTestError(testFlags, TXT("Calculate Reflection test failed. Calculating the reflection of Vector %s over the normal %s should have resulted in %s. Instead it returned %s."), testVector, normal, expected, actual);
				return false;
			}

			return true;
		});

		testVector = Vector2(1.f, 0.f);
		normal = Vector2(0.f, 1.f);
		expected = Vector2(-1.f, 0.f);
		if (!testReflection())
		{
			return false;
		}

		normal = Vector2(1.f, 0.f);
		expected = Vector2(1.f, 0.f);
		if (!testReflection())
		{
			return false;
		}

		testVector = Vector2(0.5f, 0.5f);
		expected = Vector2(0.5f, -0.5f);
		if (!testReflection())
		{
			return false;
		}

		testVector = Vector2(20.f, 5.f);
		normal = Vector2(0.f, 1.f);
		expected = Vector2(-20.f, 5.f);
		if (!testReflection())
		{
			return false;
		}

		normal = Vector2(1.f, 1.f); //The test will automatically normalize this.
		expected = Vector2(5.f, 20.f);
		if (!testReflection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	{
		Vector2 testVector = Vector2(5,5);
		TestLog(testFlags, TXT("Vector Length of <5,5> is:  %s"), testVector.VSize());
		if (abs(testVector.VSize().Value - 7.071067812) > 0.000001)
		{
			UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5> is ~7.071067812.  Expected precision is:  10^-6."));
			return false;
		}

		testVector = Vector2(-10.f, 2.f);
		TestLog(testFlags, TXT("The squared length of %s is: %s"), testVector, testVector.CalcDistSquared());
		if (Float::Abs(testVector.CalcDistSquared() - 104.f) > 0.000001f)
		{
			UnitTestError(testFlags, TXT("CalcDistSquared function failed. The expected squared length of %s is 104. Instead it returned %s."), testVector, testVector.CalcDistSquared());
			return false;
		}

		{
			Vector2 ptA(5.f, 10.f);
			Vector2 ptB(-2.f, -20.f);
			Float expectedDist = 949.f;
			Float actualDist = Vector2::CalcSquaredDistBetween(ptA, ptB);
			if (!actualDist.IsCloseTo(expectedDist))
			{
				UnitTestError(testFlags, TXT("CalcSquaredDistBetween two points function failed. The expected squared distance between %s and %s should have been %s. Instead it calculated %s."), ptA, ptB, expectedDist, actualDist);
				return false;
			}

			ptA = Vector2(1.f, -8.f);
			ptB = Vector2(8.f, 2.f);
			expectedDist = 12.20655562f;
			actualDist = Vector2::CalcDistBetween(ptA, ptB);
			if (!actualDist.IsCloseTo(expectedDist))
			{
				UnitTestError(testFlags, TXT("CalcDistBetween function failed. The expected distance between %s and %s should have been %s. Instead it calculated %s."), ptA, ptB, expectedDist, actualDist);
				return false;
			}
		}

		testVector = Vector2(5.f, 5.f);
		testVector.NormalizeInline();
		TestLog(testFlags, TXT("Normalize<5,5> results in:  %s"), testVector);
		if (testVector.X - 0.70710678f < 0.000001f && testVector.Y - 0.70710678f > 0.000001f)
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.707107, 0.707107> with minimum precision of 10^-6."));
			return false;
		}

		Vector2 normalizeInput(0.f, -20.f);
		Vector2 normalVect = Vector2::Normalize(normalizeInput);
		if (normalVect != Vector2(0.f, -1.f))
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed. The expected normalized vector of %s is <0, -1>. Instead it's %s"), normalizeInput, normalVect);
			return false;
		}

		testVector = Vector2(0, 5);
		testVector.SetLengthTo(3.5);
		TestLog(testFlags, TXT("Setting Length of <0,5> to 3.5 results in:  %s"), testVector);
		if (testVector.VSize() != 3.5f || testVector != Vector2(0, 3.5f))
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,5> to 3.5 should have resulted in <0,3.5>.  The VSize of %s is %s instead."), testVector, testVector.VSize());
			return false;
		}

		testVector = Vector2(-191, 238);
		testVector.SetLengthTo(10.f);
		TestLog(testFlags, TXT("Setting length of <-191,238> to 10 results in:  %s"), testVector);
		if (Float::Abs(testVector.VSize() - 10.f) > 0.000001f || (testVector - Vector2(-6.258932f, 7.799088f)).VSize() > 0.000001f)
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-191,238> to 10 should have resulted in ~<-6.258932,7.799088>.  The VSize of %s is %s instead."), testVector, testVector.VSize());
			return false;
		}

		{
			TestLog(testFlags, TXT("Testing Vector2 clamp functions (min, max, and clamp)..."));
			std::function<bool(const Vector2&, const Vector2&)> compareVec = [&](const Vector2& actual, const Vector2& expected)
			{
				if (actual != expected)
				{
					UnitTestError(testFlags, TXT("Clamp test failed since the actual vector (%s) does not match the expected (%s)."), actual, expected);
					return false;
				}

				return true;
			};

			Vector2 originalVector(200.f, -100.f);
			Vector2 clampVector(0.f, 0.f);
			Vector2 resultingVector = Vector2::Min(originalVector, clampVector);
			Vector2 expectedVector(0.f, -100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector2(400.f, 150.f);
			resultingVector = Vector2::Min(originalVector, clampVector);
			expectedVector = originalVector;
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector2(-200.f, 200.f);
			resultingVector = Vector2::Min(originalVector, clampVector);
			expectedVector = Vector2(-200.f, -100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			resultingVector = Vector2::Max(originalVector, clampVector);
			expectedVector = Vector2(200.f, 200.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector2(250.f, -300.f);
			resultingVector = Vector2::Max(originalVector, clampVector);
			expectedVector = Vector2(250.f, -100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			Vector2 minVec(0.f, 0.f);
			Vector2 maxVec(100.f, 100.f);
			resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
			expectedVector = Vector2(100.f, 0.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			minVec = Vector2(-1000.f, 1000.f);
			maxVec = Vector2(-500.f, 5000.f);
			resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
			expectedVector = Vector2(-500.f, 1000.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			minVec = Vector2(100.f, -300.f);
			maxVec = Vector2(300.f, 100.f);
			resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
			expectedVector = originalVector;
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}
		}

		testVector = Vector2(5,5);
		Vector2 otherVector = Vector2(-11,-13);
		TestLog(testFlags, TXT("<5,5> dot <-11,-13> = %s"), testVector.Dot(otherVector));
		if (testVector.Dot(otherVector) != -120)
		{
			UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -120."), testVector, otherVector);
			return false;
		}

		Vector2 minBounds = Vector2(0, 0);
		Vector2 maxBounds = Vector2(100, 1000);
		Float alpha = 0.5f;
		Vector2 lerpVector = Vector2::Lerp(alpha, minBounds, maxBounds);
		TestLog(testFlags, TXT("Lerping Vector with minBounds=%s and maxBounds=%s with alpha %s resulted in %s."), testVector, minBounds, maxBounds, alpha, lerpVector);
		if (lerpVector != Vector2(50.f, 500.f))
		{
			UnitTestError(testFlags, TXT("Vector2 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (50, 500).  Instead it returned %s."), minBounds, maxBounds, alpha, lerpVector);
			return false;
		}

		alpha = 0.9f;
		lerpVector = Vector2::Lerp(alpha, minBounds, maxBounds);
		if (lerpVector != Vector2(90.f, 900.f))
		{
			UnitTestError(testFlags, TXT("Vector2 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (90, 900).  Instead it returned %s."), minBounds, maxBounds, alpha, lerpVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Vector2"));

	return true;
}

bool DatatypeUnitTester::TestVector3 (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Vector3"));

	Vector3 blankVector = Vector3::ZERO_VECTOR;
	Vector3 initializedVector(1, 2, 3);
	Vector3 copiedVector(initializedVector);
	Vector3 typecastedVector(Vector2(10,20).ToVector3());

	TestLog(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
	TestLog(testFlags, TXT("    initializedVector(1,2,3)= %s"), initializedVector);
	TestLog(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
	TestLog(testFlags, TXT("    typecastedVector from Vector2(10,20)= %s"), typecastedVector);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		TestLog(testFlags, TXT("Checking initializedVector axis. . ."));
		if (initializedVector[0] != 1.f || initializedVector[1] != 2.f || initializedVector[2] != 3.f)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  Failed to retrieve individual axis of initializedVector(%s).  It returned %s, %s, and %s instead of 1, 2, 3."), initializedVector, initializedVector[0], initializedVector[1], initializedVector[2]);
			return false;
		}

		TestLog(testFlags, TXT("initializedVector != copiedVector ? %s"), Bool(initializedVector != copiedVector));
		if (initializedVector != copiedVector)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), initializedVector, copiedVector);
			return false;
		}

		Vector3 testVector(5, 10, 15);
		TestLog(testFlags, TXT("<5,10,15> != <1,2,3> && (<5,10,15> == <99,99,15> || <5,10,15> == <5,10,15>) ? %s"), Bool(testVector != copiedVector && (testVector == Vector3(99,99,15) || testVector == Vector3(5,10,15))));
		if (!(testVector != copiedVector && (testVector != Vector3(99,99,15) || testVector == Vector3(5,10,15))))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), testVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		Vector3 testVector(-4.f, 5.f, 15.f);
		if (-testVector != Vector3(4.f, -5.f, -15.f))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed. The inverted vector of %s should have been <4,-5,-15>. Instead it's %s."), testVector, (-testVector));
			return false;
		}

		testVector = copiedVector + Vector3(5,9,12);
		TestLog(testFlags, TXT("<1,2,3> + <5,9,12> = %s"), testVector);
		if (testVector != Vector3(6,11,15))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11,15>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector = copiedVector - Vector3(4, 10, 2);
		TestLog(testFlags, TXT("<1,2,3> - <4,10,2> = %s"), testVector);
		if (testVector != Vector3(-3,-8,1))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8,1>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector += Vector3(4,6,9);
		TestLog(testFlags, TXT("<-3,-8,1> += <4,6,9>  ---> %s"), testVector);
		if (testVector != Vector3(1,-2,10))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2,10>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector += Vector2(4, -4).ToVector3();
		TestLog(testFlags, TXT("<1,-2,10> += <4,-4>"));
		if (testVector != Vector3(5, -6, 10))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <5, -6, 10>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector = Vector3(5.f, 8.f, 6.f) * 2.f;
		TestLog(testFlags, TXT("<5,8,6> * 2 = %s"), testVector);
		if (testVector != Vector3(10,16,12))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16,12>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector /= 8.f;
		TestLog(testFlags, TXT("<10,16,12> /=8  ---> %s"), testVector);
		if (testVector != Vector3(1.25, 2, 1.5))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2,1.5>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector *= 10.f;
		TestLog(testFlags, TXT("<1.25,2,1.5> *=10  ---> %s"), testVector);
		if (testVector != Vector3(12.5, 20, 15))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20,15>.  It's currently:  %s"), testVector);
			return false;
		}

		testVector += ((Vector2(5.f,5.f).ToVector3() - copiedVector) * 4.f) + Vector3(-10.f,-15.f,20.f);
		TestLog(testFlags, TXT("<12.5,20,15> += ((<5,5,0> - <1,2,3>) * 4) + <-10,-15,20>    results in:  %s"), testVector);
		if (testVector != Vector3(18.5, 17, 23))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17,23>.  It's currently:  %s"), testVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Vector3 firstVector(48.2f, -942.05f, -96.17f);
		Vector3 secondVector(-154.78f, 26.1f, 813.46f);
		Vector3 readFirstVector;
		Vector3 readSecondVector;
		DataBuffer vectorBuffer;

		vectorBuffer << firstVector;
		vectorBuffer << secondVector;
		vectorBuffer >> readFirstVector;
		vectorBuffer >> readSecondVector;

		if (firstVector != readFirstVector)
		{
			UnitTestError(testFlags, TXT("Vector3 streaming test failed.  The vector %s was pushed to a data buffer.  The vector %s was pulled from that data buffer."), firstVector, readFirstVector);
			return false;
		}

		if (secondVector != readSecondVector)
		{
			UnitTestError(testFlags, TXT("Vector3 streaming test failed.  The 2nd vector %s was pushed to a data buffer.  The 2nd vector %s was pulled from that data buffer."), secondVector, readSecondVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Vector3 expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Vector3 actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Vector3 test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Vector3::ZERO_VECTOR;
		testStr = TXT("(X = 0, Y = 0, Z = 0)");
		if (!testParsing())
		{
			return false;
		}

		expected = Vector3(8.f, 5.f, 40.f);
		testStr = TXT("(X = 8, Y = 5, Z = 40.00000)");
		if (!testParsing())
		{
			return false;
		}

		expected = Vector3(3.f, 18.f, 27.f);
		testStr = TXT("(Y = 18, X = 3, Z = 27)");
		if (!testParsing())
		{
			return false;
		}

		expected = Vector3(14.6f, 31.79f, 5.2f);
		testStr = TXT("(Z=5.2, Y=31.79, X= 14.6)");
		if (!testParsing())
		{
			return false;
		}

		expected = Vector3(-24.98f, -55.21f, -1029.7f);
		testStr = TXT("NOT USED X Y Z (   Z = -1029.7 ,   X    = -24.98   , Y   = -55.21   ) TAIL 456.54");
		if (!testParsing())
		{
			return false;
		}

		expected = Vector3(298.15f, -47.42f, 728.23f);
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Reflection"));
	{
		Vector3 testVector;
		Vector3 normal;
		Vector3 expected;

		std::function<bool()> testReflection([&]()
		{
			normal.NormalizeInline();
			Vector3 actual = testVector.CalcReflection(normal);
			if (!actual.IsNearlyEqual(expected))
			{
				UnitTestError(testFlags, TXT("Calculate Reflection test failed. Calculating the reflection of Vector %s over the normal %s should have resulted in %s. Instead it returned %s."), testVector, normal, expected, actual);
				return false;
			}

			return true;
		});

		testVector = Vector3(1.f, 0.f, 0.f);
		normal = Vector3(0.f, 1.f, 0.f);
		expected = Vector3(-1.f, 0.f, 0.f);
		if (!testReflection())
		{
			return false;
		}

		normal = Vector3(1.f, 0.f, 0.f);
		expected = Vector3(1.f, 0.f, 0.f);
		if (!testReflection())
		{
			return false;
		}

		testVector = Vector3(0.5f, 0.5f, 0.5f);
		expected = Vector3(0.5f, -0.5f, -0.5f);
		if (!testReflection())
		{
			return false;
		}

		testVector = Vector3(20.f, 5.f, -6.f);
		normal = Vector3(0.f, 1.f, 0.f);
		expected = Vector3(-20.f, 5.f, 6.f);
		if (!testReflection())
		{
			return false;
		}

		normal = Vector3(1.f, 1.f, 0.f); //The test will automatically normalize this.
		expected = Vector3(5.f, 20.f, 6.f);
		if (!testReflection())
		{
			return false;
		}

		testVector = Vector3(16.f, 13.5f, 45.f);
		normal = Vector3(0.f, 0.f, 1.f);
		expected = Vector3(-16.f, -13.5f, 45.f);
		if (!testReflection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	{
		Vector3 testVector = Vector3(5,5,-10);
		TestLog(testFlags, TXT("Vector Length of <5,5,-10> is:  %s"), testVector.VSize());
		if (abs(testVector.VSize().Value - 12.24744871) > 0.000001)
		{
			UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5,-10> is ~12.24744871.  Expected precision is:  10^-6."));
			return false;
		}

		testVector.NormalizeInline();
		TestLog(testFlags, TXT("Normalize<5,5,-10> results in:  %s"), testVector);
		if (testVector.X - 0.40824829f < 0.000001f && testVector.Y - 0.40824829f > 0.000001f && testVector.Z + 0.816496581f > 0.000001f)
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.408248, 0.408248, -0.816497> with minimum precision of 10^-6."));
			return false;
		}

		Vector3 normalizeInput(5.f, 0.f, 0.f);
		Vector3 normalVect = Vector3::Normalize(normalizeInput);
		if (normalVect != Vector3(1.f, 0.f, 0.f))
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed. The expected normalized vector of %s is <1, 0, 0>. Instead it's %s"), normalizeInput, normalVect);
			return false;
		}

		testVector = Vector3(5.f, -4.f, 12.f);
		TestLog(testFlags, TXT("Vector's squared length of <5, -4, 12> is:  %s"), testVector.CalcDistSquared());
		Float expectedDist = 185.f;
		if (abs(testVector.CalcDistSquared().Value - expectedDist.Value) > 0.000001f)
		{
			UnitTestError(testFlags, TXT("Calc distance squared function failed.  Expected squared distance of vector %s is ~%s.  It computed %s instead."), testVector, expectedDist, testVector.CalcDistSquared());
			return false;
		}

		testVector = Vector3(0, 0, -15);
		testVector.SetLengthTo(4.25);
		TestLog(testFlags, TXT("Setting Length of <0,0,-15> to 4.25 results in:  %s"), testVector);
		if (testVector.VSize() != 4.25f || testVector != Vector3(0, 0, -4.25f))
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,0,-15> to 4.25 should have resulted in <0,0,4.25>.  The VSize of %s is %s instead."), testVector, testVector.VSize());
			return false;
		}

		testVector = Vector3(-84.3f, 148.2f, 76.7f);
		testVector.SetLengthTo(20.4f);
		TestLog(testFlags, TXT("Setting length of <-84.3, 148.2, 76.7> to 20.4 results in:  %s"), testVector);
		if (Float::Abs(testVector.VSize() - 20.4f) > 0.00001f || (testVector - Vector3(-9.198518f, 16.171061f, 8.369233f)).VSize() > 0.00001f)
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-84.3, 148.2, 76.7> to 20.4 should have resulted in ~<-9.198518249, 16.17106055, 8.369233093>.  The VSize of %s is %s instead."), testVector, testVector.VSize());
			return false;
		}

		{
			TestLog(testFlags, TXT("Testing Vector3 clamp functions (min, max, and clamp)..."));
			std::function<bool(const Vector3&, const Vector3&)> compareVec = [&](const Vector3& actual, const Vector3& expected)
			{
				if (actual != expected)
				{
					UnitTestError(testFlags, TXT("Clamp test failed since the actual vector (%s) does not match the expected (%s)."), actual, expected);
					return false;
				}

				return true;
			};

			Vector3 originalVector(0.f, -100.f, 100.f);
			Vector3 clampVector(0.f, 0.f, 0.f);
			Vector3 resultingVector = Vector3::Min(originalVector, clampVector);
			Vector3 expectedVector(0.f, -100.f, 0.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector3(-500.f, -750.f, -1000.f);
			resultingVector = Vector3::Min(originalVector, clampVector);
			expectedVector = clampVector;
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector3(-200.f, 100.f, 50.f);
			resultingVector = Vector3::Min(originalVector, clampVector);
			expectedVector = Vector3(-200.f, -100.f, 50.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			resultingVector = Vector3::Max(originalVector, clampVector);
			expectedVector = Vector3(0.f, 100.f, 100);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			clampVector = Vector3(-10.f, -50.f, 50.f);
			resultingVector = Vector3::Max(originalVector, clampVector);
			expectedVector = Vector3(0.f, -50.f, 100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			Vector3 minVec(0.f, 0.f, 0.f);
			Vector3 maxVec(50.f, 50.f, 50.f);
			resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
			expectedVector = Vector3(0.f, 0.f, 50.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			minVec = Vector3(-1000.f, 1000.f, 50.f);
			maxVec = Vector3(-500.f, 5000.f, 150.f);
			resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
			expectedVector = Vector3(-500.f, 1000.f, 100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			minVec = Vector3(100.f, -300.f, 0.f);
			maxVec = Vector3(350.f, -200.f, 250.f);
			resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
			expectedVector = Vector3(100.f, -200.f, 100.f);
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}

			minVec = Vector3(-10.f, -150.f, 50.f);
			maxVec = Vector3(10, -50.f, 150.f);
			resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
			expectedVector = originalVector;
			if (!compareVec(resultingVector, expectedVector))
			{
				return false;
			}
		}

		testVector = Vector3(5,5,-10);
		Vector3 otherVector = Vector3(-11,-13,17);
		TestLog(testFlags, TXT("<5,5,-10> dot <-11,-13,17> = %s"), testVector.Dot(otherVector));
		if (testVector.Dot(otherVector) != -290)
		{
			UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -290."), testVector, otherVector);
			return false;
		}

		//Example is based on:  Khan Academy.  Great place to brush up on vector math!
		testVector = Vector3(1, -7, 1);
		otherVector = Vector3(5, 2, 4);
		TestLog(testFlags, TXT("<1,-7,1> cross <5,2,4> = %s"), testVector.CrossProduct(otherVector));
		if (testVector.CrossProduct(otherVector) != Vector3(-30, 1, 37))
		{
			UnitTestError(testFlags, TXT("Cross product of vectors %s and %s failed.  Expected value is <-30,1,37>."), testVector, otherVector);
			return false;
		}

		Vector3 minBounds = Vector3(0.f, 0.f, -100.f);
		Vector3 maxBounds = Vector3(100.f, 1000.f, 100.f);
		Float alpha = 0.5f;
		Vector3 lerpVector = Vector3::Lerp(alpha, minBounds, maxBounds);
		TestLog(testFlags, TXT("Lerping Vector with minBounds=%s and maxBounds=%s with alpha %s resulted in %s."), testVector, minBounds, maxBounds, alpha, lerpVector);
		if (lerpVector != Vector3(50.f, 500.f, 0.f))
		{
			UnitTestError(testFlags, TXT("Vector3 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (50, 500, 0).  Instead it returned %s."), minBounds, maxBounds, alpha, lerpVector);
			return false;
		}

		alpha = 0.9f;
		lerpVector = Vector3::Lerp(alpha, minBounds, maxBounds);
		if (lerpVector != Vector3(90.f, 900.f, 80.f))
		{
			UnitTestError(testFlags, TXT("Vector3 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (90, 900, 80).  Instead it returned %s."), minBounds, maxBounds, alpha, lerpVector);
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Vector3"));

	return true;
}

bool DatatypeUnitTester::TestRotator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Rotator"));

	Rotator rawRotator(348, 293, 192);
	Rotator upRotator(180.f, 90.f, 45.f, Rotator::RU_Degrees);
	Rotator copiedRotator(upRotator);
	Rotator backwardRotator(-180.f, 0.f, 0.f, Rotator::RU_Degrees);

	Float tolerance = Rotator::ROTATOR_UNIT;

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		if (upRotator == backwardRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The up %s and backward %s rotators should not be the same."), upRotator, backwardRotator);
			return false;
		}

		if (upRotator != copiedRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s constructed from the up rotator %s should have been considered the same."), copiedRotator, upRotator);
			return false;
		}

		//Test wrapping (negative directions should automatically be converted to positive units).
		Rotator normalDegrees(195.f, 210.f, 18.f, Rotator::RU_Degrees);
		Rotator normalRadians(PI_FLOAT * 0.6f, PI_FLOAT * 0.8f, PI_FLOAT * 0.4f, Rotator::RU_Radians);
		Rotator reversedDegrees(-360.f + normalDegrees.GetYaw(Rotator::RU_Degrees), -360.f + normalDegrees.GetPitch(Rotator::RU_Degrees), -360.f + normalDegrees.GetRoll(Rotator::RU_Degrees), Rotator::RU_Degrees);
		Rotator reversedRadians((PI_FLOAT * -2.f) + normalRadians.GetYaw(Rotator::RU_Radians), (PI_FLOAT * -2.f) + normalRadians.GetPitch(Rotator::RU_Radians), (PI_FLOAT * -2.f) + normalRadians.GetRoll(Rotator::RU_Radians), Rotator::RU_Radians);
		if (!normalDegrees.IsNearlyEqual(reversedDegrees, tolerance) || !normalRadians.IsNearlyEqual(reversedRadians, tolerance))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Constructing rotators with negative values did not properly wrapped around to positive values.  Degrees:  expected (%s), actual (%s).  Radians:  expected (%s), actual (%s)."), normalDegrees, reversedDegrees, normalRadians, reversedRadians);
			return false;
		}

		Rotator wrappedDegrees(360.f + normalDegrees.GetYaw(Rotator::RU_Degrees), 720.f + normalDegrees.GetPitch(Rotator::RU_Degrees), 1080.f + normalDegrees.GetRoll(Rotator::RU_Degrees), Rotator::RU_Degrees);
		Rotator wrappedRadians((PI_FLOAT * 2.f) + normalRadians.GetYaw(Rotator::RU_Radians), (PI_FLOAT * 4.f) + normalRadians.GetPitch(Rotator::RU_Radians), (PI_FLOAT * 6.f) + normalRadians.GetRoll(Rotator::RU_Radians), Rotator::RU_Radians);
		if (!normalDegrees.IsNearlyEqual(wrappedDegrees, tolerance) || !normalRadians.IsNearlyEqual(wrappedRadians, tolerance))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Constructing rotators with values greater than a full revolution should have automatically wrapped back down between 0 to one revolution.  Degrees:  expected (%s), actual (%s).  Radians:  expected (%s), actual (%s)."), normalDegrees, wrappedDegrees, normalRadians, wrappedRadians);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Unit Conversions"));
	{
		//Construct three identical rotators from different units
		unsigned int quarterAngle = Rotator::FULL_REVOLUTION / 8;
		Rotator sdRotator(quarterAngle, quarterAngle * 2, quarterAngle * 7);

		Rotator degreeRotator(45.f, 90.f, 315.f, Rotator::RU_Degrees);

		Float quarterAngleRadian = PI_FLOAT / 4.f;
		Rotator radianRotator(quarterAngleRadian, quarterAngleRadian * 2.f, quarterAngleRadian * 7.f, Rotator::RU_Radians);

		if (!sdRotator.IsNearlyEqual(degreeRotator, tolerance) || !sdRotator.IsNearlyEqual(radianRotator, tolerance))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After creating 3 identical rotators from different units, the 3 rotators were not considered to be the same:  %s (SD Units), %s (degrees), %s (radians)."), sdRotator, degreeRotator, radianRotator);
			return false;
		}

		//Snap the radianRotator since PI_FLOAT is not precise enough to snap to right angles.
		radianRotator = sdRotator;

		//Pull the components in different units
		{
			//No need to test SD Units since the degree and radian rotators are already saving data in SD units, and the comparison earlier already checked for that.

			//Test degrees
			Float yawTarget = 45.f;
			Float pitchTarget = 90.f;
			Float rollTarget = 315.f;
			Float yawDegree;
			Float pitchDegree;
			Float rollDegree;
			degreeRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);

			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a degree Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), degreeRotator, yawDegree, pitchDegree, rollDegree, yawTarget, pitchTarget, rollTarget);
				return false;
			}

			sdRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);
			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a SD Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), sdRotator, yawDegree, pitchDegree, rollDegree, yawTarget, pitchTarget, rollTarget);
				return false;
			}

			radianRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);
			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a radian Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), radianRotator, yawDegree, pitchDegree, rollDegree, yawTarget, pitchTarget, rollTarget);
				return false;
			}

			//Test radians
			yawTarget = quarterAngleRadian;
			pitchTarget = quarterAngleRadian * 2.f;
			rollTarget = quarterAngleRadian * 7.f;
			Float yawRadian;
			Float pitchRadian;
			Float rollRadian;

			radianRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a radian Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), radianRotator, yawRadian, pitchRadian, rollRadian, yawTarget, pitchTarget, rollTarget);
				return false;
			}

			sdRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a SD Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), sdRotator, yawRadian, pitchRadian, rollRadian, yawTarget, pitchTarget, rollTarget);
				return false;
			}

			degreeRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a degree Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), degreeRotator, yawRadian, pitchRadian, rollRadian, yawTarget, pitchTarget, rollTarget);
				return false;
			}
		}

		//Constructed Rotator from signed ints
		Int intYaw = 40000; //Test against normal condition
		Int intPitch = 264144; //Test against wrapping
		Int intRoll = -2536; //Test against negative numbers
		Rotator signedIntRotator = Rotator::MakeRotator(intYaw, intPitch, intRoll);
		Rotator expectedIntRotator(40000, 2000, 63000);
		if (signedIntRotator != expectedIntRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Failed to construct a rotator from signed ints (Yaw=%s, Pitch=%s, Roll=%s).  It constructed %s instead of %s."), intYaw, intPitch, intRoll, signedIntRotator, expectedIntRotator);
			return false;
		}

		//Construct Rotator from a single uint64
		Rotator singleIntRotator(3483, 9462, 7193);
		uint64 intConversion = singleIntRotator.ToInt();
		Rotator convertedSingleIntRotator(intConversion);
		if (convertedSingleIntRotator != singleIntRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed. Failed to convert rotator %s to a single uint64 and using that uint64 to produce the original rotator. It ended up creating %s instead."), singleIntRotator, convertedSingleIntRotator);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		//Simple add
		Rotator left(123, 456, 789);
		Rotator right(987, 654, 321);
		Rotator sum = left + right;
		Rotator expected(1110, 1110, 1110);
		if (sum != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Adding the rotators %s and %s resulted in %s instead of %s."), left, right, sum, expected);
			return false;
		}

		//Simple subtraction
		left = Rotator(186, 3084, 8465);
		right = Rotator(4379, 1543, 7361);
		Rotator subtraction = left - right;
		expected = Rotator(61343, 1541, 1104); //Yaw should automatically wrap to positive values.
		if (subtraction != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Subtracting the rotators %s and %s resulted in %s instead of %s."), left, right, subtraction, expected);
			return false;
		}

		unsigned int incrementSize = Rotator::FULL_REVOLUTION / 36; //10 degrees
		unsigned int numIncrements = 96;
		Rotator adjustedRotator(0, 0, 0);

		for (unsigned int i = 0; i < numIncrements; ++i)
		{
			adjustedRotator.Yaw += incrementSize;

			unsigned int expectedRotation = (incrementSize * (i+1)) % Rotator::FULL_REVOLUTION;
			if (adjustedRotator.Yaw != expectedRotation)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After incrementing a rotator by 10 degrees %s time(s), the resulting rotator's yaw is %s.  Its yaw expected to be %s instead."), DString::MakeString(i), DString::MakeString(adjustedRotator.Yaw), DString::MakeString(expectedRotation));
				return false;
			}
		}

		Rotator decrementingRotator(2000, 2200, 63000);
		adjustedRotator = Rotator(0, 0, 0);

		for (unsigned int i = 0; i < numIncrements; ++i)
		{
			adjustedRotator -= decrementingRotator;
			Rotator expectedRotator = Rotator::MakeRotator(-2000 * (i+1), -2200 * (i+1), -63000 * (i+1));
			if (adjustedRotator != expectedRotator)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After decrementing a blank rotator by %s by %s many time(s), the resulting rotator is %s instead of %s."), decrementingRotator, DString::MakeString(i), adjustedRotator, expectedRotator);
				return false;
			}
		}

		//Test the add offset functions
		Float tenDegrees = Rotator::FULL_REVOLUTION_Float / 36.f;
		Rotator addOffsetTester = Rotator::ZERO_ROTATOR;
		expected = Rotator::ZERO_ROTATOR;
		expected.SetPitch(10.f, Rotator::RU_Degrees);
		addOffsetTester.AddPitch(tenDegrees);
		if (addOffsetTester != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed. Adding a 10 degree offset to a rotator should have resulted in %s. Instead it's %s."), addOffsetTester, expected);
			return false;
		}

		//Test going negative
		addOffsetTester.AddYaw(-tenDegrees);
		expected.SetYaw(350.f, Rotator::RU_Degrees);
		if (addOffsetTester != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed. Subtracting a 10 degree offset to a rotator should have resulted in %s. Instead it's %s."), addOffsetTester, expected);
			return false;
		}

		//Test going beyond 360
		addOffsetTester.AddRoll(tenDegrees * 75.f);
		expected.SetRoll(30.f, Rotator::RU_Degrees);
		if (addOffsetTester != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed. Adding a 750 degree offset to a rotator should have resulted in %s. Instead it's %s."), addOffsetTester, expected);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Rotator streamWriteRotator(164, 2983, 64028);
		Rotator streamWriteRotator2(70.f, -45.f, 720.f, Rotator::RU_Degrees);
		Rotator streamReadRotator;
		Rotator streamReadRotator2;
		DataBuffer rotatorBuffer;

		rotatorBuffer << streamWriteRotator;
		rotatorBuffer << streamWriteRotator2;
		rotatorBuffer >> streamReadRotator;
		rotatorBuffer >> streamReadRotator2;

		if (streamReadRotator != streamWriteRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s was pushed to a stream buffer, but %s was read from it instead."), streamWriteRotator, streamReadRotator);
			return false;
		}

		if (streamReadRotator2 != streamWriteRotator2)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s was pushed to a stream buffer, but %s was read from it instead."), streamWriteRotator2, streamReadRotator2);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Rotator expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Rotator actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Rotator test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Rotator::ZERO_ROTATOR;
		testStr = TXT("(Y = 0, P = 0, R = 0)");
		if (!testParsing())
		{
			return false;
		}

		expected.SetYaw(45.f, Rotator::RU_Degrees);
		expected.SetPitch(70.f, Rotator::RU_Degrees);
		expected.SetRoll(110.f, Rotator::RU_Degrees);
		testStr = TXT("(Y = 45, P=70, R=110)");
		if (!testParsing())
		{
			return false;
		}

		expected.SetYaw(15.25f, Rotator::RU_Degrees);
		expected.SetPitch(95.8f, Rotator::RU_Degrees);
		expected.SetRoll(160.15f, Rotator::RU_Degrees);
		testStr = TXT("(P = 95.8, R=160.15, Y = 15.25)");
		if (!testParsing())
		{
			return false;
		}

		expected.SetYaw(-20.25f, Rotator::RU_Degrees);
		expected.SetPitch(-35.2f, Rotator::RU_Degrees);
		expected.SetRoll(-70.1f, Rotator::RU_Degrees);
		testStr = TXT("(R = -70.1, Y = -20.25, P = -35.2)");
		if (!testParsing())
		{
			return false;
		}

		expected = Rotator(243, 297, 2398);
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Rotator < - > Vector Conversions"));
	{
		Vector3 testVector;
		Rotator expected;
		std::function<bool()> testConversion([&]()
		{
			Rotator actual(testVector);
			if (!expected.IsNearlyEqual(actual))
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), testVector, actual, expected);
				return false;
			}

			//Convert it back to a vector.
			Vector3 converted = actual.GetDirectionalVector();
			Vector3 normTestVector = Vector3::Normalize(testVector);
			if (!normTestVector.IsNearlyEqual(converted, 0.0001f))
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector as normalized.  Instead it returned %s."), actual, testVector, normTestVector);
				return false;
			}

			return true;
		});

		testVector = Vector3(1.f, 0.f, 0.f);
		expected = Rotator(0, 0, 0);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(-1.f, 0.f, 0.f);
		expected = Rotator(180.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(0.f, 0.f, 1.f);
		expected = Rotator(0.f, 90.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(0.f, 0.f, -1.f);
		expected = Rotator(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(0.f, -500.f, 0.f);
		expected = Rotator(90.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(0.f, 500.f, 0.f);
		expected = Rotator(-90.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(128.f, 128.f, 0.f);
		expected = Rotator(-45.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		testVector = Vector3(-64.f, 0.f, -64.f);
		expected = Rotator(180.f, -45.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}

		//This test confused me for a long time. But it turns out, that the test vector cannot be 1,1,1 and generate a rotation of -45,45,0.
		//If I were to draw a line through the center of the sphere at a 45 degree pitch and yaw, that line will not hit the corners of the cube.
		testVector = Vector3(0.5f, 0.5f, 0.7071068f);
		expected = Rotator(-45.f, 45.f, 0.f, Rotator::RU_Degrees);
		if (!testConversion())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Rotator Utils"));
	{
		Rotator up(0.f, 90.f, 0.f, Rotator::RU_Degrees);
		Rotator forward(0.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator down(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		Rotator right(-90.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator upRight(45.f, 45.f, 0.f, Rotator::RU_Degrees);
		Rotator other(-45.f, 0.f, 0.f, Rotator::RU_Degrees);

		//Test for parallel
		if (!up.IsParallelTo(up))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Rotators should be parallel to themselves.  For some reason %s is not parallel to itself."), up);
			return false;
		}

		if (!up.IsParallelTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  %s should have been considered parallel to %s."), up, down);
			return false;
		}

		if (up.IsParallelTo(forward) || up.IsParallelTo(right) || up.IsParallelTo(other))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The %s rotation should not be parallel to any of the following rotators:  %s, %s, %s."), up, forward, right, other);
			return false;
		}

		if (up.IsPerpendicularTo(up))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Rotations should never be perpendicular to themselves."));
			return false;
		}

		if (up.IsPerpendicularTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should not be considered perpendicular to %s."), up, down);
			return false;
		}

		if (!forward.IsPerpendicularTo(right))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), forward, right);
			return false;
		}

		if (!forward.IsPerpendicularTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), forward, down);
			return false;
		}

		if (right.IsPerpendicularTo(upRight))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should not have been considered perpendicular to %s."), right, upRight);
			return false;
		}

		if (!upRight.IsPerpendicularTo(other))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), upRight, other);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Inverting Rotators"));
	{
		Rotator testRotator;
		Rotator expected;
		std::function<bool()> testInversion([&]()
		{
			Rotator inverted(testRotator);
			inverted.Invert();
			if (!inverted.IsNearlyEqual(expected))
			{
				UnitTestError(testFlags, TXT("Rotator test failed. Inverting rotator %s should have resulted in %s. Instead it ended up being %s."), testRotator, expected, inverted);
				return false;
			}

			if (!inverted.IsParallelTo(testRotator, 0.001f) || inverted.IsNearlyEqual(testRotator))
			{
				UnitTestError(testFlags, TXT("Rotator test failed. Inverting rotator %s should generate a parallel rotator in the opposite direction. Instead it generated a %s."), testRotator, inverted);
				return false;
			}

			return true;
		});

		testRotator = Rotator(0, 0, 0);
		expected = Rotator(180.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(90.f, 0.f, 0.f, Rotator::RU_Degrees);
		expected = Rotator(-90.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(-1.f, 0.f, 0.f, Rotator::RU_Degrees);
		expected = Rotator(179.f, 0.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(0.f, 90.f, 0.f, Rotator::RU_Degrees);
		expected = Rotator(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(0.f, 45.f, 0.f, Rotator::RU_Degrees);
		expected = Rotator(180.f, -45.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(90.f, 90.f, 0.f, Rotator::RU_Degrees);
		expected = Rotator(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}

		testRotator = Rotator(10.f, 45.f, 500.f, Rotator::RU_Degrees);
		expected = Rotator(190.f, -45.f, 500.f, Rotator::RU_Degrees);
		if (!testInversion())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Short Direction Multiplier"));
	{
		std::function<bool(unsigned int, unsigned int, Float)> testShorterDirection([&](unsigned int startAngle, unsigned int desiredAngle, Float expectedDir)
		{
			Float actual = Rotator::GetShorterDirectionMultiplier(startAngle, desiredAngle);
			if (actual != expectedDir)
			{
				UnitTestError(testFlags, TXT("Rotator test failed. The expected shorter direction from angle %s to %s should have been %s. Instead it's %s."), Int(startAngle), Int(desiredAngle), expectedDir, actual);
				return false;
			}

			return true;
		});

		if (!testShorterDirection(0, 0, 0.f))
		{
			return false;
		}

		if (!testShorterDirection(0, Rotator::QUARTER_REVOLUTION, 1.f))
		{
			return false;
		}

		if (!testShorterDirection(0, Rotator::HALF_REVOLUTION - 1, 1.f))
		{
			return false;
		}

		if (!testShorterDirection(0, Rotator::HALF_REVOLUTION + 1, -1.f))
		{
			return false;
		}

		if (!testShorterDirection(0, Rotator::FULL_REVOLUTION - 1, -1.f))
		{
			return false;
		}

		if (!testShorterDirection(Rotator::QUARTER_REVOLUTION, 0, -1.f))
		{
			return false;
		}

		if (!testShorterDirection(Rotator::QUARTER_REVOLUTION, Rotator::HALF_REVOLUTION + 100, 1.f))
		{
			return false;
		}

		if (!testShorterDirection(Rotator::QUARTER_REVOLUTION, Rotator::FULL_REVOLUTION - 1, -1.f))
		{
			return false;
		}

		if (!testShorterDirection(Rotator::QUARTER_REVOLUTION * 3, 0, 1.f))
		{
			return false;
		}

		if (!testShorterDirection(Rotator::QUARTER_REVOLUTION * 3, Rotator::HALF_REVOLUTION, -1.f))
		{
			return false;
		}

		std::function<bool(Float, Float, Rotator::ERotationUnit, Float)> testShorterDirectionUnits([&](Float startAngle, Float desiredAngle, Rotator::ERotationUnit unit, Float expected)
		{
			Float actual = Rotator::GetShorterDirectionMultiplier(startAngle, desiredAngle, unit);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Rotator test failed. The expected shorter direction from angle %s to %s should have been %s. Instead it's %s."), startAngle, desiredAngle, expected, actual);
				return false;
			}

			return true;
		});

		//Run the tests twice. One for degrees. The other for radians
		for (int i = 0; i < 2; ++i)
		{
			//0 = degrees, 1 = radians
			Rotator::ERotationUnit unit = (i == 0) ? Rotator::RU_Degrees : Rotator::RU_Radians;
			Float quarterRev = (unit == Rotator::RU_Degrees) ? 90.f : PI_FLOAT * 0.5f;
			Float halfRev = quarterRev * 2.f;
			Float fullRev = halfRev * 2.f;
			Float unitOffset = (unit == Rotator::RU_Degrees) ? 1.f : 0.02f;

			if (!testShorterDirectionUnits(halfRev, halfRev, unit, 0.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(0.f, quarterRev, unit, 1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(0.f, halfRev - unitOffset, unit, 1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(0.f, halfRev + unitOffset, unit, -1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(0.f, quarterRev * 3.f, unit, -1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(quarterRev, 0.f, unit, -1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(quarterRev, halfRev, unit, 1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(quarterRev, fullRev, unit, -1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(halfRev, fullRev - unitOffset, unit, 1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(halfRev, quarterRev, unit, -1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(quarterRev * 3.f, fullRev, unit, 1.f))
			{
				return false;
			}

			if (!testShorterDirectionUnits(quarterRev * 3.f, halfRev, unit, -1.f))
			{
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Rotator"));

	return true;
}

bool DatatypeUnitTester::TestRectangle (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Rectangle"));

	Rectangle blankRectangle;
	Rectangle initializedRectangle(10.f, 5.f, Vector2(-10.f, 15.f));
	Rectangle copiedRectangle(initializedRectangle);

	TestLog(testFlags, TXT("Constructor tests:  BlankRectangle=%s"), blankRectangle);
	TestLog(testFlags, TXT("    initializedRectangle=%s"), initializedRectangle);
	TestLog(testFlags, TXT("    copiedRectangle from initializedRectangle=%s"), copiedRectangle);

	SetTestCategory(testFlags, TXT("Comparison"));
	if (blankRectangle != Rectangle(0.f, 0.f, Vector2::ZERO_VECTOR))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  blankRectangle is not zero.  Instead it's:  %s"), blankRectangle);
		return false;
	}

	TestLog(testFlags, TXT("initializedRectangle(%s) != copiedRectangle(%s) ? %s"), initializedRectangle, copiedRectangle, Bool(initializedRectangle != copiedRectangle));
	if (initializedRectangle != copiedRectangle)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRectangle != copiedRectangle.  Values are:  %s and %s"), initializedRectangle, copiedRectangle);
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Scalar"));
	{
		Rectangle testRectangle(2.f, 4.f, Vector2(-10, 10));
		Rectangle originalRectangle(testRectangle);
		Float scalar = 4.f;

		testRectangle *= scalar;
		Rectangle expectedRectangle(8.f, 16.f, Vector2(-40.f, 40.f));
		if (testRectangle != expectedRectangle)
		{
			UnitTestError(testFlags, TXT("Scalar test failed. After scaling %s by %s. It should have returned %s. Instead it's %s."), originalRectangle, scalar, expectedRectangle, testRectangle);
			return false;
		}

		testRectangle = originalRectangle;
		Rectangle scalarRect(10.f, 2.f, Vector2(-5.f, 2.f));
		testRectangle *= scalarRect;
		expectedRectangle = Rectangle(20.f, 8.f, Vector2(50.f, 20.f));
		if (testRectangle != expectedRectangle)
		{
			UnitTestError(testFlags, TXT("Scalar test failed. After scaling %s by %s. It should have returned %s. Instead it's %s."), originalRectangle, scalarRect, expectedRectangle, testRectangle);
			return false;
		}

		testRectangle = originalRectangle;
		scalar = 2.f;
		testRectangle = testRectangle / scalar;
		expectedRectangle = Rectangle(1.f, 2.f, Vector2(-5.f, 5.f));
		if (testRectangle != expectedRectangle)
		{
			UnitTestError(testFlags, TXT("Scalar test failed. After dividing %s by %s. It should have returned %s. Instead it's %s."), originalRectangle, scalar, expectedRectangle, testRectangle);
			return false;
		}

		testRectangle = originalRectangle;
		scalarRect = Rectangle(0.1f, 2.f, Vector2(-0.5f, -2.f));
		testRectangle = testRectangle / scalarRect;
		expectedRectangle = Rectangle(20.f, 2.f, Vector2(20.f, -5.f));
		if (testRectangle != expectedRectangle)
		{
			UnitTestError(testFlags, TXT("Scalar test failed. After dividing %s by %s. It should have returned %s. Instead it's %s."), originalRectangle, scalarRect, expectedRectangle, testRectangle);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Rectangle streamRectangleA(12.f, 18.f, Vector2(12.f, -35.f));
		Rectangle streamRectangleB(98.f, 4.f, Vector2(-128.f, 12.f));
		Rectangle readStreamRectangleA;
		Rectangle readStreamRectangleB;
		DataBuffer rectangleBuffer;

		rectangleBuffer << streamRectangleA;
		rectangleBuffer << streamRectangleB;
		rectangleBuffer >> readStreamRectangleA;
		rectangleBuffer >> readStreamRectangleB;

		if (streamRectangleA != readStreamRectangleA)
		{
			UnitTestError(testFlags, TXT("Rectangle stream test failed.  The first rectangle %s was pushed to data buffer.  The first rectangle %s was pulled from data buffer."), streamRectangleA, readStreamRectangleA);
			return false;
		}

		if (streamRectangleB != readStreamRectangleB)
		{
			UnitTestError(testFlags, TXT("Rectangle stream test failed.  The second rectangle %s was pushed to data buffer.  The second rectangle %s was pulled from data buffer."), streamRectangleB, readStreamRectangleB);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Rectangle expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Rectangle actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Rectangle test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Rectangle();
		testStr = TXT("(Width=0, Height=0, Center=(X=0, Y=0, Z=0))");
		if (!testParsing())
		{
			return false;
		}

		expected = Rectangle(5.f, 16.f, Vector2(12.f, 25.f));
		testStr = TXT("(Width = 5, Height = 16, Center = (X = 12, Y = 25))");
		if (!testParsing())
		{
			return false;
		}

		expected = Rectangle(15.2f, 19.4f, Vector2(42.6f, 57.621f));
		testStr = TXT("(Center=(X=42.6, Y=57.621), Width=15.2, Height=19.4)");
		if (!testParsing())
		{
			return false;
		}

		expected = Rectangle(94.25f, 21.43f, Vector2(-55.72f, -81.93f));
		testStr = TXT("(Width = 94.25, Center = (Y = -81.93, X = -55.72), Height = 21.43)");
		if (!testParsing())
		{
			return false;
		}

		expected = Rectangle(80.5f, 60.4f, Vector2(-45.602f, 48.5f));
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}

	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	{
		Rectangle testRectangle(10.f, 5.f, Vector2(2.f, 4.f));
		if (testRectangle.GetArea() != 50.f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the area of %s is 50.  It returned %s instead."), testRectangle, testRectangle.GetArea());
			return false;
		}

		if (testRectangle.GetPerimeter() != 30.f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the perimeter of %s is 30.  It returned %s instead."), testRectangle, testRectangle.GetPerimeter());
			return false;
		}

		Rectangle squareRect(23.f, 23.f, Vector2(8.f, 16.f));
		if (!squareRect.IsSquare())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered a square."), squareRect);
			return false;
		}

		if (testRectangle.IsSquare())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should not have been considered a square."), testRectangle);
			return false;
		}

		if (testRectangle.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s is not an empty rectangle."), testRectangle);
			return false;
		}

		if (!blankRectangle.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered an empty rectangle."), blankRectangle);
			return false;
		}

		if (testRectangle.GetGreaterHBorder() != 6.5f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed. The top border of %s should have been 6.5. Instead it's %s."), testRectangle, testRectangle.GetGreaterHBorder());
			return false;
		}

		if (testRectangle.GetGreaterVBorder() != 7.f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed. The right border of %s should have been 7. Instead it's %s."), testRectangle, testRectangle.GetGreaterVBorder());
			return false;
		}

		if (testRectangle.GetLesserHBorder() != 1.5f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed. The bottom border of %s should have been 1.5. Instead it's %s."), testRectangle, testRectangle.GetLesserHBorder());
			return false;
		}

		if (testRectangle.GetLesserVBorder() != -3.f)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed. The left border of %s should have been -3. Instead it's %s."), testRectangle, testRectangle.GetLesserVBorder());
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Is Overlaping"));
	{
		std::function<bool(const Rectangle&, const Rectangle&, bool)> testOverlap([&](const Rectangle& rectA, const Rectangle& rectB, bool expectsOverlap)
		{
			bool result = rectA.Overlaps(rectB);
			if (result != expectsOverlap)
			{
				if (expectsOverlap)
				{
					UnitTestError(testFlags, TXT("Rectangle overlap test failed. %s should be overlapping with %s. Instead the function returned false."), rectA, rectB);
				}
				else
				{
					UnitTestError(testFlags, TXT("Rectangle overlap test failed. %s should not be overlapping with %s. But the Overlaps function thinks otherwise."), rectA, rectB);
				}
			}

			return (result == expectsOverlap);
		});

		Rectangle testRectangle(10.f, 5.f, Vector2(2.f, 4.f));
		Rectangle otherRectangle(10.f, 5.f, Vector2(-2.f, 2.f));
		if (!testOverlap(testRectangle, otherRectangle, true))
		{
			return false;
		}

		testRectangle.Center = Vector2(20.f, 40.f);
		if (!testOverlap(testRectangle, otherRectangle, false))
		{
			return false;
		}

		//Test rectangle within another rectangle
		testRectangle.Center = otherRectangle.Center;
		testRectangle.Width = 2.f;
		testRectangle.Height = 2.f;
		if (!testOverlap(testRectangle, otherRectangle, true))
		{
			return false;
		}

		//Test rectangle left of another rectangle
		testRectangle = otherRectangle;
		testRectangle.Center.X -= 25.f;
		if (!testOverlap(testRectangle, otherRectangle, false))
		{
			return false;
		}

		//Test rectangle above other rectangle
		testRectangle.Center = otherRectangle.Center;
		testRectangle.Center.Y += 25.f;
		if (!testOverlap(testRectangle, otherRectangle, false))
		{
			return false;
		}

		//Test rectangle right of other rectangle
		testRectangle.Center = otherRectangle.Center;
		testRectangle.Center.X += 25.f;
		if (!testOverlap(testRectangle, otherRectangle, false))
		{
			return false;
		}

		//Test rectangle below of other rectangle
		testRectangle.Center = otherRectangle.Center;
		testRectangle.Center.Y -= 25.f;
		if (!testOverlap(testRectangle, otherRectangle, false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlapping Rectangle"));
	{
		std::function<bool(const Rectangle&, const Rectangle&, const Rectangle&)> testOverlappingRectangle([&](const Rectangle& a, const Rectangle& b, const Rectangle& expected)
		{
			Rectangle overlappingRect = a.GetOverlappingRectangle(b);
			if (overlappingRect != expected)
			{
				UnitTestError(testFlags, TXT("Overlapping Rectangle test failed. The overlapping rectangle between %s and %s should have been %s. Instead it's %s."), a, b, expected, overlappingRect);
				return false;
			}

			return true;
		});

		Rectangle a(10.f, 10.f, Vector2(0.f, 0.f));
		Rectangle b(10.f, 10.f, Vector2(5.f, 5.f));
		Rectangle expected(5.f, 5.f, Vector2(2.5f, 2.5f));
		if (!testOverlappingRectangle(a, b, expected))
		{
			return false;
		}

		a.Center = Vector2(-4.f, 5.f);
		expected = Rectangle(1.f, 10.f, Vector2(0.5f, 5.f));
		if (!testOverlappingRectangle(a, b, expected))
		{
			return false;
		}

		a.Center.X = -100.f;
		expected = Rectangle();
		if (!testOverlappingRectangle(a, b, expected))
		{
			return false;
		}

		a = Rectangle(100.f, 50.f, Vector2(10.f, 10.f));
		b = Rectangle(50.f, 100.f, Vector2(30.f, 20.f));
		expected = Rectangle(50.f, 50.f, Vector2(30.f, 10.f));
		if (!testOverlappingRectangle(a, b, expected))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		std::function<bool(const Rectangle&, const Vector2&, bool)> testEncompassPoint([&](const Rectangle& rect, const Vector2& point, bool expectedResult)
		{
			bool encompassesPoint = rect.EncompassesPoint(point);
			if (encompassesPoint != expectedResult)
			{
				if (expectedResult)
				{
					UnitTestError(testFlags, TXT("Encompasses Point test failed. Rectangle %s should be encompassing point %s, but it is not."), rect, point);
				}
				else
				{
					UnitTestError(testFlags, TXT("Encompasses Point test failed. Rectangle %s should not be encompassing point %s, but it is."), rect, point);
				}
			}

			return (encompassesPoint == expectedResult);
		});

		Rectangle testRectangle(100.f, 2.f, Vector2(10.f, -10.f));
		if (!testEncompassPoint(testRectangle, Vector2(0.f, 0.f), false))
		{
			return false;
		}

		if (!testEncompassPoint(testRectangle, Vector2(-5.f, -9.f), true))
		{
			return false;
		}

		if (!testEncompassPoint(testRectangle, Vector2(-35.f, -11.f), true))
		{
			return false;
		}

		//above
		if (!testEncompassPoint(testRectangle, Vector2(-35.f, -5.f), false))
		{
			return false;
		}

		//Right
		if (!testEncompassPoint(testRectangle, Vector2(70.f, -10.f), false))
		{
			return false;
		}

		//Below
		if (!testEncompassPoint(testRectangle, Vector2(14.f, -15.f), false))
		{
			return false;
		}

		//Left
		if (!testEncompassPoint(testRectangle, Vector2(-45.f, -10.f), false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Rectangle"));

	return true;
}

bool DatatypeUnitTester::TestSDFunction (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("SDFunction"));

	SetTestCategory(testFlags, TXT("Testing Lambda Functions"));
	{
		std::function<Int(Int)> getDoubleLambda([](Int a)
		{
			return a * 2;
		});

		SDFunction<Int, Int> getDouble(getDoubleLambda);

		Int testInt = 4;
		Int expected = 8;
		Int actual = getDouble.Execute(testInt);
		if (actual != expected)
		{
			UnitTestError(testFlags, TXT("SDFunction test failed. Binding a SDFunction to a lambda that doubles %s should have returned %s. Instead it returned %s."), testInt, expected, actual);
			return false;
		}

		Int originalInt(testInt);
		SDFunction<void> doubleMyInt([&]()
		{
			testInt *= 2;
		});

		doubleMyInt();
		if (testInt != expected)
		{
			UnitTestError(testFlags, TXT("SDFunction test failed. Binding a SDFunction to a lambda that doubles %s should have returned %s. Instead it returned %s."), originalInt, expected, testInt);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SDFunctionTester* functionTester = SDFunctionTester::CreateObject();
	if (functionTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Function unit test failed.  Failed to instantiate SDFunctionTester object."));
		return false;
	}

	//This is handled externally since the function test needs to test instanced objects.
	bool passedTests = functionTester->RunTest(this, testFlags);
	functionTester->Destroy();

	if (passedTests)
	{
		ExecuteSuccessSequence(testFlags, TXT("SDFunctionTester"));
	}

	return passedTests;
}

bool DatatypeUnitTester::TestResourceTree (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Resource Tree"));

	ResourceTree<Int> testTree;

	//This is only used for testing FindItem. Don't delete elements from this vector. Empty the testTree instead to delete them.
	//The elements in this vector must be equal to the index. [0] = 0, [1] = 1, etc...   This is because the FindItem test uses this vector to find the original memory address.
	std::vector<Int*> addedItems;

	SetTestCategory(testFlags, TXT("Adding Items"));
	{
		std::vector<DString> path;
		Int* newItem = nullptr;

		//testAddItem should delete newItem and empty the test tree if the test failed to prevent memory leaks.
		std::function<bool()> testAddItem([&]()
		{
			if (!testTree.AddItem(path, newItem))
			{
				UnitTestError(testFlags, TXT("ResourceTree test failed. Failed to insert %s with the path \"%s\" into a resource tree."), newItem->ToString(), DString::ListToString(path));
				delete newItem;
				newItem = nullptr;
				testTree.EmptyTree();
				ContainerUtils::Empty(OUT addedItems);
				return false;
			}

			addedItems.push_back(newItem);
			return true;
		});

		if (testTree.ContainsAnItem())
		{
			UnitTestError(testFlags, TXT("Resource tree test failed. All resource trees should be empty when created. The test tree already has an item before anything was added to it."));
			testTree.EmptyTree();
			return false;
		}

		/**
		 Construct the tree that looks like this. Diagram is in Name(Item) format.
		 Engine(0)
			Core(nullptr)
				Headers(nullptr)
					Core(1)
				Source(2)
					Utils(3)
			OtherModule(4)
				Headers(nullptr)
					DatatypeUnitTester(5)
				Source(nullptr)
					Core(6)
					DatatypeUnitTester(7)
					ResourcePool(8)
				CMakeLists(9)
		Tools(nullptr)
			SandDuneTester(nullptr)
				Source(10)
		*/

		path = {TXT("Engine")};
		newItem = new Int(0);
		if (!testAddItem()) //testAddItem should delete newItem and test tree if the test failed.
		{
			return false;
		}

		path = {TXT("Engine"), TXT("Core"), TXT("Headers"), TXT("Core")};
		newItem = new Int(1);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("Core"), TXT("Source")};
		newItem = new Int(2);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("Core"), TXT("Source"), TXT("Utils")};
		newItem = new Int(3);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule")};
		newItem = new Int(4);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Headers"), TXT("DatatypeUnitTester")};
		newItem = new Int(5);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Source"), TXT("Core")};
		newItem = new Int(6);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Source"), TXT("DatatypeUnitTester")};
		newItem = new Int(7);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Source"), TXT("ResourcePool")};
		newItem = new Int(8);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("CMakeLists")};
		newItem = new Int(9);
		if (!testAddItem())
		{
			return false;
		}

		path = {TXT("Tools"), TXT("SandDuneTester"), TXT("Source")};
		newItem = new Int(10);
		if (!testAddItem())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Find Item"));
	{
		std::vector<DString> path;
		Int* expected;
		std::function<bool()> testFindItem([&]()
		{
			Int* actual = testTree.FindItem(path, false);
			if (actual != expected)
			{
				DString actualStr = (actual != nullptr) ? actual->ToString() : TXT("NULL");
				DString expectedStr = (expected != nullptr) ? expected->ToString() : TXT("NULL");
				UnitTestError(testFlags, TXT("ResourceTree test failed. Failed to find %s from the path \"%s\". It returned %s instead."), expectedStr, DString::ListToString(path), actualStr);
				testTree.EmptyTree();
				ContainerUtils::Empty(OUT addedItems);
				return false;
			}

			return true;
		});

		path = {TXT("Engine")};
		expected = addedItems.at(0);
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Core"));
		expected = nullptr;
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Headers"));
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Core"));
		expected = addedItems.at(1);
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("Core"), TXT("Source")};
		expected = addedItems.at(2);
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Utils"));
		expected = addedItems.at(3);
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule")};
		expected = addedItems.at(4);
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Headers"));
		expected = nullptr;
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("DatatypeUnitTester"));
		expected = addedItems.at(5);
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Source")};
		expected = nullptr;
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Core"));
		expected = addedItems.at(6);
		if (!testFindItem())
		{
			return false;
		}

		path.pop_back();
		path.push_back(TXT("DatatypeUnitTester"));
		expected = addedItems.at(7);
		if (!testFindItem())
		{
			return false;
		}

		path.pop_back();
		path.push_back(TXT("ResourcePool"));
		expected = addedItems.at(8);
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("CMakeLists")};
		expected = addedItems.at(9);
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("Tools")};
		expected = nullptr;
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("SandDuneTester"));
		if (!testFindItem())
		{
			return false;
		}

		path.push_back(TXT("Source"));
		expected = addedItems.at(10);
		if (!testFindItem())
		{
			return false;
		}
		
		path.push_back(TXT("SomeExtraFile"));
		expected = nullptr;
		if (!testFindItem())
		{
			return false;
		}

		path = {TXT("SomeNonExistingBranch")};
		if (!testFindItem())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Find Items"));
	{
		std::vector<DString> path;
		std::vector<Int*> expected;
		std::function<bool()> testFindItems([&]()
		{
			std::vector<Int*> actual;
			testTree.FindItems(path, OUT actual);

			if (expected.size() != actual.size())
			{
				UnitTestError(testFlags, TXT("Resource tree test failed. Calling Find Items with the path \"%s\" should have returned a vector of size %s. Instead the returned a vector of size %s."), DString::ListToString(path), Int(expected.size()), Int(actual.size()));
				testTree.EmptyTree();
				ContainerUtils::Empty(OUT addedItems);
				return false;
			}

			for (size_t actualIdx = 0; actualIdx < actual.size(); ++actualIdx)
			{
				size_t expectedIdx = ContainerUtils::FindInVector(expected, actual.at(actualIdx));
				if (expectedIdx == UINT_INDEX_NONE)
				{
					UnitTestError(testFlags, TXT("Resource tree test failed. Calling Find Items with the path \"%s\" has found %s, which is not in the expected list."), DString::ListToString(path), actual.at(actualIdx)->ToString());
					testTree.EmptyTree();
					ContainerUtils::Empty(OUT addedItems);
					return false;
				}
			}

			for (size_t expectedIdx = 0; expectedIdx < expected.size(); ++expectedIdx)
			{
				size_t actualIdx = ContainerUtils::FindInVector(actual, expected.at(expectedIdx));
				if (actualIdx == UINT_INDEX_NONE)
				{
					UnitTestError(testFlags, TXT("Resource tree test failed. Calling Find Items with the path \"%s\" was unable to find expected item %s."), DString::ListToString(path), expected.at(expectedIdx)->ToString());
					testTree.EmptyTree();
					ContainerUtils::Empty(OUT addedItems);
					return false;
				}
			}

			return true;
		});

		path = {TXT("Engine"), TXT("Core")};
		expected.push_back(addedItems.at(1));
		expected.push_back(addedItems.at(2));
		expected.push_back(addedItems.at(3));
		if (!testFindItems())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule")};
		ContainerUtils::Empty(OUT expected);
		for (size_t i = 4; i <= 9; ++i)
		{
			expected.push_back(addedItems.at(i));
		}
		if (!testFindItems())
		{
			return false;
		}

		path.push_back(TXT("Source"));
		ContainerUtils::Empty(OUT expected);
		expected.push_back(addedItems.at(6));
		expected.push_back(addedItems.at(7));
		expected.push_back(addedItems.at(8));
		if (!testFindItems())
		{
			return false;
		}

		path.push_back(TXT("ResourcePool"));
		ContainerUtils::Empty(OUT expected);
		expected.push_back(addedItems.at(8));
		if (!testFindItems())
		{
			return false;
		}

		path.push_back(TXT("SomeFalsePath"));
		ContainerUtils::Empty(OUT expected);
		if (!testFindItems())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	//This test also tests RemoveItem. Delete Item is essentially RemoveItem + deletion.
	SetTestCategory(testFlags, TXT("Delete Item"));
	{
		std::vector<DString> path;
		std::function<bool()> testDeleteItem([&]()
		{
			if (!testTree.DeleteItem(path))
			{
				UnitTestError(testFlags, TXT("Resource tree test failed. Failed to find and delete the item from path \"%s\"."), DString::ListToString(path));
				testTree.EmptyTree();
				return false;
			}

			//Ensure the item doesn't exist
			Int* testItem = testTree.FindItem(path, false);
			if (testItem != nullptr)
			{
				UnitTestError(testFlags, TXT("Resource tree test failed. Failed to remove item from path \"%s\" since it was still able to find %s from that path."), DString::ListToString(path), testItem->ToString());
				testTree.EmptyTree();
				return false;
			}

			return true;
		});

		//The unit test no longer needs the addedItems list. Clear it since the testTree is about to delete those Ints.
		ContainerUtils::Empty(OUT addedItems);

		path = {TXT("Tools"), TXT("SandDuneTester"), TXT("Source")};
		if (!testDeleteItem())
		{
			return false;
		}

		//Test removing in the middle
		path = {TXT("Engine"), TXT("Core"), TXT("Source")};
		if (!testDeleteItem())
		{
			return false;
		}

		path.push_back(TXT("Utils"));
		if (!testDeleteItem())
		{
			return false;
		}

		//Test removing leaf
		path = {TXT("Engine"), TXT("Core"), TXT("Headers"), TXT("Core")};
		if (!testDeleteItem())
		{
			return false;
		}

		//Test removing branch connected to root
		path = {TXT("Engine")};
		if (!testDeleteItem())
		{
			return false;
		}

		//Remove everything else
		path = {TXT("Engine"), TXT("OtherModule")};
		if (!testDeleteItem())
		{
			return false;
		}

		path.push_back(TXT("Headers"));
		path.push_back(TXT("DatatypeUnitTester"));
		if (!testDeleteItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("Source"), TXT("Core")};
		if (!testDeleteItem())
		{
			return false;
		}

		path.pop_back();
		path.push_back(TXT("DatatypeUnitTester"));
		if (!testDeleteItem())
		{
			return false;
		}

		path.pop_back();
		path.push_back(TXT("ResourcePool"));
		if (!testDeleteItem())
		{
			return false;
		}

		path = {TXT("Engine"), TXT("OtherModule"), TXT("CMakeLists")};
		if (!testDeleteItem())
		{
			return false;
		}

		//At this point the tree should be empty.
		if (testTree.ContainsAnItem())
		{
			UnitTestError(testFlags, TXT("Test tree test failed. Failed to delete every item from the tree. After running the deletion functions, the test tree is still contains an item."));
			testTree.EmptyTree();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Resource Tree"));
	return true;
}

bool DatatypeUnitTester::TestMatrix (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Matrix"));

	Stopwatch matrixTimer(TXT("MatrixTimer"));
	std::function<void()> logTimer = [&matrixTimer, testFlags]()
	{
		TestLog(testFlags, TXT("The matrix unit test was running for %s milliseconds."), matrixTimer.GetElapsedTime());
	};

	Matrix voidMatrix = Matrix::InvalidMatrix;
	Matrix matrix1x5(1, 5);
	Matrix matrix5x1(5, 1);
	Matrix matrix2x2(2, 2);
	Matrix matrix6x6(6, 6);
	Matrix invalidMatrix(0, 4);

	std::vector<Float> matrixData;
	matrixData.push_back(0.f);
	matrixData.push_back(1.f);
	matrixData.push_back(2.f);
	matrixData.push_back(3.f);
	matrix2x2.SetData(matrixData);

	matrixData.push_back(4.f);
	matrix1x5.SetData(matrixData);
	matrix5x1.SetData(matrixData);

	matrixData.push_back(5.f);
	matrixData.push_back(6.f);

	for (Float curValue = 7.f; curValue < 36.f; curValue++)
	{
		matrixData.push_back(curValue);
	}
	matrix6x6.SetData(matrixData);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Construct tests.  The following matrices were created:"));
		TestLog(testFlags, TXT("    voidMatrix:  %s"), voidMatrix);
		voidMatrix.LogMatrix();
		TestLog(testFlags, TXT("    1x5 matrix:  %s"), matrix1x5);
		matrix1x5.LogMatrix();
		TestLog(testFlags, TXT("    5x1 matrix:  %s"), matrix5x1);
		matrix5x1.LogMatrix();
		TestLog(testFlags, TXT("    2x2 matrix:  %s"), matrix2x2);
		matrix2x2.LogMatrix();
		TestLog(testFlags, TXT("    6x6 matrix:  %s"), matrix6x6);
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("    0x4 matrix:  %s"), invalidMatrix);
		invalidMatrix.LogMatrix();
	}

	SetTestCategory(testFlags, TXT("Matrix Validation"));
	TestLog(testFlags, TXT("Testing matrix validation.  Of the matrices listed above, only the 0x4 and matrix with too much data should be invalid."));
	if (!matrix1x5.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 1x5 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), matrix1x5);
		logTimer();
		return false;
	}

	if (!matrix5x1.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 5x1 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), matrix5x1);
		logTimer();
		return false;
	}

	if (!matrix2x2.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 2x2 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), matrix2x2);
		logTimer();
		return false;
	}

	if (!matrix6x6.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 6x6 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), matrix6x6);
		logTimer();
		return false;
	}

	if (invalidMatrix.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 0x4 matrix should be an invalid matrix; instead it returned true for this matrix:  %s"), invalidMatrix);
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Comparison"));
	TestLog(testFlags, TXT("Is the 1x5 matrix and 5x1 matrix equal?  %s"), Bool(matrix1x5 == matrix5x1));
	if (matrix1x5 == matrix5x1)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  Matrix %s should have not been equal to %s."), matrix1x5, matrix5x1);
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Is the 6x6 matrix equal to itself?  %s"), Bool(matrix6x6 == matrix6x6));
	if (matrix6x6 != matrix6x6)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 6x6 matrix should have been equal to itself."));
		logTimer();
		return false;
	}

	Matrix other2x2(matrix2x2);
	TestLog(testFlags, TXT("Created a copy of {%s}.  Is that copy equal to the 2x2 matrix?  %s"), matrix2x2, Bool(matrix2x2 == other2x2));
	if (matrix2x2 != other2x2)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix %s should have been equal to its copy."), matrix2x2);
		logTimer();
		return false;
	}

	other2x2.At(1) = 100;
	TestLog(testFlags, TXT("Changed a value to the 2x2 copy.  Is {%s} equal to {%s} ?  %s"), matrix2x2, other2x2, Bool(matrix2x2 == other2x2));
	if (matrix2x2 == other2x2)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix %s should have not been equal to %s."), matrix2x2, other2x2);
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Accessors"));
	Matrix accessor(3, 4);
	matrixData.clear();
	for (unsigned int i = 0; i < 12; i++)
	{
		matrixData.push_back(Float::MakeFloat(i));
	}
	/*
	[0	1	2	3 ]
	[4	5	6	7 ]
	[8	9	10	11]
	*/
	accessor.SetData(matrixData);
	Float dataElement = accessor.GetDataElement(1, 2);
	TestLog(testFlags, TXT("Retrieving data element from 2nd row and 3rd column from matrix {%s} returned %s"), accessor, dataElement);
	if (dataElement != 6.f)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the data element at the 2nd row, 3rd column from matrix %s should have returned 6.  Instead it found %s"), accessor, dataElement);
		logTimer();
		return false;
	}

	ContainerUtils::Empty(matrixData);
	matrixData = accessor.GetColumn(3);
	std::vector<Float> accessorAnsKey;
	accessorAnsKey.push_back(3.f);
	accessorAnsKey.push_back(7.f);
	accessorAnsKey.push_back(11.f);
	TestLog(testFlags, TXT("Retrieving the last column from matrix {%s} returned %s"), accessor, DString::ListToString(matrixData));
	if (matrixData != accessorAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the last column from matrix %s should have resulted in %s.  Instead it found %s"), accessor, DString::ListToString(accessorAnsKey), DString::ListToString(matrixData));
		logTimer();
		return false;
	}

	matrixData.clear();
	matrixData = accessor.GetRow(1);
	accessorAnsKey.clear();
	accessorAnsKey.push_back(4.f);
	accessorAnsKey.push_back(5.f);
	accessorAnsKey.push_back(6.f);
	accessorAnsKey.push_back(7.f);
	TestLog(testFlags, TXT("Retrieving the second row from matrix {%s} returned %s"), accessor, DString::ListToString(matrixData));
	if (matrixData != accessorAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the second row from matrix %s should have resulted in %s.  Instead it found %s"), accessor, DString::ListToString(accessorAnsKey), DString::ListToString(matrixData));
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Operations"));
	Matrix a(2, 4);
	Matrix b(2, 4);
	matrixData.clear();
	matrixData.push_back(5.f);
	matrixData.push_back(12.f);
	matrixData.push_back(0.52f);
	matrixData.push_back(-15.f);
	matrixData.push_back(-4.5f);
	matrixData.push_back(9.2f);
	matrixData.push_back(18.1f);
	matrixData.push_back(-92.68f);
	a.SetData(matrixData);

	std::vector<Float> results = matrixData;

	matrixData.clear();
	matrixData.push_back(3.f);
	matrixData.push_back(16.f);
	matrixData.push_back(0.61f);
	matrixData.push_back(-12.f);
	matrixData.push_back(-8.2f);
	matrixData.push_back(5.5f);
	matrixData.push_back(29.9f);
	matrixData.push_back(92.68f);
	b.SetData(matrixData);

	for (UINT_TYPE i = 0; i < matrixData.size(); i++)
	{
		results.at(i) += matrixData.at(i);
	}

	Matrix resultMatrix(2, 4);
	Matrix answerKey(2, 4);
	answerKey.SetData(results);
	resultMatrix = a + b;
	TestLog(testFlags, TXT("The sum of %s and %s equals %s"), a, b, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  The sum of %s and %s should have resulted in %s.  Instead it's %s"), a, b, answerKey, resultMatrix);
		logTimer();
		return false;
	}

	for (UINT_TYPE i = 0; i < a.GetNumElements(); i++)
	{
		answerKey.At(i) = a.At(i) - b.At(i);
	}
	resultMatrix = a - b;
	TestLog(testFlags, TXT("The difference of %s and %s equals %s"), a, b, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  The difference of %s and %s should have resulted in %s.  Instead it's %s"), a, b, answerKey, resultMatrix);
		logTimer();
		return false;
	}

	Float scaler = 4.5f;
	for (UINT_TYPE i = 0; i < answerKey.GetNumElements(); i++)
	{
		answerKey.At(i) *= 4.5f;
	}
	Matrix oldResult(resultMatrix);
	resultMatrix *= scaler;
	TestLog(testFlags, TXT("Scaling matrix %s by %s equals %s"), oldResult, scaler, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Scaling matrix %s by %s should have resulted in %s.  Instead it's %s"), oldResult, scaler, answerKey, resultMatrix);
		logTimer();
		return false;
	}

	//Make sure the ForEachElement method works before using it
	{
		Matrix forEachTest(5, 2,
		{
			1, 1,
			2, 3,
			5, 8,
			13, 21,
			34, 55
		});

		Float expectedSum = 143.f;
		Float counter = 0;
		forEachTest.ForEachElement([&](UINT_TYPE i)
		{
			counter += forEachTest.At(i);
		});

		if (counter != expectedSum)
		{
			UnitTestError(testFlags, TXT("the Matrix ForEachElement test failed.  Taking the sum of all elements from the matrix listed below should have resulted in %s.  Instead it returned %s."), expectedSum, counter);
			forEachTest.LogMatrix();
			logTimer();
			return false;
		}
	}

	Matrix aMultiply(2, 2);
	Matrix bMultiply(2, 2);
	Matrix multiplyAnsKey(2, 2);
	Matrix multiplyResult(2, 2);
	matrixData.clear();
	matrixData.push_back(4.f);
	matrixData.push_back(9.f);
	matrixData.push_back(2.5f);
	matrixData.push_back(-2.f);
	aMultiply.SetData(matrixData);

	matrixData.clear();
	matrixData.push_back(6.f);
	matrixData.push_back(7.f);
	matrixData.push_back(4.25f);
	matrixData.push_back(-4.f);
	bMultiply.SetData(matrixData);

	matrixData.clear();
	matrixData.push_back(62.25f);
	matrixData.push_back(-8.f);
	matrixData.push_back(6.5f);
	matrixData.push_back(25.5f);
	multiplyAnsKey.SetData(matrixData);

	multiplyResult = aMultiply * bMultiply;
	//Round off accumulated float precision errors
	aMultiply.ForEachElement([&](UINT_TYPE i){aMultiply.At(i).RoundInline(3);});
	bMultiply.ForEachElement([&](UINT_TYPE i){bMultiply.At(i).RoundInline(3);});
	multiplyAnsKey.ForEachElement([&](UINT_TYPE i){multiplyAnsKey.At(i).RoundInline(3);});
	multiplyResult.ForEachElement([&](UINT_TYPE i){multiplyResult.At(i).RoundInline(3);});
	TestLog(testFlags, TXT("Multiplying %s with %s is equal to %s"), aMultiply, bMultiply, multiplyResult);
	if (multiplyResult != multiplyAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), aMultiply, bMultiply, multiplyAnsKey, multiplyResult);
		logTimer();
		return false;
	}

	Matrix aRectMultiply(3, 2,
	{
		2.f, 4.f,
		6.5f, 8.f,
		-10.f, -18.23f
	});

	Matrix bRectMultiply(2, 3,
	{
		18.2f, -3.f, -12.7f,
		2.f, 100.f, 12.5f
	});

	Matrix rectMultAnsKey(3, 3,
	{
		44.4f, 394.f, 24.6f,
		134.3f, 780.5f, 17.45f,
		-218.46f, -1793.f, -100.875f
	});

	Matrix rectMultResult(3, 3);
	rectMultResult = aRectMultiply * bRectMultiply;

	//Round off accumulated float precision errors
	aRectMultiply.ForEachElement([&](UINT_TYPE i){aRectMultiply.At(i).RoundInline(3);});
	bRectMultiply.ForEachElement([&](UINT_TYPE i){bRectMultiply.At(i).RoundInline(3);});
	rectMultAnsKey.ForEachElement([&](UINT_TYPE i){rectMultAnsKey.At(i).RoundInline(3);});
	rectMultResult.ForEachElement([&](UINT_TYPE i){rectMultResult.At(i).RoundInline(3);});
	TestLog(testFlags, TXT("Multiplying %s with %s is equal to %s"), aRectMultiply, bRectMultiply, rectMultResult);

	//comparing operators
	for (UINT_TYPE i = 0; i < rectMultResult.GetNumElements(); i++)
	{
		if (rectMultResult.At(i) != rectMultAnsKey.At(i))
		{
			UnitTestError(testFlags, TXT("Elements:  %s and %s do not match."), rectMultResult.At(i), rectMultAnsKey.At(i));
			logTimer();
			return false;
		}
	}

	if (rectMultResult != rectMultAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), aRectMultiply, bRectMultiply, rectMultAnsKey, rectMultResult);
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Note: The Matrix test will later test division after it verified the inverse functions are working correctly."));
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Streaming"));
	{
		//Create a 6x4 matrix
		Matrix streamMatrix(6, 4,
		{
			26.4f,		-2.5f,		82.18f,		981.7f,
			10.9f,		-946.7f,	-715.3f,	55.05f,
			915.6f,		7418.6f,	-210.64f,	7182.f,
			3648.13f,	836.4f,		-8745.48f,	1874.34f,
			184.1f,		-48.15f,	6.5f,		9461.f,
			5432.01f,	-4842.48f,	-8496.1f,	184.123f
		});

		//Create a 1x3 matrix
		Matrix smallStreamMatrix(1, 3, {496.15f, 84715.16f, -4845.57f});
		DataBuffer matrixStream;

		matrixStream << streamMatrix;
		matrixStream << smallStreamMatrix;

		Matrix readStreamMatrix;
		Matrix readSmallStreamMatrix;
		matrixStream >> readStreamMatrix;
		matrixStream >> readSmallStreamMatrix;

		if (readStreamMatrix != streamMatrix)
		{
			UnitTestError(testFlags, TXT("Failed to stream matrices.  The following matrix was written to buffer:  %s.  The following matrix was read from buffer:  %s."), streamMatrix, readStreamMatrix);
			logTimer();
			return false;
		}

		if (readSmallStreamMatrix != smallStreamMatrix)
		{
			UnitTestError(testFlags, TXT("Failed to stream matrices.  The following matrix was written to buffer:  %s.  The following matrix was read from buffer:  %s."), smallStreamMatrix, readSmallStreamMatrix);
			logTimer();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Matrix expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Matrix actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Matrix test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Matrix(1, 1, {4.f});
		testStr = TXT("(Rows = 1, Columns = 1, Data = (4))");
		if (!testParsing())
		{
			return false;
		}

		expected = Matrix(2, 2, {1.f, 2.f, 3.f, 4.f});
		testStr = TXT("(Rows = 2, Columns = 2, Data = (1, 2, 3, 4))");
		if (!testParsing())
		{
			return false;
		}

		expected = Matrix(3, 3, {2.5f, 4.5f, 6.5f, 10.25f, 15.25f, 20.25f, 105.33f, 205.33f, 305.33f});
		testStr = TXT("(Rows = 3, Columns = 3, Data = (2.5, 4.5, 6.5, 10.25, 15.25, 20.25, 105.33, 205.33, 305.33))");
		if (!testParsing())
		{
			return false;
		}

		expected = Matrix(3, 2, {-1.25f, -7.52f, -32.95, 4.6f, -17.f, 20.f});
		testStr = TXT("(Rows = 3, Columns = 2, Data = (-1.25, -7.52, -32.95, 4.6, -17, 20))");
		if (!testParsing())
		{
			return false;
		}

		expected = Matrix(2, 7,
		{
			39.f, 91.f, -15.6f, -912.1f, 73.1f, 457.91f, 64.5f,
			28.f, 63.5f, 63.1f, 45.2f, 33.78f, 73.6f, -789.253f
		});
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Utilities"));
	TestLog(testFlags, TXT("Is a 2x2 matrix a square?  %s"), Bool(matrix2x2.IsSquare()));
	if (!matrix2x2.IsSquare())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  A 2x2 matrix should be considered a square matrix but IsSquare returned false."));
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Is 1x5 matrix a square?  %s"), Bool(matrix1x5.IsSquare()));
	if (matrix1x5.IsSquare())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  A 1x5 matrix should not have been considered a square matrix, but IsSquare returned true."));
		logTimer();
		return false;
	}

	//Test upper/lower triangular, unitriangular, and unit matrices.
	/*
	[5	5	5]
	[0	5	5]
	[0	0	5]
	*/
	Matrix upperTriangle(3, 3,
	{
		5, 5, 5,
		0, 5, 5,
		0, 0, 5
	});

	/*
	[5	0	0]
	[5	5	0]
	[5	5	5]
	*/
	Matrix lowerTriangle(3, 3,
	{
		5, 0, 0,
		5, 5, 0,
		5, 5, 5
	});

	/*
	[1	0	0]
	[5	1	0]
	[5	5	1]
	*/
	Matrix unitriangularMatrix(3, 3,
	{
		1, 0, 0,
		5, 1, 0,
		5, 5, 1
	});

	/*
	[1	0	0]
	[0	1	0]
	[0	0	1]
	*/
	Matrix unitMatrix(3, 3,
	{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	});

	TestLog(testFlags, TXT("Constructed the following matrices:  upperTriangle, lowerTriangle, unitriangularMatrix, unitMatrix respectively. . ."));
	if (ShouldHaveDebugLogs(testFlags))
	{
		lowerTriangle.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		upperTriangle.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		unitriangularMatrix.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		unitMatrix.LogMatrix();
	}

	//Test lower triangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually a lower triangular matrix?  %s.  upperTriangle actually a lower triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), Bool(lowerTriangle.IsLowerTriangularMatrix()), Bool(upperTriangle.IsLowerTriangularMatrix()), Bool(unitriangularMatrix.IsLowerTriangularMatrix()), Bool(unitMatrix.IsLowerTriangularMatrix()));
	if (!lowerTriangle.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered a lower triangular matrix.  Instead it returned false."), lowerTriangle);
		logTimer();
		return false;
	}

	if (upperTriangle.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered a lower triangular matrix.  Instead it returned true."), upperTriangle);
		logTimer();
		return false;
	}

	//Skipping lower triangle check for unitriangular matrix since it can be either lower or upper triangular

	if (!unitMatrix.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered a lower triangular matrix.  Instead it returned false."), unitMatrix);
		logTimer();
		return false;
	}

	//Test upper triangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an upper triangular matrix?  %s.  upperTriangle actually an upper triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), Bool(lowerTriangle.IsUpperTriangularMatrix()), Bool(upperTriangle.IsUpperTriangularMatrix()), Bool(unitriangularMatrix.IsUpperTriangularMatrix()), Bool(unitMatrix.IsUpperTriangularMatrix()));
	if (lowerTriangle.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an upper triangular matrix.  Instead it returned true."), lowerTriangle);
		logTimer();
		return false;
	}

	if (!upperTriangle.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered an upper triangular matrix.  Instead it returned false."), upperTriangle);
		logTimer();
		return false;
	}

	//Skipping upper triangle check for unitriangular matrix since it can be either lower or upper triangular

	if (!unitMatrix.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered an upper triangular matrix.  Instead it returned false."), unitMatrix);
		logTimer();
		return false;
	}

	//Test unitriangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an unitriangular matrix?  %s.  upperTriangle actually an unitriangular matrix?  %s.  UnitriangularMatrix actually an unitriangular matrix?  %s.  Unit matrix actually an unitriangular matrix?  %s."), Bool(lowerTriangle.IsUnitriangularMatrix()), Bool(upperTriangle.IsUnitriangularMatrix()), Bool(unitriangularMatrix.IsUnitriangularMatrix()), Bool(unitMatrix.IsUnitriangularMatrix()));
	if (lowerTriangle.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an unitriangular matrix.  Instead it returned true."), lowerTriangle);
		logTimer();
		return false;
	}

	if (upperTriangle.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an unitriangular matrix.  Instead it returned true."), upperTriangle);
		logTimer();
		return false;
	}

	if (!unitriangularMatrix.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered an unitriangular matrix.  Instead it returned false."), unitriangularMatrix);
		logTimer();
		return false;
	}

	if (!unitMatrix.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered an unitriangular matrix.  Instead it returned false."), unitMatrix);
		logTimer();
		return false;
	}

	//Test unit matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an unit matrix?  %s.  upperTriangle actually an unit matrix?  %s.  UnitriangularMatrix actually an unit matrix?  %s.  Unit matrix actually an unit matrix?  %s."), Bool(lowerTriangle.IsUnitMatrix()), Bool(upperTriangle.IsUnitMatrix()), Bool(unitriangularMatrix.IsUnitMatrix()), Bool(unitMatrix.IsUnitMatrix()));
	if (lowerTriangle.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an unit matrix.  Instead it returned true."), lowerTriangle);
		logTimer();
		return false;
	}

	if (upperTriangle.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an unit matrix.  Instead it returned true."), upperTriangle);
		logTimer();
		return false;
	}

	if (unitriangularMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should not have been considered an unit matrix.  Instead it returned true."), unitriangularMatrix);
		logTimer();
		return false;
	}

	if (!unitMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix %s should have been considered an unit matrix.  Instead it returned false."), unitMatrix);
		logTimer();
		return false;
	}

	Matrix zeroMatrix = matrix6x6.GetZeroMatrix();
	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The zero matrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Zero Matrix listed below ---"));
		zeroMatrix.LogMatrix();
	}

	for (UINT_TYPE i = 0; i < matrix6x6.GetNumElements(); i++)
	{
		if (zeroMatrix.At(i) != 0.f)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  All elements of a zero matrix should be zero.  Instead element %s is not zero in matrix:  %s"), Int(i), zeroMatrix);
			logTimer();
			return false;
		}
	}

	Matrix identityMatrix(6, 6);
	Matrix ansKey6x6(6, 6);

	/*
	[1	0	0	0	0	0]
	[0	1	0	0	0	0]
	[0	0	1	0	0	0]
	[0	0	0	1	0	0]
	[0	0	0	0	1	0]
	[0	0	0	0	0	1]
	*/
	ansKey6x6.SetData(
	{
		1, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 1
	});

	matrix6x6.GetIdentityMatrix(identityMatrix);
	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The identity matrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Identity Matrix listed below ---"));
		identityMatrix.LogMatrix();
	}

	if (identityMatrix != ansKey6x6)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The identity matrix of the 6x6 matrix should have been %s.  Instead it returned %s"), ansKey6x6, identityMatrix);
		logTimer();
		return false;
	}

	//Transpose matrix test
	{
		Matrix testMatrix(2, 2,
		{
			1, 2,
			3, 4
		});

		Matrix expectedTransposeMatrix(2, 2,
		{
			1, 3,
			2, 4
		});

		Matrix transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected %s."), expectedTransposeMatrix);
			logTimer();
			return false;
		}

		testMatrix = Matrix(2, 4,
		{
			0, 1, 2, 3,
			4, 5, 6, 7
		});

		expectedTransposeMatrix = Matrix(4, 2,
		{
			0, 4,
			1, 5,
			2, 6,
			3, 7
		});

		transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected %s."), expectedTransposeMatrix);
			logTimer();
			return false;
		}

		testMatrix = Matrix(4, 4,
		{
			0, 1, 2, 3,
			4, 5, 6, 7,
			8, 9, 10, 11,
			12, 13, 14, 15
		});

		expectedTransposeMatrix = Matrix(4, 4,
		{
			0, 4, 8, 12,
			1, 5, 9, 13,
			2, 6, 10, 14,
			3, 7, 11, 15
		});

		transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected %s."), expectedTransposeMatrix);
			logTimer();
			return false;
		}
	}

	//Orthogonal matrix text
	{
		const Float orthoTolerance = 0.00001f;
		Matrix orthoTest(2, 2,
		{
			1, 0,
			0, 1
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix %s should have been considered an orthogonal matrix, but it is not."), orthoTest);
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			1, 0,
			0, -1
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix %s should have been considered an orthogonal matrix, but it is not."), orthoTest);
			logTimer();
			return false;
		}

		const Float oneOverSqrtTwo = 0.707106781f;
		orthoTest.SetData(
		{
			oneOverSqrtTwo, oneOverSqrtTwo,
			oneOverSqrtTwo, -oneOverSqrtTwo
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix %s should have been considered an orthogonal matrix, but it is not."), orthoTest);
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			0.f, 0.5f,
			2.f, 0.f
		});

		if (orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix %s should have NOT been considered an orthogonal matrix, but it is instead."), orthoTest);
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			2.f, 0.f,
			0.f, 0.5f
		});

		if (orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix %s should have NOT been considered an orthogonal matrix, but it is instead."), orthoTest);
			logTimer();
			return false;
		}
	}

	Matrix coMatrix = matrix6x6.GetComatrix();
	ansKey6x6.SetData(
	{
		0, -1, 2, -3, 4, -5,
		-6, 7, -8, 9, -10, 11,
		12, -13, 14, -15, 16, -17,
		-18, 19, -20, 21, -22, 23,
		24, -25, 26, -27, 28, -29,
		-30, 31, -32, 33, -34, 35
	});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The comatrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Comatrix listed below ---"));
		coMatrix.LogMatrix();
	}

	if (coMatrix != ansKey6x6)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The comatrix of the 6x6 matrix should have been %s.  Instead it returned %s"), ansKey6x6, coMatrix);
		logTimer();
		return false;
	}

	//SubMatrix test section
	{
		/*
		[0	1	2	3	5 ]
		[6	7	8	9	11]
		[12	13	14	15	17]
		[24	25	26	27	29]
		[30	31	32	33	35]
		*/
		Matrix subMatrix = matrix6x6.GetSubMatrix(3, 4);
		Matrix subMatrixAns(5, 5);

		subMatrixAns.SetData(
		{
			0, 1, 2, 3, 5,
			6, 7, 8, 9, 11,
			12, 13, 14, 15, 17,
			24, 25, 26, 27, 29,
			30, 31, 32, 33, 35
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that excludes row idx 3 column idx 4 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix that excludes row index 3 and column index 4 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		/*
		0  1  2  3  4  5
		6  7  8  9  10 11
		12 13 14 15 16 17
		18 19 20 21 22 23
		24 25 26 27 28 29
		30 31 32 33 34 35
		*/

		/*
		[9	10	11]
		[15	16	17]
		[21	22	23]
		*/
		subMatrix = matrix6x6.GetSubMatrix(1, 3, 3, 5);
		subMatrixAns = Matrix(3, 3,
		{
			9, 10, 11,
			15, 16, 17,
			21, 22, 23
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that only includes row indices 1-3 and column indices 3-5 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix with row indices 1-3 and column indices 3-5 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		/*
		[1	2	3	4	5 ]
		[7	8	9	10	11]
		[13	14	15	16	17]
		*/
		subMatrix = matrix6x6.GetSubMatrix(0, 2, 1, 5);
		subMatrixAns = Matrix(3, 5,
		{
			1, 2, 3, 4, 5,
			7, 8, 9, 10, 11,
			13, 14, 15, 16, 17
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that only includes row indices 0-2 and column indices 1-5 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix with row indices 0-2 and column indices 1-5 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}
	}

	Float deter2x2 = -2.f;
	TestLog(testFlags, TXT("The determinant of {%s} is %s"), matrix2x2, matrix2x2.CalculateDeterminant());
	if (matrix2x2.CalculateDeterminant() != deter2x2)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix %s should have been %s.  Instead it returned %s"), matrix2x2, deter2x2, matrix2x2.CalculateDeterminant());
		logTimer();
		return false;
	}

	//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/finding-the-determinant-of-a-3x3-matrix-method-1
	Matrix deterMatrix3x3(3, 3,
	{
		4, -1, 1,
		4, 5, 3,
		-2, 0, 0
	});
	Float deter3x3Ans = 16.f;
	Float deter3x3 = deterMatrix3x3.CalculateDeterminant();

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter3x3);
		deterMatrix3x3.LogMatrix();
	}

	if (deter3x3 != deter3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix %s should have been %s.  Instead it returned %s"), deterMatrix3x3, deter3x3Ans, deter3x3);
		logTimer();
		return false;
	}

	//Answer to the given matrix was generated from:  http://www.wolframalpha.com/input/?i=determinant
	/*
	[15		-7		1.4		-2.8]
	[-8.2	12		-14		4.6 ]
	[9.1	17.3	5		-22 ]
	[-12	-3.25	-10		7   ]
	*/
	Matrix deterMatrix4x4(4, 4,
	{
		15.f, -7.f, 1.4f, -2.8f,
		-8.2f, 12.f, -14.f, 4.6f,
		9.1f, 17.3f, 5.f, -22.f,
		-12.f, -3.25f, -10.f, 7.f
	});
	Float deter4x4Ans = -42104.237f;
	Float deter4x4 = deterMatrix4x4.CalculateDeterminant();
	deter4x4.RoundInline(3); //Clear any accumulated precision errors

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter4x4);
		deterMatrix4x4.LogMatrix();
	}

	if (deter4x4 != deter4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix %s should have been %s.  Instead it returned %s"), deterMatrix4x4, deter4x4Ans, deter4x4);
		logTimer();
		return false;
	}

	//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/inverting-3x3-part-1-calculating-matrix-of-minors-and-cofactor-matrix
	/*
	[-1	-2	2]
	[2	1	1]
	[3	4	5]
	*/
	Matrix majorMatrix(3, 3,
	{
		-1, -2, 2,
		2, 1, 1,
		3, 4, 5
	});

	/*
	[1		7		5]
	[-18	-11		2]
	[-4		-5		3]
	*/
	Matrix minorMatrixAns(3, 3,
	{
		1, 7, 5,
		-18, -11, 2,
		-4, -5, 3
	});

	Matrix minorMatrix(3, 3);
	majorMatrix.GetMatrixOfMinors(minorMatrix);
	minorMatrix.ForEachElement([&](UINT_TYPE i){minorMatrix.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The matrix of minors for the matrix listed below is listed below."));
		TestLog(testFlags, TXT("---- Original Matrix ----"));
		majorMatrix.LogMatrix();
		TestLog(testFlags, TXT("---- Matrix of minors ----"));
		minorMatrix.LogMatrix();
	}

	if (minorMatrix != minorMatrixAns)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix of minors for %s should have been %s.  Instead it returned %s"), majorMatrix, minorMatrixAns, minorMatrix);
		logTimer();
		return false;
	}

	/*
	[2	-6]
	[8	-4]
	*/
	Matrix adjugate2x2(2, 2,
	{
		2, -6,
		8, -4
	});

	/*
	[-4	6]
	[-8	2]
	*/
	Matrix adjugate2x2Ans(2, 2,
	{
		-4, 6,
		-8, 2
	});

	Matrix adjugate2x2Result(2, 2);
	adjugate2x2.CalculateAdjugate(adjugate2x2Result);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate2x2.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate2x2Result.LogMatrix();
	}

	if (adjugate2x2Result != adjugate2x2Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of matrix %s should have been %s.  Instead it returned %s"), adjugate2x2, adjugate2x2Ans, adjugate2x2Result);
		logTimer();
		return false;
	}

	/*
	[3	1	2 ]
	[9	-7	-6]
	[4	8	-5]
	*/
	Matrix adjugate3x3(3, 3,
	{
		3, 1, 2,
		9, -7, -6,
		4, 8, -5
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
	[83		21		8  ]
	[21		-23		36 ]
	[100	-20		-30]
	*/
	Matrix adjugate3x3Ans(3, 3,
	{
		83.f, 21.f, 8.f,
		21.f, -23.f, 36.f,
		100.f, -20.f, -30.f
	});

	Matrix adjugate3x3Result(3, 3);
	adjugate3x3.CalculateAdjugate(adjugate3x3Result);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate3x3.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate3x3Result.LogMatrix();
	}

	if (adjugate3x3Result != adjugate3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
			adjugate3x3.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			adjugate3x3Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			adjugate3x3Result.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[12		5		16		7.1 ]
	[-10	-3		4.5		-3  ]
	[11		6.2		8.5		-9.5]
	[4		9		-6		10.1]
	*/
	Matrix adjugate4x4(4, 4,
	{
		12.f, 5.f, 16.f, 7.1f,
		-10.f, -3.f, 4.5f, -3.f,
		11.f, 6.2f, 8.5f, -9.5f,
		4.f, 9.f, -6.f, 10.1f
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
	[-411.99	3032.94		30.3		1218.99 ]
	[659.45		-2749.4		-2051.6		-3209.95]
	[-1256.5	-1558.76	-148.4		280.7   ]
	[-1170.9	322.8		1728		-432.3  ]
	*/
	Matrix adjugate4x4Ans(4, 4,
	{
		-411.99f, 3032.94f, 30.3f, 1218.99f,
		659.45f, -2749.4f, -2051.6f, -3209.95f,
		-1256.5f, -1558.76f, -148.4f, 280.7f,
		-1170.9f, 322.8f, 1728.f, -432.3f
	});

	Matrix adjugate4x4Result(4, 4);
	adjugate4x4.CalculateAdjugate(adjugate4x4Result);

	//Remove accumulated precision errors
	adjugate4x4Result.ForEachElement([&](UINT_TYPE i){adjugate4x4Result.At(i).RoundInline(3);});
	adjugate4x4Ans.ForEachElement([&](UINT_TYPE i){adjugate4x4Ans.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate4x4.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate4x4Result.LogMatrix();
	}

	if (adjugate4x4Result != adjugate4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
			adjugate4x4.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			adjugate4x4Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			adjugate4x4Result.LogMatrix();
		}

		logTimer();
		return false;
	}
	/*
	[2	8]
	[4	7]
	*/
	Matrix preInverse2x2(2, 2,
	{
		2, 8,
		4, 7
	});

	/*
	[-7/18	4/9]	=>	[-0.388889	0.444444 ]
	[2/9	-1/9]	=>	[0.222222	-0.111111]
	*/
	Matrix inverse2x2Ans(2, 2,
	{
		-0.388889f, 0.444444f,
		0.222222f, -0.111111f
	});

	Matrix inverse2x2(2, 2);
	preInverse2x2.CalculateInverse(inverse2x2);

	Matrix inverseIdentity2x2(2, 2);
	inverseIdentity2x2 = preInverse2x2 * inverse2x2;

	//Truncate some decimal places due to comparison ahead
	inverse2x2.ForEachElement([&](UINT_TYPE i){inverse2x2.At(i).RoundInline(3);});
	inverse2x2Ans.ForEachElement([&](UINT_TYPE i){inverse2x2Ans.At(i).RoundInline(3);});
	inverseIdentity2x2.ForEachElement([&](UINT_TYPE i){inverseIdentity2x2.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse2x2.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
		inverse2x2.LogMatrix();
	}

	if (inverse2x2 != inverse2x2Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse2x2.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse2x2Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse2x2.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity2x2.LogMatrix();
	}

	if (!inverseIdentity2x2.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity2x2.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[20	23	20]
	[16	28	0]
	[26	24	15]
	*/
	Matrix preInverse3x3(3, 3,
	{
		20, 23, 20,
		16, 28, 0,
		26, 24, 15
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
	1/4000	[-420	-135	560]	=>	[-0.105	-0.03375	0.14  ]
			[240	220		-320]	=>	[0.06	0.055		-0.08 ]
			[344	-118	-192]	=>	[0.086	-0.0295		-0.048]
	*/
	Matrix inverse3x3Ans(3, 3,
	{
		-0.105f, -0.03375f, 0.14f,
		0.06f, 0.055f, -0.08f,
		0.086f, -0.0295f, -0.048f
	});

	Matrix inverse3x3(3, 3);
	preInverse3x3.CalculateInverse(inverse3x3);

	Matrix inverseIdentity3x3(3, 3);
	inverseIdentity3x3 = preInverse3x3 * inverse3x3;

	//Truncate some decimal places due to comparison ahead
	inverse3x3.ForEachElement([&](UINT_TYPE i){inverse3x3.At(i).RoundInline(3);});
	inverse3x3Ans.ForEachElement([&](UINT_TYPE i){inverse3x3Ans.At(i).RoundInline(3);});
	inverseIdentity3x3.ForEachElement([&](UINT_TYPE i){inverseIdentity3x3.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse3x3.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
		inverse3x3.LogMatrix();
	}

	if (inverse3x3 != inverse3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse3x3.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse3x3Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse3x3.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity3x3.LogMatrix();
	}

	if (!inverseIdentity3x3.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity3x3.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[1.08	0.15	-0.85	-2.07 ]
	[-7.1	1.5		0.68	-11.8 ]
	[0.62	-1.30	5.4		0.75  ]
	[-8.75	-3.15	-4.5	3.7   ]
	*/
	Matrix preInverse4x4(4, 4,
	{
		1.08f, 0.15f, -0.85f, -2.07f,
		-7.1f, 1.5f, 0.68f, -11.8f,
		0.62f, -1.3f, 5.4f, 0.75f,
		-8.75f, -3.15f, -4.5f, 3.7f
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
	[0.299873	-0.0590573	0.032073	-0.0270793]
	[-0.896168	0.0762782	-0.312901	-0.194678 ]
	[-0.20763	0.0303889	0.113471	-0.0422456]
	[-0.306317	-0.0377637	-0.0525347	-0.0108882]
	*/
	Matrix inverse4x4Ans(4, 4,
	{
		0.299873f, -0.0590573f, 0.032073f, -0.0270793f,
		-0.896168f, 0.0762782f, -0.312901f, -0.194678f,
		-0.20763f, 0.0303889f, 0.113471f, -0.0422456f,
		-0.306317f, -0.0377637f, -0.0525347f, -0.0108882f
	});

	Matrix inverse4x4(4, 4);
	preInverse4x4.CalculateInverse(inverse4x4);

	Matrix inverseIdentity4x4(4, 4);
	inverseIdentity4x4 = preInverse4x4 * inverse4x4;

	//Truncate some decimal places due to comparison ahead
	inverse4x4.ForEachElement([&](UINT_TYPE i){inverse4x4.At(i).RoundInline(6);});
	inverse4x4Ans.ForEachElement([&](UINT_TYPE i){inverse4x4Ans.At(i).RoundInline(6);});
	inverseIdentity4x4.ForEachElement([&](UINT_TYPE i){inverseIdentity4x4.At(i).RoundInline(6);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse4x4.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 6 decimal places) . . ."));
		inverse4x4.LogMatrix();
	}

	if (inverse4x4 != inverse4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse4x4.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse4x4Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse4x4.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity4x4.LogMatrix();
	}

	if (!inverseIdentity4x4.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity4x4.LogMatrix();
		}

		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Division"));
	{
		Matrix originalMatrix(4, 4,
		{
			4.f, 40.f, 400.f, 1.f,
			-8.f, -16.f, -32.f, 4.f,
			12.f, -12.f, 24.f, -24.f,
			80.f, 60.f, 40.f, 20.f
		});

		Matrix scaledMatrix = originalMatrix / 4.f;
		Matrix expectedMatrix = Matrix(4, 4,
		{
			1.f, 10.f, 100.f, 0.25f,
			-2.f, -4.f, -8.f, 1.f,
			3.f, -3.f, 6.f, -6.f,
			20.f, 15.f, 10.f, 5.f
		});

		if (scaledMatrix != expectedMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix division test failed. Expected matrix does not match the calculated scaled matrix."));

			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Original matrix..."));
				originalMatrix.LogMatrix();

				TestLog(testFlags, TXT("Calculated matrix after dividing original by 4..."));
				scaledMatrix.LogMatrix();

				TestLog(testFlags, TXT("Expected scaled matrix..."));
				expectedMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		Matrix multiplier = Matrix(4, 4,
		{
			3.f, 4.f, 3.f, 10.f,
			-15.f, 8.f, 6.f, 0.f,
			-2.f, 3.f, 3.f, 7.f,
			-1.f, 1.f, 5.f, 1.f
		});

		scaledMatrix = originalMatrix * multiplier;
		scaledMatrix /= multiplier;

		if (!scaledMatrix.IsNearlyEqual(originalMatrix, 0.0001f))
		{
			UnitTestError(testFlags, TXT("Matrix division test failed. After multiplying a matrix by a matrix, dividing the resulting matrix by the inverse multiplier matrix should have resulted in the original matrix."));

			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Original matrix..."));
				originalMatrix.LogMatrix();

				TestLog(testFlags, TXT("Resulting matrix..."));
				scaledMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Transform Matrix"));
	{
		TestLog(testFlags, TXT("Testing transform matrices (Scale)."));
		{
			std::function<bool(const TransformMatrix&, const Vector3&)> testScalarMatrix([&](const TransformMatrix& givenMatrix, const Vector3& expectedScalar)
			{
				Vector3 scalarVector = givenMatrix.GetScale();
				if (scalarVector != expectedScalar)
				{
					UnitTestError(testFlags, TXT("TransformMatrix::GetScale test failed. The resulting scalar vector is %s instead of %s."), scalarVector, expectedScalar);
					if (ShouldHaveDebugLogs(testFlags))
					{
						TestLog(testFlags, TXT("Given transform matrix..."));
						givenMatrix.LogMatrix();
					}
					return false;
				}

				return true;
			});

			TransformMatrix testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			if (!testScalarMatrix(testMatrix, Vector3(1.f, 1.f, 1.f)))
			{
				return false;
			}

			Vector3 expectedScale(2.f, 4.f, 8.f);
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Setting translation should not affect scale.
			testMatrix.Translate(Vector3(1234.f, 4321.f, -50.f));
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Applying another scale to the same matrix
			expectedScale *= 3.f;
			testMatrix.Scale(Vector3(3.f, 3.f, 3.f));
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Test inversions
			expectedScale = Vector3(-0.5f, 2.f, 9.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(-1.5f, -2.5f, 7.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(4.5f, -4.25f, -2.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(-4.5f, -5.f, -2.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}
		}

		TestLog(testFlags, TXT("Testing transform matrices (Rotation)."));
		{
			enum rotationAxis
			{
				RA_XAxis,
				RA_YAxis,
				RA_ZAxis
			};

			std::function<bool(Float, rotationAxis, const Vector3&, const Vector3&)> testRotationMatrix([&](Float radians, rotationAxis axis, const Vector3& startingPoint, const Vector3& expectedRotatedPoint)
			{
				TransformMatrix originalPoint;
				originalPoint.SetTranslation(startingPoint);

				DString axisString;
				TransformMatrix rotation;

				switch (axis)
				{
					case(RA_XAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutXAxis(radians);
						axisString = TXT("X");
						break;

					case(RA_YAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutYAxis(radians);
						axisString = TXT("Y");
						break;

					case(RA_ZAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutZAxis(radians);
						axisString = TXT("Z");
						break;
				}

				TransformMatrix rotatedMatrix = rotation * originalPoint;
				TransformMatrix expectedRotatedMatrix;
				expectedRotatedMatrix.SetTranslation(expectedRotatedPoint);

				Vector3 rotatedTranslation = rotatedMatrix.GetTranslation();
				Vector3 expectedTranslation = expectedRotatedMatrix.GetTranslation();
				if (!rotatedTranslation.IsNearlyEqual(expectedTranslation, 0.0001f))
				{
					UnitTestError(testFlags, TXT("Rotation matrix test failed. After rotating %s radians about the %s-axis, the expected transform does not match the calculated transform. actual translation %s != expected translation %s"), radians, axisString, rotatedTranslation, expectedTranslation);

					if (ShouldHaveDebugLogs(testFlags))
					{
						TestLog(testFlags, TXT("Original transform..."));
						originalPoint.LogMatrix();

						TestLog(testFlags, TXT("Rotation matrix..."));
						rotation.LogMatrix();

						TestLog(testFlags, TXT("Calculated rotated matrix..."));
						rotatedMatrix.LogMatrix();

						TestLog(testFlags, TXT("Expected rotated matrix..."));
						expectedRotatedMatrix.LogMatrix();
					}

					logTimer();
					return false;
				}

				return true;
			});

			Float halfPi = PI_FLOAT * 0.5f;

			//Ensure 0 rotation does not affect translation
			{
				const Vector3 noRotationVector(5.f, 5.f, 5.f);
				if (!testRotationMatrix(0.f, RA_XAxis, noRotationVector, noRotationVector))
				{
					return false;
				}

				if (!testRotationMatrix(0.f, RA_YAxis, noRotationVector, noRotationVector))
				{
					return false;
				}

				if (!testRotationMatrix(0.f, RA_ZAxis, noRotationVector, noRotationVector))
				{
					return false;
				}
			}

			//Test rotating by 90 degrees (two tests per rotate by axis to test the signess of sin(90))
			{
				if (!testRotationMatrix(halfPi, RA_XAxis, Vector3(0.f, 1.f, 0.f), Vector3(0.f, 0.f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_XAxis, Vector3(0.f, 0.f, 1.f), Vector3(0.f, 1.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_YAxis, Vector3(1.f, 0.f, 0.f), Vector3(0.f, 0.f, 1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_YAxis, Vector3(0.f, 0.f, 1.f), Vector3(-1.f, 0.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_ZAxis, Vector3(0.f, 1.f, 0.f), Vector3(1.f, 0.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(-halfPi, RA_ZAxis, Vector3(1.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f)))
				{
					return false;
				}
			}

			//Test 180 rotations (cos(180)=>1 and sin(180)=>0). One test per axis since each component in the translation vector are not zero.
			{
				if (!testRotationMatrix(halfPi * 2.f, RA_XAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(0.5f, 0.5f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 2.f, RA_YAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(-0.5f, -0.5f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 2.f, RA_ZAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(-0.5f, 0.5f, 1.f)))
				{
					return false;
				}
			}

			//Test full revolution to ensure it doesn't affect the resulting translation
			{
				const Vector3 testTranslation(0.25f, -33.f, 14.f);
				if (!testRotationMatrix(halfPi * 4.f, RA_XAxis, testTranslation, testTranslation))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 4.f, RA_YAxis, testTranslation, testTranslation))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * -4.f, RA_ZAxis, testTranslation, testTranslation))
				{
					return false;
				}
			}

			if (!testRotationMatrix(halfPi, RA_ZAxis, Vector3(-15.f, 0.1f, 64.f), Vector3(0.1f, 15.f, 64.f)))
			{
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Extracting Euler Angles from Transform Matrix"));
	{
		std::function<bool(const Rotator&)> testEulerExtraction([&](const Rotator& testRotation)
		{
			TransformMatrix rotatedMatrix;
			rotatedMatrix.Rotate(testRotation);
			Rotator extractedRotation = rotatedMatrix.GetRotation();

			//Compare by directional vector since there are multiple different transforms that could lead to the same direction.
			//For example: Pitch 135 is also equal to Yaw 180, and pitch 45.
			Vector3 testRotationDir = testRotation.GetDirectionalVector();
			Vector3 extractedRotationDir = extractedRotation.GetDirectionalVector();

			if (!testRotationDir.IsNearlyEqual(extractedRotationDir, 0.001f))
			{
				UnitTestError(testFlags, TXT("Extracting Euler Angles from Transform Matrix test failed. After rotating an identity transform matrix by %s, the test is expected to extract %s from the resulting matrix. It pulled %s instead. Compared against directional vectors: (expected) %s != (actual) %s"), testRotation, testRotation, extractedRotation, testRotationDir, extractedRotationDir);
				if (ShouldHaveDebugLogs(testFlags))
				{
					TestLog(testFlags, TXT("Rotated transform matrix..."));
					rotatedMatrix.LogMatrix();
				}

				logTimer();
				return false;
			}

			return true;
		});

		if (!testEulerExtraction(Rotator::ZERO_ROTATOR))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(45, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(80, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(100, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(180, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(200, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(270, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(300, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, -45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 135, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 0, 80, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 0, 150, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 280, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(45, 45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(85, -15, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 35, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, -20, 160, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(40, 45, 50, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(185, 60, -20, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 180, 270, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	logTimer();
	ExecuteSuccessSequence(testFlags, TXT("Matrix"));

	return true;
}
SD_END

#endif