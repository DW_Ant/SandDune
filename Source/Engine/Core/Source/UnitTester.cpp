/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTester.cpp
=====================================================================
*/

#include "Core.h"
#include "DatatypeUnitTester.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "EngineIntegrityUnitTester.h"
#include "LogCategory.h"
#include "Object.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_ABSTRACT_CLASS(SD::UnitTester, SD::Object)
SD_BEGIN

void UnitTester::TestLog (EUnitTestFlags testFlags, const DString& msg)
{
	if ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0)
	{
		UnitTestLog.Log(LogCategory::LL_Debug, msg);
	}
}

bool UnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	bool bCompletedDatatype = false;
	bool bCompletedEngineInteg = false;

	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
		{
			bCompletedDatatype = true;
			if (bCompletedEngineInteg)
			{
				return true;
			}
		}
		else if (dynamic_cast<const EngineIntegrityUnitTester*>(completedTests.at(i)) != nullptr)
		{
			bCompletedEngineInteg = true;
			if (bCompletedDatatype)
			{
				return true;
			}
		}
	}

	return false;
}

bool UnitTester::IsTestEnabled (EUnitTestFlags testFlags) const
{
	return true;
}

bool UnitTester::ShouldHaveDebugLogs (EUnitTestFlags testFlags) const
{
	return ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0);
}

void UnitTester::SetTestCategory (EUnitTestFlags testFlags, const DString& newTestCategory) const
{
	if (newTestCategory.IsEmpty() && !TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("Finished %s tests."), newTestCategory);
	}

	TestCategory = newTestCategory;
	if (!TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("Begin %s testing. . ."), TestCategory);
	}
}

void UnitTester::CompleteTestCategory (EUnitTestFlags testFlags) const
{
	if (!TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("%s tests passed!"), TestCategory);
	}

	TestCategory = TXT("");
}

void UnitTester::BeginTestSequence (EUnitTestFlags testFlags, const DString& testName) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== Begin %s Test ==-"), testName);
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
}

void UnitTester::ExecuteSuccessSequence (EUnitTestFlags testFlags, const DString& testName) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== %s Test PASSSED! ==-"), testName);
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
	TestCategory = TXT("");
}

void UnitTester::ExecuteFailSequence (EUnitTestFlags testFlags) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== ERROR:  %s Test FAILED! ==-"), GetName());
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
	TestCategory = TXT("");
}
SD_END

#endif