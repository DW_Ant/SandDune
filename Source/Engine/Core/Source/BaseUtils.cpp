/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseUtils.cpp
=====================================================================
*/

#include "BaseUtils.h"
#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "Object.h"

IMPLEMENT_ABSTRACT_CLASS(SD::BaseUtils, SD::Object)
SD_BEGIN
SD_END