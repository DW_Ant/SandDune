/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineIntegrityTester.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::EngineIntegrityTester, SD::Object)
SD_BEGIN

void EngineIntegrityTester::Destroyed ()
{
	//At least try to remove left over objects if one of the tests failed
	for (UINT_TYPE i = 0; i < InstantiatedObjects.size(); i++)
	{
		InstantiatedObjects.at(i)->Destroy();
	}

	Super::Destroyed();
}

bool EngineIntegrityTester::TestClassIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Class Iterator"));

	//Populate classes that should be found
	std::vector<const DClass*> requiredObjectClasses;
	std::vector<const DClass*> requiredEngineClasses;
	requiredObjectClasses.push_back(Object::SStaticClass());
	requiredObjectClasses.push_back(Entity::SStaticClass());
	requiredObjectClasses.push_back(EntityComponent::SStaticClass());
	requiredObjectClasses.push_back(DatatypeUnitTester::SStaticClass());
	requiredObjectClasses.push_back(EngineIntegrityUnitTester::SStaticClass());
	requiredObjectClasses.push_back(UnitTester::SStaticClass());
	requiredObjectClasses.push_back(Utils::SStaticClass());
	requiredEngineClasses.push_back(EngineComponent::SStaticClass());

	UnitTester::TestLog(testFlags, TXT("Launching object class iterator."));
	Int classCounter = 0;
	for (ClassIterator iter; iter.SelectedClass; iter++)
	{
		classCounter++;
		UnitTester::TestLog(testFlags, TXT("Class Iterator found class:  ") + iter.SelectedClass->GetDuneClassName());
		if (!iter.GetSelectedClass()->IsAbstract() && iter.SelectedClass->ClassType != DClass::CT_Object)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The object class iterator found a class that's not an object named %s."), iter.SelectedClass->GetDuneClassName());
			return false;
		}

		if (iter.GetSelectedClass()->IsAbstract() && iter.GetSelectedClass()->GetDefaultObject() != nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The class %s claims that it's abstract, but it has a ClassDefaultObject associated with it."), iter.GetSelectedClass()->GetDuneClassName());
			return false;
		}

		if (!iter.GetSelectedClass()->IsAbstract() && iter.GetSelectedClass()->GetDefaultObject() == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The class %s is not abstract, but it doesn't have a default object associated with it."), iter.GetSelectedClass()->GetDuneClassName());
			return false;
		}

		const Object* curObject = static_cast<const Object*>(iter.SelectedClass->GetDefaultObject());
		if (curObject != nullptr)
		{
			UnitTester::TestLog(testFlags, TXT("    Default object is:  ") + curObject->GetName());
		}

		for (UINT_TYPE i = 0; i < requiredObjectClasses.size(); i++)
		{
			if (requiredObjectClasses.at(i) == iter.SelectedClass)
			{
				requiredObjectClasses.erase(requiredObjectClasses.begin() + i);
				break;
			}
		}
	}

	UnitTester::TestLog(testFlags, TXT("Total number of found classes:  %s."), classCounter);
	if (classCounter <= 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  Class iterator was unable to find any registered classes."));
		return false;
	}

	if (requiredObjectClasses.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected object classes.  See the log for a list of classes that were not found:"));
		for (UINT_TYPE i = 0; i < requiredObjectClasses.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [Unit Test failure - Missing class] %s"), requiredObjectClasses.at(i)->GetDuneClassName());
		}

		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Launching engine component class iterator."));
	Int numEngineClasses = 0;
	for (ClassIterator iter(EngineComponent::SStaticClass()); iter.SelectedClass; iter++)
	{
		UnitTester::TestLog(testFlags, TXT("Engine class Iterator found class:  ") + iter.SelectedClass->GetDuneClassName());
		numEngineClasses++;
		if (iter.SelectedClass->ClassType != DClass::CT_EngineComponent)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The engine component class iterator found a class that's not an engine component named %s."), iter.SelectedClass->GetDuneClassName());
			return false;
		}

		const EngineComponent* cdo = iter.SelectedClass->GetDefaultEngineComponent();
		if (cdo != nullptr)
		{
			UnitTester::TestLog(testFlags, TXT("    Default engine component is:  ") + cdo->GetName());
		}

		for (UINT_TYPE i = 0; i < requiredEngineClasses.size(); i++)
		{
			if (requiredEngineClasses.at(i) == iter.SelectedClass)
			{
				requiredEngineClasses.erase(requiredEngineClasses.begin() + i);
				break;
			}
		}
	}

	if (requiredEngineClasses.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected engine component classes.  See the logs for a list of classes that were not found."));
		for (UINT_TYPE i = 0; i < requiredEngineClasses.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [Unit test failure - missing class] %s"), requiredEngineClasses.at(i)->GetDuneClassName());
		}

		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Class Iterator"));
	return true;
}

bool EngineIntegrityTester::TestObjectInstantiation (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Instantiation"));

	NotifyEngineComponents(EngineComponent::EIS_PreInstantiation);

	//Identify the objects that existed prior to test
	ContainerUtils::Empty(OUT AllObjectsPriorTest);
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
		AllObjectsPriorTest.push_back(iter.SelectedObject.Get());
	}

#if 0
	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		Engine::FindEngine()->LogObjectHashTable();
		UnitTester::TestLog(testFlags, TXT("There are a total of %s objects before this unit test created an object."), Int(AllObjectsPriorTest.size()));
	}
#endif

	for (ClassIterator iter; iter.SelectedClass; iter++)
	{
		const Object* defaultObject = (iter.SelectedClass->GetDefaultObject());
		if (defaultObject != nullptr && defaultObject->CanRunInObjIterTest())
		{
			Object* newObject = defaultObject->CreateObjectOfMatchingClass();
			if (newObject != nullptr)
			{
				UnitTester::TestLog(testFlags, TXT("Created a new object:  %s"), newObject->ToString());
				InstantiatedObjects.push_back(newObject);
			}
		}
	}

	UnitTester::TestLog(testFlags, TXT("Total number of instantiated objects:  %s."), Int(InstantiatedObjects.size()));
	if (InstantiatedObjects.size() <= 0)
	{
		tester->UnitTestError(testFlags, TXT("Object instantiation test failed!  Unit tester was unable to create an object."));
		return false;
	}

#if 0
	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		UnitTester::TestLog(testFlags, TXT("Displaying Engine's hash table. . ."));
		Engine::FindEngine()->LogObjectHashTable();
	}
#endif

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Instantiation"));
	return true;
}

bool EngineIntegrityTester::TestObjectIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Iterator"));

	NotifyEngineComponents(EngineComponent::EIS_PreIteration);

	Int numFoundObjects = 0;
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
#if 0
		UnitTester::TestLog(testFlags, TXT("Object iterator found %s"), iter.SelectedObject->ToString());
#endif
		numFoundObjects++;
	}

	UnitTester::TestLog(testFlags, TXT("Total number of found objects:  %s"), numFoundObjects);
	if (numFoundObjects.ToUnsignedInt() < InstantiatedObjects.size())
	{
		tester->UnitTestError(testFlags, TXT("Object iterator test failed!  The iterator is expected to find at least %s objects.  It only found %s."), Int(InstantiatedObjects.size()), numFoundObjects);
		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Iterator"));
	return true;
}

bool EngineIntegrityTester::TestCleanUp (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Cleanup"));

	NotifyEngineComponents(EngineComponent::EIS_PreCleanup);

	for (UINT_TYPE i = 0; i < InstantiatedObjects.size(); i++)
	{
#if 0
		UnitTester::TestLog(testFlags, TXT("Destroying object:  %s"), InstantiatedObjects.at(i)->ToString());
#endif
		InstantiatedObjects.at(i)->Destroy();
	}

	//Force garbage collector for immediate updates to the hash table
	Engine::FindEngine()->CollectGarbage();

#if 0
	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		UnitTester::TestLog(testFlags, TXT("Displaying Engine's hash table. . ."));
		Engine::FindEngine()->LogObjectHashTable();
	}
#endif
	InstantiatedObjects.clear();

	//Identify if there are any extra objects
	std::vector<Object*> allObjsAfterTest;
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
		allObjsAfterTest.push_back(iter.SelectedObject.Get());
	}
	UnitTester::TestLog(testFlags, TXT("There are now a total of %s objects after unit test clean up."), Int(allObjsAfterTest.size()));

	if (AllObjectsPriorTest.size() != allObjsAfterTest.size())
	{
		if (allObjsAfterTest.size() > AllObjectsPriorTest.size())
		{
			//Identify what are the extra objects
			std::vector<Object*> extraObjs;
			for (Object* obj : allObjsAfterTest)
			{
				if (ContainerUtils::FindInVector(AllObjectsPriorTest, obj) == UINT_INDEX_NONE)
				{
					extraObjs.push_back(obj);
				}
			}

			UnitTester::TestLog(testFlags, TXT("The following are objects that exist now that did not exist prior to test:"));
			for (size_t i = 0; i < extraObjs.size(); ++i)
			{
				UnitTester::TestLog(testFlags, TXT("    [%s] = %s"), Int(i), extraObjs.at(i)->ToString());
			}
		}
		else
		{
			//Identify the object that was removed when it wasn't supposed to
			std::vector<size_t> extraObjIndices;
			for (size_t i = 0; i < AllObjectsPriorTest.size(); ++i)
			{
				if (ContainerUtils::FindInVector(allObjsAfterTest, AllObjectsPriorTest.at(i)) == UINT_INDEX_NONE)
				{
					extraObjIndices.push_back(i);
				}
			}

			UnitTester::TestLog(testFlags, TXT("The following are indices to the AllObjectsPriorTest vector of objects that were removed when they were not supposed to be removed during the test:"));
			for (size_t i = 0; i < extraObjIndices.size(); ++i)
			{
				//Can't log out the object since that object is probably destroyed/cleaned up at this point. Only log out the index.
				UnitTester::TestLog(testFlags, TXT("    [%s]"), Int(extraObjIndices.at(i)));
			}
		}

		tester->UnitTestError(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), Int(AllObjectsPriorTest.size()), Int(allObjsAfterTest.size()));
		UnitTester::TestLog(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), Int(AllObjectsPriorTest.size()), Int(allObjsAfterTest.size()));
		UnitTester::TestLog(testFlags, TXT("Created objects that automatically spawn other objects should also clean up after themselves."));
		return false;
	}

	//Just to free up space, this data is no longer needed.
	ContainerUtils::Empty(OUT AllObjectsPriorTest);

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Cleanup"));
	return true;
}

bool EngineIntegrityTester::TestComponents (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Component"));

	UnitTester::TestLog(testFlags, TXT("Creating an entity, and attaching a Tick Component and attaching a Life Span component to the tick component."));
	Entity* testEntity = Entity::CreateObject();
	if (testEntity->HasComponent(TickComponent::SStaticClass(), true))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. After creating a blank Entity, HasComponent returns true despite not attaching anything to it."));
		return false;
	}

	TickComponent* testTick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	LifeSpanComponent* testLifeSpan = LifeSpanComponent::CreateObject();

	if (!testEntity->AddComponent(testTick) || !testTick->AddComponent(testLifeSpan))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach either Tick Component to entity or Life Span Component to Tick Component."));
		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Attempting to retrieve components from Entity"));
	testTick = dynamic_cast<TickComponent*>(testEntity->FindComponent(Engine::FindEngine()->GetTickComponentHashNumber()));
	if (testTick == nullptr)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Tick Component attached to %s."), testEntity->ToString());
		return false;
	}

	if (!testEntity->HasComponent(TickComponent::SStaticClass(), true))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. After attaching a TickComponent to an Entity, the Entity's HasComponent method is unable to find the TickComponent."));
		return false;
	}

	testLifeSpan = dynamic_cast<LifeSpanComponent*>(testEntity->FindComponent(LifeSpanComponent::SStaticClass(), true));
	if (testLifeSpan == nullptr)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Life Span Component from %s."), testEntity->ToString());
		return false;
	}

	/*
	Entity
		Tick0
			LifeSpan0
				tick [inherited from LifeSpan]
				tick3
		tick1
		tick2
	*/

	UnitTester::TestLog(testFlags, TXT("Attaching two more Tick Components to owning entity, and another Tick component to the life span component."));
	TickComponent* tick1 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	TickComponent* tick2 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	TickComponent* tick3 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!testEntity->AddComponent(tick1) || !testEntity->AddComponent(tick2) || !testLifeSpan->AddComponent(tick3))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach multiple tick components to the test entity."));
		return false;
	}
	std::vector<EntityComponent*> componentsToFind;
	componentsToFind.push_back(testTick);
	componentsToFind.push_back(testLifeSpan);
	componentsToFind.push_back(tick1);
	componentsToFind.push_back(tick2);
	componentsToFind.push_back(tick3);

	for (ComponentIterator iter(testEntity, true); iter.GetSelectedComponent() != nullptr; iter++)
	{
		for (UINT_TYPE i = 0; i < componentsToFind.size(); i++)
		{
			if (componentsToFind.at(i) == iter.GetSelectedComponent())
			{
				componentsToFind.erase(componentsToFind.begin() + i);
				break;
			}
		}
	}

	if (componentsToFind.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  The component iterator was not able to find all components attached to %s."), testEntity->ToString());
		UnitTester::TestLog(testFlags, TXT("The following components were not found. . ."));
		for (UINT_TYPE i = 0; i < componentsToFind.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [%s] = %s"), Int(i), componentsToFind.at(i)->ToString());
		}
		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Detaching components from their owners."));
	bool removeCompSuccessful = true;
	removeCompSuccessful &= testEntity->RemoveComponent(testTick);
	//removeCompSuccessful &= testEntity->RemoveComponent(tick3); //This component is already removed when testTick was removed.
	removeCompSuccessful &= testEntity->RemoveComponent(tick2);
	removeCompSuccessful &= testEntity->RemoveComponent(tick1); //out of order is intended for testing purposes.

	if (!removeCompSuccessful)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. Unable to detach some of the tick components from their owner."));
		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Component test passed!  Was able to find all Tick and Life Span components from %s."), testEntity->ToString());
	testEntity->Destroy();
	testTick->Destroy();
	tick1->Destroy();
	tick2->Destroy();
	//tick3->Destroy(); //No need to destroy tick3. Even though testTick is detached from the owner, tick3 is still attached to the lifespan, which is also attached to the testTick. Destroying testTick will also destroy all of its components recursively.

	UnitTester::TestLog(testFlags, TXT("Testing a component failing to attach to an owner."));
	TickComponent* failedCompTest = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	TickComponent* failedCompPtrCpy = failedCompTest; //Create a second pointer since AddComponent is suppose to null out the pointer. Create a copy to verify that the other comp is marked for destruction.
	if (failedCompTest->AddComponent(failedCompTest, true))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. A component should not be able to attach to itself."));
		return false;
	}

	if (failedCompTest != nullptr)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. A component that failed to attach to an Entity should have been nulled out. The pointer is still pointing to a memory address."));
		return false;
	}

	if (!failedCompPtrCpy->GetPendingDelete())
	{
		tester->UnitTestError(testFlags, TXT("Components test failed. A component that failed to attach to an Entity should have been marked for destruction. Otherwise this could be a memory leak assuming the calling function does not destroy the component explicitly."));
		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Component"));
	return true;
}

bool EngineIntegrityTester::TestComponentIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Component Iterator"));

	Entity* componentOwner = Entity::CreateObject();

	std::function<void()> clearEntities([&]()
	{
		componentOwner->Destroy();
	});

	//Generates a string that lists all debug names sequentially that are found in the given vector.
	std::function<DString(const std::vector<EntityComponent*>&)> vectorToStr([](const std::vector<EntityComponent*>& compList)
	{
		DString missingComps = DString::EmptyString;
		for (size_t i = 0; i < compList.size(); ++i)
		{
			missingComps += compList.at(i)->DebugName + TXT(", ");
		}

		//Remove trailing comma space
		missingComps.Remove(missingComps.Length() - 2, 2);
		return missingComps;
	});

	//Component structure
	/*
	componentOwner
	  -A
	    -A1
		-A2
	  -B
	  -C
	    -C1
		  -C1a
		  -C1b
		-C2
		  -C2a
		-C3
		  -C3a
	  -D
	*/
	EntityComponent* a = EntityComponent::CreateObject();
	if (componentOwner->AddComponent(a))
	{
		a->DebugName = TXT("A");
	}

	EntityComponent* a1 = EntityComponent::CreateObject();
	if (a->AddComponent(a1))
	{
		a1->DebugName = TXT("A1");
	}

	EntityComponent* a2 = EntityComponent::CreateObject();
	if (a->AddComponent(a2))
	{
		a2->DebugName = TXT("A2");
	}

	EntityComponent* b = EntityComponent::CreateObject();
	if (componentOwner->AddComponent(b))
	{
		b->DebugName = TXT("B");
	}

	EntityComponent* c = EntityComponent::CreateObject();
	if (componentOwner->AddComponent(c))
	{
		c->DebugName = TXT("C");
	}

	EntityComponent* c1 = EntityComponent::CreateObject();
	if (c->AddComponent(c1))
	{
		c1->DebugName = TXT("C1");
	}

	EntityComponent* c1a = EntityComponent::CreateObject();
	if (c1->AddComponent(c1a))
	{
		c1a->DebugName = TXT("C1a");
	}

	EntityComponent* c1b = EntityComponent::CreateObject();
	if (c1->AddComponent(c1b))
	{
		c1b->DebugName = TXT("C1b");
	}

	EntityComponent* c2 = EntityComponent::CreateObject();
	if (c->AddComponent(c2))
	{
		c2->DebugName = TXT("C2");
	}

	EntityComponent* c2a = EntityComponent::CreateObject();
	if (c2->AddComponent(c2a))
	{
		c2a->DebugName = TXT("C2a");
	}

	EntityComponent* c3 = EntityComponent::CreateObject();
	if (c->AddComponent(c3))
	{
		c3->DebugName = TXT("C3");
	}

	EntityComponent* c3a = EntityComponent::CreateObject();
	if (c3->AddComponent(c3a))
	{
		c3a->DebugName = TXT("C3a");
	}

	EntityComponent* d = EntityComponent::CreateObject();
	if (componentOwner->AddComponent(d))
	{
		d->DebugName = TXT("D");
	}

	tester->SetTestCategory(testFlags, TXT("NonRecursive test"));
	{
		std::vector<EntityComponent*> expectedComps({a, b, c, d});
		for (ComponentIterator iter(componentOwner, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			UINT_TYPE removedIdx = ContainerUtils::RemoveItem(OUT expectedComps, iter.GetSelectedComponent());
			if (removedIdx == UINT_INDEX_NONE)
			{
				tester->UnitTestError(testFlags, TXT("Non recursive component iterator test failed. The iterator found a component it's not suppose to find. It found %s."), iter.GetSelectedComponent()->DebugName);
				clearEntities();
				return false;
			}
		}

		if (!ContainerUtils::IsEmpty(expectedComps))
		{
			tester->UnitTestError(testFlags, TXT("Non recursive component iterator test failed. The iterator was unable to find the following components: %s"), vectorToStr(expectedComps));
			clearEntities();
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->SetTestCategory(testFlags, TXT("Recursive Component Iterator"));
	{
		std::vector<EntityComponent*> expectedComps({a, a1, a2, b, c, c1, c1a, c1b, c2, c2a, c3, c3a, d});
		for (ComponentIterator iter(componentOwner, true); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			UINT_TYPE removedIdx = ContainerUtils::RemoveItem(OUT expectedComps, iter.GetSelectedComponent());
			if (removedIdx == UINT_INDEX_NONE)
			{
				tester->UnitTestError(testFlags, TXT("Recursive component iterator test failed. It found a component it was not suppose to. It found: %s"), iter.GetSelectedComponent()->DebugName);
				clearEntities();
				return false;
			}
		}

		if (!ContainerUtils::IsEmpty(expectedComps))
		{
			tester->UnitTestError(testFlags, TXT("Recursive component iterator test failed. The iterator was unable to find the following components: %s"), vectorToStr(expectedComps));
			clearEntities();
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->SetTestCategory(testFlags, TXT("Skipping Component Iterator"));
	{
		std::vector<EntityComponent*> expectedComps({a, a1, a2, b, c, c1, c2, c3, d});
		std::vector<EntityComponent*> compsToSkip({c1, c2, c3});
		for (ComponentIterator iter(componentOwner, true); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			size_t skipIdx = ContainerUtils::FindInVector(compsToSkip, iter.GetSelectedComponent());
			if (skipIdx != UINT_INDEX_NONE)
			{
				iter.SkipSubComponents();
			}

			size_t removeIdx = ContainerUtils::RemoveItem(OUT expectedComps, iter.GetSelectedComponent());
			if (removeIdx == UINT_INDEX_NONE)
			{
				tester->UnitTestError(testFlags, TXT("Skipping component iterator test failed. It found a component it was not suppose to. It found: %s"), iter.GetSelectedComponent()->DebugName);
				clearEntities();
				return false;
			}
		}

		if (!ContainerUtils::IsEmpty(expectedComps))
		{
			tester->UnitTestError(testFlags, TXT("Skipping component iterator test failed. The iterator was unable to find the following components: %s"), vectorToStr(expectedComps));
			clearEntities();
			return false;
		}
	}
	tester->CompleteTestCategory(testFlags);

	tester->ExecuteSuccessSequence(testFlags, TXT("Component Iterator"));
	clearEntities();
	return true;
}

bool EngineIntegrityTester::TestEntityVisibility (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Entity Visibility"));
	std::vector<Entity*> entitiesToRemove;
	std::function<void()> cleanUpEntities = [&]
	{
		for (UINT_TYPE i = 0; i < entitiesToRemove.size(); ++i)
		{
			entitiesToRemove.at(i)->Destroy();
		}

		ContainerUtils::Empty(entitiesToRemove);
	};

	Entity* baseEntityA = Entity::CreateObject(); //EntityA has a shallow but wide component list
	Entity* baseEntityB = Entity::CreateObject(); //EntityB has a thin but deep component list.

	entitiesToRemove.push_back(baseEntityA);
	entitiesToRemove.push_back(baseEntityB);

	/*
	EntityA
		-compA
		-compB
		-compC
	EntityB
		-compD
			-compE
				-compF
	*/

	EntityComponent* compA = EntityComponent::CreateObject();
	EntityComponent* compB = EntityComponent::CreateObject();
	EntityComponent* compC = EntityComponent::CreateObject();
	EntityComponent* compD = EntityComponent::CreateObject();
	EntityComponent* compE = EntityComponent::CreateObject();
	EntityComponent* compF = EntityComponent::CreateObject();
	EntityComponent* testedComponent = EntityComponent::CreateObject(); //This component will move around while testing visibility flags

	//No need to add components to entitiesToRemove vector since EntityComponents are automatically destroyed when their owners are destroyed.

	bool bFailedToAttach = false;
	bFailedToAttach |= !baseEntityA->AddComponent(compA);
	bFailedToAttach |= !baseEntityA->AddComponent(compB);
	bFailedToAttach |= !baseEntityA->AddComponent(compC);
	bFailedToAttach |= !baseEntityB->AddComponent(compD);
	bFailedToAttach |= !compD->AddComponent(compE);
	bFailedToAttach |= !compE->AddComponent(compF);
	bFailedToAttach |= !compF->AddComponent(testedComponent);

	if (bFailedToAttach)
	{
		tester->UnitTestError(testFlags, TXT("Failed to initialize visibility test.  One of the EntityComponents couldn't attach to an Entity."));
		cleanUpEntities();
		return false;
	}

	if (!testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  Entities should appear visible by default."));
		cleanUpEntities();
		return false;
	}

	testedComponent->SetVisibility(false);
	if (testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After setting an EntityComponent to be invisible, it should be invisible."));
		cleanUpEntities();
		return false;
	}

	testedComponent->SetVisibility(true);
	if (!testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After restoring an EntityComponent's visibility flag, it should be visible."));
		cleanUpEntities();
		return false;
	}

	baseEntityB->SetVisibility(false);
	if (testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After setting its root owner to invisible, the EntityComponent should also be invisibie."));
		cleanUpEntities();
		return false;
	}

	compD->SetVisibility(false);
	compE->SetVisibility(false);
	compF->SetVisibility(false);
	baseEntityB->SetVisibility(true);
	std::function<bool(bool /*bExpectedVisibility*/)> testEntity = [&](bool bExpectedVisibility)
	{
		if (testedComponent->IsVisible() != bExpectedVisibility)
		{
			if (bExpectedVisibility)
			{
				tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  All of its owning components and the entity component is visible.  testedComponent->IsVisible is still returning false."));
			}
			else
			{
				tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  If any of the Owning Components are invisible, the child EntityComponent must also be invisible."));
			}

			cleanUpEntities();
			return false;
		}
		return true;
	};

	if (!testEntity(false))
	{
		return false;
	}

	compD->SetVisibility(true);
	if (!testEntity(false))
	{
		return false;
	}

	compE->SetVisibility(true);
	if (!testEntity(false))
	{
		return false;
	}

	compF->SetVisibility(true);
	if (!testEntity(true))
	{
		return false;
	}

	//Set middle component visibility to false then remove that component
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after breaking the component chain in the middle."));
	compE->SetVisibility(false);
	if (!testEntity(false))
	{
		return false;
	}

	compF->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	compD->AddComponent(compF);
	if (!testEntity(true))
	{
		return false;
	}

	//Set other Entity visibility to false, then move testComponent to that component chain to see if it inherits visibility settings.
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after moving the component to a different owner."));
	baseEntityA->SetVisibility(false);
	compA->SetVisibility(false);
	testedComponent->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	compA->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	testedComponent->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	baseEntityA->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	baseEntityA->SetVisibility(true);
	if (!testEntity(true))
	{
		return false;
	}

	//Changing sibling component visibility shouldn't affect each other's visibility
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after adjusting sibling component's visibility."));
	compB->SetVisibility(false);
	if (!testEntity(true))
	{
		return false;
	}

	//Test the case where a component attaches to an old Entity that had visibility reenabled
	baseEntityB->SetVisibility(true);
	compD->SetVisibility(true);
	testedComponent->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	compD->AddComponent(testedComponent);
	if (!testEntity(true))
	{
		return false;
	}

	testedComponent->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	//Ensure visibility flags are inherited even if the component is not attached directly to the invisible component.
	baseEntityB->SetVisibility(true);
	compD->SetVisibility(false);
	compE->SetVisibility(true);
	compF->SetVisibility(true);
	compF->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	testedComponent->DetachSelfFromOwner();
	if (!testEntity(true))
	{
		return false;
	}

	compF->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	compD->SetVisibility(true);
	if (!testEntity(true))
	{
		return false;
	}

	//Test case where testComponent is attaching directly to a singular invisible component.
	testedComponent->DetachSelfFromOwner();
	compF->SetVisibility(false);
	compF->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	cleanUpEntities();
	tester->ExecuteSuccessSequence(testFlags, TXT("Entity Visibility"));
	return true;
}

void EngineIntegrityTester::NotifyEngineComponents (EngineComponent::EEngineIntegrityStep testStep)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for (EngineComponent* engComp : localEngine->ReadEngineComponents())
	{
		engComp->ProcessEngineIntegrityTestStep(testStep);
	}
}
SD_END

#endif