/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClassIterator.cpp
=====================================================================
*/

#include "ClassIterator.h"
#include "Core.h"
#include "DClass.h"
#include "Object.h"

SD_BEGIN
ClassIterator::ClassIterator ()
{
	InitialClass = Object::SStaticClass();
	InitializeIterator();
}

ClassIterator::ClassIterator (const DClass* newInitialClass)
{
	InitialClass = newInitialClass;
	InitializeIterator();
}

ClassIterator::~ClassIterator ()
{
}

void ClassIterator::operator++ ()
{
	SelectNextClass();
}

void ClassIterator::operator++ (int)
{
	SelectNextClass();
}

const DClass* ClassIterator::GetSelectedClass () const
{
	return SelectedClass;
}

void ClassIterator::InitializeIterator ()
{
	SelectedClass = InitialClass;

	SClassIteratorInfo firstClass;

	//Insert the first child class to avoid base case on first iteration
	firstClass.ChildIndex = -1; //haven't selected this class yet
	firstClass.CurrentClass = SelectedClass;

	ParentClasses.push_back(firstClass);
}

void ClassIterator::SelectNextClass ()
{
	Int lastParentIdx = ParentClasses.size() - 1;

	//No more classes to iterate through
	if (lastParentIdx < 0)
	{
		SelectedClass = nullptr;
		return;
	}

	const DClass* curClass = ParentClasses.at(lastParentIdx.Value).CurrentClass; //only concerned with last element

	ParentClasses.at(lastParentIdx.Value).ChildIndex++;
	if (ParentClasses.at(lastParentIdx.Value).ChildIndex >= curClass->Children.size())
	{
		//already iterated through all child classes, go to next neighboring class
		ParentClasses.pop_back();
		SelectNextClass(); //try again with the next previous ParentClass
		return;
	}

	//if (curParent->Children.size() > 0)
	//child class of parent has more children
	if (curClass->Children.at(ParentClasses.at(lastParentIdx.Value).ChildIndex.Value)->Children.size() > 0)
	{
		//Insert subclass (setup vector for next iteration)
		SClassIteratorInfo newParentInfo;
		newParentInfo.ChildIndex = -1; //haven't selected this class yet
		newParentInfo.CurrentClass = curClass->Children.at(ParentClasses.at(lastParentIdx.Value).ChildIndex.Value);
		ParentClasses.push_back(newParentInfo);
	}

	SelectedClass = curClass->Children.at(ParentClasses.at(lastParentIdx.Value).ChildIndex.Value);
}
SD_END