/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineIntegrityUnitTester.cpp
=====================================================================
*/

#include "Core.h"
#include "DatatypeUnitTester.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "EngineComponent.h"
#include "EngineIntegrityTester.h"
#include "EngineIntegrityUnitTester.h"
#include "LogCategory.h"
#include "TickComponent.h"
#include "TickTester.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::EngineIntegrityUnitTester, SD::UnitTester)
SD_BEGIN

bool EngineIntegrityUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bPassed = true;

	//Smoke test
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bPassed &= TestCmdLineArgs(testFlags) && TestDClass(testFlags);
	}

	//Feature test
	if (bPassed && (testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		EngineIntegrityTester* tester = EngineIntegrityTester::CreateObject();
		if (tester == nullptr)
		{
			UnitTestError(testFlags, TXT("Failed to launch Engine Integrity Tests.  Could not instantiate test object."));
			return false;
		}

		bPassed &= (tester->TestClassIterator(this, testFlags) && tester->TestObjectInstantiation(this, testFlags) &&
				tester->TestObjectIterator(this, testFlags) &&
				tester->TestCleanUp(this, testFlags) && tester->TestComponents(this, testFlags) &&
				tester->TestComponentIterator(this, testFlags) && tester->TestEntityVisibility(this, testFlags));

		tester->Destroy();
	}

	//Async test
	if (bPassed && (testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		bPassed &= LaunchTickTester(testFlags);
	}

	return bPassed;
}

bool EngineIntegrityUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
		{
			return true;
		}
	}

	return false;
}

bool EngineIntegrityUnitTester::TestCmdLineArgs (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Command Line"));

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	DString originalCmdLine = localEngine->GetCommandLineArgs();
	DString tempCmdLine = TXT("-Switch1 -Switch2 MyInt=15 MyFloat=24.7 MyString=NoSpaces MySpaceString=\"String with spaces\" TestingQuotes=\"This -Switch should not be found.  FakeKey=However this -HiddenSwitch should be found.\" -HiddenSwitch");
	localEngine->SetCommandLineArgs(tempCmdLine);

	TestLog(testFlags, TXT("The Engine command line args reads:  \"%s\".  The command line args will restore to that value after this test."), originalCmdLine);
	TestLog(testFlags, TXT("Temporarily setting the engine command line args to:  %s"), tempCmdLine);
	DString switch1String = TXT("-Switch1");
	Bool bSwitch1 = localEngine->HasCmdLineSwitch(switch1String, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switch1String, bSwitch1);
	if (!bSwitch1)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), switch1String, tempCmdLine);
		return false;
	}

	DString switch2String = TXT("-Switch2");
	Bool bSwitch2 = localEngine->HasCmdLineSwitch(switch2String, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switch2String, bSwitch2);
	if (!bSwitch2)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), switch2String, tempCmdLine);
		return false;
	}

	DString switchString = TXT("-Switch");
	Bool bSwitch = localEngine->HasCmdLineSwitch(switchString, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switchString, bSwitch);
	if (bSwitch)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  It found \"%s\" from \"%s\""), switchString, tempCmdLine);
		return false;
	}

	DString hiddenSwitchString = TXT("-HiddenSwitch");
	Bool bHiddenSwitch = localEngine->HasCmdLineSwitch(hiddenSwitchString, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), hiddenSwitchString, bHiddenSwitch);
	if (!bHiddenSwitch)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), hiddenSwitchString, tempCmdLine);
		return false;
	}

	DString intKey = TXT("MyInt");
	DString intValueStr = localEngine->GetCmdLineValue(intKey, DString::CC_IgnoreCase);
	Int intValue(intValueStr);
	TestLog(testFlags, TXT("The raw Int value from cmd line \"%s\" is:  \"%s\".  Converting that to an Int returns %s"), tempCmdLine, intValueStr, intValue);
	if (intValueStr != TXT("15") || intValue != 15)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find the expected value \"15\" for \"%s\" within \"%s\".  Instead it returned \"%s\"."), intKey, tempCmdLine, intValueStr);
		return false;
	}

	DString floatKey = TXT("MyFloat");
	DString floatValueStr = localEngine->GetCmdLineValue(floatKey, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("The raw Float value from cmd line \"%s\" is:  \"%s\"."), tempCmdLine, floatValueStr);
	if (floatValueStr != TXT("24.7"))
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find the expected value \"24.7\" for \"%s\" within \"%s\".  Instead it returned \"%s\"."), floatKey, tempCmdLine, floatValueStr);
		return false;
	}

	DString stringKey = TXT("MyString");
	DString stringValue = localEngine->GetCmdLineValue(stringKey, DString::CC_IgnoreCase);
	DString expectedStringValue = TXT("NoSpaces");
	TestLog(testFlags, TXT("The value of \"%s\" from cmd line \"%s\" is:  \"%s\"."), stringKey, tempCmdLine, stringValue);
	if (stringValue != expectedStringValue)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  The value of \"%s\" from \"%s\" does not match the expected value \"%s\".  Instead it returned \"%s\"."), stringKey, tempCmdLine, expectedStringValue, stringValue);
		return false;
	}

	DString spaceStringKey = TXT("MySpaceString");
	DString spaceStringValue = localEngine->GetCmdLineValue(spaceStringKey, DString::CC_IgnoreCase);
	DString expectedSpaceStringValue = TXT("String with spaces");
	TestLog(testFlags, TXT("The value of \"%s\" from cmd line \"%s\" is:  \"%s\"."), spaceStringKey, tempCmdLine, spaceStringValue);
	if (spaceStringValue != expectedSpaceStringValue)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  The value of \"%s\" from \"%s\" does not match the expected value \"%s\".  Instead it returned \"%s\"."), spaceStringKey, tempCmdLine, expectedSpaceStringValue, spaceStringValue);
		return false;
	}

	DString quoteKey = TXT("-TestingQuotes");
	Bool bHasQuoteKey = localEngine->HasCmdLineSwitch(quoteKey, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("Does cmd line contain value for key \"%s\"?  %s"), quoteKey, bHasQuoteKey);
	if (bHasQuoteKey)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Found \"%s\" from \"%s\""), quoteKey, tempCmdLine);
		return false;
	}

	DString nonExistsKey = TXT("FakeKey");
	DString fakeValue = localEngine->GetCmdLineValue(nonExistsKey, DString::CC_IgnoreCase);
	if (!fakeValue.IsEmpty())
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  It found \"%s\" from \"%s\""), nonExistsKey, tempCmdLine);
		return false;
	}

	TestLog(testFlags, TXT("Restoring engine command line args to:  \"%s\""), originalCmdLine);
	localEngine->SetCommandLineArgs(originalCmdLine);

	ExecuteSuccessSequence(testFlags, TXT("Command Line"));
	return true;
}

bool EngineIntegrityUnitTester::LaunchTickTester (EUnitTestFlags testFlags) const
{
	TickTester* tickTester = TickTester::CreateObject();
	if (tickTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch Tick Tester.  Could not instantiate test object."));
		return false;
	}

	tickTester->TestFlags = testFlags;
	tickTester->OwningTester = this;
	tickTester->BeginTest();
	return true;
}

bool EngineIntegrityUnitTester::TestDClass (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("DClass"));

	SetTestCategory(testFlags, TXT("CDO Accessor"));
	{
		const Entity* entityCdo = Entity::SGetDefaultObject();
		if (entityCdo == nullptr)
		{
			UnitTestError(testFlags, TXT("DClass test failed since it was unable to obtain the Entity's default object."));
			return false;
		}

		const EntityComponent* compCdo = EntityComponent::SGetDefaultObject();
		if (compCdo == nullptr)
		{
			UnitTestError(testFlags, TXT("DClass test failed since it was unable to obtain the EntityComponent's default object."));
			return false;
		}

		const EngineIntegrityUnitTester* selfCdo = dynamic_cast<const EngineIntegrityUnitTester*>(StaticClass()->GetDefaultObject());
		if (selfCdo == nullptr)
		{
			UnitTestError(testFlags, TXT("DClass test failed since it was unable to obtain the EngineIntegrityUnitTester from an instance."));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Find DClass From String"));
	{
		std::vector<DString> testStrings(
		{
			TXT("SD::Entity"),
			TXT("SD::TickComponent"),
			TXT("SD::EngineComponent"),
			TXT("SD::ContainerUtils"),
			TXT("SD::EngineIntegrityUnitTester"),
			TXT("SD::Object")
		});

		std::vector<const DClass*> expectedClasses(
		{
			Entity::SStaticClass(),
			TickComponent::SStaticClass(),
			EngineComponent::SStaticClass(),
			ContainerUtils::SStaticClass(),
			EngineIntegrityUnitTester::SStaticClass(),
			Object::SStaticClass()
		});

		CHECK(testStrings.size() == expectedClasses.size())

		for (size_t i = 0; i < testStrings.size(); ++i)
		{
			const DClass* actual = DClass::FindDClass(testStrings.at(i));
			if (actual == nullptr)
			{
				UnitTestError(testFlags, TXT("DClass test failed since it was unable to find a DClass from the string \"%s\"."), testStrings.at(i));
				return false;
			}

			if (actual != expectedClasses.at(i))
			{
				UnitTestError(testFlags, TXT("DClass test failed since it found %s instead of %s from the string \"%s\"."), actual->GetDuneClassName(), expectedClasses.at(i)->GetDuneClassName(), testStrings.at(i));
				return false;
			}

			size_t classHash = DClassAssembler::CalcDClassHash(testStrings.at(i), OUT actual);
			if (actual != expectedClasses.at(i))
			{
				UnitTestError(testFlags, TXT("DClass test failed since it found %s instead of %s from the string \"%s\"."), actual->GetDuneClassName(), expectedClasses.at(i)->GetDuneClassName(), testStrings.at(i));
				return false;
			}

			if (classHash != expectedClasses.at(i)->GetClassHash())
			{
				UnitTestError(testFlags, TXT("DClass test failed since the class hash for %s does not match the result from DClassAssembler::CalcDClassHash. DClass hash is %s, Assembler returned hash %s."), actual->ReadDuneClassName(), DString::MakeString(actual->GetClassHash()), DString::MakeString(classHash));
				return false;
			}

			const DClass* hashedActual = DClassAssembler::FindDClass(classHash);
			if (actual != hashedActual)
			{
				UnitTestError(testFlags, TXT("DClass test failed since the hash of \"%s\" returned a different DClass. It returned %s instead of %s."), testStrings.at(i), actual->GetDuneClassName(), hashedActual->GetDuneClassName());
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Class Name without Namespaces"));
	{
		const DClass* testClass = nullptr;
		DString expected;

		std::function<bool()> testClassWithoutNamespace([&]()
		{
			DString actual = testClass->GetClassNameWithoutNamespace();
			if (actual.Compare(expected, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("DClass test failed since obtaining the class name without namespace should have returned \"%s\". Instead it returned \"%s\"."), expected, actual);
				return false;
			}

			return true;
		});

		testClass = Object::SStaticClass();
		expected = TXT("Object");
		if (!testClassWithoutNamespace())
		{
			return false;
		}

		testClass = Entity::SStaticClass();
		expected = TXT("Entity");
		if (!testClassWithoutNamespace())
		{
			return false;
		}

		testClass = EntityComponent::SStaticClass();
		expected = TXT("EntityComponent");
		if (!testClassWithoutNamespace())
		{
			return false;
		}

		testClass = EngineIntegrityUnitTester::SStaticClass();
		expected = TXT("EngineIntegrityUnitTester");
		if (!testClassWithoutNamespace())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("DClass"));
	return true;
}
SD_END

#endif