/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HashedString.cpp
=====================================================================
*/

#include "CoreMacros.h"
#include "HashedString.h"

SD_BEGIN
std::unordered_map<size_t, HashedString::SSharedData> HashedString::HashMap;
std::mutex HashedString::HashMapMutex;

HashedString::SSharedData::SSharedData (const DString& inString) :
	String(inString),
	ReferenceCounter(1)
{
	//Noop
}

HashedString::HashedString () :
	Hash(0)
{
	//Noop
}

HashedString::HashedString (const DString& inString)
{
	Hash = inString.GenerateHash();
	if (Hash != 0)
	{
		AddHash(inString);
	}
}

HashedString::HashedString (const HashedString& cpyObj) :
	Hash(cpyObj.Hash)
{
	if (Hash != 0)
	{
		const DString& string = cpyObj.ReadString();
		AddHash(string);
	}
}

HashedString::~HashedString ()
{
	RemoveHash();
}

void HashedString::ResetToDefaults ()
{
	RemoveHash();
	Hash = 0;
}

DString HashedString::ToString () const
{
	return GetString();
}

void HashedString::ParseString (const DString& str)
{
	SetString(str);
}

size_t HashedString::GetMinBytes () const
{
	return HashedString::SGetMinBytes();
}

void HashedString::Serialize (DataBuffer& outData) const
{
	const DString& str = GetString();
	outData << str;
}

bool HashedString::Deserialize (const DataBuffer& dataBuffer)
{
	DString str;
	if ((dataBuffer >> str).HasReadError())
	{
		return false;
	}

	SetString(str);

	return true;
}

void HashedString::operator= (const HashedString& cpyObj)
{
	const DString& cpyStr = cpyObj.ReadString();
	
	//Same as SetString but the Hash is already generated.
	RemoveHash();
	Hash = cpyObj.Hash;
	AddHash(cpyStr);
}

size_t HashedString::SGetMinBytes ()
{
	//Same as a string since a HashedString can be generated from a it.
	return DString::SGetMinBytes();
}

void HashedString::SetString (const DString& newString)
{
	RemoveHash();
	if (newString.IsEmpty())
	{
		Hash = 0;
		return;
	}

	Hash = newString.GenerateHash();
	AddHash(newString);
}

DString HashedString::GetString () const
{
	std::lock_guard<std::mutex> guard(HashMapMutex);
	if (HashMap.contains(Hash))
	{
		return HashMap.at(Hash).String;
	}

	return DString::EmptyString;
}

const DString& HashedString::ReadString () const
{
	if (Hash != 0)
	{
		std::lock_guard<std::mutex> guard(HashMapMutex);
		if (HashMap.contains(Hash))
		{
			return HashMap.at(Hash).String;
		}
	}

	return DString::EmptyString;
}

void HashedString::AddHash (const DString& string)
{
	CHECK(Hash != 0)

#ifdef DEBUG_MODE	
	DebugString = string;
#endif

	std::lock_guard<std::mutex> guard(HashMapMutex);
	if (HashMap.contains(Hash))
	{
		HashMap.at(Hash).ReferenceCounter++;
		return;
	}

	HashMap.insert({Hash, string});
}

void HashedString::RemoveHash ()
{
#ifdef DEBUG_MODE
	DebugString = DString::EmptyString;
#endif

	if (Hash == 0)
	{
		return;
	}

	std::lock_guard<std::mutex> guard(HashMapMutex);
	if (HashMap.contains(Hash))
	{
		HashMap.at(Hash).ReferenceCounter--;
		if (HashMap.at(Hash).ReferenceCounter <= 0)
		{
			HashMap.erase(Hash);
		}
	}
}
SD_END