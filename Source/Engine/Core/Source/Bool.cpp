/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Bool.cpp
=====================================================================
*/

#include "CoreDatatypes.h"

SD_BEGIN
Int Bool::LanguageID = INT_INDEX_NONE;
DString Bool::ToTrueText = TXT("True");
DString Bool::ToFalseText = TXT("False");
std::mutex Bool::LocalizationMutex;

Bool::Bool ()
{

}

Bool::Bool (const bool newValue)
{
	Value = newValue;
}

Bool::Bool (const Bool& copyBool)
{
	Value = copyBool.Value;
}

Bool::Bool (const Int& intValue)
{
	Value = (intValue == 0) ? false : true;
}

Bool::Bool (const Float& floatValue)
{
	Value = (floatValue == 0) ? false : true;
}

Bool::Bool (const DString& stringValue)
{
	Value = stringValue.ToBool();
}

void Bool::operator= (const Bool& copyBool)
{
	Value = copyBool.Value;
}

void Bool::operator= (const bool otherBool)
{
	Value = otherBool;
}

Bool::operator bool () const
{
	return Value;
}

bool Bool::operator ! () const
{
	return !Value;
}

void Bool::ResetToDefaults ()
{
	Value = false;
}

DString Bool::ToString () const
{
	return (Value) ? TXT("True") : TXT("False");
}

void Bool::ParseString (const DString& str)
{
	Value = (str.Compare(TXT("True"), DString::CC_IgnoreCase) == 0);
}

size_t Bool::GetMinBytes () const
{
	return Bool::SGetMinBytes();
}

void Bool::Serialize (DataBuffer& outData) const
{
	char byteToPush = (Value) ? '1' : '0';
	outData.AppendBytes(&byteToPush, 1);
}

bool Bool::Deserialize (const DataBuffer& dataBuffer)
{
	char pulledByte;
	dataBuffer.ReadBytes(&pulledByte, 1);

	Value = (pulledByte != '0');

	return true;
}

size_t Bool::SGetMinBytes ()
{
	return sizeof(Value);
}

Float Bool::ToFloat () const
{
	return (Value) ? Float(1.f) : Float(0.f);
}

Int Bool::ToInt () const
{
	return (Value) ? 1 : 0;
}

#pragma region "External Operators"
bool operator== (const Bool& left, const Bool& right)
{
	return (left.Value == right.Value);
}

bool operator== (const Bool& left, const bool& right)
{
	return (left.Value == right);
}

bool operator== (const bool& left, const Bool& right)
{
	return (left == right.Value);
}

bool operator!= (const Bool& left, const Bool& right)
{
	return (left.Value != right.Value);
}

bool operator!= (const Bool& left, const bool& right)
{
	return (left.Value != right);
}

bool operator!= (const bool& left, const Bool& right)
{
	return (left != right.Value);
}
#pragma endregion
SD_END