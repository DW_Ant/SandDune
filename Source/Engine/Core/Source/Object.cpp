/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Object.cpp
=====================================================================
*/

#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "Object.h"

IMPLEMENT_CLASS_NO_PARENT(SD::Object)
SD_BEGIN

Object::Object ()
{
	PointerReferences = nullptr;
#ifdef DEBUG_MODE
	bDestroyedIsCalled = false;
#endif
}

Object::~Object ()
{
	//Explicitly deleting an object instead of calling Destroy would cause issues when iterating through the object list.
	CHECK_INFO(NextObject == nullptr, "Object was deleted without clearing its NextObject pointer. It's possible this would break the object linked list. Make sure to call Destroy instead of explicitly deleting objects.")
}

bool Object::CanBePointed () const
{
	//Don't point at objects that are about to be deleted since DPointers are only cleared upon Destroy.
	return !bPendingDelete;
}

void Object::SetLeadingDPointer (DPointerBase* newLeadingPointer) const
{
	PointerReferences = newLeadingPointer;
}

DPointerBase* Object::GetLeadingDPointer () const
{
	return PointerReferences;
}

void Object::InitProps ()
{
#ifdef DEBUG_MODE
	DebugName = DString::EmptyString;
#endif

	bPendingDelete = false;
	ObjectHash = -1;
#ifdef DEBUG_MODE
	bRunInObjIterTest = true;
#endif

	NextObject = nullptr;
}

void Object::BeginObject ()
{
	ObjectHash = CalculateHashID();
	RegisterObject();
}

DString Object::ToString () const
{
	return GetFriendlyName();
}

bool Object::IsDefaultObject () const
{
	return (GetDefaultObject() == this);
}

void Object::Destroy ()
{
	if (CanBeDestroyed())
	{
		Destroyed();

#ifdef DEBUG_MODE
		if (!bDestroyedIsCalled)
		{
			CoreLog.Log(LogCategory::LL_Fatal, TXT("The %s Destroyed function chain failed to make it back to the root call (Object::Destroyed).  All classes are required to invoke the parent's Destroyed function so that the garbage collector may reclaim memory."), ToString());
		}
#endif
	}
}

DString Object::GetName () const
{
	return StaticClass()->GetDuneClassName();
}

DString Object::GetFriendlyName () const
{
#ifdef DEBUG_MODE
	if (!DebugName.IsEmpty())
	{
		return DebugName;
	}
#endif

	if (IsDefaultObject())
	{
		return TXT("Default_") + StaticClass()->ToString();
	}

	return StaticClass()->ToString();
}

const Object* Object::GetDefaultObject () const
{
	return StaticClass()->GetDefaultObject();
}

bool Object::GetPendingDelete () const
{
	return bPendingDelete;
}

unsigned int Object::CalculateHashID () const
{
	return Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetObjectHashNumber();
}

void Object::PostEngineInitialize () const
{
	//Do nothing
}

void Object::InitializeObject ()
{
	InitProps();

	if (!IsDefaultObject())
	{
		BeginObject();
	}
}

bool Object::CanBeDestroyed () const
{
	return !bPendingDelete;
}

void Object::Destroyed ()
{
	CHECK_INFO(!bPendingDelete, DString::CreateFormattedString(TXT("Cannot destroy object more than once.  %s is already marked for deletion."), ToString()))
	bPendingDelete = true;

#ifdef DEBUG_MODE
	bDestroyedIsCalled = true;
#endif

	//All pointers that references this object should be set to nullptrs to prevent them from dangling.
	ClearPointersPointingAtThis();
}

void Object::RegisterObject ()
{
	//Make sure this object resides in a thread that contains an Engine
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	//Add self to Engine's object list
	localEngine->RegisterObject(this);
}
SD_END