/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Rotator.cpp
=====================================================================
*/

#include "Rotator.h"
#include "TransformMatrix.h"

SD_BEGIN
const Float Rotator::DEGREE_TO_RADIAN = 0.017453293f; // pi/180
const Float Rotator::RADIAN_TO_DEGREE = 57.29577951f; // 180/pi
const Rotator Rotator::ZERO_ROTATOR = Rotator(0.f, 0.f, 0.f, RU_Degrees);
const Float Rotator::ROTATOR_UNIT = 0.00001526f;
const unsigned int Rotator::FULL_REVOLUTION = 65536;
const unsigned int Rotator::HALF_REVOLUTION = 32768;
const unsigned int Rotator::QUARTER_REVOLUTION = 16384;
const Float Rotator::FULL_REVOLUTION_Float(65536.f);
const Float Rotator::HALF_REVOLUTION_Float(32768.f);
const Float Rotator::QUARTER_REVOLUTION_Float(16384.f);

Rotator::Rotator ()
{
	//Variables are left uninitialized.
}

Rotator::Rotator (const Rotator& copyRotator) :
	Yaw(copyRotator.Yaw),
	Pitch(copyRotator.Pitch),
	Roll(copyRotator.Roll)
{
	//Noop
}

Rotator::Rotator (unsigned int inYaw, unsigned int inPitch, unsigned int inRoll) :
	Yaw(inYaw),
	Pitch(inPitch),
	Roll(inRoll)
{
	//Noop
}

Rotator::Rotator (uint64 rotations)
{
	Yaw = (rotations >> 48);
	Pitch = ((rotations & 0x0000FFFF00000000) >> 32);
	Roll = ((rotations & 0x00000000FFFF0000) >> 16);
}

Rotator::Rotator (Float inYaw, Float inPitch, Float inRoll, ERotationUnit rotationUnit)
{
	SetRotation(inYaw, inPitch, inRoll, rotationUnit);
}

Rotator::Rotator (Vector3 directionVector)
{
	if (directionVector.IsEmpty())
	{
		directionVector = Vector3(1.f, 0.f, 0.f);
	}

	directionVector.NormalizeInline();
	SetComponentsFromVector(directionVector);
}

Rotator::Rotator (const TransformMatrix& transformMatrix)
{
	*this = transformMatrix.GetRotation();
}

void Rotator::operator= (const Rotator& copyRotator)
{
	Yaw = copyRotator.Yaw;
	Pitch = copyRotator.Pitch;
	Roll = copyRotator.Roll;
}

void Rotator::operator= (Vector3 directionVector)
{
	directionVector.NormalizeInline();
	SetComponentsFromVector(directionVector);
}

void Rotator::ResetToDefaults ()
{
	Yaw = 0;
	Pitch = 0;
	Roll = 0;
}

DString Rotator::ToString () const
{
	return DString::CreateFormattedString(TXT("(Y=%s, P=%s, R=%s)"), GetYaw(RU_Degrees), GetPitch(RU_Degrees), GetRoll(RU_Degrees));
}

void Rotator::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("Y"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Rotator::ZERO_ROTATOR;
		return;
	}
	SetYaw(Float(varData), Rotator::RU_Degrees);

	varData = ParseVariable(str, TXT("P"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Rotator::ZERO_ROTATOR;
		return;
	}
	SetPitch(Float(varData), Rotator::RU_Degrees);

	varData = ParseVariable(str, TXT("R"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Rotator::ZERO_ROTATOR;
		return;
	}
	SetRoll(Float(varData), Rotator::RU_Degrees);
}

size_t Rotator::GetMinBytes () const
{
	//Yaw, Pitch, Roll
	return (Int::SGetMinBytes() * 3);
}

void Rotator::Serialize (DataBuffer& outData) const
{
	Int serializedYaw(Yaw);
	Int serializedPitch(Pitch);
	Int serializedRoll(Roll);

	outData << serializedYaw;
	outData << serializedPitch;
	outData << serializedRoll;
}

bool Rotator::Deserialize (const DataBuffer& dataBuffer)
{
	Int serializedYaw;
	Int serializedPitch;
	Int serializedRoll;

	dataBuffer >> serializedYaw >> serializedPitch >> serializedRoll;
	if (dataBuffer.HasReadError())
	{
		return false;
	}

	//Yaw, Pitch, Roll number of bytes are shorter than Ints.  Check for number overflow.
	CHECK(serializedYaw < FULL_REVOLUTION && serializedPitch < FULL_REVOLUTION && serializedRoll < FULL_REVOLUTION)

	Yaw = serializedYaw.Value;
	Pitch = serializedPitch.Value;
	Roll = serializedRoll.Value;

	return (serializedYaw < FULL_REVOLUTION && serializedPitch < FULL_REVOLUTION && serializedRoll < FULL_REVOLUTION);
}

Rotator Rotator::MakeRotator (Int inYaw, Int inPitch, Int inRoll)
{
	Int revolution(FULL_REVOLUTION);
	inYaw %= revolution;
	inPitch %= revolution;
	inRoll %= revolution;

	//Make sure the numbers are in the positive range
	if (inYaw < 0)
	{
		inYaw += revolution;
	}

	if (inPitch < 0)
	{
		inPitch += revolution;
	}

	if (inRoll < 0)
	{
		inRoll += revolution;
	}

	return Rotator(inYaw.ToUnsignedInt32(), inPitch.ToUnsignedInt32(), inRoll.ToUnsignedInt32());
}

Float Rotator::GetShorterDirectionMultiplier (unsigned int startAngle, unsigned int desiredAngle)
{
	if (startAngle == desiredAngle)
	{
		return 0.f;
	}

	//Desired is the angle relative to startAngle if startAngle moved to 0.
	Int relativeDesired = Int(desiredAngle) - Int(startAngle);
	if (relativeDesired < 0)
	{
		relativeDesired += Int(FULL_REVOLUTION);
	}

	return (relativeDesired <= Int(HALF_REVOLUTION)) ? 1.f : -1.f;
}

Float Rotator::GetShorterDirectionMultiplier (Float startAngle, Float desiredAngle, ERotationUnit unit)
{
	if (startAngle == desiredAngle)
	{
		return 0.f;
	}

	Float fullRevolution = (unit == RU_Degrees) ? 360.f : PI_FLOAT * 2.f;

	//Desired is the angle relative to startAngle if startAngle moved to 0.
	Float relativeDesired = desiredAngle - startAngle;
	if (relativeDesired < 0.f)
	{
		relativeDesired += fullRevolution;
	}

	return (relativeDesired <= (fullRevolution * 0.5f)) ? 1.f : -1.f;
}

void Rotator::SetRotation (Float inYaw, Float inPitch, Float inRoll, ERotationUnit rotationUnit)
{
	Float multiplier = GetRotatorToUnitConverter(rotationUnit);

	//Convert from user specified units to Rotator units (0-65k)
	inYaw *= multiplier;
	inPitch *= multiplier;
	inRoll *= multiplier;

	Yaw = static_cast<unsigned int>(Int(inYaw).Value);
	Pitch = static_cast<unsigned int>(Int(inPitch).Value);
	Roll = static_cast<unsigned int>(Int(inRoll).Value);
}

void Rotator::SetYaw (Float inYaw, ERotationUnit rotationUnit)
{
	inYaw *= GetRotatorToUnitConverter(rotationUnit);
	Yaw = static_cast<unsigned int>(Int(inYaw).Value);
}

void Rotator::SetPitch (Float inPitch, ERotationUnit rotationUnit)
{
	inPitch *= GetRotatorToUnitConverter(rotationUnit);
	Pitch = static_cast<unsigned int>(Int(inPitch).Value);
}

void Rotator::SetRoll (Float inRoll, ERotationUnit rotationUnit)
{
	inRoll *= GetRotatorToUnitConverter(rotationUnit);
	Roll = static_cast<unsigned int>(Int(inRoll).Value);
}

bool Rotator::IsPerpendicularTo (const Rotator& otherRotator, Float tolerance) const
{
	Vector3 a = GetDirectionalVector();
	Vector3 b = otherRotator.GetDirectionalVector();

	CHECK(a.CalcDistSquared() == 1.f && b.CalcDistSquared() == 1.f)
	return (a.Dot(b).IsCloseTo(0.f, tolerance));
}

bool Rotator::IsParallelTo (const Rotator& otherRotator, Float tolerance) const
{
	Vector3 a = GetDirectionalVector();
	Vector3 b = otherRotator.GetDirectionalVector();

	return (a.CrossProduct(b).CalcDistSquared().IsCloseTo(0.f, tolerance));
}

bool Rotator::IsNearlyEqual (const Rotator& otherRotator, Float tolerance) const
{
	Float yaw(Yaw);
	Float pitch(Pitch);
	Float roll(Roll);
	Float otherYaw(otherRotator.Yaw);
	Float otherPitch(otherRotator.Pitch);
	Float otherRoll(otherRotator.Roll);

	Float scaledTolerance = tolerance / ROTATOR_UNIT;
	return (Float::Abs(yaw - otherYaw) <= scaledTolerance && Float::Abs(pitch - otherPitch) <= scaledTolerance && Float::Abs(roll - otherRoll) <= scaledTolerance);
}

void Rotator::Invert ()
{
	unsigned int originalRoll = Roll;

	Vector3 directionalVector = GetDirectionalVector();
	directionalVector *= -1.f;
	SetComponentsFromVector(directionalVector);
	Roll = originalRoll;
}

uint64 Rotator::ToInt () const
{
	uint64 largeYaw = static_cast<uint64>(Yaw);
	uint64 largePitch = static_cast<uint64>(Pitch);
	uint64 largeRoll = static_cast<uint64>(Roll);

	uint64 result = (largeYaw << 48);
	result |= (largePitch << 32);
	result |= (largeRoll << 16);

	return result;
}

void Rotator::AddYaw (Float addAmount)
{
	Float floatYaw = Float::MakeFloat(Yaw);
	floatYaw += addAmount;
	while (floatYaw < 0.f)
	{
		floatYaw += FULL_REVOLUTION_Float;
	}

	Yaw = floatYaw.ToInt().ToUnsignedInt32();
}

void Rotator::AddPitch (Float addAmount)
{
	Float floatPitch = Float::MakeFloat(Pitch);
	floatPitch += addAmount;
	while (floatPitch < 0.f)
	{
		floatPitch += FULL_REVOLUTION_Float;
	}

	Pitch = floatPitch.ToInt().ToUnsignedInt32();
}

void Rotator::AddRoll (Float addAmount)
{
	Float floatRoll = Float::MakeFloat(Roll);
	floatRoll += addAmount;
	while (floatRoll < 0.f)
	{
		floatRoll += FULL_REVOLUTION_Float;
	}

	Roll = floatRoll.ToInt().ToUnsignedInt32();
}

Vector3 Rotator::GetDirectionalVector () const
{
	Float radianYaw;
	Float radianPitch;
	Float radianRoll;
	GetRadians(OUT radianYaw, OUT radianPitch, OUT radianRoll);

	Vector3 result;
	result.X = cos(radianYaw.Value) * cos(radianPitch.Value);
	result.Y = -sin(radianYaw.Value) * cos(radianPitch.Value);
	result.Z = sin(radianPitch.Value);

	CHECK(result.CalcDistSquared() == 1.f)

	return result;
}

void Rotator::GetDegrees (Float& outYaw, Float& outPitch, Float& outRoll) const
{
	Float multiplier = GetUnitToRotatorConverter(RU_Degrees);

	outYaw = Float::MakeFloat(Yaw) * multiplier;
	outPitch = Float::MakeFloat(Pitch) * multiplier;
	outRoll = Float::MakeFloat(Roll) * multiplier;
}

void Rotator::GetRadians (Float& outYaw, Float& outPitch, Float& outRoll) const
{
	Float multiplier = GetUnitToRotatorConverter(RU_Radians);

	outYaw = Float::MakeFloat(Yaw) * multiplier;
	outPitch = Float::MakeFloat(Pitch) * multiplier;
	outRoll = Float::MakeFloat(Roll) * multiplier;
}

Float Rotator::GetYaw (ERotationUnit rotationUnit) const
{
	return Float::MakeFloat(Yaw) * GetUnitToRotatorConverter(rotationUnit);
}

Float Rotator::GetPitch (ERotationUnit rotationUnit) const
{
	return Float::MakeFloat(Pitch) * GetUnitToRotatorConverter(rotationUnit);
}

Float Rotator::GetRoll (ERotationUnit rotationUnit) const
{
	return Float::MakeFloat(Roll) * GetUnitToRotatorConverter(rotationUnit);
}

Float Rotator::GetRotatorToUnitConverter (ERotationUnit rotationUnit) const
{
	switch (rotationUnit)
	{
		case (RU_Degrees) : return (static_cast<float>(FULL_REVOLUTION) / 360.f);
		case (RU_Radians) : return (static_cast<float>(FULL_REVOLUTION) / (PI_FLOAT * 2.f));
	}

	return 1.f;
}

Float Rotator::GetUnitToRotatorConverter (ERotationUnit rotationUnit) const
{
	switch (rotationUnit)
	{
		case (RU_Degrees) : return (360.f / static_cast<float>(FULL_REVOLUTION));
		case (RU_Radians) : return ((PI_FLOAT * 2.f) / static_cast<float>(FULL_REVOLUTION));
	}

	return 1.f;
}

void Rotator::SetComponentsFromVector (const Vector3& normalizedVector)
{
	Roll = 0; //Roll is always zero when pulling from a directional vector.

	if (normalizedVector.IsEmpty())
	{
		Yaw = 0;
		Pitch = 0;
		return;
	}

	CHECK(normalizedVector.CalcDistSquared() == 1.f)

	//Handle edge cases where the ratio would end up on an asymptote
	if (normalizedVector.X == 0.f)
	{
		//Handle case where the vector is pointing straight up or down.
		if (normalizedVector.Y == 0.f)
		{
			Yaw = 0;
			//Either +90 degrees or -90 degrees. Using left handed cordinate system, it must rotate clockwise about the Y-axis.
			Pitch = (normalizedVector.Z > 0.f) ? QUARTER_REVOLUTION : QUARTER_REVOLUTION * 3;
			return;
		}

		//Yaw is either going to be 90 or -90 depending on which direction the vector faces along the YZ-plane.
		Yaw = (normalizedVector.Y < 0.f) ? QUARTER_REVOLUTION : QUARTER_REVOLUTION * 3;
	}
	else
	{
		float yawRadian = atan2f(-normalizedVector.Y.Value, normalizedVector.X.Value);
		Yaw = Int(Float(yawRadian) * GetRotatorToUnitConverter(RU_Radians)).Value;
	}

	float pitchRadian = asin(normalizedVector.Z.Value);

	Pitch = Int(Float(pitchRadian) * GetRotatorToUnitConverter(RU_Radians)).Value;
}

#pragma region "External Operators"
bool operator== (const Rotator& left, const Rotator& right)
{
	return (left.Yaw == right.Yaw &&
		left.Pitch == right.Pitch &&
		left.Roll == right.Roll);
}

bool operator!= (const Rotator& left, const Rotator& right)
{
	return !(left == right);
}

Rotator operator+ (const Rotator& left, const Rotator& right)
{
	return Rotator(left.Yaw + right.Yaw, left.Pitch + right.Pitch, left.Roll + right.Roll);
}

Rotator& operator+= (Rotator& left, const Rotator& right)
{
	left.Yaw += right.Yaw;
	left.Pitch += right.Pitch;
	left.Roll += right.Roll;

	return left;
}

Rotator operator- (const Rotator& left, const Rotator& right)
{
	return Rotator(left.Yaw - right.Yaw, left.Pitch - right.Pitch, left.Roll - right.Roll);
}

Rotator& operator-= (Rotator& left, const Rotator& right)
{
	left.Yaw -= right.Yaw;
	left.Pitch -= right.Pitch;
	left.Roll -= right.Roll;

	return left;
}
#pragma endregion
SD_END