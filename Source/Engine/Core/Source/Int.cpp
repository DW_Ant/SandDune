/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Int.cpp
=====================================================================
*/

#include "CoreDatatypes.h"

SD_BEGIN
Int::Int ()
{

}

Int::Int (const int32 newValue)
{
	Value = newValue;
}

Int::Int (const Int& copyInt)
{
	Value = copyInt.Value;
}

Int::Int (const uint32& newValue)
{
	Value = GetInt(newValue);
}

Int::Int (const Float& newValue)
{
	Value = newValue.ToInt().Value;
}

Int::Int (const DString& text)
{
	Value = text.Atoi().Value;
}

#ifdef PLATFORM_64BIT
Int::Int (const int64 newValue)
{
	Value = newValue;
}

Int::Int (const uint64 newValue)
{
	Value = GetInt(newValue);
}
#endif

void Int::operator= (const Int& copyInt)
{
	Value = copyInt.Value;
}

void Int::operator= (const int32 otherInt)
{
	Value = otherInt;
}

void Int::operator= (const uint32 otherInt)
{
	Value = GetInt(otherInt);
}

#ifdef PLATFORM_64BIT
void Int::operator= (const int64 otherInt)
{
	Value = otherInt;
}

void Int::operator= (const uint64 otherInt)
{
	Value = GetInt(otherInt);
}
#endif

Int Int::operator++ ()
{
	return ++Value;
}

Int Int::operator++ (int)
{
	return Value++;
}

Int Int::operator- () const
{
	return -Value;
}

Int Int::operator-- ()
{
	return --Value;
}

Int Int::operator-- (int)
{
	return Value--;
}

Int Int::operator~ () const
{
	return ~Value;
}

void Int::ResetToDefaults ()
{
	Value = 0;
}

DString Int::ToString () const
{
	return DString::MakeString(Value);
}

void Int::ParseString (const DString& str)
{
	Value = str.Atoi().Value;
}

size_t Int::GetMinBytes () const
{
	return Int::SGetMinBytes();
}

void Int::Serialize (DataBuffer& outData) const
{
	/*
	Ints are compressed to a size 8-72 bits depending on the size of its numerical value.
	It comes with a leading byte that indicates how many bits can this number fit followed by the actual number.
	The leading bit also starts with a 1 for negative numbers.
	Some examples:
	Number	|	[leading byte]	binary
	0 |			[0000 0000]		
	1 |			[0000 0001]		1
	2 |			[0000 0010]		10
	3 |			[0000 0010]		11
	4 |			[0000 0011]		100
	5 |			[0000 0011]		101
	6 |			[0000 0011]		110
	7 |			[0000 0011]		111
	8 |			[0000 0100]		1000
	150 |		[0000 1000]		1001 0110
	-3 |		[1000 0010]		11
	-5000 |		[1000 1101]		1001 1100 0100 0

	NOTE: data buffers' smallest unit is a byte instead of a bit. The trailing bits are padded to fill the rest of the byte.
	For example: 2 would be: [0000 0010] 1000 0000 (the last six padded bits are ignored since the leading byte indicates that only the first 2 bits are important.)
	*/

	char leadingByte = 0;
	if (Value == 0)
	{
		//Simple case, write 8 zero bits and nothing more
		outData.AppendBytes(&leadingByte, 1);
		return;
	}

	INT_TYPE posValue(Value);
	if (posValue < 0)
	{
		//Flip the first bit to 1 for negative numbers.
		leadingByte |= (1 << 7);
		posValue = std::abs(posValue);
	}

	//Calculate the number of bits. Formula: floor(log2(x))+1
	float floatBits = std::log2f(static_cast<float>(posValue));
	floatBits = std::floorf(floatBits) + 1.f;

	leadingByte |= static_cast<size_t>(floatBits);

	size_t numBytes = static_cast<size_t>(std::ceilf(floatBits/8.f));
	char* charArray = new char[numBytes];
	memcpy(charArray, &posValue, numBytes);

	//Reverse bytes if needed
	if (outData.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(&leadingByte, 1);
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	outData.AppendBytes(&leadingByte, 1);
	outData.AppendBytes(charArray, numBytes);

	delete[] charArray;
}

bool Int::Deserialize (const DataBuffer& dataBuffer)
{
	char leadingByte = 0;
	dataBuffer.ReadBytes(&leadingByte, 1);

	bool bSwapByteOrder = dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian();
	if (bSwapByteOrder)
	{
		DataBuffer::SwapByteOrder(&leadingByte, 1);
	}

	bool bIsNegative = ((leadingByte & (1 << 7)) > 0);

	//zero out the first bit to not influence the num bytes
	leadingByte &= ~(1 << 7);
	if (leadingByte == 0)
	{
		//Nothing else to read
		Value = 0;
		return true;
	}

	size_t numBytes = static_cast<size_t>(std::ceilf((static_cast<float>(leadingByte) / 8.f)));
	if (!dataBuffer.CanReadBytes(numBytes))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to read Int from data buffer since there aren't enough bytes to read from the buffer. Expecting %s bytes."), Int(numBytes));
		return false;
	}

	char* rawData = new char[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);
	if (bSwapByteOrder)
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	Value = 0; //Zero out everything. Because of numBytes, the memcpy probably wont touch every byte in Value.
	memcpy(&Value, rawData, numBytes);

	if (bIsNegative)
	{
		Value *= -1;
	}

	delete[] rawData;
	return true;
}

Int Int::MakeInt (float value)
{
	return Int(Float(value).ToInt());
}

size_t Int::SGetMinBytes ()
{
	//Smallest case is 0. 0 only contains 8 bits (the leading byte) and no data afterwards.
	return 1;
}

Float Int::ToFloat () const
{
	return Float(static_cast<float>(Value));
}

int32 Int::ToInt32 () const
{
#ifdef PLATFORM_64BIT
	return static_cast<int32>(Value);
#else
	return Value;
#endif
}

Int Int::Abs (Int value)
{
	return abs(value.Value);
}

void Int::AbsInline ()
{
	Value = std::abs(Value);
}

int32 Int::GetInt (uint32 target)
{
	if (target > SD_MAXINT)
	{
		return std::numeric_limits<int>::max();
	}
	else
	{
		return static_cast<int32>(target);
	}
}

#ifdef PLATFORM_64BIT
int64 Int::GetInt (uint64 target)
{
	if (target > SD_MAXINT)
	{
		return SD_MAXINT;
	}
	else
	{
		return static_cast<int64>(target);
	}
}
#endif

uint32 Int::ToUnsignedInt32 (bool bClamp) const
{
	if (bClamp && Value < 0)
	{
		//Set the value to 0 so that the signed bit will not convert this negative number to a large unsigned int
		return 0;
	}

	return static_cast<uint32>(Value);
}

#ifdef PLATFORM_64BIT
uint64 Int::ToUnsignedInt64 (bool bClamp) const
{
	if (bClamp && Value < 0)
	{
		//Set the value to 0 so that the signed bit will not convert this negative number to a large unsigned int
		return 0;
	}

	return static_cast<uint64>(Value);
}
#endif

UINT_TYPE Int::ToUnsignedInt (bool bClamp) const
{
#ifdef PLATFORM_64BIT
	return ToUnsignedInt64(bClamp);
#else
	return ToUnsignedInt32(bClamp);
#endif
}

Int Int::NumDigits () const
{
	return (Value != 0) ? static_cast<int>(log10((double)abs(Value))) + 1 : 1;
}

DString Int::ToFormattedString (const Int minDigits) const
{
	Int curDigits = NumDigits();
	Int numZeroesNeeded = minDigits - curDigits;
	if (numZeroesNeeded > 0)
	{
		DString prefix = (Value >= 0) ? DString::EmptyString : TXT("-");
		return prefix + DString(TString(numZeroesNeeded.Value, '0')) + Abs(*this).ToString();
	}

	return ToString();
}

#pragma region "External Operators"
bool operator== (const Int& left, const Int& right)
{
	return (left.Value == right.Value);
}

bool operator== (const Int& left, const INT_TYPE& right)
{
	return (left.Value == right);
}

bool operator== (const INT_TYPE& left, const Int& right)
{
	return (left == right.Value);
}

bool operator!= (const Int& left, const Int& right)
{
	return (left.Value != right.Value);
}

bool operator!= (const Int& left, const INT_TYPE& right)
{
	return (left.Value != right);
}

bool operator!= (const INT_TYPE& left, const Int& right)
{
	return (left != right.Value);
}

bool operator< (const Int& left, const Int& right)
{
	return (left.Value < right.Value);
}

bool operator< (const Int& left, const INT_TYPE& right)
{
	return (left.Value < right);
}

bool operator< (const INT_TYPE& left, const Int& right)
{
	return (left < right.Value);
}

bool operator<= (const Int& left, const Int& right)
{
	return (left.Value <= right.Value);
}

bool operator<= (const Int& left, const INT_TYPE& right)
{
	return (left.Value <= right);
}

bool operator<= (const INT_TYPE& left, const Int& right)
{
	return (left <= right.Value);
}

bool operator> (const Int& left, const Int& right)
{
	return (left.Value > right.Value);
}

bool operator> (const Int& left, const INT_TYPE& right)
{
	return (left.Value > right);
}

bool operator> (const INT_TYPE& left, const Int& right)
{
	return (left > right.Value);
}

bool operator>= (const Int& left, const Int& right)
{
	return (left.Value >= right.Value);
}

bool operator>= (const Int& left, const INT_TYPE& right)
{
	return (left.Value >= right);
}

bool operator>= (const INT_TYPE& left, const Int& right)
{
	return (left >= right.Value);
}

Int operator>> (const Int& left, const Int& right)
{
	return Int(left.Value >> right.Value);
}

Int operator>> (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value >> right);
}

Int operator>> (const INT_TYPE& left, const Int& right)
{
	return Int(left >> right.Value);
}

Int& operator>>= (Int& left, const Int& right)
{
	left.Value >>= right.Value;
	return left;
}

Int& operator>>= (Int& left, const INT_TYPE& right)
{
	left.Value >>= right;
	return left;
}

INT_TYPE& operator>>= (INT_TYPE& left, const Int& right)
{
	left >>= right.Value;
	return left;
}

Int operator<< (const Int& left, const Int& right)
{
	return Int(left.Value << right.Value);
}

Int operator<< (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value << right);
}

Int operator<< (const INT_TYPE& left, const Int& right)
{
	return Int(left << right.Value);
}

Int& operator<<= (Int& left, const Int& right)
{
	left.Value <<= right.Value;
	return left;
}

Int& operator<<= (Int& left, const INT_TYPE& right)
{
	left.Value <<= right;
	return left;
}

INT_TYPE& operator<<= (INT_TYPE& left, const Int& right)
{
	left <<= right.Value;
	return left;
}

Int operator& (const Int& left, const Int& right)
{
	return Int(left.Value & right.Value);
}

Int operator& (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value & right);
}

Int operator& (const INT_TYPE& left, const Int& right)
{
	return Int(left & right.Value);
}

Int operator^ (const Int& left, const Int& right)
{
	return Int(left.Value ^ right.Value);
}

Int operator^ (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value ^ right);
}

Int operator^ (const INT_TYPE& left, const Int& right)
{
	return Int(left ^ right.Value);
}

Int operator| (const Int& left, const Int& right)
{
	return Int(left.Value | right.Value);
}

Int operator| (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value | right);
}

Int operator| (const INT_TYPE& left, const Int& right)
{
	return Int(left | right.Value);
}

Int& operator&= (Int& left, const Int& right)
{
	left.Value &= right;
	return left;
}

Int& operator&= (Int& left, const INT_TYPE& right)
{
	left.Value &= right;
	return left;
}

INT_TYPE& operator&= (INT_TYPE& left, const Int& right)
{
	left &= right.Value;
	return left;
}

Int& operator^= (Int& left, const Int& right)
{
	left.Value ^= right.Value;
	return left;
}

Int& operator^= (Int& left, const INT_TYPE& right)
{
	left.Value ^= right;
	return left;
}

INT_TYPE& operator^= (INT_TYPE& left, const Int& right)
{
	left ^= right.Value;
	return left;
}

Int& operator|= (Int& left, const Int& right)
{
	left.Value |= right.Value;
	return left;
}

Int& operator|= (Int& left, const INT_TYPE& right)
{
	left.Value |= right;
	return left;
}

INT_TYPE& operator|= (INT_TYPE& left, const Int& right)
{
	left |= right.Value;
	return left;
}

Int operator+ (const Int& left, const Int& right)
{
	return Int(left.Value + right.Value);
}

Int operator+ (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value + right);
}

Int operator+ (const INT_TYPE& left, const Int& right)
{
	return Int(left + right.Value);
}

Int& operator+= (Int& left, const Int& right)
{
	left.Value += right.Value;
	return left;
}

Int& operator+= (Int& left, const INT_TYPE& right)
{
	left.Value += right;
	return left;
}

INT_TYPE& operator+= (INT_TYPE& left, const Int& right)
{
	left += right.Value;
	return left;
}

Int operator- (const Int& left, const Int& right)
{
	return Int(left.Value - right.Value);
}

Int operator- (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value - right);
}

Int operator- (const INT_TYPE& left, const Int& right)
{
	return Int(left - right.Value);
}

Int& operator-= (Int& left, const Int& right)
{
	left.Value -= right.Value;
	return left;
}

Int& operator-= (Int& left, const INT_TYPE& right)
{
	left.Value -= right;
	return left;
}

INT_TYPE& operator-= (INT_TYPE& left, const Int& right)
{
	left -= right.Value;
	return left;
}

Int operator* (const Int& left, const Int& right)
{
	return Int(left.Value * right.Value);
}

Int operator* (const Int& left, const INT_TYPE& right)
{
	return Int(left.Value * right);
}

Int operator* (const INT_TYPE& left, const Int& right)
{
	return Int(left * right.Value);
}

Int& operator*= (Int& left, const Int& right)
{
	left.Value *= right.Value;
	return left;
}

Int& operator*= (Int& left, const INT_TYPE& right)
{
	left.Value *= right;
	return left;
}

INT_TYPE& operator*= (INT_TYPE& left, const Int& right)
{
	left *= right.Value;
	return left;
}

Int operator/ (const Int& left, const Int& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int / Int)"), left);
		return left;
	}
#endif

	return (right.Value != 0) ? Int(left.Value / right.Value) : left;
}

Int operator/ (const Int& left, const INT_TYPE& right)
{
#ifdef DEBUG_MODE
	if (right == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int / Int)"), left);
		return left;
	}
#endif

	return (right != 0) ? Int(left.Value / right) : left;
}

Int operator/ (const INT_TYPE& left, const Int& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int / Int)"), Int(left));
		return Int(left);
	}
#endif

	return (right.Value != 0) ? Int(left / right.Value) : Int(left);
}

Int& operator/= (Int& left, const Int& right)
{
	if (right.Value != 0)
	{
		left.Value /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int /= Int)"), left);
	}
#endif

	return left;
}

Int& operator/= (Int& left, const INT_TYPE& right)
{
	if (right != 0)
	{
		left.Value /= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int /= Int)"), left);
	}
#endif

	return left;
}

INT_TYPE& operator/= (INT_TYPE& left, const Int& right)
{
	if (right.Value != 0)
	{
		left /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Int /= Int)"), Int(left));
	}
#endif

	return left;
}

Int operator% (const Int& left, const Int& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0) ? Int(left.Value % right.Value) : left;
}

Int operator% (const Int& left, const INT_TYPE& right)
{
#ifdef DEBUG_MODE
	if (right == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0) ? Int(left.Value % right) : left;
}

Int operator% (const INT_TYPE& left, const Int& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), Int(left));
		return left;
	}
#endif

	return (right.Value != 0) ? Int(left % right.Value) : Int(left);
}

Int& operator%= (Int& left, const Int& right)
{
	if (right.Value != 0)
	{
		left.Value %= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif

	return left;
}

Int& operator%= (Int& left, const INT_TYPE& right)
{
	if (right != 0)
	{
		left.Value %= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif

	return left;
}

INT_TYPE& operator%= (INT_TYPE& left, const Int& right)
{
	if (right.Value != 0)
	{
		left %= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), Int(left));
	}
#endif

	return left;
}

#pragma endregion
SD_END