/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ComponentIterator.cpp
=====================================================================
*/

#include "ComponentIterator.h"
#include "Core.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "LogCategory.h"

SD_BEGIN
ComponentIterator::ComponentIterator ()
{
	bRecursive = true;
	bSkipCurSubComps = false;
}

ComponentIterator::ComponentIterator (const Entity* inBaseEntity, bool bInRecursive)
{
	bRecursive = bInRecursive;
	if (inBaseEntity->Components.size() > 0)
	{
		ComponentChain.push_back(SComponentIndex(inBaseEntity));
	}

	bSkipCurSubComps = false;
}

ComponentIterator::~ComponentIterator ()
{
	//Noop
}

void ComponentIterator::operator++ ()
{
	FindNextComponent();
}

void ComponentIterator::operator++ (int)
{
	FindNextComponent();
}

void ComponentIterator::SetBaseEntity (const Entity* newBaseEntity)
{
	ContainerUtils::Empty(ComponentChain);
	if (newBaseEntity->Components.size() > 0)
	{
		ComponentChain.push_back(SComponentIndex(newBaseEntity));
	}
}

void ComponentIterator::SkipSubComponents ()
{
	if (!bRecursive)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot skip sub components since the component iterator is not recursive. It doesn't iterate through sub components anyways."));
		return;
	}

	bSkipCurSubComps = true;
}

const Entity* ComponentIterator::GetBaseEntity () const
{
	if (ComponentChain.size() > 0)
	{
		return ComponentChain.at(0).TargetEntity;
	}

	return nullptr;
}

EntityComponent* ComponentIterator::GetSelectedComponent () const
{
	const SComponentIndex* last = (ComponentChain.size() > 0) ? &ComponentChain.at(ComponentChain.size() - 1) : nullptr;
	if (last != nullptr)
	{
		CHECK(last->ComponentIdx < last->TargetEntity->Components.size())
		return last->TargetEntity->Components.at(last->ComponentIdx);
	}

	return nullptr;
}

void ComponentIterator::FindNextComponent ()
{
	if (ComponentChain.size() <= 0)
	{
		return; //iterator already ended
	}

	SComponentIndex* last = &ComponentChain.at(ComponentChain.size() - 1);
	if (bRecursive && !bSkipCurSubComps && last->TargetEntity->Components.at(last->ComponentIdx)->Components.size() > 0) //Step into sub components
	{
		//The subcomponent has its own sub components.  Add to component chain to iterate through sub sub components.
		ComponentChain.push_back(SComponentIndex(last->TargetEntity->Components.at(last->ComponentIdx)));
		return;
	}
	bSkipCurSubComps = false;

	while (last != nullptr)
	{
		if (++last->ComponentIdx < last->TargetEntity->Components.size()) //Jump to sibling component
		{
			break;
		}

		//The component index is beyond the component list.  Pop the stack and increment the parent index
		ComponentChain.pop_back();

		//Jump to parent Entity and repeat the loop to increment its component index
		last = (ComponentChain.size() > 0) ? &ComponentChain.at(ComponentChain.size() - 1) : nullptr;
	}
}
SD_END