/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerTester.cpp
=====================================================================
*/

#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "DPointerTester.h"
#include "LogCategory.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::DPointerTester, SD::Entity)
SD_BEGIN

void DPointerTester::InitProps ()
{
	Super::InitProps();

	OtherTester = nullptr;
}

bool PrimitivePointerTester::CanBePointed () const
{
	return true;
}

void PrimitivePointerTester::SetLeadingDPointer (DPointerBase* newLeadingPointer) const
{
	LeadingPointer = newLeadingPointer;
}

DPointerBase* PrimitivePointerTester::GetLeadingDPointer () const
{
	return LeadingPointer;
}

PrimitivePointerTester::PrimitivePointerTester ()
{
	OtherTester = nullptr;
	LeadingPointer = nullptr;
}

PrimitivePointerTester::PrimitivePointerTester (const PrimitivePointerTester& other)
{
	OtherTester = other.OtherTester;
	LeadingPointer = nullptr;
}

PrimitivePointerTester::~PrimitivePointerTester ()
{
	ClearPointersPointingAtThis();
}

bool PrimitivePointerTester::operator== (const PrimitivePointerTester& other) const
{
	return (OtherTester == other.OtherTester);
}

bool PrimitivePointerTester::operator!= (const PrimitivePointerTester& other) const
{
	return (OtherTester != other.OtherTester);
}

DString PrimitivePointerTester::ToString () const
{
	return OtherTester->ToString();
}
SD_END

#endif