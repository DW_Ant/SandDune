/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SDFunctionTester.cpp
=====================================================================
*/

#include "Core.h"
#include "DatatypeUnitTester.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "Object.h"
#include "SDFunctionTester.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::SDFunctionTester, SD::Object)
SD_BEGIN

void SDFunctionTester::InitProps ()
{
	Super::InitProps();

	bTestedVoid = false;
	bTestedParamINT = false;
	bTestedMultiParam = false;
	bTestedReturn = false;
	bTestedReturnMultiParam = false;
	bTestedReturnOutParam = false;
	FunctionCounter = 0;

	MulticastTestCounter = 0;
	for (char i = 0; i < 4; ++i)
	{
		MulticastTestHandlerCalled[i] = false;
	}
	MulticastAddFunctionCalled = false;
	MulticastRemoveFunctionCalled = false;

	DatatypeTester = nullptr;
	AddFunctionCanFail = true;
	RemoveFunctionCanFail = true;
}

bool SDFunctionTester::RunTest (const DatatypeUnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	DatatypeTester = tester;
	TestFlags = testFlags;

	CHECK(DatatypeTester != nullptr)

	bool passed = true;
	if ((testFlags | UnitTester::UTF_SmokeTest) > 0 && (testFlags | UnitTester::UTF_Automatic) > 0 &&
		(testFlags | UnitTester::UTF_CanDetectErrors) > 0 && (testFlags | UnitTester::UTF_Synchronous) > 0)
	{
		passed = RunFunctionTest(testFlags) && RunMulticastTest(testFlags);
	}

	return passed;
}

bool SDFunctionTester::RunFunctionTest (UnitTester::EUnitTestFlags testFlags)
{
	DatatypeTester->SetTestCategory(TestFlags, TXT("SDFunctionTester"));

	//reset member variables
	bTestedVoid = false;
	bTestedParamINT = false;
	bTestedMultiParam = false;
	bTestedReturn = false;
	bTestedReturnMultiParam = false;
	bTestedReturnOutParam = false;
	FunctionCounter = 0;
	DString className = DString::EmptyString;
	DString handlerName = DString::EmptyString;

	SDFunction<void> voidFunction = SDFunction<void>(std::bind(&SDFunctionTester::HandleVoidFunction, this), this, TXT("SDFunctionTester::HandleVoidFunction"));
	if (!voidFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), voidFunction);
		return false;
	}

	className = voidFunction.GetEventHandlerName();
	if (className != TXT("SDFunctionTester"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve event handler name from SDFunction.  It returned:  \"%s\""), className);
		return false;
	}
	handlerName = voidFunction.GetFunctionName();
	if (handlerName != TXT("HandleVoidFunction"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve function name from SDFunction.  It returned:  \"%s\""), handlerName);
		return false;
	}

	voidFunction();
	if (!bTestedVoid)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s was not called."), voidFunction);
		return false;
	}

	SDFunction<void, int> paramFunction = SDFunction<void, int>(std::bind(&SDFunctionTester::HandleParamFunction, this, std::placeholders::_1), this, TXT("SDFunctionTester::HandleParamFunction"));
	if (!paramFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), paramFunction);
		return false;
	}

	className = paramFunction.GetEventHandlerName();
	if (className != TXT("SDFunctionTester"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve event handler name from SDFunction.  It returned:  \"%s\""), className);
		return false;
	}
	handlerName = paramFunction.GetFunctionName();
	if (handlerName != TXT("HandleParamFunction"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve function name from SDFunction.  It returned:  \"%s\""), handlerName);
		return false;
	}

	paramFunction(5);
	if (!bTestedParamINT)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either %s was not called, or the event handler did not receive the parameter it was expecting."), paramFunction);
		return false;
	}

	SDFunction<void, Int, Float, DString, unsigned int> multiParamFunction = SDFUNCTION_4PARAM(this, SDFunctionTester, HandleMultiParamFunction, void, Int, Float, DString, unsigned int);
	if (!multiParamFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), multiParamFunction);
		return false;
	}

	className = multiParamFunction.GetEventHandlerName();
	if (className != TXT("SDFunctionTester"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve event handler name from SDFunction.  It returned:  \"%s\""), className);
		return false;
	}
	handlerName = multiParamFunction.GetFunctionName();
	if (handlerName != TXT("HandleMultiParamFunction"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve function name from SDFunction.  It returned:  \"%s\""), handlerName);
		return false;
	}

	multiParamFunction(5, 5.f, TXT("Five"), 5);
	if (!bTestedMultiParam)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either %s was not called, or the event handler did not receive the parameters it was expecting."), multiParamFunction);
		return false;
	}

	SDFunction<Float> returnFunction = SDFUNCTION(this, SDFunctionTester, HandleReturnFunction, Float);
	if (!returnFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), returnFunction);
		return false;
	}

	className = returnFunction.GetEventHandlerName();
	if (className != TXT("SDFunctionTester"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve event handler name from SDFunction.  It returned:  \"%s\""), className);
		return false;
	}
	handlerName = returnFunction.GetFunctionName();
	if (handlerName != TXT("HandleReturnFunction"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to retrieve function name from SDFunction.  It returned:  \"%s\""), handlerName);
		return false;
	}

	Float returnValue = returnFunction();
	if (returnValue != 5.f)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is 5.f.  Instead it returned %s"), returnFunction, returnValue);
		return false;
	}

	if (!bTestedReturn)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s was not called."), returnFunction);
		return false;
	}

	SDFunction<Bool, Int, Object*, std::string> returnMultiParamFunction = SDFUNCTION_3PARAM(this, SDFunctionTester, HandleReturnMultiParamFunction, Bool, Int, Object*, std::string);
	if (!returnMultiParamFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), returnMultiParamFunction);
		return false;
	}

	Bool otherReturnValue = returnMultiParamFunction(5, this, TXT("Five"));
	if (!otherReturnValue)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is true.  Instead it returned %s"), returnMultiParamFunction, otherReturnValue);
		return false;
	}

	if (!bTestedReturnMultiParam)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either the %s was not called, or the event handler did not receive the function parameters it was expecting."), returnMultiParamFunction);
		return false;
	}

	SDFunction<Int, Int, Int&> returnOutParamFunction = SDFUNCTION_2PARAM(this, SDFunctionTester, HandleReturnOutParamFunction, Int, Int, Int&);
	if (!returnOutParamFunction.IsBounded())
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit testered failed.  Failed to bind %s to SDFunction"), returnOutParamFunction);
		return false;
	}

	Int outParam;
	Int returnOutValue = returnOutParamFunction(5, outParam);
	if (returnOutValue != 5)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is 5.  Instead it returned %s"), returnOutParamFunction, returnOutValue);
		return false;
	}

	if (outParam != 5)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s is expected to set second parameter to 5.  Instead the second parameter is currently %s"), returnOutParamFunction, outParam);
		return false;
	}

	if (!bTestedReturnOutParam)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either the %s was not called, or the event handler did not receive the function parameters it was expecting."), returnOutParamFunction);
		return false;
	}

	if (FunctionCounter != 6)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The total number of events invoked should have been 6.  Instead %s event handlers were invoked."), FunctionCounter);
		return false;
	}

	DatatypeTester->CompleteTestCategory(testFlags);

	return true;
}

bool SDFunctionTester::RunMulticastTest (UnitTester::EUnitTestFlags testFlags)
{
	const char numHandlerFlags = 4;
	std::function<void()> resetMulticastVars([&]()
	{
		MulticastTestCounter = 0;
		for (char i = 0; i < numHandlerFlags; ++i)
		{
			MulticastTestHandlerCalled[i] = false;
		}

		MulticastAddFunctionCalled = false;
		MulticastRemoveFunctionCalled = false;
	});

	DatatypeTester->SetTestCategory(testFlags, TXT("Multicast Test"));
	{
		resetMulticastVars();
		SDFunction<void, Int> functionA = SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastFunctionA, void, Int);
		SDFunction<void, Int> functionB = SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastFunctionB, void, Int);
		SDFunction<void, Int> functionC = SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastFunctionC, void, Int);
		SDFunction<void, Int> functionD = SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastFunctionD, void, Int);

		std::vector<SDFunction<void, Int>*> functions;
		functions.push_back(&functionA);
		functions.push_back(&functionB);
		functions.push_back(&functionC);
		functions.push_back(&functionD);

		MulticastTestBroadcaster.RegisterHandler(functionA);
		MulticastTestBroadcaster.RegisterHandler(functionB);
		MulticastTestBroadcaster.RegisterHandler(functionC);
		MulticastTestBroadcaster.RegisterHandler(functionD);

		if (MulticastTestBroadcaster.IsLocked())
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. Before it broadcasts anything, it's locked."));
			return false;
		}

		for (size_t i = 0; i < functions.size(); ++i)
		{
			if (!MulticastTestBroadcaster.IsRegistered(*functions.at(i), false))
			{
				DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have been registered by now."), functions.at(i)->ToString());
				return false;
			}
		}

		MulticastTestBroadcaster.Broadcast(1);
		Int expectedCounter = 4;
		if (MulticastTestCounter != expectedCounter)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s functions should have been invoked. Instead only %s functions were called."), expectedCounter, MulticastTestCounter);
			return false;
		}

		for (char i = 0; i < numHandlerFlags; ++i)
		{
			if (!MulticastTestHandlerCalled[i])
			{
				DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have set the flag to true."), functions.at(i)->ToString());
				return false;
			}
		}

		if (MulticastTestBroadcaster.IsLocked())
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. It should have unlocked itself after broadcasting."));
			return false;
		}

		resetMulticastVars();
		MulticastTestBroadcaster.UnregisterHandler(functionB);
		if (MulticastTestBroadcaster.IsRegistered(functionB, false))
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have unregistered from the multicast delegate."), functionB);
			return false;
		}

		MulticastTestBroadcaster.Broadcast(2);
		expectedCounter = 6;
		if (MulticastTestCounter != expectedCounter)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have been invoked. Instead only %s functions were called."), expectedCounter, MulticastTestCounter);
			return false;
		}

		MulticastTestBroadcaster.UnregisterHandler(functionA);
		MulticastTestBroadcaster.UnregisterHandler(functionC);
		MulticastTestBroadcaster.UnregisterHandler(functionD);

		//Test adding/removing delegates within a broadcast
		resetMulticastVars();
		AddFunctionCanFail = true;
		RemoveFunctionCanFail = true;
		SDFunction<void, Int> addFunction(SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastAddFunction, void, Int));
		SDFunction<void, Int> removeFunction(SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastRemoveFunction, void, Int));
		MulticastTestBroadcaster.RegisterHandler(addFunction);
		MulticastTestBroadcaster.Broadcast(1); //Handler should add removeFunction
		expectedCounter = 1;
		if (MulticastTestCounter != expectedCounter)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s functions should have been called. Instead only %s were called."), expectedCounter, MulticastTestCounter);
			return false;
		}

		if (!MulticastAddFunctionCalled)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have been called."), addFunction);
			return false;
		}

		if (MulticastRemoveFunctionCalled)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should NOT have been called even if it was registered in the middle of a broadcast."), removeFunction);
			return false;
		}

		if (!MulticastTestBroadcaster.IsRegistered(removeFunction, false))
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have registered %s by the time the Broadcast finished."), addFunction, removeFunction);
			return false;
		}

		resetMulticastVars();
		AddFunctionCanFail = false; //Disable tests for the AddFunction since those tests are checking if RemoveFunction is already added. In this case, it should have been added by now.
		MulticastTestBroadcaster.Broadcast(1); //Handler should remove the addFunction
		expectedCounter = 2;
		if (MulticastTestCounter != expectedCounter)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s functions should have been called. Instead only %s were called."), expectedCounter, MulticastTestCounter);
			return false;
		}

		if (!MulticastAddFunctionCalled)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have been called even though it was unregistered in the middle of a broadcast."), addFunction);
			return false;
		}

		if (!MulticastRemoveFunctionCalled)
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have been called."), removeFunction);
			return false;
		}

		if (MulticastTestBroadcaster.IsRegistered(addFunction, false))
		{
			DatatypeTester->UnitTestError(testFlags, TXT("Multicast test failed. %s should have unregistered %s by the time the Broadcast finished."), removeFunction, addFunction);
			return false;
		}
	}
	DatatypeTester->CompleteTestCategory(testFlags);

	return true;
}

void SDFunctionTester::HandleVoidFunction ()
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleVoidFunction event handler was invoked."));
	bTestedVoid = true;
	FunctionCounter++;
}

void SDFunctionTester::HandleParamFunction (Int paramValueTest)
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleParamFunction event handler was invoked with param %s"), paramValueTest);

	if (paramValueTest != 5)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameter for HandleParamFunction is 5.  Instead it received:  %s"), paramValueTest);
		return;
	}

	bTestedParamINT = true;
	FunctionCounter++;
}

void SDFunctionTester::HandleMultiParamFunction (Int param1, Float param2, DString param3, unsigned int param4)
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleMultiParamFunction event handler was invoked with params:  %s, %s, %s, %s"), param1, param2, param3, DString::MakeString(param4));

	if (param1 != 5 || param2 != 5.f || param3 != TXT("Five"), param4 != 5)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameters for HandleMultiParamFunction is 5, 5.5, \"Five\", 5.  Instead it received:  %s, %s, %s, %s"), param1, param2, param3, DString::MakeString(param4));
		return;
	}

	bTestedMultiParam = true;
	FunctionCounter++;
}

Float SDFunctionTester::HandleReturnFunction ()
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleReturnFunction event handler was invoked."));
	bTestedReturn = true;
	FunctionCounter++;

	return 5.f;
}

Bool SDFunctionTester::HandleReturnMultiParamFunction (Int param1, Object* param2, std::string param3)
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleReturnMultiParamFunction event handler was invoked with params:  %s, %s, %s"), param1, param2->ToString(), DString(param3));

	if (param1 != 5, param2 != this, param3 != TXT("Five"))
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameters for HandleReturnMultiParamFunction is 5, %s, \"Five\".  Instead it received:  %s, %s, %s.  Function is returning false."), ToString(), param1, param2->ToString(), DString(param3));
		return false;
	}

	bTestedReturnMultiParam = true;
	FunctionCounter++;

	return true;
}

Int SDFunctionTester::HandleReturnOutParamFunction (Int param1, Int& outParam2)
{
	UnitTester::TestLog(TestFlags, TXT("SDFunctionTester::HandleReturnOutParamFunction event handler was invoked with param:  %s"), param1);

	if (param1 != 5)
	{
		DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameter for HandleReturnOutParamFunction is 5.  Instead it received:  %s.  Function is returning 0."), param1);
		return 0;
	}

	bTestedReturnOutParam = true;
	FunctionCounter++;

	outParam2 = 5;
	return 5;
}

void SDFunctionTester::HandleMulticastFunctionA (Int incrementAmount)
{
	MulticastTestCounter += incrementAmount;
	MulticastTestHandlerCalled[0] = true;
}

void SDFunctionTester::HandleMulticastFunctionB (Int incrementAmount)
{
	MulticastTestCounter += incrementAmount;
	MulticastTestHandlerCalled[1] = true;
}

void SDFunctionTester::HandleMulticastFunctionC (Int incrementAmount)
{
	MulticastTestCounter += incrementAmount;
	MulticastTestHandlerCalled[2] = true;
}

void SDFunctionTester::HandleMulticastFunctionD (Int incrementAmount)
{
	MulticastTestCounter += incrementAmount;
	MulticastTestHandlerCalled[3] = true;
}

void SDFunctionTester::HandleMulticastAddFunction (Int incrementAmount)
{
	CHECK(DatatypeTester != nullptr)
	if (AddFunctionCanFail)
	{
		if (!MulticastTestBroadcaster.IsLocked())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. The broadcaster should be locked while the functions are handling their events."));
			return;
		}

		SDFunction<void, Int> removeFunction(SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastRemoveFunction, void, Int));
		MulticastTestBroadcaster.RegisterHandler(removeFunction);
		if (MulticastTestBroadcaster.IsRegistered(removeFunction, false))
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. %s shouldn't be able to regster to the broadcaster while it's locked."), removeFunction);
			return;
		}

		if (!MulticastTestBroadcaster.IsRegistered(removeFunction, true))
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. %s should have been pending registration."), removeFunction);
			return;
		}
	}

	//Only increment the value if everything passed to notify so that the test will fail if this handler returned early.
	MulticastTestCounter += incrementAmount;
	MulticastAddFunctionCalled = true;
}

void SDFunctionTester::HandleMulticastRemoveFunction (Int incrementAmount)
{
	CHECK(DatatypeTester != nullptr)
	if (RemoveFunctionCanFail)
	{
		if (!MulticastTestBroadcaster.IsLocked())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. The broadcaster should be locked while the functions are handling their events."));
			return;
		}

		SDFunction<void, Int> addFunction(SDFUNCTION_1PARAM(this, SDFunctionTester, HandleMulticastAddFunction, void, Int));
		MulticastTestBroadcaster.UnregisterHandler(addFunction);
		if (!MulticastTestBroadcaster.IsRegistered(addFunction, false))
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. %s shouldn't have been removed from the list while the broadcaster while it's locked."), addFunction);
			return;
		}

		if (MulticastTestBroadcaster.IsRegistered(addFunction, true))
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Multicast test failed. %s should have been pending removal."), addFunction);
			return;
		}
	}

	//Only increment the value if everything passed to notify so that the test will fail if this handler returned early.
	MulticastTestCounter += incrementAmount;
	MulticastRemoveFunctionCalled = true;
}

SD_END
#endif