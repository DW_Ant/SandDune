/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Utils.cpp
=====================================================================
*/

#include "BaseUtils.h"
#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "Utils.h"
#include "Version.h"

IMPLEMENT_ABSTRACT_CLASS(SD::Utils, SD::BaseUtils)
SD_BEGIN

DString Utils::GetBinaryConfigSuffix ()
{
	static DString suffix(DString::EmptyString);

#ifdef CMAKE_INTDIR
	static bool bSuffixInitialized = false;
	if (!bSuffixInitialized)
	{
		bSuffixInitialized = true;
		DString cmakeConfig(CMAKE_INTDIR);
		if (cmakeConfig.Compare(TXT("Debug"), DString::CC_CaseSensitive) == 0)
		{
			suffix = TXT("-Debug");
		}
		else if (cmakeConfig.Compare(TXT("MinSizeRel"), DString::CC_CaseSensitive) == 0)
		{
			suffix = TXT("-MinSize");
		}
		else if (cmakeConfig.Compare(TXT("Release"), DString::CC_CaseSensitive) == 0)
		{
			suffix = DString::EmptyString;
		}
		else if (cmakeConfig.Compare(TXT("RelWithDebInfo"), DString::CC_CaseSensitive) == 0)
		{
			suffix = TXT("-DbRel");
		}
		else
		{
			suffix = DString::EmptyString;
		}
	}
#endif

	return suffix;
}
SD_END