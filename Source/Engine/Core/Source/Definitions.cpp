/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Definitions.cpp
=====================================================================
*/

#include "Definitions.h"

SD_BEGIN
DString ProjectName = TXT("Untitled");
SD_END