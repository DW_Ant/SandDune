/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DString.cpp
=====================================================================
*/

#include "ContainerUtils.h"
#include "CoreDatatypes.h"
#include "utf8.h"
#include <codecvt>
#include <regex>

SD_BEGIN
const DString DString::EmptyString; //Empty string with NumCharacters already initialized to 0.

DString::DString ()
{
	String = TXT("");
#if USE_CACHED_LENGTHS
	NumCharacters = 0;
#endif
}

DString::DString (const TStringChar inText)
{
#if USE_CACHED_LENGTHS
	NumCharacters = 1;
#endif

	String = inText;
}

DString::DString (const TStringChar* inText)
{
#if USE_UTF32
	#error Please implement DString::DString (const TStringChar* inText) for UTF-32.
#elif USE_UTF16
	#error Please implement DString::DString (const TStringChar* inText) for UTF-16.
#elif USE_UTF8
	String = TString(reinterpret_cast<const char*>(inText));
#else
	String = TString(inText);
#endif

#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}

DString::DString (const DString& inString)
{
	String = inString.String;
#if USE_CACHED_LENGTHS
	NumCharacters = inString.NumCharacters;
#endif
}

DString::DString (const TString& inString)
{
	String = inString;
#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}

#if !USE_UTF32
DString::DString (const StringUTF32& inString)
{
#if USE_UTF16
	#error Please implement UTF-32 to UTF-16 encoding conversion.
#elif USE_UTF8
	std::vector<unsigned char> utf8Result;
	utf8::unchecked::utf32to8(inString.begin(), inString.end(), std::back_inserter(utf8Result));

	String = TString(utf8Result.begin(), utf8Result.end());
#else
	String = inString.String;
#endif

#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}
#endif

#if !USE_UTF16
DString::DString (const StringUTF16& inString)
{
#if USE_UTF32
	#error Please implement UTF-16 to UTF-32 encoding conversion.
#elif USE_UTF8
	std::vector<unsigned char> utf8Result;
	utf8::unchecked::utf16to8(inString.begin(), inString.end(), std::back_inserter(utf8Result));

	String = TString(utf8Result.begin(), utf8Result.end());
#else
	String = inString.String;
#endif

#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}
#endif

#if !USE_UTF8
DString (const StringUTF8& inString)
{
#if USE_UTF32
	#error Please implement UTF-32 to UTF-8 encoding conversion.
#elif USE_UTF16
	#error Please implement UTF-16 to UTF-8 encoding conversion.
#else
	String = inString.String;
#endif

#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}
#endif

DString::~DString ()
{

}

void DString::operator= (const DString& inString)
{
	String = inString.String;
#if USE_CACHED_LENGTHS
	NumCharacters = inString.NumCharacters;
#endif
}

void DString::operator= (const TStringChar* inStr)
{
#if USE_UTF32
	#error Please implement DString::operator= (const TStringChar* inText) for UTF-32.
#elif USE_UTF16
	#error Please implement DString::operator= (const TStringChar* inText) for UTF-16.
#elif USE_UTF8
	String = TString(reinterpret_cast<const char*>(inStr));
#else
	String = TString(inStr);
#endif

	MarkNumCharactersDirty();
}

TStringChar& DString::operator[] (size_t idx)
{
	return String[idx];
}

const TStringChar& DString::operator[] (size_t idx) const
{
	return String[idx];
}

void DString::ResetToDefaults ()
{
	String = TXT("");
#if USE_CACHED_LENGTHS
	NumCharacters = 0;
#endif
}

DString DString::ToString () const
{
	return *this;
}

void DString::ParseString (const DString& str)
{
	*this = str;
}

size_t DString::GetMinBytes () const
{
	return DString::SGetMinBytes();
}

void DString::Serialize (DataBuffer& outData) const
{
	//NumBytes could be different from number of characters.
	//Use raw character bytes regardless of encoding since we'll deserialize it the same way.
	const Int numBytes = String.length();
	const char* cStr = String.c_str();

	//First write the string length in data buffer
	outData << numBytes;

	//Append the actual text
	outData.AppendBytes(cStr, numBytes.ToUnsignedInt());
}

bool DString::Deserialize (const DataBuffer& dataBuffer)
{
	//Read how many bytes are in data buffer
	Int numBytes;
	if ((dataBuffer >> numBytes).HasReadError())
	{
		return false;
	}

	size_t uNumBytes = numBytes.ToUnsignedInt();
	if (!dataBuffer.CanReadBytes(uNumBytes))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to read DString from data buffer. It's expecting %s bytes to read from the data buffer that doesn't have that length."), numBytes);
		dataBuffer.JumpToEnd(); //Simulate reading to the end of the buffer

#ifdef DEBUG_MODE
		String = TXT("<Byte Buffer Read Error>");
#else
		String = EmptyString.String;
#endif

		MarkNumCharactersDirty();
		return false;
	}

	char* cStr = new char[uNumBytes + 1]; //include null terminator
	dataBuffer.ReadBytes(cStr, uNumBytes);
	cStr[uNumBytes] = '\0';

	String = cStr;
	delete[] cStr;

	MarkNumCharactersDirty();
	return true;
}

size_t DString::SGetMinBytes ()
{
	//All strings are required to specified their length before listing their content.
	//Smallest case would be an empty string. An empty string has a leading Int of zero.
	return Int::SGetMinBytes();
}

const TCHAR* DString::ToCString () const
{
	return String.c_str();
}

sf::String DString::ToSfmlString () const
{
	return ToUTF32();
}

StringUTF32 DString::ToUTF32 () const
{
#if USE_UTF32
	return String;
#elif USE_UTF16
	#error Please implement 'ToUTF32' for current encoding configuration.  Need to translate UTF-16 to UTF-32.
#elif USE_UTF8
	std::vector<unsigned int> utf32;
	//Translate from UTF-8 to UTF-32
	utf8::utf8to32(String.begin(), String.end(), std::back_inserter(utf32));

	return StringUTF32(utf32.begin(), utf32.end());
#else
	return String;
#endif
}

StringUTF16 DString::ToUTF16 () const
{
#if USE_UTF32
	#error Please implement 'ToUTF16' for current encoding configuration.  Need to translate UTF-32 to UTF-16.
#elif USE_UTF16
	return String;
#elif USE_UTF8
	std::vector<unsigned short> utf16;
	//Translate from UTF-8 to UTF-16
	utf8::utf8to16(String.begin(), String.end(), std::back_inserter(utf16));

	return StringUTF16(utf16.begin(), utf16.end());
#else
	return String;
#endif
}

#ifdef PLATFORM_WINDOWS
//This function is based on rubenvb's code found from:  http://stackoverflow.com/questions/3329718/utfcpp-and-win32-wide-api
std::wstring DString::ToWideStringInUTF16 () const
{
	// get length
	int length = MultiByteToWideChar(CP_UTF8, NULL, ToCString(), static_cast<int>(String.size()), NULL, 0);
	if (length <= 0)
	{
		return std::wstring();
	}
	std::wstring result;
	result.resize(length);

	//Protection against inf recursion (since log functions call this)
	if (MultiByteToWideChar(CP_UTF8, NULL, ToCString(), static_cast<int>(String.size()), &result[0], static_cast<int>(result.size())) > 0)
	{
		return result;
	}
	else
	{
		std::runtime_error("Failed to convert " + String + " to a wide string in UTF-16.");
	}

	return std::wstring();
}
#endif

TString DString::ToUTF8 () const
{
#if USE_UTF32
	#error Please implement 'ToUTF8' for current encoding configuration.  Need to translate UTF-32 to UTF-8.
#elif USE_UTF16
	#error Please implement 'ToUTF8' for current encoding configuration.  Need to translate UTF-16 to UTF-8.
#elif USE_UTF8
	return String;
#else
	return String;
#endif
}

bool DString::IsValidEncoding () const
{
#if USE_UTF32
	#error Please implement DString::IsValidUTF32String.
	return IsValidUTF32String();
#elif USE_UTF16
	#error Please implement DString::IsValidUTF16String.
	return IsValidUTF16String();
#elif USE_UTF8
	return IsValidUTF8String();
#else
	return true;
#endif
}

const TStringChar& DString::At (Int idx) const
{
#if SAFE_STRINGS
	idx = Utils::Clamp<Int>(idx, 0, Length() - 1);
#endif

	return String.at(idx.Value);
}

DString DString::FindAt (Int idx) const
{
#if SAFE_STRINGS
	idx = Utils::Clamp<Int>(idx, 0, Length() - 1);
#endif

#if USE_UTF32
	return DString(String.at(idx.Value * 4));
#elif USE_UTF16 || USE_UTF8
	StringIterator iter(this); //point to first character
	for (UINT_TYPE i = 0; i < idx; i++)
	{
		CHECK(!iter.IsAtEnd())
		++iter; //jump to next character
	}

	return iter.GetString();
#else
	return DString(At(idx));
#endif
}

bool DString::ToBool () const
{
	std::vector<DString> stringsToCmp;
	stringsToCmp.push_back(TXT("true"));
	stringsToCmp.push_back(TXT("1"));
	stringsToCmp.push_back(TXT("yes"));

	for (UINT_TYPE i = 0; i < stringsToCmp.size(); i++)
	{
		if (Compare(stringsToCmp.at(i), DString::CC_IgnoreCase) == 0)
		{
			return true;
		}
	}

	return false;
}

Int DString::Atoi () const
{
	try
	{
#ifdef PLATFORM_64BIT
		return Int(std::atoll(ToCString()));
#else
		return Int(std::atoi(ToCString()));
#endif
	}
	catch(std::out_of_range&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("There's not enough space to fit %s within an Int.  Defaulting to SD_MAXINT(%s)."), ToString(), Int(SD_MAXINT));
		return SD_MAXINT;
	}
	catch(std::invalid_argument&)
	{
		return 0;
	}
}

Float DString::Stof () const
{
	try
	{
		return Float(std::stof(String));
	}
	catch(std::out_of_range&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("There's not enough space to fit %s within a float.  Defaulting to FLT_MAX_10_EXP(%s)."), ToString(), Float(FLT_MAX_10_EXP));
		return FLT_MAX_10_EXP;
	}
	catch (std::invalid_argument&)
	{
		return 0.f;
	}
}

Int DString::Compare (const DString& otherString, ECaseComparison caseComparison) const
{
	if (caseComparison == CC_IgnoreCase)
	{
		//Create copies of strings to adjust casing
		DString localCopy = String;
		localCopy.ToLower();
		DString localOther = otherString;
		localOther.ToLower();

		return localCopy.String.compare(localOther.String);
	}

	return String.compare(otherString.String);
}

Int DString::Find (const DString& search, Int startPos, ECaseComparison caseComparison, ESearchDirection searchDirection) const
{
	if (search.Length() > Length())
	{
		return INT_INDEX_NONE; //Search is larger than the actual string. Not possible to match anything.
	}

#if SAFE_STRINGS
	if (search.IsEmpty())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Search query for %s is empty.  Cannot find sub text."), ToString());
		return INT_INDEX_NONE;
	}
#endif
	if (caseComparison == CC_IgnoreCase)
	{
		DString localCopy = String;
		localCopy.ToLower();
		DString localSearch = search;
		localSearch.ToLower();

		return localCopy.Find(localSearch, startPos, CC_CaseSensitive, searchDirection);
	}

#if SAFE_STRINGS
	startPos = Utils::Clamp<Int>(startPos, 0, Length() - 1);
#endif

#if USE_UTF32 || USE_UTF16 || USE_UTF8
	//Use StringIterators to handle jumping through multiple bytes.
	if (searchDirection == SD_LeftToRight)
	{
		StringIterator iter(this);
		for (Int i = 0; i < startPos; i++) //jump to starting position
		{
			iter++;
		}

		Int charIdx = startPos; //SearchIdx is the character index position the iterator is at.
		for (; !iter.IsAtEnd(); ++iter, charIdx++)
		{
			if (DoCharactersMatch(*this, iter.GetStringCharByteIdx(), search, 0, iter.GetSizeOfCharacter()))
			{
				//Ensure there's a complete match for all following characters
				StringIterator searchIter(&search);
				++searchIter; //go to next character since we already confirmed a match with the first
				while (!searchIter.IsAtEnd())
				{
					if (!DoCharactersMatch(*this, iter.GetStringCharByteIdx() + searchIter.GetStringCharByteIdx(), search, searchIter.GetStringCharByteIdx(), searchIter.GetSizeOfCharacter()))
					{
						break; //Break early to notify that there isn't a complete match
					}

					++searchIter;
				}

				if (searchIter.IsAtEnd())
				{
					return charIdx;
				}
			}
		}

		return INT_INDEX_NONE;
	}
	else
	{
		StringIterator iter(this, false);

		//Immediately don't bother searching the last few characters
		startPos = Utils::Max(startPos, search.Length() - 1);
		for (Int i = 0; i < startPos; i++)
		{
			iter--;
		}

		Int charIdx = Length() - 1 - startPos; //SearchIdx is the character index position the iterator is at.
		for (; !iter.IsAtBeginning(); --iter, charIdx--)
		{
			if (DoCharactersMatch(*this, iter.GetStringCharByteIdx(), search, 0, iter.GetSizeOfCharacter()))
			{
				//Ensure there's a complete match for all following characters
				StringIterator searchIter(&search);
				++searchIter; //go to next character since we already confirmed a match with the first
				while (!searchIter.IsAtEnd())
				{
					if (!DoCharactersMatch(*this, iter.GetStringCharByteIdx() + searchIter.GetStringCharByteIdx(), search, searchIter.GetStringCharByteIdx(), searchIter.GetSizeOfCharacter()))
					{
						break; //Break early to notify that there isn't a complete match
					}

					++searchIter;
				}

				if (searchIter.IsAtEnd())
				{
					return charIdx;
				}
			}
		}

		return INT_INDEX_NONE;
	}
#else
	//Use std library for ANSI strings.
	if (!bSearchFromRight)
	{
		return String.find(search.String, startPos.Value);
	}
	else
	{
		return String.rfind(search.String, startPos.Value);
	}
#endif
}

Int DString::Find (TStringChar search, Int startPos, ESearchDirection searchDirection) const
{
	if (IsEmpty())
	{
		return INT_INDEX_NONE;
	}

#if SAFE_STRINGS
	startPos = Utils::Clamp<Int>(startPos, 0, Length() - 1);
#endif

	Int multiplier = 1;
	if (searchDirection == SD_RightToLeft)
	{
		multiplier *= -1;
	}

	StringIterator iter(this, searchDirection == SD_LeftToRight);
	iter += startPos * multiplier;
	Int result;

	if (searchDirection == SD_LeftToRight)
	{
		result = startPos;
		while (!iter.IsAtEnd())
		{
			if (iter.GetSizeOfCharacter() == 1 && String[iter.GetStringCharByteIdx().Value] == search)
			{
				return result;
			}
			
			++result;
			++iter;
		}
	}
	else
	{
		result = (Length() - 1) - startPos;
		while (true)
		{
			if (iter.GetSizeOfCharacter() == 1 && String[iter.GetStringCharByteIdx().Value] == search)
			{
				return result;
			}

			if (iter.IsAtBeginning())
			{
				break;
			}

			--result;
			--iter;
		}
	}
	
	return INT_INDEX_NONE;
}

bool DString::Contains (const DString& subStr, ECaseComparison caseComparison) const
{
	return Find(subStr, 0, caseComparison) != INT_INDEX_NONE;
}

bool DString::StartsWith (const DString& expectedText, ECaseComparison caseComparison) const
{
	if (caseComparison == CC_IgnoreCase)
	{
		DString localCopy = String;
		localCopy.ToLower();
		DString localSearch = expectedText;
		localSearch.ToLower();

		return localCopy.StartsWith(localSearch, CC_CaseSensitive);
	}

	StringIterator selfIter(this, true);
	StringIterator searchIter(&expectedText, true);
	for (; !selfIter.IsAtEnd() && !searchIter.IsAtEnd(); ++selfIter, ++searchIter)
	{
		if (selfIter.GetSizeOfCharacter() != searchIter.GetSizeOfCharacter())
		{
			return false;
		}

		if (!DoCharactersMatch(*this, selfIter.GetStringCharByteIdx(), expectedText, searchIter.GetStringCharByteIdx(), selfIter.GetSizeOfCharacter()))
		{
			return false;
		}
	}

	//Only return true if the entire expectedText is within this string. Returns false if selfIter reached the end before the searchIter.
	return searchIter.IsAtEnd();
}

bool DString::StartsWith (TStringChar expectedChar) const
{
	return (String.size() > 0 && String.at(0) == expectedChar);
}

bool DString::EndsWith (const DString& expectedText, ECaseComparison caseComparison) const
{
	if (caseComparison == CC_IgnoreCase)
	{
		DString localCopy = String;
		localCopy.ToLower();
		DString localSearch = expectedText;
		localSearch.ToLower();

		return localCopy.EndsWith(localSearch, CC_CaseSensitive);
	}

	StringIterator selfIter(this, false);
	StringIterator searchIter(&expectedText, false);
	for (; !selfIter.IsAtBeginning() && !searchIter.IsAtBeginning(); --selfIter, --searchIter)
	{
		if (selfIter.GetSizeOfCharacter() != searchIter.GetSizeOfCharacter())
		{
			return false;
		}

		if (!DoCharactersMatch(*this, selfIter.GetStringCharByteIdx(), expectedText, searchIter.GetStringCharByteIdx(), selfIter.GetSizeOfCharacter()))
		{
			return false;
		}
	}

	//Only return true if the entire expectedText is within this string. Returns false if selfIter reached the beginning before the searchIter.
	return searchIter.IsAtBeginning();
}

bool DString::EndsWith (TStringChar expectedChar) const
{
	return (String.size() > 0 && String.at(String.size() - 1) == expectedChar);
}

void DString::PopBack ()
{
	//Identify how many characters there are at the end.
	StringIterator iter(this, false);
	size_t numChars = iter.GetSizeOfCharacter();

	if (numChars <= String.size())
	{
		String.resize(String.size() - numChars);

#if USE_CACHED_LENGTHS
		//Don't decrement if negative since negative characters are already marked as dirty.
		if (NumCharacters > 0)
		{
			--NumCharacters;
		}
#endif
	}
}

void DString::AppendChars (const TStringChar* newChars)
{
	*this += newChars;
}

void DString::SubString (DString& outResult, Int startIdx, Int endIdx)
{
	if (startIdx > endIdx)
	{
		//return the rest of the string after startIdx.
		endIdx = outResult.Length() - 1;
	}

#if SAFE_STRINGS
	startIdx = Utils::Max<Int>(startIdx, 0);
	endIdx = Utils::Clamp<Int>(endIdx, startIdx, Length() - 1);

	if (startIdx >= Length())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Invalid SubString range.  The starting index (%s) is beyond the string length:  %s"), startIdx, DString(String));
		return DString::EmptyString;
	}
#endif

	//+1 to include endIdx
	SubStringCount(outResult, startIdx, (endIdx - startIdx) + 1);
}

DString DString::SubString (Int startIdx, Int endIdx) const
{
	DString result(*this);
	SubString(OUT result, startIdx, endIdx);

	return result;
}

void DString::SubStringCount (DString& outResult, Int startIdx, Int count)
{
#if SAFE_STRINGS
	startIdx = Utils::Clamp<Int>(startIdx, 0, Length());
	count = Utils::Clamp<Int>(count, -1, Length() - startIdx);
#endif

	if (count < 0)
	{
		count = INT_INDEX_NONE;
	}

#if USE_UTF32
	#error Please implement DString::SubString for UTF-32 configuration.
#elif USE_UTF16
	#error Please implement DString::SubString for UTF-16 configuration.
#else //UTF-8 and ANSI
	#if USE_UTF8
	StringIterator iter(&outResult);
	for (Int i = 0; i < startIdx; i++)
	{
		++iter;
	}
	startIdx = iter.GetStringCharByteIdx();

	if (count >= 0)
	{
		for (Int i = 0; i < count; i++)
		{
			++iter;
		}
		Int endIdx = iter.GetStringCharByteIdx();
		count = endIdx - startIdx;
	}
	#endif

	outResult.String = outResult.String.substr(startIdx.Value, count.Value);
	outResult.MarkNumCharactersDirty();
#endif
}

DString DString::SubStringCount (Int startIdx, Int count) const
{
	DString result(*this);
	SubStringCount(OUT result, startIdx, count);

	return result;
}

void DString::Left (DString& outResult, Int count)
{
#if SAFE_STRINGS
	count = Utils::Min(count, Length());
#endif

	SubStringCount(outResult, 0, count);
}

DString DString::Left (Int count) const
{
	DString result(*this);
	Left(OUT result, count);

	return result;
}

void DString::Right (DString& outResult, Int count)
{
#if SAFE_STRINGS
	count = Utils::Min(count, Length());
#endif

	SubStringCount(outResult, outResult.Length() - count);
}

DString DString::Right (Int count) const
{
	DString result(*this);
	Right(OUT result, count);

	return result;
}

void DString::SplitString (Int splitIndex, DString& outFirstSegment, DString& outSecondSegment) const
{
#if SAFE_STRINGS
	splitIndex = Utils::Clamp<Int>(splitIndex, 0, Length() - 1);
#endif

#if USE_UTF32 || USE_UTF16 || USE_UTF8
	//Use DString::SubString to handle Unicode configurations.
	outFirstSegment = SubStringCount(0, splitIndex);
	outSecondSegment = SubString(splitIndex + 1);
#else
	//Use std library immediately to avoid the SAFE_STRINGS check.
	outFirstSegment = String.substr(0, splitIndex.Value);
	outSecondSegment = String.substr(splitIndex.Value + 1);
#endif
}

void DString::ParseString (TCHAR delimiter, std::vector<DString>& outSegments, bool removeEmpty) const
{
	ContainerUtils::Empty(OUT outSegments);

	Int startIdx = 0;
	Int iterCounter = 0;
	for (StringIterator iter(this); !iter.IsAtEnd(); ++iter, ++iterCounter)
	{
		if (*iter == delimiter)
		{
			if (iterCounter == startIdx) //Handle two consecutive delimiters
			{
				if (!removeEmpty)
				{
					outSegments.push_back(DString::EmptyString);
				}
			}
			else
			{
				const Int endIdx = iterCounter - 1; //Minus 1 to not include the delimiter
				DString newEntry = SubString(startIdx, endIdx);
				outSegments.push_back(newEntry);
			}

			//skip over delimiter
			startIdx = iterCounter + 1;
		}
	}

	//Push the last segment
	if (!removeEmpty || startIdx < Length())
	{
		outSegments.push_back(SubString(startIdx));
	}
}

bool DString::IsWrappedByChar (const TCHAR wrappingChar, Int charIdxToCheck) const
{
	if (IsEmpty())
	{
		return false;
	}

#if SAFE_STRINGS
	charIdxToCheck = Utils::Clamp<Int>(charIdxToCheck, 0, Length() - 1);
#endif

	Int strLength = Length();
	bool bWrapped = false;
	Int iterCounter = 0;
	for (StringIterator iter(this); iterCounter < strLength; iterCounter++, ++iter)
	{
		if (iterCounter == charIdxToCheck)
		{
			if (*iter == wrappingChar)
			{
				return false; //Wrapping characters can never wrap themselves since they define the wrapping boundaries
			}

			return bWrapped;
		}

		if (*iter == wrappingChar)
		{
			bWrapped = !bWrapped;
		}
	}

	return false; //Specified index is beyond length.
}

Int DString::Length () const
{
#if USE_UTF32
	return UTF32Length(String);
#elif USE_UTF16
	#if USE_CACHED_LENGTHS
	if (NumCharacters < 0)
	{
		NumCharacters = UTF16Length(String);
	}

	return NumCharacters;
	#else
	return UTF16Length(String);
	#endif
#elif USE_UTF8
	#if USE_CACHED_LENGTHS
	if (NumCharacters < 0)
	{
		NumCharacters = UTF8Length(String);
	}

	return NumCharacters;
	#else
	return UTF8Length(String);
	#endif
#else
	return String.length();
#endif
}

size_t DString::GenerateHash () const
{
	size_t hash = std::hash<std::string>{}(ToStr());
	return hash;
}

void DString::MarkNumCharactersDirty ()
{
#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif
}

Int DString::UTF32Length (const StringUTF32& utf32String)
{
	return utf32String.length();
}

Int DString::UTF16Length (const StringUTF16& utf16String)
{
	return utf16String.length();
}

Int DString::UTF8Length (const StringUTF8& utf8String)
{
	return utf8::distance(utf8String.begin(), utf8String.end());
}

bool DString::IsEmpty () const
{
	return String.empty();
}

void DString::Clear ()
{
	String.clear();
#if USE_CACHED_LENGTHS
	NumCharacters = 0;
#endif
}

void DString::Insert (Int idx, const TStringChar character)
{
#if SAFE_STRINGS
	idx = Utils::Clamp<Int>(idx, 0, Length() - 1);
#endif

	MarkNumCharactersDirty();

#if USE_UTF32
	#error Please implement DString::Insert for UTF-32.
#elif USE_UTF16
	#error Please implement DString::Insert for UTF-16.
#else //UTF-16 and ANSI
	String.insert(String.begin() + idx.Value, character);
#endif
}

void DString::Insert (Int idx, const DString& text)
{
#if SAFE_STRINGS
	idx = Utils::Clamp<Int>(idx, 0, Length());
#endif

	MarkNumCharactersDirty();

#if USE_UTF32 || USE_UTF16
	#error Please implement DString::Insert for your character encoding configuration.
#else //UTF-8 or ANSI
	#if USE_UTF8
	//Use string iterator to identify the starting index position.
	StringIterator iter(this);
	for (Int iterIdx = 0; iterIdx < idx; iterIdx++)
	{
		++iter;
	}
	idx = iter.GetStringCharByteIdx();
	#endif
	String.insert(idx.Value, text.String);
#endif
}

void DString::Remove (Int idx, Int count)
{
	if (count <= 0)
	{
		//Remove all characters from idx to the end.
		count = Length() - idx;
	}

#if SAFE_STRINGS
	idx = Utils::Clamp<Int>(idx, 0, Length() - 1);
	count = Utils::Min<Int>(count, Length() - idx);
#endif

#if USE_CACHED_LENGTHS
	NumCharacters -= count; //Decrement NumCharacters early before the multi-byte character offsets would displace count.
#endif

#if USE_UTF32
	#error Please implement DString::Remove for UTF-32.
#elif USE_UTF16
	#error Please implement DString::Remove for UTF-16.
#else //UTF-8 or ANSI
	#if USE_UTF8
	//Use string iterator to identify starting index position.
	StringIterator iter(this);
	iter += idx;
	idx = iter.GetStringCharByteIdx(); //Shift index a bit if there are wide characters in this string.

	//Identify how many characters to remove
	iter += count;
	count = iter.GetStringCharByteIdx() - idx; //Add extra characters if the characters we're deleting are multi-byte.
	#endif

	//Use std library to pull characters from string.
	String.erase(idx.Value, count.Value);
#endif
}

void DString::ReplaceInline (const DString& searchFor, const DString& replaceWith, ECaseComparison caseComparison)
{
	MarkNumCharactersDirty();

	Int startIdx = Find(searchFor, 0, caseComparison, SD_LeftToRight);
	while (startIdx >= 0)
	{
		Remove(startIdx, searchFor.Length());
		Insert(startIdx, replaceWith);

		startIdx += replaceWith.Length();
		startIdx = Find(searchFor, startIdx, caseComparison, SD_LeftToRight);
	}
}

DString DString::Replace (const DString& searchIn, const DString& searchFor, const DString& replaceWith, ECaseComparison caseComparison)
{
	DString results(searchIn);
	results.ReplaceInline(searchFor, replaceWith, caseComparison);

	return results;
}

DString DString::FormatString (DString text, const std::vector<DString>& values)
{
	Int searchIdx = 0;
	for (UINT_TYPE valueIdx = 0; valueIdx < values.size(); valueIdx++)
	{
		Int macroPos = text.Find(TXT("%s"), searchIdx, CC_CaseSensitive);
		if (macroPos == INT_INDEX_NONE)
		{
			return text; //Ran out of %s macros
		}

		searchIdx = macroPos + values.at(valueIdx).Length();

		//Remove the '%s' text
		text.Remove(macroPos, 2);

		//Insert the content from values
		text.Insert(macroPos, values.at(valueIdx));
	}

	return text;
}

void DString::ToUpper ()
{
	transform(String.begin(), String.end(), String.begin(), toupper);
}

void DString::ToLower ()
{
	transform(String.begin(), String.end(), String.begin(), tolower);
}

void DString::TrimSpaces ()
{
	Int firstMismatch = FindFirstMismatchingIdx(TXT(" "), CC_CaseSensitive, SD_LeftToRight);
	if (firstMismatch == INT_INDEX_NONE)
	{
		//String is all spaces or empty
		Clear();
		return;
	}

	Int lastMismatch = FindFirstMismatchingIdx(TXT(" "), CC_CaseSensitive, SD_RightToLeft);
	SubString(*this, firstMismatch, lastMismatch);
}

Int DString::FindFirstMismatchingIdx (const DString& compare, ECaseComparison caseComparison, ESearchDirection searchDirection) const
{
	if (compare.IsEmpty())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to search for an empty string when looking for the for the first mismatching character within string \"%s\"."), ToString());
		return INT_INDEX_NONE;
	}

	if (caseComparison == CC_IgnoreCase)
	{
		DString insensitiveString(String);
		insensitiveString.ToUpper();

		DString insensitiveSearch(compare);
		insensitiveSearch.ToUpper();
		return insensitiveString.FindFirstMismatchingIdx(insensitiveSearch, CC_CaseSensitive, searchDirection);
	}

	if (searchDirection == SD_LeftToRight)
	{
		Int charIdx = 0;
		StringIterator iter(this);
		while (true)
		{
			for (StringIterator subIter(&compare); !subIter.IsAtEnd(); ++subIter)
			{
				if (*subIter != *iter)
				{
					return charIdx;
				}

				charIdx++;
				++iter;
			}

			if (iter.IsAtEnd())
			{
				break; //The inner loop may have caused the iterator to move to end
			}
		}
	}
	else
	{
		if (IsEmpty())
		{
			return INT_INDEX_NONE;
		}

		Int charIdx = Length() - 1;
		StringIterator iter(this, false);
		while (true)
		{
			StringIterator subIter(&compare, false);
			while (true)
			{
				if (*subIter != *iter)
				{
					return charIdx;
				}

				charIdx--;
				if (subIter.IsAtBeginning() || iter.IsAtBeginning())
				{
					break;
				}

				--iter;
				--subIter;
			}

			if (iter.IsAtBeginning())
			{
				break;
			}

			--iter;
		}
	}

	return INT_INDEX_NONE;
}

bool DString::HasRegexMatch (const DString& pattern) const
{
	std::regex regex(pattern.ToCString());
	std::cmatch match;

	return std::regex_search(ToCString(), OUT match, regex);
}

void DString::FromWideString (const std::wstring& inString)
{
#if USE_CACHED_LENGTHS
	NumCharacters = -1;
#endif

#if USE_WIDE_STRINGS
	String = inString;
	return; //No conversion needed
#endif

#if USE_UTF32
	#if PLATFORM_WINDOWS
		#error The MSVC doesn't support UTF-32 wstrings.
	#else
		#error Please implement std::wstring to UTF32 DStrings for your platform.
	#endif
#elif USE_UTF16
	std::wstring_convert<std::codecvt_utf16<wchar_t>> conversion;
#else //UTF-8 and ansi
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conversion;
#endif
	
	try
	{
		String = conversion.to_bytes(inString);
	}
	catch (std::range_error e)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to convert wstring to a DString due to out of range exception: %s."), DString(e.what()));
		String = TXT("");
	}
}

std::wstring DString::ToWStr () const
{
#if USE_WIDE_STRINGS
	return String; //already a wstring
#else
	//Convert string to wstring
	return std::wstring(String.begin(), String.end());
#endif
}

std::string DString::ToStr () const
{
#if USE_WIDE_STRINGS
	//Convert wstring to string
	return string(String.begin(), String.end());
#else
	return String; //already a string
#endif
}

bool DString::IsValidUTF32String () const
{
	return true;
}

bool DString::IsValidUTF16String () const
{
	return true;
}

bool DString::IsValidUTF8String () const
{
	return utf8::is_valid(String.begin(), String.end());
}

bool DString::DoCharactersMatch (const DString& a, Int aIdx, const DString& b, Int bIdx, Int numElements)
{
	for (Int i = 0; i < numElements; i++)
	{
		if (a[(aIdx + i).Value] != b[(bIdx + i).Value])
		{
			return false;
		}
	}

	return true;
}

#pragma region "External Operators"
bool operator== (const DString& left, const DString& right)
{
	return (left.Compare(right, DString::CC_CaseSensitive) == 0);
}

bool operator== (const DString& left, const TCHAR* right)
{
	return (left.Compare(DString(right), DString::CC_CaseSensitive) == 0);
}

bool operator== (const TCHAR* left, const DString& right)
{
	return (right.Compare(DString(left), DString::CC_CaseSensitive) == 0);
}

bool operator== (const DString& left, const TString& right)
{
	return (left.Compare(DString(right), DString::CC_CaseSensitive) == 0);
}

bool operator== (const TString& left, const DString& right)
{
	return (right.Compare(DString(left), DString::CC_CaseSensitive) == 0);
}

bool operator!= (const DString& left, const DString& right)
{
	return (left.Compare(right, DString::CC_CaseSensitive) != 0);
}

bool operator!= (const DString& left, const TCHAR* right)
{
	return (left.Compare(DString(right), DString::CC_CaseSensitive) != 0);
}

bool operator!= (const TCHAR* left, const DString& right)
{
	return (right.Compare(DString(left), DString::CC_CaseSensitive) != 0);
}

bool operator!= (const DString& left, const TString& right)
{
	return (left.Compare(DString(right), DString::CC_CaseSensitive) != 0);
}

bool operator!= (const TString& left, const DString& right)
{
	return (right.Compare(DString(left), DString::CC_CaseSensitive) != 0);
}

DString operator+ (const DString& left, const DString& right)
{
	return DString(left.ReadString() + right.ReadString());
}

DString operator+ (const DString& left, const TString& right)
{
	return DString(left.ReadString() + right);
}

DString operator+ (const TString& left, const DString& right)
{
	return DString(left + right.ReadString());
}

DString operator+ (const DString& left, const TStringChar* right)
{
#if USE_UTF32
	#error Please implement DString::Operator+ for UTF-32.
#elif USE_UTF16
	#error Please implement DString::Operator+ for UTF-16.
#elif USE_UTF8
	return DString(left.ReadString() + reinterpret_cast<const char*>(right));
#else
	return DString(left.ReadString() + right);
#endif
}

DString operator+ (const TStringChar* left, const DString& right)
{
#if USE_UTF32
	#error Please implement DString::Operator+ for UTF-32.
#elif USE_UTF16
	#error Please implement DString::Operator+ for UTF-16.
#elif USE_UTF8
	return DString(reinterpret_cast<const char*>(left) + right.ReadString());
#else
	return DString(left + right.ReadString());
#endif
}

DString& operator+= (DString& left, const DString& right)
{
	left.EditString() += right.ReadString();
	left.MarkNumCharactersDirty();
	return left;
}

DString& operator+= (DString& left, const TString& right)
{
	left.EditString() += right;
	left.MarkNumCharactersDirty();
	return left;
}

TString& operator+= (TString& left, const DString& right)
{
	left += right.ReadString();
	return left;
}

DString& operator+= (DString& left, const TStringChar* right)
{
#if USE_UTF32
	#error Please implement DString::Operator+= for UTF-32.
#elif USE_UTF16
	#error Please implement DString::Operator+= for UTF-16.
#elif USE_UTF8
	left.EditString() += reinterpret_cast<const char*>(right);
#else
	left.EditString() += right;
#endif

	left.MarkNumCharactersDirty();
	return left;
}
#pragma endregion
SD_END