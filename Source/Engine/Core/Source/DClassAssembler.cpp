/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DClassAssembler.cpp
=====================================================================
*/

#include "ContainerUtils.h"
#include "ClassIterator.h"
#include "Engine.h"
#include "DClass.h"
#include "DClassAssembler.h"

SD_BEGIN
std::vector<DClassAssembler::SDClassInfo> DClassAssembler::PreloadedDClasses = PreloadedDClassesCopy;
std::vector<DClassAssembler::SDClassInfo> DClassAssembler::PreloadedDClassesCopy = PreloadedDClasses;
bool DClassAssembler::bLinkedDClasses = false;
std::vector<DClass*> DClassAssembler::RootClasses;
std::unordered_map<size_t, DClass*> DClassAssembler::AllClasses;

DClass* DClassAssembler::LoadClass (const DString& className, const DString& parentClass)
{
	DClass* classInstance = new DClass(className);

	PreloadedDClasses.emplace_back(SDClassInfo(classInstance, parentClass));
	PreloadedDClassesCopy.emplace_back(SDClassInfo(classInstance, parentClass));

	return classInstance;
}

bool DClassAssembler::AssembleDClasses ()
{
	CHECK_INFO(!IsInitialized(), TXT("The DClassAssembler already assembled its registered DClasses."))

	for (size_t i = 0; i < PreloadedDClasses.size(); ++i)
	{
		if (PreloadedDClasses.at(i).ParentClassName.IsEmpty())
		{
			AddRootClass(PreloadedDClasses.at(i).ClassInstance);
			continue; //root class, no need to find parent.
		}

		//Find parent
		for (size_t parentIter = 0; parentIter < PreloadedDClasses.size(); ++parentIter)
		{
			if (i == parentIter)
			{
				continue; //DClass cannot be parent to itself.
			}

			if (IsChildOf(PreloadedDClasses.at(i), PreloadedDClasses.at(parentIter)))
			{
				LinkDClasses(PreloadedDClasses.at(i), PreloadedDClasses.at(parentIter));
				break;
			}
		}
	}

	//Validate PreloadedDClasses to ensure that there aren't any broken links.
	for (size_t i = 0; i < PreloadedDClasses.size(); i++)
	{
		if (!HasFoundParent(PreloadedDClasses.at(i)))
		{
			Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("Failed to assemble class tree for %s.  The parent expected class %s is not found.  Comparisons are case sensitive."),
				PreloadedDClasses.at(i).ClassInstance->GetDuneClassName(), PreloadedDClasses.at(i).ParentClassName));
			return false;
		}
	}

	//Push DClasses to the all class map
	size_t maxAttempts = 3; //Maximum number of hash collisions permitted
	for (const SDClassInfo& loadedClass : PreloadedDClasses)
	{
		for (size_t attempt = 0; attempt < maxAttempts; ++attempt)
		{
			DString hashStr = loadedClass.ClassInstance->GetDuneClassName();
			if (attempt > 0)
			{
				hashStr += TXT("_") + Int(attempt).ToString();
			}

			size_t hash = hashStr.GenerateHash();
			if (AllClasses.find(hash) != AllClasses.end())
			{
				if (attempt + 1 >= maxAttempts)
				{
					Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("Failed to create DClass map for %s since there are too many hash collisions. Consider either changing the hash algorithm or the class name."), hashStr));
				}

				//Collision hash detected. Try again with a slight variation of the class name.
				continue;
			}

			loadedClass.ClassInstance->ClassHash = hash;
			AllClasses.emplace(hash, loadedClass.ClassInstance);
			break;
		}
	}

	//No need to Preserve the PreloadedDClasses vector.  Free up memory.
	ContainerUtils::Empty(OUT PreloadedDClasses);
	ContainerUtils::Empty(OUT PreloadedDClassesCopy);
	bLinkedDClasses = true;

	return true;
}

void DClassAssembler::DisassembleDClasses ()
{
	//We don't call destroy on DefaultObjects since they are not registered to the hash table.
	std::vector<DClass*> rootClasses = DClassAssembler::GetRootClasses();
	std::vector<const DClass*> classesToDelete;
	for (size_t i = 0; i < rootClasses.size(); i++)
	{
		for (ClassIterator iter(rootClasses.at(i)); iter.SelectedClass; iter++)
		{
			classesToDelete.push_back(iter.SelectedClass);
		}
	}

	for (size_t i = 0; i < classesToDelete.size(); i++)
	{
		delete classesToDelete.at(i);
	}

	AllClasses.clear();
}

size_t DClassAssembler::CalcDClassHash (const DString& className, const DClass*& outClass)
{
	if (!IsInitialized())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot Find DClass by string (%s) since the DClassAssembler is not yet initialized."), className);
		outClass = nullptr;
		return 0;
	}

	size_t maxAttempts = 3;
	for (size_t i = 0; i < maxAttempts; ++i)
	{
		DString hashStr = className;
		if (i > 0)
		{
			hashStr += TXT("_") + Int(i).ToString();
		}

		size_t hash = hashStr.GenerateHash();
		outClass = AllClasses.at(hash);
		if (outClass == nullptr) //nothing is mapped to specified value
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("No DClass is associated with \"%s\"."), className);
			return 0;
		}

		if (outClass->GetDuneClassName().Compare(className, DString::CC_CaseSensitive) != 0)
		{
			//Found a DClass mapped to the hash, but the class name does not match. Assume this means a collision. Try a slight variation of the class name.
			continue;
		}

		return hash;
	}

	CoreLog.Log(LogCategory::LL_Warning, TXT("Found %s hash collisions when searching for a DClass named \"%s\". Consider changing the name or hash algorithm to fix this."), Int(maxAttempts+1), className);
	outClass = nullptr;
	return 0;
}

const DClass* DClassAssembler::FindDClass (const DString& className)
{
	const DClass* result = nullptr;
	CalcDClassHash(className, OUT result);

	return result;
}

const DClass* DClassAssembler::FindDClass (size_t classNameHash)
{
	const DClass* result = AllClasses.at(classNameHash);
	if (result == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("No DClass is mapped to the hash %s."), Int(classNameHash));
	}

	return result;
}

bool DClassAssembler::IsInitialized ()
{
	return bLinkedDClasses;
}

const std::vector<DClass*> DClassAssembler::GetRootClasses ()
{
	return RootClasses;
}

bool DClassAssembler::HasFoundParent (const SDClassInfo& registeredDClass)
{
	if (registeredDClass.ParentClassName.IsEmpty())
	{
		return true; //Root class
	}

	return (registeredDClass.ClassInstance->GetSuperClass() != nullptr);
}

bool DClassAssembler::IsChildOf (const SDClassInfo& childInfo, const SDClassInfo& parentInfo)
{
	if (childInfo.ParentClassName.IsEmpty())
	{
		return false;
	}

	return (childInfo.ParentClassName.Compare(parentInfo.ClassInstance->GetDuneClassName(), DString::CC_CaseSensitive) == 0);
}

void DClassAssembler::LinkDClasses (SDClassInfo& childInfo, SDClassInfo& parentInfo)
{
	parentInfo.ClassInstance->AddChild(childInfo.ClassInstance);
	childInfo.ClassInstance->ParentClass = parentInfo.ClassInstance;
}

void DClassAssembler::AddRootClass (DClass* newRootClass)
{
	size_t index;

	//Added alphabetically for organizational purposes
	for (index = 0; index < RootClasses.size(); index++)
	{
		if (RootClasses.at(index)->GetDuneClassName().Compare(newRootClass->GetDuneClassName(), DString::CC_CaseSensitive) > 0)
		{
			break;
		}
	}

	RootClasses.insert(RootClasses.begin() + index, newRootClass);
}
SD_END