/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourcePool.cpp
=====================================================================
*/

#include "CoreMacros.h"
#include "DClassAssembler.h"
#include "ResourcePool.h"

IMPLEMENT_ABSTRACT_CLASS(SD::ResourcePool, SD::Object)
SD_BEGIN

void ResourcePool::BeginObject ()
{
	Super::BeginObject();

	ThreadID = std::this_thread::get_id();
}

void ResourcePool::Destroyed ()
{
	ReleaseResources();

	Super::Destroyed();
}
SD_END