/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickTester.cpp
=====================================================================
*/

#include "DClass.h"
#include "DClassAssembler.h"
#include "TickComponent.h"
#include "TickGroup.h"
#include "TickTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::TickTester, SD::Entity)
SD_BEGIN

void TickTester::InitProps ()
{
	Super::InitProps();

	OwningTester = nullptr;
	MaxTickCounter = 10;

	bMiscTickFirst = true;
	MiscTickCounter1 = 0;
	MiscTickCounter2 = 0;
	DebugTickCounter1 = 0;
	DebugTickCounter2 = 0;
}

void TickTester::Destroyed ()
{
	UnitTester::TestLog(TestFlags, TXT("TickTester test terminated."));

	Super::Destroyed();
}

void TickTester::BeginTest ()
{
	//OwningTester must be assigned first
	CHECK(OwningTester.IsValid())

	UnitTester::TestLog(TestFlags, TXT("TickTester test initiated."));

	TickComponent* miscTick1 = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (!AddComponent(miscTick1))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the first Misc Tick Component."));
		Destroy();
		return;
	}
	miscTick1->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleMiscTick1, void, Float));

	//Instantiate and attach debug tick before second misc tick to test instantiation order does not change the tick order.
	TickComponent* debugTick1 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(debugTick1))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the first Debug Tick Component."));
		Destroy();
		return;
	}
	debugTick1->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDebugTick1, void, Float));

	TickComponent* miscTick2 = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (!AddComponent(miscTick2))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the second Misc Tick Component."));
		Destroy();
		return;
	}
	miscTick2->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleMiscTick2, void, Float));

	TickComponent* debugTick2 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(debugTick2))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the second Debug Tick Component."));
		Destroy();
		return;
	}
	debugTick2->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDebugTick2, void, Float));

	//Create a TickComponent that should never tick (test IsTicking())
	TickComponent* disabledTick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(disabledTick))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the disabled Tick Component."));
		Destroy();
		return;
	}
	disabledTick->SetTicking(false);
	disabledTick->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDisabledTick, void, Float));
}

void TickTester::ValidateTickTest ()
{
	CHECK(OwningTester.IsValid())

	//Validate TickOrder
	if (bMiscTickFirst)
	{
		if (MiscTickCounter1 < DebugTickCounter1 || MiscTickCounter1 < DebugTickCounter2 ||
			MiscTickCounter2 < DebugTickCounter1 || MiscTickCounter2 < DebugTickCounter2)
		{
			OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a Debug Tick Component ticked before a Misc Tick Component."));
			Destroy();
			return;
		}
	}
	else
	{
		if (DebugTickCounter1 < MiscTickCounter1 || DebugTickCounter1 < MiscTickCounter2 ||
			DebugTickCounter2 < MiscTickCounter1 || DebugTickCounter2 < MiscTickCounter2)
		{
			OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a Misc Tick Component ticked before a Debug Tick Component."));
			Destroy();
			return;
		}
	}

	//Are any TickComponents skipping frames?
	if (Int::Abs(MiscTickCounter1 - MiscTickCounter2) > 1 || Int::Abs(MiscTickCounter2 - DebugTickCounter1) > 1 ||
		Int::Abs(DebugTickCounter1 - DebugTickCounter2) > 1)
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since certain tick components are ticking more frequently than other tick components.  MiscTickCounters:  %s and %s.  DebugTickCounters:  %s and %s."), MiscTickCounter1, MiscTickCounter2, DebugTickCounter1, DebugTickCounter2);
		Destroy();
		return;
	}

	//Check if all TickComponents ticked enough times
	if (MiscTickCounter1 >= MaxTickCounter && MiscTickCounter2 >= MaxTickCounter &&
		DebugTickCounter1 >= MaxTickCounter && DebugTickCounter2 >= MaxTickCounter)
	{
		UnitTester::TestLog(TestFlags, TXT("TickTester completed.  All TickComponents ticked in correct order."));
		Destroy();
		return;
	}
}

void TickTester::HandleMiscTick1 (Float deltaSec)
{
	MiscTickCounter1++;
	ValidateTickTest();
}

void TickTester::HandleMiscTick2 (Float deltaSec)
{
	MiscTickCounter2++;
	ValidateTickTest();
}

void TickTester::HandleDebugTick1 (Float deltaSec)
{
	DebugTickCounter1++;
	ValidateTickTest();
}

void TickTester::HandleDebugTick2 (Float deltaSec)
{
	DebugTickCounter2++;
	ValidateTickTest();
}

void TickTester::HandleDisabledTick (Float deltaSec)
{
	if (OwningTester.IsValid())
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a disabled TickComponent is still ticking."));
	}

	Destroy();
}
SD_END

#endif