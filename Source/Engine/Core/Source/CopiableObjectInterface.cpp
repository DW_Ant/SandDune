/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CopiableObjectInterface.cpp
=====================================================================
*/

#include "CopiableObjectInterface.h"

SD_BEGIN
CopiableObjectInterface* CopiableObjectInterface::Duplicate () const
{
	CopiableObjectInterface* result = CreateCopiableInstanceOfMatchingType();
	if (result != nullptr)
	{
		result->CopyPropertiesFrom(this);
	}

	return result;
}
SD_END