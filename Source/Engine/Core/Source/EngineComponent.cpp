/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineComponent.cpp
=====================================================================
*/

#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "Engine.h"
#include "EngineComponent.h"
#include "LogCategory.h"

IMPLEMENT_ENGINE_COMPONENT_PARENT(SD::EngineComponent,)
SD_BEGIN

EngineComponent::EngineComponent () :
	OwningEngine(nullptr),
	bTickingComponent(false)
{
	//Noop
}

EngineComponent::~EngineComponent ()
{
	//Noop
}

void EngineComponent::RegisterObjectHash ()
{

}

bool EngineComponent::CanRunPreInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies;
	GetPreInitializeDependencies(OUT dependencies);
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

bool EngineComponent::CanRunInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies;
	GetInitializeDependencies(OUT dependencies);
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

bool EngineComponent::CanRunPostInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies;
	GetPostInitializeDependencies(OUT dependencies);
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

void EngineComponent::PreInitializeComponent ()
{
	RegisterEngineComponentInstance();
}

void EngineComponent::InitializeComponent ()
{

}

void EngineComponent::PostInitializeComponent ()
{

}

void EngineComponent::FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg)
{

}

void EngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{

}

void EngineComponent::PreTick (Float deltaSec)
{

}

void EngineComponent::PostTick (Float deltaSec)
{

}

void EngineComponent::ShutdownComponent ()
{
	RemoveEngineComponentInstance();
}

void EngineComponent::SetOwningEngine (Engine* newOwningEngine)
{
	OwningEngine = newOwningEngine;
}

#ifdef DEBUG_MODE
void EngineComponent::ProcessEngineIntegrityTestStep (EEngineIntegrityStep testStep)
{
	//Noop
}
#endif

bool EngineComponent::GetTickingComponent () const
{
	return bTickingComponent;
}

void EngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	//Noop
}

void EngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	//Noop
}

void EngineComponent::GetPostInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	//Noop
}
SD_END