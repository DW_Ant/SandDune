/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DataBuffer.cpp
=====================================================================
*/

#include "DataBuffer.h"
#include "DString.h"
#include "Int.h"
#include "Engine.h"

SD_BEGIN
DataBuffer::DataBuffer () :
	DataBufferIsLittleEndian(DataBuffer::IsSystemLittleEndian()),
	ReadIdx(0),
	bReadError(false)
{
	//Noop
}

DataBuffer::DataBuffer (int numBytes) :
	DataBufferIsLittleEndian(DataBuffer::IsSystemLittleEndian()),
	ReadIdx(0),
	bReadError(false)
{
	if (numBytes > 0)
	{
		Data.reserve(numBytes);
	}
}

DataBuffer::DataBuffer (int numBytes, bool inDataBufferIsLittleEndian) :
	DataBufferIsLittleEndian(inDataBufferIsLittleEndian),
	ReadIdx(0),
	bReadError(false)
{
	if (numBytes > 0)
	{
		Data.reserve(numBytes);
	}
}

DataBuffer::~DataBuffer ()
{
	//Noop
}

bool DataBuffer::operator== (const DataBuffer& other) const
{
	if (Data.size() != other.Data.size())
	{
		return false;
	}

	for (size_t i = 0; i < Data.size(); ++i)
	{
		if (Data.at(i) != other.Data.at(i))
		{
			return false;
		}
	}

	return true;
}

bool DataBuffer::operator!= (const DataBuffer& other) const
{
	return !(*this == other);
}

bool DataBuffer::IsSystemLittleEndian ()
{
	//Nice utility found from here:  http://vijayinterviewquestions.blogspot.com/2007/07/what-little-endian-and-big-endian-how.html
	const int num = 1;

	//Look at the first byte of num to see if it contains 1 or not.
	//Little endian would format 1 as 00000001 00000000 00000000 00000000
	//Big endian would format 1 as 00000000 00000000 00000000 00000001
	return (*(char*)&num == 1);
}

void DataBuffer::SwapByteOrder (char* outCharBuffer, size_t numChars)
{
	char* end = (outCharBuffer + numChars);
	std::reverse(outCharBuffer, end);
}

void DataBuffer::SwapByteOrder ()
{
	for (char& dataByte : Data)
	{
		SwapByteOrder(&dataByte, 1);
	}

	DataBufferIsLittleEndian = !DataBufferIsLittleEndian;
}

bool DataBuffer::CanReadBytes (size_t numBytes) const
{
	return (ReadIdx + numBytes <= Data.size());
}

void DataBuffer::AdvanceIdx (size_t numBytes) const
{
	ReadIdx += numBytes;
	CHECK(ReadIdx >= 0 && ReadIdx <= Data.size())
}

void DataBuffer::ReadBytes (char* OUT outReadData, size_t numBytes) const
{
	CHECK((ReadIdx + numBytes) <= Data.size())
	CHECK(numBytes != UINT_INDEX_NONE) //Protection against inf loop (for unsigned int overflow)

	for (size_t i = 0; i < numBytes; ++i)
	{
		outReadData[i] = Data[ReadIdx + i];
	}

	AdvanceIdx(numBytes);
}

void DataBuffer::AppendBytes (const char* dataToAdd, size_t numBytes)
{
	CHECK(numBytes != UINT_INDEX_NONE) //Protection against inf loop (for unsigned int overflow)

	size_t startIdx = Data.size();
	Data.resize(Data.size() + numBytes);

	for (size_t i = 0; i < numBytes; ++i)
	{
		Data[i + startIdx] = dataToAdd[i];
	}
}

void DataBuffer::AppendBytes (const DataBuffer& source)
{
	size_t startIdx = Data.size();
	Data.resize(Data.size() + source.GetNumBytes());

	for (size_t i = 0; i < source.GetNumBytes(); ++i)
	{
		Data[i + startIdx] = source.Data[i];
	}
}

void DataBuffer::EditBytes (size_t writeIdx, const char* dataToWrite, size_t numBytes)
{
	CHECK(numBytes != UINT_INDEX_NONE)

	size_t numWritten = 0;
	for (size_t i = writeIdx; i < Data.size() && numWritten < numBytes; ++i)
	{
		Data[i] = dataToWrite[numWritten];
		++numWritten;
	}

	if (numWritten < numBytes)
	{
		//Append the rest of the bytes to the end of the data buffer
		AppendBytes(&dataToWrite[numWritten], numBytes - numWritten);
	}
}

void DataBuffer::EditBytes (size_t writeIdx, const DataBuffer& source)
{
	size_t numWritten = 0;
	for (size_t i = writeIdx; i < Data.size() && numWritten < source.GetNumBytes(); ++i)
	{
		Data[i] = source.Data[numWritten];
		++numWritten;
	}

	if (numWritten < source.GetNumBytes())
	{
		//Append the rest of the bytes to the end of the data buffer
		size_t originalSize = Data.size();
		Data.resize(Data.size() + (source.GetNumBytes() - numWritten));

		for (size_t i = originalSize; i < Data.size(); ++i)
		{
			Data[i] = source.Data.at(numWritten);
			++numWritten;
		}
	}
}

void DataBuffer::InsertBytes (size_t insertIdx, const char* dataToWrite, size_t numBytes)
{
	CHECK(numBytes != UINT_INDEX_NONE)

	if (insertIdx >= Data.size())
	{
		AppendBytes(dataToWrite, numBytes);
		return;
	}

	size_t originalLastIdx = Data.size() - 1;
	Data.resize(Data.size() + numBytes);

	//Move existing bytes to their new elements
	for (size_t srcIdx = insertIdx; srcIdx <= originalLastIdx; ++srcIdx)
	{
		Data.at(srcIdx + numBytes) = Data[srcIdx];
	}

	//Insert the char data to the inserted data
	for (size_t i = 0; i < numBytes; ++i)
	{
		Data[insertIdx + i] = dataToWrite[i];
	}
}

void DataBuffer::InsertBytes (size_t insertIdx, const DataBuffer& source)
{
	if (insertIdx >= Data.size())
	{
		AppendBytes(source);
		return;
	}

	size_t originalLastIdx = Data.size() - 1;
	Data.resize(Data.size() + source.GetNumBytes());

	//Move existing bytes to their new elements
	for (size_t srcIdx = insertIdx; srcIdx < originalLastIdx; ++srcIdx)
	{
		Data.at(srcIdx + source.GetNumBytes()) = Data[srcIdx];
	}

	//Insert the source to the index position.
	for (size_t i = 0; i < source.GetNumBytes(); ++i)
	{
		Data[insertIdx + i] = source.Data[i];
	}
}

void DataBuffer::RemoveBytes (size_t idx, size_t numBytesToRemove)
{
	if (Data.size() == 0)
	{
		return;
	}

	if (idx >= Data.size())
	{
		idx = Data.size() - 1;
	}

	if (numBytesToRemove + idx > Data.size())
	{
		numBytesToRemove = Data.size() - idx;
	}

	if (numBytesToRemove > 0)
	{
		Data.erase(Data.begin() + idx, Data.begin() + idx + numBytesToRemove);
	}
}

void DataBuffer::CopyBufferTo (DataBuffer& outDestination) const
{
	outDestination.EmptyBuffer();

	size_t numBytes = GetNumBytes();
	if (numBytes > 0)
	{
		size_t oldReadIdx = GetReadIdx();
		JumpToBeginning();

		char* dataCopy = new char[numBytes];
		ReadBytes(dataCopy, numBytes);
		outDestination.AppendBytes(dataCopy, numBytes);
		delete[] dataCopy;

		SetReadIdx(oldReadIdx);
	}
}

void DataBuffer::SerializeInt (size_t vectorSize)
{
	Int numElements(vectorSize);
	numElements.Serialize(*this);
}

bool DataBuffer::DeserializeInt (size_t& outIntValue) const
{
	Int numElements;
	if (!numElements.Deserialize(*this))
	{
		MarkReadError();
		return false;
	}

	outIntValue = numElements.ToUnsignedInt();
	return true;
}
SD_END