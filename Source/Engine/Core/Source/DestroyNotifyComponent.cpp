/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DestroyNotifyComponent.cpp
=====================================================================
*/

#include "DClassAssembler.h"
#include "DClass.h"
#include "DestroyNotifyComponent.h"

IMPLEMENT_CLASS(SD::DestroyNotifyComponent, SD::EntityComponent)
SD_BEGIN

void DestroyNotifyComponent::InitProps ()
{
	Super::InitProps();

	MostRecentOwner = nullptr;
}

void DestroyNotifyComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	MostRecentOwner = Owner.Get();
}

void DestroyNotifyComponent::Destroyed ()
{
	if (OnDestroyed.IsBounded())
	{
		OnDestroyed(this);
	}

	Super::Destroyed();
}
SD_END