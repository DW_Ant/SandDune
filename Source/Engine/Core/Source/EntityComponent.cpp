/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EntityComponent.cpp
=====================================================================
*/

#include "ComponentIterator.h"
#include "DClass.h"
#include "DClassAssembler.h"
#include "EntityComponent.h"

IMPLEMENT_CLASS(SD::EntityComponent, SD::Entity)
SD_BEGIN

void EntityComponent::InitProps ()
{
	Super::InitProps();

	Owner = nullptr;
	NumInvisibleOwners = 0;
}

void EntityComponent::AddComponentModifier (EntityComponent* newComponent)
{
	Super::AddComponentModifier(newComponent);

	if (Owner.IsValid())
	{
		//Notify the parent components to also adjust their modifiers
		Owner->AddComponentModifier(newComponent);
	}
}

bool EntityComponent::RemoveComponentModifier (EntityComponent* oldComponent)
{
	if (Super::RemoveComponentModifier(oldComponent) && Owner.IsValid())
	{
		Owner->RemoveComponentModifier(oldComponent);
		return true;
	}

	return false;
}

bool EntityComponent::IsVisible () const
{
	return (Super::IsVisible() && NumInvisibleOwners <= 0);
}

unsigned int EntityComponent::CalculateHashID () const
{
	return (Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetComponentHashNumber());
}

void EntityComponent::Destroyed ()
{
	DetachSelfFromOwner();

	Super::Destroyed();
}

Entity* EntityComponent::GetRootEntity () const
{
	if (EntityComponent* comp = dynamic_cast<EntityComponent*>(Owner.Get()))
	{
		if (Entity* root = comp->GetRootEntity())
		{
			return root;
		}

		return comp; //comp is not attached to anything. Return comp instead.
	}

	return Owner.Get();
}

bool EntityComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	return (ownerCandidate != nullptr && ownerCandidate != this);
}

void EntityComponent::DetachSelfFromOwner ()
{
	if (Owner.IsNullptr())
	{
		return; //already detached
	}

	Owner->RemoveComponent(this);
}

Entity* EntityComponent::GetOwner () const
{
	return Owner.Get();
}

void EntityComponent::AttachTo (Entity* newOwner)
{
	if (Owner == newOwner)
	{
		return;
	}

	Entity* oldOwner = Owner.Get();
	Owner = newOwner;

	//Identify how many EntityComponents are invisible in this new ownership chain.
	EntityComponent* owningComponent = dynamic_cast<EntityComponent*>(Owner.Get());
	if (owningComponent != nullptr)
	{
		NumInvisibleOwners = owningComponent->NumInvisibleOwners;
		if (!owningComponent->bVisible)
		{
			NumInvisibleOwners++;
		}

		if (NumInvisibleOwners > 0)
		{
			for (ComponentIterator subComponents(this, true); subComponents.GetSelectedComponent() != nullptr; subComponents++)
			{
				subComponents.GetSelectedComponent()->NumInvisibleOwners += NumInvisibleOwners;
			}
		}
	}
	else if (Owner.IsValid() && !Owner->IsVisible())
	{
		NumInvisibleOwners = 1;
	}

	OnOwnerChanged.Broadcast(this, oldOwner);
}

void EntityComponent::ComponentDetached ()
{
	if (Owner == nullptr)
	{
		return;
	}

	Entity* oldOwner = Owner.Get();
	Owner = nullptr;

	//Update this component and subcomponents' NumInvisibleOwners counter
	if (NumInvisibleOwners > 0)
	{
		Int numToDeduct = NumInvisibleOwners;
		if (!bVisible)
		{
			++numToDeduct;
		}

		NumInvisibleOwners = 0;
		for (ComponentIterator subComponents(this, true); subComponents.GetSelectedComponent(); subComponents++)
		{
			subComponents.GetSelectedComponent()->NumInvisibleOwners -= numToDeduct;
			CHECK(subComponents.GetSelectedComponent()->NumInvisibleOwners >= 0)
		}
	}

	OnOwnerChanged.Broadcast(this, oldOwner);
}
SD_END