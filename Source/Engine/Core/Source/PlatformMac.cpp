/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMac.cpp
=====================================================================
*/

#include "PlatformMac.h"

#ifdef PLATFORM_MAC

SD_BEGIN
int strerror_s (char *buffer, size_t numberOfElements, int errnum)
{
	buffer = strerror(errnum);
	return 0;
}

void InitializePlatform (bool bMainThread)
{
	//Noop
}

void ShutdownPlatform (bool bMainThread)
{
	//Noop
}

void PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle)
{
#error Implement this function 'PlatformOpenWindow'
}

void OS_Sleep (Int milliseconds)
{
	CHECK(milliseconds > 0)
#error Implement this function 'OS_Sleep'
}

bool OS_CopyToClipboard (const DString& copyContent)
{
#error Implement this function 'OS_CopyToClipboard'
	return false;
}

DString OS_PasteFromClipboard ()
{
#error Implement this function 'OS_PasteFromClipboard'
	return TXT("");
}

void OS_BreakExecution ()
{
#error Implement this function 'OS_BreakExecution'
}
SD_END

#endif