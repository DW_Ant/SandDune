/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickComponent.cpp
=====================================================================
*/

#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "EntityComponent.h"
#include "LogCategory.h"
#include "TickComponent.h"
#include "TickGroup.h"

IMPLEMENT_CLASS(SD::TickComponent, SD::EntityComponent)
SD_BEGIN

TickComponent::TickComponent () : Super()
{
	PendingTickGroupName = TICK_GROUP_MISC;
}

TickComponent::TickComponent (const DString& inTickGroup) : Super()
{
	PendingTickGroupName = inTickGroup;
}

void TickComponent::InitProps ()
{
	Super::InitProps();

	TickInterval = -1.f; //Update every frame
	bTicking = true;
	OwningTickGroup = nullptr;
	OnTick.ClearFunction();

	AccumulatedDeltaTime = 0.f;
}

void TickComponent::BeginObject ()
{
	Super::BeginObject();

	Engine* curEngine = Engine::FindEngine();
	CHECK(curEngine != nullptr) //TickComponents must register to an Engine

	OwningTickGroup = curEngine->FindTickGroup(PendingTickGroupName);
	if (OwningTickGroup == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("The TickGroup (%s) is not registered to the local Engine.  The TickComponent cannot Tick."), PendingTickGroupName);
		return;
	}

	OwningTickGroup->RegisterTick(this);
}

#ifdef DEBUG_MODE
DString TickComponent::GetFriendlyName () const
{
	if (OnTick.IsBounded())
	{
		//Return a string that's more descriptive
		return OnTick.ReadFriendlyName();
	}

	return Super::GetFriendlyName();
}
#endif

unsigned int TickComponent::CalculateHashID () const
{
	return Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetTickComponentHashNumber();
}

void TickComponent::Destroyed ()
{
	if (OwningTickGroup != nullptr && IsTicking())
	{
		SetTicking(false);
	}

	Super::Destroyed();
}

TickComponent* TickComponent::CreateObject (const DString& inTickGroup)
{
	TickComponent* newObject = new TickComponent(inTickGroup);
	if (newObject == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Fatal, TXT("Failed to allocate memory to instantiate a TickComponent!"));
		return nullptr;
	}

	newObject->InitializeObject();

	return newObject;
}

void TickComponent::Tick (Float deltaSec)
{
	AccumulatedDeltaTime += deltaSec;
	if (AccumulatedDeltaTime >= TickInterval)
	{
		if (OnTick.IsBounded())
		{
			OnTick(AccumulatedDeltaTime);
		}
		else if (OnTick_Ex.IsBounded())
		{
			OnTick_Ex(AccumulatedDeltaTime, this);
		}

		ResetAccumulatedTickTime();
	}
}

void TickComponent::ResetAccumulatedTickTime ()
{
	AccumulatedDeltaTime = 0.f;
}

void TickComponent::ForceTickNextFrame ()
{
	AccumulatedDeltaTime = TickInterval;
}

void TickComponent::SetTickInterval (Float newTickInterval)
{
	TickInterval = newTickInterval;
}

void TickComponent::SetTickHandler (SDFunction<void, Float> newHandler)
{
	OnTick = newHandler;
	OnTick_Ex.ClearFunction();
}

void TickComponent::SetTickExHandler (SDFunction<void, Float, TickComponent*> newHandler)
{
	OnTick_Ex = newHandler;
	OnTick.ClearFunction();
}

void TickComponent::SetTicking (bool bNewTicking)
{
	if (bNewTicking == bTicking)
	{
		return;
	}

	if (OwningTickGroup == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot activate Tick component since the specified Tick component is not associated with a TickGroup."));
		bTicking = false;
		return;
	}

	bTicking = bNewTicking;
	if (bTicking)
	{
		OwningTickGroup->RegisterTick(this);
	}
	else
	{
		OwningTickGroup->RemoveTick(this);
	}
}

bool TickComponent::IsTicking () const
{
	return bTicking;
}
SD_END