/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerInterface.cpp
=====================================================================
*/

#include "DPointerBase.h"
#include "DPointerInterface.h"

SD_BEGIN
void DPointerInterface::ClearPointersPointingAtThis ()
{
	for (DPointerBase* curPointer = GetLeadingDPointer(); curPointer != nullptr; /* Noop */ )
	{
		DPointerBase* nextPointer = curPointer->GetNextPointer();
		curPointer->ClearPointer();
		curPointer = nextPointer;
	}
}
SD_END