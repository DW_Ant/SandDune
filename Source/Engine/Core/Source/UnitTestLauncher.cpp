/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTestLauncher.cpp
=====================================================================
*/

#include "ClassIterator.h"
#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "Object.h"
#include "UnitTestLauncher.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::UnitTestLauncher, SD::Object)
SD_BEGIN

bool UnitTestLauncher::RunAllTests (UnitTester::EUnitTestFlags testFlags)
{
	//Generate a list of unit tests that needs to run
	std::vector<const UnitTester*> incompleteTests;
	for (ClassIterator iter(UnitTester::SStaticClass()); iter.SelectedClass; iter++)
	{
		const UnitTester* unitTester = dynamic_cast<const UnitTester*>(iter.SelectedClass->GetDefaultObject());
		if (unitTester != nullptr)
		{
			incompleteTests.push_back(unitTester);
		}
	}

	std::vector<const UnitTester*> completedTests;
	while(incompleteTests.size() > 0)
	{
		Int oldIncompleteSize = incompleteTests.size();

		UINT_TYPE i = 0;
		while (i < incompleteTests.size())
		{
			if (!incompleteTests.at(i)->MetRequirements(completedTests))
			{
				//Not all requirements are met.  Try next test.
				i++;
				continue;
			}

			if (incompleteTests.at(i)->IsTestEnabled(testFlags)) //If disabled, assume this test returned true
			{
				if (!incompleteTests.at(i)->RunTests(testFlags))
				{
					return false;
				}
			}

			completedTests.push_back(incompleteTests.at(i));
			incompleteTests.erase(incompleteTests.begin() + i);
		}

		if (oldIncompleteSize == incompleteTests.size())
		{
			UnitTester::TestLog(testFlags, TXT("Unable to complete all unit tests since there's a circular dependency.  The following unit tests depend on each other:"));
			for (Int i = 0; i < incompleteTests.size(); i++)
			{
				UnitTester::TestLog(testFlags, TXT("    [%s]:  %s"), i, incompleteTests.at(i.ToUnsignedInt())->GetName());
			}

			return false;
		}
	}

	UnitTester::TestLog(testFlags, TXT("========================"));
	UnitTester::TestLog(testFlags, TXT("All unit tests PASSED!"));
	UnitTester::TestLog(testFlags, TXT("========================"));
	UnitTester::TestLog(testFlags, TXT(""));

	return true;
}
SD_END

#endif