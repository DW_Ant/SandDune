/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TransformMatrix.cpp
=====================================================================
*/

#include "Engine.h"
#include "Int.h"
#include "Rotator.h"
#include "SDFloat.h"
#include "TransformMatrix.h"
#include "Vector3.h"

SD_BEGIN
const TransformMatrix TransformMatrix::IDENTITY_TRANSFORM(Matrix(4, 4,
	{
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	}));

TransformMatrix::TransformMatrix () : Matrix(IDENTITY_TRANSFORM),
	ScaleMultipliers(1.f, 1.f, 1.f)
{
	//Noop
}

TransformMatrix::TransformMatrix (const Matrix& copyObj) : Matrix(copyObj)
{
	CHECK(GetNumRows() == 4 && GetNumColumns() == 4)

	ScaleMultipliers.X = Data.at(ScaleX);
	ScaleMultipliers.Y = Data.at(ScaleY);
	ScaleMultipliers.Z = Data.at(ScaleZ);
}

TransformMatrix::TransformMatrix (const Vector3& inScaleMultipliers, const Matrix& copyObj) : Matrix(copyObj),
	ScaleMultipliers(inScaleMultipliers)
{

}

TransformMatrix::TransformMatrix (const TransformMatrix& copyObj) : Matrix(copyObj),
	ScaleMultipliers(copyObj.ScaleMultipliers)
{
	//Noop
}

TransformMatrix::~TransformMatrix ()
{
	//Noop
}

void TransformMatrix::operator= (const TransformMatrix& copyMatrix)
{
	Matrix::operator=(copyMatrix);

	ScaleMultipliers = copyMatrix.ScaleMultipliers;
}

void TransformMatrix::Serialize (DataBuffer& outData) const
{
	Matrix::Serialize(outData);

	ScaleMultipliers.Serialize(OUT outData);
}

bool TransformMatrix::Deserialize (const DataBuffer& dataBuffer)
{
	if (!Matrix::Deserialize(dataBuffer))
	{
		return false;
	}

	return ScaleMultipliers.Deserialize(dataBuffer);
}

void TransformMatrix::MatrixAdd (const Matrix& addBy)
{
	Matrix::MatrixAdd(addBy);

	if (addBy.GetNumColumns() >= 3 && addBy.GetNumRows() >= 3)
	{
		for (Int i = 0; i < 3; ++i)
		{
			ScaleMultipliers[i.Value] += addBy.GetDataElement(i, i);
		}
	}
}

void TransformMatrix::MatrixSubtract (const Matrix& subtractBy)
{
	Matrix::MatrixSubtract(subtractBy);

	if (subtractBy.GetNumColumns() >= 3 && subtractBy.GetNumRows() >= 3)
	{
		for (Int i = 0; i < 3; ++i)
		{
			ScaleMultipliers[i.Value] -= subtractBy.GetDataElement(i, i);
		}
	}
}

void TransformMatrix::MatrixMultiply (Float multiplier)
{
	Matrix::MatrixMultiply(multiplier);

	ScaleMultipliers *= multiplier;
}

void TransformMatrix::MatrixMultiply (const Matrix& multiplier)
{
	Matrix::MatrixMultiply(multiplier);

	if (const TransformMatrix* transMultiplier = dynamic_cast<const TransformMatrix*>(&multiplier))
	{
		ScaleMultipliers *= transMultiplier->ScaleMultipliers;
	}
}

bool TransformMatrix::SetData (const std::vector<Float>& newData)
{
	return Matrix::SetData(newData);
}

TransformMatrix TransformMatrix::GetRotationMatrixAboutXAxis (Float theta)
{
	Float cosTheta = std::cos(theta.Value);
	Float sinTheta = std::sin(theta.Value);

	return TransformMatrix(Vector3(1.f, 1.f, 1.f), Matrix(4, 4,
	{
		1.f,		0.f,		0.f,		0.f,
		0.f,		cosTheta,	sinTheta,	0.f,
		0.f,		-sinTheta,	cosTheta,	0.f,
		0.f,		0.f,		0.f,		1.f

	}));
}

TransformMatrix TransformMatrix::GetRotationMatrixAboutYAxis (Float theta)
{
	Float cosTheta = std::cos(theta.Value);
	Float sinTheta = std::sin(theta.Value);
	return TransformMatrix(Vector3(1.f, 1.f, 1.f), Matrix(4, 4,
	{
		cosTheta,	0.f,		-sinTheta,	0.f,
		0.f,		1.f,		0.f,		0.f,
		sinTheta,	0.f,		cosTheta,	0.f,
		0.f,		0.f,		0.f,		1.f
	}));
}

TransformMatrix TransformMatrix::GetRotationMatrixAboutZAxis (Float theta)
{
	Float cosTheta = std::cos(theta.Value);
	Float sinTheta = std::sin(theta.Value);
	return TransformMatrix(Vector3(1.f, 1.f, 1.f), Matrix(4, 4,
	{
		cosTheta,	sinTheta,	0.f,	0.f,
		-sinTheta,	cosTheta,	0.f,	0.f,
		0.f,		0.f,		1.f,	0.f,
		0.f,		0.f,		0.f,	1.f
	}));
}

void TransformMatrix::SetTranslation (const Vector3& newTranslation)
{
	Data.at(EMatrixElement::X) = newTranslation.X;
	Data.at(EMatrixElement::Y) = newTranslation.Y;
	Data.at(EMatrixElement::Z) = newTranslation.Z;
}

void TransformMatrix::Translate (const Vector3& translateOffset)
{
#if 0
	TransformMatrix translation(Vector3(1.f, 1.f, 1.f), Matrix(4, 4,
	{
		1.f,	0.f,	0.f,	translateOffset.X,
		0.f,	1.f,	0.f,	translateOffset.Y,
		0.f,	0.f,	1.f,	translateOffset.Z,
		0.f,	0.f,	0.f,	1.f
	}));

	MatrixMultiply(translation);
#else
	//Shortcut (avoids taking dot product of the columns to the left of the translation column).
	std::vector<Float> rightCol{translateOffset.X, translateOffset.Y, translateOffset.Z, 1.f};

	//X axis
	std::vector<Float> leftRow = GetRow(0);
	Data.at(EMatrixElement::X) = DotProduct(leftRow, rightCol);

	//Y axis
	leftRow = GetRow(1);
	Data.at(EMatrixElement::Y) = DotProduct(leftRow, rightCol);

	//Z axis
	leftRow = GetRow(2);
	Data.at(EMatrixElement::Z) = DotProduct(leftRow, rightCol);
#endif
}

void TransformMatrix::SetScale (const Vector3& newScale)
{
	//Remove old scale components.
	if (ScaleMultipliers.X != 0.f)
	{
		Data[0] /= ScaleMultipliers.X;
		Data[4] /= ScaleMultipliers.X;
		Data[8] /= ScaleMultipliers.X;
	}

	if (ScaleMultipliers.Y != 0.f)
	{
		Data[1] /= ScaleMultipliers.Y;
		Data[5] /= ScaleMultipliers.Y;
		Data[9] /= ScaleMultipliers.Y;
	}

	if (ScaleMultipliers.Z != 0.f)
	{
		Data[2] /= ScaleMultipliers.Z;
		Data[6] /= ScaleMultipliers.Z;
		Data[10] /= ScaleMultipliers.Z;
	}

	ScaleMultipliers = newScale;

	//Apply the new scalar components
	Data[0] *= ScaleMultipliers.X;
	Data[1] *= ScaleMultipliers.Y;
	Data[2] *= ScaleMultipliers.Z;

	Data[4] *= ScaleMultipliers.X;
	Data[5] *= ScaleMultipliers.Y;
	Data[6] *= ScaleMultipliers.Z;

	Data[8] *= ScaleMultipliers.X;
	Data[9] *= ScaleMultipliers.Y;
	Data[10] *= ScaleMultipliers.Z;
}

void TransformMatrix::Scale (const Vector3& scaleBy)
{
	TransformMatrix multiplier(scaleBy, Matrix(4, 4,
	{
		scaleBy.X,	0.f,		0.f,		0.f,
		0.f,		scaleBy.Y,	0.f,		0.f,
		0.f,		0.f,		scaleBy.Z,	0.f,
		0.f,		0.f,		0.f,		1.f
	}));

	MatrixMultiply(multiplier);
}

void TransformMatrix::Rotate (const Rotator& rotation)
{
	if (rotation.Yaw != 0)
	{
		(*this) *= GetRotationMatrixAboutZAxis(rotation.GetYaw(Rotator::ERotationUnit::RU_Radians));
	}

	if (rotation.Pitch != 0)
	{
		(*this) *= GetRotationMatrixAboutYAxis(rotation.GetPitch(Rotator::ERotationUnit::RU_Radians));
	}

	if (rotation.Roll != 0)
	{
		(*this) *= GetRotationMatrixAboutXAxis(rotation.GetRoll(Rotator::ERotationUnit::RU_Radians));
	}
}

void TransformMatrix::CalcTransformAttributes (Vector3& outTranslation, Vector3& outScale, Rotator& outRotation) const
{
	outTranslation = GetTranslation();
	outScale = GetScale();
	outRotation = GetRotation();
}

Vector3 TransformMatrix::GetTranslation () const
{
	return Vector3(Data.at(EMatrixElement::X), Data.at(EMatrixElement::Y), Data.at(EMatrixElement::Z));
}

Vector3 TransformMatrix::GetScale () const
{
	return ScaleMultipliers;

#if 0 //This method does not work for negative scalar values.
	Vector3 results;

	results.X = Vector3(Data[0], Data[4], Data[8]).VSize(true);
	results.Y = Vector3(Data[1], Data[5], Data[9]).VSize(true);
	results.Z = Vector3(Data[2], Data[6], Data[10]).VSize(true);

	return results;
#endif
}

Rotator TransformMatrix::GetRotation () const
{
	TransformMatrix rotationMatrix;
	GetRotationMatrix(OUT rotationMatrix);

	CHECK(rotationMatrix.Data.at(0) != 0.f && rotationMatrix.Data.at(5) != 0.f && rotationMatrix.Data.at(10) != 0.f)

	//Formulae are pulled from this site: http://planning.cs.uiuc.edu/node103.html
#if 0 //Right handed
	float yaw = std::atan2f(rotationMatrix.Data.at(4).Value, rotationMatrix.Data.at(0).Value);
	float pitch = std::atan2f(-rotationMatrix.Data.at(8).Value, std::sqrtf(std::powf(rotationMatrix.Data.at(9).Value, 2) + std::powf(rotationMatrix.Data.at(10).Value, 2)));
	float roll = std::atan2f(rotationMatrix.Data.at(9).Value, rotationMatrix.Data.at(10).Value);
#else //Left handed
	//Formulae are based on the answer from: https://gamedev.stackexchange.com/a/112271
	//But done for SD's left handed transform matrix

	/*
	For a Yaw * Pitch * Roll transform matrix.
	C=cosine
	S=Sine
	x=Rotation about X axis (roll)
	y=Rotation about Y axis (pitch)
	Z=Rotation about Z axis (yaw)
	[CzCy				SzCx+CzSySx			SzSx-CxCzSy		0]
	[-SzCy				CzCx-SzSySx			CzSx+SzSyCx		0]
	[Sy					-SxCy				CyCx			0]
	[0					0					0				1]
	*/

	//Default to 0 for instances where both elements in the arctan functions are essentially 0 divide by 0. The scalars are too volatile in that instance so snap to 0 to mitigate floating point precision errors.
	const float divisionTolerance = 0.0001f;
	float roll = 0.f;
	float pitch = 0.f;
	float yaw = 0.f;

	if (!rotationMatrix.Data.at(9).IsCloseTo(0.f, divisionTolerance) || !rotationMatrix.Data.at(10).IsCloseTo(0.f, divisionTolerance))
	{
		//Pick elements 9 and 10 since those have Sin(x) and Cos(x). Invert 9 due to negative element. We can ignore the Cos(y) in both elements since it's scaling both sides in the triangle evenly. Tangent of a scaled triangle does not change.
		roll = std::atan2f(-rotationMatrix.Data.at(9).Value, rotationMatrix.Data.at(10).Value);
	}

	//To find Sin(y), use element [8]. Use Pythagorean Identity to solve for Cos(y)
	float cosY = std::sqrt(1.f - std::powf(rotationMatrix.Data.at(8).Value, 2.f));
	pitch = std::atan2f(rotationMatrix.Data.at(8).Value, cosY);

	if (!rotationMatrix.Data.at(4).IsCloseTo(0.f, divisionTolerance) || !rotationMatrix.Data.at(0).IsCloseTo(0.f, divisionTolerance))
	{
		//Similar how we found roll, use elements 0 and 4 for the atan's ratio.
		yaw = std::atan2f(-rotationMatrix.Data.at(4).Value, rotationMatrix.Data.at(0).Value);
	}
#endif

	return Rotator(yaw, pitch, roll, Rotator::RU_Radians);
}

void TransformMatrix::GetRotationMatrix (TransformMatrix& outRotationMatrix) const
{
	CHECK(ScaleMultipliers.X != 0.f && ScaleMultipliers.Y != 0.f && ScaleMultipliers.Z != 0.f)

	outRotationMatrix = TransformMatrix(ScaleMultipliers, Matrix(4, 4,
	{
		Data.at(0) / ScaleMultipliers.X,	Data.at(1) / ScaleMultipliers.Y,	Data.at(2) / ScaleMultipliers.Z,	Data.at(3),
		Data.at(4) / ScaleMultipliers.X,	Data.at(5) / ScaleMultipliers.Y,	Data.at(6) / ScaleMultipliers.Z,	Data.at(7),
		Data.at(8) / ScaleMultipliers.X,	Data.at(9) / ScaleMultipliers.Y,	Data.at(10) / ScaleMultipliers.Z,	Data.at(11),
		Data.at(12),						Data.at(13),						Data.at(14),				Data.at(15)
	}));
}

#pragma region "External Operators"
TransformMatrix operator+ (const TransformMatrix& left, const TransformMatrix& right)
{
	TransformMatrix result(left);
	result.MatrixAdd(right);

	return result;
}

TransformMatrix& operator+= (TransformMatrix& left, const TransformMatrix& right)
{
	left.MatrixAdd(right);
	return left;
}

TransformMatrix operator- (const TransformMatrix& left, const TransformMatrix& right)
{
	TransformMatrix result(left);
	result.MatrixSubtract(right);

	return result;
}

TransformMatrix& operator-= (TransformMatrix& left, const TransformMatrix& right)
{
	left.MatrixSubtract(right);
	return left;
}

TransformMatrix operator* (const TransformMatrix& left, Float right)
{
	TransformMatrix result(left);
	result.MatrixMultiply(right);

	return left;
}

TransformMatrix operator* (Float left, const TransformMatrix& right)
{
	TransformMatrix result(right);
	result.MatrixMultiply(left);

	return result;
}

TransformMatrix operator* (const TransformMatrix& left, const TransformMatrix& right)
{
	TransformMatrix result(left);
	result.MatrixMultiply(right);

	return result;
}

TransformMatrix& operator*= (TransformMatrix& left, Float right)
{
	left.MatrixMultiply(right);
	return left;
}

TransformMatrix& operator*= (TransformMatrix& left, const TransformMatrix& right)
{
	left.MatrixMultiply(right);
	return left;
}

TransformMatrix operator/ (const TransformMatrix& left, Float right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (TransformMatrix / Float)"), left);
		return left;
	}
#endif

	TransformMatrix result(left);
	result.MatrixMultiply(1.f/right);

	return result;
}

TransformMatrix operator/ (const TransformMatrix& left, const TransformMatrix& right)
{
	TransformMatrix rightInverse = right.CalculateInverse();
	if (!rightInverse.IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide %s by %s since right hand side does not have an inverse matrix."), left, right);
		return left;
	}

	TransformMatrix result(left);
	result.MatrixMultiply(rightInverse);

	return result;
}

TransformMatrix& operator/= (TransformMatrix& left, Float right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (TransformMatrix /= Float)"), left);
		return left;
	}
#endif

	left.MatrixMultiply(1.f/right);
	return left;
}

TransformMatrix& operator/= (TransformMatrix& left, const TransformMatrix& right)
{
	TransformMatrix rightInverse = right.CalculateInverse();
	if (!rightInverse.IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide %s by %s since right hand side does not have an inverse matrix."), left, right);
		return left;
	}

	left.MatrixMultiply(rightInverse);
	return left;
}
#pragma endregion
SD_END