/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreUtilUnitTester.cpp
=====================================================================
*/

#include "ContainerUtils.h"
#include "CoreUtilUnitTester.h"
#include "Core.h"
#include "DClassAssembler.h"
#include "DClass.h"
#include "LogCategory.h"
#include "UnitTester.h"
#include "Utils.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::CoreUtilUnitTester, SD::UnitTester)
SD_BEGIN

bool CoreUtilUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestCoreUtils(testFlags) && TestContainerUtils(testFlags));
	}

	return true;
}

bool CoreUtilUnitTester::TestCoreUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Core Utils"));

	SetTestCategory(testFlags, TXT("Numeric Utils"));
	if (!TestNumericTemplateUtils<int>(testFlags))
	{
		return false;
	}

	//Test integer templates
	{
		int input = 4;
		bool bIsPow2 = Utils::IsPowerOf2(input);
		if (!bIsPow2)
		{
			UnitTestError(testFlags, TXT("%s should have been considered a power of 2."), DString::MakeString(input));
			return false;
		}

		input = 12;
		bIsPow2 = Utils::IsPowerOf2(input);
		if (bIsPow2)
		{
			UnitTestError(testFlags, TXT("%s should not be considered a power of 2."), DString::MakeString(input));
			return false;
		}

		input = -32;
		bIsPow2 = Utils::IsPowerOf2(input);
		if (bIsPow2)
		{
			UnitTestError(testFlags, TXT("Negative numbers such as %s should not be considered a power of 2."), DString::MakeString(input));
			return false;
		}

		input = 0;
		bIsPow2 = Utils::IsPowerOf2(input);
		if (bIsPow2)
		{
			UnitTestError(testFlags, TXT("%s should not be considered a power of 2."), DString::MakeString(input));
			return false;
		}
	}

	if (!TestNumericTemplateUtils<float>(testFlags))
	{
		return false;
	}

	if (!TestNumericTemplateUtils<double>(testFlags))
	{
		return false;
	}

	//Test float templates
	{
		float input = 4;
		float result = Utils::Round(input);
		if (result != input)
		{
			UnitTestError(testFlags, TXT("Rounding %s should have resulted in %s.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(input), DString::MakeString(result));
			return false;
		}

		input = 6.5f;
		result = Utils::Round(input);
		if (result != 7)
		{
			UnitTestError(testFlags, TXT("Rounding %s should have resulted in 7.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(result));
			return false;
		}

		input = 6.4f;
		result = Utils::Round(input);
		if (result != 6)
		{
			UnitTestError(testFlags, TXT("Rounding %s should have resulted in 6.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(result));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Core Utils"));
	return true;
}

bool CoreUtilUnitTester::TestContainerUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Container Utils"));
	SetTestCategory(testFlags, TXT("Vector Utils"));

	std::vector<Int> testVector;
	testVector.push_back(8); //[0]
	testVector.push_back(2); //[1]
	testVector.push_back(4); //[2]
	testVector.push_back(5); //[3]
	testVector.push_back(2); //[4]
	testVector.push_back(9); //[5]
	testVector.push_back(1); //[6]

	std::vector<Int> emptyVector;

	if (ContainerUtils::IsEmpty(testVector))
	{
		UnitTestError(testFlags, TXT("A vector of size %s should not be considered an empty vector."), DString::MakeString(testVector.size()));
		return false;
	}

	if (!ContainerUtils::IsEmpty(emptyVector))
	{
		UnitTestError(testFlags, TXT("A vector of size %s should have been considered an empty vector."), DString::MakeString(emptyVector.size()));
		return false;
	}

	std::vector<Int> testVectorCopy = testVector;
	ContainerUtils::Empty(testVectorCopy);
	if (!ContainerUtils::IsEmpty(testVectorCopy))
	{
		UnitTestError(testFlags, TXT("Emptying a vector have cleared its contents.  Instead the resulting vector still has a length of %s."), DString::MakeString(testVectorCopy.size()));
		return false;
	}

	UINT_TYPE index = 0;
	if (!ContainerUtils::IsValidIndex(testVector, index))
	{
		UnitTestError(testFlags, TXT("%s should have been considered a valid index for a vector of size %s."), Int(index).ToString(), DString::MakeString(testVector.size()));
		return false;
	}

	index = UINT_INDEX_NONE;
	if (ContainerUtils::IsValidIndex(testVector, index))
	{
		UnitTestError(testFlags, TXT("%s should not have been considered a valid index for a vector of size %s."), Int(index), DString::MakeString(testVector.size()));
		return false;
	}

	index = 6;
	if (!ContainerUtils::IsValidIndex(testVector, index))
	{
		UnitTestError(testFlags, TXT("%s should have been considered a valid index for a vector of size %s."), Int(index), DString::MakeString(testVector.size()));
		return false;
	}

	index = 7;
	if (ContainerUtils::IsValidIndex(testVector, index))
	{
		UnitTestError(testFlags, TXT("%s should not have been considered a valid index for a vector of size %s."), Int(index), DString::MakeString(testVector.size()));
		return false;
	}

	index = 0;
	if (ContainerUtils::IsValidIndex(emptyVector, index))
	{
		UnitTestError(testFlags, TXT("%s should not have been considered a valid index for a vector of size %s."), Int(index), DString::MakeString(emptyVector.size()));
		return false;
	}

	Int expectedLastElement = 1;
	if (ContainerUtils::GetLast(testVector) != expectedLastElement)
	{
		UnitTestError(testFlags, TXT("Container Utilities test failed. Accessing the last element of %s should have been %s instead it returned %s."), DString::ListToString(testVector), expectedLastElement.ToString(), ContainerUtils::GetLast(testVector).ToString());
		return false;
	}

	index = ContainerUtils::FindInVector<Int>(testVector, 5);
	if (index != 3)
	{
		UnitTestError(testFlags, TXT("5 is expected to be found at index 3.  Instead FindInVector returned %s when searching within vector %s"), Int(index), DString::ListToString(testVector));
		return false;
	}

	index = ContainerUtils::FindInVector<Int>(testVector, 1);
	if (index != 6)
	{
		UnitTestError(testFlags, TXT("1 is expected to be found at index 6.  Instead FindInVector returned %s when searching within vector %s"), Int(index), DString::ListToString(testVector));
		return false;
	}

	index = ContainerUtils::FindInVector<Int>(testVector, 3);
	if (index != UINT_INDEX_NONE)
	{
		UnitTestError(testFlags, TXT("3 is expected to be missing from vector.  Instead FindInVector returned %s when searching within vector %s"), Int(index), DString::ListToString(testVector));
		return false;
	}

	index = ContainerUtils::AddUnique<Int>(testVector, 3);
	UINT_TYPE expected = UINT_INDEX_NONE;
	if (index != expected)
	{
		UnitTestError(testFlags, TXT("The index of the newly inserted 3 should have been %s.  Instead AddUnique returned %s when adding 3 to the vector %s."), Int(expected), Int(index).ToString(), DString::ListToString(testVector));
		return false;
	}

	index = ContainerUtils::AddUnique<Int>(testVector, 3);
	expected = testVector.size() - 1;
	if (index != expected)
	{
		UnitTestError(testFlags, TXT("After attempting to add 3 for the second time, AddUnique returned %s instead of %s when adding 3 to the vector %s."), Int(index), Int(expected).ToString(), DString::ListToString(testVector));
		return false;
	}

	index = ContainerUtils::AddUnique<Int>(testVector, 8);
	expected = 0;
	if (index != expected)
	{
		UnitTestError(testFlags, TXT("After attempting to add 8 (which already exists in vector), AddUnique returned %s instead of %s when adding 8 to the vector %s."), Int(index), Int(expected).ToString(), DString::ListToString(testVector));
		return false;
	}

	//Test RemoveItem
	{
		std::function<bool(std::vector<Int>& /*outRemoveItemTarget*/, Int /*numToRemove*/, UINT_TYPE /*expectedIdx*/)> testRemoveItem = [&]
			(std::vector<Int>& outRemoveItemTarget, Int numToRemove, UINT_TYPE expectedIdx)
		{
			std::vector<Int> originalRemoveItem = outRemoveItemTarget;
			UINT_TYPE removedIdx = ContainerUtils::RemoveItem(outRemoveItemTarget, numToRemove);
			if (removedIdx != expectedIdx)
			{
				if (expectedIdx != UINT_INDEX_NONE)
				{
					UnitTestError(testFlags, TXT("Failed to extract %s from %s.  RemoveItem removed element from idx %s instead of idx %s."), numToRemove, DString::ListToString(originalRemoveItem), Int(removedIdx), Int(expectedIdx));
				}
				else
				{
					UnitTestError(testFlags, TXT("Removing a nonexisting item from a vector actually removed an item.  It extracted item at index %s when attempting to remove %s from list %s."), Int(removedIdx), numToRemove.ToString(), DString::ListToString(originalRemoveItem));
				}
				return false;
			}

			return true;
		};

		std::vector<Int> removeItemTarget = {5, 8, 9, 1, 3, 4, 7, 6, 2, 0};
		if (!testRemoveItem(removeItemTarget, 1, 3))
		{
			return false;
		}

		if (!testRemoveItem(removeItemTarget, 7, 5))
		{
			return false;
		}

		if (!testRemoveItem(removeItemTarget, 0, 7))
		{
			return false;
		}

		if (!testRemoveItem(removeItemTarget, 5, 0))
		{
			return false;
		}

		if (!testRemoveItem(removeItemTarget, 7, INT_INDEX_NONE))
		{
			return false;
		}
	}

	//Test RemoveItems
	{
		std::vector<Int> originalVector({5, 2, 4, 5, 6, 5, 5, 5, 9, 15, -5, 12, 5});
		std::vector<Int> testVector = originalVector;
		std::vector<Int> expectedVector({2, 4, 6, 9, 15, -5, 12});
		const Int removedItem = 5;
		Int expectedNumRemoved = ContainerUtils::CountNumItems(testVector, removedItem);
		Int numRemovedItems = ContainerUtils::RemoveItems(OUT testVector, removedItem);

		if (expectedNumRemoved != numRemovedItems)
		{
			UnitTestError(testFlags, TXT("RemoveItems test failed.  Removing %s from %s should have removed %s items.  Instead it only removed %s."), removedItem, DString::ListToString(originalVector), expectedNumRemoved, numRemovedItems);
			return false;
		}

		if (testVector.size() != expectedVector.size())
		{
			UnitTestError(testFlags, TXT("RemoveItems test failed.  There's a mismatch in vector lengths.  The resulting vector length is %s instead of %s."), DString::MakeString(testVector.size()), DString::MakeString(expectedVector.size()));
			return false;
		}

		//Verify each element
		for (UINT_TYPE i = 0; i < testVector.size(); ++i)
		{
			if (testVector.at(i) != expectedVector.at(i))
			{
				UnitTestError(testFlags, TXT("RemoveItems test failed.  There's a vector mismatch at index %s.  Expected value %s.  Actual value %s.  Expected vector:  %s      | Resulting vector: %s."), DString::MakeString(i), expectedVector.at(i), testVector.at(i), DString::ListToString(expectedVector), DString::ListToString(testVector));
				return false;
			}
		}
	}

	UINT_TYPE numItems = ContainerUtils::CountNumItems<Int>(testVector, 6);
	expected = 0;
	if (numItems != expected)
	{
		UnitTestError(testFlags, TXT("Counting number of times 6's in vector returned %s instead of %s.  Vector is %s."), Int(numItems), Int(expected).ToString(), DString::ListToString(testVector));
		return false;
	}

	numItems = ContainerUtils::CountNumItems<Int>(testVector, 4);
	expected = 1;
	if (numItems != expected)
	{
		UnitTestError(testFlags, TXT("Counting number of times 4's in vector returned %s instead of %s.  Vector is %s."), Int(numItems), Int(expected).ToString(), DString::ListToString(testVector));
		return false;
	}

	numItems = ContainerUtils::CountNumItems<Int>(testVector, 2);
	expected = 2;
	if (numItems != expected)
	{
		UnitTestError(testFlags, TXT("Counting number of times 2's in vector returned %s instead of %s.  Vector is %s."), Int(numItems), Int(expected).ToString(), DString::ListToString(testVector));
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Container Utils"));
	return true;
}

template <class T>
bool CoreUtilUnitTester::TestNumericTemplateUtils (EUnitTestFlags testFlags) const
{
	T left = 4;
	T right = 8;
	T result = Utils::Max(left, right);
	if (result != right)
	{
		UnitTestError(testFlags, TXT("The max of %s and %s should have been %s.  Instead it returned %s."), DString::MakeString(left), DString::MakeString(right), DString::MakeString(right), DString::MakeString(result));
		return false;
	}

	result = Utils::Min(left, right);
	if (result != left)
	{
		UnitTestError(testFlags, TXT("The min of %s and %s should have been %s.  Instead it returned %s."), DString::MakeString(left), DString::MakeString(right), DString::MakeString(left), DString::MakeString(result));
		return false;
	}

	right = static_cast<T>(-2.54f);
	left = static_cast<T>(7.83f);
	result = Utils::Max(left, right);
	if (result != left)
	{
		UnitTestError(testFlags, TXT("The max of %s and %s should have been %s.  Instead it returned %s."), DString::MakeString(left), DString::MakeString(right), DString::MakeString(left), DString::MakeString(result));
		return false;
	}

	result = Utils::Min(left, right);
	if (result != right)
	{
		UnitTestError(testFlags, TXT("The min of %s and %s should have been %s.  Instead it returned %s."), DString::MakeString(left), DString::MakeString(right), DString::MakeString(right), DString::MakeString(result));
		return false;
	}

	T min = 10;
	T max = 100;
	T input = 75;
	result = Utils::Clamp(input, min, max);
	if (result != input)
	{
		UnitTestError(testFlags, TXT("Clamping %s to %s and %s should have resulted in %s.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(input), DString::MakeString(result));
		return false;
	}

	input = 125;
	result = Utils::Clamp(input, min, max);
	if (result != max)
	{
		UnitTestError(testFlags, TXT("Clamping %s to %s and %s should have resulted in %s.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(max), DString::MakeString(result));
		return false;
	}

	input = -45;
	result = Utils::Clamp(input, min, max);
	if (result != min)
	{
		UnitTestError(testFlags, TXT("Clamping %s to %s and %s should have resulted in %s.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(min), DString::MakeString(result));
		return false;
	}

	min = 100;
	max = 1000;
	float ratio = 0.5f;
	result = Utils::Lerp(ratio, min, max);
	if (result != 550)
	{
		UnitTestError(testFlags, TXT("Lerping %s between %s and %s should have resulted in 550.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(result));
		return false;
	}

	ratio = 0.1f;
	result = Utils::Lerp(ratio, min, max);
	if (result != 190)
	{
		UnitTestError(testFlags, TXT("Lerping %s between %s and %s should have resulted in 190.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(result));
		return false;
	}

	min = -1000;
	max = 1000;
	ratio = 0.8f;
	result = Utils::Lerp(ratio, min, max);
	if (result != 600)
	{
		UnitTestError(testFlags, TXT("Lerping %s between %s and %s should have resulted in 600.  Instead it returned %s."), DString::MakeString(input), DString::MakeString(min), DString::MakeString(max), DString::MakeString(result));
		return false;
	}

	//Testing IsAboutEqual
	{
		T tolerance = 10;
		T a = 40;
		T b = -40;
		if (Utils::IsAboutEqual(a, b, tolerance))
		{
			UnitTestError(testFlags, TXT("IsAboutEqual test failed. %s and %s should not be considered equal since the difference is greater than %s."), DString::MakeString(a), DString::MakeString(b), DString::MakeString(tolerance));
			return false;
		}

		tolerance = 20;
		a = 25;
		b = 10;
		if (!Utils::IsAboutEqual(a, b, tolerance))
		{
			UnitTestError(testFlags, TXT("IsAboutEqual test failed. %s and %s should have been considered equal since the difference is less than %s."), DString::MakeString(a), DString::MakeString(b), DString::MakeString(tolerance));
			return false;
		}

		tolerance = 2;
		a = -8;
		b = -7;
		if (!Utils::IsAboutEqual(a, b, tolerance))
		{
			UnitTestError(testFlags, TXT("IsAboutEqual test failed. %s and %s should have been considered equal since the difference is less than %s."), DString::MakeString(a), DString::MakeString(b), DString::MakeString(tolerance));
			return false;
		}

		tolerance = 5;
		a = 105;
		b = 108;
		if (!Utils::IsAboutEqual(a, b, tolerance))
		{
			UnitTestError(testFlags, TXT("IsAboutEqual test failed. %s and %s should have been considered equal since the difference is less than %s."), DString::MakeString(a), DString::MakeString(b), DString::MakeString(tolerance));
			return false;
		}
	}

	return true;
}

SD_END

#endif