/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Float.cpp
=====================================================================
*/

#include "CoreDatatypes.h"

SD_BEGIN
Float::Float ()
{

}

Float::Float (const float newValue)
{
	Value = newValue;
}

Float::Float (const Float& copyFloat)
{
	Value = copyFloat.Value;
}

Float::Float (const Int& newValue)
{
	Value = newValue.ToFloat().Value;
}

Float::Float (const DString& text)
{
	Value = text.Stof().Value;
}

void Float::operator= (const Float& copyFloat)
{
	Value = copyFloat.Value;
}

void Float::operator= (const float otherFloat)
{
	Value = otherFloat;
}

Float Float::operator++ ()
{
	return ++Value;
}

Float Float::operator++ (int)
{
	return Value++;
}

Float Float::operator- () const
{
	return -Value;
}

Float Float::operator-- ()
{
	return --Value;
}

Float Float::operator-- (int)
{
	return Value--;
}

void Float::ResetToDefaults ()
{
	Value = 0.f;
}

DString Float::ToString () const
{
	return DString::MakeString(Value);
}

void Float::ParseString (const DString& str)
{
	Value = str.Stof().Value;
}

size_t Float::GetMinBytes () const
{
	return Float::SGetMinBytes();
}

void Float::Serialize (DataBuffer& outData) const
{
	//Convert Float to char array
	const int numBytes = sizeof(Value);
	char charArray[numBytes];
	memcpy(charArray, &Value, numBytes);

	//Reverse bytes if needed
	if (outData.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	outData.AppendBytes(charArray, numBytes);
}

bool Float::Deserialize (const DataBuffer& dataBuffer)
{
	const int numBytes = sizeof(Value);
	char rawData[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	memcpy(&Value, rawData, numBytes);
	return true;
}

Float Float::MakeFloat (int value)
{
	return Int(value).ToFloat();
}

Float Float::MakeFloat (unsigned int value)
{
	return Int(value).ToFloat();
}

#ifdef PLATFORM_64BIT
Float Float::MakeFloat (const int64& value)
{
	return Int(value).ToFloat();
}

Float Float::MakeFloat (const uint64& value)
{
	return Int(value).ToFloat();
}
#endif

size_t Float::SGetMinBytes ()
{
	return sizeof(Value);
}

bool Float::IsCloseTo (Float other, Float tolerance) const
{
	return (Abs(*this - other) <= tolerance);
}

Int Float::ToInt () const
{
	return Int(static_cast<int>(trunc(Value)));
}

Float Float::Abs (const Float value)
{
	return Float(abs(value.Value));
}

void Float::AbsInline ()
{
	Value = std::abs(Value);
}

Float Float::Round (const Float value)
{
	return Float(round(value.Value));
}

void Float::RoundInline ()
{
	Value = round(Value);
}

void Float::RoundInline (Int numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFloat().Value);
	Value = round(Value * multiplier)/multiplier;
}

Float Float::RoundUp (const Float value)
{
	return Float(ceil(value.Value));
}

void Float::RoundUpInline ()
{
	Value = ceil(Value);
}

void Float::RoundUpInline (Int numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFloat().Value);
	Value = ceil(Value * multiplier)/multiplier;
}

Float Float::RoundDown (const Float value)
{
	return Float(floor(value.Value));
}

void Float::RoundDownInline ()
{
	Value = floor(Value);
}

void Float::RoundDownInline (Int numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFloat().Value);
	Value = floor(Value * multiplier)/multiplier;
}

Float Float::Pow (Float base, Float exponent)
{
	return Float(std::powf(base.Value, exponent.Value));
}

void Float::PowInline (Float exponent)
{
	Value = std::powf(Value, exponent.Value);
}

float Float::GetFloat (double target)
{
	Value = (float)target;
	return Value;
}

double Float::ToDouble () const
{
	return static_cast<double>(Value);
}

DString Float::ToFormattedString (Int minNumDigits, Int numDecimals, eTruncateMethod rounding) const
{
	if (numDecimals <= 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Float::ToFormattedString must display at least one decimal point. If there's a need to truncate decimals, use ToInt or one of the Rounding functions."));
		return ToInt().ToString();
	}

	Float roundedFloat(Abs(*this));
	if (numDecimals >= 0)
	{
		switch (rounding)
		{
			case(TM_RoundDown):
				roundedFloat.RoundDownInline(numDecimals);
				break;

			case(TM_Round):
				roundedFloat.RoundInline(numDecimals);
				break;

			case(TM_RoundUp):
				roundedFloat.RoundUpInline(numDecimals);
				break;
		}
	}

	DString result = roundedFloat.ToString();
	Int decimalIdx = result.Find(TXT("."), 1 /* one instead of zero because floats never lead with decimal point*/, DString::CC_CaseSensitive);
	CHECK(decimalIdx != INT_INDEX_NONE) //Floats should always print a decimal

	if ((result.Length() - decimalIdx) > numDecimals)
	{
		//Drop out unwanted decimal digits.
		DString::SubString(OUT result, 0, decimalIdx + numDecimals);
	}

	const Int numIgnoreChars = 1; //Ignore one of the characters (the decimal).
	Int numLeadingZeros = minNumDigits - (result.Length() - numIgnoreChars);
	const DString prefix = (Value >= 0.f) ? DString::EmptyString : TXT("-"); //Add negative sign back
	if (numLeadingZeros > 0)
	{
		return prefix + DString(TString(numLeadingZeros.Value, '0')) + result;
	}

	return prefix + result;
}

#pragma region "External Operators"
bool operator== (const Float& left, const Float& right)
{
	return (abs(left.Value - right.Value) < FLOAT_TOLERANCE);
}

bool operator== (const Float& left, const float& right)
{
	return (abs(left.Value - right) < FLOAT_TOLERANCE);
}

bool operator== (const float& left, const Float& right)
{
	return (abs(left - right.Value) < FLOAT_TOLERANCE);
}

bool operator!= (const Float& left, const Float& right)
{
	return !(left.Value == right.Value);
}

bool operator!= (const Float& left, const float& right)
{
	return !(left.Value == right);
}

bool operator!= (const float& left, const Float& right)
{
	return !(left == right.Value);
}

bool operator< (const Float& left, const Float& right)
{
	return (left.Value < right.Value);
}

bool operator< (const Float& left, const float& right)
{
	return (left.Value < right);
}

bool operator< (const float& left, const Float& right)
{
	return (left < right.Value);
}

bool operator<= (const Float& left, const Float& right)
{
	return (left.Value <= right.Value);
}

bool operator<= (const Float& left, const float& right)
{
	return (left.Value <= right);
}

bool operator<= (const float& left, const Float& right)
{
	return (left <= right.Value);
}

bool operator> (const Float& left, const Float& right)
{
	return (left.Value > right.Value);
}

bool operator> (const Float& left, const float& right)
{
	return (left.Value > right);
}

bool operator> (const float& left, const Float& right)
{
	return (left > right.Value);
}

bool operator>= (const Float& left, const Float& right)
{
	return (left.Value >= right.Value);
}

bool operator>= (const Float& left, const float& right)
{
	return (left.Value >= right);
}

bool operator>= (const float& left, const Float& right)
{
	return (left >= right.Value);
}

Float operator+ (const Float& left, const Float& right)
{
	return Float(left.Value + right.Value);
}

Float operator+ (const Float& left, const float& right)
{
	return Float(left.Value + right);
}

Float operator+ (const float& left, const Float& right)
{
	return Float(left + right.Value);
}

Float& operator+= (Float& left, const Float& right)
{
	left.Value += right.Value;
	return left;
}

Float& operator+= (Float& left, const float& right)
{
	left.Value += right;
	return left;
}

float& operator+= (float& left, const Float& right)
{
	left += right.Value;
	return left;
}

Float operator- (const Float& left, const Float& right)
{
	return Float(left.Value - right.Value);
}

Float operator- (const Float& left, const float& right)
{
	return Float(left.Value - right);
}

Float operator- (const float& left, const Float& right)
{
	return Float(left - right.Value);
}

Float& operator-= (Float& left, const Float& right)
{
	left.Value -= right.Value;
	return left;
}

Float& operator-= (Float& left, const float& right)
{
	left.Value -= right;
	return left;
}

float& operator-= (float& left, const Float& right)
{
	left -= right.Value;
	return left;
}

Float operator* (const Float& left, const Float& right)
{
	return Float(left.Value * right.Value);
}

Float operator* (const Float& left, const float& right)
{
	return Float(left.Value * right);
}

Float operator* (const float& left, const Float& right)
{
	return Float(left * right.Value);
}

Float& operator*= (Float& left, const Float& right)
{
	left.Value *= right.Value;
	return left;
}

Float& operator*= (Float& left, const float& right)
{
	left.Value *= right;
	return left;
}

float& operator*= (float& left, const Float& right)
{
	left *= right.Value;
	return left;
}

Float operator/ (const Float& left, const Float& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Float / Float)"), left);
		return left;
	}
#endif

	return (right.Value != 0.f) ? Float(left.Value / right.Value) : left;
}

Float operator/ (const Float& left, const float& right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Float / float)"), left);
		return left;
	}
#endif

	return (right != 0.f) ? Float(left.Value / right) : left;
}

Float operator/ (const float& left, const Float& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (float / Float)"), Float(left));
		return Float(left);
	}
#endif

	return (right.Value != 0.f) ? Float(left / right.Value) : Float(left);
}

Float& operator/= (Float& left, const Float& right)
{
	if (right.Value != 0.f)
	{
		left.Value /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Float /= Float)"), left);
	}
#endif

	return left;
}

Float& operator/= (Float& left, const float& right)
{
	if (right != 0.f)
	{
		left.Value /= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Float /= float)"), left);
	}
#endif

	return left;
}

float& operator/= (float& left, const Float& right)
{
	if (right.Value != 0.f)
	{
		left /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (float /= Float)"), Float(left));
	}
#endif

	return left;
}

Float operator% (const Float& left, const Float& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0.f) ? Float(fmod(left.Value, right.Value)) : left;
}

Float operator% (const Float& left, const float& right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0.f) ? Float(fmod(left.Value, right)) : left;
}

Float operator% (const float& left, const Float& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), Float(left));
		return left;
	}
#endif

	return (right.Value != 0.f) ? Float(fmod(left, right.Value)) : Float(left);
}

Float& operator%= (Float& left, const Float& right)
{
	if (right.Value != 0.f)
	{
		left.Value = fmod(left.Value, right.Value);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif

	return left;
}

Float& operator%= (Float& left, const float& right)
{
	if (right != 0.f)
	{
		left.Value = fmod(left.Value, right);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif

	return left;
}

float& operator%= (float& left, const Float& right)
{
	if (right.Value != 0.f)
	{
		left = fmod(left, right.Value);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), Float(left));
	}
#endif

	return left;
}
#pragma endregion
SD_END