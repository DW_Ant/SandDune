/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DProperty.cpp
=====================================================================
*/

#include "DProperty.h"
#include "DString.h"
#include "Engine.h"
#include "LogCategory.h"
#include "StringIterator.h"
#include <regex>

SD_BEGIN
const DString CommonPropertyRegex::Bool(TXT("([trueTRUEfalsFALS]{4,5})")); //Covers upper and lower case True and False. Matches either four or five characters.
const DString CommonPropertyRegex::Float(TXT("(-?[0-9.]+)")); //Matches characters 0-9 and decimal for at least one character. The optional negative sign must be leading.
const DString CommonPropertyRegex::Int(TXT("(-?[0-9]+)")); //Matches characters 0-9 for at least one character. The optional negative sign must be leading.

/*
Match any character between two quotes. This is a greedy search where it'll match up til the last quote.
This could over value the string if it resides in a struct with multiple strings. In that case, use a "*?" to use a lazy search to terminate the string at the first quote instead.
In short: Use greedy if this is the only string in the data struct.
Use lazy if there are multiple strings in the data struct. If there's a need for the string to contain double quotes, then a custom character should be used to encompass the string (eg: use single quotes instead).
*/
const DString CommonPropertyRegex::STRING_GREEDY(TXT("\\\"([\\w\\W]*)\\\""));
const DString CommonPropertyRegex::STRING_LAZY(TXT("\\\"([\\w\\W]*?)\\\""));

DString DProperty::ParseVariable (const DString& data, const DString& varName, const DString& regexMatch)
{
	/*
	[\\(, ]+ = searches for open parenthesis, spaces, and commas at least zero or more times. Double slash to escape escape characters.
	varName = searches for the value of varName string literally.
	\\s*=\\s* = searches for spaces and an equal signs. Space characters are optional. Double slash to escape escape characters.
	regexMatch = The expression to match. This match should contain a grouping selection "(" and ")" in order to find out which characters should be returned.
	*/
	DString regexStr = TXT("[\\(, ]*") + varName + TXT("\\s*=\\s*") + regexMatch;
	std::regex searchPattern(regexStr.ToCString());
	std::smatch matches;
	std::string stdData = data.ToStr();
	if (!std::regex_search(stdData, OUT matches, searchPattern) || matches.size() < 2)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to parse %s from %s. Expected format: %s=data"), varName, data, varName);
		return DString::EmptyString;
	}

	//[0] is the whole match, [1] is the variable value
	return DString(matches[1].str());
}

DString DProperty::ParseStruct (const DString& data, const DString& structName)
{
	//Greedy search is intended. The string iterator later in this function will trim out the excessive content.

	/*
	[\\(, ]* - searches for open parenthesis, comma, or spaces zero or many times.
	structName - searches for the struct name literally (case sensitive).
	\\s*=\\s* - searches for equals sign literally with optional space characters around it.
	\\( - searches for open parenthesis literally.

	GROUP [1] - defined by open and close parenthesis
		[\\w\\W]*\\) - matches any character zero or more times. Greedy search. It'll keep matching until the last found closing parenthesis. The closing parenthesis is not included in the group.
	*/
	DString regexStr = TXT("[\\(, ]*") + structName + TXT("\\s*=\\s*\\(([\\w\\W]*)\\)");
	std::regex searchPattern(regexStr.ToCString());
	std::smatch matches;
	std::string stdData = data.ToStr();
	if (!std::regex_search(stdData, OUT matches, searchPattern) || matches.size() < 2)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to parse %s from the string %s. Expected: %s=(data)"), structName, data, structName);
		return DString::EmptyString;
	}

	//[0] is the whole match, [1] is the struct data and possibly following data beyond the struct.
	DString structData(matches[1].str());

	//To ensure it didn't over select, use a string iterator to reference count parenthesis.
	Int refCounter = 0;
	Int charCounter = 0;
	bool bInsideQuotes = false; //Ignore parenthesis between quotes
	bool escapedChar = false; //Becomes true if the previous character is an escape character.
	for (StringIterator iter(&structData, true); !iter.IsAtEnd(); ++iter)
	{
		++charCounter;
		if (escapedChar)
		{
			escapedChar = false;
			continue;
		}

		if (iter.GetSizeOfCharacter() == 1)
		{
			unsigned char selectedChar;
			iter.GetCurrentCharacterData(&selectedChar);

			if (selectedChar == '\"')
			{
				bInsideQuotes = !bInsideQuotes;
			}
			else if (selectedChar == '\\')
			{
				escapedChar = true;
			}
			else if (!bInsideQuotes)
			{
				if (selectedChar == '(')
				{
					++refCounter;
				}
				else if (selectedChar == ')')
				{
					--refCounter;
					if (refCounter < 0) //Found closing parenthesis within the string.
					{
						break;
					}
				}
			}
		}
	}

	if (refCounter > 0)
	{
		//Found more opening parenthesis than closing parenthesis
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to parse %s from the string %s since there's a mismatch between opening and closing parenthesis."), structName, data);
		return DString::EmptyString;
	}
	else if (refCounter == 0)
	{
		//The iterator did not find the closing parenthesis. Safe to assume that the closing parenthesis the regex found pairs up with the first one.
		return structData;
	}
	else //refCounter < 0
	{
		//The iterator found the closing parenthesis before reaching the end of the string. This struct data probably resides in another struct. Eg:   SuperStruct=(structName=(X=0,Y=0,Z=0), sibling=8)
		return structData.SubStringCount(0, charCounter);
	}
}
SD_END