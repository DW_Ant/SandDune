/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LifeSpanComponent.cpp
=====================================================================
*/

#include "DClass.h"
#include "DClassAssembler.h"
#include "LifeSpanComponent.h"
#include "TickComponent.h"
#include "TickGroup.h"

IMPLEMENT_CLASS(SD::LifeSpanComponent, SD::EntityComponent)
SD_BEGIN

void LifeSpanComponent::InitProps ()
{
	Super::InitProps();

	LifeSpan = 30.f;
	bAging = true;
}

void LifeSpanComponent::BeginObject ()
{
	Super::BeginObject();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, LifeSpanComponent, HandleTick, void, Float));
	}
}

void LifeSpanComponent::Expire ()
{
	if (Owner.IsValid())
	{
		Owner->Destroy(); //Destroying owner will also end up destroying this component
	}
	else
	{
		//No owner?  At the very least remove self to disable continuous ticking.
		Destroy();
	}
}

void LifeSpanComponent::HandleTick (Float deltaSec)
{
	if (bAging)
	{
		LifeSpan -= deltaSec;
	}

	if (LifeSpan <= 0)
	{
		Expire();
	}
}
SD_END