/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Matrix.cpp
=====================================================================
*/

#include "ContainerUtils.h"
#include "Matrix.h"
#include "LogCategory.h"
#include "Utils.h"

SD_BEGIN
const Matrix Matrix::InvalidMatrix = Matrix();

Matrix::Matrix ()
{
	Rows = 0;
	Columns = 0;
}

Matrix::Matrix (Int numRows, Int numColumns)
{
	CHECK(numRows >= 0 && numColumns >= 0) //Still support 0 dimensions to signal invalid/uninit matrices
	Rows = numRows;
	Columns = numColumns;

	if (Rows * Columns > 0)
	{
		Data.resize((Rows * Columns).ToUnsignedInt());
	}
}

Matrix::Matrix (Int numRows, Int numColumns, const std::vector<Float>& inData)
{
	CHECK(numRows >= 0 && numColumns >= 0)
	Rows = numRows;
	Columns = numColumns;
	Data = inData;
}

Matrix::Matrix (const Matrix& copyMatrix)
{
	Rows = copyMatrix.Rows;
	Columns = copyMatrix.Columns;
	Data = copyMatrix.Data;
}

Matrix::~Matrix ()
{

}

void Matrix::operator= (const Matrix& copyMatrix)
{
	Rows = copyMatrix.Rows;
	Columns = copyMatrix.Columns;
	Data = copyMatrix.Data;
}

void Matrix::ResetToDefaults ()
{
	Rows = 0;
	Columns = 0;
	ContainerUtils::Empty(OUT Data);
}

DString Matrix::ToString () const
{
	if (!IsValid())
	{
		return TXT("<Invalid Matrix>");
	}

	DString result = DString::CreateFormattedString(TXT("(Rows=%s, Columns=%s, Data=("), Rows, Columns);
	for (UINT_TYPE i = 0; i < Data.size(); i++)
	{
		result += Data.at(i).ToString() + TXT(", ");
	}

	//Replace the trailing ", " with "))"
	result = result.SubString(0, result.Length() - 3);
	return result + TXT("))");
}

void Matrix::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("Rows"), CommonPropertyRegex::Int);
	if (varData.IsEmpty())
	{
		*this = InvalidMatrix;
		return;
	}
	Rows = Int(varData);

	varData = ParseVariable(str, TXT("Columns"), CommonPropertyRegex::Int);
	if (varData.IsEmpty())
	{
		*this = InvalidMatrix;
		return;
	}
	Columns = Int(varData);

	if (Rows <= 0 || Columns <= 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to parse \"%s\" to a matrix since the number of rows or columns is not positive."), str);
		*this = InvalidMatrix;
		return;
	}

	varData = ParseStruct(str, TXT("Data"));
	varData.TrimSpaces();

	std::vector<DString> elements;
	varData.ParseString(',', OUT elements, true);
	if (Int(elements.size()) != (Rows * Columns))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to parse %s to a matrix since the number of elements (%s) does not fit in a %sx%s matrix."), str, Int(elements.size()), Rows, Columns);
		*this = InvalidMatrix;
		return;
	}

	Data.resize((Rows * Columns).Value);
	for (size_t i = 0; i < Data.size(); ++i)
	{
		Data.at(i) = Float(elements.at(i));
	}
}

size_t Matrix::GetMinBytes () const
{
	return Rows.GetMinBytes() + Columns.GetMinBytes();
}

void Matrix::Serialize (DataBuffer& outData) const
{
	//Write num rows and cols first since it's also used to determine how many bytes to read for the data vector.
	outData << Rows;
	outData << Columns;

	for (size_t i = 0; i < Data.size(); ++i)
	{
		outData << Data.at(i);
	}
}

bool Matrix::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Rows >> Columns;
	if (dataBuffer.HasReadError())
	{
		return false;
	}

	ContainerUtils::Empty(OUT Data);
	Int dataSize = (Rows * Columns);
	for (size_t i = 0; i < dataSize; ++i)
	{
		Float newData;
		if ((dataBuffer >> newData).HasReadError())
		{
			return false;
		}

		Data.push_back(newData);
	}

	return true;
}

Float Matrix::DotProduct (const std::vector<Float>& left, const std::vector<Float>& right)
{
	CHECK(left.size() == right.size());

	Float result = 0.f;
	for (size_t i = 0; i < left.size(); i++)
	{
		result += (left[i] * right[i]);
	}

	return result;
}

#ifdef DEBUG_MODE
void Matrix::LogMatrix () const
{
	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Debug, TXT("<Invalid Matrix>    ---    CurRows: %s, CurColumns: %s, DataSize: %s"), GetNumRows(), GetNumColumns(), Int(Data.size()));
		return;
	}

	for (Int curRow = 0; curRow < GetNumRows(); curRow++)
	{
		DString rowText = TXT("[ ");
		for (Int curColumn = 0; curColumn < GetNumColumns(); curColumn++)
		{
			rowText += TXT(" ") + GetDataElement(curRow, curColumn).ToString() + TXT(" ");
		}

		rowText += TXT(" ]");
		CoreLog.Log(LogCategory::LL_Debug, rowText);
	}
}
#endif

bool Matrix::IsValid () const
{
	return (Data.size() > 0 && Columns > 0 && Rows > 0 && Data.size() == (Rows * Columns));
}

bool Matrix::IsSquare () const
{
	return Rows == Columns;
}

bool Matrix::IsUpperTriangularMatrix (Float tolerance) const
{
	/*
	An example of a upper triangular matrix
	[a	b	c	d	e]
	[0	f	g	h	i]
	[0	0	j	k	l]
	[0	0	0	m	n]
	[0	0	0	0	x]
	*/
	if (!IsSquare() || !IsValid())
	{
		return false;
	}

	for (Int curColumn = 0; curColumn < Columns; curColumn++)
	{
		for (Int curRow = curColumn + 1; curRow < Rows; curRow++)
		{
			if (!Utils::IsAboutEqual(GetDataElement(curRow, curColumn).Value, 0.f, tolerance.Value))
			{
				return false;
			}
		}
	}

	return true;
}

bool Matrix::IsLowerTriangularMatrix (Float tolerance) const
{
	/*
	An example of a lower triangular matrix
	[a	0	0	0	0]
	[b	c	0	0	0]
	[d	e	f	0	0]
	[g	h	i	j	0]
	[k	l	m	n	x]
	*/
	if (!IsSquare() || !IsValid())
	{
		return false;
	}

	for (Int curRow = 0; curRow < Rows; curRow++)
	{
		for (Int curColumn = curRow + 1; curColumn < Columns; curColumn++)
		{
			if (!Utils::IsAboutEqual(GetDataElement(curRow, curColumn).Value, 0.f, tolerance.Value))
			{
				return false;
			}
		}
	}

	return true;
}

bool Matrix::IsUnitriangularMatrix (Float tolerance) const
{
	/*
	An example of a unitriangular matrix
	[1	0	0	0	0]
	[A	1	0	0	0]
	[B	C	1	0	0]
	[D	E	F	1	0]
	[G	H	I	J	1]
	*/
	if (!IsSquare() || !IsValid())
	{
		return false;
	}

	//Check main diagonal first before checking if it's either lower or upper (for increased liklihood of returning false early).
	for (Int curRow = 0; curRow < Rows; curRow++)
	{
		if (!Utils::IsAboutEqual(GetDataElement(curRow, curRow).Value, 1.f, tolerance.Value))
		{
			return false;
		}
	}

	return (IsUpperTriangularMatrix(tolerance) || IsLowerTriangularMatrix(tolerance));
}

bool Matrix::IsUnitMatrix (Float tolerance) const
{
	if (!IsUpperTriangularMatrix(tolerance) || !IsLowerTriangularMatrix(tolerance))
	{
		return false;
	}

	//Do the same as unitriangular matrix without having to check lower/upper triangular matrices (for performance)
	for (Int curRow = 0; curRow < Rows; curRow++)
	{
		if (!Utils::IsAboutEqual(GetDataElement(curRow, curRow).Value, 1.f, tolerance.Value))
		{
			return false;
		}
	}

	return true;
}

bool Matrix::IsNearlyEqual (const Matrix& compareTo, Float tolerance) const
{
	if (GetNumRows() != compareTo.GetNumRows() || GetNumColumns() != compareTo.GetNumColumns())
	{
		return false;
	}

	for (size_t i = 0; i < Data.size(); ++i)
	{
		if (Float::Abs(Data.at(i) - compareTo.Data.at(i)) > tolerance)
		{
			return false;
		}
	}

	return true;
}

bool Matrix::GetIdentityMatrix (Matrix& outIdentity) const
{
	if (!IsValid() || !IsSquare())
	{
		return false;
	}

	/*
	The identity matrix should have ones diagonally (from top left corner to bottom right corner).
	All other elements should be zero.
	1	0	0	0
	0	1	0	0
	0	0	1	0
	0	0	0	1
	*/
	outIdentity = Matrix(Rows, Columns);
	outIdentity.Data.resize((outIdentity.GetNumRows() * outIdentity.GetNumColumns()).ToUnsignedInt());
	for (UINT_TYPE i = 0; i < Data.size(); i++)
	{
		//The pattern when 1's appear are...
		//0, 3 (for 2x2)
		//0, 4, 8 (for 3x3)
		//0, 5, 10, 15 (for 4x4)
		outIdentity.Data.at(i) = (i % (Columns+1) == 0) ? 1.f : 0.f;
	}

	return true;
}

Matrix Matrix::GetTransposeMatrix () const
{
	Matrix results(Columns, Rows); //Intentionally swapping rows and columns in constructor.
	CHECK(results.Data.size() == Data.size())

	size_t numRows = Rows.ToUnsignedInt();
	size_t numCols = Columns.ToUnsignedInt();
	Int counter = 0;
	for (size_t rowIdx = 0; rowIdx < numRows; ++rowIdx)
	{
		for (size_t colIdx = 0; colIdx < numCols; ++colIdx)
		{
			results.Data.at((((counter * results.Columns) + rowIdx) % Int(results.Data.size())).ToUnsignedInt()) = Data.at((rowIdx*numCols) + colIdx);
			++counter;
		}
	}

	return results;
}

bool Matrix::IsOrthogonalMatrix (Float tolerance) const
{
	if (!IsSquare())
	{
		return false;
	}

	const Matrix testMatrix = (*this * GetTransposeMatrix());

	return (testMatrix.IsUnitMatrix(tolerance));
}

Float Matrix::CalculateDeterminant () const
{
	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot compute the determinant of an invalid matrix."));
		return 0.f;
	}

	if (!IsSquare())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot compute a determinant for non squared matrices.  Current dimensions are %sx%s"), GetNumRows(), GetNumColumns());
		return 0.f;
	}

	if (GetNumRows() > 4)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Computing determinants for %sx%s matrices are not supported.  Only matrices up to 4x4 are supported."), GetNumRows(), GetNumColumns());
		return 0.f;
	}

	switch (GetNumRows().Value)
	{
		case(1):
			return Data.at(0);
		case(2):
			return CalcDeterminant2x2();
		case(3):
			return CalcDeterminant3x3();
		case(4):
			return CalcDeterminant4x4();
	}

	return 0.f;
}

bool Matrix::CalculateAdjugate (Matrix& outAdjugate) const
{
	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot compute the adjugate of an invalid matrix."));
		return false;
	}

	if (!IsSquare())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Computing adjugates for nonsquared matrices are not supported:  %s"), ToString());
		return false;
	}

	if (GetNumRows() > 4)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Computing adjugates for matrices with dimensions greater than 4x4 is not supported.  Dimensions for current matrix is:  %sx%s"), GetNumRows(), GetNumColumns());
		return false;
	}

	outAdjugate = *this;
	switch (GetNumRows().Value)
	{
		case(1):
			break;
		case(2):
			CalcAdjugate2x2(outAdjugate);
			break;
		case(3):
			CalcAdjugate3x3(outAdjugate);
			break;
		case(4):
			CalcAdjugate4x4(outAdjugate);
	}

	return true;
}

Matrix Matrix::CalculateInverse () const
{
	Matrix inverse;
	if (!CalculateInverse(OUT inverse))
	{
		inverse = InvalidMatrix;
	}

	return inverse;
}

bool Matrix::CalculateInverse (Matrix& outInverse) const
{
	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot compute the inverse of an invalid matrix."));
		return false;
	}

	if (!IsSquare())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Computing inverses for nonsquared matrices are not supported:  %s"), ToString());
		return false;
	}

	if (GetNumRows() > 4)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Computing inverses for matrices with dimensions greater than 4x4 are not supported.  Dimensions for current matrix is:  %sx%s"), GetNumRows(), GetNumColumns());
		return false;
	}

	outInverse = *this;
	switch (GetNumRows().Value)
	{
		case(1):
			if (Data.at(0) != 0.f)
			{
				outInverse.Data.at(0) = 1/Data.at(0);
				return true;
			}

			return false;

		case(2):
			return CalcInverse2x2(outInverse);
		case(3):
			return CalcInverse3x3(outInverse);
		case(4):
			return CalcInverse4x4(outInverse);
	}

	return false;
}

Matrix Matrix::GetZeroMatrix () const
{
	Matrix results(GetNumRows(), GetNumColumns());
	unsigned int matrixSize = (results.GetNumRows() * results.GetNumColumns()).ToUnsignedInt32();
	results.Data.resize(matrixSize);
	for (unsigned int i = 0; i < matrixSize; i++)
	{
		results.Data.at(i) = 0.f;
	}

	return results;
}

Matrix Matrix::GetComatrix () const
{
	/*
	A Comatrix will have a checkboard effect of signed numbers.  Example listed below:
	Original Matrix		Comatrix Signage
	[a	b	c	d]		[+	-	+	-] =>	[ a	-b	 c	-d]
	[e	f	g	h]		[-	+	-	+] =>	[-e	 f	-g	 h]
	[i	j	k	l]		[+	-	+	-] =>	[ i	-j	 k	-l]
	[m	n	o	p]		[-	+	-	+] =>	[-m	 n	-o	 p]
	*/
	Matrix result(*this);

	for (Int curColumn = 0; curColumn < GetNumColumns(); curColumn++)
	{
		Int negOffset = ((curColumn%2) == 0) ? 1 : 0;
		for (Int curRow = negOffset; curRow < GetNumRows(); curRow += 2)
		{
			result.Data.at(result.GetDataIndex(curRow, curColumn).ToUnsignedInt()) *= -1.f;
		}
	}

	return result;
}

Matrix Matrix::GetSubMatrix (Int excludedRowIdx, Int excludedColumnIdx) const
{
	if (excludedRowIdx < 0 || excludedColumnIdx < 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot obtain a submatrix using negative indices (%s, %s)"), excludedRowIdx, excludedColumnIdx);
		return InvalidMatrix;
	}

	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot obtain a submatrix using an invalid matrix."));
		return InvalidMatrix;
	}

	if (Rows * Columns == 1)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot obtain a submatrix from a 1x1 matrix:  %s"), ToString());
		return InvalidMatrix;
	}

	Matrix result(Rows - 1, Columns - 1);
	std::vector<Float> subData;

	//Caution, the order of the nested loops are important since the data pushed into the subData depends on that order.
	for (Int curRow = 0; curRow < GetNumRows(); curRow++)
	{
		if (curRow == excludedRowIdx)
		{
			continue;
		}

		for (Int curColumn = 0; curColumn < GetNumColumns(); curColumn++)
		{
			if (curColumn == excludedColumnIdx)
			{
				continue;
			}

			subData.push_back(GetDataElement(curRow, curColumn));
		}
	}

	result.Data = subData;
	return result;
}

Matrix Matrix::GetSubMatrix (Int rowStartIdx, Int rowEndIdx, Int columnStartIdx, Int columnEndIdx) const
{
	if (!IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot retrieve a sub matrix from an invalid matrix:  %s"), ToString());
		return InvalidMatrix;
	}

	//Check for invalid parameters
	if (rowStartIdx < 0 || rowEndIdx < 0 || columnStartIdx < 0 || columnEndIdx < 0 ||
		rowEndIdx >= Rows || columnEndIdx >= Columns ||
		rowStartIdx > rowEndIdx || columnStartIdx > columnEndIdx)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot retrieve a sub matrix from this matrix:  %s.  At least one of the parameters are not valid ranges.  rowStartIdx=%s, rowEndIdx=%s, columnStartIdx=%s, columnEndIdx=%s"), ToString(), rowStartIdx, rowEndIdx, columnStartIdx, columnEndIdx);
		return InvalidMatrix;
	}

	Matrix subMatrix((rowEndIdx - rowStartIdx) + 1, (columnEndIdx - columnStartIdx) + 1);
	CHECK(subMatrix.Data.size() == subMatrix.GetNumRows() * subMatrix.GetNumColumns())

	UINT_TYPE dataIdx = 0;
	for (Int rowIdx = rowStartIdx; rowIdx <= rowEndIdx; ++rowIdx)
	{
		for (Int columnIdx = columnStartIdx; columnIdx <= columnEndIdx; ++columnIdx)
		{
			subMatrix.Data.at(dataIdx) = (GetDataElement(rowIdx, columnIdx));
			dataIdx++;
		}
	}

	return subMatrix;
}

bool Matrix::GetMatrixOfMinors (Matrix& outMatrixOfMinors) const
{
	if (!IsValid() || !IsSquare() || GetNumRows() < 2)
	{
		return false;
	}

	if (GetNumRows() > 4)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to get a matrix of minors for a matrix of size %sx%s since this function supports up to matrices of size 4x4"), GetNumRows(), GetNumColumns());
		return false;
	}

	outMatrixOfMinors = Matrix(GetNumRows(), GetNumColumns());
	std::vector<Float> minorMatrix;

	for (Int curRow = 0; curRow < GetNumRows(); curRow++)
	{
		for (Int curColumn = 0; curColumn < GetNumColumns(); curColumn++)
		{
			Float minorDeterminant = GetSubMatrix(curRow, curColumn).CalculateDeterminant();

			minorMatrix.push_back(minorDeterminant);
		}
	}

	outMatrixOfMinors.Data = minorMatrix;
	return true;
}

void Matrix::ForEachElement (std::function<void(UINT_TYPE)> elementFunction)
{
	for (UINT_TYPE i = 0; i < Data.size(); ++i)
	{
		elementFunction(i);
	}
}

void Matrix::MatrixAdd (const Matrix& addBy)
{
	if (GetNumRows() != addBy.GetNumRows() || GetNumColumns() != addBy.GetNumColumns())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to get the sum of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), GetNumRows(), GetNumColumns(), addBy.GetNumRows(), addBy.GetNumColumns());
		return;
	}

	for (size_t i = 0; i < GetNumElements(); i++)
	{
		Data[i] += addBy[i];
	}
}

void Matrix::MatrixSubtract (const Matrix& subtractBy)
{
	if (GetNumRows() != subtractBy.GetNumRows() || GetNumColumns() != subtractBy.GetNumColumns())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to get the difference of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), GetNumRows(), GetNumColumns(), subtractBy.GetNumRows(), subtractBy.GetNumColumns());
		return;
	}

	for (size_t i = 0; i < GetNumElements(); i++)
	{
		Data[i] -= subtractBy[i];
	}
}

void Matrix::MatrixMultiply (Float multiplier)
{
	for (UINT_TYPE i = 0; i < GetNumElements(); i++)
	{
		Data[i] *= multiplier;
	}
}

void Matrix::MatrixMultiply (const Matrix& multiplier)
{
	if (GetNumColumns() != multiplier.GetNumRows())
	{
		//Can't multiply these matrices
		CoreLog.Log(LogCategory::LL_Warning, TXT("Matrix %s is not compatible for matrix multiplication with %s"), *this, multiplier);
		return;
	}

	//Create a copy of this matrix since we need to write to the data while still accessing it.
	const Matrix originalMatrix(*this);
	Columns = Rows;
	Rows = multiplier.GetNumColumns();
	Data.resize((Rows * Columns).ToUnsignedInt());

	/* Matrix multiplication
	[a	c	e]		[g	j]		[ag + ch + ei		aj + ck + el]
	[b	d	f]	X	[h	k]	=	[bg + dh + fi		bj + dk + fl]
					[i	l]
	*/
	std::vector<Float> leftRow;
	std::vector<Float> rightColumn;
	for (Int curRow = 0; curRow < originalMatrix.GetNumRows(); ++curRow)
	{
		originalMatrix.GetRow(curRow, OUT leftRow);

		for (Int curColumn = 0; curColumn < multiplier.GetNumColumns(); ++curColumn)
		{
			multiplier.GetColumn(curColumn, OUT rightColumn);
			Data.at(((curRow * GetNumColumns()) + curColumn).ToUnsignedInt()) = DotProduct(leftRow, rightColumn);
		}
	}
}

Float& Matrix::At (UINT_TYPE dataIdx)
{
	CHECK(dataIdx < Data.size())
	return Data[dataIdx];
}

bool Matrix::SetData (const std::vector<Float>& newData)
{
	if (newData.size() != Data.size())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to set matrix data since the data size (%s) does not fit in current matrix dimensions (%s x %s)."), Int(newData.size()), Rows, Columns);
		return false;
	}

	return SetData(newData, Rows, Columns);
}

bool Matrix::SetData (const std::vector<Float>& newData, Int newNumRows, Int newNumColumns)
{
	CHECK(newNumRows >= 0 && newNumColumns >= 0) //Handle garbage data (eg:  rows=-2 & columns=-2)
	if (newData.size() != newNumRows * newNumColumns)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to set matrix attributes since the new data size (%s) does not fit in the specified dimensions (%s x %s)."), Int(newData.size()), newNumRows, newNumColumns);
		return false;
	}

	Rows = newNumRows;
	Columns = newNumColumns;
	Data = newData;
	return true;
}

Int Matrix::GetNumRows () const
{
	return Rows;
}

Int Matrix::GetNumColumns () const
{
	return Columns;
}

std::vector<Float> Matrix::GetRow (Int rowIdx) const
{
	std::vector<Float> result;
	GetRow(rowIdx, OUT result);

	return result;
}

void Matrix::GetRow (Int rowIdx, std::vector<Float>& outRowData) const
{
	/*
	To obtain row idx 1
	0	0	0	0	0
	x	x	x	x	x
	0	0	0	0	0
	*/
	outRowData.clear();
	outRowData.resize(Columns.ToUnsignedInt());
	Int dataOffset = rowIdx * GetNumColumns();
	for (Int i = 0; i < Columns; i++)
	{
		outRowData.at(i.ToUnsignedInt()) = Data.at((i + dataOffset).ToUnsignedInt());
	}
}

std::vector<Float> Matrix::GetColumn (Int columnIdx) const
{
	std::vector<Float> result;
	GetColumn(columnIdx, result);

	return result;
}

void Matrix::GetColumn (Int columnIdx, std::vector<Float>& outColumnData) const
{
	/*
	To obtain column idx 1
	0	x	0	0	0
	0	x	0	0	0
	0	x	0	0	0
	*/
	outColumnData.clear();
	outColumnData.resize(Rows.ToUnsignedInt());
	for (Int i = 0; i < Rows; i++)
	{
		outColumnData.at(i.ToUnsignedInt()) = Data.at((columnIdx + (Columns * i)).ToUnsignedInt());
	}
}

Float Matrix::GetDataElement (Int rowIdx, Int columnIdx) const
{
	/*
	To obtain row idx 1, column idx 2
	0	0	0	0	0
	0	0	x	0	0
	0	0	0	0	0
	*/
	return Data.at(GetDataIndex(rowIdx, columnIdx).ToUnsignedInt());
}

Int Matrix::GetDataIndex (Int rowIdx, Int columnIdx) const
{
	return (rowIdx * Columns) + columnIdx;
}

Float Matrix::At (UINT_TYPE dataIdx) const
{
	CHECK(dataIdx < Data.size())
	return Data[dataIdx];
}

Float Matrix::CalcDeterminant2x2 () const
{
	/*
	Determinant of a 2x2 matrix is:
	[a	b] = ad - bc
	[c	d]
	*/
	CHECK(GetNumColumns() == 2 && GetNumRows() == 2);
	return ((Data.at(0) * Data.at(3)) - (Data.at(1) * Data.at(2)));
}

Float Matrix::CalcDeterminant3x3 () const
{
	/*
	Determinant of a 3x3 matrix is:
	[0	1	2] [a	b	c] = a[e	f] - b[d	f] + c[d	e] = a(ei - fh) - b(di - fg) + c(dh - eg)
	[3	4	5] [d	e	f]	  [h	i]	  [g	i]	  [g	h]
	[6	7	8] [g	h	i]
	*/
	CHECK(GetNumColumns() == 3 && GetNumRows() == 3);
	return	(Data.at(0) * ((Data.at(4) * Data.at(8)) - (Data.at(5) * Data.at(7)))) -
			(Data.at(1) * ((Data.at(3) * Data.at(8)) - (Data.at(5) * Data.at(6)))) +
			(Data.at(2) * ((Data.at(3) * Data.at(7)) - (Data.at(4) * Data.at(6))));
}

Float Matrix::CalcDeterminant4x4 () const
{
	/*
	Complexity for determinants of increasing dimensions increases exponentially.  It may still seem safer/stable to hardcode the formula rather than figuring out the upper triangle.  Possibly performance increase?
	Determinant of a 4x4 matrix is:
	[a	b	c	d] = a	[f	g	h] - b	[e	g	h] + c	[e	f	h] - d	[e	f	g]
	[e	f	g	h]		[j	k	l]		[i	k	l]		[i	j	l]		[i	j	k]
	[i	j	k	l]		[n	o	p]		[m	o	p]		[m	n	p]		[m	n	o]
	[m	n	o	p]

	Alternatively, we can try placing this long formula which is faster than creating sub matrices:  http://stackoverflow.com/questions/2922690/calculating-an-nxn-matrix-determinant-in-c-sharp
	*/
	CHECK(GetNumColumns() == 4 && GetNumRows() == 4);

	//A
	const Matrix a = GetSubMatrix(1, 3, 1, 3);

	std::vector<Float> subMatrix;
	subMatrix.resize(9);

	//B
	subMatrix.at(0) = Data.at(4);
	subMatrix.at(1) = Data.at(6);
	subMatrix.at(2) = Data.at(7);
	subMatrix.at(3) = Data.at(8);
	subMatrix.at(4) = Data.at(10);
	subMatrix.at(5) = Data.at(11);
	subMatrix.at(6) = Data.at(12);
	subMatrix.at(7) = Data.at(14);
	subMatrix.at(8) = Data.at(15);
	const Matrix b(3, 3, subMatrix);

	//C
	//subMatrix.at(0) = Data.at(4);
	subMatrix.at(1) = Data.at(5);
	//subMatrix.at(2) = Data.at(7);
	//subMatrix.at(3) = Data.at(8);
	subMatrix.at(4) = Data.at(9);
	//subMatrix.at(5) = Data.at(11);
	//subMatrix.at(6) = Data.at(12);
	subMatrix.at(7) = Data.at(13);
	//subMatrix.at(8) = Data.at(15);
	const Matrix c(3, 3, subMatrix);

	//D
	const Matrix d = GetSubMatrix(1, 3, 0, 2);

	return ((Data.at(0) * a.CalcDeterminant3x3()) - (Data.at(1) * b.CalcDeterminant3x3()) + (Data.at(2) * c.CalcDeterminant3x3()) - (Data.at(3) * d.CalcDeterminant3x3()));
}

void Matrix::CalcAdjugate2x2 (Matrix& outAdjugate) const
{
	/*
	[a	b]	=>	[ d	-b]
	[c	d]		[-c	 a]
	*/
	outAdjugate.Data.at(0) = Data.at(3);
	outAdjugate.Data.at(1) = Data.at(1) * -1.f;
	outAdjugate.Data.at(2) = Data.at(2) * -1.f;
	outAdjugate.Data.at(3) = Data.at(0);
}

//Conversions for matrices 3x3 and 4x4 were generated from:  http://www.wolframalpha.com/input/?i=adjugate
void Matrix::CalcAdjugate3x3 (Matrix& outAdjugate) const
{
	/*
	[a	b	c]	=>	[ei-fh		ch-bi		bf-ce]
	[d	e	f]		[fg-di		ai-cg		cd-af]
	[g	h	i]		[dh-eg		bg-ah		ae-bd]
	*/

	//1st row
	outAdjugate.Data[0] = (Data[4] * Data[8]) - (Data[5] * Data[7]);
	outAdjugate.Data[1] = (Data[2] * Data[7]) - (Data[1] * Data[8]);
	outAdjugate.Data[2] = (Data[1] * Data[5]) - (Data[2] * Data[4]);

	//2nd row
	outAdjugate.Data[3] = (Data[5] * Data[6]) - (Data[3] * Data[8]);
	outAdjugate.Data[4] = (Data[0] * Data[8]) - (Data[2] * Data[6]);
	outAdjugate.Data[5] = (Data[2] * Data[3]) - (Data[0] * Data[5]);

	//3rd row
	outAdjugate.Data[6] = (Data[3] * Data[7]) - (Data[4] * Data[6]);
	outAdjugate.Data[7] = (Data[1] * Data[6]) - (Data[0] * Data[7]);
	outAdjugate.Data[8] = (Data[0] * Data[4]) - (Data[1] * Data[3]);
}

void Matrix::CalcAdjugate4x4 (Matrix& outAdjugate) const
{
	/*
	Indices			Letter mapping
	[0	1	2	3][a	b	c	d]	=>	[fkp-flo-gjp+gln+hjo-hkn	-bkp+blo+cjp-cln-djo+dkn	bgp-bho-cfp+chn+dfo-dgn		-bgl+bhk+cfl-chj-dfk+dgj]
	[4	5	6	7][e	f	g	h]		[-ekp+elo+gip-glm-hio+hkm	akp-alo-cip+clm+dio-dkm		-agp+aho+cep-chm-deo+dgm	agl-ahk-cel+chi+dek-dgi	]
	[8	9	10	11][i	j	k	l]		[ejp-eln-fip+flm+hin-hjm	-ajp+aln+bip-blm-din+djm	afp-ahn-bep+bhm+den-dfm		-afl+ahj+bel-bhi-dej+dfi]
	[12	13	14	15][m	n	o	p]		[-ejo+ekn+fio-fkm-gin+gjm	ajo-akn-bio+bkm+cin-cjm		-afo+agn+beo-bgm-cen+cfm	afk-agj-bek+bgi+cej-cfi	]
	*/

	//						first term							second term
	//						third term							fourth term
	//						fifth term							sixth term
	//1st row
	outAdjugate.Data[0] =	(Data[5] * Data[10] * Data[15]) -	(Data[5] * Data[11] * Data[14]) -
							(Data[6] * Data[9] * Data[15]) +	(Data[6] * Data[11] * Data[13]) +
							(Data[7] * Data[9] * Data[14]) -	(Data[7] * Data[10] * Data[13]);
	outAdjugate.Data[1] =	(-Data[1] * Data[10] * Data[15]) +	(Data[1] * Data[11] * Data[14]) +
							(Data[2] * Data[9] * Data[15]) -	(Data[2] * Data[11] * Data[13]) -
							(Data[3] * Data[9] * Data[14]) +	(Data[3] * Data[10] * Data[13]);
	outAdjugate.Data[2] =	(Data[1] * Data[6] * Data[15]) -	(Data[1] * Data[7] * Data[14]) -
							(Data[2] * Data[5] * Data[15]) +	(Data[2] * Data[7] * Data[13]) +
							(Data[3] * Data[5] * Data[14]) -	(Data[3] * Data[6] * Data[13]);
	outAdjugate.Data[3] =	(-Data[1] * Data[6] * Data[11]) +	(Data[1] * Data[7] * Data[10]) +
							(Data[2] * Data[5] * Data[11]) -	(Data[2] * Data[7] * Data[9]) -
							(Data[3] * Data[5] * Data[10]) +	(Data[3] * Data[6] * Data[9]);

	//2nd row
	outAdjugate.Data[4] =	(-Data[4] * Data[10] * Data[15]) +	(Data[4] * Data[11] * Data[14]) +
							(Data[6] * Data[8] * Data[15]) -	(Data[6] * Data[11] * Data[12]) -
							(Data[7] * Data[8] * Data[14]) +	(Data[7] * Data[10] * Data[12]);
	outAdjugate.Data[5] =	(Data[0] * Data[10] * Data[15]) -	(Data[0] * Data[11] * Data[14]) -
							(Data[2] * Data[8] * Data[15]) +	(Data[2] * Data[11] * Data[12]) +
							(Data[3] * Data[8] * Data[14]) -	(Data[3] * Data[10] * Data[12]);
	outAdjugate.Data[6] =	(-Data[0] * Data[6] * Data[15]) +	(Data[0] * Data[7] * Data[14]) +
							(Data[2] * Data[4] * Data[15]) -	(Data[2] * Data[7] * Data[12]) -
							(Data[3] * Data[4] * Data[14]) +	(Data[3] * Data[6] * Data[12]);
	outAdjugate.Data[7] =	(Data[0] * Data[6] * Data[11]) -	(Data[0] * Data[7] * Data[10]) -
							(Data[2] * Data[4] * Data[11]) +	(Data[2] * Data[7] * Data[8]) +
							(Data[3] * Data[4] * Data[10]) -	(Data[3] * Data[6] * Data[8]);

	//3rd row
	outAdjugate.Data[8] =	(Data[4] * Data[9] * Data[15]) -	(Data[4] * Data[11] * Data[13]) -
							(Data[5] * Data[8] * Data[15]) +	(Data[5] * Data[11] * Data[12]) +
							(Data[7] * Data[8] * Data[13]) -	(Data[7] * Data[9] * Data[12]);
	outAdjugate.Data[9] =	(-Data[0] * Data[9] * Data[15]) +	(Data[0] * Data[11] * Data[13]) +
							(Data[1] * Data[8] * Data[15]) -	(Data[1] * Data[11] * Data[12]) -
							(Data[3] * Data[8] * Data[13]) +	(Data[3] * Data[9] * Data[12]);
	outAdjugate.Data[10] =	(Data[0] * Data[5] * Data[15]) -	(Data[0] * Data[7] * Data[13]) -
							(Data[1] * Data[4] * Data[15]) +	(Data[1] * Data[7] * Data[12]) +
							(Data[3] * Data[4] * Data[13]) -	(Data[3] * Data[5] * Data[12]);
	outAdjugate.Data[11] =	(-Data[0] * Data[5] * Data[11]) +	(Data[0] * Data[7] * Data[9]) +
							(Data[1] * Data[4] * Data[11]) -	(Data[1] * Data[7] * Data[8]) -
							(Data[3] * Data[4] * Data[9]) +		(Data[3] * Data[5] * Data[8]);

	//4th row
	outAdjugate.Data[12] =	(-Data[4] * Data[9] * Data[14]) +	(Data[4] * Data[10] * Data[13]) +
							(Data[5] * Data[8] * Data[14]) -	(Data[5] * Data[10] * Data[12]) -
							(Data[6] * Data[8] * Data[13]) +	(Data[6] * Data[9] * Data[12]);
	outAdjugate.Data[13] =	(Data[0] * Data[9] * Data[14]) -	(Data[0] * Data[10] * Data[13]) -
							(Data[1] * Data[8] * Data[14]) +	(Data[1] * Data[10] * Data[12]) +
							(Data[2] * Data[8] * Data[13]) -	(Data[2] * Data[9] * Data[12]);
	outAdjugate.Data[14] =	(-Data[0] * Data[5] * Data[14]) +	(Data[0] * Data[6] * Data[13]) +
							(Data[1] * Data[4] * Data[14]) -	(Data[1] * Data[6] * Data[12]) -
							(Data[2] * Data[4] * Data[13]) +	(Data[2] * Data[5] * Data[12]);
	outAdjugate.Data[15] =	(Data[0] * Data[5] * Data[10]) -	(Data[0] * Data[6] * Data[9]) -
							(Data[1] * Data[4] * Data[10]) +	(Data[1] * Data[6] * Data[8]) +
							(Data[2] * Data[4] * Data[9]) -		(Data[2] * Data[5] * Data[8]);
	//wipe sweat
}

bool Matrix::CalcInverse2x2 (Matrix& outInverse) const
{
	//Formula for inverses:  Adjugate / Determinant
	Float deter = CalcDeterminant2x2();
	outInverse = *this;

	if (deter == 0.f)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Log, TXT("This matrix is not invertible:  %s"), ToString());
#endif
		return false;
	}

	CalcAdjugate2x2(outInverse);
	outInverse *= (1/deter);

	return true;
}

bool Matrix::CalcInverse3x3 (Matrix& outInverse) const
{
	Float deter = CalcDeterminant3x3();
	outInverse = *this;

	if (deter == 0.f)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Log, TXT("This matrix is not invertible:  %s"), ToString());
#endif
		return false;
	}

	CalcAdjugate3x3(outInverse);
	outInverse *= (1/deter);

	return true;
}

bool Matrix::CalcInverse4x4 (Matrix& outInverse) const
{
	Float deter = CalcDeterminant4x4();
	outInverse = *this;

	if (deter == 0.f)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Log, TXT("This matrix is not invertible:  %s"), ToString());
#endif
		return false;
	}

	CalcAdjugate4x4(outInverse);
	outInverse *= (1/deter);

	return true;
}

#pragma region "External Operators"
bool operator== (const Matrix& left, const Matrix& right)
{
	return (left.GetNumRows() == right.GetNumRows() && left.GetNumColumns() == right.GetNumColumns() && left.ReadData() == right.ReadData());
}

bool operator!= (const Matrix& left, const Matrix& right)
{
	return !(left == right);
}

Matrix operator+ (const Matrix& left, const Matrix& right)
{
	Matrix result(left);
	result.MatrixAdd(right);

	return result;
}

Matrix operator+= (Matrix& left, const Matrix& right)
{
	left.MatrixAdd(right);
	return left;
}

Matrix operator- (const Matrix& left, const Matrix& right)
{
	Matrix result(left);
	result.MatrixSubtract(right);

	return result;
}

Matrix operator-= (Matrix& left, const Matrix& right)
{
	left.MatrixSubtract(right);
	return left;
}

Matrix operator* (const Matrix& left, Float right)
{
	Matrix results(left);
	results.MatrixMultiply(right);

	return results;
}

Matrix operator* (Float left, const Matrix& right)
{
	Matrix results(right);
	results.MatrixMultiply(left);

	return results;
}

Matrix& operator*= (Matrix& left, Float right)
{
	left.MatrixMultiply(right);
	return left;
}

Matrix operator* (const Matrix& left, const Matrix& right)
{
	Matrix results(left);
	results.MatrixMultiply(right);

	return results;
}

Matrix& operator*= (Matrix& left, const Matrix& right)
{
	left.MatrixMultiply(right);
	return left;
}

Matrix operator/ (const Matrix& left, Float right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Matrix / Float)"), left);
		return left;
	}
#endif

	Matrix result(left);
	result.MatrixMultiply(1.f/right);

	return result;
}

Matrix operator/ (const Matrix& left, const Matrix& right)
{
	Matrix rightInverse = right.CalculateInverse();
	if (!rightInverse.IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide %s by %s since right hand side does not have an inverse matrix."), left, right);
		return left;
	}

	Matrix result(left);
	result.MatrixMultiply(rightInverse);

	return result;
}

Matrix& operator/= (Matrix& left, Float right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero! (Matrix /= Float)"), left);
		return left;
	}
#endif

	left.MatrixMultiply(1.f/right);
	return left;
}

Matrix& operator/= (Matrix& left, const Matrix& right)
{
	Matrix rightInverse = right.CalculateInverse();
	if (!rightInverse.IsValid())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot divide %s by %s since right hand side does not have an inverse matrix."), left, right);
		return left;
	}

	left.MatrixMultiply(rightInverse);
	return left;
}
#pragma endregion
SD_END