/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Entity.cpp
=====================================================================
*/

#include "ComponentIterator.h"
#include "ContainerUtils.h"
#include "DClass.h"
#include "DClassAssembler.h"
#include "DPointer.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "Utils.h"

IMPLEMENT_CLASS(SD::Entity, SD::Object)
SD_BEGIN

void Entity::InitProps ()
{
	Super::InitProps();

	bVisible = true;
	ComponentsModifier = 0;
}

unsigned int Entity::CalculateHashID () const
{
	return (Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetEntityHashNumber());
}

void Entity::Destroyed ()
{
	//Ensure to mark this object for deletion before deleting components (to slightly increase performance since the entity no longer needs to track its component modifier)
	//This also notifies Components to ignore updating Invisibility ref counters.
	Super::Destroyed();

	//Detach and destroy all components
	if (!ContainerUtils::IsEmpty(Components))
	{
		size_t i = Components.size() - 1;
		while (true)
		{
			Components.at(i)->Destroy();

			if (i == 0)
			{
				break;
			}

			--i;
		}

		ContainerUtils::Empty(OUT Components);
		ComponentsModifier = 0;
	}
}

bool Entity::RemoveComponent (EntityComponent* target)
{
	for (size_t i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == target)
		{
			//Invoke this before anything else since EntityComponent::RemoveComponentModifier references its owner
			RemoveComponentModifier(target);
			Components.at(i)->ComponentDetached();
			Components.erase(Components.begin() + i);

			return true;
		}
	}

	return false;
}

void Entity::AddComponentModifier (EntityComponent* newComponent)
{
	ComponentsModifier |= newComponent->GetObjectHash();
}

bool Entity::RemoveComponentModifier (EntityComponent* oldComponent)
{
	if (GetPendingDelete())
	{
		//Don't bother iterating through components if the Entity is being destroyed.
		return false;
	}

	bool bFoundMatch = false;

	//Iterate through all subcomponents to ensure that this entity does not possess another component of matching hash
	for (size_t i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) != oldComponent && Components.at(i)->GetObjectHash() == oldComponent->GetObjectHash())
		{
			bFoundMatch = true;
			break;
		}

		if (Components.at(i)->FindComponent(oldComponent->GetObjectHash()) != nullptr)
		{
			bFoundMatch = true;
			break;
		}
	}

	if (!bFoundMatch)
	{
		ComponentsModifier ^= oldComponent->GetObjectHash();
	}

	return !bFoundMatch;
}

EntityComponent* Entity::FindComponent (unsigned int targetComponentHash, bool bRecursive) const
{
	for (size_t i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == nullptr)
		{
			continue;
		}

		if ((Components.at(i)->GetObjectHash() & targetComponentHash) > 0)
		{
			return Components.at(i);
		}

		if (bRecursive && (Components.at(i)->GetComponentsModifier() & targetComponentHash) > 0)
		{
			EntityComponent* result = Components.at(i)->FindComponent(targetComponentHash);
			if (result != nullptr)
			{
				return result;
			}
		}
	}

	return nullptr;
}

EntityComponent* Entity::FindComponent (const DClass* targetClass, bool bRecursive) const
{
	for (size_t i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == nullptr)
		{
			continue;
		}

		if (Components.at(i)->StaticClass()->IsChildOf(targetClass))
		{
			return Components.at(i);
		}

		if (!bRecursive)
		{
			continue;
		}

		EntityComponent* result = Components.at(i)->FindComponent(targetClass);
		if (result != nullptr)
		{
			return result;
		}
	}

	return nullptr;
}

bool Entity::HasComponent (const DClass* componentClass, bool bRecursive) const
{
	return (FindComponent(componentClass, bRecursive) != nullptr);
}

void Entity::MoveComponentToFront (EntityComponent* comp)
{
	if (Components.size() == 0)
	{
		return;
	}

	size_t foundIdx = INDEX_NONE;

	//Iterate backwards since there's a good chance that the component is at the back of the list instead of the front.
	size_t i = Components.size() - 1;
	while (true)
	{
		if (Components.at(i) == comp)
		{
			foundIdx = i;
			break;
		}

		if (i == 1) //The component either doesn't exist in the list or is already in the front of the component list.
		{
			return;
		}

		--i;
	}

	CHECK(foundIdx != INDEX_NONE)
	Components.erase(Components.begin() + foundIdx);
	Components.insert(Components.begin(), comp);
}

void Entity::MoveComponentToBack (EntityComponent* comp)
{
	size_t foundIdx = INDEX_NONE;
	for (size_t i = 0; i < Components.size(); ++i)
	{
		if (Components.at(i) == comp)
		{
			foundIdx = i;
			break;
		}
	}

	if (foundIdx == INDEX_NONE)
	{
		return;
	}

	Components.erase(Components.begin() + foundIdx);
	Components.push_back(comp);
}

void Entity::SetVisibility (bool newVisibility)
{
	if (newVisibility == bVisible)
	{
		return;
	}

	bVisible = newVisibility;
	Int refCountMultiplier = (bVisible) ? -1 : 1;

	//Increment the EntityComponents' visibility reference counter.
	for (ComponentIterator iter(this, true); iter; iter++)
	{
		EntityComponent* selectedComp = iter.GetSelectedComponent();
		selectedComp->NumInvisibleOwners = Utils::Max<Int>(0, selectedComp->NumInvisibleOwners + (1 * refCountMultiplier));
	}
}

bool Entity::IsVisible () const
{
	return bVisible;
}

unsigned int Entity::GetComponentsModifier () const
{
	return ComponentsModifier;
}

bool Entity::AddComponent_Implementation (EntityComponent* newComponent)
{
	CHECK(newComponent != nullptr)

	if (newComponent->CanBeAttachedTo(this))
	{
		newComponent->DetachSelfFromOwner();
		newComponent->AttachTo(this);

#ifdef DEBUG_MODE
		for (size_t i = 0; i < Components.size(); i++)
		{
			if (Components.at(i) == newComponent)
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("%s is already registered to %s.  Having duplicate entries in the component list would cause unreliable behavior when removing components.  This check is only executed in debug builds."), newComponent->ToString(), ToString()));
				return false;
			}
		}
#endif
		Components.push_back(newComponent);
		AddComponentModifier(newComponent);
		return true;
	}

	return false;
}

#ifdef DEBUG_MODE
void Entity::DebugTick (Float deltaTime)
{
	//Noop
}
#endif
SD_END