/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Version.cpp
=====================================================================
*/

#include "DString.h"
#include "Int.h"
#include "Version.h"

SD_BEGIN
const Int Version::SAND_DUNE_MAJOR_VERSION = 0;
const Int Version::SAND_DUNE_MINOR_VERSION = 4;

DString Version::GetVersionNumber ()
{
	static const DString formattedStr = SAND_DUNE_MAJOR_VERSION.ToString() + TXT(".") + SAND_DUNE_MINOR_VERSION.ToString();

	return formattedStr;
}
SD_END