/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SfmlOutputStream.cpp
=====================================================================
*/

#include "CoreMacros.h"
#include "SfmlOutputStream.h"

SD_BEGIN
bool SfmlOutputStream::OutputActive = false;

SfmlOutputStream::SfmlOutputStream ()
{
	CHECK_INFO(!OutputActive, TXT("SFML's output is already redirected by another instance of SfmlOutputStream. Ensure the previous instance is destroyed before creating a new one."))

	OriginalSfmlBuffer = sf::err().rdbuf();
	sf::err().rdbuf(SfmlOutput.rdbuf());
	OutputActive = true;
}

SfmlOutputStream::~SfmlOutputStream ()
{
	sf::err().rdbuf(OriginalSfmlBuffer); //restore sfml output stream
	OutputActive = false;
}

DString SfmlOutputStream::ReadOutput () const
{
	return DString(SfmlOutput.str());
}
SD_END