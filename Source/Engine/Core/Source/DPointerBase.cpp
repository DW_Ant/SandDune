/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerBase.cpp
=====================================================================
*/

#include "DPointerBase.h"

SD_BEGIN
DPointerBase::DPointerBase ()
{
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}

DPointerBase::DPointerBase (const DPointerBase& other)
{
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}

DPointerBase::~DPointerBase ()
{

}

DPointerBase* DPointerBase::GetPointerAddress ()
{
	return this;
}

void DPointerBase::RegisterDPointer (const DPointerInterface* referencedObj)
{
	if (referencedObj == nullptr || !referencedObj->CanBePointed())
	{
		return;
	}

	DPointerBase* oldLeadingPointer = referencedObj->GetLeadingDPointer();
	if (oldLeadingPointer != nullptr && oldLeadingPointer != this)
	{
		//Insert this pointer to be at the front of the linked list
		oldLeadingPointer->PreviousPointer = this;
		NextPointer = oldLeadingPointer;
	}

	referencedObj->SetLeadingDPointer(this);
	PreviousPointer = nullptr;
}

void DPointerBase::RemoveDPointer (const DPointerInterface* oldReferencedObj)
{
	if (PreviousPointer == nullptr)
	{
		if (oldReferencedObj != nullptr)
		{
			//Need to reassign the object's leading pointer
			oldReferencedObj->SetLeadingDPointer(oldReferencedObj->CanBePointed() ? NextPointer : nullptr);
		}
	}
	else
	{
		PreviousPointer->NextPointer = NextPointer;
	}

	if (NextPointer != nullptr)
	{
		NextPointer->PreviousPointer = PreviousPointer;
	}

	//Sever this pointer from any linked list
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}
SD_END