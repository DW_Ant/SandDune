#########################################################################
# Establish directories
#########################################################################
include_directories ("Headers"
	"${ExternalDirectory}/SFML/Include"
	"${ExternalDirectory}/utf8_v2_3_4/source")
	
link_directories(${ExternalDirectory}/SFML/lib/${ArchitectureFolderName})

SET (CoreDirectory "${SourceDirectory}/Engine/Core")
SET (WorkingDirectory ${CoreDirectory})


#########################################################################
# Establish source files
#########################################################################

SET (HeaderFiles
	"Headers/BaseUtils.h"
	"Headers/Bool.h"
	"Headers/ClassIterator.h"
	"Headers/ComponentIterator.h"
	"Headers/Configuration.h"
	"Headers/ContainerUtils.h"
	"Headers/CopiableObjectInterface.h"
	"Headers/Core.h"
	"Headers/CoreDatatypes.h"
	"Headers/CoreMacros.h"
	"Headers/CoreUtilUnitTester.h"
	"Headers/DataBuffer.h"
	"Headers/DatatypeUnitTester.h"
	"Headers/DClass.h"
	"Headers/DClassAssembler.h"
	"Headers/Definitions.h"
	"Headers/DestroyNotifyComponent.h"
	"Headers/DPointer.h"
	"Headers/DPointerBase.h"
	"Headers/DPointerInterface.h"
	"Headers/DPointerTester.h"
	"Headers/DProperty.h"
	"Headers/DString.h"
	"Headers/Engine.h"
	"Headers/EngineComponent.h"
	"Headers/EngineIntegrityTester.h"
	"Headers/EngineIntegrityUnitTester.h"
	"Headers/Entity.h"
	"Headers/EntityComponent.h"
	"Headers/HashedString.h"
	"Headers/Int.h"
	"Headers/LifeSpanComponent.h"
	"Headers/LogCategory.h"
	"Headers/Matrix.h"
	"Headers/MulticastDelegate.h"
	"Headers/Object.h"
	"Headers/ObjectIterator.h"
	"Headers/PlatformMac.h"
	"Headers/PlatformWindows.h"
	"Headers/Range.h"
	"Headers/Rectangle.h"
	"Headers/ResourcePool.h"
	"Headers/ResourceTag.h"
	"Headers/ResourcePoolMacros.h"
	"Headers/ResourceTree.h"
	"Headers/Rotator.h"
	"Headers/SDFloat.h"
	"Headers/SDFunction.h"
	"Headers/SDFunctionTester.h"
	"Headers/SfmlOutputStream.h"
	"Headers/Stopwatch.h"
	"Headers/StringIterator.h"
	"Headers/TickComponent.h"
	"Headers/TickGroup.h"
	"Headers/TickTester.h"
	"Headers/TransformMatrix.h"
	"Headers/UnitTester.h"
	"Headers/UnitTestLauncher.h"
	"Headers/Utils.h"
	"Headers/Vector2.h"
	"Headers/Vector3.h"
	"Headers/Version.h"
)

SET (SourceFiles
	"Source/BaseUtils.cpp"
	"Source/Bool.cpp"
	"Source/ClassIterator.cpp"
	"Source/ComponentIterator.cpp"
	"Source/ContainerUtils.cpp"
	"Source/CopiableObjectInterface.cpp"
	"Source/CoreUtilUnitTester.cpp"
	"Source/DataBuffer.cpp"
	"Source/DatatypeUnitTester.cpp"
	"Source/DClass.cpp"
	"Source/DClassAssembler.cpp"
	"Source/Definitions.cpp"
	"Source/DestroyNotifyComponent.cpp"
	"Source/DPointerBase.cpp"
	"Source/DPointerInterface.cpp"
	"Source/DPointerTester.cpp"
	"Source/DProperty.cpp"
	"Source/DString.cpp"
	"Source/Engine.cpp"
	"Source/EngineComponent.cpp"
	"Source/EngineIntegrityTester.cpp"
	"Source/EngineIntegrityUnitTester.cpp"
	"Source/Entity.cpp"
	"Source/EntityComponent.cpp"
	"Source/HashedString.cpp"
	"Source/Int.cpp"
	"Source/LifeSpanComponent.cpp"
	"Source/LogCategory.cpp"
	"Source/Matrix.cpp"
	"Source/Object.cpp"
	"Source/ObjectIterator.cpp"
	"Source/PlatformMac.cpp"
	"Source/PlatformWindows.cpp"
	"Source/Rectangle.cpp"
	"Source/ResourcePool.cpp"
	"Source/ResourceTag.cpp"
	"Source/Rotator.cpp"
	"Source/SDFloat.cpp"
	"Source/SDFunctionTester.cpp"
	"Source/SfmlOutputStream.cpp"
	"Source/Stopwatch.cpp"
	"Source/StringIterator.cpp"
	"Source/TickComponent.cpp"
	"Source/TickGroup.cpp"
	"Source/TickTester.cpp"
	"Source/TransformMatrix.cpp"
	"Source/UnitTester.cpp"
	"Source/UnitTestLauncher.cpp"
	"Source/Utils.cpp"
	"Source/Vector2.cpp"
	"Source/Vector3.cpp"
	"Source/Version.cpp"
)


#########################################################################
# Auto Generate Include File
#########################################################################

#Convert header list to C++ include directives
SET (IncludeList ${HeaderFiles})
GenerateIncludeList(IncludeList)

#Set definitions used in generated header file.
SET (PUBLIC_INCLUDE_LIST ${IncludeList})
SET (MODULE_NAME "Core")
SET (PREPROCESSOR_MODULE_NAME "CORE")

#Configure the header file to pass some of the CMake settings to the source code.
configure_file (
	"${WorkingDirectory}/CMakeConfiguration/${MODULE_NAME}Classes.h.in"
	"${WorkingDirectory}/Headers/${MODULE_NAME}Classes.h"
)

#Add the generated header file to the file list
list (APPEND HeaderFiles "Headers/${MODULE_NAME}Classes.h")

	
#########################################################################
# Build the module
#########################################################################

SET (AllFiles ${HeaderFiles} ${SourceFiles})

#Generate project filters that align with the source file's directory location.
ConfigureProjectFilters("${AllFiles}")

add_compile_definitions(${PREPROCESSOR_MODULE_NAME}_EXPORT)

add_library(SD-${MODULE_NAME} SHARED ${AllFiles})
ApplyTargetConfiguration(SD-${MODULE_NAME})

set_target_properties (SD-${MODULE_NAME} PROPERTIES FOLDER "Engine")

target_link_libraries(SD-${MODULE_NAME}
	PRIVATE optimized sfml-system
	PRIVATE debug sfml-system-d)

#Add Window's specific lib (Needed for RoInitialize)
if (MSVC)
	target_link_libraries(SD-${MODULE_NAME}
		PRIVATE optimized runtimeobject.lib
		PRIVATE debug runtimeobject.lib)
endif (MSVC)