/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerInterface.h

  This interface allows an object to possess a linked list of DPointers.  Each DPointer that references the
  object (that implements this interface) will be within the object's DPointer linked list.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DPointerBase;

class CORE_API DPointerInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if DPointers are allowed to point at this object.  Otherwise,
	 * the pointer will be assigned to nullptr instead.
	 */
	virtual bool CanBePointed () const = 0;

	/**
	 * Assigns this DPointer object to be first in the object's DPointer linked list.
	 */
	virtual void SetLeadingDPointer (DPointerBase* newLeadingPointer) const = 0;

	/**
	 * Retrieves the first DPointer in the object's DPointer linked list.
	 */
	virtual DPointerBase* GetLeadingDPointer () const = 0;

	/**
	 * Iterates through all pointers pointing at this object, and clears those pointers (point at nullptr).
	 * This is typically called if this object is about to be destroyed. This is to prevent any dangling pointers.
	 */
	virtual void ClearPointersPointingAtThis ();
};
SD_END