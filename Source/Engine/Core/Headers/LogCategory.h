/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LogCategory.h
  A log category defines a few filters that determines when and where a collection
  of log messages should serialize and display.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"

SD_BEGIN
class CORE_API LogCategory
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/**
	 * Various log level classifications ordered in ascending order where 0 is the harmless log
	 * message, and highest values are the most important log messages.
	 */
	enum ELogLevel
	{
#ifdef DEBUG_MODE
		LL_Debug = 1, //Log message only available when debugging.  Generally this is reserved for temporary logs or logs that should not be produced in final product.  Generates compiler errors when used in Release builds.
#endif
		LL_Verbose = 2, //Log message that's typically spammy.
		LL_Log = 4, //Ordinary harmless log.
		LL_Warning = 8, //A log message describing an error.
		LL_Critical = 16, //A log message describing a major error that is likely to cause further issues.
		LL_Fatal = 32 //A log message describing the engine crash.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various flags that determines where these log messages may appear. */
	//If true, then these log messages will appear in standard cout.
	static const unsigned int FLAG_STANDARD_OUTPUT;

	//If true, then these log messages will appear in operating system specific output capturing utilities
	//In windows, this invokes OutputDebugString for Visual Studio.
	static const unsigned int FLAG_OS_OUTPUT;

	//If true, then these log messages will appear in logging file.
	static const unsigned int FLAG_LOG_FILE;

	//If true, then these log messages will appear in external output window.
	static const unsigned int FLAG_OUTPUT_WINDOW;

	//If true, then these log messages will appear in console UI.
	static const unsigned int FLAG_CONSOLE_MSG;

	static const unsigned int FLAG_ALL;

	/* Returns the verbosity value that contains all verbosity flags. */
	static const unsigned int VERBOSITY_ALL;

	/* Returns the verbosity value that contains everything except for Verbose. */
	static const unsigned int VERBOSITY_DEFAULT;

protected:
	/* Human readable log title used to describe this log group in a few words. */
	DString LogTitle;

	/* Bit wise flags that determines the which log levels will be processed.
	Logs with mismatching bitwise flags will be ignored. */
	unsigned int VerbosityFlags;

	/* Integer representing all log flag permutations that determines which logging utilities will process this category. */
	unsigned int UsageFlags;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	LogCategory (const DString& inLogTitle, unsigned int inVerbosityFlags, unsigned int inUsageFlags);
	virtual ~LogCategory ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Converts the given log level to a string.
	 */
	static DString LogLevelToString (ELogLevel logLevel);

	/**
	 * Passes a log event to the Engine for serialization and/or display.
	 */
	void Log (ELogLevel logLevel, const DString& msg) const;

	/**
	 * Returns true if the current log level configuration allows the specified log level to be processed.
	 */
	bool IsLogLevelRelevant (ELogLevel logLevel) const;

	void SetVerbosityFlags (unsigned int newVerbosityFlags);
	void SetUsageFlags (unsigned int newUsageFlags);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DString GetLogTitle () const;
	const DString& ReadLogTitle () const;
	unsigned int GetVerbosityFlags () const;
	unsigned int GetUsageFlags () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Relays the given log message to the local engine instance.
	 * Handled separately from the templated function to avoid circular dependency between Engine and LogCategory.
	 */
	virtual void SendLogToEngine (ELogLevel logLevel, const DString& msg) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Passes a log event to the Engine for serialization and/or display.
	 * Each templated argument is expected to have ToString method implemented.
	 */
	template<class ... Types>
	void Log (ELogLevel logLevel, const DString& msg, Types ... parameters) const
	{
		if (IsLogLevelRelevant(logLevel))
		{
			DString formattedMsg = DString::CreateFormattedString(msg, parameters ...);
			SendLogToEngine(logLevel, formattedMsg);
		}
	}
};

DEFINE_ENUM_FUNCTIONS(LogCategory::ELogLevel);

//Log Category defines
extern CORE_API LogCategory CoreLog;

#ifdef DEBUG_MODE
extern CORE_API LogCategory UnitTestLog;
#endif
SD_END