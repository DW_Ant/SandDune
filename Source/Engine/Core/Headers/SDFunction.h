/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SDFunction.h
  The SDFunction's sole purpose is to associate a std::function with
  an identifier to be able to debug and compare delegates.
=====================================================================
*/

#pragma once

#include "DString.h"
#include "DProperty.h"

/**
  Quick macro to define a SDFunction, bind function to a handler, and ensure the function name is consistent with the function wrapper.
  functionOwner is the actual object that owns the event handler.  className is the functionOwner's class (not a string).
  ReturnValue and parameters are types to pass into the template parameters.
  When defining parameter arguments, be sure to use the correct SDFunction to insert placeholders in function binds.  Format is:  SDFUNCTION_#PARAM()
 */
#define SDFUNCTION(functionOwner, className, functionName, returnValue) \
	SD::SDFunction<returnValue>(std::bind(&##className##::##functionName##, functionOwner), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_1PARAM(functionOwner, className, functionName, returnValue, param1) \
	SD::SDFunction<returnValue, param1>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_2PARAM(functionOwner, className, functionName, returnValue, param1, param2) \
	SD::SDFunction<returnValue, param1, param2>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_3PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3) \
	SD::SDFunction<returnValue, param1, param2, param3>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_4PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4) \
	SD::SDFunction<returnValue, param1, param2, param3, param4>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_5PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4, param5) \
	SD::SDFunction<returnValue, param1, param2, param3, param4, param5>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_6PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4, param5, param6) \
	SD::SDFunction<returnValue, param1, param2, param3, param4, param5, param6>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6), functionOwner, TXT(#className "::" #functionName))

SD_BEGIN
//Template arguments:  ReturnType, ParameterTypes
template <class R, class ... P>
class SDFunction : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Object that wraps the function pointer. */
	std::function<R(P...)> Function;

	/* Becomes true if an anonymous function/lambda is bound to Function. Although lambdas are great for
	inlining function definitions, it'll prohibit SDFunction == comparisons. */
	bool bUsingLambda;

	/* Pointer to the object that implements the callback.  This is strictly used for fingerprinting.
	FriendlyName, alone, is not sufficient since multiple objects could be implement the same callback function. */
	const void* FunctionOwner;

	/* Name of the function to make it easier for identifying and comparing.  Typically the
	format is ObjectName::MemberFunction.  This convention is not strictly enforced but
	is recommended to avoid name conflicts and for consistency. */
	DString FriendlyName;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SDFunction ()
	{
		ClearFunction();
	}

	SDFunction (std::function<R(P...)> inFunction) :
		Function(inFunction),
		bUsingLambda(inFunction != nullptr),
		FunctionOwner(nullptr),
		FriendlyName(DString::EmptyString)
	{
		//Noop
	}

	SDFunction (std::function<R(P...)> inFunction, const void* inFunctionOwner, const DString& inFriendlyName) :
		Function(inFunction),
		bUsingLambda(false),
		FunctionOwner(inFunctionOwner),
		FriendlyName(inFriendlyName)
	{
		//Noop
	}

	SDFunction (const SDFunction& otherFunction) :
		Function(otherFunction.Function),
		bUsingLambda(otherFunction.bUsingLambda),
		FunctionOwner(otherFunction.FunctionOwner),
		FriendlyName(otherFunction.FriendlyName)
	{
		//Noop
	}

	virtual ~SDFunction ()
	{

	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override
	{
		ClearFunction();
	}

	virtual DString ToString () const override
	{
		if (bUsingLambda && Function != nullptr)
		{
			return TXT("AnonymousFunction");
		}

		return FriendlyName;
	}

	virtual size_t GetMinBytes () const override
	{
		return sizeof(Function) + sizeof(FunctionOwner) + FriendlyName.GetMinBytes();
	}

	void ParseString (const DString& str) override
	{
#ifdef DEBUG_MODE
		//Initializing a SDFunction from a string is not supported.
		//CHECK macro is not used here to avoid circular dependency with Engine
		assert(false);
#endif
	}

	virtual void Serialize (DataBuffer& outData) const override
	{
#ifdef DEBUG_MODE
		//Serializing SDFunctions to data buffers is not supported.
		//CHECK macro is not used here to avoid circular dependency with Engine
		assert(false);
#endif
	}

	virtual bool Deserialize (const DataBuffer& dataBuffer) override
	{
#ifdef DEBUG_MODE
		//Deserializing SDFunctions from data buffers is not supported.
		//CHECK macro is not used here to avoid circular dependency with Engine
		assert(false);
#endif
		return false;
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const SDFunction& otherFunction)
	{
		Function = otherFunction.Function;
		bUsingLambda = otherFunction.bUsingLambda;
		FunctionOwner = otherFunction.FunctionOwner;
		FriendlyName = otherFunction.FriendlyName;
	}

	void operator= (std::function<R(P...)> lambda)
	{
		Function = lambda;
		bUsingLambda = (Function != nullptr);
		FunctionOwner = nullptr;
		FriendlyName = DString::EmptyString;
	}

	/**
	 * std::functions can't compare itself to other functions.
	 * Compare function owners and strings instead.
	 */
	bool operator== (const SDFunction& otherFunction) const
	{
		if (bUsingLambda || otherFunction.bUsingLambda)
		{
			return false;
		}

		return (FunctionOwner == otherFunction.FunctionOwner && ReadFriendlyName() == otherFunction.ReadFriendlyName());
	}

	inline bool operator!= (const SDFunction& otherFunction) const
	{
		return !(operator==(otherFunction));
	}

	inline R operator() (P... params) const
	{
		return Function(params...);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Removes the function binding, and sets all member variables to empty.
	 */
	void ClearFunction ()
	{
		Function = nullptr;
		bUsingLambda = false;
		FunctionOwner = nullptr;
		FriendlyName = DString::EmptyString;
	}

	/**
	 * Returns true if the function is bound to a function.
	 */
	inline bool IsBounded () const
	{
		return (Function != nullptr);
	}

	/**
	 * Same as the operator(), but as its own method to invoke the delegate.
	 * Useful for cases where you're calling a SDFunction from a vector.
	 */
	inline R Execute (P... params) const
	{
		return Function(params...);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsUsingLambda () const
	{
		return bUsingLambda;
	}

	inline DString GetFriendlyName () const
	{
		return FriendlyName;
	}

	inline const DString& ReadFriendlyName () const
	{
		return FriendlyName;
	}

	DString GetEventHandlerName () const
	{
		Int colonIdx = FriendlyName.Find(TXT("::"), 0, DString::CC_CaseSensitive);
		if (colonIdx == INT_INDEX_NONE || colonIdx <= 0)
		{
			return DString::EmptyString;
		}

		return FriendlyName.SubString(0, colonIdx - 1);
	}

	DString GetFunctionName () const
	{
		Int colonIdx = FriendlyName.Find(TXT("::"), 0, DString::CC_CaseSensitive);
		if (colonIdx == INT_INDEX_NONE)
		{
			return DString::EmptyString;
		}

		colonIdx += 2; //Consider text after the colons
		if (colonIdx < FriendlyName.Length())
		{
			return FriendlyName.SubString(colonIdx);
		}

		return DString::EmptyString;
	}

	inline const void* GetFunctionOwner () const
	{
		return FunctionOwner;
	}
};
SD_END