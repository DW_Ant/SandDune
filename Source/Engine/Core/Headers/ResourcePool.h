/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourcePool.h
  ResourcePool is the parent class of all resource managers.  A resource pool
  is responsible for tracking a reference to a particular resource and allow
  external objects to import or reuse resources.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Object.h"

SD_BEGIN
class CORE_API ResourcePool : public Object
{
	DECLARE_CLASS(ResourcePool)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* ThreadID this resource pool resides in.  This is accessed when iterating through ResourcePoolMacros.
	This is accessed from multiple threads, and it's not thread-safe to modify this variable after registered to resource pools vector. */
	std::thread::id ThreadID;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ReleaseResources () = 0;
};
SD_END