/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerTester.h
  Entity that simply references other DPointerTesters.

  This class is intended to test the DPointer property, and is only available in debug builds.
=====================================================================
*/

#pragma once

#include "Entity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API DPointerTester : public Entity
{
	DECLARE_CLASS(DPointerTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The primary test pointer.  This is used for testing accessing, assignment, and clearing. */
	DPointer<DPointerTester> OtherTester;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
};

/**
 * Simple pointer tester that can be copied and compared to other testers.
 * This is similar to the PointerTester with the exception that this object can be copied and compared against other instances.
 */
class PrimitivePointerTester : public DPointerInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The primary test pointer.  This is used for testing accessing, assignment, and clearing. */
	DPointer<DPointerTester> OtherTester;

private:
	mutable DPointerBase* LeadingPointer;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool CanBePointed () const override;
	virtual void SetLeadingDPointer (DPointerBase* newLeadingPointer) const override;
	virtual DPointerBase* GetLeadingDPointer () const override;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PrimitivePointerTester ();
	PrimitivePointerTester (const PrimitivePointerTester& other);
	virtual ~PrimitivePointerTester ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	bool operator== (const PrimitivePointerTester& other) const;
	bool operator!= (const PrimitivePointerTester& other) const;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString ToString () const;
};
SD_END

#endif