/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourceTree.h
  A tree that maps a variable to a string of paths. This provides a variable type with a unique
  string alias that can be searched in log(n) time (possibly in linear time for shallow paths).

  The purpose of this type is to give ResourcePools the ability to organize their assets in a tree structure
  for fast searching by name. All string comparisons are case sensitive.

  The Root should not have any items. However its branches may or may not have items.
  All leaves will have an item otherwise they would collapse when removed.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"

SD_BEGIN
template<class T>
class ResourceTree
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke to delete the item. If it's not bound to a function, it'll default to "delete Item".
	This is primarily used for Items with custom destruction methods. For example Object::Destroy(). */
	std::function<void(T*)> OnDeleteOverride;

protected:
	/* The distinct name/alias for this node. This name must be unique to sibling branches (but doesn't have to be unique to parent or child branches). */
	DString Name;

	/* List of branches that recurisvely have their own unique path + names.
	Note: This is a list instead of a vector since ResourceTrees cannot guarantee continuous memory. The ResourceTrees do not support copy constructors due to pointer ownership so vectors are avoided. */
	std::list<ResourceTree> Branches;

	/* The actual item associated with this node. Can be nullptr if this node doesn't have any data. Cannot be nullptr for leaves. */
	T* Item;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	//Construct root
	ResourceTree () :
		OnDeleteOverride(nullptr),
		Name(DString::EmptyString),
		Item(nullptr)
	{
		//Noop
	}

	//Construct branch
	ResourceTree (const DString& inName, std::function<void(T*)> inDeleteOverride) :
		OnDeleteOverride(inDeleteOverride),
		Name(inName),
		Item(nullptr)
	{
		//Noop
	}

	ResourceTree (ResourceTree&& moveFrom) noexcept :
		OnDeleteOverride(std::move(moveFrom.OnDeleteOverride)),
		Name(std::move(moveFrom.Name)),
		Branches(std::move(moveFrom.Branches)),
		Item(std::exchange(moveFrom.Item, nullptr)) //Clear branches from moveFrom so that when it's destroyed, it doesn't empty this tree.
	{
		//Noop
	}

	~ResourceTree ()
	{
		EmptyTree();
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the node to the specified path. This may add branches depending on the path.
	 * Returns true if the item was added to the tree.
	 */
	bool AddItem (const std::vector<DString>& path, T* newItem)
	{
		if (ContainerUtils::IsEmpty(path))
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot add item to the resource tree without associating an alias or path to it."));
			return false;
		}

		//Ensure none of the paths are empty since empty strings are reserved for roots.
		for (size_t i = 0; i < path.size(); ++i)
		{
			if (path.at(i).IsEmpty())
			{
				CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot add item to the resource tree since the path (%s) has an empty path. Empty paths are reserved for roots."), PathToString(path));
				return false;
			}
		}

		return AddItemIndexed(path, newItem, 0);
	}

	/**
	 * Searches and returns the item that matches the given path.
	 * @param bLogOnMissing - Displays a log warning if the specified path does not return anything.
	 */
	T* FindItem (const std::vector<DString>& path, bool bLogOnMissing = true) const
	{
		T* result = FindItemIndexed(path, 0);
		if (result == nullptr && bLogOnMissing)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to find resource at %s."), PathToString(path));
		}

		return result;
	}

	/**
	 * Returns all items within the specified path.
	 */
	void FindItems (const std::vector<DString>& path, std::vector<T*>& outItems) const
	{
		FindItemsIndexed(path, 0, OUT outItems);
	}

	/**
	 * Searches and deletes an item from this tree. Returns true if the item was found and removed.
	 * Branches will only collapse if there aren't any items within any of the subbranches.
	 */
	bool DeleteItem (const std::vector<DString>& path)
	{
		T* item = RemoveItemIndexed(path, 0);
		if (item != nullptr)
		{
			if (OnDeleteOverride != nullptr)
			{
				OnDeleteOverride(item);
			}
			else
			{
				delete item;
			}

			return true;
		}

		return false;
	}

	/**
	 * Searches and removes an item from this tree. This will NOT delete the item. Instead, it'll return the item that was removed.
	 * Returns a nullptr if the given path is not associated with an item.
	 */
	T* RemoveItem (const std::vector<DString>& path)
	{
		return RemoveItemIndexed(path, 0);
	}

	/**
	 * Recursively deletes the item and all branches deriving from this node.
	 * This function doesn't delete this particular node.
	 */
	void EmptyTree ()
	{
		Branches.clear(); //deletes the branches

		if (Item != nullptr)
		{
			if (OnDeleteOverride != nullptr)
			{
				OnDeleteOverride(Item);
			}
			else
			{
				delete Item;
			}

			Item = nullptr;
		}
	}

	/**
	 * Returns true if this node or any of its branches contains an item.
	 */
	bool ContainsAnItem () const
	{
		if (Item != nullptr)
		{
			return true;
		}

		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			if (iter->ContainsAnItem())
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns the string representation of the given path where each element is delimited by commas.
	 */
	static DString PathToString (const std::vector<DString>& path)
	{
		DString result(DString::EmptyString);
		for (size_t i = 0; i < path.size(); ++i)
		{
			result += path.at(i) + TXT(",");
		}

		//trim last comma
		result.PopBack();
		return result;
	}

	inline bool IsRoot () const
	{
		return Name.IsEmpty();
	}


	/*
	=====================
	  Accessors
	=====================
	*/

	/**
	 * Returns all items within this branch.
	 */
	void GetAllItems (std::vector<T*>& outItems) const
	{
		if (Item != nullptr)
		{
			outItems.push_back(Item);
		}

		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			iter->GetAllItems(OUT outItems);
		}
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Same as AddItem except for the index value specifies where in the path vector is being used in this recursive function.
	 */
	bool AddItemIndexed (const std::vector<DString>& path, T* newItem, size_t pathIdx)
	{
		if (pathIdx >= path.size())
		{
			//inner most node, add the item here.
			if (Item != nullptr)
			{
				//This path is already in use. Print warning and don't replace it.
				CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to add item to %s since that path is already occupied."), PathToString(path));
				return false;
			}

			Item = newItem;
			return true;
		}

		//Check if the branch already exists
		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			if (iter->Name.Compare(path.at(pathIdx), DString::CC_CaseSensitive) == 0)
			{
				//Insert the item down this branch and advance the path index.
				return iter->AddItemIndexed(path, newItem, pathIdx + 1);
			}
		}

		//Add a branch
		Branches.emplace_back(path.at(pathIdx), OnDeleteOverride);

		//Add the node to the newly created branch recursively. This also advances the path index.
		return Branches.back().AddItemIndexed(path, newItem, pathIdx + 1);
	}

	T* FindItemIndexed (const std::vector<DString>& path, size_t pathIdx) const
	{
		if (pathIdx >= path.size())
		{
			return Item;
		}

		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			if (iter->Name.Compare(path.at(pathIdx), DString::CC_CaseSensitive) == 0)
			{
				return iter->FindItemIndexed(path, pathIdx + 1);
			}
		}

		return nullptr;
	}

	void FindItemsIndexed (const std::vector<DString>& path, size_t pathIdx, std::vector<T*>& outItems) const
	{
		if (pathIdx >= path.size())
		{
			GetAllItems(OUT outItems);
			return;
		}

		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			if (iter->Name.Compare(path.at(pathIdx), DString::CC_CaseSensitive) == 0)
			{
				iter->FindItemsIndexed(path, pathIdx + 1, OUT outItems);
				return;
			}
		}
	}

	T* RemoveItemIndexed (const std::vector<DString>& path, size_t pathIdx)
	{
		if (pathIdx >= path.size())
		{
			T* result = Item;
			Item = nullptr;
			return result;
		}

		for (auto iter = Branches.begin(); iter != Branches.end(); ++iter)
		{
			if (iter->Name.Compare(path.at(pathIdx), DString::CC_CaseSensitive) == 0)
			{
				T* result = iter->RemoveItemIndexed(path, pathIdx + 1);
				if (result != nullptr)
				{
					//collapse branch if it's empty
					if (!iter->ContainsAnItem())
					{
						Branches.erase(iter);
					}

					return result;
				}
			}
		}

		return nullptr;
	}
};
SD_END