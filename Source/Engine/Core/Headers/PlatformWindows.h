/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindows.h
  Includes any Windows-specific libraries and definitions.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "Core.h"
#include "CoreMacros.h"

#ifdef PLATFORM_WINDOWS

//HACK:  Need to redirect INT so the typedef INT in minwindef.h doesn't override SD INT when working with external namespaces
#define INT MS_INT

//Do the same for other datatypes
#define FLOAT MS_FLOAT

//Platform-specific includes
#include <windows.h>
#include <shellapi.h>

#undef FLOAT
#undef INT

#if _WIN32
#include "..\Windows\resource.h"
#endif


/*
=====================
  Defines
=====================
*/

// Get 32/64 bit defines
#if _WIN64
#define PLATFORM_64BIT
#elif _WIN32
#define PLATFORM_32BIT
#else
#error "Please define your platform."
#endif

SD_BEGIN
class DString;
class Int;
class Rectangle;


/*
=====================
  Methods
=====================
*/

/**
 * Setup any platform-specific initialization.
 */
void CORE_API InitializePlatform (bool bMainThread);

/**
 * Shutdown any platform-specific functionality.
 * If bMainThread is true, then assume that the application is terminating instead of a thread.
 */
void CORE_API ShutdownPlatform (bool bMainThread);

void CORE_API PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle);

/**
 * Informs the operating system to freeze this current thread for the specified amount.
 * This function does not return until enough time elapsed.
 */
void CORE_API OS_Sleep (Int milliseconds);

/**
 * Copies the copyContent to the OS clipboard.  Returns true if successful.
 */
bool CORE_API OS_CopyToClipboard (const DString& copyContent);

/**
 * Retrieves the text buffer from the OS clipboard, and returns as result as string.
 * Returns an empty string if clipboard is empty, or if application was unable to paste from clipboard.
 */
DString CORE_API OS_PasteFromClipboard ();

/**
 * Sends a signal to allow an IDE to break the program for debugging.
 * Execution should halt if the signal was captured.
 */
void CORE_API OS_BreakExecution ();
SD_END

#endif //PLATFORM_WINDOWS