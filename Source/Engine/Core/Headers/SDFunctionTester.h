/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SDFunctionTester.h
  Object responsible for assisting the DatatypeUnitTester's SDFunction and MulticastDelegate tests.

  In short, this object will bind a couple SDFunctions to its event handlers.
  The test will only become successful if all functions were invoked.
=====================================================================
*/

#pragma once

#include "CoreDatatypes.h"
#include "MulticastDelegate.h"
#include "Object.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API SDFunctionTester : public Object
{
	DECLARE_CLASS(SDFunctionTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various flags that all must be true to be considered a successful test. */
	bool bTestedVoid;
	bool bTestedParamINT;
	bool bTestedMultiParam;
	bool bTestedReturn;
	bool bTestedReturnMultiParam;
	bool bTestedReturnOutParam;

	/* Increments every time one of its event handlers were called. */
	Int FunctionCounter;

	/* Tests related to the Multicast test */
	MulticastDelegate<Int> MulticastTestBroadcaster;
	Int MulticastTestCounter;
	bool MulticastTestHandlerCalled[4];
	bool MulticastAddFunctionCalled;
	bool MulticastRemoveFunctionCalled;

private:
	const DatatypeUnitTester* DatatypeTester;
	UnitTester::EUnitTestFlags TestFlags;

	/* If true then the Add/Remove function handlers can run additional tests. */
	bool AddFunctionCanFail;
	bool RemoveFunctionCanFail;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Method
	=====================
	*/

public:
	/**
	 * Executes all tests. Returns true if all tests passes.
	 */
	virtual bool RunTest (const DatatypeUnitTester* tester, UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if all callbacks were invoked once.
	 */
	virtual bool RunFunctionTest (UnitTester::EUnitTestFlags testFlags);

	virtual bool RunMulticastTest (UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleVoidFunction ();
	virtual void HandleParamFunction (Int paramValueTest);
	virtual void HandleMultiParamFunction (Int param1, Float param2, DString param3, unsigned int param4);
	virtual Float HandleReturnFunction ();
	virtual Bool HandleReturnMultiParamFunction (Int param1, Object* param2, std::string param3);
	virtual Int HandleReturnOutParamFunction (Int param1, Int& outParam2);

	virtual void HandleMulticastFunctionA (Int incrementAmount);
	virtual void HandleMulticastFunctionB (Int incrementAmount);
	virtual void HandleMulticastFunctionC (Int incrementAmount);
	virtual void HandleMulticastFunctionD (Int incrementAmount);
	virtual void HandleMulticastAddFunction (Int incrementAmount);
	virtual void HandleMulticastRemoveFunction (Int incrementAmount);
};
SD_END

#endif //debug_mode