/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DataBuffer.h
  A class that with an arbitrary vector of characters.  The data is organized in a continuous memory
  block.  It also provides an interface to read and extract characters sequentually to allow for quick
  data transfer.

  There is no type safety in the data buffer.  Objects using this class should access the data buffer responsibly.
=====================================================================
*/

#pragma once

#include "Bool.h"
#include "Core.h"

SD_BEGIN
class CORE_API DataBuffer
{


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* Becomes true if the Data buffer is in little endian format. */
	bool DataBufferIsLittleEndian;

	/* The vector containing the data buffer, itself.  Typically each char within Data could be part of a greater variable.
	For example, a 32-bit int would take up 4 elements in this vector (char is 1 byte).  If an int is pushed to this data buffer,
	then 4 elements in this vector make up that int.  There is no type-safety here.  It's not safe to access the middle of this
	data buffer since it could be accessing a middle of a variable. */
	std::vector<char> Data;

	/* Index of Data the we're reading from.  This should either point to the beginning of a new variable or the end of the buffer. */
	mutable size_t ReadIdx;

	/* Becomes true if an error was detected when trying to read variables from this buffer. */
	mutable bool bReadError;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Creates an empty data buffer.
	 */
	DataBuffer ();

	/**
	 * Initializes an empty data buffer of the specified size.  Sets the read/write index to the beginning.
	 */
	DataBuffer (int numBytes);
	DataBuffer (int numBytes, bool inDataBufferIsLittleEndian);
	virtual ~DataBuffer ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const DataBuffer&) = delete;

	//Returns true if all bytes in the data buffer are equal to the other buffer's bytes. The read index is not relevant.
	bool operator== (const DataBuffer& other) const;
	bool operator!= (const DataBuffer& other) const;

	/**
	 * Quick utility operator when interfacing with DProperties.
	 * Reads data from the data buffer, and places the data into variable.
	 */
	template <class T>
	const DataBuffer& operator>> (T& outVarToWriteTo) const
	{
		if (!HasReadError() && !outVarToWriteTo.Deserialize(*this))
		{
			MarkReadError();
		}

		return *this;
	}

	/**
	 * Quick utility operator when interfacing with DProperties.
	 * Appends the data buffer with the contents of the variable.
	 */
	template <class T>
	DataBuffer& operator<< (const T& varToReadFrom)
	{
		varToReadFrom.Serialize(*this);
		return *this;
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this machine uses little endian format.
	 * Otherwise, it'll return false if it's using big endian format.
	 */
	static bool IsSystemLittleEndian ();

	/**
	 * Swaps the byte order of the given character array.  Used when switching endianness.
	 *
	 * Example:  Converts the given byte data (first line) in reverse order (second line)
	 * 00000001 00000100 00010000 10000000
	 * 10000000 00010000 00000100 00000001
	 *
	 * @param outCharBuffer Raw byte data that'll be in reverse order after function execution.
	 * @param numChars Number of bytes in the character buffer that should be swapped.
	 * Specifying too many bytes will result into an access violation.
	 */
	static void SwapByteOrder (char* outCharBuffer, size_t numBytes);

	/**
	 * Iterates through every byte and reversed their endianness.
	 */
	void SwapByteOrder ();

	inline void EmptyBuffer ()
	{
		Data.clear();
		JumpToBeginning();
	}

	/**
	 * Returns the number of bytes in the data buffer.
	 */
	inline size_t GetNumBytes () const
	{
		return Data.size();
	}

	inline bool IsEmpty () const
	{
		return (Data.size() == 0);
	}

	/**
	 * Returns true if there's enough data in this buffer to read the number of bytes fromt he current read position.
	 */
	bool CanReadBytes (size_t numBytes) const;

	/**
	 * Advances the ReadIdx forward/backwards down the data buffer.
	 * Since there's no type safety in the databuffer, it's the caller's responsibility to advance
	 * the index to a valid position, where a valid position would be either at the end of the buffer, or
	 * at the start of a variable within the buffer.
	 *
	 * @param numBytes Number of bytes to jump ahead.  Specify negative numbers to jump backwards towards
	 * the beginning of the data buffer.
	 */
	void AdvanceIdx (size_t numBytes) const;

	/**
	 * Jumps the ReadIdx to the beginning of the data buffer.
	 */
	inline void JumpToBeginning () const
	{
		bReadError = false;
		ReadIdx = 0;
	}

	/**
	 * Advances the ReadIdx to the end of the data buffer.
	 */
	inline void JumpToEnd () const
	{
		ReadIdx = Data.size();
	}

	/**
	 * Reads in the next few characters from the data buffer.  Advances the ReadIdx by the number of bytes read.
	 *
	 * @param outReadData The character array that contains the a copy of the bytes pulled from data buffer.
	 * @param numBytes Determines the amount of characters to read from the data buffer.
	 */
	void ReadBytes (char* OUT outReadData, size_t numBytes) const;

	/**
	 * Writes the raw byte data at the end of the data buffer.  The data buffer may allocate new space if it needs to.
	 * Difference between AppendBytes and << operator. AppendBytes is intended when there's a need to directly combine data buffers.
	 * The << operator is intended if there's a need to add a data buffer within an existing data buffer.
	 * For example if a Int, Float, DataBuffer, DString was added, that data buffer is able to read that Int, Float, DataBuffer, DString.
	 * AppendBytes would have treat the DString as part of the DataBuffer, and the >> operator wouldn't be able to read from it.
	 *
	 * @param dataToAdd The raw byte data to append to the end of the data buffer.
	 * @param numBytes The number of bytes it should read from the given character buffer.
	 */
	void AppendBytes (const char* dataToAdd, size_t numBytes);
	void AppendBytes (const DataBuffer& source);

	/**
	 * Edits the contents for the current data buffer. It starts overwriting the contents starting from the specified index. If the writing goes out of range,
	 * the rest of the contents are appended to the end of the data buffer.
	 * Be cautious when using this! When overwriting existing content, it's possible to lead to data corruption when writing in the middle of existing data.
	 * For example, editing in the middle of a variable could produce undefined behavior.
	 */
	void EditBytes (size_t writeIdx, const char* dataToWrite, size_t numBytes);
	void EditBytes (size_t writeIdx, const DataBuffer& source);

	/**
	 * Inserts bytes to this data buffer at the specified write index.
	 * Be careful not to insert bytes in the middle of variables. Splitting variables may produce unpredictable behavior.
	 */
	void InsertBytes (size_t insertIdx, const char* dataToWrite, size_t numBytes);
	void InsertBytes (size_t insertIdx, const DataBuffer& source);

	/**
	 * Removes bytes from this data buffer starting from the given idx.
	 * Be careful not to remove bytes in the middle of variables. Splitting variables may produce unpredictable behavior.
	 */
	void RemoveBytes (size_t idx, size_t numBytesToRemove);

	/**
	 * Copies the contents of this data buffer to the destination. The destination's read index jumps to the beginning.
	 * This function does not affect this data buffer's read position.
	 */
	void CopyBufferTo (DataBuffer& outDestination) const;


	/*
	=====================
	  Mutators
	=====================
	*/

	/**
	 * Jumps the read index to a specified value. This function does not do any checks if it's within data bounds.
	 */
	inline void SetReadIdx (size_t newReadIdx) const
	{
		ReadIdx = newReadIdx;
	}

	inline void MarkReadError () const
	{
		bReadError = true;
	}

	inline void ClearReadError () const
	{
		bReadError = false;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns true if the read idx is pointing at the beginning of the data buffer.
	 */
	inline bool IsReaderAtBeginning () const
	{
		return (ReadIdx == 0);
	}

	/**
	 * Returns true if the read idx is pointing at the end of the data buffer.
	 */
	inline bool IsReaderAtEnd () const
	{
		return (ReadIdx >= Data.size());
	}

	inline bool IsDataBufferLittleEndian () const
	{
		return DataBufferIsLittleEndian;
	}

	inline const std::vector<char>& ReadRawData () const
	{
		return Data;
	}

	inline size_t GetReadIdx () const
	{
		return ReadIdx;
	}

	inline bool HasReadError () const
	{
		return bReadError;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Serializes an Int based on the given vector size.
	 * Separated from the template function primarily to avoid circular dependency between DataBuffers and Ints since the template is defined in the header.
	 */
	void SerializeInt (size_t vectorSize);

	/**
	 * Deserializes an Int from this data buffer as an indication of the vector size.
	 * This method returns true if it's able to read an Int from this buffer. If successful, it'll write to the outIntValue param.
	 * Separated from the template function primarily to avoid circular dependency between DataBuffers and Ints since the template is defined in the header.
	 */
	bool DeserializeInt (size_t& outIntValue) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Simple utility to read/write a vector to a databuffer.
	 * This assumes the template argument overrides the >> and << operator.
	 */
	template<class T>
	void SerializeArray (const std::vector<T>& data)
	{
		SerializeInt(data.size());

		for (size_t i = 0; i < data.size(); ++i)
		{
			data.at(i).Serialize(*this);
		}
	}

	template<class T>
	bool DeserializeArray (std::vector<T>& outData) const
	{
		size_t numElements = 0;
		if (!DeserializeInt(OUT numElements))
		{
			JumpToEnd();
			MarkReadError();
			return false;
		}

		T minItem;
		if (numElements < 0 || !CanReadBytes(minItem.GetMinBytes() * numElements))
		{
			//Malformed databuffer. The leading byte claims that the size of this data buffer is larger than the actual buffer.
			JumpToEnd();
			MarkReadError();
			return false;
		}

		outData.resize(numElements);
		for (size_t i = 0; i < outData.size(); ++i)
		{
			T newItem;
			if (!newItem.Deserialize(*this))
			{
				JumpToEnd();
				MarkReadError();
				return false;
			}

			outData[i] = newItem;
		}

		return true;
	}

	template<class T>
	const DataBuffer& operator>> (std::vector<T>& outVarToWriteTo) const
	{
		if (!HasReadError() && !DeserializeArray(OUT outVarToWriteTo))
		{
			MarkReadError();
		}

		return *this;
	}

	template<class T>
	DataBuffer& operator<< (const std::vector<T>& varToReadFrom)
	{
		SerializeArray(varToReadFrom);
		return *this;
	}

	template<>
	const DataBuffer& operator>> <DataBuffer> (DataBuffer& outVarToWriteTo) const
	{
		if (bReadError)
		{
			return *this;
		}

		size_t numBytes;
		if ((*this >> numBytes).HasReadError())
		{
			return *this;
		}

		if (!CanReadBytes(numBytes))
		{
			MarkReadError();
			return *this;
		}

		if (numBytes == 0)
		{
			return *this;
		}

		size_t startIdx = outVarToWriteTo.Data.size();
		outVarToWriteTo.Data.resize(outVarToWriteTo.Data.size() + numBytes);
		for (size_t i = 0; i < numBytes; ++i)
		{
			outVarToWriteTo.Data[startIdx + i] = Data[i + ReadIdx];
		}
		ReadIdx += numBytes;

		return *this;
	}

	template<>
	DataBuffer& operator<< <DataBuffer> (const DataBuffer& varToReadFrom)
	{
		*this << varToReadFrom.GetNumBytes();

		size_t startIdx = Data.size();
		Data.resize(Data.size() + varToReadFrom.GetNumBytes());
		for (size_t i = 0; i < varToReadFrom.GetNumBytes(); ++i)
		{
			Data[startIdx + i] = varToReadFrom.Data[i];
		}

		return *this;
	}

	template<>
	const DataBuffer& operator>> <bool> (bool& outVarToWriteTo) const
	{
		//Bools have the data buffer implementation
		Bool sdBool;
		sdBool.Deserialize(*this);
		outVarToWriteTo = sdBool;

		return *this;
	}

	template<>
	DataBuffer& operator<< <bool> (const bool& varToReadFrom)
	{
		//Bools have the data buffer implementation
		Bool sdBool(varToReadFrom);
		sdBool.Serialize(*this);

		return *this;
	}

	template<>
	const DataBuffer& operator>> <size_t> (size_t& outVarToWriteTo) const
	{
		if (HasReadError())
		{
			return *this;
		}

		const int numBytes = sizeof(size_t);
		if (!CanReadBytes(numBytes))
		{
			MarkReadError();
			return *this;
		}

		char rawData[numBytes];
		ReadBytes(rawData, numBytes);

		//Reverse bytes if needed
		if (IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
		{
			DataBuffer::SwapByteOrder(rawData, numBytes);
		}

		memcpy(&outVarToWriteTo, rawData, numBytes);
		return *this;
	}

	template<>
	DataBuffer& operator<< <size_t> (const size_t& varToReadFrom)
	{
		//Convert size_t to char array
		const int numBytes = sizeof(size_t);
		char charArray[numBytes];
		memcpy(charArray, &varToReadFrom, numBytes);

		//Reverse bytes if needed
		if (IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
		{
			DataBuffer::SwapByteOrder(charArray, numBytes);
		}

		AppendBytes(charArray, numBytes);
		return *this;
	}
};
SD_END