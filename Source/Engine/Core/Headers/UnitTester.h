/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTester.h
  Class containing utility functions unit tests may be using to handle
  error cases and logging.
=====================================================================
*/

#pragma once

#include "Object.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API UnitTester : public Object
{
	DECLARE_CLASS(UnitTester)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	/**
	 * Various flags the user may pass into an iterator to run specific or categorized flags.  Not all values are mutually exclusive and are grouped accordingly.
	 */
	enum EUnitTestFlags
	{
		UTF_None = 0x00000000,

		//Test Categories
		UTF_SmokeTest = 0x00000001, //Very fast test.  Runs basic tests to see if the Engine at least runs.
		UTF_FeatureTest = 0x0000002, //Generally a fast test.  Tests general functionality that are not impediment to the engine.
		UTF_StressTest = 0x00000004, //Slow test that often tests against time/performance, or memory.

		//Termination-based Categories
		UTF_Automatic = 0x00000010, //This test will automatically terminate.
		UTF_Manual = 0x00000020, //This test requires manual input to terminate.

		//Error detection flags
		UTF_CanDetectErrors = 0x00000100, //This test has the ability to detect errors (such as equations and conditions).
		UTF_NeverFails = 0x00000200, //This test cannot detect errors, and requires human attention to detect errors (such as UI appearances).

		UTF_Synchronous = 0x00001000, //This test runs in sequential order, and will not return until the test finishes.
		UTF_Asynchronous = 0x00002000, //This test will terminate long after the function returns (such as timer-based and multi-threaded tests).

		//Verbosity flags
		UTF_Verbose = 0x00010000, //This test will log out things as the test is performed.
		UTF_ErrorsOnly = 0x00020000, //This test will only log things when something wrong was detected.
		UTF_Muted = 0x00040000, //This test will not log anything.  However the objects that are being tested may still issue logs.

		//Error handling
		UTF_CrashOnError = 0x00100000, //The Engine will assert as soon as an error arises.
		UTF_AbortOnError = 0x00200000, //The test will terminate on error (but the engine continues).
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Many tests are divided into sub categories, this string will track which category is currently being tested. */
	mutable DString TestCategory;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Checks the unit test flags to see if the given log statement can be passed to the engine for processing.
	 * @param testFlags Composition of unit test flags that determines the verbosity of unit test logs.
	 * @param msg Formatted log message to be serialized and/or displayed.
	 */
	static void TestLog (EUnitTestFlags testFlags, const DString& msg);

	/**
	 * Runs all tests.  Causes a fatal error if any unit test fails.
	 * Returns true if all tests passed.
	 */
	virtual bool RunTests (EUnitTestFlags testFlags) const = 0;

	/**
	 * Returns true if all prerequisite unit tests were completed.
	 * @Param:  List of all static class references of Unit Tests that passed or skipped their tests.
	 * If one of the required unit testers is not found in parameter, then this function should return false.
	 */
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const;

	/**
	 * If this function returns true, then this unit test will run.  Otherwise the engine assumes everything
	 * this class tests is running correctly (doesn't hinder other dependant unit testers).
	 */
	virtual bool IsTestEnabled (EUnitTestFlags testFlags) const;

	virtual bool ShouldHaveDebugLogs (EUnitTestFlags testFlags) const;

	/**
	 * Establishes which subtest is currently running within the test sequence.
	 */
	virtual void SetTestCategory (EUnitTestFlags testFlags, const DString& newTestCategory) const;

	/**
	 * Notifies the unit test that the current category passed all of its tests, and may clear its reference to the category.
	 */
	virtual void CompleteTestCategory (EUnitTestFlags testFlags) const;

	virtual void BeginTestSequence (EUnitTestFlags testFlags, const DString& testName) const;
	virtual void ExecuteSuccessSequence (EUnitTestFlags testFlags, const DString& testName) const;
	virtual void ExecuteFailSequence (EUnitTestFlags testFlags) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Checks the unit test flags to see if the given log statement can be passed to the engine for processing.
	 * @param testFlags Composition of unit test flags that determines the verbosity of unit test logs.
	 * @param msg Unformatted log message where a %s macro is replaced with one of the trailing templated parameters.
	 * @param params Numerous parameters whose values will be inserted into msg.  Each parameter is expected to have a ToString method implemented.
	 * @tparam Types Types used for the params parameter.  Each type is expected to have a ToString method that returns a DString representing its value.
	 */
	template <class ... Types>
	static void TestLog (EUnitTestFlags testFlags, const DString& msg, Types ... params)
	{
		if ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0)
		{
			UnitTestLog.Log(LogCategory::LL_Debug, msg, params ... );
		}
	}

	template <class ... Types>
	static void UnitTestError_Static (EUnitTestFlags testFlags, const DString& errorMsg, Types ... params)
	{
		DString fullMsg(errorMsg);
		DString::FormatString(OUT fullMsg, params ... );
		if ((testFlags & UTF_Muted) == 0)
		{
			UnitTestLog.Log(LogCategory::LL_Warning, fullMsg);
		}

		if ((testFlags & UTF_CrashOnError) > 0)
		{
			Engine::FindEngine()->FatalError(fullMsg);
		}
	}

	template <class ... Types>
	void UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg, Types ... params) const
	{
		ExecuteFailSequence(testFlags);
		UnitTestError_Static(testFlags, errorMsg, params ... );
	}
};

#pragma region "External Operators"
DEFINE_ENUM_FUNCTIONS(UnitTester::EUnitTestFlags)
#pragma endregion
SD_END

#endif //debug_mode