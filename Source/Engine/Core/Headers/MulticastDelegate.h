/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MulticastDelegate.h

  A class that is responsible for managing and broadcasting a collection of SD Functions.

  It is safe to add or remove handlers from the MulticastDelegate even if it's in the middle of
  broadcasting an event.
=====================================================================
*/

#pragma once

#include "ContainerUtils.h"
#include "SDFunction.h"

SD_BEGIN
template <class ... P /*params*/>
class MulticastDelegate
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The list of functions to execute whenever it broadcasts. */
	std::vector<SDFunction<void, P...>> Handlers;

	/* Becomes true if this object is in the middle of broadcasting.
	The Handlers are locked until the broadcast is finished. */
	bool Locked;

	/* List of Handlers that are pending registration. These are added when something
	tries to register a handler while it was locked. These will automatically subscribe to the
	Handlers vector as soon as it unlocks. */
	std::vector<SDFunction<void, P...>> PendingAddHandlers;

	/* List of Handlers that are pending removal. These are added whne something
	tries to unregister a handler whil it was locked. These will automatically unscribe from
	the Handlers vector as soon as it unlocks. */
	std::vector<SDFunction<void, P...>> PendingRemovalHandlers;

private:
	/* Becomes true if it object should log errors if it's unable to find a callback registered
	in the Handlers vector. */
	bool bLogErrors;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	MulticastDelegate () :
		Locked(false),
		bLogErrors(true)
	{
		//Noop
	}

	virtual ~MulticastDelegate ()
	{
		CHECK_INFO(!IsLocked(), TXT("A MulticastDelegate is destroyed while it's broadcasting."))
		Empty();
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to register the given callback at the end of the handler list.
	 * The function should be unique compared to what's already registered.
	 */
	void RegisterHandler (const SDFunction<void, P...>& newHandler)
	{
		CHECK(newHandler.IsBounded())
		if (newHandler.IsUsingLambda())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot register a lambda to a multicast delegate primarily because the equal comparisons would not work. Try binding the SDFunction to a member function before registering it."));
			return;
		}

		if (IsLocked())
		{
			if (ContainerUtils::RemoveItem(OUT PendingRemovalHandlers, newHandler) != UINT_INDEX_NONE)
			{
				//Event was removed from pending remove list. It's possible that this event already exist in the list. No need to add this event to pending add list if it's already there. Steps to repro: in handle event - remove then add the same event.
				if (IsRegistered(newHandler, true))
				{
					return;
				}
			}

			PendingAddHandlers.push_back(newHandler);
			return;
		}

#if ENABLE_COMPLEX_CHECKING
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			CHECK(Handlers.at(i) != newHandler) //Ensure we're not registering the same callback more than once.
		}
#endif

		Handlers.push_back(newHandler);
	}

	/**
	 * Returns true if the handler is currently registered.
	 * If checkPending is true, then it'll only return true if it's registered by the time it unlocks.
	 */
	bool IsRegistered (const SDFunction<void, P...>& target, bool checkPending) const
	{
		if (target.IsUsingLambda())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot check if a lambda function is registered to a multicast delegate. Registered functions must be bound to a member function."));
			return false;
		}

		bool currentlyRegistered = false;
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			if (Handlers.at(i) == target)
			{
				if (!IsLocked())
				{
					return true;
				}

				currentlyRegistered = true;
				break;
			}
		}

		if (!checkPending)
		{
			return currentlyRegistered;
		}

		if (currentlyRegistered)
		{
			for (size_t i = 0; i < PendingRemovalHandlers.size(); ++i)
			{
				if (PendingRemovalHandlers.at(i) == target)
				{
					return false;
				}
			}
		}
		else
		{
			for (size_t i = 0; i < PendingAddHandlers.size(); ++i)
			{
				if (PendingAddHandlers.at(i) == target)
				{
					return true;
				}
			}
		}

		return currentlyRegistered;
	}

	/**
	 * Searches the handler list to try to remove it.
	 */
	void UnregisterHandler (const SDFunction<void, P...>& target)
	{
		if (!TryUnregisterHandler(target) && bLogErrors)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to remove %s from the handler list since it's not found."), target);
		}
	}

	/**
	 * Removes the handler from the list if it's found. Does nothing if it's already missing from the list.
	 */
	bool TryUnregisterHandler (const SDFunction<void, P...>& target)
	{
		CHECK(target.IsBounded())
		if (IsLocked())
		{
			if (ContainerUtils::RemoveItem(OUT PendingAddHandlers, target) != UINT_INDEX_NONE)
			{
				//Event was removed from pending add list. It's possible that this event already missing from the list. No need to add this event to pending remove list if it's already missing. Steps to repro: in handle event - add then remove the same event.
				if (!IsRegistered(target, true))
				{
					return true;
				}
			}

			PendingRemovalHandlers.push_back(target);
			return true;
		}

		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			if (Handlers.at(i) == target)
			{
				Handlers.erase(Handlers.begin() + i);
				return true;
			}
		}

		return false;
	}

	void Empty ()
	{
		if (IsLocked())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot empty a delegate while it's in use."));
			return;
		}

		ContainerUtils::Empty(OUT Handlers);
	}

	/**
	 * Iterates through each handler to invoke them.
	 */
	void Broadcast (P... params)
	{
		if (IsLocked())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Recursive Broadcast detected! MulticastDelegate::Broadcast is called while it's in the middle of a broadcast."));
			return;
		}

		Locked = true;
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			Handlers.at(i).Execute(params...);
		}
		Locked = false;

		//Disable log messages from calling UnregisterHandlers here due to false positives. This may happen when a callback was registered and unregistered within one broadcast cycle.
		bLogErrors = false;

		//Remove pending handlers before adding in order to reduce iteration time when searching for callbacks to remove.
		for (size_t i = 0; i < PendingRemovalHandlers.size(); ++i)
		{
			UnregisterHandler(PendingRemovalHandlers.at(i));
		}
		ContainerUtils::Empty(PendingRemovalHandlers);
		bLogErrors = true;

		for (size_t i = 0; i < PendingAddHandlers.size(); ++i)
		{
			RegisterHandler(PendingAddHandlers.at(i));
		}
		ContainerUtils::Empty(PendingAddHandlers);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<SDFunction<void, P...>>& ReadHandlers () const
	{
		return Handlers;
	}

	inline bool IsLocked () const
	{
		return Locked;
	}
};
SD_END