/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Stopwatch.h
  A lightweight utility that simply records a timestamp, and returns the elapsed time
  since its construction.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "CoreDatatypes.h"

SD_BEGIN
class CORE_API Stopwatch
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Name of this stopwatch to help the user identify what is this recording. */
	DString Name;

	/* If true, then this stopwatch will log its time whenever it's destroyed. */
	bool AutoLog;

protected:
	/* Timestamp when the timer started.  If negative, then the timer is paused. */
	Float StartTime;

	/* Previously recorded elapsed time from pausing. */
	Float RecordedTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Stopwatch ();
	Stopwatch (const DString& stopwatchName);
	Stopwatch (const DString& stopwatchName, bool inAutoLog);
	Stopwatch (const Stopwatch& cpyObj);
	virtual ~Stopwatch ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Suspends timer without losing progress of elapsed time.
	 */
	virtual void PauseTime ();

	virtual void ResumeTime ();

	/**
	 * Sets the recorded time back to 0, and resets StartTime to the current time.
	 */
	virtual void ResetTime ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Retrieves the total recorded time this stopwatch recorded (in milliseconds).
	 */
	virtual Float GetElapsedTime () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Retrieves the current timestamp from the engine.
	 * This is not a virtual function since it's invoked from the constructor.
	 */
	Float GetSystemTime () const;

	/**
	 * Logic to run when the stopwatch was destroyed.
	 */
	virtual void DestroyStopwatch ();
};
SD_END