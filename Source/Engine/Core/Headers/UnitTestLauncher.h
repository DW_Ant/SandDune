/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  UnitTestLauncher.h
  The Unit Test Launcher iterates and runs all unit tests.
=====================================================================
*/

#pragma once

#include "Object.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE
SD_BEGIN
class CORE_API UnitTestLauncher : public Object
{
	DECLARE_CLASS(UnitTestLauncher)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through all unit tests where all of its test filters aligns with the specified flags.
	 * Returns false if any test fails.
	 */
	static bool RunAllTests (UnitTester::EUnitTestFlags testFlags);
};
SD_END

#endif