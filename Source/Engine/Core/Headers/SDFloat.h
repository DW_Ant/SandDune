/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Float.h
  A class that represents a 32 bit floating point number.  Contains numeric
  utility functions, and supports typecasting to various standard C++ numbers.

  Without the "SD" prefix in the file name, the compiler will generate errors
  such as error C2065: 'FLT_RADIX' : undeclared identifier
  The file name "float.h" is already taken.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Configuration.h"
#include "DProperty.h"

#ifndef FLOAT_TOLERANCE
/* Define how close floats can be to each other to be considered equal. */
#define FLOAT_TOLERANCE 0.000001f
#endif

#ifndef MAX_FLOAT
#define MAX_FLOAT std::numeric_limits<float>::max()
#endif

#ifndef MIN_FLOAT
#define MIN_FLOAT std::numeric_limits<float>::lowest()
#endif

SD_BEGIN
class Int;

class CORE_API Float : public DProperty
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* Determines how the trailing digits should be truncated. */
	enum eTruncateMethod
	{
		TM_RoundDown,
		TM_Round,
		TM_RoundUp
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	float Value;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Float ();
	Float (const float newValue);
	Float (const Float& copyFloat);
	explicit Float (const Int& newValue);
	explicit Float (const DString& text);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const Float& copyFloat);
	virtual void operator= (float otherFloat);

	virtual Float operator++ ();
	virtual Float operator++ (int);
	virtual Float operator- () const;
	virtual Float operator-- ();
	virtual Float operator-- (int);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Float MakeFloat (int value);
	static Float MakeFloat (unsigned int value);
#ifdef PLATFORM_64BIT
	static Float MakeFloat (const int64& value);
	static Float MakeFloat (const uint64& value);
#endif

	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	/**
	 * Returns true if the value of this float is nearly equal to the other float
	 * within the given tolerance level.
	 */
	virtual bool IsCloseTo (Float other, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns an Int from this Float.  This conversion truncates all decimal values.
	 */
	virtual Int ToInt () const;

	/**
	 * Returns the absolute value of this Float.
	 */
	static [[nodiscard]] Float Abs (const Float value);
	virtual void AbsInline ();

	static [[nodiscard]] Float Round (const Float value);
	virtual void RoundInline ();
	virtual void RoundInline (Int numDecimals);

	static [[nodiscard]] Float RoundUp (const Float value);
	virtual void RoundUpInline ();
	virtual void RoundUpInline (Int numDecimals);

	static [[nodiscard]] Float RoundDown (const Float value);
	virtual void RoundDownInline ();
	virtual void RoundDownInline (Int numDecimals);

	/**
	 * Multiplies this float to the given power. For example, if pow = 2, this function will square this float.
	 */
	static [[nodiscard]] Float Pow (Float base, Float exponent);
	virtual void PowInline (Float exponent);

	/**
	 * Converts the value to an float.  This function may cause precision errors if double is large enough.
	 */
	virtual float GetFloat (double target);

	/**
	 * Returns a double from this value
	 */
	virtual double ToDouble () const;

	/**
	 * Returns a DString that displays this float based on the specified parameters.
	 *
	 * @param: minNumDigits Determines how many digits should there be. Adds leading zeros to the
	 * front to fulfill requirement (eg: 00003.14). The decimal point and negative sign does not count.
	 *
	 * @param: numDecimals Determines how many decimal places there should be. Adds trailing zeros if there
	 * aren't enough digits (eg: 1.25000). Truncates decimals if there are too many (eg: 1.2).
	 * This parameter is disabled if negative.
	 *
	 * @param: rounding if the function needs to truncate decimal places, this is the rounding method to use.
	 */
	virtual DString ToFormattedString (Int minNumDigits, Int numDecimals, eTruncateMethod rounding) const;
};

#pragma region "External Operators"
	CORE_API bool operator== (const Float& left, const Float& right);
	CORE_API bool operator== (const Float& left, const float& right);
	CORE_API bool operator== (const float& left, const Float& right);

	CORE_API bool operator!= (const Float& left, const Float& right);
	CORE_API bool operator!= (const Float& left, const float& right);
	CORE_API bool operator!= (const float& left, const Float& right);

	CORE_API bool operator< (const Float& left, const Float& right);
	CORE_API bool operator< (const Float& left, const float& right);
	CORE_API bool operator< (const float& left, const Float& right);
	CORE_API bool operator<= (const Float& left, const Float& right);
	CORE_API bool operator<= (const Float& left, const float& right);
	CORE_API bool operator<= (const float& left, const Float& right);
	CORE_API bool operator> (const Float& left, const Float& right);
	CORE_API bool operator> (const Float& left, const float& right);
	CORE_API bool operator> (const float& left, const Float& right);
	CORE_API bool operator>= (const Float& left, const Float& right);
	CORE_API bool operator>= (const Float& left, const float& right);
	CORE_API bool operator>= (const float& left, const Float& right);

	CORE_API Float operator+ (const Float& left, const Float& right);
	CORE_API Float operator+ (const Float& left, const float& right);
	CORE_API Float operator+ (const float& left, const Float& right);
	CORE_API Float& operator+= (Float& left, const Float& right);
	CORE_API Float& operator+= (Float& left, const float& right);
	CORE_API float& operator+= (float& left, const Float& right);

	CORE_API Float operator- (const Float& left, const Float& right);
	CORE_API Float operator- (const Float& left, const float& right);
	CORE_API Float operator- (const float& left, const Float& right);
	CORE_API Float& operator-= (Float& left, const Float& right);
	CORE_API Float& operator-= (Float& left, const float& right);
	CORE_API float& operator-= (float& left, const Float& right);

	CORE_API Float operator* (const Float& left, const Float& right);
	CORE_API Float operator* (const Float& left, const float& right);
	CORE_API Float operator* (const float& left, const Float& right);
	CORE_API Float& operator*= (Float& left, const Float& right);
	CORE_API Float& operator*= (Float& left, const float& right);
	CORE_API float& operator*= (float& left, const Float& right);

	CORE_API Float operator/ (const Float& left, const Float& right);
	CORE_API Float operator/ (const Float& left, const float& right);
	CORE_API Float operator/ (const float& left, const Float& right);
	CORE_API Float& operator/= (Float& left, const Float& right);
	CORE_API Float& operator/= (Float& left, const float& right);
	CORE_API float& operator/= (float& left, const Float& right);

	CORE_API Float operator% (const Float& left, const Float& right);
	CORE_API Float operator% (const Float& left, const float& right);
	CORE_API Float operator% (const float& left, const Float& right);
	CORE_API Float& operator%= (Float& left, const Float& right);
	CORE_API Float& operator%= (Float& left, const float& right);
	CORE_API float& operator%= (float& left, const Float& right);

#if 0
	CORE_API bool operator== (float a, const Float& b);
	CORE_API bool operator!= (float a, const Float& b);
	CORE_API bool operator< (float a, const Float& b);
	CORE_API bool operator<= (float a, const Float& b);
	CORE_API bool operator> (float a, const Float& b);
	CORE_API bool operator>= (float a, const Float& b);

	CORE_API Float operator+ (float a, const Float& b);
	CORE_API Float operator- (float a, const Float& b);
	CORE_API Float operator* (float a, const Float& b);
	CORE_API Float operator/ (float a, const Float& b);
	CORE_API Float operator% (float a, const Float& b);
#endif
#pragma endregion
SD_END