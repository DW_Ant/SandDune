/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HashedString.h
  A HashedString is a DString that is mapped to a size_t. This allows
  for uniquely identifying items by string without the cost of comparing them
  by strings. Instead it can compare by hash (constant time).

  In addition to that, HashedMaps with equal strings share the same data.

  The hashed strings share the same HashMap across all threads.
=====================================================================
*/

#pragma once

#include "DataBuffer.h"
#include "DProperty.h"
#include "DString.h"
#include "Int.h"

SD_BEGIN
#ifdef DEBUG_MODE
class DatatypeUnitTester;
#endif

class CORE_API HashedString : public DProperty
{


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/* The actual data within the hash map that's shared among all instances of HashedStrings. */
	struct SSharedData
	{
		/* String used to generate the hash. */
		DString String;

		/* Number of different HashedString instances are referencing this instance. If more than one HashedStrings use the same string, they are consolidated in the HashMap.
		This is to prevent duplication and to collapse the HashMap from unused strings. */
		Int ReferenceCounter;
		
		SSharedData (const DString& inString);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all HashedStrings mapped by their IDs. This is available in any thread. */
	static std::unordered_map<size_t, SSharedData> HashMap;

	/* Mutex used to protect the HashMap while it's accessed. */
	static std::mutex HashMapMutex;

	/* The hash generated from the string. This maps to an entry in the HashMap. If it's zero, then it's presume it's not mapped. */
	size_t Hash;

#ifdef DEBUG_MODE
	/* A copy of the string value found in the shared data struct. This is primarily used for debugging (when viewing through the IDE's debugger) since it's easier
	to read this string rather than the string mapped to a hash. Only available in debug builds, and even then this string isn't used anywhere in source code. */
	DString DebugString;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	HashedString ();
	HashedString (const DString& inString);
	HashedString (const HashedString& cpyObj);
	virtual ~HashedString ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const HashedString& cpyObj);

	inline bool operator== (const HashedString& other) const
	{
		return (Hash == other.Hash);
	}

	inline bool operator!= (const HashedString& other) const
	{
		return !(operator==(other));
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetString (const DString& newString);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DString GetString () const;
	const DString& ReadString () const;

	inline size_t GetHash () const
	{
		return Hash;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Registers the given string to the hash map. If it already exists, it'll simply increment its reference counter.
	 */
	virtual void AddHash (const DString& string);

	/**
	 * Finds this HashedString's entry in the map. If it exists, it'll decrement its reference counter.
	 * If this is the last hashed string in that entry (when reference counter is not positive), it'll remove it from the map.
	 */
	virtual void RemoveHash ();

#ifdef DEBUG_MODE
//Used to verify the HashedString during a unit test
friend class DatatypeUnitTester;
#endif
};
SD_END

namespace std
{
	template<>
	struct hash<SD::HashedString>
	{
		//Operator to interface with unordered_maps
		inline size_t operator() (const SD::HashedString& key) const
		{
			return key.GetHash();
		}
	};
}