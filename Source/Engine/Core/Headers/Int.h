/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Int.h
  A class that represents a 64-bit integer (64-bit architectures only).
  For 32-bit architectures, this object represents a 32-bit integer.
  Contains numeric utility functions, and supports typecasting to various standard C++ numbers.

  Warning:  The size of Ints differs based on the platform architecture (32-bit and 64-bit).
  It was decided to have a variable size for this class since 64-bit ints are often used for looping (ie: vector sizes).
  It also makes better use of the 64-bit platform rather than using 32-bit ints.

  If the memory footprint is too large for your application, then it's recommended that you build a 32-bit application instead.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "DProperty.h"
#include "DataBuffer.h"

//Max possible value of 64/32-bit unsigned int
#define UINT64_INDEX_NONE 0xffffffffffffffff
#define UINT32_INDEX_NONE 0xffffffff

#ifdef PLATFORM_64BIT
#define UINT_INDEX_NONE UINT64_INDEX_NONE
#define INDEX_NONE UINT64_INDEX_NONE
#else
#define UINT_INDEX_NONE UINT32_INDEX_NONE
#define INDEX_NONE UINT32_INDEX_NONE
#endif

#define INT_INDEX_NONE -1

//Need to create custom MAXINT define since Microsoft defined MAXINT to their Int in basetsd.h
#ifdef PLATFORM_64BIT
	#define SD_MAXINT LLONG_MAX
	#define SD_MININT LLONG_MIN
#else
	#define SD_MAXINT INT_MAX
	#define SD_MININT INT_MIN
#endif

//Standard ints to use based on architecture
#ifdef PLATFORM_64BIT
#define INT_TYPE int64
#define UINT_TYPE uint64
#else
#define INT_TYPE int32
#define UINT_TYPE uint32
#endif

SD_BEGIN
class Float;
class DString;

class CORE_API Int : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	INT_TYPE Value;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Int ();
#ifdef PLATFORM_32BIT
	Int (const int32 newValue);
#endif
	Int (const Int& copyInt);
	explicit Int (const uint32& newValue);
	explicit Int (const Float& newValue);
	explicit Int (const DString& text);

#ifdef PLATFORM_64BIT
	Int (const int64 newValue);
	explicit Int (const int32 newValue);
	explicit Int (const uint64 newValue);
#endif


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Int& copyInt);
	void operator= (const int32 otherInt);
	void operator= (const uint32 otherInt);

#ifdef PLATFORM_64BIT
	void operator= (const int64 otherInt);
	void operator= (const uint64 otherInt);
#endif

	Int operator++ (); //++iter
	Int operator++ (int); //iter++

	Int operator- () const;
	Int operator-- ();
	Int operator-- (int);

	Int operator~ () const; //Inverts all bits


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Int MakeInt (float value);

	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	/**
	 * Returns a Float from this Int.
	 */
	virtual Float ToFloat () const;

	int32 ToInt32 () const;

	inline bool IsEven () const
	{
		//Only check if the first bit is 0.
		return ((Value&1) == 0);
	}

	inline bool IsOdd () const
	{
		return !IsEven();
	}

	/**
	 * Returns the absolute value of this Int
	 */
	static [[nodiscard]] Int Abs (Int value);
	virtual void AbsInline ();

	/**
	 * Converts the value to an int.  If greater than SD_MAXINT, then the value is set to the max int.
	 */
	static int32 GetInt (uint32 target);
#ifdef PLATFORM_64BIT
	static int64 GetInt (uint64 target);
#endif

	/**
	 * Returns a converted value of this Int to an unsigned int.
	 * If bClamp is true, then negative values are converted to zero.
	 */
	virtual uint32 ToUnsignedInt32 (bool bClamp = true) const;
#ifdef PLATFORM_64BIT
	virtual uint64 ToUnsignedInt64 (bool bClamp = true) const;
#endif
	/**
	 * Returns either uint32 or uint64 based on platform configuration.
	 */
	virtual UINT_TYPE ToUnsignedInt (bool bClamp = true) const;

	virtual Int NumDigits () const;

	/**
	 * Returns a string representation of this number with leading zeroes if the min digits requirement is not met.
	 */
	virtual DString ToFormattedString (const Int minDigits) const;
};

//Define hash and equal functions which can be used for std containers like unordered_maps.
struct IntKeyHash
{
	size_t operator() (const Int& a) const
	{
		return static_cast<size_t>(a.Value);
	}
};

struct IntKeyEqual
{
	bool operator() (const Int& a, const Int& b) const
	{
		return (a.Value == b.Value);
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const Int& left, const Int& right);
	CORE_API bool operator== (const Int& left, const INT_TYPE& right);
	CORE_API bool operator== (const INT_TYPE& left, const Int& right);

	CORE_API bool operator!= (const Int& left, const Int& right);
	CORE_API bool operator!= (const Int& left, const INT_TYPE& right);
	CORE_API bool operator!= (const INT_TYPE& left, const Int& right);

	CORE_API bool operator< (const Int& left, const Int& right);
	CORE_API bool operator< (const Int& left, const INT_TYPE& right);
	CORE_API bool operator< (const INT_TYPE& left, const Int& right);
	CORE_API bool operator<= (const Int& left, const Int& right);
	CORE_API bool operator<= (const Int& left, const INT_TYPE& right);
	CORE_API bool operator<= (const INT_TYPE& left, const Int& right);
	CORE_API bool operator> (const Int& left, const Int& right);
	CORE_API bool operator> (const Int& left, const INT_TYPE& right);
	CORE_API bool operator> (const INT_TYPE& left, const Int& right);
	CORE_API bool operator>= (const Int& left, const Int& right);
	CORE_API bool operator>= (const Int& left, const INT_TYPE& right);
	CORE_API bool operator>= (const INT_TYPE& left, const Int& right);

	CORE_API Int operator>> (const Int& left, const Int& right);
	CORE_API Int operator>> (const Int& left, const INT_TYPE& right);
	CORE_API Int operator>> (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator>>= (Int& left, const Int& right);
	CORE_API Int& operator>>= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator>>= (INT_TYPE& left, const Int& right);
	CORE_API Int operator<< (const Int& left, const Int& right);
	CORE_API Int operator<< (const Int& left, const INT_TYPE& right);
	CORE_API Int operator<< (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator<<= (Int& left, const Int& right);
	CORE_API Int& operator<<= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator<<= (INT_TYPE& left, const Int& right);
	CORE_API Int operator& (const Int& left, const Int& right);
	CORE_API Int operator& (const Int& left, const INT_TYPE& right);
	CORE_API Int operator& (const INT_TYPE& left, const Int& right);
	CORE_API Int operator^ (const Int& left, const Int& right);
	CORE_API Int operator^ (const Int& left, const INT_TYPE& right);
	CORE_API Int operator^ (const INT_TYPE& left, const Int& right);
	CORE_API Int operator| (const Int& left, const Int& right);
	CORE_API Int operator| (const Int& left, const INT_TYPE& right);
	CORE_API Int operator| (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator&= (Int& left, const Int& right);
	CORE_API Int& operator&= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator&= (INT_TYPE& left, const Int& right);
	CORE_API Int& operator^= (Int& left, const Int& right);
	CORE_API Int& operator^= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator^= (INT_TYPE& left, const Int& right);
	CORE_API Int& operator|= (Int& left, const Int& right);
	CORE_API Int& operator|= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator|= (INT_TYPE& left, const Int& right);

	CORE_API Int operator+ (const Int& left, const Int& right);
	CORE_API Int operator+ (const Int& left, const INT_TYPE& right);
	CORE_API Int operator+ (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator+= (Int& left, const Int& right);
	CORE_API Int& operator+= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator+= (INT_TYPE& left, const Int& right);

	CORE_API Int operator- (const Int& left, const Int& right);
	CORE_API Int operator- (const Int& left, const INT_TYPE& right);
	CORE_API Int operator- (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator-= (Int& left, const Int& right);
	CORE_API Int& operator-= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator-= (INT_TYPE& left, const Int& right);

	CORE_API Int operator* (const Int& left, const Int& right);
	CORE_API Int operator* (const Int& left, const INT_TYPE& right);
	CORE_API Int operator* (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator*= (Int& left, const Int& right);
	CORE_API Int& operator*= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator*= (INT_TYPE& left, const Int& right);

	CORE_API Int operator/ (const Int& left, const Int& right);
	CORE_API Int operator/ (const Int& left, const INT_TYPE& right);
	CORE_API Int operator/ (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator/= (Int& left, const Int& right);
	CORE_API Int& operator/= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator/= (INT_TYPE& left, const Int& right);

	CORE_API Int operator% (const Int& left, const Int& right);
	CORE_API Int operator% (const Int& left, const INT_TYPE& right);
	CORE_API Int operator% (const INT_TYPE& left, const Int& right);
	CORE_API Int& operator%= (Int& left, const Int& right);
	CORE_API Int& operator%= (Int& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator%= (INT_TYPE& left, const Int& right);

#pragma endregion
SD_END