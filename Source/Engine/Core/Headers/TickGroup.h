/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickGroup.h
  The TickGroup contains the definition about a collection of Tick components.  This is primarily used
  for handling Tick order, and allows an observer to measure how long it takes to process a group.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"
#include "Int.h"
#include "MulticastDelegate.h"
#include "SDFloat.h"

SD_BEGIN
class TickComponent;

#define TICK_GROUP_SYSTEM "System" //Tick group related to interfacing with OS or Engine related functions
#define TICK_GROUP_PRIORITY_SYSTEM 50000
#define TICK_GROUP_MISC "Misc" //Uncategorized TickGroup
#define TICK_GROUP_PRIORITY_MISC 10000
#ifdef DEBUG_MODE
#define TICK_GROUP_DEBUG "Debug" //TickComponents used for Unit Tests and debugging
#define TICK_GROUP_PRIORITY_DEBUG 1000
#endif

class CORE_API TickGroup
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Name used to identify this group. */
	DString GroupName;

	/* Tick priority value relative to other TickGroups.  Higher priority TickGroups tick sooner before other groups in the Engine's tick cycle. */
	Int TickPriority;

	/* Multiplier that scales the delta time. 0.5 would reduce the speed by half. 2 would run the tick group twice as fast. */
	Float TimeDilation;

	/* If true, then Engines may tick components within this group. */
	bool bTicking;

	/* List of TickComponents' tick callback that belongs in this group.  The Tick order of these components should not depend on each other. */
	MulticastDelegate<Float> OnTick;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TickGroup (const DString& inGroupName, Int inTickPriority);
	virtual ~TickGroup ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the TickComponent to the list of Ticks.
	 */
	virtual void RegisterTick (TickComponent* newTick);

	/**
	 * Removes the TickComponent from the list of Ticks.
	 * If the Tick loop is accessed, then the targetTick will be removed when the it's done accessing it.
	 */
	virtual void RemoveTick (TickComponent* targetTick);

	virtual void SetTicking (bool bNewTicking);

	/**
	 * Ticks all registered TickComponents.
	 */
	virtual void TickRegisteredComponents (Float deltaSec);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTimeDilation (Float newTimeDilation);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetGroupName () const
	{
		return GroupName;
	}

	inline const DString& ReadGroupName () const
	{
		return GroupName;
	}

	inline Int GetTickPriority () const
	{
		return TickPriority;
	}

	inline Float GetTimeDilation () const
	{
		return TimeDilation;
	}

	inline bool IsTicking () const
	{
		return bTicking;
	}

	const MulticastDelegate<Float>& ReadOnTick () const
	{
		return OnTick;
	}
};
SD_END