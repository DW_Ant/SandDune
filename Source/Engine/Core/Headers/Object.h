/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Object.h
  Base object that gets registered to the local engine.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Engine.h"
#include "DPointerInterface.h"
#include "DClass.h"

// Macro to quickly check if a pointer is a valid pointer, and if the object is not going to be removed soon
#define VALID_OBJECT(objectPtr) (objectPtr != nullptr && !objectPtr->GetPendingDelete())

// Same as VALID_OBJECT but this also casts the object to a particular type.
#define VALID_OBJECT_CAST(objectPtr, castTo) (dynamic_cast<castTo>(objectPtr) != nullptr && !objectPtr->GetPendingDelete())

SD_BEGIN
class DPointerBase;

class CORE_API Object : public DPointerInterface
{
	DECLARE_CLASS(Object)


	/*
	=====================
	  Properties
	=====================
	*/

public:
#ifdef DEBUG_MODE
	/* Quick string reference used to quickly identify a particular object instance.  This is not used
	anywhere, and is expected to be set manually whenever a developer wants to track particular object(s). */
	DString DebugName;
#endif

protected:
	/* True if this object is going to be deleted next time the Engine's Garbage Collector runs. */
	bool bPendingDelete;

	unsigned int ObjectHash;

#ifdef DEBUG_MODE
	/* If true then the EngineIntegrity unit test will attempt to spawn and destroy a copy of this Object during its object iteration test.
	The test will check that this object did not leave any objects behind for possible memory leak bugs after destruction.
	This must be set in the class defaults (via: InitProps). */
	bool bRunInObjIterTest;
#endif

private:
	/* The next object in the link list with the same hash table ID.  The CDO is not part of this linked list.
	No need to create a DPointer for this property since the Engine's Garbage Collection will manage this variable. */
	Object* NextObject;

	/* Start of the linked list of DPointers that are referencing this object. */
	mutable DPointerBase* PointerReferences;

#ifdef DEBUG_MODE
	/* Becomes true if this object is marked for destruction.  This is primarily used to detect if there's a subclass
	that doesn't call Super on Destroyed.  It'll crash the engine if it detects that super is not called. */
	bool bDestroyedIsCalled;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Object ();
	Object (const Object& copyConstructor) = delete;
	virtual ~Object ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	Object& operator= (const Object& cpyObj) = delete;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool CanBePointed () const override;
	virtual void SetLeadingDPointer (DPointerBase* newLeadingPointer) const override;
	virtual DPointerBase* GetLeadingDPointer () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes all properties for this object when this object is created.
	 * The Class Default Objects (CDO) also calls this function.  However, the engine is not yet initialized
	 * when the CDOs are initializing.  If Engine access is needed within this function,
	 * refer to DClassAssembler::IsInitialized or Object::IsDefaultObject to filter out CDOs.
	 */
	virtual void InitProps ();

	/**
	 * Notifies the object (and subclasses) to initialize itself.
	 * Class Default Objects does NOT invoke this function.
	 */
	virtual void BeginObject ();

	/**
	 * Displays a string representation for this object.  This is primarily used for logging.
	 */
	virtual DString ToString () const;

	/**
	 * Returns true if this particular object is the default object its DClass references.
	 */
	bool IsDefaultObject () const;

	/**
	 * Removes this object from the game.
	 */
	void Destroy ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline unsigned int GetObjectHash () const
	{
		return ObjectHash;
	}

#ifdef DEBUG_MODE
	inline bool CanRunInObjIterTest () const
	{
		return bRunInObjIterTest;
	}
#endif

	/**
	 * Returns this object's generic name.
	 */
	DString GetName () const;

	/**
	 * Returns this object's descriptive name.  By default, it'll return the unique name, but subclasses
	 * may provide a better name.  For example, a texture object may return the texture name it references.
	 */
	virtual DString GetFriendlyName () const;

	/**
	 * Returns the object instance registered to this object's DClass.
	 */
	const Object* GetDefaultObject () const;

	bool GetPendingDelete () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Calculates the object's hash number for the Engine::ObjectHashTable.  The result should yield in powers of 2.
	 */
	virtual unsigned int CalculateHashID () const;

	/**
	 * Invoked whenever the engine and its components finished initializing.
	 * Only the DClass::DefaultObject of this class gets invoked from the main thread.
	 */
	virtual void PostEngineInitialize () const;

	/**
	 * Invoked via IMPLEMENT_CLASS macro's CreateObject function, this simply
	 * initializes the object's properties, and registers this object to the engine's hash table.
	 */
	virtual void InitializeObject ();

	/**
	 * Returns true if this Object can be destroyed.
	 */
	virtual bool CanBeDestroyed () const;

	/**
	 * Marks this object for deletion, and releases resources.
	 */
	virtual void Destroyed ();

private:
	/**
	 * Finds unique name and adds object to Engine's object linked list.
	 */
	void RegisterObject ();


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Reads in the template's static class, and returns an object of matching class.
	 * If the template class is null or does not match with the target object, then the target object is destroyed.
	 * If the template class is not null then this function will return an instance of an object that matches the template's class.
	 * @param target The object to be either destroyed (if doesn't match with template) or returned (if matched).
	 * @param objTemplate The object with a class reference that the target's class should match.
	 * @param createdNewObject Becomes true if the return value is returning a newly created object instance.
	 * @tparam The class type to cast the result object to.  This may assert if the template's class did not instantiate that type.
	 * @return An object instance that's either equal to target or a reference to the newly instantiated object.
	 */
	template<class T>
	static T* ReplaceTargetWithObjOfMatchingClass (T* target, const T* objTemplate, OUT bool &createdNewObj)
	{
		createdNewObj = false;

		if (target != nullptr &&
			(objTemplate == nullptr || target->StaticClass() != objTemplate->StaticClass()))
		{
			target->Destroy();
			target = nullptr;
		}

		if (target == nullptr && objTemplate != nullptr)
		{
			Object* newObj = objTemplate->CreateObjectOfMatchingClass();
			target = dynamic_cast<T*>(newObj);
			CHECK(target);

			if (target == nullptr)
			{
				delete newObj;
			}
			else
			{
				createdNewObj = true;
			}
		}

		return target;
	}

	template <class T>
	static T* ReplaceTargetWithObjOfMatchingClass (T* target, const T* objTemplate)
	{
		bool bUnused;
		return ReplaceTargetWithObjOfMatchingClass(target, objTemplate, OUT bUnused);
	}

	friend class ObjectIterator;
	friend class Engine;
};
SD_END