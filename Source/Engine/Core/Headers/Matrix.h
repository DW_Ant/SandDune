/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Matrix.h
  A class that represents a 2D array of numbers and contains utility functions
  revolving matrices.  The elements are listed in contiguous memory.

  All matrices are row major.  All parameters should list rows before columns.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DString.h"
#include "Int.h"
#include "SDFloat.h"

SD_BEGIN
class CORE_API Matrix : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Matrix object that represents a invalidated or null matrix. */
	static const Matrix InvalidMatrix;

protected:
	/* Series of numbers within the matrix.  The data is row major where it'll in this order (for a 4x4 matrix)
	0	1	2	3
	4	5	6	7
	8	9	10	11
	12	13	14	15
	*/
	std::vector<Float> Data;

	Int Rows;
	Int Columns;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Matrix ();
	Matrix (Int numRows, Int numColumns);
	Matrix (Int numRows, Int numColumns, const std::vector<Float>& inData);
	Matrix (const Matrix& copyMatrix);
	virtual ~Matrix ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const Matrix& copyMatrix);

	//Unchecked data access to a specific element in the matrix's vector.
	inline Float& operator[] (UINT_TYPE dataIdx)
	{
		return Data[dataIdx];
	}

	inline Float operator[] (UINT_TYPE dataIdx) const
	{
		return Data[dataIdx];
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override; //Displaying the contents of this matrix within a single line.
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the dot product of the vectors assuming the length of both vectors are equal.
	 */
	static Float DotProduct (const std::vector<Float>& left, const std::vector<Float>& right);

#ifdef DEBUG_MODE
	/**
	 * Contrast to the ToString method, this logs out this matrix in a multi-line format.
	 */
	virtual void LogMatrix () const;
#endif

	/**
	 * Returns true if the length of the Data vector fits within the num columns and rows.
	 */
	bool IsValid () const;

	/**
	 * Returns true this matrix is a square matrix (number of rows is equal to the number of columns).
	 */
	bool IsSquare () const;

	/**
	 * Returns true if all elements below the main diagonal is 0.
	 */
	bool IsUpperTriangularMatrix (Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns true if all elements above the main diagonal is 0.
	 */
	bool IsLowerTriangularMatrix (Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns if the matrix is a Unitriangular matrix (must either be upper or lower triangular matrix, and
	 * and all elements within the main diagonal is equal to 1).
	 */
	bool IsUnitriangularMatrix (Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns true if the matrix is an identity matrix (must be both upper and lower triangular matrix and a unitriangular matrix.
	 */
	bool IsUnitMatrix (Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns true if all elements within this matrix are nearly equal to the given matrix.
	 */
	bool IsNearlyEqual (const Matrix& compareTo, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns the identity matrix for this squared matrix.
	 * Returns false if this matrix does not have an identity matrix or is not squared.
	 * This function does not handle nonsquared matrices.
	 */
	bool GetIdentityMatrix (Matrix& outIdentity) const;

	/**
	 * Returns the transpose matrix of this matrix.
	 *
	 * Example:
	 * A =	[1	2]		A' =	[1	3]
	 *		[3	4]				[2	4]
	 *
	 * A =	[1	2	3]	A' =	[1	4]
	 *		[4	5	6]			[2	5]
	 *							[3	6]
	 *
	 * A =	[1	2	3	4 ]		A' =	[1	5	9	13]
	 *		[5	6	7	8 ]				[2	6	10	14]
	 *		[9	10	11	12]				[3	7	11	15]
	 *		[13	14	15	16]				[4	8	12	16]
	 */
	Matrix GetTransposeMatrix () const;


	/**
	 * Returns true if this Matrix is an Orthogonal matrix where multiplying this matrix by its
	 * transpose matrix returns the identity matrix.
	 */
	bool IsOrthogonalMatrix (Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Computes the determinant of this matrix.  Only matrices up to 4x4 are supported.
	 */
	virtual Float CalculateDeterminant () const;

	/**
	 * Computes the adjugate of this matrix.  Only matrices up to 4x4 are supported.
	 */
	virtual bool CalculateAdjugate (Matrix& outAdjugate) const;

	/**
	 * Computes the inverse of this matrix.  Only matrices up to 4x4 are supported.
	 * Returns false if the matrix is not invertible.
	 */
	virtual Matrix CalculateInverse () const;
	virtual bool CalculateInverse (Matrix& outInverse) const;

	/**
	 * Returns a matrix of equal dimensions with all elements being 0.
	 */
	Matrix GetZeroMatrix () const;

	/**
	 * Returns the Matrix of Cofactors of this matrix, where each element of this matrix alternates signage.
	 */
	Matrix GetComatrix () const;

	/**
	 * Returns a sub matrix of this matrix that is composed of all elements excluding the specified row and column.
	 */
	Matrix GetSubMatrix (Int excludedRowIdx, Int excludedColumnIdx) const;

	/**
	 * Returns a matrix that only contains the elements of the specified matrix within the given column and row ranges.
	 *
	 * Example:
	 * [a	b	c	d]
	 * [e	f	g	h]
	 * [i	j	k	l]
	 *
	 * If rowStartIdx = 0, rowEndIdx = 2, columnStartIdx = 1, and columnEndIdx = 2, it'll return a matrix that's equal to:
	 * [b	c]
	 * [f	g]
	 * [j	k]
	 *
	 * Returns an invalid Matrix if the parameters are invalid or out of range.
	 */
	Matrix GetSubMatrix (Int rowStartIdx, Int rowEndIdx, Int columnStartIdx, Int columnEndIdx) const;

	/**
	 * Caculates and returns the matrix of minors of this matrix.  A matrix of minors is a matrix of determinants of sub matrices.
	 * Returns false if this cannot generate a matrix of minors from this matrix.  Supports up to 4x4 matrices.
	 * Warning:  This function is expensive (increases in complexity exponentially based on number of dimensions).
	 */
	bool GetMatrixOfMinors (Matrix& outMatrixOfMinors) const;

	/**
	 * Quick function that'll invoke the lambda function on each data element.
	 */
	void ForEachElement (std::function<void(UINT_TYPE /* dataIndex */)> elementFunction);

	virtual void MatrixAdd (const Matrix& addBy);
	virtual void MatrixSubtract (const Matrix& subtractBy);

	/**
	 * Multiplies this matrix with the parameter.
	 */
	virtual void MatrixMultiply (Float multiplier);
	virtual void MatrixMultiply (const Matrix& multiplier);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	Float& At (UINT_TYPE dataIdx);

	/**
	 * Replaces the matrix data with the given contents.
	 * The new data size must be equal to the current data size.
	 * Returns true if the data was applied.
	 */
	virtual bool SetData (const std::vector<Float>& newData);

	/**
	 * Resizes the matrix data by the specified dimensions, and fills that data
	 * with the specified contents.  The size of the new content must completely fill
	 * the new dimensions of the matrix.  Returns true if changes were applied.
	 */
	virtual bool SetData (const std::vector<Float>& newData, Int newNumRows, Int newNumColumns);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Int GetNumRows () const;
	virtual Int GetNumColumns () const;
	virtual std::vector<Float> GetRow (Int rowIdx) const;
	virtual void GetRow (Int rowIdx, std::vector<Float>& outRowData) const;
	virtual std::vector<Float> GetColumn (Int columnIdx) const;
	virtual void GetColumn (Int columnIdx, std::vector<Float>& outColumnData) const;
	virtual Float GetDataElement (Int rowIdx, Int columnIdx) const;
	virtual Int GetDataIndex (Int rowIdx, Int columnIdx) const;

	inline UINT_TYPE GetNumElements () const
	{
		return Data.size();
	}

	inline const std::vector<Float>& ReadData () const
	{
		return Data;
	}

	Float At (UINT_TYPE dataIdx) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual Float CalcDeterminant2x2 () const;
	virtual Float CalcDeterminant3x3 () const;
	virtual Float CalcDeterminant4x4 () const;

	virtual void CalcAdjugate2x2 (Matrix& outAdjugate) const;
	virtual void CalcAdjugate3x3 (Matrix& outAdjugate) const;
	virtual void CalcAdjugate4x4 (Matrix& outAdjugate) const;

	virtual bool CalcInverse2x2 (Matrix& outInverse) const;
	virtual bool CalcInverse3x3 (Matrix& outInverse) const;
	virtual bool CalcInverse4x4 (Matrix& outInverse) const;
};

#pragma region "External Operators"
	CORE_API bool operator== (const Matrix& left, const Matrix& right);
	CORE_API bool operator!= (const Matrix& left, const Matrix& right);

	CORE_API Matrix operator+ (const Matrix& left, const Matrix& right);
	CORE_API Matrix operator+= (Matrix& left, const Matrix& right);
	CORE_API Matrix operator- (const Matrix& left, const Matrix& right);
	CORE_API Matrix operator-= (Matrix& left, const Matrix& right);

	CORE_API Matrix operator* (const Matrix& left, Float right);
	CORE_API Matrix operator* (Float left, const Matrix& right);
	CORE_API Matrix operator* (const Matrix& left, const Matrix& right);
	CORE_API Matrix& operator*= (Matrix& left, Float right);
	CORE_API Matrix& operator*= (Matrix& left, const Matrix& right);

	CORE_API Matrix operator/ (const Matrix& left, Float right);
	CORE_API Matrix operator/ (const Matrix& left, const Matrix& right);
	CORE_API Matrix& operator/= (Matrix& left, Float right);
	CORE_API Matrix& operator/= (Matrix& left, const Matrix& right);
#pragma endregion
SD_END