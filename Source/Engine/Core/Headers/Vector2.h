/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Vector2.h
  Self-contained 2D vector class with some utilities.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "SDFloat.h"
#include "Utils.h"

SD_BEGIN
class Vector3;

class CORE_API Vector2 : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Float X;
	Float Y;

	/* A 2D vector that has all of its components equal to 0. */
	static const Vector2 ZERO_VECTOR;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Vector2 ();

	Vector2 (const float x, const float y);

	Vector2 (const Float x, const Float y);

	Vector2 (const Vector2& copyVector);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Vector2& copyVector);
	Vector2 operator- () const;
	Float& operator[] (UINT_TYPE axisIdx);
	const Float& operator[] (UINT_TYPE axisIdx) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Vector2 SFMLtoSD (const sf::Vector2f sfVector);
	static sf::Vector2f SDtoSFML (const Vector2 sdVector);

	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	bool IsNearlyEqual (const Vector2& compareTo, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns the magnitude of this vector.
	 */
	Float VSize () const;

	/**
	 * Same as VSize except without the sqrt.  This is typically used to quickly compare relative distances
	 * without the performance hit of square rooting them.
	 */
	Float CalcDistSquared () const;

	/**
	 * Returns the distance between these two points.
	 */
	static Float CalcDistBetween (const Vector2& ptA, const Vector2& ptB);

	/**
	 * Returns the squared distance between these two points.
	 */
	static Float CalcSquaredDistBetween (const Vector2& ptA, const Vector2& ptB);

	/**
	 * Adjusts this vector's size so the length of the vector is equal to the specified length.
	 */
	void SetLengthTo (Float targetLength);

	Vector2 static Min (Vector2 input, const Vector2& min);
	void MinInline (const Vector2& min);
	Vector2 static Max (Vector2 input, const Vector2& max);
	void MaxInline (const Vector2& max);

	/**
	 * Clamps both axis to be within the min/max range. Both axis acts independently from each other.
	 */
	Vector2 static Clamp (Vector2 input, const Vector2& min, const Vector2& max);
	void ClampInline (const Vector2& min, const Vector2& max);

	Float Dot (const Vector2& otherVector) const;

	/**
	 * Returns a reflection vector where the angles between this vector and the normal is equal to the angle between the normal and the resulting vector.
	 * The normal vector (by its name suggestion) is expected to have its magnitude equal to 1.
	 */
	Vector2 CalcReflection (const Vector2& normal) const;

	/**
	 * Adjusts this vector's components so that its magnitude is 1.
	 */
	void NormalizeInline ();
	static Vector2 Normalize (Vector2 vect);

	/**
	 * Returns true of all axis of this vector are the equal.
	 */
	bool IsUniformed () const;

	/**
	 * Returns true if all axis are 0.
	 */
	bool IsEmpty () const;

	/**
	 * Returns a Vector3 from this vector's components.  Z axis is 0.
	 */
	Vector3 ToVector3 () const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Executes a linear interpolation between minBounds and maxBounds based on the given ratio for all axis.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam T The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and Floats) are supported.
	 */
	template <class T>
	static Vector2 Lerp(T ratio, const Vector2& minBounds, const Vector2& maxBounds)
	{
		Vector2 result;

		result.X = Utils::Lerp(ratio, minBounds.X, maxBounds.X);
		result.Y = Utils::Lerp(ratio, minBounds.Y, maxBounds.Y);

		return result;
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const Vector2& left, const Vector2& right);
	CORE_API bool operator!= (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator+ (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator+ (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator+ (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator+= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator+= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator+= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator- (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator- (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator- (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator-= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator-= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator-= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator* (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator* (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator* (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator*= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator*= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator*= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator/ (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator/ (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator/ (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator/= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator/= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator/= (sf::Vector2f& left, const Vector2& right);

	template<class T>
	Vector2 operator* (const Vector2& left, const T& right)
	{
		return Vector2(left.X * right, left.Y * right);
	}

	template<class T>
	Vector2 operator* (const T& left, const Vector2& right)
	{
		return Vector2(left * right.X, left * right.Y);
	}

	template<class T>
	Vector2& operator*= (Vector2& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;

		return left;
	}

	template<class T>
	Vector2 operator/ (const Vector2& left, const T& right)
	{
		if (right != 0)
		{
			return Vector2(left.X / right, left.Y / right);
		}

		return left;
	}

	template<class T>
	Vector2& operator/= (Vector2& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
		}

		return left;
	}
#pragma endregion
SD_END