/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Vector3.h
  Self-contained 3D vector class with some utilities.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "SDFloat.h"
#include "Utils.h"

SD_BEGIN
class Vector2;

class CORE_API Vector3 : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Float X;
	Float Y;
	Float Z;

	/* A 3D vector that has all of its components equal to 0. */
	static const Vector3 ZERO_VECTOR;

	static const Vector3 Right;
	static const Vector3 Up;
	static const Vector3 Forward;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Vector3 ();

	Vector3 (const float x, const float y, const float z);

	Vector3 (const Float x, const Float y, const Float z);

	Vector3 (const Vector3& copyVector);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Vector3& copyVector);
	Vector3 operator- () const;
	Float& operator[] (UINT_TYPE axisIdx);
	const Float& operator[] (UINT_TYPE axisIdx) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Vector3 SFMLtoSD (const sf::Vector3f sfVector);
	static sf::Vector3f SDtoSFML (const Vector3 sdVector);

	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	bool IsNearlyEqual (const Vector3& compareTo, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns the length of this vector.
	 */
	Float VSize (bool bIncludeZ = true) const;

	/**
	 * Same as VSize except without the sqrt.  This is typically used to quickly compare relative distances
	 * without the performance hit of square rooting them.
	 */
	Float CalcDistSquared (bool bIncludeZ = true) const;

	/**
	 * Adjusts this vector's size so the length of the vector is equal to the specified length.
	 */
	void SetLengthTo (Float targetLength);

	Vector3 static Min (Vector3 input, const Vector3& min);
	void MinInline (const Vector3& min);
	Vector3 static Max (Vector3 input, const Vector3& max);
	void MaxInline (const Vector3& max);

	/**
	 * Clamps each axis to be within the min/max range. Each axis acts independently from others.
	 */
	Vector3 static Clamp (Vector3 input, const Vector3& min, const Vector3& max);
	void ClampInline (const Vector3& min, const Vector3& max);

	Float Dot (const Vector3& otherVector) const;

	/**
	 * Gets the orthogonal vector to this vector and the given vector.
	 */
	Vector3 CrossProduct (const Vector3& otherVector) const;

	/**
	 * Returns a reflection vector where the angles between this vector and the normal is equal to the angle between the normal and the resulting vector.
	 * The normal vector (by its name suggestion) is expected to have its magnitude equal to 1.
	 */
	Vector3 CalcReflection (const Vector3& normal) const;

	/**
	 * Adjusts this vector's components so that its magnitude is 1.
	 */
	void NormalizeInline ();
	static Vector3 Normalize (Vector3 vect);

	/**
	 * Returns true of all axis of this vector are the equal.
	 */
	bool IsUniformed () const;

	/**
	 * Returns true if all axis are 0.
	 */
	bool IsEmpty () const;

	/**
	 * Returns a Vector2 from this vector's components.
	 */
	Vector2 ToVector2 () const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Executes a linear interpolation between minBounds and maxBounds based on the given ratio for all axis.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam T The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and Floats) are supported.
	 */
	template <class T>
	static Vector3 Lerp(T ratio, const Vector3& minBounds, const Vector3& maxBounds)
	{
		Vector3 result;

		result.X = Utils::Lerp(ratio, minBounds.X, maxBounds.X);
		result.Y = Utils::Lerp(ratio, minBounds.Y, maxBounds.Y);
		result.Z = Utils::Lerp(ratio, minBounds.Z, maxBounds.Z);

		return result;
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const Vector3& left, const Vector3& right);
	CORE_API bool operator!= (const Vector3& left, const Vector3& right);
	CORE_API Vector3 operator+ (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator+= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator- (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator-= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator* (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator*= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator/ (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator/= (Vector3& left, const Vector3& right);

	template<class T>
	Vector3 operator* (const Vector3& left, const T& right)
	{
		return Vector3(left.X * right, left.Y * right, left.Z * right);
	}

	template<class T>
	Vector3 operator* (const T& left, const Vector3& right)
	{
		return Vector3(left * right.X, left * right.Y, left * right.Z);
	}

	template<class T>
	Vector3 operator*= (Vector3& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;
		left.Z *= right;

		return left;
	}

	template<class T>
	Vector3 operator/ (const Vector3& left, const T& right)
	{
		if (right != 0)
		{
			return Vector3(left.X / right, left.Y / right, left.Z / right);
		}

		return left;
	}

	template<class T>
	Vector3& operator/= (Vector3& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
			left.Z /= right;
		}

		return left;
	}
#pragma endregion
SD_END