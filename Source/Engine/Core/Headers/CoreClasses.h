/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreClasses.h
  Contains all header includes for the Core module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the CoreClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#ifdef WITH_CORE
#include "BaseUtils.h"
#include "Bool.h"
#include "ClassIterator.h"
#include "ComponentIterator.h"
#include "Configuration.h"
#include "ContainerUtils.h"
#include "CopiableObjectInterface.h"
#include "Core.h"
#include "CoreDatatypes.h"
#include "CoreMacros.h"
#include "CoreUtilUnitTester.h"
#include "DataBuffer.h"
#include "DatatypeUnitTester.h"
#include "DClass.h"
#include "DClassAssembler.h"
#include "Definitions.h"
#include "DestroyNotifyComponent.h"
#include "DPointer.h"
#include "DPointerBase.h"
#include "DPointerInterface.h"
#include "DPointerTester.h"
#include "DProperty.h"
#include "DString.h"
#include "Engine.h"
#include "EngineComponent.h"
#include "EngineIntegrityTester.h"
#include "EngineIntegrityUnitTester.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "HashedString.h"
#include "Int.h"
#include "LifeSpanComponent.h"
#include "LogCategory.h"
#include "Matrix.h"
#include "MulticastDelegate.h"
#include "Object.h"
#include "ObjectIterator.h"
#include "PlatformMac.h"
#include "PlatformWindows.h"
#include "Range.h"
#include "Rectangle.h"
#include "ResourcePool.h"
#include "ResourceTag.h"
#include "ResourcePoolMacros.h"
#include "ResourceTree.h"
#include "Rotator.h"
#include "SDFloat.h"
#include "SDFunction.h"
#include "SDFunctionTester.h"
#include "SfmlOutputStream.h"
#include "Stopwatch.h"
#include "StringIterator.h"
#include "TickComponent.h"
#include "TickGroup.h"
#include "TickTester.h"
#include "TransformMatrix.h"
#include "UnitTester.h"
#include "UnitTestLauncher.h"
#include "Utils.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Version.h"

#endif
