/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LifeSpanComponent.h
  Component responsible for timing out its owning entity.
=====================================================================
*/

#pragma once

#include "EntityComponent.h"

SD_BEGIN
class CORE_API LifeSpanComponent : public EntityComponent
{
	DECLARE_CLASS(LifeSpanComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* When this value is no longer positive, then this component will destroy the owning entity. */
	Float LifeSpan;

	/* If true, LifeSpan will continuously drain from Tick.  Setting this false will pause the timer.*/
	bool bAging;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Destroys the owning entity because the LifeSpan expired.
	 */
	virtual void Expire ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END