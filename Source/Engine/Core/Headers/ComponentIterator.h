/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ComponentIterator.h
  The ComponentIterator will iterate through the components of the specified entity.

  For recursive iterators (where the iterator will find EntityComponents within EntityComponents), the
  order of components iterates through the sub components before iterating to the next sibling component.

  Entity
	[0] ComponentA
		[1] SubComponentA
			[2] SubSubComponentA
			[3] SubSubComponentB
		[4] SubComponentB
		[5] SubComponentC
			[6] SubSubComponentA
	[7] ComponentB
		[8] SubComponentA
		[9] SubComponentB

  Warning:  Don't change the owning Entity's component list while an iterator is
  cycling for that may cause other components in next cycle to be skipped.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Int.h"

SD_BEGIN
class Entity;
class EntityComponent;

class CORE_API ComponentIterator
{


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SComponentIndex
	{
	public:
		/* The Entity that owns the components this data struct is iterating through. */
		const Entity* TargetEntity;

		/* Component index of TargetEntity's Component List. */
		UINT_TYPE ComponentIdx;

		SComponentIndex (const Entity* inTargetEntity)
		{
			TargetEntity = inTargetEntity;
			ComponentIdx = 0;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this iterator will find components within components.  Otherwise, the iterator will only
	jump from one sibling component to the next sibling. */
	bool bRecursive;

protected:
	/* Stack of Entities this object is iterating through.  It'll always iterate the last Entity in the list, and
	when it found that Entity's last component, it'll pop it from the list.  For non recursive iterators, this
	vector size is always 1, and it'll only iterate through sibling components. */
	std::vector<SComponentIndex> ComponentChain;

	/* Becomes true if the component iterator should skip over any sub components (only for the current selected component). */
	bool bSkipCurSubComps;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ComponentIterator ();
	ComponentIterator (const Entity* inBaseEntity, bool bInRecursive);
	virtual ~ComponentIterator ();



	/*
	=====================
	  Operators
	=====================
	*/

public:
	inline operator bool () const
	{
		return (GetSelectedComponent() != nullptr);
	}

	inline bool operator! () const
	{
		return (GetSelectedComponent() == nullptr);
	}

	void operator++ (); //++iter
	void operator++ (int); //iter++


	/*
	=====================
	  Methods
	=====================
	*/

public:
	void SetBaseEntity (const Entity* newBaseEntity);

	/**
	 * Instructs this iterator to skip all sub components attached to the current selected component.
	 * Calling this function does NOT iterate or change the current selected component.
	 * This will apply the next time the increment operators are called.
	 */
	void SkipSubComponents ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the root Entity this iterator began with.
	 */
	const Entity* GetBaseEntity () const;
	EntityComponent* GetSelectedComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the index to point to the next child component.  If there aren't any more children, then
	 * the index will point to the next sibling component (if any).
	 */
	void FindNextComponent ();
};
SD_END