/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointer.h
  A property that maintains a pointer.  The class protects the pointers from dangling.
  Should an object be destroyed, all other pointers pointing to that object will be set to nullptr.

  This is slightly different from Shared Pointers.  In Sand Dune, any entity that has reference to another
  Entity have the ability to destroy the referenced entity regardless if there are other pointers referencing it.

  Here are the advantages of the DPointer system:
  * No sense of pointer ownership.  Any object that has a pointer to an object can flag that object to be destroyed.
  You don't have to wait for all pointers to stop referencing the object to destroy the object.

  * Protection against dangling pointers.  Before an object is destroyed, it can cycle through the pointer linked list,
  and set all of the pointers referencing that object to null.  This is the primary reason why this was implemented.

  * Makes it easier to debug when you need to identify which objects are depending/referencing other objects.

  --------------------------------------------------
  Here are the disadvantages of the DPointer system:
  * The object the DPointers references must implement the DPointerInterface.

  * Compared to the shared pointer system, the DPointers require more space.  There's a double linked list where each
  DPointer references the next and previous DPointer.  In addition to that, each object will have a reference to the
  first DPointer in the linked list of pointers referencing that object.

  * Compared to the shared pointer system, the DPointers are significantly slower.  Every time, the pointers are reassigned,
  the pointers will need to unregister themselves from their current linked list (rebinds 3-5 pointers), and registers
  the pointer to the front of the new linked list (rebinds 2-4 pointers).
=====================================================================
*/

#pragma once

#include "DPointerBase.h"

SD_BEGIN
template <class T>
class DPointer : public DPointerBase
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The raw pointer to the object.  The object that this pointer references must implement the DPointerInterface. */
	T* Pointer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DPointer () : DPointerBase()
	{
		Pointer = nullptr;
	}

	DPointer (T* other) : DPointerBase()
	{
		Pointer = other;
		RegisterDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));
	}

	DPointer (const DPointer& other) : DPointerBase(other)
	{
		Pointer = other.Pointer;
		RegisterDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));
	}

	virtual ~DPointer ()
	{
		ClearPointer();
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (T* otherPointer)
	{
		//Remove pointer from old referenced object list
		RemoveDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));

		Pointer = otherPointer;

		//Register point to new referened object list
		RegisterDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));
	}

	void operator= (const DPointer<T>& otherPointer)
	{
		//Remove pointer from old referenced object list
		RemoveDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));

		Pointer = otherPointer.Pointer;

		//Register point to new referened object list
		RegisterDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));
	}

	inline T& operator* () const
	{
		return *Pointer;
	}

	inline T* operator-> () const
	{
		return Pointer;
	}

	virtual operator bool () const override
	{
		return (Pointer != nullptr);
	}

	virtual bool operator! () const override
	{
		return (Pointer == nullptr);
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ClearPointer () override
	{
		if (Pointer != nullptr)
		{
			/**
			This accomplishes the following things...
			 * Removes DPointer reference from Object's internal linked list, while preserving integrity of object's linked list.
			 * Sets this pointer's NextPointer and PreviousPointer to nullptr.
			 */
			RemoveDPointer(reinterpret_cast<const DPointerInterface*>(Pointer));
			Pointer = nullptr;
		}
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a copy of the pointer.  The returned pointer is not managed.
	 */
	inline T* Get () const
	{
		return Pointer;
	}

	/**
	 * Returns true if this pointer is not pointing to nullptr.
	 */
	inline bool IsValid () const
	{
		return (Pointer != nullptr);
	}

	/**
	 * Returns true if this pointer is pointing to nullptr.
	 */
	inline bool IsNullptr () const
	{
		return (Pointer == nullptr);
	}
};

#pragma region "External Operators"
template <class T, class U>
bool operator== (const DPointer<T>& left, const DPointer<U>& right)
{
	return (left.Get() == right.Get());
}

template <class T>
bool operator== (const DPointer<T>& left, const void* right)
{
	return (left.Get() == right);
}

template <class T>
bool operator== (const void* left, const DPointer<T>& right)
{
	return (left == right.Get());
}

template <class T, class U>
bool operator!= (const DPointer<T>& left, const DPointer<U>& right)
{
	return (left.Get() != right.Get());
}

template <class T>
bool operator!= (const DPointer<T>& left, const void* right)
{
	return (left.Get() != right);
}

template <class T>
bool operator!= (const void* left, const DPointer<T>& right)
{
	return (left != right.Get());
}
#pragma endregion
SD_END