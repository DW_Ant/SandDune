/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Rectangle.h
  A datatype that represents a rectangular region.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "DProperty.h"
#include "SDFloat.h"
#include "Vector2.h"

SD_BEGIN
class CORE_API Rectangle : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Float Width;
	Float Height;
	Vector2 Center;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rectangle ();
	Rectangle (Float inWidth, Float inHeight, const Vector2& inCenter);
	Rectangle (const Rectangle& copyRectangle);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Rectangle& copyRectangle);
	bool operator== (const Rectangle& otherRect) const;
	bool operator!= (const Rectangle& otherRect) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	inline Float GetArea () const
	{
		return Width * Height;
	}

	inline Float GetPerimeter () const
	{
		return (Width * 2.f) + (Height * 2.f);
	}

	/**
	 * Returns true if this rectangle is a square.
	 */
	inline bool IsSquare () const
	{
		return (Width == Height);
	}

	/**
	 * Returns true if all borders of this rectangle is zero.
	 */
	inline bool IsEmpty () const
	{
		return (Width == 0.f && Height == 0.f);
	}

	/**
	 * Returns the horizontal border that is numerically greater than Center's Y component regardless of coordinate space.
	 * This is the upper border for coordinates where positive Y is up.
	 */
	inline Float GetGreaterHBorder () const
	{
		return Center.Y + (Height * 0.5f);
	}

	/**
	 * Returns the vertical border that is numerically greater than Center's X component regardless of coordinate space.
	 * This is the right border for coordinates where positive X is right.
	 */
	inline Float GetGreaterVBorder () const
	{
		return Center.X + (Width * 0.5f);
	}

	/**
	 * Returns the horizontal border that is numerically less than Center's Y component regardless of coordinate space.
	 * This is the bottom border for coordinates where negative Y is down.
	 */
	inline Float GetLesserHBorder () const
	{
		return Center.Y - (Height * 0.5f);
	}

	/**
	 * Returns the vertical border that is numerically less than Center's X component regardless of coordinate space.
	 * This is the left border for coordinates where negative X is left.
	 */
	inline Float GetLesserVBorder () const
	{
		return Center.X - (Width * 0.5f);
	}

	/**
	 * Calculates the overlapping rectangle.
	 * Returns an empty rectangle if the two rectangles do not overlap.
	 */
	Rectangle GetOverlappingRectangle (const Rectangle& otherRect) const;

	/**
	 * Returns true if this rectangle overlaps with other rectangle.
	 */
	bool Overlaps (const Rectangle& otherRect) const;

	/**
	 * Returns true if the given point is inside this rectangle.
	 */
	bool EncompassesPoint (const Vector2& targetPoint) const;
};

#pragma region "External Operators"
	CORE_API Rectangle operator* (const Rectangle& left, Float right);
	CORE_API Rectangle operator* (Float left, const Rectangle& right);
	CORE_API Rectangle operator* (const Rectangle& left, const Rectangle& right);
	CORE_API Rectangle& operator*= (Rectangle& left, Float right);
	CORE_API Rectangle& operator*= (Rectangle& left, const Rectangle& right);

	CORE_API Rectangle operator/ (const Rectangle& left, Float right);
	CORE_API Rectangle operator/ (const Rectangle& left, const Rectangle& right);
	CORE_API Rectangle& operator/= (Rectangle& left, Float right);
	CORE_API Rectangle& operator/= (Rectangle& left, const Rectangle& right);
#pragma endregion
SD_END