/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EntityComponent.h
  EntityComponent is the base class of all Entity Components.
  EntityComponents extend its owning entity's functionality since the component
  may influence how the entity behaves.
=====================================================================
*/

#pragma once

#include "Entity.h"
#include "MulticastDelegate.h"

SD_BEGIN
class CORE_API EntityComponent : public Entity
{
	DECLARE_CLASS(EntityComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Broadcasted whenever this component's Owner property changed. This is called after the change is applied. */
	MulticastDelegate<EntityComponent* /*componentThatChangedOwners*/, Entity* /*prevOwner*/> OnOwnerChanged;

protected:
	/* Entity this component is attached to. */
	DPointer<Entity> Owner;

	/* Reference counter to the number of Owning Entities that are invisible.  0 means
	no Entities in the ownership chain are invisible. */
	Int NumInvisibleOwners;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void AddComponentModifier (EntityComponent* newComponent) override;
	virtual bool RemoveComponentModifier (EntityComponent* oldComponent) override;
	virtual bool IsVisible () const override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Retrieves the root entity that this component influences.
	 * Climbs up the attached component chain until it finds a non-component entity.
	 */
	virtual Entity* GetRootEntity () const;

	/**
	 * Returns true if this component is allowed to be attached to the specified entity.
	 */
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const;

	/**
	 * Notifies the owning entity to RemoveComponent(this)
	 */
	void DetachSelfFromOwner ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the immediate owner of this component.
	 */
	virtual Entity* GetOwner () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Notifies this component to attach itself to the specified entity.
	 * Refer to Entity::AddComponent to call this function.
	 */
	virtual void AttachTo (Entity* newOwner);

	/**
	 * Method invoked whenever this component is detached from owning entity.
	 * This function is never called if the Owning Entity is destroyed for a performance gain.
	 * If the Owning Entity is destroyed, all sub components are also automatically destroyed, but it also doesn't
	 * need to recompute its visibility counters and hash values.
	 * See Entity::Destroyed for more information.
	 */
	virtual void ComponentDetached ();

	friend class Entity;
};
SD_END