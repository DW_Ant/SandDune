/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Engine.h
  The Engine is an object responsible for managing instanced Objects.

  The Engine handles the following:
  The Object table, updating objects (Tick, elapsed time, engine component hooks), and cleaning up.

  Functionality of the Engine could be extended using an EngineComponent, where
  module-specific engine implementations are handled throughout various events.

  There can be at most one Engine per thread.  Engines that are not on the main thread
  cannot terminate the application.  Although the sub engines can terminate themselves,
  it's the engine on the main thread that has the ability to terminate the application.
  Should the main engine terminate, all sub engines will terminate.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "Core.h"
#include "DString.h"
#include "LogCategory.h"
#include "SDFloat.h"
#include "SDFunction.h"

SD_BEGIN
class EngineComponent;
class DClass;
class Object;
class TickGroup;

class CORE_API Engine
{


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/**
	 * Collection of properties related to a registered object hash identifier.
	 */
	struct SHashTableMeta
	{
		unsigned int HashNumber;

		/* Brief descriptive name for the group of objects using this hash identifier. */
		DString FriendlyName;

		/* First object in linked list with matching HashNumber is referenced here. */
		Object* LeadingObject;

		SHashTableMeta ()
		{
			LeadingObject = nullptr;
			HashNumber = 0;
			FriendlyName = TXT("Unknown Hash Meta");
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const size_t MAIN_ENGINE_IDX;

	/* Set the DebugEngine at the last spot in EngineInstance since Engine::FindEngine searches from start to end.
	For a debugging, performance isn't a priority so the debug engine should have the biggest hit when invoking FindEngine. */
	static const size_t DEBUG_ENGINE_IDX;

protected:
	/* List of Engines that are managing the Objects and are cycling through their Tick loops.  There's at most one Engine per thread.
	The first Engine in this vector is the Main Engine.  The Main Engine is the Engine that resides in main thread. */
	static Engine* EngineInstances[8];

	/* List of all engine components that's influencing the engine. */
	std::vector<EngineComponent*> EngineComponents;

	/* List of engine components that are registered to the Tick cycle. */
	std::vector<EngineComponent*> TickingEngineComponents;

	/* ID that matches the thread's ID this engine resides in. */
	std::thread::id ThreadID;

	/* Index this Engine resides in EngineInstances array. */
	size_t EngineIndex;

	/* String representation of the user specified parameters passed into the executable. */
	DString CommandLineArgs;

	/* Cached list of command line arguments. These are delimited by spaces with the exception of those between quotation marks. */
	std::vector<DString> ParsedCmdLineArgs;

#ifdef DEBUG_MODE
	/* Brief name of this engine instance.  Typically this is the name of the thread it resides in. */
	DString DebugName;
#endif

	/* Time reference variable from previous frame.  Used to generate deltaTime for tick. */
	clock_t PrevTickTime;

	/* Time at which this engine began.  Contrast to PrevTickTime, this variable is never updated after engine initialization. */
	clock_t StartTime;

	/* If greater than zero, this is the maximum delta time this engine will report.  Not to be confused with Max Frame Rate!
	Essentially increasing this value will reduce the liklihood of causing drastic update rates (ie:  physics causing character
	moving 30 meters within a tick if the application is returning from a break point).  But raising this value too high will
	actually change the result of the physics if the machines are running below minimum frame rate (ie:  physics causes character
	to travel slower than normal).  This value is in delta time (seconds) instead of frames per second. */
	Float MaxDeltaTime;

	/* If greater than zero, this ensures that the engine doesn't invoke objects' Tick Rate faster than this specified amount.
	Instead, it'll cause the application to sleep until enough time elapsed.  This value is in delta time (seconds) instead of frames per second. */
	Float MinDeltaTime;

	/* Object hash IDs that are used to determine where an object can be found within the ObjectHashTable. */
	unsigned int ObjectHashNumber;
	unsigned int EntityHashNumber;
	unsigned int ComponentHashNumber;
	unsigned int TickComponentHashNumber;

	/* Time how long this engine instance was running for (in seconds). */
	Float ElapsedTime;

	/* Counter how many times the engine ticked. */
	size_t FrameCounter;

	/* List of TickGroups this Engine will iterate through for each cycle.  The TickComponents within each group will Tick in
	order the TickGroup appears in this vector.  A TickGroup resides in one thread.  Sub Engines may have their own TickGroup copies. */
	std::vector<TickGroup*> TickGroups;

	/* Determines how often will the engine remove objects that are pending deletion from the object hash table (in seconds). */
	Float GarbageCollectInterval;

	/* List of all objects that may be found through the object iterator (generally includes everything except for default objects).
	Each entry within the vector represents a group with matching HashIDs.  It's sorted in increasing order.  Max size:  31.*/
	std::vector<SHashTableMeta> ObjectHashTable;

	/* List of event handlers that will be notified prior to each garbage collection cycle. */
	std::vector<SDFunction<void>> PreGarbageCollectionEvents;

	/* If true, then the engine is cleaning up resources before it removes itself.
	If this is the main engine, then the application will terminate at the engine's tick cycle. */
	bool bShuttingDown;

private:
	/* List of declared TickGroup names that should be instantiated upon Engine init. */
	static std::vector<DString> TickGroupNames;

	/* Becomes true when the Engine and all of its components has finished initializing. */
	bool bFinishedInit;

	/* Time stamp when the garbage collector last ran. */
	Float PreviousGarbageCollectTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Engine ();

	/**
	 * Unless you're the main application loop, you should never call this destructor!
	 */
	virtual ~Engine ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the current thread is the thread that has the main Engine.
	 */
	static bool IsMainThread ();

	/**
	 * Instructs the Engine to initialize itself and its engine components.
	 */
	virtual void InitializeEngine (size_t engineIdx, const std::vector<EngineComponent*>& engineComponents);

	void SetCommandLineArgs (const DString& newCommandLineArgs);

	/**
	 * Returns true if the specified switch is found in this engine's command line arguments.
	 * A switch in the command line is essentially a flag that becomes true whenever it resides somewhere in the arguments.
	 * Example of a switch always begins with a hyphen (eg:  "-MySwitch")
	 * The parameter should include the hyphen.
	 */
	virtual bool HasCmdLineSwitch (const DString& cmdArgSwitch, DString::ECaseComparison caseComparison) const;

	/**
	 * Returns the value associated with the given key found in this engine's command line arguments.
	 * A command line argument with a key value pair are formatted like: Key="Value" or Key=Value.
	 * The format with quotes around it supports spaces inside the value.
	 * This function returns the value wihtout the quotes.
	 * If the Key is not found in the command line arguments, then this function will return an empty string.
	 */
	virtual DString GetCmdLineValue (const DString& cmdArgKey, DString::ECaseComparison caseComparison) const;

	/**
	 * Reserves a part in the ObjectHashTable and returns the hash number for the newly created entry.
	 * Friendly name is the brief description what this entry is for.
	 * This function will return UINT_INDEX_NONE if the hash table ran out of space (max size:  31).
	 */
	virtual unsigned int RegisterObjectHash (const DString& friendlyName);

	/**
	 * Returns true if this Engine resides in the main thread.
	 */
	bool IsMainEngine () const;

	/**
	 * Causes the engine to crash out with a dialog window to appear.
	 */
	virtual void FatalError (const DString& errorMsg);

	/**
	 * Registers a function handler to this Engine's Pre Garbage Collection event list.
	 */
	virtual void RegisterPreGarbageCollectEvent (SDFunction<void> newHandler);

	/**
	 * Removes the targeted function handler from this Engine's Pre Garbage Collect event list.
	 */
	virtual void RemovePreGarbageCollectEvent (SDFunction<void> oldHandler);

	/**
	 * Deletes all objects with PendingDelete to true.
	 */
	virtual void CollectGarbage ();

	/**
	 * Passes the log message to EngineComponents for serialization and display.
	 * @param category LogCategory that determines which group this log message belongs to.
	 * @param logLevel Importance level this message is associated with.
	 * @param msg The message, itself, to process.
	 */
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& msg);

#ifdef DEBUG_MODE
	virtual void LogObjectHashTable ();
#endif

	/**
	 * Creates a TickGroup instance with the given name and priority where higher priority Tick Groups are called earlier in the Tick cycle.
	 */
	virtual void CreateTickGroup (const DString& tickGroupName, Int tickPriority);

	/**
	 * Invokes Tick upon all objects
	 */
	virtual void Tick ();

	/**
	 * Finds the TickGroup of matching group name.
	 */
	virtual TickGroup* FindTickGroup (const DString& groupName) const;

#ifdef DEBUG_MODE
	virtual void SetDebugName (const DString& newDebugName);
#endif

	virtual void SetMinDeltaTime (Float newMinDeltaTime);
	virtual void SetMaxDeltaTime (Float newMaxDeltaTime);

	/**
	 * Attempts to synchronously add and fully initialize (pre, init, and post) the given engine component.
	 * The Engine will reject the given engine component if it already has a component of matching DClass.
	 * This is used for engine components to be added during runtime (eg: Plugins).
	 * Returns true if the new component is added and initialized.
	 */
	virtual bool AddEngineComponent (EngineComponent* newComponent);

	/**
	 * Removes and deletes a registered engine component with matching DClass.
	 */
	virtual void RemoveEngineComponent (const DClass* engineClass);

	/**
	 * Terminates the engine in current thread at the end of Tick.  If this is the main engine,
	 * then the engine will shutdown all other engines from other threads before closing the application.
	 */
	virtual void Shutdown ();

	/**
	 * Explicitly shuts down the engine on the main thread, which subsequently shuts down all engines,
	 * and will close the application at the end of Tick.
	 */
	static void ShutdownMainEngine ();

	/**
	 * Returns true if this engine is terminating.
	 */
	virtual bool IsShuttingDown () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the instantiated engine by index.
	 */
	static Engine* GetEngine (size_t engineIdx);

	/**
	 * Searches through the engines to find the engine that resides in current thread.
	 */
	static Engine* FindEngine ();

	/**
	 * Searches through the engines to find the engine that resides in the specified thread.
	 */
	static Engine* FindEngine (std::thread::id threadID);

	/**
	 * Retrieves the maximum number of Engine Instances that may be instantiated.
	 */
	static inline Int GetMaxNumPossibleEngines ()
	{
		return 8; //This value must match the EngineInstance's static array length.
	}

	inline size_t GetEngineIndex () const
	{
		return EngineIndex;
	}

	inline DString GetCommandLineArgs () const
	{
		return CommandLineArgs;
	}

	std::thread::id GetThreadID () const;

	inline bool IsInitialized () const
	{
		return bFinishedInit;
	}

	inline std::vector<EngineComponent*> GetEngineComponents () const
	{
		return EngineComponents;
	}

	inline const std::vector<EngineComponent*>& ReadEngineComponents () const
	{
		return EngineComponents;
	}

#ifdef DEBUG_MODE
	inline DString GetDebugName () const
	{
		return DebugName;
	}
#endif

	/**
	 * Obtains the engine's current run time.
	 */
	virtual Float GetElapsedTime () const;

	/**
	 * Obtains the clock that contains the timestamp when this engine launched.
	 * Use this to obtain the delta time that is independent from the engine's time update that is only updated upon Tick.
	 */
	virtual const clock_t& GetStartTime () const;

	virtual size_t GetFrameCounter () const;
	virtual Float GetMinDeltaTime () const;
	virtual Float GetMaxDeltaTime () const;

	virtual unsigned int GetObjectHashNumber () const;
	virtual unsigned int GetEntityHashNumber () const;
	virtual unsigned int GetComponentHashNumber () const;
	virtual unsigned int GetTickComponentHashNumber () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Function to register a new engine component (this should be called at startup time).
	 */
	void RegisterEngineComponent (EngineComponent* newComponent);

	/**
	 * Executes the sequence that'll invoke all registered engine component's pre initialize, initialize,
	 * and post initialize components in dependency order.
	 * This may output a fatal error if a circular dependency is detected.
	 */
	virtual void InitializeEngineComponents ();

	/**
	 * Adds an object to the linked list
	 */
	virtual void RegisterObject (Object* newObject);

	/**
	 * Formats fatal errors to a uniformed template that appears in the crash dialogue window.
	 */
	virtual DString GenerateFatalErrorMsg (const DString& specificError);

	virtual void ShutdownSandDune ();

	friend class Object; //to register objects
	friend class ObjectIterator; //to iterate through linked list
	friend class EngineComponent; //to register components
};
SD_END