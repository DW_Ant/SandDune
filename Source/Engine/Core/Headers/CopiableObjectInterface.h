/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CopiableObjectInterface.h

  This interface adds functions that determine how an object's properties are copied to others.
  This isn't a duplicator where every property is copied (such as delegates and implementation properties [like timestamps]).
  This is intended for systems such as editors' copy/paste, UI styles, and gameplay copying mechanics.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "CoreMacros.h"
#include "Engine.h"

SD_BEGIN
class CORE_API CopiableObjectInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a new CopiableObjectInterface instance that matches the exact type of this instance.
	 * For Objects for example, this is simply the class default object's CreateObjectOfMatchingClass.
	 * Returns the instance that was created.
	 */
	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const = 0;

	/**
	 * Copies various properties from the given template.
	 * The template's class should match or be a parent of this object's class.
	 */
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) = 0;

	/**
	 * Creates and returns a copy of this instance.
	 */
	virtual CopiableObjectInterface* Duplicate () const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Utility that performs a deep copy of the contents from one vector of CopiableObjects to the other.
	 * This will also ensure the two vectors are equal length.
	 *
	 * This function assumes it's a vector of CopiableObjectInterface pointers. Being that they reside on the heap, a deletion method must be provided.
	 * @param deleteElement This lambda is responsible for deleting the object instance. If it's an Object, calling destroy is sufficient enough.
	 */
	template <class T>
	static void CopyVector (const std::vector<T*>& src, std::vector<T*>& outDest, const std::function<void(T* /*objToDelete*/)>& deleteElement)
	{
		CHECK(deleteElement != nullptr)

		for (size_t i = src.size(); i < outDest.size(); ++i)
		{
			if (outDest.at(i) != nullptr)
			{
				deleteElement(outDest.at(i));
				outDest.at(i) = nullptr;
			}
		}

		outDest.resize(src.size());
		for (size_t i = 0; i < outDest.size(); ++i)
		{
			if (outDest.at(i) == nullptr && src.at(i) != nullptr)
			{
				outDest.at(i) = dynamic_cast<T*>(src.at(i)->Duplicate());
			}
			else if (outDest.at(i) != nullptr && src.at(i) == nullptr)
			{
				deleteElement(outDest.at(i));
				outDest.at(i) = nullptr;
			}
			else
			{
				outDest.at(i)->CopyPropertiesFrom(src.at(i));
			}
		}
	}
};
SD_END