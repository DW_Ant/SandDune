/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineIntegrityUnitTester.h
  This class is responsible for conducting unit tests for object handling.
  It'll test object instantiation, object iterators, class iterators,
  object clean up, object references, and the object hash table.
=====================================================================
*/

#pragma once

#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API EngineIntegrityUnitTester : public UnitTester
{
	DECLARE_CLASS(EngineIntegrityUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Tests if the Engine is able to parse through its command line arguments correctly.
	 */
	virtual bool TestCmdLineArgs (EUnitTestFlags testFlags) const;

	/**
	 * Instantiates and kicks off the tick tester.  Returns true if the test launched.
	 */
	virtual bool LaunchTickTester (EUnitTestFlags testFlags) const;

	/**
	 * Launches a simple test that verifies the DClass and its accessors.
	 */
	virtual bool TestDClass (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode