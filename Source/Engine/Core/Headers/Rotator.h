/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Rotator.h
  A datatype that represents 3 dimensional Euler direction.  As the rotation increases, the rotator will rotate
  clockwise from the perspective of facing down the positive axis.

  The starting rotation (all axis equal to 0) begins facing down the X-axis.

  The rotators assume a left handed coordinate system where X is to the right and Z is up.

  The rotators internally are represented in integers to have a form of 'snapping' by the nearest
  ~0.0055 degrees.

  Additionally, the unsigned ints are used to automatically wrap around to 0 degrees once it passes one full revolution.

  Rotators also support converting unit types from and to degrees and radians.  Be mindful of precision errors
  when converting units.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DProperty.h"
#include "SDFloat.h"
#include "Vector3.h"

SD_BEGIN
class TransformMatrix;

class CORE_API Rotator : public DProperty
{


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum ERotationUnit
	{
		RU_Degrees,
		RU_Radians
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Multiplier to use when converting degrees to radians. */
	static const Float DEGREE_TO_RADIAN;

	/* Multiplier to use when converting radians to degrees. */
	static const Float RADIAN_TO_DEGREE;

	/* Rotator where all rotation axis are zeroed (points along positive X-axis). */
	static const Rotator ZERO_ROTATOR;

	/* Smallest unit for the rotators (1 / 65536). This value is rounded up */
	static const Float ROTATOR_UNIT;

	/* The amount of int units that makes up one entire revolution (360 degrees, 180 degrees, and 90 degrees respectively). */
	static const unsigned int FULL_REVOLUTION;
	static const unsigned int HALF_REVOLUTION;
	static const unsigned int QUARTER_REVOLUTION;

	static const Float FULL_REVOLUTION_Float;
	static const Float HALF_REVOLUTION_Float;
	static const Float QUARTER_REVOLUTION_Float;

	/* Rotation about the Z-axis.  Positive turns left, negative turns right. Ranges from 0-65535. */
	unsigned int Yaw : 16;

	/* Rotation about the Y-axis.  Positive spins up, negative spins down. turns Ranges from 0-65535. */
	unsigned int Pitch : 16;

	/* Rotation about the X-axis.  Positive spins banks clockwise, Negative banks counterclockwise. Ranges from 0-65535. */
	unsigned int Roll : 16;

private:
	/* Extra buffer to fill in the remaining byte (for alignment purposes). */
	unsigned int Unused : 16;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rotator ();

	Rotator (const Rotator& copyRotator);

	/* Constructs a rotator through SD rotation units. */
	Rotator (unsigned int inYaw, unsigned int inPitch, unsigned int inRoll);

	/* Constructs a rotator from a single uint64. First 16 bits are yaw, second 16 bits are Pitch, third 16 bits are roll. Last 16 bits does nothing. */
	Rotator (uint64 rotations);

	/**
	 * Constructs a rotator from the specified components.
	 * @param rotationUnit signals the constructor what units the parameters are using.
	 */
	Rotator (Float inYaw, Float inPitch, Float inRoll, ERotationUnit rotationUnit);

	/* Constructs a rotator from the specified directional vectors.  Vectors are automatically normalized. */
	Rotator (Vector3 directionVector);

	/* Computes the Euler angles from a 4x4 transform matrix. */
	Rotator (const TransformMatrix& transformMatrix);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Rotator& copyRotator);
	void operator= (Vector3 directionVector);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Constructs a Rotator from signed integers.  Values are automatically wrapped (not clamped)
	 * to be within 0-65k.  Units are in SD units.
	 */
	static Rotator MakeRotator (Int inYaw, Int inPitch, Int inRoll);

	/**
	 * Returns the direction (either 1, 0, or -1) that would lead the start angle closer to desired angle
	 * 1 implies rotating along the positive direction will reach the desired angle sooner than rotating counter clockwise.
	 * -1 implies rotating along the negative direction will arrive sooner.
	 * Returns 0 if the startAngle is already equal to the desiredAngle.
	 *
	 * For example: if the start angle is 15 and the desired angle is 65520, then this function will return -1 since it will
	 * be faster to cross over 0 degrees in order to reach 65520.
	 */
	static Float GetShorterDirectionMultiplier (unsigned int startAngle, unsigned int desiredAngle);
	static Float GetShorterDirectionMultiplier (Float startAngle, Float desiredAngle, ERotationUnit unit);

	/**
	 * Sets the rotation from individual components.
	 */
	void SetRotation (Float inYaw, Float inPitch, Float inRoll, ERotationUnit rotationUnit);

	void SetYaw (Float inYaw, ERotationUnit rotationUnit);
	void SetPitch (Float inPitch, ERotationUnit rotationUnit);
	void SetRoll (Float inRoll, ERotationUnit rotationUnit);

	/**
	 * Returns true if the given rotator is perpendicular to this rotator.
	 */
	virtual bool IsPerpendicularTo (const Rotator& otherRotator, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns true if the given rotator is parallel to this rotator.
	 */
	virtual bool IsParallelTo (const Rotator& otherRotator, Float tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns true if this rotator is approximately close to the other rotator.
	 */
	virtual bool IsNearlyEqual (const Rotator& otherRotator, Float tolerance = ROTATOR_UNIT) const;

	/**
	 * Inverts this rotator to face the opposite direction.
	 * For example: A rotator with a positive 90 degrees in Yaw would end up being a rotator with a 270 degrees Yaw.
	 * The roll axis is not affected.
	 */
	virtual void Invert ();

	/**
	 * Returns an int used to represent a single rotation with all three axis.
	 */
	virtual uint64 ToInt () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Adds the axis by the given value while properly wrapping the angle to be within one revolution.
	 * Units are in SD units.
	 */
	virtual void AddYaw (Float addAmount);
	virtual void AddPitch (Float addAmount);
	virtual void AddRoll (Float addAmount);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns a unit length vector that points in the same direction as this rotator.
	 */
	virtual Vector3 GetDirectionalVector () const;

	void GetDegrees (Float& outYaw, Float& outPitch, Float& outRoll) const;
	void GetRadians (Float& outYaw, Float& outPitch, Float& outRoll) const;

	Float GetYaw (ERotationUnit rotationUnit) const;
	Float GetPitch (ERotationUnit rotationUnit) const;
	Float GetRoll (ERotationUnit rotationUnit) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Retrieves the multiplier that converts from Rotator to specified unit.
	 */
	Float GetRotatorToUnitConverter (ERotationUnit rotationUnit) const;

	/**
	 * Retrieves the multiplier that converts from specified unit to rotator unit.
	 */
	Float GetUnitToRotatorConverter (ERotationUnit rotationUnit) const;

	/**
	 * Sets Yaw, Pitch, Roll based on the direction of the given normalized vector.
	 * This function will prioritize yaw over pitch where it'll try to rotate along the XY plane before
	 * adjusting the pitch.
	 * This function assumes a left handed coordinate system where each axis are rotated clockwise.
	 * The roll is always 0.
	 */
	void SetComponentsFromVector (const Vector3& normalizedVector);
};

#pragma region "External Operators"
CORE_API bool operator== (const Rotator& left, const Rotator& right);
CORE_API bool operator!= (const Rotator& left, const Rotator& right);
CORE_API Rotator operator+ (const Rotator& left, const Rotator& right);
CORE_API Rotator& operator+= (Rotator& left, const Rotator& right);
CORE_API Rotator operator- (const Rotator& left, const Rotator& right);
CORE_API Rotator& operator-= (Rotator& left, const Rotator& right);
#pragma endregion
SD_END