/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickComponent.h
  TickComponent contains a delegate that is continuously called from the Engine every frame.
=====================================================================
*/

#pragma once

#include "EntityComponent.h"

SD_BEGIN
class TickGroup;

class CORE_API TickComponent : public EntityComponent
{
	DECLARE_CLASS(TickComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines how frequently this TickComponent will invoke OnTick.  If negative, it'll update every frame,
	otherwise, it'll tick at this given interval. */
	Float TickInterval;

	/* Function to callback on every update.  DeltaSec is the amount of time elapsed since last OnTick. */
	SDFunction<void, Float /*deltaSec*/> OnTick;

	/* Same as OnTick but it includes an additional parameter that references the tick component. This callback is mutually exclusive from OnTick.
	'Ex' in the variable name is short for extended. */
	SDFunction<void, Float /*deltaSec*/, TickComponent* /*tick*/> OnTick_Ex;

	/* TickGroup this component is registered to.  This TickComponent may not be registered to the TickGroup if the component
	is not ticking. */
	TickGroup* OwningTickGroup;

	/* Only used at init, this is the TickGroup this TickComponent is about to register to. */
	DString PendingTickGroupName;

	/* If true, then the Tick function is active and is being called every frame. */
	bool bTicking;

private:
	/* Accumulated deltaTime since last OnTick execution. */
	Float AccumulatedDeltaTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TickComponent ();
	TickComponent (const DString& inTickGroup);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
#ifdef DEBUG_MODE
	virtual DString GetFriendlyName () const override;
#endif

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Overload method to specify which Tick group this component should register to.
	 */
	static TickComponent* CreateObject (const DString& tickGroupName);

	/**
	 * This function runs every update call where deltaTime is the time since the last update call.
	 */
	virtual void Tick (Float deltaSec);

	/**
	 * For TickComponents with TickIntervals, this resets the accumulated Tick time so that the Tick function
	 * will not be invoked until enough time elapsed.  The accumulated tick time resets automatically on Tick,
	 * this function only needs to be invoked to reset the time before Tick is invoked.
	 */
	virtual void ResetAccumulatedTickTime ();

	/**
	 * Sets the accumulated time to the tick interval in order to force the tick cycle the next time Tick is invoked.
	 */
	virtual void ForceTickNextFrame ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTickInterval (Float newTickInterval);

	/**
	 * Assigns the OnTick delegate to the given parameter.
	 * This function will unbind the OnTickEx delegate.
	 */
	virtual void SetTickHandler (SDFunction<void, Float> newHandler);

	/**
	 * Assigns the OnTick_Ex delegate to the given parameter.
	 * This function will unbind the OnTick delegate.
	 */
	virtual void SetTickExHandler (SDFunction<void, Float, TickComponent*> newHandler);

	virtual void SetTicking (bool bNewTicking);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool IsTicking () const;

	inline Float GetTickInterval () const
	{
		return TickInterval;
	}

	inline const SDFunction<void, Float>& ReadTickHandler () const
	{
		return OnTick;
	}

	inline const SDFunction<void, Float, TickComponent*>& ReadTickExHandler () const
	{
		return OnTick_Ex;
	}
};
SD_END