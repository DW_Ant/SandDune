/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Utils.h
  Contains multiple commonly used static utility functions.
=====================================================================
*/

#pragma once

#include "BaseUtils.h"
#include "DString.h"

#define PI 3.14159265358
#define PI_FLOAT 3.141593f

SD_BEGIN
class CORE_API Utils : public BaseUtils
{
	DECLARE_CLASS(Utils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the string suffix of executables and dlls for the current build configuration.
	 * For example: it'll return the "-Debug" in "SD-Core-Debug.dll" in debug builds.
	 */
	static DString GetBinaryConfigSuffix ();

	/**
	 * Returns the string prefix of executables and dlls for a Sand Dune module.
	 * For example: it'll return the "SD-" in "SD-Core-Debug.dll".
	 */
	static inline DString GetBinarySdPrefix ()
	{
		return TXT("SD-");
	}


	/*
	=====================
	  Templates
	=====================
	*/

public:
#pragma region "Templates"
	/**
	 * Returns the larger number of the two numbers.
	 */
	template <class Type>
	static inline const Type Max (const Type& a, const Type& b)
	{
		return (a > b) ? a : b;
	}

	/**
	 * Returns the smaller number of the two numbers.
	 */
	template <class Type>
	static inline const Type Min (const Type& a, const Type& b)
	{
		return (a < b) ? a : b;
	}

	/**
	 * Returns a copy of input that is bounded to min and max.
	 */
	template <class Type>
	static inline const Type Clamp (const Type& input, const Type& min, const Type& max)
	{
		return Max(Min(input, max), min);
	}

	/**
	 * Linear interpolation  between minBounds and maxBounds based on the given ratio.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam Type The datatype used for minBounds and maxBounds.
	 * @tparam A The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and Floats) are supported.
	 */
	template <class Type, class A>
	static inline const Type Lerp (const A& ratio, const Type& minBounds, const Type& maxBounds)
	{
		return static_cast<Type>((ratio * static_cast<A>(maxBounds - minBounds)) + static_cast<A>(minBounds));
	}

	/**
	 * Rounds the given number to the nearest whole value.
	 * [0-0.5) rounds to 0.
	 * [0.5-1.0) rounds to 1.
	 * Only decimal template arguments (such as float, double, and Float) are supported.
	 */
	template <class Type>
	static inline const Type Round (const Type& a)
	{
		return std::floor(a + 0.5f);
	}

	/**
	 * Returns true if the two numbers are approximately equal to each other
	 * within the specified tolerance.
	 */
	template <class Type>
	static inline bool IsAboutEqual (const Type& a, const Type& b, const Type& tolerance)
	{
		return (std::abs(a - b) <= tolerance);
	}

	/**
	 * Returns true if the given is in powers of 2.
	 * Only integers are supported.
	 */
	template <class Type>
	static inline bool IsPowerOf2 (const Type& num)
	{
		return (num > 0 && !(num & (num - 1)));
	}
#pragma endregion Templates
};
SD_END