/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreDatatypes.h
  Includes all Core datatypes.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DProperty.h"
#include "DataBuffer.h"
#include "Int.h"
#include "SDFloat.h"
#include "Bool.h"
#include "DString.h"
#include "StringIterator.h"
#include "Range.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Rotator.h"
#include "Rectangle.h"
#include "Matrix.h"
#include "SDFunction.h"
#include "DPointerInterface.h"
#include "DPointerBase.h"
#include "DPointer.h"