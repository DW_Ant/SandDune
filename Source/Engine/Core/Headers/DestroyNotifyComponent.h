/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DestroyNotifyComponent.h
  A simple component that invokes a delegate when it's destroyed.

  This component can either be destroyed manually or automatically whenever its
  owner is also destroyed.
=====================================================================
*/

#pragma once

#include "EntityComponent.h"
#include "SDFunction.h"

SD_BEGIN
class Entity;

class CORE_API DestroyNotifyComponent : public EntityComponent
{
	DECLARE_CLASS(DestroyNotifyComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever this component is destroyed. */
	SDFunction<void, DestroyNotifyComponent* /*destroyedComp*/> OnDestroyed;

	/* The Owner reference may be cleared by the time OnDestroyed is called.
	Reference this variable if there's a need to access the Owner this component was attached to. */
	Entity* MostRecentOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Entity* GetMostRecentOwner () const
	{
		return MostRecentOwner;
	}
};
SD_END