/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DClass.h
  At startup time, the DClass will generate a class tree of all classes
  (with DECLARE_CLASS macro).  The DClasses may possess a reference to the
  parent and children DClasses.

  NOTE:  You can still have multi-inherited classes.  This class is mostly
  used to help iterate through classes, and obtain default objects from each class.
=====================================================================
*/

#pragma once

#include "Core.h"

#include "DString.h"

SD_BEGIN
class Object;
class EngineComponent;

class CORE_API DClass
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EClassType
	{
		CT_None,
		CT_Object,
		CT_EngineComponent,
		CT_Other
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SOrphanInfo
	{
		DClass* ClassInstance;
		DString ParentClass;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Type of class this DClass refers to, and helps determine which default object pointer in DefaultInstance union is used. */
	enum EClassType ClassType;

protected:
	/* Readable name of the class including its namespace (eg: "SD::Object").  This was renamed from ClassName to DuneClassName to avoid naming conflict with Microsoft's GetClassName macro. */
	DString DuneClassName;
	DClass* ParentClass;

	/* List of all direct subclasses (does not include children subclasses).  This is sorted alphabetically by DuneClassName. */
	std::vector<DClass*> Children;

	/* Remains true until a CDO instance is associated with this object. */
	bool IsAbstractClass;

	/* The unique hash of this class that can be used to find this class in the DClassAssembler map. */
	size_t ClassHash;

	union DefaultInstance
	{
		/* Reference to an instantiated object that simply contains a reference to its default properties,
		and is not meant for any modifications at run time.*/
		Object* DefaultObject;
		EngineComponent* DefaultEngineComponent;
	} CDO;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DClass (const DString& className);

	virtual ~DClass ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to find the DClass instance from the given string.
	 * See DClassAssembler::FindDClass.
	 */
	static const DClass* FindDClass (const DString& className);
	static const DClass* FindDClass (size_t classHash);

	/**
	 * Updates the DefaultObject's value if it's not set already.
	 * This DefaultObject will automatically be deleted when this DClass instance is destroyed.
	 */
	virtual void SetCDO (Object* newCDO);
	virtual void SetCDO (EngineComponent* newCDO);

	/**
	 * Returns true if the parameter is equal to or a parent class of the current class.
	 */
	virtual bool IsChildOf (const DClass* targetParentClass) const;

	/**
	 * Returns true if the parameter is equal to or a child of the current class.
	 */
	virtual bool IsParentOf (const DClass* targetChildClass) const;

	inline bool IsA (const DClass* parentClass) const
	{
		return IsChildOf(parentClass);
	}

	/**
	 * Returns the class name of this class without the namespace prefixes.
	 */
	virtual DString GetClassNameWithoutNamespace () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetDuneClassName () const
	{
		return DuneClassName;
	}

	inline const DString& ReadDuneClassName () const
	{
		return DuneClassName;
	}

	virtual DString ToString () const;

	const DClass* GetSuperClass () const;
	inline bool IsAbstract () const
	{
		return IsAbstractClass;
	}

	inline size_t GetClassHash () const
	{
		return ClassHash;
	}

	const Object* GetDefaultObject () const;
	const EngineComponent* GetDefaultEngineComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void AddChild (DClass* child);

	friend class DClassAssembler;
	friend class ClassIterator;
};
SD_END