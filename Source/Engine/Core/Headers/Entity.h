/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Entity.h
  Entity is an object that does not have a definition by itself.  Entities
  are essentially defined through their components.
=====================================================================
*/

#pragma once

#include "DPointer.h"
#include "Object.h"

SD_BEGIN
class EntityComponent;

class CORE_API Entity : public Object
{
	DECLARE_CLASS(Entity)


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* List of all immediate entity components this Entity owns.  This does not
	reference entity components that are owned by this entity's entity components. */
	std::vector<EntityComponent*> Components;

	/* If true, then this Entity is visible.  Typically if the Owner is invisible, all sub components are invisible, too. */
	bool bVisible;

	/* An number that represents the "sum" of components' hash IDs.  This provides a quick glance
	in what components are affecting this entity without having to iterate through them all.
	Components within components also affect this value. */
	unsigned int ComponentsModifier;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to remove an component from this entity.
	 * Returns true if component was found and detached form component list.
	 * This does NOT destroy the component.
	 */
	virtual bool RemoveComponent (EntityComponent* target);

	/**
	 * Adjusts the ComponentsModifier value based on the new component's hash.
	 */
	virtual void AddComponentModifier (EntityComponent* newComponent);

	/**
	 * Adjusts the ComponentsModifier value based on the new component's hash.
	 * The value will not be adjusted if this entity possesses another component with the same hash.
	 * Returns true if the ComponentModifier was modified.
	 */
	virtual bool RemoveComponentModifier (EntityComponent* oldComponent);

	/**
	 * Recursively searches through this entity's component list to find the first
	 * component whose hash matches the given parameters.
	 * if bRecursive is false, then this function will not search components within components.
	 * This will only return the first component it finds. Use a ComponentIterator to find multiple components.
	 */
	virtual EntityComponent* FindComponent (unsigned int targetComponentHash, bool bRecursive = true) const;

	/**
	 * Recursively searches through this entity's component list to find one that matches or is a subclass of the given class.
	 * If bRecursive is false, then this function will not search components within components.
	 * This will only return the first component it finds. Use a ComponentIterator to find multiple components.
	 */
	virtual EntityComponent* FindComponent (const DClass* targetClass, bool bRecursive = true) const;

	/**
	 * Returns true if this Entity contains at least one component that matches (or is a subclass) the given class.
	 * If bRecursive is false, then this function wll not search components within components.
	 */
	virtual bool HasComponent (const DClass* componentClass, bool bRecursive = true) const;

	/**
	 * Moves the specified component to the beginning of the Component list, causing the component to be the first object found when using a component iterator.
	 * Does nothing if the component does not reside in this Entity.
	 */
	virtual void MoveComponentToFront (EntityComponent* comp);

	/**
	 * Moves the specified component to the end of the component list, causing the component to be the last object found when using a component iterator.
	 * Does nothing if the component does not reside in this Entity.
	 */
	virtual void MoveComponentToBack (EntityComponent* comp);

	virtual void SetVisibility (bool newVisibility);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<EntityComponent*>& ReadComponents () const
	{
		return Components;
	}

	virtual bool IsVisible () const;
	virtual unsigned int GetComponentsModifier () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Attempts to add the given component to the Entity's component list.
	 * Returns true if successfully attached.
	 */
	virtual bool AddComponent_Implementation (EntityComponent* newComponent);

#ifdef DEBUG_MODE
	/**
	 * For debug builds, this function runs every frame.
	 * It's a quick utility for checking status on this entity without having to create a TickComponent.
	 */
	virtual void DebugTick (Float deltaTime);
#endif


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Adds the new component to the entity's component array.
	 * @param outNewComponent - The component instance to attempt to attach to this Entity.
	 * @param bDeleteOnFail - If true then the outNewComponent instance will be destroyed and the pointer will be nulled out if it failed to attach to this Entity.
	 * Returns true if outNewComponent successfully attached to this Entity.
	 */
	template <class T>
	bool AddComponent (T*& outNewComponent, bool bDeleteOnFail = true)
	{
		if (outNewComponent == nullptr)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot attach an undefined component to %s"), ToString());
			return false;
		}

		bool success = AddComponent_Implementation(outNewComponent);
		if (!success)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to attach %s to %s since the component refused to attach itself to the specified entity."), outNewComponent->ToString(), ToString());
		}

		if (!success && bDeleteOnFail)
		{
			outNewComponent->Destroy();
			outNewComponent = nullptr;
		}

		return success;
	}

	template <class T>
	bool AddComponent (DPointer<T>& outNewComponent, bool bDeleteOnFail = true)
	{
		if (outNewComponent.IsNullptr())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot attach an undefined component to %s"), ToString());
			return false;
		}

		bool success = AddComponent_Implementation(outNewComponent.Get());
		if (!success)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to attach %s to %s since the component refused to attach itself to the specified entity."), outNewComponent->ToString(), ToString());
		}

		if (!success && bDeleteOnFail)
		{
			outNewComponent->Destroy();
			//DPointers are already nulled out when the object they're pointing is destroyed.
		}

		return success;
	}

	friend class ComponentIterator;
	friend class Engine;
	friend class EntityComponent;
};
SD_END