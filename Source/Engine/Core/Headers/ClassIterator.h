/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClassIterator.h
  The ClassIterator is an utility class that makes it easy to iterate
  through DClasses.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Int.h"

SD_BEGIN
class DClass;

class CORE_API ClassIterator
{


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/* When stepping through the class hierarchy, this struct contains necessary information which child class was iterated. */
	struct SClassIteratorInfo
	{
		const DClass* CurrentClass = nullptr;

		/* Reference to the index of the current selected child class. */
		Int ChildIndex = -1;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to the DClass this iterator is currently pointing to. */
	const DClass* SelectedClass;

protected:
	/* Reference to the initial class (does not iterator through parent classes of initial class). */
	const DClass* InitialClass;

	/* Chain of parent classes between the SelectedClass and the initial class. */
	std::vector<SClassIteratorInfo> ParentClasses;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Create a class iterator that searches through all Objects.
	 */
	ClassIterator ();

	/**
	 * Create a class iterator that searches from the given initial class and all of its subclasses.
	 */
	ClassIterator (const DClass* newInitialClass);

	virtual ~ClassIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator++ (); //++iter
	virtual void operator++ (int); //iter++


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	const DClass* GetSelectedClass () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeIterator ();

	/**
	 * Selects the next DClass in the hierarchy.  It'll iterate through child classes first.
	 * Then it'll step up in the ParentClasses vector.  Updates Selected Object.
	 */
	virtual void SelectNextClass ();
};
SD_END