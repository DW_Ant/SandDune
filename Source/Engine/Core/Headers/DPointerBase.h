/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DPointerBase.h
  Defines the functionality how a DPointer may register and remove itself from the DPointer linked list.

  This class does not contain a pointer to an object.
=====================================================================
*/

#pragma once

#include "DPointerInterface.h"

SD_BEGIN
class CORE_API DPointerBase
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Previous DPointer in the Object's DPointer linked list.  The object that this pointer points to will maintain a
	linked list of all DPointers that references that object.  If null, then this pointer is leading the pointer linked list. */
	DPointerBase* PreviousPointer;

	/* Next DPointer in the Object's DPointer linked list.  The object that this pointer points to will maintain a
	linked list of all DPointers that references that object.  If null, then this pointer is at the end of the pointer linked list. */
	DPointerBase* NextPointer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DPointerBase ();
	DPointerBase (const DPointerBase& other);
	virtual ~DPointerBase ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	//Checking if this pointer is pointing to nullptr or not
	virtual operator bool () const = 0;
	virtual bool operator! () const = 0;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the pointer to point to nullptr.  Warning, calling this function will sever this pointer
	 * from the pointer linked list.
	 */
	virtual void ClearPointer () = 0;

	/**
	 * Returns a raw pointer of this DPointer instance (not the actual object it's pointing at).
	 */
	virtual DPointerBase* GetPointerAddress ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DPointerBase* GetPreviousPointer () const
	{
		return PreviousPointer;
	}

	inline DPointerBase* GetNextPointer () const
	{
		return NextPointer;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Registers this DPointer to the object's DPointer linked list.
	 */
	void RegisterDPointer (const DPointerInterface* referencedObj);

	/**
	 * Removes this DPointer from the referenced object's DPointer linked list.
	 */
	void RemoveDPointer (const DPointerInterface* oldReferencedObj);
};
SD_END