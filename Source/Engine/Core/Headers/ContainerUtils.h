/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContainerUtils.h
  Various utility functions supporting std vector containers.
=====================================================================
*/

#pragma once

#include "BaseUtils.h"

SD_BEGIN
class CORE_API ContainerUtils : public BaseUtils
{
	DECLARE_CLASS(ContainerUtils)


	/*
	=====================
	  Templates
	=====================
	*/

public:
#pragma region "Templates"
	/**
	 * Returns true if the vector does not contain any elements (length 0).
	 */
	template <class Type>
	static inline bool IsEmpty (const std::vector<Type>& vectorToCheck)
	{
		return vectorToCheck.empty(); //'empty' is my least favorite function name from the std library.
	}

	/**
	 * Removes all elements within the vector.
	 */
	template <class Type>
	static inline void Empty (std::vector<Type>& outVectorToClear)
	{
		outVectorToClear.clear();
	}

	/**
	 * Returns true if the specified index is within the range of the vector's indices.
	 * Checks to see if it's greater than or equal to 0, and it's less than the vector size.
	 */
	template <class Type>
	static inline bool IsValidIndex(const std::vector<Type>& vector, Int index)
	{
		return (index < vector.size() && index >= 0);
	}

	template <class Type>
	static inline bool IsValidIndex(const std::vector<Type>& vector, size_t index)
	{
		return (index < vector.size());
	}

	/**
	 * Returns the last element of the given vector.  Asserts if the vector is empty.
	 */
	template <class Type>
	static inline Type GetLast (const std::vector<Type>& vector)
	{
		CHECK(!IsEmpty(vector));
		return vector.at(vector.size() - 1);
	}

	template <class Type>
	static inline Type& GetLast (std::vector<Type>& vector)
	{
		CHECK(!IsEmpty(vector))
		return vector.at(vector.size() - 1);
	}

	/**
	 * Returns the first index that matches the specified target.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE FindInVector (const std::vector<Type>& vector, Type searchFor)
	{
		for (UINT_TYPE i = 0; i < vector.size(); i++)
		{
			if (vector.at(i) == searchFor)
			{
				return i;
			}
		}

		return UINT_INDEX_NONE;
	}

	/**
	 * Removes the specified item from the vector.  Complexity is linear.
	 * @return The index the item was removed from, or returns INT_INDEX_NONE if the item is not found.
	 */
	template <class Type>
	static UINT_TYPE RemoveItem (std::vector<Type>& outVector, Type target)
	{
		UINT_TYPE itemIdx = FindInVector(outVector, target);
		if (itemIdx != UINT_INDEX_NONE)
		{
			outVector.erase(outVector.begin() + itemIdx);
		}

		return itemIdx;
	}

	/**
	 * Removes all items that match the target.  Complexity is linear.
	 * @return The number of items that were removed from the vector.
	 */
	template <class Type>
	static Int RemoveItems (std::vector<Type>& outVector, Type target)
	{
		if (IsEmpty(outVector))
		{
			return 0;
		}

		Int numRemoved = 0;
		//Iterate backwards so that the erases do not interfere with indices.
		UINT_TYPE i = outVector.size() - 1;
		while (true)
		{
			if (outVector.at(i) == target)
			{
				outVector.erase(outVector.begin() + i);
				++numRemoved;
			}

			if (i == 0)
			{
				break;
			}
			--i;
		}

		return numRemoved;
	}

	/**
	 * Adds a unique element to the specified vector to avoid duplicate entries.
	 * Returns the index the item is found in vector if it already exists.
	 * If the item doesn't exist, and it was added to the end of the vector, it will return UINT_INDEX_NONE.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE AddUnique (std::vector<Type>& outVector, Type itemToAdd)
	{
		UINT_TYPE itemIdx = FindInVector(outVector, itemToAdd);
		if (itemIdx == UINT_INDEX_NONE)
		{
			outVector.push_back(itemToAdd);
		}

		return itemIdx;
	}

	/**
	 * Counts and returns the number of times this value appears in the specified vector.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE CountNumItems (const std::vector<Type>& vector, Type itemTarget)
	{
		UINT_TYPE count = 0;
		for (UINT_TYPE i = 0; i < vector.size(); ++i)
		{
			if (vector.at(i) == itemTarget)
			{
				count++;
			}
		}

		return count;
	}
#pragma endregion Templates
};
SD_END