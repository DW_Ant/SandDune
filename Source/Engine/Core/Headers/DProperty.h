/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DProperty.h
  Short for Dune Property, this abstract class is the parent class of
  all Sand Dune data types.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DString;
class DataBuffer;

struct CORE_API CommonPropertyRegex
{
public:
	/* Common regexes to use when parsing strings to a particular variable type. */
	static const DString Bool;
	static const DString Float;
	static const DString Int;
	static const DString STRING_GREEDY;
	static const DString STRING_LAZY;
};

class CORE_API DProperty
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the contents of this variable to a clean slate. Typically this assigns everything back to zero.
	 */
	virtual void ResetToDefaults () = 0;

	/**
	 * Returns a string representation of this data type.
	 */
	virtual DString ToString () const = 0;

	/**
	 * Initializes this variable based on the contents of the string.
	 * A variable should be able to parse a string that was generated from ToString.
	 */
	virtual void ParseString (const DString& str) = 0;

	/**
	 * Returns the minimum number of bytes required to read from a ByteBuffer for this variable.
	 * For dynamically sized properties (such as strings), this function returns the minimal case (eg: empty string).
	 */
	virtual size_t GetMinBytes () const = 0;

	/**
	 * Saves this variable to the given dataBuffer.
	 */
	virtual void Serialize (DataBuffer& outData) const = 0;

	/**
	 * Reads from the given dataBuffer, and records the byte contents to this variable.
	 * Returns true on success. Returns false on malformed data or if there aren't enough bytes in the buffer.
	 */
	virtual bool Deserialize (const DataBuffer& dataBuffer) = 0;

	/**
	 * Parses through the string and returns the contents of the variable's value if found.
	 * @param data - This function expects the string to be in Name=Value format. Variable names can be preceded by spaces, commas, and open parenthesis. It is case sensitive.
	 * For example, a data struct could be in this format: Struct=(Var1=A, Var2 = B , Var3=C)
	 * @param varName - The name of the variable within data this function is suppose to match.
	 * @param regexMatch - The pattern of characters that are permitted in the var's value. There must be a grouping in the string (eg: parenthesis) to instruct this function
	 * which character(s) should be returned. See CommonPropertyRegex struct for examples.
	 * Returns an empty string if the variable is not found.
	 */
	static DString ParseVariable (const DString& data, const DString& varName, const DString& regexMatch);

	/**
	 * Parses a data struct from the string, and returns the parenthesis and everything between the parenthesis.
	 * This uses a string iterator to count the open/closing parenthesis to handle nested structs.
	 * Parenthesis within quotes are ignored since it would assume it resides in a string variable.
	 * Strings should escape their quotes using the '\' character if the quote is part of the string value.
	 * Returns an empty string if the struct variable is not found or if there isn't a matching closing parenthesis.
	 * Complexity is linear. Strings are case sensitive.
	 */
	static DString ParseStruct (const DString& data, const DString& structName);
};
SD_END