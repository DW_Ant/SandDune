/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreUtilUnitTester.h
  This class is responsible for conducting unit tests for the Util classes.
=====================================================================
*/

#pragma once

#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API CoreUtilUnitTester : public UnitTester
{
	DECLARE_CLASS(CoreUtilUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestCoreUtils (EUnitTestFlags testFlags) const;
	virtual bool TestContainerUtils (EUnitTestFlags testFlags) const;


	/*
	=====================
	  Templates
	=====================
	*/

protected:
	template <class T>
	bool TestNumericTemplateUtils (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode