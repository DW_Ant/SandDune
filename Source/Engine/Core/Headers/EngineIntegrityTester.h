/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineIntegrityTester.h
  This object assists the EngineIntegrityUnitTester in ensuring the objects and properties
  instantiated from its test is also cleaned up.
=====================================================================
*/

#pragma once

#include "CoreDatatypes.h"
#include "EngineComponent.h"
#include "Object.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API EngineIntegrityTester : public Object
{
	DECLARE_CLASS(EngineIntegrityTester)


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* Reference to all instantiated objects this unit tester created. */
	std::vector<DPointer<Object>> InstantiatedObjects;

	/* List of all objects that were registered to the hash table before this unit test created objects. */
	std::vector<Object*> AllObjectsPriorTest;

	/* Properties for the property iterator to find. */
	Int PropIterINT;
	Float PropIterFloat;
	DString PropIterString;
	Vector3 PropIterVector;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual bool TestClassIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestObjectInstantiation (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestObjectIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestCleanUp (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestComponents (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestComponentIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	virtual bool TestEntityVisibility (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each engine component to notify them about this test's next step.
	 */
	virtual void NotifyEngineComponents (EngineComponent::EEngineIntegrityStep testStep);
};
SD_END

#endif //debug_mode