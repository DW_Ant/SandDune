/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Range.h
  A templated class responsible for tracking min/max numbers.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "DProperty.h"
#include "DString.h"
#include "Utils.h"

SD_BEGIN
template<class T>
class Range : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	typename T Min;
	typename T Max;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Range ()
	{
	}

	Range (const Range& copyConstructor)
	{
		Min = copyConstructor.Min;
		Max = copyConstructor.Max;
	}

	Range (const T& inMin, const T& inMax)
	{
		Min = inMin;
		Max = inMax;
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Range& copyValue)
	{
		Min = copyValue.Min;
		Max = copyValue.Max;
	}

	bool operator== (const Range& otherRange) const
	{
		return (Min == otherRange.Min && Max == otherRange.Max);
	}

	bool operator!= (const Range& otherRange) const
	{
		return !(operator==(otherRange));
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	void ResetToDefaults () override
	{
		Min.ResetToDefaults();
		Max.ResetToDefaults();
	}

	DString ToString () const override
	{
		return DString::CreateFormattedString(TXT("(Min=%s, Max=%s)"), Min, Max);
	}

	void ParseString (const DString& str) override
	{
		DString varData = ParseVariable(str, TXT("Min"), CommonPropertyRegex::Float);
		if (!varData.IsEmpty())
		{
			Min = T(varData);
		}

		varData = ParseVariable(str, TXT("Max"), CommonPropertyRegex::Float);
		if (!varData.IsEmpty())
		{
			Max = T(varData);
		}
	}

	size_t GetMinBytes () const override
	{
		return sizeof(T) * 2;
	}

	void Serialize (DataBuffer& outData) const override
	{
		outData << Min;
		outData << Max;
	}

	bool Deserialize (const DataBuffer& dataBuffer) override
	{
		dataBuffer >> Min >> Max;
		return !dataBuffer.HasReadError();
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the min/max values are equal.
	 */
	virtual bool IsConverged () const
	{
		return (Min == Max);
	}

	/**
	 * Returns true if any of the values between min/max resides within the other range.
	 */
	virtual bool Intersects (const Range& otherRange) const
	{
		return (Min < otherRange.Max && Max > otherRange.Min);
	}

	/**
	 * Returns the intersection of this range and the other range.
	 */
	virtual Range GetIntersectionFrom (const Range& otherRange) const
	{
		if (!Intersects(otherRange))
		{
			return Range();
		}

		return Range(Utils::Max(Min, otherRange.Min), Utils::Min(Max, otherRange.Max));
	}

	/**
	 * Returns true this range entirely fits within the otherRange.
	 * (Evaluates:  otherRange.Min <= Min && otherRange.Max >= Max).
	 */
	virtual bool IsWithin (const Range& otherRange) const
	{
		return (otherRange.Min <= Min && otherRange.Max >= Max);
	}

	/**
	 * Returns true if this range completely encapsulates the otherRange (Opposite of IsWithin).
	 */
	virtual bool Surrounds (const Range& otherRange) const
	{
		return otherRange.IsWithin(*this);
	}

	/**
	 * Returns true if the Max is less than Min
	 */
	virtual bool IsInverted () const
	{
		return (Max < Min);
	}

	/**
	 * Swaps Min/Max values.
	 */
	virtual void Invert ()
	{
		const T oldMin = Min;
		Min = Max;
		Max = oldMin;
	}

	/**
	 * Ensures the Min is at most the Max.
	 */
	virtual void FixRange ()
	{
		if (IsInverted())
		{
			Invert();
		}
	}

	/**
	 * Returns true if this range encapsulates the given value. (Evaluates:  Min <= targetValue <= Max)
	 */
	virtual bool ContainsValue (const T targetValue) const
	{
		return (Min <= targetValue && Max >= targetValue);
	}

	/**
	 * Returns the percent the input resides within the range. If the input is 15 for the range 10-30, it'll return 0.25.
	 */
	virtual Float CalcAlpha (T input) const
	{
		return Float(input - Min) / Float(Max - Min);
	}

	/**
	 * Returns the difference between the min and max values.
	 */
	virtual T Difference () const
	{
		return (Max - Min);
	}

	/**
	 * Returns the average of Min and Max.
	 */
	virtual T Center () const
	{
		return (Max + Min)/2;
	}

	/**
	 * Clamps the value to be within this range's bounds.
	 */
	virtual T GetClampedValue (const T input) const
	{
		return Utils::Clamp<T>(input, Min, Max);
	}
};
SD_END