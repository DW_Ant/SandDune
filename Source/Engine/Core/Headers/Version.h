/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Version.h
  Defines the version number may be used to help identify what version Sand Dune is running in.

  The version number should reflect the version number of Sand Dune's master branch.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class CORE_API Version
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The major version number increments every time there's a new major release in the master branch. */
	static const Int SAND_DUNE_MAJOR_VERSION;

	/* The minor version number increments for every hot fix released within a major version. */
	static const Int SAND_DUNE_MINOR_VERSION;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Generates a formatted string of Sand Dune's version.
	 * Format:  "Major.Minor"
	 */
	static DString GetVersionNumber ();
};
SD_END