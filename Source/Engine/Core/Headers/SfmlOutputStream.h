/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SfmlOutputStream.h
  A scoped object that'll redirect SFML's output stream to a local stream that then can
  be used to read from.

  It'll then restore SFML's stream after destruction. Only one instance of SfmlOutputStream can
  exist at the same time.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"

SD_BEGIN
class CORE_API SfmlOutputStream
{

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	std::ostringstream SfmlOutput;
	std::streambuf* OriginalSfmlBuffer;

private:
	static bool OutputActive;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SfmlOutputStream ();
	virtual ~SfmlOutputStream ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString ReadOutput () const;
};
SD_END