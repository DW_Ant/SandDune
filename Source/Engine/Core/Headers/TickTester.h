/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TickTester.h
  Debugging Entity used to ensure its various TickComponents ticks as expected.
=====================================================================
*/

#pragma once

#include "DPointer.h"
#include "Entity.h"
#include "Int.h"
#include "SDFloat.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API TickTester : public Entity
{
	DECLARE_CLASS(TickTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	UnitTester::EUnitTestFlags TestFlags;
	DPointer<const UnitTester> OwningTester;

	/* When all TickCounters meets or exceeds this value, then the UnitTest completes, and the entity destroys itself. */
	Int MaxTickCounter;

protected:
	/* Becomes true if the Misc TickGroup should tick before the Debug TickGroup. */
	bool bMiscTickFirst;

	/* Various counters that increments on each tick. */
	Int MiscTickCounter1;
	Int MiscTickCounter2;
	Int DebugTickCounter1;
	Int DebugTickCounter2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void BeginTest ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the tick counters to ensure the tick order is working as expected.
	 * This conditionally terminates the test if a failure was detected, or if all conditions passed.
	 */
	virtual void ValidateTickTest ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMiscTick1 (Float deltaSec);
	virtual void HandleMiscTick2 (Float deltaSec);
	virtual void HandleDebugTick1 (Float deltaSec);
	virtual void HandleDebugTick2 (Float deltaSec);
	virtual void HandleDisabledTick (Float deltaSec);
};
SD_END

#endif //debug_mode