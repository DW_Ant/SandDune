/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjectIterator.h
  The ObjectIterator is an utility class that makes it easy to iterate
  through all objects that are registered to the engine.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DPointer.h"

SD_BEGIN
class Engine;
class Object;

class CORE_API ObjectIterator
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The object this iterator is currently pointing at. */
	DPointer<Object> SelectedObject;

	/* If true, then the Object iterator will move down the hash table whenever it reached to the end of the linked list.
		Set this to true if you desire to iterate through all subsequent objects following the given hash number (ie:  iterate all entities and entity components). */
	bool bMultiHashIDs;

protected:
	/* Informs the iterator which objects to iterate through.  It only iterates through objects with matching hash values. */
	unsigned int TargetedHash;

	/* The index value of the ObjectHashTable the SelectedObject currently resides in. */
	unsigned int ObjTableIdx;

private:
	Engine* LocalEngine;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Iterates through all objects.
	 */
	ObjectIterator ();

	/**
	 * Only iterates through objects with matching hash values.
	 */
	ObjectIterator (unsigned int targetedHash, bool bUseMultiHashIDs = false);

	virtual ~ObjectIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator++ (); //++iter
	void operator++ (int); //iter++


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual unsigned int GetTargetedHash () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Assigns the SelectedObject to the first entry in the hash table.
	 */
	virtual void FindFirstObject ();

	/**
	 * Calculates the index that the TargetHashTable value corresponds to.
	 */
	virtual unsigned int CalcTableIdx () const;

	/**
	 * Redirects SelectedObject to the next object in the linked list.  If none found, then it'll iterate
	 * down the object hash table.  note:  This is not virtual since this is called from constructor.
	 */
	void FindNextObject ();
};
SD_END