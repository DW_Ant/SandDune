/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DatatypeUnitTester.h
  This class is responsible for conducting unit tests for the custom
  data types such as Vectors and Ints.
=====================================================================
*/

#pragma once

#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class Rotator;

class CORE_API DatatypeUnitTester : public UnitTester
{
	DECLARE_CLASS(DatatypeUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestDString (EUnitTestFlags testFlags) const;
	virtual bool TestDataBuffer (EUnitTestFlags testFlags) const;
	virtual bool TestStringIterator (EUnitTestFlags testFlags) const;
	virtual bool TestCharacterEncoding (EUnitTestFlags testFlags) const;
	virtual bool TestBool (EUnitTestFlags testFlags) const;
	virtual bool TestInt (EUnitTestFlags testFlags) const;
	virtual bool TestFloat (EUnitTestFlags testFlags) const;
	virtual bool TestHashedString (EUnitTestFlags testFlags) const;
	virtual bool TestDPointer (EUnitTestFlags testFlags) const;
	virtual bool TestRange (EUnitTestFlags testFlags) const;
	virtual bool TestVector2 (EUnitTestFlags testFlags) const;
	virtual bool TestVector3 (EUnitTestFlags testFlags) const;
	virtual bool TestRotator (EUnitTestFlags testFlags) const;
	virtual bool TestRectangle (EUnitTestFlags testFlags) const;
	virtual bool TestSDFunction (EUnitTestFlags testFlags) const;
	virtual bool TestResourceTree (UnitTester::EUnitTestFlags testFlags) const;
	virtual bool TestMatrix (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode