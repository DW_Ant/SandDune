/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DString.h
  Short for Dune String, this class interfaces with string conversions,
  and contains string-related utility functions.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "Configuration.h"

#include "DProperty.h"
#include "Int.h"
#include <SFML\System.hpp>

#ifndef _TCHAR_DEFINED
	//Define TCHAR for non-Windows platforms
	#if USE_WIDE_STRINGS
		//Note:  Not all compilers understand wchar_t; you may have to define what wchar_t is.
		typedef wchar_t TCHAR;
	#else
		typedef char TCHAR;
	#endif

	#define _TCHAR_DEFINED
#endif

/**
 * Although C++20 introduced char8_t, char16_t, and char32_t, Sand Dune will still use primitive types for its characters for the following reasons:
 * UTF-8 is already implemented for DStrings. I haven't tested std's unicode implementation in conjunction with Sand Dune's types. For example, would char8_t conflict with StringIterators since string iterators could jump a few characters based on the encoding.
 * I also would want defined behavior for conversions between char* and char8_t*. Although it doesn't quite make sense to translate encoded characters to ansi, the conversion between the types is desirable especially when writing to DataBuffers.
 * In addition to that, the standard library is lacking several utilities for std::strings with char8_t as its type. For example, unicode strings does not interface with standard output streams.
 * See: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1423r2.html
 */
typedef unsigned int UTF32Char;
typedef unsigned short UTF16Char;
typedef char UTF8Char; //Leaving this signed so that the debugger will evaluate to characters instead of numbers.

typedef std::basic_string<UTF32Char> StringUTF32;
typedef std::basic_string<UTF16Char> StringUTF16;
typedef std::basic_string<UTF8Char> StringUTF8;

/*
Define various macros that's based on the string configuration:
TString string type that is used to define what kind of std::basic_string's templated arguments is used for the DString::String variable.
TStringChar Smallest possible size a character may be.  For multi-byte character encodings (UTF-8 and UTF-16), it could take multiple TStringChars to represent a single character.
TXT Prefix to insert prior to a character literal.  For example:  Log(TXT("This is a UTF-x encoded literal inside a LOG macro."));
C++20 update: The TXT macro now has a reinterpret_cast to allow unicode assignment to DStrings. DStrings still use std::strings due to reasons described above (see: comment for UTF32Char, UTF16Char, and UTF8Char).
STRING_CONFIG_NAME Simple define that defines another alias for the string configuration.  Used primarily for functions/variables to output the string configuration name.
*/
#if USE_WIDE_STRINGS
	typedef std::wstring TString;
	typedef TCHAR TStringChar;
	#define TXT(msg) L##msg
	#define STRING_CONFIG_NAME TXT("Wide")
#elif USE_UTF32
	typedef StringUTF32 TString;
	typedef UTF32Char TStringChar;
	#define TXT(msg) reinterpret_cast<const unsigned int*>(U##msg##)
	#define STRING_CONFIG_NAME TXT("UTF-32")
#elif USE_UTF16
	typedef StringUTF16 TString;
	typedef UTF16Char TStringChar;
	#define TXT(msg) reinterpret_cast<const unsigned short*>(u##msg##)
	#define STRING_CONFIG_NAME TXT("UTF-16")
#elif USE_UTF8
	typedef StringUTF8 TString;
	typedef UTF8Char TStringChar;
	#define TXT(msg) reinterpret_cast<const char*>(u8##msg##)
	#define STRING_CONFIG_NAME TXT("UTF-8")
#else
	typedef std::string TString;
	typedef TCHAR TStringChar;
	#define TXT(msg) msg
	#define STRING_CONFIG_NAME TXT("ANSI")
#endif

//When true, then the string's length will be cached.  DStrings will take extra 32/64-bits to store this info, but Length() can be accessed at constant time after computing it for the first time.
#define USE_CACHED_LENGTHS USE_UTF16 || USE_UTF8

SD_BEGIN
class CORE_API DString : public DProperty
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ECaseComparison
	{
		CC_CaseSensitive,
		CC_IgnoreCase
	};

	enum ESearchDirection
	{
		SD_LeftToRight,
		SD_RightToLeft
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* A valid string variable with no characters in it. */
	static const DString EmptyString;

protected:
	/* Contains the contents of this DString.  Based on configuration, this string may be encoded in Unicode or in Ansi.  See the typedef of TString for details. */
	TString String;

#if USE_CACHED_LENGTHS
	/* Since DString::Length() cannot be found in constant time since the string configuration is multi-byte.
	   The string's Length is cached every time DString::Length() is called.  This variable is the number of characters within DString.
	   This variable becomes negative if the string's contents changed since last time Length was called.
	   This variable does not exist for uniform sized characters. */
	mutable Int NumCharacters;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DString ();
	DString (const TStringChar* inText);
	DString (const TStringChar inText);
	DString (const DString& inString);
	DString (const TString& inString);

#if !USE_UTF32
	explicit DString (const StringUTF32& inString);
#endif

#if !USE_UTF16
	explicit DString (const StringUTF16& inString);
#endif

#if !USE_UTF8
	explicit DString (const StringUTF8& inString);
#endif

	virtual ~DString ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const DString& inString);
	virtual void operator= (const TStringChar* inStr);

	TStringChar& operator[] (size_t idx);
	const TStringChar& operator[] (size_t idx) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	/**
	 * Generates a TCHAR array from this string.  This function ignores unicode encoding.
	 */
	virtual const TCHAR* ToCString () const;

	/**
	 * Translates the character encoding to SFML's expected character encoding, and returns the result.
	 */
	sf::String ToSfmlString () const;

	/**
	 * Returns a copy of this string that's encoded in UTF-32.
	 */
	StringUTF32 ToUTF32 () const;

	/**
	 * Returns a copy of this string that's encoded in UTF-16.
	 */
	StringUTF16 ToUTF16 () const;

#ifdef PLATFORM_WINDOWS
	/**
	 * Returns a copy of this string as a wide string encoded in UTF-16.  Typically this wide string is good for interfacing with WinAPI functions.
	 */
	std::wstring ToWideStringInUTF16 () const;
#endif

	/**
	 * Returns a copy of this string that's encoded in UTF-8.
	 */
	TString ToUTF8 () const;

	/**
	 * Returns true if this string is correctly encoded in the current UTF configuration.
	 * For ANSI strings, this function always return true.
	 */
	virtual bool IsValidEncoding () const;

	/**
	 * Returns TStringChar at specified index.
	 * WARNING:  This function may return the wrong character if the string is using unicode.
	 * However, the complexity of this function is constant instead of linear.  To consider Unicode characters, use FindAt instead.
	 */
	virtual const TStringChar& At (Int idx) const;

	/**
	 * Returns the character at the specified index.  Contrast to the At function, this function considers Unicode characters.
	 * This function will step through the string until it's at the correct index.
	 * The complexity of this function is linear if the string is encoded in UTF-8 or UTF-16.  For ANSI strings and UTF-32, the complexity of this function is constant.
	 */
	virtual DString FindAt (Int idx) const;

	/**
	 * Returns true if the text is either 1, true, or Yes.
	 */
	virtual bool ToBool () const;

	/**
	 * A wrapper to std's atoi function.
	 * This function returns an integer from this string. It doesn't return the ascii equivalent.
	 * Example: "4" returns 4 instead of 52.
	 * Uses atoll for 64 bit platforms.
	 *
	 * See std::atoi documentation for a detailed description.
	 */
	virtual Int Atoi () const;

	/**
	 * A wrapper to std::stof. See stof documentation for a detailed description.
	 * This function returns a float from this string.
	 */
	virtual Float Stof () const;

	/**
	 * Compares this string to the other string.  Returns 0, if the strings are identical.
	 * Returns negative if the other string is larger than this string.
	 * If ignoring case comparison, then this function complexity increases (by n) since it will internally create a copy of the string.
	 */
	virtual Int Compare (const DString& otherString, ECaseComparison caseComparison) const;

	/**
	 * Searches for matching text within string.  Starts search from startPos.
	 * Returns index position of the first character of the search string.  Returns negative if not found.
	 * Complexity of this function is linear times the length of the search string.
	 * @Param search:  The string to search for.  The whole string must match within this string for this to return a positive number.
	 * @Param startPos:  Position to begin searching from.  This is index 0 for left-to-right searches and Length-1 index position for right-to-left searches.
	 * @Param caseComparison:  Determines if this function ignores case sensitivity or not.  Ignoring Case comparison increases function complexity since it would internally create string copies.
	 * @Param searchDirection:  Determines which direction the string should search from startPos.  This will still return the index position of the first character in search string.
	 */
	virtual Int Find (const DString& search, Int startPos, ECaseComparison caseComparison, ESearchDirection searchDirection = SD_LeftToRight) const;

	/**
	 * Same as Find but it only searches for a single character.
	 * NOTE: This function cannot find characters of size greater than one byte due to the size of TStringChar. If there's a need to search for unicode characters,
	 * use the Find(DString) overload instead.
	 */
	virtual Int Find (TStringChar search, Int startPos, ESearchDirection searchDirection = SD_LeftToRight) const;

	/**
	 * Returns true if this string contains the given substring.
	 * Complexity of this function is linear times the length of the search string.
	 * @param search:  The string to search for. The whole string must match within this string for this to return true.
	 * @param caseComparison:  Determines if this function ignores case sensitivity or not. Ignoring case comparison increases function complexity since it would internally create string copies.
	 */
	virtual bool Contains (const DString& subStr, ECaseComparison caseComparison) const;

	/**
	 * Returns true if this string starts with the given characters.
	 * Complexity is the length of the expectedText.
	 */
	virtual bool StartsWith (const DString& expectedText, ECaseComparison caseComparison) const;
	virtual bool StartsWith (TStringChar expectedChar) const;

	/**
	 * Returns true if this string ends with the given characters.
	 * Complexity is the length of the expectedText.
	 */
	virtual bool EndsWith (const DString& expectedText, ECaseComparison caseComparison) const;
	virtual bool EndsWith (TStringChar expectedChar) const;

	/**
	 * Efficiently removes the last character from this string.
	 * Complexity of this function is constant.
	 */
	virtual void PopBack ();

	/**
	 * Appends the character string to this string.
	 * Same as the operator+=
	 * Explicitly defined since templated functions in this header don't have access to += without changing the ordering of the overrides.
	 */
	virtual void AppendChars (const TStringChar* newChars);

	/**
	 * Returns text between start and end indices.
	 * If endIdx is less than startIdx, then the rest of the string after startIdx is returned.
	 * The startIdx and endIdx is inclusive.
	 */
	static void SubString (DString& outResult, Int startIdx, Int endIdx = -1);
	virtual DString SubString (Int startIdx, Int endIdx = -1) const;

	/**
	 * Returns text from start index, and the number of characters following start index.
	 * If count is negative, then it'll return the remaining string from startIdx.
	 */
	static void SubStringCount (DString& outResult, Int startIdx, Int count = -1);
	virtual DString SubStringCount (Int startIdx, Int count = -1) const;

	/**
	 * Returns the first set of characters from this string (up to count).
	 */
	static void Left (DString& outResult, Int count);
	virtual DString Left (Int count) const;

	/**
	 * Returns the last set of characters from this string (up to count).
	 */
	static void Right (DString& outResult, Int count);
	virtual DString Right (Int count) const;

	/**
	 * The first string is populated by the text content before the selected index.
	 * The second string is populated by the text content after the selected index.
	 * The character splitting the string is not included in either string.
	 */
	virtual void SplitString (Int splitIndex, DString& outFirstSegment, DString& outSecondSegment) const;

	/**
	 * Splits the string into segments for each delimiter found within the string.
	 * @param delimiter the character that'll determine where in the string should be divided into segments.
	 * @param outSegments the list of strings that sum up to this string.  The segments do not include the delimiter.
	 * @param removeEmpty If true, then any empty segments are removed from the segment list.
	 */
	virtual void ParseString (TCHAR delimiter, std::vector<DString>& outSegments, bool removeEmpty) const;

	/**
	 * Returns true if the character at the specified index is surrounded by the specified character.
	 * For example, if checking for double quotes in the following string:  Some "string" value with quotes.
	 * Index 2 'm' returns false, index 7 't' returns true, index 12 '"' returns false, index 25 'q' returns false.
	 * Warning:  This function does not consider wrapped by Unicode characters.  Be sure to use ANSI characters for the wrappingChar.
	 */
	virtual bool IsWrappedByChar (const TCHAR wrappingChar, Int charIdxToCheck) const;

	/**
	 * Returns the number of characters within this string.
	 * This is similar to calling std::string::length(), but this returns the correct number of characters based on this string's character encoding.
	 * Complexity of this function is either linear or constant.  For UTF-32 or ANSI configuration, the complexity of this function is constant.
	 * The Complexity of this function for UTF-16 and UTF-8 configuration is linear the first time this is called.  Afterwards, the length is cached, and
	 * the length can be accessed at constant time until the string's contents changed.
	 */
	virtual Int Length () const;

	/**
	 * Generates a distinct number based on the contents of this string. The number is not guaranteed to be unique, but it's a good number to use as a key for a map.
	 */
	virtual size_t GenerateHash () const;

	/**
	 * Flags the internal NumCharacters to be dirty, which would notify the DString::Length() to recompute its number of characters next time Length() is called.
	 * This function should be called whenever the String's internal string variable was modified.
	 * This is only applicable if USE_CACHED_LENGTHS is true.  Otherwise, this function does nothing.
	 */
	void MarkNumCharactersDirty ();

	/**
	 * Returns the number of characters for the given UTF-32 string.
	 */
	static Int UTF32Length (const StringUTF32& utf32String);

	/**
	 * Returns the number of characters for the given UTF-16 string.
	 */
	static Int UTF16Length (const StringUTF16& utf16String);

	/**
	 * Returns the number of characters for the given UTF-8 string.
	 */
	static Int UTF8Length (const StringUTF8& utf8String);

	/**
	 * Returns true if this string does not contain any text.
	 */
	virtual bool IsEmpty () const;

	/**
	 * Removes all text content from string.
	 */
	virtual void Clear ();

	/**
	 * Inserts a character at the specified index.
	 */
	virtual void Insert (Int idx, const TStringChar character);

	/**
	 * Inserts a string within this string where the first letter of the
	 * inserted string is found at the specified index.
	 */
	virtual void Insert (Int idx, const DString& text);

	/**
	 * Removes character(s) starting from specified index up to count.
	 * @Param idx:  Character index position at which the first character is removed.
	 * @Param count:  Determines the number of characters to be removed including the character at idx. If count is not positive, it'll remove all characters from idx to the end of the string.
	 * Complexity of this function is generally linear.
	 */
	virtual void Remove (Int idx, Int count = 1);

	/**
	 * Replaces all found instances of searchFor with ReplaceWith
	 */
	virtual void ReplaceInline (const DString& searchFor, const DString& replaceWith, ECaseComparison caseComparison);
	static DString Replace (const DString& searchIn, const DString& searchFor, const DString& replaceWith, ECaseComparison caseComparison);

	/**
	 * Similar to FormatString (template function), this finds an instance of '%s', and replaces it with the value of the next entry from the vector.
	 * For compile-time list assembly, use the FormatString overload function that uses an initializer_list.
	 */
	static DString FormatString (DString text, const std::vector<DString>& values);

	/**
	 * Converts all characters to upper-case letters.
	 */
	virtual void ToUpper ();

	/**
	 * Converts all characters to lower-case letters.
	 */
	virtual void ToLower ();

	/**
	 * Removes all white spaces from the beginning and end of the string.
	 */
	virtual void TrimSpaces ();

	/**
	 * Searches this string until it finds the first character that does not match with specified string.  Returns the index when the strings do not match.
	 * This function will return INT_INDEX_NONE if the strings are identical, or the repeating compare string aligns with this string to the end.
	 * Searching "Sand Dune" within "Sand Pit" should return 5.
	 * Searching "DString within "String should return 0.
	 * Searching "Entity" within "EntityComponent" should return 6.
	 * Searching "Engine" within "Engine" should return INT_INDEX_NONE.
	 * Searching "Something " within "Something Something Dark Side" should return 20 (since the search string loops).
	 * Searching " " or "  " within "Trailing Spaces  " should return 14.
	 * @Param compare:  The string phrase to look for.  When iterating through the string, the first character mismatch found will have its index returned.
	 * If the compare string is smaller than this string, then the compare string will repeat back to the beginning.  For example:  searching "Yo" within "Yoyo" (case insensitive) will return INT_INDEX_NONE.
	 * @Param caseComparison:  Determines if the search should ignore case comparison or not.  If ignoring cases, the performance of this function is reduced since it'll create copies of 2 strings.
	 * @Param searchDirection: Determines whether this function should search left to right or right to left.  When searching from right to left, the string comparisons are in reversed order.
	 * For example:  searching for "na" within "banana" from right to left will return 0.
	 * The complexity of this function is linear.
	 */
	virtual Int FindFirstMismatchingIdx (const DString& compare, ECaseComparison caseComparison, ESearchDirection searchDirection = SD_LeftToRight) const;

#pragma region "Regex Functions"
	/**
	 * Returns true if the given regex pattern finds at least one match in this string.
	 */
	virtual bool HasRegexMatch (const DString& pattern) const;
#pragma endregion

#pragma region "Conversions"
	std::wstring ToWStr () const;
	std::string ToStr () const;

#ifdef PLATFORM_WINDOWS
	//Windows-specific conversions
	//Use this macro to get the various windows LP strings (the LPCWSTR gets cleared whenever the string goes off scope).
	//Strings would expire when doing:
	//LPCSTR myVar((DString + DString).ToCString());
	//LPCSTR myVar(TXT("Text"));
	//	return myVar; //expires after returning
	//For details see:  http://stackoverflow.com/questions/16559641/c-converting-between-string-and-lpcwstr
#define StrToLPCWSTR(source) source.ToWStr().c_str()
#define StrToLPCSTR(source) source.ToStr().c_str()
#endif

	/**
	 * Converts a std::wstring to a DString based on this platform's configuration.
	 * FromWideString is created instead of using an explicit constructor to prevent ambiguous constructors (eg:  sf::String).
	 */
	virtual void FromWideString (const std::wstring& inString);
#pragma endregion


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Read-only access to the internal string array.  To edit the string use the = operators or EditString().
	 */
	inline const TString& ReadString () const
	{
		return String;
	}

	/**
	 * Direct access to the internal string array.
	 * After editing the string, make sure to call MarkNumCharactersDirty so the string will report the correct Length.
	 */
	inline TString& EditString ()
	{
		return String;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	bool IsValidUTF32String () const;
	bool IsValidUTF16String () const;
	bool IsValidUTF8String () const;

	/**
	 * Compares character idx within the two strings.  It may compare more than one element for characters that take up more than one element.
	 * Returns true if all bytes within numElements match up.
	 * This function expects the two strings to be using the same character sizes. Recommend using StringIterators to check the character sizes before running this function.
	 */
	static bool DoCharactersMatch (const DString& a, Int aIdx, const DString& b, Int bIdx, Int numElements);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Generates a string from standard cpp data type.
	 */
	template <class Type>
	static inline DString MakeString (Type value)
	{
		DString newString;
		MakeString(value, newString);

		return newString;
	}

	template <class Type>
	static inline void MakeString (Type value, DString& outString)
	{
#if USE_WIDE_STRINGS
		outString = std::to_wstring(value);
#else
		outString = std::to_string(value);
#endif
	}

#ifdef DEBUG_MODE
	/**
	 * Lists out the contents of the specified vector within a DString line.
	 */
	template <class Type> static DString ListToString (const std::vector<Type>& list)
	{
		DString result = TXT("(");
		for (size_t i = 0; i < list.size(); i++)
		{
			if (i != 0)
			{
				result.AppendChars(TXT(","));
			}

			result += list.at(i).ToString();
		}

		result.AppendChars(TXT(")"));
		return result;
	}
#endif

	/**
	 * Generates a string where "%s" text is replaced by the value of the specified parameters.
	 * Complexity of this function is linear, and it creates a copy of input before replacing percent macros.
	 * @param input - Original string input with % macros that are expected to be replaced with param values.
	 * @param params - Template arguments whose values will be inserted within the copy of input where %s macros are found.
	 * @tparam Types - Variadic template arguments where each argument is expected to implement a ToString() method that returns a DString object.
	 * Returns a modified copy of input where its %s macros are replaced with the values of template arguments.
	 */
	template <class ... Types>
	static DString CreateFormattedString (const DString& input, Types ... params)
	{
		DString output = input;
		Format_Implementation(output, 0, params ... );

		return output;
	}

	/**
	 * Modifies output where "%s" text is replaced by the value of the specified parameters.
	 * Complexity of this function is linear.
	 * @param output - String with % macros that are expected to be replaced with param values.
	 * @param params - Template arguments whose values will be inserted within the copy of input where %s macros are found.
	 * @tparam Types - Variadic template arguments where each argument is expected to implement a ToString() method that returns a DString object.
	 * Returns a modified copy of input where its %s macros are replaced with the values of template arguments.
	 */
	template <class ... Types>
	static void FormatString (OUT DString& output, Types ... params)
	{
		Format_Implementation(output, 0, params ... );
	}

private:
	/**
	 * Modifies output where "%s" text is replaced by the value of the specified parameters.
	 * Complexity of this function is linear.
	 * @param output - String with % macros that are expected to be replaced with param values.
	 * @param startSearchIdx - Character index in which to start searching for %s macros.
	 * @param params - Template arguments whose values will be inserted within the copy of input where %s macros are found.
	 * @tparam Types - Variadic template arguments where each argument is expected to implement a ToString() method that returns a DString object.
	 * Returns a modified copy of input where its %s macros are replaced with the values of template arguments.
	 */
	template <class T, class ... Types>
	static void Format_Implementation (OUT DString& output, Int startSearchIdx, T param, Types ... params)
	{
		Int curIdx = output.Find(TXT("%s"), startSearchIdx, CC_CaseSensitive, SD_LeftToRight);

		if (curIdx != INT_INDEX_NONE)
		{
			output.Remove(curIdx, 2); //Remove the '%s'

			DString insertContent = param.ToString();
			output.Insert(curIdx, insertContent);
			Format_Implementation(output, curIdx + insertContent.Length(), params ... );
		}
	}

	/**
	 * Base case for the DString::Format_Implementation function.  Since there aren't any parameters to insert into
	 * output, this function essentially does nothing.  Remaining '%s' macros will remain in output.
	 */
	static void Format_Implementation (OUT DString& output, Int startSearchIdx)
	{
		//NOOP
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const DString& left, const DString& right);
	CORE_API bool operator== (const DString& left, const TString& right);
	CORE_API bool operator== (const TString& left, const DString& right);
	CORE_API bool operator== (const DString& left, const TStringChar* right);
	CORE_API bool operator== (const TStringChar* left, const DString& right);

	//Can't use operator~= for case insensitive compare
	CORE_API bool operator!= (const DString& left, const DString& right);
	CORE_API bool operator!= (const DString& left, const TString& right);
	CORE_API bool operator!= (const TString& left, const DString& right);
	CORE_API bool operator!= (const DString& left, const TStringChar* right);
	CORE_API bool operator!= (const TStringChar* left, const DString& right);

	CORE_API DString operator+ (const DString& left, const DString& right);
	CORE_API DString operator+ (const DString& left, const TString& right);
	CORE_API DString operator+ (const TString& left, const DString& right);

	/*It's not recommended to use these operations since multi-byte unicode characters sometimes doesn't fit within a single TStringChar.
	For example, UTF-8 characters could range from 1-4 bytes, but the TStringChar is only 1 byte.  You should use DString+DString operations instead.*/
	CORE_API DString operator+ (const DString& left, const TStringChar* right);
	CORE_API DString operator+ (const TStringChar* left, const DString& right);

	CORE_API DString& operator+= (DString& left, const DString& right);
	CORE_API DString& operator+= (DString& left, const TString& right);
	CORE_API TString& operator+= (TString& left, const DString& right);
	CORE_API DString& operator+= (DString& left, const TStringChar* right);
#pragma endregion
SD_END