/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Bool.h
  A class that represents a simple bool containing various utility
  functions and conversions.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DProperty.h"

SD_BEGIN
class Int;
class Float;

class CORE_API Bool : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	bool Value;

protected:
	/* Cached language this boolean was translated for (no need to compute Bool to DString every time). */
	static Int LanguageID; //id equivalent to the ELanguages enum
	static DString ToTrueText;
	static DString ToFalseText;

	/* Mutex that locks static localization-related variables such as LanguageID and ToTrueText to make the ToString method thread safe. */
	static std::mutex LocalizationMutex;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Bool ();
	Bool (const bool newValue);
	Bool (const Bool& copyBool);
	explicit Bool (const Int& intValue);
	explicit Bool (const Float& floatValue);
	explicit Bool (const DString& stringValue);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const Bool& copyBool);
	virtual void operator= (const bool otherBool);
	virtual operator bool () const;
	virtual bool operator ! () const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Static method for the GetMinBytes virtual function.
	 * Returns the minimum number of bytes required to read for this variable.
	 * It's important this name is consistent with other SGetMinBytes functions in order to interface with templated array methods.
	 */
	static size_t SGetMinBytes ();

	virtual Float ToFloat () const;
	virtual Int ToInt () const;
};

#pragma region "External Operators"
	CORE_API bool operator== (const Bool& left, const Bool& right);
	CORE_API bool operator== (const Bool& left, const bool& right);
	CORE_API bool operator== (const bool& left, const Bool& right);

	CORE_API bool operator!= (const Bool& left, const Bool& right);
	CORE_API bool operator!= (const Bool& left, const bool& right);
	CORE_API bool operator!= (const bool& left, const Bool& right);
#pragma endregion
SD_END