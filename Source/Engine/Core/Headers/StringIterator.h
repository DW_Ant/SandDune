/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  StringIterator.h
  The StringIterator is a bidirectional iterator that can iterate through the string's characters while
  considering the string's current Unicode configuration.

  For UTF-8 strings, the size of a single string character ranges from 1-4 bytes.  This iterator will jump to the
  next code point based on the character size of the current character this iterator points at.

  This iterator is based on the iterator described here:  http://www.nubaria.com/en/blog/?p=371
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"

SD_BEGIN

//See Section 5 from http://www.nubaria.com/en/blog/?p=371 for explanation for parent class template arguments.
class CORE_API StringIterator : public std::iterator<std::bidirectional_iterator_tag, char32_t, std::string::difference_type, const char32_t*, const char32_t&>
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The std string iterator that actually points to the string's raw bytes. */
	std::string::const_iterator BaseIterator;

	/* The byte data of the current character. */
#if USE_UTF32 || USE_UTF16 || USE_UTF8
	mutable char CurrentCharacter[4];
#else
	mutable char CurrentCharacter[1];
#endif

	/* Identifies the size of the current character (in bytes).  This value ranges from 0-4. */
	mutable unsigned char SizeOfCharacter;

	/* If true, then this iterator will need to recalculate the current character data.  Otherwise, it's assumed that the CurrentCharacter variable is up to date. */
	mutable bool bDirty;

	/* String this iterator is cycling through.  This is primarily used to determine the beginning of the string to ensure decrementing cannot decrement beyond the string's start. */
	mutable const DString* String;

	/* This is the starting byte index of the current character relative to the string's beginning of the byte array.
	This is not the same as the number of characters the iterator is away from the beginning. Unicode characters can take up more than one byte. */
	mutable Int StringCharByteIdx;

private:
	/* Reflects the array size of CurrentCharacter's static array. */
	static const unsigned int CharacterArraySize;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Constructs a StringIterator that will be iterating through the specified string.
	 * If bPointToBeginning is true, then the iterator will begin at inString.begin(), otherwise, the iterator will be pointing at the string's last character.
	 */
	StringIterator (const DString* inString, bool bPointToBeginning = true);
	StringIterator (const StringIterator& other);
	virtual ~StringIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	StringIterator& operator++ (); //++iter
	StringIterator operator++ (int); //iter++
	StringIterator& operator-- (); //--iter
	StringIterator operator-- (int); //iter--

	StringIterator operator+ (Int right) const;
	StringIterator& operator+= (Int right);
	StringIterator operator- (Int right) const;
	StringIterator& operator-= (Int right);

	StringIterator& operator= (const StringIterator& other);
	bool operator== (const StringIterator& other) const;
	bool operator!= (const StringIterator& other) const;
	bool operator== (const std::string::iterator& other) const;
	bool operator!= (const std::string::iterator& other) const;
	bool operator== (const std::string::const_iterator& other) const;
	bool operator!= (const std::string::const_iterator& other) const;

	/**
	 * Retrieves the current character.
	 */
	DString operator* () const;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a DString containing the selected character.
	 */
	DString GetString () const;

	/**
	 * Returns true if this iterator is pointing at the string's beginning (pointing at the very first character).
	 */
	virtual bool IsAtBeginning () const;

	/**
	 * Returns true if this iterator is pointing beyond the string's end (not pointing at valid character).
	 */
	virtual bool IsAtEnd () const;

	/**
	 * Updates the iterator to point to the first character in the string.
	 */
	void JumpToBeginning ();

	/**
	 * Updates the iterator to point to the last character in the string.
	 */
	void JumpToEnd ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Copies the byte array of the current character to the outByteArray.
	 * This function assumes enough memory is allocated for outByteArray.  Call GetSizeOfCharacter to identify how many bytes that array should be.
	 */
	void GetCurrentCharacterData (unsigned char* outByteArray) const;

	inline unsigned char GetSizeOfCharacter () const
	{
		return SizeOfCharacter;
	}

	inline Int GetStringCharByteIdx () const
	{
		return StringCharByteIdx;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads in the string's bytes to identify the full contents of the selected character, and caches the data to CurrentCharacter member variable.
	 */
	virtual void ReadCharacterData () const;

	/**
	 * Reads in the iterator's first byte of information to identify how many bytes the current character is.
	 * Note:  this function is not virtual since this is invoked in constructor.
	 */
	void CalcSizeOfChar () const;
};
SD_END