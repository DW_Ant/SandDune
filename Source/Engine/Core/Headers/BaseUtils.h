/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseUtils.h
  Parent class for all misc utility classes.  They all share a common class
  for easy object iteration, parsing, and documenting.
=====================================================================
*/

#pragma once

#include "Object.h"

SD_BEGIN
class CORE_API BaseUtils : public Object
{
	DECLARE_CLASS(BaseUtils)


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Makes this class abstract.
	 */
	virtual void AbstractClass () = 0;
};
SD_END