/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMac.h
  Converts any used functions into a format Macs may understand.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#ifdef PLATFORM_MAC

#include <unistd.h>


/*
=====================
  defines
=====================
*/

// Get 32/64 bit defines
#if __GNUC__
	#if __x86_64__ || __ppc64__
		#define PLATFORM_64BIT
	#else
		#define PLATFORM_32BIT
	#endif
#else
	#error "Please define your platform."
#endif

#include "Core.h"
#include "CoreMacros.h"
#include "Int.h"
#include "Rectangle.h"

SD_BEGIN


/*
=====================
  "_s" conversions
=====================
*/

int CORE_API strerror_s (char *buffer, size_t numberOfElements, int errnum);


/*
=====================
  Methods
=====================
*/

/**
 * Setup any platform-specific initialization.
 */
void CORE_API InitializePlatform (bool bMainThread);

/**
 * Shutdown any platform-specific functionality.
 * If bMainThread is true, then assume that the application is terminating instead of a thread.
 */
void CORE_API ShutdownPlatform (bool bMainThread);

void CORE_API PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle);

/**
 * Informs the operating system to freeze this this thread for the specified amount.
 * This function does not return until enough time elapsed.
 */
void CORE_API OS_Sleep (Int milliseconds);

/**
 * Copies the copyContent to the OS clipboard.  Returns true if successful.
 */
bool CORE_API OS_CopyToClipboard (const DString& copyContent);

/**
 * Retrieves the text buffer from the OS clipboard, and returns as result as string.
 * Returns an empty string if clipboard is empty, or if application was unable to paste from clipboard.
 */
DString CORE_API OS_PasteFromClipboard ();

/**
 * Sends a signal to allow an IDE to break the program for debugging.
 * Execution should halt if the signal was captured.
 */
void CORE_API OS_BreakExecution ();
SD_END

#endif