/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TransformMatrix.h
  A special kind of Matrix that encompasses a transformation within a 4x4 matrix.

  This class forces the matrix to be 4x4, and it includes various utilities for transform
  manipulation.

  The TransformMatrix defines the scalar and rotation in the top left 9 elements.  The translation
  portion is the last column.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "Matrix.h"

SD_BEGIN
class CORE_API TransformMatrix : public Matrix
{


	/*
	=====================
	  DataType
	=====================
	*/

public:
	/**
	 * Enum that identifies/provides the transform matrix's element with an alias.
	 * Each number of the enumeration maps to the Matrix's data index.
	 */
	enum EMatrixElement
	{
		X = 3,
		Y = 7,
		Z = 11,

		ScaleX = 0,
		ScaleY = 5,
		ScaleZ = 10,
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* A transform matrix that has no translation and no scaling attributes. */
	static const TransformMatrix IDENTITY_TRANSFORM;

private:
	/* Scalar information can be lost when multiplied with other transform matrices with rotations, or when there's negative scaling.
	This variable caches the scalar multipliers. */
	Vector3 ScaleMultipliers;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TransformMatrix ();
	TransformMatrix (const Matrix& copyObj);
	TransformMatrix (const Vector3& inScaleMultipliers, const Matrix& copyObj);
	TransformMatrix (const TransformMatrix& copyObj);
	virtual ~TransformMatrix ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const TransformMatrix& copyMatrix);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;
	virtual void MatrixAdd (const Matrix& addBy) override;
	virtual void MatrixSubtract (const Matrix& subtractBy) override;
	virtual void MatrixMultiply (Float multiplier) override;
	virtual void MatrixMultiply (const Matrix& multiplier) override;

private:
	//Forces the matrix size to be always 4x4 (hides parent call to SetData(vector, Int, Int))
	virtual bool SetData (const std::vector<Float>& newData) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a Transform matrix that can rotate a point about the X, Y, or Z axis.
	 * The given angle is in radians.
	 * Matrices are based on the matrices provided from this pdf: https://www.geometrictools.com/Documentation/EulerAngles.pdf
	 */
	static TransformMatrix GetRotationMatrixAboutXAxis (Float theta);
	static TransformMatrix GetRotationMatrixAboutYAxis (Float theta);
	static TransformMatrix GetRotationMatrixAboutZAxis (Float theta);

	void SetTranslation (const Vector3& newTranslation);
	void Translate (const Vector3& translateOffset);

	void SetScale (const Vector3& newScale);
	void Scale (const Vector3& scaleBy);

	/**
	 * Rotates this matrix by the given Rotator. Assumed order of rotation is Yaw, Pitch, Roll.
	 */
	void Rotate (const Rotator& rotation);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	void CalcTransformAttributes (Vector3& outTranslation, Vector3& outScale, Rotator& outRotation) const;
	Vector3 GetTranslation () const;

	/**
	 * Returns the X,Y,Z scalar axis from this transform.
	 */
	Vector3 GetScale () const;

	/**
	 * Returns Euler angles from this transform matrix. This assumes the matrix was constructed in the
	 * order of Yaw, Pitch, Roll.
	 */
	Rotator GetRotation () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void GetRotationMatrix (TransformMatrix& outRotationMatrix) const;
};

#pragma region "External Operators"
	CORE_API TransformMatrix operator+ (const TransformMatrix& left, const TransformMatrix& right);
	CORE_API TransformMatrix& operator+= (TransformMatrix& left, const TransformMatrix& right);
	CORE_API TransformMatrix operator- (const TransformMatrix& left, const TransformMatrix& right);
	CORE_API TransformMatrix& operator-= (TransformMatrix& left, const TransformMatrix& right);

	CORE_API TransformMatrix operator* (const TransformMatrix& left, Float right);
	CORE_API TransformMatrix operator* (Float left, const TransformMatrix& right);
	CORE_API TransformMatrix operator* (const TransformMatrix& left, const TransformMatrix& right);
	CORE_API TransformMatrix& operator*= (TransformMatrix& left, Float right);
	CORE_API TransformMatrix& operator*= (TransformMatrix& left, const TransformMatrix& right);

	CORE_API TransformMatrix operator/ (const TransformMatrix& left, Float right);
	CORE_API TransformMatrix operator/ (const TransformMatrix& left, const TransformMatrix& right);
	CORE_API TransformMatrix& operator/= (TransformMatrix& left, Float right);
	CORE_API TransformMatrix& operator/= (TransformMatrix& left, const TransformMatrix& right);
#pragma endregion
SD_END