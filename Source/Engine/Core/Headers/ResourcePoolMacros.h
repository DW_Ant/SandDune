/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourcePoolMacros.h
  Defines various macros to make it easier to implement various ResourcePools.
  Using Macros instead of templated classes primarily to utilize Object macros
  such as DECLARE_CLASS() and IMPLEMENT_CLASS().  This will also make it easier
  to implement any additional functions to a particular resource pool should
  there ever be a need to handle unique cases.
=====================================================================
*/

#pragma once

#include "Core.h"

/**
  Declares various functions and variables for a ResourcePool.  The ResourcePool must be named [ResourceType]Pool.
 */
#define DECLARE_RESOURCE_POOL(ResourceType) \
\
/* Properties */ \
\
protected: \
	/* Resource instances.  Just like an Engine Component, each resource instance is placed in its own thread. */ \
	static std::vector<SD::DPointer<##ResourceType##Pool>> ResourceType##PoolInstances; \
\
	/* Mutex used for accessing or modifying the ResourcePoolInstances vector. */ \
	static std::mutex ResourceType##PoolInstancesMutex; \
\
/* Accessors */ \
public: \
	static ResourceType##Pool* Find##ResourceType##Pool (); \
\
/* Implementation */ \
protected: \
	/** \
	 * Registers this resource pool instance to the ResourcePoolInstance vectors. \
	 * This function will refuse if an instance with the same ThreadID is already registered, \
	 * or if this resource pool is already registered. \
	*/ \
	bool RegisterResourcePool (); \
\
	/** \
	 * Removes this resource pool instance from the ResourcePoolInstance vectors. \
	 */ \
	void RemoveResourcePool (); \
/* Restore class access-specifier defaults. */ \
private:

/**
  Implements the functions and variables declared from DECLARE_RESOURCE_POOL
 */
#define IMPLEMENT_RESOURCE_POOL(ResourceType) \
std::vector<SD::DPointer<##ResourceType##Pool>> ResourceType##Pool::ResourceType##PoolInstances; \
std::mutex ResourceType##Pool::ResourceType##PoolInstancesMutex; \
\
ResourceType##Pool* ResourceType##Pool::Find##ResourceType##Pool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	\
	std::thread::id threadID = std::this_thread::get_id(); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{	\
		if (##ResourceType##PoolInstances.at(i)->ThreadID == threadID) \
		{ \
			return ResourceType##PoolInstances.at(i).Get(); \
		} \
	} \
	return nullptr; \
} \
\
bool ResourceType##Pool::RegisterResourcePool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{ \
		/* Ensure this resource pool isn't already registered. */ \
		if (##ResourceType##PoolInstances.at(i).Get() == this) \
		{ \
			return false; \
		} \
		\
		/* Ensure no other resource pools (in the same thread) were already registered. */ \
		if (##ResourceType##PoolInstances.at(i)->ThreadID == ThreadID) \
		{ \
			return false; \
		} \
	} \
	\
	ResourceType##PoolInstances.push_back(this); \
	return true; \
} \
\
void ResourceType##Pool::RemoveResourcePool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{ \
		if (##ResourceType##PoolInstances.at(i).Get() == this) \
		{ \
			ResourceType##PoolInstances.erase(##ResourceType##PoolInstances.begin() + i); \
			return; \
		} \
	} \
}