/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Definitions.h
  This file declares global variables that every executable should define.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"

SD_BEGIN
/* The Project Name is the human-readable string representation for the project. */
extern CORE_API DString ProjectName;
SD_END