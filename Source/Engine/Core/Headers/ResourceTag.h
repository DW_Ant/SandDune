/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResourceTag.h
  A resource tag associated with the number of references where a reference is a single resource using that group.
  A tag is primarily used to help organize and find resources of matching tag.
  For example: for sound assets a singe sound asset could have the following tags "localized" "dialogue" "CharacterName"
  Another example for a texture asset could be: "Environmental" "Ground" "Repeated" "Foliage"
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"
#include "HashedString.h"
#include "Int.h"

SD_BEGIN
class CORE_API ResourceTag
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The category name that is referenced in content files. This is the text that is also displayed in the content browsers.
	If empty, then this Tag is not in use. */
	HashedString TagName;

	/* List of similarly spelled names. For example: "Repeated" vs "Repeat" vs "Wrapped". Or another example: "Gun fire", "gunfire" */
	std::vector<DString> Aliases;

protected:
	/* Number of resources that are referencing this tag. When zero or below, it's safe to remove this tag. */
	Int References;


	/*
	=====================
	  Constructor
	=====================
	*/

public:
	ResourceTag ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the specified tag to the given list of tags. If the tag already exists in the list, it will increment its reference number.
	 * Returns the index number of the tag list where the new tag is referenced or added.
	 */
	static size_t AddTag (const HashedString& tagName, std::vector<ResourceTag>& outTagList);

	/**
	 * Searches for the tag in the given list, and if it's found, it'll decrement its reference counter.
	 * Being that tags are usually index based, it doesn't actually delete the tag, instead it'll only free up its slot.
	 * Returns true if the tag was found and removed from the list.
	 */
	static bool RemoveTag (const HashedString& tagName, std::vector<ResourceTag>& outTagList);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void AddReference ();

	/**
	 * Returns true if there are no more references.
	 */
	bool RemoveReference ();
};
SD_END