/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EngineComponent.h
  The Engine Component contains methods to extend the Engine's functionality.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "LogCategory.h"

SD_BEGIN
class DClass;
class Engine;

class CORE_API EngineComponent
{
	DECLARE_ENGINE_COMPONENT(EngineComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
#ifdef DEBUG_MODE
	enum EEngineIntegrityStep
	{
		EIS_PreInstantiation,
		EIS_PreIteration,
		EIS_PreCleanup
	};
#endif


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Engine this component is registered to. */
	Engine* OwningEngine;

	/* If true, then this EngineComponent will register itself to the Engine's Tick cycle,
	and receive Pre and Post Tick Updates. */
	bool bTickingComponent;


	/*
	=====================
	  Constructors
	=====================
	*/

protected:
	EngineComponent ();
	virtual ~EngineComponent ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Allows engine component to register parts of the Engine's Object Hash Table to group specific objects
	 * for faster object iteration.  This is called prior to any engine component receiving a PreInitializeComponent call.
	 */
	virtual void RegisterObjectHash ();

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute PreInitializeComponent.
	 * @Param:  List of EngineComponents that already executed their PreInitializeComponent method.
	 */
	virtual bool CanRunPreInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute InitializeComponent.
	 * @Param:  List of EngineComponents that already executed their InitializeComponent method.
	 */
	virtual bool CanRunInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute PostInitializeComponent.
	 * @Param:  List of EngineComponents that already executed their PostInitializeComponent method.
	 */
	virtual bool CanRunPostInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Called prior to majority of essential object instantiation.  Resource Pools and DrawLayers
	 * are not guaranteed to be instantiated at this point.  Object Hash values are already
	 * registered, and is safe to instantiate objects and entities.
	 */
	virtual void PreInitializeComponent ();

	/**
	 * Called after essential objects are instantiated.  Resource Pools, draw layers are registered,
	 * and is safe to import resources such as textures and fonts.
	 */
	virtual void InitializeComponent ();

	/**
	 * Called after essential objects and resources are imported.
	 */
	virtual void PostInitializeComponent ();

	/**
	 * Allows EngineComponents to modify the log message prior to EngineComponents from processing the message.
	 * @param category Log classification this message belongs to.
	 * @param logLevel Importance level associated with this message.
	 * @param msg The actual log message, itself.
	 */
	virtual void FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg);

	/**
	 * Notifies EngineComponent of incoming log message to serialize and/or display.
	 * This is invoked after EngineComponents handled their FormatLog methods.
	 * @param category Log classification this message belongs to.
	 * @param logLevel Importance level associated with this message.
	 * @param formattedMsg The actual log message, itself.
	 */
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg);

	/**
	 * Called before the Engine executes its tick cycle.  The render pipeline did not execute yet, and
	 * all TickComponents did not call receive their Tick updates yet.
	 * You must set bTickingComponent to true in the constructor to be able to receive updates.
	 */
	virtual void PreTick (Float deltaSec);

	/**
	 * Called after the Engine cycles through all TickComponents.  It's not guaranteed if the objects are
	 * displayed or not at the time this function is called.
	 * You must set bTickingComponent to true in the constructor to be able to receive updates.
	 */
	virtual void PostTick (Float deltaSec);

	/**
	 * Invoked whenever the engine is about to shutdown.
	 */
	virtual void ShutdownComponent ();

	void SetOwningEngine (Engine* newOwningEngine);

#ifdef DEBUG_MODE
	/**
	 * Allows the EngineComponent to adjust anything during the EngineIntegrity tests. The enum specifies which step in the test is running.
	 * Most EngineComponents should be agnostic to the unit test, but there may be some instances that need to revert some of its state changes from having
	 * the unit test instantiate all classes. Override this function to restore its state prior to the unit test if needed.
	 */
	virtual void ProcessEngineIntegrityTestStep (EEngineIntegrityStep testStep);
#endif


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Engine* GetOwningEngine () const
	{
		return OwningEngine;
	}

	virtual bool GetTickingComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns a list of EngineComponents that must execute their PreInitializeComponent prior
	 * to this component running its PreInitializeComponent.
	 */
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const;

	/**
	 * Returns a list of EngineComponents that must execute their InitializeComponent prior
	 * to this component running its InitializeComponent.
	 */
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const;

	/**
	 * Returns a list of EngineComponents that must execute their PostInitializeComponent prior
	 * to this component running its PostInitializeComponent.
	 */
	virtual void GetPostInitializeDependencies (std::vector<const DClass*>& outDependencies) const;

friend class Engine;
friend class DClass;
};
SD_END