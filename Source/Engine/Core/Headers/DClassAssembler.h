/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DClassAssembler.h
  Before Main runs, countless Objects and EngineComponents will instantiate a DClass
  object.  These DClasses will notify the DClassAssembler about their existance, and
  their expected parent class.

  When Main runs and before the Main Engine is initialized, the DClassAssembler will take all
  registered DClass instances and generate a class tree relationship.  Assertions are thrown
  if there are broken connections.

  Although DClasses can only have one parent classes, the Objects themselves can still
  have multi-inheritance.  However, they can only derive from at most one parent with an associated
  DClass.

  The class tree is visible in all threads.  Only the main thread creates and cleans up the tree.
=====================================================================
*/

#pragma once

#include "Core.h"
#include "DString.h"
#include <unordered_map>

SD_BEGIN
class Object;
class EngineComponent;
class DClass;

class CORE_API DClassAssembler
{


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SDClassInfo
	{
		DClass* ClassInstance;
		DString ParentClassName;

		SDClassInfo (DClass* inClassInstance, const DString& inParentClassName) :
			ClassInstance(inClassInstance),
			ParentClassName(inParentClassName)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Vector tracking registered DClasses at static initialization time. */
	static std::vector<SDClassInfo> PreloadedDClasses;

	/**
	 * Vector containing the exact same data as PreloadedDClasses.  This is a workaround to how static vectors are initialized.
	 * In Microsoft's C++, a vector clears is size when initialized.  However, it could have data (if DClasses are initialized before this vector).
	 * To preserve the initialized data, there's a copy of PreloadedDClasses.  When the PreloadedDClasses are initialized, it simply copies from its copy.
	 */
	static std::vector<SDClassInfo> PreloadedDClassesCopy;

	/* Becomes true when all DClasses have been initialized, and the assembler constructed the class tree. */
	static bool bLinkedDClasses;

	/* A list of all classes without a parent class sorted alphabetically.
	Use this if there's a need to iterate through the class tree (using ClassIterators). */
	static std::vector<DClass*> RootClasses;

	/* List of all DClasses that are accessible through a key. The key is a hash of DClass's ClassName.
	Use this if there's a need to find a class from a DString. */
	static std::unordered_map<size_t, DClass*> AllClasses;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a DClass instance.  The DClass is not yet initialized, but it's registered for initialization
	 * when the Engine initializes.
	 * @param className - Technical (cpp) name of the Object instance. This includes the namespace (eg:  "SD::Object")
	 * @param parentClass - Technical (cpp) name of the parent class that also has a DClass instance.
	 * The parent's DClass's className must match this value to correctly link the two classes.  If empty, then
	 * it's assumed that the DClass instance does not have a parent.
	 * Returns the DClass instance that is not yet initialized.  This instance will be initialized when AssembleDClasses is called.
	 */
	static DClass* LoadClass (const DString& className, const DString& parentClass);

	/**
	 * Takes all registered DClasses, and assembles a class tree.  This also links all DClass' Children and parent pointers.
	 * Returns true if all registered DClasses are linked.
	 */
	static bool AssembleDClasses ();

	/**
	 * Deletes all DClasses.
	 */
	static void DisassembleDClasses ();

	/**
	 * Returns the hash of the given DClass name. The name of the DClass should include its namespace and colons (eg: "SD::Entity").
	 * Returns 0 if there is an error such as the assembler is not yet initialized, or if there are too many collisions.
	 * @param outClass - Pointer to the DClass that is found when generating the hash. Becomes nullptr if there was an error.
	 */
	static size_t CalcDClassHash (const DString& className, const DClass*& outClass);

	/**
	 * Attempts to find a DClass associated with the specified name.
	 * This function will generate a hash from the given string, and use that hash to quickly find the DClass in the map.
	 * It'll then compare the resulting DClass, if the DClass's class name does not match the parameter, it'll assume there was a collision.
	 * It'll then search with a variation of the parameter in the hopes to find find the correct DClass.
	 * @param className - The name of the DClass including its namespace and the colons (eg:  "SD::Entity")
	 */
	static const DClass* FindDClass (const DString& className);
	static const DClass* FindDClass (size_t classNameHash);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns true if the assembler generated the class tree, and all DClasses' relationships are assembled.
	 */
	static bool IsInitialized ();
	static const std::vector<DClass*> GetRootClasses ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the registeredDClass is linked with its parent (if any).
	 */
	static bool HasFoundParent (const SDClassInfo& registeredDClass);

	/**
	 * Returns true if the child DClass info's parent info matches the potential parent DClass info.
	 */
	static bool IsChildOf (const SDClassInfo& childInfo, const SDClassInfo& parentInfo);

	/**
	 * Establish the parent/child relationship between the two DClasses.
	 */
	static void LinkDClasses (SDClassInfo& childInfo, SDClassInfo& parentInfo);

	/**
	 * Adds new class instance to the root classes vector.
	 */
	static void AddRootClass (DClass* newRootClass);

private:
	virtual void AbstractClass () = 0;
};
SD_END