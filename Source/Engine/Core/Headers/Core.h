/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Core.h
  Contains important file includes and definitions for the Core module.

  The Core module includes essential classes and utilities majority of
  other modules require.  The Core module defines the following:

	- Global definitions (ie:  platforms and environment)
	- Commonly used header includes (ie:  string.h, math.h)
	- Essential Macros (DECLARE_CLASS, IMPLEMENT_CLASS)
	- Data types (Ints, Floats, Vectors)
	- Engine, Engine Component, and DClass implementations
	- Primitive classes such as Object, Entity, and Entity Component
=====================================================================
*/

#pragma once

#include "Configuration.h"

#ifndef __cplusplus
#error A C++ compiler is required to compile the Core module.
#endif

#if __cplusplus < 202002L
#error Your compiler must support C++20 to be able to compile the Core module.
#endif

#ifdef _WIN32
//Notify the Engine that we're using a Windows platform (true for both 32 and 64 bit)
#define PLATFORM_WINDOWS
#elif __APPLE__
//Notify the Engine that we're using a Mac platform
#define PLATFORM_MAC
#else
#error Please define your platform here.
#endif

//common includes
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <mutex>
#include <algorithm>
#include <vector>
#include <list>
#include <map>
#include <math.h>
#include <limits>
#include <functional> //C++11 function callbacks
#include <type_traits> //used to check abstract classes

#include <SFML/System.hpp>

//Check Solution Configuration
#ifdef _DEBUG
#define DEBUG_MODE
#endif

//Configuration-specific common includes
#ifdef DEBUG_MODE
#include <cassert>
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef CORE_EXPORT
		#define CORE_API __declspec(dllexport)
	#else
		#define CORE_API __declspec(dllimport)
	#endif
#else
	#define CORE_EXPORT
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindows.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMac.h"
#endif

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

#include "CoreMacros.h"