/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControlMiscSettings.h
  Access control object that denies entry to this process based on various configuration
  settings that typically every process would need to configure.
=====================================================================
*/

#pragma once

#include "AccessControl.h"

SD_BEGIN
class NETWORK_API AccessControlMiscSettings : public AccessControl
{
	DECLARE_CLASS(AccessControlMiscSettings)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Bitwise flags that map to the NetworkSignature::ENetworkRole enum (and potentially other enum types).
	If the network signature with a role that has any of the flags that are set to 0, then this access control
	will deny access. For example, if a signature has role client and server, but the process' server flag is 0,
	the request will be denied even if the process' client flag is 1.

	For centralized networks
	Typically server processes will accept anything except other servers. Clients will not accept any connections.
	
	For peer-to-peer
	Clients will accept other clients. Server types are typically not used. */
	Int AllowedRoles;

	/* Maximum number of connections this process is allowed to have. Typically a game server can specify the maximum number of players.
	If not positive, then there is no restrictions regarding the number of connections. */
	Int MaxConnections;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;
};
SD_END