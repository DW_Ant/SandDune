/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControlIpAddress.h
  Gatekeeper that allows or denies access based on the user's ip address. Most applications
  would have an instance of this object.

  WARNING:
  This access control is not effective against users using VPN. It's strongly encouraged to
  use this access control along with something else (such as blocking by steam accounts).
=====================================================================
*/

#pragma once

#include "AccessControl.h"

SD_BEGIN
class NetworkSignature;

class NETWORK_API AccessControlIpAddress : public AccessControl
{
	DECLARE_CLASS(AccessControlIpAddress)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SIpActivity
	{
		sf::IpAddress Address;

		/* Timestamp when the address last generated a request connection. */
		Float LastRequestTime;

		/* Number of times this address attempted and failed to connect to this process. Resets back to 0 every time enough time elapsed. */
		Int NumFailedAttempts;

		SIpActivity (const sf::IpAddress& inAddress, Float inLastRequestTime, Int inNumFailedAttempts) :
			Address(inAddress),
			LastRequestTime(inLastRequestTime),
			NumFailedAttempts(inNumFailedAttempts)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of IP addresses that are not allowed to connect to this process.
	Asterisks are used as wild card characters. For example: "255.255.255.*" will block all addresses beginning with "255.255.255" */
	std::vector<DString> BlockedIpAddresses;

	/* List of IP addresses that are currently suspended for sending too much information.
	Note: This does not protect from DDOS attacks. This is merely an early catch for brute forced connection requests. */
	std::vector<SIpActivity> SuspendedIps;

	/* Minimum allowable interval permitted before a client may make another attempt to connect (in seconds).
	If a client attempts to connect within this time frame, then their fail attempt will increment.
	If enough attempts were made, then their requests will be ignored until enough time elapses.
	Disabled if not positive. */
	Float MinAttemptInterval;

	/* The number of times a client can fail to connect while not being on the suspended list. When they failed to connect this many times
	within the MinAttemptInterval, their IP Address will be temporarily suspended until MinAttemptInterval expired. */
	Int MaxAttemptsAllowed;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;
	virtual void ConnectionRejected (NetworkSignature* signature, unsigned char rejectReason) override;

protected:
	virtual void LoadConfigSettings (ConfigWriter* config) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the given IP Address is found in this instance's blocked IP Address list.
	 * This supports asterisks as wild characters (eg:  "255.255.255.*" will block anything beginning with "255.255.255")
	 */
	virtual bool IsIpBlocked (const sf::IpAddress& address) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline std::vector<DString> GetBlockedIpAddresses () const
	{
		return BlockedIpAddresses;
	}

	inline const std::vector<DString>& ReadBlockedIpAddresses () const
	{
		return BlockedIpAddresses;
	}

	inline std::vector<DString>& EditBlockedIpAddresses ()
	{
		return BlockedIpAddresses;
	}

	inline Float GetMinAttemptInterval () const
	{
		return MinAttemptInterval;
	}
};
SD_END