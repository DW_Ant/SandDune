/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControl.h
  Gatekeeping object that is responsible for determining when a connection request
  is permitted or not.

  An AccessControl class may check on a variety of aspects when approving requests:
  - Checking a ban list
  - Checking if the server is at capacity
  - Checking if the request is valid
  
  Developers can use add custom AccessControl classes for specific functionality.
  For example, one could integrate with Steam SDK to deny requests based on their Steam accounts.

  WARNING:
  If your application requires your users to pass in a password, do NOT store the password anywhere!
  Only store a encrypted version of the password!
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
class NetworkSignature;

class NETWORK_API AccessControl : public Object
{
	DECLARE_CLASS(AccessControl)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* Various reasons why a connection may be rejected.
	Custom classes may have their own reasons that are not from this enum.
	NOTE: Using multiple access controls with custom enums may conflict. If this is the case,
	either use one of the libraries, or have the client side hande both cases knowing that one reason switch
	could mean two different types of rejections. */
	enum ERejectReason : unsigned char
	{
		RR_None = 0,
		RR_DestNotFound = 1, //The remote process is not found. Possibly invalid ip address, process is not online, or the connection is blocked.
		RR_TimedOut = 2, //The remote process failed to respond within the time limit. Best to assume the connection is either dropped or ignored.
		RR_InvalidType = 3, //The process does not accept the connection to connect with those types. For example, a server may reject other connections that are attempting to connect as a server.
		RR_Banned = 4, //The user does not have permission to connect to this process.
		RR_AtCapacity = 5, //The server is full, and cannot accept any new connections.
		RR_OutOfResource = 6, //The server or client is missing sufficient resources to establish a connection. For example, the client may not have any available TcpSockets.
		RR_Suspended = 7, //The client generated already sent too many requests. It's currently suppressed.
		RR_AlreadyConnected = 8, //There is already a connection between the client and this instance.
		RR_InvalidData = 9, //The client's connection request does not have sufficient data in its request. For example, a GuidAccessControl instance would expect clients to pass in a GUID.

		RR_InternalError = 15, //General catch all for generic internal errors (eg: remote process was unable to insert network signature to connection map).
		RR_Max = 16, //Not a reason. This is just to reserve reasons 9-15 to prevent other enums from different AccessControl instances from clashing nums.
	};

	/* List of Network Roles that this access control will check against. By default, it'll check for every role.
	Setting this to a specific role will cause other roles to bypass this check. For example if this access control role check is set to 
	NR_Server, then any connection requests that don't have that role are not required to meet this AccessControl's requirements. */
	Int RelevantRoles;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * If true, then the given NetworkSignature will be checked against this AccessControl.
	 * Otherwise this NetworkSignature will bypass this control's requirements.
	 */
	virtual bool IsRelevant (NetworkSignature* signature) const;

	/**
	 * Returns true if the specified user is permitted to connect to this process.
	 * outRejectReason is a num that maps to a reject reason enum such as ERejectReason.
	 * Developers can customize a rejection reason using their own enums with values beyond ERejectReason::RR_Max.
	 */
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) = 0;

	/**
	 * Called whenever one of the AccessControl instances rejected this signature. This is a hook to allow
	 * instances to react to a rejection.
	 */
	virtual void ConnectionRejected (NetworkSignature* signature, unsigned char rejectReason);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads various configurable variables from the given config file.
	 */
	virtual void LoadConfigSettings (ConfigWriter* config);
};
SD_END