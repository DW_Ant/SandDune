/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SdRpc.h

  Short for Remote Procedural Call.
  This is a templated object responsible for sending and receiving events to/from a remote processes.

  This can only replicate DProperty params primarily because it interfaces with DataBuffers.
  DPointers are not supported since it doesn't make sense to pass a memory address from a remote process.

  Return values are not permitted primarily because these operations are asynchronous.

  NOTE: This file was originally named Rpc.h, but one of Window's header files is named rpc.h. This caused
  Windows.h to include this file. The 'Sd' prefix was added to avoid this issue.
=====================================================================
*/

#pragma once

#include "ConnectionManager.h"
#include "BaseRpc.h"
#include "Network.h"
#include "NetworkEngineComponent.h"
#include "NetworkRoleComponent.h"
#include "NetworkSignature.h"
#include "RpcPtr.h"

SD_BEGIN
template <class ... P>
class Rpc : public BaseRpc
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The call to the actual function pointer. This function should check its parameters.
	If this function returns false, it'll notify the caller that their RPC attempt is not valid. */
	SDFunction<bool, P...> OnRpcImplementation;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rpc () : BaseRpc()
	{
		//Noop
	}

	virtual ~Rpc ()
	{
		//Noop
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override
	{
		if (OnRpcImplementation.IsBounded())
		{
			return OnRpcImplementation.ReadFriendlyName();
		}

		return TXT("<Unbounded RPC>");
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	/**
	 * Attempts to execute the procedure on all processes connected to the associated NetworkComponent.
	 * Returns true if it constructed at least one RPC packet in the network engine's pending list.
	 */
	bool operator() (ERpcType rpcType, P... params) const
	{
		if (NetComp == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s on remote processes without registering the RemoteProceduralCall to a NetworkComponent."), OnRpcImplementation.ReadFriendlyName());
			return false;
		}

		//Permissions vary based on the destination. Not going rewrite the data buffer for each destination so it'll remain outside the loop; the destination will check against permissions anyways. To skip the permission checks, simply pass P_Everything in the parameter.
		DataBuffer paramBuffer;
		if (!WriteParam(OUT paramBuffer, NetworkGlobals::P_Everything, params...))
		{
			return false;
		}

		NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
		CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)

		bool bConstructedPacket = false;
		for (const auto& externalComp : NetComp->ReadExternalNetComps())
		{
			//Check for role first
			if (externalComp.second.RemoteProcess == nullptr || !externalComp.second.RemoteProcess->HasRole(static_cast<NetworkSignature::ENetworkRole>(rpcType & ERpcType::RT_RoleMask)))
			{
				//This component doesn't have any of the roles
				continue;
			}

			//Check for Network ownership only if the NetRole mask is true.
			if ((rpcType & ERpcType::RT_NetOwnerFlag) > 0 && externalComp.second.NetRole == NetworkRoleComponent::NR_Impotent)
			{
				//The remote role is not Authoritative or a NetOwner. Skip it
				continue;
			}

			if (IsAvailableFor(localNetEngine->GetLocalSignature(), externalComp.second.RemoteProcess) == RA_Available)
			{
				bConstructedPacket |= localNetEngine->GetConnections()->ExecuteRpc(externalComp.second.RemoteProcess, NetComp, RpcHash, paramBuffer);
			}
			//Don't log for else conditions since this loop should simply skip over invalid destinations.
		}

		return bConstructedPacket;
	}

	/**
	 * Attempts to execute the procedure on the specified process.
	 * Returns true if it constructed at a RPC packet in the network engine's pending list.
	 */
	bool operator() (NetworkSignature* invokeOn, P... params) const
	{
		if (NetComp == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s on remote processes without registering the RemoteProceduralCall to a NetworkComponent."), OnRpcImplementation.ReadFriendlyName());
			return false;
		}

		if (invokeOn == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute a RPC without specifying a destination. Either specifiy a NetworkSignature or use ERpcType."));
			return false;
		}

		//Ensure this component is connected to invokeOn
		if (!NetComp->ReadExternalNetComps().contains(invokeOn->GetNetId()))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s on Network Signature %s from a Network Component with ID %s since that Network Component is not connected to the specified signature."), OnRpcImplementation.ReadFriendlyName(), invokeOn->GetNetId(), NetComp->GetNetId());
			return false;
		}

		NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
		CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)

		//Ensure this RPC is available for destination
		ERpcAvailability availability = IsAvailableFor(localNetEngine->GetLocalSignature(), invokeOn);
		switch (availability)
		{
			case(RA_Available):
				break;

			case(RA_Unavailable):
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s on Network Signature %s since the RPC doesn't execute on that signature."), ToString(), invokeOn->GetNetId());
				return false;

			case(RA_NotPermitted):
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s on Network Signature %s since this process does not have permission to remotely execute that function."), ToString(), invokeOn->GetNetId());
				return false;
		}

		ConnectionManager* connections = localNetEngine->GetConnections();
		NetworkGlobals::EPermissions localPermissions = connections->GetLocalPermissions(invokeOn);

		DataBuffer paramBuffer;
		if (!WriteParam(OUT paramBuffer, localPermissions, params...))
		{
			return false;
		}

		return localNetEngine->GetConnections()->ExecuteRpc(invokeOn, NetComp, RpcHash, paramBuffer);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Assigns the given delegate to be executed whenever a remote process invokes this RPC.
	 * This function also generates the function hash.
	 */
	void BindRpc (const SDFunction<bool, P...>& inOnRpcImplementation)
	{
		OnRpcImplementation = inOnRpcImplementation;

		if (OnRpcImplementation.IsBounded())
		{
			RpcHash = OnRpcImplementation.ReadFriendlyName().GenerateHash();
		}
		else
		{
			RpcHash = 0;
		}
	}

	/**
	 * Locally executes the implementation function.
	 * To invoke the implementation on a remote process, use operator() instead.
	 */
	ERpcResult LocalExecute (P... params) const
	{
		if (!OnRpcImplementation.IsBounded())
		{
			return RR_Internal;
		}

		if (!OnRpcImplementation.Execute(params...))
		{
			return RR_InvalidCall;
		}

		return RR_Success;
	}

	template <class A, class ... B>
	ERpcResult ReadParam (NetworkSignature* sentFrom, const DataBuffer& paramBuffer, A& outParam, B& ... outParams) const
	{
		if (!paramBuffer.CanReadBytes(outParam.GetMinBytes()))
		{
			return RR_InvalidBuffer;
		}

		if ((paramBuffer >> outParam).HasReadError())
		{
			return RR_InvalidBuffer;
		}

		return ReadParam(sentFrom, paramBuffer, OUT outParams...);
	}

	template <class A, class ... B>
	ERpcResult ReadParam (NetworkSignature* sentFrom, const DataBuffer& paramBuffer, std::vector<A>& outParam, B& ... outParams) const
	{
		if ((paramBuffer >> outParam).HasReadError())
		{
			return RR_InvalidBuffer;
		}

		return ReadParam(sentFrom, paramBuffer, OUT outParams...);
	}

	template <class A, class ... B>
	ERpcResult ReadParam (NetworkSignature* sentFrom, const DataBuffer& paramBuffer, RpcPtr<A>& outParam, B& ... outParams) const
	{
		if (sentFrom == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot process ReadParam without specifying a sentFrom pointer since it is needed to figure out the Entity pointer."));
			return RR_Internal;
		}

		Int externalNetId = 0;
		if (!paramBuffer.CanReadBytes(externalNetId.GetMinBytes()))
		{
			return RR_InvalidBuffer;
		}

		if ((paramBuffer >> externalNetId).HasReadError())
		{
			return RR_InvalidBuffer;
		}

		if (externalNetId == 0)
		{
			//Client passed 'nullptr' in the data buffer, which could be valid
			outParam = nullptr;
			return ReadParam(sentFrom, paramBuffer, OUT outParams...);
		}

		ConnectionManager* connections = ConnectionManager::GetConnectionManager();
		NetworkRoleComponent* roleComp = connections->FindRoleCompFromExternalId(sentFrom, externalNetId);
		if (roleComp == nullptr)
		{
#ifdef DEBUG_MODE
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Unable to read param for RPC since there isn't a Network Role Component that has a connection to another with an ID %s."), externalNetId);
#endif
			return RR_InvalidBuffer;
		}
		else if (roleComp->GetOwner() == nullptr)
		{
#ifdef DEBUG_MODE
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to read param for RPC since the found role component is not attached to an Entity."));
#endif
			return RR_InvalidBuffer;
		}

		NetworkGlobals::EPermissions remotePerms = connections->GetRemotePermissions(sentFrom);
		if ((remotePerms & NetworkGlobals::P_RefAnything) == 0)
		{
			if ((remotePerms & NetworkGlobals::P_RefNetOwner) == 0)
			{
				//Not permitted to reference objects in RPCs
				return RR_NotPermitted;
			}

			//Ensure the client has permission to reference the object (must be a NetOwner or Authoritative)
			NetworkRoleComponent::ENetRole remoteRole = roleComp->GetRemoteNetRole(sentFrom);
			if (remoteRole == NetworkRoleComponent::NR_Unknown || remoteRole == NetworkRoleComponent::NR_Impotent)
			{
#ifdef DEBUG_MODE
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to process RPC function since %s does not have the correct permissions to reference %s."), sentFrom->ToString(), roleComp->GetOwner()->ToString());
#endif
				return RR_NotPermitted;
			}
		}

		outParam = roleComp->GetOwner();
		return ReadParam(sentFrom, paramBuffer, OUT outParams...);
	}

	ERpcResult ReadParam (NetworkSignature* sentFrom, const DataBuffer& paramBuffer) const
	{
		return RR_Success;
	}

	
	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	template <class A, class ... B>
	bool WriteParam (DataBuffer& outParamBuffer, NetworkGlobals::EPermissions localPermissions, const A& param, const B& ... params) const
	{
		outParamBuffer << param;
		return WriteParam(OUT outParamBuffer, localPermissions, params...);
	}

	template <class A, class ... B>
	bool WriteParam (DataBuffer& outParamBuffer, NetworkGlobals::EPermissions localPermissions, const RpcPtr<A>& param, const B& ... params) const
	{
		if (param.IsNullptr())
		{
			//Send 0 to indicate nullptr
			Int nullId(0);
			outParamBuffer << nullId;
			return WriteParam(OUT outParamBuffer, localPermissions, params...);
		}

		//Find the RoleComponent attached to the Entity
		NetworkRoleComponent* roleComp = nullptr;
		for (ComponentIterator iter(param.Get(), false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (roleComp = dynamic_cast<NetworkRoleComponent*>(iter.GetSelectedComponent()))
			{
				break;
			}
		}

		if (roleComp == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Only Entities with a Network Role Component may be passed through RPCs."));
			return false;
		}
		else if (roleComp->GetNetId() == 0)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("The Network Role Component attached to %s cannot be used in a RPC since its NetId is not assigned."), param->ToString());
			return false;
		}

		if ((localPermissions & NetworkGlobals::P_RefAnything) == 0)
		{
			if ((localPermissions & NetworkGlobals::P_RefNetOwner) == 0)
			{
				//Not permitted to reference any object in RPCs
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to process RPC function since this process does not have the correct permission to reference any object in RPC methods."));
				return false;
			}

			//Ensure the client has permission to reference the object (must be a NetOwner or Authoritative)
			if (roleComp->GetNetRole() == NetworkRoleComponent::NR_Unknown || roleComp->GetNetRole() == NetworkRoleComponent::NR_Impotent)
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to process RPC function since this process does not have the correct permissions to reference %s in RPC methods."), param->ToString());
				return false;
			}
		}

		Int netId = roleComp->GetNetId();
		outParamBuffer << netId;

		return WriteParam(OUT outParamBuffer, localPermissions, params...);
	}

	bool WriteParam (DataBuffer& outParamBuffer, NetworkGlobals::EPermissions localPermissions) const
	{
		return true;
	}
};
SD_END