/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RpcPtr.h
  A simple wrapper to a raw pointer.

  The main reason why this class exists is to invoke a specific template function instance
  based on this type. When invoking RPC's, raw pointers cannot be passed across the network.
  The template functions converts the raw pointers to a net identifier and converts it back
  to a pointer on the receiving end.

  Design decision notes:
  * Could not invoke a specific instance using ReadParam<Entity*>() primarily because ReadParam
  recursively calls into itself for each variadic template argument.
  * Also can't implement edge cases for pointers in order to maintain the convenience factor
  for using the BIND_RPC macros.

  Implementing a wrapper (RpcPtr) seems like a decent compromise to support variadic template
  arguments for function binding as well as supporting the RPC macros.
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
template <class T>
class RpcPtr final
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The raw pointer to the Entity. */
	T* Pointer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	RpcPtr ()
	{
		Pointer = nullptr;
	}

	RpcPtr (T* other)
	{
		Pointer = other;
	}

	RpcPtr (const RpcPtr& other)
	{
		Pointer = other.Pointer;
	}

	~RpcPtr ()
	{
		//Noop
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	template <class A>
	void operator= (A* otherPointer)
	{
		Pointer = dynamic_cast<T*>(otherPointer);
	}

	template <class A>
	void operator= (const DPointer<A>& otherPointer)
	{
		Pointer = dynamic_cast<T*>(otherPointer.Pointer);
	}

	template <class A>
	void operator= (const RpcPtr<A>& otherPointer)
	{
		Pointer = dynamic_cast<T*>(otherPointer.Pointer);
	}

	inline T& operator* () const
	{
		return *Pointer;
	}

	inline T* operator-> () const
	{
		return Pointer;
	}

	virtual operator bool () const
	{
		return (Pointer != nullptr);
	}

	virtual bool operator! () const
	{
		return (Pointer == nullptr);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this pointer is not pointing to nullptr.
	 */
	inline bool IsValid () const
	{
		return (Pointer != nullptr);
	}

	/**
	 * Returns true if this pointer is pointing to nullptr.
	 */
	inline bool IsNullptr () const
	{
		return (Pointer == nullptr);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline T* Get () const
	{
		return Pointer;
	}
};

#pragma region "External Operators"
template <class T, class U>
bool operator== (const RpcPtr<T>& left, const RpcPtr<U>& right)
{
	return (left.Get() == right.Get());
}

template <class T>
bool operator== (const RpcPtr<T>& left, void* right)
{
	return (left.Get() == right);
}

template <class T, class U>
bool operator== (const RpcPtr<T>& left, const DPointer<U>& right)
{
	return (left.Get() == right.Get());
}

template <class T>
bool operator== (void* left, const RpcPtr<T>& right)
{
	return (left == right.Get());
}

template <class T, class U>
bool operator== (const DPointer<T>& left, const RpcPtr<U>& right)
{
	return (left.Get() == right.Get());
}

template <class T, class U>
bool operator!= (const RpcPtr<T>& left, const RpcPtr<U>& right)
{
	return (left.Get() != right.Get());
}

template <class T>
bool operator!= (const RpcPtr<T>& left, void* right)
{
	return (left.Get() != right);
}

template <class T, class U>
bool operator!= (const RpcPtr<T>& left, const DPointer<U>& right)
{
	return (left.Get() != right.Get());
}

template <class T>
bool operator!= (void* left, const RpcPtr<T>& right)
{
	return (left != right.Get());
}

template <class T, class U>
bool operator!= (const DPointer<T>& left, const RpcPtr<U>& right)
{
	return (left.Get() != right.Get());
}
#pragma endregion
SD_END