/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkMonitor.h
  Base class for objects that can monitor network activity.

  This particular object is responsible for monitoring the network activity sent and received
  from this process. The objective behind this is to observe the rejected network packets.
  This entity will forcibly kick remote process that are causing too many rejection packets
  in a short period of time.

  Without this, there is a vulnerablity where a client could maliciously continuously send
  false packets, flooding the network, and preventing others from updating. In addition to that,
  clients causing too many rejected packets are probably attempting to cheat the game or their
  connection is too unreliable to continue.
=====================================================================
*/

#pragma once

#include "ConnectionManager.h"
#include "Network.h"

SD_BEGIN
class ConnectionManagerPacket;
class NetworkSignature;

class NETWORK_API NetworkMonitor : public Entity
{
	DECLARE_CLASS(NetworkMonitor)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SOffenderData
	{
		NetworkSignature* Offender;
		Int NumOffenses;

		SOffenderData (NetworkSignature* inOffender);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Maximum number of recent offenses before this process kicks the offender. */
	Int MaxOffenses;

	/* List of clients that recently caused this process to send a reject packet. */
	std::vector<SOffenderData> RecentOffenders;

	/* Tick Component that's responsible for decrementing offenses from each client. */
	DPointer<TickComponent> ForgivenessTick;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Allows the NetworkMonitor to inspect the connection packet before it's sent off to the receiver.
	 */
	virtual void ReadOutboundPacket (ConnectionManager::ENetMessageType msgType, ConnectionManagerPacket* packet, NetworkSignature* receiver);

	/**
	 * Allows the NetworkMonitor to inspect the incoming connection packet from the sender.
	 * This is processed before the ConnectionManager processes the packet.
	 */
	virtual void ReadInboundPacket (ConnectionManager::ENetMessageType msgType, ConnectionManagerPacket* packet, NetworkSignature* sender);

	/**
	 * Notifies the monitor that the specified client sent a malformed packet. This can be called directly for offenders that sent illegible packets.
	 * This method will register the sender in the offender list, which may potentially instigate a kick.
	 */
	virtual void AddOffender (NetworkSignature* offender);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Removes the given offender from this process.
	 */
	virtual void KickOffender (NetworkSignature* offender);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleForgivenessTick (Float deltaSec);
	virtual void HandleDisconnect (NetworkSignature* disconnected);
};
SD_END