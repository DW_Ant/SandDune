/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Network.h
  Contains important file includes and definitions for the Network module.

  The Network module enables communication between two different processes.
  This leverages SFML's network module to do the heavy lifting. This module essentially
  structures the framework to support the following:

  * Conditionally replicate variables based on developer specifications.
  * Register network callbacks, allowing component owners to react to Remote Procedural Calls.
  * Agnostic network approach, allowing developers to choose peer-to-peer or centralized network structures.
  * Leverage Sand Dune's Entity Component system allowing developers to attach Network Components to
  any Entity to transfer data to or from the remote process.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#include <SFML/Network.hpp>

#ifdef PLATFORM_WINDOWS
	#ifdef NETWORK_EXPORT
		#define NETWORK_API __declspec(dllexport)
	#else
		#define NETWORK_API __declspec(dllimport)
	#endif
#else
	#define NETWORK_API
#endif

#define TICK_GROUP_NETWORK "Network"
#define TICK_GROUP_PRIORITY_NETWORK 7500 //Handle network data pretty early in the tick cycle.

SD_BEGIN
extern LogCategory NETWORK_API NetworkLog;

namespace NetworkGlobals
{
	/* List of permission flags a single connection may have.
	Note: This would normally reside in ConnectionManager, but it moved to NetworkGlobals since both ConnectionManager and PacketsConnectionManager references this. */
	enum EPermissions : unsigned char
	{
		P_None = 0, //This process can read messages only. It cannot send anything back to the remote process.
		P_SendDataNetOwner = 1, //This process can only send data to NetworkComponents if they are the NetworkOwners over that object.
		P_SendData = 2, //This process can send data to NetworkCompnents regardless if they're net owners or not.

		P_RpcNetOwner = 4, //This process can invoke a NetworkComponent's RPC function if they are the NetworkOwners over that object.
		P_Rpc = 8, //This process can invoke any NetworkComponent's RPC function.

		P_RefAnything = 16, //When populating RPC parameters, this process can reference any network Entity.
		P_RefNetOwner = 32, //When populating RPC parameters, this process can only reference network Entities the process has net ownership over.

		P_Client = 37, //P_SendDataNetOwner, P_RpcNetOwner, and P_RefNetOwner. Typical permissions for a client in a centralized architecture.

		P_InstantiateComponent = 64, //This process can instantiate NetworkComponents on the remote process.

		P_Everything = 255 //This process can do everything! Enable all permissions. Typically servers and trusted clients have this permission.
	};
}

DEFINE_ENUM_FUNCTIONS(NetworkGlobals::EPermissions)
SD_END