/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseReplicatedVariable.h
  A lightweight object responsible for determining when it's appropriate to send
  variable data across the network.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"

SD_BEGIN
class NetworkComponent;

class NETWORK_API BaseReplicatedVariable
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EReplicationFlags : unsigned char
	{
		RF_None = 0x0, //No flags. This variable will not send data across the network. It's probably only receiving variable updates from a remote process.
		RF_SyncOnInit = 0x1, //If enabled then this variable is sent across the network immediately after instantiating an instance on the remote process.
		RF_SyncOnChange = 0x2, //If enabled then this variable will sync to the other process(es) whenever it changes.
		RF_BroadcastToAll = 0x4, //If enabled then this variable will replicate to everyone. Otherwise it'll only replicate to net owners.
		RF_WaitForReceive = 0x8 //If true, then this variable will not replicate until after it received an update. This is used for clients that are waiting for init data from authoritative components.
	};

	enum EReplicationResults : unsigned char
	{
		RR_Success,
		RR_NoWritePermissions, //The process is not permitted to write to this variable.
		RR_InvalidBuffer, //The buffer does not contain the appropriate bytes to read the data.
		RR_InvalidData //The owner determined that the replicated value is not possible. Either from cheat detection or invalid range.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever this variable has changed from a remote process.
	This is called on the Sender side to save bandwidth. Use this to determine relevance and to save bandwidth. 
	To verify data on the recipient side, see ReplicatedVariable::OnValidateData. Use that callback for security purposes. */
	SDFunction<void> OnReplicated;

	/* If bounded then this object will check if this lambda returns true. If this lambda returns true, it'll
	allow the replication process to proceed. Otherwise it'll prevent the variable from being replicated to remote processes.
	This is only called on the process that intends to send the variable out. */
	SDFunction<bool, NetworkSignature* /*destination*/> OnShouldReplicate;

	/* Determines which processes are permitted to write and broadcast this variable's value to other processes.
	This enumeration must be assigned on both ends of the connection since this is checked when sending and receiving variable updates.
	By default, any connection that has a reference to this replicated variable has write access. */
	NetworkSignature::ENetworkRole WritePermissions;

protected:
	/* List of bit flags that defines how this variable replicates across the network. See EReplicationFlags. */
	unsigned char ReplicationFlags;

	/* The minimum time interval before evaluating when to send the variable. Prevents sending excessive number of packets when it changes every frame.
	This is only applicable when the replication flags have RF_SyncOnChange set to enabled. This variable is in seconds. */
	Float SyncInterval;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	BaseReplicatedVariable ();
	virtual ~BaseReplicatedVariable ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Prevents this variable from sending data to the remote process.
	 * Call this on instances intended to only receive variable updates, or to reset the flags.
	 */
	virtual void ClearReplicationFlags ();

	/**
	 * Returns true if this variable should be replicated across the network.
	 */
	virtual bool ShouldReplicate (NetworkSignature* localSignature, NetworkSignature* destination, NetworkComponent* owningComp, Float engineTimestamp) const;

	/**
	 * Actually conducts the replication. It'll append the contents of this variable to the given data buffer.
	 */
	virtual void ReplicateVariable (NetworkSignature* destination, Float engineTimestamp, DataBuffer& outVarDataPack);

	/**
	 * Reads from the given data buffer and updates the variable reference from its contents.
	 */
	virtual EReplicationResults ReceiveVariableUpdate (NetworkSignature* source, Float engineTimestamp, const DataBuffer& varDataPack);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSyncOnInit (bool bNewSyncOnInit);
	virtual void SetSyncOnChange (bool bNewSyncOnChange);
	virtual void SetBroadcastToAll (bool bNewBroadcastToAll);
	virtual void SetWaitForReceive (bool bNewWaitForReceive);
	virtual void SetSyncInterval (Float newSyncInterval);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool GetSyncOnInit () const
	{
		return ((ReplicationFlags & RF_SyncOnInit) != 0);
	}

	inline bool GetSyncOnChange () const
	{
		return ((ReplicationFlags & RF_SyncOnChange) != 0);
	}

	inline bool GetBroadcastToAll () const
	{
		return ((ReplicationFlags & RF_BroadcastToAll) != 0);
	}

	inline bool GetWaitForReceive () const
	{
		return ((ReplicationFlags & RF_WaitForReceive) != 0);
	}

	inline Float GetSyncInterval () const
	{
		return SyncInterval;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns RR_Success if this object is allowed to receive variable updates from the given source.
	 */
	virtual EReplicationResults CanReceiveUpdates (NetworkSignature* source) const;

	/**
	 * Attempts to extract data from the given data buffer.
	 * If successful, it'll copy its contents to the variable.
	 */
	virtual EReplicationResults WriteVariableFromBuffer (NetworkSignature* source, Float engineTimestamp, const DataBuffer& varDataPack) = 0;
};
SD_END