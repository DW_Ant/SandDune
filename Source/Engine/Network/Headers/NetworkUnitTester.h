/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkUnitTester.h
  Tests various aspects of the Network Module.
=====================================================================
*/

#pragma once

#include "Network.h"

#ifdef DEBUG_MODE
SD_BEGIN
class NETWORK_API NetworkUnitTester : public UnitTester
{
	DECLARE_CLASS(NetworkUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestAccessControl (UnitTester::EUnitTestFlags testFlags) const;
};
SD_END
#endif