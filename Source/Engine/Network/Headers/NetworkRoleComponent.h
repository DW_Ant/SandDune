/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkRoleComponent.h

  Defines a component for an Entity that allows the Entity to replicate its Network
  Components to a remote process. This component not only manages the NetworkComponent tree,
  but also this defines the network role for the entire component chain.

  In addition to that, this role component allows the propagation of the owning Entity to reside
  in multiple processes.

  NetworkComponents cannot propagate to a remote process if their root owner does not have
  this component. This component cannot be attached to EntityComponents. An Entity can only
  have one of these components.

  Design choice notes:
  These requirements scream to use an interface instead of a component primarily because
  of the restriction of having only one instance on the Entity level. The reasons we
  went with a component approach because of the following reasons:
  * Need to have access to Entity functions and hooks.
    -Gain access to BeginObject, InitProps, and Destroyed. This object can properly initialize itself
	and clean up without relying on the Entity on overriding certain functions.
  * Can add sub components. In this case, Tick Component is used for checking relevance. This is a better
  approach than relying on the Connection Manager holding a reference to every role Entity to update them.
  * Prevent other classes from inheriting from the network interface. Objects shouldn't inherit from it since
  they cannot have NetworkComponents. And EntityComponents should not inherit from the interface since the
  remote processes would need context when propagating class instances.
  * Allows developers to add NetworkRoleComponents to Entities that originally did not have reference to a Network
  module (since components can be added dynamically rather than compile time).
  * Allows for specialization role component classes (eg: A particular Entity could either attach the default role
  class compared to customized/derived role component).
  * Avoids diamond problem.
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
class ConnectionManager;
class NetworkComponent;
class NetworkSignature;

class NETWORK_API NetworkRoleComponent : public EntityComponent
{
	DECLARE_CLASS(NetworkRoleComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ENetRole
	{
		NR_Unknown, //Unknown. Typically used for uninitialized variables.
		NR_Local, //This process owns this Entity however this will not broadcast to other processes since this Entity is meant for this process alone.
		NR_Impotent, //This Entity was propagated from a remote process, and this client is not an owner. Minimum permissions granted.
		NR_NetOwner, //This Entity was propagated from a remote process, and this client is considered an owner. Some permissions are granted.
		NR_Authority //This process owns this Entity, and it may broadcast this Entity to remote processes. All permissions are granted.
	};

protected:
	enum EClientStatus
	{
		CS_None, //This process did not send anything to this client. Or the client disconnected.
		CS_Pending, //This process sent a packet on its way. Currently waiting for the response of its results.
		CS_Connected //There is an instance of this Entity residing on the remote process.
	};


	/*
	=====================
	  Struct
	=====================
	*/

protected:
	struct SRemoteClientData
	{
		/* The network signature of the client that is aware of this Entity's existance. */
		DPointer<NetworkSignature> RemoteClient;

		EClientStatus Status;

		/* The NetworkRole of the external component. */
		ENetRole RemoteRole;

		/* Timestamp when this was sent. Used for detecting timeout (in seconds). */
		Float RequestSentTime;

		/* The net id of the role component on the remote client. When accessing the paired role comp, this process
		should refer to that entity using this ID. */
		Int RemoteId;

		SRemoteClientData () :
			Status(CS_None),
			RemoteRole(NR_Unknown),
			RequestSentTime(0.f),
			RemoteId(0)
		{
			//Noop
		}

		SRemoteClientData (NetworkSignature* inRemoteClient, ENetRole inRemoteRole, Float inRequestSentTime) :
			RemoteClient(inRemoteClient),
			Status(CS_Pending),
			RemoteRole(inRemoteRole),
			RequestSentTime(inRequestSentTime),
			RemoteId(0)
		{
			//Noop
		}

		SRemoteClientData (const SRemoteClientData& cpyObj) :
			RemoteClient(cpyObj.RemoteClient),
			Status(cpyObj.Status),
			RemoteRole(cpyObj.RemoteRole),
			RequestSentTime(cpyObj.RequestSentTime),
			RemoteId(cpyObj.RemoteId)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SDFunction<void, std::vector<NetworkComponent*>& /*outNetComponents*/> OnGetComponentList;

	/**
	 * Allows component owners to react to this network event.
	 * See NetInitialize.
	 */
	SDFunction<void, NetworkSignature* /*connectedTo*/> OnNetInitialize;

	/**
	 * Returns true if this Entity is relevant to the given remote client.
	 * This function is only used when determining when it's appropriate to broadcast this Entity to remote processes.
	 * NetworkComponents would still need to determine when it's appropriate when to send variables across the bounds.
	 * This is only called for Entities where their NetRole is Authority.
	 * If this delegate is not bound, it'll default to being relevant to everyone.
	 *
	 * Examples:
	 * Characters would only return true if they are within the client's view frustum.
	 * Objective markers may be only relevant to squads/teams pursuing that objective.
	 * Scoring information would always be relevant to everyone.
	 */
	SDFunction<bool, NetworkSignature* /*remoteClient*/> OnIsNetRelevant;

	/**
	 * If this delegate returns true, then the client will be a NetOwner over this component's owner.
	 * If it's not bound, then this component will assume no remote client is a net owner.
	 * This is only executed on Authoritative components that are about to instantiate this object on remote clients.
	 * Examples when a client should be a net owner:
	 * Giving a player control over their character.
	 * Giving a team the ability to command their allies.
	 */
	 SDFunction<bool, NetworkSignature* /*remoteClient*/> OnIsNetOwner;

	/**
	 * Invoked whenever this entity is no longer relevant before it's destroyed.
	 * The destroy on lose relevance flag can overwritten to prevent destruction.
	 * This can only execute when the role is Impotent or NetOwner.
	 */
	SDFunction<void> OnLoseRelevance;

	/* If true, then the owning Entity will be destroyed when it loses net relevance. */
	bool bDestroyWhenLostRelevance;

protected:
	/* Used to uniquely identify this Entity from others within the same processes. When communicating between processes, the systems refer
	to specific Entity instances using this number.
	This is set whenever the network role is set to something other than Local. */
	Int NetId;

	ENetRole NetRole;

	/* Maximum wait time (in seconds) before this Entity will drop a pending connection. The client must respond within this time frame or else it will be dropped. */
	Float TimeoutTime;

	/* List of remote clients that are aware of this Entity's existance. This is only applicable when this Entity's role is Authority. */
	std::vector<SRemoteClientData> RemoteClients;

	/* The client responsible for instantiating this object. This is only applicable when net role is either Impotent or NetOwner. */
	SRemoteClientData AuthoritativeClient;

	/* Tick component responsible for evaluating when remote entities are no longer relevant as well as iterating through NetSignatures to see if
	there are any clients that should be aware of this Entity. This only needs to tick when the net role is authoritative. */
	DPointer<TickComponent> RelevanceTick;

private:
	DPointer<ConnectionManager> Connections;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Called whenever this component successfully registered to the ConnectionManager's RoleComponents map. This will be invoked for each
	 * connected process.
	 * For centralized network architecture, clients will only receive one call whenever they connect to the server. The server however will
	 * get call for each client.
	 * Local net roles will not be invoked.
	 *
	 * It is safe to assume that this component's net role, remote clients, and ownership is assigned by the time this function executes.
	 */
	virtual void NetInitialize (NetworkSignature* connectedTo);

	/**
	 * Returns true if this component was instantiated from a remote process where this component does not have authority over the owning Entity.
	 */
	inline bool IsClient () const
	{
		return (NetRole == NR_Impotent || NetRole == NR_NetOwner);
	}

	/**
	 * Returns true if the local process instantiated this component. This component has authoritative control over the owning Entity.
	 */
	inline bool HasAuthority () const
	{
		return (NetRole == NR_Authority || NetRole == NR_Local);
	}

	/**
	 * Generates a list of NetworkComponents that resides somewhere in this Entity's component tree.
	 * The order of this vector determines the order of the Network ID's found in the data buffer when propagating this Entity.
	 * This component list must be the same on both processes to properly pair up the components.
	 * Should certain components be missing, this function should still add a nullptr reference to the vector to avoid displacing the data values.
	 */
	virtual void GetNetComponentList (std::vector<NetworkComponent*>& outComponentList) const;

	/**
	 * Sends a data packet to the given client to propagate this Entity to their process.
	 * This is only applicable for Entities where their NetRole is Authority.
	 * Returns true on success.
	 */
	virtual bool BroadcastNetEntity (NetworkSignature* remoteClient);

	/**
	 * Invoked whenever this Entity no longer becomes relevant. The process with authority over this Entity
	 * deemed that Entity is no longer relevant to this process. This function should clean up this Entity.
	 * This is only called when the role is either Impotence or NetOwner.
	 */
	virtual void LoseRelevance ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetNetRole (ENetRole newNetRole);
	virtual void SetTimeoutTime (Float newTimeoutTime);
	virtual void SetAuthoritativeClient (NetworkSignature* authClientSignature, Int externalNetId);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetNetId () const
	{
		return NetId;
	}

	inline ENetRole GetNetRole () const
	{
		return NetRole;
	}

	ENetRole GetRemoteNetRole (NetworkSignature* remoteProcess) const;

	inline Float GetTimeoutTime () const
	{
		return TimeoutTime;
	}

	inline TickComponent* GetRelevanceTick () const
	{
		return RelevanceTick.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Generates a unique Id for this Entity. Does nothing if it's already assigned.
	 * There's no need to call this function if the net role is local.
	 */
	virtual void GenerateNetEntityId ();

	/**
	 * Returns true if this owning Entity is relevant to the specified remote signature.
	 */
	virtual bool IsRelevant (NetworkSignature* remoteClient) const;

	/**
	 * Returns true if the remoteClient is a Network Owner of this component's owner.
	 */
	virtual bool IsNetOwner (NetworkSignature* remoteClient) const;

	/**
	 * The remote process accepted this component's instantiation request.
	 * This function updates this component's status and pairs up its NetworkComponents.
	 */
	virtual void ProcessInstantiationAccepted (NetworkSignature* remoteClient, Int remoteEntityId, const DataBuffer& remoteNetCompIds);

	/**
	 * The remote process rejected this component's instantiation request.
	 * This function updates drops any references to clients waiting for this request.
	 */
	virtual void ProcessInstantiationRejected (NetworkSignature* remoteClient);

	/**
	 * Disconnects any network components associated with the specified signature.
	 * The externalNetId is the network id of the NetworkRoleComponent on the remote client. If it's not zero, then it'll send a network packet
	 * to inform the remote process to disconnect its entity as well.
	 */
	virtual void DropClient (NetworkSignature* remoteClient, Int externalNetId);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleRelevanceTick (Float deltaSec);

	friend class ConnectionManager;
};
SD_END