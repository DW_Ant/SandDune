/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SignatureIterator.h
  An iterator that searches through the connection manager's active connection list,
  and returning its network signatures.

  This will not find any NetworkSignatures that are not in the Connection Manager
  (eg: local signature in the NetworkEngineComponent and CDOs).
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
class ConnectionManager;
class NetworkSignature;

class NETWORK_API SignatureIterator final
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The Connection Manager instance this iterator is iterating through. */
	ConnectionManager* Manager;

	/* Number of times to increment the iterator when iterating through the map. */
	size_t IterAmount;

	/* The Network Signature instance this iterator is currently pointing to. */
	DPointer<NetworkSignature> SelectedSignature;

	/* If true, then this iterator will iterate through all signatures including those that are not yet connected. */
	bool bIncludeDisconnected;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SignatureIterator (bool inIncludeDisconnected);
	~SignatureIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator++ (); //++iter
	void operator++ (int); //iter++


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	NetworkSignature* GetSelectedSignature () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void SelectFirstSignature ();
	void SelectNextSignature ();
};
SD_END