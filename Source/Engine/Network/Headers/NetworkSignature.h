/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkSignature.h

  An Entity used to uniquely identify a user from others.
  A user is identified based on a collection of distinct signatures.

  It is up to the developer to specify signature types. Typically an application would
  only care for the user's ip address and some account (such as a Steam account).

  The purpose of the NetworkSignature is to help maintain AccessControl by allowing administrators
  to add/remove account permissions, give certain accounts access to aspects of the application
  (eg: admins, users with certain DLCs, etc...), and lastly, this can add persistent
  data with each account (for example have the server save game data for each account).
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
class NetworkIdentifier;

class NETWORK_API NetworkSignature : public Entity
{
	DECLARE_CLASS(NetworkSignature)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* An enum used to figure out the connection types. Clients could have different identifiers from servers and services.
	The enum values must be in powers of two since it's possible for a single network signature to be classified in multiple types
	For example a client can also be a server. */
	enum ENetworkRole : unsigned char
	{
		NR_None = 0,
		NR_Client = 1, //The end user or player.
		NR_Server = 2, //The process that has authority to determine the game flow.
		NR_MasterServer = 4, //The process that coordinates through multiple servers (eg: server browser, server manager for MMOs, etc...)
		NR_Service = 8, //An external process that interfaces with this application. This could be a web hook (such as WebAdmin), integration with another process, etc...
		NR_Other = 16, //A connection that doesn't classify with the other types. Typically this is up for developers to use for special cases in their products.
		NR_Everything = 255 //The enum that contains all role flags.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Broadcasted whenever this object is destroyed or disconnected from this process.
	Event handlers shouldn't send disconnect packets since the signature is already severed. */
	MulticastDelegate<NetworkSignature* /*signature*/> OnDisconnected;

protected:
	/* The ip address of the process that instigated this signature. */
	sf::IpAddress Address;

	/* Unique identifier that other components can use to reference certain users from a separate process.
	A network ID is not permament across sessions. A new number is generated every time a user connects even if it's the same user.
	The ID is only unique to this process. Other processes may have different or same IDs for the same user. */
	Int NetId;

	/* The flag(s) from ENetworkRoles that will determine what kind of identifiers this signature may have. Most
	signatures will only have one role, but it's possible for certain signatures to have multiple roles. */
	ENetworkRole NetworkRoles;

private:
	/* Integer representation of the Address. Used for fast comparisons. */
	sf::Uint32 AddressInt;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual DString ToString () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the NetworkSignature that represents this process.
	 */
	static NetworkSignature* GetLocalSignature ();

	/**
	 * Generates and assigns a NetId to this NetworkSignature instance.
	 */
	virtual void GenerateNetId ();

	/**
	 * Returns true if this Network Signature's type has any roles this function is testing against.
	 */
	virtual bool HasRole (ENetworkRole role) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAddress (const sf::IpAddress& newAddress);
	virtual void SetNetworkRoles (ENetworkRole newNetworkRole);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline sf::IpAddress GetAddress () const
	{
		return Address;
	}

	inline const sf::IpAddress& ReadAddress () const
	{
		return Address;
	}

	inline Int GetNetId () const
	{
		return NetId;
	}

	inline ENetworkRole GetNetworkRoles () const
	{
		return NetworkRoles;
	}

	inline sf::Uint32 GetAddressInt () const
	{
		return AddressInt;
	}
};

DEFINE_ENUM_FUNCTIONS(NetworkSignature::ENetworkRole)
SD_END