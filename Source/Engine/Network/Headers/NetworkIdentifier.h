/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkIdentifier.h

  An EntityComponent that belongs to a NetworkSignature.

  This component is used for adding a unique aspect that can be used to distinguish one
  connected user from others.

  A NetworkSignature is defined based on a collection of identifiers to protect against
  impersonation.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"

SD_BEGIN
class NETWORK_API NetworkIdentifier : public EntityComponent
{
	DECLARE_CLASS(NetworkIdentifier)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Looks at this system to initialize this object.
	 * The network roles are the owning NetworkSignature's role value used to distinguish servers from clients.
	 * Returns true on success.
	 */
	virtual bool InitializeIdToCurSystem (NetworkSignature::ENetworkRole networkRoles) = 0;

	/**
	 * Initializes this component's data from the given stream buffer.
	 * The data must be in the same order as WriteDataToBuffer.
	 * Returns true if the data buffer has valid data.
	 * NOTE: This data is obtained from a foreign process. It's important to verify the integrity of each data.
	 */
	virtual bool ReadDataFromBuffer (const DataBuffer& data) = 0;

	/**
	 * Writes the contents of this identifier to the given DataBuffer.
	 * The contents must be in the same order as ReadDataFromBuffer.
	 */
	virtual void WriteDataToBuffer (DataBuffer& outData) = 0;
};
SD_END