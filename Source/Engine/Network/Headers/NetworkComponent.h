/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkComponent.h
  A component that allows its owner to sync variables, send Remote Procedural Calls (RPC)
  Calls (RPC) to a Network Component on a remote process.
  This component may also react to RPC's invoked from the paired Network Component

  It is worth noting that the Owning Entity can still act on its own locally.
  It can call any function, set any variable. It's only a matter in how much this process
  should desync from others.

  The NetworkComponent interfaces with the ConnnectionManager in order to send/receive data
  from remote processes.

  NetworkComponents should be attached to Entities by the time BeginObject finishes execution.
  Otherwise, when the remote process attempts to instantiate a replica of the Entity, it would not know
  where the other component resides, and it may reject the instantiate or data transfer request
  (when the net id is not found).
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkRoleComponent.h"
#include "ReplicatedVariable.h"

SD_BEGIN
class BaseRpc;
class NetworkSignature;

class NETWORK_API NetworkComponent : public EntityComponent
{
	DECLARE_CLASS(NetworkComponent)


	/*
	=====================
	  Enum
	=====================
	*/

public:
	enum ENetProtocol
	{
		NP_Invalid,
		NP_TCP, //Transmission Control Protocol: Reliable, always sends data in correct order, communicates with one machine.
		NP_UDP //User Datagram Protocol: Unreliable, end machine may receive duplicate data, random order, faster, and can be broadcasted across the network.
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SRemoteNetComponentData
	{
		NetworkSignature* RemoteProcess;

		/* The ID of the Network Component on the external process this component is communicating with. */
		Int NetId;

		/* The role of this remote component has over this component.
		If this is Authoritative, then this component can only be impotent or NetOwner. 
		if this is Importent or net owner, then this component can only be Authoritative. */
		NetworkRoleComponent::ENetRole NetRole;

		SRemoteNetComponentData (NetworkSignature* inRemoteProcess, Int inNetId, NetworkRoleComponent::ENetRole inNetRole) :
			RemoteProcess(inRemoteProcess),
			NetId(inNetId),
			NetRole(inNetRole)
		{
			CHECK(NetRole != NetworkRoleComponent::ENetRole::NR_Local) //Doesn't make sense for a remote component to be local and connected to this component.
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The ID used to uniquely identify this component from others. The external process references this ID to pair up data.
	This ID is only unique within this process scope. IDs are not unique to external processes. */
	Int NetId;

	/* Client network components are only connected to the one network component responsible for instantiating this entity.
	For authoritative network components, this is a list of every network component it's responsible for.
	The key to this map is equal to the NetworkSignature's NetId.*/
	std::unordered_map<Int, SRemoteNetComponentData, IntKeyHash, IntKeyEqual> ExternalNetComps;

	/* Reference to the role component responsible for determining this component's role.
	This role component is the component attached to this component's root Entity. */
	mutable NetworkRoleComponent* RoleComp;

	/* List of variables that can send or receive updates across the network. */
	std::vector<BaseReplicatedVariable*> ReplicatedVariables;

	/* List of functions remote processes are allowed to call. The key is the hash of the function string (ClassName::FunctionName).
	Because of this, a single NetworkComponent cannot have two RPCs of the same function of two different instances. */
	std::unordered_map<size_t, BaseRpc*> Rpcs;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the given RPC to a map. The NetworkComponent does not take ownership of the object primarily because the caller
	 * would typically own it. However registration is needed in order to quickly find the matching instance
	 * on other processes.
	 *
	 * There's no need to unregister the RPC from the NetworkCompnent. The RPC will automatically remove itself from its associated NetworkComponent upon destruction.
	 */
	virtual void RegisterRpc (BaseRpc* newRpc);

	virtual void UnregisterRpc (BaseRpc* targetRpc);

	/**
	 * Returns true if this component has an active connection, and is capable of sending/receiving data.
	 */
	inline bool IsConnected () const
	{
		return (ExternalNetComps.size() > 0);
	}

	/**
	 * Returns the role of this component. The role is found from the NetworkRoleComponent attached to the root Entity.
	 * Searches and assigns the role component if it's not yet associated with a role component.
	 */
	virtual NetworkRoleComponent::ENetRole GetNetRole () const;

	/**
	 * Returns the ExternalNetId of the NetworkComponent connected to the given remote process.
	 * Returns 0 if this component is not connected to the given signature.
	 */
	virtual Int GetExternalNetId (NetworkSignature* remoteProcess) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetNetId () const
	{
		return NetId;
	}

	inline const std::unordered_map<Int, SRemoteNetComponentData, IntKeyHash, IntKeyEqual>& ReadExternalNetComps () const
	{
		return ExternalNetComps;
	}

	inline const std::vector<BaseReplicatedVariable*>& ReadReplicatedVariables () const
	{
		return ReplicatedVariables;
	}

	inline const std::unordered_map<size_t, BaseRpc*>& ReadRpcs () const
	{
		return Rpcs;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Generates a unique identifier and assigns it to this component's NetId.
	 */
	virtual void GenerateId ();

	/**
	 * Notifies this NetworkComponent that it was instantiated from a remote process.
	 * This would prevent the network component from forcibly replicating its initial variable data back to the one that spawned this.
	 */
	virtual void ProcessNonAuthoritativeRole ();

	/**
	 * Notifies this component that it is now connected to the specified remote process.
	 * The ExternalNetId is the NetId of the NetworkComponent on the remote process.
	 */
	virtual void AddExternalNetComp (NetworkSignature* remoteProcess, Int externalNetId, NetworkRoleComponent::ENetRole remoteNetRole);

	/**
	 * Drops the network component that matches the given remote process.
	 * This does not send a drop connection packet to the remote process.
	 */
	virtual void RemoveExternalNetComp (NetworkSignature* remoteProcess);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSignatureDisconnected (NetworkSignature* signature);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Creates and initializes a replicated variable using the default replication properties.
	 */
	template <class V>
	size_t AddReplicatedVariable (V* source)
	{
		return AddReplicatedVariable<V>(source, [](ReplicatedVariable<V>& outInitRepVar)
		{
			//Noop - Using default properties
		});
	}

	/**
	 * Creates and initializes a replicated variable. This registers the given variable to the replication list.
	 * The initRepVariable is a callback that allows the caller to set the properties for the newly instantiated replicated variable.
	 * Returns the index where the replicated variable is found in this component's vector.
	 */
	template <class V>
	size_t AddReplicatedVariable (V* source, std::function<void(ReplicatedVariable<V>&)> onInitRepVar)
	{
		if (source == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot add a null replicated variable."));
			return UINT_INDEX_NONE;
		}

		ReplicatedVariable<V>* repVar = new ReplicatedVariable<V>(source);
		CHECK(repVar != nullptr)

		onInitRepVar(*repVar);
		ReplicatedVariables.emplace_back(repVar);
		return ReplicatedVariables.size() - 1;
	}

	friend class ConnectionManager;
	friend class NetworkRoleComponent;
};
SD_END