/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseRpc.h
  Short for Base Remote Procedural Call

  A lightweight object that permits NetworkComponents to contain a map of pointers
  to a RPC without 'templatizing' the component.

  Also allows for accessing the enums without having to reference each template argument.
  For example ERpcType is in base class to avoid having to write:
  Rpc<Param1, Param2, ...>::ERpcType.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"

SD_BEGIN
class NetworkComponent;

class NETWORK_API BaseRpc
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/**
	 * Enum that allows for broadcasting RPC's to multiple remote processes without having a direct reference to NetworkSignatures.
	 * This enumeration must match to NetworkSignature::ENetworkRole due to bitwise operations when checking for relevant signatures.
	 * This enumeration also supports mixing and matching roles. For example a developer can invoke a RPC using (RT_Client | RT_Service).
	 *
	 * This enumeration is not designed for security. It is designed for convenience, making it easy to broadcast RPC's to certain types of connections
	 * without having to iterate or directly reference NetworkSignatures.
	 * If there's a need to securely filter RPCs based on type, use the ERpcAvailability enum since that filter is checked on both ends of the connection.
	 */
	enum ERpcType : unsigned char
	{
		RT_Nobody = 0x00, //This RPC type will not broadcast to any process. This should never be used other than initializing an enum.
		RT_Clients = NetworkSignature::NR_Client, //This RPC type will be broadcasted on every client.
		RT_Servers = NetworkSignature::NR_Server, //This RPC type will be broadcasted to all servers.
		RT_MasterServers = NetworkSignature::NR_MasterServer, //This RPC type will be broadcasted to all master servers.
		RT_Services = NetworkSignature::NR_Service, //This RPC type will be broadcasted to all services.
		RT_Others = NetworkSignature::NR_Other, //This RPC type will be broadcasted to all connections that are connected as "other".
		RT_BroadcastToAll = 0x7F, //[0111 1111] This RPC will be invoked on every NetworkComponent that is connected to this particular instance.
		RT_RoleMask = RT_BroadcastToAll, //[0111 1111] Not intended to be used as a broadcast type. This is a mask used to filter out the NetOwner flag from NetworkRoles.
		RT_NetOwnerFlag = 0x80, //[1000 0000] Not intended to be used as a broadcast type. This is a bit flag used for combining Roles with NetOwnership.
		RT_NetOwnerClients = RT_NetOwnerFlag | NetworkSignature::NR_Client, //This RPC will only be invoked on NetOwner clients.
		RT_NetOwnerServers = RT_NetOwnerFlag | NetworkSignature::NR_Server, //This RPC will only be invoked on NetOwner servers.
		RT_NetOwnerMasterServers = RT_NetOwnerFlag | NetworkSignature::NR_MasterServer, //This RPC will only be invoked on NetOwner master servers.
		RT_NetOwnerServices = RT_NetOwnerFlag | NetworkSignature::NR_Service, //This RPC will only be invoked on NetOwner services.
		RT_NetOwnerOthers = RT_NetOwnerFlag | NetworkSignature::NR_Other, //This RPC will only be invoked on other connections that are also network owners or authoritative.
		RT_NetOwnersOnly = 0xFF //[1111 1111] This RPC will only be invoked on all NetworkComponents that are either NetOwners or Authoritative.
	};

	enum ERpcAvailability
	{
		RA_Available, //This RPC can be executed from the given Network Signature.
		RA_Unavailable, //This local process is not accepting remote calls due to its current role.
		RA_NotPermitted, //The given Network Signature does not have permission to execute this RPC.
	};

	enum ERpcResult : unsigned char
	{
		RR_Success,
		RR_InvalidBuffer, //The buffer does not contain the appropriate bytes to read the parameters.
		RR_InvalidCall, //The owner determined that the Rpc call is invalid and will be ignored.
		RR_NotPermitted, //The remote client is attempting to reference an object they don't have permission to access.
		RR_Internal //There is a bug in this process that prevented it from processing the RPC.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Lambda used to execute the RPC Implementation based on the parameters read from the given data buffer. */
	std::function<ERpcResult(NetworkSignature* /*sentFrom*/, const DataBuffer& /*params*/)> OnExecuteBuffer;

	/* Roles that are allowed to receive remote calls. If the local NetworkSignature does not have any of the roles, it'll ignore all remote calls.
	Local executions are still permitted. By default, this is available for any role. */
	NetworkSignature::ENetworkRole CanBeExecutedOn;

	/* Roles that are permitted to execute this RPC. A rejection message occures if a NetworkSignature without any of these roles is attempting to invoke this RPC.
	This is different from NetworkSignature's role permissions where permissions are global permissions. This enumerator is for handling a permissions
	at a per RPC basis. By default, this can be executed from any role. Even if this RPC can be executed from all, the NetworkSignature must still have
	the correct global permissions to execute RPCs. */
	NetworkSignature::ENetworkRole CanBeExecutedFrom;

protected:
	/* NetworkComponent that owns this instance. */
	NetworkComponent* NetComp;

	/* The remote NetworkSignature that's responsible for invoking this RPC. This is only assigned while the ConnectionManager is currently executing this RPC.
	This pointer automatically clears after execution.
	This is useful if there's a need to obtain a reference during OnExecute callback to the remote process that's responsible for executing this RPC. */
	NetworkSignature* InstigatedFrom;

	/* Unique hash of the function name that is used to quickly find the RPC in the remote process' hash.
	Defaults to zero if a hash is not generated. */
	size_t RpcHash;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	BaseRpc ();
	virtual ~BaseRpc ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this RPC can be called from source and executed on destination.
	 */
	virtual ERpcAvailability IsAvailableFor (NetworkSignature* calledFrom, NetworkSignature* executedOn) const;

	/**
	 * Returns true if this RPC is bound to a function.
	 */
	inline bool IsValid () const
	{
		return (RpcHash != 0);
	}

	/**
	 * Returns a friendly name that may help the developer understand this particular RPC instance.
	 */
	virtual DString ToString () const = 0;

	/**
	 * Locally processes the bound delegate: OnExecuteBuffer
	 * That delegate should extract parameters from the given data buffer before locally calling the actual function.
	 * During this function's execution, the InstigatedFrom pointer is assigned to sentFrom.
	 */
	virtual ERpcResult ExecuteBuffer (NetworkSignature* sentFrom, const DataBuffer& params);

	
	/*
	=====================
	  Mutators
	=====================
	*/

protected:
	/**
	 * Replaces the NetOwner with the new pointer.
	 * Call NetworkComponent::RegisterRpc to set the NetOwner.
	 */
	virtual void SetNetComp (NetworkComponent* newNetComp);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline NetworkSignature* GetInstigatedFrom () const
	{
		return InstigatedFrom;
	}

	inline size_t GetRpcHash () const
	{
		return RpcHash;
	}

	friend class NetworkComponent;
};

DEFINE_ENUM_FUNCTIONS(BaseRpc::ERpcType)
SD_END