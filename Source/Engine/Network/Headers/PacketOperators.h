/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PacketOperators.h
  Defines a series of operators to interface SD types with the sf::Packet.
=====================================================================
*/

#pragma once

#include "Network.h"

SD_BEGIN
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Bool& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const DString& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Float& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Int& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Rotator& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Vector2& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const Vector3& data);
NETWORK_API sf::Packet& operator<< (sf::Packet& packet, const DataBuffer& data);

NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Bool& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, DString& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Float& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Int& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Rotator& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Vector2& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, Vector3& outData);
NETWORK_API sf::Packet& operator>> (sf::Packet& packet, DataBuffer& outData);
SD_END