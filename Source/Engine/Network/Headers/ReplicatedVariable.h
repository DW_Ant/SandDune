/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReplicatedVariable.h

  A templated object responsible for sending and receiving a particular
  variable from a data buffer.

  This can only replicate DProperties primarily because it interfaces with DataBuffers.
=====================================================================
*/

#pragma once

#include "BaseReplicatedVariable.h"
#include "NetworkSignature.h"

SD_BEGIN
template <class V>
class ReplicatedVariable : public BaseReplicatedVariable
{


	/*
	=====================
	  Struct
	=====================
	*/

protected:
	struct SClientState
	{
		/* The value of Source back when it sent or received data. */
		V LatestSyncData;

		/* Timestamp when this client sent or received an update (in seconds). */
		Float LastSyncTime;

		SClientState () :
			LastSyncTime(-1.f)
		{
			//Noop
		}

		SClientState (const V& inLatestSyncData, Float inLastSyncTime) :
			LatestSyncData(inLatestSyncData),
			LastSyncTime(inLastSyncTime)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If bounded then this lambda will check if the received data permits the given input.
	Source will be written to only when the lambda returns true. Otherwise, it'll send a rejection packet.
	This is primarily used for cheat detection and to ensure the incoming data is valid. */
	SDFunction<bool, const V& /*pendingData*/> OnValidateData;

protected:
	/* Reference to the actual variable on an owning that will receive or send updates from a remote process.
	Being that this variable interfaces with DataBuffers, only DProperties are supported. */
	V* Source;

	/* A map of client data used to track if the client needs a variable update or not. The key of the map is the NetworkSignature's NetId. */
	std::unordered_map<Int, SClientState, IntKeyHash, IntKeyEqual> ClientSyncData;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ReplicatedVariable (V* inSource) : BaseReplicatedVariable(),
		Source(inSource)
	{
		CHECK(Source != nullptr)
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool ShouldReplicate (NetworkSignature* localSignature, NetworkSignature* destination, NetworkComponent* owningComp, Float engineTimestamp) const override
	{
		if (!BaseReplicatedVariable::ShouldReplicate(localSignature, destination, owningComp, engineTimestamp))
		{
			return false;
		}

		if (Source == nullptr || destination == nullptr)
		{
			return false;
		}

		if (!ClientSyncData.contains(destination->GetNetId()))
		{
			//destination will be added to ClientSyncData after the replication.
			return ((ReplicationFlags & RF_SyncOnInit) > 0 || (ReplicationFlags & RF_SyncOnChange) > 0);
		}

		const SClientState& clientState = ClientSyncData.at(destination->GetNetId());
		if ((ReplicationFlags & RF_SyncOnInit) > 0 && clientState.LastSyncTime <= 0.f)
		{
			return true;
		}

		if ((engineTimestamp - clientState.LastSyncTime) < SyncInterval)
		{
			return false;
		}

		if ((ReplicationFlags & RF_SyncOnChange) > 0 && (*Source) != clientState.LatestSyncData)
		{
			return true;
		}

		return false;
	}

	virtual void ReplicateVariable (NetworkSignature* destination, Float engineTimestamp, DataBuffer& outVarDataPack) override
	{
		BaseReplicatedVariable::ReplicateVariable(destination, engineTimestamp, OUT outVarDataPack);

		CHECK(Source != nullptr)
		outVarDataPack << (*Source);

		if (destination != nullptr)
		{
			try
			{
				SClientState& client = ClientSyncData.at(destination->GetNetId());
				client.LatestSyncData = (*Source);
				client.LastSyncTime = engineTimestamp;
			}
			catch (std::out_of_range&)
			{
				//Destination is not found. Add to the map
				ClientSyncData.insert({destination->GetNetId(), SClientState(*Source, engineTimestamp)});
			}
		}
	}

protected:
	virtual EReplicationResults WriteVariableFromBuffer (NetworkSignature* source, Float engineTimestamp, const DataBuffer& varDataPack) override
	{
		CHECK(Source != nullptr)
		if (!varDataPack.CanReadBytes(Source->GetMinBytes()))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to extract variable from data buffer since there aren't enough bytes in the data buffer to extract."));
			return RR_InvalidBuffer;
		}

		if (OnValidateData.IsBounded())
		{
			//Perform an extra copy and test the pending data before writing to the original source.
			V pendingData;
			if ((varDataPack >> pendingData).HasReadError())
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to extract replicated variable from data buffer since the data is malformed."));
				return RR_InvalidBuffer;
			}

			if (!OnValidateData(pendingData))
			{
				return RR_InvalidData;
			}

			(*Source) = pendingData;
		}
		else
		{
			if ((varDataPack >> (*Source)).HasReadError())
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to extract replicated variable from data buffer since the data is malformed."));
				return RR_InvalidBuffer;
			}
		}

		if (source != nullptr)
		{
			try
			{
				SClientState& client = ClientSyncData.at(source->GetNetId());
				client.LatestSyncData = (*Source);
				client.LastSyncTime = engineTimestamp;
			}
			catch (std::out_of_range&)
			{
				ClientSyncData.insert({source->GetNetId(), SClientState((*Source), engineTimestamp)});
			}
		}

		return RR_Success;
	}
};
SD_END