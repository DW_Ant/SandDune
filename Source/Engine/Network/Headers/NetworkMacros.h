/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkMacros.h
  Defines commonly used macros related to the Network module.

  One of the most significant macros are for the Rpcs. The sole reason behind using
  a macro is because RPCs need to be able to assemble variadic template arguments as
  a local variable instead of a parameter, but something like that is not possible
  in C++. So a lambda is used to convert a data buffer into a variadic template argument.

  In order to reduce boilerplate code (and ensure type safety), preprocessor macros are used.
=====================================================================
*/

#pragma once

#include "Network.h"

/**
 BIND_RPC macros - convenience macro that binds the buffer converter and the actual RPC in one line.
 This also ensures the parameters match up.

 const ref variables are not supported with these macros. If there's a need to use const ref parameters,
 don't use these macros and bind the delegate and lambda manually.

 Common mistakes to look out for when using these macros.
 * Make sure the function handler's name is equal to the RPC Name with "_Implementation" appended to the end of it.
 * Make sure the function handler returns a bool. This bool should be true if the callback finds the remote call valid.
 * Make sure the function handler parameters are not const ref or ref. This is because OnExecuteBuffer assumes pointers or copies.
 If there's a need to use ref, OnExecuteBuffer and BindRpc can be manually set.
*/
#define BIND_RPC(netComp, rpcName, functionOwner, className) \
	rpcName##.BindRpc(SDFUNCTION(##functionOwner##, className##, rpcName##_Implementation, bool)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		return rpcName##.LocalExecute(); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_1PARAM(netComp, rpcName, functionOwner, className, param1) \
	rpcName##.BindRpc(SDFUNCTION_1PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

/**
 * Same as BIND_RPC_1PARAM except the parameter is a const reference.
 * For this macro, be sure you do NOT include the "const &" in the parameter since one is automatically added.
 */
#define BIND_RPC_1REF_PARAM(netComp, rpcName, functionOwner, className, param1) \
	rpcName##.BindRpc(SDFUNCTION_1PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, const param1##&)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_2PARAM(netComp, rpcName, functionOwner, className, param1, param2) \
	rpcName##.BindRpc(SDFUNCTION_2PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##, param2##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		param2 b; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a, OUT b); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a, b); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_3PARAM(netComp, rpcName, functionOwner, className, param1, param2, param3) \
	rpcName##.BindRpc(SDFUNCTION_3PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##, param2##, param3##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		param2 b; \
		param3 c; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a, OUT b, OUT c); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a, b, c); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_4PARAM(netComp, rpcName, functionOwner, className, param1, param2, param3, param4) \
	rpcName##.BindRpc(SDFUNCTION_4PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##, param2##, param3##, param4##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		param2 b; \
		param3 c; \
		param4 d; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a, OUT b, OUT c, OUT d); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a, b, c, d); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_5PARAM(netComp, rpcName, functionOwner, className, param1, param2, param3, param4, param5) \
	rpcName##.BindRpc(SDFUNCTION_5PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##, param2##, param3##, param4##, param5##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		param2 b; \
		param3 c; \
		param4 d; \
		param5 e; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a, OUT b, OUT c, OUT d, OUT e); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a, b, c, d, e); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);

#define BIND_RPC_6PARAM(netComp, rpcName, functionOwner, className, param1, param2, param3, param4, param5, param6) \
	rpcName##.BindRpc(SDFUNCTION_6PARAM(##functionOwner##, className##, rpcName##_Implementation, bool, param1##, param2##, param3##, param4##, param5##, param6##)); \
	rpcName##.OnExecuteBuffer = [&](NetworkSignature* sentFrom, const DataBuffer& buffer) \
	{ \
		param1 a; \
		param2 b; \
		param3 c; \
		param4 d; \
		param5 e; \
		param6 f; \
		BaseRpc::ERpcResult result = rpcName##.ReadParam(sentFrom, buffer, OUT a, OUT b, OUT c, OUT d, OUT e, OUT f); \
		if (result != BaseRpc::RR_Success) \
		{ \
			return result; \
		} \
		\
		return rpcName##.LocalExecute(a, b, c, d, e, f); \
	}; \
	netComp##->RegisterRpc(&##rpcName##);