/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ConnectionManager.h
  This component is responsible for maintaining all active connections to external
  processes while also sending and receiving netpackets in batches. Most network
  communication goes through this Entity.

  This Entity communicates with all NetworkComponents to identify when a connection
  should be severed and what data should be transferred across the network.

  This Entity also sends and receives heartbeat events to identify ping, packetloss,
  and when a connection timed out.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"
#include "PacketsConnectionManager.h"

/* Maximum allowable number of connections a process may have at the same time.
Typically clients will only need one connection, but servers would have a lot more. */
#ifndef SD_NETWORK_MAX_CONNECTIONS
#define SD_NETWORK_MAX_CONNECTIONS 1024
#endif

#ifndef SD_NETWORK_LATEST_PINGS_SIZE
#define SD_NETWORK_LATEST_PINGS_SIZE 10
#endif

SD_BEGIN
class NetworkRoleComponent;
class NetworkComponent;
class NetworkMonitor;

class NETWORK_API ConnectionManager : public Entity
{
	DECLARE_CLASS(ConnectionManager)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* List of default ports used for SD applications. These are only defaults. Developers are permitted to use their own. */
	enum EDefaultPorts : unsigned short
	{
		DP_Any = 0, //Same as SFML's any port. Searches for any port available.
		DP_Game = 2011, //Used for end users and servers. 2011 is not a well known port, and the Internet Assigned Numbers Authority did not reserve this port. Also same release year for Monster Evo.
		DP_Service = 2012, //Used for external processes and web servers to interact with a server. Not meant for end users.
		DP_MasterServer = 2013, //Used for servers that need to coordinate with other servers.
	};

	/* Various unique messages that instructs the manager how to behave.
	Enums beginning with 'Req' are requests. These requests expect a response.
	Enums beginning with 'Resp' are responses to a request.
	Enums beginning with 'Msg' are messages that are not expecting a response.
	
	This enum is the header ID the Manager will use to figure out what to do with this information.
	It's important that the first bit is 0 and the last bit is a 1; this is used to detect endianness. */
	enum ENetMessageType : unsigned char
	{
		NMT_Unknown = 0, //[0000 0000] - Invalid header

		NMT_Req_OpenConnection = 3, //[0000 0011] - Remote process would like to open up a connection to this process.
		NMT_Resp_ConnectionAccepted = 5, //[0000 0101] - This process accepted the connection request.
		NMT_Resp_ConnectionRejected = 7, //[0000 0111] - This process refuses to connect to the remote process.

		NMT_Req_InstantiateObj = 31, //[0001 1111] - Remote process is requesting that this process should instantiate an object with a Network Component.
		NMT_Resp_AcceptInstantiation = 33, //[0010 0001] - The process accepted and instantiated an Entity with a Network Component paired with the instigating Network Component.
		NMT_Resp_RejectInstantiation = 35, //[0010 0011] - The process refuses to instantiate an Entity
		NMT_Msg_ObjLoseRelevance = 37, //[0010 0101] - The authoritative process is informing a client that a particular Entity is no longer relevant to them. That Entity will no longer receive net updates.

		NMT_Msg_NetCompData = 51, //[0011 0011] - Data is being sent from a Network Component (such as variable replication).
		NMT_Resp_RejNetCompData = 53, //[0011 0101] - (Rejected Network Component Data) - The process is not authorized to update the remote process' variables.

		NMT_Msg_NetCompRpc = 71, //[0100 0111] - The process is invoking a NetworkComponent's RPC function.
		NMT_Resp_RejNetCompRpc = 73, //[0100 1001] - (Rejected Network Component RPC) - The process is not authorized to make the RPC.

		NMT_Msg_RemoveNetComp = 91, //[0101 1011] - The process is removing a NetworkComponent.

		NMT_Req_Heartbeat = 111, //[0110 1111] - Process is requesting a hearbeat response to see if the connection is still alive.
		NMT_Resp_Heartbeat = 113, //[0111 0001] - Process acknowledges and responds to the Heartbeat event.
	};


	/*
	=====================
	  Struct
	=====================
	*/

protected:
	struct SSocketData
	{
		enum ESocketStatus : unsigned char
		{
			SS_Dormant, //A listener is not using this socket, and this socket is not connected to anything.
			SS_Listening, //This socket is not connected to anything, but a TcpListener is currently using this.
			SS_Active //This socket has an active connection to a remote process.
		};
		
		ESocketStatus Status;
		sf::TcpSocket Socket;

		SSocketData ();
	};

	struct SListenerData
	{
		sf::TcpListener Listener;

		/* The socket this listener is currently observing (prevent multiple TcpListeners from observing the same socket). */
		SSocketData* Socket;

		SListenerData (SSocketData* inSocket);
		void operator= (const SListenerData& cpyObj) = delete;
	};

	struct SPacketData final
	{
		ENetMessageType PacketType;
		ConnectionManagerPacket* Packet;

		SPacketData (ENetMessageType inPacketType, ConnectionManagerPacket* inPacket);
		~SPacketData ();
	};

	/* Data struct that maps to an IP Address to a timestamp to when they attempted to connect. */
	struct SConnectionAttempt
	{
		sf::IpAddress Address;

		/* The timestamp when this address attempted to connect to the engine (in seconds). */
		Float ConnectionTime;

		SConnectionAttempt (const sf::IpAddress& inAddress, Float inConnectionTime);
	};

	/* Data struct containing heartbeat related information. */
	struct SHeartbeatData
	{
		/* If true, then this heartbeat has sent out a data packet and is waiting for a response. Used for detecting timeout. */
		bool bWaitingResponse;

		/* Latest ID that was sent out, and is currently waiting for a response with matching ID. */
		Int LatestHeartbeatId;

		/* Timestamp when the heartbeat was last sent out (in seconds). Used for calculating ping. */
		Float LatestSentTimestamp;

		/* Timestamp when the expected heartbeat ID was received (in seconds). */
		Float LatestReceivedTimestamp;

		/* List of latest computed pings (in seconds). */
		Float LatestPings[SD_NETWORK_LATEST_PINGS_SIZE];

		/* Index of the LatestPing that contains the oldest entry. The oldest entry is going to be written on next heartbeat event. */
		unsigned int LatestPingsIdx;

		/* Average value of each entry in the Latest pings. Ignores all values with 0. */
		Float AvgPing;

		SHeartbeatData ();
	};

	/* A data struct pairing a NetworkComponent to a destination with a TCP connection. */
	struct SActiveConnection
	{
		/* The signature that represents the process this is connected to. */
		NetworkSignature* Destination;

		/* The socket used to send data to destination. This points to one of the elements in the Sockets array. */
		SSocketData* Socket;

		/* The packets that are currently in transit. */
		sf::Packet InboundPacket;
		sf::Packet OutboundPacket;

		/* The accumulated data that is next to be pushed to the transit data whenever the Socket becomes available. */
		std::vector<SPacketData> PendingData;

		/* Becomes true if this connection is established and approved. Otherwise, assumed that this connection is still waiting for the other
		process to approve this connection. */
		bool bApprovedConnection;

		/* Timestamp when an establish connection request was sent out (in seconds).
		If negative, then the request is not yet sent out. */
		Float ConnectionRequestTime;

		/* The permissions this process has over the remote connection. Although the remote process will check them, this can be used as a preliminary check (save bandwidth by not bothering sending packets they don't have permisssion over). */
		NetworkGlobals::EPermissions LocalPermissions;

		/* The permissions the remote process has over this connection. */
		NetworkGlobals::EPermissions RemotePermissions;

		SHeartbeatData Heartbeat;

		/* When processing packets from this connection, each connection has a time limit in a single frame. If that time limit is reached, this variable is assigned
		to indicate which packet it left off on so it may continue next frame. If negative, then this connection is not in the middle of processing packets. */
		Int ReceivePacketNum;

		/* When this application couldn't process the abundance of packets within a time limit, this value specifies the number of packets pending to be processed.
		Otherwise it's assigned to a negative number. */
		Int TotalReceivePackets;

		/* If true, then this connection will terminate as soon as it finishes sending its pending data. This is typically set to true,
		when sending a response regarding a rejection or connection termination. */
		bool bTerminateOnSend;

		/* If true, then this data struct will be purged by the end of the current loop. */
		bool bPendingDelete;

		/* List of local Role Components that uses this connection. The map's key is equal to the component's NetId. */
		std::unordered_map<Int, NetworkRoleComponent*, IntKeyHash, IntKeyEqual> RoleComponents;

		/* List of components sharing this destination. The key to the map is equal to the NetworkComponent's ID. */
		std::unordered_map<Int, NetworkComponent*, IntKeyHash, IntKeyEqual> NetComponents;

		SActiveConnection (NetworkSignature* inDestination, SSocketData* inSocket);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Broadcasted whenever this process connected to a remote process. */
	MulticastDelegate<NetworkSignature* /*remoteSignature*/> OnConnectionAccepted;

	/* Broadcasted whenever this process' connection request was rejected. Applications receiving these requests never broadcasts this event even if the other disconnected. */
	MulticastDelegate<const sf::IpAddress& /*remoteAddr*/, Int /*reason*/> OnConnectionRejected;

	/* Broadcasted whenever a connection between this and a remote process is severed. This is only invoked on NetworkSignatures that have an established AND approved connection. */
	MulticastDelegate<NetworkSignature* /*remoteSignature*/> OnDisconnect;


protected:
	/* All network responses must respond to a request within this many seconds. Otherwise the ConnectionManager will assume the connection dropped.
	Setting this to a small value may lead to timeouts through laggy connections.
	Setting this to a large value may cause the end user to wait for a while before the application sends a timeout error message.
	If not positive, then this application will never timeout even if it means it'll wait for a response indefinitely. */
	Float TimeoutTime;

	/* When receiving network packets, each connection must process all packets under this time limit. If it exceeds this limit, the processing will pause until next frame.
	This is a failsafe to prevent clients from flooding the network, causing lag for others. This value is in seconds. The time limit is disabled if not positive.
	Setting this to a high duration would enable the server to freeze up to this time limit. Setting this to a low duration may cause receiving packets to be delayed (and potentially backed up). */
	Float ReceivePacketTimeLimit;

	/* Maximum number of bytes permitted to send when creating a open connection request. Should an external process send a DataBuffer beyond this limit,
	this process will drop the connection, and ignore its request. It wont even send a Reject response to let the other process timeout.*/
	Int OpenConnectionMaxSize;

	/* If true, then this object will check for duplicate clients. If a connection request is made with an ip address that's already connected, then
	ConnectionManager will reject the request. */
	bool bRejectDuplicateConnections;

	/* Determines how frequently this process will send heartbeats to each process (in seconds). */
	Float HeartbeatSendInterval;

	/* Any process that makes a connection request to this process, this process will only accept connections that will grant them these permissions.
	Typically servers and trusted clients will have this set to max (all permissions). */
	NetworkGlobals::EPermissions ListenerPermissions;

	/* List of permissions granted to each connection type. If a client is connecting with multiple types, then the permissions are combined for each type. */
	std::vector<std::pair<NetworkSignature::ENetworkRole, NetworkGlobals::EPermissions>> RolePermissions;

	/* Whenever this process makes a connection request, the remote process must give this process these sets of permissions.
	Typically clients will have this set to either P_None or P_Client. */
	NetworkGlobals::EPermissions DesiredPermissions;

	/* List of TCP sockets that are listening for new connections. These are pointers since the TcpListeners are not copiable. */
	std::vector<std::shared_ptr<SListenerData>> Listeners;

	/* List of TcpSockets that are waiting for a client or is an active connection to another process.
	This is a raw dynamic array since the SActiveConnections struct contains a pointer to one of these elements. */
	SSocketData Sockets[SD_NETWORK_MAX_CONNECTIONS];

	/* List of every connection. Each element in the container must have a unique destination.
	The key in the map is equal to the Connection's destination's NetId. */
	std::unordered_map<Int, SActiveConnection, IntKeyHash, IntKeyEqual> Connections;

	/* List of IP addresses that recently attempted (and possibly successfully) connected to this process.
	The purpose behind this is to protect against brute force attacks by adding a time limit between each connection
	attempt. They are permitted to reconnect once enough time elapsed. The timer does not reset if their connection is
	rejected because it's suspended. */
	std::vector<SConnectionAttempt> RecentAttempts;

	/* List of Entities that were instantiated from remote processes. These Entities will simulate on their own.
	Anything in this list will be destroyed whenever the ConnectionManager is removed. */
	std::list<DPointer<Entity>> InstantiatedEntities;

	/* If positive then this is the number of seconds required to elapse before clients are allowed to make another attempt to connect to this process.
	Although this may protect against brute force attacks, client using VPNs can bypass this. */
	Float ConnectionAttemptInterval;

	/* TickComponent that's responsible for checking the active connections, requests, and heartbeat events. It may generate timeout events. */
	TickComponent* HeartbeatTick;

	/* TickComponent that's responsible for kicking off the net requests. */
	DPointer<TickComponent> NetProcessTick;

	/* Entity that'll be responsible for kicking clients from this process if they send too many incorrect packets in a short period of time. */
	NetworkMonitor* NetMonitor;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Obtains the connection manager from the network engine component residing in the current thread.
	 */
	static ConnectionManager* GetConnectionManager ();

	/**
	 * Opens up a port to listen for incoming connections.
	 * Generally only servers listen for connections.
	 * Returns true on success and if it wasn't listening on that port before.
	 */
	virtual bool OpenPort (unsigned short port = DP_Game);

	/**
	 * Closes the specified port. Returns true if found and closed.
	 */
	virtual bool ClosePort (unsigned short port);

	/**
	 * Iterates and shutsdown every port that this process opened.
	 */
	virtual void CloseAllPorts ();

	/**
	 * Attempts to establish a connection to the specified address/destination.
	 * This function is an async function where it'll construct a network packet to generate a connection request.
	 * The signature is the identity of this process. The remote process will need that signature to authenticate.
	 * It is up to the destination to accept the connection.
	 * The OnConnectionAccepted or OnConnectionRejected delegates will be broadcasted based on what the remote process returns.
	 */
	virtual void OpenConnection (const sf::IpAddress& destination, unsigned short port = DP_Game);

	/**
	 * Notifies the specified client about an Entity's existance. This process must have the correct permissions to instantiate
	 * objects on the remote process.
	 */
	virtual void InstantiateEntity (NetworkRoleComponent* roleComp, NetworkSignature* destination, bool isDestinationNetOwner);

	/**
	 * Sends the given NetworkComponent's variable data across the network. It doesn't send immediately. Rather it appends to the pending data.
	 * NOTE: All replicated data automatically replicates whenever the pending data is empty. Call this function only when there's a need to replicate
	 * data immediately regardless of what's pending.
	 * NOTE: Be mindful of calling this frequently. PendingData could accumulate if requests are sent out faster than the connection throughput.
	 */
	virtual void ReplicateVariables (NetworkSignature* destination, NetworkComponent* netComp, const DataBuffer& varData);

	/**
	 * Sends a packet that will invoke a function on the specified destination.
	 * Returns true if it added a packet to the PendingList, and it'll eventually send that packet out.
	 */
	virtual bool ExecuteRpc (NetworkSignature* destination, NetworkComponent* netComp, size_t functionHash, const DataBuffer& params);

	/**
	 * Simply unregisters every reference this manager has on it. This function will NOT destroy the NetworkComponent, nor will it
	 * send packets to notify remote clients. This function is primarily used for local cleanup purposes.
	 */
	virtual void UnregisterNetComponent (NetworkComponent* targetComp);

	/**
	 * Notifies the specified client that an Entity is no longer giving it network updates.
	 * This will also sever the connection between the local role component from the one on the remote process.
	 */
	virtual void DisconnectEntity (NetworkRoleComponent* roleComponent, NetworkSignature* remoteSignature, Int externalNetId);

	/**
	 * Severs the connection from this process before destroying the pointer reference.
	 * If bForceTerminate is true, then this process will disconnect regardless if it finished sending packets or not.
	 */
	virtual void Disconnect (NetworkSignature* disconnectFrom, bool bForceTerminate = false);

	/**
	 * Disconnects this process from everything it's connected to.
	 * If bForceTerminate is true, then this process will disconnect regardless if it finished sending packets or not.
	 */
	virtual void DisconnectFromAll (bool bForceTerminate);

	/**
	 * Searches through the instantiated entities list. If it's found, it'll remove from the list.
	 * The purpose behind this function is to allow external systems (within this process) to claim
	 * ownership over the specfied Entity.
	 * The NetworkComponents can still communicate even if the ConnectionManager no longer has a reference
	 * to the base Entity.
	 *
	 * Typical examples when to remove ownership.
	 * Case 1:
	 * A projectile Entity that will automatically perish at some point (either collision or timeout). In
	 * this case there's no need for the ConnectionManager to hold ownership primarily because it'll inevitably perish.
	 *
	 * Case 2:
	 * A map Entity that should be purged when the map expires. In this case, the map will claim ownership over
	 * this Entity. Should the ConnectionManager expire before the map, this entity will persist until the map expires.
	 * Should the map expire, this Entity will purge.
	 *
	 * Complexity is linear.
	 * Returns true if the Entity was found and removed from the list.
	 */
	virtual bool RemoveOwnershipFromEntity (Entity* targetEntity);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTimeoutTime (Float newTimeoutTime);
	virtual void SetReceivePacketTimeLimit (Float newReceivePacketTimeLimit);
	virtual void SetRejectDuplicateConnections (bool bNewRejectDuplicateConnections);
	virtual void SetListenerPermissions (NetworkGlobals::EPermissions newListenerPermissions);
	virtual void SetRolePermissions (NetworkSignature::ENetworkRole netRole, NetworkGlobals::EPermissions newPermissions);
	virtual void SetDesiredPermissions (NetworkGlobals::EPermissions newDesiredPermissions);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetTimeoutTime () const
	{
		return TimeoutTime;
	}

	inline Float GetReceivePacketTimeLimit () const
	{
		return ReceivePacketTimeLimit;
	}

	inline bool IsRejectingDuplicateConnections () const
	{
		return bRejectDuplicateConnections;
	}

	inline NetworkGlobals::EPermissions GetListenerPermissions () const
	{
		return ListenerPermissions;
	}

	inline const std::vector<std::pair<NetworkSignature::ENetworkRole, NetworkGlobals::EPermissions>>& ReadRolePermissions () const
	{
		return RolePermissions;
	}

	inline std::vector<std::pair<NetworkSignature::ENetworkRole, NetworkGlobals::EPermissions>>& EditRolePermissions ()
	{
		return RolePermissions;
	}

	inline NetworkGlobals::EPermissions GetDesiredPermissions () const
	{
		return DesiredPermissions;
	}

	/**
	 * Obtains the permissions this process has over the remote process.
	 */
	NetworkGlobals::EPermissions GetLocalPermissions (NetworkSignature* remoteProcess) const;

	/**
	 * Obtains the permissions the remote process has over this process.
	 */
	NetworkGlobals::EPermissions GetRemotePermissions (NetworkSignature* remoteProcess) const;

	inline size_t GetNumConnections () const
	{
		return Connections.size();
	}

	inline TickComponent* GetNetProcessTick () const
	{
		return NetProcessTick.Get();
	}

	void GetListeningPorts (std::vector<unsigned short>& outListeningPorts) const;

	/**
	 * Attempts to find the NetworkRoleComponent that is connected to the given remoteProcess.
	 * It'll return the specific role component that has a connection to another with its Net ID equal to the given external ID.
	 */
	NetworkRoleComponent* FindRoleCompFromExternalId (NetworkSignature* remoteProcess, Int roleCompExternalId) const;

	/**
	 * Returns the average ping between this process and the given signature (in seconds).
	 * If this process is not connected to the given signature, or if this connection manager does not recognize that signature, then it'll return a negative value.
	 */
	virtual Float GetPing (NetworkSignature* remoteProcess) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each listener to process their status.
	 * If they receive a new connection, it will create a new SActiveConnection to be processed for approval.
	 */
	virtual void ProcessListeners ();

	/**
	 * Iterates through all network requests from NetworkComponents in this process.
	 * This function may process the following:
	 * -Establish new connections if the destination is not recognized.
	 * -Group new NetworkComponents to existing connections if the establish connection request is recognized.
	 * -Send data out to the network for NetworkComponents with active connections.
	 * -Sever any connections requested by NetworkComponents.
	 */
	virtual void ProcessSendPackets ();

	/**
	 * Iterates through each NetworkComponent for the given connection, and adds pending packets based
	 * on the RPC's and variables that need to be replicated.
	 */
	virtual void GatherSendPackets (NetworkSignature* localSignature, SActiveConnection& outConnection);

	/**
	 * Processes all data received from external processes. This function branches off to various Processing functions based
	 * on the message type.
	 */
	virtual void ProcessReceivePackets ();

	/**
	 * Iterates through each connection. If any connection is currently waiting to the other process to approve its request,
	 * this function will check its request time in order to see if it should timeout or not.
	 */
	virtual void CheckConnectionRequests ();

	/**
	 * Iterates through each connection to see if any of them should timeout due to lack of heartbeat responses.
	 * This also checks if it's time to send another heartbeat event.
	 */
	virtual void CheckHeartbeats ();

	/**
	 * The given ip address is a client attempting to connect to this process.
	 * This function updates the RecentAttempts vector by removing older entries.
	 * This will also check if the given address already recently connected.
	 * Returns true, if this ip address is permitted to connect (not suspended).
	 */
	virtual bool ProcessNewConnection (const sf::IpAddress& newConnectionAddr);

	/**
	 * An external process is requesting to connect to this process. Typically servers handle this function.
	 *
	 * This function will query the AccessControl to see if a connection should be made or not.
	 * This function should send a Accept or Deny packet back to the requester.
	 */
	virtual void ProcessConnectionRequest (SActiveConnection& outConnection, const PacketOpenConnection& openRequest);

	/**
	 * The external process accepted this process's request to connect. Typically clients handle this function.
	 *
	 * This function will update the ConnectionManager and NetworkEngineComponent of the available connection.
	 */
	virtual void ProcessConnectionAccepted (SActiveConnection& outConnection, const PacketAcceptConnection& acceptConnection);

	/**
	 * The external process rejected this process's request to connect. Typically clients handle this function.
	 *
	 * This function will update the ConnectionManager to terminate pending requests.
	 */
	virtual void ProcessConnectionRejected (SActiveConnection& outConnection, const PacketRejectConnection& rejectConnection);

	/**
	 * An external process is requesting that this process should create a NetworkComponent that'll communicate with another.
	 * Typically the client handles this function.
	 *
	 * This function will query the NetPermissions object to identify if it should instantiate a NetworkComponent or not.
	 * If accepted, it'll locally create a NetworkComponent that'll communicate with the paired component.
	 * It should then either send an accept or deny message.
	 */
	virtual void ProcessInstantiateNetObj (SActiveConnection& outConnection, const PacketInstantiateObj& netCompReq);

	/**
	 * The request to instantiate an Entity on the remote process is accepted. This function handles that response.
	 * Typically servers handle this function.
	 *
	 * This function essentially pairs up the NetworkComponents with the Ids returned from the remote process.
	 */
	virtual void ProcessAcceptNetObj (SActiveConnection& outConnection, const PacketAcceptInstantiateObj& acceptedObj);

	/**
	 * The request to instantiate an Entity on the remote process is denied. This function handles that response.
	 * Typically servers handle this function.
	 *
	 * This function notifies the instigating NetworkRoleComponent that their pending request is denied. It should drop its client.
	 */
	virtual void ProcessRejectNetObj (SActiveConnection& outConnection, const PacketRejectInstantiateObj& rejectedObj);

	/**
	 * The remote process is notifying this process that one of its entities are being disconnected.
	 * Typically clients handle this function due to net relevance, but servers could potentially handle this function due to disconnecting clients.
	 *
	 * This function essentially disconnects the local with the matching NetId entity from the network.
	 * It'll also notify that Entity that it will not receive any more data from the network suggesting that they may have lost net relevance.
	 */
	virtual void ProcessDisconnectEntity (SActiveConnection& outConnection, const PacketDisconnectEntity& disconnectData);

	/**
	 * The remote process is attempting to send new data to a particular NetworkComponent. Typically the server and clients handle this function.
	 *
	 * This function should query the associated NetworkComponent to identify if the remote process has permission to send that data.
	 * If they do have permission, it's up to the NetworkComponent to interpret the data.
	 * If they do not have permission, the NetworkComponent should (but not required) send a message that their data request was rejected.
	 */
	virtual void ProcessNetCompData (SActiveConnection& outConnection, const PacketNetCompData& compData);

	/**
	 * The remote process rejected this process's attempt to send data to a particular NetworkComponent. Typically the server and clients handle this function.
	 *
	 * This function sends the information back to the original NetworkComponent.
	 * It is up to the NetworkComponent to react to this event. Typically they would ignore this event and carry out even though they are desync'd.
	 */
	virtual void ProcessRejectNetCompData (SActiveConnection& outConnection, const PacketRejectNetCompData& rejectData);

	/**
	 * The remote process is attempting to call a NetworkComponent's function in this instance. Typically the server and clients handle this function.
	 *
	 * This function will direct this message to the associated NetworkComponent.
	 * The NetworkComponent is responsible for checking if the remote process as permission to invoke the call as well as verifying the parameters.
	 * Should there should be an issue, the NetworkComponent should send a rejection message. Otherwise the NetworkComponent is responsible for locally calling the function.
	 */
	virtual void ProcessNetCompRpc (SActiveConnection& outConnection, const PacketNetCompRpc& netCompRpc);

	/**
	 * The remote process is rejecting the Remote Procedure Call.
	 *
	 * The NetworkComponent responsible for generating the request should handle this error.
	 * Typically NetworkComponents will proceed as normal even though the RPC wasn't executed.
	 */
	virtual void ProcessRejectNetCompRpc (SActiveConnection& outConnection, const PacketRejectNetCompRpc& rejectedRpc);

	/**
	 * The remote process is terminating its NetworkComponent's connection to this process.
	 *
	 * This function should relay this information to its associated NetworkComponent to notify it to sever connection as well.
	 * A response message is not needed since it's assumed that the process sending this message will disconnect its NetworkComponent
	 * regardless how this function processes the message.
	 */
	virtual void ProcessDisconnectNetComp (SActiveConnection& outConnection, const PacketDisconnectNetComp& disconnectNetComp);

	/**
	 * A remote process is sending a heartbeat event. It is expected that this function should send a response to keep the connection alive.
	 * When sending the response, its Heartbeat ID should be equal to the ID of the original sender's heartbeat ID to help compute ping and estimate packetloss.
	 */
	virtual void ProcessSentHeartbeat (SActiveConnection& outConnection, const PacketSendHeartbeat& heartbeat);

	/**
	 * The remote process received and acknowledged this process's heartbeat event.
	 *
	 * This function should update the ping and estimate packet loss.
	 * Lastly it should update its heartbeat timer to prevent the connection from timing out.
	 */
	virtual void ProcessReceivedHeartbeat (SActiveConnection& outConnection, const PacketReceiveHeartbeat& heartbeat);

private:
	/**
	 * For each active connection that is waiting to be deleted, this function actually deletes them.
	 */
	virtual void RemovePendingDeleteConnections ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleHeartbeatCheck (Float deltaSec);
	virtual void HandleNetProcessTick (Float deltaSec);
	virtual void HandlePreGarbageCollection ();

	friend class SignatureIterator;
};

sf::Packet& operator<< (sf::Packet& packet, ConnectionManager::ENetMessageType msgType);
sf::Packet& operator>> (sf::Packet& packet, ConnectionManager::ENetMessageType& outMsgType);
SD_END