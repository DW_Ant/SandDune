/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkEngineComponent.h
  This component is responsible for maintaining all active connections to external
  processes while also sending and receiving netpackets in batches.

  The EngineComponent communicates with all NetworkComponents to understand what connections
  should be enabled, and when connections should expire. NetworkComponents that send data
  to the same destination will share a connection.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"

SD_BEGIN
class AccessControl;
class ConnectionManager;

class NETWORK_API NetworkEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(NetworkEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The ip address of this computer in the Local Area Network. */
	static sf::IpAddress LanAddress;

	/* The ip address of this computer seen from the external/distance computer's point of view. */
	static sf::IpAddress PublicAddress;

	/* This client's network signature that is used to inform remote processes who they are.
	GenerateLocalSignature must be called before this can be used. */
	DPointer<NetworkSignature> LocalSignature;

	/* The DClass used to instantiate the ConnectionManager Entity. This variable must be assigned prior to InitializeComponent. If null, then it'll default to the base ConnectionManager class. */
	const DClass* ConnectionManagerClass;

	/* The Entity responsible for bundling NetworkComponents with a common address to share a connection. */
	DPointer<ConnectionManager> Connections;

	/* List of objects that are responsible for determining which connection requests are approved or not.
	The NetworkEngineComponent takes ownership over these objects where it'll destroy these objects when shutting down. */
	std::vector<AccessControl*> AccessControls;

	/* If false, then IP addresses will be hidden in logs. */
	bool bAllowIpLogging;

	/* Only applicable prior to PostInitializeComponent. If true, then this component will automatically open ports based on its command line arguments. */
	bool bCmdLineOpenPorts;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	NetworkEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes a NetworkSignature that will identify this system.
	 * If a local NetworkSignature is already initialized, this function will replace that signature.
	 *
	 * @param networkRoles - Define the network signature intended role such as server, client, service, etc...
	 * See: NetworkSignature::NetworkRoles.
	 * @param identifierClasses - List of NetworkIdentifier classes to use for this signature. Typically servers
	 * may use different identifiers from clients. The important thing is that this list of identifiers must
	 * match the identifiers remote processes are expecting. If the remote process is unable to find these identifiers,
	 * they would probably reject all connection attempts.
	 *
	 * Returns true if the local signature is created and initialized.
	 */
	virtual bool InitializeLocalNetworkSignature (NetworkSignature::ENetworkRole networkRoles, const std::vector<const DClass*>& identifierClasses);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetConnectionManagerClass (const DClass* newConnectionManagerClass);
	virtual void SetCmdLineOpenPorts (bool bNewCmdLineOpenPorts);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static inline sf::IpAddress GetLanAddress ()
	{
		return LanAddress;
	}

	static inline sf::IpAddress GetPublicAddress ()
	{
		return PublicAddress;
	}

	inline NetworkSignature* GetLocalSignature () const
	{
		return LocalSignature.Get();
	}

	inline const std::vector<AccessControl*>& ReadAccessControls () const
	{
		return AccessControls;
	}

	inline std::vector<AccessControl*>& EditAccessControls ()
	{
		return AccessControls;
	}

	inline ConnectionManager* GetConnections () const
	{
		return Connections.Get();
	}

	inline bool IsIpLogginedPermitted () const
	{
		return bAllowIpLogging;
	}
};
SD_END