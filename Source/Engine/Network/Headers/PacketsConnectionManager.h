/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PacketsConnectionManager.h
  Defines a series of data structs that are used to send and receive data through
  network packets.
=====================================================================
*/

#pragma once

#include "Network.h"
#include "NetworkSignature.h"

SD_BEGIN
/* Parent struct of all ConnectionManager's packet types. */
struct ConnectionManagerPacket
{
private:
	/**
	 * Simple function that converts this class to a polymorphic type, which would allow dynamic_casting.
	 * Adding a parent class also allows for generic pointers to packets for containers.
	 */
	virtual int EnableDynamicCasting () const;
};

//=====================
/* Structs related to establishing a connection. */
struct PacketOpenConnection : public ConnectionManagerPacket
{
	/* The ip address of the process that generated this request. */
	sf::IpAddress RequesterIp;

	/* When generating a connection request, the client must specify what it's attempting to connect as. 
	This is to inform the other process that this connection request is for a client, service, etc...
	The other process it's attempting to connect to will look at this to determine if the connection should be accepted or not. */
	NetworkSignature::ENetworkRole RequesterRole;

	/* Additional data used when constructing Network Signatures with specific identifiers. The databuffer will have the following data:
	For each identifier class:
	<size_t> - Hash of the DClass string that maps to the DClass. This DClass is used to instantiate a NetworkIdentifier.
	<IdentitySpecificData> - The Identifier subclass will interpret this data to initialize its variables. */
	DataBuffer IdentifierData;

	PacketOpenConnection () :
		RequesterIp(sf::IpAddress::None),
		RequesterRole(NetworkSignature::NR_None)
	{
		//Noop
	}

	PacketOpenConnection (const sf::IpAddress& inRequesterIp, NetworkSignature::ENetworkRole inRequesterRole, const DataBuffer& inIdentifierData) :
		RequesterIp(inRequesterIp),
		RequesterRole(inRequesterRole)
	{
		inIdentifierData.CopyBufferTo(OUT IdentifierData);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketOpenConnection& openConnection);
sf::Packet& operator>> (sf::Packet& packet, PacketOpenConnection& outOpenConnection);

struct PacketAcceptConnection : public ConnectionManagerPacket
{
	/* The IP address of the computer that accepted this connection. */
	sf::IpAddress AcceptedIp;

	/* The role/connection types of the remote process that accepted the connection request. */
	NetworkSignature::ENetworkRole RemoteNetworkRoles;

	/* This is the permissions the remote process will have if they connect.
	It's a take it or leave it situation. The local process can terminate the connection if they deem the permissions are overreaching. */
	NetworkGlobals::EPermissions ListenerPermissions;

	/* This is the permissions for the process that instantiated the connection request. */
	NetworkGlobals::EPermissions RequesterPermissions;

	/* Additional data used when constructing a Network Signature about the process this connected to.
	This can be used to check against impersonation. Should this process connect to a computer it wasn't expecting, it can reject the connection. */
	DataBuffer RemoteIdentifierData;

	PacketAcceptConnection () :
		AcceptedIp(sf::IpAddress::None),
		RemoteNetworkRoles(NetworkSignature::NR_None),
		ListenerPermissions(NetworkGlobals::P_None),
		RequesterPermissions(NetworkGlobals::P_None)
	{
		//Noop
	}

	PacketAcceptConnection (const sf::IpAddress& inAcceptedIp, NetworkSignature::ENetworkRole inRemoteNetworkRoles, NetworkGlobals::EPermissions inListenerPermissions,
		NetworkGlobals::EPermissions inRequesterPermissions, const DataBuffer& inRemoteIdentifierData) :

		AcceptedIp(inAcceptedIp),
		RemoteNetworkRoles(inRemoteNetworkRoles),
		ListenerPermissions(inListenerPermissions),
		RequesterPermissions(inRequesterPermissions)
	{
		inRemoteIdentifierData.CopyBufferTo(OUT RemoteIdentifierData);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketAcceptConnection& acceptConnection);
sf::Packet& operator>> (sf::Packet& packet, PacketAcceptConnection& outAcceptConnection);

struct PacketRejectConnection : public ConnectionManagerPacket
{
	/* The IP address of the computer that rejected this connection. */
	sf::IpAddress RejectedIp;

	/* The number associated with a reason. This is useful for sending an identifier that maps to a string,
	which then the string could be used to translate into human readable text to their language.
	This is optional. A server does not have to explain why it refuses connection.
	The AccessControl class will define the reasons why it may refuse a connection. */
	Int ReasonSwitch;

	PacketRejectConnection () :
		RejectedIp(sf::IpAddress::None),
		ReasonSwitch(0)
	{
		//Noop
	}

	PacketRejectConnection (const sf::IpAddress& inRejectedIp, Int inReasonSwitch) :
		RejectedIp(inRejectedIp),
		ReasonSwitch(inReasonSwitch)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectConnection& rejectConnection);
sf::Packet& operator>> (sf::Packet& packet, PacketRejectConnection& outRejectConnection);


//=====================
/* Structs related to instantiating an Entity. */
struct PacketInstantiateObj : public ConnectionManagerPacket
{
	/* The id of the NetworkRoleComponent that generated this request. */
	Int NetId;

	/* The NetworkRoleComponent's owner hashed class name. The class instance is used as a template to create
	a copy of this on the remote process. */
	size_t OwnerClassHash;

	/* If true, then both processes are considered a network owner over this component where they are given
	additional permissions to interact with it.
	If false, then only the process that instantiated this request is the network owner. */
	bool bIsBothNetOwner;

	/* Data buffer that contains each Network Component's NetId. */
	DataBuffer NetCompNetIds;

	PacketInstantiateObj () :
		NetId(0),
		OwnerClassHash(0),
		bIsBothNetOwner(false)
	{
		//Noop
	}

	PacketInstantiateObj (Int inNetId, size_t inOwnerClassHash, bool inIsBothNetOwner, const DataBuffer& inNetCompNetIds) :
		NetId(inNetId),
		OwnerClassHash(inOwnerClassHash),
		bIsBothNetOwner(inIsBothNetOwner)
	{
		inNetCompNetIds.CopyBufferTo(OUT NetCompNetIds);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketInstantiateObj& netCompData);
sf::Packet& operator>> (sf::Packet& packet, PacketInstantiateObj& outNetCompData);

struct PacketAcceptInstantiateObj : public ConnectionManagerPacket
{
	/* The NetId of the NetworkComponent that instigated the connect request. This is equal to PacketConnectNetComp::NetId. */
	Int InstigatorNetId;

	/* The id of the NetworkComponent that was generated from the ConnectNetComp packet. */
	Int NetId;

	/* List of NetIds for each NetworkComponent on the remote process (the process that sent this accepted packet). This list must
	correspond 1-to-1 to the DataBuffer of net IDs the original requester sent. */
	DataBuffer AcceptedNetCompIds;

	PacketAcceptInstantiateObj () :
		InstigatorNetId(0),
		NetId(0)
	{
		AcceptedNetCompIds.EmptyBuffer();
	}

	PacketAcceptInstantiateObj (Int inInstigatorNetId, Int inNetId, bool inBothNetOwners, const DataBuffer& inAcceptedNetCompIds) :
		InstigatorNetId(inInstigatorNetId),
		NetId(inNetId)
	{
		inAcceptedNetCompIds.CopyBufferTo(OUT AcceptedNetCompIds);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketAcceptInstantiateObj& acceptedData);
sf::Packet& operator>> (sf::Packet& packet, PacketAcceptInstantiateObj& outAcceptedData);

struct PacketRejectInstantiateObj : public ConnectionManagerPacket
{
	/* The NetId of the NetworkRoleComponent that sent the instantiation request. */
	Int InstigatorNetId;

	PacketRejectInstantiateObj () :
		InstigatorNetId(0)
	{
		//Noop
	}

	PacketRejectInstantiateObj (Int inInstigatorNetId) :
		InstigatorNetId(inInstigatorNetId)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectInstantiateObj& rejectedData);
sf::Packet& operator>> (sf::Packet& packet, PacketRejectInstantiateObj& outRejectedData);


//=====================
/* Structs related to disconnecting a NetworkRoleComponent. */

struct PacketDisconnectEntity : public ConnectionManagerPacket
{
	/* The Network Identifier of the NetworkRoleComponent on the process receiving this packet. This is NOT the id of the instigating entity. */
	Int ReceiverNetId;

	PacketDisconnectEntity () :
		ReceiverNetId(0)
	{
		//Noop
	}

	PacketDisconnectEntity (Int inReceiverNetId) :
		ReceiverNetId(inReceiverNetId)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketDisconnectEntity& disconnectData);
sf::Packet& operator>> (sf::Packet& packet, PacketDisconnectEntity& outDisconnectData);

//=====================
/* Structs related to sending messages between two network components */
struct PacketNetCompData : public ConnectionManagerPacket
{
	/* The NetId of the NetworkComponent that's receiving the data. */
	Int CompNetId;

	/* The actual data being sent. The data sent assumes that this is in the correct order of the network component's replicated variable vector.
	This is formatted in the following structure:
	For each Replicated variable:
		Bool - If true, then this data packet will have information about this variable. Otherwise, this data packet does not have anything about this var.
		Then it contains the actual contents for the variable.
	*/
	DataBuffer CompData;

	PacketNetCompData () :
		CompNetId(0)
	{
		//Noop
	}

	PacketNetCompData (Int inCompNetId, const DataBuffer& inCompData) :
		CompNetId(inCompNetId)
	{
		inCompData.CopyBufferTo(OUT CompData);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketNetCompData& netData);
sf::Packet& operator>> (sf::Packet& packet, PacketNetCompData& outNetData);

struct PacketRejectNetCompData : public ConnectionManagerPacket
{
	enum ERejectionReason : unsigned char
	{
		RR_Unknown,
		RR_MissingNetComponent, //The net id is not associated with the NetworkComponent or it doesn't exists.
		RR_NotAuthorized, //The client does not have the right permissions to send data to this component.
		RR_NoWritePermissions, //The process does not have write access to send variable updates to a particular replicated variable.
		RR_InvalidBuffer, //The buffer does not contain appropriate data to assign to a variable.
		RR_InvalidValue, //The network component deems that the given value should not be possible (cheat detection or corrupt data or out of range).
		RR_Other
	};

	/* The NetId of the NetworkComponent that instigated the data transfer. This is zero if the reason is missing component.
	This is the component that sent the data request, not receiving it. */
	Int CompNetId;

	/* The index of the NetworkComponent's replicated variables vector of the offending variable that instigated
	this rejection. If the rejection is not related to a particular variable, then this is set to INDEX_NONE. */
	size_t RepVarIdx;

	/* A quick description why the data transfer is rejected. */
	ERejectionReason Reason;

	PacketRejectNetCompData () :
		CompNetId(0),
		RepVarIdx(UINT_INDEX_NONE),
		Reason(RR_Unknown)
	{
		//Noop
	}

	PacketRejectNetCompData(Int inCompNetId, size_t inRepVarIdx, ERejectionReason inReason) :
		CompNetId(inCompNetId),
		RepVarIdx(inRepVarIdx),
		Reason(inReason)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectNetCompData& rejectData);
sf::Packet& operator>> (sf::Packet& packet, PacketRejectNetCompData& outRejectData);


//=====================
/* Structs related to RPCing a NetworkComponent's function */
struct PacketNetCompRpc : public ConnectionManagerPacket
{
	/* The NetId of the NetworkComponent that's handling the RPC. Negative values implies it's unable to obtain the net component ID. */
	Int NetId;

	/* Hash map that maps to a particular RPC function. The IDs of the functions must match between both processes.
	Typically this is the hash of the string equal to the className::functionName. */
	size_t FunctionId;

	/* Data containing the parameters. */
	DataBuffer Parameters;

	PacketNetCompRpc () :
		NetId(0),
		FunctionId(0)
	{
		//Noop
	}

	PacketNetCompRpc (Int inNetId, size_t inFunctionId, const DataBuffer& inParameters) :
		NetId(inNetId),
		FunctionId(inFunctionId)
	{
		inParameters.CopyBufferTo(OUT Parameters);
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketNetCompRpc& rpcData);
sf::Packet& operator>> (sf::Packet& packet, PacketNetCompRpc& outRpcData);

struct PacketRejectNetCompRpc : public ConnectionManagerPacket
{
	enum ERejectReason : unsigned char
	{
		RR_Unknown,
		RR_MissingNetComp, //The specified network ID is not associated with a network component that is connected to the instigating process.
		RR_NotAuthorized, //The process does not have permission to execute RPCs on this instance, or that particular RPC is refusing execution from the process.
		RR_AccessingInvalidObj, //Slightly different from NotAuthorized. This reason is returned when attempting to reference an object the process shouldn't have write access to.
		RR_MissingFunction, //The process does not have a function mapped to the given function hash.
		RR_NotBound, //One of the delegates is not bound to a local function. Cannot execute RPC. This is a bug, and the developers would need to bind their functions.
		RR_InvalidBuffer, //The given data buffer contains insufficient data for its parameters.
		RR_InvalidCall, //The owner of the RPC determined the call or its parameters to be illegal.
		RR_NotAvailable, //The RPC is not available for remote execution for that particular process.
		RR_Internal //There is a bug in the RPC handler that prevented it from processing the request.
	};

	/* The NetId of the NetworkComponent that instigated the RPC request. */
	Int NetId;

	/* The int that maps to the RPC function that was refused to be invoked. */
	size_t FunctionId;

	/* The int that maps to a reason why the call was rejected. */
	ERejectReason Reason;

	PacketRejectNetCompRpc () :
		NetId(0),
		FunctionId(0),
		Reason(RR_Unknown)
	{
		//Noop
	}

	PacketRejectNetCompRpc (Int inNetId, size_t inFunctionId, ERejectReason inReason) :
		NetId(inNetId),
		FunctionId(inFunctionId),
		Reason(inReason)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectNetCompRpc& rejectData);
sf::Packet& operator>> (sf::Packet& packet, PacketRejectNetCompRpc& outRejectData);


//=====================
/* Structs related to disconnecting a NetworkComponent. */
struct PacketDisconnectNetComp : public ConnectionManagerPacket
{
	/* The NetId of the NetworkComponent that also needs to be severed.
	This is the ID of the Network Component residing in the process that's receiving this packet. */
	Int NetId;

	PacketDisconnectNetComp () :
		NetId(0)
	{
		//Noop
	}

	PacketDisconnectNetComp (Int inNetId) :
		NetId(inNetId)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketDisconnectNetComp& disconnectData);
sf::Packet& operator>> (sf::Packet& packet, PacketDisconnectNetComp& outDisconnectData);


//=====================
/* Structs related to the heartbeat event. */
struct PacketSendHeartbeat : public ConnectionManagerPacket
{
	/* Identifier used to distinguish one hearbeat from others. */
	Int HeartbeatId;

	PacketSendHeartbeat () :
		HeartbeatId(0)
	{
		//Noop
	}

	PacketSendHeartbeat (Int inHeartbeatId) :
		HeartbeatId(inHeartbeatId)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketSendHeartbeat& heartbeat);
sf::Packet& operator>> (sf::Packet& packet, PacketSendHeartbeat& outHeartbeat);

struct PacketReceiveHeartbeat : public ConnectionManagerPacket
{
	/* Identifier used to distinguish one heartbeat from others. This should be equal to the Heartbeat ID from the SendHeartbeat packet. */
	Int HeartbeatId;

	PacketReceiveHeartbeat () :
		HeartbeatId(0)
	{
		//Noop
	}

	PacketReceiveHeartbeat (Int inHeartbeatId) :
		HeartbeatId(inHeartbeatId)
	{
		//Noop
	}
};

sf::Packet& operator<< (sf::Packet& packet, const PacketReceiveHeartbeat& heartbeat);
sf::Packet& operator>> (sf::Packet& packet, PacketReceiveHeartbeat& outHeartbeat);
SD_END