/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkUnitTester.cpp
=====================================================================
*/

#include "AccessControlIpAddress.h"
#include "AccessControlMiscSettings.h"
#include "NetworkSignature.h"
#include "NetworkUnitTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::NetworkUnitTester, SD::UnitTester)
SD_BEGIN

bool NetworkUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult &= TestAccessControl(testFlags);
	}

	return bResult;
}

bool NetworkUnitTester::TestAccessControl (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Access Control"));

	SetTestCategory(testFlags, TXT("IP Address"));
	{
		AccessControlIpAddress* testControl = AccessControlIpAddress::CreateObject();
		ContainerUtils::Empty(OUT testControl->EditBlockedIpAddresses()); //Remove blocked ips that were pulled from config

		std::vector<DString> testBlockedIps(
		{
			TXT("255.240.230.128"),
			TXT("4.6.8.12"),
			TXT("100.100.1*1.86"),
			TXT("128.250.200.*"),
			TXT("113.128.*"),
			TXT("*.96.95.94"),
			TXT("155.54.4*.4*")
		});
		testControl->EditBlockedIpAddresses() = testBlockedIps;

		sf::IpAddress testAddr;
		bool expectedBlocked;
		std::function<bool()> testIp([&]()
		{
			bool actuallyBlocked = testControl->IsIpBlocked(testAddr);
			if (actuallyBlocked != expectedBlocked)
			{
				if (expectedBlocked)
				{
					UnitTestError(testFlags, TXT("IP Address Access Control test failed. It's expected that the address %s should be blocked, but it is not."), DString(testAddr.toString()));
				}
				else
				{
					UnitTestError(testFlags, TXT("IP Address Access Control test failed. It's expected that the address %s should NOT be blocked, but it is."), DString(testAddr.toString()));
				}

				testControl->Destroy();
				testControl = nullptr;
				return false;
			}

			return true;
		});

		testAddr = sf::IpAddress(TXT("255.240.230.127"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("255.240.230.128"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("254.240.230.128"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("4.6.8.12"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("004.006.008.012"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("4.6.8.120"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("100.100.101.86"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("100.100.151.86"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("100.100.11.86"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("100.100.115.86"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("128.250.200.0"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("128.250.200.101"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("128.250.200.96"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("128.250.204.100"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("113.128.0.0"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("113.128.95.105"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("113.128.250.4"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("113.127.90.106"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("200.96.95.94"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("200.196.95.94"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("200.96.95.194"));
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("96.96.95.94"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("4.96.95.94"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("155.54.40.40"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("155.54.04.40"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("155.54.4.4"));
		expectedBlocked = false;
		if (!testIp())
		{
			return false;
		}

		testAddr = sf::IpAddress(TXT("155.54.42.49"));
		expectedBlocked = true;
		if (!testIp())
		{
			return false;
		}

		testControl->Destroy();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Misc Settings"));
	{
		AccessControlMiscSettings* testControl = AccessControlMiscSettings::CreateObject();
		testControl->MaxConnections = 0; //This unit test is not testing max connections since that queries the ConnectionManager linked with the NetworkEngineComponent.
		testControl->AllowedRoles = -1;

		NetworkSignature* testSignature = NetworkSignature::CreateObject();
		bool expectedAllowed;

		std::function<bool()> testAllowAccess([&]()
		{
			bool bTestPassed = true;
			if (!testControl->IsRelevant(testSignature))
			{
				UnitTestError(testFlags, TXT("Misc Settings Access Control test failed. The access control determined that the test signature is not relevant to check."));
				bTestPassed = false;
			}

			if (bTestPassed)
			{
				unsigned char reason = AccessControl::RR_None;
				bool allowedConnection = testControl->AllowConnection(testSignature, OUT reason);
				if (allowedConnection != expectedAllowed)
				{
					if (expectedAllowed)
					{
						UnitTestError(testFlags, TXT("Misc Settings Access Control test failed. The MiscSettingsAccessControl should have allowed a connection request with roles %s when the access control allows roles %s. Rejection reason: %s."), Int(testSignature->GetNetworkRoles()), testControl->AllowedRoles, Int(reason));
					}
					else
					{
						UnitTestError(testFlags, TXT("Misc Settings Access Control test failed. The MiscSettingsAccessControl should have not allowed a connection request with roles %s when the access control allows roles %s."), Int(testSignature->GetNetworkRoles()), testControl->AllowedRoles);
					}

					bTestPassed = false;
				}
				else if (!allowedConnection && reason != AccessControl::RR_InvalidType)
				{
					UnitTestError(testFlags, TXT("Misc Settings Access Control test failed. Although the connection is rejected, it was rejected for the wrong reasons. It should have rejected because of reason %s instead of reason %s."), Int(AccessControl::RR_InvalidType), Int(reason));
					bTestPassed = false;
				}
			}


			if (!bTestPassed)
			{
				testControl->Destroy();
				testSignature->Destroy();
				testControl = nullptr;
				testSignature = nullptr;
			}

			return bTestPassed;
		});

		//Test when the access control is only accepting clients
		testControl->AllowedRoles = NetworkSignature::NR_Client;
		{
			testSignature->SetNetworkRoles(NetworkSignature::NR_Client);
			expectedAllowed = true;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Server);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Client | NetworkSignature::NR_Server);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(testSignature->GetNetworkRoles() | NetworkSignature::NR_MasterServer | NetworkSignature::NR_Server);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}
		}

		testControl->AllowedRoles = NetworkSignature::NR_Server;
		{
			testSignature->SetNetworkRoles(NetworkSignature::NR_Client);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Client | NetworkSignature::NR_Server);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Server);
			expectedAllowed = true;
			if (!testAllowAccess())
			{
				return false;
			}
		}

		testControl->AllowedRoles = NetworkSignature::NR_Client | NetworkSignature::NR_Server;
		{
			testSignature->SetNetworkRoles(NetworkSignature::NR_Client);
			expectedAllowed = true;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Server);
			expectedAllowed = true;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(NetworkSignature::NR_Client | NetworkSignature::NR_Server);
			expectedAllowed = true;
			if (!testAllowAccess())
			{
				return false;
			}

			testSignature->SetNetworkRoles(testSignature->GetNetworkRoles() | NetworkSignature::NR_Service);
			expectedAllowed = false;
			if (!testAllowAccess())
			{
				return false;
			}
		}

		testControl->Destroy();
		testSignature->Destroy();
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Access Control"));
	return true;
}
SD_END
#endif