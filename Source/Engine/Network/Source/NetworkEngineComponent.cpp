/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkEngineComponent.cpp
=====================================================================
*/

#include "AccessControl.h"
#include "AccessControlIpAddress.h"
#include "AccessControlMiscSettings.h"
#include "ConnectionManager.h"
#include "NetworkEngineComponent.h"
#include "NetworkIdentifier.h"

IMPLEMENT_ENGINE_COMPONENT(SD::NetworkEngineComponent)
SD_BEGIN

sf::IpAddress NetworkEngineComponent::LanAddress(sf::IpAddress::None);
sf::IpAddress NetworkEngineComponent::PublicAddress(sf::IpAddress::None);

NetworkEngineComponent::NetworkEngineComponent () : Super(),
	ConnectionManagerClass(nullptr),
	bAllowIpLogging(true),
	bCmdLineOpenPorts(true)
{
	//Noop
}

void NetworkEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Network.ini")), false))
	{
		bAllowIpLogging = config->GetProperty<Bool>(TXT("NetworkEngineComponent"), TXT("bAllowIpLogging"));
	}
	config->Destroy();

	if (LanAddress == sf::IpAddress::None) //Ensure this only runs once (in case there are multiple NetworkEngineComponents on different threads). The IP Address shouldn't change based on the thread though.
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("Initializing Network Engine Component. Figuring out this computer's public IP address."));
		PublicAddress = sf::IpAddress::getPublicAddress(sf::seconds(30.f)); //Blocking process. This function may take awhile to execute.
		if (PublicAddress == sf::IpAddress::None)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain this computer's public IP address. It will not be able to send messages to others on the internet."));
		}
		LanAddress = sf::IpAddress::getLocalAddress();

		NetworkLog.Log(LogCategory::LL_Log, TXT("IP address identified for this computer. IP address assigned in NetworkEngineComponent::PublicAddress."));

#ifdef DEBUG_MODE
		if (bAllowIpLogging)
		{
			NetworkLog.Log(LogCategory::LL_Log, TXT("This computer's IP address is %s, and its LAN address is %s."), DString(PublicAddress.toString()), DString(LanAddress.toString()));
		}
#endif
	}

	Engine* owningEngine = GetOwningEngine();
	CHECK(owningEngine != nullptr)
	owningEngine->CreateTickGroup(TICK_GROUP_NETWORK, TICK_GROUP_PRIORITY_NETWORK);

	//Instantiate AccessControls nearly every application would want. These can still be removed via EditAccessControls vector.
	AccessControlIpAddress* ipControl = AccessControlIpAddress::CreateObject();
	AccessControlMiscSettings* miscControl = AccessControlMiscSettings::CreateObject();
	AccessControls.push_back(ipControl);
	AccessControls.push_back(miscControl);
}

void NetworkEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	if (ConnectionManagerClass == nullptr)
	{
		Connections = ConnectionManager::CreateObject();
	}
	else
	{
		Connections = dynamic_cast<ConnectionManager*>(ConnectionManagerClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(Connections.IsValid())
	}
}

void NetworkEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	if (bCmdLineOpenPorts && OwningEngine != nullptr && Connections.IsValid())
	{
		if (OwningEngine->HasCmdLineSwitch(TXT("-server"), DString::CC_IgnoreCase))
		{
			Connections->OpenPort(ConnectionManager::DP_Game);
		}
	}
}

void NetworkEngineComponent::ShutdownComponent ()
{
	if (Connections.IsValid())
	{
		Connections->Destroy();
		Connections = nullptr;
	}

	for (AccessControl* control : AccessControls)
	{
		control->Destroy();
	}
	ContainerUtils::Empty(OUT AccessControls);

	if (LocalSignature.IsValid())
	{
		LocalSignature->Destroy();
	}

	Super::ShutdownComponent();
}

bool NetworkEngineComponent::InitializeLocalNetworkSignature (NetworkSignature::ENetworkRole networkRoles, const std::vector<const DClass*>& identifierClasses)
{
	if (LocalSignature.IsValid())
	{
		LocalSignature->Destroy();
	}

	LocalSignature = NetworkSignature::CreateObject();
	LocalSignature->SetNetworkRoles(networkRoles);

	for (const DClass* identifierClass : identifierClasses)
	{
		if (identifierClass->IsAbstract())
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize local network signature since %s is an abstract class. It cannot be instantiated."), identifierClass->ReadDuneClassName());
			LocalSignature->Destroy();
			return false;
		}

		if (const NetworkIdentifier* identifier = dynamic_cast<const NetworkIdentifier*>(identifierClass->GetDefaultObject()))
		{
			NetworkIdentifier* subComp = dynamic_cast<NetworkIdentifier*>(identifier->CreateObjectOfMatchingClass());
			CHECK(subComp != nullptr)

			if (LocalSignature->AddComponent(subComp))
			{
				if (!subComp->InitializeIdToCurSystem(LocalSignature->GetNetworkRoles()))
				{
					NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize local network signature since the class %s is unable to initialize itself to the local system."), identifierClass->ReadDuneClassName());
					LocalSignature->Destroy();
					return false;
				}
			}
			else
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize local network signature since the component of class %s failed to attach to signature."), identifierClass->ReadDuneClassName());
				LocalSignature->Destroy();
				return false;
			}
		}
		else
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize local network signature since the given class %s is not a NetworkIdentifier class."), identifierClass->ReadDuneClassName());
			LocalSignature->Destroy();
			return false;
		}
	}

	//Skipping generating an ID is intended since this signature will not reside in the connection manager (it will not be used).
	LocalSignature->SetAddress(sf::IpAddress::LocalHost);
	return true;
}

void NetworkEngineComponent::SetConnectionManagerClass (const DClass* newConnectionManagerClass)
{
	if (OwningEngine != nullptr && OwningEngine->IsInitialized())
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot assign NetworkEngineComponent's ConnectionManager class after initialization. The class must be specified prior to InitializeComponent."));
		return;
	}

	if (newConnectionManagerClass != nullptr && !newConnectionManagerClass->IsA(ConnectionManager::SStaticClass()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot assign %s as the NetworkEngineComponent's ConnectionManager class since that is not a ConnectionManager class."), newConnectionManagerClass->ToString());
		return;
	}

	if (newConnectionManagerClass != nullptr && newConnectionManagerClass->IsAbstract())
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot assign %s as the NetworkEngineComponent's ConnectionManager class since that is an abstract class."), newConnectionManagerClass->ToString());
		return;
	}

	ConnectionManagerClass = newConnectionManagerClass;
}

void NetworkEngineComponent::SetCmdLineOpenPorts (bool bNewCmdLineOpenPorts)
{
	bCmdLineOpenPorts = bNewCmdLineOpenPorts;
}
SD_END