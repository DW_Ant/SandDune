/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkRoleComponent.cpp
=====================================================================
*/

#include "BaseReplicatedVariable.h"
#include "ConnectionManager.h"
#include "NetworkComponent.h"
#include "NetworkEngineComponent.h"
#include "NetworkRoleComponent.h"
#include "NetworkSignature.h"
#include "SignatureIterator.h"

IMPLEMENT_CLASS(SD::NetworkRoleComponent, SD::EntityComponent)
SD_BEGIN

void NetworkRoleComponent::InitProps ()
{
	Super::InitProps();

	bDestroyWhenLostRelevance = true;

	NetId = 0;

	//By default, NetworkRoleComponents should replicate its data. NOTE: The connection manager will set this to Impotent or NetOwner if this object was instantiated from a remote process.
	NetRole = NR_Authority;
	TimeoutTime = 30.f;
}

void NetworkRoleComponent::BeginObject ()
{
	Super::BeginObject();

	RelevanceTick = TickComponent::CreateObject(TICK_GROUP_NETWORK);
	if (AddComponent(RelevanceTick))
	{
		RelevanceTick->SetTickInterval(1.f);
		RelevanceTick->SetTickHandler(SDFUNCTION_1PARAM(this, NetworkRoleComponent, HandleRelevanceTick, void, Float));
		RelevanceTick->SetTicking(NetRole == NR_Authority);
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	Connections = localNetEngine->GetConnections();

	if (NetRole != NR_Local) //in case subclasses reverted defaults back to Local
	{
		GenerateNetEntityId();
	}
}

bool NetworkRoleComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	if (dynamic_cast<EntityComponent*>(ownerCandidate) != nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("NetworkRoleComponents cannot be attached to another component."));
		return false;
	}

	//Ensure there's only one component of this type.
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (dynamic_cast<NetworkRoleComponent*>(iter.GetSelectedComponent()) != nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Entities can only have one NetworkRoleComponent attached."));
			return false;
		}
	}

	return true;
}

void NetworkRoleComponent::Destroyed ()
{	
	if (NetRole == NR_Authority && !ContainerUtils::IsEmpty(RemoteClients))
	{
		size_t idx = RemoteClients.size() - 1; //It's faster to pop_back than to erase from the beginning
		while (true)
		{
			if (RemoteClients.at(idx).Status != CS_None)
			{
				DropClient(RemoteClients.at(idx).RemoteClient.Get(), RemoteClients.at(idx).RemoteId);
			}

			if (idx == 0)
			{
				break;
			}

			--idx;
		}
		ContainerUtils::Empty(OUT RemoteClients);
	}

	Super::Destroyed();
}

void NetworkRoleComponent::NetInitialize (NetworkSignature* connectedTo)
{
	if (OnNetInitialize.IsBounded())
	{
		OnNetInitialize.Execute(connectedTo);
	}
}

void NetworkRoleComponent::GetNetComponentList (std::vector<NetworkComponent*>& outComponentList) const
{
	if (OnGetComponentList.IsBounded())
	{
		OnGetComponentList.Execute(OUT outComponentList);
	}
}

bool NetworkRoleComponent::BroadcastNetEntity (NetworkSignature* remoteClient)
{
	if (NetRole != NR_Authority || Connections.IsNullptr())
	{
		return false;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	bool isNetOwner = IsNetOwner(remoteClient);
	ENetRole remoteRole = (isNetOwner) ? NR_NetOwner : NR_Impotent;
	RemoteClients.emplace_back(SRemoteClientData(remoteClient, remoteRole, localEngine->GetElapsedTime()));
	Connections->InstantiateEntity(this, remoteClient, isNetOwner);

	return true;
}

void NetworkRoleComponent::LoseRelevance ()
{
	if (NetRole == NR_Local || NetRole == NR_Authority)
	{
		DString ownerName = (GetOwner() != nullptr) ? GetOwner()->ToString() : TXT("<Detached Role Component>");
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Local and Authoritative Entities should never lose relevance. LoseRelevance was called on %s with role %s."), ownerName, Int(NetRole));
		return;
	}

	SetAuthoritativeClient(nullptr, 0);

	if (OnLoseRelevance.IsBounded())
	{
		OnLoseRelevance.Execute();
	}

	if (bDestroyWhenLostRelevance && GetOwner() != nullptr)
	{
		GetOwner()->Destroy();
	}
	else
	{
		SetNetRole(NR_Local);
	}
}

void NetworkRoleComponent::SetNetRole (ENetRole newNetRole)
{
	if (NetRole == newNetRole)
	{
		return;
	}

	//Drop existing clients
	if (!ContainerUtils::IsEmpty(RemoteClients))
	{
		size_t idx = RemoteClients.size() - 1;
		while (true)
		{
			if (RemoteClients.at(idx).RemoteClient.IsValid() && RemoteClients.at(idx).Status != CS_None && RemoteClients.at(idx).RemoteId != 0)
			{
				DropClient(RemoteClients.at(idx).RemoteClient.Get(), RemoteClients.at(idx).RemoteId);
			}

			if (idx == 0)
			{
				break;
			}

			--idx;
		}
		ContainerUtils::Empty(OUT RemoteClients);
	}

	NetRole = newNetRole;

	if (NetId == 0 && NetRole != NR_Local)
	{
		GenerateNetEntityId();
	}

	if (RelevanceTick.IsValid())
	{
		RelevanceTick->SetTicking(NetRole == NR_Authority);

		//The first frame is most important, and it should tick immediately to reduce latency.
		//The subsequent ticks not so much since that is used to evaluate relevance for new clients or lose relevance for current clients.
		RelevanceTick->ForceTickNextFrame();
	}
}

void NetworkRoleComponent::SetTimeoutTime (Float newTimeoutTime)
{
	TimeoutTime = Utils::Max<Float>(newTimeoutTime, 1.f);
}

void NetworkRoleComponent::SetAuthoritativeClient (NetworkSignature* authClientSignature, Int externalNetId)
{
	AuthoritativeClient.RemoteClient = authClientSignature;
	AuthoritativeClient.Status = (authClientSignature != nullptr) ? CS_Connected : CS_None;
	AuthoritativeClient.RemoteId = externalNetId;
}

NetworkRoleComponent::ENetRole NetworkRoleComponent::GetRemoteNetRole (NetworkSignature* remoteProcess) const
{
	if (remoteProcess == nullptr)
	{
		return NR_Unknown;
	}

	if (NetRole == NR_Authority)
	{
		for (const SRemoteClientData& remoteClient : RemoteClients)
		{
			if (remoteClient.RemoteClient == remoteProcess)
			{
				return remoteClient.RemoteRole;
			}
		}
	}
	else if (AuthoritativeClient.RemoteClient == remoteProcess)
	{
		return NR_Authority;
	}

	return NR_Unknown;
}

void NetworkRoleComponent::GenerateNetEntityId ()
{
	CHECK(NetRole != NR_Local) //Should only be called on nonlocal Entities
	if (NetId != 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to generate NetEntityId since the Entity already has the ID of %s."), NetId);
		return;
	}

	/*
	No need to check for wrap around issues.
	signed int64 is a HUGE number. Even if role components generated an ID every frame on a 300 fps application, it would take over 9.7x10^8 years to overflow to negative numbers.
	32 bit signed however, it would take 82 days for it to overflow.
	*/
	static Int nextId = 0;
	++nextId;
	NetId = nextId;
}

bool NetworkRoleComponent::IsRelevant (NetworkSignature* remoteClient) const
{
	if (GetOwner() == nullptr)
	{
		return false;
	}

	if (OnIsNetRelevant.IsBounded())
	{
		return OnIsNetRelevant.Execute(remoteClient);
	}

	return true;
}

bool NetworkRoleComponent::IsNetOwner (NetworkSignature* remoteClient) const
{
	if (OnIsNetOwner.IsBounded())
	{
		return OnIsNetOwner.Execute(remoteClient);
	}

	//By default, no one is a net owner
	return false; 
}

void NetworkRoleComponent::ProcessInstantiationAccepted (NetworkSignature* remoteClient, Int remoteEntityId, const DataBuffer& remoteNetCompIds)
{
	ENetRole remoteRole = NR_Unknown;
	for (size_t i = 0; i < RemoteClients.size(); ++i)
	{
		if (RemoteClients.at(i).RemoteClient == remoteClient)
		{
			RemoteClients.at(i).Status = CS_Connected;
			RemoteClients.at(i).RemoteId = remoteEntityId;
			remoteRole = RemoteClients.at(i).RemoteRole;
			break;
		}
	}

	std::vector<NetworkComponent*> localNetComps;
	GetNetComponentList(OUT localNetComps);
	for (NetworkComponent* localNetComp : localNetComps)
	{
		Int externalNetId;
		if (!remoteNetCompIds.CanReadBytes(externalNetId.GetMinBytes()))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to pair NetworkComponents. The returned data buffer does not contain enough NetworkComponent IDs. It's expecting %s IDs."), Int(localNetComps.size()));
			return;
		}

		if ((remoteNetCompIds >> externalNetId).HasReadError())
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to pair NetworkComponents. The returned data buffer contains malformed bytes that's preventing it from reading an Int for the NetworkComponent ID. It's expecting %s IDs."), Int(localNetComps.size()));
			return;
		}

		if (externalNetId != 0) //Handles when this component didn't get a disconnect packet before receiving a accept packet
		{
			localNetComp->AddExternalNetComp(remoteClient, externalNetId, remoteRole);
		}
	}

	//Check if there's a need to send initial data to the remote client.
	if (NetRole == NR_Authority && Connections.IsValid())
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		for (NetworkComponent* localNetComp : localNetComps)
		{
			DataBuffer repVarData;
			bool bHasData = false;
			
			for (BaseReplicatedVariable* repVar : localNetComp->ReadReplicatedVariables())
			{
				if (repVar->GetSyncOnInit()) //Force a sync on all 'SyncOnInit' types
				{
					bHasData = true;
					repVarData << Bool(true);
					repVar->ReplicateVariable(remoteClient, localEngine->GetElapsedTime(), OUT repVarData);
				}
				else
				{
					repVarData << Bool(false);
				}
			}

			if (bHasData)
			{
				Connections->ReplicateVariables(remoteClient, localNetComp, repVarData);
			}
		}
	}
}

void NetworkRoleComponent::ProcessInstantiationRejected (NetworkSignature* remoteClient)
{
	for (size_t i = 0; i < RemoteClients.size(); ++i)
	{
		if (RemoteClients.at(i).RemoteClient == remoteClient)
		{
			RemoteClients.erase(RemoteClients.begin() + i);
			break;
		}
	}
}

void NetworkRoleComponent::DropClient (NetworkSignature* remoteClient, Int externalNetId)
{
	switch (NetRole)
	{
		case(NR_Authority):
		{
			for (size_t i = 0; i < RemoteClients.size(); ++i)
			{
				if (RemoteClients.at(i).RemoteClient == remoteClient)
				{
					RemoteClients.erase(RemoteClients.begin() + i);
					break;
				}
			}
			
			break;
		}

		case(NR_NetOwner):
		case(NR_Impotent):
			AuthoritativeClient.RemoteClient = nullptr;
			AuthoritativeClient.Status = CS_None;
			AuthoritativeClient.RemoteId = 0;
			break;
	}

	if (Connections.IsValid())
	{
		Connections->DisconnectEntity(this, remoteClient, externalNetId); 
	}
}

void NetworkRoleComponent::HandleRelevanceTick (Float deltaSec)
{
	if (Connections.IsNullptr() || NetRole != NR_Authority)
	{
		return;
	}

	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		//Ensure this is not already relevant
		bool alreadyRelevant = false;
		for (size_t i = 0; i < RemoteClients.size(); ++i)
		{
			if (RemoteClients.at(i).RemoteClient == iter.GetSelectedSignature())
			{
				alreadyRelevant = true;
				break;
			}
		}

		if (!alreadyRelevant && IsRelevant(iter.GetSelectedSignature()))
		{
			BroadcastNetEntity(iter.GetSelectedSignature());
		}
	}

	size_t i = 0;
	while (i < RemoteClients.size())
	{
		if (RemoteClients.at(i).Status == CS_Pending)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)
			if (localEngine->GetElapsedTime() - RemoteClients.at(i).RequestSentTime >= TimeoutTime)
			{
				DropClient(RemoteClients.at(i).RemoteClient.Get(), 0);
				continue;
			}
		}
		else if (RemoteClients.at(i).RemoteId != 0 && !IsRelevant(RemoteClients.at(i).RemoteClient.Get()))
		{
			DropClient(RemoteClients.at(i).RemoteClient.Get(), RemoteClients.at(i).RemoteId); //This will remove this reference from the ConnectionManager's internal map
			continue;
		}

		++i;
	}
}
SD_END