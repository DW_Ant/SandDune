/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkMonitor.cpp
=====================================================================
*/

#include "NetworkEngineComponent.h"
#include "NetworkMonitor.h"
#include "NetworkSignature.h"
#include "PacketsConnectionManager.h"

IMPLEMENT_CLASS(SD::NetworkMonitor, SD::Entity)
SD_BEGIN

NetworkMonitor::SOffenderData::SOffenderData (NetworkSignature* inOffender) :
	Offender(inOffender),
	NumOffenses(1)
{
	//Noop
}

void NetworkMonitor::InitProps ()
{
	Super::InitProps();

	MaxOffenses = 10;
}

void NetworkMonitor::BeginObject ()
{
	Super::BeginObject();

	Float forgivenessRate = 4.f;

	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Network.ini")), false))
	{
		MaxOffenses = config->GetProperty<Int>(TXT("NetworkMonitor"), TXT("MaxOffenses"));
		forgivenessRate = config->GetProperty<Float>(TXT("NetworkMonitor"), TXT("ForgivenessRate"));
	}
	config->Destroy();

	ForgivenessTick = TickComponent::CreateObject(TICK_GROUP_NETWORK);
	if (AddComponent(ForgivenessTick))
	{
		ForgivenessTick->SetTickInterval(forgivenessRate);
		ForgivenessTick->SetTickHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleForgivenessTick, void, Float));
	}
}

void NetworkMonitor::Destroyed ()
{
	for (SOffenderData& offender : RecentOffenders)
	{
		if (offender.Offender != nullptr)
		{
			offender.Offender->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleDisconnect, void, NetworkSignature*));
		}
	}
	ContainerUtils::Empty(OUT RecentOffenders);

	Super::Destroyed();
}

void NetworkMonitor::ReadOutboundPacket (ConnectionManager::ENetMessageType msgType, ConnectionManagerPacket* packet, NetworkSignature* receiver)
{
	bool bIsOffense = false;

	//Note: Ignoring NMT_Resp_ConnectionRejected since the offender did not connect to this process.
	switch (msgType)
	{
		case(ConnectionManager::NMT_Unknown):
		case(ConnectionManager::NMT_Resp_RejectInstantiation):
		case(ConnectionManager::NMT_Resp_RejNetCompData):
		case(ConnectionManager::NMT_Resp_RejNetCompRpc):
			bIsOffense = true;
			break;

		default:
			break;
	}

	if (bIsOffense)
	{
		AddOffender(receiver);
	}
}

void NetworkMonitor::ReadInboundPacket (ConnectionManager::ENetMessageType msgType, ConnectionManagerPacket* packet, NetworkSignature* sender)
{
	//Noop
}

void NetworkMonitor::AddOffender (NetworkSignature* offender)
{
	if (MaxOffenses <= 1)
	{
		KickOffender(offender);
		return;
	}

	for (SOffenderData& recentOffender : RecentOffenders)
	{
		if (recentOffender.Offender == offender)
		{
			recentOffender.NumOffenses++;
			if (recentOffender.NumOffenses >= MaxOffenses)
			{
				KickOffender(offender);
			}

			return;
		}
	}

	offender->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleDisconnect, void, NetworkSignature*));
	RecentOffenders.emplace_back(SOffenderData(offender));
}

void NetworkMonitor::KickOffender (NetworkSignature* offender)
{
	CHECK(offender != nullptr)

	offender->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleDisconnect, void, NetworkSignature*));

	size_t i = 0;
	while (i < RecentOffenders.size())
	{
		if (RecentOffenders.at(i).Offender == offender)
		{
			RecentOffenders.erase(RecentOffenders.begin() + i);
			continue;
		}

		++i;
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	if (ConnectionManager* connections = localNetEngine->GetConnections())
	{
		connections->Disconnect(offender, true);
	}
}

void NetworkMonitor::HandleForgivenessTick (Float deltaSec)
{
	size_t i = 0;
	while (i < RecentOffenders.size())
	{
		RecentOffenders.at(i).NumOffenses--;
		if (RecentOffenders.at(i).NumOffenses <= 0)
		{
			if (RecentOffenders.at(i).Offender != nullptr)
			{
				RecentOffenders.at(i).Offender->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleDisconnect, void, NetworkSignature*));
			}

			RecentOffenders.erase(RecentOffenders.begin() + i);
			continue;
		}

		++i;
	}
}

void NetworkMonitor::HandleDisconnect (NetworkSignature* disconnected)
{
	size_t i = 0;
	while (i < RecentOffenders.size())
	{
		if (RecentOffenders.at(i).Offender == disconnected)
		{
			disconnected->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, NetworkMonitor, HandleDisconnect, void, NetworkSignature*));
			RecentOffenders.erase(RecentOffenders.begin() + i);
			continue;
		}

		++i;
	}
}
SD_END