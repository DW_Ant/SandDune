/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PacketOperators.cpp
=====================================================================
*/

#include "PacketOperators.h"

SD_BEGIN
sf::Packet& operator<< (sf::Packet& packet, const Bool& data)
{
	return (packet << data.Value);
}

sf::Packet& operator<< (sf::Packet& packet, const DString& data)
{
#if USE_WIDE_STRINGS
	return (packet << data.ToWStr());
#else
	return (packet << data.ToStr());
#endif
}

sf::Packet& operator<< (sf::Packet& packet, const Float& data)
{
	return (packet << data.Value);
}

sf::Packet& operator<< (sf::Packet& packet, const Int& data)
{
	//There would be a lot of leading zeroes for these 64-bit ints. Consider investigating some fast lossless compression methods before sending the packet out.
	return (packet << data.Value);
}

sf::Packet& operator<< (sf::Packet& packet, const Rotator& data)
{
	return (packet << data.ToInt());
}

sf::Packet& operator<< (sf::Packet& packet, const Vector2& data)
{
	return (packet << data.X.Value << data.Y.Value);
}

sf::Packet& operator<< (sf::Packet& packet, const Vector3& data)
{
	return (packet << data.X.Value << data.Y.Value << data.Z.Value);
}

sf::Packet& operator<< (sf::Packet& packet, const DataBuffer& data)
{
	//Note: SFML's packet class already handle endianness. There's no need to send endian data from this buffer, and assume the receiving data correctly converts it.
	size_t numBytes = data.GetNumBytes();
	packet << numBytes;

	for (char byte : data.ReadRawData())
	{
		packet << static_cast<sf::Int8>(byte);
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Bool& outData)
{
	if (!(packet >> outData.Value))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Bool from packet."));
		outData = false;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, DString& outData)
{
#if USE_WIDE_STRINGS
	std::wstring extractedData;
#else
	std::string extractedData;
#endif

	if (packet >> extractedData)
	{
		outData = DString(extractedData);
	}
	else
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract DString from packet."));

#ifdef DEBUG_MODE
		outData = TXT("<Invalid Net Packet>");
#else
		outData = DString::EmptyString;
#endif
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Float& outData)
{
	if (!(packet >> outData.Value))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Float from packet."));
		outData = -1.f;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Int& outData)
{
	if (!(packet >> outData.Value))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Int from packet."));
		outData = -1;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Rotator& outData)
{
	uint64 rotationData;
	if (packet >> rotationData)
	{
		outData = Rotator(rotationData);
	}
	else
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Rotator from packet."));
		outData = Rotator::ZERO_ROTATOR;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Vector2& outData)
{
	if (!(packet >> outData.X.Value) || !(packet >> outData.Y.Value))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Vector2 from packet."));
		outData = Vector2::ZERO_VECTOR;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, Vector3& outData)
{
	if (!(packet >> outData.X.Value) || !(packet >> outData.Y.Value) || !(packet >> outData.Z.Value))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract Vector3 from packet."));
		outData = Vector3::ZERO_VECTOR;
	}

	return packet;
}

sf::Packet& operator>> (sf::Packet& packet, DataBuffer& outData)
{
	outData.EmptyBuffer();
	size_t numBytes;
	if (!(packet >> numBytes))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract the number of bytes for a data buffer from a packet."));
		return packet;
	}

	char* newBytes = new char[numBytes];

	//Note: Read one byte at a time instead of calling sf::Packet::getData since a single packet may have more data beyond the data buffer.
	for (size_t i = 0; i < numBytes; ++i)
	{
		sf::Int8 dataByte; //sf::Int8 are typedef'd signed chars
		if (!(packet >> dataByte))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract bytes from a packet for a data buffer that's expecting %s bytes."), Int(numBytes));
			delete[] newBytes;
			return packet;
		}
		else
		{
			newBytes[i] = static_cast<char>(dataByte);
		}
	}

	outData.AppendBytes(newBytes, numBytes);
	delete[] newBytes;
	return packet;
}
SD_END