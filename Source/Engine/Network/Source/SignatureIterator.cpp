/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SignatureIterator.cpp
=====================================================================
*/

#include "ConnectionManager.h"
#include "NetworkEngineComponent.h"
#include "NetworkSignature.h"
#include "SignatureIterator.h"

SD_BEGIN
SignatureIterator::SignatureIterator (bool inIncludeDisconnected) :
	Manager(nullptr),
	IterAmount(0),
	bIncludeDisconnected(inIncludeDisconnected)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	Manager = localNetEngine->GetConnections();

	if (Manager->Connections.size() > 0)
	{
		SelectFirstSignature();
	}
}

SignatureIterator::~SignatureIterator ()
{
	//Noop
}

void SignatureIterator::operator++ ()
{
	SelectNextSignature();
}

void SignatureIterator::operator++ (int)
{
	SelectNextSignature();
}

NetworkSignature* SignatureIterator::GetSelectedSignature () const
{
	return SelectedSignature.Get();
}

void SignatureIterator::SelectFirstSignature ()
{
	IterAmount = 0;
	for (auto iter = Manager->Connections.begin(); iter != Manager->Connections.end(); ++iter)
	{
		if (iter->second.Destination == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Critical, TXT("The ConnectionManager has a 'connection' entry with a null NetworkSignature at count %s."), Int(IterAmount));
			continue;
		}

		if (iter->second.bApprovedConnection)
		{
			if (bIncludeDisconnected || (!iter->second.bPendingDelete && !iter->second.bTerminateOnSend))
			{
				SelectedSignature = iter->second.Destination;
				break;
			}
		}

		++IterAmount;
	}
}

void SignatureIterator::SelectNextSignature ()
{
	if (SelectedSignature.IsNullptr())
	{
		return; //Done iterating
	}

	if (Manager == nullptr)
	{
		SelectedSignature = nullptr;
		return;
	}

	//Ensure the index still points to the signature. Otherwise, try to recover
	auto iter = Manager->Connections.begin();
	std::advance(iter, IterAmount);

	//Check if the Connections changed size. If so, reset the iterator to point to the destination that matches the SelectedSignature
	if (IterAmount >= Manager->Connections.size() || iter == Manager->Connections.end() || iter->second.Destination != SelectedSignature)
	{
		IterAmount = 0;
		for (iter = Manager->Connections.begin(); iter != Manager->Connections.end(); ++iter)
		{
			if (iter->second.Destination == SelectedSignature)
			{
				break;
			}

			++IterAmount;
		}

		if (iter == Manager->Connections.end())
		{
			//The selected element was the last item. Stop iterating
			SelectedSignature = nullptr;
			return;
		}
	}

	while (true)
	{
		++IterAmount;
		++iter;

		if (iter == Manager->Connections.end())
		{
			SelectedSignature = nullptr;
			return; //Done iterating
		}

		if (iter->second.Destination == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Critical, TXT("The ConnectionManager has a 'connection' entry with a null NetworkSignature at iterator amount %s."), Int(IterAmount));
			continue;
		}

		if (iter->second.bApprovedConnection)
		{
			if (bIncludeDisconnected || (!iter->second.bPendingDelete && !iter->second.bTerminateOnSend))
			{
				SelectedSignature = iter->second.Destination;
				break;
			}
		}
	}
}
SD_END