/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControlIpAddress.cpp
=====================================================================
*/

#include "AccessControlIpAddress.h"
#include "NetworkSignature.h"

IMPLEMENT_CLASS(SD::AccessControlIpAddress, SD::AccessControl)
SD_BEGIN

void AccessControlIpAddress::InitProps ()
{
	Super::InitProps();

	MinAttemptInterval = 15.f;
	MaxAttemptsAllowed = 3;
}

bool AccessControlIpAddress::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	CHECK(signature != nullptr)

	//Check if this ip is throttled
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	size_t i = 0;
	while (i < SuspendedIps.size())
	{
		Float deltaSec = (SuspendedIps.at(i).LastRequestTime - localEngine->GetElapsedTime());
		if (deltaSec >= MinAttemptInterval)
		{
			//Enough time elapsed. Can remove this ip from suspended list.
			SuspendedIps.erase(SuspendedIps.begin() + i);
			continue;
		}

		if (SuspendedIps.at(i).Address == signature->ReadAddress())
		{
			if (SuspendedIps.at(i).NumFailedAttempts < MaxAttemptsAllowed)
			{
				break;
			}

			outRejectReason = RR_Suspended;
			return false;
		}

		++i;
	}

	if (IsIpBlocked(signature->ReadAddress()))
	{
		outRejectReason = RR_Banned;
		return false;
	}

	return true;
}

void AccessControlIpAddress::ConnectionRejected (NetworkSignature* signature, unsigned char rejectReason)
{
	CHECK(signature != nullptr)

	Super::ConnectionRejected(signature, rejectReason);

	if (MinAttemptInterval > 0.f && rejectReason != RR_Suspended)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		for (SIpActivity& suspendedIp : SuspendedIps)
		{
			if (suspendedIp.Address == signature->ReadAddress())
			{
				suspendedIp.NumFailedAttempts++;
				suspendedIp.LastRequestTime = localEngine->GetElapsedTime();
				return;
			}
		}

		//Add to suspended list to act as an early out to somewhat protect against brute force attacks.
		SuspendedIps.emplace_back(SIpActivity(signature->ReadAddress(), localEngine->GetElapsedTime(), 1));
	}
}

void AccessControlIpAddress::LoadConfigSettings (ConfigWriter* config)
{
	Super::LoadConfigSettings(config);

	config->GetArrayValues<DString>(TXT("AccessControlIpAddress"), TXT("BlockedIpAddresses"), OUT BlockedIpAddresses);
	MinAttemptInterval = config->GetProperty<Float>(TXT("AccessControlIpAddress"), TXT("MinAttemptInterval"));
	MaxAttemptsAllowed = config->GetProperty<Int>(TXT("AccessControlIpAddress"), TXT("MaxAttemptsAllowed"));
}

bool AccessControlIpAddress::IsIpBlocked (const sf::IpAddress& address) const
{
	DString addressStr(address.toString());
	for (const DString& blockedIp : BlockedIpAddresses)
	{
		DString regexString(DString::Replace(blockedIp, TXT("*"), TXT("[0-9]+"), DString::CC_CaseSensitive));
		regexString.ReplaceInline(TXT("."), TXT("\\."), DString::CC_CaseSensitive); //Need to match literal periods instead of matching any character
		if (addressStr.HasRegexMatch(regexString))
		{
			return true;
		}
	}

	return false;
}
SD_END