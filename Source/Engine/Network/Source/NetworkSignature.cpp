/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkSignature.cpp
=====================================================================
*/

#include "NetworkEngineComponent.h"
#include "NetworkSignature.h"
#include "SignatureIterator.h"

IMPLEMENT_CLASS(SD::NetworkSignature, SD::Object)
SD_BEGIN

void NetworkSignature::InitProps ()
{
	Super::InitProps();

	Address = sf::IpAddress::None;
	NetId = 0;
	NetworkRoles = NR_None;

	AddressInt = 0;
}

void NetworkSignature::BeginObject ()
{
	Super::BeginObject();

	GenerateNetId();
}

DString NetworkSignature::ToString () const
{
	return Super::ToString() + TXT("_") + DString(Address.toString());
}

void NetworkSignature::Destroyed ()
{
	OnDisconnected.Broadcast(this);

	Super::Destroyed();
}

NetworkSignature* NetworkSignature::GetLocalSignature ()
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	return localNetEngine->GetLocalSignature();
}

void NetworkSignature::GenerateNetId ()
{
	if (NetId != 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Already generated a NetId for the NetworkSignature. Its ID is already set to %s."), Int(NetId));
		return;
	}

	static Int nextId = 0;
	++nextId;
	NetId = nextId;
}

bool NetworkSignature::HasRole (ENetworkRole role) const
{
	return ((NetworkRoles & role) > 0);
}

void NetworkSignature::SetAddress (const sf::IpAddress& newAddress)
{
	Address = newAddress;
	AddressInt = Address.toInteger();
}

void NetworkSignature::SetNetworkRoles (ENetworkRole newNetworkRoles)
{
	NetworkRoles = newNetworkRoles;
}
SD_END