/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControlMiscSettings.cpp
=====================================================================
*/

#include "AccessControlMiscSettings.h"
#include "ConnectionManager.h"
#include "NetworkEngineComponent.h"
#include "NetworkSignature.h"

IMPLEMENT_CLASS(SD::AccessControlMiscSettings, SD::AccessControl)
SD_BEGIN

void AccessControlMiscSettings::InitProps ()
{
	Super::InitProps();

	AllowedRoles = -1; //0xFFFFFFF... Enables any type
	MaxConnections = 0;
}

bool AccessControlMiscSettings::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	CHECK(signature != nullptr)

	/*
	signature->GetNetworkRoles() ^ AllowedRoles = get all bit flags that are not equal to each other.
	<result> & (~AllowedRoles) = of the flags that are different, only look at the flags where AllowedRoles is also true
	This produces a num where every 1 represents an AllowedRoles being 0 while signature->GetNetworkRoles is a 1.
	*/
	if (((signature->GetNetworkRoles() ^ AllowedRoles) & (~AllowedRoles)) > 0)
	{
		outRejectReason = RR_InvalidType;
		return false;
	}

	if (MaxConnections > 0)
	{
		NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
		CHECK(localNetEngine != nullptr)
		if (ConnectionManager* netManager = localNetEngine->GetConnections())
		{
			if (netManager->GetNumConnections() >= MaxConnections)
			{
				outRejectReason = RR_AtCapacity;
				return false;
			}
		}
	}

	return true;
}
SD_END