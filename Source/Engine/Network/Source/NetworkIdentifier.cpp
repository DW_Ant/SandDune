/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkIdentifier.cpp
=====================================================================
*/

#include "NetworkIdentifier.h"

IMPLEMENT_ABSTRACT_CLASS(SD::NetworkIdentifier, SD::EntityComponent)
SD_BEGIN
SD_END