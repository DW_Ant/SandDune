/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseRpc.cpp
=====================================================================
*/

#include "BaseRpc.h"
#include "NetworkComponent.h"

SD_BEGIN
BaseRpc::BaseRpc () :
	CanBeExecutedOn(NetworkSignature::NR_Everything),
	CanBeExecutedFrom(NetworkSignature::NR_Everything),
	NetComp(nullptr),
	InstigatedFrom(nullptr),
	RpcHash(0)
{
	//Noop
}

BaseRpc::~BaseRpc ()
{
	if (NetComp != nullptr)
	{
		NetComp->UnregisterRpc(this);
	}
}

BaseRpc::ERpcAvailability BaseRpc::IsAvailableFor (NetworkSignature* calledFrom, NetworkSignature* executedOn) const
{
	if (calledFrom == nullptr || executedOn == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot check if RPC is available without specifying source and destination."));
		return RA_NotPermitted;
	}

	if ((executedOn->GetNetworkRoles() & CanBeExecutedOn) == 0)
	{
		return RA_Unavailable;
	}

	if ((calledFrom->GetNetworkRoles() & CanBeExecutedFrom) == 0)
	{
		return RA_NotPermitted;
	}

	return RA_Available;
}

BaseRpc::ERpcResult BaseRpc::ExecuteBuffer (NetworkSignature* sentFrom, const DataBuffer& params)
{
	ERpcResult result = RR_Internal;
	if (OnExecuteBuffer != nullptr)
	{
		InstigatedFrom = sentFrom;
		result = OnExecuteBuffer(sentFrom, params);
		InstigatedFrom = nullptr;
	}

	return result;
}

void BaseRpc::SetNetComp (NetworkComponent* newNetComp)
{
	if (NetComp != nullptr)
	{
		NetComp->UnregisterRpc(this);
	}

	NetComp = newNetComp;
}
SD_END