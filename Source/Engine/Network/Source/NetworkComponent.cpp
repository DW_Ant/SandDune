/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NetworkComponent.cpp
=====================================================================
*/

#include "BaseRpc.h"
#include "ConnectionManager.h"
#include "NetworkComponent.h"
#include "NetworkEngineComponent.h"

IMPLEMENT_CLASS(SD::NetworkComponent, SD::EntityComponent)
SD_BEGIN

void NetworkComponent::InitProps ()
{
	Super::InitProps();

	NetId = 0;

	RoleComp = nullptr;
}

void NetworkComponent::BeginObject ()
{
	Super::BeginObject();

	GenerateId();
}

void NetworkComponent::Destroyed ()
{
	for (size_t i = 0; i < ReplicatedVariables.size(); ++i)
	{
		if (ReplicatedVariables.at(i) != nullptr)
		{
			delete ReplicatedVariables.at(i);
		}
	}
	ContainerUtils::Empty(OUT ReplicatedVariables);

	for (auto& rpc : Rpcs)
	{
		if (rpc.second != nullptr)
		{
			rpc.second->SetNetComp(nullptr);
		}
	}
	Rpcs.clear();

	//Unregister this component from the connection manager.
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	if (localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	{
		localNetEngine->GetConnections()->UnregisterNetComponent(this);
	}

	//Ensure the callbacks are unregistered in case this network component was forcibly destroyed.
	while (ExternalNetComps.size() > 0)
	{
		RemoveExternalNetComp(ExternalNetComps.begin()->second.RemoteProcess);
	}

	Super::Destroyed();
}

void NetworkComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (newOwner != nullptr)
	{
		Entity* rootEntity = GetRootEntity();
		CHECK(rootEntity != nullptr)
		RoleComp = dynamic_cast<NetworkRoleComponent*>(rootEntity->FindComponent(NetworkRoleComponent::SStaticClass(), false));
	}
}

void NetworkComponent::ComponentDetached ()
{
	RoleComp = nullptr;

	Super::ComponentDetached();
}

void NetworkComponent::RegisterRpc (BaseRpc* newRpc)
{
	if (!newRpc->IsValid())
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to add RPC to %s since that is not bound to a function instance. Call BindRpc before registering it to a NetworkComponent."), ToString());
		return;
	}

	if (Rpcs.contains(newRpc->GetRpcHash()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to add RPC to %s primarily because the function hash %s is already registered to the network component."), ToString(), Int(newRpc->GetRpcHash()));
		return;
	}

	Rpcs.insert({newRpc->GetRpcHash(), newRpc});
	newRpc->SetNetComp(this);
}

void NetworkComponent::UnregisterRpc (BaseRpc* targetRpc)
{
	if (targetRpc == nullptr || !targetRpc->IsValid())
	{
		return;
	}

	if (Rpcs.contains(targetRpc->GetRpcHash()))
	{
		BaseRpc* foundRpc = Rpcs.at(targetRpc->GetRpcHash());
		if (foundRpc == targetRpc)
		{
			Rpcs.at(targetRpc->GetRpcHash()) = nullptr; //Set to null instead of clearing element primarily because remote processes may still be referencing its hash number.
			targetRpc->SetNetComp(nullptr);
		}
	}
}

NetworkRoleComponent::ENetRole NetworkComponent::GetNetRole () const
{
	if (RoleComp == nullptr)
	{
		Entity* rootEntity = GetRootEntity();
		if (rootEntity != nullptr)
		{
			RoleComp = dynamic_cast<NetworkRoleComponent*>(rootEntity->FindComponent(NetworkRoleComponent::SStaticClass(), false));
		}
	}

	if (RoleComp == nullptr)
	{
		return NetworkRoleComponent::NR_Local; //Assume that this is a local entity if there isn't a role component associated with this component tree.
	}

	return RoleComp->GetNetRole();
}

Int NetworkComponent::GetExternalNetId (NetworkSignature* remoteProcess) const
{
	if (remoteProcess == nullptr)
	{
		return 0;
	}

	if (ExternalNetComps.contains(remoteProcess->GetNetId()))
	{
		return ExternalNetComps.at(remoteProcess->GetNetId()).NetId;
	}

	return 0;
}

void NetworkComponent::GenerateId ()
{
	if (NetId != 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to generate an ID for %s since this component already has an id assigned. Its id is %s."), ToString(), NetId);
		return;
	}

	static Int newId = 0;
	++newId;
	NetId = newId;
}

void NetworkComponent::ProcessNonAuthoritativeRole ()
{
	for (BaseReplicatedVariable* repVar : ReplicatedVariables)
	{
		//Prevent this variable from sending its initial data back to the authoritative component.
		repVar->SetWaitForReceive(true);
	}
}

void NetworkComponent::AddExternalNetComp (NetworkSignature* remoteProcess, Int externalNetId, NetworkRoleComponent::ENetRole remoteNetRole)
{
	CHECK(remoteProcess != nullptr)

	ExternalNetComps.insert({remoteProcess->GetNetId(), SRemoteNetComponentData(remoteProcess, externalNetId, remoteNetRole)});
	remoteProcess->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, NetworkComponent, HandleSignatureDisconnected, void, NetworkSignature*));
}

void NetworkComponent::RemoveExternalNetComp (NetworkSignature* remoteProcess)
{
	CHECK(remoteProcess != nullptr)

	if (ExternalNetComps.contains(remoteProcess->GetNetId()))
	{
		ExternalNetComps.erase(remoteProcess->GetNetId());
		remoteProcess->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, NetworkComponent, HandleSignatureDisconnected, void, NetworkSignature*));
	}
}

void NetworkComponent::HandleSignatureDisconnected (NetworkSignature* signature)
{
	CHECK(signature != nullptr)

	if (ExternalNetComps.contains(signature->GetNetId()))
	{
		ExternalNetComps.erase(signature->GetNetId());
	}
}
SD_END