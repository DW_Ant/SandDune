/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseReplicatedVariable.cpp
=====================================================================
*/

#include "BaseReplicatedVariable.h"
#include "NetworkComponent.h"
#include "NetworkRoleComponent.h"

SD_BEGIN
BaseReplicatedVariable::BaseReplicatedVariable () :
	WritePermissions(NetworkSignature::NR_Everything), //By default, everyone has write access
	ReplicationFlags(RF_SyncOnInit | RF_SyncOnChange | RF_BroadcastToAll),
	SyncInterval(0.67f)
{
	//Noop
}

BaseReplicatedVariable::~BaseReplicatedVariable ()
{
	//Noop
}

void BaseReplicatedVariable::ClearReplicationFlags ()
{
	ReplicationFlags = RF_None;
}

bool BaseReplicatedVariable::ShouldReplicate (NetworkSignature* localSignature, NetworkSignature* destination, NetworkComponent* owningComp, Float engineTimestamp) const
{
	if (ReplicationFlags == RF_None)
	{
		return false;
	}

	if ((ReplicationFlags & RF_WaitForReceive) > 0)
	{
		return false;
	}

	if (!localSignature->HasRole(WritePermissions))
	{
		return false;
	}

	if (OnShouldReplicate.IsBounded() && !OnShouldReplicate(destination))
	{
		return false;
	}

	if ((ReplicationFlags & RF_BroadcastToAll) == 0)
	{
		CHECK(destination != nullptr)

		if (!owningComp->ReadExternalNetComps().contains(destination->GetNetId()))
		{
			return false; //NetworkComponent is not connected to destination
		}

		const NetworkComponent::SRemoteNetComponentData& externalData = owningComp->ReadExternalNetComps().at(destination->GetNetId());
		if (externalData.NetRole == NetworkRoleComponent::NR_Local || externalData.NetRole == NetworkRoleComponent::NR_Impotent)
		{
			//Replication flags say that this must be a network owner (or authorative)
			return false;
		}
	}

	return true;
}

void BaseReplicatedVariable::ReplicateVariable (NetworkSignature* destination, Float engineTimestamp, DataBuffer& outVarDataPack)
{
	//Noop
}

BaseReplicatedVariable::EReplicationResults BaseReplicatedVariable::ReceiveVariableUpdate (NetworkSignature* source, Float engineTimestamp, const DataBuffer& varDataPack)
{
	EReplicationResults result = CanReceiveUpdates(source);
	if (result != RR_Success)
	{
		return result;
	}

	result = WriteVariableFromBuffer(source, engineTimestamp, varDataPack);
	if (result != RR_Success)
	{
		return result;
	}

	SetWaitForReceive(false);

	if (OnReplicated.IsBounded())
	{
		OnReplicated.Execute();
	}

	return RR_Success;
}

void BaseReplicatedVariable::SetSyncOnInit (bool bNewSyncOnInit)
{
	if (bNewSyncOnInit)
	{
		ReplicationFlags |= RF_SyncOnInit;
	}
	else
	{
		ReplicationFlags &= ~RF_SyncOnInit;
	}
}

void BaseReplicatedVariable::SetSyncOnChange (bool bNewSyncOnChange)
{
	if (bNewSyncOnChange)
	{
		ReplicationFlags |= RF_SyncOnChange;
	}
	else
	{
		ReplicationFlags &= ~RF_SyncOnChange;
	}
}

void BaseReplicatedVariable::SetBroadcastToAll (bool bNewBroadcastToAll)
{
	if (bNewBroadcastToAll)
	{
		ReplicationFlags |= RF_BroadcastToAll;
	}
	else
	{
		ReplicationFlags &= ~RF_BroadcastToAll;
	}
}

void BaseReplicatedVariable::SetWaitForReceive (bool bNewWaitForReceive)
{
	if (bNewWaitForReceive)
	{
		ReplicationFlags |= RF_WaitForReceive;
	}
	else
	{
		ReplicationFlags &= ~RF_WaitForReceive;
	}
}

void BaseReplicatedVariable::SetSyncInterval (Float newSyncInterval)
{
	SyncInterval = newSyncInterval;
}

BaseReplicatedVariable::EReplicationResults BaseReplicatedVariable::CanReceiveUpdates (NetworkSignature* source) const
{
	if (!source->HasRole(WritePermissions))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Refusing to receive variable updates from %s since that signature does not have write access."), source->ToString());
		return RR_NoWritePermissions;
	}

	return RR_Success;
}
SD_END