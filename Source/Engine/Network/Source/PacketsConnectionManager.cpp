/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PacketsConnectionManager.cpp
=====================================================================
*/

#include "PacketOperators.h"
#include "PacketsConnectionManager.h"

SD_BEGIN

int ConnectionManagerPacket::EnableDynamicCasting () const
{
	return 0;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketOpenConnection& openConnection)
{
	unsigned char roleNum = static_cast<unsigned char>(openConnection.RequesterRole);
	return (packet << openConnection.RequesterIp.toInteger() << roleNum << openConnection.IdentifierData);
}

sf::Packet& operator>> (sf::Packet& packet, PacketOpenConnection& outOpenConnection)
{
	sf::Uint32 address;
	unsigned char roleNum;
	if (!(packet >> address) || !(packet >> roleNum) || !(packet >> outOpenConnection.IdentifierData))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketOpenConnection from packet."));
		outOpenConnection.RequesterIp = sf::IpAddress::None;
		outOpenConnection.RequesterRole = NetworkSignature::NR_None;
		outOpenConnection.IdentifierData.EmptyBuffer();
	}
	else
	{
		outOpenConnection.RequesterIp = sf::IpAddress(address);
		outOpenConnection.RequesterRole = static_cast<NetworkSignature::ENetworkRole>(roleNum);
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketAcceptConnection& acceptConnection)
{
	unsigned char netRoleNum = static_cast<unsigned char>(acceptConnection.RemoteNetworkRoles);
	unsigned char listenerPerm = static_cast<unsigned char>(acceptConnection.ListenerPermissions);
	unsigned char requesterPerm = static_cast<unsigned char>(acceptConnection.RequesterPermissions);
	return (packet << acceptConnection.AcceptedIp.toInteger() << netRoleNum << listenerPerm << requesterPerm << acceptConnection.RemoteIdentifierData);
}

sf::Packet& operator>> (sf::Packet& packet, PacketAcceptConnection& outAcceptConnection)
{
	sf::Uint32 address;
	unsigned char netRoleNum;
	unsigned char listenerPerm;
	unsigned char requesterPerm;
	if (!(packet >> address) || !(packet >> netRoleNum) || !(packet >> listenerPerm) || !(packet >> requesterPerm) || !(packet >> outAcceptConnection.RemoteIdentifierData))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketAcceptConnection from packet."));
		outAcceptConnection.AcceptedIp = sf::IpAddress::None;
		outAcceptConnection.RemoteNetworkRoles = NetworkSignature::NR_None;
		outAcceptConnection.ListenerPermissions = NetworkGlobals::P_None;
		outAcceptConnection.RequesterPermissions = NetworkGlobals::P_None;
		outAcceptConnection.RemoteIdentifierData.EmptyBuffer();
	}
	else
	{
		outAcceptConnection.AcceptedIp = sf::IpAddress(address);
		outAcceptConnection.RemoteNetworkRoles = static_cast<NetworkSignature::ENetworkRole>(netRoleNum);
		outAcceptConnection.ListenerPermissions = static_cast<NetworkGlobals::EPermissions>(listenerPerm);
		outAcceptConnection.RequesterPermissions = static_cast<NetworkGlobals::EPermissions>(requesterPerm);
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectConnection& rejectConnection)
{
	return (packet << rejectConnection.RejectedIp.toInteger() << rejectConnection.ReasonSwitch);
}

sf::Packet& operator>> (sf::Packet& packet, PacketRejectConnection& outRejectConnection)
{
	sf::Uint32 address;
	if (!(packet >> address) || !(packet >> outRejectConnection.ReasonSwitch))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketRejectConnection from packet."));
		outRejectConnection.RejectedIp = sf::IpAddress::None;
		outRejectConnection.ReasonSwitch = -1;
	}
	else
	{
		outRejectConnection.RejectedIp = sf::IpAddress(address);
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketInstantiateObj& netEntityData)
{
	return (packet << netEntityData.NetId << netEntityData.OwnerClassHash << netEntityData.bIsBothNetOwner << netEntityData.NetCompNetIds);
}

sf::Packet& operator>> (sf::Packet& packet, PacketInstantiateObj& outNetEntityData)
{
	if (!(packet >> outNetEntityData.NetId) || !(packet >> outNetEntityData.OwnerClassHash) || !(packet >> outNetEntityData.bIsBothNetOwner) ||
		!(packet >> outNetEntityData.NetCompNetIds))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketInstantiateObj from packet."));
		outNetEntityData.NetId = 0;
		outNetEntityData.OwnerClassHash = 0;
		outNetEntityData.bIsBothNetOwner = false;
		outNetEntityData.NetCompNetIds.EmptyBuffer();
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketAcceptInstantiateObj& acceptedData)
{
	return (packet << acceptedData.InstigatorNetId << acceptedData.NetId << acceptedData.AcceptedNetCompIds);
}

sf::Packet& operator>> (sf::Packet& packet, PacketAcceptInstantiateObj& outAcceptedData)
{
	if (!(packet >> outAcceptedData.InstigatorNetId) || !(packet >> outAcceptedData.NetId) || !(packet >> outAcceptedData.AcceptedNetCompIds))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketAcceptInstantiateObj from packet."));
		outAcceptedData.InstigatorNetId = 0;
		outAcceptedData.NetId = 0;
		outAcceptedData.AcceptedNetCompIds.EmptyBuffer();
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectInstantiateObj& rejectData)
{
	return (packet << rejectData.InstigatorNetId);
}

sf::Packet& operator>> (sf::Packet& packet, PacketRejectInstantiateObj& outRejectData)
{
	if (!(packet >> outRejectData.InstigatorNetId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketRejectInstantiateObj from packet."));
		outRejectData.InstigatorNetId = 0;
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketDisconnectEntity& disconnectData)
{
	return (packet << disconnectData.ReceiverNetId);
}

sf::Packet& operator>> (sf::Packet& packet, PacketDisconnectEntity& outDisconnectData)
{
	if (!(packet >> outDisconnectData.ReceiverNetId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketDisconnectEntity from the packet."));
		outDisconnectData.ReceiverNetId = 0;
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketNetCompData& netData)
{
	return (packet << netData.CompNetId << netData.CompData);
}

sf::Packet& operator>> (sf::Packet& packet, PacketNetCompData& outNetData)
{
	if (!(packet >> outNetData.CompNetId) || !(packet >> outNetData.CompData))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketNetCompData from packet."));
		outNetData.CompNetId = 0;
		outNetData.CompData.EmptyBuffer();
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectNetCompData& rejectData)
{
	unsigned char reasonNum = static_cast<unsigned char>(rejectData.Reason);
	return (packet << rejectData.CompNetId << rejectData.RepVarIdx << reasonNum);
}

sf::Packet& operator>> (sf::Packet& packet, PacketRejectNetCompData& outRejectData)
{
	unsigned char reasonNum;
	if (!(packet >> outRejectData.CompNetId) || !(packet >> outRejectData.RepVarIdx) || !(packet >> reasonNum))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketRejectNetCompData from packet."));
		outRejectData.CompNetId = 0;
		outRejectData.RepVarIdx = 0;
		outRejectData.Reason = PacketRejectNetCompData::RR_Unknown;
	}
	else
	{
		outRejectData.Reason = static_cast<PacketRejectNetCompData::ERejectionReason>(reasonNum);
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketNetCompRpc& rpcData)
{
	return (packet << rpcData.NetId << rpcData.FunctionId << rpcData.Parameters);
}

sf::Packet& operator>> (sf::Packet& packet, PacketNetCompRpc& outRpcData)
{
	if (!(packet >> outRpcData.NetId) || !(packet >> outRpcData.FunctionId) || !(packet >> outRpcData.Parameters))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketNetCompRpc from packet."));
		outRpcData.NetId = 0;
		outRpcData.FunctionId = 0;
		outRpcData.Parameters.EmptyBuffer();
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketRejectNetCompRpc& rejectData)
{
	unsigned char reasonNum = static_cast<unsigned char>(rejectData.Reason);
	return (packet << rejectData.NetId << rejectData.FunctionId << reasonNum);
}

sf::Packet& operator>> (sf::Packet& packet, PacketRejectNetCompRpc& outRejectData)
{
	unsigned char reasonNum;
	if (!(packet >> outRejectData.NetId) || !(packet >> outRejectData.FunctionId) || !(packet >> reasonNum))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketRejectNetCompRpc from packet."));
		outRejectData.NetId = 0;
		outRejectData.FunctionId = 0;
		outRejectData.Reason = PacketRejectNetCompRpc::RR_Unknown;
	}
	else
	{
		outRejectData.Reason = static_cast<PacketRejectNetCompRpc::ERejectReason>(reasonNum);
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketDisconnectNetComp& disconnectData)
{
	return (packet << disconnectData.NetId);
}

sf::Packet& operator>> (sf::Packet& packet, PacketDisconnectNetComp& outDisconnectData)
{
	if (!(packet >> outDisconnectData.NetId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketDisconnectNetComp from packet."));
		outDisconnectData.NetId = 0;
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketSendHeartbeat& heartbeat)
{
	return (packet << heartbeat.HeartbeatId);
}

sf::Packet& operator>> (sf::Packet& packet, PacketSendHeartbeat& outHeartbeat)
{
	if (!(packet >> outHeartbeat.HeartbeatId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketSendHeartbeat from packet."));
		outHeartbeat.HeartbeatId = 0;
	}

	return packet;
}

sf::Packet& operator<< (sf::Packet& packet, const PacketReceiveHeartbeat& heartbeat)
{
	return (packet << heartbeat.HeartbeatId);
}

sf::Packet& operator>> (sf::Packet& packet, PacketReceiveHeartbeat& outHeartbeat)
{
	if (!(packet >> outHeartbeat.HeartbeatId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract PacketReceiveHeartbeat from packet."));
		outHeartbeat.HeartbeatId = 0;
	}

	return packet;
}
SD_END