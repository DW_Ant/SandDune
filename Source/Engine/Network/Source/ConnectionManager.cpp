/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ConnectionManager.cpp
=====================================================================
*/

#include "AccessControl.h"
#include "BaseReplicatedVariable.h"
#include "BaseRpc.h"
#include "ConnectionManager.h"
#include "NetworkComponent.h"
#include "NetworkEngineComponent.h"
#include "NetworkIdentifier.h"
#include "NetworkMonitor.h"
#include "NetworkRoleComponent.h"
#include "NetworkSignature.h"
#include "PacketsConnectionManager.h"
#include "PacketOperators.h"

IMPLEMENT_CLASS(SD::ConnectionManager, SD::Entity)
SD_BEGIN

ConnectionManager::SSocketData::SSocketData () :
	Status(SS_Dormant)
{
	Socket.setBlocking(false);
}

ConnectionManager::SListenerData::SListenerData (SSocketData* inSocket) :
	Socket(inSocket)
{
	Listener.setBlocking(false);
}

ConnectionManager::SPacketData::SPacketData (ENetMessageType inPacketType, ConnectionManagerPacket* inPacket) :
	PacketType(inPacketType),
	Packet(inPacket)
{
	//Noop
}

ConnectionManager::SPacketData::~SPacketData ()
{
	//Noop
}

ConnectionManager::SConnectionAttempt::SConnectionAttempt (const sf::IpAddress& inAddress, Float inConnectionTime) :
	Address(inAddress),
	ConnectionTime(inConnectionTime)
{
	//Noop
}

ConnectionManager::SHeartbeatData::SHeartbeatData () :
	bWaitingResponse(false),
	LatestHeartbeatId(0),
	LatestSentTimestamp(-1.f),
	LatestReceivedTimestamp(-1.f),
	LatestPingsIdx(0),
	AvgPing(0.f)
{
	for (unsigned int i = 0; i < SD_NETWORK_LATEST_PINGS_SIZE; ++i)
	{
		LatestPings[i] = 0.f;
	}
}
	
ConnectionManager::SActiveConnection::SActiveConnection (NetworkSignature* inDestination, SSocketData* inSocket) :
	Destination(inDestination),
	Socket(inSocket),
	bApprovedConnection(false),
	ConnectionRequestTime(-1.f),
	LocalPermissions(NetworkGlobals::P_None),
	RemotePermissions(NetworkGlobals::P_None),
	ReceivePacketNum(-1),
	TotalReceivePackets(-1),
	bTerminateOnSend(false),
	bPendingDelete(false)
{
	//Update the heartbeat timestamps to current time
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	Heartbeat.LatestSentTimestamp = localEngine->GetElapsedTime();
	Heartbeat.LatestReceivedTimestamp = localEngine->GetElapsedTime();
}

void ConnectionManager::InitProps ()
{
	Super::InitProps();

	TimeoutTime = 30.f;
	ReceivePacketTimeLimit = 1.f;
	bRejectDuplicateConnections = false;
	HeartbeatSendInterval = 4.f;
	OpenConnectionMaxSize = 1024; //One kb limit should suffice. Can be overwritten in ini.
	ListenerPermissions = NetworkGlobals::P_Everything;
	DesiredPermissions = NetworkGlobals::P_Client;
	ConnectionAttemptInterval = 8.f;
	HeartbeatTick = nullptr;
	NetMonitor = nullptr;

	RolePermissions =
	{
		{NetworkSignature::NR_Client, NetworkGlobals::P_Client},
		{NetworkSignature::NR_Server, NetworkGlobals::P_Everything},
		{NetworkSignature::NR_MasterServer, NetworkGlobals::P_Everything},
		{NetworkSignature::NR_Service, NetworkGlobals::P_Client},
		{NetworkSignature::NR_Other, NetworkGlobals::P_None}
	};
}

void ConnectionManager::BeginObject ()
{
	Super::BeginObject();

	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Network.ini")), false))
	{
		TimeoutTime = config->GetProperty<Float>(TXT("ConnectionManager"), TXT("TimeoutTime"));
		ReceivePacketTimeLimit = config->GetProperty<Float>(TXT("ConnectionManager"), TXT("ReceivePacketTimeLimit"));
		HeartbeatSendInterval = config->GetProperty<Float>(TXT("ConnectionManager"), TXT("HeartbeatSendInterval"));
		Int openMaxSize = config->GetProperty<Int>(TXT("ConnectionManager"), TXT("OpenConnectionMaxSize"));
		if (openMaxSize > 0) //failsafe in case the ini does not specify max size. It's dangerous to allow unauthorized processes to send infinite data.
		{
			OpenConnectionMaxSize = openMaxSize;
		}

		ConnectionAttemptInterval = config->GetProperty<Float>(TXT("ConnectionManager"), TXT("ConnectionAttemptInterval"));
	}
	config->Destroy();

	HeartbeatTick = TickComponent::CreateObject(TICK_GROUP_NETWORK);
	if (AddComponent(HeartbeatTick))
	{
		HeartbeatTick->SetTickHandler(SDFUNCTION_1PARAM(this, ConnectionManager, HandleHeartbeatCheck, void, Float));
		HeartbeatTick->SetTickInterval(1.f);
		HeartbeatTick->SetTicking(TimeoutTime > 0.f);
	}

	NetProcessTick = TickComponent::CreateObject(TICK_GROUP_NETWORK);
	if (AddComponent(NetProcessTick))
	{
		NetProcessTick->SetTickHandler(SDFUNCTION_1PARAM(this, ConnectionManager, HandleNetProcessTick, void, Float));
	}

	NetMonitor = NetworkMonitor::CreateObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, ConnectionManager, HandlePreGarbageCollection, void));
}

void ConnectionManager::Destroyed ()
{
	if (!IsDefaultObject())
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, ConnectionManager, HandlePreGarbageCollection, void));
	}

	if (NetMonitor != nullptr)
	{
		NetMonitor->Destroy();
		NetMonitor = nullptr;
	}

	for (std::shared_ptr<SListenerData>& listener : Listeners)
	{
		listener->Listener.close();

		if (listener->Socket != nullptr)
		{
			listener->Socket->Socket.disconnect();
			listener->Socket->Status = SSocketData::SS_Dormant;
		}
	}
	ContainerUtils::Empty(OUT Listeners);

	bool logIpPermitted = false;
	NetworkEngineComponent* netEngine = NetworkEngineComponent::Find();
	if (netEngine != nullptr)
	{
		logIpPermitted = netEngine->IsIpLogginedPermitted();
	}

	if (!logIpPermitted)
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("Terminating %s connection(s)."), Int(Connections.size()));
	}

	//Terminate any active connections and notify the NetworkComponents
	for (auto& activeConnection : Connections)
	{
		if (logIpPermitted)
		{
			NetworkLog.Log(LogCategory::LL_Log, TXT("Terminating connection to %s."), DString(activeConnection.second.Destination->ReadAddress().toString()));
		}

		if (activeConnection.second.bApprovedConnection)
		{
			OnDisconnect.Broadcast(activeConnection.second.Destination);
		}

		activeConnection.second.Destination->Destroy();
		activeConnection.second.Socket->Socket.disconnect();
		activeConnection.second.Socket->Status = SSocketData::SS_Dormant;
	}
	Connections.clear();

	for (auto iter = InstantiatedEntities.begin(); iter != InstantiatedEntities.end(); ++iter)
	{
		if ((*iter).IsValid())
		{
			(*iter)->Destroy();
		}
	}
	InstantiatedEntities.clear();

	Super::Destroyed();
}

ConnectionManager* ConnectionManager::GetConnectionManager ()
{
	if (NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find())
	{
		return localNetEngine->GetConnections();
	}

	return nullptr;
}

bool ConnectionManager::OpenPort (unsigned short port)
{
	for (const std::shared_ptr<SListenerData>& listener : Listeners)
	{
		if (listener->Listener.getLocalPort() == port)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("OpenPort to %s is ignored since there is already a socket listening on that port."), Int(port));
			return false;
		}
	}

	//Find a socket available
	size_t socketIdx = UINT_INDEX_NONE;
	for (size_t i = 0; i < SD_NETWORK_MAX_CONNECTIONS; ++i)
	{
		if (Sockets[i].Status == SSocketData::SS_Dormant)
		{
			socketIdx = i;
			break;
		}
	}

	if (socketIdx == UINT_INDEX_NONE)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open port %s since all sockets are currently in use. Consider reducing the number of connections or increasing the value of SD_NETWORK_MAX_CONNECTIONS."), Int(port));
		return false;
	}

	size_t listenIdx = Listeners.size();
	Sockets[socketIdx].Status = SSocketData::SS_Listening;
	Listeners.push_back(std::shared_ptr<SListenerData>(new SListenerData(&Sockets[socketIdx])));
	if (Listeners.at(listenIdx)->Listener.listen(port) != sf::Socket::Status::Done)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to create a TcpListener that'll listen on port %s."), Int(port));
		Sockets[socketIdx].Status = SSocketData::SS_Dormant;
		Listeners.pop_back();
		return false;
	}

	NetworkLog.Log(LogCategory::LL_Log, TXT("Port %s is now listening for incoming connections."), Int(port));

	return true;
}

bool ConnectionManager::ClosePort (unsigned short port)
{
	for (size_t i = 0; i < Listeners.size(); ++i)
	{
		if (Listeners.at(i)->Listener.getLocalPort() == port)
		{
			Listeners.at(i)->Listener.close();
			Listeners.at(i)->Socket->Status = SSocketData::SS_Dormant;
			Listeners.erase(Listeners.begin() + i);
			NetworkLog.Log(LogCategory::LL_Log, TXT("Port %s is now closed."), Int(port));
			return true;
		}
	}

	NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to close port %s since there aren't any listeners on that port."), Int(port));
	return false;
}

void ConnectionManager::CloseAllPorts ()
{
	while (Listeners.size() > 0)
	{
		size_t idx = Listeners.size() - 1;
		Listeners.at(idx)->Listener.close();
		Listeners.at(idx)->Socket->Status = SSocketData::SS_Dormant;
		Listeners.pop_back();
	}
}

void ConnectionManager::OpenConnection (const sf::IpAddress& destination, unsigned short port)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)

	NetworkSignature* localSignature = localNetEngine->GetLocalSignature();
	if (localSignature == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open connection to %s since a network signature is not specified."), DString(destination.toString()));
		OnConnectionRejected.Broadcast(destination, AccessControl::RR_InvalidData);
		return;
	}

	if (destination == sf::IpAddress::None)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open connection to %s since that is not a valid ip address."), DString(destination.toString()));
		OnConnectionRejected.Broadcast(destination, AccessControl::RR_DestNotFound);
		return;
	}

	if (IsRejectingDuplicateConnections())
	{
		//Ensure this process is not already connected
		sf::Uint32 destInt = destination.toInteger();
		for (const auto& connection : Connections)
		{
			if (connection.second.Destination->GetAddressInt() == destInt)
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open connection to %s since there is already an active connection to that address."), DString(destination.toString()));
				OnConnectionRejected.Broadcast(destination, AccessControl::RR_AlreadyConnected);
				return;
			}
		}
	}

	//Find a socket available
	size_t socketIdx = UINT_INDEX_NONE;
	for (size_t i = 0; i < SD_NETWORK_MAX_CONNECTIONS; ++i)
	{
		if (Sockets[i].Status == SSocketData::SS_Dormant)
		{
			socketIdx = i;
			break;
		}
	}

	if (socketIdx == UINT_INDEX_NONE)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open connection to %s since there aren't any TcpSockets available to use."), DString(destination.toString()));
		OnConnectionRejected.Broadcast(destination, AccessControl::RR_OutOfResource);
		return;
	}

	DataBuffer sigData;
	for (ComponentIterator iter(localSignature, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (NetworkIdentifier* identifier = dynamic_cast<NetworkIdentifier*>(iter.GetSelectedComponent()))
		{
			const DClass* notUsed = nullptr;
			size_t identifierClassHash = DClassAssembler::CalcDClassHash(identifier->StaticClass()->ReadDuneClassName(), OUT notUsed);
			sigData << identifierClassHash;
			identifier->WriteDataToBuffer(OUT sigData);
		}
	}

	PacketOpenConnection* openConnectionPacket = new PacketOpenConnection(NetworkEngineComponent::GetPublicAddress(), localSignature->GetNetworkRoles(), sigData);

	NetworkSignature* newSignature = NetworkSignature::CreateObject();
	newSignature->SetAddress(destination);
	auto insertionResult = Connections.insert({newSignature->GetNetId(), SActiveConnection(newSignature, &Sockets[socketIdx])});
	if (!insertionResult.second)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open a connection to %s since the Network ID %s is already occupied in the Connections map."), DString(destination.toString()), newSignature->GetNetId());
		OnConnectionRejected.Broadcast(destination, AccessControl::RR_InternalError);
		return;
	}

	sf::Socket::Status status = insertionResult.first->second.Socket->Socket.connect(destination, port, sf::Time(sf::seconds(TimeoutTime.Value)));
	//NOTE: on Windows, connecting always seem to return WSAEWOULDBLOCK. SFML maps that error code to Socket::NotReady. Ignore this particular error message. The other case SFML maps to NotReady is WSAEALREADY, which is already checked in the ConnectionManager.
	if (status != sf::Socket::Done && status != sf::Socket::NotReady)
	{
		Connections.erase(newSignature->GetNetId());
#ifdef PLATFORM_WINDOWS
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open a connection to %s. There was an internal error when connecting the socket. WSAGetLastError returns %s."), DString(destination.toString()), Int(WSAGetLastError()));
#else
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to open a connection to %s. There was an internal error when connecting the socket."), DString(destination.toString()));
#endif
		OnConnectionRejected.Broadcast(destination, AccessControl::RR_InternalError);
		return;
	}

	insertionResult.first->second.Socket->Status = SSocketData::SS_Active;
	insertionResult.first->second.PendingData.emplace_back(SPacketData(NMT_Req_OpenConnection, openConnectionPacket));

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	insertionResult.first->second.ConnectionRequestTime = localEngine->GetElapsedTime();
}

void ConnectionManager::InstantiateEntity (NetworkRoleComponent* roleComp, NetworkSignature* destination, bool isDestinationNetOwner)
{
	if (roleComp == nullptr || roleComp->GetOwner() == nullptr || destination == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot instantiate an entity on the remote process without specifying a destination and without an role component attached to an entity."));
		return;
	}

	if (roleComp->GetNetRole() != NetworkRoleComponent::NR_Authority)
	{
		roleComp->ProcessInstantiationRejected(destination);
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Only authoritative NetworkRoleComponent are permitted to instantiate entities on remote processes."));
		return;
	}

	if (!Connections.contains(destination->GetNetId()))
	{
		roleComp->ProcessInstantiationRejected(destination);
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate %s to %s since there isn't a connection to that address."), roleComp->GetOwner()->ToString(), DString(destination->GetAddress().toString()));
		return;
	}

	SActiveConnection& connection = Connections.at(destination->GetNetId());
	if (!connection.bApprovedConnection)
	{
		roleComp->ProcessInstantiationRejected(destination);
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate %s to %s since this process is not yet connected to that address."), roleComp->GetOwner()->ToString(), destination->ToString());
		return;
	}

	if ((connection.LocalPermissions & NetworkGlobals::P_InstantiateComponent) == 0)
	{
		roleComp->ProcessInstantiationRejected(destination);
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate %s to %s since this process does not have permissions to instantiate objects on the remote process."), roleComp->GetOwner()->ToString(), destination->ToString());
		return;
	}

	std::vector<NetworkComponent*> netComps;
	roleComp->GetNetComponentList(OUT netComps);

	//Ensure it's not already connected
	if (connection.RoleComponents.contains(roleComp->GetNetId()))
	{
		roleComp->ProcessInstantiationRejected(destination);
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate %s to %s since that role component already has either a pending request to instantiate or already has an object paired up on the remote process."), roleComp->GetOwner()->ToString(), destination->ToString());
		return;
	}
	connection.RoleComponents.insert({roleComp->GetNetId(), roleComp});

	DataBuffer componentData;
	for (size_t i = 0; i < netComps.size(); ++i)
	{
		if (netComps.at(i) != nullptr)
		{
			auto insertResult = connection.NetComponents.insert({netComps.at(i)->GetNetId(), netComps.at(i)});
			if (!insertResult.second)
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate %s to %s since the network component %s is already inserted in the map."), roleComp->GetOwner()->ToString(), destination->ToString(), netComps.at(i)->GetNetId());
				roleComp->ProcessInstantiationRejected(destination);

				//Remove previous net components
				for (size_t removeIdx = 0; removeIdx < i; ++removeIdx)
				{
					connection.NetComponents.erase(netComps.at(removeIdx)->GetNetId());
				}

				connection.RoleComponents.erase(roleComp->GetNetId());
				return;
			}

			componentData << netComps.at(i)->GetNetId();
		}
		else
		{
			//Add a blank entry to notify the remote process that normally there would be a component there, but the one on this process is destroyed. Stub it out without displacing the id buffers.
			componentData << Int(0);
		}
	}

	CHECK(roleComp->GetOwner()->StaticClass() != nullptr)
	size_t classHash = roleComp->GetOwner()->StaticClass()->GetClassHash();
	PacketInstantiateObj* objPacket = new PacketInstantiateObj(roleComp->GetNetId(), classHash, isDestinationNetOwner, componentData);
	connection.PendingData.emplace_back(SPacketData(NMT_Req_InstantiateObj, objPacket));
}

void ConnectionManager::ReplicateVariables (NetworkSignature* destination, NetworkComponent* netComp, const DataBuffer& varData)
{
	if (destination == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to replicate variables for a null destination"));
		return;
	}

	if (netComp == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to replicate variables from a null NetworkComponent."));
		return;
	}

	if (!Connections.contains(destination->GetNetId()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to replicate variables to %s since the Connections does not map to a signature with ID %s."), destination->ToString(), destination->GetNetId());
		return;
	}

	SActiveConnection& connection = Connections.at(destination->GetNetId());
	if (connection.bTerminateOnSend || !connection.bApprovedConnection)
	{
		return;
	}

	NetworkGlobals::EPermissions checkPerms = NetworkGlobals::P_SendData;
	if (netComp->GetNetRole() != NetworkRoleComponent::NR_Impotent)
	{
		checkPerms |= NetworkGlobals::P_SendDataNetOwner;
	}

	if ((connection.LocalPermissions & checkPerms) == 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to replicate variables to %s since this process does not have the correct permissions to do so."), connection.Destination->ToString());
		return;
	}

	Int externalNetId = netComp->GetExternalNetId(destination);
	PacketNetCompData* newData = new PacketNetCompData(externalNetId, varData);
	connection.PendingData.emplace_back(SPacketData(NMT_Msg_NetCompData, newData));
}

bool ConnectionManager::ExecuteRpc (NetworkSignature* destination, NetworkComponent* netComp, size_t functionHash, const DataBuffer& params)
{
	if (destination == nullptr || netComp == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute RPC without specifying a signature or a network component."));
		return false;
	}

	if (!Connections.contains(destination->GetNetId()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute RPC on %s since this process is not connected to that destination."), destination->ToString());
		return false;
	}

	SActiveConnection& connection = Connections.at(destination->GetNetId());
	if (connection.bTerminateOnSend || !connection.bApprovedConnection)
	{
		return false;
	}

	NetworkGlobals::EPermissions permCheck = NetworkGlobals::P_Rpc;
	if (netComp->GetNetRole() != NetworkRoleComponent::NR_Impotent)
	{
		permCheck |= NetworkGlobals::P_RpcNetOwner;
	}

	if ((connection.LocalPermissions & permCheck) == 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute RPC on %s with net component %s since this process does not have permission for remote execution."), destination->ToString(), netComp->ToString());
		return false;
	}

	Int remoteCompId = netComp->GetExternalNetId(destination);
	if (remoteCompId == 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot execute RPC on %s since the net component %s is not connected to that destination."), destination->ToString(), netComp->ToString());
		return false;
	}

	PacketNetCompRpc* rpcPacket = new PacketNetCompRpc(remoteCompId, functionHash, params);
	connection.PendingData.emplace_back(SPacketData(NMT_Msg_NetCompRpc, rpcPacket));
	return true;
}

void ConnectionManager::UnregisterNetComponent (NetworkComponent* targetComp)
{
	if (targetComp == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot unregister a null NetworkComponent from the connection manager."));
		return;
	}

	for (const auto& externalComp : targetComp->ReadExternalNetComps())
	{
		if (externalComp.second.RemoteProcess == nullptr)
		{
			continue;
		}

		if (Connections.contains(externalComp.second.RemoteProcess->GetNetId()))
		{
			SActiveConnection& connection = Connections.at(externalComp.second.RemoteProcess->GetNetId());
			connection.NetComponents.erase(targetComp->GetNetId());
		}
	}
}

void ConnectionManager::DisconnectEntity (NetworkRoleComponent* roleComp, NetworkSignature* remoteSignature, Int externalNetId)
{
	if (roleComp == nullptr || remoteSignature == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to disconnect entity. Cannot disconnect from a null role component or remote signature."));
		return;
	}

	if (!Connections.contains(remoteSignature->GetNetId()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to disconnect %s since this process is not connected to %s."), roleComp->GetOwner()->ToString(), remoteSignature->ToString());
		return;
	}

	SActiveConnection& connection = Connections.at(remoteSignature->GetNetId());
	size_t numErased = connection.RoleComponents.erase(roleComp->GetNetId());
	if (numErased == 0)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to disconnect entity since %s is not connected to %s."), roleComp->GetOwner()->ToString(), remoteSignature->ToString());
		return;
	}

	std::vector<NetworkComponent*> netComps;
	roleComp->GetNetComponentList(OUT netComps);
	for (NetworkComponent* netComp : netComps)
	{
		if (netComp != nullptr)
		{
			if (netComp->GetNetId() != 0)
			{
				connection.NetComponents.erase(netComp->GetNetId());
			}

			if (netComp->IsConnected())
			{
				netComp->RemoveExternalNetComp(remoteSignature);
			}
		}
	}

	if (roleComp->GetNetId() == 0 || roleComp->GetNetRole() == NetworkRoleComponent::NR_Local)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to disconnect entity since %s is already a local entity."), roleComp->GetOwner()->ToString());
		return;
	}

	//This could be zero if a NetworkRoleComponent timed out while waiting for a response. In that case, there's no need to set the client a notification to disconnect Entity since one was never established.
	//This could also happen if this process is disconnecting the entity without wanting to notify the remote process (possibly because the remote process is already disconnected).
	if (externalNetId != 0)
	{
		//Notify the remote process about the disconnect
		PacketDisconnectEntity* disconnectPacket = new PacketDisconnectEntity(externalNetId);
		connection.PendingData.emplace_back(SPacketData(NMT_Msg_ObjLoseRelevance, disconnectPacket));
	}
}

void ConnectionManager::Disconnect (NetworkSignature* disconnectFrom, bool bForceTerminate)
{
	if (disconnectFrom == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot disconnect from a nullptr signature."));
		return;
	}

	if (!Connections.contains(disconnectFrom->GetNetId()))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("%s is not registered to the ConnectionManager. Unable to disconnect from %s."), disconnectFrom->ToString(), DString(disconnectFrom->ReadAddress().toString()));
		return;
	}

	SActiveConnection& connection = Connections.at(disconnectFrom->GetNetId());

	//Notify any component associated with this connection to disconnect.
	std::vector<NetworkRoleComponent*> compsToRemove;
	for (auto& roleComp : connection.RoleComponents)
	{
		if (roleComp.second->GetNetRole() != NetworkRoleComponent::NR_Local)
		{
			compsToRemove.push_back(roleComp.second);
		}
	}

	for (size_t i = 0; i < compsToRemove.size(); ++i)
	{
		PacketDisconnectEntity disconnectData(compsToRemove.at(i)->GetNetId());
		ProcessDisconnectEntity(OUT connection, disconnectData);
	}
	ContainerUtils::Empty(OUT compsToRemove);

	if (bForceTerminate)
	{
		if (connection.bApprovedConnection)
		{
			NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
			if (localNetEngine != nullptr && localNetEngine->IsIpLogginedPermitted())
			{
				NetworkLog.Log(LogCategory::LL_Log, TXT("Terminating connection to %s"), DString(connection.Destination->ReadAddress().toString()));
			}
			else
			{
				NetworkLog.Log(LogCategory::LL_Log, TXT("Terminating a connection."));
			}
		}

		connection.Socket->Socket.disconnect();
		connection.Socket->Status = SSocketData::SS_Dormant;
		connection.bPendingDelete = true;
	}
	else
	{
		connection.bTerminateOnSend = true;
	}
}

void ConnectionManager::DisconnectFromAll (bool bForceTerminate)
{
	for (auto& connection : Connections)
	{
		if (connection.second.Destination == nullptr)
		{
			continue;
		}

		Disconnect(connection.second.Destination, bForceTerminate);
	}
}

bool ConnectionManager::RemoveOwnershipFromEntity (Entity* targetEntity)
{
	for (auto iter = InstantiatedEntities.begin(); iter != InstantiatedEntities.end(); ++iter)
	{
		if (*iter == targetEntity)
		{
			iter = InstantiatedEntities.erase(iter);
			return true;
		}
	}

	return false;
}

void ConnectionManager::SetTimeoutTime (Float newTimeoutTime)
{
	TimeoutTime = newTimeoutTime;

	if (HeartbeatTick != nullptr)
	{
		HeartbeatTick->SetTicking(TimeoutTime > 0.f);
	}
}

void ConnectionManager::SetReceivePacketTimeLimit (Float newReceivePacketTimeLimit)
{
	ReceivePacketTimeLimit = newReceivePacketTimeLimit;
}

void ConnectionManager::SetRejectDuplicateConnections (bool bNewRejectDuplicateConnections)
{
	bRejectDuplicateConnections = bNewRejectDuplicateConnections;
}

void ConnectionManager::SetListenerPermissions (NetworkGlobals::EPermissions newListenerPermissions)
{
	ListenerPermissions = newListenerPermissions;
}

void ConnectionManager::SetRolePermissions (NetworkSignature::ENetworkRole netRole, NetworkGlobals::EPermissions newPermissions)
{
	for (size_t i = 0; i < RolePermissions.size(); ++i)
	{
		if (RolePermissions.at(i).first == netRole)
		{
			RolePermissions.at(i).second = newPermissions;
			return;
		}
	}

	RolePermissions.emplace_back(std::pair<NetworkSignature::ENetworkRole, NetworkGlobals::EPermissions>(netRole, newPermissions));
}

void ConnectionManager::SetDesiredPermissions (NetworkGlobals::EPermissions newDesiredPermissions)
{
	DesiredPermissions = newDesiredPermissions;
}

NetworkGlobals::EPermissions ConnectionManager::GetLocalPermissions (NetworkSignature* remoteProcess) const
{
	if (remoteProcess == nullptr)
	{
		return NetworkGlobals::P_None;
	}

	if (Connections.contains(remoteProcess->GetNetId()))
	{
		const SActiveConnection& connection = Connections.at(remoteProcess->GetNetId());
		return connection.LocalPermissions;
	}

	return NetworkGlobals::P_None; //Not connected. No permissions granted
}

NetworkGlobals::EPermissions ConnectionManager::GetRemotePermissions (NetworkSignature* remoteProcess) const
{
	if (remoteProcess == nullptr)
	{
		return NetworkGlobals::P_None;
	}

	if (Connections.contains(remoteProcess->GetNetId()))
	{
		const SActiveConnection& connection = Connections.at(remoteProcess->GetNetId());
		return connection.RemotePermissions;
	}

	return NetworkGlobals::P_None; //Not connected. No permissions granted
}

void ConnectionManager::GetListeningPorts (std::vector<unsigned short>& outListeningPorts) const
{
	for (size_t i = 0; i < Listeners.size(); ++i)
	{
		if (Listeners.at(i)->Socket != nullptr && Listeners.at(i)->Socket->Status == SSocketData::SS_Listening)
		{
			outListeningPorts.push_back(Listeners.at(i)->Listener.getLocalPort());
		}
	}
}

NetworkRoleComponent* ConnectionManager::FindRoleCompFromExternalId (NetworkSignature* remoteProcess, Int roleCompExternalId) const
{
	if (remoteProcess == nullptr)
	{
		return nullptr;
	}

	if (!Connections.contains(remoteProcess->GetNetId()))
	{
		//Not connected to the process
		return nullptr;
	}

	const SActiveConnection& connection = Connections.at(remoteProcess->GetNetId());
	for (const auto& roleCompMap : connection.RoleComponents)
	{
		if (roleCompMap.second->HasAuthority())
		{
			for (NetworkRoleComponent::SRemoteClientData remoteClient : roleCompMap.second->RemoteClients)
			{
				if (remoteClient.RemoteId == roleCompExternalId)
				{
					return roleCompMap.second;
				}
			}
		}
		else
		{
			if (roleCompMap.second->AuthoritativeClient.RemoteId == roleCompExternalId)
			{
				return roleCompMap.second;
			}
		}
	}

	return nullptr;
}

Float ConnectionManager::GetPing (NetworkSignature* remoteProcess) const
{
	if (remoteProcess != nullptr && Connections.contains(remoteProcess->GetNetId()))
	{
		const SHeartbeatData& heartbeat = Connections.at(remoteProcess->GetNetId()).Heartbeat;
		return heartbeat.AvgPing;
	}

	return -1.f;
}

void ConnectionManager::ProcessListeners ()
{
	for (std::shared_ptr<SListenerData>& listener : Listeners)
	{
		if (listener->Socket == nullptr)
		{
			//Search for an available socket
			for (size_t i = 0; i < SD_NETWORK_MAX_CONNECTIONS; ++i)
			{
				if (Sockets[i].Status == SSocketData::SS_Dormant)
				{
					listener->Socket = &Sockets[i];
					listener->Socket->Status = SSocketData::SS_Listening;
					break;
				}
			}
		}

		sf::TcpListener::Status status = listener->Listener.accept(listener->Socket->Socket);
		switch(status)
		{
			case(sf::Socket::Done):
			{
				sf::IpAddress remoteAddr = listener->Socket->Socket.getRemoteAddress();
				if (remoteAddr == sf::IpAddress::None)
				{
					//Client disconnected right when the connection was accepted?
					listener->Socket->Socket.disconnect();
					listener->Socket->Status = SSocketData::SS_Listening;
				}
				else if (!ProcessNewConnection(remoteAddr))
				{
					//Client is currently suspended. Reject connection.
					listener->Socket->Socket.disconnect();
					listener->Socket->Status = SSocketData::SS_Listening;
				}
				else
				{
					//Open up the new connection
					NetworkSignature* remoteSignature = NetworkSignature::CreateObject();
					remoteSignature->SetAddress(remoteAddr);
					auto insertResult = Connections.insert({remoteSignature->GetNetId(), SActiveConnection(remoteSignature, listener->Socket)});
					if (!insertResult.second)
					{
						NetworkLog.Log(LogCategory::LL_Critical, TXT("Failed to connect to %s. The Net ID %s is already taken."), DString(remoteAddr.toString()), remoteSignature->GetNetId());
						remoteSignature->Destroy();
						listener->Socket->Socket.disconnect();
						listener->Socket->Status = SSocketData::SS_Listening;
					}
					else
					{
						listener->Socket->Status = SSocketData::SS_Active;

						//Move this listener to another socket
						size_t socketIdx = UINT_INDEX_NONE;
						for (size_t i = 0; i < SD_NETWORK_MAX_CONNECTIONS; ++i)
						{
							if (Sockets[i].Status == SSocketData::SS_Dormant)
							{
								socketIdx = i;
								break;
							}
						}

						if (socketIdx == UINT_INDEX_NONE)
						{
							NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to find available socket. The TcpListener will idle until one becomes available."));
							listener->Socket = nullptr;
						}
						else
						{
							listener->Socket = &Sockets[socketIdx];
							listener->Socket->Status = SSocketData::SS_Listening;
						}
					}
				}

				break;
			}

			case(sf::Socket::NotReady):
			case(sf::Socket::Partial):
			case(sf::Socket::Disconnected):
				break; //try again later

			case(sf::Socket::Error):
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Unable to connect to TcpSocket. There was TcpListener error when listening to a socket at port %s."), Int(listener->Listener.getLocalPort()));
				break;
		}
	}
}

void ConnectionManager::ProcessSendPackets ()
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (localSignature == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Cannot send packets to remote connections since the local NetworkSignature is not yet setup. See: NetworkEngineComponent::InitializeLocalNetworkSignature."));
		return;
	}

	for (auto& connectPair : Connections)
	{
		SActiveConnection& connection = connectPair.second;
		sf::Socket::Status status = sf::Socket::Done;
		if (connection.OutboundPacket.getDataSize() > 0)
		{
			status = connection.Socket->Socket.send(connection.OutboundPacket);
			if (status == sf::Socket::Done)
			{
				connection.OutboundPacket.clear();
			}
		}

		switch(status)
		{
			case(sf::Socket::Done):
				if (!ContainerUtils::IsEmpty(connection.PendingData))
				{
					Int numPackets = Int(connection.PendingData.size());
					connection.OutboundPacket << numPackets;

					//Copy pending data to the packet
					for (size_t i = 0; i < connection.PendingData.size(); ++i)
					{
						if (NetMonitor != nullptr)
						{
							NetMonitor->ReadOutboundPacket(connection.PendingData.at(i).PacketType, connection.PendingData.at(i).Packet, connection.Destination);
						}

						connection.OutboundPacket << connection.PendingData.at(i).PacketType;
						switch (connection.PendingData.at(i).PacketType)
						{
							case(NMT_Req_OpenConnection):
								if (PacketOpenConnection* packetData = dynamic_cast<PacketOpenConnection*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_ConnectionAccepted):
								if (PacketAcceptConnection* packetData = dynamic_cast<PacketAcceptConnection*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_ConnectionRejected):
								if (PacketRejectConnection* packetData = dynamic_cast<PacketRejectConnection*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Req_InstantiateObj):
								if (PacketInstantiateObj* packetData = dynamic_cast<PacketInstantiateObj*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_AcceptInstantiation):
								if (PacketAcceptInstantiateObj* packetData = dynamic_cast<PacketAcceptInstantiateObj*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_RejectInstantiation):
								if (PacketRejectInstantiateObj* packetData = dynamic_cast<PacketRejectInstantiateObj*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Msg_ObjLoseRelevance):
								if (PacketDisconnectEntity* packetData = dynamic_cast<PacketDisconnectEntity*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Msg_NetCompData):
								if (PacketNetCompData* packetData = dynamic_cast<PacketNetCompData*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_RejNetCompData):
								if (PacketRejectNetCompData* packetData = dynamic_cast<PacketRejectNetCompData*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Msg_NetCompRpc):
								if (PacketNetCompRpc* packetData = dynamic_cast<PacketNetCompRpc*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_RejNetCompRpc):
								if (PacketRejectNetCompRpc* packetData = dynamic_cast<PacketRejectNetCompRpc*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Msg_RemoveNetComp):
								if (PacketDisconnectNetComp* packetData = dynamic_cast<PacketDisconnectNetComp*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Req_Heartbeat):
								if (PacketSendHeartbeat* packetData = dynamic_cast<PacketSendHeartbeat*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;

							case(NMT_Resp_Heartbeat):
								if (PacketReceiveHeartbeat* packetData = dynamic_cast<PacketReceiveHeartbeat*>(connection.PendingData.at(i).Packet))
								{
									connection.OutboundPacket << *packetData;
								}
								break;
						}

						delete connection.PendingData.at(i).Packet;
						connection.PendingData.at(i).Packet = nullptr;
					}

					ContainerUtils::Empty(OUT connection.PendingData);

					sf::Socket::Status sendStatus = connection.Socket->Socket.send(connection.OutboundPacket);
					if (connection.bTerminateOnSend && sendStatus != sf::Socket::Partial)
					{
						//Regardless if the socket errored, disconnected, or succeeded, terminate the connection. Only wait if it's partial.
						Disconnect(connection.Destination, true);
					}
					else if (sendStatus == sf::Socket::Done)
					{
						connection.OutboundPacket.clear();
					}
				}
				else if (connection.bTerminateOnSend)
				{
					Disconnect(connection.Destination, true);
				}
				else
				{
					//This connection doesn't have pending data to send. Check the network components to see if they have any information to send.
					GatherSendPackets(localSignature, OUT connection);
				}

				break;

			case(sf::Socket::NotReady):
				break; //Not yet connected, try again later. This is probably a socket that's waiting for the next client.

			case(sf::Socket::Partial):
				connection.Socket->Socket.send(connection.OutboundPacket); //Resend the packet. The OutboundPacket object internally saves the byte offset where it last left off.
				break;

			case(sf::Socket::Disconnected):
				//TODO: Need to test this on a remote machine. If a disconnected message is returned on a outage, then leave this alone. If heartbeats time it out, then this case should terminate the connection. The bug is that disconnecting during the connection allows NetworkSignatures to linger even though the connection severed.
				break; //Possible connection interruption. Try again later. Heartbeat events may time out and disconnect if it's an outage.

			case(sf::Socket::Error):
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to send packet across the network."));
				connection.OutboundPacket.clear(); //abort current data
				break;
		}
	}

	RemovePendingDeleteConnections();
}

void ConnectionManager::GatherSendPackets (NetworkSignature* localSignature, SActiveConnection& outConnection)
{
	if (outConnection.bTerminateOnSend || !outConnection.bApprovedConnection)
	{
		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr && localSignature != nullptr)

	for (auto& netComp : outConnection.NetComponents)
	{
		if (netComp.second == nullptr)
		{
			continue;
		}

		NetworkGlobals::EPermissions checkPerms = NetworkGlobals::P_SendData;
		if (netComp.second->GetNetRole() != NetworkRoleComponent::NR_Impotent)
		{
			checkPerms |= NetworkGlobals::P_SendDataNetOwner;
		}

		if ((outConnection.LocalPermissions & checkPerms) == 0)
		{
			//This process does not have permission to send data to this destination.
			continue;
		}

		Int externalCompId = netComp.second->GetExternalNetId(outConnection.Destination);
		if (externalCompId == 0)
		{
			//This net component is not yet connected to the given destination. Most likely it's waiting for the client to send a instantiate accept packet. Try again later.
			return;
		}

		DataBuffer replicatedVars;
		bool bHasData = false;

		for (BaseReplicatedVariable* repVar : netComp.second->ReadReplicatedVariables())
		{
			if (repVar->ShouldReplicate(localSignature, outConnection.Destination, netComp.second, localEngine->GetElapsedTime()))
			{
				replicatedVars << Bool(true);
				repVar->ReplicateVariable(outConnection.Destination, localEngine->GetElapsedTime(), OUT replicatedVars);
				bHasData = true;
			}
			else
			{
				replicatedVars << Bool(false);
			}
		}

		if (bHasData)
		{
			PacketNetCompData* compData = new PacketNetCompData(externalCompId, replicatedVars);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Msg_NetCompData, compData));
		}
	}
}

void ConnectionManager::ProcessReceivePackets ()
{
	for (auto& connection : Connections)
	{
		Stopwatch timer(DString::EmptyString, false);
		sf::Packet& inboundPacket = connection.second.InboundPacket;
		sf::Socket::Status status = sf::Socket::Done;

		if (connection.second.ReceivePacketNum <= 0) //Else it'll continue where it left off.
		{
			status = connection.second.Socket->Socket.receive(OUT inboundPacket);
		}

		switch (status)
		{
			case(sf::Socket::Done):
			{
				if (inboundPacket.getDataSize() == 0)
				{
					connection.second.ReceivePacketNum = -1;
					connection.second.TotalReceivePackets = -1;
					break; //Nothing to extract
				}

				if (connection.second.TotalReceivePackets <= 0)
				{
					if (!(inboundPacket >> connection.second.TotalReceivePackets))
					{
#ifdef DEBUG_MODE
						NetworkLog.Log(LogCategory::LL_Warning, TXT("Invalid packet received. All packet bundles must begin with an Int that indicates how many packets are received."));
#endif
						if (NetMonitor != nullptr)
						{
							NetMonitor->AddOffender(connection.second.Destination);
						}
						connection.second.ReceivePacketNum = -1;
						connection.second.TotalReceivePackets = -1;

						break;
					}

					connection.second.ReceivePacketNum = 0;
				}

				for ( ; connection.second.ReceivePacketNum < connection.second.TotalReceivePackets; ++connection.second.ReceivePacketNum)
				{
					if (timer.GetElapsedTime() >= ReceivePacketTimeLimit * 1000.f && ReceivePacketTimeLimit > 0.f)
					{
						//Add a log in case server administrators are investigating lag issues.
						NetworkLog.Log(LogCategory::LL_Log, TXT("%s sent too many packets in a short time span. Recieve packet time limit reached (%s seconds). Delaying processing until next frame."), connection.second.Destination->ToString(), ReceivePacketTimeLimit);
						break;
					}

					ENetMessageType messageType;
					if (!(inboundPacket >> messageType))
					{
#ifdef DEBUG_MODE
						NetworkLog.Log(LogCategory::LL_Warning, TXT("Invalid packet received. All packets must prefix themselves with a ENetMessageType."));
#endif
						if (NetMonitor != nullptr)
						{
							NetMonitor->AddOffender(connection.second.Destination);
						}

						break;
					}

					switch(messageType)
					{
						default:
						case(NMT_Unknown):
#ifdef DEBUG_MODE
							NetworkLog.Log(LogCategory::LL_Warning, TXT("Invalid packet message received. Received a message type with an invalid header (NMT_Unknown). Ignoring packet."));
#endif
							if (NetMonitor != nullptr)
							{
								NetMonitor->ReadInboundPacket(messageType, nullptr, connection.second.Destination);
							}

							break;

						case(NMT_Req_OpenConnection):
						{
							PacketOpenConnection openConnection;
							if (inboundPacket >> openConnection)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &openConnection, connection.second.Destination);
								}

								ProcessConnectionRequest(OUT connection.second, openConnection);
							}

							break;
						}

						case(NMT_Resp_ConnectionAccepted):
						{
							PacketAcceptConnection acceptConnection;
							if (inboundPacket >> acceptConnection)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &acceptConnection, connection.second.Destination);
								}

								ProcessConnectionAccepted(OUT connection.second, acceptConnection);
							}

							break;
						}

						case(NMT_Resp_ConnectionRejected):
						{
							PacketRejectConnection rejectConnection;
							if (inboundPacket >> rejectConnection)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &rejectConnection, connection.second.Destination);
								}

								ProcessConnectionRejected(OUT connection.second, rejectConnection);
							}

							break;
						}

						case(NMT_Req_InstantiateObj):
						{
							PacketInstantiateObj netCompReq;
							if (inboundPacket >> netCompReq)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &netCompReq, connection.second.Destination);
								}

								ProcessInstantiateNetObj(OUT connection.second, netCompReq);
							}

							break;
						}

						case(NMT_Resp_AcceptInstantiation):
						{
							PacketAcceptInstantiateObj acceptResponse;
							if (inboundPacket >> acceptResponse)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &acceptResponse, connection.second.Destination);
								}

								ProcessAcceptNetObj(OUT connection.second, acceptResponse);
							}

							break;
						}

						case(NMT_Resp_RejectInstantiation):
						{
							PacketRejectInstantiateObj rejectResponse;
							if (inboundPacket >> rejectResponse)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &rejectResponse, connection.second.Destination);
								}

								ProcessRejectNetObj(OUT connection.second, rejectResponse);
							}

							break;
						}

						case(NMT_Msg_ObjLoseRelevance):
						{
							PacketDisconnectEntity disconnectData;
							if (inboundPacket >> disconnectData)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &disconnectData, connection.second.Destination);
								}

								ProcessDisconnectEntity(OUT connection.second, disconnectData);
							}

							break;
						}

						case(NMT_Msg_NetCompData):
						{
							PacketNetCompData compData;
							if (inboundPacket >> compData)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &compData, connection.second.Destination);
								}

								ProcessNetCompData(OUT connection.second, compData);
							}

							break;
						}

						case(NMT_Resp_RejNetCompData):
						{
							PacketRejectNetCompData rejectData;
							if (inboundPacket >> rejectData)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &rejectData, connection.second.Destination);
								}

								ProcessRejectNetCompData(OUT connection.second, rejectData);
							}

							break;
						}

						case(NMT_Msg_NetCompRpc):
						{
							PacketNetCompRpc netCompRpc;
							if (inboundPacket >> netCompRpc)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &netCompRpc, connection.second.Destination);
								}

								ProcessNetCompRpc(OUT connection.second, netCompRpc);
							}

							break;
						}

						case(NMT_Resp_RejNetCompRpc):
						{
							PacketRejectNetCompRpc rejectData;
							if (inboundPacket >> rejectData)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &rejectData, connection.second.Destination);
								}

								ProcessRejectNetCompRpc(OUT connection.second, rejectData);
							}

							break;
						}

						case(NMT_Msg_RemoveNetComp):
						{
							PacketDisconnectNetComp disconnectNetComp;
							if (inboundPacket >> disconnectNetComp)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &disconnectNetComp, connection.second.Destination);
								}

								ProcessDisconnectNetComp(OUT connection.second, disconnectNetComp);
							}

							break;
						}

						case(NMT_Req_Heartbeat):
						{
							PacketSendHeartbeat heartbeat;
							if (inboundPacket >> heartbeat)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &heartbeat, connection.second.Destination);
								}

								ProcessSentHeartbeat(OUT connection.second, heartbeat);
							}

							break;
						}

						case(NMT_Resp_Heartbeat):
						{
							PacketReceiveHeartbeat heartbeat;
							if (inboundPacket >> heartbeat)
							{
								if (NetMonitor != nullptr)
								{
									NetMonitor->ReadInboundPacket(messageType, &heartbeat, connection.second.Destination);
								}

								ProcessReceivedHeartbeat(OUT connection.second, heartbeat);
							}

							break;
						}
					} //MessageType
				} //NumPackets

				if (connection.second.ReceivePacketNum >= connection.second.TotalReceivePackets)
				{
					connection.second.ReceivePacketNum = -1;
					connection.second.TotalReceivePackets = -1;
				}

				break; //break connection status switch statement
			} //receive status == sf::Socket::Done

			case(sf::Socket::NotReady):
				break; //Do nothing. Wait until next cycle to see if it becomes ready. This is probably a socket that's waiting for a client.

			case(sf::Socket::Partial):
			{
				Int packetSize = Int(inboundPacket.getDataSize());
				if (packetSize > OpenConnectionMaxSize)
				{
#ifdef DEBUG_MODE //Don't spam log in release builds if someone's flooding the network with open request.
					NetworkLog.Log(LogCategory::LL_Warning, TXT("OpenConnectionMaxSize limit reached (%s bytes). The ConnectionManager is dropping the connection to %s."), packetSize, DString(connection.second.Destination->GetAddress().toString()));
#endif
					connection.second.Socket->Socket.disconnect();
					connection.second.Socket->Status = SSocketData::SS_Dormant;
					connection.second.bPendingDelete = true;
				}

				break;
			}

			case(sf::Socket::Disconnected):
				OnConnectionRejected.Broadcast(connection.second.Destination->ReadAddress(), AccessControl::RR_DestNotFound);
				Disconnect(connection.second.Destination, true);

				break;

			case(sf::Socket::Error):
				NetworkLog.Log(LogCategory::LL_Warning, TXT("An error has occured when receiving packets from %s."), DString(connection.second.Destination->GetAddress().toString()));
				break;
		}
	}

	RemovePendingDeleteConnections();
}

void ConnectionManager::CheckConnectionRequests ()
{
	auto iter = Connections.begin();
	while (iter != Connections.end())
	{
		if (!iter->second.bApprovedConnection && iter->second.ConnectionRequestTime > 0.f)
		{
			Engine* localEngine = Engine::FindEngine(); //This is cached inside the loop instead of outside since it's seldom that a connection will be waiting for approval.
			CHECK(localEngine != nullptr)
			if ((localEngine->GetElapsedTime() - iter->second.ConnectionRequestTime) > TimeoutTime)
			{
				NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
				if (localNetEngine != nullptr && localNetEngine->IsIpLogginedPermitted())
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Dropping connection to %s since the connection request timed out."), DString(iter->second.Destination->GetAddress().toString()));
				}
				else
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Dropping a connection since their connection request timed out."));
				}

				iter->second.Socket->Socket.disconnect();
				iter->second.Socket->Status = SSocketData::SS_Dormant;
				OnConnectionRejected.Broadcast(iter->second.Destination->GetAddress(), AccessControl::RR_TimedOut);
				iter->second.Destination->Destroy();
				iter = Connections.erase(iter);
				continue;
			}
		}

		++iter;
	}
}

void ConnectionManager::CheckHeartbeats ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	Float curTime = localEngine->GetElapsedTime();

	for (auto& connection : Connections)
	{
		if (connection.second.bPendingDelete || connection.second.bTerminateOnSend)
		{
			continue;
		}
		
		if (connection.second.Heartbeat.bWaitingResponse && curTime - connection.second.Heartbeat.LatestReceivedTimestamp >= TimeoutTime && TimeoutTime > 0.f)
		{
			NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
			if (localNetEngine != nullptr && localNetEngine->IsIpLogginedPermitted())
			{
				NetworkLog.Log(LogCategory::LL_Log, TXT("No heartbeat response from %s within %s seconds. Dropping connection."), connection.second.Destination->ToString(), TimeoutTime);
			}
			else
			{
				NetworkLog.Log(LogCategory::LL_Log, TXT("No heartbeat response within %s seconds. Dropping that connection."), TimeoutTime);
			}

			Disconnect(connection.second.Destination, false);
			continue;
		}
		
		if (curTime - connection.second.Heartbeat.LatestSentTimestamp >= HeartbeatSendInterval)
		{
			//Send another heartbeat
			connection.second.Heartbeat.LatestHeartbeatId++;
			connection.second.Heartbeat.LatestSentTimestamp = curTime;
			connection.second.Heartbeat.bWaitingResponse = true;

			PacketSendHeartbeat* heartbeat = new PacketSendHeartbeat(connection.second.Heartbeat.LatestHeartbeatId);
			connection.second.PendingData.emplace_back(SPacketData(NMT_Req_Heartbeat, heartbeat));
		}
	}
}

bool ConnectionManager::ProcessNewConnection (const sf::IpAddress& newConnectionAddr)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	size_t i = 0;
	while (i < RecentAttempts.size())
	{
		if (localEngine->GetElapsedTime() - RecentAttempts.at(i).ConnectionTime >= ConnectionAttemptInterval)
		{
			RecentAttempts.erase(RecentAttempts.begin() + i);
			continue;
		}

		++i;
	}

	//Ensure this ip address is not suspended.
	for (const SConnectionAttempt& attempt : RecentAttempts)
	{
		if (attempt.Address == newConnectionAddr)
		{
			return false; //Still suspended
		}
	}

	if (ConnectionAttemptInterval > 0.f)
	{
		RecentAttempts.emplace_back(SConnectionAttempt(newConnectionAddr, localEngine->GetElapsedTime()));
	}

	return true;
}

void ConnectionManager::ProcessConnectionRequest (SActiveConnection& outConnection, const PacketOpenConnection& openRequest)
{
	NetworkSignature* destination = outConnection.Destination;

	if (IsRejectingDuplicateConnections())
	{
		//Ensure this process is not already connected
		sf::Uint32 destInt = destination->GetAddressInt();
		for (const auto& connection : Connections)
		{
			//If the ip addresses are the same and the parameter is not equal to the connection in the loop.
			if (connection.second.Destination->GetAddressInt() == destInt && connection.second.Destination->GetNetId() != destination->GetNetId())
			{
#ifdef DEBUG_MODE
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since there is already an active connection to that process."), DString(openRequest.RequesterIp.toString()));
#endif
				
				PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_AlreadyConnected);
				outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
				outConnection.bTerminateOnSend = true;
				return;
			}
		}
	}

	destination->SetNetworkRoles(openRequest.RequesterRole);

	while (!openRequest.IdentifierData.IsReaderAtEnd())
	{
		size_t identifierClassHash;
		if (!openRequest.IdentifierData.CanReadBytes(sizeof(identifierClassHash)))
		{
#ifdef DEBUG_MODE
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since there isn't sufficient bytes to read the identifier class name."), DString(openRequest.RequesterIp.toString()));
#endif

			PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_InvalidData);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
			outConnection.bTerminateOnSend = true;
			return;
		}

		if ((openRequest.IdentifierData >> identifierClassHash).HasReadError())
		{
#ifdef DEBUG_MODE
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since the data to read the identifier class hash is malformed."), DString(openRequest.RequesterIp.toString()));
#endif
			PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_InvalidData);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
			outConnection.bTerminateOnSend = true;
			return;
		}

		const DClass* identifierClass = DClassAssembler::FindDClass(identifierClassHash);
		if (identifierClass == nullptr || !identifierClass->IsChildOf(NetworkIdentifier::SStaticClass()) || identifierClass->IsAbstract() || identifierClass->GetDefaultObject() == nullptr)
		{
#ifdef DEBUG_MODE
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since the identifier class mapped to %s is not valid. An identifier class must be a subclass of NetworkIdentifier, and it cannot be abstract."), DString(openRequest.RequesterIp.toString()), Int(identifierClassHash));
#endif
			PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_InvalidData);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
			outConnection.bTerminateOnSend = true;
			return;
		}

		NetworkIdentifier* identifier = dynamic_cast<NetworkIdentifier*>(identifierClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(identifier != nullptr) //should never happen due to the check above
		if (destination->AddComponent(identifier))
		{
			bool bReadSuccess = identifier->ReadDataFromBuffer(openRequest.IdentifierData);
			if (!bReadSuccess)
			{
#ifdef DEBUG_MODE
				NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since the identifier %s is unable to assign its variables from the data buffer."), DString(openRequest.RequesterIp.toString()), identifier->ToString());
#endif
				PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_InvalidData);
				outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
				outConnection.bTerminateOnSend = true;
				return;
			}
		}
		else
		{
			NetworkLog.Log(LogCategory::LL_Critical, TXT("For some reason, the network signature refuses to attach a network identifier: %s"), identifier->ToString());
			//Fallthrough is intended. There's a good chance one of the access controls will reject the connection because of this.
		}
	}

	if (!openRequest.IdentifierData.IsReaderAtEnd())
	{
#ifdef DEBUG_MODE
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Rejecting open connection request from %s since there is too much data in the identifier data buffer."), DString(openRequest.RequesterIp.toString()));
#endif

		PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_InvalidData);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
		outConnection.bTerminateOnSend = true;
		return;
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)

	unsigned char rejectReason = AccessControl::RR_None;
	const std::vector<AccessControl*>& accessControls = localNetEngine->ReadAccessControls();
	for (size_t i = 0; i < accessControls.size(); ++i)
	{
		if (!accessControls.at(i)->IsRelevant(destination))
		{
			//Bypass check since destination is not relevant to it.
			continue;
		}

		if (!accessControls.at(i)->AllowConnection(destination, OUT rejectReason))
		{
			if (rejectReason == AccessControl::RR_Suspended)
			{
				if (localNetEngine->IsIpLogginedPermitted())
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Suspending %s since that client instigated too many connections in a short period of time."), DString(outConnection.Destination->ReadAddress().toString()));
				}
				else
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Suspending a connection since that client instigated too many connections in a short period of time."));
				}

				//The client is sending too many requests in a short period of time. Don't send a response back. Let the client timeout.
				outConnection.Socket->Socket.disconnect();
				outConnection.Socket->Status = SSocketData::SS_Dormant;
				outConnection.bPendingDelete = true;
			}
			else
			{
				if (localNetEngine->IsIpLogginedPermitted())
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Rejecting open connection from %s since AccessControl %s does not approve the request. Rejection reason number %s."), DString(outConnection.Destination->ReadAddress().toString()), accessControls.at(i)->ToString(), Int(rejectReason));
				}
				else
				{
					NetworkLog.Log(LogCategory::LL_Log, TXT("Rejecting an open connection since AccessControl %s does not approve the request. Rejection reason number %s."), accessControls.at(i)->ToString(), Int(rejectReason));
				}

				PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), Int(rejectReason));
				outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
				outConnection.bTerminateOnSend = true;
			}

			return;
		}
	}

	NetworkSignature* localSignature = localNetEngine->GetLocalSignature();
	if (localSignature == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Critical, TXT("The ConnectionManager has TcpListeners connecting to %s, but this process does not have a local signature! All connection requests are rejected until one is instantiated."), DString(openRequest.RequesterIp.toString()));

		PacketRejectConnection* rejectConnection = new PacketRejectConnection(NetworkEngineComponent::GetPublicAddress(), AccessControl::RR_OutOfResource);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionRejected, rejectConnection));
		outConnection.bTerminateOnSend = true;
		return;
	}

	DataBuffer localIdentifierData;
	for (ComponentIterator iter(localSignature, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (NetworkIdentifier* localIdentifier = dynamic_cast<NetworkIdentifier*>(iter.GetSelectedComponent()))
		{
			const DClass* unused = nullptr;
			size_t identifierHash = DClassAssembler::CalcDClassHash(localIdentifier->StaticClass()->ReadDuneClassName(), OUT unused);
			CHECK(identifierHash != 0)
			localIdentifierData << identifierHash;
			localIdentifier->WriteDataToBuffer(OUT localIdentifierData);
		}
	}

	if (localNetEngine->IsIpLogginedPermitted())
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("This process accepted the connection to %s!"), DString(openRequest.RequesterIp.toString()));
	}
	else
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("This process accepted a connection!"));
	}

	//Connection approved! Remove the remote process from the recent attempt list since it's a valid connection (allowing them to immediately send another connection request if they desire).
	for (size_t i = 0; i < RecentAttempts.size(); ++i)
	{
		if (RecentAttempts.at(i).Address == outConnection.Destination->GetAddress())
		{
			RecentAttempts.erase(RecentAttempts.begin() + i);
			break;
		}
	}

	NetworkGlobals::EPermissions requesterPermissions = NetworkGlobals::P_None;
	for (size_t i = 0; i < RolePermissions.size(); ++i)
	{
		if (destination->HasRole(RolePermissions.at(i).first))
		{
			requesterPermissions |= RolePermissions.at(i).second;
		}
	}

	PacketAcceptConnection* acceptConnection = new PacketAcceptConnection(NetworkEngineComponent::GetPublicAddress(), localSignature->GetNetworkRoles(), ListenerPermissions, requesterPermissions, localIdentifierData);
	outConnection.bApprovedConnection = true;
	outConnection.LocalPermissions = ListenerPermissions;
	outConnection.RemotePermissions = requesterPermissions;
	outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_ConnectionAccepted, acceptConnection));
	OnConnectionAccepted.Broadcast(destination);
}

void ConnectionManager::ProcessConnectionAccepted (SActiveConnection& outConnection, const PacketAcceptConnection& acceptConnection)
{
	NetworkSignature* remoteSignature = outConnection.Destination;
	remoteSignature->SetNetworkRoles(acceptConnection.RemoteNetworkRoles);

	NetworkGlobals::EPermissions allowedPermissions = NetworkGlobals::P_None; //Bit flags of permissions the remote process may potentially have.
	for (size_t i = 0; i < RolePermissions.size(); ++i)
	{
		if ((RolePermissions.at(i).first & acceptConnection.RemoteNetworkRoles) > 0)
		{
			allowedPermissions |= RolePermissions.at(i).second;
		}
	}

	/*
	Explanation:
	~allowedPermissions = Inverts the permitted flags. Anything that's a 1 are relevant bits. Every 1 represents a flag that the remote process cannot have.
	ListenerPermissions & ~allwedPermissions = If the ListenerPermissions has a match with any forbidden bit, then the remote process is overreaching with its permissions.
	Note: It's intended that underreaching remote processes are accepted.
	*/
	if ((acceptConnection.ListenerPermissions & (~allowedPermissions)) > 0)
	{
		//The remote process is assuming it has permissions that this process does not allow. Reject the connection.
		NetworkLog.Log(LogCategory::LL_Log, TXT("This process is rejecting the connection to %s since the remote process is overreaching with its permissions. This process does not allow a connection with roles %s with permissions %s."), DString(acceptConnection.AcceptedIp.toString()), Int(acceptConnection.RemoteNetworkRoles), Int(acceptConnection.ListenerPermissions));
		outConnection.bPendingDelete = true;
		return;
	}

	while (!acceptConnection.RemoteIdentifierData.IsReaderAtEnd())
	{
		size_t classHash;
		if (!acceptConnection.RemoteIdentifierData.CanReadBytes(sizeof(classHash)))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("This process is rejecting the connection to %s. The remote process returned malformed data in its identifer buffer."), DString(acceptConnection.AcceptedIp.toString()));
			outConnection.bPendingDelete = true;
			return;
		}

		if ((acceptConnection.RemoteIdentifierData >> classHash).HasReadError())
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("This process is rejecting the connection to %s. The remote process returned malformed bytes, preventing it from reading the class identifier hash."), DString(acceptConnection.AcceptedIp.toString()));
			outConnection.bPendingDelete = true;
			return;
		}

		const DClass* foundClass = DClass::FindDClass(classHash);
		if (foundClass == nullptr)
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("This process is rejecting the connection to %s. The class hash in the identifier buffer is not mapped to a DClass."), DString(acceptConnection.AcceptedIp.toString()));
			outConnection.bPendingDelete = true;
			return;
		}

		if (!foundClass->IsA(NetworkIdentifier::SStaticClass()) || foundClass->IsAbstract())
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("This process is rejecting the connection to %s. %s is not a valid NetworkIdentifier."), DString(acceptConnection.AcceptedIp.toString()), foundClass->ReadDuneClassName());
			outConnection.bPendingDelete = true;
			return;
		}

		NetworkIdentifier* identifier = dynamic_cast<NetworkIdentifier*>(foundClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(identifier != nullptr)
		if (remoteSignature->AddComponent(identifier))
		{
			if (!identifier->ReadDataFromBuffer(acceptConnection.RemoteIdentifierData))
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("This process is rejecting the connection to %s. Malformed data detected when reading data from buffer for network signature identifier type %s."), DString(acceptConnection.AcceptedIp.toString()), foundClass->ReadDuneClassName());
				outConnection.bPendingDelete = true;
				return;
			}
		}
		else
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to attach %s to a remote signature. Cannot verify the integrity of %s signature. Dropping connection."), DString(acceptConnection.AcceptedIp.toString()));
			outConnection.bPendingDelete = true;
			return;
		}
	}

	NetworkEngineComponent* localNetworkEngine = NetworkEngineComponent::Find();
	CHECK(localNetworkEngine != nullptr)

	const std::vector<AccessControl*>& accessControls = localNetworkEngine->ReadAccessControls();
	for (AccessControl* accessControl : accessControls)
	{
		if (!accessControl->IsRelevant(remoteSignature))
		{
			continue; //Bypass AccessControl with this signature
		}

		unsigned char rejectReason;
		if (!accessControl->AllowConnection(remoteSignature, OUT rejectReason))
		{
			NetworkLog.Log(LogCategory::LL_Log, TXT("This process is rejecting the connection to %s since the %s did not accept the remote process's network signature. Rejection reason num: %s"), DString(acceptConnection.AcceptedIp.toString()), accessControl->ToString(), Int(rejectReason));
			outConnection.bPendingDelete = true;
			return;
		}
	}

	if (localNetworkEngine->IsIpLogginedPermitted())
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("This process is now connected to %s!"), DString(acceptConnection.AcceptedIp.toString()));
	}
	else
	{
		NetworkLog.Log(LogCategory::LL_Log, TXT("This process is now connected!"));
	}

	//Accept the connection
	outConnection.LocalPermissions = acceptConnection.RequesterPermissions;
	outConnection.RemotePermissions = acceptConnection.ListenerPermissions;
	outConnection.bApprovedConnection = true;
	OnConnectionAccepted.Broadcast(remoteSignature);
}

void ConnectionManager::ProcessConnectionRejected (SActiveConnection& outConnection, const PacketRejectConnection& rejectConnection)
{
	NetworkLog.Log(LogCategory::LL_Log, TXT("This process' request to connect to %s is rejected. Reason switch: %s."), DString(rejectConnection.RejectedIp.toString()), rejectConnection.ReasonSwitch);

	outConnection.bPendingDelete = true;
	OnConnectionRejected.Broadcast(rejectConnection.RejectedIp, rejectConnection.ReasonSwitch);
}

void ConnectionManager::ProcessInstantiateNetObj (SActiveConnection& outConnection, const PacketInstantiateObj& netCompReq)
{
	//Ignore instantiation requests for connections that are not yet approved
	if (outConnection.bTerminateOnSend || !outConnection.bApprovedConnection)
	{
		return;
	}

	if ((outConnection.RemotePermissions & NetworkGlobals::P_InstantiateComponent) == 0)
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Rejecting %s 's request to instantate an Entity since that address does not have permission."), DString(outConnection.Destination->ReadAddress().toString()));
		PacketRejectInstantiateObj* rejectResp = new PacketRejectInstantiateObj(netCompReq.NetId);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejectInstantiation, rejectResp));
		return;
	}

	const DClass* targetClass = DClass::FindDClass(netCompReq.OwnerClassHash);

	//A class must be found. The class must also be an Entity. It cannot be an Entity Component. It cannot be abstract.
	if (targetClass == nullptr || !targetClass->IsA(Entity::SStaticClass()) || targetClass->IsA(EntityComponent::SStaticClass()) || targetClass->IsAbstract())
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Rejecting %s 's request to instantiate an Entity since the given class hash is not a valid hash to an Entity class."), DString(outConnection.Destination->ReadAddress().toString()));
		PacketRejectInstantiateObj* rejectResp = new PacketRejectInstantiateObj(netCompReq.NetId);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejectInstantiation, rejectResp));
		return;
	}

	Entity* newNetEntity = dynamic_cast<Entity*>(targetClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	CHECK(newNetEntity != nullptr)

	NetworkRoleComponent* roleComp = dynamic_cast<NetworkRoleComponent*>(newNetEntity->FindComponent(NetworkRoleComponent::SStaticClass(), false));
	if (roleComp == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Rejecting %s 's request to instantiate an Entity since the given class does not instantiate its own NetworkRoleComponent. It cannot communicate without one."), DString(outConnection.Destination->ReadAddress().toString()));
		PacketRejectInstantiateObj* rejectResp = new PacketRejectInstantiateObj(netCompReq.NetId);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejectInstantiation, rejectResp));
		newNetEntity->Destroy();
		return;
	}

	DataBuffer localCompIds;
	std::vector<NetworkComponent*> netComps;
	roleComp->GetNetComponentList(OUT netComps);
	for (NetworkComponent* netComp : netComps)
	{	
		Int externalId;
		if (!netCompReq.NetCompNetIds.CanReadBytes(externalId.GetMinBytes()))
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Rejecting %s ' request to instantiate an Entity since there aren't enough bytes in the NetComponent IDs data buffer. The IDs must correspond to the number of IDs on the remote process. Number of expected components: %s"), DString(outConnection.Destination->ReadAddress().toString()), Int(netComps.size()));
			PacketRejectInstantiateObj* rejectResp = new PacketRejectInstantiateObj(netCompReq.NetId);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejectInstantiation, rejectResp));
			newNetEntity->Destroy();
			return;
		}

		if ((netCompReq.NetCompNetIds >> externalId).HasReadError())
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Rejecting %s ' request to instantiate an Entity since the data for the NetComponent's ID is malformed. The IDs must correspond to the number off IDs on the remote process. Number of expected components: %s"), DString(outConnection.Destination->ReadAddress().toString()), Int(netComps.size()));
			PacketRejectInstantiateObj* rejectResp = new PacketRejectInstantiateObj(netCompReq.NetId);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejectInstantiation, rejectResp));
			newNetEntity->Destroy();
			return;
		}

		if (netComp != nullptr && externalId > 0)
		{
			localCompIds << netComp->GetNetId();
			netComp->AddExternalNetComp(outConnection.Destination, externalId, NetworkRoleComponent::NR_Authority);
			netComp->ProcessNonAuthoritativeRole();
		}
		else
		{
			localCompIds << Int(0); //notify remote process that this net component is invalid.
		}
	}

	roleComp->SetAuthoritativeClient(outConnection.Destination, netCompReq.NetId);
	if (netCompReq.bIsBothNetOwner)
	{
		roleComp->SetNetRole(NetworkRoleComponent::NR_NetOwner);
	}
	else
	{
		roleComp->SetNetRole(NetworkRoleComponent::NR_Impotent);
	}

	outConnection.RoleComponents.insert({roleComp->GetNetId(), roleComp});
	for (NetworkComponent* netComp : netComps)
	{
		outConnection.NetComponents.insert({netComp->GetNetId(), netComp});
	}
	
	InstantiatedEntities.push_back(newNetEntity);
	PacketAcceptInstantiateObj* acceptResp = new PacketAcceptInstantiateObj(netCompReq.NetId, roleComp->GetNetId(), netCompReq.bIsBothNetOwner, localCompIds);
	outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_AcceptInstantiation, acceptResp));

	roleComp->NetInitialize(outConnection.Destination);
}

void ConnectionManager::ProcessAcceptNetObj (SActiveConnection& outConnection, const PacketAcceptInstantiateObj& acceptedObj)
{
	try
	{
		NetworkRoleComponent* comp = outConnection.RoleComponents.at(acceptedObj.InstigatorNetId);
		if (comp != nullptr)
		{
			comp->ProcessInstantiationAccepted(outConnection.Destination, acceptedObj.NetId, acceptedObj.AcceptedNetCompIds);
			comp->NetInitialize(outConnection.Destination);
		}
	}
	catch (std::out_of_range&)
	{
		//Noop - NetworkRoleComponent must have been dropped or destroyed while waiting for a response.
	}
}

void ConnectionManager::ProcessRejectNetObj (SActiveConnection& outConnection, const PacketRejectInstantiateObj& rejectedObj)
{
	try
	{
		NetworkRoleComponent* comp = outConnection.RoleComponents.at(rejectedObj.InstigatorNetId);
		outConnection.RoleComponents.erase(rejectedObj.InstigatorNetId);

		if (comp != nullptr)
		{
			comp->ProcessInstantiationRejected(outConnection.Destination);
		}
	}
	catch (std::out_of_range&)
	{
		//Noop
	}
}

void ConnectionManager::ProcessDisconnectEntity (SActiveConnection& outConnection, const PacketDisconnectEntity& disconnectData)
{
	try
	{
		NetworkRoleComponent* roleComp = outConnection.RoleComponents.at(disconnectData.ReceiverNetId);
		if (roleComp != nullptr)
		{
			if (roleComp->GetNetRole() == NetworkRoleComponent::NR_Authority)
			{
				//Send 0 for ExternalNetPacket to stop this process from sending another packet back. The remote process already disconnected its entity.
				roleComp->DropClient(outConnection.Destination, 0);
			}
			else
			{
				roleComp->LoseRelevance();
			}
			
			//NOTE: LoseRelevance typically destroys this entity. No longer safe to reference this variable.
		}
	}
	catch (std::out_of_range&)
	{
		//Already disconnected do nothing.
	}
}

void ConnectionManager::ProcessNetCompData (SActiveConnection& outConnection, const PacketNetCompData& compData)
{
	if (outConnection.bTerminateOnSend || !outConnection.bApprovedConnection)
	{
		return;
	}

	if (!outConnection.NetComponents.contains(compData.CompNetId))
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Unable to process network component data since there isn't a component associated with %s."), compData.CompNetId);
		PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(0, UINT_INDEX_NONE, PacketRejectNetCompData::RR_MissingNetComponent);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
		return;
	}

	NetworkComponent* netComp = outConnection.NetComponents.at(compData.CompNetId);
	if (netComp == nullptr || compData.CompData.GetNumBytes() == 0)
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Unable to process network component data since there isn't a component associated with %s."), compData.CompNetId);
		PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(0, UINT_INDEX_NONE, PacketRejectNetCompData::RR_MissingNetComponent);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
		return;
	}

	//Check network permissions
	{
		if (!netComp->ReadExternalNetComps().contains(outConnection.Destination->GetNetId()))
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is refusing the accept data for network component ID %s since that network component is not connected to a Network Signature %s."), compData.CompNetId, outConnection.Destination->ToString());
			PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(0, UINT_INDEX_NONE, PacketRejectNetCompData::RR_MissingNetComponent);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
			return;
		}

		NetworkGlobals::EPermissions checkPerms = NetworkGlobals::P_SendData;
		if (netComp->ReadExternalNetComps().at(outConnection.Destination->GetNetId()).NetRole != NetworkRoleComponent::NR_Impotent)
		{
			checkPerms |= NetworkGlobals::P_SendDataNetOwner; //If the client is a network owner, allow additional permissions.
		}

		if ((outConnection.RemotePermissions & checkPerms) == 0)
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is refusing to accept data for network component ID %s since the remote process does not have permissions to send data."), compData.CompNetId);
			PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), UINT_INDEX_NONE, PacketRejectNetCompData::RR_NotAuthorized);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
			return;
		}
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	size_t varIdx = 0;
	for (BaseReplicatedVariable* repVar : netComp->ReadReplicatedVariables())
	{
		Bool bHasData = false;
		if (!compData.CompData.CanReadBytes(Bool::SGetMinBytes()))
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting network component (%s) data since there are missing bytes from data buffer. Network Components are suppose to begin with Bool true/false to indicate which variables actually have data or not."), netComp->GetNetId());
			PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), varIdx, PacketRejectNetCompData::RR_InvalidBuffer);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
			return;
		}

		if ((compData.CompData >> bHasData).HasReadError())
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting network component (%s) data since the data is malformed, preventing the data buffer from reading bHasData Bool. This Entity is unable to determine if the network component has data to begin with or not."), netComp->GetNetId());
			PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), varIdx, PacketRejectNetCompData::RR_InvalidBuffer);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
			return;
		}

		if (bHasData)
		{
			BaseReplicatedVariable::EReplicationResults results = repVar->ReceiveVariableUpdate(outConnection.Destination, localEngine->GetElapsedTime(), compData.CompData);
			switch (results)
			{
				default:
				case(BaseReplicatedVariable::RR_Success):
					break; //OK! Break from switch statement

				case(BaseReplicatedVariable::RR_NoWritePermissions):
				{
					NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting network component (%s) data since %s does not have write permissions to send variable updates (for variable idx %s)."), netComp->GetNetId(), outConnection.Destination->ToString(), Int(varIdx));
					PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), varIdx, PacketRejectNetCompData::RR_NoWritePermissions);
					outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
					return;
				}

				case(BaseReplicatedVariable::RR_InvalidBuffer):
				{
					NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting network component (%s) data since the buffer has insufficient data to read from variable idx %s."), netComp->GetNetId(), Int(varIdx));
					PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), varIdx, PacketRejectNetCompData::RR_InvalidBuffer);
					outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
					return;
				}

				case(BaseReplicatedVariable::RR_InvalidData):
				{
					NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting network component (%s) data for var idx %s due to the incorrect value passed in."), compData.CompNetId, Int(varIdx));
					PacketRejectNetCompData* rejectData = new PacketRejectNetCompData(netComp->GetExternalNetId(outConnection.Destination), varIdx, PacketRejectNetCompData::RR_InvalidValue);
					outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompData, rejectData));
					break; //break from switch statement. Other variables may be ok.
				}
			}
		}

		++varIdx;
	}	
}

void ConnectionManager::ProcessRejectNetCompData (SActiveConnection& outConnection, const PacketRejectNetCompData& rejectData)
{
	NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to replicate variables to %s. NetworkComponent ID = %s. Variable Idx = %s. Reason = %s."), outConnection.Destination->ToString(), rejectData.CompNetId, Int(rejectData.RepVarIdx), Int(rejectData.Reason));

	switch (rejectData.Reason)
	{
		default:
		case(PacketRejectNetCompData::RR_Unknown):
			break;

		case(PacketRejectNetCompData::RR_NotAuthorized):
		{
			//Update local permissions to prevent more unauthorized rejections
			//First try removing NetOwner flag. If it fails again, it'll remove SendData entirely.
			if ((outConnection.LocalPermissions & NetworkGlobals::P_SendDataNetOwner) > 0)
			{
				outConnection.LocalPermissions &= ~NetworkGlobals::P_SendDataNetOwner;
			}
			else
			{
				outConnection.LocalPermissions &= ~NetworkGlobals::P_SendDataNetOwner;
			}

			break;
		}

		case(PacketRejectNetCompData::RR_NoWritePermissions):
		{
			NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
			CHECK(localSignature != nullptr)

			//Try to update this replicated variable's write access to prevent further errors.
			if (outConnection.NetComponents.contains(rejectData.CompNetId))
			{
				NetworkComponent* netComp = outConnection.NetComponents.at(rejectData.CompNetId);

				//Only change permissions for NetComponents if they are not authoritative (to prevent impotent or NetOwner components from influencing write permissions to the original NetComponent).
				if (netComp != nullptr && netComp->GetNetRole() != NetworkRoleComponent::NR_Authority && ContainerUtils::IsValidIndex(netComp->ReadReplicatedVariables(), rejectData.RepVarIdx))
				{
					//Remove all flags (found in local signature's roles) from the replicated variable's WritePermissions.
					netComp->ReadReplicatedVariables().at(rejectData.RepVarIdx)->WritePermissions &= ~localSignature->GetNetworkRoles();
				}
			}

			break;
		}

		case(PacketRejectNetCompData::RR_InvalidBuffer):
		case(PacketRejectNetCompData::RR_InvalidValue):
		case(PacketRejectNetCompData::RR_Other):
			break;
	}
}

void ConnectionManager::ProcessNetCompRpc (SActiveConnection& outConnection, const PacketNetCompRpc& netCompRpc)
{
	if (outConnection.bTerminateOnSend || !outConnection.bApprovedConnection)
	{
		return;
	}

	if (!outConnection.NetComponents.contains(netCompRpc.NetId))
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting RPC from %s since that process is not connected to a network component with ID %s."), outConnection.Destination->ToString(), netCompRpc.NetId);
		PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(-1, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_MissingNetComp);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
		return;
	}

	NetworkComponent* localNetComp = outConnection.NetComponents.at(netCompRpc.NetId);
	CHECK(localNetComp != nullptr)

	Int instigatorId = localNetComp->GetExternalNetId(outConnection.Destination);

	//Check network permissions
	{
		if (!localNetComp->ReadExternalNetComps().contains(outConnection.Destination->GetNetId()))
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting RPC from %s since the Network Component with ID %s is not associated with that connection."), outConnection.Destination->ToString(), localNetComp->GetNetId());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_MissingNetComp);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}

		NetworkGlobals::EPermissions checkPerms = NetworkGlobals::P_Rpc;
		if (localNetComp->ReadExternalNetComps().at(outConnection.Destination->GetNetId()).NetRole != NetworkRoleComponent::NR_Impotent)
		{
			checkPerms |= NetworkGlobals::P_RpcNetOwner; //If the client is a network owner, allow additional permissions.
		}

		if ((outConnection.RemotePermissions & checkPerms) == 0)
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting RPC from %s since that process does not have permission for remote execution."), outConnection.Destination->ToString());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_NotAuthorized);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}
	}

	if (!localNetComp->ReadRpcs().contains(netCompRpc.FunctionId))
	{
		NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting RPC from %s since the NetworkComponent %s does not have a RPC function with hash %s."), outConnection.Destination->ToString(), localNetComp->ToString(), Int(netCompRpc.FunctionId));
		PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_MissingFunction);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
		return;
	}

	BaseRpc* rpc = localNetComp->ReadRpcs().at(netCompRpc.FunctionId);
	CHECK(rpc != nullptr)

	if (rpc->OnExecuteBuffer == nullptr)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Process is rejecting RPC from %s since the RPC object's OnExecuteBuffer is not bound to anything. Cannot convert DataBuffer to an execution call."), outConnection.Destination->ToString());
		PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_NotBound);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
		return;
	}

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	BaseRpc::ERpcAvailability availability = rpc->IsAvailableFor(outConnection.Destination, localSignature);
	if (availability == BaseRpc::RA_Unavailable)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Process is rejecting RPC from %s since the RPC %s is not available to be remotely executed in this process."), outConnection.Destination->ToString(), rpc->ToString());
		PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_NotAvailable);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
		return;
	}
	else if (availability == BaseRpc::RA_NotPermitted)
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Process is rejecting RPC from %s since that process does not have permission to execute %s."), outConnection.Destination->ToString(), rpc->ToString());
		PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_NotAuthorized);
		outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
		return;
	}

	BaseRpc::ERpcResult results = rpc->ExecuteBuffer(outConnection.Destination, netCompRpc.Parameters);
	switch (results)
	{
		default:
		case (BaseRpc::RR_Success):
			//OK! Do nothing more
			return;

		case (BaseRpc::RR_InvalidBuffer):
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting a RPC from %s since the given data buffer contains insufficient data for its parameters."), outConnection.Destination->ToString());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_InvalidBuffer);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}

		case (BaseRpc::RR_InvalidCall):
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting a RPC from %s since the component owner determined that the event or its parameters are illegal."), outConnection.Destination->ToString());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_InvalidCall);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}

		case (BaseRpc::RR_NotPermitted):
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting a RPC from %s since that process is attempting to access an object it doesn't have permission for write access."), outConnection.Destination->ToString());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_AccessingInvalidObj);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}

		case (BaseRpc::RR_Internal):
		{
			NetworkLog.Log(LogCategory::LL_Verbose, TXT("Process is rejecting a RPC from %s due to an internal error that's most likely because there's a bug in this process."), outConnection.Destination->ToString());
			PacketRejectNetCompRpc* rejectData = new PacketRejectNetCompRpc(instigatorId, netCompRpc.FunctionId, PacketRejectNetCompRpc::RR_Internal);
			outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_RejNetCompRpc, rejectData));
			return;
		}
	}
}

void ConnectionManager::ProcessRejectNetCompRpc (SActiveConnection& outConnection, const PacketRejectNetCompRpc& rejectedRpc)
{
	NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to invoke RPC on %s. NetworkComponent ID = %s. Function Idx = %s. Reason = %s."), outConnection.Destination->ToString(), rejectedRpc.NetId, Int(rejectedRpc.FunctionId), Int(rejectedRpc.Reason));
	if (rejectedRpc.NetId <= 0)
	{
		return;
	}

	if (!outConnection.NetComponents.contains(rejectedRpc.NetId))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to obtain information about the rejection packet since the network component ID %s is not found in this process."), rejectedRpc.NetId);
		return;
	}

	NetworkComponent* netComp = outConnection.NetComponents.at(rejectedRpc.NetId);
	CHECK(netComp != nullptr)

	//Get information about the RPC that failed
	{
		if (!netComp->ReadRpcs().contains(rejectedRpc.FunctionId))
		{
			NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to obtain information about the rejection packet since the function id %s is not registered to the network component %s."), Int(rejectedRpc.FunctionId), netComp->ToString());
		}
		else
		{
			BaseRpc* rpc = netComp->ReadRpcs().at(rejectedRpc.FunctionId);
			if (rpc != nullptr)
			{
				NetworkLog.Log(LogCategory::LL_Warning, TXT("    The associated NetworkComponent is %s. The RPC function that failed is %s."), netComp->ToString(), rpc->ToString());
			}
		}
	}
}

void ConnectionManager::ProcessDisconnectNetComp (SActiveConnection& outConnection, const PacketDisconnectNetComp& disconnectNetComp)
{
	if (!outConnection.NetComponents.contains(disconnectNetComp.NetId))
	{
		//Noop - already disconnected
		return;
	}

	NetworkComponent* netComp = outConnection.NetComponents.at(disconnectNetComp.NetId);
	netComp->RemoveExternalNetComp(outConnection.Destination); //Remove the connection without sending a packet back
	outConnection.NetComponents.erase(disconnectNetComp.NetId);
}

void ConnectionManager::ProcessSentHeartbeat (SActiveConnection& outConnection, const PacketSendHeartbeat& heartbeat)
{
	//Simply send a response
	PacketReceiveHeartbeat* response = new PacketReceiveHeartbeat(heartbeat.HeartbeatId);
	outConnection.PendingData.emplace_back(SPacketData(NMT_Resp_Heartbeat, response));
}

void ConnectionManager::ProcessReceivedHeartbeat (SActiveConnection& outConnection, const PacketReceiveHeartbeat& heartbeat)
{
	if (outConnection.Heartbeat.LatestHeartbeatId != heartbeat.HeartbeatId)
	{
		//Must be an old packet. Not the one we're expecting.
		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	outConnection.Heartbeat.bWaitingResponse = false; //stop timeout in case the developer specified a really high heartbeat interval time.
	outConnection.Heartbeat.LatestReceivedTimestamp = localEngine->GetElapsedTime();
	outConnection.Heartbeat.LatestPings[outConnection.Heartbeat.LatestPingsIdx] = (localEngine->GetElapsedTime() - outConnection.Heartbeat.LatestSentTimestamp);
	outConnection.Heartbeat.LatestPingsIdx++;
	outConnection.Heartbeat.LatestPingsIdx %= SD_NETWORK_LATEST_PINGS_SIZE;

	//Update average ping
	Float numPings = 0.f;
	Float totalTime = 0.f;
	for (unsigned int i = 0; i < SD_NETWORK_LATEST_PINGS_SIZE; ++i)
	{
		if (outConnection.Heartbeat.LatestPings[i] > 0.f)
		{
			totalTime += outConnection.Heartbeat.LatestPings[i];
			numPings += 1.f;
		}
	}

	if (numPings > 0.f)
	{
		outConnection.Heartbeat.AvgPing = totalTime / numPings;
	}
}

void ConnectionManager::RemovePendingDeleteConnections ()
{
	auto iter = Connections.begin();
	while (iter != Connections.end())
	{
		if (iter->second.bPendingDelete && iter->second.ReceivePacketNum <= 0)
		{
			if (iter->second.bApprovedConnection)
			{
				OnDisconnect.Broadcast(iter->second.Destination);
			}

			iter->second.Destination->Destroy();
			iter = Connections.erase(iter);
			continue;
		}

		++iter;
	}
}

void ConnectionManager::HandleHeartbeatCheck (Float deltaSec)
{
	CheckConnectionRequests();
	CheckHeartbeats();
}

void ConnectionManager::HandleNetProcessTick (Float deltaSec)
{
	ProcessListeners();
	ProcessSendPackets();
	ProcessReceivePackets();
}

void ConnectionManager::HandlePreGarbageCollection ()
{
	//Clear any null elements from the InstantiatedEntities
	auto iter = InstantiatedEntities.begin();
	while (iter != InstantiatedEntities.end())
	{
		if ((*iter).IsNullptr())
		{
			iter = InstantiatedEntities.erase(iter);
			continue;
		}

		++iter;
	}
}

sf::Packet& operator<< (sf::Packet& packet, ConnectionManager::ENetMessageType msgType)
{
	unsigned char msgNum = static_cast<unsigned char>(msgType);
	return (packet << msgNum);
}

sf::Packet& operator>> (sf::Packet& packet, ConnectionManager::ENetMessageType& outMsgType)
{
	unsigned char msgNum;
	if (!(packet >> msgNum))
	{
		NetworkLog.Log(LogCategory::LL_Warning, TXT("Failed to extract ConnectionManager::ENetMessageType from packet."));
		outMsgType = ConnectionManager::NMT_Unknown;
	}
	else
	{
		outMsgType = static_cast<ConnectionManager::ENetMessageType>(msgNum);
	}

	return packet;
}
SD_END