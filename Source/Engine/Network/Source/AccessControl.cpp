/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AccessControl.cpp
=====================================================================
*/

#include "AccessControl.h"
#include "NetworkSignature.h"

IMPLEMENT_ABSTRACT_CLASS(SD::AccessControl, SD::Object)
SD_BEGIN

void AccessControl::InitProps ()
{
	Super::InitProps();

	//By default, all roles are relevant
	RelevantRoles = SD_MAXINT;
}

void AccessControl::BeginObject ()
{
	Super::BeginObject();

	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Network.ini")), false))
	{
		LoadConfigSettings(config);
	}
	config->Destroy();
}

bool AccessControl::IsRelevant (NetworkSignature* signature) const
{
	return ((signature->GetNetworkRoles() & RelevantRoles) > 0);
}

void AccessControl::ConnectionRejected (NetworkSignature* signature, unsigned char rejectReason)
{
	//Noop
}

void AccessControl::LoadConfigSettings (ConfigWriter* config)
{
	//Noop
}
SD_END