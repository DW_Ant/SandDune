All directories and files including this ReadMe.txt within the UnitTest directory is read from the File module's unit test.

Be sure to update the File module's unit test when making changes to this directory.

Temporary files may be generated from the unit test, and may cause the unit test to fail if they are found.  It's safe to delete the temporary files.

Adding files should not cause the unit tester to fail.

Use the ConfigTestBackup.ini if you accidentally changed the ConfigTest.ini in a format that causes the Config unit tester to fail.  Alternatively, you can delete ConfigTest.ini.  Next time the unit test runs, it should use ConfigTestBackup.ini to create ConfigTest.ini.