/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacFile.h
  Utilities containing Mac-specific file system utilities.
=====================================================================
*/

#pragma once

#include "FileAttributes.h"

#ifdef PLATFORM_MAC

#define OUTPUT_LOCATION "Mac"

#endif //PLATFORM_MAC