/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFileWriter.h
  The TextFileWriter class is responsible for writing text/strings
  to a file.
=====================================================================
*/

#pragma once

#include "File.h"

SD_BEGIN
class FILE_API TextFileWriter : public Object
{
	DECLARE_CLASS(TextFileWriter)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Buffer that is pending to be written to the file. */
	std::vector<DString> TextBuffer;

	/* Maximmum size of TextBuffer before the text is automatically flushed to the text file. */
	UINT_TYPE BufferLimit;

	std::fstream File;

	/* Name of the file that's currently opened. */
	DString CurrentFile;

	/* If true, then data cannot be written to this file during run time. */
	bool bReadOnly;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Reassigns the File variable to write to.  It will close the previously opened file if already opened.
	 * If the file already exists, the file will be replaced with this new file if bReplaceFile is true
	 * fileName parameter should include the path.
	 */
	virtual bool OpenFile (const DString& fileName, bool bReplaceFile);
	virtual bool OpenFile (const FileAttributes& fileAttributes, bool bReplaceFile);

	/**
	 * Adds a new entry to the TextBuffer
	 */
	virtual void AddTextEntry (const DString& newEntry);

	/**
	 * Writes the entire TextBuffer at the end of the file.  Returns true if successful and clears the TextBuffer.
	 * If it fails, the TextBuffer will clear only if bClearBufferIfFailed is true.
	 */
	virtual bool WriteToFile (bool bClearBufferIfFailed = false);

	/**
	 * Clears all text from the file.
	 */
	virtual bool EmptyFile ();

	/**
	 * Reads the next line from File stream.  Returns true if it found text.  To read all
	 * lines, reset the seek position, and continuously call this function until it returns false.
	 */
	virtual bool ReadLine (DString& outCurrentLine);

	virtual bool IsFileOpened () const;

	virtual void SetBufferLimit (UINT_TYPE newBufferLimit);

	virtual void SetReadOnly (bool bNewReadOnly);


	/*
	=====================
	  Accessors
	=====================
	*/

	virtual UINT_TYPE GetBufferLimit () const;
	virtual DString GetCurrentFileName () const;
	virtual bool GetReadOnly () const;


	/*
	=====================
	  Implementation
	=====================
	*/

	/**
	 * Resets seeking position for ifile stream.
	 */
	virtual void ResetSeekPosition ();
};
SD_END