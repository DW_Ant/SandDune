/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ProcessInfo.h
  An object that represents a child process that was launched from this engine.
=====================================================================
*/

#pragma once

#include "File.h"
#include "FileAttributes.h"

SD_BEGIN

// OS handle to the process.
#ifdef PLATFORM_WINDOWS
#define SD_PROC_HANDLE HANDLE
#else
#define SD_PROC_HANDLE void*
#endif


struct FILE_API ProcessInfo final
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The executing file. */
	FileAttributes ExecFile;

	/* The OS handle to the child process. */
	SD_PROC_HANDLE Handle;

	/* The process' unique identifier. */
	Int ProcessId;

	/* Becomes true if this object holds a handle. This is used to check if they need to be released when calling ReleaseHandles. */
	bool bHasHandles;

#ifdef PLATFORM_WINDOWS
	/* !Windows specific! Handle to the start process. This handle needs to be closed when it's no longer in use. */
	STARTUPINFO OS_StartInfo;

	/* !Windows specific! Essentially contains copies of Handle and ProcessId. This handle needs to be closed when it's no longer in use. */
	PROCESS_INFORMATION OS_ProcessInfo;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ProcessInfo ();
	ProcessInfo (const ProcessInfo&) = delete; //Copying is prohibited to prevent accidentally closing handles.
	~ProcessInfo ();


	/*
	=====================
	  Operators
	=====================
	*/

private:
	void operator= (const ProcessInfo&) = delete;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this process finished execution.
	 */
	bool IsFinished () const;

	/**
	 * If this process holds any handles to the process, this function will release them.
	 * This is automatically called in the destructor.
	 */
	void ReleaseHandles ();
};
SD_END