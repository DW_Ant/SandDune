/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileUtils.h
  Abstract class containing generic utility functions related to file
  systems.
=====================================================================
*/

#pragma once

#include "File.h"

SD_BEGIN
class FILE_API FileUtils : public BaseUtils
{
	DECLARE_CLASS(FileUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Splits the full file name from the path. If a path is given, then the outPath will include the last directory separator.
	 * For example "D:\MyDocuments\MyFile.txt" would result in: outPath="D:\MyDocuments\" outFileName="MyFile.txt"
	 */
	static void ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFileName);

	/**
	 * Extracts the file name from its extension (the leading period is trimmed).
	 * If the file name contains multiple periods then the extension is considered the block of text after the first period.
	 * For example "MyTexture.txtr.ini" would result in: outFileName="MyTexture" outExtension="txtr.ini"
	 */
	static void ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension);

	/**
	 * Copies the contents of one file to another (regardless if destination already exists or not).
	 * The source and destination expects path and file extensions.
	 */
	static void CopyFileContents (const DString& source, const DString& destination);
};
SD_END