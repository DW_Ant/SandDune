/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsFile.h
  Utilities containing Windows-specific file system utilities.
=====================================================================
*/

#pragma once

#ifdef PLATFORM_WINDOWS

//Windows-specific includes related to file structures
#include <direct.h>

#ifdef PLATFORM_64BIT
#define OUTPUT_LOCATION TXT("Win64")
#else
#define OUTPUT_LOCATION TXT("Win32")
#endif

/* Extension used for the owning process.  For Windows, it would be SandDune(.exe) */
#define OUTPUT_EXTENSION TXT(".exe")

SD_BEGIN
class Directory;
class FileDialogueOptions;
class LaunchProcessOptions;
class FileAttributes;
class ProcessInfo;

/**
 * Converts the relative path to absolute path.  Path is relative to the base directory.
 * A word of caution:  Some operating systems such as Windows does not have a unique name for
 * a full path (meaning, it's not a good idea to use string comparison for directories).
 *
 * Example:
 * Sometimes it includes the driver and working directory.
 * "test" -> C:\Documents and Settings\user\My Documents\test
 *
 * Sometimes it includes only the driver
 * "\\test" -> C:\test
 *
 * And I simply don't have an explanation for this one mentioned in MSDN...
 * "..\\test" -> C:\Documents and Settings\user\test
 *
 * Returns empty string if there was an error.
 */
DString FILE_API OS_FullPath (const DString& relativePath);

/**
 * Sets this process's current working directory to the specified location.
 * Returns true on success.
 */
bool FILE_API OS_SetWorkingDirectory (const Directory& newWorkingDirectory);

/**
 * Retrieves a handle to a file from attributes.  You must remember to call OS_CloseFile to release resources.
 */
FilePtr FILE_API OS_GetFileHandle (const FileAttributes& attributes);
FilePtr FILE_API OS_GetFileHandle (const DString& absDirectory, const DString& fullFileName);

/**
 * Attempts to create a file, and returns a handle to that file.
 */
FilePtr FILE_API OS_CreateFile (const Directory& path, const DString& fullFileName);

/**
 * Writes data to the file handle.  Returns true on success.
 */
bool FILE_API OS_WriteToFile (FilePtr fileHandle, const void* values, int numBytes);

/**
 * Makes a copy from source to destination.  If bOverwriteExisting is true and if there's a file already
 * named destAddress, then it'll replace that file.  Address contains absolute path and the full file name.
 * Returns true on success.
 */
bool FILE_API OS_CopyFile (const FileAttributes& fileSource, const Directory& destinationPath, const DString& destName, bool bOverwriteExisting = false);

/**
 * Attempts to delete a file with the given name.
 * Returns false if it was unable to delete file.
 */
bool FILE_API OS_DeleteFile (const FileAttributes& target);
bool FILE_API OS_DeleteFile (const DString& absPath, const DString& fullFileName);

void FILE_API OS_CloseFile (FilePtr& outHandle);

/**
 * Returns if the given file object points to an existing file.
 */
bool FILE_API OS_CheckIfFileExists (const FileAttributes& file);
bool FILE_API OS_CheckIfFileExists (const DString& absDirectory, const DString& fullFileName);

/**
 * Returns true if the directory exists in the OS.
 */
bool FILE_API OS_CheckIfDirectoryExists (const Directory& dir);
bool FILE_API OS_CheckIfDirectoryExists (const DString& dir);

/**
 * Attempts to create a directory. Returns true if the directory is created or exists.
 * This function also ensures the parent directories are exists before creating the inner most directory.
 */
bool FILE_API OS_CreateDirectory (const Directory& dir);

/**
 * Attempts to delete the specified directory from the OS. Returns true if the directory was found and deleted.
 * If there are empty sub directories, those will be deleted as well.
 */
bool FILE_API OS_DeleteDirectory (const Directory& dir);

/**
 * Retrieves the file size in bytes.
 */
unsigned long long FILE_API OS_GetFileSize (const FileAttributes& file);
unsigned long long FILE_API OS_GetFileSize (const DString& absPath, const DString& fullFileName);

/**
 * Returns true if the file is ReadOnly.
 */
bool FILE_API OS_IsFileReadOnly (const FileAttributes& file);
bool FILE_API OS_IsFileReadOnly (const DString& absPath, const DString& fullFileName);

/**
 * Sets the timestamps with the file's creation date and last modified date.
 * Returns true on success.
 */
bool FILE_API OS_GetFileTimestamps (const FileAttributes& file, DateTime& outCreatedDate, DateTime& outModifiedDate);
bool FILE_API OS_GetFileTimestamps (const DString& absPath, const DString& fullFileName, DateTime& outCreatedDate, DateTime& outModifiedDate);

/**
 * Retrieves the file name and directory of a given handle.
 */
void FILE_API OS_GetFileName (FilePtr fileHandle, DString& outAbsPath, DString& outFullFileName);

/**
 * Populates the parameters with the path and file name of the executable location.
 */
void FILE_API OS_GetProcessLocation (DString& outPath, DString& outProcessName);
void FILE_API OS_GetProcessLocation (DString& outPath);

/**
 * Returns true if the given characters could represent a parent or current directory.
 * For example:  returns true for '.' (current directory), or '..' (parent directory) for Windows.
 */
bool FILE_API OS_IsDirectoryReflection (const Directory& dir);

/**
 * Freezes the current thread and popups the file browser dialogue window.
 * The selectedFiles will only have one item in there for single select open or saving a file. The vector may have more than one file attributes when opening multiple files.
 * This function returns true on success even if the user cancelled (selecting nothing). Returns false if there was an error.
 */
bool FILE_API OS_BrowseFiles (const FileDialogueOptions& options, std::vector<FileAttributes>& outSelectedFiles);

/**
 * Freezes the current thread and popups a dialogue window that allows the user to select a directory.
 * The function returns true on success even if the user cancelled (selecting nothing). Returns false if there was an error.
 * The outSelectedDirectory is equal to Directory::INVALID_DIRECTORY if the user cancelled the operation.
 */
bool FILE_API OS_BrowseDirectory (Directory defaultDirectory, Directory& outSelectedDirectory, Int persistenceId = 0);

/**
 * Attempts to execute the process with the specified options.
 * The results of the child process is then written to the ProcessInfo.
 * Returns true on success.
 */
bool FILE_API OS_LaunchProcess (const LaunchProcessOptions& launchOptions, ProcessInfo& outProcessInfo);

/**
 * Freezes the current thread until the given process terminates or until enough time elapsed.
 * The maxWaitTime is in seconds.
 * If the maxWaitTime is not positive, then there is no time limit.
 * Returns true if the process terminated. Returns false if the max wait time expired.
 */
bool FILE_API OS_WaitForProcessFinish (const ProcessInfo& procInfo, float maxWaitTime);

/**
 * Returns true if the given process is still running.
 */
bool FILE_API OS_IsProcessAlive (const ProcessInfo& procInfo);

/**
 * Passes the kill command to terminate the specified process.
 * Returns true if successful.
 */
bool FILE_API OS_KillProcess (const ProcessInfo& procInfo, int exitCode = 0);

/**
 * Returns the exit code of the given process.
 * If the process is still running, then this function returns false and the outExitCode is not assigned.
 */
bool FILE_API OS_GetExitCode (const ProcessInfo& procInfo, int& outExitCode);
SD_END
#endif //PLATFORM_WINDOWS