/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  File.h
  Contains important file includes and definitions for the File module.

  The File module interfaces with the operating system's file and
  directory structures.  This module includes file-related utilities.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Time\Headers\TimeClasses.h"

#include <fstream>
#include <sstream>
#include <iterator>

typedef void* FilePtr;

#ifdef PLATFORM_WINDOWS
	#ifdef FILE_EXPORT
		#define FILE_API __declspec(dllexport)
	#else
		#define FILE_API __declspec(dllimport)
	#endif
#else
	#define FILE_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsFile.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacFile.h"
#endif

SD_BEGIN
extern FILE_API LogCategory FileLog;
SD_END