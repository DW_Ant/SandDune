/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileDialogueOptions.h
  Defines a data struct that is used for the OS_BrowseFiles function. This struct allows
  developers to configure settings for the dialogue window.
=====================================================================
*/

#pragma once

#include "Directory.h"
#include "File.h"

SD_BEGIN
struct FILE_API FileDialogueOptions
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EDialogueType
	{
		DT_Open, //Open one or more files
		DT_SelectDir, //Select a particular directory
		DT_Save //Select a specific file to save
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	EDialogueType DialogueType;

	Directory DefaultFolder;

	/* If true, then the user is able to select multiple files. Only applicable when opening files. */
	bool bMultiSelect;

	/* Identifier the OS use for persistent data. For example, Windows may match identifier information to remember the last browsed folder.
	Leave it as zero to use default behavior. */
	Int PersistenceId;

	/* If true, then the user is only allowed to save as a file type specified in the file type list. Not applicable when selecting directories. */
	bool bStrictFileTypesOnly;

	/* For open dialogues, this is a list of file types that may be opened. For saving dialogue, this is a list of file types that are permitted to be saved.
	NOTE: On Windows systems this save restriction is only enforced if bStrictFileTypesOnly is true. Otherwise the user can choose the file extension.
	Leave this vector empty if there are no file type restrictions. Don't include the periods (eg:  "txt", "jpg").
	Not applicable when selecting directories. */
	std::vector<DString> FileTypes;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	FileDialogueOptions (EDialogueType inDialogueType);
};
SD_END