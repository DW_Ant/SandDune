/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileUnitTester.h
  Tests data types and utilities related to the File module.
=====================================================================
*/

#pragma once

#include "File.h"

#ifdef DEBUG_MODE

SD_BEGIN
class FILE_API FileUnitTester : public UnitTester
{
	DECLARE_CLASS(FileUnitTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Directory that contains files for this UnitTest. */
	static const Directory UnitTestLocation;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestFileUtils (EUnitTestFlags testFlags) const;
	virtual bool TestDirectories (EUnitTestFlags testFlags) const;
	virtual bool TestOSCalls (EUnitTestFlags testFlags) const;
	virtual bool TestFileAttributes (EUnitTestFlags testFlags) const;
	virtual bool TestFileIterator (EUnitTestFlags testFlags) const;
	virtual bool TestFileWriter (EUnitTestFlags testFlags) const;
	virtual bool TestConfig (EUnitTestFlags testFlags) const;
	virtual bool TestBinaryFile (EUnitTestFlags testFlags) const;
};
SD_END
#endif