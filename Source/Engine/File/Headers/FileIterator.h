/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileIterator.h
  The FileIterator is an utility class that makes it easy to iterate
  through files and directories within a directory.
=====================================================================
*/

#pragma once

#include "File.h"
#include "Directory.h"

SD_BEGIN
class FILE_API FileIterator
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ESearchFlags
	{
		SF_None = 0,
		SF_IncludeFiles = 1,
		SF_IncludeDirectories = 2,
		SF_IncludeSubDirs = 4, //Flag to determine if the FileIterator should select OS objects within sub directories.
		SF_IncludeReflectiveDirs = 8, //Flag to determine if FileIterator should include reflective directories such as current and parent directory.
		SF_IncludeEverything = 255
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various bitwise flags this iterator uses to determine if an OS object should be selected or not. */
	ESearchFlags SearchFlags;

	/* Attributes describing the current OS object (could either be a directory or a file. */
	FileAttributes SelectedAttributes;

	/* Directory the iterator is searching through.  Relative Directories are automatically converted to absolute paths. */
	Directory BaseDirectory;

	/* The sub directory this iterator is currently searching through.  If the path is empty, then it assumes that there isn't a subiterator. */
	Directory SelectedDirectory;

	/* Some OS like Windows does not seem to support recursive searches.  So the iterator creates iterators
	when iterating through files within subdirectories.  This value is not used if SearchFlags does not include subdirectories. */
	FileIterator* SubIterator;

#ifdef PLATFORM_WINDOWS
	/* Windows-specific helper to iterate through files. */
	WIN32_FIND_DATA WindowsFindData;

	/* Windows needs a handle to the initial file for some undocumented reason.
	My guess is that Windows cache information to the handle for the FindNextFile to reference later.
	For example the wild card character "\\*" must have been cached somewhere.  Was it cached in the handle, itself? */
	FilePtr InitialFile;
#endif

private:
	/* Name of the most recent directory that expanded into another FileIterator.
	Used to determine if this iterator needs to create a subiterator for a directory. */
	Directory PreviousExpandedDirectory;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	FileIterator (const Directory& inDirectory);
	FileIterator (const Directory& inDirectory, ESearchFlags inSearchFlags);
	virtual ~FileIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator++ (); //++iter
	virtual void operator++ (int); //iter++


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Retrieves additional information about the selected file.
	 */
	virtual AdvFileAttributes RetrieveFileAttributes () const;

	/**
	 * Returns true if the iterator (or its subiterator) selected a directory.
	 */
	virtual bool IsDirectorySelected () const;

	/**
	 * Returns true, if the iterator can no longer find another file or a directory.
	 */
	virtual bool FinishedIterating () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ESearchFlags GetSearchFlags () const
	{
		return SearchFlags;
	}

	/**
	 * Returns the current selected file.  This may return the SubIterator's SelectedFile
	 * if the FileIterator is currently searching through a subdirectory.
	 */
	virtual FileAttributes GetSelectedAttributes () const;
	virtual const FileAttributes& ReadSelectedAttributes () const;

	/**
	 * Returns the current selected directory.  This may return the SubIterator's
	 * SelectedDirectory if the FileIterator is currently searching through a subdirectory.
	 */
	virtual Directory GetSelectedDirectory () const;
	virtual const Directory& ReadSelectedDirectory () const;

	inline Directory GetBaseDirectory () const
	{
		return BaseDirectory;
	}

	inline const Directory& ReadBaseDirectory () const
	{
		return BaseDirectory;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Selects the first file within the directory.  The path is relative to the FileIterator's Directory property.
	 */
	virtual void SelectFirstFile ();

	/**
	 * Selects the next available file.  If there aren't any more files, then it'll return the next
	 * file within another directory if sub directories are relevant.
	 */
	virtual void SelectNextFile ();
};

DEFINE_ENUM_FUNCTIONS(FileIterator::ESearchFlags)
SD_END