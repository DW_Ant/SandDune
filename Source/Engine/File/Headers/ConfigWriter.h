/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ConfigWriter.h
  A Text File Writer that's responsible for serializing configurable properties
  to a specified ini file.  This class is also responsible for retrieving saved
  properties from an ini file.
=====================================================================
*/

#pragma once

#include "TextFileWriter.h"

SD_BEGIN
class FILE_API ConfigWriter : public TextFileWriter
{
	DECLARE_CLASS(ConfigWriter)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SDataSection
	{
	public:
		DString SectionName = DString::EmptyString;

		/* List of each property value in this section. Multiline entries are automatically collapsed into one element, but when they're saved to a file, they will be written to multiple lines.
		 Example:
		 SectionData Element (runtime):  "MyMultiLineString=Line 1\nLine 2"
		 How it appears in file: "MyMultiLineString=Line 1\
		 Line 2
		 */
		std::vector<DString> SectionData;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Character used to signal that the property value will carry on to the next line. */
	static const TCHAR MULTI_LINE_CHARACTER;

protected:
	/* If false, then this file will only read from the header data (will not read from section data). This must be set before the file is opened. */
	bool bReadEverything;

	std::vector<SDataSection> Sections;

	/* Lines in the file that are not associated with any section. */
	std::vector<DString> HeaderData;

	/* True if a property changed and this config file needs to be saved. */
	bool bNeedsSave;

private:
	/* Number of lines on the file that makes up the HeaderData. NOTE: This isn't always the vector size of HeaderData since
	multiple lines in the file could be consolidated to one element in the vector for multi lined properties. */
	Int NumLinesInFileHeader;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool OpenFile (const DString& fileName, bool bReplaceFile) override;
	virtual bool OpenFile (const FileAttributes& fileAttributes, bool bReplaceFile) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Saves all TextEntries (from AddTextEntry function) and properties to the configuration file.
	 */
	virtual void SaveConfig ();

	/**
	 * Saves the string property to the config file within the specified section name.
	 */
	virtual void SavePropertyText (const DString& sectionName, const DString& propertyName, const DString& propertyValue);

	/**
	 * Automatically instantiates a ConfigWriter object, and saves the PropertyText.  The instantiated object is then immediately destroyed.
	 * Returns true if the property successfully saved to the ini file.
	 */
	static bool SavePropertyText (const DString& fileName, const DString& sectionName, const DString& propertyName, const DString& propertyValue);

	/**
	 * Returns the string representation of the property value that's associated with the propertyName within the specified section name.
	 * If section name is not specified, it'll only search through the header data.
	 */
	virtual DString GetPropertyText (const DString& sectionName, const DString& propertyName) const;

	/**
	 * Returns true if this config file contains the specified section regardless if it's empty or not.
	 * SectionName comparison is case insensitive.
	 */
	virtual bool ContainsSection (const DString& sectionName) const;

	/**
	 * Returns true if this config file contains a property value within the given section.
	 */
	virtual bool ContainsProperty (const DString& sectionName, const DString& propertyName) const;

	/**
	 * Removes all data associated with the given section name.
	 * @param deleteSection If true, then the entire section including the header text will be removed. Otherwise, the section will be emptied, but the header text will remain.
	 * Returns true if the section was found and removed.
	 */
	virtual bool ClearSection (const DString& sectionName, bool deleteSection);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetReadEverything (bool newReadEverything);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsReadingEverything () const
	{
		return bReadEverything;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads all text and populates it to the Header vector until the first section is encountered.
	 */
	virtual void PopulateHeaderData ();

	/**
	 * Reads the config file and populates the section data as it goes.
	 */
	virtual void PopulateSectionData ();

	/**
	 * Returns true if the current line is a section header format (ie:  "[SectionName]")
	 */
	virtual bool IsSectionHeaderFormat (const DString& text) const;

	/**
	 * Returns true if the given text is a comment.
	 */
	virtual bool IsCommentFormat (const DString& line) const;

	/**
	 * Searches through all sections until the section name matches the parameter name.  Returns the index of that section.
	 */
	virtual size_t FindSectionIndex (const DString& sectionName) const;

	/**
	 * Searches and returns property value that's associated with the propertyName within the specified section.
	 * @param sectionIndex The index of the section to search in. If it's INDEX_NONE, then it'll only search in the header data.
	 * @param outPropertyIndex is the index value where the property value resides within the section.
	 */
	virtual DString FindPropertyWithinSection (size_t sectionIndex, const DString& propertyName) const;
	virtual DString FindPropertyWithinSection (size_t sectionIndex, const DString& propertyName, size_t& outPropertyIndex) const;
	virtual DString FindPropertyWithinHeader (const DString& propertyName, size_t& outPropertyIndex) const;

	/**
	 * Attempts to extract the property value of the given line and records the value to parameter.
	 * Returns false if the given line is not in correct format (ie:  comment), or if the propertyName does not match.
	 */
	virtual bool ReadPropertyValue (const DString& fullLine, const DString& targetPropertyName, DString& outPropertyValue) const;

	/**
	 * Returns true if the given line matches the target property name.
	 * @Param:  outEqualPosIdx is the character index the equal sign is found within fullLine.
	 */
	virtual bool ContainsPropertyValue (const DString& fullLine, const DString& targetPropertyName, Int& outEqualPosIdx) const;

	/**
	 * Parses through the given entry, and for each new line character would be written as a separate text entry.
	 * If there are no new line characters, then this function acts the same as directly calling AddTextEntry.
	 */
	virtual void WriteMultiLineText (const DString& newLine);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class Type>
	void SaveProperty (const DString& sectionName, const DString& propertyName, Type propertyValue)
	{
		SavePropertyText(sectionName, propertyName, propertyValue.ToString());
	}

	template <class Type>
	Type GetProperty (const DString& sectionName, const DString& propertyName) const
	{
		DString txtResult = GetPropertyText(sectionName, propertyName);

		Type result;
		result.ParseString(txtResult);
		
		return result;
	}

	/**
	 * Populates the array with every match of propertyName found within the given section.
	 * If the sectionName is not specified, then it'll populate the array with matches within the header.
	 */
	template <class Type>
	void GetArrayValues (const DString& sectionName, const DString& propertyName, std::vector<Type>& outValues) const
	{
		ContainerUtils::Empty(OUT outValues);
		if (!sectionName.IsEmpty())
		{
			size_t sectionIdx = FindSectionIndex(sectionName);

			if (sectionIdx < Sections.size())
			{
				FindArrayWithinSection<Type>(sectionIdx, propertyName, outValues);
			}
		}
		else
		{
			//Populate values from header since section is not specified
			for (size_t i = 0; i < HeaderData.size(); i++)
			{
				DString propValue;
				if (ReadPropertyValue(HeaderData.at(i), propertyName, propValue))
				{
					Type newItem;
					newItem.ParseString(propValue);
					outValues.push_back(newItem);
				}
			}
		}
	}

	/**
	 * Updates the array values of the specified section to the new values.
	 * If sectionName is not found, then it'll apply for the header data.
	 */
	template <class Type>
	void SaveArray (const DString& sectionName, const DString& arrayName, const std::vector<Type>& arrayValues)
	{
		//Index at which the array appears within the section or header section
		size_t arrayIdx = UINT_INDEX_NONE;

		if (!sectionName.IsEmpty())
		{
			size_t sectionIdx = FindSectionIndex(sectionName);

			if (sectionIdx >= Sections.size())
			{
				//Section not found.  Append new section
				sectionIdx = Sections.size();
				SDataSection newSection;
				newSection.SectionName = sectionName;
				Sections.push_back(newSection);
				arrayIdx = 0;
			}
			else
			{
				//Iterate through the current section and clear all property values with matching arrayName
				size_t i = 0;
				while (i < Sections.at(sectionIdx).SectionData.size())
				{
					Int equalPos;
					if (ContainsPropertyValue(Sections.at(sectionIdx).SectionData.at(i), arrayName, equalPos))
					{
						if (arrayIdx == UINT_INDEX_NONE)
						{
							arrayIdx = i; //the array should appear here.
						}

						//Clear line
						Sections.at(sectionIdx).SectionData.erase(Sections.at(sectionIdx).SectionData.begin() + i);
						continue;
					}

					i++;
				}
			}

			if (arrayIdx == UINT_INDEX_NONE)
			{
				//Didn't find where the array was previously located.  Append to end
				arrayIdx = Sections.at(sectionIdx).SectionData.size();
			}

			//Add array to this section (traverse backwards to not mess up the array order)
			for (Int i = arrayValues.size() - 1; i >= 0; i--)
			{
				Sections.at(sectionIdx).SectionData.insert(Sections.at(sectionIdx).SectionData.begin() + arrayIdx, arrayName + TXT("=") + arrayValues.at(i.ToUnsignedInt()).ToString());
			}
		}
		else //Update header data
		{
			//Iterate through the header data and clear all property values with matching arrayName
			size_t i = 0;
			while (i < HeaderData.size())
			{
				Int equalPos;
				if (ContainsPropertyValue(HeaderData.at(i), arrayName, equalPos))
				{
					if (arrayIdx == UINT_INDEX_NONE)
					{
						arrayIdx = i; //the array should appear here.
					}

					//Clear header line
					HeaderData.erase(HeaderData.begin() + i);
					continue;
				}

				i++;
			}

			if (arrayIdx == UINT_INDEX_NONE)
			{
				//Didn't find where the array was previously located.  Append to end
				arrayIdx = HeaderData.size();
			}

			//Add array to this section (traverse backwards to not mess up the array order)
			for (Int i = arrayValues.size() - 1; i >= 0; i--)
			{
				HeaderData.insert(HeaderData.begin() + arrayIdx, arrayName + TXT("=") + arrayValues.at(i.ToUnsignedInt()).ToString());
			}
		}

		bNeedsSave = true;
	}

protected:
	/**
	 * Appends all matching property values to the array.
	 */
	template <class Type>
	void FindArrayWithinSection (size_t sectionIdx, const DString& propertyName, std::vector<Type>& outValues) const
	{
		for (size_t i = 0; i < Sections.at(sectionIdx).SectionData.size(); i++)
		{
			DString propValue;
			if (ReadPropertyValue(Sections.at(sectionIdx).SectionData.at(i), propertyName, propValue))
			{
				Type newElement;
				newElement.ParseString(propValue);
				outValues.push_back(newElement);
			}
		}
	}
};
SD_END