/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Directory.h
  An object that represents a OS directory, and contains utilities to format and access directories.
=====================================================================
*/

#pragma once

#include "File.h"

SD_BEGIN
class FILE_API Directory : public DProperty
{


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EDirectoryState
	{
		DS_Undefined,
		DS_AbsolutePath,
		DS_RelativePath
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString DIRECTORY_SEPARATOR;

	/* List of forbidden characters that cannot be used for a directory name on current platform. */
	static const std::vector<char> FORBIDDEN_CHARACTERS;

	/* Directory containing invalid characters that indicates this is not a valid directory. */
	static const Directory INVALID_DIRECTORY;

	/* Important SandDune directories */
	static const Directory BASE_DIRECTORY;
	static const Directory BINARY_DIRECTORY;
	static const Directory CONFIG_DIRECTORY;
	static const Directory CONTENT_DIRECTORY;
	static const Directory DEV_ASSET_DIRECTORY;
	static const Directory DOCUMENT_DIRECTORY;
	static const Directory SOURCE_DIRECTORY;

protected:
	/* The full path name this variable is representing including drive letter and parent paths.
	If this directory path does not include a drive letter then it assumes it's a relative path to another.
	This variable should always end with a directory separator to prepend well with other directories and file names. */
	DString DirectoryPath;

	/* Variable cache to identify if it's using absolute or relative paths. */
	mutable EDirectoryState DirectoryState;

	/* If true, then this Directory object will automatically format its DirectoryPath to conform to platform's specifications.
	Setting this to true is safer, but it can get expensive when conducting numerous modifications to the path. */
	bool AutoFormat;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Directory (); //Creates a directory object pointing in current working directory
	Directory (const Directory& inDirectory);
	explicit Directory (const DString& inPath, bool inAutoFormat);
	virtual ~Directory ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Directory& other);
	void operator= (const DString& inPath);

	/**
	 * Returns true if this directory's exact path is the same as the other directory.  Note:  since there are multiple
	 * ways to reach a particular path, this operation may return false even if they point to the same location.
	 */
	bool operator== (const Directory& other) const;
	bool operator!= (const Directory& other) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Gets this process's current working directory.
	 * Call OS_SetWorkingDirectory to notify the Operating System to change the working directory.
	 * Always returns in absolute path.
	 */
	static Directory GetWorkingDirectory ();

	/**
	 * Converts the given relative path to absolute path.  The path is relative to the base directory.
	 * This function also guarantees that the results ends with a directory separator character.
	 */
	static DString ToAbsolutePath (const DString& relativePath);

	/**
	 * Converts this Directory object to absolute path.  Does nothing if already using absolute paths.
	 */
	virtual void ConvertToAbsPath ();

	/**
	 * Returns a copy of this Directory object that's in absolute path.
	 */
	Directory GetAbsPath () const;

	/**
	 * Returns true if this directory exists on the environment's file system.
	 */
	inline bool Exists () const
	{
		return OS_CheckIfDirectoryExists(*this);
	}

	/**
	 * Splits the directories into an ordered list where the first element is the drive, and
	 * the last element is the inner-most directory.
	 *
	 * Example:
	 * C:\SandDune\SomeSubDirectory\InnerMostDirectory\
	 * Returns
	 * [0] = "C:"
	 * [1] = "SandDune"
	 * [2] = "SomeSubDirectory"
	 * [3] = "InnerMostDirectory"
	 */
	virtual std::vector<DString> SplitDirectoryToList () const;

	/**
	 * Constructs a directory object from the given list.
	 *
	 * Example:
	 * [0] = "C:"
	 * [1] = "SandDune"
	 * [2] = "SomeSubDirectory"
	 * [3] = "InnerMostDirectory"
	 * Returns "C:\SandDune\SomeSubDirectory\InnerMostDirectory\"
	 */
	static Directory ListToDirectory (const std::vector<DString>& directoryList);

	/**
	 * Returns the name of the inner-most directory name.
	 *
	 * Example:
	 * C:\SandDune\SomeSubDirectory\InnerMostDirectory\
	 * Returns "InnerMostDirectory"
	 */
	virtual DString GetInnerMostDirectory () const;

	/**
	 * Returns the immediate parent directory from this directory.
	 * Returns true if this directory has a parent directory.
	 */
	virtual bool GetParentDir (Directory& outParentDir) const;

#ifdef PLATFORM_WINDOWS
	/**
	 * If this is an absolute path, this returns the drive letter this directory resides in.
	 * If using relative paths, then this will return an empty string.
	 */
	virtual DString GetDriveLetter () const;
#endif


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDirectory (const DString& newDirectory);
	virtual void SetAutoFormat (bool newAutoFormat);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetDirectoryPath () const
	{
		return DirectoryPath;
	}

	inline const DString& ReadDirectoryPath () const
	{
		return DirectoryPath;
	}

	/**
	 * Returns true if this directory is an absolute path.  Otherwise it's a relative path to something.
	 */
	virtual bool IsAbsPath () const;

	inline bool IsAutoFormatting () const
	{
		return AutoFormat;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Finds the engine's base absolute directory, and records the value.
	 */
	static Directory FindBaseDirectory ();

	/**
	 * Returns the directory that houses the executable location.  Note:  the location may vary based on platform.
	 */
	static Directory FindBinaryDirectory ();

	/**
	 * Edits the given string to remove all invalid characters from it.
	 * @param removeColons If true, then this function will remove all colons from the given string.  This is only used on windows platforms.
	 */
	static void RemoveInvalidChars (DString& outDirToEdit, bool removeColons);

	/**
	 * Ensures the directory path is in the expected format (eg:  always ending with a directory separator).
	 * This also normalizes the directory separators.
	 * This will also remove any invalid characters (which is platform-specific).
	 * This function is not virtual since constructors invoke this function.
	 */
	void SanitizeInternalVariables ();

	/**
	 * Reads the beginning of the directory path to identify if this directory is using relative or absolute paths.
	 */
	virtual void ComputeDirectoryState () const;
};

#pragma region "External Operators"
	FILE_API Directory operator/ (const Directory& left, const Directory& right);
	FILE_API Directory operator/ (const Directory& left, const DString& right);
	FILE_API Directory operator/ (const Directory& left, const char* right);
	FILE_API Directory operator/ (const DString& left, const Directory& right);
	FILE_API Directory operator/ (const DString& left, const DString& right);
	FILE_API Directory operator/ (const DString& left, const char* right);
	FILE_API Directory operator/ (const char* left, const Directory& right);
	FILE_API Directory operator/ (const char* left, const DString& right);
	//FILE_API Directory operator/ (const char* left, const char* right); :(

	FILE_API Directory& operator/= (Directory& left, const Directory& right);
	FILE_API Directory& operator/= (Directory& left, const DString& right);
	FILE_API Directory& operator/= (Directory& left, const char* right);
#pragma endregion
SD_END