/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ComponentSerialize.h
  An object responsible for serializing TickComponents and EntityComponents to a binary file.
  Recursive components are not supported since this class is only used for testing purposes.

  This debug class is intended to verify the BinaryFile object.
=====================================================================
*/

#pragma once

#include "BinaryFile.h"

#ifdef DEBUG_MODE
SD_BEGIN
/*
The binary file is in this format:
<HeaderData>
<Int - NumEntities>
<Begin foreach Entity>
	<DString - Entity DebugName>
	<Int - Number of Component>
	<Begin foreach component>
		<Int - ComponentClass>
		<DString - Component DebugName>
		<Reads data based on component type>
	<End foreach component>
<End foreach Entity>
*/
class FILE_API ComponentSerializer : public BinaryFile
{
	DECLARE_CLASS(ComponentSerializer)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	/**
	 * List of supported components that can be saved to the file.
	 * For simplicity purposes, each component simply maps to a unique number.
	 * A more robust approach would require all serializable objects implement an Interface that defines
	 * how an object serializes and deserializes to a data buffer.
	 */
	enum EClassType
	{
		CT_EntityComponent = 0,
		CT_TickComponent = 1
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of Entities that were instantiated when reading from a file. This Entity takes ownership over these Entities where if this
	Entity is destroyed, all Entities in this list are also destroyed.
	Removing Entities from this list will remove this object's ownership over those removed Entities.
	When saving to a file, all entities in this list will be serialized to the file. */
	std::vector<Entity*> StagedEntities;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual bool ReadContents (const DataBuffer& incomingData) override;
	virtual bool SaveContents (DataBuffer& outBuffer) const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline std::vector<Entity*>& EditStagedEntities ()
	{
		return StagedEntities;
	}
};
SD_END
#endif