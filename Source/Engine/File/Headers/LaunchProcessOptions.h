/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LaunchProcessOptions.h
  A data struct that ties with OS_LaunchProcess that contains various parameters to
  configure how the process should launch.
=====================================================================
*/

#pragma once

#include "File.h"
#include "FileAttributes.h"

SD_BEGIN
struct FILE_API LaunchProcessOptions
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The file name to the executable. */
	FileAttributes ExecFile;

	/* Command line arguments passed to the child process. */
	DString CmdLineArgs;

	/* If true, then the process will be a child process of this process. Otherwise, it'll be independent. */
	bool bChildProcess;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	LaunchProcessOptions (const FileAttributes& inExecFile);
};
SD_END