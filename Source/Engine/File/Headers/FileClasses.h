/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileClasses.h
  Contains all header includes for the File module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the FileClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_FILE
#include "AdvFileAttributes.h"
#include "BinaryFile.h"
#include "ComponentSerializer.h"
#include "ConfigWriter.h"
#include "Directory.h"
#include "File.h"
#include "FileAttributes.h"
#include "FileDialogueOptions.h"
#include "FileIterator.h"
#include "FileUnitTester.h"
#include "FileUtils.h"
#include "LaunchProcessOptions.h"
#include "PlatformMacFile.h"
#include "PlatformWindowsFile.h"
#include "ProcessInfo.h"
#include "TextFileWriter.h"

#endif
