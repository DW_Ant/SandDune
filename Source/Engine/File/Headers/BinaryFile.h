/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BinaryFile.h
  The base class of binary files. This class is responsible for defining the header information,
  and establishing the framework for saving and loading.

  This object reads and saves files synchronously.
=====================================================================
*/

#pragma once

#include "File.h"
#include "FileAttributes.h"

SD_BEGIN
class FILE_API BinaryFile : public Object
{
	DECLARE_CLASS(BinaryFile)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EPlatformBitSize
	{
		PS_32Bit,
		PS_64Bit
	};

	enum EStringEncoding
	{
		SE_Ansi,
		SE_Utf8,
		SE_Utf16,
		SE_Utf32,
		SE_Wide
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The platform size of the current buffer this object is reading from. */
	EPlatformBitSize PlatformBitSize;

	/* The character encoding the current buffer this object is reading from. */
	EStringEncoding StrEncoding;

	/* The file this instance most recently accessed for reading or writing. */
	mutable FileAttributes LatestAccessedFile;

	/* Becomes true if this object is currently accessing a file. It should only read or write to
	one file at a time. */
	mutable bool bAccessingFile;

private:
	/* The hard coded magic number that is used to detect data corruption, and if the binary file is in the right format. */
	static const uint32 MAGIC_NUMBER;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to open the specified file in binary format. If the header data is compatible, it'll proceed with the ReadContents function,
	 * where the subclasses define the behavior when loading the specified file.
	 * Returns true if this object is able to read the file contents correctly.
	 * Returns false if the file is not found, corrupted, or is in a later version number than this engine instance.
	 * This can also return false if the contents of the file does not match the expected file format.
	 */
	bool OpenFile (const FileAttributes& file);

	/**
	 * Saves the contents to the specified file in binary format.
	 * This will automatically add the expected header data before appending the file type specific data.
	 * Returns true if the contents are saved to the given file.
	 * This function will not overwrite this file if it already exists and is read only.
	 */
	bool SaveFile (const FileAttributes& file) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsAccessingFile () const
	{
		return bAccessingFile;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Converts the DataBuffer from its older engine version to this current version for backwards compatibility.
	 * Forward compatibility is not supported.
	 * Returns true if the data buffer is successfully converted. This function does not write to the file.
	 */
	virtual bool ConvertToCurrentVersion (DataBuffer& outData, Int oldMajorVersion, Int oldMinorVersion);

	/**
	 * Assumes the read position in the given data buffer is pointing at a string, this function will pull a string from the data buffer.
	 * Then it returns that string with the encoding that matches this environment's encoding. The read position in the buffer will advance.
	 * @param outBytesRead - set to true if it was successfully able to read data from the buffer.
	 */
	virtual DString ReadStringFromBuffer (const DataBuffer& incomingData, bool& outBytesRead) const;

	/**
	 * Assumes that the read position in the given data buffer is pointing at an int, this function will pull an Int from the data buffer.
	 * Then it returns that Int in the correct size that matches this application's configuration. The read position in the buffer will advance.
	 * @param outBytesRead - set to true if this function is able to read an Int from the buffer.
	 */
	virtual Int ReadIntFromBuffer (const DataBuffer& incomingData, bool& outBytesRead) const;

	/**
	 * Reads through the given data buffer to extract contents from it.
	 * Returns true if this object is able to read all of its data it expects.
	 */
	virtual bool ReadContents (const DataBuffer& incomingData);

	/**
	 * Writes out the contents to the given data buffer.
	 * Returns true if the contents are serialized to the data buffer.
	 */
	virtual bool SaveContents (DataBuffer& outBuffer) const;
};
SD_END