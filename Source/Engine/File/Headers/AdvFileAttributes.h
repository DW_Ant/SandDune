/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AdvFileAttributes.h
  Cross-platform file object that contains detailed information about
  a file.
=====================================================================
*/

#pragma once

#include "FileAttributes.h"

SD_BEGIN
class FILE_API AdvFileAttributes : public FileAttributes
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Becomes true if this file is read only. */
	bool bReadOnly = false;

	/* Number of bytes this file contains. */
	unsigned long long FileSize = 0;

	/* Timestamp this file was last modified. */
	DateTime LastModifiedDate;

	/* Timestamp this file was created. */
	DateTime CreatedDate;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	AdvFileAttributes ();
	explicit AdvFileAttributes (const FilePtr& fileHandle);
	explicit AdvFileAttributes (const DString& relativeDirectory, const DString& fileName);
	explicit AdvFileAttributes (const Directory& dir, const DString& fileName);
	AdvFileAttributes (const FileAttributes& attributesCopy);
	AdvFileAttributes (const AdvFileAttributes& attributesCopy);
	virtual ~AdvFileAttributes ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual void ClearValues () override;

protected:
	virtual void PopulateFileInfo () override;
	virtual void PopulateFileInfoFromHandle (FilePtr handle) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetReadOnly () const;
	virtual Int GetFileSize () const;
	virtual unsigned long long GetFileSizeLarge () const;
	virtual DateTime GetLastModifiedDate () const;
	virtual DateTime GetCreatedDate () const;
};
SD_END