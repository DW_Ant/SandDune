/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileAttributes.h
  Cross-platform file object that essentially contains basic
  attributes about the OS's file.
=====================================================================
*/

#pragma once

#include "File.h"
#include "Directory.h"

SD_BEGIN
class AdvFileAttributes;

class FILE_API FileAttributes : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Name of the file without the extension. */
	DString FileName;

	/* Location this file resides. Path could either be
	relative to the engine's location or absolute.
	This should be converted to absolute path for faster performance though. */
	Directory Path;

	/* File name extension without the '.' character. */
	DString FileExtension;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	FileAttributes ();
	explicit FileAttributes (const FilePtr& fileHandle);
	explicit FileAttributes (const DString& path, const DString& fullFileName);
	explicit FileAttributes (const Directory& dir, const DString& fullFileName);
	FileAttributes (const FileAttributes& attributesCopy);
	FileAttributes (const AdvFileAttributes& attributesCopy);

	virtual ~FileAttributes ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	bool operator== (const FileAttributes& other) const;
	bool operator!= (const FileAttributes& other) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a string representation of this object.
	 */
	virtual DString GetName (bool includePath, bool includeExtension) const;

	/**
	 * Clears all property values for these attributes.
	 */
	virtual void ClearValues ();

	/**
	 * Updates attributes to describe the file that matches the newly specified address.
	 * Returns true if the file exists.
	 */
	virtual bool SetFileName (const DString& path, const DString& fullFileName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetFileName () const
	{
		return FileName;
	}

	inline const DString& ReadFileName () const
	{
		return FileName;
	}

	inline DString GetFileExtension () const
	{
		return FileExtension;
	}

	inline const DString& ReadFileExtension () const
	{
		return FileExtension;
	}

	inline Directory GetPath () const
	{
		return Path;
	}

	inline const Directory& ReadPath () const
	{
		return Path;
	}

	inline Directory& EditPath ()
	{
		return Path;
	}

	virtual bool GetFileExists () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the target directory to see if the file exists.  If the file exists, then the properties
	 * of this object will be filled accordingly.  This does not create a file.
	 * Returns true if the file exists.
	 */
	virtual void PopulateFileInfo ();

	/**
	 * Populates property values from the given file pointer.
	 */
	virtual void PopulateFileInfoFromHandle (FilePtr handle);

	/**
	 * Handle that's invoked when it failed to find a file.
	 */
	virtual void HandleMissingFile ();
};
SD_END