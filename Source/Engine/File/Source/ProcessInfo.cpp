/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ProcessInfo.cpp
=====================================================================
*/

#include "ProcessInfo.h"

SD_BEGIN
ProcessInfo::ProcessInfo () :
	ExecFile(DString::EmptyString, DString::EmptyString),
	Handle(nullptr),
	ProcessId(0),
	bHasHandles(false)
{
#ifdef PLATFORM_WINDOWS
	ZeroMemory(&OS_StartInfo, sizeof(OS_StartInfo));
	ZeroMemory(&OS_ProcessInfo, sizeof(OS_ProcessInfo));
#endif
}

ProcessInfo::~ProcessInfo ()
{
	ReleaseHandles();
}

bool ProcessInfo::IsFinished () const
{
	return !OS_IsProcessAlive(*this);
}

void ProcessInfo::ReleaseHandles ()
{
	if (bHasHandles)
	{
#ifdef PLATFORM_WINDOWS
		//Just in case the handles are open, close them all. According to MS docs, it's safe to close invalid handles. It'll just return a ERROR_INVALID_HANDLE error.
		CloseHandle(OS_StartInfo.hStdError);
		CloseHandle(OS_StartInfo.hStdInput);
		CloseHandle(OS_StartInfo.hStdOutput);

		CloseHandle(OS_ProcessInfo.hProcess);
		CloseHandle(OS_ProcessInfo.hThread);
#endif

		bHasHandles = false;
	}
}
SD_END