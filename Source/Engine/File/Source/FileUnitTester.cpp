/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileUnitTester.cpp
=====================================================================
*/

#include "AdvFileAttributes.h"
#include "BinaryFile.h"
#include "ComponentSerializer.h"
#include "ConfigWriter.h"
#include "Directory.h"
#include "File.h"
#include "FileAttributes.h"
#include "FileIterator.h"
#include "FileUnitTester.h"
#include "FileUtils.h"
#include "TextFileWriter.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::FileUnitTester, SD::UnitTester)
SD_BEGIN

const Directory FileUnitTester::UnitTestLocation(Directory::BASE_DIRECTORY.ReadDirectoryPath() + TXT("/Source/Engine/File/UnitTest/"), true);

bool FileUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestFileUtils(testFlags) && TestFileIterator(testFlags) && TestDirectories(testFlags) && TestOSCalls(testFlags) &&
			TestFileAttributes(testFlags) && TestFileWriter(testFlags) && TestConfig(testFlags) && TestBinaryFile(testFlags));
	}

	return true;
}

bool FileUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	if (!Super::MetRequirements(completedTests))
	{
		return false;
	}

	//The FileUnitTester depends on the TimeUnitTester
	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (completedTests.at(i) == TimeUnitTester::SStaticClass()->GetDefaultObject())
		{
			return true;
		}
	}

	//TimeUnitTester is not completed yet.  Try again later.
	return false;
}

bool FileUnitTester::TestFileUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Utils"));

	SetTestCategory(testFlags, TXT("ExtractFileName"));
	{
		DString fullAddress;
		DString expectedPath;
		DString expectedFile;
		std::function<bool()> testExtractFileName([&]()
		{
			//Replace hyphens with path separators
			fullAddress.ReplaceInline(TXT("-"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
			expectedPath.ReplaceInline(TXT("-"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
			expectedFile.ReplaceInline(TXT("-"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);

			DString actualPath;
			DString actualFile;
			FileUtils::ExtractFileName(fullAddress, OUT actualPath, OUT actualFile);

			if (expectedPath.Compare(actualPath, DString::CC_CaseSensitive) != 0 || expectedFile.Compare(actualFile, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("File Utils test failed. Extracting \"%s\" resulted in Path=\"%s\", File=\"%s\" when it should have been Path=\"%s\", File=\"%s\"."), fullAddress, actualPath, actualFile, expectedPath, expectedFile);
				return false;
			}

			return true;
		});

		fullAddress = TXT("D:-My Documents-My Text doc.txt");
		expectedPath = TXT("D:-My Documents-");
		expectedFile = TXT("My Text doc.txt");
		if (!testExtractFileName())
		{
			return false;
		}

		fullAddress = TXT("-Some-Relative-Path-..-AH-MyFile.ini");
		expectedPath = TXT("-Some-Relative-Path-..-AH-");
		expectedFile = TXT("MyFile.ini");
		if (!testExtractFileName())
		{
			return false;
		}

		fullAddress = TXT("-SomeExe.exe");
		expectedPath = TXT("-");
		expectedFile = TXT("SomeExe.exe");
		if (!testExtractFileName())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("ExtractFileExtension"));
	{
		DString fullFileName;
		DString expectedName;
		DString expectedExtension;

		std::function<bool()> testExtraction([&]()
		{
			DString actualName;
			DString actualExtension;
			FileUtils::ExtractFileExtension(fullFileName, OUT actualName, OUT actualExtension);

			if (expectedName.Compare(actualName, DString::CC_CaseSensitive) != 0 && expectedExtension.Compare(actualExtension, DString::CC_CaseSensitive) != 0)
			{
				UnitTestError(testFlags, TXT("File Utils test failed. When extracting file name and extension from \"%s\", the expected should have been Name=\"%s\" Extension=\"%s\". Instead it's Name=\"%s\" Extension=\"%s\"."), fullFileName, expectedName, expectedExtension, actualName, actualExtension);
				return false;
			}

			return true;
		});

		fullFileName = TXT("MyDoc.txt");
		expectedName = TXT("MyDoc");
		expectedExtension = TXT("txt");
		if (!testExtraction())
		{
			return false;
		}

		fullFileName = TXT("My File With Spaces.ini");
		expectedName = TXT("My File With Spaces");
		expectedExtension = TXT("ini");
		if (!testExtraction())
		{
			return false;
		}

		fullFileName = TXT("FileWithoutExtension");
		expectedName = fullFileName;
		expectedExtension = DString::EmptyString;
		if (!testExtraction())
		{
			return false;
		}

		fullFileName = TXT("MyTexture.txtr.ini");
		expectedName = TXT("MyTexture");
		expectedExtension = TXT("txtr.ini");
		if (!testExtraction())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("File Utils"));
	return true;
}

bool FileUnitTester::TestDirectories (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Directories"));

	TestLog(testFlags, TXT("The engine's base directory is:  %s."), Directory::BASE_DIRECTORY);
	TestLog(testFlags, TXT("The engine's binary directory is:  %s."), Directory::BINARY_DIRECTORY);
	TestLog(testFlags, TXT("The engine's document directory is:  %s."), Directory::DOCUMENT_DIRECTORY);
	TestLog(testFlags, TXT("The engine's source directory is:  %s."), Directory::SOURCE_DIRECTORY);
	TestLog(testFlags, TXT("The current working directory is:  %s."), Directory::GetWorkingDirectory());

	SetTestCategory(testFlags, TXT("Fundamental Operations"));
	{
		const DString subFolder(TXT("TestSubDir"));
		Directory testDir(subFolder, true);
		TestLog(testFlags, TXT("Adding a subfolder %s relative from the current working directory.  Resulting directory object is:  %s"), subFolder);
		testDir.ConvertToAbsPath();
		TestLog(testFlags, TXT("Converting the directory to absolute path:  %s"), testDir);
		if (testDir.ToString().Find(Directory::GetWorkingDirectory().ReadDirectoryPath(), 0, DString::CC_CaseSensitive) < 0)
		{
			UnitTestError(testFlags, TXT("Appending subfolder to current working directory failed since the current working directory is not found in resulting directory object converted to abs path:  %s"), testDir);
			return false;
		}

		Directory copyDir(testDir);
		if (copyDir != testDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed since creating a copy of %s does not create a duplicate object.  The directory copy is %s"), testDir, copyDir);
			return false;
		}

		Directory workingDir(Directory::GetWorkingDirectory());
		workingDir.ConvertToAbsPath();
		if (testDir == Directory(Directory::GetWorkingDirectory()))
		{
			UnitTestError(testFlags, TXT("Directory test failed since the working directory + subfolder (%s) is the same as simply the working directory (%s)."), testDir, workingDir);
			return false;
		}

		Directory absPathTest(TXT("SubDir/InnerMostDir"), true);
		if (absPathTest.IsAbsPath())
		{
			UnitTestError(testFlags, TXT("Directory test failed since it believes %s is an absolute path."), absPathTest);
			return false;
		}

		absPathTest.ConvertToAbsPath();
		if (!absPathTest.IsAbsPath())
		{
			UnitTestError(testFlags, TXT("Directory test failed since it believes %s is a relative path."), absPathTest);
			return false;
		}

		TestLog(testFlags, TXT("Retrieving inner-most directory."));
		const DString expectedInnerDir(TXT("InnerDir"));
		Directory innerDirTest(DString(TXT("SomeDir")) / TXT("MiddleDir") / expectedInnerDir);
		if (innerDirTest.GetInnerMostDirectory() != expectedInnerDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed since getting the inner most directory of %s returned %s instead of %s."), innerDirTest, innerDirTest.GetInnerMostDirectory(), expectedInnerDir);
			return false;
		}

		TestLog(testFlags, TXT("Retrieving parent directory."));
		Directory expectedParent = Directory::BASE_DIRECTORY;
		Directory actualParent;
		if (!Directory::SOURCE_DIRECTORY.GetParentDir(OUT actualParent))
		{
			UnitTestError(testFlags, TXT("Directory test failed since Directory::GetParentDir is unable to obtain the parent directory of %s."), Directory::SOURCE_DIRECTORY);
			return false;
		}

		if (actualParent != expectedParent)
		{
			UnitTestError(testFlags, TXT("Directory test failed since retrieving the parent directory of %s returned %s instead of %s."), Directory::SOURCE_DIRECTORY, actualParent, expectedParent);
			return false;
		}

#ifdef PLATFORM_WINDOWS
		TestLog(testFlags, TXT("Retrieving drive letter."));
		Directory driveLetterTest(DString(TXT("D:")) / TXT("BaseDir") / TXT("SubDir") / TXT("InnerDir"));
		const DString expectedDriveLetter = TXT("D");

		if (driveLetterTest.GetDriveLetter() != expectedDriveLetter)
		{
			UnitTestError(testFlags, TXT("Directory test failed since it was not able to properly pull the drive letter from %s.  It pulled %s instead of %s."), driveLetterTest, driveLetterTest.GetDriveLetter(), expectedDriveLetter);
			return false;
		}
#endif
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Interface to Vectors"));
	{
		const std::vector<DString> folderList({TXT("Base Dir"), TXT("Outer Dir"), TXT("Branch Dir"), TXT(".."), TXT("Some other Dir"), TXT("Inner Dir")});
		Directory appendedDir(folderList.at(0), true);
		DString expectedList = folderList.at(0) + Directory::DIRECTORY_SEPARATOR;
		for (UINT_TYPE i = 1; i < folderList.size(); ++i)
		{
			TestLog(testFlags, TXT("Appending %s to %s"), folderList.at(i), appendedDir);

			appendedDir /= folderList.at(i);
			expectedList += folderList.at(i) + Directory::DIRECTORY_SEPARATOR;

			if (appendedDir.ToString() != expectedList)
			{
				UnitTestError(testFlags, TXT("Directory test failed since appending %s to the directory list should have resulted in %s.  Instead it's %s."), folderList.at(i), expectedList, appendedDir);
				return false;
			}
		}

		TestLog(testFlags, TXT("Resulting directory list is %s"), appendedDir.ToString());
		std::vector<DString> convertedDirList;
		std::vector<DString> expectedDirList;
		expectedList.ParseString(*Directory::DIRECTORY_SEPARATOR.ToCString(), expectedDirList, true);
		convertedDirList = appendedDir.SplitDirectoryToList();
		if (expectedDirList.size() != folderList.size() || convertedDirList.size() != expectedDirList.size())
		{
			UnitTestError(testFlags, TXT("Directory to vector of strings conversion error.  There's a mismatch in vector sizes.  Original list size (%s), expected list size (%s), converted list size (%s)."), DString::MakeString(folderList.size()), DString::MakeString(expectedDirList.size()), DString::MakeString(convertedDirList.size()));
			return false;
		}

		for (UINT_TYPE i = 0; i < expectedDirList.size(); ++i)
		{
			if (expectedDirList.at(i) != folderList.at(i))
			{
				UnitTestError(testFlags, TXT("Directory test failed since the expected folder list is different from the original folder list.  At index %s, the original folder name is \"%s\" but it's expecting \"%s\"."), DString::MakeString(i), folderList.at(i), expectedDirList.at(i));
				return false;
			}

			if (expectedDirList.at(i) != convertedDirList.at(i))
			{
				UnitTestError(testFlags, TXT("Directory test failed since the expected folder list is different from the converted folder list.  At index %s, the expected folder name is \"%s\" but the converted folder name is \"%s\"."), DString::MakeString(i), expectedDirList.at(i), convertedDirList.at(i));
				return false;
			}
		}

		TestLog(testFlags, TXT("Successfully converted Directory object (%s) to a list of strings (%s)."), appendedDir, DString::ListToString(convertedDirList));
		Directory convertedDir = Directory::ListToDirectory(convertedDirList);
		TestLog(testFlags, TXT("Converting list (%s) back to Directory object (%s)."), DString::ListToString(convertedDirList), convertedDir);
		if (convertedDir != appendedDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed.  After converting directory (%s) to a list and converting the list back to a directory object, the directory object (%s) is not the same as the original."), appendedDir, convertedDir);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Directory Streaming"));
	{
		Directory dir1(TXT("RelativeDir"), true);
		dir1 /= TXT("SubDir");

		Directory dir2(TXT("SomeAbsPath"), true);
		dir2.ConvertToAbsPath();

		Directory dir3(dir2);
		dir3 /= dir1 / TXT("SubDirWithinAbsPath");

		DataBuffer directoryBuffer;

		directoryBuffer << dir1;
		directoryBuffer << dir2;
		directoryBuffer << dir3;

		Directory readDir1;
		Directory readDir2;
		Directory readDir3;
		directoryBuffer >> readDir1;
		directoryBuffer >> readDir2;
		directoryBuffer >> readDir3;

		if (dir1 != readDir1)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), dir1, readDir1);
			return false;
		}

		if (dir2 != readDir2)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), dir2, readDir2);
			return false;
		}

		if (dir3 != readDir3)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), dir3, readDir3);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Directory expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Directory actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Directory test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		testStr = TXT("D:\\MyTestDir\\SubDir\\");
		expected = Directory(testStr, false);
		if (!testParsing())
		{
			return false;
		}

		testStr = TXT("\\..\\SomeRel\\Path\\");
		expected = Directory(testStr, false);
		if (!testParsing())
		{
			return false;
		}
		
		testStr = TXT("\\Path\\To/Sub\\Dir/");
		expected = Directory(testStr, false);
		if (!testParsing())
		{
			return false;
		}

		expected = Directory::BINARY_DIRECTORY;
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Directory Sanitation"));
	{
		DString input = TXT("\\Something/else\\Testing Directory/Slashes");
		Directory testDir(input, true);
		DString expected = TXT("%sSomething%selse%sTesting Directory%sSlashes%s");
		expected.ReplaceInline(TXT("%s"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directory characters are not normalized to use %s.  Expected:  %s  |  Received:  %s"), Directory::DIRECTORY_SEPARATOR, expected, testDir);
			return false;
		}

		input = TXT("/BaseDir:\\Something.Else/Some Other @ Dir : With \"Bad Chars\" \\/..\\/ Unreal > VBS \\ VBS < Unity / Pipe | Star * Confusion ??\\Inner most directory.WithPeriod");
		testDir.SetDirectory(input);
#ifdef PLATFORM_WINDOWS
		expected = TXT("%sBaseDir%sSomething.Else%sSome Other @ Dir  With Bad Chars %s..%s Unreal  VBS %s VBS  Unity %s Pipe  Star  Confusion %sInner most directory.WithPeriod%s");
#else
		expected = TXT("%sBaseDir:%sSomething.Else%sSome Other @ Dir : With \"Bad Chars\" %s..%s Unreal > VBS %s VBS < Unity %s Pipe | Star * Confusion ??%sInner most directory.WithPeriod%s");
#endif
		expected.ReplaceInline(TXT("%s"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directories with invalid characters do not match expected format.  Expected:  %s  |  Received:  %s"), expected, testDir);
			return false;
		}

		//Ensure Window's absolute path format is fine
		input = TXT("D:") + input;
		expected = TXT("D:") + expected;
		testDir.SetDirectory(input);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directories with invalid characters do not match expected format.  Expected:  %s  |  Received:  %s"), expected, testDir);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Directories"));
	return true;
}

bool FileUnitTester::TestOSCalls (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File OS Calls"));

	SetTestCategory(testFlags, TXT("Relative to Absolute Path"));
	TestLog(testFlags, TXT("Testing conversion from relative path to absolute path."));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	TestLog(testFlags, TXT("Testing if system can check if a file exists or not."));
	if (!OS_CheckIfFileExists(FileAttributes(absUnitTestLocation, TXT("FileA"))))
	{
		UnitTestError(testFlags, TXT("Check if file test failed.  Could not find FileA within %s.  FileA should exist within that location."), absUnitTestLocation);
		return false;
	}
	TestLog(testFlags, TXT("Unit test was able find the file."));

	DString nonExistingFile(TXT("FileUnitTesterExpectsThisFileNotToExist"));
	TestLog(testFlags, TXT("Testing if system does not get a handle to a nonexisting file:  %s%s"), UnitTestLocation, nonExistingFile);
	if (OS_CheckIfFileExists(FileAttributes(absUnitTestLocation, nonExistingFile)))
	{
		UnitTestError(testFlags, TXT("Check if file does not exist test failed.  Unit test obtained a handle to a file that should not exist:  %s%s"), absUnitTestLocation, nonExistingFile);
		return false;
	}
	TestLog(testFlags, TXT("Unit test could not find a non existing file [this is good!]."));

	TestLog(testFlags, TXT("Testing if system could get a file handle."));
	FilePtr fileHandle = OS_GetFileHandle(FileAttributes(absUnitTestLocation, TXT("FileA")));
	if (fileHandle == nullptr)
	{
		UnitTestError(testFlags, TXT("File handle test failed.  Could not obtain a handle to %sFileA."), absUnitTestLocation);
		return false;
	}

	DString handlePath;
	DString handleName;
	OS_GetFileName(fileHandle, handlePath, handleName);
	if (handlePath.IsEmpty())
	{
		OS_CloseFile(fileHandle);
		UnitTestError(testFlags, TXT("File handle test failed.   Handle path to the file 'FileA' is empty."));
		return false;
	}

	if (handleName != TXT("FileA"))
	{
		OS_CloseFile(fileHandle);
		UnitTestError(testFlags, TXT("File handle test failed.  Unable to get file name from handle.  Found name is:  ") + handleName + TXT(".  The expected value is:  FileA."));
		return false;
	}

	OS_CloseFile(fileHandle);
	if (fileHandle != nullptr)
	{
		UnitTestError(testFlags, TXT("File handle test failed.  Was not able to close the file handle to FileA"));
		return false;
	}
	TestLog(testFlags, TXT("System was able to obtain File handles!"));

	SetTestCategory(testFlags, TXT("Check if Directory Exists"));
	{
		Directory testDir;
		bool bExpectedExists;
		std::function<bool()> testDirExist([&]()
		{
			bool bActual = OS_CheckIfDirectoryExists(testDir);
			if (bActual != bExpectedExists)
			{
				DString errorStr = (bExpectedExists) ? DString::EmptyString : TXT("NOT ");
				UnitTestError(testFlags, TXT("Directory test failed. %s should %sexist. OS_CheckIfDirectoryExists suggests otherwise."), testDir, errorStr);
				return false;
			}

			return true;
		});

		testDir = Directory::BASE_DIRECTORY;
		bExpectedExists = true;
		if (!testDirExist())
		{
			return false;
		}

		testDir = Directory::BINARY_DIRECTORY;
		bExpectedExists = true;
		if (!testDirExist())
		{
			return false;
		}

		testDir = UnitTestLocation / TXT("This Directory should not exist") / TXT("Otherwise the file unit test") / TXT("will fail");
		bExpectedExists = false;
		if (!testDirExist())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Create and Delete Directory"));
	{
		Directory outerDir(UnitTestLocation / TXT("CreateDirTest"));
		Directory innerDir(outerDir / TXT("SubDir"));
		if (OS_CheckIfDirectoryExists(outerDir))
		{
			if (!OS_DeleteDirectory(outerDir))
			{
				UnitTestError(testFlags, TXT("OS Calls test failed. Failed to delete directory: %s."), outerDir);
				return false;
			}
		}

		if (!OS_CreateDirectory(innerDir)) //Should automatically create outerDir
		{
			UnitTestError(testFlags, TXT("OS Calls test failed. Failed to create directory: %s."), innerDir);
			return false;
		}

		if (!OS_DeleteDirectory(outerDir)) //Should automatically delete innerDir
		{
			UnitTestError(testFlags, TXT("OS Calls test failed. Failed to delete directory: %s."), outerDir);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	TestLog(testFlags, TXT("Testing file size."));
	Int fileSize = static_cast<UINT_TYPE>(OS_GetFileSize(FileAttributes(absUnitTestLocation, TXT("FileA"))));
	if (fileSize != 75)
	{
		UnitTestError(testFlags, TXT("File size test failed.  File size is: %s bytes.  The expected value is:  75 bytes."), fileSize);
		return false;
	}
	TestLog(testFlags, TXT("The file size of FileA is ") + fileSize.ToString() + TXT(" bytes."));

#if 0
	//Disabled this test since source control may flip this flag.
	TestLog(testFlags, TXT("Testing obtaining Read Only attribute."));
	FileAttributes testFile(absUnitTestLocation / TXT("SubDirectoryTest"), TXT("SubFileB"));
	if (!OS_IsFileReadOnly(testFile))
	{
		UnitTestError(testFlags, TXT("Read only test failed.  SubFileB within %s should be read only."), testFile.GetName(true, true));
		return false;
	}
	TestLog(testFlags, TXT("System was able to check the Read Only attribute."));
#endif

	TestLog(testFlags, TXT("Testing file timestamps."));
	DateTime createdDate;
	DateTime modifiedDate;
	DateTime minDate;
	minDate.Year = 2015;
	OS_GetFileTimestamps(FileAttributes(absUnitTestLocation, TXT("FileA")), createdDate, modifiedDate);
	TestLog(testFlags, TXT("Created date for FileA is:  ") + createdDate.ToIsoFormat() + TXT("."));
	TestLog(testFlags, TXT("Last modified date for FileA is:  ") + modifiedDate.ToIsoFormat() + TXT("."));
	if (createdDate == DateTime() || modifiedDate == DateTime())
	{
		UnitTestError(testFlags, TXT("Timestamp test failed.  Either created date [") + createdDate.ToIsoFormat() + TXT("] or last modified date [") + modifiedDate.ToIsoFormat() + TXT("] is empty."));
		return false;
	}

	if (createdDate.Year < minDate.Year || modifiedDate.Year < minDate.Year)
	{
		UnitTestError(testFlags, TXT("Timestamp test failed.  The created date [") + createdDate.ToIsoFormat() + TXT("] and the last modified date [") + modifiedDate.ToIsoFormat() + TXT("] must be later than ") + minDate.ToIsoFormat() + TXT("."));
		return false;
	}

	TestLog(testFlags, TXT("Testing file creation."));
	DString tempFileName = TXT("TemporaryFileTest");
	FilePtr createdFile = OS_CreateFile(absUnitTestLocation, tempFileName);
	if (createdFile == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to create file named:  %s.  Make sure the file does not already exist."), absUnitTestLocation.ToString() + tempFileName);
		return false;
	}
	TestLog(testFlags, TXT("System was able to create files."));

	TestLog(testFlags, TXT("Testing file writing."));
	const TCHAR* dataBuffer = TXT("The FileUnitTester wrote this data to this file.\n");
	int bufferSize = static_cast<int>(strlen(dataBuffer));
	if (!OS_WriteToFile(createdFile, dataBuffer, bufferSize))
	{
		OS_CloseFile(createdFile);
		UnitTestError(testFlags, TXT("Failed to write %s to %s.  Make sure application has write access to this file."), DString(dataBuffer), tempFileName);
		UnitTestError(testFlags, TXT("You'll need to manually delete %s since the unit test will be attempting to create a new file with the same name."), absUnitTestLocation.ToString() + tempFileName);
		return false;
	}
	TestLog(testFlags, TXT("Write to file test passed!"));
	OS_CloseFile(createdFile);

	TestLog(testFlags, TXT("Testing file copy."));
	DString fileCopy = tempFileName + TXT("_Copy");
	FileAttributes tempFile(absUnitTestLocation, tempFileName);
	if (!OS_CopyFile(tempFile, absUnitTestLocation, fileCopy, true))
	{
		UnitTestError(testFlags, TXT("File copy test failed.  Failed to copy %s to %s."), tempFileName, fileCopy);
		return false;
	}

	TestLog(testFlags, TXT("Testing file deletion."));
	if (!OS_DeleteFile(tempFile))
	{
		UnitTestError(testFlags, TXT("Failed to delete %s.  Make sure application has write access to this file."), tempFile.GetName(true, true));
		TestLog(testFlags, TXT("You'll need to manually delete this file since the unt test will be attempting to create a new file with the same name."));
		return false;
	}

	if (!OS_DeleteFile(FileAttributes(absUnitTestLocation, fileCopy)))
	{
		UnitTestError(testFlags, TXT("Failed to delete %s%s.  Make sure application has write access to this file."), absUnitTestLocation, fileCopy);
		//No need to notify user to manually delete fileCopy since this file will be replaced next time this unit tester runs.
		return false;
	}
	TestLog(testFlags, TXT("System was able to delete files."));

	ExecuteSuccessSequence(testFlags, TXT("File OS Calls"));
	return true;
}

bool FileUnitTester::TestFileAttributes (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Attributes"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	TestLog(testFlags, TXT("Constructing File Attributes from handle."));
	FilePtr fileHandle = OS_GetFileHandle(absUnitTestLocation.ReadDirectoryPath(), TXT("FileA"));
	if (fileHandle == nullptr)
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  Unable to get a handle to FileA"));
		return false;
	}

	AdvFileAttributes attributes(fileHandle);
	OS_CloseFile(fileHandle);
	if (attributes.GetFileName() != TXT("FileA"))
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes file name (%s) is not equal to FileA."), attributes.GetFileName());
		return false;
	}

	if (attributes.GetFileSize() != 75)
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes describing FileA is %s bytes.  The expected size is 75 bytes."), attributes.GetFileSize());
		return false;
	}

	TestLog(testFlags, TXT("Constructing File Attributes from string."));
	AdvFileAttributes stringAttributes(UnitTestLocation, TXT("FileA"));
	if (!stringAttributes.GetFileExists())
	{
		UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  Unable to locate FileA from %s."), absUnitTestLocation);
		return false;
	}

	TestLog(testFlags, TXT("The created date for FileA is %s.  The last modified date is %s."), stringAttributes.GetCreatedDate().ToIsoFormat(), stringAttributes.GetLastModifiedDate().ToIsoFormat());
	if (stringAttributes.GetCreatedDate().Year < 2015 || stringAttributes.GetLastModifiedDate().Year < 2015)
	{
		UnitTestError(testFlags, TXT("Unable to obtain correct created date for FileA.  The year should at least be 2015.  The created date is [%s]."), stringAttributes.GetCreatedDate().ToIsoFormat());
		TestLog(testFlags, TXT("    The last modified date for FileA reported [%s]."), stringAttributes.GetLastModifiedDate().ToIsoFormat());
		return false;
	}

	if (stringAttributes.GetFileSize() != 75)
	{
		UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  The attributes describing FileA suggests it's %s bytes.  The expected file size is 75 bytes."), stringAttributes.GetFileSize());
		return false;
	}

	FileAttributes fileACopy(stringAttributes);
	if (fileACopy != stringAttributes)
	{
		UnitTestError(testFlags, TXT("After constructing a file attributes using a copy constructor, the file attributes %s is still not equal to %s."), fileACopy, stringAttributes);
		return false;
	}

	//Change the extension
	fileACopy.SetFileName(fileACopy.GetPath().GetDirectoryPath(), fileACopy.GetFileName() + TXT(".jnk"));
	if (fileACopy.GetFileExists())
	{
		UnitTestError(testFlags, TXT("After renaming the file attributes to an unknown extension, the file attributes still believes the file exists:  %s"), fileACopy);
		return false;
	}

	if (fileACopy == stringAttributes)
	{
		UnitTestError(testFlags, TXT("After changing the file extension, a file attributes comparison believes that %s is equal to %s."), fileACopy, stringAttributes);
		return false;
	}

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		AdvFileAttributes attr(absUnitTestLocation, TXT("FileA"));
		FileAttributes primAttr(absUnitTestLocation, TXT("ConfigTest.ini"));

		//Reversing file attribute classes is intended.  The classes should be agnostic to whichever version was saved to buffer.
		FileAttributes readAttr;
		AdvFileAttributes readPrimAttr;

		DataBuffer fileDataBuffer;

		fileDataBuffer << attr;
		fileDataBuffer << primAttr;
		fileDataBuffer >> readAttr;
		fileDataBuffer >> readPrimAttr;

		if (attr != readAttr)
		{
			UnitTestError(testFlags, TXT("File streaming test failed.  After pushing %s to a data buffer, %s was pulled from that data buffer."), attr, readAttr);
			return false;
		}

		if (primAttr != readPrimAttr)
		{
			UnitTestError(testFlags, TXT("File streaming test failed.  After pushing the second file attributes %s to a data buffer, %s was pulled from that data buffer."), primAttr, readPrimAttr);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		FileAttributes expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			FileAttributes actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("File Attributes test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = FileAttributes(Directory(TXT("\\Rel\\Path\\"), false), TXT("TestFile.txt"));
		testStr = TXT("Path=\"\\Rel\\Path\\\", FileName=\"TestFile\", FileExtension=\"txt\"");
		if (!testParsing())
		{
			return false;
		}

		expected = FileAttributes(Directory(TXT("C:\\Some\\Abs\\Path\\"), false), TXT("CoolPic.jpg"));
		testStr = TXT("FileName = \"CoolPic\", Path = \"C:\\Some\\Abs\\Path\\\", FileExtension = \"jpg\"");
		if (!testParsing())
		{
			return false;
		}

		expected = FileAttributes(Directory(TXT("..\\..\\Some\\Parent\\Dir\\"), false), TXT("Content.bin"));
		testStr = TXT("FileExtension=\"bin\", FileName=\"Content\", Path=\"..\\..\\Some\\Parent\\Dir\\\"");
		if (!testParsing())
		{
			return false;
		}

		expected = FileAttributes(absUnitTestLocation, TXT("ReadMe.txt"));
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("File Attributes"));
	return true;
}

bool FileUnitTester::TestFileIterator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Iterator"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	//List all files within the unit test directory that are expected to be captured from the file iterator.
	std::vector<DString> expectedFileNames;
	expectedFileNames.push_back(TXT("FileA"));
	expectedFileNames.push_back(TXT("ReadMe.txt"));
	expectedFileNames.push_back(TXT("SubFileA"));
	expectedFileNames.push_back(TXT("SubFileB"));
	Int fileCounter = 0;

	for (FileIterator fileIter(absUnitTestLocation, FileIterator::SF_IncludeEverything); !fileIter.FinishedIterating(); fileIter++)
	{
		if (fileIter.IsDirectorySelected())
		{
			TestLog(testFlags, TXT("File iterator found a directory named:  %s"), fileIter.GetSelectedDirectory());
			continue;
		}

		DString curFileName = fileIter.GetSelectedAttributes().GetName(false, true);
		TestLog(testFlags, TXT("File iterator found an file named:  %s"), curFileName);
		fileCounter++;

		for (UINT_TYPE i = 0; i < expectedFileNames.size(); i++)
		{
			if (expectedFileNames.at(i) == curFileName)
			{
				expectedFileNames.erase(expectedFileNames.begin() + i);
				break;
			}
		}
	}

	if (expectedFileNames.size() > 0)
	{
		UnitTestError(testFlags, TXT("File iterator test failed.  Not all expected file names were found.  See the logs for details."));

		TestLog(testFlags, TXT("The file iterator did not find the following files within the %s directory. . ."), absUnitTestLocation.ReadDirectoryPath());
		for (UINT_TYPE i = 0; i < expectedFileNames.size(); i++)
		{
			TestLog(testFlags, TXT("    ") + expectedFileNames.at(i));
		}

		return false;
	}

	TestLog(testFlags, TXT("The file iterator found a total of %s files."), fileCounter);

	//Run case where the file iterator shouldn't find anything
	for (FileIterator iter(absUnitTestLocation, FileIterator::SF_None); !iter.FinishedIterating(); ++iter)
	{
		UnitTestError(testFlags, TXT("File iterator test failed. The file iterator found %s even though the iterator's search flags is none."), iter.GetSelectedAttributes());
		return false;
	}

	//Run case where the file iterator should only find directories
	for (FileIterator iter(absUnitTestLocation, FileIterator::SF_IncludeDirectories | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		if (!iter.IsDirectorySelected())
		{
			UnitTestError(testFlags, TXT("File iterator test failed. The file iterator found %s even though the iterator flags forbids matching files."), iter.GetSelectedAttributes());
			return false;
		}
	}

	//Run case where the file iterator should only find files
	for (FileIterator iter(absUnitTestLocation, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		if (iter.IsDirectorySelected())
		{
			UnitTestError(testFlags, TXT("File iterator test failed. The file iterator found %s even though the iterator flags forbids matching directories."), iter.GetSelectedAttributes());
			return false;
		}
	}

	ExecuteSuccessSequence(testFlags, TXT("File Iterator"));
	return true;
}

bool FileUnitTester::TestFileWriter (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Writer"));

	DString fullFileName = UnitTestLocation.ReadDirectoryPath() + TXT("FileWriterTest.txt");
	TextFileWriter* testWriter = TextFileWriter::CreateObject();
	if (testWriter == nullptr)
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Unable to instantiate a TextFileWriter."));
		return false;
	}

	if (!testWriter->OpenFile(fullFileName, true))
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Failed to open file named:  %s"), fullFileName);
		return false;
	}

	std::vector<DString> contentToWrite;
	contentToWrite.push_back(TXT("First Line"));
	contentToWrite.push_back(TXT("Second Line"));
	contentToWrite.push_back(TXT("Third Line"));
	contentToWrite.push_back(TXT(""));
	contentToWrite.push_back(TXT("Last Line"));

	for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
	{
		TestLog(testFlags, TXT("Writing %s to the file."), contentToWrite.at(i));
		testWriter->AddTextEntry(contentToWrite.at(i));
	}

	if (!testWriter->WriteToFile())
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Failed to flush buffer to the file."));
		return false;
	}

	TestLog(testFlags, TXT("Finished writing to file.  Now reading all text from %s"), fullFileName);
	DString currentLine;
	testWriter->ResetSeekPosition();
	while(testWriter->ReadLine(currentLine))
	{
		TestLog(testFlags, TXT("Recalled \"%s\" from text file."), currentLine);

		for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
		{
			if (contentToWrite.at(i) == currentLine)
			{
				contentToWrite.erase(contentToWrite.begin() + i);
				break;
			}
		}
	}

	TestLog(testFlags, TXT("Finished reading from file."));

	if (contentToWrite.size() > 0)
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Some of the written text was not recalled when reading from file.  See the logs for details."));
		TestLog(testFlags, TXT("File writer test failed.  The following written text was not recalled when reading file."));
		for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
		{
			TestLog(testFlags, TXT("    %s"), contentToWrite.at(i));
		}

		return false;
	}

	TestLog(testFlags, TXT("Deleting file writer test file."));
	testWriter->Destroy(); //closes file

	DString path;
	DString fileName;
	FileUtils::ExtractFileName(fullFileName, OUT path, OUT fileName);
	if (!OS_DeleteFile(FileAttributes(path, fileName)))
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Unable to clean up resources.  Attempted to delete file using %s"), fullFileName);
		return false;
	}

	ExecuteSuccessSequence(testFlags, TXT("File Writer"));
	return true;
}

bool FileUnitTester::TestConfig (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Config"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	DString fileName = absUnitTestLocation.ReadDirectoryPath() + TXT("ConfigTest.ini");
	ConfigWriter* testConfig = ConfigWriter::CreateObject();
	if (testConfig == nullptr)
	{
		UnitTestError(testFlags, TXT("Config test failed.  Unable to instantiate a ConfigWriter."));
		return false;
	}

	if (!OS_CheckIfFileExists(FileAttributes(absUnitTestLocation, TXT("ConfigTest.ini"))))
	{
		//File doesn't exist, restore file using backup
		TestLog(testFlags, TXT("Unable to find ConfigTest.ini.  Attempting to restore unit test config file by copying its backup file."));
		OS_CopyFile(FileAttributes(absUnitTestLocation, TEXT("ConfigTestBackup.ini")), absUnitTestLocation, TEXT("ConfigTest.ini"));

		if (!OS_CheckIfFileExists(FileAttributes(absUnitTestLocation, TXT("ConfigTest.ini"))))
		{
			UnitTestError(testFlags, TXT("Config test failed.  Failed to restore missing file named %s."), fileName);
			testConfig->Destroy();
			return false;
		}
	}

	FileAttributes configAttr(absUnitTestLocation, TXT("ConfigTest.ini"));
	if (!testConfig->OpenFile(configAttr, false))
	{
		UnitTestError(testFlags, TXT("Config test failed.  Failed to open file named:  %s"), fileName);
		testConfig->Destroy();
		return false;
	}

	DString unitTestSection = TXT("UnitTest");
	DateTime current;
	current.SetToCurrentTime();
	DString headerTestFormat(TXT("The current day is %s!"));
	TestLog(testFlags, TXT("Begin reading from config test."));
	{
		if (!testConfig->ContainsSection(unitTestSection))
		{
			UnitTestError(testFlags, TXT("Config test failed.  The test config writer does not have the %s section defined."), unitTestSection);
			return false;
		}

		DString falseSectionName = TXT("FalseSectionName");
		if (testConfig->ContainsSection(falseSectionName))
		{
			UnitTestError(testFlags, TXT("Config test failed.  The test config contains a section named %s when it should not have that section."), falseSectionName);
			return false;
		}

		DString testProperty = TXT("FullTimeStamp");
		if (!testConfig->ContainsProperty(unitTestSection, testProperty))
		{
			UnitTestError(testFlags, TXT("Config test failed. The test config test failed. The file %s does not contain the property %s within section %s."), fileName, testProperty, unitTestSection);
			return false;
		}

		testProperty = TXT("FakeProperty");
		if (testConfig->ContainsProperty(unitTestSection, testProperty))
		{
			UnitTestError(testFlags, TXT("Config test failed. The test config test failed. The file %s contains the property %s within section %s. This property should not exist."), fileName, testProperty, unitTestSection);
			return false;
		}

		DString oldTimeStampStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
		if (oldTimeStampStr.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Config test failed.  Unable to get FullTimeStamp property from %s section within %s file."), unitTestSection, fileName);
			testConfig->Destroy();
			return false;
		}

		//Retrieve the same variable without specifying a section
		DString oldTimeStampStrNoSection = testConfig->GetProperty<DString>(TXT(""), TXT("FullTimeStamp"));
		if (oldTimeStampStrNoSection == oldTimeStampStr)
		{
			UnitTestError(testFlags, TXT("Config test failed.  Retrieving the FullTimeStamp property without specifying a section within %s file should have failed to retrieve that property."), fileName);
			testConfig->Destroy();
			return false;
		}

		//Retrieving a property value that's not within a section
		DString sectionlessValue = testConfig->GetProperty<DString>(TXT(""), TXT("PropertyWithoutSection"));
		if (sectionlessValue != TXT("Some Value"))
		{
			UnitTestError(testFlags, TXT("Config test failed.  The expected value of PropertyWithoutSection is \"Some Value\" from config file %s.  Instead it returned \"%s\"."), fileName, sectionlessValue);
			testConfig->Destroy();
			return false;
		}
		TestLog(testFlags, TXT("Successfully extracted FullTimeStamp from %s section.  The value is:  %s"), unitTestSection, oldTimeStampStr);

		DateTime oldTimeStamp;
		oldTimeStamp.Year = testConfig->GetProperty<Int>(unitTestSection, TXT("Year"));
		oldTimeStamp.Month = testConfig->GetProperty<Int>(unitTestSection, TXT("Month"));
		oldTimeStamp.Day = testConfig->GetProperty<Int>(unitTestSection, TXT("Day"));
		oldTimeStamp.Hour = testConfig->GetProperty<Int>(unitTestSection, TXT("Hour"));
		oldTimeStamp.Minute = testConfig->GetProperty<Int>(unitTestSection, TXT("Minute"));
		oldTimeStamp.Second = testConfig->GetProperty<Int>(unitTestSection, TXT("Second"));
		TestLog(testFlags, TXT("Extracted the following Ints from Config:  Year=%s, Month=%s, Day=%s, Hour=%s, Minute=%s, Second=%s."), oldTimeStamp.Year, oldTimeStamp.Month, oldTimeStamp.Day, oldTimeStamp.Hour, oldTimeStamp.Minute, oldTimeStamp.Second);
		if (oldTimeStamp.CalculateTotalSeconds() <= 0.f)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The time stamp from %s is not positive (%s seconds).  The unit test should have been able to extract the previous unit time's time stamp."), oldTimeStamp.ToIsoFormat(), oldTimeStamp.CalculateTotalSeconds());
			testConfig->Destroy();
			return false;
		}

		if (oldTimeStamp.ToIsoFormat() != oldTimeStampStr)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The components of the time stamp in config should have been equal to the string representation.  Instead the components string representation is %s.  The string representation in config is %s."), oldTimeStamp.ToIsoFormat(), oldTimeStampStr);
			testConfig->Destroy();
			return false;
		}
		TestLog(testFlags, TXT("Reading variables from config successful.  The components of the config's time stamp's string representation is equal to the string representation in config.  %s"), oldTimeStamp.ToIsoFormat());

		TestLog(testFlags, TXT("Testing saving data to config.  Updating time stamp variables to current time:  %s"), current.ToIsoFormat());

		testConfig->SaveProperty<DString>(unitTestSection, TXT("FullTimeStamp"), current.ToIsoFormat());
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Year"), current.Year);
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Month"), current.Month);
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Day"), current.Day);
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Hour"), current.Hour);
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Minute"), current.Minute);
		testConfig->SaveProperty<Int>(unitTestSection, TXT("Second"), current.Second);

		testConfig->SavePropertyText(DString::EmptyString, TXT("WriteHeaderTest"), DString::CreateFormattedString(headerTestFormat, current.Day));
		testConfig->SaveConfig();

#if 0
		//This was removed since this will most likely cause a sharing violation.
		//Check if data was actually saved to the file (not just in buffer)
		if (OS_GetFileSize(UnitTestLocation, TXT("ConfigTest.ini")) <= 0)
		{
			UnitTestError(testFlags, TXT("Save config test failed.  %s file size is zero after calling SaveConfig()."), fileName);
			testConfig->Destroy();
			return false;
		}
#endif
	}

	TestLog(testFlags, TXT("Retrieving newly saved data from config."));
	{
		DString currentReaderStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
		if (currentReaderStr != current.ToIsoFormat())
		{
			UnitTestError(testFlags, TXT("Config test failed.  The retrieved value of FullTimeStamp should be equal to %s.  Instead it returned %s."), current.ToIsoFormat(), currentReaderStr);
			return false;
		}

		DateTime currentReader;
		currentReader.Year = testConfig->GetProperty<Int>(unitTestSection, TXT("Year"));
		currentReader.Month = testConfig->GetProperty<Int>(unitTestSection, TXT("Month"));
		currentReader.Day = testConfig->GetProperty<Int>(unitTestSection, TXT("Day"));
		currentReader.Hour = testConfig->GetProperty<Int>(unitTestSection, TXT("Hour"));
		currentReader.Minute = testConfig->GetProperty<Int>(unitTestSection, TXT("Minute"));
		currentReader.Second = testConfig->GetProperty<Int>(unitTestSection, TXT("Second"));

		if (currentReader != current)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The retrieved DateTime should be equal to %s.  Instead it retrieved %s."), current.ToIsoFormat(), currentReader.ToIsoFormat());
			testConfig->Destroy();
			return false;
		}

		DString headerTest = testConfig->GetPropertyText(DString::EmptyString, TXT("WriteHeaderTest"));
		DString expectedHeader = DString::CreateFormattedString(headerTestFormat, currentReader.Day);
		if (headerTest.Compare(expectedHeader, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Config test failed. The retrieved WriteHeaderTest should be equal to \"%s\". Instead it retrieved \"%s\"."), expectedHeader, headerTest);
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Saving data to config test passed!  Was able to retrieve the current time from config:  %s"), currentReader.ToIsoFormat());
	}

	SetTestCategory(testFlags, TXT("Multi Line String"));
	{
		const DString expectedMultiLineStringValue(TXT("The value of this string is covered in multiple lines \n\
The line must end with a \\ in order to continue onward to the next string. \n\
\n\
FakeProperty=This line including \"FakeProperty=\" should be included in MultiLineString. \n\
\n\
[FakeSection]"));
		const DString fakeSectionStr = TXT("FakeSection");
		const DString fakePropertyKey = TXT("FakeProperty");

		TestLog(testFlags, TXT("Verifying that the %s section is not created."), fakeSectionStr);
		if (testConfig->ContainsSection(fakeSectionStr))
		{
			UnitTestError(testFlags, TXT("Config test failed. The ConfigWriter object created a section [%s] from a multi-line string. That section should not exist since it's part of a property value."), fakeSectionStr);
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Verifying that the %s property key doesn't exist in ConfigWriter."), fakePropertyKey);
		DString fakeValue = testConfig->GetPropertyText(unitTestSection, fakePropertyKey);
		if (!fakeValue.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Config test failed. The ConfigWriter object created a property line %s within the section %s when it shouldn't have since that line is part of a multi line string value."), fakePropertyKey, unitTestSection);
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Verifying the contents of the multi line string."));
		DString multiLineStringValue = testConfig->GetPropertyText(unitTestSection, TXT("MultiLineString"));
		if (multiLineStringValue.Compare(expectedMultiLineStringValue, DString::CC_CaseSensitive) != 0)
		{
			FileLog.Log(LogCategory::LL_Critical, TXT("Config unit tester failed. Expected multi line string value:  %s"), expectedMultiLineStringValue);
			FileLog.Log(LogCategory::LL_Critical, TXT("Config unit tester failed. Actual multi line string value:  %s"), multiLineStringValue);
			UnitTestError(testFlags, TXT("Config test failed. The value of the MultiLineString value does not match the expected value. See logs for expected and actual values."));
			testConfig->Destroy();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Config Array"));
	{
		TestLog(testFlags, TXT("Reading the counter from the config array."));
		std::vector<Int> counterKey;
		for (UINT_TYPE i = 1; i < 11; i++)
		{
			counterKey.push_back(Int(i));
		}

		std::vector<Int> counter;
		testConfig->GetArrayValues<Int>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  There's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), Int(counterKey.size()), Int(counter.size()));
			testConfig->Destroy();
			return false;
		}

		for (UINT_TYPE i = 0; i < counter.size(); i++)
		{
			TestLog(testFlags, TXT("    Counter[%s]=%s"), Int(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  Reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), Int(i), counterKey.at(i), counter.at(i));
				testConfig->Destroy();
				return false;
			}
		}

		//Test saving array
		ContainerUtils::Empty(OUT counter);
		testConfig->SaveArray<Int>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		//Retrieve that array to see if the result is empty
		testConfig->GetArrayValues<Int>(TXT("ArrayTest"), TXT("CounterArray"), counter);
		if (!counter.empty())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After clearing the counter array, and updating the config file array with an empty one, retrieving that same array should have also returned an empty array.  Instead the size of the array is %s"), Int(counter.size()));
			testConfig->Destroy();
			return false;
		}

		//Repopulate array
		testConfig->SaveArray<Int>(TXT("ArrayTest"), TXT("CounterArray"), counterKey);
		testConfig->GetArrayValues<Int>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, there's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), Int(counterKey.size()), Int(counter.size()));
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Rereading the counter array after updating the config file."));
		for (UINT_TYPE i = 0; i < counter.size(); i++)
		{
			TestLog(testFlags, TXT("    Counter[%s]=%s"), Int(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), Int(i), counterKey.at(i), counter.at(i));
				testConfig->Destroy();
				return false;
			}
		}

		std::vector<Vector3> vectorKey;
		for (Float i = 1.f; i <= 8.f; i += 1.f)
		{
			vectorKey.push_back(Vector3(-i, i * 10.f, 32.2f * -i));
		}

		std::vector<Vector3> configVector;
		testConfig->GetArrayValues<Vector3>(TXT("ArrayTest"), TXT("ArrayOfStructs"), OUT configVector);

		if (vectorKey.size() != configVector.size())
		{
			UnitTestError(testFlags, TXT("Config array test failed. Reading in the array of structs does not match the expected size. The config has %s elements while the test expected %s elements."), Int(configVector.size()), Int(vectorKey.size()));
			testConfig->Destroy();
			return false;
		}

		for (size_t i = 0; i < configVector.size(); ++i)
		{
			if (!configVector.at(i).IsNearlyEqual(vectorKey.at(i), 0.0001f))
			{
				UnitTestError(testFlags, TXT("Config array test failed. The array of structs at index %s should have been %s. Instead it's %s."), Int(i), vectorKey.at(i), configVector.at(i));
				testConfig->Destroy();
				return false;
			}
		}

		//Test saving an array of structs
		testConfig->SaveArray<Vector3>(TXT("ArrayTest"), TXT("ArrayOfStructs"), vectorKey);

		//Reread from config to ensure the saved data can be read.
		ContainerUtils::Empty(OUT configVector);
		testConfig->GetArrayValues<Vector3>(TXT("ArrayTest"), TXT("ArrayOfStructs"), OUT configVector);

		if (vectorKey.size() != configVector.size())
		{
			UnitTestError(testFlags, TXT("Config array test failed. After saving the array of structs, the resulting data does not match the original vector. The original vector size is %s. The resulting vector size is %s."), Int(vectorKey.size()), Int(configVector.size()));
			testConfig->Destroy();
			return false;
		}

		for (size_t i = 0; i < configVector.size(); ++i)
		{
			if (!configVector.at(i).IsNearlyEqual(vectorKey.at(i), 0.0001f))
			{
				UnitTestError(testFlags, TXT("Config array test failed. After saving the array of structs, the unit test is unable to retrieve the original vector from the config file. The array of structs at index %s should have been %s. Instead it's %s."), Int(i), vectorKey.at(i), configVector.at(i));
				testConfig->Destroy();
				return false;
			}
		}

		testConfig->Destroy();
		testConfig = nullptr;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Reading Headers Only"));
	{
		CHECK(testConfig == nullptr);
		testConfig = ConfigWriter::CreateObject();
		testConfig->SetReadEverything(false); //Only read header data
		if (!testConfig->OpenFile(configAttr, false))
		{
			UnitTestError(testFlags, TXT("Config test failed. When setting read everything to false, the ConfigWriter is unable to read from %s."), fileName);
			testConfig->Destroy();
			return false;
		}

		if (testConfig->ContainsSection(unitTestSection))
		{
			UnitTestError(testFlags, TXT("Config test failed. When setting read everything to false, the ConfigWriter is still able to populate section %s"), unitTestSection);
			testConfig->Destroy();
			return false;
		}

		DString propName(TXT("PropertyWithoutSection"));
		DString expected(TXT("Some Value"));
		DString actual = testConfig->GetPropertyText(DString::EmptyString, propName);
		if (actual.Compare(expected, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Config test failed. When reading header property (%s) from a config file that's only reading from header, the expected value of that property is \"%s\". Instead it is \"%s\"."), propName, expected, actual);
			testConfig->Destroy();
			return false;
		}

		testConfig->Destroy();
		testConfig = nullptr;
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Config"));
	return true;
}

bool FileUnitTester::TestBinaryFile (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Binary File"));

#ifdef PLATFORM_64BIT
	FileAttributes testFile(UnitTestLocation, TXT("BinaryFileTest_64.bin"));
#else
	FileAttributes testFile(UnitTestLocation, TXT("BinaryFileTest_32.bin"));
#endif

	std::vector<Object*> testObjs;
	std::function<void()> endTest([&]()
	{
		for (Object* obj : testObjs)
		{
			obj->Destroy();
		}
		ContainerUtils::Empty(OUT testObjs);

		if (testFile.GetFileExists())
		{
			OS_DeleteFile(testFile);
		}
	});

	if (testFile.GetFileExists())
	{
		OS_DeleteFile(testFile);
	}

	//Create two test objects
	Entity* singleCompEntity = Entity::CreateObject();
	{
		singleCompEntity->DebugName = TXT("Entity With Single Component");
		EntityComponent* comp = EntityComponent::CreateObject();
		singleCompEntity->AddComponent(comp);
		testObjs.push_back(singleCompEntity);
	}

	Entity* multiCompEntity = Entity::CreateObject();
	{
		multiCompEntity->DebugName = TXT("Double Component Entity");
		TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
		if (multiCompEntity->AddComponent(tick))
		{
			tick->SetTicking(false);
			tick->SetTickInterval(6.5f);
		}

		EntityComponent* comp = EntityComponent::CreateObject();
		multiCompEntity->AddComponent(comp);
		testObjs.push_back(multiCompEntity);
	}

	SetTestCategory(testFlags, TXT("Saving File"));
	{
		ComponentSerializer* saveFile = ComponentSerializer::CreateObject();

		saveFile->EditStagedEntities().push_back(singleCompEntity);
		saveFile->EditStagedEntities().push_back(multiCompEntity);
		if (!saveFile->SaveFile(testFile))
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Failed to save components to file %s."), testFile.GetName(true, true));
			saveFile->Destroy();
			endTest();
			return false;
		}

		//Remove all Entities from the staged list so that the saveFile will not destroy them. This unit test will destroy them instead.
		ContainerUtils::Empty(OUT saveFile->EditStagedEntities());
		saveFile->Destroy();
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Loading File"));
	{
		ComponentSerializer* loadFile = ComponentSerializer::CreateObject();
		testObjs.push_back(loadFile);

		if (!loadFile->OpenFile(testFile))
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Failed to load contents from %s."), testFile.GetName(true, true));
			endTest();
			return false;
		}

		//Verify the single component entity loaded correctly
		Entity* otherSingleEntity = nullptr;
		for (Entity* loadedEntity : loadFile->EditStagedEntities())
		{
			if (loadedEntity->ReadComponents().size() == 1)
			{
				//Asume this is the right entity to compare against
				otherSingleEntity = loadedEntity;
				break;
			}
		}

		if (otherSingleEntity == nullptr)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not load the single component Entity from %s."), testFile.GetName(true, true));
			endTest();
			return false;
		}

		if (otherSingleEntity->DebugName.Compare(singleCompEntity->DebugName, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not properly load the entity's DebugName from %s. Original Debug Name = \"%s\". Loaded Debug Name = \"%s\"."), testFile.GetName(true, true), singleCompEntity->DebugName, otherSingleEntity->DebugName);
			endTest();
			return false;
		}

		//Ensure it loaded the Entity Component
		CHECK(singleCompEntity->ReadComponents().size() == 1)
		if (otherSingleEntity->ReadComponents().at(0)->StaticClass() != EntityComponent::SStaticClass() || singleCompEntity->ReadComponents().at(0)->StaticClass() != EntityComponent::SStaticClass())
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not load the Entity Component from %s."), testFile.GetName(true, true));
			endTest();
			return false;
		}

		//Verify if the multi component loaded correctly.
		Entity* loadedMultiEntity = nullptr;
		for (Entity* loadedEntity : loadFile->EditStagedEntities())
		{
			if (loadedEntity->ReadComponents().size() == multiCompEntity->ReadComponents().size())
			{
				loadedMultiEntity = loadedEntity;
				break;
			}
		}

		if (loadedMultiEntity == nullptr)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not load the multi component Entity from %s."), testFile.GetName(true, true));
			endTest();
			return false;
		}

		if (loadedMultiEntity->DebugName.Compare(multiCompEntity->DebugName, DString::CC_CaseSensitive) != 0)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not properly load the entity's DebugName from %s. Original Debug Name = \"%s\". Loaded Debug Name = \"%s\"."), testFile.GetName(true, true), multiCompEntity->DebugName, loadedMultiEntity->DebugName);
			endTest();
			return false;
		}

		//Find the TickComponent
		TickComponent* originalTick = dynamic_cast<TickComponent*>(multiCompEntity->FindComponent(TickComponent::SStaticClass(), false));
		if (originalTick == nullptr)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Somehow the unit test was unable to find the original tick component from the multi component entity."));
			endTest();
			return false;
		}

		TickComponent* loadedTick = dynamic_cast<TickComponent*>(loadedMultiEntity->FindComponent(TickComponent::SStaticClass(), false));
		if (loadedTick == nullptr)
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Could not load a Tick Component from %s."), testFile.GetName(true, true));
			endTest();
			return false;
		}

		if (loadedTick->IsTicking() != originalTick->IsTicking())
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Failed to properly extract IsTicking from %s. Original IsTicking = %s. Loaded IsTicking = %s."), testFile.GetName(false, true), Bool(originalTick->IsTicking()), Bool(loadedTick->IsTicking()));
			endTest();
			return false;
		}

		if (loadedTick->GetTickInterval() != originalTick->GetTickInterval())
		{
			UnitTestError(testFlags, TXT("Binary File test failed. Failed to properly extract the tick interval from %s. Original tick interval = %s. Loaded tick interval = %s."), testFile.GetName(false, true), originalTick->GetTickInterval(), loadedTick->GetTickInterval());
			endTest();
			return false;
		}

		for (size_t i = 0; i < testObjs.size(); ++i)
		{
			if (testObjs.at(i) == loadFile)
			{
				testObjs.erase(testObjs.begin() + i);
				break;
			}
		}

		//Since the loaded entities were not removed from the loadFile staged entities, the ownership did not transfer. All loaded Entities will also be destroyed by destroying loadFile.
		loadFile->Destroy();
	}
	CompleteTestCategory(testFlags);

	endTest();

	ExecuteSuccessSequence(testFlags, TXT("Binary File"));
	return true;
}
SD_END
#endif