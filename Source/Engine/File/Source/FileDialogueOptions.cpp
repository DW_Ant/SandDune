/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileDialogueOptions.cpp
=====================================================================
*/

#include "FileDialogueOptions.h"

SD_BEGIN
FileDialogueOptions::FileDialogueOptions (EDialogueType inDialogueType) :
	DialogueType(inDialogueType),
	DefaultFolder(Directory::GetWorkingDirectory()),
	bMultiSelect(false),
	PersistenceId(0),
	bStrictFileTypesOnly(true)
{
	//Noop
}
SD_END