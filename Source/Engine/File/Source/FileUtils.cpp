/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileUtils.cpp
=====================================================================
*/

#include "FileClasses.h"

IMPLEMENT_ABSTRACT_CLASS(SD::FileUtils, SD::BaseUtils)
SD_BEGIN

void FileUtils::ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFullFileName)
{
	Int idx = fullAddress.ReadString().find_last_of(*Directory::DIRECTORY_SEPARATOR.ToCString());
	if (idx == std::string::npos)
	{
		//No path was given.
		outPath.Clear();
		outFullFileName = fullAddress;
		return;
	}

	outPath = fullAddress.SubString(0, idx);
	outFullFileName = fullAddress.SubString(idx + 1);
}

void FileUtils::ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension)
{
	Int extIdx = fullFileName.Find(TXT("."), 1, DString::CC_CaseSensitive);
	if (extIdx <= 0)
	{
		//No extension was given.
		outExtension.Clear();
		outFileName = fullFileName;
		return;
	}

	fullFileName.SplitString(extIdx, outFileName, outExtension);
}

void FileUtils::CopyFileContents (const DString& source, const DString& destination)
{
	//This seems to be the most simplistic way to copy one file to another:  http://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way
	std::ifstream sourceStream(source.ToCString());
	std::ofstream destinationStream(destination.ToCString());

	destinationStream << sourceStream.rdbuf();

	sourceStream.close();
	destinationStream.close();
}
SD_END