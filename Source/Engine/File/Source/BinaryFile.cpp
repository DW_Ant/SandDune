/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BinaryFile.cpp
=====================================================================
*/

#include "BinaryFile.h"

IMPLEMENT_CLASS(SD::BinaryFile, SD::Object)
SD_BEGIN

const uint32 BinaryFile::MAGIC_NUMBER = 2011611911; //2011-11-11 = Monster Evolution's release date.

void BinaryFile::InitProps ()
{
	Super::InitProps();

#ifdef PLATFORM_64BIT
	PlatformBitSize = PS_64Bit;
#else
	PlatformBitSize = PS_32Bit;
#endif

#if USE_WIDE_STRINGS
	StrEncoding = SE_Wide;
#elif USE_UTF32
	StrEncoding = SE_Utf32;
#elif USE_UTF16
	StrEncoding = SE_Utf16;
#elif USE_UTF8
	StrEncoding = SE_Utf8;
#else
	StrEncoding = SE_Ansi;
#endif

	bAccessingFile = false;
}

bool BinaryFile::OpenFile (const FileAttributes& file)
{
	DString fileName = file.GetName(true, true);
	FileLog.Log(LogCategory::LL_Verbose, TXT("Attempting to read %s."), fileName);

	if (IsAccessingFile())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot open %s since this BinaryFile instance is already accessing another file. Create a different instance."), fileName);
		return false;
	}
	LatestAccessedFile = file;
	bAccessingFile = true;

	if (!OS_CheckIfFileExists(file))
	{
		FileLog.Log(LogCategory::LL_Log, TXT("%s does not exist."), fileName);
		bAccessingFile = false;
		return false;
	}

	std::ifstream stream;
	stream.open(fileName.ToCString(), std::ios::in | std::ios::binary);
	if (!stream || stream.bad())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Failed to open file %s."), fileName);
		bAccessingFile = false;
		return false;
	}

	/*
	First byte contains header.
	Bytes 2-5 is a 32 bit magic number to check against data corruption.
	Bytes 6-9 is another 32 bit number that is the engine major version. Note in 64-bit platforms, this would be 8 bytes instead of 4. But this only checks for minimum size.
	Bytes 10-13 is another 32 bit number that is the engine minor version.
	*/
	Int minSize = 13;

	//Get the length of the file
	stream.seekg(0, stream.end);
	int numBytes = stream.tellg();
	if (numBytes < 0)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("The stream to the file %s does not support the tellg operation."), fileName);
		bAccessingFile = false;
		return false;
	}
	else if (numBytes < minSize)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("%s is too small. All binary files are required to have at least %s bytes of header data."), fileName, minSize);
		bAccessingFile = false;
		return false;
	}
	stream.seekg(0, stream.beg); //Move back to the beginning.

	//Write the contents of the file to a data buffer
	char* rawBytes = new char[numBytes];
	stream.read(OUT rawBytes, numBytes);
	stream.close();

	//The first bit is the endianness. 0 means little endian. 1 means big endian.
	bool bLittleEndian = ((rawBytes[0] & (1 << 7)) == 0); //?000 0000
	if (bLittleEndian != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(OUT rawBytes, numBytes);
	}

	DataBuffer readData;
	readData.AppendBytes(rawBytes, numBytes);
	delete[] rawBytes;

	//Read the header content
	char headerData;
	readData.ReadBytes(&headerData, 1);

	/*
	[0] = Endian check. 0 means little endian. 1 means big endian.
	[1] = 0 means 32 bit ints. 1 means 64 bit ints.
	[2][3] = string encoding. 00 = ansi, 01 = UTF8, 10 = UTF16, 11 = UTF32
	Bits [4-6] are reserved.
	[7] = Always equal to bit [0] in case the endianness is reversed.
	*/

	//The first bit is already parsed before it was written to the data buffer.

	//[1] = Check for size of ints.
	if ((headerData & (1 << 6)) != 0) //0?00 0000
	{
		PlatformBitSize = PS_64Bit;
#ifdef PLATFORM_32BIT
		//Give warning about precision loss when opening 64 bit files in 32 bit.
		FileLog.Log(LogCategory::LL_Warning, TXT("A 64 bit application created %s. Opening this file in a 32 bit application may introduce clamping issues. Large integers and arrays may be clamped to fit in a register. Generally it's a good idea to keep 32 bit files separate from 64 bit files."), fileName);
#endif
	}
	else
	{
		PlatformBitSize = PS_32Bit;
	}

	//[2-3] Check for string encoding
	char encodingBits = headerData; //00?? 0000
	encodingBits = (encodingBits >> 4); //Bring to zero bit
	encodingBits &= 3; //Mask out everything except for the first two bits
	switch (encodingBits)
	{
		case(0):
			StrEncoding = SE_Ansi;
			break;

		case(1):
			StrEncoding = SE_Utf8;
			break;

		case(2):
			StrEncoding = SE_Utf16;
			break;

		case(3):
			StrEncoding = SE_Utf32;
			break;

		default:
			//This should never happen due to bit masking above.
			FileLog.Log(LogCategory::LL_Fatal, TXT("Error when opening %s. Failed to properly read string encoding after masking out bits."), fileName);
	}
	
	//[4-6] bits are reserved.
	//[7] is used for endianness to handle reversed bytes.

	//The next 4 bytes are simply comparing against the magic num
	const int magicNumSize = 4;
	char magicNumBytes[magicNumSize];
	readData.ReadBytes(magicNumBytes, magicNumSize);

	uint32 magicNum;
	memcpy(&magicNum, magicNumBytes, magicNumSize);
	if (magicNum != MAGIC_NUMBER)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Error when opening %s. The file's header information does not match the expected number. It's either not in Sand Dune's format, or the data is corrupted."), fileName);
		bAccessingFile = false;
		return false;
	}

	//Check for engine versions
	bool success; //Not checked since we checked for min size earlier.
	size_t versionIdx = readData.GetReadIdx();
	Int majorVersion = ReadIntFromBuffer(OUT readData, OUT success);
	Int minorVersion = ReadIntFromBuffer(OUT readData, OUT success);

	if (majorVersion > Version::SAND_DUNE_MAJOR_VERSION ||
		//Ignore minor version if the major version is greater than the file's major version.
		(minorVersion > Version::SAND_DUNE_MINOR_VERSION && majorVersion == Version::SAND_DUNE_MAJOR_VERSION))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot open %s since it was created using a later version of the engine. Forward compatibility is not supported. Upgrade this application to be able to access that file."), fileName);
		bAccessingFile = false;
		return false;
	}

	//Check if this file is created using an older version. If so upgrade the byte buffer.
	if (majorVersion != Version::SAND_DUNE_MAJOR_VERSION || minorVersion != Version::SAND_DUNE_MINOR_VERSION)
	{
		FileLog.Log(LogCategory::LL_Log, TXT("%s was created in version %s.%s. Converting the file contents to version %s."), fileName, majorVersion, minorVersion, Version::GetVersionNumber());
		size_t oldReadIdx = readData.GetReadIdx();

		bool conversionSuccess = ConvertToCurrentVersion(OUT readData, majorVersion, minorVersion);
		if (!conversionSuccess)
		{
			bAccessingFile = false;
			return false;
		}

		//Update the version number
		DataBuffer versionNumbers;
		versionNumbers << Version::SAND_DUNE_MAJOR_VERSION;
		versionNumbers << Version::SAND_DUNE_MINOR_VERSION;
		readData.EditBytes(versionIdx, versionNumbers);

		//Update the actual file
		size_t saveDataBytes = readData.GetNumBytes();
		const std::vector<char>& rawData = readData.ReadRawData();
		std::ofstream newFile(file.GetName(true, true).ToCString(), std::ios::binary | std::ios::out);
		newFile.write(&rawData[0], saveDataBytes);

		//Restore the read index back to the end of the header data.
		readData.JumpToBeginning();
		readData.AdvanceIdx(oldReadIdx);
	}

	//Now read the actual contents of the file.
	bool bSuccess = ReadContents(OUT readData);
	bAccessingFile = false;

	return bSuccess;
}

bool BinaryFile::SaveFile (const FileAttributes& file) const
{
	DString fileName = file.GetName(true, true);
	FileLog.Log(LogCategory::LL_Verbose, TXT("Attempting to save %s."), fileName);

	if (IsAccessingFile())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s since this BinaryFile instance is already accessing another file. Create a different instance."), fileName);
		return false;
	};

	LatestAccessedFile = file;
	bAccessingFile = true;

	if (OS_CheckIfFileExists(file) && OS_IsFileReadOnly(file))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s since that file is read only."), file.GetName(false, true));
		bAccessingFile = false;
		return false;
	}

	if (!OS_CheckIfDirectoryExists(file.ReadPath()))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s since that directory does not exist."), file.GetName(true, true));
		bAccessingFile = false;
		return false;
	}

	char headerData = 0;
	if (!DataBuffer::IsSystemLittleEndian())
	{
		//1xxx xxx1
		headerData |= (1 << 7);
		headerData |= 1;
	}

#ifdef PLATFORM_64BIT
	headerData |= (1 << 6); //x1xx xxxx
#endif

#if USE_UTF32
	headerData |= (3 << 4); //xx11 xxxx
#elif USE_UTF16
	headerData |= (1 << 5); //xx10 xxxx
#elif USE_UTF8
	headerData |= (1 << 4); //xx01 xxxx
#endif

	//Bits [4-6] are reserved. Bit [7] is assigned from endianness

	DataBuffer saveData;
	saveData.AppendBytes(&headerData, 1);

	//Write the magic number
	const int numBytes = 4;
	char charArray[numBytes];
	memcpy(charArray, &MAGIC_NUMBER, numBytes);
	saveData.AppendBytes(charArray, numBytes);

	//Write the version numbers
	saveData << Version::SAND_DUNE_MAJOR_VERSION;
	saveData << Version::SAND_DUNE_MINOR_VERSION;

	bool bSuccess = SaveContents(OUT saveData);
	if (!bSuccess)
	{
		bAccessingFile = false;
		return false;
	}

	size_t saveDataBytes = saveData.GetNumBytes();
	const std::vector<char>& rawData = saveData.ReadRawData();
	std::ofstream newFile(file.GetName(true, true).ToCString(), std::ios::binary | std::ios::out);
	newFile.write(&rawData[0], saveDataBytes);
	bAccessingFile = false;

	return true;
}

bool BinaryFile::ConvertToCurrentVersion (DataBuffer& outData, Int oldMajorVersion, Int oldMinorVersion)
{
	return true;
}

DString BinaryFile::ReadStringFromBuffer (const DataBuffer& incomingData, bool& outBytesRead) const
{
	if (incomingData.IsReaderAtEnd())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot read DString from %s. There aren't any more bytes in the file to read from."), LatestAccessedFile.GetName(false, true));
		outBytesRead = false;
		return DString::EmptyString;
	}

	DString result;
	if ((incomingData >> result).HasReadError())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot read DString from %s. The bytes in the file is malformed, preventing it from reading a DString."), LatestAccessedFile.GetName(false, true));
		outBytesRead = false;
		return DString::EmptyString;
	}

	outBytesRead = true;

	switch (StrEncoding)
	{
		case(SE_Ansi):
			//UTF strings are already compatible with ansi strings
			return result;

		case(SE_Utf8):
#if USE_UTF32
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-8 to UTF-32 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#elif USE_UTF16
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-8 to UTF-16 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#endif
			//Note: UTF-8 is already in correct format. And for Ansi strings, all character encodings are backwards compatible to ansi.
			return result;

		case(SE_Utf16):
#if USE_UTF32
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-16 to UTF-32 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#else USE_UTF8
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-16 to UTF-8 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#endif
			return result;

		case(SE_Utf32):
#if USE_UTF16
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-16 to UTF-32 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#elif USE_UTF8
			FileLog.Log(LogCategory::LL_Warning, TXT("Error when reading from %s. String encoding converter from UTF-32 to UTF-8 is not implemented. \"%s\" may appear incorrect."), LatestAccessedFile.GetName(false, true), result);
#endif
			return result;
	}

	return result;
}

Int BinaryFile::ReadIntFromBuffer (const DataBuffer& incomingData, bool& outBytesRead) const
{
#ifdef PLATFORM_64BIT
	if (PlatformBitSize == PS_32Bit)
	{
		if (!incomingData.CanReadBytes(4))
		{
			FileLog.Log(LogCategory::LL_Warning, TXT("Cannot read Int from %s. There aren't any more bytes in the file to read from."), LatestAccessedFile.GetName(false, true));
			incomingData.JumpToEnd();
			outBytesRead = false;
			return 0;
		}

		//The file was saved in 32 bit. Only extract 4 bytes of data and store that to a 64-bit Int.
		const int numBytes = 4;
		char rawData[numBytes];
		incomingData.ReadBytes(rawData, numBytes);

		int32 originalInt;
		memcpy(&originalInt, rawData, numBytes);

		outBytesRead = true;
		return Int(originalInt);
	}
#endif

	if (!incomingData.CanReadBytes(Int::SGetMinBytes()))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot read Int from %s. There aren't enough bytes in the file to read from."), LatestAccessedFile.GetName(false, true));
		incomingData.JumpToEnd();
		outBytesRead = false;
		return 0;
	}

	Int result;
	if ((incomingData >> result).HasReadError())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot read Int from %s. The bytes in the file are not properly formatted to read an Int."), LatestAccessedFile.GetName(false, true));
		incomingData.JumpToEnd();
		outBytesRead = false;
		return 0;
	}

	outBytesRead = true;

#ifdef PLATFORM_32BIT
	if (PlatformBitSize == PS_64Bit)
	{
		FileLog.Log(LogCategory::LL_Verbose, TXT("32-bit systems may have trouble reading from 64-bit Ints in file %s. There could be number overflows."), LatestAccessedFile.GetName(false, true));
	}
#endif
	
	return result;
}

bool BinaryFile::ReadContents (const DataBuffer& incomingData)
{
	return true;
}

bool BinaryFile::SaveContents (DataBuffer& outBuffer) const
{
	return true;
}
SD_END