/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LaunchProcessInfo.cpp
=====================================================================
*/

#include "LaunchProcessOptions.h"

SD_BEGIN
LaunchProcessOptions::LaunchProcessOptions (const FileAttributes& inExecFile) :
	ExecFile(inExecFile),
	CmdLineArgs(DString::EmptyString),
	bChildProcess(true)
{
	//Noop
}
SD_END