/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Directory.cpp
=====================================================================
*/

#include "Directory.h"

SD_BEGIN

#ifdef PLATFORM_WINDOWS
const DString Directory::DIRECTORY_SEPARATOR = TXT("\\");
#else
const DString Directory::DIRECTORY_SEPARATOR = TXT("/");
#endif

//List is based on stack overflow post:  https://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
//Note:  We include slashes since the Directory object will interpret slashes as a sub folder.
const std::vector<char> Directory::FORBIDDEN_CHARACTERS =
{
	'\0'
#ifdef PLATFORM_WINDOWS
	, '<',
	'>',
	'\"',
	'|',
	'?',
	'*'
#endif
};

const Directory Directory::INVALID_DIRECTORY(TXT("?Huh?"), false);
const Directory Directory::BASE_DIRECTORY = FindBaseDirectory();
const Directory Directory::BINARY_DIRECTORY = FindBinaryDirectory();
const Directory Directory::CONFIG_DIRECTORY = BASE_DIRECTORY / TXT("Config");
const Directory Directory::CONTENT_DIRECTORY = BASE_DIRECTORY / TXT("Content");
const Directory Directory::DEV_ASSET_DIRECTORY = BASE_DIRECTORY / TXT("DevAssets");
const Directory Directory::DOCUMENT_DIRECTORY = BASE_DIRECTORY / TXT("Documents");
const Directory Directory::SOURCE_DIRECTORY = BASE_DIRECTORY / TXT("Source");

Directory::Directory () :
	DirectoryPath(TXT(".")),
	DirectoryState(DS_RelativePath),
	AutoFormat(false)
{
	//Noop
}

Directory::Directory (const Directory& inDirectory) :
	DirectoryPath(inDirectory.DirectoryPath),
	DirectoryState(inDirectory.DirectoryState),
	AutoFormat(inDirectory.AutoFormat)
{
	//Noop
}

Directory::Directory (const DString& inPath, bool inAutoFormat) :
	DirectoryPath(inPath),
	DirectoryState(DS_Undefined),
	AutoFormat(inAutoFormat)
{
	if (AutoFormat)
	{
		SanitizeInternalVariables();
	}
}


Directory::~Directory ()
{
	//Noop
}

void Directory::operator= (const Directory& other)
{
	DirectoryPath = other.DirectoryPath;
	DirectoryState = other.DirectoryState;
	AutoFormat = other.AutoFormat;
}

void Directory::operator= (const DString& inPath)
{
	DirectoryPath = inPath;
	DirectoryState = DS_Undefined;

	if (AutoFormat)
	{
		SanitizeInternalVariables();
	}
}

bool Directory::operator== (const Directory& other) const
{
	return (DirectoryPath.Compare(other.DirectoryPath, DString::CC_CaseSensitive) == 0);
}

bool Directory::operator!= (const Directory& other) const
{
	return !(*this == other);
}

void Directory::ResetToDefaults ()
{
	DirectoryPath = TXT(".");
	DirectoryState = DS_RelativePath;
	AutoFormat = false;
}

DString Directory::ToString () const
{
	return DirectoryPath;
}

void Directory::ParseString (const DString& str)
{
	DirectoryPath = str;
	DirectoryState = DS_Undefined;
}

size_t Directory::GetMinBytes () const
{
	return DirectoryPath.GetMinBytes() + sizeof(AutoFormat);
}

void Directory::Serialize (DataBuffer& outData) const
{
	outData << DirectoryPath;
}

bool Directory::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> DirectoryPath;
	DirectoryState = DS_Undefined;

	return !dataBuffer.HasReadError();
}

Directory Directory::GetWorkingDirectory ()
{
	return Directory(ToAbsolutePath(TXT(".")), true);
}

DString Directory::ToAbsolutePath (const DString& relativePath)
{
	DString result = OS_FullPath(relativePath);

	if (result.FindAt(result.Length() - 1) != DIRECTORY_SEPARATOR)
	{
		result += DIRECTORY_SEPARATOR;
	}

	return result;
}

void Directory::ConvertToAbsPath ()
{
	if (IsAbsPath())
	{
		return; //Already abs path... do nothing
	}

	DirectoryPath = ToAbsolutePath(DirectoryPath);
	DirectoryState = DS_AbsolutePath;
}

Directory Directory::GetAbsPath () const
{
	const DString absPath = (DirectoryState != DS_AbsolutePath) ? ToAbsolutePath(DirectoryPath) : DirectoryPath;

	return Directory(absPath, AutoFormat);
}

std::vector<DString> Directory::SplitDirectoryToList () const
{
	std::vector<DString> results;

	DirectoryPath.ParseString(*DIRECTORY_SEPARATOR.ToCString(), OUT results, true);
	return results;
}

Directory Directory::ListToDirectory (const std::vector<DString>& directoryList)
{
	DString fullPath = DString::EmptyString;
	for (UINT_TYPE i = 0; i < directoryList.size(); ++i)
	{
		fullPath += directoryList.at(i) + DIRECTORY_SEPARATOR;
	}

	return Directory(fullPath, true);
}

DString Directory::GetInnerMostDirectory () const
{
	if (DirectoryPath.Length() <= 2)
	{
		return DirectoryPath; //Directory path without directory separators?  Must be an unformatted string.
	}

	//Regarding specifying position 2, find second to last directory separator to exclude the trailing slash.
	Int folderIdx = DirectoryPath.Find(DIRECTORY_SEPARATOR, 2, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	if (folderIdx == INDEX_NONE)
	{
		return DirectoryPath.SubString(0, DirectoryPath.Length() - 2); //Don't include trailing slash
	}
	else
	{
		//Don't include the slashes surrounding the inner folder name.
		return DirectoryPath.SubString(folderIdx + 1, DirectoryPath.Length() - 2);
	}
}

bool Directory::GetParentDir (Directory& outParentDir) const
{
	DString absPath = GetAbsPath().ToString();
	Int lastSeparatorIdx = absPath.Find(Directory::DIRECTORY_SEPARATOR, 1, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	if (lastSeparatorIdx == INT_INDEX_NONE)
	{
		//Already root directory
		return false;
	}

	DString parentDir = absPath.SubString(0, lastSeparatorIdx);
	outParentDir = Directory(parentDir, true);
	return true;
}

#ifdef PLATFORM_WINDOWS
DString Directory::GetDriveLetter () const
{
	if (IsAbsPath())
	{
		return DirectoryPath.FindAt(0);
	}

	return DString::EmptyString;
}
#endif

void Directory::SetDirectory (const DString& newDirectory)
{
	DirectoryPath = newDirectory;
	DirectoryState = DS_Undefined;
	if (AutoFormat)
	{
		SanitizeInternalVariables();
	}
}

void Directory::SetAutoFormat (bool newAutoFormat)
{
	AutoFormat = newAutoFormat;
	if (AutoFormat)
	{
		SanitizeInternalVariables();
	}
}

bool Directory::IsAbsPath () const
{
	if (DirectoryState == DS_Undefined)
	{
		ComputeDirectoryState();
		CHECK(DirectoryState != DS_Undefined) //ComputeDirectoryState should always define the directory state.
	}

	return (DirectoryState == DS_AbsolutePath);
}

Directory Directory::FindBaseDirectory ()
{
	DString exeLocation;
	OS_GetProcessLocation(OUT exeLocation);

	//Validate location
	Int idx = exeLocation.Find(TXT("Binaries"), 0, DString::CC_CaseSensitive, DString::SD_LeftToRight);
	if (idx < 0 || idx >= exeLocation.Length() - 1)
	{
		Engine::FindEngine()->FatalError(TXT("Invalid binaries location.  The binaries are expected to be located within the binaries directory."));
		return Directory(DString::EmptyString, true);
	}

	//Move up two directories since the binaries is located 2 levels within base directory 'Binaries/PlatformName'
	exeLocation = exeLocation.SubString(0, idx - 1); //includes directory separator

	//Collapse directories (ie:  '/../')
	DString results = ToAbsolutePath(exeLocation);

	//Update the working directory to base directory
	if (!OS_SetWorkingDirectory(Directory(results, false)))
	{
		Engine::FindEngine()->FatalError(TXT("Unable to set working directory to:  ") + results);
	}

	return Directory(results, true);
}

Directory Directory::FindBinaryDirectory ()
{
	DString exeLocation;
	OS_GetProcessLocation(OUT exeLocation);
	exeLocation = ToAbsolutePath(exeLocation);

	return Directory(exeLocation, true);
}

void Directory::RemoveInvalidChars (DString& outDirToEdit, bool removeColons)
{
	//Remove invalid characters
	for (UINT_TYPE i = 0; i < FORBIDDEN_CHARACTERS.size(); ++i)
	{
		outDirToEdit.ReplaceInline(FORBIDDEN_CHARACTERS.at(i), DString::EmptyString, DString::CC_CaseSensitive);
	}

#ifdef PLATFORM_WINDOWS
	if (removeColons)
	{
		outDirToEdit.ReplaceInline(TXT(":"), DString::EmptyString, DString::CC_CaseSensitive);
	}
#endif
}

void Directory::SanitizeInternalVariables ()
{
	//Normalize directory separators to platform's specification
	const char incorrectSeparator = (DIRECTORY_SEPARATOR == '/') ? '\\' : '/';
	DirectoryPath.ReplaceInline(incorrectSeparator, DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);

	RemoveInvalidChars(OUT DirectoryPath, false);

	//Remove double slashes
	while (true)
	{
		//Search backwards since the end of the string is more likely to change rather than the beginning (less iteration over the same characters).
		Int doubleSlashIdx = DirectoryPath.Find(DString(DIRECTORY_SEPARATOR) + DIRECTORY_SEPARATOR, 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		if (doubleSlashIdx == INDEX_NONE)
		{
			break;
		}

		//Remove one of the two slashes
		DirectoryPath.Remove(doubleSlashIdx, 1);
	}

#ifdef PLATFORM_WINDOWS
	//For Windows systems, absolute paths should start with a drive letter, colon, and a separator.
	//Find the colon character, and ensure there's only one found at the second character position followed by a separator
	bool hasValidColon = (DirectoryPath.Length() >= 3 && DirectoryPath.SubString(1, 2).Compare(TXT(":") + DIRECTORY_SEPARATOR, DString::CC_CaseSensitive) == 0);

	DirectoryPath.ReplaceInline(TXT(":"), DString::EmptyString, DString::CC_CaseSensitive); //Remove all colons
	if (hasValidColon && DirectoryPath.Length() > 1)
	{
		//Restore the only valid colon character
		DirectoryPath.Insert(1, ':');
	}
#endif

	//Ensure there's a trailing slash.
	if (!DirectoryPath.IsEmpty() && DirectoryPath.At(DirectoryPath.Length() - 1) != DIRECTORY_SEPARATOR)
	{
		DirectoryPath += DIRECTORY_SEPARATOR;
	}
}

void Directory::ComputeDirectoryState () const
{
#ifdef PLATFORM_WINDOWS
	if (DirectoryPath.Length() >= 3)
	{
		bool isAbsPath = (DirectoryPath.FindAt(1) == ':' && DirectoryPath.FindAt(2) == DIRECTORY_SEPARATOR);
		DirectoryState = (isAbsPath) ? DS_AbsolutePath : DS_RelativePath;
		return;
	}
#else
	if (DirectoryPath.Length() >= 2)
	{
		//Must begin with a directory separator to start from root
		bool isAbsPath = (DirectoryPath.FindAt(0) == DIRECTORY_SEPARATOR);
		DirectoryState = (isAbsPath) ? DS_AbsolutePath : DS_RelativePath;
		return;
	}
#endif

	//For really short Directory names, one can only assume that it's a short folder name relative to something else.
	DirectoryState = DS_RelativePath;
}

#pragma region "External Operators"
Directory operator/ (const Directory& left, const Directory& right)
{
	if (right.IsAbsPath())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to combine %s to %s since %s is not a relative path."), right, left, right);
		return left;
	}

	return Directory(left.ReadDirectoryPath() + right.ReadDirectoryPath(), left.IsAutoFormatting());
}

Directory operator/ (const Directory& left, const DString& right)
{
	return Directory(left.ReadDirectoryPath() + right + Directory::DIRECTORY_SEPARATOR, left.IsAutoFormatting());
}

Directory operator/ (const Directory& left, const char* right)
{
	return left / DString(right);
}

Directory operator/ (const DString& left, const Directory& right)
{
	if (right.IsAbsPath())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to combine %s to %s since %s is not a relative path."), right, left, right);
		return right;
	}

	return Directory(left + Directory::DIRECTORY_SEPARATOR + right.ReadDirectoryPath(), right.IsAutoFormatting());
}

Directory operator/ (const DString& left, const DString& right)
{
	return Directory(left + Directory::DIRECTORY_SEPARATOR + right + Directory::DIRECTORY_SEPARATOR, true);
}

Directory operator/ (const DString& left, const char* right)
{
	return left / DString(right);
}

Directory operator/ (const char* left, const Directory& right)
{
	return DString(left) / right;
}

Directory operator/ (const char* left, const DString& right)
{
	return DString(left) / right;
}

Directory& operator/= (Directory& left, const Directory& right)
{
	if (right.IsAbsPath())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to combine %s to %s since %s is not a relative path."), right, left, right);
		return left;
	}

	Int startIdx = 0;
	if (right.ReadDirectoryPath().FindAt(0) == Directory::DIRECTORY_SEPARATOR)
	{
		startIdx = 1; //Don't include leading slash since that would cause a double slash (first directory always end in a slash).
	}

	left.SetDirectory(left.ReadDirectoryPath() + right.ReadDirectoryPath().SubString(startIdx));
	return left;
}

Directory& operator/= (Directory& left, const DString& right)
{
	left.SetDirectory(left.ReadDirectoryPath() + right + Directory::DIRECTORY_SEPARATOR);
	return left;
}

Directory& operator/= (Directory& left, const char* right)
{
	return left /= DString(right);
}
#pragma endregion
SD_END