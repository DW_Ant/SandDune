/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileAttributes.cpp
=====================================================================
*/

#include "AdvFileAttributes.h"
#include "FileAttributes.h"
#include "FileUtils.h"

SD_BEGIN
FileAttributes::FileAttributes ()
{
	FileName = DString::EmptyString;
	FileExtension = DString::EmptyString;
}

FileAttributes::FileAttributes (const FilePtr& fileHandle)
{
	DString fullFileName;
	DString pathName;
	OS_GetFileName(fileHandle, OUT pathName, OUT fullFileName);
	Path = pathName;
	FileUtils::ExtractFileExtension(fullFileName, OUT FileName, OUT FileExtension);
}

FileAttributes::FileAttributes (const DString& inPath, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = inPath;
}

FileAttributes::FileAttributes (const Directory& dir, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = dir;
}

FileAttributes::FileAttributes (const FileAttributes& attributesCopy)
{
	FileName = attributesCopy.FileName;
	FileExtension = attributesCopy.FileExtension;
	Path = attributesCopy.Path;
}

FileAttributes::FileAttributes (const AdvFileAttributes& attributesCopy)
{
	FileName = attributesCopy.FileName;
	FileExtension = attributesCopy.FileExtension;
	Path = attributesCopy.Path;
}

FileAttributes::~FileAttributes ()
{
	//Noop
}

bool FileAttributes::operator== (const FileAttributes& other) const
{
	//Use absolute paths since there are infinitely many ways to represent a particular path using relative paths.
	return ((Path.GetAbsPath().ReadDirectoryPath() + FileName + FileExtension) == (other.Path.GetAbsPath().ReadDirectoryPath() + other.FileName + other.FileExtension));
}

bool FileAttributes::operator!= (const FileAttributes& other) const
{
	return !(*this == other);
}

void FileAttributes::ResetToDefaults ()
{
	FileName = DString::EmptyString;
	Path.ResetToDefaults();
	FileExtension = DString::EmptyString;
}

DString FileAttributes::ToString () const
{
	return DString::CreateFormattedString(TXT("Path=\"%s\", FileName=\"%s\", FileExtension=\"%s\""), Path, FileName, FileExtension);
}

void FileAttributes::ParseString (const DString& str)
{
	Path = ParseVariable(str, TXT("Path"), CommonPropertyRegex::STRING_LAZY);
	FileName = ParseVariable(str, TXT("FileName"), CommonPropertyRegex::STRING_LAZY);
	FileExtension = ParseVariable(str, TXT("FileExtension"), CommonPropertyRegex::STRING_LAZY);
}

size_t FileAttributes::GetMinBytes () const
{
	//FileName, FileExtention
	return (DString::SGetMinBytes() * 2) + Path.GetMinBytes();
}

void FileAttributes::Serialize (DataBuffer& outData) const
{
	outData << Path;

	DString fullFileName = GetName(false, true);
	outData << fullFileName;
}

bool FileAttributes::Deserialize (const DataBuffer& dataBuffer)
{
	DString path;
	DString fullFileName;
	dataBuffer >> path >> fullFileName;
	if (dataBuffer.HasReadError())
	{
		return false;
	}

	//This will initialize all other properties in this class and subclasses (due to virtual PopulateFileInfo function).
	SetFileName(path, fullFileName);

	return true;
}

DString FileAttributes::GetName (bool includePath, bool includeExtension) const
{
	DString results = DString::EmptyString;
	if (includePath)
	{
		results = Path.GetDirectoryPath();
	}

	results += FileName;

	if (includeExtension && !FileName.IsEmpty() && !FileExtension.IsEmpty())
	{
		results += TXT(".") + FileExtension;
	}

	return results;
}

void FileAttributes::ClearValues ()
{
	FileName = DString::EmptyString;
	FileExtension = DString::EmptyString;
	Path = DString::EmptyString;
}

bool FileAttributes::SetFileName (const DString& path, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = path;
	PopulateFileInfo();

	return OS_CheckIfFileExists(*this);
}

bool FileAttributes::GetFileExists () const
{
	return OS_CheckIfFileExists(*this);
}

void FileAttributes::PopulateFileInfo ()
{
	if (!OS_CheckIfFileExists(Path.ReadDirectoryPath(), GetName(false, true)))
	{
		HandleMissingFile();
	}
}

void FileAttributes::PopulateFileInfoFromHandle (FilePtr fileHandle)
{
	DString fullFileName;
	DString absPath;
	OS_GetFileName(fileHandle, OUT absPath, OUT fullFileName);
	Path = absPath;
	FileUtils::ExtractFileExtension(fullFileName, OUT FileName, OUT FileExtension);
}

void FileAttributes::HandleMissingFile ()
{
	FileLog.Log(LogCategory::LL_Log, FileName + TXT(" does not exist."));
}
SD_END