/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FileIterator.cpp
=====================================================================
*/

#include "AdvFileAttributes.h"
#include "Directory.h"
#include "FileIterator.h"

SD_BEGIN
FileIterator::FileIterator (const Directory& inBaseDirectory) :
	SearchFlags(SF_IncludeFiles | SF_IncludeDirectories | SF_IncludeSubDirs),
	PreviousExpandedDirectory(DString::EmptyString, false),
	BaseDirectory(inBaseDirectory)
{
	BaseDirectory.ConvertToAbsPath();
	SelectFirstFile();
}

FileIterator::FileIterator (const Directory& inBaseDirectory, ESearchFlags inSearchFlags) :
	SearchFlags(inSearchFlags),
	PreviousExpandedDirectory(DString::EmptyString, false),
	BaseDirectory(inBaseDirectory)
{
	BaseDirectory.ConvertToAbsPath();
	SelectFirstFile();
}

FileIterator::~FileIterator ()
{
	if (SubIterator != nullptr)
	{
		delete SubIterator;
		SubIterator = nullptr;
	}

#ifdef PLATFORM_WINDOWS
	if (InitialFile != nullptr)
	{
		FindClose(InitialFile);
		InitialFile = nullptr;
	}
#else
	OS_CloseFile(InitialFile);
#endif
}

void FileIterator::operator++ ()
{
	SelectNextFile();
}

void FileIterator::operator++ (int)
{
	SelectNextFile();
}

AdvFileAttributes FileIterator::RetrieveFileAttributes () const
{
	return AdvFileAttributes(GetSelectedAttributes());
}

bool FileIterator::IsDirectorySelected () const
{
	return (SubIterator != nullptr) ? SubIterator->IsDirectorySelected() : !SelectedDirectory.ReadDirectoryPath().IsEmpty();
}

bool FileIterator::FinishedIterating () const
{
	return (SubIterator == nullptr && SelectedDirectory.ReadDirectoryPath().IsEmpty() && !SelectedAttributes.GetFileExists());
}

FileAttributes FileIterator::GetSelectedAttributes () const
{
	return (SubIterator != nullptr) ? SubIterator->GetSelectedAttributes() : SelectedAttributes;
}

const FileAttributes& FileIterator::ReadSelectedAttributes () const
{
	return (SubIterator != nullptr) ? SubIterator->ReadSelectedAttributes() : SelectedAttributes;
}

Directory FileIterator::GetSelectedDirectory () const
{
	return (SubIterator != nullptr) ? SubIterator->GetSelectedDirectory() : SelectedDirectory;
}

const Directory& FileIterator::ReadSelectedDirectory () const
{
	return (SubIterator != nullptr) ? SubIterator->ReadSelectedDirectory() : SelectedDirectory;
}

void FileIterator::SelectFirstFile ()
{
	SelectedDirectory = DString::EmptyString;
	SelectedAttributes = FileAttributes();
	SubIterator = nullptr;

#ifdef PLATFORM_WINDOWS
	/**
	Since Windows has a terrible implementation for file iterators (ie:  Inconsistent params and return
	values for FindFirstFile and FindNextFile, and being overly dependant on MS-specific structs and
	flags); it's cleaner to have the iterator handle the file selection than extracting functionality
	to platform independent utility functions, which was implemented earlier (but was messy).
	*/
	//Don't use Directory objects.  It would strip the * and add a trailing slash.  We can't have that for FindFirstFile function.
	DString searchQuery = BaseDirectory.ReadDirectoryPath() + TXT("*");
	InitialFile = FindFirstFile(searchQuery.ToCString(), &WindowsFindData);
	if (InitialFile == INVALID_HANDLE_VALUE)
	{
		return; //Nothing was found
	}

	if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		if ((SearchFlags & SF_IncludeDirectories) == 0)
		{
			SelectNextFile(); //Skip directories
			return;
		}

		if ((SearchFlags & SF_IncludeReflectiveDirs) == 0 && OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
		{
			SelectNextFile(); //Skip reflective directories
			return;
		}

		SelectedDirectory = BaseDirectory / WindowsFindData.cFileName;
		return;
	}
	else
	{
		if ((SearchFlags & SF_IncludeFiles) == 0)
		{
			SelectNextFile(); //Skip files
			return;
		}

		SelectedAttributes = FileAttributes(InitialFile);
	}
#else
	#error Please implement SelectFirstFile for your platform.
#endif
}

void FileIterator::SelectNextFile ()
{
	if (SubIterator != nullptr)
	{
		SubIterator->SelectNextFile();
		if (!SubIterator->FinishedIterating())
		{
			return;
		}

		//finished iterating through sub directory.  Carry on with this iterator
		delete SubIterator;
		SubIterator = nullptr;
	}

	//Check if it needs to search sub directory.  This is implemented outside of loop to have the parent directory listed before subdirectories.
	if ((SearchFlags & SF_IncludeSubDirs) > 0 && !SelectedDirectory.ReadDirectoryPath().IsEmpty() && SelectedDirectory != PreviousExpandedDirectory && !OS_IsDirectoryReflection(SelectedDirectory))
	{
		//File iterator found a directory that wasn't investigated yet.
		SubIterator = new FileIterator(SelectedDirectory, SearchFlags);
		if (!SubIterator->FinishedIterating())
		{
			PreviousExpandedDirectory = SelectedDirectory;
			return; //Using sub iterator until it finishes
		}

		delete SubIterator;
		SubIterator = nullptr;
		//Nothing in the sub directory. Fall through and find next file
	}

#ifdef PLATFORM_WINDOWS
	if (FindNextFile(InitialFile, &WindowsFindData))
	{
		if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if ((SearchFlags & SF_IncludeDirectories) == 0)
			{
				if ((SearchFlags & SF_IncludeSubDirs) > 0 && !OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
				{
					SubIterator = new FileIterator(BaseDirectory / WindowsFindData.cFileName, SearchFlags);
					if (!SubIterator->FinishedIterating())
					{
						return;
					}

					//The sub iterator found nothing. Proceed as if we're skipping that sub directory.
					delete SubIterator;
					SubIterator = nullptr;
				}

				SelectNextFile(); //Skip directories
				return;
			}

			if ((SearchFlags & SF_IncludeReflectiveDirs) == 0 && OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
			{
				SelectNextFile(); //Skip reflective directories
				return;
			}

			SelectedAttributes = FileAttributes();
			SelectedDirectory = BaseDirectory / WindowsFindData.cFileName;
		}
		else
		{
			if ((SearchFlags & SF_IncludeFiles) == 0)
			{
				SelectNextFile(); //Skip files
				return;
			}

			SelectedAttributes = FileAttributes(BaseDirectory, WindowsFindData.cFileName);
			SelectedDirectory = DString::EmptyString;
		}
	}
	else //Nothing is found
	{
		SelectedDirectory = DString::EmptyString;
		SelectedAttributes = FileAttributes();
	}
#else
	#error Please implement SelectNextFile for your platform.
#endif
}
SD_END