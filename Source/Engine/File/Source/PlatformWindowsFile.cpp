/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsFile.cpp
=====================================================================
*/

#include "Directory.h"
#include "File.h"
#include "FileAttributes.h"
#include "FileIterator.h"
#include "FileUtils.h"
#include "FileDialogueOptions.h"
#include "LaunchProcessOptions.h"
#include "PlatformWindowsFile.h"
#include "ProcessInfo.h"

#define FLOAT MS_FLOAT
#define INT MS_INT
#include "shobjidl_core.h" //For the file dialogue
#include <atlbase.h> // Contains the declaration of CComPtr.
#undef FLOAT
#undef INT

#ifdef PLATFORM_WINDOWS

SD_BEGIN
DString OS_FullPath (const DString& relativePath)
{
	TCHAR fullPath[_MAX_PATH];

#if USE_WIDE_STRINGS
	if (_wfullpath(fullPath, relativePath.ToCString(), _MAX_PATH) != NULL)
#else
	if (_fullpath(fullPath, relativePath.ToCString(), _MAX_PATH) != NULL)
#endif
	{
		return DString(fullPath);
	}

#ifdef DEBUG_MODE
	FileLog.Log(LogCategory::LL_Warning, TXT("Unable to find absolute path from the given relativePath (%s)."), relativePath);
#endif
	return DString::EmptyString;
}

bool OS_SetWorkingDirectory (const Directory& newWorkingDirectory)
{
	LPCTSTR winWorkingDirectory = newWorkingDirectory.ReadDirectoryPath().ToCString();

	return (SetCurrentDirectory(winWorkingDirectory) != 0);
}

FilePtr OS_GetFileHandle (const FileAttributes& attributes)
{
	if (!attributes.GetFileExists())
	{
		return nullptr;
	}

	return OS_GetFileHandle(attributes.ReadPath().ReadDirectoryPath(), attributes.GetName(false, true));
}

FilePtr OS_GetFileHandle (const DString& absDirectory, const DString& fullFileName)
{
	// Do I seriously have to call CreateFile to get a handle of an existing file!?

	DString scopedString(absDirectory + fullFileName);
	LPCTSTR lpFileName = (scopedString).ToCString();
	HANDLE fileHandle = CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to open file named %s.  Error code:  %s"), scopedString, DString::MakeString(GetLastError()));
		return nullptr;
	}

	return fileHandle;
}

FilePtr OS_CreateFile (const Directory& path, const DString& fullFileName)
{
	DString scopedString(path.GetAbsPath().ReadDirectoryPath() + fullFileName);
	LPCTSTR lpFileName = (scopedString).ToCString();
	HANDLE fileHandle = CreateFile(lpFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to create file named:  %s.  Error code:  %s"), scopedString, DString::MakeString(GetLastError()));
		return nullptr;
	}

	return fileHandle;
}

bool OS_WriteToFile (FilePtr fileHandle, const void* values, int numBytes)
{
	DWORD bytesWritten = 0; //not used
	return (WriteFile(fileHandle, values, numBytes, &bytesWritten, NULL) != 0);
}

bool OS_CopyFile (const FileAttributes& fileSource, const Directory& destinationPath, const DString& destName, bool bOverwriteExisting)
{
	if (!fileSource.GetFileExists())
	{
		return false;
	}

	bool bFailIfExist = !bOverwriteExisting;
	//Use MS's implementation to ensure we're not overriding existing file.
	DString scopedStringSrc = fileSource.GetName(true, true);
	DString scopedStringDest = destinationPath.ReadDirectoryPath() + destName;
	LPCTSTR source = scopedStringSrc.ToCString();
	LPCTSTR destination = scopedStringDest.ToCString();
	if (!CopyFile(source, destination, bFailIfExist))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to copy %s to %s.  Error code:  %s"), scopedStringSrc, scopedStringDest, DString::MakeString(GetLastError()));
		return false;
	}

	return true;
}

bool FILE_API OS_DeleteFile (const FileAttributes& target)
{
	return OS_DeleteFile(target.ReadPath().GetAbsPath().ReadDirectoryPath(), target.GetName(false, true));
}

bool OS_DeleteFile (const DString& absPath, const DString& fullFileName)
{
	DString scopedFile(absPath + fullFileName);
	LPCTSTR fullAddress = (scopedFile).ToCString();

	if (DeleteFile(fullAddress))
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Deleted file:  %s"), absPath + fullFileName);
		return true;
	}

	FileLog.Log(LogCategory::LL_Warning, TXT("Attempted to delete file:  %s.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
	return false;
}

void OS_CloseFile (FilePtr& outHandle)
{
	if (outHandle != nullptr)
	{
		CloseHandle(outHandle);
		outHandle = nullptr;
	}
}

bool OS_CheckIfFileExists (const FileAttributes& file)
{
	return OS_CheckIfFileExists(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

bool OS_CheckIfFileExists (const DString& absDirectory, const DString& fullFileName)
{
	if (fullFileName.IsEmpty())
	{
		//Early out.  Also if the directory is given without a file name returns FILE_ATTRIBUTE_DIRECTORY.  This function only cares if a file exists or not.
		return false;
	}

	DString scopedString(absDirectory + fullFileName);
	LPCTSTR microsoftPath = (scopedString).ToCString();
	DWORD fileAttributes = GetFileAttributes(microsoftPath);

	return (fileAttributes != INVALID_FILE_ATTRIBUTES);
}

bool FILE_API OS_CheckIfDirectoryExists (const Directory& dir)
{
	return OS_CheckIfDirectoryExists(dir.ToString());
}

bool FILE_API OS_CheckIfDirectoryExists (const DString& dir)
{
	DWORD fileAttr = GetFileAttributes(StrToLPCSTR(dir));
	
	if (fileAttr == INVALID_FILE_ATTRIBUTES)
	{
		//Path doesn't exist or is invalid
		return false;
	}

	return ((fileAttr & FILE_ATTRIBUTE_DIRECTORY) > 0);
}

bool FILE_API OS_CreateDirectory (const Directory& dir)
{
	Directory parentDir;
	if (dir.GetParentDir(parentDir))
	{
		if (!OS_CheckIfDirectoryExists(parentDir))
		{
			//Need to create the parent directory before this directory can be created.
			if (!OS_CreateDirectory(parentDir))
			{
				return false;
			}
		}
	}

	DString dirName = dir.ToString();
	if (!CreateDirectoryA(StrToLPCSTR(dirName), NULL))
	{
		DWORD errorCode = GetLastError();
		if (errorCode == ERROR_ALREADY_EXISTS)
		{
			return true;
		}
		else if (errorCode == ERROR_PATH_NOT_FOUND)
		{
			FileLog.Log(LogCategory::LL_Log, TXT("Failed to create directory %s. The path is not found."), dirName);
		}

		return false;
	}

	return true;
}

bool FILE_API OS_DeleteDirectory (const Directory& dir)
{
	if (!dir.Exists())
	{
		return true; //already deleted
	}

	//Ensure there aren't any files within the directory tree.
	for (FileIterator iter(dir, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Failed to delete directory %s since there the file %s exists within that directory tree."), dir.ToString(), iter.GetSelectedAttributes().GetName(true, true));
		return false;
	}

	//Safe to delete every sub directory within dir.
	std::vector<Directory> subDirs;
	for (FileIterator iter(dir, FileIterator::SF_IncludeDirectories | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		subDirs.push_back(iter.GetSelectedDirectory());
	}

	//Ensure every sub directory is deleted before this directory is deleted
	for (size_t i = 0; i < subDirs.size(); ++i)
	{
		if (subDirs.at(i) != dir)
		{
			if (!OS_DeleteDirectory(subDirs.at(i)))
			{
				return false;
			}
		}
	}

	DString dirName = dir.ToString();
	if (!RemoveDirectoryA(StrToLPCSTR(dirName)))
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Failed to delete directory %s. Error code: %s"), dirName, DString::MakeString(GetLastError()));
		return false;
	}

	return true;
}

unsigned long long OS_GetFileSize (const FileAttributes& file)
{
	return OS_GetFileSize(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

unsigned long long OS_GetFileSize (const DString& absPath, const DString& fullFileName)
{
	if (!OS_CheckIfFileExists(absPath, fullFileName))
	{
		return 0;
	}

	HANDLE fileHandle = OS_GetFileHandle(absPath, fullFileName);
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain file size for:  %s.  Could not gain a handle to file.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return 0;
	}

	LARGE_INTEGER fileSize;
	if (!GetFileSizeEx(fileHandle, &fileSize))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain file size for:  %s.  Could not read file size.   Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		CloseHandle(fileHandle);
		return 0;
	}

	OS_CloseFile(fileHandle);

	return fileSize.QuadPart;
}

bool OS_IsFileReadOnly (const FileAttributes& file)
{
	return OS_IsFileReadOnly(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

bool FILE_API OS_IsFileReadOnly (const DString& absPath, const DString& fullFileName)
{
	DString scopedFile(absPath + fullFileName);
	LPCTSTR lpFileName = (scopedFile).ToCString();
	DWORD fileAttributes = GetFileAttributes(lpFileName);

	if (fileAttributes == INVALID_FILE_ATTRIBUTES)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to check if the file (%s) is read only or not.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	return ((fileAttributes & FILE_ATTRIBUTE_READONLY) > 0);
}

bool OS_GetFileTimestamps (const FileAttributes& file, DateTime& outCreatedDate, DateTime& outModifiedDate)
{
	return OS_GetFileTimestamps(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true), OUT outCreatedDate, OUT outModifiedDate);
}

bool FILE_API OS_GetFileTimestamps (const DString& absPath, const DString& fullFileName, DateTime& outCreatedDate, DateTime& outModifiedDate)
{
	FILETIME createdTime;
	FILETIME accessedTime;
	FILETIME writeTime;
	HANDLE fileHandle = OS_GetFileHandle(absPath, fullFileName);
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since GetFileTimestamps could not get a handle to the file."), absPath + fullFileName);
		return false;
	}

	if (!GetFileTime(fileHandle, &createdTime, &accessedTime, &writeTime))
	{
		OS_CloseFile(fileHandle);
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s.  System could not get file time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	OS_CloseFile(fileHandle);

	//break apart the file time
	SYSTEMTIME createdSystemTime;
	SYSTEMTIME writeSystemTime;

	if (!FileTimeToSystemTime(&createdTime, &createdSystemTime))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since system could not convert file time to system time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	if (!FileTimeToSystemTime(&writeTime, &writeSystemTime))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since system could not convert file time to system time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	//Convert LPSYSTEMTIME to DateTime
	outCreatedDate.Second = createdSystemTime.wSecond;
	outCreatedDate.Minute = createdSystemTime.wMinute;
	outCreatedDate.Hour = createdSystemTime.wHour;
	outCreatedDate.Day = createdSystemTime.wDay - 1; //SystemTime range from 1-31.  DateTime range from 0-30.
	outCreatedDate.Month = createdSystemTime.wMonth - 1; //SystemTime range from 1-12.  DateTime range from 0-11.
	outCreatedDate.Year = createdSystemTime.wYear;

	outModifiedDate.Second = writeSystemTime.wSecond;
	outModifiedDate.Minute = writeSystemTime.wMinute;
	outModifiedDate.Hour = writeSystemTime.wHour;
	outModifiedDate.Day = writeSystemTime.wDay - 1;
	outModifiedDate.Month = writeSystemTime.wMonth - 1;
	outModifiedDate.Year = createdSystemTime.wYear;

	return true;
}

void OS_GetFileName (FilePtr fileHandle, DString& outAbsPath, DString& outFullFileName)
{
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Attempted to get a file name from an invalid file handle."));
		return;
	}

	TCHAR msPath[BUFSIZ];
	DWORD resultLength;
	//resultLength = GetFinalPathNameByHandle(fileHandle, msPath, BUFSIZ, FILE_NAME_NORMALIZED);
	resultLength = GetFinalPathNameByHandle(fileHandle, OUT msPath, BUFSIZ, VOLUME_NAME_NONE);
	if (resultLength >= BUFSIZ)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Could not get file name from a handle.  The required buffer size is:  %s"), DString::MakeString(resultLength));
		return;
	}

	if (resultLength <= 0)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Could not get file name from handle.  Error code:  %s"), DString::MakeString(GetLastError()));
		return;
	}

	DString fullAddress = msPath;
	FileUtils::ExtractFileName(fullAddress, OUT outAbsPath, OUT outFullFileName);
}

void OS_GetProcessLocation (DString& outPath, DString& outFileName)
{
	DWORD bufferSize = MAX_PATH;
	TCHAR fileName[MAX_PATH];
	DWORD strLength = GetModuleFileName(NULL, fileName, bufferSize);

	DString fullName(TString(fileName, strLength));

	//Split the directory from the executable path.
	FileUtils::ExtractFileName(fullName, OUT outPath, OUT outFileName);
}

void OS_GetProcessLocation (DString& outPath)
{
	DString fileName; //Not used
	OS_GetProcessLocation(OUT outPath, OUT fileName);
}

bool OS_IsDirectoryReflection (const Directory& dir)
{
	DString innerDir = dir.GetInnerMostDirectory();
	return (innerDir == TXT(".") || innerDir == TXT(".."));
}

bool OS_BrowseFiles (const FileDialogueOptions& options, std::vector<FileAttributes>& outSelectedFiles)
{
	IFileOpenDialog *openDialogue = NULL;
	IFileSaveDialog *saveDialogue = NULL;
	IFileDialog* dialogue = NULL; //points to either open or save dialogue
	HRESULT result;

	switch (options.DialogueType)
	{
		case(FileDialogueOptions::DT_Open):
		case(FileDialogueOptions::DT_SelectDir):
			result = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&openDialogue));
			dialogue = openDialogue;
			break;

		case(FileDialogueOptions::DT_Save):
			result = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&saveDialogue));
			dialogue = saveDialogue;
			break;
	}

	if (!SUCCEEDED(result))
	{
		return false;
	}

	DWORD dialogueOptions;
	result = dialogue->GetOptions(&dialogueOptions);
	if (!SUCCEEDED(result))
	{
		dialogue->Release();
		return false;
	}

	dialogueOptions |= FOS_NOCHANGEDIR; //Never change the current working directory. Call OS_SetWorkingDirectory if needed.
	dialogueOptions |= FOS_FORCEFILESYSTEM;
	dialogueOptions |= FOS_PATHMUSTEXIST;

	if (options.bMultiSelect)
	{
		dialogueOptions |= FOS_ALLOWMULTISELECT;
	}

	switch (options.DialogueType)
	{
		case(FileDialogueOptions::DT_Open):
			dialogueOptions |= FOS_FILEMUSTEXIST;
			break;

		case(FileDialogueOptions::DT_SelectDir):
			dialogueOptions |= FOS_PICKFOLDERS;
			break;

		case(FileDialogueOptions::DT_Save):
			dialogueOptions |= FOS_OVERWRITEPROMPT | FOS_NOREADONLYRETURN;

			if (options.bStrictFileTypesOnly)
			{
				dialogueOptions |= FOS_STRICTFILETYPES;
			}

			break;
	}

	result = dialogue->SetOptions(dialogueOptions);
	if (!SUCCEEDED(result))
	{
		dialogue->Release();
		return false;
	}

	if (options.PersistenceId != 0)
	{
		GUID guid;
#ifdef PLATFORM_64BIT
		//Convert a 64-bit int to Guid (Data1=unsigned long (4 bytes), unsigned short (2 bytes), and another unsigned short)
		{
			uint64 bits = options.PersistenceId.Value;
			bits >>= 32;
			guid.Data1 = static_cast<unsigned long>(bits);

			bits = options.PersistenceId.Value;
			bits &= 0xFFFFFFFF;
			bits >>= 16;

			uint16 trailPart = static_cast<uint16>(bits);
			guid.Data2 = trailPart;

			bits = options.PersistenceId.Value;
			bits &= 0xFFFFFFFF;
			trailPart = static_cast<uint16>(bits);
			guid.Data3 = trailPart;
		}
#else
		guid.Data1 = options.PersistenceId.ToUnsignedInt32(); //Everything can fit in the first Data
		guid.Data2 = 0;
		guid.Data3 = 0;
#endif
	
		//Zero everything else
		for (int i = 0; i < 8; ++i)
		{
			guid.Data4[i] = 0;
		}

		result = dialogue->SetClientGuid(guid);
		if (!SUCCEEDED(result))
		{
			dialogue->Release();
			return false;
		}
	}

	//Convert Directory to IShellItem
	CComPtr<IShellItem> shellFolder;
	result = SHCreateItemFromParsingName(StrToLPCWSTR(options.DefaultFolder.ToString()), NULL, IID_PPV_ARGS(&shellFolder));
	if (!SUCCEEDED(result))
	{
		dialogue->Release();
		return false;
	}

	result = dialogue->SetDefaultFolder(shellFolder);
	if (!SUCCEEDED(result))
	{
		dialogue->Release();
		return false;
	}

	if (options.DialogueType != FileDialogueOptions::DT_SelectDir)
	{
		if (ContainerUtils::IsEmpty(options.FileTypes))
		{
			COMDLG_FILTERSPEC filterSpec[]
			{
				{L"All", L"*.*"}
			};

			result = dialogue->SetFileTypes(ARRAYSIZE(filterSpec), filterSpec);
		}
		else
		{
			DString fileList = DString::EmptyString;
			for (const DString& type : options.FileTypes)
			{
				fileList += TXT("*.") + type + TXT(";");
			}
			fileList.PopBack(); //Remove trailing semicolon

			std::wstring wStr = fileList.ToWideStringInUTF16();
			COMDLG_FILTERSPEC filterSpec[]
			{
				{L"File Types", wStr.c_str()}
			};

			result = dialogue->SetFileTypes(ARRAYSIZE(filterSpec), filterSpec);
		}

		if (!SUCCEEDED(result))
		{
			dialogue->Release();
			return false;
		}
	}

	//Freeze the thread and show the dialogue
	result = dialogue->Show(NULL);
	if (!SUCCEEDED(result))
	{
		DWORD resultCode = HRESULT_CODE(result);
		dialogue->Release();
		return (resultCode == ERROR_CANCELLED);
	}

	//Utility to obtain FileAttributes from an IShellItem. Return false if there was an error.
	std::function<bool(IShellItem*, FileAttributes&)> shellToFileAttr([&options](IShellItem* shellItem, FileAttributes& outFileAttr)
	{
		LPWSTR winFileName;
		HRESULT conversionResult = shellItem->GetDisplayName(SIGDN_DESKTOPABSOLUTEEDITING, &winFileName);
		if (!SUCCEEDED(conversionResult))
		{
			return false;
		}

		std::wstring wstr(winFileName);
		DString fullName;
		fullName.FromWideString(wstr);

		if (options.DialogueType != FileDialogueOptions::DT_SelectDir)
		{
			DString path;
			DString fileName;
			FileUtils::ExtractFileName(fullName, OUT path, OUT fileName);

			outFileAttr = FileAttributes(path, fileName);
		}
		else
		{
			//The fullName IS the path
			outFileAttr = FileAttributes(fullName, DString::EmptyString);
		}

		//Needed for DT_SelectDir since the path may not end with a trailing slash. This is needed to ensure the last path is not interpreted as a file.
		outFileAttr.EditPath().SetAutoFormat(true);

		return true;
	});

	if (options.DialogueType == FileDialogueOptions::DT_Open || options.DialogueType == FileDialogueOptions::DT_SelectDir)
	{
		IShellItemArray* userSelection;
		result = openDialogue->GetResults(&userSelection);
		if (!SUCCEEDED(result))
		{
			dialogue->Release();
			return false;
		}

		DWORD numSelectedItems;
		result = userSelection->GetCount(&numSelectedItems);
		if (!SUCCEEDED(result))
		{
			userSelection->Release();
			dialogue->Release();
			return false;
		}

		for (DWORD i = 0; i < numSelectedItems; ++i)
		{
			IShellItem* selectedItem;
			result = userSelection->GetItemAt(i, &selectedItem);
			if (!SUCCEEDED(result))
			{
				userSelection->Release();
				dialogue->Release();
				return false;
			}

			FileAttributes selectedFile;
			if (!shellToFileAttr(selectedItem, OUT selectedFile))
			{
				userSelection->Release();
				dialogue->Release();
				return false;
			}

			outSelectedFiles.emplace_back(selectedFile);
		}

		userSelection->Release();
	}
	else if (options.DialogueType == FileDialogueOptions::DT_Save)
	{
		IShellItem* userSelection;
		result = saveDialogue->GetResult(&userSelection);
		if (!SUCCEEDED(result))
		{
			dialogue->Release();
			return false;
		}

		FileAttributes selectedFile;
		if (!shellToFileAttr(userSelection, OUT selectedFile))
		{
			userSelection->Release();
			dialogue->Release();
			return false;
		}

		outSelectedFiles.emplace_back(selectedFile);
		userSelection->Release();
	}

	dialogue->Release();
	return true;
}

bool OS_BrowseDirectory (Directory defaultDirectory, Directory& outSelectedDirectory, Int persistenceId)
{
	outSelectedDirectory = Directory::INVALID_DIRECTORY;

	FileDialogueOptions options(FileDialogueOptions::DT_SelectDir);
	options.DefaultFolder = defaultDirectory;
	options.PersistenceId = persistenceId;
	std::vector<FileAttributes> selectedFiles;

	bool bResult = OS_BrowseFiles(options, OUT selectedFiles);
	if (bResult)
	{
		if (selectedFiles.size() > 0)
		{
			outSelectedDirectory = selectedFiles.at(0).GetPath();
		}
	}

	return bResult;
}

bool OS_LaunchProcess (const LaunchProcessOptions& launchOptions, ProcessInfo& outProcessInfo)
{
	if (outProcessInfo.bHasHandles)
	{
		outProcessInfo.ReleaseHandles();
	}

	if (!OS_CheckIfFileExists(launchOptions.ExecFile))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot launch %s since that file does not exist."), launchOptions.ExecFile.GetName(true, true));
		return false;
	}

	LPSTR msCmdArgs = strdup(launchOptions.CmdLineArgs.ToCString()); //Create a copy since the 2nd parameter needs write access.
	DWORD creationFlags = 0;
	if (!launchOptions.bChildProcess)
	{
		creationFlags |= CREATE_NEW_PROCESS_GROUP | DETACHED_PROCESS;
	}

	BOOL bSuccess = CreateProcess(StrToLPCSTR(launchOptions.ExecFile.GetName(true, true)),
		msCmdArgs,
		NULL,
		NULL,
		launchOptions.bChildProcess,
		creationFlags,
		NULL,
		NULL,
		&outProcessInfo.OS_StartInfo,
		&outProcessInfo.OS_ProcessInfo);

	if (bSuccess)
	{
		outProcessInfo.ExecFile = launchOptions.ExecFile;
		outProcessInfo.bHasHandles = true;

		//Copy variables to the OS agnostic vars.
		outProcessInfo.Handle = outProcessInfo.OS_ProcessInfo.hProcess;
		outProcessInfo.ProcessId = static_cast<INT_TYPE>(outProcessInfo.OS_ProcessInfo.dwProcessId);
	}
	else
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Failed to launch the %s process. Error code: %s."), launchOptions.ExecFile.GetName(true, true), DString::MakeString(GetLastError()));
	}

	free(msCmdArgs);
	return bSuccess;
}

bool OS_WaitForProcessFinish (const ProcessInfo& procInfo, float maxWaitTime)
{
	DWORD msWaitTime = INFINITE;
	if (maxWaitTime > 0.f)
	{
		//Convert from seconds to ms
		maxWaitTime *= 1000.f;

		msWaitTime = static_cast<DWORD>(maxWaitTime);
	}

	DWORD result = WaitForSingleObject(procInfo.Handle, msWaitTime);
	if (result == WAIT_FAILED)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("WaitForProcessFinish failed. Calling WaitForSingleObject returned an error. Error code: %s."), DString::MakeString(GetLastError()));
		return false;
	}

	return (result != WAIT_TIMEOUT);
}

bool OS_IsProcessAlive (const ProcessInfo& procInfo)
{
	return (WaitForSingleObject(procInfo.Handle, 0) == WAIT_TIMEOUT);
}

bool OS_KillProcess (const ProcessInfo& procInfo, int exitCode)
{
	UINT msExitCode = static_cast<UINT>(exitCode);
	bool bSuccess = TerminateProcess(procInfo.Handle, msExitCode);
	if (!bSuccess)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Failed to kill %s. Error code: %s"), procInfo.ExecFile.GetName(true, true), DString::MakeString(GetLastError()));
	}

	return bSuccess;
}

bool OS_GetExitCode (const ProcessInfo& procInfo, int& outExitCode)
{
	//Use WaitForSingleObject instead of checking if it's STILL_ACTIVE just in case the process's exit code is 259 (which is equal to STILL_ACTIVE).
	if (OS_IsProcessAlive(procInfo))
	{
		return false;
	}

	DWORD msExitCode;
	if (!GetExitCodeProcess(procInfo.Handle, &msExitCode))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Failed to get the exit code for %s. Error code: %s"), procInfo.ExecFile.GetName(true, true), DString::MakeString(GetLastError()));
		return false;
	}
	else
	{
		outExitCode = static_cast<int>(msExitCode);
		return true;
	}
}
SD_END
#endif //PLATFORM_WINDOWS