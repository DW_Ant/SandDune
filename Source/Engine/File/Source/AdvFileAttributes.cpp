/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AdvFileAttributes.cpp
=====================================================================
*/

#include "FileClasses.h"

SD_BEGIN
AdvFileAttributes::AdvFileAttributes () : FileAttributes()
{
	bReadOnly = false;
	FileSize = 0;
}

AdvFileAttributes::AdvFileAttributes (const FilePtr& fileHandle) : FileAttributes(fileHandle)
{
	FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
	bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
	OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
}

AdvFileAttributes::AdvFileAttributes (const DString& relativeDirectory, const DString& fullFileName) : FileAttributes(relativeDirectory, fullFileName)
{
	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), FileName + TXT(".") + FileExtension))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

AdvFileAttributes::AdvFileAttributes (const Directory& dir, const DString& fileName) : FileAttributes(dir, fileName)
{
	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), FileName + TXT(".") + FileExtension))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

AdvFileAttributes::AdvFileAttributes (const FileAttributes& attributesCopy) : FileAttributes(attributesCopy)
{
	bReadOnly = false;
	FileSize = 0;
	PopulateFileInfo();
}

AdvFileAttributes::AdvFileAttributes (const AdvFileAttributes& attributesCopy) : FileAttributes(attributesCopy)
{
	bReadOnly = attributesCopy.bReadOnly;
	FileSize = attributesCopy.FileSize;
	LastModifiedDate = attributesCopy.LastModifiedDate;
	CreatedDate = attributesCopy.CreatedDate;
}

AdvFileAttributes::~AdvFileAttributes ()
{

}

void AdvFileAttributes::ResetToDefaults ()
{
	FileAttributes::ResetToDefaults();

	bReadOnly = false;
	FileSize = 0;
	LastModifiedDate.ResetToDefaults();
	CreatedDate.ResetToDefaults();
}

void AdvFileAttributes::ClearValues ()
{
	FileAttributes::ClearValues();

	bReadOnly = false;
	FileSize = 0;
}

void AdvFileAttributes::PopulateFileInfo ()
{
	FileAttributes::PopulateFileInfo();

	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), GetName(false, true)))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

void AdvFileAttributes::PopulateFileInfoFromHandle (FilePtr handle)
{
	FileAttributes::PopulateFileInfoFromHandle(handle);

	FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
	bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
	OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
}

bool AdvFileAttributes::GetReadOnly () const
{
	return bReadOnly;
}

Int AdvFileAttributes::GetFileSize () const
{
	return Int(static_cast<UINT_TYPE>(FileSize));
}

unsigned long long AdvFileAttributes::GetFileSizeLarge () const
{
	return FileSize;
}

DateTime AdvFileAttributes::GetLastModifiedDate () const
{
	return LastModifiedDate;
}

DateTime AdvFileAttributes::GetCreatedDate () const
{
	return CreatedDate;
}
SD_END