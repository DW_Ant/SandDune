/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ComponentSerializer.cpp
=====================================================================
*/

#include "ComponentSerializer.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::ComponentSerializer, SD::BinaryFile)
SD_BEGIN

bool ComponentSerializer::ReadContents (const DataBuffer& incomingData)
{
	if (!Super::ReadContents(incomingData))
	{
		return false;
	}

	bool success;
	Int numEntities = ReadIntFromBuffer(incomingData, OUT success);
	if (!success)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Could not read number of entities from file."), LatestAccessedFile.GetName(false, true));
		return false;
	}

	for (Int entityCount = 0; entityCount < numEntities; ++entityCount)
	{
		Entity* newEntity = Entity::CreateObject();
		StagedEntities.push_back(newEntity);
		newEntity->DebugName = ReadStringFromBuffer(incomingData, OUT success);
		if (!success)
		{
			FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Not enough bytes to read DebugName."), LatestAccessedFile.GetName(false, true));
			return false;
		}

		Int numComps = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Not enough bytes to read the number of components."), LatestAccessedFile.GetName(false, true));
			return false;
		}

		for (Int compCount = 0; compCount < numComps; ++compCount)
		{
			Int compClass = ReadIntFromBuffer(incomingData, OUT success);
			if (!success)
			{
				FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Not enough bytes to read the component class."), LatestAccessedFile.GetName(false, true));
				return false;
			}

			if (compClass == CT_EntityComponent)
			{
				EntityComponent* newComp = EntityComponent::CreateObject();
				if (newEntity->AddComponent(newComp))
				{
					//EntityComponent doesn't have any information
				}
			}
			else if (compClass == CT_TickComponent)
			{
				TickComponent* tickComp = TickComponent::CreateObject(TICK_GROUP_DEBUG);
				if (newEntity->AddComponent(tickComp))
				{
					if (!incomingData.CanReadBytes(Float::SGetMinBytes()))
					{
						FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Not enough bytes to read tick interval."), LatestAccessedFile.GetName(false, true));
						return false;
					}

					Float tickInterval;
					if ((incomingData >> tickInterval).HasReadError())
					{
						FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. The bytes are malformed, preventing it from reading the TickInterval."), LatestAccessedFile.GetName(false, true));
						return false;
					}

					tickComp->SetTickInterval(tickInterval);

					if (!incomingData.CanReadBytes(Bool::SGetMinBytes()))
					{
						FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. Not enough bytes to read isTicking."), LatestAccessedFile.GetName(false, true));
						return false;
					}

					Bool isTicking;
					if ((incomingData >> isTicking).HasReadError())
					{
						FileLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s. The bytes are malformed, preventing it from reading IsTicking."), LatestAccessedFile.GetName(false, true));
						return false;
					}
					tickComp->SetTicking(isTicking);
				}
			}
			else
			{
				FileLog.Log(LogCategory::LL_Warning, TXT("Unknown class type detected when deserializing component ID. %s is not mapped to a class."), compClass);
				return false;
			}
		}
	}

	return true;
}

bool ComponentSerializer::SaveContents (DataBuffer& outBuffer) const
{
	if (!Super::SaveContents(outBuffer))
	{
		return false;
	}

	Int numEntities(StagedEntities.size());
	outBuffer << numEntities;

	for (Entity* entity : StagedEntities)
	{
		outBuffer << entity->DebugName;

		Int compCounter = 0;
		for (ComponentIterator iter(entity, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (iter.GetSelectedComponent()->StaticClass() == EntityComponent::SStaticClass() ||
				iter.GetSelectedComponent()->StaticClass() == TickComponent::SStaticClass())
			{
				compCounter++;
			}
		}
		outBuffer << compCounter;

		for (ComponentIterator iter(entity, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (iter.GetSelectedComponent()->StaticClass() == EntityComponent::SStaticClass())
			{
				Int compClass = CT_EntityComponent;
				outBuffer << compClass;
			}
			else if (iter.GetSelectedComponent()->StaticClass() == TickComponent::SStaticClass())
			{
				TickComponent* tick = dynamic_cast<TickComponent*>(iter.GetSelectedComponent());
				CHECK(tick != nullptr)

				Int compClass = CT_TickComponent;
				outBuffer << compClass;

				outBuffer << tick->GetTickInterval();
				outBuffer << Bool(tick->IsTicking());
			}
		}
	}

	return true;
}

void ComponentSerializer::Destroyed ()
{
	while (!ContainerUtils::IsEmpty(StagedEntities))
	{
		if (Entity* entity = ContainerUtils::GetLast(StagedEntities))
		{
			entity->Destroy();
		}

		StagedEntities.pop_back();
	}

	Super::Destroyed();
}
SD_END
#endif