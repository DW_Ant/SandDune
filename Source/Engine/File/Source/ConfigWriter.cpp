/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ConfigWriter.cpp
=====================================================================
*/

#include "FileClasses.h"

IMPLEMENT_CLASS(SD::ConfigWriter, SD::TextFileWriter)
SD_BEGIN

const TCHAR ConfigWriter::MULTI_LINE_CHARACTER = '\\';

void ConfigWriter::InitProps ()
{
	Super::InitProps();

	bReadEverything = true;
	bNeedsSave = false;
	BufferLimit = 8192; //Large buffer since all content will be written on SaveConfig

	NumLinesInFileHeader = 0;
}

bool ConfigWriter::OpenFile (const DString& fileName, bool bReplaceFile)
{
	if (Super::OpenFile(fileName, bReplaceFile))
	{
		ContainerUtils::Empty(HeaderData);
		ContainerUtils::Empty(Sections);

		//abort early since there's nothing in the new file
		if (bReplaceFile)
		{
#ifdef DEBUG_MODE
			//Typically bReplaceFile is undesirable for config writers.  Display a log message just in case.
			FileLog.Log(LogCategory::LL_Log, TXT("Opening a config file (%s) with bReplaceFile to true.  You'll not be able to recall saved properties since you're clearing the content before reading it."), fileName);
#endif
			return true;
		}

		PopulateHeaderData();
		PopulateSectionData();

		return true;
	}

	return false;
}

bool ConfigWriter::OpenFile (const FileAttributes& fileAttributes, bool bReplaceFile)
{
	return OpenFile(fileAttributes.GetName(true, true), bReplaceFile);
}

void ConfigWriter::Destroyed ()
{
	if (File.is_open() && bNeedsSave && bReadEverything)
	{
		SaveConfig();
	}

	Super::Destroyed();
}

void ConfigWriter::SaveConfig ()
{
	if (!bNeedsSave || bReadOnly)
	{
		return;
	}

	if (!File.is_open())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Attempting to write to the configuration file %s when the file is not opened."), CurrentFile);
		return;
	}

	EmptyFile();

	//Write the header data
	for (size_t i = 0; i < HeaderData.size(); i++)
	{
		WriteMultiLineText(HeaderData.at(i));
	}

	//Ensure there's a space between the header and first section data
	if (!ContainerUtils::IsEmpty(HeaderData))
	{
		DString lastItem = ContainerUtils::GetLast(HeaderData); //Create copy of last item for space trimming
		lastItem.TrimSpaces();
		if (!lastItem.IsEmpty())
		{
			AddTextEntry(DString::EmptyString);
		}
	}

	//Write the properties to config
	for (size_t sectionIdx = 0; sectionIdx < Sections.size(); ++sectionIdx)
	{
		AddTextEntry(TXT("[") + Sections[sectionIdx].SectionName + TXT("]"));

		for (const DString& propertyLine : Sections[sectionIdx].SectionData)
		{
			WriteMultiLineText(propertyLine);
		}

		//Ensure there's a blank line between sections for readability
		if (sectionIdx != Sections.size() - 1 && !ContainerUtils::IsEmpty(Sections.at(sectionIdx).SectionData) &&
			!Sections.at(sectionIdx).SectionData.at(Sections.at(sectionIdx).SectionData.size() - 1).IsEmpty())
		{
			AddTextEntry(DString::EmptyString);
		}
	}

	WriteToFile(true);
	bNeedsSave = false;
}

void ConfigWriter::SavePropertyText (const DString& sectionName, const DString& propertyName, const DString& propertyValue)
{
	if (sectionName.IsEmpty())
	{
		size_t propIdx;
		FindPropertyWithinHeader(propertyName, OUT propIdx);
		if (propIdx != INDEX_NONE)
		{
			HeaderData.at(propIdx) = propertyName + TXT("=") + propertyValue;
		}
		else
		{
			HeaderData.emplace_back(propertyName + TXT("=") + propertyValue);
		}
	}
	else
	{
		size_t i = FindSectionIndex(sectionName);
		if (i == INDEX_NONE)
		{
			//Create a new section and add the data there
			SDataSection newSection;
			newSection.SectionName = sectionName;
			newSection.SectionData.push_back(propertyName + TXT("=") + propertyValue);
			Sections.push_back(newSection);
		}
		else
		{
			//Search where the property resides in the section
			size_t propIndex;
			FindPropertyWithinSection(i, propertyName, OUT propIndex);

			if (propIndex != INDEX_NONE)
			{
				Sections.at(i).SectionData.at(propIndex) = propertyName + TXT("=") + propertyValue;
			}
			else
			{
				//Add property at the end of the section
				DString newData = propertyName + TXT("=") + propertyValue;
				Sections.at(i).SectionData.push_back(newData);
			}
		}
	}

	bNeedsSave = true;
}

bool ConfigWriter::SavePropertyText (const DString& fileName, const DString& sectionName, const DString& propertyName, const DString& propertyValue)
{
	ConfigWriter* writer = ConfigWriter::CreateObject();
	if (writer == nullptr)
	{
		return false;
	}

	if (!writer->OpenFile(fileName, false))
	{
		writer->Destroy();
		return false;
	}

	writer->SavePropertyText(sectionName, propertyName, propertyValue);
	writer->Destroy();
	return true;
}

DString ConfigWriter::GetPropertyText (const DString& sectionName, const DString& propertyName) const
{
	size_t sectionIndex = UINT_INDEX_NONE;
	if (!sectionName.IsEmpty())
	{
		sectionIndex = FindSectionIndex(sectionName);
		if (sectionIndex < Sections.size())
		{
			return FindPropertyWithinSection(sectionIndex, propertyName);
		}

		FileLog.Log(LogCategory::LL_Warning, TXT("Section name %s is not found within file %s"), sectionName, CurrentFile);
		return DString::EmptyString;
	}

	//Section name is not specified, search through header data
	for (size_t i = 0; i < HeaderData.size(); i++)
	{
		DString result;
		if (ReadPropertyValue(HeaderData.at(i), propertyName, result))
		{
			return result;
		}
	}

	return DString::EmptyString;
}

bool ConfigWriter::ContainsSection (const DString& sectionName) const
{
	return (FindSectionIndex(sectionName) != INDEX_NONE);
}

bool ConfigWriter::ContainsProperty (const DString& sectionName, const DString& propertyName) const
{
	size_t sectionIdx = FindSectionIndex(sectionName);
	size_t propertyIdx;
	FindPropertyWithinSection(sectionIdx, propertyName, OUT propertyIdx);
	
	return (propertyIdx != INDEX_NONE);
}

bool ConfigWriter::ClearSection (const DString& sectionName, bool deleteSection)
{
	for (size_t i = 0; i < Sections.size(); ++i)
	{
		if (Sections.at(i).SectionName.Compare(sectionName, DString::CC_IgnoreCase) == 0)
		{
			if (deleteSection)
			{
				Sections.erase(Sections.begin() + i);
			}
			else
			{
				ContainerUtils::Empty(OUT Sections.at(i).SectionData);
			}

			return true;
		}
	}

	return false;
}

void ConfigWriter::SetReadEverything (bool newReadEverything)
{
	if (!CurrentFile.IsEmpty())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Cannot set %s ReadEverything after opening the file."), CurrentFile);
		return;
	}

	bReadEverything = newReadEverything;
}

void ConfigWriter::PopulateHeaderData ()
{
	ResetSeekPosition();
	NumLinesInFileHeader = 0;

	DString currentLine;
	DString accumulatedData = DString::EmptyString;

	//HeaderData
	while(std::getline(File, currentLine.EditString()))
	{
		currentLine.MarkNumCharactersDirty();

		//This is the beginning of actual data.  Stop writing to the header.
		if (accumulatedData.IsEmpty() && IsSectionHeaderFormat(currentLine))
		{
			break;
		}
		++NumLinesInFileHeader;

		if (currentLine.EndsWith(MULTI_LINE_CHARACTER))
		{
			if (!accumulatedData.IsEmpty())
			{
				//When combining lines, add a delimiter
				accumulatedData += TXT("\n");
			}

			//Add to HeaderData later when we find the entirety of the value.
			accumulatedData += currentLine;
			accumulatedData.PopBack(); //Remove trailing slash
		}
		else if (!accumulatedData.IsEmpty())
		{
			HeaderData.push_back(accumulatedData + TXT("\n") + currentLine);
			accumulatedData = DString::EmptyString;
		}
		else
		{
			HeaderData.push_back(currentLine);
		}
	}

	if (!accumulatedData.IsEmpty())
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("In file %s, the last line in the header data ended with a multi line character and yet there's no more data following it. Found in: %s"), GetCurrentFileName(), accumulatedData);
		HeaderData.push_back(accumulatedData);
	}
}

void ConfigWriter::PopulateSectionData ()
{
	if (!bReadEverything)
	{
		return; //Skipping section data
	}

	ResetSeekPosition();

	DString currentLine;
	DString accumulatedData = DString::EmptyString;
	SDataSection currentSection;
	Int headerCounter = 0;

	while(std::getline(File, currentLine.EditString()))
	{
		currentLine.MarkNumCharactersDirty();
		if (++headerCounter <= NumLinesInFileHeader)
		{
			continue; //Ignore this line since this is already recorded in HeaderData
		}

		bool isNewSection = accumulatedData.IsEmpty() && IsSectionHeaderFormat(currentLine);
		if (!isNewSection)
		{
			if (currentLine.EndsWith(MULTI_LINE_CHARACTER))
			{
				if (!accumulatedData.IsEmpty())
				{
					//When combining lines, add a delimiter
					accumulatedData += TXT("\n");
				}

				accumulatedData += currentLine;
				accumulatedData.PopBack(); //Remove trailing slash
			}
			else if (!accumulatedData.IsEmpty())
			{
				currentSection.SectionData.push_back(accumulatedData + TXT("\n") + currentLine);
				accumulatedData = DString::EmptyString;
			}
			else
			{
				currentSection.SectionData.push_back(currentLine);
			}
		}

		//Insert the section data to the Sections vector and reset currentSection
		if (File.eof() || isNewSection)
		{
			//Add currentSection to the Sections vector
			if (!currentSection.SectionName.IsEmpty())
			{
				if (currentSection.SectionData.size() > 0 && currentSection.SectionData.at(currentSection.SectionData.size() - 1).Length() < 2)
				{
					//Ignore the blank line between sections since that's automatically added in SaveConfig
					currentSection.SectionData.pop_back();
				}

				Sections.push_back(currentSection);
			}

			if (File.eof())
			{
				currentSection.SectionName = DString::EmptyString;
				break;
			}

			currentSection.SectionName = currentLine.SubStringCount(1, currentLine.Length() - 2); //Include the text between the square brackets
			currentSection.SectionData.clear(); //Reset recycled variable for the next section
		}
	}

	//getline treats new lines as file terminators. This handles the case where there are blank lines at the end of the config file.
	//If the loop didn't have a chance to flush the section data, do so now.
	if (!currentSection.SectionName.IsEmpty())
	{
		Sections.push_back(currentSection);
	}
}

bool ConfigWriter::IsSectionHeaderFormat (const DString& text) const
{
	//Must contain square brackets at the beginning and end.  Must contain text within square brackets.
	return (text.Length() > 2 && text.StartsWith('[') && text.EndsWith(']'));
}

bool ConfigWriter::IsCommentFormat (const DString& line) const
{
	return (line.IsEmpty() || line.StartsWith(';'));
}

size_t ConfigWriter::FindSectionIndex (const DString& sectionName) const
{
	for (size_t i = 0; i < Sections.size(); i++)
	{
		if (sectionName.Compare(Sections.at(i).SectionName, DString::CC_IgnoreCase) == 0)
		{
			return i;
		}
	}

	return INDEX_NONE;
}

DString ConfigWriter::FindPropertyWithinSection (size_t sectionIndex, const DString& propertyName) const
{
	size_t unusedIndex;
	return (FindPropertyWithinSection(sectionIndex, propertyName, OUT unusedIndex));
}

DString ConfigWriter::FindPropertyWithinSection (size_t sectionIndex, const DString& propertyName, size_t& outPropertyIndex) const
{
	outPropertyIndex = INDEX_NONE;

	const std::vector<DString>* searchIn = (sectionIndex == INDEX_NONE) ? &HeaderData : &Sections.at(sectionIndex).SectionData;

	for (size_t i = 0; i < searchIn->size(); i++)
	{
		DString result;
		if (ReadPropertyValue(searchIn->at(i), propertyName, result))
		{
			outPropertyIndex = i;
			return result;
		}
	}

	return DString::EmptyString;
}

DString ConfigWriter::FindPropertyWithinHeader (const DString& propertyName, size_t& outPropertyIndex) const
{
	return FindPropertyWithinSection(INDEX_NONE, propertyName, OUT outPropertyIndex);
}

bool ConfigWriter::ReadPropertyValue (const DString& fullLine, const DString& expectedPropertyName, DString& outPropertyValue) const
{
	Int equalPosIdx;
	if (ContainsPropertyValue(fullLine, expectedPropertyName, OUT equalPosIdx))
	{
		//Return the content after the equals
		outPropertyValue = fullLine.SubString(equalPosIdx + 1); //Plus 1 to start from the character after equal.
		return true;
	}

	return false;
}

bool ConfigWriter::ContainsPropertyValue (const DString& fullLine, const DString& targetPropertyName, Int& outEqualPosIdx) const
{
	outEqualPosIdx = INT_INDEX_NONE;
	if (IsCommentFormat(fullLine))
	{
		return false;
	}

	outEqualPosIdx = fullLine.Find(TXT("="), 0, DString::CC_IgnoreCase, DString::SD_LeftToRight);
	if (outEqualPosIdx < 0)
	{
		return false;
	}

	return (fullLine.SubString(0, outEqualPosIdx - 1).Compare(targetPropertyName, DString::CC_IgnoreCase) == 0);
}

void ConfigWriter::WriteMultiLineText (const DString& newLine)
{
	std::vector<DString> lines;
	newLine.ParseString('\n', OUT lines, false);
	for (size_t i = 0; i < lines.size(); ++i)
	{
		if (i < lines.size() - 1)
		{
			AddTextEntry(lines.at(i) + TXT("\\"));
		}
		else
		{
			AddTextEntry(lines.at(i));
		}
	}
}
SD_END