/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFileWriter.cpp
=====================================================================
*/

#include "FileClasses.h"

IMPLEMENT_CLASS(SD::TextFileWriter, SD::Object)
SD_BEGIN

void TextFileWriter::InitProps ()
{
	Super::InitProps();

	CurrentFile = DString::EmptyString;
	BufferLimit = 12;
	bReadOnly = false;
}

void TextFileWriter::Destroyed ()
{
	if (!bReadOnly)
	{
		WriteToFile(true);
	}

	if (File.is_open())
	{
		File.close();
	}

	Super::Destroyed();
}

DString TextFileWriter::GetFriendlyName () const
{
	if (!CurrentFile.IsEmpty())
	{
		return GetName() + TXT(":  ") + CurrentFile;
	}

	return Super::GetFriendlyName();
}

bool TextFileWriter::OpenFile (const DString& fileName, bool bReplaceFile)
{
	if (File.is_open())
	{
		WriteToFile(true);
		File.close();
	}

	if (bReplaceFile && !bReadOnly)
	{
		remove(fileName.ToCString());
	}

	File.open(fileName.ReadString(), std::fstream::out | std::fstream::in | std::fstream::app);

	CurrentFile = fileName;

	return File.is_open();
}

bool TextFileWriter::OpenFile (const FileAttributes& fileAttributes, bool bReplaceFile)
{
	//Quick check if the directory exists. The file will fail to open otherwise.
	if (!OS_CheckIfDirectoryExists(fileAttributes.ReadPath()))
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Cannot open file %s since the path does not exist."), fileAttributes.GetName(true, true));
		return false;
	}

	return OpenFile(fileAttributes.GetName(true, true), bReplaceFile);
}

void TextFileWriter::AddTextEntry (const DString& newEntry)
{
	if (newEntry.IsEmpty() || newEntry.At(newEntry.Length() - 1) != '\n')
	{
		TextBuffer.emplace_back(newEntry + '\n');
	}
	else
	{
		TextBuffer.emplace_back(newEntry);
	}

	if (TextBuffer.size() >= BufferLimit)
	{
		WriteToFile(true);
	}
}

bool TextFileWriter::WriteToFile (bool bClearBufferIfFailed)
{
	if (bReadOnly)
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Cannot write to %s since it's read only."), CurrentFile);
		return false;
	}

	bool bResult = false;

	if (TextBuffer.size() <= 0)
	{
		return true; //Nothing to copy
	}

	if (File.is_open())
	{
		for (size_t i = 0; i < TextBuffer.size() - 1; i++)
		{
			File << TextBuffer.at(i).ReadString();
		}

		File << TextBuffer.at(TextBuffer.size() - 1).ReadString();

		File.flush();
		bResult = (!File.bad());
	}

	if (bResult || bClearBufferIfFailed)
	{
		TextBuffer.clear();
	}

	return bResult;
}

bool TextFileWriter::EmptyFile ()
{
	if (bReadOnly)
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Cannot empty %s since it's read only."), CurrentFile);
		return false;
	}

	bool bOpenFile = false;

	if (File.is_open())
	{
		bOpenFile = true;
		File.close();
	}

	remove(CurrentFile.ToCString());

	//Reopen files
	if (bOpenFile)
	{
		File.open(CurrentFile.ToCString(), std::fstream::in | std::fstream::out | std::ofstream::app);
	}

	return true;
}

bool TextFileWriter::ReadLine (DString& outCurrentLine)
{
	if (File.eof())
	{
		return false;
	}

	getline(File, outCurrentLine.EditString());
	outCurrentLine.MarkNumCharactersDirty();

	//Sometimes File.eof won't become true until it reads an empty string at the end. Should return false if the last entry isn't valid (eg: empty string at the end of the file).
	return (!File.eof() || !outCurrentLine.IsEmpty());
}

bool TextFileWriter::IsFileOpened () const
{
	return File.is_open();
}

void TextFileWriter::SetBufferLimit (UINT_TYPE newBufferLimit)
{
	BufferLimit = newBufferLimit;

	if (TextBuffer.size() >= BufferLimit)
	{
		WriteToFile(true);
	}
}

void TextFileWriter::SetReadOnly (bool bNewReadOnly)
{
	if (bNewReadOnly)
	{
		WriteToFile(true);
	}

	bReadOnly = bNewReadOnly;
}

UINT_TYPE TextFileWriter::GetBufferLimit () const
{
	return BufferLimit;
}

DString TextFileWriter::GetCurrentFileName () const
{
	return CurrentFile;
}

bool TextFileWriter::GetReadOnly () const
{
	return bReadOnly;
}

void TextFileWriter::ResetSeekPosition ()
{
	//Clear any flags that may prevent seeking (ie:  EOF flag)
	File.clear();

	//Set file position back to the beginning
	File.seekg(0);
}
SD_END