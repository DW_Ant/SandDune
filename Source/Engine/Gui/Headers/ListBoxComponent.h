/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ListBoxComponent.h
  Gui Component that lists items as text vertically.

  This Gui component has the ability to invoke callbacks whenever the user selects
  or deselects an item.  Additionally, this has parameters to support multi-selection.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "FocusInterface.h"
#include "GuiDataElement.h"

SD_BEGIN
class FrameComponent;
class LabelComponent;

class GUI_API ListBoxComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(ListBoxComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SHighlightSelectionInfo
	{
	public:
		/* Dummy GuiComponent that defines the transform for the render component. */
		PlanarTransformComponent* RenderOwner;

		/* The color component that's drawn over the text.  This component may render 1 or more lines based
		on how many consecutive selected items there are. */
		ColorRenderComponent* RenderComp;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever the user selects an item.  This is invoked after the item is added to SelectedItemList. */
	SDFunction<void, Int /*selectedItemIdx*/> OnItemSelected;

	/* Callback to invoke whenever the user deselects an item.  This is invoked before the item is removed from the SelectedItemList. */
	SDFunction<void, Int /*deselectedItemIdx*/> OnItemDeselected;

	/* Callback invoked whenever the user clicked on an item regardless if its selection was toggled or not. It's possible an item is not toggled due to min/max num selection restraints.
	This is invoked after the item is selected or deselected. */
	SDFunction<void, Int /*itemIdx*/> OnItemClicked;

protected:
	/* List of available options.  Each option may have a pointer to an object but does not check if the object is valid.
	   The list, itself, is a pointer to BaseGuiDataElement in order to support polymorphic types for casting. 
	   The ListBoxComponent will take ownership over the BaseGuiDataElement instances. */
	std::vector<BaseGuiDataElement*> List;

	/* List of indices to the List vector that represents the displayed text. May be sorted in a way differently from the List.
	Omitted options may also be removed from the List for filtering. */
	std::vector<size_t> DisplayedListIndices;

	/* If greater than 0, then this is the maxium number of items the user can select simultaneously. */
	Int MaxNumSelected;

	/* If greater than 0, then this is the minimum number of items that can be selected. If the user clicks on a selected item, and if
	the number of selected is equal to this number, then that item will NOT deselect. */
	Int MinNumSelected;

	/* If true, then all selected items will be automatically deselected when the user clicks on a new item. */
	bool bAutoDeselect;

	/* If true then this component will automatically scale vertically to fit each item. */
	bool bAutoSizeVertically;

	/* If true, the item list cannot be selected and is for reading only.  This only prevents user from selecting/deselection options via mouse clicks.
	   Selecting items are still allowed if the program automatically selects items based on external events. */
	bool bReadOnly;

	/* An ordered (in ascending order) list of item indices that are selected. */
	std::vector<Int> SelectedItems;

	/* Index that shifts along the List (moving the highlight bar) without actually selecting the item.  If negative, then nothing is getting browsed. */
	Int BrowseIndex;

	DPointer<FrameComponent> Background;

	/* Text that renders the list. */
	DPointer<LabelComponent> ItemListText;

	/* Color component that'll be rendering the user's mouse hover. */
	DPointer<ColorRenderComponent> HighlightHoverRender;
	DPointer<PlanarTransformComponent> HighlightOwner;

	/* Color components that'll be rendering a highlight over each selected item. */
	std::vector<SHighlightSelectionInfo> HighlightSelections;

	/* Color components that'll be rendering a highlight over each read only item. */
	std::vector<SHighlightSelectionInfo> ReadOnlySelections;

	/* Color to use for the selected items' highlight bars. */
	Color SelectionColor;

	/* Color to use for the hover bar. */
	Color HoverColor;

	/* Color to use when the item is read only. */
	Color ReadOnlyColor;

	/* When refreshing the text, this filter is used to prevent irrelevant options from appearing. If empty, then no filter is applied. */
	DString Filter;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual bool CanBeFocused () const override;
	virtual void GainFocus () override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the list item (by index) is selected.
	 */
	virtual bool IsItemSelectedByIndex (Int index) const;

	/**
	 * Returns the displayed index from the List index.
	 */
	virtual size_t GetDisplayedIndex (Int listIdx) const;

	/**
	 * Returns the List index from the displayed index.
	 */
	virtual Int GetListIndex (size_t displayIdx) const;

	/**
	 * Ensures that none of the items within the list are selected.
	 */
	virtual void ClearSelection ();

	/**
	 * Ensures that all of the items within the list are selected.
	 */
	virtual void SelectAll ();

	/**
	 * Selects the specified items by their index values. Anything not in this list will be deselected.
	 */
	virtual void SelectItemsByIdx (const std::vector<Int>& itemIndices);

	/**
	 * Removes the specified item by index from this ListBoxComponent.
	 * Any selection indices will automatically be refreshed.
	 */
	virtual void RemoveItemByIdx (Int itemIdx);

	/**
	 * Adds an item to the selection. Anything previously selected remains selected.
	 */
	virtual void AddItemSelectionByIdx (Int selectedItemIdx);

	/**
	 * Deselects everything except for the item specified.
	 */
	virtual void ReplaceItemSelectionByIdx (Int selectedItemIdx);

	/**
	 * Sets an item by matching index to be either selected or deselected state.
	 */
	virtual void SetItemSelectionByIdx (Int itemIdx, bool bSelected);

	/**
	 * Calculates the text to display on the label component.
	 * Invoke this when manually editing an existing GuiDataElement.
	 */
	virtual void UpdateTextContent ();

	/**
	 * Deallocates the list of pointers, and clears text and selections.
	 */
	virtual void ClearList ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetBrowseIndex (Int newBrowseIndex);
	virtual void SetList (const std::vector<BaseGuiDataElement*>& newList);
	virtual void SetMaxNumSelected (Int newMaxNumSelected);
	virtual void SetMinNumSelected (Int newMinNumSelected);
	virtual void SetAutoDeselect (bool bNewAutoDeselect);
	virtual void SetAutoSizeVertically (bool bNewAutoSizeVertically);
	virtual void SetReadOnly (bool bNewReadOnly);
	virtual void SetBackground (FrameComponent* newBackground);
	virtual void SetItemListText (LabelComponent* newItemListText);
	virtual void SetSelectionColor (Color newSelectionColor);
	virtual void SetHoverColor (Color newHoverColor);
	virtual void SetReadOnlyColor (Color newReadOnlyColor);
	virtual void SetFilter (const DString& newFilter);


	/*
	=====================
	  Accessors
	=====================
	*/

	inline Int GetBrowseIndex () const
	{
		return BrowseIndex;
	}

	inline std::vector<BaseGuiDataElement*> GetList () const
	{
		return List;
	}

	inline const std::vector<BaseGuiDataElement*>& ReadList () const
	{
		return List;
	}

	inline const std::vector<size_t>& ReadDisplayedListIndices () const
	{
		return DisplayedListIndices;
	}

	inline Int GetMaxNumSelected () const
	{
		return MaxNumSelected;
	}

	inline Int GetMinNumSelected () const
	{
		return MinNumSelected;
	}

	inline bool IsReadOnly () const
	{
		return bReadOnly;
	}

	inline bool GetAutoDeselect () const
	{
		return bAutoDeselect;
	}

	inline bool GetAutoSizeVertically () const
	{
		return bAutoSizeVertically;
	}

	inline std::vector<Int> GetSelectedItems () const
	{
		return SelectedItems;
	}

	inline const std::vector<Int>& ReadSelectedItems () const
	{
		return SelectedItems;
	}

	inline FrameComponent* GetBackground () const
	{
		return Background.Get();
	}

	inline LabelComponent* GetItemListText () const
	{
		return ItemListText.Get();
	}

	inline ColorRenderComponent* GetHighlightHoverRender () const
	{
		return HighlightHoverRender.Get();
	}

	inline Color GetSelectionColor () const
	{
		return SelectionColor;
	}

	inline Color GetHoverColor () const
	{
		return HoverColor;
	}

	inline Color GetReadOnlyColor () const
	{
		return ReadOnlyColor;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeBackground ();
	virtual void InitializeItemListText ();
	virtual void InitializeHighlightBar ();

	/**
	 * Reconstructs all color transforms to reflect the current selection state.
	 */
	virtual void RefreshHighlightSelectionComps ();

	/**
	 * Reconstructs all color transforms over ReadOnly options to reflect the current selection state.
	 */
	virtual void RefreshReadOnlyColorComps ();

	/**
	 * Updates the hover highlight transform to be over the current browse position.
	 */
	virtual void RenderHighlightHoverOverBrowsedItem ();

	/**
	 * Returns the line index the given mouse coordinates is over.  The line index is relative to the
	 * scroll position where the absolute line index equals return value + current scroll position.
	 * Returns negative if the coordinates are either beyond the transform region, or not over any item.
	 */
	virtual Int GetLineIdxFromCoordinates (Float xPos, Float yPos) const;

	/**
	 * Returns the rectangular region that covers the specified list item index.
	 * @Param lineIdx:  The item index in absolute terms where 0 is the first item regardless of scroll position.
	 * Returns a rectangle region in absolute coordinates.  Becomes empty if specified index doesn't
	 * exist or is currently not visible (due to scroll position).
	 * The rectangle are in UI coordinate system where positive X is right and positive Y is down.
	 */
	virtual Rectangle GetItemRegion (Int lineIdx) const;

	/**
	 * Adjusts this component's vertical size to fit every item.
	 */
	virtual void RefreshVerticalSize ();


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Adds an item to the list.  The pointer to the object is not validated.  If an object is deleted,
	 * it's the calling object's responsibility to clear the list and repopulate it.
	 * Returns the index of the newly added option.
	 */
	template <class T>
	size_t AddOption (const GuiDataElement<T>& inNewDataElement)
	{
		size_t result = List.size();
		List.push_back(new GuiDataElement<T>(inNewDataElement));
		UpdateTextContent();

		if (inNewDataElement.bReadOnly)
		{
			RefreshReadOnlyColorComps();
		}

		return result;
	}

	template <class T>
	void SetList (const std::vector<GuiDataElement<T>>& newList)
	{
		ClearList();

		for (size_t i = 0; i < newList.size(); ++i)
		{
			List.push_back(new GuiDataElement<T>(newList.at(i)));
		}

		UpdateTextContent();
		RefreshReadOnlyColorComps();
	}

	template <class T>
	void GetSelectedDataElements (std::vector<GuiDataElement<T>>& outSelectedItems) const
	{
		outSelectedItems.empty();

		for (size_t i = 0; i < SelectedItems.size(); ++i)
		{
			outSelectedItems.push_back(List.at(SelectedItems.at(i).ToUnsignedInt()));
		}
	}

	template <class T>
	void GetSelectedItems (std::vector<T>& outSelectedItems) const
	{
		ContainerUtils::Empty(outSelectedItems);

		for (size_t i = 0; i < SelectedItems.size(); ++i)
		{
			if (GuiDataElement<T>* item = dynamic_cast<GuiDataElement<T>*>(List.at(SelectedItems.at(i).ToUnsignedInt())))
			{
				outSelectedItems.push_back(item->Data);
			}
		}
	}

	template <class T>
	bool GetItemByIdx (Int itemIdx, T& outResult) const
	{
		if (ContainerUtils::IsValidIndex(List, itemIdx.ToUnsignedInt()))
		{
			if (GuiDataElement<T>* item = dynamic_cast<GuiDataElement<T>*>(List.at(itemIdx.ToUnsignedInt())))
			{
				outResult = item->Data;
				return true;
			}
		}

		return false;
	}

	template <class T>
	Int FindItemIdx (const T& searchFor) const
	{
		for (size_t i = 0; i < List.size(); ++i)
		{
			if (GuiDataElement<T>* item = dynamic_cast<GuiDataElement<T>*>(List.at(i)))
			{
				if (item->Data == searchFor)
				{
					return Int(i);
				}
			}
		}

		return INT_INDEX_NONE;
	}

	/**
	 * Returns if a matching list item is selected.
	 */
	template <class T>
	bool IsItemSelected (const T& targetItem) const
	{
		for (size_t i = 0; i < SelectedItems.size(); i++)
		{
			if (GuiDataElement<T>* selectedItem = dynamic_cast<GuiDataElement<T>*>(List.at(SelectedItems.at(i).ToUnsignedInt())))
			{
				if (selectedItem->Data == targetItem)
				{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Select all data items within the list that matches the specified data element.
	 * Any other element will be deselected.
	 */
	template <class T>
	void SetSelectedItems (const std::vector<T>& targetItems)
	{
		std::vector<Int> newItemSelection;
		for (size_t i = 0; i < List.size(); ++i)
		{
			GuiDataElement<T>* castElement = dynamic_cast<GuiDataElement<T>*>(List.at(i));
			if (castElement == nullptr)
			{
				continue;
			}

			for (T targetItem : targetItems)
			{
				if (castElement->Data == targetItem)
				{
					newItemSelection.push_back(Int(i));
					break; //break inner loop. Don't break outer loop in case there are duplicates in ListBox.
				}
			}
		}

		SelectItemsByIdx(newItemSelection);
	}

	/**
	 * Deselects all items except for the first item found in list that matches the specified data element.
	 */
	template <class T>
	void SetSelectedItem (T target)
	{
		for (size_t i = 0; i < List.size(); i++)
		{
			GuiDataElement<T>* castElement = dynamic_cast<GuiDataElement<T>*>(List.at(i));
			if (castElement == nullptr)
			{
				continue;
			}

			if (castElement->Data == target)
			{
				ReplaceItemSelectionByIdx(Int(i));
				break;
			}
		}
	}

	/**
	 * Removes item(s) from this list if the given lambda returns true.
	 * Returns the number of items that was removed.
	 */
	template <class T>
	size_t RemoveItems (const std::function<bool(const T&)>& predicate)
	{
		if (ContainerUtils::IsEmpty(List))
		{
			return 0; //Nothing to remove
		}

		size_t i = List.size() - 1;
		std::vector<size_t> removedIndices;

		while (true)
		{
			GuiDataElement<T>* castType = dynamic_cast<GuiDataElement<T>*>(List.at(i));
			if (castType != nullptr && predicate(castType->Data))
			{
				removedIndices.push_back(i);
				List.erase(List.begin() + i);

				//If the user selected this item, remove it from the list.
				ContainerUtils::RemoveItem(SelectedItems, Int(i));

				//Any SelectedItems after i must be displaced by 1.
				for (Int& selectedItem : SelectedItems)
				{
					if (selectedItem > i)
					{
						--selectedItem;
					}
				}
			}

			if (i == 0)
			{
				break;
			}

			--i;
		}

		if (OnItemDeselected.IsBounded())
		{
			for (size_t removedIdx : removedIndices)
			{
				OnItemDeselected(Int(removedIdx));
			}
		}

		UpdateTextContent();
		RefreshHighlightSelectionComps();
		RefreshReadOnlyColorComps();

		return (removedIndices.size());
	}
};
SD_END