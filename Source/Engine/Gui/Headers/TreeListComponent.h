/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TreeListComponent.h
  A component that displays a list of GuiDataElements and their relationships between parents
  and their children.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "GuiDataElement.h"
#include "FocusInterface.h"

SD_BEGIN
class LabelComponent;
class ButtonComponent;
class ScrollbarComponent_Deprecated;

class GUI_API TreeListComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(TreeListComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	struct SDataBranch
	{
	public:
		/* Data element that holds a pointer to the actual data.  This object (not the data it points to) will be deleted when the tree list is cleared.
		This object is not deleted in this struct's destructor since there could be multiple SDataBranches that point to the same object.  Instead the object
		that owns this branch (such as the TreeListComponent) should delete this object. */
		BaseGuiDataElement* Data;
		std::vector<SDataBranch*> Children;

		/* If true, then the children branches are visible. */
		bool bExpanded;

		/* Information regarding the branch where one of its children is this branch. */
		SDataBranch* ParentBranch;

		SDataBranch ()
		{
			Data = nullptr;
			bExpanded = false;
			ParentBranch = nullptr;
		}

		SDataBranch (BaseGuiDataElement* inData)
		{
			Data = inData;
			bExpanded = false;
			ParentBranch = nullptr;
		}

		SDataBranch (BaseGuiDataElement* inData, SDataBranch* inParentBranch)
		{
			Data = inData;
			bExpanded = false;
			ParentBranch = inParentBranch;
			if (inParentBranch != nullptr)
			{
				inParentBranch->Children.push_back(this);
			}
		}

		SDataBranch (BaseGuiDataElement* inData, SDataBranch* inParentBranch, std::vector<SDataBranch*> inChildren)
		{
			Data = inData;
			ParentBranch = inParentBranch;
			if (inParentBranch != nullptr)
			{
				inParentBranch->Children.push_back(this);
			}

			Children = inChildren;
			bExpanded = false;
		}

		virtual ~SDataBranch ()
		{
		}

		/**
		 * Assembles the relationship between parent and child branches.
		 */
		static void LinkBranches (SDataBranch& outParent, SDataBranch& outChild);

		bool IsValid () const;

		/**
		 * Recursively checks parent branches, and returns true if all parents' bExpanded flag is true.
		 */
		bool IsVisible () const;

		/**
		 * Returns the number of branches between this branch and the root branch.
		 */
		Int GetNumParentBranches () const;

		/**
		 * Returns the number of sub branches from this branch (this includes the children branches of this branch's children).
		 */
		Int CountNumChildren (bool bIncludeCollapsed) const;
	};

	/* Maps a button to a data branch. */
	struct SButtonMapping
	{
		ButtonComponent* Button;
		SDataBranch* Branch;

		SButtonMapping ()
		{
			Button = nullptr;
			Branch = nullptr;
		}

		SButtonMapping (ButtonComponent* inButton, SDataBranch* inBranch)
		{
			Button = inButton;
			Branch = inBranch;
		}
	};

private:
	/**
	 * Struct that contains specific data regarding assembling connections between branches.
	 */
	struct SBranchConnectionInfo
	{
		SDataBranch* ParentBranch;
		SDataBranch* ChildBranch;

		/* Point at which the vertical connection will connect to meet with the parent. */
		Vector2 ParentConnectPosition;

		SBranchConnectionInfo ()
		{
			ParentBranch = nullptr;
			ChildBranch = nullptr;
		}

		SBranchConnectionInfo (SDataBranch* inParentBranch, SDataBranch* inChildBranch, const Vector2& inConnectPos)
		{
			ParentBranch = inParentBranch;
			ChildBranch = inChildBranch;
			ParentConnectPosition = inConnectPos;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever an option was selected where the parameter is the index of the list item. */
	SDFunction<void, Int> OnOptionSelected;

	/* When focused, this is the amount the browse index will jump when the control key is held. */
	Int BrowseJumpAmount;

	/* Determines how far do the children shift to the left relevative to their parents (in pixels). */
	Float IndentSize;

	/* Determines the height of a single line that represents a single item. */
	Float BranchHeight;

	/* The thickness (width of vertical connectors, height for horizontal connectors) in pixels. */
	Float ConnectionThickness;
	sf::Color ConnectionColor;

protected:
	/* Since the RefreshTree function is wasteful (destroys old buttons and labels), this counter keeps track of number of pending destroy entities for all trees. */
	Int NumPendingDeleteComponents;

	/* Maximum number the NumPendingDeleteComponents may achieve before the tree list forces the garbage collector.  If negative, then this will not force garbage collection. */
	Int MaxNumPendingDeleteComponents;

	/* List of available options.  Each option may have a pointer to an object but does not check if the object is valid. */
	std::vector<SDataBranch*> TreeList;

	/* Selected item in the tree list where if all children of the tree was expanded, this selected index increments from top to bottom (if the tree was top-down). */
	Int SelectedItemIdx;

	/* Selected item within the TreeList. */
	SDataBranch* SelectedBranch;

	/* Contrast to the SelectedItemIdx, this index shifts along the List (moving the highlight bar) without actually selecting the item. */
	Int BrowseIndex;

	/* List of buttons associated for each branch option.  These buttons are continuously spawned/destroyed whenever the list updates or the scroll position changes.
	The buttons are also sorted based on index position. */
	std::vector<SButtonMapping> ExpandButtons;

	/* GuiComponents that'll represent the connections between parents and their children. */
	std::vector<DPointer<GuiComponent>> Connections;

	/* List of labels where each element represents a single branch within tree. */
	std::vector<DPointer<LabelComponent>> TreeLabels;

	/* Texture to use for the ExpandButtons that could expand to sub branches. */
	DPointer<Texture> ExpandButtonTexture;

	/* Texture to use for the ExpandButtons that could collapse the branch. */
	DPointer<Texture> CollapseButtonTexture;

	/* Highlight bar over the current selected item. */
	DPointer<ColorRenderComponent> SelectedBar;
	DPointer<GuiComponent> SelectedBarOwner;

	/* Highlight bar over the current browsed item.  This solid color will also highlight over a region when hovering over the expand buttons. */
	DPointer<ColorRenderComponent> HoverBar;
	DPointer<GuiComponent> HoverBarOwner;

	/* If true, then the HoverTransform will not update its attributes on mouse movement. */
	bool bIgnoreHoverMovement;

	DPointer<ScrollbarComponent_Deprecated> ListScrollbar;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void BeginObject () override;

	virtual bool CanBeFocused () const override;
	virtual void GainFocus () override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sorts the data branch such that all children of the branch is listed before the siblings of the branch.
	 */
	static void SortDataList (std::vector<SDataBranch*>& outDataList);

	virtual void ExpandBranch (Int branchIdx);
	virtual void ExpandBranch (SDataBranch* branch);
	virtual void CollapseBranch (Int branchIdx);
	virtual void CollapseBranch (SDataBranch* branch);

	virtual void ExpandAll ();
	virtual void CollapseAll ();

	/**
	 * Moves scroll bar position such that the specified branch index is within view.
	 */
	virtual void JumpScrollbarToViewBranch (Int branchIdx);

	void SetMaxNumPendingDeleteComponents (Int newMaxNumPendingDeleteComponents);

	virtual void SetBrowseIndex (Int newHoverIndex);

	/**
	 * Sets the selected item by index value.
	 */
	virtual void SetSelectedItem (Int itemIndex);

	/**
	 * Sets the selected item by branch.
	 */
	virtual void SetSelectedItem (SDataBranch* item);

	/**
	 * Replaces the entire tree with the new list.
	 */
	virtual void SetTreeList (const std::vector<SDataBranch*>& newTreeList);

	/**
	 * Deletes all tree elements, and destroys all sub Gui components associated with those items.
	 * The data braches, themselves, are deleted.
	 */
	virtual void ClearTreeList ();

	virtual void SetExpandButtonTexture (Texture* newExpandButtonTexture);
	virtual void SetCollapseButtonTexture (Texture* newCollapseButtonTexture);
	virtual void SetSelectedBar (ColorRenderComponent* newSelectedBar);
	virtual void SetHoverBar (ColorRenderComponent* newHoverBar);
	virtual void SetListScrollbar (ScrollbarComponent_Deprecated* newListScrollbar);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Texture* GetExpandButtonTexture () const;
	virtual Texture* GetCollapseButtonTexture () const;
	virtual ColorRenderComponent* GetSelectedBar () const;
	virtual ColorRenderComponent* GetHoverBar () const;
	virtual ScrollbarComponent_Deprecated* GetListScrollbar () const;

	virtual Int GetBrowseIndex () const;

	/**
	 * This function retrieves the data branch based on the specified index from this entities' TreeList.
	 * The index order is sorted from root to immediate children to siblings (same order of TreeBranchIterator).
	 */
	virtual SDataBranch* GetBranchFromIdx (Int treeIdx) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeScrollbar ();
	virtual void InitializeSolidColors ();

	/**
	 * Updates selected color transforms.  If bInvokeCallback is true, then the OnOptionSelected callback is invoked.
	 */
	virtual void UpdateSelectedItem (bool bInvokeCallback);

	/**
	 * Searches through the tree label list, and returns the label that is currently within bounds of the specified window coordinates.
	 */
	virtual LabelComponent* FindLabelAtCoordinates (Float x, Float y) const;

	/**
	 * Iterates through the tree list to count how many lines are currently visible (including the branches beyond the scroll visibility range).
	 */
	virtual Int CountNumVisibleEntries () const;

	/**
	 * Iterates through the tree until it finds the branch that is the right amount of distance from from the anchor index.
	 * All index values align with the index values of the TreeList vector.
	 *
	 * @Param anchorBranchIdx:  The branch index to base the distance from.  Result = anchorBranch - distanceAboveAnchor
	 * @Param distanceFromAnchor:  The number of branches between the anchor branch and return value.  Negative values will search for branches above the anchor branch.
	 * @Param bIncludeCollapsed:  If false, then all invisible branches (via collapsed flag) will be ignored when measuring distance.
	 * @Return:  The branch index that is at the right distance away from the anchor branch.  Becomes INT_INDEX_NONE if there aren't any branches at the right distance.
	 */
	virtual Int FindBranchIdxFromAnchor (Int anchorBranchIdx, Int distanceFromAnchor, bool bIncludeCollapsed) const;

	/**
	 * Figures out which branch should be displayed on top of this component considering collapsed branches and scroll position.
	 */
	virtual SDataBranch* FindFirstVisibleBranch () const;

	/**
	 * Iterates through the tree lists backwards and returns the first visible branch idx regardless of scroll position.
	 */
	Int FindLastVisibleBranchIdx () const;

	/**
	 * Returns the expand button that maps to the branch index.
	 */
	virtual ButtonComponent* FindExpandButtonAtIdx (Int branchIdx) const;

	/**
	 * Iterates through each data branch and sets their bExpanded flag equal to the specified value.
	 */
	virtual void SetExpandForAllBranches (bool bExpanded);

	/**
	 * Recalculates each component of the tree list in a way it maintains its structure.
	 */
	virtual void RefreshComponentTransforms ();

	/**
	 * Calculates the hover transform so that it covers all visible children branches for the given ExpandButton.
	 */
	virtual void CalcHoverTransform (UINT_TYPE expandButtonIdx);

	/**
	 * Updates the hover transform so that it covers over the specified tree list index.
	 */
	virtual void CalcHoverTransformAtIdx (Int branchIdx);

	/**
	 * Calculates the selected bar's transformation and visibility based on the selected item and current scroll position.
	 */
	virtual void UpdateSelectedBar ();

	/**
	 * Sets the given GuiComponent's transform to cover the specified branch.  Returns true if the transformation is visible (within range of scroll position).
	 */
	virtual bool CalcTransformAtIdx (GuiComponent* component, SDataBranch* branch);

	/**
	 * Destroys all buttons, connections, and labels that make up the tree list assembly.  This does not clear the tree list vector, itself.
	 */
	virtual void DestroyTreeComponents ();

	/**
	 * Replaces all button and labels to reflect the recent changes in tree structure while considering current scroll position.
	 */
	virtual void RefreshTree ();

	/**
	 * Creates a pair of solid color components such that they connect with parent and child points.
	 * Creates a horizontal connection if the child and parent X position is greater than connector thickness.
	 */
	virtual void CreateConnections (const Vector2& parentPt, const Vector2 childPt);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleExpandButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleExpandButtonRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleExpandButtonRolledOut (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleScrollPosChanged (Int newScrollPosition);
	virtual void HandleGarbageCollection ();


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class T>
	GuiDataElement<T>* GetSelectedDataElement () const
	{
		SDataBranch* selectedBranch = GetBranchFromIdx(SelectedItemIdx);
		if (selectedBranch != nullptr)
		{
			return dynamic_cast<GuiDataElement<T>*>(selectedBranch->Data);
		}

		return nullptr;
	}

	//TODO: Uncomment and fix the circular dependency between TreeListComponent and TreeBranchIterator.
#if 0
	/**
	 * Searches through this component's branches, and selects the first branch it finds that contains matching data with specified targetData.
	 * This function is not recommended if there are duplicated entries within the tree.
	 */
	template <class T>
	void SetSelectedItem (T targetData)
	{
		if (TreeList.size() <= 0)
		{
			return;
		}

		Int oldSelectedIdx = SelectedItemIdx;
		Int iterIdx = -1;
		SelectedItemIdx = INT_INDEX_NONE;
		SelectedBranch = nullptr;

		//Find branch containing the targetData
		for (TreeBranchIterator<SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
		{
			iterIdx++;
			GuiDataElement<T>* dataElement = dynamic_cast<GuiDataElement<T>*>(iter.GetSelectedBranch()->Data);
			if (dataElement != nullptr && dataElement->Data == targetData)
			{
				SelectedBranch = iter.GetSelectedBranch();
				SelectedItemIdx = iterIdx;
				break;
			}
		}

		UpdateSelectedItem(oldSelectedIdx != SelectedItemIdx);
	}
#endif
};
SD_END