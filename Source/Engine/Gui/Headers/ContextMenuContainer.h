/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContextMenuContainer.h
  Gui Entity that is responsible for managing the revealed ContextMenu GuiEntity instances.

  The purpose of this Entity is to allow the context menu GuiEntity instance to be rendered above
  other Entities regardless if the context menu component is inside a scrollbar or not.
  This Entity is also responsible for ensuring only at most one context menu is visible at 
  any given time.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

SD_BEGIN
class ContextMenuComponent;

class GUI_API ContextMenuContainer : public GuiEntity
{
	DECLARE_CLASS(ContextMenuContainer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The menu instance that is currently being revealed. */
	DPointer<GuiEntity> RevealedMenu;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Hides the current revealed instance if there is already one.
	 * Creates a GuiEntity class instance from the given menuClass. If successful, it'll assign it to RevealedMenu pointer.
	 * Returns the pointer to the revealed menu. Returns nullptr if nothing is instantiated.
	 */
	virtual GuiEntity* InstantiateMenu (const DClass* menuClass);

	/**
	 * If there is a RevealedMenu instance, this function will destroy that instance.
	 */
	virtual void RemoveMenuInstance ();

	/**
	 * Assumes that the RevealMenu instance is initialized (including its size), this function will position the menu near the revealPosition,
	 * and sets it to be visible.
	 */
	virtual void RevealContextMenu (ContextMenuComponent* comp, MousePointer* mouse);
};
SD_END