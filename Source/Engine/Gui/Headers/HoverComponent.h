/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HoverComponent.h
  Component that'll continuously evaluate the owning Entity's hover state, and invokes
  a callback whenever its hover state changes.
=====================================================================
*/

#pragma once

#include "Gui.h"

#include "GuiComponent.h"

SD_BEGIN
class GUI_API HoverComponent : public GuiComponent
{
	DECLARE_CLASS(HoverComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever the mouse is no longer hovering over its owner. */
	SDFunction<void, MousePointer*, Entity*> OnRollOut;

	/* Callback to invoke whenever the mouse is now hovering over its owner. */
	SDFunction<void, MousePointer*, Entity*> OnRollOver;

protected:
	/* Becomes true if the mouse is currently hovering over Owner. */
	bool bIsHoveringOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetIsHovering () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void EvaluateHoverState (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);
};
SD_END