/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiEntity.h
  Entity is a simple container that may possess a collection of GuiComponents.
  These Entities are allowed to be registered to the GuiDrawLayer for consistent rendering patterns.

  If focused, this Entity also relays focused input to sub components.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "ScrollableInterface.h"

SD_BEGIN
class GuiComponent;
class GuiDrawLayer;
class FocusComponent;
class FocusInterface;

class GUI_API GuiEntity : public Entity, public PlanarTransform, public ScrollableInterface
{
	DECLARE_CLASS(GuiEntity)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Default frequency when a GuiEntity reevaluates its size when AutoSize is true (in seconds).
	This can be overriden by accessing this Entity's Tick component. */
	static const Float DEFAULT_AUTO_SIZE_CHECK_FREQUENCY;

	/* Component that'll pass input events to focused entity.  This also handles tab ordering. */
	DPointer<FocusComponent> Focus;

	/* The draw layer responsible for rendering this Entity. */
	DPointer<GuiDrawLayer> DrawLayer;

	/* Input that passes in events to the GuiComponents. */
	DPointer<InputComponent> Input;

	/* Becomes true if this Entity is automatically adjusting its size based on its component size.
	This is only available when all immediate components are using absolute size rather than relative size.
	When this is true, this Entity will continuously iterate through all immediate Planar Components, and will
	automatically adjust its size based on the largest component.  Relative sized components are ignored. */
	bool AutoSizeHorizontal;
	bool AutoSizeVertical;

	/* The Scrollbar Component that is rendering this GuiEntity. If null, then this Entity is most likely drawn directy on a window. */
	DPointer<ScrollbarComponent> OwningScrollbar;

	/* Only applicable for GuiEntities rendered inside a scrollbar. This is the size this GuiEntity will take relative
	to the size of the scrollbar's frame size. 0.5 means half the size of the frame. 2 = twice the frame's size.
	If not a positive number, then these values are ignored.
	These are only applied whenever the Scrollbar's frame size changes. */
	Vector2 GuiSizeToOwningScrollbar;

	/* Tick Component that'll determine the frequency this Entity will recompute its size (only active when
	AutoSize is true. */
	DPointer<TickComponent> Tick;

	/* If not empty, the GuiEntity will apply the theme's style to any GuiComponents that attaches themselves to this Entity.
	Setting this variable will NOT automatically apply the style to existing components. */
	DString StyleName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual void InitializeScrollableInterface (ScrollbarComponent* scrollbarOwner) override;
	virtual ScrollbarComponent* GetScrollbarOwner () const override;
	virtual void RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner) override;
	virtual void UnregisterFromDrawLayer (RenderTexture* drawLayerOwner) override;
	virtual void GetTotalSize (Vector2& outSize) const override;
	virtual void HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize) override;
	virtual void ExecuteScrollableInterfaceInput (const sf::Event& evnt) override;
	virtual void ExecuteScrollableInterfaceText (const sf::Event& evnt) override;
	virtual void ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual bool ExecuteScrollableInterfaceConsumableInput (const sf::Event& evnt) override;
	virtual bool ExecuteScrollableInterfaceConsumableText (const sf::Event& evnt) override;
	virtual bool ExecuteScrollableInterfaceConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteScrollableInterfaceConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual void ClearScrollbarOwner (ScrollbarComponent* oldScrollbarOwner) override;
	virtual void RemoveScrollableObject () override;

protected:
	virtual bool AddComponent_Implementation (EntityComponent* newComponent) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets up this GuiEntity to be rendered and accept input from the main window.
	 * @param registerToMainRender If true, then this GuiEntity will register to the local GuiEngineComponent's MainOverlay draw layer.
	 * @param registerToMainInput If true, then this GuiEntity will subscribe to the input broadcaster affiliated with the main window.
	 * @param inputPriority If registering to the main window's input broadcaster, this input priority is used to determine the input
	 * order this entity will receive input events relative to other InputComponents.
	 */
	virtual void RegisterToMainWindow (bool registerToMainRender = true, bool registerToMainInput = true, Int inputPriority = 100);

	/**
	 * Removes focus from this GuiEntity and its components.
	 */
	virtual void LoseFocus ();

	/**
	 * Adds focus to this GuiEntity and its components.
	 * Gaining focus for one Entity does not lose focus for other Entities to allow multiple
	 * independent Entities to have focus if needed.
	 */
	virtual void GainFocus ();

	/**
	 * Creates or replaces the Input component that's subscribed to the specified InputBroadcaster.
	 * This InputComponent will relay input events to this Entity's GuiComponents.
	 */
	virtual void SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority);

	/**
	 * Iterates through all components to identify how large this Entity should be.
	 * Normally this is invoked automatically based on its Tick interval, but this can be invoked directly
	 * for force evaluation immediately.
	 * Only available when AutoSize is true.
	 */
	virtual void RefreshEntitySize ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDrawLayer (GuiDrawLayer* newDrawLayer);
	virtual void SetAutoSizeHorizontal (bool newAutoSizeHorizontal);
	virtual void SetAutoSizeVertical (bool newAutoSizeVertical);

	/* Determines how frequently this Entity should evaluate its auto sizing. */
	virtual void SetAutoSizeCheckFrequency (Float checksPerSec);

	virtual void SetGuiSizeToOwningScrollbar (const Vector2& newGuiSizeToOwningScrollbar);
	virtual void SetStyleName (const DString& newStyleName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	// Returns the component that selects which UI element is focused
	FocusComponent* GetFocus () const;

	inline GuiDrawLayer* GetDrawLayer () const
	{
		return DrawLayer.Get();
	}

	// Returns the focused object
	FocusInterface* GetFocusedObj () const;

	inline bool GetAutoSizeHorizontal () const
	{
		return AutoSizeHorizontal;
	}

	inline bool GetAutoSizeVertical () const
	{
		return AutoSizeVertical;
	}

	inline Vector2 GetGuiSizeToOwningScrollbar () const
	{
		return GuiSizeToOwningScrollbar;
	}

	inline TickComponent* GetTick () const
	{
		return Tick.Get();
	}

	inline DString GetStyleName () const
	{
		return StyleName;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Construct GuiComponents that'll assemble this UI instance.
	 */
	virtual void ConstructUI ();

	/**
	 * Creates and initializes the focus component.  This is where the tab order of the focus component is specified.
	 */
	virtual void InitializeFocusComponent ();

	/**
	 * Adjusts this GuiEntity's size based on it's owning Scrollbar frame size.
	 * Only applicable if at least one of the dimensions of GuiSizeToOwningScrollbar is positive.
	 */
	virtual void UpdateSizeBasedOnScrollbar (const Vector2& frameSize);

	/**
	 * Iterates through each GuiComponent, and invokes the given delegate to that component.
	 * This function will iterate in reverse direction to give components with higher render priority (rendered on top), delegate priority.
	 * If the delegate returns true, it'll abort the iterator.
	 */
	virtual void InvokeOnEachComponent (const std::function<bool(GuiComponent*)>& lambda);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePassiveInput (const sf::Event& evnt);
	virtual void HandlePassiveText (const sf::Event& evnt);
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual void HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual void HandlePassiveMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);
	virtual bool HandleConsumableInput (const sf::Event& evnt);
	virtual bool HandleConsumableText (const sf::Event& evnt);
	virtual bool HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool HandleConsumableMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	virtual void HandleAutoSizeTick (Float deltaSec);
};
SD_END