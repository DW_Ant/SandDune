/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorButtonState.h
  A component that adjusts the owning button's fill color based on the
  button's state.
=====================================================================
*/

#pragma once

#include "ButtonStateComponent.h"

SD_BEGIN

class GUI_API ColorButtonState : public ButtonStateComponent
{
	DECLARE_CLASS(ColorButtonState)


	/*
	=====================
	 Properties
	=====================
	*/

protected:
	/* Various colors to use on the button based on its state. */
	Color DefaultColor;
	Color DisabledColor;
	Color HoverColor;
	Color DownColor;


	/*
	=====================
	 Inherited
	=====================
	*/

public:
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* cpy) override;
	virtual void SetDefaultAppearance () override;
	virtual void SetDisableAppearance () override;
	virtual void SetHoverAppearance () override;
	virtual void SetDownAppearance () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetDefaultColor (Color newDefaultColor);
	void SetDisabledColor (Color newDisabledColor);
	void SetHoverColor (Color newHoverColor);
	void SetDownColor (Color newDownColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Color GetDefaultColor () const
	{
		return DefaultColor;
	}

	inline Color GetDisabledColor () const
	{
		return DisabledColor;
	}

	inline Color GetHoverColor () const
	{
		return HoverColor;
	}

	inline Color GetDownColor () const
	{
		return DownColor;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sets the owning button's color fill to the specified value.
	 */
	virtual void SetButtonColor (Color newColorFill);
};
SD_END