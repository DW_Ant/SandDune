/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollbarComponent_Deprecated.h
  A component that tracks a scroll position and calls back any
  changes to that scroll position for its owner.
=====================================================================
*/

#pragma once

#include "FrameComponent.h"

SD_BEGIN
class ButtonComponent;
class ScrollbarTrackComponent;

class GUI_API ScrollbarComponent_Deprecated : public GuiComponent
{
	DECLARE_CLASS(ScrollbarComponent_Deprecated)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to the track component that resides between the two scroll buttons. */
	DPointer<ScrollbarTrackComponent> Track;

	/* Sprite component responsible for drawing the arrows representing the MiddleMousePosition. */
	DPointer<GuiComponent> MiddleMouseSpriteOwner;
	DPointer<SpriteComponent> MiddleMouseSprite;

	/* Sprite component that renders over the mouse cursor while the Middle Mouse is actively scrolling this bar. */
	DPointer<GuiComponent> PanningMiddleMouseCursorOwner;
	DPointer<SpriteComponent> PanningMiddleMouseCursor;

	DPointer<ButtonComponent> TopScrollButton;
	DPointer<ButtonComponent> BottomScrollButton;

	/* When either scroll buttons are pressed, this determines how many indices the scrollbar may jump. */
	Int ScrollJumpInterval;

	/* If true, then the user may scroll this scrollbar using the middle mouse button. */
	bool bEnableMiddleMouseScrolling;

	/* If MiddleMousePosition is actively scrolling this scrollbar, then this determines how much time
	is needed before the scrollbar may scroll again.  The interval varies based on the cursor's position
	relative to the MiddleMousePosition variable.  Max value is achieved when the distance is greater than
	the owner's absolute size.*/
	Range<Float> MiddleMouseScrollIntervalRange;

	/* If greater than 0, then this determines how much time is needed for the MouseWheel to be held
	down for the MiddleMousePosition to be in 'release mode' where the user must release the middle mouse
	button again to turn off the middle mouse position.  Otherwise (when middle mouse button was tapped),
	then the user must click the middle mouse button again to turn off middle mouse position. */
	Float MiddleMouseHoldThreshold;

	/* When pressing the top/bottom scroll buttons, this is the time needed for the user to hold before
	the scrollbar will continuously increment. */
	Float ContinuousScrollDelay;

	/* This determines how much time is needed to pass to jump again when continuously scrolling. */
	Float ContinuousInterval;

	/* If true, then the mouse pointer will be clamped to the owning entity whenever scrolling via middle mouse button. */
	bool bClampsMouseWhenScrolling;

	/* Callback whenever the scroll bar changed visibility whenever num scroll positions passed the num visible scroll position threshold. */
	SDFunction<void> OnToggleVisibility;

protected:
	/* Sets the current scrolling index where 0 means very top, and higher values are lower. */
	Int ScrollPosition;

	/* Maximum number of visible entries that includes the number of visible scroll positions. */
	Int MaxScrollPosition;

	/* Determines how many scroll positions are visible at once.  For example, this is how many lines
	may be displayed in a TextField without having to scroll. */
	Int NumVisibleScrollPositions;

	/* True if the scroll bar may be interacted. */
	bool bEnabled;

	/* If true, then the scrollbar will hide itself when the number of visible scroll positions is less than max scroll position. */
	bool bHideWhenInsufficientScrollPos;

	/* Becomes true while the scroll buttons are held. */
	bool bHoldingScrollButtons;

	/* If nonnegative, then the scroll bar will be scrolling automatically based on the mouse's position
	relative to the MiddleMousePosition.  This toggles whenever the middle mouse button was pressed. */
	Vector2 MiddleMousePosition;

	/* Timestamp when the middle mouse button was pressed down. */
	Float MiddleMouseTimeStamp;

	/* Function callback whenever the scroll position changed. */
	std::function<void(Int newScrollPosition)> OnScrollPositionChanged;

	/* Mouse that interacted with this scrollbar via middle mouse click.  Used to restore mouse visibility after
	interaction, and to calculate scroll panning interval. */
	DPointer<MousePointer> InteractingMouse;

private:
	/* Direction where the middle mouse is currently panning.  1 = up, 0 = none, -1 = down. */
	Int MiddleMousePanDirection;

	/* Time remaining before the scrollbar pans again due to middle mouse position.  Disabled if negative. */
	Float MiddleMousePanTimeRemaining;

	/* Time remaining before the scrollbar pans again due to one of its buttons is held.  Disabled if negative. */
	Float ButtonPanTimeRemaining;

	/* If true, then this component is clamping the mouse pointer. */
	bool bClampingMousePointer;

	/* Becomes true if this scrollbar is visible since there are enough scroll positions (used to see if it needs to invoke OnToggleVisibility). */
	bool bScrollbarVisible;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

protected:
	virtual void InitializeComponents () override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual bool AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the specified index is currently visible based on this scrollbar's current scroll position.
	 */
	virtual bool IsIndexVisible (const Int index) const;

	/**
	 * Jumps the scroll position relative to the current scroll position.
	 * Negative value typically means scrolling up.
	 */
	virtual void IncrementScrollPosition (Int amountToJump);
	virtual void SetScrollPositionChanged (std::function<void(Int newScrollPosition)> newHandler);
	virtual void SetScrollPosition (Int newScrollPosition);
	virtual void SetMaxScrollPosition (Int newMaxScrollPosition);
	virtual void SetNumVisibleScrollPositions (Int newNumVisibleScrollPositions);
	virtual void SetEnabled (bool bNewEnabled);
	virtual void SetHideWhenInsufficientScrollPos (bool bNewHideWhenInsufficientScrollPos);
	virtual void SetMiddleMousePosition (MousePointer* mouse, const Vector2& newPosition);

	/**
	 * Quick method to adjust scroll button height to the given value.
	 * Automatically adjusts the scrollbar track component to go between the buttons.
	 */
	virtual void SetScrollButtonHeight (Float newScrollButtonHeight);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Int GetScrollPosition () const;
	virtual Int GetMaxScrollPosition () const;
	virtual Int GetLastVisibleScrollPosition () const;
	virtual Int GetNumVisibleScrollPositions () const;
	virtual bool GetEnabled () const;
	virtual bool GetHideWhenInsufficientScrollPos () const;
	virtual Vector2 GetMiddleMousePosition () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Hook to initialize default properties for the top/bottom scroll buttons.
	 */
	virtual void InitializeScrollButtons ();

	/**
	 * Hook to initialize the Scrollbar Track Component.
	 */
	virtual void InitializeTrackComponent ();

	virtual void InitializePanningMiddleMouse (MousePointer* mouse);

	/**
	 * Pans the scrollbar from the middle mouse click, and recomputes the next pan interval.
	 */
	virtual void PanByMiddleMouse ();

	/**
	 * Pans the scrollbar from one of its buttons being held down.
	 */
	virtual void PanByButtonPress ();

	/**
	 * Considers all factors that determine if the top/bottom scroll buttons should be enabled or not.
	 */
	virtual void EvaluateScrollButtonEnabledness ();

	/**
	 * Considers all factors that determine if the scrollbar should be visible or not.
	 */
	virtual void EvaluateScrollVisibility ();

	/**
	 * Computes the time interval needed before HandleMiddleMouseMove is called again.
	 */
	virtual Float CalculateMiddleMouseMoveInterval ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * Invoked whenever either scroll button was pressed.
	 */
	virtual void HandleScrollButtonPressed (ButtonComponent* uiComponent);

	/**
	 * Invoked whenever either scroll button was released.
	 */
	virtual void HandleScrollButtonReleased (ButtonComponent* uiComponent);

	/**
	 * Invoked whenever the track component wishes to change the scroll index.
	 */
	virtual void HandleTrackChangedPosition (Int newScrollPosition);

	/**
	 * Returns false whenever this scrollbar should no longer clamp the mouse pointer.
	 */
	virtual bool HandleLimitCallback (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvent);

	virtual void HandleTick (Float deltaSec);
};
SD_END