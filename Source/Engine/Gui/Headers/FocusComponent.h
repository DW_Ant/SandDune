/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FocusComponent.h
  Component that passes redirected input events to a targeted Entity, and handles tabbing through Entities.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class FocusInterface;

class GUI_API FocusComponent : public EntityComponent
{
	DECLARE_CLASS(FocusComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Entities to iterate whenever the tab key was pressed.  This vector is not checked.  Should an object be destroyed, should also remove itself from this vector.*/
	std::vector<FocusInterface*> TabOrder;

protected:
	/* Index of TabOrder that determines the current selected entity.  Becomes INT_INDEX_NONE if nothing is selected. */
	Int SelectedEntityIdx;

	DPointer<InputComponent> Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Finds the next entity to be selected based on the TabOrder.  Resets back to the beginning if it reached the end of list.
	 */
	virtual void SelectNextEntity ();

	/**
	 * Attempts to select the specified object. This object must reside in the TabOrder vector, and it must be focusable.
	 * Returns true if target is found, and it's focused. Selecting a focused target also returns true even if it doesn't do anything.
	 * Returns false if it's either not in the TabOrder vector or not focusable.
	 */
	virtual bool SelectObject (FocusInterface* target);

	/**
	 * Ensures this component is not selecting an Entity.
	 */
	virtual void ClearSelection ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual FocusInterface* GetSelectedEntity () const;
	virtual InputComponent* GetInput () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeInput ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& keyEvent);
	virtual bool HandleText (const sf::Event& keyEvent);
};
SD_END