/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FakeMouseOwnerInterface.h

  An interface the FakeMouse references.

  All objects that manages a FakeMouse instance must inherit from this interface.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GUI_API FakeMouseOwnerInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a vector that converts the given mouse coordinates (in outer coordinates) to inner mouse coordinates.
	 */
	virtual Vector2 FakeMouseToInnerCoordinates (const Vector2& outerMousePos) const = 0;

	/**
	 * Returns a vector that converts the given mouse coordinates (in inner coordinates) to outer coordinates.
	 */
	virtual Vector2 FakeMouseToOuterCoordinates (const Vector2& innerMousePos) const = 0;
};
SD_END