/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiDrawLayer.h

  A DrawLayer for UI overlays where the draw order of registered Entities is based on EntityComponents
  where subcomponents are rendered over their owners.

  This DrawLayer interprets transformation objects as data relative to the screenspace and to the immediate
  owning Entity.  The screen space is first defined before kicking off the iterator.  It'll then
  sort all registered GuiEntities based on their transform's Z-value where GuiEntities with higher Z-values
  are rendered over other GuiEntities with lesser Z-values.

  For each GuiEntity, the iterator will step through its components where sub components are rendered over
  their owners.  The component's transform data is interpretted as either absolute or scale multipliers.

  For Translation:
  Values ranging from 0-1 implies that the Component is positioned relative to their owner where 0 is
  left aligned, 0.5 is centered, and 1 is right aligned.  Right aligned implying that the left border of the
  component meets the right border of the Owner.

  Values ranging above 1 implies that the component is positioned at an offset relative to the owning component.
  The offset is determined based on the transform's translation.  For example if the translation is (32, 64),
  then the component's top left corner is rendered 32 pixels to the left and 64 pixels below the owning component's
  top left corner.

  For Scaling:
  Values ranging from 0-1 act like multipliers to the owning component's scale where 0 is no scaling, 0.5 is half
  scale, and 1.0 is full scale.  If the owning component is 512 pixels wide, and the component's scaling is 0.75,
  then the resulting size of the component is 384 pixels wide.

  Values ranging above 1 are interpretted as absolute size.  If a component's transform scale is (64, 32), then
  the component will be 64 pixels wide and 32 pixels tall regardless of the size of its owners.

  Culling:
  The GuiDrawLayer only culls components that are invisible.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiEntity;
class TooltipGuiEntity;

class GUI_API GuiDrawLayer : public PlanarDrawLayer
{
	DECLARE_CLASS(GuiDrawLayer)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct GUI_API SCommonGuiClasses
	{
		/* The DClass reference to a TooltipGuiEntity. */
		const DClass* TooltipClass;

		/* The DClass reference to a DropdownGuiEntity. There isn't a reference in the editor theme since this class doesn't copy anything from it. */
		const DClass* DropdownEntityClass;

		/* The DClass reference to a ContextMenuContainer. This isn't used in the UseTheme since there isn't anything to copy from the theme. */
		const DClass* ContextContainerClass;

		/**
		 * Defaults to all classes to use the base classes.
		 * If bUseTheme is true, then it'll reference classes from the current theme instead.
		 */
		SCommonGuiClasses (bool bUseTheme);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Priority value that determines the draw order the main UI overlay renders relative to other draw layers. */
	static const Int MAIN_OVERLAY_PRIORITY;
	static const Int FOREGROUND_PRIORITY;

protected:
	/* List of GuiEntities to render through each Tick. */
	std::vector<GuiEntity*> RegisteredMenus;

private:
	/* Becomes true whenever this draw layer is in the middle of accessing its RegisteredMenus. */
	bool bAccessingMenus;

	/* List of menus that are pending to be removed from the RegisteredMenus. */
	std::vector<GuiEntity*> PendingMenuRemoveList;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void HandleGarbageCollection () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the specified menu to the list of menus to render.
	 */
	virtual void RegisterMenu (GuiEntity* newMenu);

	/**
	 * Iterates through the registered menus and removes it from the list if found.
	 */
	virtual void UnregisterMenu (GuiEntity* target);

	/**
	 * Creates and initializes GuiEntity commonly used GuiEntities. If the given data struct contains nullptr references, this function will skip them.
	 * If an InputBroadcaster is specified, the GuiEntities will register themselves to that broadcaster.
	 */
	virtual void InitCommonEntities (const SCommonGuiClasses& inCommonClasses, InputBroadcaster* input);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<GuiEntity*>& ReadRegisteredMenus () const
	{
		return RegisteredMenus;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sifts through the RegisteredMenus to identify the order the Entities should render.
	 * Sorts the vector of RegisteredMenu based on their transform Z-order.
	 * This function will also clear all empty and destroyed RegisteredMenus.
	 */
	virtual void PrepareRegisteredMenus ();

	/**
	 * Iterates through the list of registered menu, and removes them from the list if they are
	 * destroyed or empty.
	 */
	virtual void PurgeExpiredMenus ();

	/**
	 * Iterates through the specified GuiEntity to render all of its GuiComponents.
	 */
	virtual void RenderGuiEntity (const GuiEntity* menu, RenderTarget* renderTarget, const Camera* camera) const;
};
SD_END