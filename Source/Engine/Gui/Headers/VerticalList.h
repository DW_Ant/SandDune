/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VerticalList.h
  A simple component that dynamically positions each of its ui component list to be listed sequentially.
  This component will also horizontally and vertically scale to fit each ui component.

  The order the components appear in the list is determined by the ComponentOrder vector.

  Only immediate visible ui components are considered.
  This component will not position its subcomponents horizontally.

  Ui components that are using vertical normalized coordinates are ignored since they scale according to
  the VerticaList size instead.

  The reason why it uses ticks for updates instead of relying on the sub component's transform
  changed delegates is to prevent excessive refresh calls since each ui component would update
  their transforms on moves or when the transform this object is relative to changes.
  Using the tick approach allows the developer to configure the update frequency, keeps things simple,
  and it prevents excessive calls to refresh.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class GUI_API VerticalList : public GuiComponent
{
	DECLARE_CLASS(VerticalList)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Specifies the vertical spacing between each component. These are in absolute units since it doesn't make
	sense to use normalized coordinates for an owning component that automatically adjusts size based on sub component size. */
	Float ComponentSpacing;

protected:
	/* List of components to consider when determining this component's vertical size. This component will also position each of these
	components based on the order they appear in this vector. Clean up is not necessary. Destroyed components will automatically be removed
	from this list. The reason this vector is considered instead of simply using this Entity's component list primarily to support arbitrary
	ordering, differentiate draw order from sort order, and support component insertion without having to manipulate this entity's component list. */
	std::vector<DPointer<GuiComponent>> ListOrder;

	/* The Entity that'll cause the vertical list to update its ui component transforms.
	This tick's update can be set. If disabled, then RefreshComponentTransforms must be explicitly called. */
	DPointer<TickComponent> RefreshingTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

protected:
	virtual bool AddComponent_Implementation (EntityComponent* newComponent) override;
	virtual bool RemoveComponent (EntityComponent* target) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through the items to recompute their positions and adjusts this component's size to fit all items.
	 */
	virtual void RefreshComponentTransforms ();

	/**
	 * If the given component exists in the ListOrder, it'll move it to the beginning (top) of the list.
	 */
	virtual void MoveComponentToTop (GuiComponent* comp);

	/**
	 * If the given component exists in the ListOrder, it'll move it to the end (bottom) of the list.
	 */
	virtual void MoveComponentToBottom (GuiComponent* comp);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<DPointer<GuiComponent>>& ReadListOrder () const
	{
		return ListOrder;
	}

	inline std::vector<DPointer<GuiComponent>>& EditListOrder ()
	{
		return ListOrder;
	}

	inline TickComponent* GetRefreshingTick () const
	{
		return RefreshingTick.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleRefreshTick (Float deltaSec);
};
SD_END