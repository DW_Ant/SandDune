/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFieldRenderComponent.h
  A render component that renders the cursor blinker that represents the cursor position.

  It is also responsible for rendering the highlight background to represent
  the selected characters.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class TextFieldComponent;

class GUI_API TextFieldRenderComponent : public TextRenderComponent
{
	DECLARE_CLASS(TextFieldRenderComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SHighlightLine
	{
		/* The SFML instance responsible for rendering the highlight. This also contains the absolute position and size. */
		sf::RectangleShape DrawRect;

		/* Index to the TextField's line vector that indicates what this DrawRect will highlight behind. */
		size_t LineIdx;

		/* Index to the character position in the specified line where the highlight begins. */
		size_t StartIdx;

		/* Index to the character position in the specified line where the highlight ends on this line. */
		size_t EndIdx;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the time interval the cursor will toggle visibility. */
	Float CursorBlinkInterval;

	/* Parameters that adjusts how the cursor appears. */
	sf::Color CursorColor;
	Float CursorWidth;

	/* Multiplies the cursor's vertical length by this value. */
	Float CursorHeightMultiplier;

	/* Determines how long the cursor should render solid before blinking again after the cursor position recently changed.
	Specifying negative values would have the cursor blink regardless if the cursor moved recently. */
	Float MoveDetectionDuration;

protected:
	/* Time when the cursor can resume blinking behavior.  If negative, then this is disabled. */
	Float SolidCursorRenderEndTime;

	/* The text position that indicates where the highlighting begins. Becomes negative if not highlighting. */
	Int StartHighlightPosition;

	/* The text position that indicates where the highlighting ends. Becomes negative if not highlighting. */
	Int EndHighlightPosition;

	/* Reference to TextField that's responsible for placing the cursor positions. */
	DPointer<TextFieldComponent> OwningTextField;

	std::vector<SHighlightLine> HighlightLines;

	/* Color to use for the highlight lines. */
	Color HighlightColor;

	/* If true, then this component will recalculate the cursor and highlight draw coordinates on next Tick regardless
	* if the cursor changed position or not. This flag will reset to false after each tick. */
	bool bForceRecalcDrawCoordinates;

private:
	/* Engine this component is registered to. */
	Engine* LocalEngine;

	/* Used to detect cursor movement.  The cursor should always be visible if it recently moved. */
	Float LastCursorMoveTime;

	/* Cached results from FindCursorCoordinates that's associated with the latest cursor position. */
	sf::Vector2f CursorCoordinates;

	/* Projection data recorded to track when it needs to recalculate the cursor position. */
	Transformation::SScreenProjectionData LastProjectionData;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Temporarily pauses the cursor's blinking behavior so that it renders for the given renderTime.
	 * @Param renderTime:  Number of seconds the cursor will render for before returning to blinking behavior.
	 */
	virtual void PauseBlinker (Float renderTime);

	/**
	 * Notifies the RenderComponent to recalculate the draw coordinates for the cursor position and highlighting shapes.
	 */
	virtual void RefreshDrawCoordinates ();

	/**
	 * Moves the StartHighlightPosition by the specified amount. A positive parameter moves the start position away from the first character.
	 * A negative parameter moves the start position towards the first character.
	 */
	virtual void ShiftStartHighlight (Int shiftAmount);

	/**
	 * Moves the EndHighlightPosition by the specified amount. A positive parameter moves the end position away from the first character.
	 * A negative parameter moves the end position towards the first character.
	 */
	virtual void ShiftEndHighlight (Int shiftAmount);

	/**
	 * Replaces the highlight shapes with new ones that'll cover between start and end highlight positions.
	 * The highlight positions are absolute where 0 indicates the front of the first character on the first line.
	 * StartHighlightPosition must be less than EndHighlightPosition.
	 * If either variable is negative, then it removes all highlighting.
	 */
	virtual void SetHighlightRange (Int newStartHighlightPosition, Int newEndHighlightPosition);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetHighlightColor (Color newHighlightColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetStartHighlightPosition () const
	{
		return StartHighlightPosition;
	}

	inline Int GetEndHighlightPosition () const
	{
		return EndHighlightPosition;
	}

	inline TextFieldComponent* GetOwningTextField () const
	{
		return OwningTextField.Get();
	}

	inline Color GetHighlightColor () const
	{
		return HighlightColor;
	}

	inline const sf::Vector2f& ReadCursorCoordinates () const
	{
		return CursorCoordinates;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the given highlight line's end position is at or beyond the line's last position.
	 * This function does not perform any sanity checks.
	 */
	virtual bool IsAtEnd (const SHighlightLine& highlightLine) const;

	/**
	 * Given the cursor position, this function will find the absolute coordinates where the cursor should be drawn.
	 * This will return negative values if the cursor position should not be rendered.
	 */
	virtual sf::Vector2f FindCursorCoordinates (const Transformation::SScreenProjectionData& projectionData, Int cursorIndex);

	/**
	 * Updates the SFML draw rectangles for each highlight line that matches the new projection data.
	 */
	virtual void CalcHighlightTransforms (const Transformation::SScreenProjectionData& projectionData);
};
SD_END