/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiDataElement.h
  An object representing information of an object that can be displayed within Gui containers
  such as dropdowns and tables.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
/**
 * Base class for GuiDataElement so the non templated variables may point to templated types.
 */
class BaseGuiDataElement
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If not empty, this is the text to display on the UI component to represent this object. */
	DString LabelText;

	/* If true, then the user cannot toggle this element's selection.
	The system would still be able to set the item selection. This flag simply prohibits the user from doing so. */
	bool bReadOnly;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	BaseGuiDataElement () :
		LabelText(DString::EmptyString),
		bReadOnly(false)
	{
		//Noop
	}

	BaseGuiDataElement (const BaseGuiDataElement& cpyObj) :
		LabelText(cpyObj.LabelText),
		bReadOnly(cpyObj.bReadOnly)
	{
		//Noop
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString GetLabelText () const
	{
		return LabelText;
	}

	inline const DString& ReadLabelText () const
	{
		return LabelText;
	}
};

template <class T>
class GuiDataElement : public BaseGuiDataElement
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The represented object that could be a DProperty, DClass, Object, etc... */
	T Data;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GuiDataElement () : BaseGuiDataElement()
	{
		//Noop
	}

	GuiDataElement (const T& inData, const DString& inLabelText) : BaseGuiDataElement()
	{
		Data = inData;
		LabelText = inLabelText;
	}

	GuiDataElement (const GuiDataElement& inCopyData) : BaseGuiDataElement(inCopyData)
	{
		Data = inCopyData.Data;
		LabelText = inCopyData.LabelText;
	}

	virtual ~GuiDataElement ()
	{
		//Noop
	}
};
SD_END