/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiTheme.h
  A class that references resources that should persist throughout multiple Gui components
  even if the textures are used within one class (to have a consistent feel throughout the application).
  Subclassed themes may have additional texture types, and also additional shared
  aesthetics across UI Components (such as sounds).

  In summary:
  A single theme may hold multiple styles that vary from one another. Typically an app sticks with one theme.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiComponent;

class GUI_API GuiTheme : public Object
{
	DECLARE_CLASS(GuiTheme)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SStyleInfo
	{
		/* Name that uniquely identifies this style. */
		DString Name;

		/* List of UI templates other UI components of identical classes will use to copy appearance properties from. */
		std::vector<GuiComponent*> Templates;

		SStyleInfo ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString DEFAULT_STYLE_NAME;

	/* Various mouse pointer icon overrides. */
	DPointer<Texture> TextPointer;
	DPointer<Texture> ResizePointerVertical;
	DPointer<Texture> ResizePointerHorizontal;
	DPointer<Texture> ResizePointerDiagonalTopLeft;
	DPointer<Texture> ResizePointerDiagonalBottomLeft;
	DPointer<Texture> ScrollbarPointer; //Overrides mouse pointer when using middle mouse to scroll.  The sprite is assumed to have 3 subdivisions (scroll up, no scroll, and scroll down).

	/* Various frame textures. */
	DPointer<Texture> FrameBorder;
	DPointer<Texture> FrameFill;

	DPointer<Texture> CheckboxTexture;

	/* Various button textures. */
	DPointer<Texture> ButtonBorder;
	DPointer<Texture> ButtonFill; //For stretchable buttons (framed buttons), the background is filled with this sprite.
	DPointer<Texture> ButtonSingleSpriteBackground; //Sprite to use for single sprite button components (using subdivisions)

	/* Various scrollbar textures. */
	DPointer<Texture> ScrollbarUpButton;
	DPointer<Texture> ScrollbarDownButton;
	DPointer<Texture> ScrollbarLeftButton;
	DPointer<Texture> ScrollbarRightButton;
	DPointer<Texture> ScrollbarZoomButton;
	DPointer<Texture> ScrollbarMiddleMouseScrollAnchor; //sprite to display where the middle mouse button was pressed
	DPointer<Texture> ScrollbarMouseAnchorStationary;
	DPointer<Texture> ScrollbarMouseAnchorPanUp;
	DPointer<Texture> ScrollbarMouseAnchorPanUpRight;
	DPointer<Texture> ScrollbarMouseAnchorPanRight;
	DPointer<Texture> ScrollbarMouseAnchorPanDownRight;
	DPointer<Texture> ScrollbarMouseAnchorPanDown;
	DPointer<Texture> ScrollbarMouseAnchorPanDownLeft;
	DPointer<Texture> ScrollbarMouseAnchorPanLeft;
	DPointer<Texture> ScrollbarMouseAnchorPanUpLeft;

	/* Various dropdown textures. */
	DPointer<Texture> DropdownCollapsedButton;
	DPointer<Texture> DropdownExpandedButton;

	DPointer<Texture> TooltipBackground;

	/* Various tree list textures. */
	DPointer<Texture> TreeListAddButton;
	DPointer<Texture> TreeListSubtractButton;

	DPointer<Font> GuiFont;

	/* TooltipGuiClass to use when instantiating the tootip for GuiDrawLayers. */
	const DClass* TooltipClass;

	/* The default ContextMenu GuiEntity instance to use when ContextMenuComponents don't specify a context menu class. */
	const DClass* DefaultContextMenuClass;

protected:
	/* Index to the style list that is the default style newly created UI Components will mirror their default appearances from.
	Becomes negative if there isn't a default style selected. */
	Int DefaultStyleIdx;

	/* List of styles UI components can retrieve their appearance info from. */
	std::vector<SStyleInfo> Styles;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and returns a new StyleInfo that contains a copy of the copyObj's Templates.
	 * This is essentially a deep copy where all Gui Template mirrored their properites from copyObj's templates.
	 */
	static SStyleInfo CreateStyleFrom (const SStyleInfo& copyObj);

	/**
	 * Imports various resources such as textures and fonts UI elements may access.
	 */
	virtual void InitializeTheme ();

	/**
	 * Creates all UI styles that UI components may access to mirror their appearance properties from.
	 */
	virtual void InitializeStyles ();

	/**
	 * Retrieves a reference to the UI template with matching class.
	 * @param styleName Name of the style collection to search for the template in.
	 * @param templateClass UIComponent's class that must match the template's class.  Parent and subclasses are excluded.
	 */
	virtual const GuiComponent* FindTemplate (const DString& styleName, const DClass* templateClass) const;
	static const GuiComponent* FindTemplate (const SStyleInfo* style, const DClass* templateClass);
	static GuiComponent* FindTemplate (SStyleInfo* style, const DClass* templateClass);

	/**
	 * Searches through the style list and returns the data struct with matching name (case sensitive).
	 * Complexity is linear.
	 */
	const SStyleInfo* FindStyle (const DString& styleName) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDefaultStyle (const DString& styleName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Retrieves the Gui Theme from the GuiEngineComponent associated with the calling/local thread.
	 */
	static GuiTheme* GetGuiTheme ();

	/**
	 * Return the style info that GuiComponents will reference when they're instantiated.
	 */
	virtual const SStyleInfo* GetDefaultStyle () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Instantiates various UI Components to be the templates for the default style.
	 */
	virtual void InitializeDefaultStyle ();
};
SD_END