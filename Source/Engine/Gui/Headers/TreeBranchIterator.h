/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TreeBranchIterator.h
  This iterator will iterate through the branches for a TreeListComponent.

  Note:  This is a templated class since this iterator is agnostic to the pointer it refers to.
  This will help mitigate the conflict between const SDataBranch and SDataBranch.
=====================================================================
*/

#pragma once

#include "Gui.h"

#include "TreeListcomponent.h"

SD_BEGIN
template <class T>
class TreeBranchIterator
{


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/* When stepping through the branch hierarchy, this struct contains necessary information which child class was iterated. */
	struct SParentBranchInfo
	{
		T* ParentBranch;

		/* The index of the ParentBranch's children vector that is the equal to the current selected branch. */
		Int ChildIndex;

		SParentBranchInfo ()
		{
			ParentBranch = nullptr;
			ChildIndex = -1;
		}

		SParentBranchInfo(T* inParentBranch, int inChildIndex)
		{
			ParentBranch = inParentBranch;
			ChildIndex = inChildIndex;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Data branch this iterator currently sits on. */
	T* SelectedBranch;

	/* Information regarding the selected branch's chain of parent branches where element 0 is the root and the last element is the immediate parent. */
	std::vector<SParentBranchInfo> ParentBranchChain;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TreeBranchIterator (T* baseBranch)
	{
		SelectedBranch = baseBranch;
	}

	TreeBranchIterator (const TreeBranchIterator& copyObj)
	{
		SelectedBranch = copyObj.SelectedBranch;
		ParentBranchChain = copyObj.ParentBranchChain;
	}

	~TreeBranchIterator ()
	{

	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator++ ()
	{
		SelectNextBranch(false);
	}

	void operator++ (int)
	{
		SelectNextBranch(false);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates this branch based on if the current selected branch is expanded or not.
	 */
	void SelectNextVisibleBranch ()
	{
		SelectNextBranch(!GetSelectedBranch()->bExpanded);
	}

	/**
	 * Iterates to the next branch, but skipping all children branches of selected branch.
	 */
	void SelectSiblingBranch ()
	{
		SelectNextBranch(true);
	}

	/**
	 * Returns a vector of all parents of selected branch where element 0 is the root, and the last element is the one closest to branch.
	 */
	std::vector<T*> GetParentBranches () const
	{
		std::vector<TreeListComponent::SDataBranch*> parentChain;

		for (UINT_TYPE i = 0; i < ParentBranchChain.size(); i++)
		{
			parentChain.push_back(ParentBranchChain.at(i).ParentBranch);
		}

		return parentChain;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	T* GetSelectedBranch () const
	{
		return SelectedBranch;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Selects next available branch.  If bIgnoreChildren is false, this will iterates to the next child branch.
	 * If no child branches are available, then it'll select the next sibling branch.
	 * If no sibling branches are available, then it'll iterate the parent branch's next sibling.
	 */
	void SelectNextBranch (bool bIgnoreChildren)
	{
		if (SelectedBranch == nullptr)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to iterate to next branch since the iterator's current branch is null!"));
			return;
		}

		/*
		For a tree that has the structure listed below, this is the order of branches selected.
		0
		+----1
		|	 +----2
		|	 +----3
		+----4
		+----5
		|    +----6
		+----7
		     +----8
		*/

		//First try selecting a child
		if (!bIgnoreChildren && SelectedBranch->Children.size() > 0)
		{
			ParentBranchChain.push_back(SParentBranchInfo(SelectedBranch, 0));
			SelectedBranch = SelectedBranch->Children.at(0);
			return;
		}
		else
		{
			//Select siblings
			while (ParentBranchChain.size() > 0)
			{
				//Get next immediate parent
				SParentBranchInfo* curParent = &ParentBranchChain.at(ParentBranchChain.size() - 1);
				curParent->ChildIndex++; //Select next sibling

				if (curParent->ChildIndex.ToUnsignedInt() >= curParent->ParentBranch->Children.size())
				{
					//Finished iterating curParent.  Remove from chain
					ParentBranchChain.pop_back();
				}
				else
				{
					//Select that sibling
					SelectedBranch = curParent->ParentBranch->Children.at(curParent->ChildIndex.ToUnsignedInt());
					return;
				}
			}
		}

		//Ran out of branches - terminate iterator
		SelectedBranch = nullptr;
	}
};
SD_END