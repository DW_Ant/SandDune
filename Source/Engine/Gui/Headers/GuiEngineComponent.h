/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiEngineComponent.h
  Loads default resources such as fonts and icons.  This also instantiates
  global Gui objects such as themes and tooltip entity.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiTheme;
class GuiDrawLayer;

class GUI_API GuiEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(GuiEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* UI Theme to spawn when this component initializes. */
	const DClass* DefaultThemeClass;

	/* The active GuiTheme that determines the overall appearance for all UI. */
	DPointer<GuiTheme> RegisteredGuiTheme;

	/* UI layer that'll be rendering general menus over the main window. */
	DPointer<GuiDrawLayer> MainOverlay;

	/* UI layer that'll be rendering above most menus. Typically this contains 'pop out' entities that shouldn't reside in a scrollbar such as tooltips, context menus, and expanded Dropdowns. */
	DPointer<GuiDrawLayer> ForegroundUiLayer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GuiEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Replaces the default Gui theme with the specified theme.  This function must be called prior to
	 * InitializeComponent.  The DClass must be a type GuiTheme.
	 */
	virtual void SetDefaultThemeClass (const DClass* newDefaultThemeClass);

	virtual void SetGuiTheme (GuiTheme* newGuiTheme);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual GuiTheme* GetGuiTheme () const;

	inline GuiDrawLayer* GetMainOverlay () const
	{
		return MainOverlay.Get();
	}

	inline GuiDrawLayer* GetForegroundUiLayer () const
	{
		return ForegroundUiLayer.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportGuiEssentialTextures ();
};
SD_END