/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiTester.h
  Class that'll instantiate various UI Components so that the user
  may test their functionality without external interference.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class BaseGuiDataElement;
class ButtonComponent;
class CheckboxComponent;
class DropdownComponent;
class FrameComponent;
class GuiComponent;
class GuiEntity;
class LabelComponent;
class ListBoxComponent;
class ScrollbarComponent;
class TextFieldComponent;
class TreeListComponent;
class VerticalList;

class GUI_API GuiTester : public GuiEntity
{
	DECLARE_CLASS(GuiTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	UnitTester::EUnitTestFlags TestFlags;

protected:
	/* List of Gui Components that are outside of staging area (cleans up at destroy). */
	std::vector<DPointer<GuiComponent>> BaseComponents;

	/* List of Gui Components that are inside staging area (cleans up at stage change). */
	std::vector<DPointer<GuiComponent>> StageComponents;

	/* Component that makes up the stage's frame. */
	DPointer<FrameComponent> StageFrame;

	/* Buttons that make up the button bar.  This list may be used for testing lists (such as dropdown components). */
	std::vector<DPointer<ButtonComponent>> ButtonBar;

	/* Maximum number of buttons on the ButtonBar is displayed at any given time. */
	Int MaxButtonsPerPage;

	/* Which section of the ButtonBar is the user currently viewing. */
	Int ButtonBarPageIdx;

	DPointer<ButtonComponent> NextButtonBarPage;
	DPointer<ButtonComponent> PrevButtonBarPage;

	/* For the button test, these are the two buttons that'll be toggling each other. */
	DPointer<ButtonComponent> ToggleButtons[2];
	Int ButtonTestCounter;

	/* For the Checkbox test, this is to toggle visibility and enabled flags on TargetTestCheckbox. */
	DPointer<CheckboxComponent> EnableToggleCheckbox;
	DPointer<CheckboxComponent> VisibilityToggleCheckbox;
	DPointer<CheckboxComponent> TargetTestCheckbox;

	/* For the Scrollbar test, this is the Scrollbar that'll be viewing various GuiComponents based on the button the user pressed. */
	DPointer<ScrollbarComponent> ScrollbarTester;
	Float ScrollbarTesterAnchorTop;
	Float ScrollbarTesterAnchorRight;
	Float ScrollbarTesterAnchorBottom;
	Float ScrollbarTesterAnchorLeft;
	DPointer<GuiEntity> ScrollbarTesterViewedObject;
	DPointer<ButtonComponent> ScrollbarTesterLabelTest;
	DPointer<ButtonComponent> ScrollbarTesterButtonTest;
	DPointer<ButtonComponent> ScrollbarTesterSizeChangeTest;
	DPointer<ButtonComponent> ScrollbarTesterDifferentFrameSizeTest;
	DPointer<ButtonComponent> ScrollbarTesterOuterSizeChangeTest;
	DPointer<ButtonComponent> ScrollbarTesterNestedScrollbars;
	DPointer<ButtonComponent> ScrollbarTesterLaunchFramedScrollbarTest;
	DPointer<ScrollbarComponent> ScrollbarTesterFramedScrollbar;
	DPointer<ButtonComponent> ScrollbarTesterComplexUi;
	DPointer<ButtonComponent> ScrollbarTesterDecreaseSizeButton;
	DPointer<ButtonComponent> ScrollbarTesterIncreaseSizeButton;
	DPointer<FrameComponent> ScrollbarTesterSizeChangeFrame;
	std::vector<GuiComponent*> ScrollbarTestObjects; //List of GUI Components to remove when changing scrollbar tests.

	/* For the TextField tests. */
	std::vector<TextFieldComponent*> TextFields;

	/* For the ListBox test, this text field is to apply a filter on all ListBoxComponents. */
	TextFieldComponent* ListBoxFilterField;

	/* For the ListBox test, this is a list of strings the List Box components will be referencing. */
	std::vector<DString> ListBoxStrings;

	/* For the ListBox test, this List Box has a limit in how many items can be selected. */
	DPointer<ListBoxComponent> MaxNumSelectableListBox;

	/* For the dropdown test, this is a list of numbers the numeric dropdown menus will be referencing. */
	std::vector<Int> DropdownNumbers;

	/* For the dropdown test, this is the dropdown component that reports the button bar content. */
	DPointer<DropdownComponent> ButtonBarDropdown;

	/* For the VerticalList test, this is the test VerticalList that will be modified during the test. */
	VerticalList* VerticalListMainList;

	/* for the VerticalList test, these are the list of checkboxes and buttons that were added to VerticalList. */
	std::vector<CheckboxComponent*> VerticalListCheckboxes;
	std::vector<ButtonComponent*> VerticalListButtons;

	/* List of GuiEntities instantiated for the GuiEntity test. */
	std::vector<DPointer<GuiEntity>> GuiEntities;

	/* Index of the GuiEntities list that is currently focused. */
	UINT_TYPE FocusedGuiEntityIdx;

	/* For the GuiEntity test, this is the dropdown that selects which GuiEntity should be focused. */
	DPointer<DropdownComponent> SetFocusDropdown;

	/* For the HoverComponent test, this is the label component that will be adjusting its text based on if the user is hovering over it or not. */
	DPointer<LabelComponent> HoverLabel;

	/* For the HoverComponent test, this is the button component that'll be adjusting its text based on if the user is hovering over it or not. */
	DPointer<ButtonComponent> HoverButton;

	/* For the TreeList test, this is the class browser tree list that'll notify the user which class they've selected. */
	DPointer<TreeListComponent> TreeListClassBrowser;

	/* Window handle that'll be rendering objects from this test. */
	DPointer<Window> TestWindowHandle;

	/* Draw Layer that'll render UI elements to the TestWindowHandle. */
	DPointer<GuiDrawLayer> TestDrawLayer;

	/* Input-related objects associated with the TestWindowHandle. */
	DPointer<InputBroadcaster> TestWindowInput;
	DPointer<MousePointer> TestWindowMouse;

private:
	/* Counter for the button panels (external from stage). */
	Int PanelButtonCounter;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void BeginUnitTest ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeStageFrame ();

	/**
	 * Creates and initializes all UI components that make up the components external to the testing stage.
	 */
	virtual void CreateBaseButtons ();

	virtual void ClearStageComponents ();

	/**
	 * Resets all Entities associated with the Scrollbar's test.
	 */
	virtual void ResetScrollbarTest ();

	/**
	 * Creates and initializes a button to the options panel.
	 */
	virtual ButtonComponent* AddPanelButton ();

	/**
	 * Sets the relevant buttons on the button bar to be visible based on the current page idx.
	 */
	virtual void UpdateButtonBarPageIdx ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
	virtual void HandleWindowEvent (const sf::Event& newWindowEvent);

	virtual void HandleFrameButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleLabelButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleAutoSizeLabelButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleButtonButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleCheckboxButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleContextMenuButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTextFieldButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleListBoxButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleDropdownButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleVerticalListButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTooltipButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleGuiEntityButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleHoverButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTreeListButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleCloseClicked (ButtonComponent* uiComponent);
	virtual void HandleNextPageClicked (ButtonComponent* uiComponent);
	virtual void HandlePrevPageClicked (ButtonComponent* uiComponent);

	virtual void HandleButtonTestPressed (ButtonComponent* uiComponent);
	virtual void HandleButtonTestReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonTestToggleReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonTestIncrementCounter (ButtonComponent* uiComponent);
	virtual void HandleButtonTestDecrementCounter (ButtonComponent* uiComponent);

	virtual void HandleCheckboxToggle (CheckboxComponent* uiComponent);
	virtual void HandleToggleEnableCheckboxToggle (CheckboxComponent* uiComponent);
	virtual void HandleToggleVisibilityCheckboxToggle (CheckboxComponent* uiComponent);

	virtual void HandleCloseContextMenu (ButtonComponent* uiComponent);
	virtual void HandleDynamicListMenuCmd (BaseGuiDataElement* data);

	virtual void HandleScrollbarTesterLabelTestClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterButtonTestClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterTempButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterSizeChangeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHideWhenFull (CheckboxComponent* uiComponent);
	virtual void HandleScrollbarTesterDecreaseSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterIncreaseSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterSizeChangerTick (Float deltaSec);
	virtual void HandleScrollbarTesterDifferentFrameSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHalfHorizontalButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHalfVertialButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterOuterSizeChangeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterNestedScrollbarsClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleFoodItem (CheckboxComponent* uiComponent);
	virtual void HandleScrollbarTesterFramedScrollbarClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterFrameGripped (bool isGripped);
	virtual void HandleScrollbarTesterComplexUiClicked (ButtonComponent* uiComponent);

	virtual void HandleTextFieldLeftAlignButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTextFieldCenterAlignButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTextFieldRightAlignButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTextFieldTextChanged (TextFieldComponent* uiComponent);
	virtual bool HandleTextFieldLimitedInput (const DString& txt);
	virtual void HandleTextFieldSingleLineReturn (TextFieldComponent* textField);

	virtual bool HandleListBoxFilterEdit (const sf::Event& sfEvent);
	virtual void HandleBasicListBoxOptionSelected (Int selectedIdx);
	virtual void HandleBasicListBoxOptionDeselected (Int deselectedIdx);
	virtual void HandleReadOnlyBoxOptionSelected (Int selectedIdx);
	virtual void HandleReadOnlyBoxOptionDeselected (Int deselectedIdx);
	virtual void HandleMaxNumSelectedBoxOptionSelected (Int selectedIdx);
	virtual void HandleMaxNumSelectedBoxOptionDeselected (Int deselectedIdx);
	virtual void HandleMinNumSelectedBoxOptionSelected (Int selectedIdx);
	virtual void HandleMinNumSelectedBoxOptionDeselected (Int deselectedIdx);
	virtual void HandleAutoDeselectBoxOptionSelected (Int selectedIdx);
	virtual void HandleAutoDeselectBoxOptionDeselected (Int deselectedIdx);

	virtual void HandleBasicDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx);
	virtual void HandleFullBasicDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx);
	virtual void HandleButtonBarDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx);
	virtual void HandleSearchableDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx);

	virtual void HandleVertListToggleCheckboxVisibility (CheckboxComponent* uiComponent);
	virtual void HandleVertListToggleButtonVisibility (CheckboxComponent* uiComponent);
	virtual void HandleVertListAddButton (ButtonComponent* uiComponent);
	virtual void HandleVertListRemoveButton (ButtonComponent* uiComponent);
	virtual void HandleVertListAddCheckbox (ButtonComponent* uiComponent);
	virtual void HandleVertListRemoveCheckbox (ButtonComponent* uiComponent);

	virtual void HandleFocusChange (DropdownComponent* comp, Int newOptionIdx);

	virtual void HandleHoverFrameRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverFrameRolledOut (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverLabelRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverLabelRolledOut (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverButtonRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverButtonRolledOut (MousePointer* mouse, Entity* hoverOwner);

	virtual void HandleTreeListClassSelected (Int newOptionIdx);
};
SD_END

#endif //debug mode