/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicListMenu.h
  A GuiEntity responsible for listing a series of options that can also expand
  into more options. The list of options are dynamically populated.

  This is primarily used for context menus and dropdown menus.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

SD_BEGIN
class BaseGuiDataElement;
class ContextMenuComponent;
class FrameComponent;

class GUI_API DynamicListMenu : public GuiEntity
{
	DECLARE_CLASS(DynamicListMenu)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct GUI_API SInitMenuOption
	{
		/**
		 * Determines the full path and display text for this option. Every '>' character is interpreted as a submenu. The last chunk after '>' is the displayed text.
		 * Use an escape character (eg: "\\>") if there's a need to display the '>' on the context menu, itself, instead of moving the text to a sub menu.
		 * If there are multiple commands with a shared path, the commands will consolidate to a common root menu.
		 *
		 * For example if there are commands such as:
		 * Root Menu>Mid Menu>Command 1
		 * Root Menu>Mid Menu>Command 2
		 * Command 3
		 * Root Menu>Command 4
		 * Root Menu>Mid Menu>Command 5
		 * Then the DynamicListMenu will present the following:
		 * + RootMenu
		 * L---+ Mid Menu
		 *     |---Command 1
		 *     |---Command 2
		 *     L---Command 5
		 *     Command 4
		 * Command 3
		 *
		 * Any sections equal to the DynamicListMenu's DIVIDER DString will be replaced with a separator.
		 */
		DString FullCommandPath;

		/* Optional gui data element to associate this command. The DynamicListMenu will take ownership over this object reference. It will automatically delete this
		data object on destruction. */
		mutable BaseGuiDataElement* Data;

		/* Delegate to invoke whenever the user selected this option. */
		SDFunction<void, BaseGuiDataElement* /*data*/> OnCommand;

		/* If not None, then this command will execute if the user presses this key while this option is visible. */
		sf::Keyboard::Key Keybind;

		/* If a keybind is set, then the keybind can only be executed if these keyboard states are also held down. */
		EKeyboardState RequiredState;

		SInitMenuOption (const DString& pathToDivider); //This constructor assumes the last element ends with DynamicListMenu::DIVIDER
		SInitMenuOption (const DString& inFullCommandPath, const std::function<void(BaseGuiDataElement*)>& inOnCommand);
		SInitMenuOption (const DString& inFullCommandPath, sf::Keyboard::Key inKeybind, EKeyboardState inRequiredState, const std::function<void(BaseGuiDataElement*)>& inOnCommand);
	};

protected:
	struct SMenuOption
	{
		DString DisplayText;

		/* Text to display on the right side of this option. This is typically the expand menu character ">" or the keybind shortcut (eg: "(Ctrl + V)" for pasting). */
		DString DisplaySuffix;

		/* If true, then this option is merely a divider. */
		bool bIsDivider;

		/* Optional GuiDataElement to be associated with this option. The OnCommand callback will return this instance. */
		BaseGuiDataElement* Data;

		/* Delegate to invoke whenever the user selected this option. This is only applicable if this menu option does not have any suboptions. */
		SDFunction<void, BaseGuiDataElement* /*data*/> OnCommand;

		/* If not INDEX_NONE, then this is the index to the AllOptions vector of the parent option that expands to this option. */
		size_t ParentOption;

		/* If not empty, then this menu will expand to these options whenever the user hovers over this option. These are indices to the AllOptions vector.
		If the index is INDEX_NONE, then this sub option is simply a divider. */
		std::vector<size_t> SubOptions;

		/* If not None, then this command will execute if the user presses this key while this option is visible. */
		sf::Keyboard::Key Keybind;

		/* If a keybind is set, then the keybind can only be executed if these keyboard states are also held down. */
		EKeyboardState RequiredState;

		SMenuOption ();
	};

	struct SMenuBlock
	{
		/* Frame Component that is the root GuiComponent for this menu block. */
		FrameComponent* Frame;

		/* Transform component for the color highlight. */
		PlanarTransformComponent* ColorTransform;

		/* Index to the menu options indicating which options are available for this menu block. If the index is INDEX_NONE, then this option is simply a divider. */
		std::vector<size_t> Options;

		/* Index of the AllOptions vector that is currently being hovered. Becomes negative if not hovering over anything. */
		Int HoverIdx;

		/* If not INT_INDEX_NONE, then this is the index to the Options vector that is currently expanded. */
		Int ExpandedOption;

		SMenuBlock ();
		~SMenuBlock ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Unique string used to distinguish a divider compared to a command. */
	static const DString DIVIDER;

protected:
	/* The number of seconds that must elapse before the hovered item expands its menu. */
	Float HoverActivationDelay;

	/* Timestamp when a command option is hovered (in seconds). */
	Float HoveredCommandTimestamp;

	/* The number of seconds that must elapse before the unhovered menu collapses. This doesn't apply to the root menu. */
	Float CollapseMenuDelay;

	/* The timestamp when the top menu is no longer hovered (in seconds). */
	Float UnhoveredMenuTimestamp;

	/* When the user selected an option, this is the number of seconds it takes to fade out this menu. */
	Float FadeOutDuration;

	/* Timestamp when the fade out process began (in seconds). */
	Float FadeOutTimestamp;

	/* Determines the character size for each option. */
	Float OptionSize;

	/* Determines the minimum number of pixels between the option's display text and the option's suffix. */
	Float MinSuffixDistance;

	/* Determines the minimum horizontal size for each menu (in pixels). */
	Float MinMenuWidth;

	/* Determines the vertical space between each option. */
	Float OptionSpacing;

	/* Determines how close the right label (suffix text that displays keybinds) is to the frame's right border (in normalized coordinates). 0 implies the suffix it's right against the border.
	0.1 implies 10% margin between the label and frame. */
	Float SuffixPadding;

	/* The height multiplier to use for divider options. This multiplier is relative to (OptionSize + OptionSpacing). */
	Float DividerHeightMultiplier;

	/* Determines how tall the divider is visible. This is different from DividerHeightMultiplier where the height multiplier will determine how much space a divider takes up.
	This variable determines how much is visible. This value is relative to the DividerHeightMultiplier. */
	Float DividerVisibleHeight;

	/* Color to use for the dividers. */
	Color DividerColor;

	/* Color to use for the menu background. */
	Color BackgroundColor;

	/* Color to use for the highlighter. */
	Color HighlightColor;

	/* Color to use for the display text. */
	Color PrimaryTextColor;

	/* Color to use for the suffix text (eg: the text that displays the keybind). */
	Color SecondaryTextColor;

	/* List of all options including submenus. This vector is meant to be persistent regardless which menus are visible. */
	std::vector<SMenuOption> AllOptions;

	/* List of all menus. Element 0 is root level; any elements after index 0 are expanded sub menus, sorted in order by root-most to outer-most menu.
	This is a pointer instead of an object since there's an issue where it'll cause a heap corruption crash if this is a SMenuBlock instead of SMenuBlock*.
	Steps to repro, change this vector back to an object, run the ContextMenuTest, execute a command in a sub menu multiple times, the heap crash would eventually occur (typically within 90 seconds). */
	std::vector<SMenuBlock*> VisibleMenus;

private:
	/* When popping visible menus due to unhovered menus, the UnhoveredMenuTime will stop if the menu on top is equal to this menu (in index to VisibleMenus).
	Becomes INDEX_NONE if not hovering over anything. */
	size_t LatestHoveredMenu;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool HandleConsumableInput (const sf::Event& evnt) override;
	virtual bool HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Populates the command list, and launches the root menu from the mouse position.
	 */
	virtual void InitializeList (ContextMenuComponent* contextComp, MousePointer* mouse, const std::vector<SInitMenuOption>& inOptions);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetHoverActivationDelay (Float newHoverActivationDelay);
	virtual void SetCollapseMenuDelay (Float newCollapseMenuDelay);
	virtual void SetFadeOutDuration (Float newFadeOutDuration);

	//These mutators must be set before InitializeList is called.
	virtual void SetOptionSize (Float newOptionSize);
	virtual void SetMinSuffixDistance (Float newMinSuffixDistance);
	virtual void SetMinMenuWidth (Float newMinMenuWidth);
	virtual void SetOptionSpacing (Float newOptionSpacing);
	virtual void SetSuffixPadding (Float newSuffixPadding);
	virtual void SetDividerHeightMultiplier (Float newDividerHeightMultiplier);
	virtual void SetDividerVisibleHeight (Float newDividerVisibleHeight);
	virtual void SetDividerColor (Color newDividerColor);
	virtual void SetBackgroundColor (Color newBackgroundColor);
	virtual void SetHighlightColor (Color newHighlightColor);
	virtual void SetPrimaryTextColor (Color newPrimaryTextColor);
	virtual void SetSecondaryTextColor (Color newSecondaryTextColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetHoverActivationDelay () const
	{
		return HoverActivationDelay;
	}

	inline Float GetCollapseMenuDelay () const
	{
		return CollapseMenuDelay;
	}

	inline Float GetFadeOutDuration () const
	{
		return FadeOutDuration;
	}

	inline Float GetOptionSize () const
	{
		return OptionSize;
	}

	inline Float GetMinSuffixDistance () const
	{
		return MinSuffixDistance;
	}

	inline Float GetMinMenuWidth () const
	{
		return MinMenuWidth;
	}

	inline Float GetOptionSpacing () const
	{
		return OptionSpacing;
	}

	inline Float GetSuffixPadding () const
	{
		return SuffixPadding;
	}

	inline Float GetDividerHeightMultiplier () const
	{
		return DividerHeightMultiplier;
	}

	inline Float GetDividerVisibleHeight () const
	{
		return DividerVisibleHeight;
	}

	inline Color GetDividerColor () const
	{
		return DividerColor;
	}

	inline Color GetBackgroundColor () const
	{
		return BackgroundColor;
	}

	inline Color GetHighlightColor () const
	{
		return HighlightColor;
	}

	inline Color GetPrimaryTextColor () const
	{
		return PrimaryTextColor;
	}

	inline Color GetSecondaryTextColor () const
	{
		return SecondaryTextColor;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns the number of pixels between the top of the menu block's frame and the top of its first option.
	 */
	inline Float GetTopMargin () const
	{
		return (OptionSpacing * 0.5f);
	}

	/**
	 * Calculates which menu option this mouse coordinates are hovering over. This function assumes mousePos is hovering over the given block's FrameComponent.
	 * Returns the index to the AllOptions vector.
	 */
	virtual Int CalcHoveredOption (const Vector2& mousePos, const SMenuBlock& block) const;

	/**
	 * Updates the given menu block to highlight the given index number.
	 * Returns true if this block is now hovering over an option that expands to a sub menu.
	 */
	virtual bool HighlightIdxInBlock (SMenuBlock& outBlock, Int newHoverIdx);

	/**
	 * Sets the color's visibility if it's hovering over an valid option. Becomes invisible when hovering over a divider.
	 * This function also updates the color's transformation to hover over the block's option based on its hover idx.
	 */
	virtual void UpdateHoverTransform (const SMenuBlock& block);

	/**
	 * Creates the GuiComponents needed to render the associated commands.
	 * The results are stored in the end of the VisibleMenus vector.
	 * The secondaryLocation is used if it cannot fit the new menu at the primaryLocation.
	 * This function assumes the primaryLocation is to the right of the secondaryLocation. If the secondaryLocation is used, it'll render the new menu relative to its right border instead of left border.
	 */
	virtual void RevealMenuBlock (const std::vector<size_t>& commandIndices, const Vector2& primaryLocation, const Vector2& secondaryLocation);
	virtual void RevealMenuBlock (const std::vector<size_t>& commandIndices, ContextMenuComponent* contextComp, const Vector2& revealLocation);

	/**
	 * Initializes the menu block with the exception of setting its frame component's position.
	 * Returns a reference to the FrameComponent that still needs to have its position set.
	 **/
	virtual FrameComponent* InitializeNewMenuBlock (const std::vector<size_t>& commandIndices);

	/**
	 * Removes the last menu block from the visible menus vector.
	 */
	virtual void RemoveLastVisibleMenu ();

	/**
	 * Async function that begins the fade out process. When the process finishes fading, it'll destroy this menu.
	 */
	virtual void BeginFadeOut ();

	/**
	 * Iterates through all options to clean up resources associated with them.
	 */
	virtual void ClearAllOptions ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HoverMonitorTick (Float deltaSec);
};
SD_END