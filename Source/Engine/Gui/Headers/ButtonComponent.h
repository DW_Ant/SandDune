/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ButtonComponent.h
  A component that broadcasts a delegated function whenever clicked.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "FocusInterface.h"

SD_BEGIN
class LabelComponent;
class FrameComponent;
class ButtonStateComponent;

class GUI_API ButtonComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(ButtonComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to the component that's responsible for displaying text over the button. */
	DPointer<LabelComponent> CaptionComponent;

	/* Reference to the component that'll determine how the appearance of this button changes over various states. */
	DPointer<ButtonStateComponent> StateComponent;

	/* GuiComponent that determines the sprite background and its borders. */
	DPointer<FrameComponent> Background;

	/* True if the button can be interacted. */
	bool bEnabled;

	/* Becomes true whenever the mouse pointer clicked on it but haven't released yet. */
	bool bPressedDown;

	/* Becomes true whenever the mouse pointer is hovering over this button. */
	bool bHovered;

	/* Function callback to invoke when this button was pressed */
	SDFunction<void, ButtonComponent* /*uiComponent*/> OnButtonPressed;

	/* Function callback to invoke when this button was released */
	SDFunction<void, ButtonComponent* /*uiComponent*/> OnButtonReleased;

	/* Separate callbacks for right mouse click. */
	SDFunction<void, ButtonComponent* /*uiComponent*/> OnButtonPressedRightClick;
	SDFunction<void, ButtonComponent* /*uiComponent*/> OnButtonReleasedRightClick;

private:
	/* Becomes true when this button detected the mouse button release. This is used primarily to let go of the button press
	regardless of the mouse position, but only consume the event if the mouse cursor is over the button. */
	bool bPendingReleaseButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	//FocusInterface
	virtual bool CanBeFocused () const override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Simulates pushing down on the button.
	 */
	virtual void SetButtonDown (bool invokeDelegate, bool isLeftClick);

	/**
	 * Simulates releasing the button.
	 */
	virtual void SetButtonUp (bool invokeDelegate, bool isLeftClick);

	virtual void SetButtonPressedHandler (SDFunction<void, ButtonComponent* /*uiComponent*/> newHandler);
	virtual void SetButtonReleasedHandler (SDFunction<void, ButtonComponent* /*uiComponent*/> newHandler);
	virtual void SetButtonPressedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler);
	virtual void SetButtonReleasedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler);
	virtual void SetCaptionText (DString newText);
	virtual void SetBackground (FrameComponent* newBackground);
	virtual void SetEnabled (bool bNewEnabled);

	/**
	 * Removes old ButtonStateComponent in place of a new ButtonStateComponent.  Links the newly created
	 * state component with this component.  Returns a pointer of the newly created ButtonStateComponent.  A nullptr will be returned
	 * if the function was unable to create or set the ButtonStateComponent.  If it fails, it will not destroy the old state component.
	 */
	virtual ButtonStateComponent* ReplaceStateComponent (const DClass* newButtonStateClass);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetEnabled () const;
	virtual bool GetHovered () const;
	virtual bool GetPressedDown () const;

	inline const SDFunction<void, ButtonComponent*>& ReadOnButtonPressed () const
	{
		return OnButtonPressed;
	}

	inline const SDFunction<void, ButtonComponent*>& ReadOnButtonReleased () const
	{
		return OnButtonReleased;
	}

	inline const SDFunction<void, ButtonComponent*>& ReadOnButtonPressedRightClick () const
	{
		return OnButtonPressedRightClick;
	}

	inline const SDFunction<void, ButtonComponent*>& ReadOnButtonReleasedRightClick () const
	{
		return OnButtonReleasedRightClick;
	}

	virtual DString GetCaptionText () const;
	inline LabelComponent* GetCaptionComponent () const
	{
		return CaptionComponent.Get();
	}

	inline ButtonStateComponent* GetStateComponent () const
	{
		return StateComponent.Get();
	}

	inline FrameComponent* GetBackground () const
	{
		return Background.Get();
	}

	//Returns the Background's RenderComponent
	RenderComponent* GetRenderComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeBackground ();

	/**
	 * Creates and sets up the CaptionComponent (text) to be associated with this button.
	 */
	virtual void InitializeCaptionComponent ();

	inline bool HasRightClickHandler () const
	{
		return (OnButtonPressedRightClick.IsBounded() || OnButtonReleasedRightClick.IsBounded());
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSpriteTextureChanged ();
};
SD_END