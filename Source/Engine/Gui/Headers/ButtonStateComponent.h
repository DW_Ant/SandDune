/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ButtonStateComponent.h
  A component that contains methods that'll transition the button
  component to various states such as hovered, down, and disabled.

  Note:  The reason why this is seperated from the ButtonComponent is
  to avoid code duplication whenever there's a need to subclass the
  ButtonComponent without having to override transition state functionality.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class ButtonComponent;

class GUI_API ButtonStateComponent : public EntityComponent, public CopiableObjectInterface
{
	DECLARE_CLASS(ButtonStateComponent)


	/*
	=====================
	 Properties
	=====================
	*/

protected:
	/* Reference to the owning button component. */
	DPointer<ButtonComponent> OwningButton;


	/*
	=====================
	 Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;


	/*
	=====================
	 Methods
	=====================
	*/

public:
	/**
	 * Various hooks to change the button appearance based on its state.
	 * ie:  Use a single sprite with multi-subdivisions, swapping textures altogether, or changing frame component's borders.
	 */
	virtual void SetDefaultAppearance ();
	virtual void SetDisableAppearance ();
	virtual void SetHoverAppearance ();
	virtual void SetDownAppearance ();

	/**
	 * Figures out what state the owning component is and adjusts its appearance accordingly.
	 */
	virtual void RefreshState ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual ButtonComponent* GetOwningButton () const;
};
SD_END