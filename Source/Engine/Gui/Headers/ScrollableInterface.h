/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollableInterface.h
  A class that implements this interface gain the capability to be viewed in a scrollbar.

  This interface declares the functions a ScrollbarComponent may invoke.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class ScrollbarComponent;

class GUI_API ScrollableInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* When executed, the scrollbar viewing this interface will move to the given position.
	If deltaOffset is true, then the newScrollPosition is relative to the current camera position instead
	of being absolute coordinates.
	The ScrollbarComponent handles this delegate, the object inheriting this interface invokes it.*/
	SDFunction<void, const Vector2& /*newScrollPosition*/, bool /*bDeltaOffset*/> OnScrollToPosition;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * This object is now rendered inside the given Scrollbar.  Nothing is really needed to be executed
	 * in this function unless there's a need for this object to hold a reference to the scrollbar
	 * (such as there's a need to move the camera to a spot after a particular event).
	 */
	virtual void InitializeScrollableInterface (ScrollbarComponent* scrollbarOwner) {}

	/**
	 * Returns a reference to the ScrollbarComponent that's managing this object instance.
	 */
	virtual ScrollbarComponent* GetScrollbarOwner () const = 0;

	/**
	 * Instructs this object to register itself to a draw layer within the specified RenderTexture.
	 * If the texture does not have the correct DrawLayer, then it's expected that this object creates
	 * the appropriate DrawLayer.
	 */
	virtual void RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner) = 0;

	/**
	 * The ScrollbarComponent is about to view a different object.  This object should unregister
	 * itself from the draw layer.
	 */
	virtual void UnregisterFromDrawLayer (RenderTexture* drawLayerOwner) = 0;

	/**
	 * Used to determine the amount to scroll.  This is checked every frame.  Computed size values should be
	 * cached to a variable for performance reasons.
	 */
	virtual void GetTotalSize (Vector2& outSize) const = 0;

	/**
	 * Invoked whenever the scrollbar's frame size changed to the new dimensions (in absolute pixels).
	 */
	virtual void HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize) {}

	/**
	 * Various input methods passed in from the owning Scrollbar component.
	 * If the consumable input events return true, then the input event will not be relayed further down the input list.
	 */
	virtual void ExecuteScrollableInterfaceInput (const sf::Event& evnt) {}
	virtual void ExecuteScrollableInterfaceText (const sf::Event& evnt) {}
	virtual void ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) {}
	virtual void ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) {}
	virtual void ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) {}
	virtual bool ExecuteScrollableInterfaceConsumableInput (const sf::Event& evnt) {return false;}
	virtual bool ExecuteScrollableInterfaceConsumableText (const sf::Event& evnt) {return false;}
	virtual bool ExecuteScrollableInterfaceConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) {return false;}
	virtual bool ExecuteScrollableInterfaceConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) {return false;}

	/**
	 * Invoked whenever the Scrollbar is no longer viewing this object. (eg: SetViewedObject was called on something else).
	 * This function implies that this Entity should continue to persist even if the Scrollbar is no longer viewing and referencing it.
	 */
	virtual void ClearScrollbarOwner (ScrollbarComponent* oldScrollbarOwner) {}

	/**
	 * The scrollbar no longer needs this object, and the memory allocated for this object should be restored
	 * (Examples:  Invoke Object::Destroy() or call delete this)
	 * The scrollbar will no longer reference this object when this function finishes executing.
	 */
	virtual void RemoveScrollableObject () = 0;
};
SD_END