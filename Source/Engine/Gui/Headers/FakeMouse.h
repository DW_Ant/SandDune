/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FakeMouse.h
  A MousePointer hack that moves a mouse pointer within an inner component such as inside a scrollbar coordinate space.

  Objects within the inner coordinate space will have access to this MousePointer instance, but this
  MousePointer instance doesn't directly subscribed to the Gui Input Broadcaster.

  Instead the owning Entity will relay events from the original mouse event, translate
  it to inner coordinates (instead of window coordinates), and send it off to the viewed object.

  Although this solution is a bit hacky, it's worth the cost (via hack and code duplication) compared to the alternative.
  Without this MousePointer, objects in the inner space would received displaced mouse coordinates.
  For example if a frame component received a mouse move event, the event would be in window space
  instead of scrollbar space. This may cause the mouse coordinates to be displaced compared to where the object is placed.

  The scrollbar can displace the event based on its coordinates when sending off the event, but
  there are still systems that require mouse coordinates without going through the event. For example,
  the MousePointer has a callback for determining when it should override the mouse icon. In order for
  this FrameComponent to effectively convert the coordinate space, it would have to be:
  1. Aware that it's inside a Scrollbar.
  2. Find the Render Texture it's being rendered in.
  3. Displace the mouse coordinates based on where this texture is drawn.
  4. Find the camera associated with the scrollbar, and displace it based on the camera position.
  5. Get the scrollbar's dimensions to determine if the mouse pointer is hovering over the scrollbar.

  Having ViewedObjects and its inner components be aware of the scrollbar, the camera, and the RenderTexture
  is too intrusive, expensive, and too much "spaghetti code".
  
  An alternative to that is this ScrollbarMouse. This MousePointer only exists in Scrollbar space. The scrollbar
  communicates its input events to its ViewedObject through this Entity. This Entity will automatically
  handle the conversions and have awareness of the scrollbar. That way the ViewedObjects can assume the mouse
  pointer and its events are already in the correct coordinate space. They can treat the ScrollbarMouse and the
  original MousePointer the same way.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class FakeMouseOwnerInterface;

class GUI_API FakeMouse : public MousePointer
{
	DECLARE_CLASS(FakeMouse)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The mouse coordinates whenever the original mouse is outside the Scrollbar's borders. This is a quick way to simulate a mouse going out of bounds without
	having the sub components check for boundaries, itself. */
	static const Vector2 OUT_OF_BOUNDS;

protected:
	/* The object instance that owns this FakeMouse instance. */
	FakeMouseOwnerInterface* MouseOwner;

	/* The mouse pointer that instigated the original input events. */
	DPointer<MousePointer> OriginalMouse;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual Vector2 ToOuterCoordinates () const override;
	virtual Vector2 ToWindowCoordinates () const override;
	virtual void PushPositionLimit (Rectangle newLimitRegion, SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> limitCallback) override;
	virtual bool RemovePositionLimit (SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback) override;
	virtual void PushMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction) override;
	virtual bool UpdateMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction) override;
	virtual bool IsIconOverrideCurrentlyViewed (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback) override;
	virtual void SetDefaultIcon (Texture* newIcon) override;
	virtual void SetMousePosition (const Vector2& newPos) override;
	virtual void SetMouseVisibility (bool bVisible) override;
	virtual void SetLocked (bool bNewLocked) override;
	virtual void SetOwningWindowHandle (Window* newOwningWindowHandle) override;
	virtual void EvaluateMouseIconOverrides () override;
	virtual void SetDoubleClickThreshold (Float newDoubleClickThreshold) override;
	virtual MousePointer* GetRootMouse () override;
	virtual Float GetDoubleClickThreshold () const override;
	virtual Window* GetOwningWindowHandle () const override;

protected:
	virtual void Destroyed () override;

	virtual void LoadMouseSettings () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMouseOwner (FakeMouseOwnerInterface* newMouseOwner);
	virtual void SetOriginalMouse (MousePointer* newOriginalMouse);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline MousePointer* GetOriginalMouse () const
	{
		return OriginalMouse.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt);
};
SD_END