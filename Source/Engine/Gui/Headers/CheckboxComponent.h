/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CheckboxComponent.h
  A component that can be rendered in two states (on or off).
  This component broadcasts delegates whenever it changes states.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "FocusInterface.h"

SD_BEGIN
class FrameComponent;
class LabelComponent;
class CheckboxStateComponent;

class GUI_API CheckboxComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(CheckboxComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Function callback to invoke when this checkbox's state changed */
	SDFunction<void, CheckboxComponent* /*uiComponent*/> OnChecked;

protected:
	/* The component that's responsible for displaying text next to the checkbox sprite. */
	DPointer<LabelComponent> CaptionComponent;

	/* Component that'll be rendering the checked and hollowed box.
	The sprite is expected to support 8 subdivisions:
	+=======================+===================+
	|Unchecked Default		|Checked Default	|
	+=======================+===================+
	|Unchecked Hovered		|Checked Hovered	|
	+=======================+===================+
	|Unchecked Pressed		|Checked Pressed	|
	+=======================+===================+
	|Unchecked Disabled		|Checked Disabled	|
	+=======================+===================+
	*/
	DPointer<FrameComponent> CheckboxSprite;

	/* True if the checkbox can be interacted. */
	bool bEnabled;

	/* Becomes true whenever the mouse pointer clicked on the checkbox but haven't released yet. */
	bool bPressedDown;

	/* Becomes true whenever the mouse pointer is hovering over the checkbox. */
	bool bHovered;

	/* Becomes true if the checkbox is on. */
	bool bChecked;

	/* If true, then the checkbox sprite will appear on right side of caption text. */
	bool bCheckboxRightSide;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	//FocusInterface
	virtual bool CanBeFocused () const override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	//PlanarTransform
	virtual void PostAbsTransformUpdate () override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetEnabled (bool bNewEnabled);
	virtual void SetChecked (bool bNewChecked);
	virtual void SetCheckboxRightSide (bool bNewCheckboxRightSide);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline LabelComponent* GetCaptionComponent () const
	{
		return CaptionComponent.Get();
	}

	inline FrameComponent* GetCheckboxSprite () const
	{
		return CheckboxSprite.Get();
	}

	virtual bool GetEnabled () const;
	virtual bool GetHovered () const;
	virtual bool GetChecked () const;
	virtual bool GetCheckboxRightSide () const;

	inline bool IsChecked () const
	{
		return GetChecked();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeCaptionComponent ();
	virtual void InitializeCheckboxSprite ();

	/**
	 * Positions the caption component and the sprite in proper locations based on the bCheckboxRightSide flag.
	 */
	virtual void RefreshComponentPositions ();

	/**
	 * Computes which subdivision the Checkbox Sprite component should render, then
	 * applies that subdivision.
	 */
	virtual void RefreshCheckboxSprite ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSpriteTextureChanged ();
};
SD_END