/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContextMenuComponent.h
  A GuiComponent that listens for click events to reveal a menu in the foreground.

  This component will contain the meta data needed for the menu, but this will not
  implement the popup, itself. This component is intended to be attached to other 
  GuiComponents to determine the behavior when the user clicked on those components.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class GuiEntity;

class GUI_API ContextMenuComponent : public GuiComponent
{
	DECLARE_CLASS(ContextMenuComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ERevealDirection
	{
		RD_BottomLeft = 0x0,

		RD_RightBit = 0x01,
		RD_BottomRight = RD_RightBit,

		RD_TopBit = 0x02,
		RD_TopLeft = RD_TopBit,
		
		RD_TopRight = RD_RightBit | RD_TopBit,
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate used to initialize the context menu. The MenuOwner is an instance of the ContextClass that needs to be initialized.
	Returns true if the menu is successfully initialized. The context menu instance would be destroyed if this delegate fails to intiailize it. */
	SDFunction<bool, GuiEntity* /*menu*/, ContextMenuComponent* /*contextComp*/, MousePointer* /*mouse*/ > OnInitializeMenu;

protected:
	/* GuiEntity class used to instantiate the context menu. If this is nullptr, then it'll use the GuiTheme's context menu class. */
	const DClass* ContextClass;

	/* Determines the direction where the context menu expands relative to the mouse position. If the menu cannot fit in that direction, it'll flip over to the
	other direction. This enumerator determines the direction priority. */
	ERevealDirection RevealDirection;

	/* The specific mouse button to listen for when detecting if a context menu should be revealed or not. */
	sf::Mouse::Button RevealMenuButton;

private:
	/* Reference to the revealed menu this component instantiated. This is used to automatically remove the revealed menu when this component is destroyed. */
	DPointer<GuiEntity> RevealedMenu;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Calculates the new draw location based on this component's reveal direction and the dimensions of the given GuiEntity.
	 * Returns the vector that indicates the top left corner where the GuiEntity should reside. Although the reveal direction will take priority, this method may return
	 * a different reveal direction if there aren't enough space for the GuiEntity when using the primary reveal direction.
	 * @param containerSize The size of the stage where the GuiEntity can move around in. This is typically the absolute size of the ContextMenuContainer.
	 */
	virtual Vector2 CalcRevealLocation (const Vector2& contextMenuSize, const Vector2& revealSpot, const Vector2& containerSize) const;

	/**
	 * Returns true if the context container is currently showing this component's data.
	 */
	virtual bool IsRevealed () const;

	/**
	 * Notifies the ContextGuiEntity to initialize and reveal a context menu from this component.
	 */
	virtual void RevealContextMenu (MousePointer* mouse);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetContextClass (const DClass* newContextClass);
	virtual void SetRevealDirection (ERevealDirection newRevealDirection);
	virtual void SetRevealMenuButton (sf::Mouse::Button newRevealMenuButton);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const DClass* GetContextClass () const
	{
		return ContextClass;
	}

	inline ERevealDirection GetRevealDirection () const
	{
		return RevealDirection;
	}

	inline sf::Mouse::Button GetRevealMenuButton () const
	{
		return RevealMenuButton;
	}
};

DEFINE_ENUM_FUNCTIONS(ContextMenuComponent::ERevealDirection);
SD_END