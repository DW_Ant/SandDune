/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SingleSpriteButtonState.h
  A Button State Component that uses segments of a single texture to
  display various states of the button.

  The button's sprite component should be divided into 1x4 sections.
  Top section is the button's default state.
  Second from top is the button's disabled state.
  Third from top is the button's hover state.
  Bottom section is the button's down state.
=====================================================================
*/

#pragma once

#include "ButtonStateComponent.h"

SD_BEGIN
class GUI_API SingleSpriteButtonState : public ButtonStateComponent
{
	DECLARE_CLASS(SingleSpriteButtonState)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* cpy) override;
	virtual void SetDefaultAppearance () override;
	virtual void SetDisableAppearance () override;
	virtual void SetHoverAppearance () override;
	virtual void SetDownAppearance () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns the owning button component's background as a sprite component.
	 */
	virtual SpriteComponent* GetSpriteComp () const;
};
SD_END