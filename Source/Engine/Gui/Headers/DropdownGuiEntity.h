/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DropdownGuiEntity.h
  Gui Entity that is responsible for rendering expanded dropdown components.

  The purpose of this Entity is to allow the dropdown component options to render
  above other Entities regardless if it's rendered within a scrollbar or not.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

SD_BEGIN
class DropdownComponent;
class GuiComponent;

class GUI_API DropdownGuiEntity : public GuiEntity
{
	DECLARE_CLASS(DropdownGuiEntity)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The Dropdown Component this instance is currently revealing. Becomes nullptr when it's not showing anything. */
	DPointer<DropdownComponent> ExpandedDropdown;

	/* The GuiComponent the ExpandComp use to be attached to on the ExpandedDropdown. */
	DPointer<GuiComponent> OriginalOwner;

	/* The GuiComponent within ExpandedDropdown that moved from OriginalOwner to TransformOwner. This will move back to the OriginalOwner when collapsing the Dropdown. */
	DPointer<GuiComponent> ExpandComp;

	/* The GuiComponent that'll be the temporary component owner for the expand menu. This component acts like a transformation that merely translates around to reflect the DropdownComponent's transform.
	This is a GuiComponent instead of a PlanarTransformComponent since a GuiComponent can relay input events to the ExpandComp. */
	GuiComponent* TransformOwner;

private:
	/* List of Transform Entities this Entity subscribed to its transform changed delegate. */
	mutable std::vector<Transformation*> RegisteredTransformDelegates;

	/* List of camera zoom delegates this Entity is currently subscribed to. */
	mutable std::vector<PlanarCamera*> RegisteredZoomDelegates;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Reveals the dropdown component options and displays it on this Entity.
	 */
	virtual void DisplayDropdown (DropdownComponent* dropdown);

	/**
	 * The specified dropdown component is collapsing its options. This Entity should no longer render it.
	 */
	virtual void HideDropdown (DropdownComponent* dropdown);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Looks at the specified DropdownComponent to figure out where it's drawn on the window.
	 * @param registerDelegates If true, then this function will register the HandleExpandTransformChanged handler to each relevant transform Entity.
	 */
	virtual Vector2 FindDrawLocation (DropdownComponent* dropdown, bool registerDelegates) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleExpandTransformChanged () const;
	virtual void HandleZoomChanged (Float newZoom) const;
};
SD_END