/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TestMenuFocusInterface.h
  Object that'll be testing GuiEntities.  This entity will be testing various UI elements that
  implement the FocusInterface.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class FrameComponent;
class ButtonComponent;
class TreeListComponent;
class TextFieldComponent;

class GUI_API TestMenuFocusInterface : public GuiEntity
{
	DECLARE_CLASS(TestMenuFocusInterface)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SPageComponents
	{
	public:
		std::vector<GuiComponent*> Components;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Int PageIdx;

	std::vector<SPageComponents> PageData;
	DPointer<FrameComponent> MenuFrame;
	DPointer<ButtonComponent> NextPage;
	DPointer<ButtonComponent> PrevPage;

	DPointer<ButtonComponent> FocusButton;
	DPointer<DropdownComponent> FocusDropdown;
	DPointer<TextFieldComponent> FocusTextField;
	DPointer<ListBoxComponent> FocusListBox;
	DPointer<TreeListComponent> FocusTreeList;

private:
	std::vector<DString> ListBoxEntries;
	std::vector<DString> DropdownEntries;

	std::vector<DString> TreeListEntries;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void LoseFocus () override;
	virtual void GainFocus () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sets all focus component's visibility flags based on the active page.
	 */
	virtual void RefreshPageVisibility ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNextPageReleased (ButtonComponent* uiComponent);
	virtual void HandlePrevPageReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonReleased (ButtonComponent* uiComponent);
	virtual void HandleDropdownOptionSelected (DropdownComponent* comp, Int newIdx);
	virtual void HandleListBoxItemSelected (Int selectedIdx);
	virtual void HandleListBoxItemDeselected (Int deselectedIdx);
	virtual void HandleTreeListOptionSelected (Int optionIdx);
};
SD_END

#endif