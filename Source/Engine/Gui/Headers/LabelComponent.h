/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LabelComponent.h
  A component that renders text to the screen within a bounding box.
  This class handles word wrapping, alignment, and truncating the text.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class FrameComponent;
class GuiEntity;
class TextRenderComponent;
class ScrollbarComponent;

class GUI_API LabelComponent : public GuiComponent
{
	DECLARE_CLASS(LabelComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum eHorizontalAlignment
	{
		HA_Right,
		HA_Left,
		HA_Center
	};

	enum eVerticalAlignment
	{
		VA_Top,
		VA_Center,
		VA_Bottom
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all characters that are searched for when determining where to wrap the string to the next line. */
	static const std::vector<char> WRAP_CHARS;

	/* Reference to the current font that's applied to all text instances for the render component.
	All new text instances will be created with using this font. */
	DPointer<const Font> CurrentFont;

	/* Current character size that's applied to all text instances for the render component.
	All new text instances will be created with using this character size. */
	Int CharacterSize;

	/* If true, then this component will automatically apply changes as soon as properties change. */
	bool bAutoRefresh;

	/* If true, then the LabelComponent will automatically move text to a new line
	whenever the text reached beyond the label component's boundaries. */
	bool bWrapText;

	/* If true, then the text will be truncated if it exceeds the bounding box. */
	bool bClampText;

	/* If true, then this LabelComponent will automatically scale horizontally to fit the width of the text content.  This takes
	precedence over the WrapText property and words will not wrap to the next line. */
	bool AutoSizeHorizontal;

	/* If true then this LabelComponent will automatically scale vertically to fit the height (number of lines).  This takes
	precedence over the ClampText property and characters will not truncate. */
	bool AutoSizeVertical;

	/* Determines the distance between lines. */
	Float LineSpacing;

	/* The amount of text displaced for vertical alignment purposes. This is in number of pixels. */
	Float FirstLineOffset;

	/* Content that's set to the instanced SFML's Text class.  Each element within this vector represents a single line. */
	std::vector<DString> Content;

	/* Text that was truncated due to clamped text. This is used to prevent data loss in case the component size grows again. */
	DString ClampedText;

	/* Text vertical alignment to the UI component's transformation. */
	eVerticalAlignment VerticalAlignment;

	/* Text horizontal alignment to the UI component's transformation. */
	eHorizontalAlignment HorizontalAlignment;

	/* Determines how many sf::Text instances may fit within boundaries considering font size and line spacing. */
	Int MaxNumLines;

	/* Render component that will render behind the text. */
	DPointer<FrameComponent> BackgroundComponent;

	/* Reference to the render component that draws the text. */
	DPointer<TextRenderComponent> RenderComponent;

	/* Class used to instantiate a render component (must be set before BeginObject is called). */
	const DClass* RenderComponentClass;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual void PostAbsTransformUpdate () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Convenient function that creates a LabelComponent within a GuiEntity that's viewed
	 * within the given ScrollbarComponent.
	 */
	static void CreateLabelWithinScrollbar (ScrollbarComponent* viewer, GuiEntity*& outLabelOwner, LabelComponent*& outLabelComp);

	/**
	 * Updates all Text instances to use this new font, and all new instances will use this font, too.
	 */
	virtual void SetFont (const Font* newFont);

	/**
	 * Updates all Text instances to use this character size, and all new instances will use this character size, too.
	 */
	virtual void SetCharacterSize (Int newCharacterSize);

	/**
	 * Overrides the current Content with the given new string.
	 * This will also notify the LabelComponent to word wrap, clamp text, and align text.
	 */
	virtual void SetText (const DString& newContent);

	/**
	 * Calculates MaxNumLines, and reapplies word wrapping, alignment changes, and clamping.
	 * You should invoke this function if you made any changes to the RenderComponent's Text
	 * that may have changed any spacing/size (such as fonts and character size).
	 */
	virtual void RefreshText ();

	/**
	 * Same as RefreshText but it only affects lines starting from startingLineIdx and any following lines affected from any significant changes.
	 *
	 * This function assumes the contents vector is already populated.
	 * This function will perform the following:
	 * Handle word wrapping if enabled: This function will iterate through each element starting from the specified index, and will rewrap everything
	 * until it hits a new line character or until the line is not affected.
	 * If AutoSizeHorizontal is true, it'll only process new line characters and recalculate the new horizontal width.
	 *
	 * Recalculate draw offsets for lines affected.
	 * Potentially truncate text if clamping is enabled.
	 */
	virtual void RefreshTextFromLine (size_t startingLineIdx);

	virtual void SetAutoRefresh (bool bNewAutoRefresh);

	virtual void SetWrapText (bool bNewWrapText);
	virtual void SetClampText (bool bNewClampText);
	virtual void SetAutoSizeHorizontal (bool newAutoSizeHorizontal);
	virtual void SetAutoSizeVertical (bool newAutoSizeVertical);
	virtual void SetLineSpacing (Float newLineSpacing);

	virtual void SetVerticalAlignment (eVerticalAlignment newVerticalAlignment);
	virtual void SetHorizontalAlignment (eHorizontalAlignment newHorizontalAlignment);

	virtual void SetRenderComponent (TextRenderComponent* newRenderComponent);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual const Font* GetFont () const;
	virtual Int GetCharacterSize () const;
	virtual bool GetAutoRefresh () const;
	virtual bool GetWrapText () const;
	virtual bool GetClampText () const;
	virtual bool GetAutoSizeHorizontal () const;
	virtual bool GetAutoSizeVertical () const;
	virtual Float GetLineSpacing () const;
	virtual Float GetFirstLineOffset () const;
	virtual DString GetContent () const; //Returns a copy of every line including the clamped text into a single string.
	virtual void GetContent (std::vector<DString>& outContent, bool bIncludeClamped) const; //Returns a copy of every line in the given vector. If bIncludeClamped is true, then the clamped text will be appended at the end of the vector if any.
	virtual std::vector<DString>& EditContent (); //Returns an editable reference of each line. Clamped text is excluded.
	virtual const std::vector<DString>& ReadContent () const; //Returns a read only reference of each line. Clamped text is excluded.

	/**
	 * Returns character size + line spacing.
	 */
	virtual Float GetLineHeight () const;

	/**
	 * Retrieves the line of matching index of ContentBody.
	 * Returns an empty string if the specified line index is beyond the content's number of lines.
	 */
	virtual DString GetLine (Int lineIdx) const;

	virtual eVerticalAlignment GetVerticalAlignment () const;
	virtual eHorizontalAlignment GetHorizontalAlignment () const;

	virtual Int GetMaxNumLines () const;

	virtual FrameComponent* GetBackgroundComponent () const;

	virtual TextRenderComponent* GetRenderComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeBackgroundComponent ();
	virtual void InitializeRenderComponent ();

	/**
	 * Moves all text in the content vector to the first element. All clamp text is also appended to the first element.
	 * Used to reset the content array to its original state before it was wrapped and clamped.
	 */
	virtual void ResetContent ();

	/**
	 * Iterates through all characters.  Every time a new line character is encountered, the text
	 * after the new line character is then transfered to the next Text instance within the render component.
	 * If there isn't enough space for a new line (vertical absolute size is not large enough), then
	 * this function will not add another line (and the remaining text will go beyond the last text's boundaries.
	 * Returns true if at least one new line character is found.
	 */
	virtual bool ParseNewLineCharacters ();

	/**
	 * Iterates through each character in the specified line. If a new line character is found, it'll move all text from the rest of the line to the beginning of the next (if there is one).
	 * If it's the last line, it'll create a new line at the end. This function will not process the next line even if there are new line characters that moved from the previous line.
	 * Returns true if at least one new line character is found.
	 */
	virtual bool ParseNewLineCharactersForLine (size_t lineIdx);

	/**
	 * Repositions any newline characters to ensure the text fits within the box.
	 */
	virtual void WrapText ();

	/**
	 * Removes text that extends beyond the bounding box.
	 * For performance purposes, this only considers the last line.
	 * Naturally, WrapText would have already taken care of characters beyond the width's boundaries.
	 * This function also updates the ContentTail and ContentBody based on the text that was truncated.
	 */
	virtual void ClampText ();

	/**
	 * Iterates through each line to identify the widest line.
	 * Then this function adjusts this component's size X attribute to fit the widest line.
	 */
	virtual void ApplyHorizontalAutoSize ();

	/**
	 * Resizes this component so that all lines fit in this label component.
	 */
	virtual void ApplyVerticalAutoSize ();

	/**
	 * Copies over the ContentBody vector to the RenderComponent's text vector.
	 */
	virtual void RenderContent ();

	/**
	 * Updates the render component's text instance to reflect this LabelComponent's content. If the render component does not have the text instance, it'll create one.
	 * @param lineIdx index to the Content vector. This parameter indicates which text instance to update.
	 */
	virtual void UpdateRenderContent (size_t lineIdx);

	/**
	 * Same as UpdateRenderContent, but it'll update all lines starting from the specified lineIdx to the last index.
	 */
	virtual void UpdateRenderContentFromLineToEnd (size_t firstLineIdx);

	/**
	 * Positions each text in the render component so that the text's vertical placement matches current alignment.
	 */
	virtual void AlignVertically ();

	/**
	 * Positions each text in the render component so that the text's horizontal placement matches current alignment.
	 */
	virtual void AlignHorizontally ();

	/**
	 * Same as AlignHorizontally but only applies for the given particular line.
	 * This function assumes the content and render component has instances within the line idx.
	 */
	virtual void AlignLineHorizontally (size_t lineIdx);

	/**
	 * Iterates backwards through the line of text starting from startPosition (defaults to end of line)
	 * and returns the character index that first matches any of the given targetCharacters.
	 * Returns INT_INDEX_NONE, if none of the characters were found within that line.
	 * @param startPosition The index position from where to search from. If not positive, it'll start from the very end of the line.
	 * @param outDeltaWidth The total width of the characters between startPosition and the return value.
	 */
	virtual Int FindPreviousCharacter (const std::vector<char>& targetCharacters, size_t textLine, Int startPosition, Float& outDeltaWidth) const;

	/**
	 * Calculates how many sf::Text instances may fit within this label component, and updates the
	 * MaxNumLines variable.  This function considers font size and line spacing.
	 */
	virtual void CalculateMaxNumLines ();
};
SD_END