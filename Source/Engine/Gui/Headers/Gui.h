/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Gui.h
  Contains important file includes and definitions for the Gui module.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"

#ifdef WITH_LOCALIZATION
#include "Engine\Localization\Headers\LocalizationClasses.h"
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef GUI_EXPORT
		#define GUI_API __declspec(dllexport)
	#else
		#define GUI_API __declspec(dllimport)
	#endif
#else
	#define GUI_API
#endif

#define TICK_GROUP_GUI "Gui"
#define TICK_GROUP_PRIORITY_GUI 5000

SD_BEGIN
extern LogCategory GUI_API GuiLog;
SD_END