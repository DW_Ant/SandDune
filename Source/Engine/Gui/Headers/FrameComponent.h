/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FrameComponent.h
  A component that implements a resizable bounding box.  Whenever a frame component
  is resized when the user drags any of its borders, its position and size are converted
  from relative values to unit values.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class GUI_API FrameComponent : public GuiComponent
{
	DECLARE_CLASS(FrameComponent)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EBorder
	{
		B_None,
		B_Top,
		B_Right,
		B_Bottom,
		B_Left
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this frame component will consume all mouse click events that are within this bounding box. */
	bool ConsumesInput;

	/* Callback to invoke whenever the user pressed or released a border. */
	SDFunction<void, bool /*isGrabbed*/> OnBorderGrip;

protected:
	/* Various flags that are associated with a EBorder type. */
	static const Int HorizontalBorderFlag;
	static const Int VerticalBorderFlag;

	/* If true, then this frame component will not resize when dragged. */
	bool bLockedFrame;

	/* Minimum size the user can compress this frame component when dragging borders (in pixels). */
	Vector2 MinDragSize;

	/* Maximum size the user can stretch this frame component when dragging borders (in pixels). */
	Vector2 MaxDragSize;

	/* The BorderThickness determines the how far the mouse pointer may be from the edge of this component and still grab the borders (in pixels). */
	Float BorderThickness;

	/* Reference to the border that was grabbed for resizing. */
	EBorder HorGrabbedBorder; //horizontal borders
	EBorder VertGrabbedBorder; //vertical borders

	Int HoverFlags;

	/* MousePointer this FrameComponent is currently overriding its mouse pointer.  Becomes null if it's not overriding any mouse pointer's icon. */
	DPointer<MousePointer> IconOverridePointer;

	/* Texture used on the mouse icon override. */
	DPointer<Texture> IconOverrideTexture;

	/* Reference to the component that's responsible for displaying the frames. */
	DPointer<BorderRenderComponent> BorderComp;

	/* Component responsible for placing the CenterComp within the borders. */
	PlanarTransformComponent* CenterTransform;

	/* Reference to the component responsible for rendering the center. This is typically either a SpriteComponent or ColorComponent. */
	DPointer<RenderComponent> CenterComp;

private:
	/* Limited bounds this frame can stretch to within the scope of the current mouse drag (in abs coordinates). */
	Range<Float> ClampMouseX;
	Range<Float> ClampMouseY;

	/* Dimensions of this frame component's absolute borders when the user began dragging any of its borders. */
	Rectangle StartDragBorders;

	/* Dimensions of the owner's absolute borders this frame component could stretch up to. */
	Rectangle OwnerAbsBorders;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes the RenderComponent. If it's not a SpriteComponent already, it'll create one.
	 * It'll set the center sprite component's texture to the new texture.
	 */
	virtual void SetCenterTexture (const Texture* newCenterTexture);
	virtual void SetCenterTexture (RenderTexture* newCenterTexture);

	/**
	 * Initializes the RenderComponent. If it's not a ColorRenderComponent already, it'll create one.
	 * It'll set the center color component's color to the new color.
	 */
	virtual void SetCenterColor (Color newCenterColor);

	/**
	 * If the center component is a sprite component, it'll update the sprite component's color modifier to this value.
	 * If the center component is a color render component, it'll simply set that component's color to this value.
	 */
	virtual void SetColorMultiplier (Color newColorModifier);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetLockedFrame (bool bLockedFrame);
	virtual void SetMinDragSize (const Vector2& newMinDragSize);
	virtual void SetMaxDragSize (const Vector2& newMaxDragSize);
	virtual void SetBorderThickness (Float newBorderThickness);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetLockedFrame () const;
	virtual Float GetBorderThickness () const;

	inline BorderRenderComponent* GetBorderComp () const
	{
		return BorderComp.Get();
	}

	inline RenderComponent* GetCenterComp () const
	{
		return CenterComp.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Evaluates the current cursor position and updates the mouse icon if necessary.
	 * This function may update the moues icon override based on its status.
	 * @param mousePos The mouse coordinates relative to the same space as this FrameComponent.
	 * Returns true if this should continue to override the mouse icon.
	 */
	virtual bool ShouldOverrideMouseIcon (MousePointer* mouse, const Vector2& mousePos);

	/**
	 * Calculates the frame's transformation attributes based on the mouse's coordinates (for dragging borders).
	 */
	virtual void CalculateTransformationChanges (Float mousePosX, Float mousePosY);

	/**
	 * Calculates the min/max coordinates and size of the frame component.
	 * Separated from the Transformation since it's need to be calculated at a per drag update since the scale limits is based on coordinates and size.
	 */
	virtual void CalculateTransformationLimits (Range<Float>& outAbsCoordinatesXLimits, Range<Float>& outAbsCoordinatesYLimits, Range<Float>& outSizeXLimits, Range<Float>& outSizeYLimits) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * Function callback to see if it should continue to override the mouse pointer icon.
	 * Returns false if this instance should no longer override the icon.
	 */
	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);
	virtual void HandleBorderThicknessChanged (Float newBorderThickness);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template<class T>
	inline T* GetCenterCompAs () const
	{
		return dynamic_cast<T*>(CenterComp.Get());
	}
};
SD_END