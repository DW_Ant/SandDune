/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TooltipComponent.h
  A component that grants its owning component the ability to reveal additional text when the user
  hovers over it.

  This component essentially holds the necessary data for the tooltip text, and mouse move thresholds.

  Although it's the TooltipGuiEntity that's responsible for rendering the tooltip and dictating which tooltip
  should be displayed, this component is responsible for detecting mouse movements and determining the boundaries
  which mouse positions should reveal this tooltip.

  Since the tooltip is suppose to be rendered above all while ignoring the boundaries, the tooltip render
  components are handled in an external TooltipGuiEntity.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class TooltipGuiEntity;

class GUI_API TooltipComponent : public GuiComponent
{
	DECLARE_CLASS(TooltipComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The number of seconds for the mouse to hover and pause before revealing the tooltip. */
	Float RevealDelay;

	/* Once the tooltip is revealed, this is how far the mouse can move before the tooltip disappears (in pixels). */
	Float HideMoveThreshold;

	/* Once the tooltip is hidden, this is how far the mouse must move before the tooltip can reappear (in pixels).
	This is disabled when the user stopped hovering over the component. */
	Float ShowMoveThreshold;

	/* If true, then this component does not have any text associated with it. Instead this is merely used to block other components of lesser input
	priority from notifying the TooltipGuiEntity. This is used for GuiComponents that overlap others. */
	bool bTooltipBlocker;

protected:
	/* The Tooltip Entity that is responsible for rendering the tooltip. */
	DPointer<TooltipGuiEntity> TooltipEntity;

	/* The text to reveal when displaying this tooltip. */
	DString TooltipText;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Quick utility to hide the tooltip if it's currently showing this component's tooltip.
	 */
	virtual void HideTooltip ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTooltipText (const DString& newTooltipText);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline TooltipGuiEntity* GetTooltipEntity () const
	{
		return TooltipEntity.Get();
	}

	inline const DString& ReadTooltipText () const
	{
		return TooltipText;
	}
};
SD_END