/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TestMenuButtonList.h
  Object that'll be testing GuiEntities.  This entity will be testing tab order, and will be testing
  unfocusable buttons.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class FrameComponent;
class ButtonComponent;

class GUI_API TestMenuButtonList : public GuiEntity
{
	DECLARE_CLASS(TestMenuButtonList)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<FrameComponent> MenuFrame;

	std::vector<DPointer<ButtonComponent>> ButtonList;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void LoseFocus () override;
	virtual void GainFocus () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual ButtonComponent* InitializeButton ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleButtonReleased (ButtonComponent* uiComponent);
};
SD_END

#endif