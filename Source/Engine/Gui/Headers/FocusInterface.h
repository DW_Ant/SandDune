/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FocusInterface.h
  An interface that declares all essential functions needed to communicate with a FocusComponent.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GUI_API FocusInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

private:
	bool FocusInterface_bHasFocus = false;
	bool bCanBeFocused = true;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true, if the focus component is allowed to focus on this Entity.
	 */
	virtual bool CanBeFocused () const;

	/**
	 * Returns true if this focused object's current state allows the user to tab out.
	 * Can the user press tab to focus the next object?
	 */
	virtual bool CanTabOut () const;

	/**
	 * Called whenever a FocusComponent is now focusing on this Entity.
	 */
	virtual void GainFocus ();

	/**
	 * Called whenever a FocusComponent is no longer focusing on this Entity.
	 */
	virtual void LoseFocus ();

	/**
	 * Similar to InputComponent, this function consumes the input event if returns true.
	 */
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) = 0;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) = 0;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCanBeFocused (bool bNewCanBeFocused);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool HasFocus () const;
};
SD_END