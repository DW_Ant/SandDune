/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollbarComponent.h
  A component that can manage UI elements within a frame.  Useful when there are
  UI components that are far larger than the viewable space.

  The scrollbar frame is a Render Texture that renders more components.  The scrollbar,
  itself, relays input events to those components.  If those events are not consumed, then
  the scrollbar handles the event.

  The Scrollbar is also responsible for managing the camera perspective for the Render Texture.
  It contains controls and input handlers for adjusting camera position and size.

  The ScrollbarComponent can only communicate with objects that implement the ScrollableInterface.
=====================================================================
*/

#pragma once

#include "FakeMouseOwnerInterface.h"
#include "GuiComponent.h"

SD_BEGIN
class ButtonComponent;
class LabelComponent;
class ScrollableInterface;
class FakeMouse;

class GUI_API ScrollbarComponent : public GuiComponent, public FakeMouseOwnerInterface
{
	DECLARE_CLASS(ScrollbarComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	/**
	 * Enumeration that defines which direction the thumb is traveling.
	 */
	enum ETravelTrackDirection
	{
		TTD_None,
		TTD_Up,
		TTD_Down,
		TTD_Left,
		TTD_Right
	};

	enum EControlState
	{
		CS_Uninitialized,
		CS_Disabled,
		CS_Enabled
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
#pragma region "configurable variables"
	/* When panning the camera via ViewedObject's OnScrollToPosition, this is how quickly the camera will reach to
	the specified destination (in seconds). If not positive, then the camera will snap to the destination. */
	Float ScrollToPositionTime;

	/* When clicking on a track or pan button, this determines how much the camera moves initially
	before it continuously pans.
	This is for users who tap on controls rather than holding on to them. */
	Float InitialPanDistance;

	/* The speed multiplier when control/alt/shift is held. */
	Float ShiftSpeedMultiplier;
	Float AltSpeedMultiplier;

	/* The speed the camera pans when the vertical/horizontal scroll buttons are held (in pixels per second). */
	Float ScrollSpeed;

	/* The speed the camera pans when the vertical/horizontal tracks are held (in pixels per second). */
	Float TravelTrackSpeed;

	/* The amount of velocity increase (in pixels per second) if more mouse wheel input was sent in the same direction as camera momentum. */
	Float WheelSpeedAccumulationRate;

	/* Min/Max range in how many pixels per second the camera pans using the middle mouse. */
	Range<Float> AnchorPanSpeed;

	/* The min/max distance the mouse pointer must be away from anchor position to achieve min/max panning speed.
	If the mouse distance is less than minimum, then the camera will stop panning.  Units are in pixels.
	The distances are calculated independently with X axis and Y axis. */
	Range<Float> AnchorDistanceLimits;

	/* When anchoring, this is the time that must elapse to change a hold to drag to a toggle. */
	Float MinToggleTime;

	/* When holding the panning buttons or the track, this is the amount of time needed to elapse before
	it continuously pans (in seconds). */
	Float HoldToDragTime;

	/* Min/Max zoom values for the FrameCamera. */
	Range<Float> ZoomRange;

	/* List of zoom values that the camera may snap to when dragging near their values. */
	std::vector<Float> ZoomSnapValues;

	/* If the camera's zoom value is within this threshold to any snap value, then it'll snap to the closest
	snap value. */
	Float ZoomSnapTolerance;

	/* Determines how quickly the camera can zoom towards DesiredZoom. The formula it uses is y=x^2 where x is the amount of work (mouse movement or wheel),
	and y is the zoom amount. This variable determines how quickly the zoom moves along the x-axis (in 1 x-unit per second). */
	Float  ZoomSpeed;

	/* Determines the zoom amount per mouse wheel Formula NewDesiredZoom=sqrt(DesiredZoom)*ZoomSensitivity. */
	Float ZoomSensitivity;

	/* If true, then this scrollbar component will destroy its viewed object when this scrollbar component is destroyed. */
	bool bDestroyViewedObjectOnDestruction;
#pragma endregion

protected:
#pragma region "Objects"
	/* The scrollable object the frame is viewing. */
	ScrollableInterface* ViewedObject;

	/* The Render Texture that'll be rendering the external UI components.  The size of the texture
	is based on the total dimensions of the external UI components. */
	DPointer<RenderTexture> FrameTexture;

	/* If true, then the FrameTexture will stretch to the scrollbar's size, and it will not reconstruct itself until this flag is false.
	This is primarily used to avoid reconstructing the texture on frame update (such as dragging a frame component to rescale). */
	bool bFreezeTextureSize;

	/* The camera that'll be panning/zooming based on this component's controls. */
	DPointer<PlanarCamera> FrameCamera;

	/* The sprite that's rendering the frame. */
	DPointer<PlanarTransformComponent> FrameSpriteTransform;
	DPointer<SpriteComponent> FrameSprite;

	/* The mouse instance that resides in scrollbar coordinate space. */
	DPointer<FakeMouse> FrameMouse;

	DPointer<ButtonComponent> ScrollUpButton;
	DPointer<ButtonComponent> ScrollDownButton;
	DPointer<ButtonComponent> ScrollLeftButton;
	DPointer<ButtonComponent> ScrollRightButton;

	/* The transforms for the track components. */
	PlanarTransformComponent* VerticalTrackTransform;
	PlanarTransformComponent* HorizontalTrackTransform;

	/* The track that the thumb traverses through. */
	DPointer<ColorRenderComponent> VerticalTrack;
	DPointer<ColorRenderComponent> HorizontalTrack;

	/* The highlight over one of the thumb tracks that represents where the thumb is traveling to. */
	PlanarTransformComponent* TravelTrackTransform;
	DPointer<ColorRenderComponent> TravelTrack;

	PlanarTransformComponent* VerticalThumbTransform;
	PlanarTransformComponent* HorizontalThumbTransform;

	/* The thumb that can be dragged around to pan.  The size of the thumb reflects the size of the external
	UI components within the frame relative to the size of the FrameSprite. */
	DPointer<ColorRenderComponent> VerticalThumb;
	DPointer<ColorRenderComponent> HorizontalThumb;

	PlanarTransformComponent* MiddleMouseAnchorTransform;

	/* The sprite that's displays the center of the middle mouse anchor when the user presses the middle mouse
	button over the frame. */
	DPointer<SpriteComponent> MiddleMouseAnchor;

	/* Button that toggles the zoom controls when pressed/released. */
	DPointer<ButtonComponent> ZoomButton;

	PlanarTransformComponent* ZoomFeedbackTransform;

	/* The label that describes the zoom level. */
	DPointer<LabelComponent> ZoomLabel;

	/* The mouse pointer that most recently interacted with this component. */
	DPointer<MousePointer> Mouse;

	DPointer<TickComponent> Tick;
#pragma endregion

	/* Position where the middle mouse anchor is placed (in absolute coordinates).
	Becomes negative if the scrollbar is not in anchored mode. */
	Vector2 AnchorPosition;

	/* The latest texture computed when overriding mouse icon. */
	DPointer<Texture> AnchoredMouseIcon;

	/* Determines how often the mouse icon is computed (in seconds). */
	Float AnchorIconRefreshTimeInterval;

	/* Timestamp when the anchor icon was last computed. */
	Float AnchorIconTime;

	/* Timestamp when the anchor was placed (used to differentiate hold to drag vs toggle drag). */
	Float StartAnchorTime;

	/* If true, then the vertical/horizontal scrollbars are full when the render target is able to view the full
	width/height of the Sub UI.  Otherwise, they will become disabled when there's nothing to pan. */
	bool HideControlsWhenFull;

	/* Color to use on the track when scrolling controls are enabled. */
	Color EnabledTrackColor;

	/* Color to use on the thumb when scrolling controls are enabled. */
	Color EnabledThumbColor;

	/* Color to use on the thumb when scrolling controls are disabled.  Note:  There is no disabled track color since the
	thumb covers the entire track when it's disabled. */
	Color DisabledThumbColor;

	/* Color to use when the thumb is pressed (being dragged). */
	Color ThumbDragColor;

	/* If true, then zoom controls are enabled and the zoom components are visible. */
	bool ZoomEnabled;

	/* Control states used to detect state changes (if it needs to change control visibility/enabledness). */
	EControlState HorizontalControlState;
	EControlState VerticalControlState;

	/* Velocity the camera is moving (in pixels per second).  This is shared with anchor movement, held scroll buttons,
	track traversal.  This is the base speed.  The keyboard modifiers (ctrl, alt, shift) are not considered here. */
	Vector2 CameraPanVelocity;

	/* The position the camera is heading towards.  May adjust CameraPanVelocity to ensure the camera reaches here.
	If negative, then the camera is not heading towards this position.  This is in pixels in frame coordinate space. */
	Vector2 CameraDestination;

	/* The zoom 'destination' the camera zoom is moving towards. If negative, then the camera is not zooming anywhere. */
	Float DesiredZoom;

	/* Direction where one of the thumbs are moving towards. */
	ETravelTrackDirection TravelTrackDirection;

	/* When panning by holding the track, this is the original velocity the camera is moving.  The camera velocity can be adjusted
	based on mouse cursor's relative position to the thumb, and the velocity may need to be restored based on user mouse placement. */
	Vector2 OriginalCamPanVelocity;

	/* When comparing last known camera position and frame size to the current, it'll compare against this value to see
	if the difference is greater than this threshold.  If so, then it'll recalculate thumb transforms. */
	Float TransformDetectionThreshold;

private:
	/* Timestamp when the OnScrollToPosition delegate was handled (in Engine real time seconds). Disabled when not positive. */
	Float LastScrollToPositionTime;

	/* Destination where the FrameCamera is headed due to the OnScrollToPosition handling. */
	Vector2 ScrollToPositionSrc;
	Vector2 ScrollToPositionDest;

	/* Determines the thickness of the scrollbar. */
	Float ScrollbarThickness;

	/* Becomes true if any of the scroll buttons are held. */
	bool IsHoldingScrollButton;

	/* When a thumb is pressed, this is the click offset the user clicked on the thumb.  If negative, then the user isn't holding on a thumb.
	For vertical thumb, this is the number of pixels away from its top.  For horizontal, this is number of pixels away from its left. */
	Float VerticalThumbHoldOffset;
	Float HorizontalThumbHoldOffset;

	/* Becomes true if this scrollbar is resetting the mouse position while zooming. */
	bool SettingMousePosition;

	/* Becomes true if this component needs to reconstruct the frame texture to fit the frame's current size.
	Setting this to true will also prevent the ViewedObject from receiving notifications about an update about the new scrollbar abs size. */
	bool bUpdateFrameTextureSize;

	/* Cached variables used to determine if thumb transforms needs to be updated. */
	Vector2 CamLatestPos;
	Vector2 SpriteLatestSize;
	Vector2 ObjLatestSize;
	Float CamLatestZoom;

	/* The sprite size of the mouse cursor before the middle mouse anchor overwrote its size. */
	Vector2 OriginalMouseSize;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual void PostAbsTransformUpdate () override;

	virtual void ExecuteInput (const sf::Event& evnt) override;
	virtual void ExecuteText (const sf::Event& evnt) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual bool ExecuteConsumableInput (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableText (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;

	virtual Vector2 FakeMouseToInnerCoordinates (const Vector2& outerMousePos) const override;
	virtual Vector2 FakeMouseToOuterCoordinates (const Vector2& innerMousePos) const override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Destroys the viewed object, and clears the reference to that object.
	 */
	void DestroyViewedObject ();

	/**
	 * Returns true if either the vertical or horizontal scrollbars can scroll.
	 * Returns false if both axis cannot scroll due to ViewedObject size relative to FrameSprite size.
	 */
	virtual bool IsScrollingEnabled () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Replaces the viewed object with the specified one.  This does not delete the old view object.
	 * Use DestroyViewedObject to destroy the object.
	 */
	void SetViewedObject (ScrollableInterface* newViewedObject);
	virtual void SetFreezeTextureSize (bool bNewFreezeTextureSize);
	void SetHideControlsWhenFull (bool newHideControlsWhenFull);

	virtual void SetEnabledTrackColor (Color newEnabledTrackColor);
	virtual void SetEnabledThumbColor (Color newEnabledThumbColor);
	virtual void SetDisabledThumbColor (Color newDisabledThumbColor);
	virtual void SetThumbDragColor (Color newThumbDragColor);
	virtual void SetZoomEnabled (bool newZoomEnabled);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool GetHideControlsWhenFull () const
	{
		return HideControlsWhenFull;
	}

	inline Color GetEnabledTrackColor () const
	{
		return EnabledTrackColor;
	}

	inline Color GetEnabledThumbColor () const
	{
		return EnabledThumbColor;
	}

	inline Color GetDisabledThumbColor () const
	{
		return DisabledThumbColor;
	}

	inline Color GetThumbDragColor () const
	{
		return ThumbDragColor;
	}

	inline bool IsZoomEnabled () const
	{
		return ZoomEnabled;
	}

	ScrollableInterface* GetViewedObject () const;
	RenderTexture* GetFrameTexture () const;
	PlanarCamera* GetFrameCamera () const;
	SpriteComponent* GetFrameSprite () const;
	PlanarTransformComponent* GetFrameSpriteTransform () const;
	ButtonComponent* GetScrollUpButton () const;
	ButtonComponent* GetScrollDownButton () const;
	ButtonComponent* GetScrollLeftButton () const;
	ButtonComponent* GetScrollRightButton () const;
	ColorRenderComponent* GetVerticalTrack () const;
	ColorRenderComponent* GetHorizontalTrack () const;
	ColorRenderComponent* GetTravelTrack () const;
	ColorRenderComponent* GetVerticalThumb () const;
	ColorRenderComponent* GetHorizontalThumb () const;
	SpriteComponent* GetMiddleMouseAnchor () const;
	ButtonComponent* GetZoomButton () const;
	LabelComponent* GetZoomLabel () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Computes the camera's Zoom based on the DesiredZoom and the ZoomSpeed.
	 */
	virtual void UpdateZoom (Float deltaSec);

	/**
	 * Returns coordinates that's relative to the ViewedObject.
	 * @param absPosition - Position in absolute space (relative to top left corner of window).
	 * @return The coordinates in ViewedFrame space.
	 */
	virtual Vector2 CalcViewedFramePosition (const Vector2& absPosition) const;

	/**
	 * Returns the min and maximum scrollable region for the frame camera.
	 * This is in absolute units in the ViewedObject space.
	 * This function considers the sprite's viewable space, and the camera's view extents.
	 */
	virtual void GetCamPositionLimits (Vector2& outMinPos, Vector2& outMaxPos) const;

	/**
	 * Adjusts the given camera position parameter to ensure that the position is within the allowable scrollable region.
	 * This considers the sprite's viewable space.
	 * This function is independent from camera's current position.
	 */
	virtual void ClampCamPosition (Vector2& outClampedPosition) const;

	/**
	 * While the camera is panning (due to the track traveling), this evaluates the mouse relative
	 * position to the scroll thumb. This adjusts the camera velocity if the mouse is in oposite direction
	 * of travel track direction.
	 */
	virtual void UpdateTravelTrackVelocity (const Vector2& mousePos);

	/**
	 * Computes TravelTrack's transform is updated based on mouse position and direction.
	 */
	virtual void CalculateTravelTrackTransform (const Vector2& mousePos);

	/**
	 * Evaluates the scrollable region and the size of the view frame, and recomputes the vertical/horizontal
	 * thumb sizes.  This also refreshes the thumbs' colors based on the scrollbar's "enabledness".
	 * Doesn't update anything if the camera and size remains the same since last calculation.
	 */
	virtual void CalculateThumbTransforms ();

	/**
	 * Sets the button visibility/enabledness based on if this scrollbar is scrollable.
	 */
	virtual void RefreshScrollButtonConditions (bool isVertScrollingEnabled, bool isHorScrollingEnabled);

	/**
	 * Checks the viewable region of the sprite that displays ViewedObject.
	 * Updates the sprite so that viewed object is not scaled.
	 */
	virtual void RefreshSpriteTextureRegion ();

	/**
	 * Replaces the frame texture with a new texture that matches the scrollbar's dimensions.
	 * Does nothing if the frame texture is already the correct size.
	 */
	virtual void ReconstructFrameTexture ();

	/**
	 * Computes the anchored mouse icon based on the mouse position relative to anchor position.
	 */
	virtual Texture* CalculateAnchoredMouseIcon (const Vector2& mousePos) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleScrollUpButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollDownButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollLeftButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollRightButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonReleased (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonRightClickReleased (ButtonComponent* uiComponent);
	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvnt);
	virtual void HandleZoomChanged (Float newZoom);
	virtual void HandleTick (Float deltaSec);
	virtual void HandleScrollToPosition (const Vector2& newScrollPosition, bool bDeltaOffset);
};
SD_END