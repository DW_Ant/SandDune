/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TooltipGuiEntity.h
  Gui Entity that is responsible for managing the tooltip display.

  Tooltips are handled slightly unconventionally since the tooltips should render above
  all other GUI components, and they should render anywhere within the render target.

  Lastly, there should only be one tooltip displayed at a time.

  This is why this Entity class exist. The GuiDrawLayer will have a single reference to the
  tooltip manager (TooltipGuiEntity) where any components may invoke the draw layer to display
  the tooltip.

  Also this Entity handles tooltip input conflicts. Instead of each tooltip component handling their own
  mouse events, this Entity only handles the tooltip component with the highest input priority.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

SD_BEGIN
class FrameComponent;
class LabelComponent;
class TooltipComponent;

class GUI_API TooltipGuiEntity : public GuiEntity
{
	DECLARE_CLASS(TooltipGuiEntity)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Number of seconds it takes for the tooltip to show itself. */
	Float RevealFadeTime;

	/* Number of seconds it takes for the tooltip to hide. */
	Float HideFadeTime;

protected:
	/* Number of extra pixels to pad between the text and the background borders. */
	Float TopMargin;
	Float RightMargin;
	Float BottomMargin;
	Float LeftMargin;

	/* The mouse position when the tooltip was originally revealed. */
	Vector2 TooltipShowPosition;

	/* Number of seconds that needs to elapse before the tooltip is going to be revealed. The user cannot move the mouse without resetting this timer. */
	Float StartRevealTimeDelay;

	FrameComponent* TooltipFrame;
	LabelComponent* TooltipText;

	/* TooltipComponent instance this Entity is currently displaying. This component does not attach to this Entity. Instead it's merely the tooltip
	that called process mouse move with the highest input priority. This Entity does not own this component. */
	DPointer<TooltipComponent> TooltipOwner;

	/* Tick component responsible for counting when it's time to reveal the tooltips. */
	TickComponent* RevealTick;

	/* Tick component responsible for handling fading the tooltip in and out. */
	TickComponent* FadingTick;

	/* Timestamp when the tooltip started to frade in and out (in seconds). If negative, then this Entity is not fading the tooltip. */
	Float StartFadeTime;

	/* The alpha channel of the border component when the tooltip is fully revealed. */
	sf::Uint8 DefaultBorderAlpha;

	/* The alpha channel the components are fading from. */
	sf::Uint8 FadeSource;

	/* The alpha channel the components are fading from for the border component. */
	sf::Uint8 FadeSourceBorder;

	/* The alpha channel the components are fading towards. */
	sf::Uint8 FadeDestination;

	/* The alpha channel the border component is fading towards. */
	sf::Uint8 FadeBorderDestination;

private:
	/* This Entity needs to prioritize Tooltip process mouse move events. In order to detect when it's a different mouse move event, it'll compare against
	this vector. If this vector is different from the mouse's current position, then this entity assumes it's a different mouse move event. */
	Vector2 LastMousePosition;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual void HandlePassiveInput (const sf::Event& evnt) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * A mouse move event detected from a TooltipComponent. This event is relayed here to handle input priority. If this Entity is already
	 * handling a mouse move event, then this event is ignored.
	 * @param isWithinBounds - If true, then the mouse pointer instance is currently hovering over the tooltip.
	 */
	virtual void ProcessTooltipMouseMove (MousePointer* mouse, bool isWithinBounds, TooltipComponent* tooltip);

	/**
	 * Starts the fade out process regardless if it's currently fading in.
	 * If the HideFadeTime is not positive, then this function will instantly hide the tooltip.
	 */
	virtual void HideTooltip ();


	/*
	=====================
	  Mutators
	=====================
	*/

	virtual void SetTopMargin (Float newTopMargin);
	virtual void SetRightMargin (Float newRightMargin);
	virtual void SetBottomMargin (Float newBottomMargin);
	virtual void SetLeftMargin (Float newLeftMargin);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetTopMargin () const
	{
		return TopMargin;
	}

	inline Float GetRightMargin () const
	{
		return RightMargin;
	}

	inline Float GetBottomMargin () const
	{
		return BottomMargin;
	}

	inline Float GetLeftMargin () const
	{
		return LeftMargin;
	}

	inline TooltipComponent* GetTooltipOwner () const
	{
		return TooltipOwner.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Starts the fade in sequence to reveal the tooltip over the reveal location.
	 */
	virtual void RevealTooltip ();

	/**
	 * Calculates the tooltip frame position so that its borders do not go beyond the RenderTarget's canvas.
	 * It'll prioritize picking a position that is above and horizontally centered over desiredPosition.
	 * If vertically occupied, it'll try placing below it.
	 * It'll either try to snap to the left or right if there's horizontal overlap between the borders when centered.
	 */
	virtual Vector2 CalcTooltipFramePosition (const Vector2& desiredPosition);

	/**
	 * Modifies the components' alpha channel(s) to the specified value to implement the fade effect.
	 */
	virtual void ApplyAlpha (sf::Uint8 newAlpha, sf::Uint8 newBorderAlpha);

	/**
	 * Returns the current alpha channel from the components.
	 */
	virtual void GetAlpha (sf::Uint8& outAlpha, sf::Uint8& outBorderAlpha) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleRevealTick (Float deltaSec);
	virtual void HandleFadingTick (Float deltaSec);
};
SD_END