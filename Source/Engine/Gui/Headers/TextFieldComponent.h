/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFieldComponent.h
  A component where the user may manpulate the text of a LabelComponent.
  This implements text selection for copy/pasting, and implements inserting text at cursor.
=====================================================================
*/

#pragma once

#include "FocusInterface.h"
#include "GuiComponent.h"
#include "LabelComponent.h"

SD_BEGIN
class ContextMenuComponent;
class GuiEntity;

class GUI_API TextFieldComponent : public LabelComponent, public FocusInterface
{
	DECLARE_CLASS(TextFieldComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the character limit in how many characters could the user enter into the text.  This only limits user input. Ignored if negative. */
	Int MaxNumCharacters;

	/* Rate (in seconds) the scroll bar moves up/down when dragging over text beyond current scroll position. */
	Float DragScrollRate;

	/* If true, then the entire text is automatically selected when the user clicks on the field. */
	bool bAutoSelect;

	/* Event to invoke whenever text input is pressed while editing text.  If the handler returns true, then the input is consumed before the TextFieldComponent processes it. */
	SDFunction<bool, const sf::Event&> OnEdit;

	/* Event to invoke whenever the user entered text input inside this field. If it returns true, then the TextField will process the text. */
	SDFunction<bool, const DString& /*txt*/> OnAllowTextInput;

	/* Delegate invoked every time the user modified the contents of this text field. This delegate is processed after the component formatted its text. */
	SDFunction<void, TextFieldComponent* /*uiComponent*/> OnTextChanged;

	/* Event to broadcast whenever the user pressed 'enter' to apply changes.
	If this is bound, then the user cannot add new lines to this text field since it acts like it's applying changes instead. */
	SDFunction<void, TextFieldComponent* /*uiComponent*/> OnReturn;

protected:
	/* If true, then the user may edit the text of this field. */
	bool bEditable;

	/* If true, then the user may drag over text for highlighting and copying. */
	bool bSelectable;

	/* If true, then this text field will not have any new line characters. There's no word wrapping.
	Instead the text will scroll horizontally to display characters around the cursor. */
	bool bSingleLine;

	/* Index position where the cursor resides.  This also determines the "pivot" point when dragging for highlighting.
	This ranges from 0 to ContentLength where position 0 is before the first character, and position ContentLength is after last character.
	Note:  there are two cursor positions between each line.  One is at the end, and the other is found at the beginning of the next line. */
	Int CursorPosition;

	/* Line at which the CursorPosition may be located.  This may include lines beyond scroll position.  Becomes negative if CursorPosition is not set. */
	Int ActiveLineIndex;

	/* Other end of the selected text.  Text between this value and the CursorPosition is highlighted.  Nothing is highlighted if negative. */
	Int HighlightEndPosition;

	/* For single line text fields, this is where the true cursor position is located in the head or tail text. Negative values implies it resides in the header text.
	Positive values implies it resides in the tail text. Zero implies that the cursor resides in the displayed text.
	This only exist for the CursorPosition. There is nothing for highlight overflow since the HighlightEndPosition should always be within the displayed text. */
	Int SingleLineCursorOverflow;

	/* The horizontal position where the cursor (or HighlightEndPosition if highlighting) is found relative to the TextFieldComponent's left border.
	This is used whenever the cursor jumps up or down. This is only updated when navigating left or right.
	Becomes negative is not active. */
	Float HorizontalCursorPosition;

	/* MousePointer that needs its mouse icon restored. */
	DPointer<MousePointer> IconOverrideMouse;

	/* For single line text fields, this is the amount of character positions the cursor will skip over when panning left/right off the field. */
	Int SingleLineCursorJumpAmount;

	/* For single line text fields, this text is what appears before the first visible character. */
	DString SingleLineHeaderText;

	/* For single line text fields, this text is what appears after the last visible character. */
	DString SingleLineTailText;

	/* When manipulating the highlight range via mouse drag, this range determines the min and max distances the mouse position must be beyond the text field component's
	left and right borders. The rate the highlight drags beyond the visible text will linearly interpolate based on this range's min and max range and the mouse position.
	This is only applicable for single line text field components. */
	Range<Float> SingleLineHighlightDragBorderRange;

	/* When manipulating the highlight range via mouse drag, this range determines the min and max rate the highlight will move when highlighting characters in the header and tail.
	The rate is in characters per second. The rate linearly interpolates between min and max based on the mouse's position within the drag border range.
	This is only applicable for single line text field components. */
	Range<Float> SingleLineHighlightDragCharRate;

	DPointer<ContextMenuComponent> ContextComp;

private:
	/* If true, then the next text event detected while this component is focused, is ignored. */
	bool bIgnoreNextFocusedTextEvent;

	/* True if the user is dragging the mouse over the text for highlighting. */
	bool bDraggingOverText;

	/* Last time the scroll bar moved because the user dragged beyond the visible scroll length. */
	Float LastDragScrollTime;

	/* The latest mouse position when it was dragging over the text. */
	Vector2 LatestDragMousePosition;

	/* The original content of the text field at the time when the user clicked or focused the field. In case they need to undo their work. */
	DString OriginalContent;

	/* The cached screen coordinates of the cursor coordinates last time CalcCharPosition was called where the parameter is equal to the cursor position.
	This becomes invalid if bLastCursorScreenPosDirty is true. */
	mutable Vector2 LastCursorScreenPos;
	mutable bool bLastCursorScreenPosDirty;

	/* Becomes a valid texture if this text field is currently overriding the mouse pointer's icon.
	This is typically true when it's hovered. */
	DPointer<Texture> OverridingMouseIcon;

	/* Becomes true if the latest key was pressed down with the control key held (to make the release order of Ctrl+Key independent).
	For example, pressing Ctrl then C then releasing Ctrl before releasing C should still register the C key as a copy command. */
	bool bIsCtrlCmd;

	/* Tick component used for selecting characters in the header or tail. */
	TickComponent* SingleLineHighlightDragTick;

	Float SingleLineHighlightDragTickRate;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	//Object
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	//Transformation
	virtual void PostAbsTransformUpdate () override;

	//Focus
	virtual bool CanBeFocused () const override;
	virtual bool CanTabOut () const override;
	virtual void GainFocus () override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	//Label
	virtual void SetText (const DString& newContent) override;
	virtual void RefreshTextFromLine (size_t startingLineIdx) override;
	virtual DString GetContent () const override;
	virtual void GetContent (std::vector<DString>& outContent, bool bIncludeClamped) const override;

	//GuiComponent
	virtual bool ExecuteConsumableInput (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableText (const sf::Event& evnt) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	//LabelComponent
	virtual void InitializeBackgroundComponent () override;

	//GuiComponent
	virtual void InitializeComponents () override;

	//Object
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the current cursor position is visible.  Returns false if cursor position is not visible or set.
	 * If an absCursorPos is specified, the function will use those coordinates to determine if that pixel is visible. Otherwise it'll fallback to the CursorPosition's coordinates.
	 */
	virtual bool IsCursorVisible () const;
	virtual bool IsCursorVisible (const Vector2& absCursorPos) const;

	/**
	 * Inserts the new text at the current cursor position.
	 */
	virtual void InsertTextAtCursor (DString insertedText);

	virtual void SelectAll ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetEditable (bool bNewEditable);

	virtual void SetSelectable (bool bNewSelectable);

	/**
	 * Sets the TextField to be in single line or multi line mode.
	 * Invoking this function will set the text field's wrapping and clamping properties.
	 */
	virtual void SetSingleLine (bool bNewSingleLine);

	/**
	 * Updates the cursor position and the scroll position if necessary.
	 * Automatically clamps the newPosition to the length of the displayed text.
	 * If there's a need to move the cursor beyond the displayed text for single line text fields, use MoveCursor or JumpCursorToBeginning or JumpCursorToEnd.
	 * @param bMoveForwardIfAfterNewLine Cursor positions cannot be placed after a new line character. If this method detects that the new position would end up placing the cursor
	 * after a new line character, it'll advance the newPosition to either forwards or backwards. This boolean determines the direction.
	 */
	virtual void SetCursorPosition (Int newPosition, bool bMoveForwardIfAfterNewLine = false);

	/**
	 * Moves the cursor to the first character. If bMoveHighlight is true, then it'll move the highlighting cursor instead of the cursor to the beginning.
	 */
	virtual void JumpCursorToBeginning (bool bMoveHighlight);

	/**
	 * Moves the cursor to the end of the text. If bMoveHighlight is true, then the cursor will stay where it's at. Instead the highlight position will jump to the end.
	 */
	virtual void JumpCursorToEnd (bool bMoveHighlight);

	/**
	 * Updates the HighlightEndPosition to the new position. This is only relevant to the displayed text.
	 * If there's a need to move the HighlightEndPosition to the clamped text for single line text fields, use MoveCursor or JumpCursorToBeginning or JumpCursorToEnd.
	 */
	virtual void SetHighlightEndPosition (Int newPosition);

	virtual void SetSingleLineHighlightDragBorderRange (const Range<Float>& newSingleLineHighlightDragBorderRange);
	virtual void SetSingleLineHighlightDragCharRate (const Range<Float>& newSingleLineHighlightDragCharRate);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetEditable () const;
	virtual bool GetSelectable () const;

	inline bool IsSingleLine () const
	{
		return bSingleLine;
	}

	inline Int GetCursorPosition () const
	{
		return CursorPosition;
	}

	inline Int GetHighlightEndPosition () const
	{
		return HighlightEndPosition;
	}

	/**
	 * This returns the cursor's text position.  Different from CursorPosition where there are extra
	 * cursor positions between each line.  This function returns the text index position the cursor may be found.
	 */
	virtual Int GetTextPosition () const;

	/**
	 * Same as GetTextPosition but allows for any arbitrary cursor index to be passed in.
	 * The cursor idx is only relative to the displayed text (excludes text found in the head or tailing text for single line text field components).
	 */
	virtual Int GetTextPosition (Int cursorIdx, Int lineIdx) const;

	/**
	 * Returns the text section that's selected.
	 */
	virtual DString GetSelectedText () const;

	inline Range<Float> GetSingleLineHighlightDragBorderRange () const
	{
		return SingleLineHighlightDragBorderRange;
	}

	inline Range<Float> GetSingleLineHighlightDragCharRate ()
	{
		return SingleLineHighlightDragCharRate;
	}

	inline ContextMenuComponent* GetContextComp () const
	{
		return ContextComp.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns the text that is visible for single line text fields (excludes header and tail text). For multi-line text fields, it's the same as GetContent().
	 */
	virtual DString GetDisplayedText () const;

	/**
	 * Processes the new text for single line TextFieldComponents.
	 * This function will truncate the text to fit inside the component.
	 * Some of the text will move to the header and tail in a way so that the cursor position (if any) is visible.
	 * If the cursor isn't placed, it'll simply render the beginning of the text while moving the rest to the tail.
	 * Returns a portion of newText that should be displayed in this component.
	 */
	virtual DString ProcessSingleLineSetText (const DString& newText);

	/**
	 * Checks the mouse event to see if it should start overriding the mouse icon.
	 */
	virtual void UpdateMousePointerIcon (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);

	/**
	 * Finds the cursor index position based on the given absolute mouse coordinates.
	 * Returns negative values if the specified coordinates are out of bounds.
	 */
	virtual Int FindCursorPos (Int absX, Int absY);

	/**
	 * Returns the total number of cursor positions within the text body content.
	 */
	virtual Int CountNumCursorPositions () const;

	inline Int CountHighestCursorIdx () const
	{
		return (CountNumCursorPositions() - 1);
	}

	/**
	 * Calculates and updates the Cursor's horizontal position based on the given cursor position.
	 */
	virtual void UpdateCursorHorizontalPosition (Int cursorPos);

	/**
	 * Marks the cached data as dirty so that next time it needs the cursor position, it'll
	 * recalculate it rather than referring to the cached data.
	 */
	virtual void MarkCacheDirty () const;

	/**
	 * Calculates and returns the x,y position in absolute space at the specified cursor index.
	 * The cursor position is cached.
	 * The vertical multiplier is used to return the offset relative to the line height. For example: 0 means top of the line, 0.5 means center, 1 means bottom of the line.
	 */
	virtual Vector2 CalcCharPosition (Int absCursorIdx, Float verticalMultiplier) const;

	/**
	 * Same as CalcCharPosition, but it expects an arbitrary line and cursor position relative to that line.
	 * The vertical multiplier is used to return the offset relative to the line height. For example: 0 means top of the line, 0.5 means center, 1 means bottom of the line.
	 */
	virtual Vector2 CalcCharPosition (size_t lineIdx, Int localCursorIdx, Float verticalMultiplier) const;

	/**
	 * Retrieves the cursor index position relative to the line the given global cursor index may be found.
	 * @Param cursorIdx:  cursor index position in Content.
	 * @Param outLocalCursorIdx:  cursor index position relative to the current line.
	 * Returns the line index the cursor may be found.  The line index is an index of the full text array (includes head, body, and tail).
	 * Returns -1 if the given cursorIdx is not within full text range.
	 */
	virtual Int GetCursorLineData (Int cursorIdx, Int& outLocalCursorIdx) const;
	virtual Int GetCursorLineData (Int cursorIdx) const;

	/**
	 * Returns the cursor position that marks the beginning of the specified line.
	 */
	virtual Int GetLineCursorIdx (size_t lineIdx) const;

	/**
	 * Updates the state of this TextFieldComponent based on the new drag text flag.
	 */
	virtual void SetDragOverText (bool isDraggingOverText);

	/**
	 * Deletes the selected text from the text field. This also shifts the cursor position to be at the start of the former highlighted text.
	 */
	virtual void DeleteSelectedText ();

	/**
	 * Deletes a single character adjacent to the cursor.
	 * If bDeleteForward is true, it'll delete the character after the cursor position. Otherwise it'll delete the character in front of the cursor.
	 */
	virtual void DeleteAdjacentCharacter (bool bDeleteForward);

	/**
	 * Compares the states between the TextFieldRenderComponent and this component's selected text state.
	 * This function will update the render component's highlighting state to match this component.
	 * @param startHighlightPos The cursor position where highlighting begins. Negative if there is no highlighting.
	 * @param endHighlightPos The cursor position where highlighting ends. Negative if there is no highlighting.
	 */
	virtual void UpdateHighlightOnRenderComponent (Int startHighlightPos, Int endHighlightPos);

	/**
	 * Notifies the text render component to briefly pause the cursor's blinking behavior where
	 * The cursor will solid.
	 */
	virtual void PauseBlinker ();

	/**
	 * Moves the cursor or the HighlightEndPosition by the specified amount of characters.
	 * Negative values move the cursor towards the beginning.
	 */
	virtual void MoveCursor (Int numChars, bool bMoveHighlight);

	/**
	 * Moves the cursor or the HighlightEndPosition to the beginning of the previous word.
	 */
	virtual void JumpCursorToPrevWord (bool bMoveHighlight);

	/**
	 * Moves the cursor or the HighlightEndPosition to the beginning of the next word.
	 */
	virtual void JumpCursorToNextWord (bool bMoveHighlight);

	/**
	 * Moves the cursor to the beginning of the line.
	 */
	virtual void JumpCursorToBeginningLine (bool bMoveHighlight);

	/**
	 * Moves the cursor to the end of the line.
	 */
	virtual void JumpCursorToEndLine (bool bMoveHighlight);

	/**
	 * Moves the cursor up/down the text field relative to the current cursor position.
	 * If deltaLines is negative, then it'll move up.  Otherwise, it'll move down the Text Field.
	 * If bMoveHighlight is true, it'll move the HighlightEndPosition instead of the CursorPosition.
	 */
	virtual void JumpCursorToLine (Int deltaLines, bool bMoveHighlight);

	/**
	 * For single line text field components, this navigates the displayed text towards the beginning of the string.
	 * This function will automatically clamp to the beginning of the text.
	 * Returns the actual number of characters that were shifted.
	 */
	virtual Int ShiftDisplayedTextTowardsHeader (Int jumpAmount);

	/**
	 * For single line text field components, this navigates the displayed text towards the end of the string.
	 * This function will automatically clamp to the end of the text.
	 * Returns the actual number of characters that were shifted.
	 */
	virtual Int ShiftDisplayedTextTowardsTail (Int jumpAmount);

	virtual bool ExecuteTextFieldInput (const sf::Event& keyEvent);
	virtual bool ExecuteTextFieldText (const sf::Event& keyEvent);


	/*
	=====================
	  Event Handlers
	=====================
	*/

	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);
	virtual void HandleSingleLineHighlightDrag (Float deltaSec);
	virtual bool HandleInitContextComp (GuiEntity* menu, ContextMenuComponent* contextComp, MousePointer* mouse);
};
SD_END