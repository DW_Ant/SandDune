/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiComponent.h
  Contains base functionality for all UI Components.

  GuiComponents assume that the PlanarTransform they derive from are only drawn to a single
  RenderTarget since it doesn't make sense to have a UI element drawn on two windows, and the
  user is able to interact with either one of them.

  With this assumption, the GuiComponent often access the transform's abs position and size.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "GuiTheme.h"

SD_BEGIN
class GUI_API GuiComponent : public EntityComponent, public CopiableObjectInterface, public PlanarTransform
{
	DECLARE_CLASS(GuiComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EInputAvailability
	{
		IA_AlwaysAvailable,
		IA_OnlyVisible, //Input is only processed when the component is visible.
		IA_Disabled //Input is never processed
	};

	enum EInputEvent : unsigned char
	{
		IE_None = 0x00,
		IE_Input = 0x01, //Input pressed, Input released, Input repeat
		IE_Text = 0x02, //Text pressed, released, and repeat
		IE_MouseMove = 0x04,
		IE_MouseEvent = 0x08, //Mouse pressed, mouse released
		IE_MouseWheel = 0x10,
		IE_All = 0xFF
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, this component will automatically stylize any sub GuiComponents that matches this component's style when
	they attach themselves to this component. This only works when ApplyStyle function is called. */
	bool PropagateStyle;

protected:
	/* Determines when the input events are processed. Any component that filters out the input via availability will prevent its
	subcomponents from also processing the input event. */
	EInputAvailability InputAvailability;

	/* Determines which input events are allowed to be processed down to its components. The inner components may have inner input priority,
	but the owning component should be able to determine which events are permitted to be passed down. */
	EInputEvent AcceptedInputEvents;

	/* If true, then this component will consume mouse click and mouse wheel events when the cursor is hovering this component.
	regardless if none of its inner components consumes it. This is primarily used for components overlapping each other, and this flag
	would prevent the lesser priority component from receiving any mouse events. */
	bool bAlwaysConsumeMouseEvents;

private:
	/* Name of the style this component is using. This is updated when ApplyStyle is called. Becomes empty if a default style or no style is used. */
	DString StyleName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

protected:
	virtual bool AddComponent_Implementation (EntityComponent* newComponent) override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual void AttachTo (Entity* newOwner) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Replaces copiable properties with the given style's template that matches this class.
	 * This method should be called before editing any properties since this function will overwrite them.
	 * If the given styleName is empty, or if the style name is not found, it'll fallback to the theme's default style.
	 */
	virtual void ApplyStyle (const DString& styleName);

	/**
	 * Returns the minimum amount of space this GuiComponent needs.
	 * This is primarily used for owning components to allocate space for their components (such as a VerticalList).
	 * Most components simply return their size. Some components overrides this to allow overlap (such as dropdown components only considering the size of the top line).
	 */
	virtual Vector2 GetMinimumSize () const;

	/**
	 * Returns true if this GuiComponent is the GuiComponent closest to the owning entity within the scope of local component chain (see diagram below).
	 * Entity
	 *		OtherComponent
	 *			GuiComponent <---- Outermost
	 *			GuiComponent <---- Outermost
	 *				SubGuiComponent <---- Not outermost
	 *					SubGuiComponent <---- Innermost
	 *			GuiComponent
	 *				Component
	 *					SubGuiComponent <---- Innermost
	 *		GuiComponent <---- Outermost
	 */
	virtual bool IsOutermostGuiComponent () const;

	/**
	 * Returns true if this component does not have any Gui sub components.
	 * If bOnlyImmediateComponents is true, then it'll only consider Gui components that this component directly owns (ignores sub components of this component's sub components).
	 */
	virtual bool IsInnermostGuiComponent (bool bOnlyImmediateComponents = false) const;

	/**
	 * Returns true if this Text/Input event should be passed to this component and its subcomponents.
	 * For MouseEvents, see AcceptsMouseEvents.
	 */
	virtual bool AcceptsInputEvent (EInputEvent inputEvent, const sf::Event& evnt) const;

	/**
	 * Returns true if the mouse events should be passed to this component and its subcomponents.
	 */
	virtual bool AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const;

	/**
	 * Relays input events to the execution sequence.
	 * This run two passes:
	 * The first pass will iterate through all subcomponents to notify them about the input event. This event cannot be consumed.
	 * The second pass will iterate through all subcomponents again except this time it'll abort the loop as soon as one consumes
	 * the input event. The MouseMove event does not have two passes since that event cannot be consumed.
	 */
	bool ProcessInput (const sf::Event& evnt);
	bool ProcessText (const sf::Event& evnt);
	void ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	bool ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	bool ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Broadcasts a nonconsumable event to the subcomponents.
	 */
	void BroadcastNonconsumableInput (const sf::Event& evnt);
	void BroadcastNonconsumableText (const sf::Event& evnt);
	void BroadcastNonconsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	void BroadcastNonconsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Broadcasts a consumable event to the subcomponents where the innermost subcomponents receive priority.
	 * The first component to consume the event will prevent others from receiving it.
	 */
	bool BroadcastConsumableInput (const sf::Event& evnt);
	bool BroadcastConsumableText (const sf::Event& evnt);
	bool BroadcastConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	bool BroadcastConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Event notification that there was an input event.
	 * This event cannot be consumed, and this is invoked prior to consumable input events.
	 */
	virtual void ExecuteInput (const sf::Event& evnt);
	virtual void ExecuteText (const sf::Event& evnt);
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual void ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Event notification to respond to input events.  Innermost input components will receive input priority over outermost components.
	 * Should this function return true, then the input event will not continue to other input components.
	 */
	virtual bool ExecuteConsumableInput (const sf::Event& evnt);
	virtual bool ExecuteConsumableText (const sf::Event& evnt);
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetInputAvailability (EInputAvailability newInputAvailability);
	virtual void SetAcceptedInputEvents (EInputEvent newPermittedInputEvents);
	virtual void SetAlwaysConsumeMouseEvents (bool newAlwaysConsumeMouseEvents);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EInputAvailability GetInputAvailability () const
	{
		return InputAvailability;
	}

	inline EInputEvent GetAcceptedInputEvents () const
	{
		return AcceptedInputEvents;
	}

	inline bool IsAlwaysConsumingMouseEvents () const
	{
		return bAlwaysConsumeMouseEvents;
	}

	inline const DString& ReadStyleName () const
	{
		return StyleName;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Instantiates various internal components that makes up this component's functionality.
	 * This function assumes there is no theme/style available, and should initialize default
	 * component behavior should no UI template be available.
	 */
	virtual void InitializeComponents ();

	/**
	 * Recursively iterates through all sub GuiComponents and executes the given lambda function. If the lambda function returns true,
	 * then it will consume and prevent the other components from executing the lambda.
	 * The inner most sub GuiComponents takes priority over their owners.
	 *
	 * @param shouldStepIn - If this lambda returns true, it'll step into the subcomponents to check for lambda execution. The parent-most components call this before subcomponents.
	 * @param lambda - The function that could consume the event. This is invoked before the owning components. The function returns true as soon as this returns true.
	 * Returns true if this function should terminate the recursive chain.
	 */
	virtual bool RecursivelyExecuteInnerMostFirst (std::function<bool(GuiComponent*)> shouldStepIn, std::function<bool(GuiComponent* /*guiComp*/)> lambda);
};

DEFINE_ENUM_FUNCTIONS(GuiComponent::EInputEvent)
SD_END