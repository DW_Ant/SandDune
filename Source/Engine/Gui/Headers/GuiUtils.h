/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiUtils.h
  A collection of common utilities related to the GUI module.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "GuiComponent.h"
#include "GuiDrawLayer.h"
#include "GuiEntity.h"
#include "ScrollbarComponent.h"

SD_BEGIN
class GUI_API GuiUtils : public BaseUtils
{
	DECLARE_CLASS(GuiUtils)


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Attempts to find the GuiEntity instance from a DrawLayer being rendered directly to a window.
	 * The given component provides context to which window handle to look in. If this component resides in a scrollbar, it'll recursively search through the
	 * hierarchy until it finds a GuiEntity that doesn't reside in a ScrollbarComponent.
	 */
	template <class T>
	static T* FindEntityInSameWindowAs (GuiComponent* context)
	{
		//Need to find the Entity drawn directly on the window instead of RootEntity. We don't want the tooltips to be drawn inside scrollbars.
		GuiComponent* searchFrom = context;
		while (true)
		{
			GuiEntity* rootEntity = dynamic_cast<GuiEntity*>(searchFrom->GetRootEntity());
			if (rootEntity == nullptr)
			{
				GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to find %s from context %s. This component's root entity is not a GuiEntity."), T::SStaticClass()->ToString(), context->ToString());
				return nullptr;
			}

			if (ScrollbarComponent* owningScrollbar = rootEntity->GetScrollbarOwner())
			{
				//This Entity resides inside a scrollbar. Repeeat the search from the scrollbar's perspective.
				searchFrom = owningScrollbar;
				continue;
			}

			break;
		}

		if (searchFrom == nullptr)
		{
			return nullptr;
		}

		if (const RenderTarget* renderTarg = searchFrom->FindRootRenderTarget())
		{
			//Read all draw layers drawn to the window until we find the GuiDrawLayer with the correct Entity.
			for (const RenderTarget::SDrawLayerCamera& drawLayer : renderTarg->ReadDrawLayers())
			{
				if (GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(drawLayer.Layer))
				{
					//Search for the Gui entity
					for (GuiEntity* registeredMenu : guiLayer->ReadRegisteredMenus())
					{
						if (T* result = dynamic_cast<T*>(registeredMenu))
						{
							return result;
						}
					}
				}
			}
		}

		return nullptr;
	}

	/**
	 * Finds the GuiDrawLayer being rendered in the same window as the given MousePointer.
	 * Then it'll try to find the GuiEntity within that GuiDrawLayer.
	 * 
	 * The reason why there's an overload for the MousePointer is because the MousePointer is typically rendered directly to a window,
	 * and the MousePointer instance is passed around numerous GuiComponents for various input events, making it readably accessible.
	 */
	template<class T>
	static T* FindEntityInSameWindowAs (MousePointer* mouse)
	{
		if (mouse == nullptr)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot process FindEntityInSameWindowAs without passing a valid mouse pointer reference."));
			return nullptr;
		}

		mouse = mouse->GetRootMouse();
		CHECK(mouse != nullptr)

		Window* owningWindow = mouse->GetOwningWindowHandle();
		if (owningWindow == nullptr)
		{
			return nullptr; //Mouse is not registered to a window?
		}

		//Find the Gui draw layer associated with the window handle
		for (const RenderTarget::SDrawLayerCamera& drawLayer : owningWindow->ReadDrawLayers())
		{
			if (GuiDrawLayer* guiDrawLayer = dynamic_cast<GuiDrawLayer*>(drawLayer.Layer))
			{
				for (GuiEntity* guiEntity : guiDrawLayer->ReadRegisteredMenus())
				{
					if (T* result = dynamic_cast<T*>(guiEntity))
					{
						return result;
					}
				}
			}
		}

		return nullptr;
	}
};
SD_END