/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiUnitTester.h
  Class that'll instantiate various UI Components so that the user
  may test their functionality without external interference.

  This unit tester does not have any crash conditions.  It's
  up to the user to test the components' functionality manually.
=====================================================================
*/

#pragma once

#include "Gui.h"

#ifdef DEBUG_MODE

SD_BEGIN
class GuiComponent;

class GUI_API GuiUnitTester : public UnitTester
{
	DECLARE_CLASS(GuiUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestTreeBranchIterator (EUnitTestFlags testFlags) const;
	virtual void SpawnTestComponents (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug mode