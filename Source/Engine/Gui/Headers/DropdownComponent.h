/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DropdownComponent.h
  A component that expands to a list of objects, and the user can specify which object to select.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "GuiDataElement.h"
#include "FocusInterface.h"
#include "ListBoxComponent.h"

SD_BEGIN
class ButtonComponent;
class DropdownGuiEntity;
class FrameComponent;
class GuiEntity;
class LabelComponent;
class ScrollbarComponent;
class TextFieldComponent;

class GUI_API DropdownComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(DropdownComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SDFunction<void> OnExpanded;
	SDFunction<void> OnCollapsed;

	/* Callback to invoke whenever an option was selected where the parameter is the index of the list item. */
	SDFunction<void, DropdownComponent*, Int> OnOptionSelected;

protected:
	/* The vertical size the selected item label. Same as setting its Y size. */
	Float ItemSelectionHeight;

	/* Text to display on selected item field when no item is selected. */
	DString NoSelectedItemText;

	/* Becomes true if the list is expanded and visible. */
	bool bExpanded;

	/* If false, then the user cannot expand the Dropdown component or change its selected item. */
	bool bEnabled;

	/* If true, then the expand menu will move to the DropdownGuiEntity when revealed, allowing it to appear in the outer draw layer. For example if it resides inside a scrollbar, the
	dropdown component will appear on top of the scrollbar. */
	bool bShowExpandMenuOnTop;

	/* Timestamp when this Entity is displaying a new dropdown component options. This is to prevent collapsing the menu immediately after opening it (in seconds). */
	Float ExpandTimestamp;

	/* Minimum time that must elapse before the mouse release button becomes a drag release event. For example, it would not register the mouse up button event as a selection
	if the dropdown was recently expanded. But after this much time elapses, the user would be able to drag the mouse to the option and release to choose that option.
	This is in seconds. */
	Float DragSelectThreshold;

	/* The GuiEntity reference that's currently holding the expand menu. The components are moved to this Entity so it can be rendered in the foreground. */
	DPointer<DropdownGuiEntity> ExpandHolder;

	/* List of available options in expansion menu. */
	DPointer<ScrollbarComponent> ExpandMenuScrollbar;
	GuiEntity* ExpandMenu;
	DPointer<ListBoxComponent> ExpandMenuList;

	DPointer<ButtonComponent> ExpandButton;

	/* Texture to use for the expand button while the dropdown is collapsed. */
	DPointer<Texture> ButtonTextureCollapsed;

	/* Texture to use for the expand button while the dropdown is expanded. */
	DPointer<Texture> ButtonTextureExpanded;

	DPointer<FrameComponent> SelectedObjBackground;

	DPointer<LabelComponent> SelectedItemLabel;

	/* If it exists, this text field will steal focus when the drop down expands. Here, the user can enter keys to filter the contents of the dropdown menu.
	The text field becomes invisible when the dropdown collapses. */
	DPointer<TextFieldComponent> SearchBar;

private:
	/* If true, then the next enter release key event is ignored. */
	bool bIgnoreEnterRelease;

	/* For focused dropdowns, this is the search string the user is currently entering. */
	DString SearchString;

	/* Timestamp when the latest character was added to SearchString. */
	Float SearchStringTimestamp;

	/* Maximum time to elapse before the search string clears. */
	Float SearchStringClearTime;

	/* The index of the selected item in the Dropdown's ListBoxComponent. This is used so that the DropdownComponent can restore the selected item in the ListBoxComponent
	if the mouse event is ignored due to DragSelectThreshold. */
	Int PrevSelectIdx;

	/* Latest mouse instance to send an input event. This is used as a reference point in case the DropdownComponent is unable to find the DropdownGuiEntity.
	The DropdownGuiEntity wouldn't be found if this DropdownComponent is found inside a RenderTexture that isn't managed by a ScrollbarComponent. */
	DPointer<MousePointer> DetectedMouse;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeFocused () const override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;
	virtual Vector2 GetMinimumSize () const override;
	virtual void ExecuteInput (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Expand ();
	virtual void Collapse ();
	bool IsExpanded () const;

	/**
	 * Retrieves the height of this focus component while it's collapsed (in absolute space).
	 */
	virtual Float GetCollapsedHeight () const;

	/**
	 * Sets the selected item by index value.
	 */
	virtual void SetSelectedItemByIdx (Int itemIndex);
	virtual void SetNoSelectedItemText (const DString& newNoSelectedItemText);
	virtual void ClearList ();

	/**
	 * Returns the index value of the selected item.
	 */
	virtual Int GetSelectedItemByIdx () const;

	inline bool IsItemSelected () const
	{
		return (GetSelectedItemByIdx() != INDEX_NONE);
	}

	/**
	 * If enabled, then this component will contain a text field component that allows the user to filter and search through the dropdown's options.
	 * Otherwise, it'll destroy the search text field component.
	 */
	virtual void SetEnableSearchBar (bool bEnabled);


	/*
	=====================
	  Mutators
	=====================
	*/

	virtual void SetItemSelectionHeight (Float newItemSelectionHeight);
	virtual void SetEnabled (bool newEnabled);
	virtual void SetShowExpandMenuOnTop (bool newShowExpandMenuOnTop);
	virtual void SetDragSelectThreshold (Float newDragSelectThreshold);
	virtual void SetExpandMenu (ListBoxComponent* newExpandMenu);
	virtual void SetExpandButton (ButtonComponent* newExpandButton);
	virtual void SetButtonTextureCollapsed (Texture* newButtonTextureCollapsed);
	virtual void SetButtonTextureExpanded (Texture* newButtonTextureExpanded);
	virtual void SetSelectedObjBackground (FrameComponent* newSelectedObjBackground);
	virtual void SetSelectedItemLabel (LabelComponent* newSelectedItemLabel);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsEnabled () const
	{
		return bEnabled;
	}

	inline Float GetDragSelectThreshold () const
	{
		return DragSelectThreshold;
	}

	inline ScrollbarComponent* GetExpandMenuScrollbar () const
	{
		return ExpandMenuScrollbar.Get();
	}

	inline ListBoxComponent* GetExpandMenuList () const
	{
		return ExpandMenuList.Get();
	}

	/**
	 * Returns either the ExpandMenuScrollbar if it exists. Otherwise it'll return the ExpandMenuList.
	 */
	virtual GuiComponent* GetExpandComponent () const;

	virtual DString GetNoSelectedItemText () const;

	ButtonComponent* GetExpandButton () const;
	Texture* GetButtonTextureCollapsed () const;
	Texture* GetButtonTextureExpanded () const;
	FrameComponent* GetSelectedObjBackground () const;
	LabelComponent* GetSelectedItemLabel () const;
	TextFieldComponent* GetSearchBar () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeExpandButton ();
	virtual void InitializeSelectedObjBackground ();
	virtual void InitializeSelectedItemLabel ();
	virtual void InitializeExpandMenu ();

	/**
	 * Refreshes the options text based on what the user searched for.
	 */
	virtual void FilterExpandList ();

	/**
	 * Updates the selection text to reflect the selected item in the ExpandMenu.
	 */
	virtual void UpdateSelectionText (size_t selectedIdx);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleExpandButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleExpandItemSelected (Int selectedItemIdx);
	virtual void HandleExpandItemClicked (Int itemIdx);
	virtual bool HandleSearchEdit (const sf::Event& sfmlEvent);
	virtual void HandleSearchReturn (TextFieldComponent* textComp);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class T>
	GuiDataElement<T>* GetSelectedGuiDataElement () const
	{
		if (ExpandMenuList.IsValid())
		{
			const std::vector<Int>& selectedItems = ExpandMenuList->ReadSelectedItems();
			if (selectedItems.size() > 0)
			{
				return dynamic_cast<GuiDataElement<T>*>(ExpandMenuList->ReadList().at(selectedItems.at(0).ToUnsignedInt()));
			}
		}

		return nullptr;
	}

	template <class T>
	T GetSelectedItem () const
	{
		GuiDataElement<T>* dataElement = GetSelectedGuiDataElement<T>();
		CHECK(dataElement != nullptr)
		return dataElement->Data;
	}

	/**
	 * Attempts to select the data item within the table that matches the specified data element.
	 */
	template <class T>
	void SetSelectedItem (T targetData)
	{
		if (ExpandMenuList.IsValid())
		{
			for (size_t i = 0; i < ExpandMenuList->ReadList().size(); ++i)
			{
				if (GuiDataElement<T>* item = dynamic_cast<GuiDataElement<T>*>(ExpandMenuList->ReadList().at(i)))
				{
					if (item->Data == targetData)
					{
						SetSelectedItemByIdx(i);
						break;
					}
				}
			}
		}
	}
};
SD_END