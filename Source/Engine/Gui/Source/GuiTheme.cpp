/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiTheme.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "CheckboxComponent.h"
#include "ContextMenuComponent.h"
#include "DropdownComponent.h"
#include "DynamicListMenu.h"
#include "FrameComponent.h"
#include "GuiComponent.h"
#include "GuiEngineComponent.h"
#include "GuiTheme.h"
#include "LabelComponent.h"
#include "ListBoxComponent.h"
#include "ScrollbarComponent.h"
#include "SingleSpriteButtonState.h"
#include "TextFieldComponent.h"
#include "TextFieldRenderComponent.h"
#include "TooltipComponent.h"
#include "TooltipGuiEntity.h"

IMPLEMENT_CLASS(SD::GuiTheme, SD::Object)
SD_BEGIN

const DString GuiTheme::DEFAULT_STYLE_NAME = TXT("DefaultStyle");

GuiTheme::SStyleInfo::SStyleInfo () :
	Name(DString::EmptyString)
{
	//Noop
}

void GuiTheme::InitProps ()
{
	Super::InitProps();

	TooltipClass = TooltipGuiEntity::SStaticClass();
	DefaultContextMenuClass = DynamicListMenu::SStaticClass();
	DefaultStyleIdx = 0;
}

void GuiTheme::Destroyed ()
{
	while (Styles.size() > 0)
	{
		for (GuiComponent* uiTemplate : Styles.at(0).Templates)
		{
			uiTemplate->Destroy();
		}

		Styles.erase(Styles.begin());
	}

	Super::Destroyed();
}

GuiTheme::SStyleInfo GuiTheme::CreateStyleFrom (const GuiTheme::SStyleInfo& copyObj)
{
	SStyleInfo newStyle;

	newStyle.Name = copyObj.Name;
	for (GuiComponent* uiTemplate : copyObj.Templates)
	{
		GuiComponent* cpyTemplate = uiTemplate->CreateObjectOfMatchingClass();
		CHECK(cpyTemplate != nullptr)
		cpyTemplate->CopyPropertiesFrom(uiTemplate);
		newStyle.Templates.push_back(cpyTemplate);
	}

	return newStyle;
}

void GuiTheme::InitializeTheme ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Mouse Pointer Icons
	TextPointer = localTexturePool->EditTexture(HashedString("Engine.Input.CursorText"));
	ResizePointerVertical = localTexturePool->EditTexture(HashedString("Engine.Input.CursorResizeVertical"));
	ResizePointerHorizontal = localTexturePool->EditTexture(HashedString("Engine.Input.CursorResizeHorizontal"));
	ResizePointerDiagonalTopLeft = localTexturePool->EditTexture(HashedString("Engine.Input.CursorResizeTopLeft"));
	ResizePointerDiagonalBottomLeft = localTexturePool->EditTexture(HashedString("Engine.Input.CursorResizeBottomLeft"));
	ScrollbarPointer = localTexturePool->EditTexture(HashedString("Engine.Input.CursorScrollbarPan"));

	//Frame component textures
	FrameBorder = localTexturePool->EditTexture(HashedString("Engine.Gui.FrameBorder"));
	FrameFill = localTexturePool->EditTexture(HashedString("Engine.Gui.FrameComponentFill"));

	CheckboxTexture = localTexturePool->EditTexture(HashedString("Engine.Gui.Checkbox"));

	//Button component textures
	ButtonBorder = localTexturePool->EditTexture(HashedString("Engine.Gui.FrameBorder"));
	ButtonFill = localTexturePool->EditTexture(HashedString("Engine.Gui.FrameComponentFill"));
	ButtonSingleSpriteBackground = localTexturePool->EditTexture(HashedString("Engine.Gui.TextureTestStates"));

	//Scrollbar component textures
	ScrollbarUpButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollUpButton"));
	ScrollbarDownButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollDownButton"));
	ScrollbarLeftButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollLeftButton"));
	ScrollbarRightButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollRightButton"));
	ScrollbarZoomButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollZoomButton"));
	ScrollbarMiddleMouseScrollAnchor = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMiddleMouseSprite"));
	ScrollbarMouseAnchorStationary = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorStationary"));
	ScrollbarMouseAnchorPanUp = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanUp"));
	ScrollbarMouseAnchorPanUpRight = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanUpRight"));
	ScrollbarMouseAnchorPanRight = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanRight"));
	ScrollbarMouseAnchorPanDownRight = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanDownRight"));
	ScrollbarMouseAnchorPanDown = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanDown"));
	ScrollbarMouseAnchorPanDownLeft = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanDownLeft"));
	ScrollbarMouseAnchorPanLeft = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanLeft"));
	ScrollbarMouseAnchorPanUpLeft = localTexturePool->EditTexture(HashedString("Engine.Gui.ScrollbarMouseAnchorPanUpLeft"));

	//Dropdown component textures
	DropdownCollapsedButton = localTexturePool->EditTexture(HashedString("Engine.Gui.CollapsedButton"));
	DropdownExpandedButton = localTexturePool->EditTexture(HashedString("Engine.Gui.ExpandedButton"));

	//Tooltip textures
	TooltipBackground = localTexturePool->EditTexture(HashedString("Engine.Gui.TooltipBackground"));

	//Tree list component textures
	TreeListAddButton = localTexturePool->EditTexture(HashedString("Engine.Gui.TreeListAddButton"));
	TreeListSubtractButton = localTexturePool->EditTexture(HashedString("Engine.Gui.TreeListSubtractButton"));

	GuiFont = FontPool::FindFontPool()->GetDefaultFont();
}

void GuiTheme::InitializeStyles ()
{
	InitializeDefaultStyle();
}

const GuiComponent* GuiTheme::FindTemplate (const DString& styleName, const DClass* templateClass) const
{
	const SStyleInfo* style = FindStyle(styleName);
	if (style != nullptr)
	{
		return FindTemplate(style, templateClass);
	}

	return nullptr;
}

const GuiComponent* GuiTheme::FindTemplate (const SStyleInfo* style, const DClass* templateClass)
{
	for (GuiComponent* uiTemplate : style->Templates)
	{
		if (uiTemplate->StaticClass() == templateClass)
		{
			return uiTemplate;
		}
	}

	return nullptr;
}

GuiComponent* GuiTheme::FindTemplate (SStyleInfo* style, const DClass* templateClass)
{
	for (GuiComponent* uiTemplate : style->Templates)
	{
		if (uiTemplate->StaticClass() == templateClass)
		{
			return uiTemplate;
		}
	}

	return nullptr;
}

const GuiTheme::SStyleInfo* GuiTheme::FindStyle (const DString& styleName) const
{
	for (UINT_TYPE i = 0; i < Styles.size(); ++i)
	{
		if (Styles.at(i).Name == styleName)
		{
			return &Styles.at(i);
		}
	}

	GuiLog.Log(LogCategory::LL_Warning, TXT("The Gui Theme does not have a style named %s."), styleName);
	return nullptr;
}

void GuiTheme::SetDefaultStyle (const DString& styleName)
{
	for (size_t i = 0; i < Styles.size(); ++i)
	{
		if (Styles.at(i).Name == styleName)
		{
			DefaultStyleIdx = Int(i);
			return;
		}
	}

	DefaultStyleIdx = INT_INDEX_NONE;
}

GuiTheme* GuiTheme::GetGuiTheme ()
{
	GuiEngineComponent* localGuiEngine = GuiEngineComponent::Find();
	if (localGuiEngine != nullptr)
	{
		return localGuiEngine->GetGuiTheme();
	}

	return nullptr;
}

const GuiTheme::SStyleInfo* GuiTheme::GetDefaultStyle () const
{
	if (ContainerUtils::IsValidIndex(Styles, DefaultStyleIdx))
	{
		return &Styles.at(DefaultStyleIdx.ToUnsignedInt());
	}

	return nullptr;
}

void GuiTheme::InitializeDefaultStyle ()
{
	SStyleInfo defaultStyle;
	defaultStyle.Name = DEFAULT_STYLE_NAME;

	//ButtonComponent
	{
		ButtonComponent* buttonTemplate = ButtonComponent::CreateObject();
		buttonTemplate->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

		FrameComponent* buttonBackground = buttonTemplate->GetBackground();
		if (buttonBackground != nullptr)
		{
			if (BorderRenderComponent* borderComp = buttonBackground->GetBorderComp())
			{
				borderComp->Destroy();
			}

			buttonBackground->SetBorderThickness(0.f);
			buttonBackground->SetLockedFrame(true);
			buttonBackground->SetCenterTexture(ButtonSingleSpriteBackground.Get());
		}

		defaultStyle.Templates.push_back(buttonTemplate);
	}

	//CheckboxComponent
	{
		CheckboxComponent* checkboxTemplate = CheckboxComponent::CreateObject();
		checkboxTemplate->SetCheckboxRightSide(true);

		LabelComponent* checkboxCaption = checkboxTemplate->GetCaptionComponent();
		if (checkboxCaption != nullptr)
		{
			checkboxCaption->SetAutoRefresh(false);
			checkboxCaption->SetFont(GuiFont.Get());
			checkboxCaption->SetCharacterSize(16);
			checkboxCaption->SetWrapText(true);
			checkboxCaption->SetClampText(true);
			checkboxCaption->SetVerticalAlignment(LabelComponent::VA_Center);
			checkboxCaption->SetHorizontalAlignment(LabelComponent::HA_Left);
			checkboxCaption->SetAutoRefresh(true);
		}

		FrameComponent* checkboxSprite = checkboxTemplate->GetCheckboxSprite();
		if (checkboxSprite != nullptr)
		{
			if (BorderRenderComponent* borderComp = checkboxSprite->GetBorderComp())
			{
				borderComp->Destroy();
			}

			checkboxSprite->SetBorderThickness(0.f);
			checkboxSprite->SetLockedFrame(true);
			checkboxSprite->SetCenterTexture(CheckboxTexture.Get());
		}

		defaultStyle.Templates.push_back(checkboxTemplate);
	}

	//ContextMenuComponent
	{
		ContextMenuComponent* contextMenuTemplate = ContextMenuComponent::CreateObject();
		contextMenuTemplate->SetRevealDirection(ContextMenuComponent::RD_BottomRight);
		contextMenuTemplate->SetRevealMenuButton(sf::Mouse::Right);
		defaultStyle.Templates.push_back(contextMenuTemplate);
	}

	//DropdownComponent
	{
		DropdownComponent* dropdownTemplate = DropdownComponent::CreateObject();
		if (dropdownTemplate->GetSelectedObjBackground() != nullptr)
		{
			dropdownTemplate->GetSelectedObjBackground()->SetBorderThickness(2.f);
		}

		defaultStyle.Templates.push_back(dropdownTemplate);
	}

	//FrameComponent
	{
		FrameComponent* frameTemplate = FrameComponent::CreateObject();
		if (BorderRenderComponent* borderComp = frameTemplate->GetBorderComp())
		{
			borderComp->SetBorderTexture(FrameBorder.Get());
		}

		frameTemplate->SetCenterTexture(FrameFill.Get());
		frameTemplate->SetBorderThickness(8.f);
		frameTemplate->SetLockedFrame(true);
		defaultStyle.Templates.push_back(frameTemplate);
	}

	//LabelComponent
	{
		LabelComponent* labelTemplate = LabelComponent::CreateObject();
		labelTemplate->SetAutoRefresh(false);
		labelTemplate->SetFont(GuiFont.Get());
		labelTemplate->SetCharacterSize(16);
		labelTemplate->SetWrapText(true);
		labelTemplate->SetClampText(true);
		labelTemplate->SetLineSpacing(0.f);
		labelTemplate->SetVerticalAlignment(LabelComponent::VA_Top);
		labelTemplate->SetHorizontalAlignment(LabelComponent::HA_Left);
		labelTemplate->SetAutoRefresh(true);

		TextRenderComponent* labelRender = labelTemplate->GetRenderComponent();
		if (labelRender != nullptr)
		{
			labelRender->SetTextShader(nullptr);
		}

		defaultStyle.Templates.push_back(labelTemplate);
	}

	//ListBoxComponent
	{
		ListBoxComponent* listTemplate = ListBoxComponent::CreateObject();
		listTemplate->SetMaxNumSelected(0);
		listTemplate->SetAutoDeselect(false);
		listTemplate->SetReadOnly(false);
		listTemplate->SetSelectionColor(Color(0x666666));
		listTemplate->SetHoverColor(Color(0x1db9b6));

		FrameComponent* listBackground = listTemplate->GetBackground();
		if (listBackground == nullptr)
		{
			listBackground = FrameComponent::CreateObject();
			listTemplate->SetBackground(listBackground);
		}

		if (BorderRenderComponent* borderComp = listBackground->GetBorderComp())
		{
			borderComp->SetBorderTexture(FrameBorder.Get());
		}

		listBackground->SetCenterTexture(FrameFill.Get());
		listBackground->SetLockedFrame(true);
		listBackground->SetBorderThickness(2.f);

		LabelComponent* listText = listTemplate->GetItemListText();
		if (listText == nullptr)
		{
			listText = LabelComponent::CreateObject();
			listTemplate->SetItemListText(listText);
		}

		defaultStyle.Templates.push_back(listTemplate);
	}

	//ScrollbarComponent
	{
		ScrollbarComponent* scrollbarTemplate = ScrollbarComponent::CreateObject();

		//use defaults
		defaultStyle.Templates.push_back(scrollbarTemplate);
	}

	//TextFieldComponent
	{
		TextFieldComponent* textFieldTemplate = TextFieldComponent::CreateObject();

		textFieldTemplate->MaxNumCharacters = -1;
		textFieldTemplate->DragScrollRate = 0.2f;
		textFieldTemplate->bAutoSelect = true;
		textFieldTemplate->SetEditable(true);
		textFieldTemplate->SetSelectable(true);

		textFieldTemplate->SetAutoRefresh(false);
		textFieldTemplate->SetFont(GuiFont.Get());
		textFieldTemplate->SetCharacterSize(16);
		textFieldTemplate->SetWrapText(true);
		textFieldTemplate->SetClampText(true);
		textFieldTemplate->SetLineSpacing(0.f);
		textFieldTemplate->SetVerticalAlignment(LabelComponent::VA_Top);
		textFieldTemplate->SetHorizontalAlignment(LabelComponent::HA_Left);
		textFieldTemplate->SetAutoRefresh(true);

		if (TextFieldRenderComponent* textRender = dynamic_cast<TextFieldRenderComponent*>(textFieldTemplate->GetRenderComponent()))
		{
			textRender->SetHighlightColor(Color(15, 60, 110, 175));
		}

		defaultStyle.Templates.push_back(textFieldTemplate);
	}

	//TooltipComponent
	{
		TooltipComponent* tooltipTemplate = TooltipComponent::CreateObject();
		tooltipTemplate->RevealDelay = 0.75f;
		defaultStyle.Templates.push_back(tooltipTemplate);
	}

#if 0
	//TreeListComponent
	{
		TreeListComponent* treeTemplate = TreeListComponent::CreateObject();
		treeTemplate->BrowseJumpAmount = 10;
		treeTemplate->IndentSize = 14.f;
		treeTemplate->BranchHeight = 16.f;
		treeTemplate->ConnectionThickness = 4.f;
		treeTemplate->ConnectionColor = sf::Color::Black;

		treeTemplate->SetExpandButtonTexture(TreeListAddButton.Get());
		treeTemplate->SetCollapseButtonTexture(TreeListSubtractButton.Get());

		ColorRenderComponent* selectedColor = treeTemplate->GetSelectedBar();
		if (selectedColor == nullptr)
		{
			selectedColor = ColorRenderComponent::CreateObject();
			treeTemplate->SetSelectedBar(selectedColor);
		}

		selectedColor->SolidColor = sf::Color::Cyan;

		ColorRenderComponent* hoverColor = treeTemplate->GetHoverBar();
		if (hoverColor == nullptr)
		{
			hoverColor = ColorRenderComponent::CreateObject();
			treeTemplate->SetHoverBar(hoverColor);
		}

		hoverColor->SolidColor = Color(200, 200, 200, 96);

		ScrollbarComponent_Deprecated* treeScrollbar = treeTemplate->GetListScrollbar();
		if (treeScrollbar == nullptr)
		{
			treeScrollbar = ScrollbarComponent_Deprecated::CreateObject();
			treeTemplate->SetListScrollbar(treeScrollbar);
		}

		treeScrollbar->SetHideWhenInsufficientScrollPos(true);

		defaultStyle.Templates.push_back(treeTemplate);
	}
#endif

	Styles.push_back(defaultStyle);
}
SD_END