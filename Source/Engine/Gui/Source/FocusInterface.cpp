/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FocusInterface.cpp
=====================================================================
*/

#include "FocusInterface.h"

SD_BEGIN
bool FocusInterface::CanBeFocused () const
{
	return bCanBeFocused;
}

bool FocusInterface::CanTabOut () const
{
	return true;
}

void FocusInterface::GainFocus ()
{
	FocusInterface_bHasFocus = true;
}

void FocusInterface::LoseFocus ()
{
	FocusInterface_bHasFocus = false;
}

void FocusInterface::SetCanBeFocused (bool bNewCanBeFocused)
{
	bCanBeFocused = bNewCanBeFocused;
}

bool FocusInterface::HasFocus () const
{
	return FocusInterface_bHasFocus;
}
SD_END