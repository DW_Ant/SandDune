/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContextMenuComponent.cpp
=====================================================================
*/

#include "ContextMenuComponent.h"
#include "ContextMenuContainer.h"
#include "GuiEngineComponent.h"
#include "GuiEntity.h"
#include "GuiTheme.h"
#include "GuiUtils.h"

IMPLEMENT_CLASS(SD::ContextMenuComponent, SD::GuiComponent)
SD_BEGIN
void ContextMenuComponent::InitProps ()
{
	Super::InitProps();

	ContextClass = nullptr;
	RevealDirection = RD_BottomRight;
	RevealMenuButton = sf::Mouse::Right;
}

void ContextMenuComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const ContextMenuComponent* contextTemplate = dynamic_cast<const ContextMenuComponent*>(objTemplate))
	{
		ContextClass = contextTemplate->ContextClass;
		RevealDirection = contextTemplate->RevealDirection;
		RevealMenuButton = contextTemplate->RevealMenuButton;
	}
}

bool ContextMenuComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (eventType == sf::Event::MouseButtonReleased && sfmlEvent.button == RevealMenuButton)
	{
		if (IsWithinBounds(mouse->ReadCachedAbsPosition()))
		{
			RevealContextMenu(mouse);
			return true;
		}
	}

	return false;
}

void ContextMenuComponent::Destroyed ()
{
	if (RevealedMenu != nullptr)
	{
		RevealedMenu->Destroy();
	}

	Super::Destroyed();
}

Vector2 ContextMenuComponent::CalcRevealLocation (const Vector2& contextMenuSize, const Vector2& revealSpot, const Vector2& containerSize) const
{
	Vector2 topLeftPos(revealSpot); //If this marks the top left corner, then the reveal direction relative to the mouse is bottom right.

	//If it should switch from right direction to left direction.
	if ((RevealDirection & ContextMenuComponent::RD_RightBit) == 0 ||
		topLeftPos.X + contextMenuSize.X >= containerSize.X)
	{
		if (topLeftPos.X - contextMenuSize.X >= 0.f)
		{
			topLeftPos.X -= contextMenuSize.X;
		}
	}

	//If it should switch from bottom direction to top direction.
	if ((RevealDirection & ContextMenuComponent::RD_TopBit) > 0 ||
		topLeftPos.Y + contextMenuSize.Y >= containerSize.Y)
	{
		if (topLeftPos.Y > contextMenuSize.Y)
		{
			topLeftPos.Y -= contextMenuSize.Y;
		}
	}

	//If either side doesn't fit, fallback to left corner
	if (topLeftPos.X < 0 || topLeftPos.X + contextMenuSize.X >= containerSize.X)
	{
		topLeftPos.X = 0.f;
	}

	if (topLeftPos.Y < 0 || topLeftPos.Y + contextMenuSize.Y >= containerSize.Y)
	{
		topLeftPos.Y = 0.f;
	}

	return topLeftPos;
}

bool ContextMenuComponent::IsRevealed () const
{
	return (RevealedMenu != nullptr);
}

void ContextMenuComponent::RevealContextMenu (MousePointer* mouse)
{
	const DClass* desiredClass = ContextClass;
	if (desiredClass != nullptr && !desiredClass->IsA(GuiEntity::SStaticClass()))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid context menu class. Only GuiEntities are supported as context menus."));
		desiredClass = nullptr; //fallback to the GuiTheme's context menu
	}

	if (desiredClass == nullptr)
	{
		GuiEngineComponent* localGuiEngine = GuiEngineComponent::Find();
		CHECK(localGuiEngine != nullptr)
		if (GuiTheme* localTheme = localGuiEngine->GetGuiTheme())
		{
			desiredClass = localTheme->DefaultContextMenuClass;
			if (desiredClass != nullptr && !desiredClass->IsA(GuiEntity::SStaticClass()))
			{
				GuiLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid context menu class in the GuiTheme. Only GuiEntities are supported as context menus."));
				desiredClass = nullptr;
			}
		}
	}

	if (desiredClass != nullptr)
	{
		ContextMenuContainer* container = GuiUtils::FindEntityInSameWindowAs<ContextMenuContainer>(this);
		if (container == nullptr)
		{
			container = GuiUtils::FindEntityInSameWindowAs<ContextMenuContainer>(mouse);
		}

		if (container != nullptr)
		{
			GuiEntity* contextMenuInstance = container->InstantiateMenu(desiredClass);
			if (contextMenuInstance != nullptr)
			{
				if (OnInitializeMenu.IsBounded())
				{
					bool bSuccess = OnInitializeMenu(contextMenuInstance, this, mouse);
					if (!bSuccess)
					{
						container->RemoveMenuInstance();
						return;
					}
				}

				RevealedMenu = contextMenuInstance;
				container->RevealContextMenu(this, mouse);
			}
		}
	}
}

void ContextMenuComponent::SetContextClass (const DClass* newContextClass)
{
	ContextClass = newContextClass;
}

void ContextMenuComponent::SetRevealDirection (ERevealDirection newRevealDirection)
{
	RevealDirection = newRevealDirection;
}

void ContextMenuComponent::SetRevealMenuButton (sf::Mouse::Button newRevealMenuButton)
{
	RevealMenuButton = newRevealMenuButton;
}
SD_END