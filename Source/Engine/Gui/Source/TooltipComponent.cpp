/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TooltipComponent.cpp
=====================================================================
*/

#include "GuiUtils.h"
#include "TooltipComponent.h"
#include "TooltipGuiEntity.h"

IMPLEMENT_CLASS(SD::TooltipComponent, SD::GuiComponent)
SD_BEGIN

void TooltipComponent::InitProps ()
{
	Super::InitProps();

	RevealDelay = 0.3f;
	HideMoveThreshold = 16.f;
	ShowMoveThreshold = 16.f;
	bTooltipBlocker = false;

	TooltipText = DString::EmptyString;
}

void TooltipComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const TooltipComponent* tooltipTemplate = dynamic_cast<const TooltipComponent*>(objTemplate))
	{
		RevealDelay = tooltipTemplate->RevealDelay;
		HideMoveThreshold = tooltipTemplate->HideMoveThreshold;
		ShowMoveThreshold = tooltipTemplate->ShowMoveThreshold;
		bTooltipBlocker = tooltipTemplate->bTooltipBlocker;
		SetTooltipText(tooltipTemplate->TooltipText);
	}
}

void TooltipComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (TooltipText.IsEmpty())
	{
		return;
	}

	if (TooltipEntity == nullptr)
	{
		TooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this);

		if (TooltipEntity == nullptr)
		{
			//Entity is not found. Try the mouse pointer next since the mouse is almost always rendered at window-level.
			TooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(mouse);
		}
	}

	if (TooltipEntity.IsValid())
	{
		bool withinBounds = IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y));
		TooltipEntity->ProcessTooltipMouseMove(mouse, withinBounds, this);
	}
}

void TooltipComponent::HideTooltip ()
{
	if (TooltipEntity != nullptr && TooltipEntity->GetTooltipOwner() == this)
	{
		TooltipEntity->HideTooltip();
	}
}

void TooltipComponent::SetTooltipText (const DString& newTooltipText)
{
	TooltipText = newTooltipText;
}
SD_END