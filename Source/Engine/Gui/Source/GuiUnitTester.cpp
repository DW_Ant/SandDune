/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiUnitTester.cpp
=====================================================================
*/

#include "GuiDataElement.h"
#include "GuiTester.h"
#include "GuiUnitTester.h"
#include "TreeBranchIterator.h"
#include "TreeListComponent.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::GuiUnitTester, SD::UnitTester)
SD_BEGIN

bool GuiUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult = TestTreeBranchIterator(testFlags);
	}

	if (bResult && (testFlags & UTF_Manual) > 0 && (testFlags & UTF_NeverFails) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		SpawnTestComponents(testFlags);
	}

	return bResult;
}

bool GuiUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	if (!Super::MetRequirements(completedTests))
	{
		return false;
	}

	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (dynamic_cast<const GraphicsUnitTester*>(completedTests.at(i)) != nullptr)
		{
			return true;
		}
	}

	return false;
}

bool GuiUnitTester::TestTreeBranchIterator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Tree Branch Iterator"));

	//Populate the expected order of numbers the iterator should find
	std::vector<Int> iteratorOrder;
	iteratorOrder.push_back(32);
	iteratorOrder.push_back(12);
	iteratorOrder.push_back(1);
	iteratorOrder.push_back(6);
	iteratorOrder.push_back(16);
	iteratorOrder.push_back(28);
	iteratorOrder.push_back(67);
	iteratorOrder.push_back(33);
	iteratorOrder.push_back(47);
	iteratorOrder.push_back(39);
	iteratorOrder.push_back(38);
	iteratorOrder.push_back(59);
	iteratorOrder.push_back(55);
	iteratorOrder.push_back(84);
	iteratorOrder.push_back(80);
	iteratorOrder.push_back(97);

	//Construct data branch
	std::vector<TreeListComponent::SDataBranch> branches;
	for (UINT_TYPE i = 0; i < iteratorOrder.size(); i++)
	{
		branches.push_back(TreeListComponent::SDataBranch(new GuiDataElement<Int>(iteratorOrder.at(i), iteratorOrder.at(i).ToString())));
		branches.at(branches.size() - 1).bExpanded = true;
	}

	//link the branches in this structure
	/*
	[0] 32
	+----[1] 12
	|    +----[2] 1
	|    |    +----[3] 6
	|    |
	|	 +----[4] 16
	|	      +----[5] 28
	+----[6] 67
		    +----[7] 33
		    |    +----[8] 47
	        |	       +----[9] 39
			|		   |    +----[10] 38
			|          |
			|  	       +----[11] 59
			|		        +----[12] 55
			|
			+----[13] 84
			    +----[14] 80
				+----[15] 97
	*/
	TreeListComponent::SDataBranch::LinkBranches(branches.at(0), branches.at(1));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(1), branches.at(2));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(2), branches.at(3));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(1), branches.at(4));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(4), branches.at(5));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(0), branches.at(6));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(6), branches.at(7));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(7), branches.at(8));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(8), branches.at(9));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(9), branches.at(10));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(8), branches.at(11));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(11), branches.at(12));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(6), branches.at(13));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(13), branches.at(14));
	TreeListComponent::SDataBranch::LinkBranches(branches.at(13), branches.at(15));

	Int branchIdx = 0;
	bool bAborted = false;
	for (TreeBranchIterator<const TreeListComponent::SDataBranch> iter(&branches.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
	{
		TestLog(testFlags, TXT("Tree Branch Iterator found branch:  %s"), iter.GetSelectedBranch()->Data->GetLabelText());
		GuiDataElement<Int>* curBranchData = dynamic_cast<GuiDataElement<Int>*>(iter.GetSelectedBranch()->Data);
		if (curBranchData == nullptr)
		{
			UnitTestError(testFlags, TXT("Tree Branch Iterator test failed.  Failed to obtain an Int from branch data."));
			bAborted = true;
			break;
		}

		if (iteratorOrder.at(branchIdx.ToUnsignedInt()) != curBranchData->Data)
		{
			UnitTestError(testFlags, TXT("Tree Branch Iterator test failed.  Failed to find data in expected order.  It expected to retrieve %s.  Instead it found %s."), iteratorOrder.at(branchIdx.ToUnsignedInt()), (curBranchData->Data));
			bAborted = true;
			break;
		}

		branchIdx++;
	}

	if (!bAborted)
	{
		TestLog(testFlags, TXT("Collapsing odd numbered branches, and the iterator should skip over odd number branches' children."));
		std::vector<Int> collapsedOrder;
		collapsedOrder.push_back(32);
		collapsedOrder.push_back(12);
		collapsedOrder.push_back(1);
		collapsedOrder.push_back(16);
		collapsedOrder.push_back(28);
		collapsedOrder.push_back(67);

		for (UINT_TYPE i = 0; i < branches.size(); i++)
		{
			GuiDataElement<Int>* curBranch = dynamic_cast<GuiDataElement<Int>*>(branches.at(i).Data);
			if (curBranch->Data.IsOdd())
			{
				branches.at(i).bExpanded = false;
			}
		}

		branchIdx = 0;
		for (TreeBranchIterator<const TreeListComponent::SDataBranch> iter(&branches.at(0)); iter.GetSelectedBranch() != nullptr; iter.SelectNextVisibleBranch())
		{
			TestLog(testFlags, TXT("Tree Branch Iterator found visible branch:  %s"), iter.GetSelectedBranch()->Data->GetLabelText());
			GuiDataElement<Int>* curBranchData = dynamic_cast<GuiDataElement<Int>*>(iter.GetSelectedBranch()->Data);
			if (collapsedOrder.at(branchIdx.ToUnsignedInt()) != curBranchData->Data)
			{
				UnitTestError(testFlags, TXT("Tree Branch Iterator test failed.  Failed to find data in expected collapsed order.  It expected to retrieve %s.  Instead it found %s."), collapsedOrder.at(branchIdx.ToUnsignedInt()), (curBranchData->Data));
				bAborted = true;
				break;
			}

			branchIdx++;
		}
	}

	for (UINT_TYPE i = 0; i < branches.size(); i++)
	{
		delete branches.at(i).Data;
	}
	branches.clear();

	if (bAborted)
	{
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Tree Branch Iterator"));
	return true;
}

void GuiUnitTester::SpawnTestComponents (EUnitTestFlags testFlags) const
{
	GuiTester* tester = GuiTester::CreateObject(); //The GuiTester is self contained, and will destroy itself based on user input.
	CHECK(tester != nullptr)

	tester->TestFlags = testFlags;
	tester->BeginUnitTest();
}
SD_END

#endif