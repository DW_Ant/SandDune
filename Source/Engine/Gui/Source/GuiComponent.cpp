/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiComponent.cpp
=====================================================================
*/

#include "GuiComponent.h"
#include "GuiEngineComponent.h"

IMPLEMENT_CLASS(SD::GuiComponent, SD::EntityComponent)
SD_BEGIN

void GuiComponent::InitProps ()
{
	Super::InitProps();

	PropagateStyle = true;
	InputAvailability = IA_OnlyVisible;
	AcceptedInputEvents = IE_All;
	bAlwaysConsumeMouseEvents = false;

	StyleName = TXT("Undefined");
}

void GuiComponent::BeginObject ()
{
	Super::BeginObject();

	InitializeComponents();
}

CopiableObjectInterface* GuiComponent::CreateCopiableInstanceOfMatchingType () const
{
	if (const DClass* duneClass = StaticClass())
	{
		if (const GuiComponent* cdo = dynamic_cast<const GuiComponent*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void GuiComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	CHECK(objTemplate != this) //It's pointless for a GuiComponent to copy properties from self.

	const GuiComponent* uiTemplate = dynamic_cast<const GuiComponent*>(objTemplate);
	if (uiTemplate != nullptr)
	{
		CopyPlanarTransform(uiTemplate); //Copy transform data
		SetInputAvailability(uiTemplate->InputAvailability);
		SetAlwaysConsumeMouseEvents(uiTemplate->bAlwaysConsumeMouseEvents);
	}
}

bool GuiComponent::AddComponent_Implementation (EntityComponent* newComponent)
{
	bool success = Super::AddComponent_Implementation(newComponent);
	if (success && PropagateStyle)
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(newComponent);
		if (guiComp != nullptr)
		{
			guiComp->ApplyStyle(StyleName);
		}
	}

	return success;
}

bool GuiComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	//Owner must implement the PlanarTransform.
	return (Super::CanBeAttachedTo(ownerCandidate) && dynamic_cast<const PlanarTransform*>(ownerCandidate) != nullptr);
}

void GuiComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	PlanarTransform* ownerTransform = dynamic_cast<PlanarTransform*>(newOwner);
	CHECK(ownerTransform != nullptr) //CanBeAttachedTo should have prevented this.
	SetRelativeTo(ownerTransform);
}

void GuiComponent::ApplyStyle (const DString& styleName)
{
	if (StyleName.Compare(styleName, DString::CC_CaseSensitive) == 0)
	{
		return;
	}

	StyleName = styleName;
	if (PropagateStyle)
	{
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()))
			{
				guiComp->ApplyStyle(StyleName);
			}
		}
	}

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)

	GuiTheme* curTheme = guiEngine->GetGuiTheme();
	if (curTheme == nullptr)
	{
		return;
	}

	const GuiTheme::SStyleInfo* defaultStyle = curTheme->GetDefaultStyle();
	const GuiTheme::SStyleInfo* style = defaultStyle;
	if (!styleName.IsEmpty())
	{
		style = curTheme->FindStyle(styleName);
		if (style == nullptr) //Fallback to default
		{
			style = defaultStyle;
			if (style != nullptr)
			{
				StyleName = defaultStyle->Name;
			}
		}
	}

	if (style != nullptr)
	{
		const GuiComponent* uiTemplate = GuiTheme::FindTemplate(style, StaticClass());
		if (uiTemplate != nullptr)
		{
			CopyPropertiesFrom(uiTemplate);
		}
	}
}

Vector2 GuiComponent::GetMinimumSize () const
{
	return ReadSize();
}

bool GuiComponent::IsOutermostGuiComponent () const
{
	EntityComponent* nextOwner = dynamic_cast<EntityComponent*>(GetOwner());
	while (nextOwner != nullptr)
	{
		if (dynamic_cast<GuiComponent*>(nextOwner) != nullptr)
		{
			return false; //Another GuiComponent is closer to the entity than this component
		}

		nextOwner = dynamic_cast<EntityComponent*>(nextOwner->GetOwner());
	}

	return true; //No owning components are Gui Components
}

bool GuiComponent::IsInnermostGuiComponent (bool bOnlyImmediateComponents) const
{
	for (ComponentIterator iter(this, !bOnlyImmediateComponents); iter.GetSelectedComponent() != nullptr; iter++)
	{
		if (dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()) != nullptr)
		{
			return false;
		}
	}

	return true;
}

bool GuiComponent::AcceptsInputEvent (EInputEvent inputEvent, const sf::Event& evnt) const
{
	if ((AcceptedInputEvents & inputEvent) == 0)
	{
		return false;
	}

	if (InputAvailability == IA_OnlyVisible)
	{
		return IsVisible();
	}

	return (InputAvailability == IA_AlwaysAvailable);
}

bool GuiComponent::AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	if ((AcceptedInputEvents & inputEvent) == 0)
	{
		return false;
	}

	if (InputAvailability == IA_OnlyVisible)
	{
		return IsVisible();
	}

	return (InputAvailability == IA_AlwaysAvailable);
}

bool GuiComponent::ProcessInput (const sf::Event& evnt)
{
	if (!AcceptsInputEvent(IE_Input, evnt))
	{
		return false;
	}

	BroadcastNonconsumableInput(evnt);
	return BroadcastConsumableInput(evnt);
}

bool GuiComponent::ProcessText (const sf::Event& evnt)
{
	if (!AcceptsInputEvent(IE_Text, evnt))
	{
		return false;
	}

	BroadcastNonconsumableText(evnt);
	return BroadcastConsumableText(evnt);
}

void GuiComponent::ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (!AcceptsMouseEvents(IE_MouseMove, sfmlEvent.x, sfmlEvent.y))
	{
		return;
	}

	//Invoke the inner most children first since those are drawn over the parent (they have input priority).
	RecursivelyExecuteInnerMostFirst([&sfmlEvent](GuiComponent* guiComp)
	{
		return guiComp->AcceptsMouseEvents(IE_Input, sfmlEvent.x, sfmlEvent.y);
	},
	[&](GuiComponent* guiComp)
	{
		guiComp->ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
		return false;
	});
}

bool GuiComponent::ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (!AcceptsMouseEvents(IE_MouseEvent, sfmlEvent.x, sfmlEvent.y))
	{
		return false;
	}

	BroadcastNonconsumableMouseClick(mouse, sfmlEvent, eventType);
	return BroadcastConsumableMouseClick(mouse, sfmlEvent, eventType);
}

bool GuiComponent::ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (!AcceptsMouseEvents(IE_MouseWheel, sfmlEvent.x, sfmlEvent.y))
	{
		return false;
	}

	BroadcastNonconsumableMouseWheelMove(mouse, sfmlEvent);
	return BroadcastConsumableMouseWheelMove(mouse, sfmlEvent);
}

void GuiComponent::BroadcastNonconsumableInput (const sf::Event& evnt)
{
	ExecuteInput(evnt);
	for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp == nullptr || !guiComp->AcceptsInputEvent(IE_Input, evnt))
		{
			iter.SkipSubComponents();
			continue;
		}

		guiComp->ExecuteInput(evnt);
	}
}

void GuiComponent::BroadcastNonconsumableText (const sf::Event& evnt)
{
	ExecuteText(evnt);
	for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp == nullptr || !guiComp->AcceptsInputEvent(IE_Text, evnt))
		{
			iter.SkipSubComponents();
			continue;
		}

		guiComp->ExecuteText(evnt);
	}
}

void GuiComponent::BroadcastNonconsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	ExecuteMouseClick(mouse, sfmlEvent, eventType);
	for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp == nullptr || !guiComp->AcceptsMouseEvents(IE_MouseEvent, sfmlEvent.x, sfmlEvent.y))
		{
			iter.SkipSubComponents();
			continue;
		}

		guiComp->ExecuteMouseClick(mouse, sfmlEvent, eventType);
	}
}

void GuiComponent::BroadcastNonconsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	ExecuteMouseWheelMove(mouse, sfmlEvent);
	for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp == nullptr || !guiComp->AcceptsMouseEvents(IE_MouseWheel, sfmlEvent.x, sfmlEvent.y))
		{
			iter.SkipSubComponents();
			continue;
		}

		guiComp->ExecuteMouseWheelMove(mouse, sfmlEvent);
	}
}

bool GuiComponent::BroadcastConsumableInput (const sf::Event& evnt)
{
	//Invoke the inner most children first since those are drawn over the parent (they have input priority).
	return RecursivelyExecuteInnerMostFirst([&evnt](GuiComponent* guiComp)
	{
		return guiComp->AcceptsInputEvent(IE_Input, evnt);
	},
	[&evnt](GuiComponent* guiComp)
	{
		return guiComp->ExecuteConsumableInput(evnt);
	});
}

bool GuiComponent::BroadcastConsumableText (const sf::Event& evnt)
{
	return RecursivelyExecuteInnerMostFirst([&evnt](GuiComponent* guiComp)
	{
		return guiComp->AcceptsInputEvent(IE_Text, evnt);
	},
	[&evnt](GuiComponent* guiComp)
	{
		return guiComp->ExecuteConsumableText(evnt);
	});
}

bool GuiComponent::BroadcastConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool bConsumeEvent = RecursivelyExecuteInnerMostFirst([&sfmlEvent](GuiComponent* guiComp)
	{
		return guiComp->AcceptsMouseEvents(IE_MouseEvent, sfmlEvent.x, sfmlEvent.y);
	},
	[&](GuiComponent* guiComp)
	{
		return guiComp->ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType);
	});

	if (!bConsumeEvent && bAlwaysConsumeMouseEvents && IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y)))
	{
		//Note: we don't have this within ExecuteConsumableMouseClick primarily because subclasses don't react if the super class consumes the event first (eg:  if (Super::ExecuteConsumableMouseClick) return true;).
		bConsumeEvent = true;
	}

	return bConsumeEvent;
}

bool GuiComponent::BroadcastConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	bool bConsumeEvent = RecursivelyExecuteInnerMostFirst([&sfmlEvent](GuiComponent* guiComp)
	{
		return guiComp->AcceptsMouseEvents(IE_MouseWheel, sfmlEvent.x, sfmlEvent.y);
	},
	[&](GuiComponent* guiComp)
	{
		return guiComp->ExecuteConsumableMouseWheelMove(mouse, sfmlEvent);
	});

	if (!bConsumeEvent && bAlwaysConsumeMouseEvents && IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y)))
	{
		bConsumeEvent = true;
	}

	return bConsumeEvent;
}

void GuiComponent::ExecuteInput (const sf::Event& evnt)
{
	//Noop
}

void GuiComponent::ExecuteText (const sf::Event& evnt)
{
	//Noop
}

void GuiComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	//Noop
}

void GuiComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	//Noop
}

void GuiComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	//Noop
}

bool GuiComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	return false;
}

bool GuiComponent::ExecuteConsumableText (const sf::Event& evnt)
{
	return false;
}

bool GuiComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	return false;
}

bool GuiComponent::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	return false;
}

void GuiComponent::SetInputAvailability (EInputAvailability newInputAvailability)
{
	InputAvailability = newInputAvailability;
}

void GuiComponent::SetAcceptedInputEvents (EInputEvent newAcceptedInputEvents)
{
	AcceptedInputEvents = newAcceptedInputEvents;
}

void GuiComponent::SetAlwaysConsumeMouseEvents (bool newAlwaysConsumeMouseEvents)
{
	bAlwaysConsumeMouseEvents = newAlwaysConsumeMouseEvents;
}

void GuiComponent::InitializeComponents ()
{
	//Noop
}

bool GuiComponent::RecursivelyExecuteInnerMostFirst (std::function<bool(GuiComponent*)> shouldStepIn, std::function<bool(GuiComponent*)> lambda)
{
	//Iterate backwards since components at the end of the list are rendered on top. If there are overlapping sibling components, the component rendered last should have input priority.
	if (!ContainerUtils::IsEmpty(ReadComponents()))
	{
		size_t i = ReadComponents().size() - 1;
		while (true)
		{
			if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(ReadComponents().at(i)))
			{
				if (shouldStepIn(guiComp))
				{
					bool consumed = guiComp->RecursivelyExecuteInnerMostFirst(shouldStepIn, lambda);
					if (consumed)
					{
						return consumed;
					}
				}
			}

			if (i == 0)
			{
				break;
			}

			--i;
		}
	}

	//At this point, it's safe to assume none of the children consumed the event. Pass the event to the base component.
	return lambda(this);
}
SD_END