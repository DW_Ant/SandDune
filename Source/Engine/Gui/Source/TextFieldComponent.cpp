/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFieldComponent.cpp
=====================================================================
*/

#include "ContextMenuComponent.h"
#include "DynamicListMenu.h"
#include "FrameComponent.h"
#include "GuiEntity.h"
#include "GuiDataElement.h"
#include "ScrollbarComponent.h"
#include "ScrollableInterface.h"
#include "TextFieldComponent.h"
#include "TextFieldRenderComponent.h"

IMPLEMENT_CLASS(SD::TextFieldComponent, SD::LabelComponent)
SD_BEGIN

void TextFieldComponent::InitProps ()
{
	Super::InitProps();

	RenderComponentClass = TextFieldRenderComponent::SStaticClass();

	MaxNumCharacters = -1;
	DragScrollRate = 0.2f;
	bAutoSelect = true;

	SetRenderComponent(nullptr);
	bEditable = true;
	bSelectable = true;
	bSingleLine = false;
	CursorPosition = -1;
	ActiveLineIndex = -1;
	HighlightEndPosition = -1;
	SingleLineCursorOverflow = 0;
	HorizontalCursorPosition = -1.f;
	IconOverrideMouse = nullptr;
	SingleLineCursorJumpAmount = 3;
	SingleLineHighlightDragBorderRange = Range<Float>(6.f, 384.f);
	SingleLineHighlightDragCharRate = Range<Float>(1.f, 48.f);

	bIgnoreNextFocusedTextEvent = false;
	bDraggingOverText = false;
	LastDragScrollTime = 0.f;
	OriginalContent = DString::EmptyString;
	bLastCursorScreenPosDirty = true;
	OverridingMouseIcon = nullptr;
	bIsCtrlCmd = false;
	SingleLineHighlightDragTick = nullptr;
	SingleLineHighlightDragTickRate = 0.2f; //5 times per second
}

void TextFieldComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextFieldComponent* textFieldTemplate = dynamic_cast<const TextFieldComponent*>(objTemplate);
	if (textFieldTemplate != nullptr)
	{
		bool bCreatedObj = false;
		ContextComp = ReplaceTargetWithObjOfMatchingClass(ContextComp.Get(), textFieldTemplate->GetContextComp(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ContextComp);
		}

		if (ContextComp != nullptr)
		{
			ContextComp->CopyPropertiesFrom(textFieldTemplate->GetContextComp());
		}

		MaxNumCharacters = textFieldTemplate->MaxNumCharacters;
		DragScrollRate = textFieldTemplate->DragScrollRate;
		bAutoSelect = textFieldTemplate->bAutoSelect;
		SetEditable(textFieldTemplate->GetEditable());
		SetSelectable(textFieldTemplate->GetSelectable());
		SingleLineCursorJumpAmount = textFieldTemplate->SingleLineCursorJumpAmount;
		SetSingleLineHighlightDragBorderRange(textFieldTemplate->SingleLineHighlightDragBorderRange);
		SetSingleLineHighlightDragCharRate(textFieldTemplate->SingleLineHighlightDragCharRate);
	}
}

void TextFieldComponent::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	if (bAutoRefresh && bSingleLine)
	{
		Super::SetText(ProcessSingleLineSetText(GetContent()));
	}
}

bool TextFieldComponent::CanBeFocused () const
{
	if (!bSelectable && !bEditable)
	{
		return false;
	}

	return (FocusInterface::CanBeFocused() && IsVisible());
}

bool TextFieldComponent::CanTabOut () const
{
	if (CursorPosition >= 0)
	{
		return false; //The user is currently editing text.
	}

	return FocusInterface::CanTabOut();
}

void TextFieldComponent::GainFocus ()
{
	FocusInterface::GainFocus();

	bool bSelectAll = (CursorPosition < 0 && bAutoSelect);

	JumpCursorToBeginning(false);
	OriginalContent = GetContent();
	bIgnoreNextFocusedTextEvent = true; //The user is in the middle of a tab press.  We'll need to ignore the next key event so that tab is not applied to the text content.

	if (bSelectAll)
	{
		SelectAll();
	}
}

void TextFieldComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	SetCursorPosition(-1);
	SetHighlightEndPosition(-1);
}

bool TextFieldComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (OnEdit.IsBounded() && OnEdit(keyEvent))
	{
		return true;
	}

	if (keyEvent.key.code == sf::Keyboard::Escape && keyEvent.type == sf::Event::KeyReleased && CursorPosition >= 0)
	{
		SetCursorPosition(-1);
		SetHighlightEndPosition(-1);
		return true;
	}
	else if (keyEvent.key.code == sf::Keyboard::Return && keyEvent.type == sf::Event::KeyReleased && CursorPosition < 0)
	{
		JumpCursorToBeginning(false);
		return true;
	}

	return ExecuteTextFieldInput(keyEvent);
}

bool TextFieldComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	if (bIgnoreNextFocusedTextEvent)
	{
		bIgnoreNextFocusedTextEvent = false;
		return true; //Cause the default text event to be ignored, too.
	}

	return ExecuteTextFieldText(keyEvent);
}

void TextFieldComponent::SetText (const DString& newContent)
{
	if (CursorPosition >= 0)
	{
		MarkCacheDirty();
	}

	if (HighlightEndPosition >= 0)
	{
		SetHighlightEndPosition(-1);
	}

	if (bSingleLine)
	{
		Super::SetText(ProcessSingleLineSetText(newContent));
	}
	else
	{
		Super::SetText(newContent);
	}

	if (OnTextChanged.IsBounded())
	{
		OnTextChanged(this);
	}
}

void TextFieldComponent::RefreshTextFromLine (size_t startingLineIdx)
{
	Super::RefreshTextFromLine(startingLineIdx);

	if (CursorPosition >= 0)
	{
		MarkCacheDirty();
	}

	if (HighlightEndPosition >= 0)
	{
		SetHighlightEndPosition(-1);
	}
}

DString TextFieldComponent::GetContent () const
{
	if (bSingleLine)
	{
		return SingleLineHeaderText + Super::GetContent() + SingleLineTailText;
	}

	return Super::GetContent();
}

void TextFieldComponent::GetContent (std::vector<DString>& outContent, bool bIncludeClamped) const
{
	Super::GetContent(OUT outContent, bIncludeClamped);

	if (bSingleLine && bIncludeClamped)
	{
		if (!SingleLineHeaderText.IsEmpty())
		{
			outContent.insert(outContent.begin(), SingleLineHeaderText);
		}

		if (!SingleLineTailText.IsEmpty())
		{
			outContent.push_back(SingleLineTailText);
		}
	}
}

bool TextFieldComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableInput(evnt))
	{
		return true;
	}

	if (CursorPosition >= 0 && OnEdit.IsBounded() && OnEdit(evnt))
	{
		return true;
	}

	return ExecuteTextFieldInput(evnt);

}

bool TextFieldComponent::ExecuteConsumableText (const sf::Event& evnt)
{
	return ExecuteTextFieldText(evnt);
}

void TextFieldComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	UpdateMousePointerIcon(mouse, sfmlEvent);

	if (bDraggingOverText)
	{
		LatestDragMousePosition = Vector2(sfmlEvent.x, sfmlEvent.y);
		Float elapsedTime = Engine::FindEngine()->GetElapsedTime();
		if (elapsedTime - LastDragScrollTime >= DragScrollRate && !IsWithinBounds(LatestDragMousePosition))
		{
			LastDragScrollTime = elapsedTime;
			const Vector2& textPosition = ReadCachedAbsPosition();

			if (ScrollableInterface* owningInterface = dynamic_cast<ScrollableInterface*>(GetRootEntity()))
			{
				if (owningInterface->OnScrollToPosition.IsBounded())
				{
					owningInterface->OnScrollToPosition(textPosition, false);
				}
			}
		}

		Int charIndex = FindCursorPos(sfmlEvent.x, sfmlEvent.y);
		if (charIndex >= 0)
		{
			SetHighlightEndPosition(charIndex);
		}
	}
}

void TextFieldComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		return;
	}

	if (ContextComp != nullptr && ContextComp->IsRevealed())
	{
		//Don't deselect if the context menu component is active since the user may be clicking out of bounds to close the context menu, or they could be clicking within a context menu option to edit the selected text.
		return;
	}

	bool bWithinRange = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));
	if (!bWithinRange && !bDraggingOverText)
	{
		//deselect
		SetCursorPosition(-1);
		SetHighlightEndPosition(-1);
	}
}

bool TextFieldComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (!bSelectable || !IsVisible())
	{
		return false;
	}

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		//This component only consumes left click mouse events
		return false;
	}

	if (ContextComp != nullptr && ContextComp->IsRevealed())
	{
		return false;
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		bool bOldDraggingOverText = bDraggingOverText;
		SetDragOverText(false);

		return bOldDraggingOverText;
	}
	else if (IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		Int charIndex = FindCursorPos(Int(sfmlEvent.x), Int(sfmlEvent.y));

		bool bSelectAll = false;
		if (charIndex >= 0)
		{
			if (CursorPosition < 0)
			{
				OriginalContent = GetContent();
				bSelectAll = bAutoSelect;
			}

			SetCursorPosition(charIndex);
			UpdateCursorHorizontalPosition(CursorPosition);
		}

		if (bSelectAll)
		{
			SelectAll();
		}
		else
		{
			SetHighlightEndPosition(-1);
			SetDragOverText(true);
		}

		return true;
	}

	return false;
}

void TextFieldComponent::InitializeBackgroundComponent ()
{
	Super::InitializeBackgroundComponent();

	if (BackgroundComponent.IsValid())
	{
		BackgroundComponent->SetVisibility(true);
	}
}

void TextFieldComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	ContextComp = ContextMenuComponent::CreateObject();
	if (AddComponent(ContextComp))
	{
		ContextComp->OnInitializeMenu = SDFUNCTION_3PARAM(this, TextFieldComponent, HandleInitContextComp, bool, GuiEntity*, ContextMenuComponent*, MousePointer*);
	}
}

void TextFieldComponent::Destroyed ()
{
	if (IconOverrideMouse.IsValid())
	{
		IconOverrideMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, TextFieldComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		OverridingMouseIcon = nullptr;
		IconOverrideMouse = nullptr;
	}

	Super::Destroyed();
}

bool TextFieldComponent::IsCursorVisible () const
{
	if (CursorPosition < 0 || SingleLineCursorOverflow != 0)
	{
		return false;
	}

	Vector2 lastCursorPos = CalcCharPosition(CursorPosition, 0.5f); //Regarding 0.5, consider the center of the cursor instead of the top of the cursor.
	if (lastCursorPos.IsNearlyEqual(Vector2(-1.f, -1.f)))
	{
		//The TextField doesn't have any text instances to calculate character position.
		return false;
	}

	return IsCursorVisible(lastCursorPos);
}

bool TextFieldComponent::IsCursorVisible (const Vector2& absCursorPos) const
{
	Vector2 relativePos(absCursorPos);

	//Offset the cursor position based on the camera.
	Float zoomMultiplier = 1.f;
	if (GuiEntity* guiEntity = dynamic_cast<GuiEntity*>(GetRootEntity()))
	{
		if (ScrollbarComponent* scrollbar = guiEntity->GetScrollbarOwner())
		{
			if (PlanarCamera* cam = scrollbar->GetFrameCamera())
			{
				zoomMultiplier = cam->GetZoom();
				relativePos -= (cam->ReadPosition() - (cam->GetZoomedExtents() * 0.5f));
			}
		}
	}
	

	if (relativePos.X < 0.f || relativePos.Y < 0.f)
	{
		return false;
	}

	//Check if the cursor is to the left or below the render target borders.
	const RenderTarget* rootRenderTarget = FindRootRenderTarget();
	if (rootRenderTarget != nullptr)
	{
		Vector2 targetSize = rootRenderTarget->GetSize() / zoomMultiplier;
		if (relativePos.X > targetSize.X || relativePos.Y > targetSize.Y)
		{
			return false;
		}
	}

	return true;
}

void TextFieldComponent::InsertTextAtCursor (DString insertedText)
{
	if (CursorPosition < 0)
	{
		return;
	}

	if (bSingleLine)
	{
		//Single line text field components prohibit new line characters
		insertedText.ReplaceInline(TXT("\n"), TXT(" "), DString::CC_CaseSensitive);
		insertedText.ReplaceInline(TXT("\r"), TXT(" "), DString::CC_CaseSensitive);
	}

	Int localCursorIdx;
	size_t lineIdx = GetCursorLineData(CursorPosition, OUT localCursorIdx).ToUnsignedInt();

	if (MaxNumCharacters > 0)
	{
		//Need to verify if more characters can be inserted
		Int totalChars = SingleLineHeaderText.Length() + SingleLineTailText.Length();
		for (const DString& line : Content)
		{
			totalChars += line.Length();
		}
		totalChars += ClampedText.Length();

		Int availChars = MaxNumCharacters - totalChars;
		if (availChars < insertedText.Length())
		{
			if (availChars <= 0)
			{
				return;
			}

			DString::Left(OUT insertedText, availChars);
		}
	}

	size_t oldNumLines = Content.size();
	if (ContainerUtils::IsEmpty(Content))
	{
		SetText(insertedText);
	}
	else if (bSingleLine)
	{
		//For single line, rely on using full text content to consider the header and tailing text. In addition to that, SetText will process the horizontal scrolling, too.
		//Also single lines don't really need to RefreshTextFromLine since it's one line anyways.
		DString fullText = GetContent();
		Int absCursorPos = GetTextPosition();
		fullText.Insert(absCursorPos, insertedText);
		SetText(fullText);
	}
	else
	{
		Content.at(lineIdx).Insert(localCursorIdx, insertedText);
		RefreshTextFromLine(lineIdx);

		if (OnTextChanged.IsBounded())
		{
			OnTextChanged(this);
		}
	}

	if (!bSingleLine)
	{
		//Need to consider how many lines were inserted to accurately calculate the new cursor position.
		//For each line, there is an extra cursor position at the end.
		//For each line that was inserted, offset the cursor position by 1.
		size_t numNewLines = Content.size() - oldNumLines;
		SetCursorPosition(CursorPosition + insertedText.Length() + numNewLines);
	}
	else //SingleLine
	{
		Int maxCursorPos = CountHighestCursorIdx();
		Int newCursorPos = CursorPosition + SingleLineCursorOverflow + insertedText.Length();
		if (newCursorPos > maxCursorPos)
		{
			ShiftDisplayedTextTowardsTail((newCursorPos - maxCursorPos) + SingleLineCursorJumpAmount);
			SetCursorPosition(localCursorIdx + insertedText.Length());
		}
		else if (newCursorPos < 0)
		{
			//The cursor is somewhere in the header
			ShiftDisplayedTextTowardsHeader(newCursorPos);
			SetCursorPosition(0);
		}
		else
		{
			SetCursorPosition(newCursorPos); //SetCursorPosition automatically clamps the cursor position to the displayed text.
		}

		SingleLineCursorOverflow = 0;
	}

	UpdateCursorHorizontalPosition(CursorPosition);
}

void TextFieldComponent::SelectAll ()
{
	//Only select all if there is text to select
	if (!ContainerUtils::IsEmpty(Content) && !Content.at(0).IsEmpty())
	{
		//Have cursor jump to the beginning so that the highlight end position is at the last character. This is useful for determining which end to display for single line text fields.
		JumpCursorToBeginning(false);

		if (bSingleLine && !SingleLineTailText.IsEmpty())
		{
			//Use complicated method to handle text outside of displayed text
			MoveCursor(SingleLineTailText.Length() + Content.at(0).Length(), true);
		}
		else
		{
			//Use faster method since there's no hidden text outside of displayed text
			SetHighlightEndPosition(SD_MAXINT);
		}
	}
}

void TextFieldComponent::SetEditable (bool bNewEditable)
{
	if (bEditable == bNewEditable)
	{
		return; //Nothing needs changing
	}

	bEditable = bNewEditable;
	if (!bEditable)
	{
		//deselect
		SetHighlightEndPosition(-1);
		SetCursorPosition(-1);
		if (!bSelectable)
		{
			//Notify mouse pointer that this is not overriding mouse pointer
			OverridingMouseIcon = nullptr;
		}
	}
}

void TextFieldComponent::SetSelectable (bool bNewSelectable)
{
	if (bSelectable == bNewSelectable)
	{
		return;
	}

	bSelectable = bNewSelectable;
	if (!bSelectable)
	{
		//deselect
		SetHighlightEndPosition(-1);
		SetCursorPosition(-1);
		if (!bEditable)
		{
			//Notify mouse pointer that this is not overriding mouse pointer
			OverridingMouseIcon = nullptr;
		}
	}
}

void TextFieldComponent::SetSingleLine (bool bNewSingleLine)
{
	if (bSingleLine == bNewSingleLine)
	{
		return;
	}

	bSingleLine = bNewSingleLine;
	SetWrapText(!bSingleLine);
	SetClampText(!bSingleLine); //Optimization: Don't bother with clamping text in LabelComponent since TextFieldComponent::SetText will process text clamping.

	//Clear highlighting and cursor position since the text may be repositioned in this new mode.
	SetHighlightEndPosition(-1);
	SetCursorPosition(-1);

	if (bSingleLine && SingleLineHighlightDragTick == nullptr)
	{
		SingleLineHighlightDragTick = TickComponent::CreateObject(TICK_GROUP_GUI);
		if (AddComponent(SingleLineHighlightDragTick))
		{
			SingleLineHighlightDragTick->SetTickInterval(SingleLineHighlightDragTickRate);
			SingleLineHighlightDragTick->SetTickHandler(SDFUNCTION_1PARAM(this, TextFieldComponent, HandleSingleLineHighlightDrag, void, Float));
			SingleLineHighlightDragTick->SetTicking(false);
		}
	}
	else if (!bSingleLine && SingleLineHighlightDragTick != nullptr)
	{
		SingleLineHighlightDragTick->Destroy();
		SingleLineHighlightDragTick = nullptr;
	}
}

void TextFieldComponent::SetCursorPosition (Int newPosition, bool bMoveForwardIfAfterNewLine)
{
	if (newPosition == CursorPosition)
	{
		return;
	}

	Int maxPos = CountHighestCursorIdx();
	newPosition = Utils::Min(newPosition, maxPos);

	Int localCursorPosition;
	ActiveLineIndex = GetCursorLineData(newPosition, OUT localCursorPosition);

	//Ensure the cursor is not after a new line character. It would be misleading to the user if they see a cursor position at the end of the line, but if they enter a new character, the new character jumps to the next line.
	size_t lineIdx = ActiveLineIndex.ToUnsignedInt();
	if (localCursorPosition > 0 && ContainerUtils::IsValidIndex(Content, lineIdx))
	{
		const DString& curLine = Content.at(lineIdx);
		if (!curLine.IsEmpty() && localCursorPosition >= curLine.Length() &&
			(curLine.EndsWith('\n') || curLine.EndsWith('\r')) ) //Only compare against the last character since it's not possible to have a new line character in the middle of that string.
		{
			if (bMoveForwardIfAfterNewLine && newPosition < maxPos) //If it's at the very last position, always fallback to move backwards since it cannot advance to another valid position.
			{
				++newPosition;

				//Assume it's jumping to the next line
				localCursorPosition = 0;
				++ActiveLineIndex;
			}
			else
			{
				--newPosition;
				--localCursorPosition;
			}
		}
	}

	CursorPosition = newPosition;
	MarkCacheDirty();

	if (!bSingleLine)
	{
		//Check if the scrollbar needs to snap to view the cursor position.
		if (ScrollableInterface* scrollInterface = dynamic_cast<ScrollableInterface*>(GetRootEntity()))
		{
			if (scrollInterface->OnScrollToPosition.IsBounded())
			{
				Vector2 absCursorCoordinates = CalcCharPosition(ActiveLineIndex.ToUnsignedInt(), localCursorPosition, 0.5f);
				if (CursorPosition >= 0 && !absCursorCoordinates.IsNearlyEqual(Vector2(-1.f, -1.f)) && !IsCursorVisible(absCursorCoordinates))
				{
					scrollInterface->OnScrollToPosition(absCursorCoordinates, false);
				}
			}
		}
	}

	if (CursorPosition < 0)
	{
		SingleLineCursorOverflow = 0;
		HorizontalCursorPosition = -1.f;	
	}

	//Update the highlight positions.
	if (HighlightEndPosition >= 0)
	{
		Int minHighlight = Utils::Min(CursorPosition, HighlightEndPosition);
		Int maxHighlight = Utils::Max(CursorPosition, HighlightEndPosition);
		UpdateHighlightOnRenderComponent(minHighlight, maxHighlight);
	}
}

void TextFieldComponent::JumpCursorToBeginning (bool bMoveHighlight)
{
	Int absCursorIdx = -1;
	if (bSingleLine && bMoveHighlight)
	{
		absCursorIdx = GetTextPosition();
	}

	ShiftDisplayedTextTowardsHeader(SD_MAXINT);
	SingleLineCursorOverflow = 0;

	if (bMoveHighlight)
	{
		SetHighlightEndPosition(0);

		if (bSingleLine)
		{
			Int maxCursorPos = CountHighestCursorIdx();
			if (absCursorIdx > maxCursorPos) //Cursor resides in the tail text
			{
				SingleLineCursorOverflow = (absCursorIdx - maxCursorPos);
				SetCursorPosition(maxCursorPos);
			}
			else //Cursor resides in the displayed text
			{
				SetCursorPosition(absCursorIdx);
			}
		}
	}
	else
	{
		SetHighlightEndPosition(-1);
		SetCursorPosition(0);
	}

	HorizontalCursorPosition = 0.f;
}

void TextFieldComponent::JumpCursorToEnd (bool bMoveHighlight)
{
	Int absCursorIdx = -1;
	if (bSingleLine && bMoveHighlight)
	{
		absCursorIdx = GetTextPosition(); //Must be computed first before performing the shift
	}

	Int shiftAmount = ShiftDisplayedTextTowardsTail(SD_MAXINT);
	SingleLineCursorOverflow = 0;

	//This function will automatically clamp to the max cursor position.
	if (bMoveHighlight)
	{
		SetHighlightEndPosition(SD_MAXINT);
		if (bSingleLine && shiftAmount != 0)
		{
			if (absCursorIdx < SingleLineHeaderText.Length()) //If the cursor index is within header
			{
				SingleLineCursorOverflow = (SingleLineHeaderText.Length() - absCursorIdx) * -1;
				SetCursorPosition(0);
			}
			else //Cursor is in the displayed text
			{
				SetCursorPosition(absCursorIdx - SingleLineHeaderText.Length());
			}
		}
	}
	else
	{
		SetHighlightEndPosition(-1);
		SetCursorPosition(SD_MAXINT);
	}

	UpdateCursorHorizontalPosition(SD_MAXINT);
}

void TextFieldComponent::SetHighlightEndPosition (Int newPosition)
{
	if (HighlightEndPosition == newPosition)
	{
		return;
	}

	if (newPosition < 0)
	{
		SingleLineCursorOverflow = 0;
	}

	HighlightEndPosition = Utils::Min(newPosition, CountHighestCursorIdx());

	//Update the render component
	Int minValue = Utils::Min(CursorPosition, HighlightEndPosition);
	Int maxValue = Utils::Max(CursorPosition, HighlightEndPosition);
	UpdateHighlightOnRenderComponent(minValue, maxValue);
}

void TextFieldComponent::SetSingleLineHighlightDragBorderRange (const Range<Float>& newSingleLineHighlightDragBorderRange)
{
	SingleLineHighlightDragBorderRange = newSingleLineHighlightDragBorderRange;
}

void TextFieldComponent::SetSingleLineHighlightDragCharRate (const Range<Float>& newSingleLineHighlightDragCharRate)
{
	SingleLineHighlightDragCharRate = newSingleLineHighlightDragCharRate;
}

bool TextFieldComponent::GetEditable () const
{
	return bEditable;
}

bool TextFieldComponent::GetSelectable () const
{
	return bSelectable;
}

Int TextFieldComponent::GetTextPosition () const
{
	Int offset = 0;
	if (bSingleLine)
	{
		offset = SingleLineHeaderText.Length(); //The header text should displace the CursorPosition.
		offset += SingleLineCursorOverflow; //If the cursor is actually in the head or tail, displace it by this amount.
	}

	//There are extra cursor positions at the end of each line, need to offset delete index based on the line the cursor is found.
	return (CursorPosition - ActiveLineIndex) + offset;
}

Int TextFieldComponent::GetTextPosition (Int cursorIdx, Int lineIdx) const
{
	Int offset = 0;
	if (bSingleLine)
	{
		offset = SingleLineHeaderText.Length();
	}

	//There are extra cursor positions at the end of each line, need to offset delete index based on the line the cursor is found.
	return (cursorIdx - lineIdx) + offset;
}

DString TextFieldComponent::GetSelectedText () const
{
	if (HighlightEndPosition < 0 || CursorPosition < 0)
	{
		return DString::EmptyString; //Nothing is selected
	}
	
	//Handle case where the cursor and highlight end position resides in the same spot, but the real cursor resides in the header or tail text.
	if (HighlightEndPosition == CursorPosition && SingleLineCursorOverflow == 0)
	{
		return DString::EmptyString; //Nothing is selected
	}

	//Need to run separate calculation for HighlightEndPosition since there isn't an ActiveLineIndex variable for HighlightEndPosition (don't want to recalculate ActiveLineIdx on mouse move for HighlightEndPos).
	Int highlightEndPosLineIdx = GetCursorLineData(HighlightEndPosition);
	CHECK(highlightEndPosLineIdx != INT_INDEX_NONE)

	Int absTextPosition = GetTextPosition();
	Int absHighlightEndTextPos = GetTextPosition(HighlightEndPosition, highlightEndPosLineIdx);
	DString result = GetContent();
	if (Int::Abs(absTextPosition - absHighlightEndTextPos) == 1) //Only one character is selected
	{
		DString::SubStringCount(OUT result, Utils::Min(absTextPosition, absHighlightEndTextPos), 1);
	}
	else if (absHighlightEndTextPos < absTextPosition) //The trailing highlight should not include the following character (unlike the start of highlight)
	{
		DString::SubString(OUT result, absHighlightEndTextPos, absTextPosition - 1);
	}
	else
	{
		DString::SubString(OUT result, absTextPosition, absHighlightEndTextPos - 1);
	}

	return result;
}

DString TextFieldComponent::GetDisplayedText () const
{
	if (bSingleLine)
	{
		if (!ContainerUtils::IsEmpty(Content))
		{
			return Content.at(0);
		}

		return DString::EmptyString;
	}
	else
	{
		CHECK_INFO(false, "GetDisplayedText should only be called for bSingleLine text field components. Otherwise directly call GetContent instead.");
		return GetContent();
	}
}

DString TextFieldComponent::ProcessSingleLineSetText (const DString& newText)
{
	if (const Font* curFont = GetFont())
	{
		Float totalWidth = curFont->CalculateStringWidth(newText, GetCharacterSize());
		if (totalWidth <= ReadCachedAbsSize().X)
		{
			//Everything fits. Ensure the header and tail are clear from previous data
			SingleLineHeaderText = DString::EmptyString;
			SingleLineTailText = DString::EmptyString;
			return newText;
		}
		else
		{
			//The text will not fit in this single line text field component. Need to figure out which text to truncate.
			Int firstDisplayedChar = SingleLineHeaderText.Length();
			Float curWidth = 0.f;
			StringIterator iter(&newText);
			iter += firstDisplayedChar;
			Int lastCharIdx = firstDisplayedChar;

			for (Int charPos = firstDisplayedChar; charPos < newText.Length(); ++charPos)
			{
				curWidth += curFont->CalculateStringWidth(*iter, GetCharacterSize());
				++iter;

				if (curWidth >= ReadCachedAbsSize().X)
				{
					break;
				}

				++lastCharIdx;
			}

			--lastCharIdx; //Do not include the character that goes beyond the width or the character beyond the newContent.Length

			if (lastCharIdx < 0)
			{
				//TextFieldComponent is too small to render anything.
				SingleLineHeaderText = DString::EmptyString;
				SingleLineTailText = newText;
				return DString::EmptyString;
			}

			if (curWidth < ReadCachedAbsSize().X)
			{
				//The last character is within range. Iterate backwards from first displayedChar until it fills up.
				iter.JumpToBeginning();
				iter += firstDisplayedChar;
				Int charIdx = firstDisplayedChar;
				for (Int charPos = firstDisplayedChar - 1; charPos >= 0; --charPos)
				{
					curWidth += curFont->CalculateStringWidth(*iter, GetCharacterSize());
					--iter;
					--charIdx;
					if (curWidth >= ReadCachedAbsSize().X)
					{
						++charIdx; //This character is not allowed to be displayed
						break;
					}
				}

				//The end of the string fits, but the beginning does not. Shift the string to render where the first displayed char resides.
				if (curWidth >= ReadCachedAbsSize().X)
				{
					if (charIdx > 0)
					{
						DString displayedString = newText.SubString(charIdx);
						SingleLineHeaderText = newText.SubString(0, charIdx - 1);
						SingleLineTailText = DString::EmptyString;
						return displayedString;
					}
					else
					{
						//Nothing fits, move everything to tail
						SingleLineHeaderText = DString::EmptyString;
						SingleLineTailText = newText;
						return DString::EmptyString;
					}
				}

				//Everything fits, display everything
				return newText;
			}
			else
			{
				//The end of the string doesn't fit. Display the string between start position and end position.
				DString displayedString = newText.SubString(firstDisplayedChar, lastCharIdx);
				SingleLineHeaderText = (firstDisplayedChar > 0) ? newText.SubString(0, firstDisplayedChar - 1) : DString::EmptyString;
				SingleLineTailText = newText.SubString(lastCharIdx + 1);
				return displayedString;
			}
		}
	}

	return newText;
}

void TextFieldComponent::UpdateMousePointerIcon (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	//If mouse is not specified or if already overriding mouse icon...
	if (mouse == nullptr || OverridingMouseIcon.IsValid())
	{
		return;
	}

	bool bHoveringOverText = ((bSelectable || bEditable) && IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))));
	if (bHoveringOverText && IsVisible())
	{
		OverridingMouseIcon = GuiTheme::GetGuiTheme()->TextPointer;
		mouse->PushMouseIconOverride(OverridingMouseIcon.Get(), SDFUNCTION_2PARAM(this, TextFieldComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		IconOverrideMouse = mouse;
	}
}

Int TextFieldComponent::FindCursorPos (Int absX, Int absY)
{
	CHECK(GetRenderComponent() != nullptr)
	if (ContainerUtils::IsEmpty(Content))
	{
		return 0; //Jump to beginning.
	}

	const Vector2& finalCoordinates = ReadCachedAbsPosition();
	Int skippedPos = 0;
	const std::vector<TextRenderComponent::STextData>& textData = GetRenderComponent()->ReadTexts();
	if (ContainerUtils::IsEmpty(textData))
	{
		return 0; //Jump to beginning. This could happen if Content only has empty strings (empty strings do not instantiate STextData.
	}

	Int lineIndex = -1;
	for (size_t i = 0; i < textData.size(); i++)
	{
		//Special case for last line:  causes the last line to be selected if the user clicks on the blank space under last line.
		if (i == textData.size() - 1)
		{
			if (absY.ToFloat() - finalCoordinates.Y > ReadCachedAbsSize().Y)
			{
				return -1; //out of bounds
			}

			lineIndex = i;
			break;
		}
		else if ((textData.at(i+1).DrawOffset.y + finalCoordinates.Y.Value) >= absY.ToFloat().Value)
		{
			lineIndex = i;
			break;
		}

		//Add one since there are String.Length+1 cursor positions for every line.  For example:  for string "abcd", there are 5 cursor positions ranging from 0-4.
		skippedPos += Content.at(i).Length() + 1;
	}

	if (lineIndex < 0)
	{
		return -1; //out of bounds
	}

	Float widthLimit = absX.ToFloat() - finalCoordinates.X - textData.at(lineIndex.ToUnsignedInt()).DrawOffset.x;
	Int curCharIndex = GetFont()->FindCharNearWidthLimit(Content.at(lineIndex.ToUnsignedInt()), GetCharacterSize(), widthLimit);

	//Move to cursor position that is right before the clicked character.  For example for string "abcd", if user clicked on character 'b', the charIdx returns 0, we want to place cursor after 'a'
	curCharIndex++;

	return (skippedPos + curCharIndex);
}

Int TextFieldComponent::CountNumCursorPositions () const
{
	if (ContainerUtils::IsEmpty(Content))
	{
		//Even when there aren't any lines, there are at least one cursor position.
		return 1;
	}

	Int totalCursorPositions = 0;
	const Int numExtraPositionsPerLine = 1;
	for (size_t i = 0; i < Content.size(); ++i)
	{
		totalCursorPositions += Content.at(i).Length() + numExtraPositionsPerLine;
	}

	return totalCursorPositions;
}

void TextFieldComponent::UpdateCursorHorizontalPosition (Int cursorPos)
{
	HorizontalCursorPosition = 0.f;

	if (const Font* curFont = GetFont())
	{
		Int localCursorIdx;
		Int lineIdx = GetCursorLineData(cursorPos, OUT localCursorIdx);
		if (lineIdx != INT_INDEX_NONE && lineIdx < Content.size())
		{
			Float result = 0.f;
			if (localCursorIdx > 0)
			{
				const DString& lineStr = Content.at(lineIdx.ToUnsignedInt());
				if (localCursorIdx >= lineStr.Length())
				{
					localCursorIdx = -1; //indicate to calculate the whole string's length
				}

				result = curFont->CalculateStringWidth(lineStr.SubString(0, localCursorIdx), GetCharacterSize());
			}

			if (RenderComponent != nullptr && lineIdx < RenderComponent->ReadTexts().size())
			{
				result += RenderComponent->ReadTexts().at(lineIdx.ToUnsignedInt()).DrawOffset.x;
			}

			HorizontalCursorPosition = result;
		}
	}
}

void TextFieldComponent::MarkCacheDirty () const
{
	bLastCursorScreenPosDirty = true;
	
	if (TextFieldRenderComponent* renderComp = dynamic_cast<TextFieldRenderComponent*>(RenderComponent.Get()))
	{
		renderComp->RefreshDrawCoordinates();
	}
}

Vector2 TextFieldComponent::CalcCharPosition (Int absCursorIdx, Float verticalMultiplier) const
{
	if (!bLastCursorScreenPosDirty && absCursorIdx == CursorPosition)
	{
		return LastCursorScreenPos;
	}

	if (GetRenderComponent() == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate character position for %s since it does not possess a valid render component."), ToString());
		return Vector2(-1.f, -1.f);
	}

	if (absCursorIdx < 0)
	{
		return Vector2(-1.f, -1.f);
	}

	Int localCursorIdx;
	Int lineIdx;

	lineIdx = GetCursorLineData(absCursorIdx, OUT localCursorIdx);
	if (lineIdx < 0)
	{
		return Vector2(-1.f, -1.f);
	}

	Vector2 result = CalcCharPosition(lineIdx.ToUnsignedInt(), localCursorIdx, verticalMultiplier);

	if (CursorPosition == absCursorIdx)
	{
		LastCursorScreenPos = result;
		bLastCursorScreenPosDirty = false;
	}

	return result;
}

Vector2 TextFieldComponent::CalcCharPosition (size_t lineIdx, Int localCursorIdx, Float verticalMultiplier) const
{
	TextRenderComponent* renderComp = GetRenderComponent();
	if (renderComp == nullptr || !ContainerUtils::IsValidIndex(renderComp->ReadTexts(), lineIdx))
	{
		return Vector2(-1.f, -1.f);
	}

	Vector2 result = Vector2::SFMLtoSD(renderComp->ReadTexts().at(lineIdx).DrawOffset);

	DString partialLine = Content.at(lineIdx).Left(localCursorIdx);
	result.X += GetFont()->CalculateStringWidth(partialLine, GetCharacterSize());
	result += ReadCachedAbsPosition();

	result.Y += (GetCharacterSize().ToFloat() * verticalMultiplier);

	return result;
}

Int TextFieldComponent::GetCursorLineData (Int cursorIdx, Int& outLocalCursorIdx) const
{
	if (cursorIdx < 0)
	{
		outLocalCursorIdx = INT_INDEX_NONE;
		return INT_INDEX_NONE;
	}

	if (ContainerUtils::IsEmpty(Content))
	{
		outLocalCursorIdx = 0;
		return 0;
	}

	Int beginningLineCursorIdx = 0;
	size_t lineIdx = UINT_INDEX_NONE;

	//Find the line the cursorIdx is positioned.
	for (size_t i = 0; i < Content.size(); i++)
	{
		if (Content.at(i).Length() + beginningLineCursorIdx >= cursorIdx)
		{
			lineIdx = i;
			break;
		}

		beginningLineCursorIdx += Content.at(i).Length() + 1;
	}

	if (lineIdx == UINT_INDEX_NONE)
	{
		outLocalCursorIdx = INT_INDEX_NONE;
		return INT_INDEX_NONE;
	}

	outLocalCursorIdx = cursorIdx - beginningLineCursorIdx;
	return lineIdx;
}

Int TextFieldComponent::GetCursorLineData (Int cursorIdx) const
{
	Int unusedLocalIdx = 0;
	return GetCursorLineData(cursorIdx, OUT unusedLocalIdx);
}

Int TextFieldComponent::GetLineCursorIdx (size_t lineIdx) const
{
	Int cursorPos = 0;
	for (size_t i = 0; i < lineIdx && i < Content.size(); ++i)
	{
		cursorPos += Content.at(i).Length() + 1;
	}

	return cursorPos;
}

void TextFieldComponent::SetDragOverText (bool isDraggingOverText)
{
	if (isDraggingOverText == bDraggingOverText)
	{
		return;
	}

	bDraggingOverText = isDraggingOverText;
	if (SingleLineHighlightDragTick != nullptr)
	{
		SingleLineHighlightDragTick->SetTicking(bDraggingOverText);
	}
}

void TextFieldComponent::DeleteSelectedText ()
{
	/* If the Content's size is greater than this threshold, this function will change algorithms to partially update the Content rather.
	If Content is less than or equal to this threshold, it'll use the simpler brute force approach that'll consolidate the entire Content as a
	single string and simply perform one Remove operation on that string before sending the whole thing back to the LabelComponent.*/
	const size_t THRESHOLD_FOR_PARTIAL_UPDATE = 2;

	if (HighlightEndPosition < 0 || CursorPosition < 0 || HighlightEndPosition == CursorPosition)
	{
		return;
	}

	bool bMoveCursor = (HighlightEndPosition >= 0 && CursorPosition > HighlightEndPosition);
	Int newCursorPos = HighlightEndPosition;

	if (bSingleLine || Content.size() <= THRESHOLD_FOR_PARTIAL_UPDATE)
	{
		//Use simpler approach which would get the whole text. However this approach could be very expensive for multi-line text field components.
		DString fullText = GetContent();
		Int cursorTextPos = GetTextPosition();
		Int highlightEndLineIdx = GetCursorLineData(HighlightEndPosition);
		Int highlightEndTextIdx = GetTextPosition(HighlightEndPosition, highlightEndLineIdx);

		Int startIdx = (highlightEndTextIdx < cursorTextPos) ? highlightEndTextIdx : cursorTextPos;
		Int numCharsToDelete = (highlightEndTextIdx < cursorTextPos) ? (cursorTextPos - highlightEndTextIdx) : (highlightEndTextIdx - cursorTextPos);
		if (bSingleLine && SingleLineCursorOverflow < 0)
		{
			newCursorPos = Utils::Max<Int>(newCursorPos + SingleLineCursorOverflow, 0); //Move cursor backwards
		}

		if (startIdx + numCharsToDelete > fullText.Length())
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to delete selected text for %s since the start highlight position (%s) and the number of characters to delete (%s) goes beyond the text length (%s)."), ToString(), startIdx, numCharsToDelete, fullText.Length());
			return;
		}

		fullText.Remove(startIdx, numCharsToDelete);
		SetText(fullText);
	}
	else //Multi-line
	{
		//Use alternative method that doesn't require extracting the entire text block, and only modifies the text near the selection.
		Int firstLocalIdx;
		size_t firstLineIdx = GetCursorLineData(Utils::Min(CursorPosition, HighlightEndPosition), OUT firstLocalIdx).ToUnsignedInt();

		Int lastLocalIdx;
		size_t lastLineIdx = GetCursorLineData(Utils::Max(CursorPosition, HighlightEndPosition), OUT lastLocalIdx).ToUnsignedInt();

		CHECK(ContainerUtils::IsValidIndex(Content, firstLineIdx))

		bool bLineRemoved = false;
		if (firstLineIdx == lastLineIdx)
		{
			//Partially delete the one and only selected line
			Content.at(firstLineIdx).Remove(firstLocalIdx, (lastLocalIdx - firstLocalIdx));
		}
		else
		{
			//Multiple lines are affected
			CHECK(ContainerUtils::IsValidIndex(Content, lastLineIdx) && lastLineIdx > firstLineIdx)

			if (lastLocalIdx >= Content.at(lastLineIdx).Length())
			{
				//deleting the entire last line
				Content.erase(Content.begin() + lastLineIdx);
				bLineRemoved = true;
			}
			else if (lastLocalIdx > 0)
			{
				//Delete everything from start up to lastLocalIdx
				DString::SubString(OUT Content.at(lastLineIdx), lastLocalIdx);
			}

			if (lastLineIdx - firstLineIdx > 1)
			{
				//Remove everything between first and last lines
				Content.erase(Content.begin() + firstLineIdx + 1, Content.begin() + lastLineIdx);
				bLineRemoved = true;
			}
		
			if (firstLocalIdx == 0)
			{
				//Delete the first line
				Content.erase(Content.begin() + firstLineIdx);
				bLineRemoved = true;
			}
			else if (firstLocalIdx < Content.at(firstLineIdx).Length()) //If the cursor position is not at the end of this line
			{
				//Partially delete the first line
				DString::SubString(OUT Content.at(firstLineIdx), 0, firstLocalIdx - 1);
			}
		}

		RefreshTextFromLine(firstLineIdx);
		if (bLineRemoved)
		{
			//RefreshTextFromLine only considers lines near the firstLineIdx. Since this function removed a vector, we must explicitly sync up the render component.
			UpdateRenderContentFromLineToEnd(firstLineIdx + 1);
		}

		if (OnTextChanged.IsBounded())
		{
			OnTextChanged(this);
		}
	}

	if (bMoveCursor)
	{
		SetCursorPosition(newCursorPos);
		UpdateCursorHorizontalPosition(CursorPosition);
	}
}

void TextFieldComponent::DeleteAdjacentCharacter (bool bDeleteForward)
{
	if (CursorPosition < 0)
	{
		return;
	}

	if (bSingleLine)
	{
		//May be simpler to use the full text content since GetContent and GetTextPosition considers text not in the label, and there's no cost regarding word wrapping since that is disabled for single line text field components.
		DString fullText = GetContent();
		Int deleteIdx = GetTextPosition();
		if (!bDeleteForward)
		{
			--deleteIdx;
		}

		if (deleteIdx < 0 || deleteIdx >= fullText.Length())
		{
			return; //already at limit. Nothing to delete.
		}

		fullText.Remove(deleteIdx);
		SetText(fullText);
	}
	else
	{
		//Use alternative approach for deletion since rewrapping multiple lines can be costly.
		Int deleteIdx;
		size_t lineIdx = GetCursorLineData(CursorPosition, OUT deleteIdx).ToUnsignedInt();
		if (!ContainerUtils::IsValidIndex(Content, lineIdx))
		{
			return;
		}

		if (!bDeleteForward)
		{
			--deleteIdx;
		}

		//Handle case where it should delete a character on a different line instead
		if (deleteIdx < 0)
		{
			if (lineIdx == 0)
			{
				//Already at the very beginning. Nothing to delete.
				return;
			}

			--lineIdx;
			deleteIdx = Content.at(lineIdx).Length() - 1; //Note: if the previous line is empty, deleteIdx may be negative, but there's an empty check later in this function.
		}
		else if (deleteIdx >= Content.at(lineIdx).Length())
		{
			if (lineIdx == Content.size() - 1)
			{
				//Already at the very end. Nothing to delete.
				return;
			}

			++lineIdx;
			deleteIdx = 0;
		}

		bool bLineRemoved = Content.at(lineIdx).IsEmpty();
		if (!bLineRemoved)
		{
			//Couple edge cases, if the line being deleted only has end line characters, delete the line anyways.
			if (Content.at(lineIdx).Length() == 1)
			{
				bLineRemoved = (Content.at(lineIdx).At(0) == '\n' || Content.at(lineIdx).At(0) == '\r');
			}

			if (!bLineRemoved)
			{
				TStringChar deletedChar = Content.at(lineIdx).At(deleteIdx);
				bool bDeletingNewLine = (deletedChar == '\n' || deletedChar == '\r');

				//Remove a character
				Content.at(lineIdx).Remove(deleteIdx);

				if (bDeletingNewLine && lineIdx + 1 < Content.size())
				{
					//If deleting a new line character, bring the next line to the affected line.
					Content.at(lineIdx) += Content.at(lineIdx + 1);
					Content.erase(Content.begin() + lineIdx + 1);
				}
			}
		}
		
		if (bLineRemoved)
		{
			//Remove the empty line
			Content.erase(Content.begin() + lineIdx);
		}

		RefreshTextFromLine(lineIdx);
		if (bLineRemoved)
		{
			//RefreshTextFromLine only considers lines near the firstLineIdx. Since this function removed a vector, we must explicitly sync up the render component.
			UpdateRenderContentFromLineToEnd(lineIdx);
		}

		if (OnTextChanged.IsBounded())
		{
			OnTextChanged(this);
		}
	}
}

void TextFieldComponent::UpdateHighlightOnRenderComponent (Int startHighlightPos, Int endHighlightPos)
{
	if (TextFieldRenderComponent* textRender = dynamic_cast<TextFieldRenderComponent*>(RenderComponent.Get()))
	{
		if (startHighlightPos < 0 || endHighlightPos < 0)
		{
			textRender->SetHighlightRange(-1, -1);
		}
		else
		{
			bool bInvertHighlight = (textRender->GetEndHighlightPosition() <= startHighlightPos || textRender->GetStartHighlightPosition() >= endHighlightPos);

			//If inverting highlight range or switching from no highlight to highlight
			if (textRender->GetStartHighlightPosition() < 0 || textRender->GetEndHighlightPosition() < 0 || bInvertHighlight)
			{
				textRender->SetHighlightRange(startHighlightPos, endHighlightPos);
			}
			else
			{
				//Changing highlight positions (either expanding or shrinking). Use the faster shift highlight functions
				Int startShiftAmount = startHighlightPos - textRender->GetStartHighlightPosition();
				if (startShiftAmount != 0)
				{
					textRender->ShiftStartHighlight(startShiftAmount);
				}

				Int endShiftAmount = endHighlightPos - textRender->GetEndHighlightPosition();
				if (endShiftAmount != 0)
				{
					textRender->ShiftEndHighlight(endShiftAmount);
				}
			}
		}
	}
}

void TextFieldComponent::PauseBlinker ()
{
	TextFieldRenderComponent* textRender = dynamic_cast<TextFieldRenderComponent*>(GetRenderComponent());
	if (textRender != nullptr)
	{
		textRender->PauseBlinker(textRender->MoveDetectionDuration);
	}
}

void TextFieldComponent::MoveCursor (Int numChars, bool bMoveHighlight)
{
	if (!bMoveHighlight)
	{
		if (HighlightEndPosition >= 0)
		{
			//If there is text highlighted, start from the left side if moving left. Start from the right side if moving right.
			bool bMovingInward = ((CursorPosition - HighlightEndPosition) * numChars < 0); //Remember numChars can be negative to indicate moving left.
			if (bMovingInward)
			{
				SetCursorPosition(HighlightEndPosition);
			}

			SetHighlightEndPosition(-1);
		}
	}
	else if (HighlightEndPosition < 0) //and moving highlight
	{
		SetHighlightEndPosition(CursorPosition);
	}

	if (numChars < 0)
	{
		//Moving to the left
		Int curPos = (bMoveHighlight) ? HighlightEndPosition : CursorPosition;
		bool isAtBeginning = (curPos < Int::Abs(numChars));
		if (bSingleLine && isAtBeginning && !SingleLineHeaderText.IsEmpty())
		{
			//Need to perform a horizontal shift
			Int availableSpacesAfterCursor = CountHighestCursorIdx() - CursorPosition;
			Int originalTailLength = SingleLineTailText.Length();
			Int jumpAmount = (-numChars + SingleLineCursorJumpAmount) - curPos; //Remember numChars is negative
			Int headerShiftAmount = ShiftDisplayedTextTowardsHeader(jumpAmount);
			Int tailShiftAmount = SingleLineTailText.Length() - originalTailLength; //tailShiftAmount and headerShiftAmount may be different due to variable character widths.
			Int newPos = Utils::Max<Int>(headerShiftAmount - (jumpAmount - SingleLineCursorJumpAmount), 0);

			if (bMoveHighlight)
			{
				bool cursorUpdated = false;
				if (headerShiftAmount != 0)
				{
					//Check for overflow
					if (SingleLineCursorOverflow < 0)
					{
						cursorUpdated = true;

						//Handle case where the HighlightEndPosition is moving towards the cursor.
						if (SingleLineCursorOverflow * -1 >= headerShiftAmount)
						{
							//Cursor will still remain in overflow
							SingleLineCursorOverflow += headerShiftAmount;
							//Don't bother updating CursorPosition since it's still at zero.
						}
						else
						{
							Int newCursorPosition = headerShiftAmount + SingleLineCursorOverflow; //Remember that SingleLineCursorOverflow is negative
							Int numCursorPositions = CountNumCursorPositions();
							if (newCursorPosition <= numCursorPositions)
							{
								//Cursor is moving towards displayed text
								SetCursorPosition(newCursorPosition);
								SingleLineCursorOverflow = 0;
							}
							else
							{
								//The cursor is jumping from the head to the tail.
								SetCursorPosition(numCursorPositions);
								SingleLineCursorOverflow = numCursorPositions - numCursorPositions;
							}
						}
					}
					else
					{
						//Handle case where the HighlightEndPosition is moving away from the cursor.
						bool bCursorOverflow = (tailShiftAmount >= availableSpacesAfterCursor);
						if (bCursorOverflow)
						{
							SingleLineCursorOverflow += tailShiftAmount - availableSpacesAfterCursor;
							SetCursorPosition(SD_MAXINT); 
							cursorUpdated = true;
						}
					}
				}

				if (!cursorUpdated)
				{
					SetCursorPosition(CursorPosition + headerShiftAmount);
				}

				SetHighlightEndPosition(newPos);
			}
			else //Not moving highlight
			{
				SetCursorPosition(newPos);
			}

			UpdateCursorHorizontalPosition(newPos);
			MarkCacheDirty(); //In case the cursor position didn't change, but the characters that shifted did cause a shift horizontally due to variable glyph size.
			return;
		}
	}
	else if (bSingleLine && !SingleLineTailText.IsEmpty())
	{
		//Moving to the right
		Int lastCursorPos = CountHighestCursorIdx();
		Int cursorPos = (bMoveHighlight) ? HighlightEndPosition : CursorPosition;
		bool isAtEnd = (cursorPos > (lastCursorPos - numChars));
		if (isAtEnd)
		{
			Int originalHeaderLength = SingleLineHeaderText.Length();

			//Need to perform a horizontal shift
			Int jumpAmount = numChars + SingleLineCursorJumpAmount - (lastCursorPos - cursorPos);
			Int numTailShifts = ShiftDisplayedTextTowardsTail(jumpAmount);
			Int headerShiftAmount = SingleLineHeaderText.Length() - originalHeaderLength; //numTailShifts and headerShiftAmount may be different due to variable character widths.
			Int newPos = CountHighestCursorIdx(); //snap to end
			if (!SingleLineTailText.IsEmpty())
			{
				//If there is tailing text, then add a little buffer between end character and the cursor position
				newPos = Utils::Max<Int>(newPos - (SingleLineCursorJumpAmount + 1), 0);
			}

			if (bMoveHighlight)
			{
				bool cursorUpdated = false;
				if (numTailShifts != 0)
				{
					if (SingleLineCursorOverflow > 0)
					{
						//Handle the HighlightEndPosition moving towards the CursorPosition
						cursorUpdated = true;

						if (SingleLineCursorOverflow > headerShiftAmount)
						{
							SingleLineCursorOverflow -= headerShiftAmount;
							//Don't bother updating the cursor position since it's still out of range.
						}
						else
						{
							Int newCursorPosition = CountHighestCursorIdx() - (numTailShifts - SingleLineCursorOverflow);
							if (newCursorPosition >= 0)
							{
								//Cursor is moving from tail to displayed text
								SetCursorPosition(newCursorPosition);
								SingleLineCursorOverflow = 0;
							}
							else
							{
								//The shift is jumping to the other side of the CursorPosition. The cursor needs to move from tail to head
								SetCursorPosition(0);
								SingleLineCursorOverflow = newCursorPosition;
							}
						}
					}
					else
					{
						//Handle case where the HighlightEndPosition is moving away from CursorPosition.
						if (CursorPosition < headerShiftAmount)
						{
							//SingleLineCursorOverflow -= (numShifts - CursorPosition);
							SingleLineCursorOverflow -= (headerShiftAmount - CursorPosition);
							SetCursorPosition(0);
							cursorUpdated = true;
						}
					}
				}

				if (!cursorUpdated)
				{
					SetCursorPosition(CursorPosition - headerShiftAmount);
				}

				SetHighlightEndPosition(newPos);
			}
			else //Not moving the highlight
			{
				SetCursorPosition(newPos);
			}

			UpdateCursorHorizontalPosition(newPos);
			MarkCacheDirty(); //In case the cursor position didn't change, but the characters that shifted did cause a shift horizontally due to variable glyph size.
			return;
		}
	}
	
	//Move normally
	if (bMoveHighlight)
	{
		SetHighlightEndPosition(Utils::Max<Int>(HighlightEndPosition + numChars, 0));
		UpdateCursorHorizontalPosition(HighlightEndPosition);
	}
	else
	{
		SetCursorPosition(Utils::Max<Int>(CursorPosition + numChars, 0), (numChars > 0));
		UpdateCursorHorizontalPosition(CursorPosition);
	}
}

void TextFieldComponent::JumpCursorToPrevWord (bool bMoveHighlight)
{
	if (CursorPosition < 0)
	{
		return;
	}

	Int localCursorPosition;
	Int lineIdx;
	if (bMoveHighlight)
	{
		if (HighlightEndPosition < 0)
		{
			SetHighlightEndPosition(CursorPosition);
		}

		lineIdx = GetCursorLineData(HighlightEndPosition, OUT localCursorPosition);
	}
	else
	{
		SetHighlightEndPosition(-1);
		lineIdx = GetCursorLineData(CursorPosition, OUT localCursorPosition);
	}

	CHECK(lineIdx != INT_INDEX_NONE)
	DString curLine = GetLine(lineIdx);

	StringIterator iter(&curLine);
	iter += (localCursorPosition - 1);

	Int newLocalPosition = localCursorPosition - 1;
	if (iter.IsAtBeginning() && lineIdx > 0)
	{
		//Jump to the beginning of the last word in the previous line.
		--lineIdx;
		--newLocalPosition;
		curLine = GetLine(lineIdx);
		iter = StringIterator(&curLine, false);
	}

	if (!iter.IsAtBeginning())
	{
		--iter; //ignore current character
		--newLocalPosition;

		//Keep iterating backwards until we find a nonspace character (handles cases where there are multiple spaces)
		while (*iter == ' ' && !iter.IsAtBeginning())
		{
			//Already at a space character, keep navigating backwards until we find a non space character
			--iter;
			--newLocalPosition;
		}

		//Now keep decrementing until we find a space character
		while (!iter.IsAtBeginning())
		{
			if (*iter == ' ')
			{
				++newLocalPosition; //Make sure it's after the space character
				break;
			}

			--iter;
			--newLocalPosition;
		}
	}

	if (bSingleLine && newLocalPosition == 0 && !SingleLineHeaderText.IsEmpty()) //If cursor is jumping to position 0 while there are more characters found in the header
	{
		//Keep navigating backwards until we find the beginning of the header or a space character.
		for (StringIterator iter(&SingleLineHeaderText, false); true; --iter)
		{
			if (*iter == ' ')
			{
				break;
			}

			--newLocalPosition;
			if (iter.IsAtBeginning())
			{
				break;
			}
		}
	}

	Int deltaMove = newLocalPosition - localCursorPosition;
	MoveCursor(deltaMove, bMoveHighlight);
}

void TextFieldComponent::JumpCursorToNextWord (bool bMoveHighlight)
{
	if (CursorPosition < 0)
	{
		return;
	}

	Int localCursorPosition;
	Int lineIdx;
	if (bMoveHighlight)
	{
		if (HighlightEndPosition < 0)
		{
			SetHighlightEndPosition(CursorPosition);
		}

		lineIdx = GetCursorLineData(HighlightEndPosition, OUT localCursorPosition);
	}
	else
	{
		SetHighlightEndPosition(-1);
		lineIdx = GetCursorLineData(CursorPosition, OUT localCursorPosition);
	}

	CHECK(lineIdx != INT_INDEX_NONE)
	DString curLine = GetLine(lineIdx);

	StringIterator iter(&curLine);
	iter += localCursorPosition;

	bool bFoundSpace = false;
	Int newLocalPosition = localCursorPosition;
	for ( ; newLocalPosition < curLine.Length(); newLocalPosition++)
	{
		if (!bFoundSpace)
		{
			//Keep moving until we found a space character
			bFoundSpace = (*iter == ' ');
		}
		else if (*iter != ' ') //found a non space character after the first space character.
		{
			break; //Found a character after a space position.
		}

		++iter;
	}

	Int deltaCursorPos = newLocalPosition - localCursorPosition;
	if (deltaCursorPos <= 0 && !bSingleLine)
	{
		//Cursor is already at the end of the line. Jump to the next word on the next line
		size_t nextLineIdx = lineIdx.ToUnsignedInt();
		++nextLineIdx;
		if (nextLineIdx >= Content.size())
		{
			return; //already at the end
		}

		bFoundSpace = false;
		for (StringIterator nextLineIter(&Content.at(nextLineIdx)); !nextLineIter.IsAtEnd(); ++nextLineIter)
		{
			if (!bFoundSpace)
			{
				bFoundSpace = (*nextLineIter == ' ');
			}
			else if (*nextLineIter != ' ')
			{
				break;
			}

			++deltaCursorPos;
		}
	}

	Int targetCursorPos = (bMoveHighlight) ? HighlightEndPosition : CursorPosition;

	//For single lines, if there is tailing text, and the cursor is jumping to the end of displayed text. Add +1 in the num cursor position check since there's an extra cursor position for the end position.
	if (bSingleLine && !SingleLineTailText.IsEmpty() && targetCursorPos + deltaCursorPos + 1 >= CountNumCursorPositions())
	{
		bFoundSpace = false;
		for (StringIterator iter(&SingleLineTailText); !iter.IsAtEnd(); ++iter)
		{
			DString curChar = *iter;
			if (!bFoundSpace)
			{
				bFoundSpace = (curChar == ' ');
			}
			else if (curChar != ' ')
			{
				break;
			}

			++deltaCursorPos;
		}
	}

	MoveCursor(deltaCursorPos, bMoveHighlight);
}

void TextFieldComponent::JumpCursorToBeginningLine (bool bMoveHighlight)
{
	if (bSingleLine)
	{
		JumpCursorToBeginning(bMoveHighlight);
		return;
	}

	Int skippedChars = 0;

	for (size_t i = 0; i < Content.size(); i++)
	{
		if (i >= ActiveLineIndex)
		{
			break;
		}

		skippedChars += Content.at(i).Length() + 1; //include end line character
	}

	if (bMoveHighlight)
	{
		SetHighlightEndPosition(skippedChars);
	}
	else
	{
		SetHighlightEndPosition(-1);
		SetCursorPosition(skippedChars);
	}

	UpdateCursorHorizontalPosition(skippedChars);
}

void TextFieldComponent::JumpCursorToEndLine (bool bMoveHighlight)
{
	if (bSingleLine)
	{
		JumpCursorToEnd(bMoveHighlight);
		return;
	}

	Int skippedChars = 0;

	for (size_t i = 0; i < Content.size(); i++)
	{
		skippedChars += Content.at(i).Length() + 1; //include end line character
		if (i >= ActiveLineIndex)
		{
			break;
		}
	}
	skippedChars--; //Move cursor at the end of current line instead of moving it to beginning of next line.

	if (bMoveHighlight)
	{
		SetHighlightEndPosition(skippedChars);
	}
	else
	{
		SetHighlightEndPosition(-1);
		SetCursorPosition(skippedChars);
	}

	UpdateCursorHorizontalPosition(skippedChars);
}

void TextFieldComponent::JumpCursorToLine (Int deltaLines, bool bMoveHighlight)
{
	if (bSingleLine)
	{
		if (deltaLines <= 0)
		{
			JumpCursorToBeginning(bMoveHighlight);
		}
		else
		{
			JumpCursorToEnd(bMoveHighlight);
		}

		return;
	}

	Int localCursorPosition;
	Int oldLineIdx;
	
	if (bMoveHighlight)
	{
		if (HighlightEndPosition < 0)
		{
			SetHighlightEndPosition(CursorPosition);
		}

		oldLineIdx = GetCursorLineData(HighlightEndPosition, OUT localCursorPosition);
	}
	else
	{
		oldLineIdx = GetCursorLineData(CursorPosition, OUT localCursorPosition);
	}

	if (oldLineIdx < 0)
	{
		return; //cursor not placed
	}

	Int newLineIdx = Utils::Clamp<Int>(oldLineIdx + deltaLines, 0, Content.size() - 1);
	const DString& oldLine = Content.at(oldLineIdx.ToUnsignedInt());
	const DString& newLine = Content.at(newLineIdx.ToUnsignedInt());

	Int newLineCursorIdx = GetFont()->FindCharNearWidthLimit(newLine, GetCharacterSize(), HorizontalCursorPosition);
	newLineCursorIdx++; //Include the character at cursorPosX

	//Identify how many cursor positions we'll need to move between current position to newLineCursorIdx
	Int newCursorPos = 0;
	if (deltaLines > 0)
	{
		newCursorPos = (bMoveHighlight) ? HighlightEndPosition : CursorPosition;
		newCursorPos += (oldLine.Length() + 1) - localCursorPosition; //Calc how many positions need to skip to exit cur line.
		newCursorPos += newLineCursorIdx; //Move from beginning of new line to specific spot in new line.

		//If skipping over multiple lines, we need to count how many additional cursor positions we'll need to skip over.
		for (Int i = oldLineIdx + 1; i < newLineIdx; i++)
		{
			newCursorPos += Content.at(i.ToUnsignedInt()).Length() + 1;
		}
	}
	else
	{
		newCursorPos = (bMoveHighlight) ? HighlightEndPosition : CursorPosition;
		newCursorPos -= (localCursorPosition + 1);
		newCursorPos -= (newLine.Length() - newLineCursorIdx);

		//If skipping over multiple lines, we need to count how many additional cursor positions we'll need to skip over.
		for (Int i = oldLineIdx - 1; i > newLineIdx; i--)
		{
			newCursorPos -= (Content.at(i.ToUnsignedInt()).Length() + 1);
		}

		newCursorPos = Utils::Max<Int>(newCursorPos, 0); //Jump cursor to beginning of first line if user is moving up from first line.
	}

	if (bMoveHighlight)
	{
		SetHighlightEndPosition(newCursorPos);
	}
	else
	{
		SetHighlightEndPosition(-1);
		SetCursorPosition(newCursorPos);
	}
}

Int TextFieldComponent::ShiftDisplayedTextTowardsHeader (Int jumpAmount)
{
	if (jumpAmount < 0)
	{
		return ShiftDisplayedTextTowardsTail(-jumpAmount);
	}

	if (SingleLineHeaderText.IsEmpty())
	{
		return 0; //Nothing to shift
	}

	const Font* curFont = GetFont();
	CHECK(curFont != nullptr)

	Int originalHeaderLength = SingleLineHeaderText.Length();
	Int firstDisplayedChar = Utils::Max<Int>(SingleLineHeaderText.Length() - jumpAmount, 0);
	DString displayedText = SingleLineHeaderText.SubString(firstDisplayedChar) + GetDisplayedText();
	SingleLineHeaderText = (firstDisplayedChar > 0) ? SingleLineHeaderText.SubString(0, firstDisplayedChar - 1) : DString::EmptyString;

	//Need to calculate how many characters can fit in the space.
	Int lastDisplayedChar = 0;
	Float curWidth = 0.f;
	for (StringIterator iter(&displayedText); !iter.IsAtEnd(); ++iter)
	{
		curWidth += curFont->CalculateStringWidth(iter.GetString(), GetCharacterSize());
		if (curWidth >= ReadCachedAbsSize().X)
		{
			--lastDisplayedChar; //Do not include the character that would be going beyond the width.
			break;
		}

		++lastDisplayedChar;
	}

	if (curWidth >= ReadCachedAbsSize().X)
	{
		if (lastDisplayedChar < displayedText.Length())
		{
			SingleLineTailText.Insert(0, displayedText.SubString(lastDisplayedChar + 1));
			displayedText = displayedText.SubString(0, lastDisplayedChar);
		}
	}
	else if (!SingleLineTailText.IsEmpty())
	{
		Int numCharsFromTail = 0;

		//There is more space available, start adding characters from the tail towards displayed text
		for (StringIterator iter(&SingleLineTailText); !iter.IsAtEnd(); ++iter)
		{
			curWidth += curFont->CalculateStringWidth(iter.GetString(), GetCharacterSize());
			if (curWidth >= ReadCachedAbsSize().X)
			{
				break;
			}

			++numCharsFromTail;
		}

		if (numCharsFromTail > 0)
		{
			displayedText += SingleLineTailText.SubString(0, numCharsFromTail - 1);
			SingleLineTailText.Remove(0, numCharsFromTail);
		}
	}

	//Call Super::SetText instead of SetText since TextFieldComponents override SetText to handle SingleLine truncation.
	Super::SetText(displayedText);

	return (originalHeaderLength - SingleLineHeaderText.Length());
}

Int TextFieldComponent::ShiftDisplayedTextTowardsTail (Int jumpAmount)
{
	if (jumpAmount < 0)
	{
		return ShiftDisplayedTextTowardsHeader(-jumpAmount);
	}

	if (SingleLineTailText.IsEmpty())
	{
		return 0; //Nothing to shift
	}

	const Font* curFont = GetFont();
	CHECK(curFont != nullptr)

	Int originalTailLength = SingleLineTailText.Length();
	Int lastDisplayedCharIdx = Utils::Min<Int>(SingleLineTailText.Length() - 1, jumpAmount);
	DString displayedText = GetDisplayedText() + SingleLineTailText.SubString(0, lastDisplayedCharIdx);
	SingleLineTailText = (lastDisplayedCharIdx < SingleLineTailText.Length()) ? SingleLineTailText.SubString(lastDisplayedCharIdx + 1) : DString::EmptyString;

	Int firstDisplayedCharIdx = displayedText.Length() - 1;
	Float curWidth = 0.f;
	for (StringIterator iter(&displayedText, false); true; --iter)
	{
		curWidth += curFont->CalculateStringWidth(iter.GetString(), GetCharacterSize());
		if (curWidth >= ReadCachedAbsSize().X)
		{
			++firstDisplayedCharIdx; //Do not include the character that would be going beyond the width.
			break;
		}

		if (iter.IsAtBeginning() || firstDisplayedCharIdx <= 0)
		{
			break;
		}

		--firstDisplayedCharIdx;
	}

	if (curWidth >= ReadCachedAbsSize().X)
	{
		if (firstDisplayedCharIdx > 0)
		{
			SingleLineHeaderText += displayedText.SubString(0, firstDisplayedCharIdx - 1);
			displayedText.Remove(0, firstDisplayedCharIdx);
		}
	}
	else if (!SingleLineHeaderText.IsEmpty())
	{
		Int numCharsFromHead = 0;

		//There's more space available, start adding characters from the header towards the displayed text
		for (StringIterator iter(&SingleLineHeaderText, false); true; --iter)
		{
			curWidth += curFont->CalculateStringWidth(iter.GetString(), GetCharacterSize());
			if (curWidth >= ReadCachedAbsSize().X)
			{
				break;
			}

			++numCharsFromHead;
			if (iter.IsAtBeginning())
			{
				break;
			}
		}

		if (numCharsFromHead > 0)
		{
			Int firstCharFromHeaderIdx = SingleLineHeaderText.Length() - numCharsFromHead;
			displayedText.Insert(0, SingleLineHeaderText.SubString(firstCharFromHeaderIdx));
			SingleLineHeaderText.Remove(firstCharFromHeaderIdx, numCharsFromHead);
		}
	}

	//Call Super::SetText instead of SetText since TextFieldComponents override SetText to handle SingleLine truncation.
	Super::SetText(displayedText);

	return (originalTailLength - SingleLineTailText.Length());
}

bool TextFieldComponent::ExecuteTextFieldInput (const sf::Event& keyEvent)
{
	if (CursorPosition < 0)
	{
		return false;
	}

	if (!bSelectable && !bEditable)
	{
		return false;
	}

	bool bIsReleaseEvent = (keyEvent.type == sf::Event::KeyReleased);
	if (!bIsReleaseEvent)
	{
		bIsCtrlCmd = (InputBroadcaster::GetCtrlHeld());
	}

	const std::function<void(const Vector2&)> shiftScrollPosition([&](const Vector2& deltaMove)
	{
		if (ScrollableInterface* scrollInterface = dynamic_cast<ScrollableInterface*>(GetRootEntity()))
		{
			if (scrollInterface->OnScrollToPosition.IsBounded())
			{
				scrollInterface->OnScrollToPosition(deltaMove, true);
			}
		}
	});

	if (InputBroadcaster::GetCtrlHeld() || bIsCtrlCmd)
	{
		if (bIsReleaseEvent && keyEvent.key.code != sf::Keyboard::LControl && keyEvent.key.code != sf::Keyboard::RControl)
		{
			bIsCtrlCmd = false;
		}

		switch (keyEvent.key.code)
		{
			case(sf::Keyboard::A):
				if (bIsReleaseEvent)
				{
					SelectAll();
				}
				return true;

			case(sf::Keyboard::C):
				if (bIsReleaseEvent)
				{
					OS_CopyToClipboard(GetSelectedText());
				}
				return true;

			case(sf::Keyboard::V):
				if (bEditable && bIsReleaseEvent)
				{
					DString pastedText = OS_PasteFromClipboard();
					if (!pastedText.IsEmpty())
					{
						DeleteSelectedText();
						InsertTextAtCursor(pastedText);
					}
				}

				return true;

			case(sf::Keyboard::X):
				if (bIsReleaseEvent)
				{
					OS_CopyToClipboard(GetSelectedText());
					if (bEditable)
					{
						DeleteSelectedText();
					}
				}
				return true;

			case(sf::Keyboard::End):
				if (!bIsReleaseEvent)
				{
					//Select text from current position to the end
					JumpCursorToEnd(InputBroadcaster::GetShiftHeld());
				}
				return true;

			case(sf::Keyboard::Home):
				if (!bIsReleaseEvent)
				{
					JumpCursorToBeginning(InputBroadcaster::GetShiftHeld());
				}
				return true;

			case(sf::Keyboard::Left):
				if (!bIsReleaseEvent)
				{
					JumpCursorToPrevWord(InputBroadcaster::GetShiftHeld());
				}
				return true;

			case(sf::Keyboard::Right):
				if (!bIsReleaseEvent)
				{
					JumpCursorToNextWord(InputBroadcaster::GetShiftHeld());
				}
				return true;

			case(sf::Keyboard::Up):
				if (!bIsReleaseEvent)
				{
					shiftScrollPosition(Vector2(0.f, -GetLineHeight()));
				}

				return true;

			case(sf::Keyboard::Down):
				if (!bIsReleaseEvent)
				{
					shiftScrollPosition(Vector2(0.f, GetLineHeight()));
				}

				return true;

			case (sf::Keyboard::Delete):
				if (bIsReleaseEvent || !bEditable)
				{
					return true;
				}

				if (HighlightEndPosition >= 0)
				{
					DeleteSelectedText();
				}
				else
				{
					//Delete rest of line from cursor position
					JumpCursorToEndLine(true);
					DeleteSelectedText();
					InsertTextAtCursor(TXT("\n"));
				}
				PauseBlinker();

				return true;
		}
	}

	switch (keyEvent.key.code)
	{
		case(sf::Keyboard::Escape):
			if (bIsReleaseEvent)
			{
				SetCursorPosition(-1);
				SetText(OriginalContent);
			}
			return true;

		case(sf::Keyboard::Home):
			if (!bIsReleaseEvent)
			{
				JumpCursorToBeginningLine(InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::End):
			if (!bIsReleaseEvent)
			{
				JumpCursorToEndLine(InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::PageUp):
			if (!bIsReleaseEvent)
			{
				JumpCursorToLine(-5, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::PageDown):
			if (!bIsReleaseEvent)
			{
				JumpCursorToLine(5, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::Left):
			if (!bIsReleaseEvent)
			{
				MoveCursor(-1, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::Right):
			if (!bIsReleaseEvent)
			{
				MoveCursor(1, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::Up):
			if (!bIsReleaseEvent)
			{
				JumpCursorToLine(-1, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::Down):
			if (!bIsReleaseEvent)
			{
				JumpCursorToLine(1, InputBroadcaster::GetShiftHeld());
			}
			return true;

		case(sf::Keyboard::Delete):
			if (bIsReleaseEvent || !bEditable)
			{
				return true;
			}

			if (HighlightEndPosition >= 0)
			{
				DeleteSelectedText();
			}
			else
			{
				Int originalHeaderLength = SingleLineHeaderText.Length();

				//delete next character
				DeleteAdjacentCharacter(true);

				Int shiftAmount = originalHeaderLength - SingleLineHeaderText.Length(); //In case a character shifted from header to displayed
				if (shiftAmount != 0)
				{
					SetCursorPosition(CursorPosition + shiftAmount);
				}
			}

			//Forcibly make cursor render solid since the cursor position did not change here.
			PauseBlinker();

			return true;

	}

	//Capture input anyway since this text field is focused
	return true;
}

bool TextFieldComponent::ExecuteTextFieldText (const sf::Event& keyEvent)
{
	if (CursorPosition < 0 || InputBroadcaster::GetCtrlHeld() || !bEditable)
	{
		return false;
	}

	DString msg = sf::String(keyEvent.text.unicode).toAnsiString();
	switch (keyEvent.text.unicode)
	{
		case(8): //backspace
		{
			if (HighlightEndPosition >= 0)
			{
				DeleteSelectedText();
				return true;
			}

			Int originalHeaderLength = SingleLineHeaderText.Length();
			Int oldLineIdx = ActiveLineIndex;

			bool isEmptyLineAbove = false;
			if (oldLineIdx > 0 && Content.at((oldLineIdx - 1).ToUnsignedInt()).IsEmpty())
			{
				//If there happens to be an empty line above, there's no extra cursor position to remove. This flag is used to prohibit the extra cursor shift.
				isEmptyLineAbove = true;
			}

			//Delete previous character
			DeleteAdjacentCharacter(false);

			if (bSingleLine && CursorPosition == 0)
			{
				Int shiftAmount = ShiftDisplayedTextTowardsHeader(SingleLineCursorJumpAmount);
				SetCursorPosition(Utils::Max<Int>(shiftAmount - 1, 0));
			}
			else
			{
				Int shiftAmount = originalHeaderLength - SingleLineHeaderText.Length(); //In case a character shifted from header to displayed

				Int newPos = (CursorPosition - 1) + shiftAmount;
				Int newLineIdx = GetCursorLineData(newPos);
				if (newLineIdx != oldLineIdx && !isEmptyLineAbove)
				{
					--newPos; //Offset by one since when the cursor jumps a line, it needs to move over the extra cursor position at the end of the prev line.
				}

				SetCursorPosition(Utils::Max<Int>(newPos, 0));
			}

			UpdateCursorHorizontalPosition(CursorPosition);

			return true;
		}

		case(9): //tab
			msg = TXT("\t");
			break;

		case(13): //return
			if (OnReturn.IsBounded())
			{
				SetCursorPosition(-1);
				OnReturn(this);
				return true;
			}

			msg = TXT("\n");
			break;

		case(27): //escape
			//Escaping from text field will be handled in the InputKey event handler.
			return true;
	}

	if (OnAllowTextInput.IsBounded() && !OnAllowTextInput(msg))
	{
		return false;
	}

	if (HighlightEndPosition >= 0)
	{
		DeleteSelectedText();
	}

	InsertTextAtCursor(msg);
	return true;
}

bool TextFieldComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	if (OverridingMouseIcon.IsNullptr())
	{
		return false;
	}

	bool isHovering = ((bEditable || bSelectable) && IsWithinBounds(Vector2(Float(sfmlEvent.x), Float(sfmlEvent.y))));
	if (!isHovering)
	{
		OverridingMouseIcon = nullptr;
	}

	return (OverridingMouseIcon.IsValid());
}

void TextFieldComponent::HandleSingleLineHighlightDrag (Float deltaSec)
{
	CHECK(SingleLineHighlightDragTick != nullptr)

	if (!bDraggingOverText)
	{
		SingleLineHighlightDragTick->SetTicking(false);
		return;
	}

	if (LatestDragMousePosition.X < ReadCachedAbsPosition().X - SingleLineHighlightDragBorderRange.Min)
	{
		//Drag to the left
		if (SingleLineHeaderText.IsEmpty())
		{
			SetHighlightEndPosition(0);
			return; //Already at the very left edge
		}

		Float dist = ReadCachedAbsPosition().X - LatestDragMousePosition.X;
		Float alpha = Utils::Clamp<Float>(SingleLineHighlightDragBorderRange.CalcAlpha(dist), 0.f, 1.f);
		Float moveAmount = Utils::Lerp(alpha, SingleLineHighlightDragCharRate.Min, SingleLineHighlightDragCharRate.Max);
		moveAmount /= SingleLineHighlightDragTick->GetTickInterval();
		MoveCursor(Utils::Min<Int>(-moveAmount.ToInt(), -1), true);
	}
	else if (LatestDragMousePosition.X > ReadCachedAbsPosition().X + ReadCachedAbsSize().X)
	{
		//Drag to the right
		if (SingleLineTailText.IsEmpty())
		{
			SetHighlightEndPosition(CountHighestCursorIdx());
			return; //Already at the very right edge
		}

		Float dist = LatestDragMousePosition.X - (ReadCachedAbsPosition().X + ReadCachedAbsSize().X);
		Float alpha = Utils::Clamp<Float>(SingleLineHighlightDragBorderRange.CalcAlpha(dist), 0.f, 1.f);
		Float moveAmount = Utils::Lerp(alpha, SingleLineHighlightDragCharRate.Min, SingleLineHighlightDragCharRate.Max);
		moveAmount /= SingleLineHighlightDragTick->GetTickInterval();
		MoveCursor(Utils::Max<Int>(moveAmount.ToInt(), 1), true);
	}
}

bool TextFieldComponent::HandleInitContextComp (GuiEntity* menu, ContextMenuComponent* contextComp, MousePointer* mouse)
{
	if (DynamicListMenu* dynList = dynamic_cast<DynamicListMenu*>(menu))
	{
		std::vector<DynamicListMenu::SInitMenuOption> options;

		static std::vector<DString> displayText; //Only need to initialize this vector once
		if (ContainerUtils::IsEmpty(displayText))
		{
#ifdef WITH_LOCALIZATION
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)

			DString fileName = TXT("Gui");
			DString sectionName = TXT("TextFieldComponent");
			displayText =
			{
				translator->TranslateText(TXT("Cut"), fileName, sectionName),
				translator->TranslateText(TXT("Copy"), fileName, sectionName),
				translator->TranslateText(TXT("Paste"), fileName, sectionName),
				translator->TranslateText(TXT("SelectAll"), fileName, sectionName)
			};
#else
			displayText = 
			{
				TXT("Cut"),
				TXT("Copy"),
				TXT("Paste"),
				TXT("Select All")
			};
#endif
		}

		CHECK(displayText.size() >= 4)

		if (bEditable && HighlightEndPosition >= 0)
		{
			options.emplace_back(displayText.at(0), sf::Keyboard::X, KS_Ctrl, [&](BaseGuiDataElement* data)
			{
				OS_CopyToClipboard(GetSelectedText());
				if (bEditable)
				{
					DeleteSelectedText();
				}
			});
		}

		if (HighlightEndPosition >= 0)
		{
			options.emplace_back(displayText.at(1), sf::Keyboard::C, KS_Ctrl, [&](BaseGuiDataElement* data)
			{
				OS_CopyToClipboard(GetSelectedText());
			});
		}
		
		if (bEditable && CursorPosition >= 0)
		{
			DString pastedText = OS_PasteFromClipboard();
			if (!pastedText.IsEmpty())
			{
				options.emplace_back(displayText.at(2), sf::Keyboard::V, KS_Ctrl, [&](BaseGuiDataElement* data)
				{
					//Update pasted text in case the user changed the clipboard while the context menu is opened
					DString updatedpastedText = OS_PasteFromClipboard();

					if (!updatedpastedText.IsEmpty())
					{
						DeleteSelectedText();
						InsertTextAtCursor(updatedpastedText);
					}
				});
			}
		}

		if (bSelectable)
		{
			options.emplace_back(displayText.at(3), sf::Keyboard::A, KS_Ctrl, [&](BaseGuiDataElement* data)
			{
				SelectAll();
			});
		}

		if (!ContainerUtils::IsEmpty(options))
		{
			dynList->InitializeList(contextComp, mouse, options);
			return true;
		}
	}

	return false;
}
SD_END