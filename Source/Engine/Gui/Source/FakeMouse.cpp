/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FakeMouse.cpp
=====================================================================
*/

#include "FakeMouse.h"
#include "FakeMouseOwnerInterface.h"

IMPLEMENT_CLASS(SD::FakeMouse, SD::MousePointer)
SD_BEGIN

//Some very low value where all Entities that evaluates the mouse position should determine its out of bounds even if the Entity, themselves, are out of bounds.
const Vector2 FakeMouse::OUT_OF_BOUNDS(MIN_FLOAT, MIN_FLOAT);

void FakeMouse::InitProps ()
{
	Super::InitProps();

	//This mouse instance will not setup an InputComponent since it'll receive its events from the MouseOwner.
	bSetupInputComp = false;

	MouseOwner = nullptr;
}

void FakeMouse::BeginObject ()
{
	Super::BeginObject();

	//Revert mouse input component primarily because the Scrollbar will provide the input events.
	if (MouseInput.IsValid())
	{
		MouseInput->Destroy();
	}

	//Revert the mouse icon since the original mouse will handle the rendering. This mouse instance is invisible.
	if (MouseIcon.IsValid())
	{
		MouseIcon->Destroy();
	}
}

Vector2 FakeMouse::ToOuterCoordinates () const
{
	if (MouseOwner != nullptr)
	{
		return MouseOwner->FakeMouseToOuterCoordinates(ReadPosition());
	}

	return GetPosition();
}

Vector2 FakeMouse::ToWindowCoordinates () const
{
	if (OriginalMouse != nullptr)
	{
		return OriginalMouse->ToWindowCoordinates();
	}

	return GetPosition();
}

void FakeMouse::PushPositionLimit (Rectangle newLimitRegion, SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> limitCallback)
{
	if (OriginalMouse.IsValid())
	{
		Vector2 displacement = ToOuterCoordinates();
		newLimitRegion.Center -= displacement;

		OriginalMouse->PushPositionLimit(newLimitRegion, limitCallback);
	}
}

bool FakeMouse::RemovePositionLimit (SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback)
{
	if (OriginalMouse.IsValid())
	{
		return OriginalMouse->RemovePositionLimit(targetCallback);
	}

	return false;
}

void FakeMouse::PushMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction)
{
	if (!callbackFunction.IsBounded())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Cannot set a mouse pointer icon (%s) without associating a callback for this override."), newIcon->GetTextureName());
		return;
	}

	SIconOverride newOverride;
	newOverride.Icon = newIcon;
	newOverride.OverrideCallback = callbackFunction;
	MouseIconOverrideStack.emplace_back(newOverride);

	if (OriginalMouse != nullptr)
	{
		if (IconOverrideTick->IsTicking())
		{
			//If this component is ticking, assume that this mouse instance is already registered to the OriginalMouse (avoid duplicate registrations and allows to make mouse cursor updates via UpdateIcon function).
			OriginalMouse->UpdateMouseIconOverride(newIcon, SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			return;
		}

		//Since this mouse doesn't have a sprite, this instance must handle the override slightly differently.
		//Don't set the mouse icon. Instead recursively register a callback on the original mouse.
		OriginalMouse->PushMouseIconOverride(newIcon, SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));

		if (IconOverrideTick != nullptr)
		{
			IconOverrideTick->SetTicking(true);
		}
	}
}

bool FakeMouse::UpdateMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction)
{
	//Update the icon for this mouse instance in case there are multiple subscribed callbacks.
	for (size_t i = 0; i < MouseIconOverrideStack.size(); ++i)
	{
		if (MouseIconOverrideStack.at(i).OverrideCallback == callbackFunction)
		{
			if (MouseIconOverrideStack.at(i).Icon != newIcon)
			{
				MouseIconOverrideStack.at(i).Icon = newIcon;
				if (i >= MouseIconOverrideStack.size() - 1 && OriginalMouse.IsValid())
				{
					//This icon is currently being viewed, update the mouse sprite on the original mouse pointer.
					OriginalMouse->UpdateMouseIconOverride(newIcon, SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
				}
			}

			return true;
		}
	}

	return false;
}

bool FakeMouse::IsIconOverrideCurrentlyViewed (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback)
{
	//First check if the original mouse is viewing this instance's callback.
	if (OriginalMouse.IsValid() && OriginalMouse->IsIconOverrideCurrentlyViewed(SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&)))
	{
		//Then check if the callback is on the top of the stack of this instance.
		return Super::IsIconOverrideCurrentlyViewed(targetCallback);
	}

	return false;
}

void FakeMouse::SetDefaultIcon (Texture* newIcon)
{
	if (OriginalMouse.IsValid())
	{
		OriginalMouse->SetDefaultIcon(newIcon);
	}
}

void FakeMouse::SetMousePosition (const Vector2& newPos)
{
	if (OriginalMouse != nullptr)
	{
		Vector2 outerPos = ToOuterCoordinates();
		OriginalMouse->SetMousePosition(outerPos);
	}
}

void FakeMouse::SetMouseVisibility (bool bVisible)
{
	if (OriginalMouse.IsValid())
	{
		OriginalMouse->SetMouseVisibility(bVisible);
	}
}

void FakeMouse::SetLocked (bool bNewLocked)
{
	if (OriginalMouse.IsValid())
	{
		OriginalMouse->SetLocked(bNewLocked);
	}
}

void FakeMouse::SetOwningWindowHandle (Window* newOwningWindowHandle)
{
	//Noop - This Mouse instance will never be associated with a window handle. Don't bother assigning it to avoid running an object iterator on destruction.
}

void FakeMouse::EvaluateMouseIconOverrides ()
{
	//This function is mostly the same as the parent class with the exception of how the MouseIcon is handled.
	if (ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		return;
	}

	//No need to diplace the coordinates since this instance is already in the correct coordinate space.
	sf::Event::MouseMoveEvent newMoveEvent;
	newMoveEvent.x = ReadPosition().X.ToInt().ToInt32();
	newMoveEvent.y = ReadPosition().Y.ToInt().ToInt32();

	bool bPoppedOverride = false;
	while (!ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		const SIconOverride& iconOverride = MouseIconOverrideStack.at(MouseIconOverrideStack.size() - 1);
		if (iconOverride.Icon.IsNullptr() || !iconOverride.OverrideCallback.IsBounded() || !iconOverride.OverrideCallback(this, newMoveEvent))
		{
			MouseIconOverrideStack.pop_back();
			bPoppedOverride = true;
			continue;
		}

		//Update the sprite on the original mouse instance
		if (bPoppedOverride && OriginalMouse.IsValid())
		{
			OriginalMouse->UpdateMouseIconOverride(iconOverride.Icon.Get(), SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}

		return;
	}

	//At this point we're assuming the stack is empty
	CHECK(ContainerUtils::IsEmpty(MouseIconOverrideStack));

	if (IconOverrideTick != nullptr)
	{
		IconOverrideTick->SetTicking(false);
	}

	if (OriginalMouse.IsValid())
	{
		OriginalMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
	}
}

void FakeMouse::SetDoubleClickThreshold (Float newDoubleClickThreshold)
{
	if (OriginalMouse != nullptr)
	{
		OriginalMouse->SetDoubleClickThreshold(newDoubleClickThreshold);
	}
}

MousePointer* FakeMouse::GetRootMouse ()
{
	if (OriginalMouse != nullptr)
	{
		return OriginalMouse->GetRootMouse();
	}

	return this;
}

Float FakeMouse::GetDoubleClickThreshold () const
{
	if (OriginalMouse != nullptr)
	{
		//The owning mouse should hold the double click threshold since the FakeMouse doesn't read from the config file.
		return OriginalMouse->GetDoubleClickThreshold();
	}

	return DoubleClickThreshold;
}

Window* FakeMouse::GetOwningWindowHandle () const
{
	if (OriginalMouse != nullptr)
	{
		return OriginalMouse->GetOwningWindowHandle();
	}

	return OwningWindowHandle.Get();
}

void FakeMouse::Destroyed ()
{
	if (!ContainerUtils::IsEmpty(MouseIconOverrideStack) && OriginalMouse.IsValid())
	{
		OriginalMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, FakeMouse, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		ContainerUtils::Empty(OUT MouseIconOverrideStack);
	}

	Super::Destroyed();
}

void FakeMouse::LoadMouseSettings ()
{
	//Noop - Improve performance by not having each FakeMouse instance read from the Input.ini. Instead the settings will be pulled from the OriginalMouse.
}

void FakeMouse::SetMouseOwner (FakeMouseOwnerInterface* newMouseOwner)
{
	MouseOwner = newMouseOwner;
}

void FakeMouse::SetOriginalMouse (MousePointer* newOriginalMouse)
{
	OriginalMouse = newOriginalMouse;
}

bool FakeMouse::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt)
{
	return !ContainerUtils::IsEmpty(MouseIconOverrideStack);
}
SD_END