/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FrameComponent.cpp
=====================================================================
*/

#include "FrameComponent.h"

IMPLEMENT_CLASS(SD::FrameComponent, SD::GuiComponent)
SD_BEGIN

/* Various flags that are associated with a EBorder type. */
const Int FrameComponent::HorizontalBorderFlag = 1;
const Int FrameComponent::VerticalBorderFlag = 2;

void FrameComponent::InitProps ()
{
	Super::InitProps();

	ConsumesInput = false;

	bLockedFrame = false;
	MinDragSize = Vector2(0.f, 0.f);
	MaxDragSize = Vector2(MAX_FLOAT, MAX_FLOAT);
	HorGrabbedBorder = B_None;
	VertGrabbedBorder = B_None;
	SetBorderThickness(3.f);
	HoverFlags = 0;
	CenterTransform = nullptr;
}

void FrameComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const FrameComponent* frameTemplate = dynamic_cast<const FrameComponent*>(objTemplate);
	if (frameTemplate != nullptr)
	{
		bool bCreatedObj;
		BorderComp = ReplaceTargetWithObjOfMatchingClass(BorderComp.Get(), frameTemplate->BorderComp.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(BorderComp))
			{
				BorderComp->OnThicknessChanged = SDFUNCTION_1PARAM(this, FrameComponent, HandleBorderThicknessChanged, void, Float);
			}
		}

		if (BorderComp != nullptr)
		{
			BorderComp->CopyPropertiesFrom(frameTemplate->BorderComp.Get());
		}

		CenterComp = ReplaceTargetWithObjOfMatchingClass(CenterComp.Get(), frameTemplate->CenterComp.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			CHECK(CenterTransform != nullptr)
			CenterTransform->AddComponent(CenterComp);
		}

		if (CenterComp.IsValid())
		{
			CenterComp->CopyPropertiesFrom(frameTemplate->CenterComp.Get());
		}

		SetBorderThickness(frameTemplate->GetBorderThickness());
		SetLockedFrame(frameTemplate->GetLockedFrame());
		SetMinDragSize(frameTemplate->MinDragSize);
		SetMaxDragSize(frameTemplate->MaxDragSize);
	}
}

void FrameComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (HoverFlags > 0)
	{
		IconOverridePointer = mouse;
	}

	if (bLockedFrame)
	{
		return;
	}

	//Update mouse pointer if hovering over borders
	if (HorGrabbedBorder == B_None && VertGrabbedBorder == B_None)
	{
		//Update the mouse icon if necessary
		if (ShouldOverrideMouseIcon(mouse, Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))) && IconOverrideTexture.IsValid())
		{
			mouse->PushMouseIconOverride(IconOverrideTexture.Get(), SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}

		return;
	}

	// User is grabbing the borders.  Reposition and/or rescale the borders.
	CalculateTransformationChanges(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
}

void FrameComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//Release grabbing borders
	if (eventType == sf::Event::MouseButtonReleased)
	{
		if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
		{
			HorGrabbedBorder = B_None;
			VertGrabbedBorder = B_None;
			if (OnBorderGrip.IsBounded())
			{
				OnBorderGrip(false);
			}
		}
	}
}

bool FrameComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (!IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		return false;
	}

	//Frames only listen to left mouse button
	if (sfmlEvent.button != sf::Mouse::Left || eventType != sf::Event::MouseButtonPressed)
	{
		//Consume all input when clicking inside this frame
		return ConsumesInput;
	}

	if (bLockedFrame)
	{
		//Consume all input when clicking inside this frame
		return ConsumesInput;
	}

	const Vector2 finalCoordinates = GetCachedAbsPosition();
	Vector2 dimensions = ReadCachedAbsSize();
	StartDragBorders = Rectangle(dimensions.X, dimensions.Y, ReadCachedAbsPosition() + (dimensions * 0.5f));
	OwnerAbsBorders = (GetRelativeTo() != nullptr) ? Rectangle(GetRelativeTo()->ReadCachedAbsSize().X, GetRelativeTo()->ReadCachedAbsSize().Y, GetRelativeTo()->ReadCachedAbsPosition() + (GetRelativeTo()->ReadCachedAbsSize() * 0.5f)) : Rectangle(MAX_FLOAT, MAX_FLOAT, Vector2::ZERO_VECTOR);

	//check vertical borders
	if (Float::MakeFloat(sfmlEvent.x) <= finalCoordinates.X + BorderThickness)
	{
		VertGrabbedBorder = B_Left;
		ClampMouseX.Min = OwnerAbsBorders.GetLesserVBorder();
		ClampMouseX.Max = StartDragBorders.GetGreaterVBorder() - (BorderThickness * 2.f);

		//Apply drag size limits
		ClampMouseX.Max = Utils::Min(ClampMouseX.Max, StartDragBorders.GetGreaterVBorder() - MinDragSize.X);
		ClampMouseX.Min = Utils::Max(ClampMouseX.Min, StartDragBorders.GetGreaterVBorder() - MaxDragSize.X);
	}
	else if (Float::MakeFloat(sfmlEvent.x) >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness)
	{
		VertGrabbedBorder = B_Right;
		ClampMouseX.Min = StartDragBorders.GetLesserVBorder() + (BorderThickness * 2.f);
		ClampMouseX.Max = OwnerAbsBorders.GetGreaterVBorder();

		//Apply drag limits
		ClampMouseX.Min = Utils::Max(ClampMouseX.Min, StartDragBorders.GetLesserVBorder() + MinDragSize.X);
		ClampMouseX.Max = Utils::Min(ClampMouseX.Max, StartDragBorders.GetLesserVBorder() + MaxDragSize.X);
	}

	//check horizontal borders
	if (Float::MakeFloat(sfmlEvent.y) <= finalCoordinates.Y + BorderThickness)
	{
		HorGrabbedBorder = B_Top;

		ClampMouseY.Min = OwnerAbsBorders.GetLesserHBorder();
		ClampMouseY.Max = StartDragBorders.GetGreaterHBorder() - (BorderThickness * 2.f);

		//Apply drag size limits
		ClampMouseY.Max = Utils::Min(ClampMouseY.Max, StartDragBorders.GetGreaterHBorder() - MinDragSize.Y);
		ClampMouseY.Min = Utils::Max(ClampMouseY.Min, StartDragBorders.GetGreaterHBorder() - MaxDragSize.Y);
	}
	else if (Float::MakeFloat(sfmlEvent.y) >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness)
	{
		HorGrabbedBorder = B_Bottom;
		ClampMouseY.Min = StartDragBorders.GetLesserHBorder() + (BorderThickness * 2.f);
		ClampMouseY.Max = OwnerAbsBorders.GetGreaterHBorder();

		//Apply drag limits
		ClampMouseY.Min = Utils::Max(ClampMouseY.Min, StartDragBorders.GetLesserHBorder() + MinDragSize.Y);
		ClampMouseY.Max = Utils::Min(ClampMouseY.Max, StartDragBorders.GetLesserHBorder() + MaxDragSize.Y);
	}

	if (OnBorderGrip.IsBounded() && (VertGrabbedBorder != B_None || HorGrabbedBorder != B_None))
	{
		OnBorderGrip(true);
	}

	return (ConsumesInput || VertGrabbedBorder != B_None || HorGrabbedBorder != B_None);
}

void FrameComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	GuiTheme* curTheme = GuiTheme::GetGuiTheme();
	CHECK(curTheme != nullptr)

	//Setup default properties in case default style does not initialize them.
	BorderComp = BorderRenderComponent::CreateObject();
	if (AddComponent(BorderComp))
	{
		BorderComp->SetBorderThickness(BorderThickness);
		BorderComp->SetBorderTexture(curTheme->FrameBorder.Get());
	}

	CenterTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(CenterTransform))
	{
		CenterTransform->SetAnchorTop(BorderThickness);
		CenterTransform->SetAnchorRight(BorderThickness);
		CenterTransform->SetAnchorBottom(BorderThickness);
		CenterTransform->SetAnchorLeft(BorderThickness);

		SpriteComponent* spriteComp = SpriteComponent::CreateObject();
		if (CenterTransform->AddComponent(spriteComp))
		{
			CenterComp = spriteComp;
			spriteComp->SetSpriteTexture(curTheme->FrameFill.Get());
		}
	}
}

void FrameComponent::Destroyed ()
{
	//Remove callback in case this was destroyed while user was hovering over it
	if (IconOverridePointer.IsValid())
	{
		IconOverridePointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	Super::Destroyed();
}

void FrameComponent::SetCenterTexture (const Texture* newCenterTexture)
{
	CHECK(CenterTransform != nullptr)

	SpriteComponent* spriteComp = dynamic_cast<SpriteComponent*>(CenterComp.Get());

	if (spriteComp == nullptr && CenterComp != nullptr)
	{
		//Current component is not a sprite component. Destroy previous instance.
		CenterComp->Destroy();
	}

	if (spriteComp == nullptr)
	{
		spriteComp = SpriteComponent::CreateObject();
		if (CenterTransform->AddComponent(spriteComp))
		{
			CenterComp = spriteComp;
		}
	}

	if (spriteComp != nullptr)
	{
		spriteComp->SetSpriteTexture(newCenterTexture);
	}
}

void FrameComponent::SetCenterTexture (RenderTexture* newCenterTexture)
{
	CHECK(CenterTransform != nullptr)

	SpriteComponent* spriteComp = dynamic_cast<SpriteComponent*>(CenterComp.Get());

	if (spriteComp == nullptr && CenterComp != nullptr)
	{
		//Current component is not a sprite component. Destroy previous instance.
		CenterComp->Destroy();
	}

	if (spriteComp == nullptr)
	{
		spriteComp = SpriteComponent::CreateObject();
		if (CenterTransform->AddComponent(spriteComp))
		{
			CenterComp = spriteComp;
		}
	}

	if (spriteComp != nullptr)
	{
		spriteComp->SetSpriteTexture(newCenterTexture);
	}
}

void FrameComponent::SetCenterColor (Color newCenterColor)
{
	CHECK(CenterTransform != nullptr)

	ColorRenderComponent* colorComp = dynamic_cast<ColorRenderComponent*>(CenterComp.Get());

	if (colorComp == nullptr && CenterComp != nullptr)
	{
		//Current component is not a color component. Destroy previous instance.
		CenterComp->Destroy();
	}

	if (colorComp == nullptr)
	{
		colorComp = ColorRenderComponent::CreateObject();
		if (CenterTransform->AddComponent(colorComp))
		{
			CenterComp = colorComp;
		}
	}

	if (colorComp != nullptr)
	{
		colorComp->SolidColor = newCenterColor;
	}
}

void FrameComponent::SetColorMultiplier (Color newColorModifier)
{
	if (SpriteComponent* spriteComp = GetCenterCompAs<SpriteComponent>())
	{
		if (sf::Sprite* sprite = spriteComp->GetSprite())
		{
			sprite->setColor(newColorModifier.Source);
		}
	}
	else if (ColorRenderComponent* colorComp = GetCenterCompAs<ColorRenderComponent>())
	{
		colorComp->SolidColor = newColorModifier;
	}
}

void FrameComponent::SetLockedFrame (bool bNewLockedFrame)
{
	bLockedFrame = bNewLockedFrame;

	if (bLockedFrame)
	{
		if (OnBorderGrip.IsBounded() && (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None))
		{
			OnBorderGrip(false);
		}

		//Release mouse controls
		HorGrabbedBorder = B_None;
		VertGrabbedBorder = B_None;
		HoverFlags = 0;
	}
}

void FrameComponent::SetMinDragSize (const Vector2& newMinDragSize)
{
	MinDragSize = newMinDragSize;
}

void FrameComponent::SetMaxDragSize (const Vector2& newMaxDragSize)
{
	MaxDragSize = newMaxDragSize;
}

void FrameComponent::SetBorderThickness (Float newBorderThickness)
{
	if (BorderThickness.IsCloseTo(newBorderThickness))
	{
		return;
	}

	BorderThickness = Utils::Max(Float(0.f), newBorderThickness);

	if (BorderComp != nullptr)
	{
		BorderComp->SetBorderThickness(BorderThickness);
	}

	if (CenterTransform != nullptr)
	{
		CenterTransform->SetAnchorTop(BorderThickness);
		CenterTransform->SetAnchorRight(BorderThickness);
		CenterTransform->SetAnchorBottom(BorderThickness);
		CenterTransform->SetAnchorLeft(BorderThickness);
	}
}

bool FrameComponent::GetLockedFrame () const
{
	return bLockedFrame;
}

Float FrameComponent::GetBorderThickness () const
{
	return BorderThickness;
}

bool FrameComponent::ShouldOverrideMouseIcon (MousePointer* mouse, const Vector2& mousePos)
{
	CHECK(mouse != nullptr)
	if (bLockedFrame)
	{
		IconOverridePointer = nullptr;
		return false;
	}

	if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
	{
		return true;
	}

	Int oldHoverFlags = HoverFlags;
	HoverFlags = 0;

	if (!IsWithinBounds(mousePos))
	{
		IconOverridePointer = nullptr;
		return false; //not overriding pointer
	}

	const Vector2 finalCoordinates = GetCachedAbsPosition();

	//check vertical borders
	bool bHoveringVerticalBorder = (mousePos.X <= finalCoordinates.X + BorderThickness || mousePos.X >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness);
	bool bHoveringHorizontalBorder = (mousePos.Y <= finalCoordinates.Y + BorderThickness || mousePos.Y >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness);

	if (bHoveringVerticalBorder)
	{
		HoverFlags |= VerticalBorderFlag;
	}

	if (bHoveringHorizontalBorder)
	{
		HoverFlags |= HorizontalBorderFlag;
	}

	if (HoverFlags == 0)
	{
		IconOverridePointer = nullptr;
		return false;
	}

	if (HoverFlags != oldHoverFlags)
	{
		if (bHoveringVerticalBorder && bHoveringHorizontalBorder)
		{
			//Check if diagonal pointer should move in the \ or / direction
			bool bUpperLeft = ((mousePos.X <= finalCoordinates.X + BorderThickness && mousePos.Y <= finalCoordinates.Y + BorderThickness) ||
				(mousePos.X >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness && mousePos.Y >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness));

			IconOverrideTexture = (bUpperLeft) ? GuiTheme::GetGuiTheme()->ResizePointerDiagonalTopLeft.Get() : GuiTheme::GetGuiTheme()->ResizePointerDiagonalBottomLeft.Get();
		}
		else if ((HoverFlags & HorizontalBorderFlag) != 0)
		{
			IconOverrideTexture = GuiTheme::GetGuiTheme()->ResizePointerVertical.Get();
		}
		else if ((HoverFlags & VerticalBorderFlag) != 0)
		{
			IconOverrideTexture = GuiTheme::GetGuiTheme()->ResizePointerHorizontal.Get();
		}

		mouse->UpdateMouseIconOverride(IconOverrideTexture.Get(), SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	return (IconOverrideTexture.IsValid());
}

void FrameComponent::CalculateTransformationChanges (Float mousePosX, Float mousePosY)
{
	//Ensure the mouse positions aren't negative (ie:  dragging beyond window borders)
	mousePosX = Utils::Max<Float>(mousePosX, 0.f);
	mousePosY = Utils::Max<Float>(mousePosY, 0.f);

	Range<Float> posXLimits;
	Range<Float> posYLimits;
	Range<Float> sizeXLimits;
	Range<Float> sizeYLimits;
	CalculateTransformationLimits(posXLimits, posYLimits, sizeXLimits, sizeYLimits);

	Vector2 newSize = GetCachedAbsSize();
	Vector2 newPos = GetCachedAbsPosition();

	//handle vertical borders
	if (VertGrabbedBorder == B_Left)
	{
		newPos.X = Utils::Clamp(mousePosX, posXLimits.Min, posXLimits.Max);
		newSize.X = Utils::Clamp(StartDragBorders.GetGreaterVBorder() - newPos.X, sizeXLimits.Min, sizeXLimits.Max);
	}
	else if (VertGrabbedBorder == B_Right)
	{
		newSize.X = Utils::Clamp(mousePosX - StartDragBorders.GetLesserVBorder(), sizeXLimits.Min, sizeXLimits.Max);
	}

	//handle horizontal borders
	if (HorGrabbedBorder == B_Top)
	{
		newPos.Y = Utils::Clamp(mousePosY, posYLimits.Min, posYLimits.Max);
		newSize.Y = Utils::Clamp(StartDragBorders.GetGreaterHBorder() - newPos.Y, sizeYLimits.Min, sizeYLimits.Max);
	}
	else if (HorGrabbedBorder == B_Bottom)
	{
		newSize.Y = Utils::Clamp(mousePosY - StartDragBorders.GetLesserHBorder(), sizeYLimits.Min, sizeYLimits.Max);
	}

	Vector2 relativeTo = (GetRelativeTo() != nullptr) ? GetRelativeTo()->GetCachedAbsPosition() : Vector2::ZERO_VECTOR;

	//Snap positions to 0 if it's close to 0 to prevent the transform from converting from abs to relative.
	{
		if ((newPos.X - relativeTo.X) <= 1.f)
		{
			newPos.X = relativeTo.X;
		}

		if ((newPos.Y - relativeTo.Y) <= 1.f)
		{
			newPos.Y = relativeTo.Y;
		}
	}

	SetSize(newSize);
	SetPosition(newPos - relativeTo);
}

void FrameComponent::CalculateTransformationLimits (Range<Float>& outAbsCoordinatesXLimits, Range<Float>& outAbsCoordinatesYLimits, Range<Float>& outSizeXLimits, Range<Float>& outSizeYLimits) const
{
	if (VertGrabbedBorder == B_Left)
	{
		outSizeXLimits.Min = BorderThickness * 2.f;
		outSizeXLimits.Max = StartDragBorders.GetGreaterVBorder() - OwnerAbsBorders.GetLesserVBorder();
	}
	else if (VertGrabbedBorder == B_Right)
	{
		outSizeXLimits.Min = ClampMouseX.Min - StartDragBorders.GetLesserVBorder();
		outSizeXLimits.Max = ClampMouseX.Max - StartDragBorders.GetLesserVBorder();
	}
	outAbsCoordinatesXLimits.Min = ClampMouseX.Min;
	outAbsCoordinatesXLimits.Max = ClampMouseX.Max;

	if (HorGrabbedBorder == B_Top)
	{
		outSizeYLimits.Min = BorderThickness * 2.f;
		outSizeYLimits.Max = StartDragBorders.GetGreaterHBorder() - OwnerAbsBorders.GetLesserHBorder();
	}
	else if (HorGrabbedBorder == B_Bottom)
	{
		outSizeYLimits.Min = ClampMouseY.Min - StartDragBorders.GetLesserHBorder();
		outSizeYLimits.Max = ClampMouseY.Max - StartDragBorders.GetLesserHBorder();
	}

	outAbsCoordinatesYLimits.Min = ClampMouseY.Min;
	outAbsCoordinatesYLimits.Max = ClampMouseY.Max;
}

bool FrameComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	return ShouldOverrideMouseIcon(mouse, Vector2(Float(sfmlEvent.x), Float(sfmlEvent.y)));
}

void FrameComponent::HandleBorderThicknessChanged (Float newBorderThickness)
{
	SetBorderThickness(newBorderThickness);
}
SD_END