/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextFieldRenderComponent.cpp
=====================================================================
*/

#include "TextFieldComponent.h"
#include "TextFieldRenderComponent.h"

IMPLEMENT_CLASS(SD::TextFieldRenderComponent, SD::TextRenderComponent)
SD_BEGIN

void TextFieldRenderComponent::InitProps ()
{
	Super::InitProps();

	CursorBlinkInterval = 0.75f;
	CursorColor = sf::Color::White;
	CursorWidth = 1.5f;
	CursorHeightMultiplier = 1.25f;
	MoveDetectionDuration = 1.f;

	SolidCursorRenderEndTime = -1.f;
	StartHighlightPosition = -1;
	EndHighlightPosition = -1;
	HighlightColor = Color(48, 48, 150, 150);
	bForceRecalcDrawCoordinates = true;

	LocalEngine = nullptr;
	LastCursorMoveTime = -1.f;
}

void TextFieldRenderComponent::BeginObject ()
{
	Super::BeginObject();

	LocalEngine = Engine::FindEngine();
	CHECK(LocalEngine != nullptr)
}

void TextFieldRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextFieldRenderComponent* textTemplate = dynamic_cast<const TextFieldRenderComponent*>(objTemplate);
	if (textTemplate != nullptr)
	{
		CursorBlinkInterval = textTemplate->CursorBlinkInterval;
		CursorColor = textTemplate->CursorColor;
		CursorWidth = textTemplate->CursorWidth;
		MoveDetectionDuration = textTemplate->MoveDetectionDuration;
		SetHighlightColor(textTemplate->GetHighlightColor());
		RefreshDrawCoordinates();
	}
}

bool TextFieldRenderComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (dynamic_cast<TextFieldComponent*>(ownerCandidate) == nullptr)
	{
		return false;
	}

	return Super::CanBeAttachedTo(ownerCandidate);
}

void TextFieldRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	if (!ContainerUtils::IsEmpty(HighlightLines) && OwningTextField != nullptr)
	{
		const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);
		if (bForceRecalcDrawCoordinates || projectionData != LastProjectionData)
		{
			CalcHighlightTransforms(projectionData);
			bForceRecalcDrawCoordinates = false; //It's okay to clear the flag here since it's not possible to render the highlight and the cursor at the same time.
			LastProjectionData = projectionData;
		}

		//Draw each highlight before rendering the text
		for (const SHighlightLine& highlight : HighlightLines)
		{
			renderTarget->Draw(this, camera, highlight.DrawRect);
		}
	}

	Super::Render(renderTarget, camera); //Render text

	CHECK(renderTarget != nullptr)
	if (OwningTextField == nullptr)
	{
		return;
	}

	Int cursorPos = OwningTextField->GetCursorPosition();
	if (!ContainerUtils::IsEmpty(HighlightLines) || cursorPos < 0)
	{
		//Not rendering the cursor
		return;
	}

	if (CursorBlinkInterval <= 0)
	{
		return; //invisible cursor
	}

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);
	if (bForceRecalcDrawCoordinates || projectionData != LastProjectionData)
	{
		LastCursorMoveTime = LocalEngine->GetElapsedTime();
		CursorCoordinates = FindCursorCoordinates(projectionData, cursorPos);
		bForceRecalcDrawCoordinates = false;
		LastProjectionData = projectionData;
	}

	if (CursorCoordinates.x < 0 && CursorCoordinates.y < 0)
	{
		return; //Don't render cursor since it's at text viewport.
	}

	bool bSolidCursor = false;
	if (SolidCursorRenderEndTime > 0)
	{
		bSolidCursor = (LocalEngine->GetElapsedTime() < SolidCursorRenderEndTime);
		if (!bSolidCursor)
		{
			SolidCursorRenderEndTime = -1.f; //disable solid cursor
		}
	}

	bool bRecentlyMoved = ((LocalEngine->GetElapsedTime() - LastCursorMoveTime) < MoveDetectionDuration);
	if (bSolidCursor || bRecentlyMoved || sin((LocalEngine->GetElapsedTime().Value * PI) / CursorBlinkInterval.Value) > 0)
	{
		sf::Vector2f cursorSize;
		cursorSize.x = CursorWidth.Value;
		cursorSize.y = OwningTextField->GetCharacterSize().ToFloat().Value;
		cursorSize.y *= CursorHeightMultiplier;
		cursorSize *= projectionData.Scale;

		sf::RectangleShape cursor;
		cursor.setSize(cursorSize);
		cursor.setPosition(CursorCoordinates);
		cursor.setFillColor(CursorColor);
		renderTarget->Draw(this, camera, cursor);
	}
}

void TextFieldRenderComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningTextField = dynamic_cast<TextFieldComponent*>(newOwner);
}

void TextFieldRenderComponent::ComponentDetached ()
{
	OwningTextField = nullptr;

	Super::ComponentDetached();
}

void TextFieldRenderComponent::PauseBlinker (Float renderTime)
{
	SolidCursorRenderEndTime = LocalEngine->GetElapsedTime() + renderTime;
}

void TextFieldRenderComponent::RefreshDrawCoordinates ()
{
	bForceRecalcDrawCoordinates = true;
}

void TextFieldRenderComponent::ShiftStartHighlight (Int shiftAmount)
{
	if (StartHighlightPosition < 0 || EndHighlightPosition < 0 || shiftAmount == 0 || OwningTextField == nullptr)
	{
		return;
	}

	if (StartHighlightPosition + shiftAmount == EndHighlightPosition)
	{
		SetHighlightRange(-1, -1);
		return;
	}

	if (ContainerUtils::IsEmpty(HighlightLines))
	{
		SetHighlightRange(StartHighlightPosition + shiftAmount, EndHighlightPosition);
		return;
	}

	if (shiftAmount < 0)
	{
		Int posShift = Int::Abs(shiftAmount);

		//Expanding the highlight towards the header
		if (posShift <= HighlightLines.at(0).StartIdx)
		{
			HighlightLines.at(0).StartIdx -= posShift.ToUnsignedInt();
		}
		else
		{
			//Need to expand the header to the previous line(s)
			Int remainingShift = posShift - HighlightLines.at(0).StartIdx;
			HighlightLines.at(0).StartIdx = 0;
			if (HighlightLines.at(0).LineIdx > 0) //If not the first line
			{
				size_t curLine = HighlightLines.at(0).LineIdx - 1;
				while (remainingShift > 0)
				{
					const DString& lineText = OwningTextField->ReadContent().at(curLine);
					SHighlightLine newLine;
					newLine.DrawRect.setFillColor(HighlightColor.Source);
					newLine.LineIdx = curLine;
					newLine.EndIdx = lineText.Length().ToUnsignedInt();

					if (remainingShift > lineText.Length())
					{
						newLine.StartIdx = 0;
						remainingShift -= (lineText.Length() + 1).ToUnsignedInt();
					}
					else
					{
						newLine.StartIdx = ((lineText.Length() + 1) - remainingShift).ToUnsignedInt();
						remainingShift = 0;
					}

					HighlightLines.insert(HighlightLines.begin(), newLine);
					if (curLine == 0)
					{
						break;
					}
					--curLine;
				}
			}
		}
	}
	else
	{
		//Expanding header towards tail
		if (StartHighlightPosition + shiftAmount >= EndHighlightPosition)
		{
			//Inverting selections. Simply replace everything
			SetHighlightRange(EndHighlightPosition, StartHighlightPosition + shiftAmount);
			return;
		}

		Int remainingShift = shiftAmount;
		while (remainingShift > 0)
		{
			SHighlightLine& firstLine = HighlightLines.at(0);
			size_t numChars = firstLine.EndIdx - firstLine.StartIdx;
			if (numChars >= remainingShift)
			{
				//Partially unhighlight this line
				firstLine.StartIdx += remainingShift.ToUnsignedInt();
				remainingShift = 0;
			}
			else
			{
				//Remove this line entirely
				remainingShift -= (numChars + 1);
				HighlightLines.erase(HighlightLines.begin());
			}
		}
	}

	StartHighlightPosition += shiftAmount;
	RefreshDrawCoordinates();
}

void TextFieldRenderComponent::ShiftEndHighlight (Int shiftAmount)
{
	if (StartHighlightPosition < 0 || EndHighlightPosition < 0 || shiftAmount == 0 || OwningTextField == nullptr)
	{
		return;
	}

	if (EndHighlightPosition + shiftAmount == StartHighlightPosition)
	{
		SetHighlightRange(-1, -1);
		return;
	}

	if (ContainerUtils::IsEmpty(HighlightLines))
	{
		SetHighlightRange(StartHighlightPosition, EndHighlightPosition + shiftAmount);
		return;
	}

	if (shiftAmount < 0)
	{
		//Shrinking end highlight towards beginning
		if (EndHighlightPosition + shiftAmount <= StartHighlightPosition)
		{
			//Inverting selections. Simply replace everything.
			SetHighlightRange(EndHighlightPosition + shiftAmount, StartHighlightPosition);
			return;
		}

		Int remainingShift = Int::Abs(shiftAmount);
		while (remainingShift > 0)
		{
			SHighlightLine& lastLine = ContainerUtils::GetLast(HighlightLines);
			size_t lastPos = lastLine.EndIdx;
			if (lastPos >= remainingShift)
			{
				//Partially unhighlight this line
				lastLine.EndIdx = (lastPos - remainingShift.ToUnsignedInt());
				remainingShift = 0;
			}
			else
			{
				//Remove the last line highlight
				remainingShift -= (lastPos + 1);
				HighlightLines.pop_back();
			}
		}
	}
	else
	{
		//Expanding the end highlight towards the end
		SHighlightLine& lastLine = ContainerUtils::GetLast(HighlightLines);
		const DString& lastText = OwningTextField->ReadContent().at(lastLine.LineIdx);
		if (!IsAtEnd(lastLine) && lastText.Length() - (Int(lastLine.EndIdx) + shiftAmount) > 0)
		{
			//Partially highlight more of the last line
			lastLine.EndIdx += shiftAmount.ToUnsignedInt();
		}
		else
		{
			Int remainingShift = !IsAtEnd(lastLine) ? shiftAmount - (lastText.Length() - lastLine.EndIdx) : shiftAmount; //IsAtEnd condition explanation: handles the case where the selected line is already at the end.
			lastLine.EndIdx = lastText.Length().ToUnsignedInt();
			size_t curLineIdx = lastLine.LineIdx + 1;
			while (remainingShift > 0 && curLineIdx < OwningTextField->ReadContent().size())
			{
				SHighlightLine& newLine = HighlightLines.emplace_back();
				newLine.DrawRect.setFillColor(HighlightColor.Source);
				newLine.LineIdx = curLineIdx;
				newLine.StartIdx = 0;

				Int lineLength = OwningTextField->ReadContent().at(curLineIdx).Length();
				if (remainingShift < lineLength)
				{
					newLine.EndIdx = remainingShift.ToUnsignedInt() - 1; //-1 to include the cursor position at the beginning of the line.
					remainingShift = 0;
				}
				else
				{
					//Completely highlight this line
					newLine.EndIdx = lineLength.ToUnsignedInt();
					remainingShift -= lineLength + 1;
				}

				curLineIdx++;
			}
		}
	}

	EndHighlightPosition += shiftAmount;
	RefreshDrawCoordinates();
}

void TextFieldRenderComponent::SetHighlightRange (Int newStartHighlightPosition, Int newEndHighlightPosition)
{
#ifdef DEBUG_MODE
	CHECK(newStartHighlightPosition <= newEndHighlightPosition)
#endif

	if (newStartHighlightPosition > newEndHighlightPosition)
	{
		//Reverse the order to ensure the start position is less than end position
		SetHighlightRange(newEndHighlightPosition, newStartHighlightPosition);
		return;
	}

	if (StartHighlightPosition == newStartHighlightPosition && EndHighlightPosition == newEndHighlightPosition)
	{
		return;
	}

	StartHighlightPosition = newStartHighlightPosition;
	EndHighlightPosition = newEndHighlightPosition;
	ContainerUtils::Empty(OUT HighlightLines);
	if (StartHighlightPosition < 0 || EndHighlightPosition < 0 || StartHighlightPosition == EndHighlightPosition)
	{
		return;
	}

	SHighlightLine& firstLine = HighlightLines.emplace_back();
	firstLine.DrawRect.setFillColor(HighlightColor.Source);

	CHECK(OwningTextField != nullptr)
	size_t cursorIdx = 0;
	for (size_t i = 0; i < OwningTextField->ReadContent().size(); ++i)
	{
		Int lineLength = OwningTextField->ReadContent().at(i).Length();
		if (StartHighlightPosition >= cursorIdx + lineLength)
		{
			cursorIdx += lineLength.ToUnsignedInt() + 1; //+1 to include cursor position at the end of the line
			continue;
		}

		firstLine.LineIdx = i;
		firstLine.StartIdx = StartHighlightPosition.ToUnsignedInt() - cursorIdx;

		if (EndHighlightPosition <= cursorIdx + lineLength)
		{
			//The end highlight ends at the same line
			firstLine.EndIdx = EndHighlightPosition.ToUnsignedInt() - cursorIdx;
		}
		else
		{
			firstLine.EndIdx = lineLength.ToUnsignedInt();
		}

		cursorIdx += lineLength.ToUnsignedInt();
		break;
	}

	if (IsAtEnd(firstLine))
	{
		for (size_t i = firstLine.LineIdx + 1; i < OwningTextField->ReadContent().size(); ++i)
		{
			Int localCharIdx = cursorIdx + 1;

			//Ignore if highlighting ends at the start of the next line
			if (EndHighlightPosition == localCharIdx)
			{
				break;
			}

			SHighlightLine& nextLine = HighlightLines.emplace_back();
			nextLine.DrawRect.setFillColor(HighlightColor.Source);
			nextLine.LineIdx = i;
			nextLine.StartIdx = 0;
			Int lineLength = OwningTextField->ReadContent().at(i).Length();
			if (EndHighlightPosition > localCharIdx + lineLength)
			{
				nextLine.EndIdx = lineLength.ToUnsignedInt();
				cursorIdx += nextLine.EndIdx + 1; //+1 to include cursor position at the end of the line
				continue;
			}

			nextLine.EndIdx = (EndHighlightPosition - localCharIdx).ToUnsignedInt();
			break;
		}
	}

	RefreshDrawCoordinates();
}

void TextFieldRenderComponent::SetHighlightColor (Color newHighlightColor)
{
	HighlightColor = newHighlightColor;

	for (SHighlightLine& highlightLine : HighlightLines)
	{
		highlightLine.DrawRect.setFillColor(HighlightColor.Source);
	}
}

bool TextFieldRenderComponent::IsAtEnd (const SHighlightLine& highlightLine) const
{
	const DString& curLine = OwningTextField->ReadContent().at(highlightLine.LineIdx);
	return (highlightLine.EndIdx >= curLine.Length());
}

sf::Vector2f TextFieldRenderComponent::FindCursorCoordinates (const Transformation::SScreenProjectionData& projectionData, Int cursorIndex)
{
	CHECK(cursorIndex >= 0 && OwningTextField->GetFont() != nullptr)

	if (ContainerUtils::IsEmpty(Texts))
	{
		return projectionData.Position;
	}

	Int accumulatedPos = 0;
	size_t lineIdx = 0;
	while (lineIdx < OwningTextField->ReadContent().size())
	{
		if (accumulatedPos + OwningTextField->ReadContent().at(lineIdx).Length() < cursorIndex)
		{
			//+1 to include cursor position at end of line.
			accumulatedPos += OwningTextField->ReadContent().at(lineIdx).Length() + 1;
		}
		else
		{
			//Found the line that contains the cursor.
			break;
		}

		++lineIdx;
	}

	if (lineIdx >= OwningTextField->ReadContent().size())
	{
		return sf::Vector2f(-1.f, -1.f);
	}

	//Compute the yPosition
	Float posY = (Float::MakeFloat(lineIdx) * OwningTextField->GetLineHeight()) + OwningTextField->GetFirstLineOffset();
	posY *= projectionData.Scale.y;

	//Compute the xPosition
	Float posX = OwningTextField->GetFont()->CalculateStringWidth(OwningTextField->ReadContent().at(lineIdx), OwningTextField->GetCharacterSize(), 0, cursorIndex - accumulatedPos);
	posX += Texts.at(lineIdx).DrawOffset.x;
	posX *= projectionData.Scale.x;

	return (projectionData.Position + sf::Vector2f(posX.Value, posY.Value));
}

void TextFieldRenderComponent::CalcHighlightTransforms (const Transformation::SScreenProjectionData& projectionData)
{
	CHECK(OwningTextField != nullptr)

	const Font* curFont = OwningTextField->GetFont();
	CHECK(curFont != nullptr)

	Int charSize = OwningTextField->GetCharacterSize();

	for (SHighlightLine& highlightData : HighlightLines)
	{
		CHECK(highlightData.LineIdx < OwningTextField->ReadContent().size() && highlightData.LineIdx < Texts.size())

		sf::Vector2f newPos(0.f, 0.f);
		if (highlightData.StartIdx > 0)
		{
			newPos.x = curFont->CalculateStringWidth(OwningTextField->ReadContent().at(highlightData.LineIdx).SubString(0, highlightData.StartIdx - 1), charSize).Value;
		}
		newPos.x += Texts.at(highlightData.LineIdx).DrawOffset.x;
		newPos.y = Texts.at(highlightData.LineIdx).DrawOffset.y;
		newPos *= projectionData.Scale;
		newPos += projectionData.Position;
		highlightData.DrawRect.setPosition(newPos);

		sf::Vector2f newSize;
		if (IsAtEnd(highlightData))
		{
			//Rest of the line
			newSize.x = curFont->CalculateStringWidth(OwningTextField->ReadContent().at(highlightData.LineIdx).SubString(highlightData.StartIdx), charSize).Value;
		}
		else
		{
			//Partial line
			if (highlightData.EndIdx == 0)
			{
				//This case can happen when the highlight end position is at the beginning of the next line. The size of this highlight is zero anyways so set size to zero.
				newSize.x = 0.f;
			}
			else
			{
				newSize.x = curFont->CalculateStringWidth(OwningTextField->ReadContent().at(highlightData.LineIdx).SubString(highlightData.StartIdx, highlightData.EndIdx - 1), charSize).Value;
			}
		}
		newSize.y = charSize.ToFloat().Value;
		newSize *= projectionData.Scale;
		highlightData.DrawRect.setSize(newSize);
	}
}
SD_END