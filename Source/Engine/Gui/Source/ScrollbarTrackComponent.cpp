/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollbarTrackComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

IMPLEMENT_CLASS(SD::ScrollbarTrackComponent, SD::GuiComponent)
SD_BEGIN

void ScrollbarTrackComponent::InitProps ()
{
	Super::InitProps();

	OwningScrollbar = nullptr;
	TrackColor = nullptr;
	TravelTrackColorOwner = nullptr;
	TravelTrackColor = nullptr;
	Thumb = nullptr;
	TravelDelay = 0.75f;
	TravelInterval = 0.2f;

	bEnabled = true;
	bScaleThumb = true;
	DraggingThumbPointerOffset = -1;
	TravelDirection = 0;
	TravelTimeRemaining = -1.f;
	OnThumbChangedPosition = nullptr;
	RelevantMouse = nullptr;
}

void ScrollbarTrackComponent::BeginObject ()
{
	Super::BeginObject();

	RefreshScrollbarTrack();
}

void ScrollbarTrackComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarTrackComponent* trackTemplate = dynamic_cast<const ScrollbarTrackComponent*>(objTemplate);
	if (trackTemplate != nullptr)
	{
		TravelDelay = trackTemplate->TravelDelay;
		TravelInterval = trackTemplate->TravelInterval;
		SetScaleThumb(trackTemplate->GetScaleThumb());

		bool bCreatedObj;
		TrackColor = ReplaceTargetWithObjOfMatchingClass(TrackColor.Get(), trackTemplate->TrackColor.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(TrackColor);
		}

		if (TrackColor.IsValid())
		{
			TrackColor->CopyPropertiesFrom(trackTemplate->TrackColor.Get());
		}

		TravelTrackColor = ReplaceTargetWithObjOfMatchingClass(TravelTrackColor.Get(), trackTemplate->TravelTrackColor.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(TravelTrackColor);
		}

		if (TravelTrackColor.IsValid())
		{
			TravelTrackColor->CopyPropertiesFrom(trackTemplate->TravelTrackColor.Get());
		}

		Thumb = ReplaceTargetWithObjOfMatchingClass(Thumb.Get(), trackTemplate->Thumb.Get(), bCreatedObj);
		if (bCreatedObj && AddComponent(Thumb))
		{
			Thumb->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleThumbPressed, void, ButtonComponent*));
			Thumb->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleThumbReleased, void, ButtonComponent*));
		}

		if (Thumb.IsValid())
		{
			Thumb->CopyPropertiesFrom(trackTemplate->Thumb.Get());
		}
	}
}

void ScrollbarTrackComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningScrollbar = dynamic_cast<ScrollbarComponent_Deprecated*>(newOwner);
}

void ScrollbarTrackComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleTick, void, Float));
	}

	TrackColor = ColorRenderComponent::CreateObject();
	if (AddComponent(TrackColor))
	{
		TrackColor->SolidColor = sf::Color(96, 96, 96, 255);
	}

	TravelTrackColorOwner = GuiComponent::CreateObject();
	if (AddComponent(TravelTrackColorOwner))
	{
		TravelTrackColor = ColorRenderComponent::CreateObject();
		if (TravelTrackColorOwner->AddComponent(TravelTrackColor))
		{
			TravelTrackColorOwner->SetVisibility(false);
			TravelTrackColor->SolidColor = sf::Color(48, 48, 48, 255);
		}
		else
		{
			TravelTrackColorOwner->Destroy();
		}
	}

	InitializeThumb();
}

void ScrollbarTrackComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	RelevantMouse = mouse;

	if (TravelDirection != 0)
	{
		if (TravelTrackColor.IsValid())
		{
			CalculateTravelTrackAttributes(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
		}
		return;
	}

	if (DraggingThumbPointerOffset < 0 || Thumb.IsNullptr())
	{
		return;
	}

	Float newThumbPosY = (sfmlEvent.y - DraggingThumbPointerOffset).ToFloat();
	Thumb->SetPosition(Vector2(ReadPosition().X, newThumbPosY));
	OnThumbChangedPosition(CalculateScrollIndex());
}

bool ScrollbarTrackComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		if (DraggingThumbPointerOffset < 0 && TravelDirection == 0)
		{
			return false;
		}

		DraggingThumbPointerOffset = -1;
		TravelDirection = 0;

		if (TravelTrackColorOwner.IsValid())
		{
			TravelTrackColorOwner->SetVisibility(false);
		}

		return true;
	}

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		return false;
	}

	if (!bEnabled || !IsVisible() || !IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))) || Thumb.IsNullptr())
	{
		return false;
	}

	TravelDirection = (sfmlEvent.y > Thumb->ReadCachedAbsPosition().Y.ToInt()) ? -1 : 1;

	if (TravelTrackColorOwner.IsValid())
	{
		TravelTrackColorOwner->SetVisibility(true);
		CalculateTravelTrackAttributes(Int(sfmlEvent.x).ToFloat(), Int(sfmlEvent.y).ToFloat());
	}

	TravelTimeRemaining = TravelDelay;
	return true;
}

bool ScrollbarTrackComponent::AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(inputEvent, mousePosX, mousePosY));
}

#if 0
void ScrollbarTrackComponent::HandleSizeChange ()
{
	Super::HandleSizeChange();

	//Need to readjust scaling, position, and position clamps for the thumb.
	RefreshScrollbarTrack();
}
#endif

void ScrollbarTrackComponent::RefreshScrollbarTrack ()
{
	if (RelevantMouse.IsNullptr())
	{
		return;
	}

	if (Thumb.IsValid())
	{
		CalculateThumbScale();
		CalculateThumbPositionClamps();
		CalculateThumbPosition();
		ReevaluateThumbVisibility();
	}

	const Vector2& mousePos = RelevantMouse->ReadPosition();

	CalculateTravelTrackAttributes(mousePos.X, mousePos.Y);
}

void ScrollbarTrackComponent::SetThumbChangedPositionHandler (std::function<void(Int newScrollIndex)> newHandler)
{
	OnThumbChangedPosition = newHandler;
}

void ScrollbarTrackComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;

	if (!bEnabled)
	{
		if (TravelDirection != 0)
		{
			TravelTimeRemaining = -1.f;
		}
		TravelDirection = 0;

		if (TravelTrackColorOwner.IsValid())
		{
			TravelTrackColorOwner->SetVisibility(false);
		}

		DraggingThumbPointerOffset = -1;
	}

	ReevaluateThumbVisibility();
}

void ScrollbarTrackComponent::SetScaleThumb (bool bNewScaleThumb)
{
	bScaleThumb = bNewScaleThumb;

	if (!bScaleThumb && Thumb.IsValid() && Thumb->GetRenderComponent() != nullptr)
	{
		Vector2 textureSize(1.f, 1.f);
		if (Thumb->GetBackground() != nullptr)
		{
			if (SpriteComponent* spriteComp = Thumb->GetBackground()->GetCenterCompAs<SpriteComponent>())
			{
				textureSize = spriteComp->GetTextureSize();
			}
		}

		//restore default scale
		Thumb->SetSize(textureSize);
	}
	else
	{
		CalculateThumbScale();
	}
}

bool ScrollbarTrackComponent::ReversedDirections () const
{
	if (RelevantMouse.IsNullptr())
	{
		return false;
	}

	const Vector2& mousePos = RelevantMouse->ReadPosition();

	//bReversedDirection becomes true when a user clicks above/below the thumb, then switches sides as the thumb travels.
	bool bReversedDir = (TravelDirection > 0 && mousePos.Y > Thumb->ReadCachedAbsPosition().Y); //mouse below the thumb as thumb travels up
	bReversedDir = bReversedDir || (TravelDirection < 0 && mousePos.Y < Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y); //mouse above thumb as thumb travels down

	return bReversedDir;
}

bool ScrollbarTrackComponent::GetEnabled () const
{
	return bEnabled;
}

bool ScrollbarTrackComponent::GetScaleThumb () const
{
	return bScaleThumb;
}

void ScrollbarTrackComponent::InitializeThumb ()
{
	if (Thumb.IsNullptr())
	{
		Thumb = ButtonComponent::CreateObject();
		if (AddComponent(Thumb))
		{
			if (Thumb->GetRenderComponent() != nullptr)
			{
#if 0
				Thumb->GetBackground()->RenderComponent->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarTrackFill.Get());
				Thumb->GetBackground()->RenderComponent->TopBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTop;
				Thumb->GetBackground()->RenderComponent->RightBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderRight;
				Thumb->GetBackground()->RenderComponent->BottomBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottom;
				Thumb->GetBackground()->RenderComponent->LeftBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderLeft;
				Thumb->GetBackground()->RenderComponent->TopRightCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTopRight;
				Thumb->GetBackground()->RenderComponent->BottomRightCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottomRight;
				Thumb->GetBackground()->RenderComponent->BottomLeftCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottomLeft;
				Thumb->GetBackground()->RenderComponent->TopLeftCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTopLeft;
#endif
				Thumb->GetBackground()->SetBorderThickness(2);
				Thumb->GetBackground()->SetCenterColor(Color(64, 64, 64, 255));
			}

			Thumb->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, ScrollbarTrackComponent::HandleThumbPressed, void, ButtonComponent*));
			Thumb->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, ScrollbarTrackComponent::HandleThumbReleased, void, ButtonComponent*));

			LabelComponent* buttonCaption = Thumb->GetCaptionComponent();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
			}
		}
	}
}

void ScrollbarTrackComponent::TravelByThumbDirection ()
{
	if (TravelDirection == 0 || OwningScrollbar.IsNullptr())
	{
		//Thumb is no longer panning
		return;
	}

	if (!ReversedDirections())
	{
		OwningScrollbar->IncrementScrollPosition(OwningScrollbar->GetNumVisibleScrollPositions() * TravelDirection * -1);
	}

	TravelTimeRemaining = TravelInterval;
}

void ScrollbarTrackComponent::ReevaluateThumbVisibility ()
{
	if (OwningScrollbar.IsValid())
	{
		Thumb->SetVisibility(bEnabled && OwningScrollbar->GetNumVisibleScrollPositions() < OwningScrollbar->GetMaxScrollPosition());
	}
}

void ScrollbarTrackComponent::CalculateThumbPositionClamps ()
{
}

void ScrollbarTrackComponent::CalculateThumbPosition ()
{
	if (OwningScrollbar.IsNullptr())
	{
		return;
	}

	if (OwningScrollbar->GetMaxScrollPosition() <= 0)
	{
		//There's nothing to scroll here (the Thumb should be invisible at this point)
		return;
	}

	Float percentScrolled = (OwningScrollbar->GetScrollPosition().ToFloat() / OwningScrollbar->GetMaxScrollPosition().ToFloat());
	Float thumbAbsPosY = (percentScrolled * (ReadSize().Y));
	Thumb->SetPosition(Vector2(0.f, thumbAbsPosY));
}

void ScrollbarTrackComponent::CalculateThumbScale ()
{
	if (!bScaleThumb || OwningScrollbar.IsNullptr())
	{
		return;
	}

	if (OwningScrollbar->GetMaxScrollPosition() <= 0)
	{
		//There's nothing to scroll here
		return;
	}

	Float scaleRatio = Utils::Clamp<Float>((OwningScrollbar->GetNumVisibleScrollPositions().ToFloat() / OwningScrollbar->GetMaxScrollPosition().ToFloat()), 0.05f, 1.f);
	Float newSize = (scaleRatio * ReadCachedAbsSize().Y);
	Thumb->SetSize(Vector2(ReadSize().X, newSize));
}

Int ScrollbarTrackComponent::CalculateScrollIndex ()
{
	if (Thumb.IsNullptr())
	{
		return 0;
	}

	Float thumbPosY = Thumb->ReadCachedAbsPosition().Y;
	Float percentScrolled = thumbPosY / ReadCachedAbsSize().Y;

	return Float(round((OwningScrollbar->GetMaxScrollPosition().ToFloat() * percentScrolled).Value)).ToInt();
}

bool ScrollbarTrackComponent::CalculateTravelTrackAttributes (Float mousePosX, Float mousePosY)
{
	CHECK(TravelTrackColorOwner.IsNullptr())

	bool bStopped = (TravelDirection == 0 || ReversedDirections());
	TravelTrackColorOwner->SetVisibility(!bStopped);

	if (bStopped)
	{
		//Thumb finished traveling to mouse pointer
		return false;
	}

	//clamp mousePosY to be within track size
	mousePosY = Utils::Clamp(mousePosY, ReadCachedAbsPosition().Y, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y);

	if (TravelDirection > 0) //scrolling up
	{
		Float absSizeY = Thumb->ReadCachedAbsPosition().Y - mousePosY;
		TravelTrackColorOwner->SetSize(Vector2(ReadSize().X, absSizeY));
		TravelTrackColorOwner->SetPosition(Vector2(ReadCachedAbsPosition().X, mousePosY));
	}
	else //scrolling down
	{
		Float absSizeY = mousePosY - (Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y);
		TravelTrackColorOwner->SetSize(Vector2(ReadSize().X, absSizeY));
		TravelTrackColorOwner->SetPosition(Vector2(ReadCachedAbsPosition().X, Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y));
	}

	return true;
}

void ScrollbarTrackComponent::HandleThumbPressed (ButtonComponent* uiComponent)
{
	CHECK(uiComponent != nullptr)
	if (RelevantMouse.IsNullptr())
	{
		return;
	}

	const Vector2& mousePos = RelevantMouse->ReadPosition();

	DraggingThumbPointerOffset = mousePos.Y.ToInt() - uiComponent->ReadCachedAbsPosition().Y.ToInt();
}

void ScrollbarTrackComponent::HandleThumbReleased (ButtonComponent* uiComponent)
{
	DraggingThumbPointerOffset = -1;
}

void ScrollbarTrackComponent::HandleTick (Float deltaSec)
{
	if (TravelTimeRemaining > 0.f)
	{
		TravelTimeRemaining -= deltaSec;
		if (TravelTimeRemaining <= 0.f)
		{
			TravelByThumbDirection();
		}
	}
}
SD_END