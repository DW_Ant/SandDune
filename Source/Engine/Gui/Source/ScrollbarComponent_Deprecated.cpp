/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollbarComponent_Deprecated.cpp
=====================================================================
*/

#include "GuiClasses.h"

IMPLEMENT_CLASS(SD::ScrollbarComponent_Deprecated, SD::GuiComponent)
SD_BEGIN

void ScrollbarComponent_Deprecated::InitProps ()
{
	Super::InitProps();

	Track = nullptr;
	MiddleMouseSpriteOwner = nullptr;
	MiddleMouseSprite = nullptr;
	PanningMiddleMouseCursorOwner = nullptr;
	PanningMiddleMouseCursor = nullptr;
	TopScrollButton = nullptr;
	BottomScrollButton = nullptr;
	ScrollJumpInterval = 3;
	bEnableMiddleMouseScrolling = true;
	MiddleMouseScrollIntervalRange = Range<Float>(0.05f, 1.f);
	MiddleMouseHoldThreshold = 0.75f;
	ContinuousScrollDelay = 0.75f;
	ContinuousInterval = 0.2f;
	bClampsMouseWhenScrolling = true;

	ScrollPosition = 0;
	MaxScrollPosition = 0;
	NumVisibleScrollPositions = 1;
	bEnabled = true;
	bHideWhenInsufficientScrollPos = true;
	bHoldingScrollButtons = false;
	MiddleMousePosition = Vector2(-1, -1);
	MiddleMouseTimeStamp = -1;
	OnScrollPositionChanged = nullptr;
	InteractingMouse = nullptr;

	MiddleMousePanDirection = 0;
	MiddleMousePanTimeRemaining = -1.f;
	ButtonPanTimeRemaining = -1.f;
	bClampingMousePointer = false;
	bScrollbarVisible = true;
}

void ScrollbarComponent_Deprecated::BeginObject ()
{
	Super::BeginObject();

	//Refresh the scroll bar for draw positions and track scale
	SetNumVisibleScrollPositions(NumVisibleScrollPositions);
	SetMaxScrollPosition(MaxScrollPosition);
	SetScrollPosition(0);
}

void ScrollbarComponent_Deprecated::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarComponent_Deprecated* scrollbarTemplate = dynamic_cast<const ScrollbarComponent_Deprecated*>(objTemplate);
	if (scrollbarTemplate != nullptr)
	{
		ScrollJumpInterval = scrollbarTemplate->ScrollJumpInterval;
		bEnableMiddleMouseScrolling = scrollbarTemplate->bEnableMiddleMouseScrolling;
		MiddleMouseScrollIntervalRange = scrollbarTemplate->MiddleMouseScrollIntervalRange;
		MiddleMouseHoldThreshold = scrollbarTemplate->MiddleMouseHoldThreshold;
		ContinuousScrollDelay = scrollbarTemplate->ContinuousScrollDelay;
		ContinuousInterval = scrollbarTemplate->ContinuousInterval;
		bClampsMouseWhenScrolling = scrollbarTemplate->bClampsMouseWhenScrolling;
		SetHideWhenInsufficientScrollPos(scrollbarTemplate->GetHideWhenInsufficientScrollPos());

		bool bCreatedObj;
		MiddleMouseSprite = ReplaceTargetWithObjOfMatchingClass(MiddleMouseSprite.Get(), scrollbarTemplate->MiddleMouseSprite.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(MiddleMouseSprite);
		}

		if (MiddleMouseSprite.IsValid())
		{
			MiddleMouseSprite->CopyPropertiesFrom(scrollbarTemplate->MiddleMouseSprite.Get());
		}

		TopScrollButton = ReplaceTargetWithObjOfMatchingClass(TopScrollButton.Get(), scrollbarTemplate->TopScrollButton.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(TopScrollButton))
			{
				TopScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonPressed, void, ButtonComponent*));
				TopScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonReleased, void, ButtonComponent*));
			}
		}

		if (TopScrollButton.IsValid())
		{
			TopScrollButton->CopyPropertiesFrom(scrollbarTemplate->TopScrollButton.Get());
		}

		BottomScrollButton = ReplaceTargetWithObjOfMatchingClass(BottomScrollButton.Get(), scrollbarTemplate->BottomScrollButton.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(BottomScrollButton))
			{
				BottomScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonPressed, void, ButtonComponent*));
				BottomScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonReleased, void, ButtonComponent*));
			}
		}

		if (BottomScrollButton.IsValid())
		{
			BottomScrollButton->CopyPropertiesFrom(scrollbarTemplate->BottomScrollButton.Get());
		}

		Track = ReplaceTargetWithObjOfMatchingClass(Track.Get(), scrollbarTemplate->Track.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(Track))
			{
				Track->SetThumbChangedPositionHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleTrackChangedPosition, void, Int));
			}
		}

		if (Track.IsValid())
		{
			Track->CopyPropertiesFrom(scrollbarTemplate->Track.Get());
		}
	}
}

void ScrollbarComponent_Deprecated::InitializeComponents ()
{
	Super::InitializeComponents();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleTick, void, Float));
	}

	MiddleMouseSpriteOwner = GuiComponent::CreateObject();
	if (AddComponent(MiddleMouseSpriteOwner))
	{
		MiddleMouseSprite = SpriteComponent::CreateObject();
		if (MiddleMouseSpriteOwner->AddComponent(MiddleMouseSprite))
		{
			MiddleMouseSprite->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarMiddleMouseScrollAnchor.Get());
		}

		MiddleMouseSpriteOwner->SetSize(MiddleMouseSprite->GetTextureSize());
		MiddleMouseSpriteOwner->SetVisibility(false);
	}

	InitializeScrollButtons();
	InitializeTrackComponent();
}

void ScrollbarComponent_Deprecated::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (PanningMiddleMouseCursor.IsNullptr() || PanningMiddleMouseCursorOwner.IsNullptr())
	{
		return;
	}

	PanningMiddleMouseCursorOwner->SetPosition(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));

	Int oldPanDirection = MiddleMousePanDirection;
	Float deltaPos = MiddleMousePosition.Y - Float::MakeFloat(sfmlEvent.y);

	if (deltaPos < -8)
	{
		MiddleMousePanDirection = -1;
	}
	else if (deltaPos > 8)
	{
		MiddleMousePanDirection = 1;
	}
	else
	{
		MiddleMousePanDirection = 0;
	}

	if (MiddleMousePanDirection != oldPanDirection)
	{
		switch(MiddleMousePanDirection.Value)
		{
			case(-1):
				PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 1);
				break;
			case(0):
				PanningMiddleMouseCursor->SetSubDivision(1, 1, 0, 0);
				break;
			case(1):
				PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 0);
				break;
		}
	}
}

void ScrollbarComponent_Deprecated::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (bEnabled && bEnableMiddleMouseScrolling && sfmlEvent.button == sf::Mouse::Middle &&
			IsVisible() && eventType == sf::Event::MouseButtonReleased)
	{
		if (Engine::FindEngine()->GetElapsedTime() - MiddleMouseTimeStamp > MiddleMouseHoldThreshold)
		{
			//The middle mouse button was held long enough to be considered a "hold to move" action.
			SetMiddleMousePosition(mouse, Vector2(-1, -1));
		}
	}
}

bool ScrollbarComponent_Deprecated::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//Only listen to middle mouse
	if (!bEnabled || sfmlEvent.button != sf::Mouse::Middle || !IsVisible())
	{
		return false;
	}

	if (!bEnableMiddleMouseScrolling || eventType != sf::Event::MouseButtonPressed)
	{
		return false;
	}

	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(Owner.Get());
	if (guiOwner != nullptr && guiOwner->IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
		{
			//Toggle off
			SetMiddleMousePosition(mouse, Vector2(-1, -1));
			return true;
		}

		MiddleMouseTimeStamp = Engine::FindEngine()->GetElapsedTime();
		SetMiddleMousePosition(mouse, Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));

		return true;
	}

	return false;
}

bool ScrollbarComponent_Deprecated::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::ExecuteConsumableMouseWheelMove(mouse, sfmlEvent))
	{
		return true;
	}

	if (!bEnabled || !IsVisible())
	{
		return false;
	}

	//Only mouse wheel events are relevant when the mouse cursor is over the owner.
	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(Owner.Get());
	if (guiOwner != nullptr && guiOwner->IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		Int scrollMultiplier = -1;
		if (InputBroadcaster::GetCtrlHeld())
		{
			scrollMultiplier *= NumVisibleScrollPositions;
		}

		IncrementScrollPosition(Float(sfmlEvent.delta).ToInt() * scrollMultiplier);
		return true;
	}

	return false;
}

bool ScrollbarComponent_Deprecated::AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	if (!bEnabled)
	{
		return false;
	}

	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(GetOwner());
	return (Super::AcceptsMouseEvents(inputEvent, mousePosX, mousePosY) || !guiOwner || guiOwner->IsWithinBounds(Vector2(Float::MakeFloat(mousePosX), Float::MakeFloat(mousePosY))));
}

#if 0
void ScrollbarComponent_Deprecated::HandleSizeChange ()
{
	Super::HandleSizeChange();

	if (TopScrollButton != nullptr)
	{
		TopScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
		TopScrollButton->SetPosition(Vector2::ZERO_VECTOR);

		if (BottomScrollButton != nullptr)
		{
			BottomScrollButton->SetSize(TopScrollButton->ReadSize());
			BottomScrollButton->SetPosition(Vector2(0.f, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y));
		}

		if (Track != nullptr)
		{
			Track->SetSize(ReadSize() - Vector2(0.f, TopScrollButton->ReadSize().Y * 2));
			Track->SetPosition(Vector2(0.f, TopScrollButton->ReadCachedAbsSize().Y));
		}
	}
}
#endif

void ScrollbarComponent_Deprecated::Destroyed ()
{
	bClampingMousePointer = false;

	if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
	{
		//restore mouse pointer changes
		SetMiddleMousePosition(InteractingMouse.Get(), Vector2(-1,-1));
	}

	Super::Destroyed();
}

bool ScrollbarComponent_Deprecated::IsIndexVisible (const Int index) const
{
	return (index >= ScrollPosition && index < ScrollPosition + NumVisibleScrollPositions && index < (MaxScrollPosition + NumVisibleScrollPositions));
}

void ScrollbarComponent_Deprecated::IncrementScrollPosition (Int amountToJump)
{
	SetScrollPosition(ScrollPosition + amountToJump);
}

void ScrollbarComponent_Deprecated::SetScrollPositionChanged (std::function<void(Int newScrollPosition)> newHandler)
{
	OnScrollPositionChanged = newHandler;
}

void ScrollbarComponent_Deprecated::SetScrollPosition (Int newScrollPosition)
{
	newScrollPosition = Utils::Clamp<Int>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
	if (newScrollPosition == ScrollPosition)
	{
		return; //No need to update scroll position if the values did not change.
	}

	ScrollPosition = newScrollPosition;

	if (Track.IsValid())
	{
		Track->RefreshScrollbarTrack();
	}

	if (OnScrollPositionChanged != nullptr)
	{
		OnScrollPositionChanged(ScrollPosition);
	}
}

void ScrollbarComponent_Deprecated::SetMaxScrollPosition (Int newMaxScrollPosition)
{
	MaxScrollPosition = Utils::Max<Int>(newMaxScrollPosition, 0);
	EvaluateScrollButtonEnabledness();
	if (bHideWhenInsufficientScrollPos)
	{
		EvaluateScrollVisibility();
	}

	if (Track.IsValid())
	{
		//This will toggle the thumb's visibility if NumVisibleScrollPositions <= MaxScrollPosition
		Track->RefreshScrollbarTrack();
	}
}

void ScrollbarComponent_Deprecated::SetNumVisibleScrollPositions (Int newNumVisibleScrollPositions)
{
	NumVisibleScrollPositions = Utils::Max<Int>(newNumVisibleScrollPositions, 1);
	EvaluateScrollButtonEnabledness();
	if (bHideWhenInsufficientScrollPos)
	{
		EvaluateScrollVisibility();
	}

	if (Track.IsValid())
	{
		Track->RefreshScrollbarTrack();
	}
}

void ScrollbarComponent_Deprecated::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;

	if (Track.IsValid())
	{
		Track->SetEnabled(bNewEnabled);
	}

	EvaluateScrollButtonEnabledness();

	if (!bEnabled)
	{
		SetMiddleMousePosition(InteractingMouse.Get(), Vector2(-1, -1));
	}
}

void ScrollbarComponent_Deprecated::SetHideWhenInsufficientScrollPos (bool bNewHideWhenInsufficientScrollPos)
{
	bHideWhenInsufficientScrollPos = bNewHideWhenInsufficientScrollPos;
	EvaluateScrollVisibility();
}

void ScrollbarComponent_Deprecated::SetMiddleMousePosition (MousePointer* mouse, const Vector2& newPosition)
{
	if (newPosition == MiddleMousePosition || mouse == nullptr)
	{
		return;
	}

	MiddleMousePosition = newPosition;
	if (MiddleMouseSprite.IsValid() && MiddleMouseSpriteOwner.IsValid())
	{
		MiddleMouseSpriteOwner->SetPosition(newPosition);
		MiddleMouseSpriteOwner->SetVisibility(newPosition.X >= 0 && newPosition.Y >= 0);
	}

	if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
	{
		//Restore original mouse cursor
		mouse->SetMouseVisibility(true);
		InteractingMouse = nullptr;
		bClampingMousePointer = false;
		if (PanningMiddleMouseCursor.IsValid())
		{
			RemoveComponent(PanningMiddleMouseCursor.Get());
			PanningMiddleMouseCursor->Destroy();
			PanningMiddleMouseCursor = nullptr;
		}

		//Disable middle mouse pan timer
		MiddleMousePanTimeRemaining = -1.f;
	}
	else
	{
		//We hide the mouse cursor to implement additional functionality (such as overriding appearance even when hovering over other components and subdivisions).
		mouse->SetMouseVisibility(false);
		InteractingMouse = mouse;

		//clamp the mouse pointer so that it cannot go beyond the parent component's boundaries
		GuiComponent* parentComponent = dynamic_cast<GuiComponent*>(GetOwner());
		if (parentComponent != nullptr && bClampsMouseWhenScrolling)
		{
			const Vector2& parentCoordinates = parentComponent->ReadCachedAbsPosition();
			Rectangle limitRegion(parentComponent->ReadCachedAbsSize().X, parentComponent->ReadCachedAbsSize().Y, parentCoordinates + (parentComponent->ReadCachedAbsSize() * 0.5f));
			mouse->PushPositionLimit(limitRegion, SDFUNCTION_2PARAM(this, ScrollbarComponent_Deprecated, HandleLimitCallback, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			bClampingMousePointer = true;
		}

		InitializePanningMiddleMouse(mouse);

		MiddleMousePanTimeRemaining = CalculateMiddleMouseMoveInterval();
	}
}

void ScrollbarComponent_Deprecated::SetScrollButtonHeight (Float newScrollButtonHeight)
{
	if (TopScrollButton.IsValid())
	{
		TopScrollButton->SetSize(Vector2(ReadSize().X, newScrollButtonHeight));
	}

	if (BottomScrollButton.IsValid())
	{
		BottomScrollButton->SetSize(Vector2(ReadSize().X, newScrollButtonHeight));
	}

	if (Track.IsValid())
	{
		//Ensure the track component is between the two buttons
		Track->SetSize(Vector2(ReadSize().X, ReadSize().Y - (newScrollButtonHeight * 2.f)));
		Track->SetPosition(Vector2(0.f, newScrollButtonHeight));
	}
}

Int ScrollbarComponent_Deprecated::GetScrollPosition () const
{
	return ScrollPosition;
}

Int ScrollbarComponent_Deprecated::GetMaxScrollPosition () const
{
	return MaxScrollPosition;
}

Int ScrollbarComponent_Deprecated::GetLastVisibleScrollPosition () const
{
	return ScrollPosition + NumVisibleScrollPositions - 1;
}

Int ScrollbarComponent_Deprecated::GetNumVisibleScrollPositions () const
{
	return NumVisibleScrollPositions;
}

bool ScrollbarComponent_Deprecated::GetEnabled () const
{
	return bEnabled;
}

bool ScrollbarComponent_Deprecated::GetHideWhenInsufficientScrollPos () const
{
	return bHideWhenInsufficientScrollPos;
}

Vector2 ScrollbarComponent_Deprecated::GetMiddleMousePosition () const
{
	return MiddleMousePosition;
}

void ScrollbarComponent_Deprecated::InitializeScrollButtons ()
{
	LabelComponent* buttonCaption = nullptr;
	if (TopScrollButton.IsNullptr())
	{
		TopScrollButton = ButtonComponent::CreateObject();
		if (AddComponent(TopScrollButton))
		{
			if (TopScrollButton->GetRenderComponent() != nullptr)
			{
				//TopScrollButton->GetRenderComponent()->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarTopButton.Get());
			}

			TopScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
			TopScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			TopScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonPressed, void, ButtonComponent*));
			TopScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonReleased, void, ButtonComponent*));

			buttonCaption = TopScrollButton->GetCaptionComponent();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
				buttonCaption = nullptr;
			}
		}
	}

	if (BottomScrollButton.IsNullptr())
	{
		BottomScrollButton = ButtonComponent::CreateObject();
		if (AddComponent(BottomScrollButton))
		{
			if (BottomScrollButton->GetRenderComponent() != nullptr)
			{
				//BottomScrollButton->GetRenderComponent()->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarBottomButton.Get());
			}

			BottomScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			BottomScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
			BottomScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonPressed, void, ButtonComponent*));
			BottomScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonReleased, void, ButtonComponent*));

			buttonCaption = BottomScrollButton->GetCaptionComponent();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
				buttonCaption = nullptr;
			}
		}
	}

	EvaluateScrollButtonEnabledness();
}

void ScrollbarComponent_Deprecated::InitializeTrackComponent ()
{
	if (Track.IsNullptr())
	{
		Track = ScrollbarTrackComponent::CreateObject();
		Track->SetSize(Vector2(ReadSize().X, ReadSize().Y - (TopScrollButton->ReadCachedAbsSize().Y * 2.f)));
		Track->SetPosition(Vector2(0.f, TopScrollButton->ReadCachedAbsSize().Y));
		Track->SetThumbChangedPositionHandler(std::bind(&ScrollbarComponent_Deprecated::HandleTrackChangedPosition, this, std::placeholders::_1));
		AddComponent(Track);
	}
}

void ScrollbarComponent_Deprecated::InitializePanningMiddleMouse (MousePointer* mouse)
{
	PanningMiddleMouseCursorOwner = GuiComponent::CreateObject();
	if (AddComponent(PanningMiddleMouseCursorOwner))
	{
		PanningMiddleMouseCursor = SpriteComponent::CreateObject();
		if (PanningMiddleMouseCursorOwner->AddComponent(PanningMiddleMouseCursor))
		{
			Texture* scrollbarPointer = GuiTheme::GetGuiTheme()->ScrollbarPointer.Get();
			PanningMiddleMouseCursor->SetSpriteTexture(scrollbarPointer);

			Vector2 spriteSize;
			scrollbarPointer->GetDimensions(OUT spriteSize);
			PanningMiddleMouseCursorOwner->SetSize(spriteSize * Vector2(1.f, 0.5f)); //flatten the sprite to be half size horizontally
			PanningMiddleMouseCursorOwner->SetPosition(mouse->ReadPosition());
		}
		else
		{
			PanningMiddleMouseCursorOwner->Destroy();
		}
	}

	MiddleMousePanDirection = 0;
}

void ScrollbarComponent_Deprecated::PanByMiddleMouse ()
{
	if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
	{
		return;
	}

	if (InteractingMouse.IsNullptr())
	{
		return;
	}

	if (MiddleMousePanDirection != 0) //If the user is not hovering over middle mouse position
	{
		const Vector2& mousePos = InteractingMouse->ReadPosition();

		Int scrollDirection = (mousePos.Y > MiddleMousePosition.Y) ? 1 : -1;
		IncrementScrollPosition(ScrollJumpInterval * scrollDirection);
	}

	MiddleMousePanTimeRemaining = CalculateMiddleMouseMoveInterval();
}

void ScrollbarComponent_Deprecated::PanByButtonPress ()
{
	//User must have pressed the scroll button somehow.  This shouldn't happen unless one of the buttons are destroyed.
	CHECK(TopScrollButton.IsValid() && BottomScrollButton.IsValid())

	Int scrollAmount = ScrollJumpInterval;
	if (TopScrollButton->GetPressedDown())
	{
		//Reverse direction
		scrollAmount *= -1;
	}
	else if (!BottomScrollButton->GetPressedDown())
	{
		//Neither button is held
		return;
	}

	IncrementScrollPosition(scrollAmount);
	ButtonPanTimeRemaining = ContinuousInterval;
}

void ScrollbarComponent_Deprecated::EvaluateScrollButtonEnabledness ()
{
	if (TopScrollButton.IsValid())
	{
		TopScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
	}

	if (BottomScrollButton.IsValid())
	{
		BottomScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
	}
}

void ScrollbarComponent_Deprecated::EvaluateScrollVisibility ()
{
	bool bShouldBeVisible = (!bHideWhenInsufficientScrollPos || NumVisibleScrollPositions < MaxScrollPosition);
	if (bShouldBeVisible != bScrollbarVisible)
	{
		bScrollbarVisible = bShouldBeVisible;
		SetVisibility(bShouldBeVisible);

		if (OnToggleVisibility.IsBounded())
		{
			OnToggleVisibility();
		}
	}
}

Float ScrollbarComponent_Deprecated::CalculateMiddleMouseMoveInterval ()
{
	CHECK(InteractingMouse.IsValid())
	if (MiddleMouseScrollIntervalRange.IsConverged())
	{
		//No use calculating the interval if the min/max are equal
		return (MiddleMouseScrollIntervalRange.Min);
	}

	const Vector2& mousePos = InteractingMouse->ReadPosition();

	Float relativeDistance = 0.f;
	Float distRatio;
	if (MiddleMousePanDirection < 0) //scrolling down
	{
		relativeDistance = mousePos.Y - MiddleMousePosition.Y;
		distRatio = relativeDistance / (ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - MiddleMousePosition.Y);
	}
	else if (MiddleMousePanDirection > 0) //scrolling up
	{
		relativeDistance = mousePos.Y - MiddleMousePosition.Y;
		distRatio = relativeDistance / (ReadCachedAbsPosition().Y - MiddleMousePosition.Y);
	}
	else
	{
		return 0.5f; //check periodically
	}

	//safety check to avoid setting timer to 0 seconds
	distRatio = Utils::Clamp<Float>(distRatio, 0.01f, 0.99f);

	return Utils::Lerp(1-distRatio, MiddleMouseScrollIntervalRange.Min, MiddleMouseScrollIntervalRange.Max);
}

void ScrollbarComponent_Deprecated::HandleScrollButtonPressed (ButtonComponent* uiComponent)
{
	bHoldingScrollButtons = true;
	Int scrollDirection = (uiComponent == TopScrollButton) ? -1 : 1;
	IncrementScrollPosition(ScrollJumpInterval * scrollDirection);
	ButtonPanTimeRemaining = ContinuousScrollDelay;
}

void ScrollbarComponent_Deprecated::HandleScrollButtonReleased (ButtonComponent* uiComponent)
{
	bHoldingScrollButtons = false;
	ButtonPanTimeRemaining = -1.f; //Disable button pan timer
}

void ScrollbarComponent_Deprecated::HandleTrackChangedPosition (Int newScrollPosition)
{
	//Do SetScrollPosition without refreshing the track component
	newScrollPosition = Utils::Clamp<Int>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
	if (newScrollPosition == ScrollPosition)
	{
		return;
	}

	ScrollPosition = newScrollPosition;

	if (OnScrollPositionChanged != nullptr)
	{
		OnScrollPositionChanged(ScrollPosition);
	}
}

bool ScrollbarComponent_Deprecated::HandleLimitCallback (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvent)
{
	return (bClampingMousePointer);
}

void ScrollbarComponent_Deprecated::HandleTick (Float deltaSec)
{
	if (InteractingMouse.IsValid() && MiddleMousePanTimeRemaining > 0.f)
	{
		MiddleMousePanTimeRemaining -= deltaSec;
		if (MiddleMousePanTimeRemaining <= 0.f)
		{
			PanByMiddleMouse();
		}
	}

	if (ButtonPanTimeRemaining > 0.f)
	{
		ButtonPanTimeRemaining -= deltaSec;
		if (ButtonPanTimeRemaining <= 0.f)
		{
			PanByButtonPress();
		}
	}
}
SD_END