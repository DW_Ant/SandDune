/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiEngineComponent.cpp
=====================================================================
*/

#include "GuiEngineComponent.h"
#include "GuiDrawLayer.h"
#include "GuiTheme.h"

IMPLEMENT_ENGINE_COMPONENT(SD::GuiEngineComponent)
SD_BEGIN

GuiEngineComponent::GuiEngineComponent () : EngineComponent()
{
	DefaultThemeClass = GuiTheme::SStaticClass();
}

void GuiEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_GUI, TICK_GROUP_PRIORITY_GUI);

	MainOverlay = GuiDrawLayer::CreateObject();
	MainOverlay->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
}

void GuiEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	InputEngineComponent* inputEngine = InputEngineComponent::Find();
	CHECK(inputEngine != nullptr)

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)

	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	CHECK(mainWindow != nullptr)

	ImportGuiEssentialTextures();

	if (!DefaultThemeClass->IsChildOf(GuiTheme::SStaticClass()))
	{
		Engine::FindEngine()->FatalError(DString::FormatString(TXT("Failed to initialize GuiEngineComponent.  The class of DefaultThemeClass (%s) is not a GuiTheme."), {DefaultThemeClass->ToString()}));
		return;
	}

	RegisteredGuiTheme = dynamic_cast<GuiTheme*>(DefaultThemeClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	RegisteredGuiTheme->InitializeTheme();
	RegisteredGuiTheme->InitializeStyles();

	ForegroundUiLayer = GuiDrawLayer::CreateObject();
	ForegroundUiLayer->SetDrawPriority(GuiDrawLayer::FOREGROUND_PRIORITY);
	ForegroundUiLayer->InitCommonEntities(GuiDrawLayer::SCommonGuiClasses(false), inputEngine->GetMainBroadcaster());

	mainWindow->RegisterDrawLayer(ForegroundUiLayer.Get());
	mainWindow->RegisterDrawLayer(MainOverlay.Get());
}

void GuiEngineComponent::ShutdownComponent ()
{
	if (RegisteredGuiTheme.IsValid())
	{
		RegisteredGuiTheme->Destroy();
	}

	if (MainOverlay.IsValid())
	{
		MainOverlay->Destroy();
	}

	if (ForegroundUiLayer.IsValid())
	{
		ForegroundUiLayer->Destroy();
	}

	Super::ShutdownComponent();
}

void GuiEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	//Depends on GraphicsEngineComponent since the GuiEngineComponent registers the MainOverlay.
	outDependencies.push_back(GraphicsEngineComponent::SStaticClass());
}

void GuiEngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetInitializeDependencies(OUT outDependencies);

	//Needs the input engine to import the mouse icons before the theme initializes.
	outDependencies.push_back(InputEngineComponent::SStaticClass());
}

void GuiEngineComponent::SetDefaultThemeClass (const DClass* newDefaultThemeClass)
{
	if (!newDefaultThemeClass->IsChildOf(GuiTheme::SStaticClass()))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot assign %s to the GuiEngine's DefaultThemeClass since it's not a Gui Theme class."), newDefaultThemeClass->ToString());
		return;
	}

	if (RegisteredGuiTheme.IsValid())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot assign the default theme class to %s since the theme is already assigned.  You can only call this function prior to InitializeComponent."), newDefaultThemeClass->ToString());
		return;
	}

	DefaultThemeClass = newDefaultThemeClass;
}

void GuiEngineComponent::SetGuiTheme (GuiTheme* newGuiTheme)
{
	RegisteredGuiTheme = newGuiTheme;
}

GuiTheme* GuiEngineComponent::GetGuiTheme () const
{
	return RegisteredGuiTheme.Get();
}

void GuiEngineComponent::ImportGuiEssentialTextures ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//CheckboxComponent
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("Checkbox.txtr")));

	//ButtonComponent
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("TextureTestStates.txtr")));

	//FrameComponent
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("FrameBorder.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("FrameComponentFill.txtr")));

	//ScrollbarComponent
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollUpButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollDownButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollLeftButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollRightButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollZoomButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMiddleMouseSprite.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorStationary.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanUp.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanUpRight.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanRight.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanDownRight.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanDown.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanDownLeft.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanLeft.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ScrollbarMouseAnchorPanUpLeft.txtr")));

	//DropdownComponent
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("CollapsedButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("ExpandedButton.txtr")));

	//Tooltips
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("TooltipBackground.txtr")));

	//Tree List
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("TreeListAddButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Gui"), TXT("TreeListSubtractButton.txtr")));
}
SD_END