/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LabelComponent.cpp
=====================================================================
*/

#include "FrameComponent.h"
#include "GuiEntity.h"
#include "GuiTheme.h"
#include "LabelComponent.h"
#include "ScrollbarComponent.h"

IMPLEMENT_CLASS(SD::LabelComponent, SD::GuiComponent)
SD_BEGIN

const std::vector<char> LabelComponent::WRAP_CHARS({' ', '-'});

void LabelComponent::InitProps ()
{
	Super::InitProps();

	if (GuiTheme::GetGuiTheme() != nullptr)
	{
		SetFont(GuiTheme::GetGuiTheme()->GuiFont.Get());
	}

	SetCharacterSize(16);
	bAutoRefresh = true;
	bWrapText = true;
	bClampText = true;
	AutoSizeHorizontal = false;
	AutoSizeVertical = false;
	LineSpacing = 4;
	FirstLineOffset = 0.f;

	//Should have at least one line in the Content vector to handle cases where TextFieldComponents initialize to empty strings (when SetText is not called).
	//Otherwise, the user would be unable to click to a CursorPosition since there aren't any lines.
	Content.push_back(DString::EmptyString);

	ClampedText = DString::EmptyString;
	RenderComponent = nullptr;
	RenderComponentClass = TextRenderComponent::SStaticClass();
	VerticalAlignment = VA_Top;
	HorizontalAlignment = HA_Left;
	MaxNumLines = 1;
}

void LabelComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const LabelComponent* labelTemplate = dynamic_cast<const LabelComponent*>(objTemplate);
	if (labelTemplate != nullptr)
	{
		//Handle RenderComponent first since various LabelComponent properties (such as setting font) affects the render component
		bool bCreatedObj;
		BackgroundComponent = ReplaceTargetWithObjOfMatchingClass(BackgroundComponent.Get(), labelTemplate->GetBackgroundComponent(), bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(BackgroundComponent);
		}

		if (BackgroundComponent != nullptr)
		{
			BackgroundComponent->CopyPropertiesFrom(labelTemplate->BackgroundComponent.Get());
		}

		RenderComponent = ReplaceTargetWithObjOfMatchingClass(RenderComponent.Get(), labelTemplate->GetRenderComponent(), bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(RenderComponent);
		}

		if (RenderComponent.IsValid())
		{
			RenderComponent->CopyPropertiesFrom(labelTemplate->RenderComponent.Get());
		}

		SetAutoRefresh(false);
		SetFont(labelTemplate->GetFont());
		SetCharacterSize(labelTemplate->GetCharacterSize());
		SetWrapText(labelTemplate->GetWrapText());
		SetClampText(labelTemplate->GetClampText());
		SetLineSpacing(labelTemplate->GetLineSpacing());
		FirstLineOffset = labelTemplate->FirstLineOffset;
		SetVerticalAlignment(labelTemplate->GetVerticalAlignment());
		SetHorizontalAlignment(labelTemplate->GetHorizontalAlignment());
		SetAutoRefresh(true);
	}
}

void LabelComponent::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeBackgroundComponent();
	InitializeRenderComponent();
	CalculateMaxNumLines();
}

void LabelComponent::CreateLabelWithinScrollbar (ScrollbarComponent* viewer, GuiEntity*& outLabelOwner, LabelComponent*& outLabelComp)
{
	CHECK(viewer != nullptr) //A ScrollbarComponent must be specified.

	outLabelOwner = GuiEntity::CreateObject();
	outLabelOwner->SetAutoSizeVertical(true);
	outLabelOwner->SetEnableFractionScaling(false);
	outLabelOwner->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));

	outLabelComp = LabelComponent::CreateObject();
	if (outLabelOwner->AddComponent(outLabelComp))
	{
		outLabelComp->SetAutoRefresh(false);
		outLabelComp->SetClampText(false);
		outLabelComp->SetWrapText(true);
		outLabelComp->SetEnableFractionScaling(true);
		outLabelComp->SetSize(Vector2(1.f, 1.f));
		outLabelComp->SetPosition(Vector2::ZERO_VECTOR);
		outLabelComp->SetAutoSizeVertical(true);
		outLabelComp->SetAutoSizeHorizontal(false);
		outLabelComp->SetAutoRefresh(true);
		viewer->SetViewedObject(outLabelOwner);
	}
	else
	{
		outLabelOwner->Destroy();
		outLabelOwner = nullptr;
	}
}

void LabelComponent::SetFont (const Font* newFont)
{
	CurrentFont = newFont;

	if (RenderComponent.IsValid())
	{
		RenderComponent->SetTextFont(CurrentFont.Get());
	}

	CalculateMaxNumLines();

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetCharacterSize (Int newCharacterSize)
{
	CharacterSize = newCharacterSize;

	if (RenderComponent.IsValid())
	{
		RenderComponent->SetFontSize(CharacterSize);
	}

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetText (const DString& newContent)
{
	//Reset label's text
	ContainerUtils::Empty(OUT Content);
	Content.push_back(newContent); //Place everything on first line
	ClampedText = DString::EmptyString;

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::RefreshText ()
{
	ResetContent();
	CalculateMaxNumLines();
	ParseNewLineCharacters();

	if (AutoSizeHorizontal)
	{
		ApplyHorizontalAutoSize();
	}
	else
	{
		WrapText();
	}

	if (AutoSizeVertical)
	{
		ApplyVerticalAutoSize();
	}
	else
	{
		ClampText(); //This function updates the ContentBody and ContentTail based on the truncated text
	}

	if (RenderComponent.IsValid())
	{
		//Reset render component
		RenderComponent->DeleteAllText();

		RenderContent();

		AlignVertically();
		AlignHorizontally();
	}
}

void LabelComponent::RefreshTextFromLine (size_t startingLineIdx)
{
	CHECK(CurrentFont != nullptr)

	size_t originalContentLength = Content.size();
	std::vector<size_t> indicesToUpdate({startingLineIdx}); //Assume that the startingLineIdx line has changed.
	const Float width = (ReadCachedAbsSize().X > 0.f) ? ReadCachedAbsSize().X : ReadSize().X; //If abs size is not yet computed, try local size.

	bool shouldWrap = (bWrapText && !AutoSizeHorizontal);
	if (!shouldWrap)
	{
		//Need to process new line characters in case the edited text contains multiple new line characters.
		//Also this should only be done when word wrap is false since word wrapping uses new line characters as an indication where the breaks are.
		for (size_t i = startingLineIdx; i < Content.size(); ++i)
		{
			bool bFoundNewLine = ParseNewLineCharactersForLine(i);
			if (!bFoundNewLine)
			{
				//No need to check subsequent lines since they shouldn't have been affected if no characters moved over there.
				break;
			}

			indicesToUpdate.push_back(i);
		}
	}

	if (AutoSizeHorizontal)
	{
		//Scale up or down the horizontal size of this component based on the widest line.
		ApplyHorizontalAutoSize();
	}
	else if (shouldWrap)
	{
		for (size_t i = startingLineIdx; i < Content.size(); ++i)
		{
			//Find the spot where the line should break
			Int lastAcceptedChar = -1;
			Float curWidth = 0.f;
			bool bEndsOnNewLine = false;
			for (StringIterator iter(&Content.at(i)); !iter.IsAtEnd(); ++iter)
			{
				DString curChar = *iter;
				curWidth += CurrentFont->GetCharacterWidth(curChar, CharacterSize);
				if (curWidth > width)
				{
					break;
				}

				++lastAcceptedChar;
				if (curChar.At(0) == '\n' || curChar.At(0) == '\r') //Need to add this new line check since the inserted text may contain these characters.
				{
					bEndsOnNewLine = true;
					break;
				}
			}

			if (lastAcceptedChar < 0)
			{
				//Label is too narrow. Can't wrap anything.
				break;
			}

			if (lastAcceptedChar > 0 && !bEndsOnNewLine && curWidth > width)
			{
				//Iterate backwards until we find a valid character to break the line.
				Float deltaWidth;
				Int wrapPos = FindPreviousCharacter(WRAP_CHARS, i, lastAcceptedChar, OUT deltaWidth);
				if (wrapPos != INT_INDEX_NONE)
				{
					lastAcceptedChar = wrapPos;
					curWidth -= deltaWidth;
				}
			}

			size_t nextLineIdx = (i+1);
			if ((lastAcceptedChar+1) < Content.at(i).Length()) //If we need to move characters from this line to the next
			{
				DString sectionToMove = Content.at(i).SubString(lastAcceptedChar+1);
				Content.at(i).Remove(lastAcceptedChar + 1, -1);

				if (nextLineIdx < Content.size())
				{
					Content.at(nextLineIdx).Insert(0, sectionToMove);
					ContainerUtils::AddUnique(OUT indicesToUpdate, nextLineIdx);
				}
				else //Adding new line at the end
				{
					if (bClampText && Content.size() >= MaxNumLines && !AutoSizeVertical)
					{
						//Can't fit this text. Insert it to the clamped text
						ClampedText = sectionToMove + ClampedText;
					}
					else
					{
						Content.insert(Content.begin() + nextLineIdx, sectionToMove);
						ContainerUtils::AddUnique(OUT indicesToUpdate, nextLineIdx);
					}
				}

				continue; //Proceed to the next line regardless if this line ends with a new line character or not. The next line needs to be wrapped, and there could have been multiple inserted new line characters.
			}
			else if (curWidth < width) //if there is more space available (potentially move characters from next line to this line)
			{
				while (nextLineIdx < Content.size() && !bEndsOnNewLine)
				{
					//Check if we can move more characters from next line to this line.
					Int nextLineAcceptedChar = -1;
					for (StringIterator iter(&Content.at(nextLineIdx)); !iter.IsAtEnd(); ++iter)
					{
						DString curChar = *iter;
						curWidth += CurrentFont->GetCharacterWidth(curChar, CharacterSize);
						if (curWidth > width)
						{
							break;
						}

						++nextLineAcceptedChar;
						if (curChar.At(0) == '\r' || curChar.At(0) == '\n')
						{
							bEndsOnNewLine = true;
							break;
						}
					}

					if (nextLineAcceptedChar >= 0 && !bEndsOnNewLine)
					{
						//Need to ensure we're not splitting a word
						Float deltaWidth;
						Int wrapPos = FindPreviousCharacter(WRAP_CHARS, nextLineIdx, nextLineAcceptedChar, OUT deltaWidth);
						if (wrapPos == INT_INDEX_NONE)
						{
							//This would be splitting a word; don't move any character from next line to this line
							nextLineAcceptedChar = -1;
						}
						else
						{
							curWidth -= deltaWidth;
							nextLineAcceptedChar = wrapPos;
						}
					}

					if (nextLineAcceptedChar >= 0) //If we're moving at least one character
					{
						if ((nextLineAcceptedChar + 1) >= Content.at(nextLineIdx).Length())
						{
							//Move entire line to the current line
							Content.at(i) += Content.at(nextLineIdx);

							if (nextLineIdx + 1 >= Content.size() && ClampedText.IsEmpty())
							{
								//This is the last line, but there is still clamped text we can add. Move clamped text to the last line and repeat this process
								Content.at(nextLineIdx) = ClampedText;
								ClampedText.Clear();
								ContainerUtils::AddUnique(OUT indicesToUpdate, nextLineIdx);

								continue; //Repeat the process for the next line (next line being the clamped text).
							}
							else
							{
								Content.erase(Content.begin() + nextLineIdx);
							}

							if (ContainerUtils::FindInVector(indicesToUpdate, nextLineIdx) == INDEX_NONE) //If not running this loop a second time (nextLineIdx doesn't increment. Instead only the Content shrinks each time a line is removed).
							{
								for (size_t j = nextLineIdx; j < Content.size(); ++j)
								{
									indicesToUpdate.push_back(j);
								}
							}

							if (!bEndsOnNewLine)
							{
								continue; //Repeat the process for the next line
							}
						}
						else
						{
							//Move extra characters from previous line to current line
							Content.at(i) += Content.at(nextLineIdx).SubString(0, nextLineAcceptedChar);
							Content.at(nextLineIdx).Remove(0, nextLineAcceptedChar + 1);
							ContainerUtils::AddUnique(OUT indicesToUpdate, nextLineIdx);
						}

						ContainerUtils::AddUnique(OUT indicesToUpdate, i);
					}

					break; //Don't bother checking if the next line should move to this line since it's either at max width, ends on the new line, or nothing was transferred.
				}
			}

			if (bEndsOnNewLine)
			{
				if (nextLineIdx >= Content.size())
				{
					//Add at least one blank line if the last line ends with a new line character. New line characters at the end allows the user to edit the line below.
					Content.push_back(DString::EmptyString);
					indicesToUpdate.push_back(Content.size() - 1);
				}

				//No need to bother wrapping on the following lines since this current line already found the end of the paragraph.
				break;
			}
		}
	}

	//Sort the update idx in ascending order since the RenderComponent's text instances must correspond to the label component's Content vector.
	std::sort(indicesToUpdate.begin(), indicesToUpdate.end(), [](size_t a, size_t b)
	{
		return (a < b);
	});

	//Update the render component's text and alignment
	for (size_t updateIdx : indicesToUpdate)
	{
		UpdateRenderContent(updateIdx);
		AlignLineHorizontally(updateIdx);
	}

	if (originalContentLength != Content.size())
	{
		//There is a change in number of lines. Need to apply vertical size and alignment.
		if (AutoSizeVertical)
		{
			ApplyVerticalAutoSize();
		}

		AlignVertically();
	}
}

void LabelComponent::SetAutoRefresh (bool bNewAutoRefresh)
{
	if (bAutoRefresh != bNewAutoRefresh)
	{
		bAutoRefresh = bNewAutoRefresh;
		if (bAutoRefresh)
		{
			RefreshText();
		}
	}
}

void LabelComponent::SetWrapText (bool bNewWrapText)
{
	bWrapText = bNewWrapText;

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetClampText (bool bNewClampText)
{
	bClampText = bNewClampText;

	if (bClampText && bAutoRefresh)
	{
		ClampText();
	}
}

void LabelComponent::SetAutoSizeHorizontal (bool newAutoSizeHorizontal)
{
	AutoSizeHorizontal = newAutoSizeHorizontal;

	if (bAutoRefresh && AutoSizeHorizontal)
	{
		RefreshText();
	}
}

void LabelComponent::SetAutoSizeVertical (bool newAutoSizeVertical)
{
	AutoSizeVertical = newAutoSizeVertical;

	if (bAutoRefresh && AutoSizeVertical)
	{
		RefreshText();
	}
}

void LabelComponent::SetLineSpacing (Float newLineSpacing)
{
	LineSpacing = newLineSpacing;

	Int oldMaxNumLines = MaxNumLines;
	CalculateMaxNumLines();

	if (!bAutoRefresh)
	{
		return;
	}

	if (oldMaxNumLines != MaxNumLines)
	{
		RefreshText(); //Do a full refresh since more text can be revealed or clamped.
	}
	else
	{
		AlignVertically();
	}
}

void LabelComponent::SetVerticalAlignment (eVerticalAlignment newVerticalAlignment)
{
	VerticalAlignment = newVerticalAlignment;

	if (bAutoRefresh)
	{
		AlignVertically();
	}
}

void LabelComponent::SetHorizontalAlignment (eHorizontalAlignment newHorizontalAlignment)
{
	HorizontalAlignment = newHorizontalAlignment;

	if (bAutoRefresh)
	{
		AlignHorizontally();
	}
}

void LabelComponent::SetRenderComponent (TextRenderComponent* newRenderComponent)
{
	if (RenderComponent.IsValid())
	{
		RenderComponent->Destroy();
	}

	RenderComponent = newRenderComponent;
	if (RenderComponent.IsValid())
	{
		if (AddComponent(RenderComponent))
		{
			RenderComponent->SetTextFont(CurrentFont.Get());
			RenderComponent->SetFontSize(CharacterSize);
		}
	}
}

const Font* LabelComponent::GetFont () const
{
	return CurrentFont.Get();
}

Int LabelComponent::GetCharacterSize () const
{
	return CharacterSize;
}

bool LabelComponent::GetAutoRefresh () const
{
	return bAutoRefresh;
}

bool LabelComponent::GetWrapText () const
{
	return bWrapText;
}

bool LabelComponent::GetClampText () const
{
	return bClampText;
}

bool LabelComponent::GetAutoSizeHorizontal () const
{
	return AutoSizeHorizontal;
}

bool LabelComponent::GetAutoSizeVertical () const
{
	return AutoSizeVertical;
}

Float LabelComponent::GetLineSpacing () const
{
	return LineSpacing;
}

Float LabelComponent::GetFirstLineOffset () const
{
	return FirstLineOffset;
}

DString LabelComponent::GetContent () const
{
	DString results = DString::EmptyString;

	for (size_t i = 0; i < Content.size(); ++i)
	{
		results += Content.at(i);
	}

	results += ClampedText;

	return results;
}

void LabelComponent::GetContent (std::vector<DString>& outContent, bool bIncludeClamped) const
{
	outContent = Content;

	if (!ClampedText.IsEmpty() && bIncludeClamped)
	{
		outContent.push_back(ClampedText);
	}
}

std::vector<DString>& LabelComponent::EditContent ()
{
	return Content;
}

const std::vector<DString>& LabelComponent::ReadContent () const
{
	return Content;
}

Float LabelComponent::GetLineHeight () const
{
	return CharacterSize.ToFloat() + LineSpacing;
}

DString LabelComponent::GetLine (Int lineIdx) const
{
	if (!ContainerUtils::IsValidIndex(Content, lineIdx))
	{
		return DString::EmptyString;
	}

	return Content.at(lineIdx.ToUnsignedInt());
}

LabelComponent::eVerticalAlignment LabelComponent::GetVerticalAlignment () const
{
	return VerticalAlignment;
}

LabelComponent::eHorizontalAlignment LabelComponent::GetHorizontalAlignment () const
{
	return HorizontalAlignment;
}

Int LabelComponent::GetMaxNumLines () const
{
	return MaxNumLines;
}

FrameComponent* LabelComponent::GetBackgroundComponent () const
{
	return BackgroundComponent.Get();
}

TextRenderComponent* LabelComponent::GetRenderComponent () const
{
	return RenderComponent.Get();
}

void LabelComponent::InitializeBackgroundComponent ()
{
	BackgroundComponent = FrameComponent::CreateObject();
	if (AddComponent(BackgroundComponent))
	{
		BackgroundComponent->SetPosition(Vector2::ZERO_VECTOR);
		BackgroundComponent->SetSize(Vector2(1.f, 1.f));
		BackgroundComponent->SetLockedFrame(true);
		BackgroundComponent->SetCenterColor(Color(0, 0, 0, 96));
		if (BorderRenderComponent* borderComp = BackgroundComponent->GetBorderComp())
		{
			borderComp->SetBorderTexture(nullptr);
			borderComp->SetBorderColor(Color(64, 64, 64, 96));
		}

		BackgroundComponent->SetVisibility(false);
	}
}

void LabelComponent::InitializeRenderComponent ()
{
	TextRenderComponent* newRenderComponent = dynamic_cast<TextRenderComponent*>(RenderComponentClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	CHECK(newRenderComponent != nullptr)
	SetRenderComponent(newRenderComponent);
}

void LabelComponent::ResetContent ()
{
	for (size_t i = 1; i < Content.size(); ++i)
	{
		Content[0] += std::move(Content[i]);
	}

	if (!ContainerUtils::IsEmpty(Content))
	{
		Content.resize(1); //Delete lines 2-n from vector

		//Append any clamped text to the first line.
		Content.at(0) += std::move(ClampedText);
		//std::move doesn't zero out the memory. Make sure we reassign ClampedText to an empty string.
	}

	ClampedText = DString::EmptyString;
}

bool LabelComponent::ParseNewLineCharacters ()
{
	if (RenderComponent.IsNullptr())
	{
		return false; //Can't fit all the text here since there are no defined boundaries to render the text in.
	}

	bool bFoundNewLine = false;
	for (size_t i = 0; i < Content.size(); ++i)
	{
		Int curCharIndex = Content.at(i).Find('\n', 0);

		//Exit loop if no new line characters were found
		if (curCharIndex == INT_INDEX_NONE)
		{
			return bFoundNewLine;
		}

		bFoundNewLine = true;

		if (curCharIndex >= Content.at(i).Length() - 1)
		{
			//The string ends with a new line character. This means there should be a blank line at the end.
			Content.push_back(DString::EmptyString);
			break;
		}

		//Transfer text after the charIndex to the next line
		Content.emplace_back(Content.at(i).SubString(curCharIndex + 1));
		Content.at(i) = Content.at(i).SubString(0, curCharIndex);
	}

	return bFoundNewLine;
}

bool LabelComponent::ParseNewLineCharactersForLine (size_t lineIdx)
{
	if (lineIdx >= Content.size())
	{
		return false;
	}

	//Don't care about strings that end with new line characters since that's the end of the line anyways.
	DString& curLine = Content.at(lineIdx);
	while (!curLine.IsEmpty())
	{
		if (!curLine.EndsWith('\n') && !curLine.EndsWith('\r'))
		{
			break;
		}

		curLine.PopBack();
	}

	bool bResult = false;
	Int newLineCharIdx = 0;
	for (StringIterator iter(&curLine); !iter.IsAtEnd(); ++iter, ++newLineCharIdx)
	{
		DString curChar = *iter;
		if (curChar.At(0) == '\n' || curChar.At(0) == '\r')
		{
			bResult = true;
			break; //Found a character
		}
	}

	if (bResult)
	{
#ifdef DEBUG_MODE //Only in debug builds only since DString::Length might iterate through the entire string to calculate the length for multi-byte character sets
		CHECK(curLine.Length() > 1) //Shouldn't be possible to contain a line that only has a new line character since the while loop earlier in this function should have removed it.
#endif

		size_t nextLineIdx = lineIdx + 1;
		if (nextLineIdx < Content.size())
		{
			Content.at(nextLineIdx) = curLine.SubString(newLineCharIdx + 1) + Content.at(nextLineIdx);
		}
		else
		{
			Content.emplace_back(curLine.SubString(newLineCharIdx + 1)); //+1 to exclude the new line character
		}

		if (newLineCharIdx == 0)
		{
			curLine.Clear();
		}
		else
		{
			curLine = curLine.SubString(0, newLineCharIdx - 1);
		}
	}
	
	return bResult;
}

void LabelComponent::WrapText ()
{
	if (!bWrapText)
	{
		return;
	}

	const Float width = (ReadCachedAbsSize().X > 0.f) ? ReadCachedAbsSize().X : ReadSize().X; //If abs size is not yet computed, try local size.

	for (size_t i = 0; i < Content.size(); i++)
	{
		Int charIndex = CurrentFont->FindCharNearWidthLimit(Content.at(i), CharacterSize, width);

		if (charIndex >= Content.at(i).Length() - 1 || charIndex <= 0)
		{
			continue; //Either All characters in this line is within width or the first character is beyond width.
		}

		//Find the place to insert a new line character (usually between words)
		Float deltaWidth;
		Int wrapPosition = FindPreviousCharacter(WRAP_CHARS, i, charIndex, OUT deltaWidth);
		if (wrapPosition == INT_INDEX_NONE)
		{
			//Was unable to find a good place to wrap text.  Wrap at the character that went beyond boundaries.
			wrapPosition = charIndex;
		}
		else
		{
			//Increment by one to keep the current character at the previous line
			wrapPosition++;
		}

		//Divide the string into two parts
		Content.insert(Content.begin() + i + 1, Content.at(i).SubString(wrapPosition));
		Content.at(i) = Content.at(i).SubString(0, wrapPosition - 1);
	}
}

void LabelComponent::ClampText ()
{
	if (!bClampText || Content.size() <= 0)
	{
		return;
	}

	if (bWrapText)
	{
		//Figure out how many lines needs to be clamped.
		Int numClampedLines = Int(Content.size()) - MaxNumLines;
		if (numClampedLines > 0) //If Content size is greater than the permitted number of lines
		{
			DString oldClamp = std::move(ClampedText);
			ClampedText = DString::EmptyString; //std::move doesn't zero out the memory

			for (Int curLine = MaxNumLines; curLine < Int(Content.size()); ++curLine)
			{
				ClampedText += std::move(Content.at(curLine.ToUnsignedInt()));
			}
			ClampedText += oldClamp;

			//Efficiently truncate the content vector
			Content.resize(MaxNumLines.ToUnsignedInt());
		}
	}
	else //!bWrapText
	{
		if (Content.at(Content.size() - 1).Length() <= 0)
		{
			//Nothing to clamp for an empty string
			return;
		}

		//Simply remove the text beyond width from the last line.  90% of the time there will only be one line of text for unwrapped label components.
		//For multi-lined unwrapped label components, it's assumed that the predetermined line breaks are there for a particular reason and will not truncate that.
		Int charIdx = CurrentFont->FindCharNearWidthLimit(Content.at(Content.size() - 1), CharacterSize, ReadCachedAbsSize().X);

		//If charIdx is in middle of Content
		if (charIdx > 0 && charIdx < ContainerUtils::GetLast(Content).Length() - 1)
		{
			ClampedText = ContainerUtils::GetLast(Content).SubString(charIdx + 1);
			Content.at(Content.size() - 1) = ContainerUtils::GetLast(Content).SubString(0, charIdx);
		}
	}
}

void LabelComponent::ApplyHorizontalAutoSize ()
{
	if (!AutoSizeHorizontal)
	{
		return;
	}

	Float widestLine = 0.f;
	for (size_t i = 0; i < Content.size(); ++i)
	{
		widestLine = Utils::Max(CurrentFont->CalculateStringWidth(Content.at(i), CharacterSize), widestLine);
	}

	SetSize(Vector2(widestLine, ReadSize().Y));
}

void LabelComponent::ApplyVerticalAutoSize ()
{
	if (!AutoSizeVertical)
	{
		return;
	}

	Int numLines(Content.size());
	Float totalSize = numLines.ToFloat() * (CharacterSize.ToFloat() + LineSpacing);

	if (numLines > 0)
	{
		//Add a small buffer so the characters with dipping glyphs (eg: gjpq ...), are within bounds
		totalSize += (CharacterSize.ToFloat() * 0.5f);
	}

	SetSize(Vector2(ReadSize().X, totalSize));
}

void LabelComponent::RenderContent ()
{
	for (size_t i = 0; i < Content.size(); ++i)
	{
		UpdateRenderContent(i);
	}

	//Purge any extra lines from the render component.
	if (RenderComponent != nullptr)
	{
		while (RenderComponent->ReadTexts().size() > Content.size())
		{
			RenderComponent->DeleteTextData(Content.size());
		}
	}
}

void LabelComponent::UpdateRenderContent (size_t lineIdx)
{
	if (CurrentFont == nullptr || RenderComponent == nullptr)
	{
		return;
	}

	if (lineIdx >= Content.size())
	{
		//Nothing exist here anymore. Make sure the associated text data is removed
		if (lineIdx < RenderComponent->EditTexts().size())
		{
			RenderComponent->DeleteTextData(lineIdx);
		}

		return;
	}

	if (lineIdx >= RenderComponent->EditTexts().size())
	{
		//Create new text instance
		TextRenderComponent::STextData& newLineData = RenderComponent->CreateTextInstance(Content.at(lineIdx), CurrentFont.Get(), CharacterSize.ToUnsignedInt32());
		newLineData.DrawOffset.y = ((CharacterSize.ToFloat() + LineSpacing) * Float::MakeFloat(lineIdx)).Value;
	}
	else
	{
		//Update existing one
		TextRenderComponent::STextData& textData = RenderComponent->EditTexts().at(lineIdx);
		textData.SfmlInstance->setString(Content.at(lineIdx).ToSfmlString());
	}
}

void LabelComponent::UpdateRenderContentFromLineToEnd (size_t firstLineIdx)
{
	for (size_t i = firstLineIdx; i < Content.size(); ++i)
	{
		UpdateRenderContent(i);
		AlignLineHorizontally(i);
	}

	//Purge any extra lines from the render component.
	if (RenderComponent != nullptr)
	{
		while (RenderComponent->ReadTexts().size() > Content.size())
		{
			RenderComponent->DeleteTextData(Content.size());
		}
	}
}

void LabelComponent::AlignVertically ()
{
	Float lastLinePos = ReadCachedAbsPosition().Y + ((CharacterSize.ToFloat() + LineSpacing) * Float::MakeFloat(RenderComponent->ReadTexts().size()));
	Float unusedSpace = (ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y) - lastLinePos.Value;
	FirstLineOffset = 0.f; //Distance between the top of this component's yPos and the first Text's yPos.

	//Clamp unused space in case it's negative (when the characters are larger than the component, itself
	unusedSpace = Utils::Max<Float>(0.f, unusedSpace);
	if (AutoSizeVertical)
	{
		//For auto sized components, the text should fill the component from top to bottom.  Always align top in this case.
		unusedSpace = 0.f;
	}

	switch(VerticalAlignment)
	{
		case(VA_Top):
			break;
		case(VA_Center):
			FirstLineOffset = unusedSpace / 2;
			break;

		case(VA_Bottom):
			FirstLineOffset = unusedSpace;
			break;
	}

	for (size_t i = 0; i < RenderComponent->EditTexts().size(); i++)
	{
		RenderComponent->EditTexts().at(i).DrawOffset.y = (FirstLineOffset + ((CharacterSize.ToFloat() + LineSpacing) * Float::MakeFloat(i))).Value;
	}
}

void LabelComponent::AlignHorizontally ()
{
	if (CurrentFont.IsNullptr())
	{
		return;
	}

	//Use faster/simpler algorithm when aligning text to the left
	if (HorizontalAlignment == HA_Left)
	{
		for (size_t i = 0; i < RenderComponent->EditTexts().size(); i++)
		{
			RenderComponent->EditTexts().at(i).DrawOffset.x = 0.f;
		}

		return;
	}

	for (size_t i = 0; i < Content.size() && i < RenderComponent->EditTexts().size(); i++)
	{
		AlignLineHorizontally(i);
	}
}

void LabelComponent::AlignLineHorizontally (size_t lineIdx)
{
	if (lineIdx >= Content.size())
	{
		return;
	}

	if (HorizontalAlignment == HA_Left)
	{
		RenderComponent->EditTexts().at(lineIdx).DrawOffset.x = 0.f;
		return;
	}

	Float lineWidth = CurrentFont->CalculateStringWidth(Content.at(lineIdx), CharacterSize);
	Float unusedSpace = ReadCachedAbsSize().X - lineWidth;
	unusedSpace = Utils::Max<Float>(unusedSpace, 0.f); //Avoid negative values (this could happen with unwrapped lines going beyond width).
	Float horizontalShift = 0.f;

	switch (HorizontalAlignment)
	{
		case(HA_Center):
			horizontalShift = unusedSpace / 2.f;
			break;

		case(HA_Right):
			horizontalShift = unusedSpace;
			break;
	}

	RenderComponent->EditTexts().at(lineIdx).DrawOffset.x = horizontalShift.Value;
}

Int LabelComponent::FindPreviousCharacter (const std::vector<char>& targetCharacters, size_t textLine, Int startPosition, Float& outDeltaWidth) const
{
	CHECK(CurrentFont != nullptr)

	outDeltaWidth = 0.f;
	if (Content.at(textLine).Length() <= 0)
	{
		return INT_INDEX_NONE;
	}

	if (startPosition < 0)
	{
		startPosition = Content.at(textLine).Length() - 1;
	}

	StringIterator iter(&Content.at(textLine));
	iter += startPosition;

	Int charIdx = startPosition;
	while (true)
	{
		DString curChar = *iter;
		for (size_t j = 0; j < targetCharacters.size(); j++)
		{
			if (targetCharacters.at(j) == curChar)
			{
				return charIdx;
			}
		}

		outDeltaWidth += CurrentFont->CalculateStringWidth(curChar, CharacterSize);

		if (iter.IsAtBeginning())
		{
			break;
		}
		--iter;
		charIdx--;
	}

	return INT_INDEX_NONE;
}

void LabelComponent::CalculateMaxNumLines ()
{
	Int oldMaxLines = MaxNumLines;

	if (RenderComponent.IsNullptr())
	{
		MaxNumLines = 1;
	}
	else
	{
		Float result = ReadCachedAbsSize().Y / (CharacterSize.ToFloat() + LineSpacing);
		result = trunc(result.Value);
		MaxNumLines = Utils::Max<Int>(1, result.ToInt());
	}
}
SD_END