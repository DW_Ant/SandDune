/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CheckboxComponent.cpp
=====================================================================
*/

#include "CheckboxComponent.h"
#include "FrameComponent.h"
#include "LabelComponent.h"

IMPLEMENT_CLASS(SD::CheckboxComponent, SD::GuiComponent)
SD_BEGIN

void CheckboxComponent::InitProps ()
{
	Super::InitProps();

	bEnabled = true;
	bPressedDown = false;
	bHovered = false;
	bChecked = false;
	bCheckboxRightSide = true;
}

void CheckboxComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const CheckboxComponent* checkboxTemplate = dynamic_cast<const CheckboxComponent*>(objTemplate);
	if (checkboxTemplate != nullptr)
	{
		SetCheckboxRightSide(checkboxTemplate->GetCheckboxRightSide());

		bool bCreatedObj;
		CaptionComponent = ReplaceTargetWithObjOfMatchingClass(CaptionComponent.Get(), checkboxTemplate->CaptionComponent.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(CaptionComponent);
		}

		if (CaptionComponent.IsValid())
		{
			CaptionComponent->CopyPropertiesFrom(checkboxTemplate->CaptionComponent.Get());
		}

		CheckboxSprite = ReplaceTargetWithObjOfMatchingClass(CheckboxSprite.Get(), checkboxTemplate->CheckboxSprite.Get(), OUT bCreatedObj);
		if (bCreatedObj && AddComponent(CheckboxSprite))
		{
			if (SpriteComponent* spriteComp = CheckboxSprite->GetCenterCompAs<SpriteComponent>())
			{
				spriteComp->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, CheckboxComponent, HandleSpriteTextureChanged, void));
			}
		}

		if (CheckboxSprite.IsValid())
		{
			CheckboxSprite->CopyPropertiesFrom(checkboxTemplate->CheckboxSprite.Get());
		}
	}
}

bool CheckboxComponent::CanBeFocused () const
{
	return (FocusInterface::CanBeFocused() && bEnabled);
}

bool CheckboxComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (keyEvent.key.code == sf::Keyboard::Return || keyEvent.key.code == sf::Keyboard::Space)
	{
		if (keyEvent.type == sf::Event::KeyPressed)
		{
			bPressedDown = true;
			RefreshCheckboxSprite();
			return true;
		}
		else if (keyEvent.type == sf::Event::KeyReleased)
		{
			bPressedDown = false;
			bChecked = !bChecked;
			RefreshCheckboxSprite();
			return true;
		}
	}

	return true;
}

bool CheckboxComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return false;
}

void CheckboxComponent::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	if (CheckboxSprite == nullptr)
	{
		return;
	}

	SpriteComponent* spriteComp = CheckboxSprite->GetCenterCompAs<SpriteComponent>();
	if (spriteComp == nullptr)
	{
		return;
	}

	Vector2 textureSize = spriteComp->GetTextureSize();

	//Reduce texture size since the sprite is subdivided
	textureSize.X *= 0.5f;
	textureSize.Y *= 0.25f;

	Float textureSizeRatio = textureSize.X / textureSize.Y;

	//The sprite height is equal to the height of this component.  Compute the width so that the texture's aspect ratio is preserved.
	Float spriteWidth = ReadCachedAbsSize().Y * textureSizeRatio;
	CheckboxSprite->SetSize(Vector2(spriteWidth, ReadCachedAbsSize().Y));

	RefreshComponentPositions();
}


void CheckboxComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	bool bCurrentHovered = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));

	if (bCurrentHovered != bHovered)
	{
		bHovered = bCurrentHovered;
		RefreshCheckboxSprite();
	}
}

void CheckboxComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//If user released mouse button outside of checkbox borders, then clear pressed down state.
	if (bPressedDown && eventType == sf::Event::MouseButtonReleased && sfmlEvent.button == sf::Mouse::Left &&
		!IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		bPressedDown = false;
		RefreshCheckboxSprite();
	}
}

bool CheckboxComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (IsVisible() && sfmlEvent.button == sf::Mouse::Left &&
		IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		bPressedDown = (eventType == sf::Event::MouseButtonPressed);
		if (eventType == sf::Event::MouseButtonReleased)
		{
			bChecked = !bChecked;
			if (OnChecked.IsBounded())
			{
				OnChecked(this);
			}
		}

		RefreshCheckboxSprite();
		return true;
	}

	return Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType);
}

bool CheckboxComponent::AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(inputEvent, mousePosX, mousePosY));
}

void CheckboxComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeCaptionComponent();
	InitializeCheckboxSprite();
	RefreshCheckboxSprite();
	RefreshComponentPositions();
}

void CheckboxComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;
	RefreshCheckboxSprite();
}

void CheckboxComponent::SetChecked (bool bNewChecked)
{
	if (bNewChecked != bChecked)
	{
		bChecked = bNewChecked;
		RefreshCheckboxSprite();
	}
}

void CheckboxComponent::SetCheckboxRightSide (bool bNewCheckboxRightSide)
{
	if (bCheckboxRightSide == bNewCheckboxRightSide)
	{
		return;
	}

	bCheckboxRightSide = bNewCheckboxRightSide;
	if (CheckboxSprite.IsValid() && CaptionComponent.IsValid())
	{
		RefreshComponentPositions();
		CaptionComponent->SetHorizontalAlignment((bCheckboxRightSide) ? LabelComponent::eHorizontalAlignment::HA_Left : LabelComponent::eHorizontalAlignment::HA_Right);
	}
}

bool CheckboxComponent::GetEnabled () const
{
	return bEnabled;
}

bool CheckboxComponent::GetHovered () const
{
	return bHovered;
}

bool CheckboxComponent::GetChecked () const
{
	return bChecked;
}

bool CheckboxComponent::GetCheckboxRightSide () const
{
	return bCheckboxRightSide;
}

void CheckboxComponent::InitializeCaptionComponent ()
{
	CaptionComponent = LabelComponent::CreateObject();
	if (AddComponent(CaptionComponent))
	{
		CaptionComponent->SetAutoRefresh(false);
		CaptionComponent->SetWrapText(true);
		CaptionComponent->SetClampText(true);
		CaptionComponent->SetHorizontalAlignment((bCheckboxRightSide) ? LabelComponent::eHorizontalAlignment::HA_Left : LabelComponent::eHorizontalAlignment::HA_Right);
		CaptionComponent->SetVerticalAlignment(LabelComponent::eVerticalAlignment::VA_Center);
		CaptionComponent->SetAutoRefresh(true);
	}
}

void CheckboxComponent::InitializeCheckboxSprite ()
{
	CheckboxSprite = FrameComponent::CreateObject();
	if (AddComponent(CheckboxSprite))
	{
		if (BorderRenderComponent* borderComp = CheckboxSprite->GetBorderComp())
		{
			borderComp->Destroy();
		}

		CheckboxSprite->SetLockedFrame(true);
		CheckboxSprite->SetBorderThickness(0.f);
		CheckboxSprite->SetCenterTexture(GuiTheme::GetGuiTheme()->CheckboxTexture.Get());
	}
}

void CheckboxComponent::RefreshComponentPositions ()
{
	CHECK(CaptionComponent.IsValid() && CheckboxSprite.IsValid())

	//Setting the left and right anchors on the caption component to scale the component to fit the rest of the Checkbox based on the amount of space the sprite takes.
	//Only set one of the anchors on the sprite component to snap it on one of the sides.

	const Float checkboxWidth = CheckboxSprite->ReadSize().X;
	if (bCheckboxRightSide)
	{
		CaptionComponent->SetAnchorLeft(0.f);
		CaptionComponent->SetAnchorRight(checkboxWidth);
		CheckboxSprite->SetAnchorLeft(-1.f);
		CheckboxSprite->SetAnchorRight(0.f);
	}
	else
	{
		CaptionComponent->SetAnchorLeft(checkboxWidth);
		CaptionComponent->SetAnchorRight(0.f);
		CheckboxSprite->SetAnchorLeft(0.f);
		CheckboxSprite->SetAnchorRight(-1.f);
	}
}

void CheckboxComponent::RefreshCheckboxSprite ()
{
	CHECK(CheckboxSprite != nullptr)
	
	SpriteComponent* spriteComp = CheckboxSprite->GetCenterCompAs<SpriteComponent>();
	if (spriteComp == nullptr)
	{
		return;
	}

	Int column = Int(bChecked);
	Int row = 0;

	if (!bEnabled)
	{
		row = 3;
	}
	else if (bPressedDown)
	{
		row = 2;
	}
	else if (bHovered)
	{
		row = 1;
	}

	spriteComp->SetSubDivision(2, 4, column, row);
}

void CheckboxComponent::HandleSpriteTextureChanged ()
{
	RefreshCheckboxSprite();
}
SD_END