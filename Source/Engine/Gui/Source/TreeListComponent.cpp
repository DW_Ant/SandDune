/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TreeListComponent.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "HoverComponent.h"
#include "LabelComponent.h"
#include "ScrollbarComponent_Deprecated.h"
#include "SingleSpriteButtonState.h"
#include "TreeBranchIterator.h"
#include "TreeListComponent.h"

IMPLEMENT_CLASS(SD::TreeListComponent, SD::GuiComponent)
SD_BEGIN

void TreeListComponent::SDataBranch::LinkBranches (SDataBranch& outParent, SDataBranch& outChild)
{
	outParent.Children.push_back(&outChild);
	outChild.ParentBranch = &outParent;
}

bool TreeListComponent::SDataBranch::IsValid () const
{
	return Data != nullptr;
}

bool TreeListComponent::SDataBranch::IsVisible () const
{
	if (ParentBranch == nullptr)
	{
		return true; //Root branches are always visible
	}
	else if (!ParentBranch->bExpanded)
	{
		return false;
	}
	else
	{
		return ParentBranch->IsVisible();
	}
}

Int TreeListComponent::SDataBranch::GetNumParentBranches () const
{
	if (ParentBranch != nullptr)
	{
		return ParentBranch->GetNumParentBranches() + 1;
	}

	return 0;
}

Int TreeListComponent::SDataBranch::CountNumChildren (bool bIncludeCollapsed) const
{
	Int result = -1; //negative one to ignore this branch.  This branch should not be included in count.

	TreeBranchIterator<const TreeListComponent::SDataBranch> iter(this);
	while (iter.GetSelectedBranch() != nullptr)
	{
		result++;
		(bIncludeCollapsed) ? iter++ : iter.SelectNextVisibleBranch();
	};

	return result;
}

void TreeListComponent::InitProps ()
{
	Super::InitProps();

	BrowseJumpAmount = 10;
	IndentSize = 14.f;
	BranchHeight = 16.f;
	ConnectionThickness = 4.f;
	ConnectionColor = sf::Color::Black;

	NumPendingDeleteComponents = 0;
	MaxNumPendingDeleteComponents = 4096;
	SelectedItemIdx = INT_INDEX_NONE;
	SelectedBranch = nullptr;
	BrowseIndex = INT_INDEX_NONE;
	ExpandButtonTexture = nullptr;
	CollapseButtonTexture = nullptr;
	SelectedBar = nullptr;
	SelectedBarOwner = nullptr;
	HoverBar = nullptr;
	HoverBarOwner = nullptr;
	bIgnoreHoverMovement = false;
	ListScrollbar = nullptr;
}

void TreeListComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TreeListComponent* treeTemplate = dynamic_cast<const TreeListComponent*>(objTemplate);
	if (treeTemplate != nullptr)
	{
		BrowseJumpAmount = treeTemplate->BrowseJumpAmount;
		IndentSize = treeTemplate->IndentSize;
		BranchHeight = treeTemplate->BranchHeight;
		ConnectionThickness = treeTemplate->ConnectionThickness;
		ConnectionColor = treeTemplate->ConnectionColor;

		SetExpandButtonTexture(treeTemplate->GetExpandButtonTexture());
		SetCollapseButtonTexture(treeTemplate->GetCollapseButtonTexture());

		bool bCreatedObj;
		SelectedBar = ReplaceTargetWithObjOfMatchingClass(SelectedBar.Get(), treeTemplate->GetSelectedBar(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetSelectedBar(SelectedBar.Get());
		}

		if (SelectedBar.IsValid())
		{
			SelectedBar->CopyPropertiesFrom(treeTemplate->GetSelectedBar());
		}

		HoverBar = ReplaceTargetWithObjOfMatchingClass(HoverBar.Get(), treeTemplate->GetHoverBar(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetHoverBar(HoverBar.Get());
		}

		if (HoverBar.IsValid())
		{
			HoverBar->CopyPropertiesFrom(treeTemplate->GetHoverBar());
		}

		ListScrollbar = ReplaceTargetWithObjOfMatchingClass(ListScrollbar.Get(), treeTemplate->GetListScrollbar(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetListScrollbar(ListScrollbar.Get());
		}

		if (ListScrollbar.IsValid())
		{
			ListScrollbar->CopyPropertiesFrom(treeTemplate->GetListScrollbar());
		}
	}
}

void TreeListComponent::BeginObject ()
{
	Super::BeginObject();

	Engine::FindEngine()->RegisterPreGarbageCollectEvent(SDFUNCTION(this, TreeListComponent, HandleGarbageCollection, void));

	if (ExpandButtonTexture.IsNullptr())
	{
		ExpandButtonTexture = GuiTheme::GetGuiTheme()->TreeListAddButton;
	}

	if (CollapseButtonTexture.IsNullptr())
	{
		CollapseButtonTexture = GuiTheme::GetGuiTheme()->TreeListSubtractButton;
	}
}

bool TreeListComponent::CanBeFocused () const
{
	return (FocusInterface::CanBeFocused() && IsVisible() && TreeList.size() > 0);
}

void TreeListComponent::GainFocus ()
{
	FocusInterface::GainFocus();

	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetVisibility(true);
		SetBrowseIndex(Utils::Max<Int>(SelectedItemIdx, 0));
	}
}

void TreeListComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetVisibility(false);
	}
}

bool TreeListComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (keyEvent.type == sf::Event::KeyReleased)
	{
		if (keyEvent.key.code == sf::Keyboard::Return)
		{
			SetSelectedItem(BrowseIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Up)
		{
			Int scrollAmount = (InputBroadcaster::GetCtrlHeld()) ? BrowseJumpAmount : 1;
			Int newIndex = FindBranchIdxFromAnchor(BrowseIndex, -scrollAmount, false);
			if (newIndex < 0)
			{
				newIndex = Utils::Max<Int>(FindLastVisibleBranchIdx(), 0); //select last visible entry
			}

			SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Down)
		{
			Int scrollAmount = (InputBroadcaster::GetCtrlHeld()) ? BrowseJumpAmount : 1;
			Int newIndex = FindBranchIdxFromAnchor(BrowseIndex, scrollAmount, false);
			if (newIndex >= TreeList.size() || newIndex == INT_INDEX_NONE)
			{
				newIndex = 0;
			}

			SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Left)
		{
			//Collapse button
 			CollapseBranch(BrowseIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Right)
		{
			//Expand button
			ExpandBranch(BrowseIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Space)
		{
			//Toggle button
			ButtonComponent* selectedButton = FindExpandButtonAtIdx(BrowseIndex);
			if (selectedButton != nullptr)
			{
				selectedButton->SetButtonUp(true, true);
			}

			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::PageUp && ListScrollbar.IsValid())
		{
			Int scrollAmount = ListScrollbar->GetNumVisibleScrollPositions();
			Int newIndex = FindBranchIdxFromAnchor(BrowseIndex, -scrollAmount, false);
			if (newIndex == INT_INDEX_NONE)
			{
				newIndex = Utils::Max<Int>(FindLastVisibleBranchIdx(), 0); //select last visible entry
			}

			SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::PageDown && ListScrollbar.IsValid())
		{
			Int scrollAmount = ListScrollbar->GetNumVisibleScrollPositions();
			Int newIndex = FindBranchIdxFromAnchor(BrowseIndex, scrollAmount, false);
			if (newIndex >= TreeList.size() || newIndex == INT_INDEX_NONE)
			{
				newIndex = 0;
			}

			SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Home)
		{
			SetBrowseIndex(0);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::End)
		{
			Int newIndex = Utils::Max<Int>(FindLastVisibleBranchIdx(), 0); //select last visible entry
			SetBrowseIndex(newIndex);
			return true;
		}

	}

	return false;
}

bool TreeListComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return false;
}

void TreeListComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
	if (!IsVisible() || bIgnoreHoverMovement)
	{
		return;
	}

	//Update browsing highlight bar and region
	bool bIsHovering = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));
	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetVisibility(bIsHovering);
	}

	if (!bIsHovering)
	{
		return;
	}

	LabelComponent* hoveredLabel = FindLabelAtCoordinates(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
	if (hoveredLabel == nullptr)
	{
		HoverBarOwner->SetVisibility(false);
		return;
	}

	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetSize(Vector2(hoveredLabel->ReadCachedAbsSize().X, BranchHeight));
		HoverBarOwner->SetPosition(hoveredLabel->ReadPosition());
	}
}

bool TreeListComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (IsVisible() && eventType == sf::Event::MouseButtonReleased && sfmlEvent.button == sf::Mouse::Left)
	{
		bool bWithinBounds = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));
		if (!bWithinBounds)
		{
			return false;
		}

		SDataBranch* selectedBranch = nullptr;
		LabelComponent* hoveredLabel = FindLabelAtCoordinates(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
		if (hoveredLabel != nullptr)
		{
			CHECK(TreeList.size() > 0) //If we have clicked on a label, we should have some tree list data.

			Int labelIdx = INT_INDEX_NONE;
			for (UINT_TYPE i = 0; i < TreeLabels.size(); i++)
			{
				if (TreeLabels.at(i) == hoveredLabel)
				{
					labelIdx = Int(i);
					break;
				}
			}

			CHECK(labelIdx != INT_INDEX_NONE)

			//Find branch index from the label idx
			Int scrollPosition = (ListScrollbar.IsValid()) ? ListScrollbar->GetScrollPosition() : 0;
			Int branchIdx = 0;
			for (TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter.SelectNextVisibleBranch())
			{
				if ((branchIdx - scrollPosition) == labelIdx)
				{
					selectedBranch = iter.GetSelectedBranch();
					break;
				}

				branchIdx++;
			}
		}

		SetSelectedItem(selectedBranch);
		return true;
	}

	return false;
}

void TreeListComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeScrollbar();
	InitializeSolidColors();
}

void TreeListComponent::Destroyed ()
{
	Engine::FindEngine()->RemovePreGarbageCollectEvent(SDFUNCTION(this, TreeListComponent, HandleGarbageCollection, void));
	ClearTreeList(); //Need to deallocate the list of pointers

	Super::Destroyed();
}

void TreeListComponent::SortDataList (std::vector<SDataBranch*>& outDataList)
{
	//Find the root branch
	UINT_TYPE rootIdx = UINT_INDEX_NONE;
	for (UINT_TYPE i = 0; i < outDataList.size(); i++)
	{
		if (outDataList.at(i)->ParentBranch == nullptr)
		{
			rootIdx = i;
			break;
		}
	}

	if (rootIdx == UINT_INDEX_NONE)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to sort data list.  Unable to find root branch."));
		return;
	}

	UINT_TYPE i = 0;
	for (TreeBranchIterator<SDataBranch> iter(outDataList.at(rootIdx)); iter.GetSelectedBranch() != nullptr; iter++)
	{
		outDataList.at(i) = iter.GetSelectedBranch();
		i++;
	}
}

void TreeListComponent::ExpandBranch (Int branchIdx)
{
	SDataBranch* branch = GetBranchFromIdx(branchIdx);
	if (branch != nullptr)
	{
		ExpandBranch(branch);
	}
}

void TreeListComponent::ExpandBranch (SDataBranch* branch)
{
	branch->bExpanded = true;

	if (ListScrollbar.IsValid())
	{
		ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
	}

	RefreshTree();
	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		if (ExpandButtons.at(i).Branch == branch)
		{
			CalcHoverTransform(i);
			break;
		}
	}
}

void TreeListComponent::CollapseBranch (Int branchIdx)
{
	SDataBranch* branch = GetBranchFromIdx(branchIdx);
	if (branch != nullptr)
	{
		CollapseBranch(branch);
	}
}

void TreeListComponent::CollapseBranch (SDataBranch* branch)
{
	branch->bExpanded = false;

	if (ListScrollbar.IsValid())
	{
		ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
	}

	RefreshTree();

	//Clear hover transform that's hovering over children branches
	CalcHoverTransform(UINT_INDEX_NONE);
}

void TreeListComponent::ExpandAll ()
{
	SetExpandForAllBranches(true);
}

void TreeListComponent::CollapseAll ()
{
	SetExpandForAllBranches(false);
}

void TreeListComponent::JumpScrollbarToViewBranch (Int branchIdx)
{
	if (TreeList.size() <= 0 || ListScrollbar.IsNullptr())
	{
		return;
	}

	Int scrollIdx = 0;
	Int numVisibleBranches = 0;
	for (TreeBranchIterator<const SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
	{
		if (scrollIdx == branchIdx)
		{
			if (ListScrollbar->IsIndexVisible(numVisibleBranches))
			{
				return; //already within view
			}
			else if (numVisibleBranches < ListScrollbar->GetScrollPosition())
			{
				//Scroll to up
				ListScrollbar->SetScrollPosition(numVisibleBranches - 3);
				return;
			}
			else
			{
				//Scroll down
				ListScrollbar->SetScrollPosition(numVisibleBranches - ListScrollbar->GetNumVisibleScrollPositions() + 3);
				return;
			}
		}

		scrollIdx++;
		if (iter.GetSelectedBranch()->IsVisible())
		{
			numVisibleBranches++;
		}
	}
}

void TreeListComponent::SetMaxNumPendingDeleteComponents (Int newMaxNumPendingDeleteComponents)
{
	MaxNumPendingDeleteComponents = newMaxNumPendingDeleteComponents;
	if (MaxNumPendingDeleteComponents > 0 && NumPendingDeleteComponents > MaxNumPendingDeleteComponents)
	{
		NumPendingDeleteComponents = 0; //Set this var early to protect against inf recursion (if some other object set this function during garbage collection).
		Engine::FindEngine()->CollectGarbage();
	}
}

void TreeListComponent::SetBrowseIndex (Int newBrowseIndex)
{
	BrowseIndex = newBrowseIndex;

	if (BrowseIndex >= 0 && BrowseIndex < TreeList.size())
	{
		JumpScrollbarToViewBranch(BrowseIndex);
		CalcHoverTransformAtIdx(BrowseIndex);
	}
}

void TreeListComponent::SetSelectedItem (Int itemIndex)
{
	Int oldSelectedItemIdx = SelectedItemIdx;
	SelectedItemIdx = itemIndex;
	SelectedBranch = GetBranchFromIdx(SelectedItemIdx);
	if (SelectedBranch == nullptr)
	{
		//The item index was not valid index
		SelectedItemIdx = INT_INDEX_NONE;
	}

	UpdateSelectedItem(oldSelectedItemIdx != SelectedItemIdx);
}

void TreeListComponent::SetSelectedItem (SDataBranch* item)
{
	Int oldSelectedItemIdx = SelectedItemIdx;

	if (item == nullptr || TreeList.size() <= 0)
	{
		SelectedItemIdx = INT_INDEX_NONE;
		SelectedBranch = nullptr;
	}
	else
	{
		//Verify the item is within the tree list
		Int index = -1;
		for (TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
		{
			index++;
			if (iter.GetSelectedBranch() == item)
			{
				SelectedBranch = item;
				break;
			}
		}

		SelectedItemIdx = index;
	}

	UpdateSelectedItem(oldSelectedItemIdx != SelectedItemIdx);
}

void TreeListComponent::SetTreeList (const std::vector<SDataBranch*>& newTreeList)
{
	ClearTreeList();

	TreeList = newTreeList;

	RefreshTree();
}

void TreeListComponent::ClearTreeList ()
{
	while (TreeList.size() > 0)
	{
		delete TreeList.at(TreeList.size() - 1)->Data;
		delete TreeList.at(TreeList.size() - 1);
		TreeList.pop_back();
	}

	DestroyTreeComponents();

	if (ListScrollbar.IsValid())
	{
		ListScrollbar->SetMaxScrollPosition(0);
	}
}

void TreeListComponent::SetExpandButtonTexture (Texture* newExpandButtonTexture)
{
	ExpandButtonTexture = newExpandButtonTexture;

	for (UINT_TYPE i = 0; i < ExpandButtons.size(); ++i)
	{
		if (ExpandButtons.at(i).Button != nullptr && ExpandButtons.at(i).Branch != nullptr && ExpandButtons.at(i).Branch->bExpanded)
		{
			ExpandButtons.at(i).Button->GetBackground()->SetCenterTexture(ExpandButtonTexture.Get());
		}
	}
}

void TreeListComponent::SetCollapseButtonTexture (Texture* newCollapseButtonTexture)
{
	CollapseButtonTexture = newCollapseButtonTexture;

	for (UINT_TYPE i = 0; i < ExpandButtons.size(); ++i)
	{
		if (ExpandButtons.at(i).Button != nullptr && ExpandButtons.at(i).Branch != nullptr && !ExpandButtons.at(i).Branch->bExpanded)
		{
			ExpandButtons.at(i).Button->GetBackground()->SetCenterTexture(CollapseButtonTexture.Get());
		}
	}
}

void TreeListComponent::SetSelectedBar (ColorRenderComponent* newSelectedBar)
{
	if (SelectedBar.IsValid())
	{
		SelectedBar->Destroy();
	}

	CHECK(SelectedBarOwner.IsValid())
	if (newSelectedBar != nullptr && SelectedBarOwner->AddComponent(newSelectedBar))
	{
		SelectedBar = newSelectedBar;
	}
}

void TreeListComponent::SetHoverBar (ColorRenderComponent* newHoverBar)
{
	CHECK(HoverBarOwner.IsValid())
	if (HoverBar.IsValid())
	{
		HoverBar->Destroy();
	}

	if (newHoverBar != nullptr && HoverBarOwner->AddComponent(newHoverBar))
	{
		HoverBar = newHoverBar;
	}
}

void TreeListComponent::SetListScrollbar (ScrollbarComponent_Deprecated* newListScrollbar)
{
	if (ListScrollbar.IsValid())
	{
		ListScrollbar->Destroy();
	}

	if (newListScrollbar != nullptr && AddComponent(newListScrollbar))
	{
		ListScrollbar = newListScrollbar;
		ListScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, TreeListComponent, HandleScrollPosChanged, void, Int));
	}
}

Texture* TreeListComponent::GetExpandButtonTexture () const
{
	return ExpandButtonTexture.Get();
}

Texture* TreeListComponent::GetCollapseButtonTexture () const
{
	return CollapseButtonTexture.Get();
}

ColorRenderComponent* TreeListComponent::GetSelectedBar () const
{
	return SelectedBar.Get();
}

ColorRenderComponent* TreeListComponent::GetHoverBar () const
{
	return HoverBar.Get();
}

ScrollbarComponent_Deprecated* TreeListComponent::GetListScrollbar () const
{
	return ListScrollbar.Get();
}

Int TreeListComponent::GetBrowseIndex () const
{
	return BrowseIndex;
}

TreeListComponent::SDataBranch* TreeListComponent::GetBranchFromIdx (Int treeIdx) const
{
	if (treeIdx < 0 || TreeList.size() <= 0)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to find branch from Idx %s.  Out of bounds from TreeList of size %s."), treeIdx, Int(TreeList.size()));
		return nullptr;
	}

	return TreeList.at(treeIdx.ToUnsignedInt());
}

void TreeListComponent::InitializeScrollbar ()
{
	ListScrollbar = ScrollbarComponent_Deprecated::CreateObject();
	if (!AddComponent(ListScrollbar))
	{
		return;
	}

	ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
	ListScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, TreeListComponent, HandleScrollPosChanged, void, Int));
}

void TreeListComponent::InitializeSolidColors ()
{
	//Initialize selected bar
	SelectedBarOwner = GuiComponent::CreateObject();
	if (AddComponent(SelectedBarOwner))
	{
		SelectedBar = ColorRenderComponent::CreateObject();
		if (!SelectedBarOwner->AddComponent(SelectedBar))
		{
			SelectedBar->SolidColor = sf::Color::Cyan;
			SelectedBarOwner->SetVisibility(false); //This will become visible upon selecting an item
		}
	}

	HoverBarOwner = GuiComponent::CreateObject();
	if (AddComponent(HoverBarOwner))
	{
		HoverBar = ColorRenderComponent::CreateObject();
		if (HoverBarOwner->AddComponent(HoverBar))
		{
			HoverBar->SolidColor = sf::Color(200, 200, 200, 96);
			HoverBar->SetVisibility(false); //This will become visible upon hovering or browsing items
		}
	}
}

void TreeListComponent::UpdateSelectedItem (bool bInvokeCallback)
{
	UpdateSelectedBar();

	if (bInvokeCallback && OnOptionSelected.IsBounded())
	{
		OnOptionSelected(SelectedItemIdx);
	}
}

LabelComponent* TreeListComponent::FindLabelAtCoordinates (Float x, Float y) const
{
	for (UINT_TYPE i = 0; i < TreeLabels.size(); i++)
	{
		//If the labels are in sorted order, keep going down the list until the label's bottom bounds is beyond the mouse.
		if (TreeLabels.at(i)->ReadCachedAbsPosition().Y + TreeLabels.at(i)->ReadCachedAbsSize().Y >= y)
		{
			return TreeLabels.at(i).Get();
		}
	}

	return nullptr;
}

Int TreeListComponent::CountNumVisibleEntries () const
{
	if (TreeList.size() <= 0)
	{
		return 0;
	}

	Int result = 0;
	TreeBranchIterator<const TreeListComponent::SDataBranch> iter(TreeList.at(0));
	while (iter.GetSelectedBranch() != nullptr)
	{
		result++;
		iter.SelectNextVisibleBranch();
	}

	return result;
}

Int TreeListComponent::FindBranchIdxFromAnchor (Int anchorBranchIdx, Int distanceFromAnchor, bool bIncludeCollapsed) const
{
	if (bIncludeCollapsed) //No need to check for collapsed branches; use fast solution rather than iteration
	{
		Int result = anchorBranchIdx + distanceFromAnchor;
		if (result < 0)
		{
			result = INT_INDEX_NONE;
		}

		return result;
	}

	Int curIdx = Utils::Min(anchorBranchIdx, Int(TreeList.size() - 1));
	Int stepsFromAnchor = 0;
	while (true)
	{
		(distanceFromAnchor > 0) ? curIdx++ : curIdx--;

		if (curIdx < 0) //Reached beyond idx 0 in TreeList
		{
			return INT_INDEX_NONE;
		}
		else if (curIdx >= TreeList.size())
		{
			return INT_INDEX_NONE;
		}

		if (TreeList.at(curIdx.ToUnsignedInt())->IsVisible())
		{
			stepsFromAnchor++;
			if (stepsFromAnchor == Int::Abs(distanceFromAnchor))
			{
				return curIdx;
			}
		}
	}

	return INT_INDEX_NONE;
}

TreeListComponent::SDataBranch* TreeListComponent::FindFirstVisibleBranch () const
{
	if (TreeList.size() <= 0)
	{
		return nullptr;
	}

	if (ListScrollbar->GetNumVisibleScrollPositions() >= ListScrollbar->GetMaxScrollPosition())
	{
		return TreeList.at(0);
	}

	Int scrollIdx = -1;
	TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0));
	while (iter.GetSelectedBranch() != nullptr)
	{
		scrollIdx++;
		if (scrollIdx >= ListScrollbar->GetScrollPosition())
		{
			return iter.GetSelectedBranch();
		}

		iter.SelectNextVisibleBranch();
	}

	GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to find first visible branch since the scroll bar is at an invalid scroll position:  %s out of %s with num visible scroll positions being %s"), ListScrollbar->GetScrollPosition(), ListScrollbar->GetMaxScrollPosition(), ListScrollbar->GetNumVisibleScrollPositions());
	return nullptr;
}

Int TreeListComponent::FindLastVisibleBranchIdx () const
{
	for (Int i = Int(TreeList.size() - 1); i >= 0; i--)
	{
		if (TreeList.at(i.ToUnsignedInt())->IsVisible())
		{
			return i;
		}
	}

	GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to find last visible branch from %s since all of the branches are collapsed."), ToString());
	return INT_INDEX_NONE;
}

ButtonComponent* TreeListComponent::FindExpandButtonAtIdx (Int branchIdx) const
{
	SDataBranch* branch = GetBranchFromIdx(branchIdx);
	if (branch == nullptr)
	{
		return nullptr;
	}

	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		if (ExpandButtons.at(i).Branch == branch)
		{
			return ExpandButtons.at(i).Button;
		}
	}

	return nullptr;
}

void TreeListComponent::SetExpandForAllBranches (bool bExpanded)
{
	if (TreeList.size() <= 0)
	{
		return;
	}

	for (TreeBranchIterator<SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
	{
		iter.GetSelectedBranch()->bExpanded = bExpanded;
	}

	if (ListScrollbar.IsValid())
	{
		ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
	}

	RefreshTree();
}

void TreeListComponent::RefreshComponentTransforms ()
{
	//Reconstruct the tree entries first to place the buttons, connections, and labels
	RefreshTree();

	UpdateSelectedBar();

	//Simply hide the hover bar since that's based on mouse position which is likely not relevant to the new change in transform dimensions.
	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetVisibility(false);
	}

	if (ListScrollbar.IsValid())
	{
		ListScrollbar->SetSize(Vector2(ListScrollbar->ReadSize().X, ReadCachedAbsSize().Y));
		ListScrollbar->SetPosition(Vector2((ReadCachedAbsSize().X + ReadCachedAbsPosition().X) - ListScrollbar->ReadCachedAbsSize().X, 0.f));
		ListScrollbar->SetNumVisibleScrollPositions((ReadCachedAbsSize().Y / BranchHeight).ToInt());
	}
}

void TreeListComponent::CalcHoverTransform (UINT_TYPE expandButtonIdx)
{
	if (expandButtonIdx == UINT_INDEX_NONE)
	{
		bIgnoreHoverMovement = false;
		return;
	}

	bIgnoreHoverMovement = true;
	Int numOptions = ExpandButtons.at(expandButtonIdx).Branch->CountNumChildren(false);
	numOptions++; //include parent branch

	//Set hover bar over the label component next to button
	Vector2 newPosition = ExpandButtons.at(expandButtonIdx).Button->GetPosition();
	newPosition.X += ExpandButtons.at(expandButtonIdx).Button->ReadCachedAbsSize().X;
	HoverBarOwner->SetPosition(newPosition);

	//Ensure the hover bar doesn't go beyond the bottom bounds
	const Float maxSizeY = ReadCachedAbsSize().Y - HoverBarOwner->ReadCachedAbsSize().Y;
	HoverBarOwner->SetSize(Vector2(HoverBarOwner->ReadSize().X, Utils::Min(BranchHeight * numOptions.ToFloat(), maxSizeY)));
}

void TreeListComponent::CalcHoverTransformAtIdx (Int branchIdx)
{
	if (HoverBarOwner.IsNullptr())
	{
		return;
	}

	SDataBranch* branch = GetBranchFromIdx(branchIdx);
	if (branch != nullptr)
	{
		bool bWithinScrollRange = CalcTransformAtIdx(HoverBarOwner.Get(), branch);
		HoverBarOwner->SetVisibility(bWithinScrollRange);
	}
}

void TreeListComponent::UpdateSelectedBar ()
{
	if (SelectedBarOwner.IsNullptr())
	{
		return;
	}

	bool bWithinScrollRange = CalcTransformAtIdx(SelectedBarOwner.Get(), SelectedBranch);
	SelectedBarOwner->SetVisibility(bWithinScrollRange);
}

bool TreeListComponent::CalcTransformAtIdx (GuiComponent* component, TreeListComponent::SDataBranch* branch)
{
	if (TreeList.size() <= 0 || branch == nullptr)
	{
		return false;
	}

	//Find out how many branches the selected bar must offset from
	Int scrollIdx = 0;
	Int maxVisibleScrollIdx = (ListScrollbar.IsValid()) ? ListScrollbar->GetLastVisibleScrollPosition() : SD_MAXINT;
	for (TreeBranchIterator<const TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter.SelectNextVisibleBranch())
	{
		if (iter.GetSelectedBranch() == branch)
		{
			break;
		}

		scrollIdx++;
		if (scrollIdx > maxVisibleScrollIdx)
		{
			//Selected branch is either collapsed or beyond current scroll position
			return false;
		}
	}

	Int topScrollIdx = (ListScrollbar.IsValid()) ? ListScrollbar->GetScrollPosition() : 0;
	if (scrollIdx < topScrollIdx)
	{
		//selected branch is above current scroll position.
		return false;
	}

	//Now we can finally calculate the selected bar's transformation
	Float absX = branch->GetNumParentBranches().ToFloat() * IndentSize;
	Float absY = (scrollIdx - topScrollIdx).ToFloat() * BranchHeight;

	component->SetPosition(Vector2(absX, absY));
	component->SetSize(Vector2(ReadSize().X - component->ReadPosition().X, BranchHeight));

	return true;
}

void TreeListComponent::DestroyTreeComponents ()
{
	NumPendingDeleteComponents += ExpandButtons.size() + Connections.size() + TreeLabels.size();

	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		ExpandButtons.at(i).Button->Destroy();
	}
	ExpandButtons.clear();
	bIgnoreHoverMovement = false; //buttons cleared, we can ignore their hover states

	for (UINT_TYPE i = 0; i < Connections.size(); i++)
	{
		Connections.at(i)->Destroy();
	}
	ContainerUtils::Empty(Connections);

	for (UINT_TYPE i = 0; i < TreeLabels.size(); i++)
	{
		TreeLabels.at(i)->Destroy();
	}
	ContainerUtils::Empty(TreeLabels);
}

void TreeListComponent::RefreshTree ()
{
	//Destroy old buttons, labels, and connectors
	DestroyTreeComponents();
	if (TreeList.size() <= 0)
	{
		return;
	}

	Int topBranchScrollIdx = (ListScrollbar.IsValid()) ? ListScrollbar->GetScrollPosition() : 0;
	Int curScrollIdx = -1;
	Float expandButtonSize = BranchHeight;
	const Vector2& treeFinalCoordinates = ReadCachedAbsPosition();
	std::vector<SDataBranch*> parentsAboveScroll; //List of expanded branches that are above the current scroll position
	std::vector<SBranchConnectionInfo> orphans; //List of branches that needs to be connected to their parent.
	TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0));

	//Construct the tree
	while (iter.GetSelectedBranch() != nullptr)
	{
		curScrollIdx++;
		SDataBranch* curBranch = iter.GetSelectedBranch();
		Vector2 curBranchPos = Vector2(curBranch->GetNumParentBranches().ToFloat() * IndentSize, (curScrollIdx - topBranchScrollIdx).ToFloat() * BranchHeight);
		curBranchPos += treeFinalCoordinates;

		if (curScrollIdx < topBranchScrollIdx)
		{
			//Keep track of this branch in case we need to draw a connection to it
			if (curBranch->bExpanded && curBranch->Children.size() > 0)
			{
				//Handle case where the expand button is above the scroll position but the child is below the scroll position
				if (curScrollIdx + curBranch->CountNumChildren(false) >= ListScrollbar->GetScrollPosition() + ListScrollbar->GetNumVisibleScrollPositions())
				{
					//Draw a straight line from cur branch to bottom of transform
					Vector2 parentPos = Vector2(curBranchPos.X, treeFinalCoordinates.Y);
					parentPos.X += (expandButtonSize * 0.5f); //center horizontally relative to the button beyond scroll position.

					Vector2 childPos = parentPos;
					childPos.Y = ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y;

					CreateConnections(parentPos, childPos);
				}

				//At least one child may be visible within scroll position.  Store a reference to this branch since one of the children may need to link to this.
				parentsAboveScroll.push_back(curBranch);
			}

			//This branch is not visible since the scrollbar is not at the top.  Skip this branch.
			iter.SelectNextVisibleBranch();
			continue;
		}

		//Check if we need to draw a connection from cur branch to a branch above current scoll position
		for (UINT_TYPE i = 0; i < parentsAboveScroll.size(); i++)
		{
			if (parentsAboveScroll.at(i) == curBranch->ParentBranch)
			{
				Vector2 parentPos = Vector2(curBranchPos.X, treeFinalCoordinates.Y);
				parentPos.X += (expandButtonSize * 0.5f); //center horizontally relative to the button beyond scroll position.
				parentPos.X -= IndentSize; //The parent branch would be one unit to the left.

				Vector2 childPos = curBranchPos;
				childPos.X += (expandButtonSize * 0.75f); //Stretch through the button to remove the gap between horizontal bar and label for branches without expand buttons.
				childPos.Y += (BranchHeight * 0.5f); //Vertical center align

				CreateConnections(parentPos, childPos);

				break;
			}
		}

		//Create the expand/collapse button
		if (curBranch->Children.size() > 0)
		{
			ButtonComponent* expandButton = ButtonComponent::CreateObject();
			if (AddComponent(expandButton))
			{
				if (FrameComponent* background = expandButton->GetBackground())
				{
					background->SetCenterTexture(curBranch->bExpanded ? CollapseButtonTexture.Get() : ExpandButtonTexture.Get());
				}

				expandButton->SetPosition(curBranchPos - treeFinalCoordinates);
				expandButton->SetSize(Vector2(expandButtonSize, expandButtonSize));

				if (expandButton->GetCaptionComponent() != nullptr)
				{
					expandButton->GetCaptionComponent()->Destroy();
				}

				expandButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				expandButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TreeListComponent, HandleExpandButtonClicked, void, ButtonComponent*));
				ExpandButtons.push_back(SButtonMapping(expandButton, curBranch));

				HoverComponent* expandHover = HoverComponent::CreateObject();
				if (expandButton->AddComponent(expandHover))
				{
					expandHover->OnRollOver = SDFUNCTION_2PARAM(this, TreeListComponent, HandleExpandButtonRolledOver, void, MousePointer*, Entity*);
					expandHover->OnRollOut = SDFUNCTION_2PARAM(this, TreeListComponent, HandleExpandButtonRolledOut, void, MousePointer*, Entity*);
				}

				//Draw connection between current branch and child
				if (curBranch->bExpanded)
				{
					Vector2 connectionPoint = expandButton->GetCachedAbsPosition();
					connectionPoint += Vector2(expandButton->ReadCachedAbsSize().X * 0.5f, expandButton->ReadCachedAbsSize().Y);
					for (UINT_TYPE i = 0; i < curBranch->Children.size(); i++)
					{
						orphans.push_back(SBranchConnectionInfo(curBranch, curBranch->Children.at(i), connectionPoint));
					}
				}
			}
		}

		LabelComponent* branchLabel = LabelComponent::CreateObject();
		if (AddComponent(branchLabel))
		{
			branchLabel->SetWrapText(false);
			Vector2 labelPos = curBranchPos;
			labelPos.X += expandButtonSize;
			branchLabel->SetPosition(labelPos - treeFinalCoordinates);
			branchLabel->SetSize(Vector2(ReadCachedAbsSize().X - branchLabel->ReadCachedAbsPosition().X, expandButtonSize));
			branchLabel->SetCharacterSize(expandButtonSize.ToInt());
			branchLabel->SetText(curBranch->Data->GetLabelText());
			TreeLabels.push_back(branchLabel);
		}

		//Check if curBranch is an orphan
		for (UINT_TYPE i = 0; i < orphans.size(); i++)
		{
			if (orphans.at(i).ChildBranch != curBranch)
			{
				continue;
			}

			//Create a connection between child and parent
			Vector2 parentPos = orphans.at(i).ParentConnectPosition;
			Vector2 childPos = curBranchPos;
			childPos.X += (expandButtonSize * 0.75f); //Stretch through the button to remove the gap between horizontal bar and label for branches without expand buttons.
			childPos.Y += (BranchHeight * 0.5f); //Vertical center align
			CreateConnections(parentPos, childPos);

			//Connection successful, remove orphan reference
			orphans.erase(orphans.begin() + i);
			break;
		}

		if (ListScrollbar.IsValid() && !ListScrollbar->IsIndexVisible(curScrollIdx + 1))
		{
			//Any orphans remaining implies that they're beyond the current visible scroll position.  Draw connections to the bottom from each parent.
			for (UINT_TYPE i = 0; i < orphans.size(); i++)
			{
				Vector2 childPos;
				childPos.X = orphans.at(i).ParentConnectPosition.X;
				childPos.Y = ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y; //Child position will be straight down from parent
				CreateConnections(orphans.at(i).ParentConnectPosition, childPos);
			}
			orphans.clear();

			//Reached at bottom of scrollbar... stop constructing new branches
			break;
		}

		iter.SelectNextVisibleBranch();
	}

	UpdateSelectedBar();

	//Reset hover transform's height to a single line in case it was hovering over a button.
	if (HoverBarOwner.IsValid())
	{
		HoverBarOwner->SetSize(Vector2(HoverBarOwner->ReadSize().X, BranchHeight));
	}

	//Check if we need to run garbage collection
	if (NumPendingDeleteComponents > 0 && NumPendingDeleteComponents > MaxNumPendingDeleteComponents)
	{
		NumPendingDeleteComponents = 0; //Set variable early to protect against inf recursion
		Engine::FindEngine()->CollectGarbage();
	}
}

void TreeListComponent::CreateConnections (const Vector2& parentPt, const Vector2 childPt)
{
	Float highestPt = Utils::Min(parentPt.Y, childPt.Y);
	Float lowestPt = Utils::Max(parentPt.Y, childPt.Y);
	Float leftestPt = Utils::Min(parentPt.X, childPt.X);
	Float rightestPt = Utils::Max(parentPt.X, childPt.X);

	//Center align with connection thickness into consideration
	lowestPt += (ConnectionThickness * 0.5f);

	//Vertical line
	GuiComponent* vertOwner = GuiComponent::CreateObject();
	if (!AddComponent(vertOwner))
	{
		return;
	}

	ColorRenderComponent* verticalConnection = ColorRenderComponent::CreateObject();
	if (!vertOwner->AddComponent(verticalConnection))
	{
		vertOwner->Destroy();
		return;
	}

	verticalConnection->SolidColor = ConnectionColor;

	vertOwner->SetPosition(Vector2(leftestPt - (ConnectionThickness * 0.5f), highestPt)); //Align horizontally over source
	vertOwner->SetSize(Vector2(ConnectionThickness, lowestPt - highestPt));
	Connections.push_back(vertOwner);

	//Horizontal line
	if (childPt.X - parentPt.X < ConnectionThickness)
	{
		return; //Don't bother drawing a horizontal line since the two positions are directly vertical from each other
	}

	GuiComponent* colorOwner = GuiComponent::CreateObject();
	if (!AddComponent(colorOwner))
	{
		return;
	}

	ColorRenderComponent* horConnection = ColorRenderComponent::CreateObject();
	if (!colorOwner->AddComponent(horConnection))
	{
		colorOwner->Destroy();
		return;
	}

	horConnection->SolidColor = ConnectionColor;
	colorOwner->SetPosition(Vector2(leftestPt, lowestPt - ConnectionThickness));
	colorOwner->SetSize(Vector2(rightestPt - leftestPt, ConnectionThickness));

	Connections.push_back(colorOwner);
}

void TreeListComponent::HandleExpandButtonClicked (ButtonComponent* uiComponent)
{
	//Identify which button was clicked - then toggle bExpanded based on the button's associated branch.
	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		if (ExpandButtons.at(i).Button == uiComponent)
		{
			SDataBranch* branch = ExpandButtons.at(i).Branch;
			(branch->bExpanded) ? CollapseBranch(branch) : ExpandBranch(branch);
			break;
		}
	}
}

void TreeListComponent::HandleExpandButtonRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	ButtonComponent* expandButton = dynamic_cast<ButtonComponent*>(hoverOwner);
	if (expandButton == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Expand button rolled over callback for %s is not an expand button.  Instead it's a %s"), ToString(), hoverOwner->ToString());
		return;
	}

	//Find the databranch this button is associated with
	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		if (expandButton == ExpandButtons.at(i).Button)
		{
			CalcHoverTransform(i);
			break;
		}
	}
}

void TreeListComponent::HandleExpandButtonRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	CHECK(mouse != nullptr)
	const Vector2& mousePos = mouse->ReadPosition();

	//Identify if user is hovering over a different expand button
	for (UINT_TYPE i = 0; i < ExpandButtons.size(); i++)
	{
		if (ExpandButtons.at(i).Button->IsWithinBounds(Vector2(mousePos.X, mousePos.Y)))
		{
			CalcHoverTransform(i);
			return;
		}
	}

	//Not hovering over any button, remove children hover state.
	CalcHoverTransform(UINT_INDEX_NONE);
}

void TreeListComponent::HandleScrollPosChanged (Int newScrollPosition)
{
	RefreshTree();
}

void TreeListComponent::HandleGarbageCollection ()
{
	NumPendingDeleteComponents = 0;
}
SD_END