/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiEntity.cpp
=====================================================================
*/

#include "FocusComponent.h"
#include "FocusInterface.h"
#include "GuiDrawLayer.h"
#include "GuiEngineComponent.h"
#include "GuiEntity.h"
#include "GuiTheme.h"
#include "ScrollbarComponent.h"

IMPLEMENT_CLASS(SD::GuiEntity, SD::Entity)
SD_BEGIN

const Float GuiEntity::DEFAULT_AUTO_SIZE_CHECK_FREQUENCY = 0.67f;

void GuiEntity::InitProps ()
{
	Super::InitProps();

	Focus = nullptr;
	Input = nullptr;
	AutoSizeHorizontal = false;
	AutoSizeVertical = false;
	GuiSizeToOwningScrollbar = Vector2(-1.f, -1.f);
}

void GuiEntity::BeginObject ()
{
	Super::BeginObject();

	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	if (localTheme != nullptr && localTheme->GetDefaultStyle() != nullptr)
	{
		SetStyleName(localTheme->GetDefaultStyle()->Name);
	}

	Tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiEntity, HandleAutoSizeTick, void, Float));
		Tick->SetTickInterval(DEFAULT_AUTO_SIZE_CHECK_FREQUENCY);
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}

	ConstructUI();
	InitializeFocusComponent();
}

void GuiEntity::InitializeScrollableInterface (ScrollbarComponent* scrollbarOwner)
{
	ScrollableInterface::InitializeScrollableInterface(scrollbarOwner);

	OwningScrollbar = scrollbarOwner;

	if (OwningScrollbar.IsValid())
	{
		if (scrollbarOwner->PropagateStyle && !scrollbarOwner->ReadStyleName().IsEmpty())
		{
			StyleName = scrollbarOwner->ReadStyleName();
		}

		if (PlanarTransform* spriteTransform = OwningScrollbar->GetFrameSpriteTransform())
		{
			UpdateSizeBasedOnScrollbar(spriteTransform->ReadCachedAbsSize());
		}
	}
}

ScrollbarComponent* GuiEntity::GetScrollbarOwner () const
{
	return OwningScrollbar.Get();
}

void GuiEntity::RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner)
{
	std::vector<RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	//Find the GuiEntity draw layer
	for (UINT_TYPE i = 0; i < drawLayers.size(); ++i)
	{
		GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(drawLayers.at(i).Layer);
		if (guiLayer != nullptr)
		{
			guiLayer->RegisterMenu(this);
			return;
		}
	}

	//GuiLayer is not found.  Add one to the texture.
	GuiDrawLayer* newLayer = GuiDrawLayer::CreateObject();
	drawLayerOwner->RegisterDrawLayer(newLayer, scrollbar->GetFrameCamera());
}

void GuiEntity::UnregisterFromDrawLayer (RenderTexture* drawLayerOwner)
{
	std::vector<RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	for (UINT_TYPE i = 0; i < drawLayers.size(); ++i)
	{
		GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(drawLayers.at(i).Layer);
		if (guiLayer != nullptr)
		{
			guiLayer->UnregisterMenu(this);
			return;
		}
	}
}

void GuiEntity::GetTotalSize (Vector2& outSize) const
{
	outSize = GetCachedAbsSize();
}

void GuiEntity::HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize)
{
	UpdateSizeBasedOnScrollbar(newFrameSize);
}

void GuiEntity::ExecuteScrollableInterfaceInput (const sf::Event& evnt)
{
	HandlePassiveInput(evnt);
}

void GuiEntity::ExecuteScrollableInterfaceText (const sf::Event& evnt)
{
	HandlePassiveText(evnt);
}

void GuiEntity::ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	HandleMouseMove(mouse, sfmlEvent, deltaMove);
}

void GuiEntity::ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	HandlePassiveMouseClick(mouse, sfmlEvent, eventType);
}

void GuiEntity::ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	HandlePassiveMouseWheel(mouse, sfmlEvent);
}

bool GuiEntity::ExecuteScrollableInterfaceConsumableInput (const sf::Event& evnt)
{
	return HandleConsumableInput(evnt);
}

bool GuiEntity::ExecuteScrollableInterfaceConsumableText (const sf::Event& evnt)
{
	return HandleConsumableText(evnt);
}

bool GuiEntity::ExecuteScrollableInterfaceConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	return HandleConsumableMouseClick(mouse, sfmlEvent, eventType);
}

bool GuiEntity::ExecuteScrollableInterfaceConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	return HandleConsumableMouseWheel(mouse, sfmlEvent);
}

void GuiEntity::ClearScrollbarOwner (ScrollbarComponent* oldScrollbarOwner)
{
	OwningScrollbar = nullptr;

	ScrollableInterface::ClearScrollbarOwner(oldScrollbarOwner);
}

void GuiEntity::RemoveScrollableObject ()
{
	Destroy();
}

bool GuiEntity::AddComponent_Implementation (EntityComponent* newComponent)
{
	bool success = Super::AddComponent_Implementation(newComponent);
	if (success)
	{
		if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(newComponent))
		{
			guiComp->ApplyStyle(StyleName);
		}
	}

	return success;
}

void GuiEntity::RegisterToMainWindow (bool registerToMainRender, bool registerToMainInput, Int inputPriority)
{
	if (registerToMainRender)
	{
		GuiEngineComponent* localGuiEngine = GuiEngineComponent::Find();
		CHECK(localGuiEngine != nullptr)

		GuiDrawLayer* mainOverlay = localGuiEngine->GetMainOverlay();
		if (mainOverlay != nullptr)
		{
			mainOverlay->RegisterMenu(this);
		}
		else
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to register %s to the Main Overlay since the local GUI Engine Component does not have a GUI draw layer."), ToString());
		}
	}

	if (registerToMainInput)
	{
		InputEngineComponent* localInput = InputEngineComponent::Find();
		CHECK(localInput != nullptr)

		InputBroadcaster* mainBroadcaster = localInput->GetMainBroadcaster();
		if (mainBroadcaster != nullptr)
		{
			SetupInputComponent(mainBroadcaster, inputPriority);
		}
		else
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to register %s to the main input broadcaster since the local InputEngineComponent does not have an input broadcaster bound to the main window."), ToString());
		}
	}
}

void GuiEntity::LoseFocus ()
{
	if (Focus.IsValid())
	{
		Focus->ClearSelection();
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(false);
		}
	}
}

void GuiEntity::GainFocus ()
{
	if (Focus.IsValid())
	{
		Focus->SelectNextEntity();
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(true);
		}
	}
}

void GuiEntity::SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority)
{
	if (Input.IsValid())
	{
		Input->SetInputPriority(inputPriority);
		return;
	}

	if (broadcaster == nullptr)
	{
		return;
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		broadcaster->AddInputComponent(Input.Get(), inputPriority);
		Input->OnPassiveInput = SDFUNCTION_1PARAM(this, GuiEntity, HandlePassiveInput, void, const sf::Event&);
		Input->OnPassiveText = SDFUNCTION_1PARAM(this, GuiEntity, HandlePassiveText, void, const sf::Event&);
		Input->OnPassiveMouseClick = SDFUNCTION_3PARAM(this, GuiEntity, HandlePassiveMouseClick, void, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		Input->OnPassiveMouseWheel = SDFUNCTION_2PARAM(this, GuiEntity, HandlePassiveMouseWheel, void, MousePointer*, const sf::Event::MouseWheelScrollEvent&);

		Input->OnMouseMove = SDFUNCTION_3PARAM(this, GuiEntity, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);

		Input->OnInput = SDFUNCTION_1PARAM(this, GuiEntity, HandleConsumableInput, bool, const sf::Event&);
		Input->OnText = SDFUNCTION_1PARAM(this, GuiEntity, HandleConsumableText, bool, const sf::Event&);
		Input->OnMouseClick = SDFUNCTION_3PARAM(this, GuiEntity, HandleConsumableMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		Input->OnMouseWheel = SDFUNCTION_2PARAM(this, GuiEntity, HandleConsumableMouseWheel, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
	}
}

void GuiEntity::RefreshEntitySize ()
{
	if (!AutoSizeHorizontal && !AutoSizeVertical)
	{
		//Auto sizing is disabled
		return;
	}

	Vector2 newSize(1.f, 1.f);

	for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Ignore invisible components
		if (!iter.GetSelectedComponent()->IsVisible())
		{
			continue;
		}

		PlanarTransform* transform = dynamic_cast<PlanarTransform*>(iter.GetSelectedComponent());
		if (transform != nullptr)
		{
			//Ignore transforms that scale relative to its owner (since their owner size is scaled based on its components).
			if (!transform->GetEnableFractionScaling() || transform->ReadSize().X > 1.f)
			{
				newSize.X = Utils::Max(newSize.X, transform->ReadPosition().X + transform->ReadSize().X);
			}

			if (!transform->GetEnableFractionScaling() || transform->ReadSize().Y > 1.f)
			{
				newSize.Y = Utils::Max(newSize.Y, transform->ReadPosition().Y + transform->ReadSize().Y);
			}
		}
	}

	//Reset certain axis of the new size if their corresponding AutoSizing flag is disabled.
	if (!AutoSizeHorizontal)
	{
		newSize.X = ReadSize().X;
	}

	if (!AutoSizeVertical)
	{
		newSize.Y = ReadSize().Y;
	}

	SetSize(newSize);

	if (Tick.IsValid())
	{
		Tick->ResetAccumulatedTickTime();
	}
}

void GuiEntity::SetDrawLayer (GuiDrawLayer* newDrawLayer)
{
	DrawLayer = newDrawLayer;
}

void GuiEntity::SetAutoSizeHorizontal (bool newAutoSizeHorizontal)
{
	AutoSizeHorizontal = newAutoSizeHorizontal;
	if (Tick.IsValid())
	{
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}
}

void GuiEntity::SetAutoSizeVertical (bool newAutoSizeVertical)
{
	AutoSizeVertical = newAutoSizeVertical;
	if (Tick.IsValid())
	{
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}
}

void GuiEntity::SetAutoSizeCheckFrequency (Float checksPerSec)
{
	if (Tick.IsValid())
	{
		Tick->SetTickInterval(checksPerSec);
	}
}

void GuiEntity::SetGuiSizeToOwningScrollbar (const Vector2& newGuiSizeToOwningScrollbar)
{
	GuiSizeToOwningScrollbar = newGuiSizeToOwningScrollbar;

	if (OwningScrollbar.IsValid() && OwningScrollbar->GetFrameSpriteTransform() != nullptr)
	{
		UpdateSizeBasedOnScrollbar(OwningScrollbar->GetFrameSpriteTransform()->ReadCachedAbsSize());
	}
}

void GuiEntity::SetStyleName (const DString& newStyleName)
{
	StyleName = newStyleName;
}

FocusComponent* GuiEntity::GetFocus () const
{
	return Focus.Get();
}

FocusInterface* GuiEntity::GetFocusedObj () const
{
	return (Focus.IsValid()) ? Focus->GetSelectedEntity() : nullptr;
}

void GuiEntity::ConstructUI ()
{
	//Noop
}

void GuiEntity::InitializeFocusComponent ()
{
	Focus = FocusComponent::CreateObject();
	if (AddComponent(Focus))
	{
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(false); //Wait until the GuiEntity is focused before enabling.
		}
	}
}

void GuiEntity::UpdateSizeBasedOnScrollbar (const Vector2& frameSize)
{
	Vector2 newSize = GetSize();
	bool sizeChanged = false;

	if (GuiSizeToOwningScrollbar.X > 0.f)
	{
		newSize.X = (frameSize.X * GuiSizeToOwningScrollbar.X);
		sizeChanged = true;
	}

	if (GuiSizeToOwningScrollbar.Y > 0.f)
	{
		newSize.Y = (frameSize.Y * GuiSizeToOwningScrollbar.Y);
		sizeChanged = true;
	}

	if (sizeChanged)
	{
		SetSize(newSize);
	}
}

void GuiEntity::InvokeOnEachComponent (const std::function<bool(GuiComponent*)>& lambda)
{
	CHECK(lambda != nullptr)

	if (ContainerUtils::IsEmpty(ReadComponents()))
	{
		return;
	}

	size_t i = ReadComponents().size() - 1;
	while (true)
	{
		if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(ReadComponents().at(i)))
		{
			if (lambda(guiComp))
			{
				break;
			}
		}

		if (i == 0)
		{
			break;
		}

		--i;
	}
}

void GuiEntity::HandlePassiveInput (const sf::Event& evnt)
{
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsInputEvent(GuiComponent::IE_Input, evnt))
		{
			guiComp->BroadcastNonconsumableInput(evnt);
		}

		return false;
	});
}

void GuiEntity::HandlePassiveText (const sf::Event& evnt)
{
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsInputEvent(GuiComponent::IE_Text, evnt))
		{
			guiComp->BroadcastNonconsumableText(evnt);
		}

		return false;
	});
}

void GuiEntity::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsMouseEvents(GuiComponent::IE_MouseMove, sfmlEvent.x, sfmlEvent.y))
		{
			guiComp->ProcessMouseMove(mouse, sfmlEvent, deltaMove);
		}

		return false;
	});
}

void GuiEntity::HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsMouseEvents(GuiComponent::IE_MouseEvent, sfmlEvent.x, sfmlEvent.y))
		{
			guiComp->BroadcastNonconsumableMouseClick(mouse, sfmlEvent, eventType);
		}

		return false;
	});
}

void GuiEntity::HandlePassiveMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsMouseEvents(GuiComponent::IE_MouseWheel, sfmlEvent.x, sfmlEvent.y))
		{
			guiComp->BroadcastNonconsumableMouseWheelMove(mouse, sfmlEvent);
		}

		return false;
	});
}

bool GuiEntity::HandleConsumableInput (const sf::Event& evnt)
{
	bool bConsumeEvent = false;
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsInputEvent(GuiComponent::IE_Input, evnt) && guiComp->BroadcastConsumableInput(evnt))
		{
			bConsumeEvent = true;
			return true;
		}

		return false;
	});

	return bConsumeEvent;
}

bool GuiEntity::HandleConsumableText (const sf::Event& evnt)
{
	bool bConsumeEvent = false;
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsInputEvent(GuiComponent::IE_Text, evnt) && guiComp->BroadcastConsumableText(evnt))
		{
			bConsumeEvent = true;
			return true;
		}

		return false;
	});

	return bConsumeEvent;
}

bool GuiEntity::HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool bConsumeEvent = false;
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsMouseEvents(GuiComponent::IE_MouseEvent, sfmlEvent.x, sfmlEvent.y) && guiComp->BroadcastConsumableMouseClick(mouse, sfmlEvent, eventType))
		{
			bConsumeEvent = true;
			return true;
		}

		return false;
	});

	return bConsumeEvent;
}

bool GuiEntity::HandleConsumableMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	bool bConsumeEvent = false;
	InvokeOnEachComponent([&](GuiComponent* guiComp)
	{
		if (guiComp->AcceptsMouseEvents(GuiComponent::IE_MouseWheel, sfmlEvent.x, sfmlEvent.y) && guiComp->BroadcastConsumableMouseWheelMove(mouse, sfmlEvent))
		{
			bConsumeEvent = true;
			return true;
		}

		return false;
	});

	return bConsumeEvent;
}

void GuiEntity::HandleAutoSizeTick (Float deltaSec)
{
	RefreshEntitySize();
}
SD_END