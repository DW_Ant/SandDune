/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiDrawLayer.cpp
=====================================================================
*/

#include "ContextMenuContainer.h"
#include "DropdownGuiEntity.h"
#include "GuiEntity.h"
#include "GuiDrawLayer.h"
#include "GuiTheme.h"
#include "TooltipGuiEntity.h"

IMPLEMENT_CLASS(SD::GuiDrawLayer, SD::PlanarDrawLayer)
SD_BEGIN

const Int GuiDrawLayer::MAIN_OVERLAY_PRIORITY = 500;
const Int GuiDrawLayer::FOREGROUND_PRIORITY = 600;

GuiDrawLayer::SCommonGuiClasses::SCommonGuiClasses (bool bUseTheme) :
	TooltipClass(TooltipGuiEntity::SStaticClass()),
	DropdownEntityClass(DropdownGuiEntity::SStaticClass()),
	ContextContainerClass(ContextMenuContainer::SStaticClass())
{
	if (bUseTheme)
	{
		if (GuiTheme* localTheme = GuiTheme::GetGuiTheme())
		{
			TooltipClass = localTheme->TooltipClass;
		}
	}
}

void GuiDrawLayer::InitProps ()
{
	Super::InitProps();

	bAccessingMenus = false;
}

void GuiDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	bAccessingMenus = true;

	PrepareRegisteredMenus();

	for (size_t i = 0; i < RegisteredMenus.size(); ++i)
	{
		RegisteredMenus.at(i)->MarkProjectionDataDirty();
		if (RegisteredMenus.at(i)->IsVisible() && RegisteredMenus.at(i)->IsWithinView(renderTarget, cam))
		{
			RenderGuiEntity(RegisteredMenus.at(i), renderTarget, cam);
		}
	}

	//Render loose components (such as the mouse) over the UI menus.
	RenderComponents(renderTarget, cam);

	bAccessingMenus = false;
	for (size_t i = 0; i < PendingMenuRemoveList.size(); ++i)
	{
		UnregisterMenu(PendingMenuRemoveList.at(i));
	}
	ContainerUtils::Empty(OUT PendingMenuRemoveList);
}

void GuiDrawLayer::HandleGarbageCollection ()
{
	Super::HandleGarbageCollection();

	PurgeExpiredMenus();
}

void GuiDrawLayer::RegisterMenu (GuiEntity* newMenu)
{
#if ENABLE_COMPLEX_CHECKING
	//Make sure the menu is not already registered.
	CHECK(ContainerUtils::FindInVector(RegisteredMenus, newMenu) == INT_INDEX_NONE)
#endif

	newMenu->SetDrawLayer(this);
	RegisteredMenus.push_back(newMenu);
}

void GuiDrawLayer::UnregisterMenu (GuiEntity* target)
{
	if (bAccessingMenus)
	{
		PendingMenuRemoveList.push_back(target);
		return;
	}

	UINT_TYPE menuIdx = ContainerUtils::FindInVector(RegisteredMenus, target);
	if (menuIdx != UINT_INDEX_NONE)
	{
		RegisteredMenus.at(menuIdx)->SetDrawLayer(nullptr);
		RegisteredMenus.erase(RegisteredMenus.begin() + menuIdx);
	}
}

void GuiDrawLayer::InitCommonEntities (const SCommonGuiClasses& inCommonClasses, InputBroadcaster* input)
{
	//Process TooltipEntity
	if (inCommonClasses.TooltipClass != nullptr)
	{
		//Replace old TooltipEntity
		for (GuiEntity* menu : RegisteredMenus)
		{
			if (dynamic_cast<TooltipGuiEntity*>(menu) != nullptr)
			{
				menu->Destroy();
				break;
			}
		}

		const TooltipGuiEntity* tooltipCdo = dynamic_cast<const TooltipGuiEntity*>(inCommonClasses.TooltipClass->GetDefaultObject());
		if (tooltipCdo == nullptr)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize common GuiEntities since %s is not a TooltipGuiEntity."), inCommonClasses.TooltipClass->ToString());
			return;
		}

		TooltipGuiEntity* newEntity = tooltipCdo->CreateObjectOfMatchingClass();
		if (newEntity != nullptr)
		{
			newEntity->SetDepth(-10.f); //Tooltips are rendered in front of most things
			RegisterMenu(newEntity);

			if (input != nullptr)
			{
				//Setup input only with high priority. TooltipEntities generally don't consume input. They only listen to input to know when it should hide its tooltips.
				newEntity->SetupInputComponent(input, 1000000);
			}
		}
	}

	//Process DropdownGuiEntity
	if (inCommonClasses.DropdownEntityClass != nullptr)
	{
		//Replace old dropdown entity
		for (GuiEntity* menu : RegisteredMenus)
		{
			if (dynamic_cast<DropdownGuiEntity*>(menu) != nullptr)
			{
				menu->Destroy();
				break;
			}
		}

		const DropdownGuiEntity* dropdownCdo = dynamic_cast<const DropdownGuiEntity*>(inCommonClasses.DropdownEntityClass->GetDefaultObject());
		if (dropdownCdo == nullptr)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize common GuiEntities since %s is not a DropdownGuiEntity."), inCommonClasses.DropdownEntityClass->ToString());
			return;
		}

		DropdownGuiEntity* newEntity = dropdownCdo->CreateObjectOfMatchingClass();
		if (newEntity != nullptr)
		{
			newEntity->SetDepth(-5.f); //Render behind tooltips.
			RegisterMenu(newEntity);

			if (input != nullptr)
			{
				newEntity->SetupInputComponent(input, 1000);
			}
		}
	}

	//Process ContextMenuContainer
	if (inCommonClasses.ContextContainerClass != nullptr)
	{
		//Replace old dropdown entity
		for (GuiEntity* menu : RegisteredMenus)
		{
			if (dynamic_cast<ContextMenuContainer*>(menu) != nullptr)
			{
				menu->Destroy();
				break;
			}
		}

		const ContextMenuContainer* contextCdo = dynamic_cast<const ContextMenuContainer*>(inCommonClasses.ContextContainerClass->GetDefaultObject());
		if (contextCdo == nullptr)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize common GuiEntities since %s is not a ContextMenuContainer."), inCommonClasses.ContextContainerClass->ToString());
			return;
		}

		ContextMenuContainer* newEntity = contextCdo->CreateObjectOfMatchingClass();
		if (newEntity != nullptr)
		{
			newEntity->SetDepth(-8.f); //Render behind tooltips and in front of dropdown components
			RegisterMenu(newEntity);

			if (input != nullptr)
			{
				newEntity->SetupInputComponent(input, 1100);
			}
		}
	}
}

void GuiDrawLayer::PrepareRegisteredMenus ()
{
	PurgeExpiredMenus();

	std::sort(RegisteredMenus.begin(), RegisteredMenus.end(), [](GuiEntity* a, GuiEntity* b)
	{
		return (a->GetDepth() > b->GetDepth());
	});
}

void GuiDrawLayer::PurgeExpiredMenus ()
{
	//Remove empty/destroyed menus from list
	size_t i = 0;
	while (i < RegisteredMenus.size())
	{
		if (!VALID_OBJECT(RegisteredMenus.at(i)))
		{
			RegisteredMenus.erase(RegisteredMenus.begin() + i);
			continue;
		}

		++i;
	}
}

void GuiDrawLayer::RenderGuiEntity (const GuiEntity* menu, RenderTarget* renderTarget, const Camera* camera) const
{
	for (ComponentIterator iter(menu, true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		EntityComponent* curComponent = iter.GetSelectedComponent();
		if (PlanarTransform* planeComp = dynamic_cast<PlanarTransform*>(curComponent))
		{
			planeComp->MarkProjectionDataDirty();
			if (!planeComp->IsWithinView(renderTarget, camera))
			{
				//Skip to next sibling component
				iter.SkipSubComponents();
				continue;
			}
		}

		//Check if the component is visible (must be executed after ComputeAbsTransform just in case external classes depends on that transform being updated.
		if (!curComponent->IsVisible())
		{
			//Skip to next sibling component
			iter.SkipSubComponents();
			continue;
		}

		//Even though RenderComponents are not planar transforms (since they are agnostic to Planar and Scene transforms),
		//Their owners are required to implement a transform coordinate system, and that transform was checked in an earlier iteration.
		if (RenderComponent* renderComp = dynamic_cast<RenderComponent*>(curComponent))
		{
			renderComp->Render(renderTarget, camera);
		}
	}
}
SD_END