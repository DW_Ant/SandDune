/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ListBoxComponent.cpp
=====================================================================
*/

#include "FrameComponent.h"
#include "LabelComponent.h"
#include "ListBoxComponent.h"
#include "ScrollableInterface.h"

IMPLEMENT_CLASS(SD::ListBoxComponent, SD::GuiComponent)
SD_BEGIN

void ListBoxComponent::InitProps ()
{
	Super::InitProps();

	MaxNumSelected = 0;
	MinNumSelected = 0;
	bAutoDeselect = false;
	bAutoSizeVertically = true;
	bReadOnly = false;
	BrowseIndex = INT_INDEX_NONE;
	SelectionColor = Color(0x666666);
	HoverColor = Color(0xbdb9b6);
	ReadOnlyColor = Color(25, 25, 5, 150);

	Background = nullptr;
	ItemListText = nullptr;
	HighlightHoverRender = nullptr;
	HighlightOwner = nullptr;
}

void ListBoxComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ListBoxComponent* listTemplate = dynamic_cast<const ListBoxComponent*>(objTemplate);
	if (listTemplate != nullptr)
	{
		SetMaxNumSelected(listTemplate->GetMaxNumSelected());
		SetMinNumSelected(listTemplate->GetMinNumSelected());
		SetAutoDeselect(listTemplate->GetAutoDeselect());
		SetAutoSizeVertically(listTemplate->GetAutoSizeVertically());
		SetReadOnly(listTemplate->IsReadOnly());
		SetSelectionColor(listTemplate->GetSelectionColor());
		SetHoverColor(listTemplate->GetHoverColor());
		SetReadOnlyColor(listTemplate->GetReadOnlyColor());

		bool bCreatedObj;
		Background = ReplaceTargetWithObjOfMatchingClass(Background.Get(), listTemplate->GetBackground(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Background);
		}

		if (Background.IsValid())
		{
			Background->CopyPropertiesFrom(listTemplate->GetBackground());
		}

		ItemListText = ReplaceTargetWithObjOfMatchingClass(ItemListText.Get(), listTemplate->GetItemListText(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ItemListText);
		}

		if (ItemListText.IsValid())
		{
			ItemListText->CopyPropertiesFrom(listTemplate->GetItemListText());
		}
	}
}

bool ListBoxComponent::CanBeFocused () const
{
	return (FocusInterface::CanBeFocused() && !bReadOnly && IsVisible());
}

void ListBoxComponent::GainFocus ()
{
	FocusInterface::GainFocus();

	SetBrowseIndex(0);
}

void ListBoxComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	SetBrowseIndex(-1);
}

bool ListBoxComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	CHECK(!bReadOnly) //Shouldn't gain focus if this component is read only

	/**
	 * Controls:
	 * Up/Down arrows moves browse index between 0 - [Size()-1]
	 * Enter toggles selection.
	 */
	if (keyEvent.type != sf::Event::KeyReleased)
	{
		return true;
	}

	switch(keyEvent.key.code)
	{
		case(sf::Keyboard::Up):
			SetBrowseIndex(Utils::Max<Int>(BrowseIndex - 1, 0));
			break;

		case(sf::Keyboard::Down):
			SetBrowseIndex(Utils::Min<Int>(BrowseIndex + 1, DisplayedListIndices.size() - 1));
			break;

		case(sf::Keyboard::Return):
		{
			bool isSelected = IsItemSelectedByIndex(BrowseIndex);
			if (MinNumSelected <= 0 || !isSelected || SelectedItems.size() > MinNumSelected.ToUnsignedInt()) //Ensure we're not deselecting an item that would go below MinNumSelected
			{
				if (ContainerUtils::IsValidIndex(List, BrowseIndex.ToUnsignedInt()) && !List.at(BrowseIndex.ToUnsignedInt())->bReadOnly)
				{
					SetItemSelectionByIdx(BrowseIndex, !isSelected);
				}
			}
			
			break;
		}
	}

	return true;
}

bool ListBoxComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return true;
}

void ListBoxComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (bReadOnly || !IsVisible() || HighlightOwner.IsNullptr())
	{
		return;
	}

	Int lineIdx = GetLineIdxFromCoordinates(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
	if (lineIdx == INT_INDEX_NONE || ContainerUtils::IsEmpty(DisplayedListIndices))
	{
		SetBrowseIndex(INT_INDEX_NONE);
		return;
	}

	Int listIdx = GetListIndex(lineIdx.ToUnsignedInt());
	if (List.at(listIdx.ToUnsignedInt())->bReadOnly)
	{
		SetBrowseIndex(INT_INDEX_NONE);
		return;
	}

	if (BrowseIndex != lineIdx)
	{
		SetBrowseIndex(lineIdx);
	}
}

bool ListBoxComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (bReadOnly || sfmlEvent.button != sf::Mouse::Button::Left || !IsVisible() || !IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		return false;
	}

	if (eventType == sf::Event::MouseButtonReleased && DisplayedListIndices.size() > 0)
	{
		Int displayedIdx = GetLineIdxFromCoordinates(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
		if (displayedIdx == INT_INDEX_NONE)
		{
			return true;
		}

		Int lineIdx = GetListIndex(displayedIdx.ToUnsignedInt());
		bool isSelected = IsItemSelectedByIndex(lineIdx);
		if (!List.at(lineIdx.ToUnsignedInt())->bReadOnly)
		{
			if (MinNumSelected <= 0 || !isSelected || SelectedItems.size() > MinNumSelected.ToUnsignedInt()) //Ensure we're not deselecting an item that would go below MinNumSelected
			{
				//Toggle item selection
				SetItemSelectionByIdx(lineIdx, !isSelected);
			}

			if (OnItemClicked.IsBounded())
			{
				size_t selectedItemIdx = ContainerUtils::FindInVector(SelectedItems, lineIdx);
				OnItemClicked(selectedItemIdx);
			}
		}
	}

	return true;
}

void ListBoxComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeBackground();
	InitializeItemListText();
	InitializeHighlightBar();
}

void ListBoxComponent::Destroyed ()
{
	ClearList();

	Super::Destroyed();
}

bool ListBoxComponent::IsItemSelectedByIndex (Int index) const
{
	for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
	{
		if (SelectedItems.at(i) == index)
		{
			return true;
		}
	}

	return false;
}

size_t ListBoxComponent::GetDisplayedIndex (Int listIdx) const
{
	for (size_t i = 0; i < DisplayedListIndices.size(); ++i)
	{
		if (DisplayedListIndices.at(i) == listIdx)
		{
			return i;
		}
	}

	return INDEX_NONE;
}

Int ListBoxComponent::GetListIndex (size_t displayIdx) const
{
	CHECK(ContainerUtils::IsValidIndex(DisplayedListIndices, displayIdx))
	return Int(DisplayedListIndices.at(displayIdx));
}

void ListBoxComponent::ClearSelection ()
{
	if (SelectedItems.size() > 0)
	{
		std::vector<Int> selectedIndices = SelectedItems;
		ContainerUtils::Empty(OUT SelectedItems);

		for (SHighlightSelectionInfo& selection : HighlightSelections)
		{
			selection.RenderOwner->Destroy();
		}
		ContainerUtils::Empty(OUT HighlightSelections);

		if (OnItemDeselected.IsBounded())
		{
			for (Int selectedItem : selectedIndices)
			{
				OnItemDeselected(selectedItem);
			}
		}
	}
}

void ListBoxComponent::SelectAll ()
{
	if (bAutoDeselect)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot select all items on a ListBoxComponent (%s) that automatically deselect items upon selection."), ToString());
		return;
	}

	if (MaxNumSelected > 0)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot select all items on a ListBoxComponent (%s) with a max number of selected items constraint."), ToString());
		return;
	}

	if (SelectedItems.size() == DisplayedListIndices.size())
	{
		return; //Everything is already selected
	}

	for (size_t i = 0; i < List.size(); i++)
	{
		//SelectedItems must be a sorted list
		if (i >= SelectedItems.size() || SelectedItems.at(i) != i)
		{
			//Found missing item
			SelectedItems.insert(SelectedItems.begin() + i, Int(i));

			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(Int(i));
			}
		}
	}

	//Sanity check - make sure there aren't any duplicated values
	if (SelectedItems.size() > List.size())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Error detected when selecting all items in ListBoxComponent.  There are more selected items than there are options.  This may cause rendering issues."));
		GuiLog.Log(LogCategory::LL_Warning, TXT("The following values are marked selected more than once..."));
		for (size_t i = 0; i < List.size(); i++)
		{
			UINT_TYPE numItems = ContainerUtils::CountNumItems(SelectedItems, Int(i));
			if (numItems > 1)
			{
				GuiLog.Log(LogCategory::LL_Warning, TXT("    %s"), Int(i));
			}
		}
	}

	//Replace color transforms with a single one that covers everything
	for (size_t i = 1; i < HighlightSelections.size(); ++i)
	{
		HighlightSelections.at(i).RenderOwner->Destroy();
	}

	if (HighlightSelections.size() > 1)
	{
		//Remove extra highlight selections
		HighlightSelections.resize(1);
	}
	else if (ContainerUtils::IsEmpty(HighlightSelections))
	{
		SHighlightSelectionInfo newInfo;
		newInfo.RenderOwner = PlanarTransformComponent::CreateObject();
		if (ItemListText->AddComponent(newInfo.RenderOwner))
		{
			newInfo.RenderComp = ColorRenderComponent::CreateObject();
			if (newInfo.RenderOwner->AddComponent(newInfo.RenderComp))
			{
				newInfo.RenderComp->SolidColor = SelectionColor;
				HighlightSelections.push_back(newInfo);
			}
		}
	}

	CHECK(HighlightSelections.size() == 1)
	if (HighlightSelections.size() > 0)
	{
		HighlightSelections.at(1).RenderOwner->SetPosition(Vector2::ZERO_VECTOR);
		HighlightSelections.at(1).RenderOwner->SetSize(Vector2(1.f, 1.f));
	}
}

void ListBoxComponent::SelectItemsByIdx (const std::vector<Int>& itemIndices)
{
	if (bAutoDeselect)
	{
		ClearSelection();
	}

	for (UINT_TYPE i = 0; i < itemIndices.size(); i++)
	{
		if (MaxNumSelected > 0 && SelectedItems.size() >= MaxNumSelected)
		{
			break;
		}

		if (itemIndices.at(i) < 0 || itemIndices.at(i) >= List.size())
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to select item by index (%s) since that index is beyond the limits of the list.  The List's length is %s"), itemIndices.at(i), Int(List.size()));
			continue;
		}

		UINT_TYPE itemIdx = ContainerUtils::FindInVector(SelectedItems, itemIndices.at(i));
		if (itemIdx == UINT_INDEX_NONE)
		{
			SelectedItems.push_back(itemIndices.at(i));
			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(itemIndices.at(i));
			}
		}
	}

	//Ensure the SelectedItems vector is sorted in ascending order to make it easier to group color components together.
	sort(SelectedItems.begin(), SelectedItems.end());
	RefreshHighlightSelectionComps();
}

void ListBoxComponent::RemoveItemByIdx (Int itemIdx)
{
	if (!ContainerUtils::IsValidIndex(List, itemIdx))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to remove ListBoxComponent's item by index. Index (%s) is not a valid index for a ListBoxComponent of size %s."), itemIdx, Int(List.size()));
		return;
	}

	List.erase(List.begin() + itemIdx.ToUnsignedInt());

	//Update the selected items to avoid displacing them.
	size_t i = 0;
	while (i < SelectedItems.size())
	{
		if (SelectedItems.at(i) == itemIdx)
		{
			//Remove this selected item since the element it points to no longer exists.
			SelectedItems.erase(SelectedItems.begin() + i);
			if (OnItemDeselected.IsBounded())
			{
				OnItemDeselected(itemIdx);
			}

			continue;
		}
		else if (SelectedItems.at(i) > itemIdx)
		{
			SelectedItems.at(i) -= 1;
		}

		++i;
	}

	UpdateTextContent();
	RefreshHighlightSelectionComps();
	RefreshReadOnlyColorComps();
}

void ListBoxComponent::AddItemSelectionByIdx (Int selectedItemIdx)
{
	if (!ContainerUtils::IsValidIndex(List, selectedItemIdx))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid index to select an item in a ListBoxComponent with a list size of %s."), selectedItemIdx, Int(List.size()));
		return;
	}

	//The for loop doesn't check for equal items in the first element. Handle it here.
	if (SelectedItems.size() > 0 && SelectedItems.at(0) == selectedItemIdx)
	{
		//Already selected. Do nothing.
		return;
	}

	bool bInsertedSelection = false;

	//Insert the selected item in ascending order
	for (size_t i = 0; (i+1) < SelectedItems.size(); ++i)
	{
		if (SelectedItems.at(i+1) == selectedItemIdx)
		{
			return; //Item is already selected. Do nothing.
		}
		else if (SelectedItems.at(i+1) > selectedItemIdx)
		{
			SelectedItems.insert(SelectedItems.begin() + i, selectedItemIdx);
			bInsertedSelection = true;
			break;
		}
	}

	if (!bInsertedSelection)
	{
		SelectedItems.push_back(selectedItemIdx);
	}

	RefreshHighlightSelectionComps();
}

void ListBoxComponent::ReplaceItemSelectionByIdx (Int selectedItemIdx)
{
	bool bItemAlreadySelected = false;

	std::vector<Int> deselectedItems;
	for (Int oldSelectedItem : SelectedItems)
	{
		if (oldSelectedItem != selectedItemIdx)
		{
			deselectedItems.push_back(oldSelectedItem);
		}
		else
		{
			bItemAlreadySelected = true;
		}
	}

	ContainerUtils::Empty(OUT SelectedItems);
	SelectedItems.push_back(selectedItemIdx);

	for (SHighlightSelectionInfo& selectionInfo : HighlightSelections)
	{
		selectionInfo.RenderOwner->Destroy();
	}
	ContainerUtils::Empty(OUT HighlightSelections);

	size_t displayedIdx = GetDisplayedIndex(selectedItemIdx);
	if (displayedIdx != INDEX_NONE)
	{
		SHighlightSelectionInfo newInfo;
		newInfo.RenderOwner = PlanarTransformComponent::CreateObject();
		if (ItemListText->AddComponent(newInfo.RenderOwner))
		{
			newInfo.RenderOwner->SetPosition(Vector2(0.f, ItemListText->GetLineHeight() * Int(displayedIdx).ToFloat()));
			newInfo.RenderOwner->SetSize(Vector2(1.f, ItemListText->GetLineHeight()));
			newInfo.RenderComp = ColorRenderComponent::CreateObject();
			if (newInfo.RenderOwner->AddComponent(newInfo.RenderComp))
			{
				newInfo.RenderComp->SolidColor = SelectionColor;
			}
		}
		HighlightSelections.push_back(newInfo);
	}

	if (OnItemDeselected.IsBounded())
	{
		for (Int deselectedIdx : deselectedItems)
		{
			OnItemDeselected(deselectedIdx);
		}
	}

	if (!bItemAlreadySelected && OnItemSelected.IsBounded())
	{
		OnItemSelected(selectedItemIdx);
	}
}

void ListBoxComponent::SetItemSelectionByIdx (Int itemIdx, bool bSelected)
{
	UINT_TYPE selectedItemIdx = ContainerUtils::FindInVector(SelectedItems, itemIdx);
	if (selectedItemIdx != UINT_INDEX_NONE && !bSelected)
	{
		SelectedItems.erase(SelectedItems.begin() + selectedItemIdx);
		RefreshHighlightSelectionComps();
		if (OnItemDeselected.IsBounded())
		{
			OnItemDeselected(itemIdx);
		}
	}
	else if (selectedItemIdx == UINT_INDEX_NONE && bSelected)
	{
		if (bAutoDeselect)
		{
			ClearSelection();
		}

		if (MaxNumSelected <= 0 || SelectedItems.size() < MaxNumSelected)
		{
			//Find where in the vector the SelectedItem should be inserted
			bool bInserted = false;
			for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
			{
				if (SelectedItems.at(i) >= itemIdx)
				{
					SelectedItems.insert(SelectedItems.begin() + i, itemIdx);
					bInserted = true;
					break;
				}
			}

			if (!bInserted)
			{
				SelectedItems.push_back(itemIdx);
			}

			RefreshHighlightSelectionComps();

			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(itemIdx);
			}
		}
	}
}

void ListBoxComponent::UpdateTextContent ()
{
	CHECK(ItemListText.IsValid())

	ContainerUtils::Empty(OUT DisplayedListIndices);
	DString newContent = DString::EmptyString;
	for (size_t i = 0; i < List.size(); ++i)
	{
		DString curLine = List.at(i)->GetLabelText();
		if (!Filter.IsEmpty())
		{
			DString lineCpy(curLine);
			lineCpy.ReplaceInline(TXT(" "), DString::EmptyString, DString::CC_CaseSensitive);
			lineCpy.ToUpper();
			if (lineCpy.Find(Filter, 0, DString::CC_CaseSensitive) == INT_INDEX_NONE)
			{
				continue;
			}
		}

		Int endChar = ItemListText->GetFont()->FindCharNearWidthLimit(curLine, ItemListText->GetCharacterSize(), ItemListText->ReadCachedAbsSize().X);
		if (endChar > 0 && endChar < curLine.Length() - 1)
		{
			curLine = curLine.Left(endChar);
		}

		newContent += curLine + TXT("\n");
		DisplayedListIndices.push_back(i);
	}

	ItemListText->SetText(newContent);
	if (bAutoSizeVertically)
	{
		RefreshVerticalSize();
	}
}

void ListBoxComponent::ClearList ()
{
	ClearSelection();

	//Destroy the allocated GuiDataElement objects
	for (UINT_TYPE i = 0; i < List.size(); i++)
	{
		delete List.at(i);
	}

	ContainerUtils::Empty(List);

	UpdateTextContent();
	RefreshReadOnlyColorComps();
}

void ListBoxComponent::SetBrowseIndex (Int newBrowseIndex)
{
	BrowseIndex = newBrowseIndex;

#if 0
	//This works, but it's annoying to have it auto scroll.
	if (ScrollableInterface* scrollInterface = dynamic_cast<ScrollableInterface*>(GetRootEntity()))
	{
		if (scrollInterface->OnScrollToPosition.IsBounded())
		{
			Float lineHeight = ItemListText->GetLineHeight();
			Vector2 newPos = GetCachedAbsPosition();
			newPos += Vector2(ReadCachedAbsSize().X * 0.5f, (lineHeight * newBrowseIndex.ToFloat()) + (lineHeight * 0.5f));
			scrollInterface->OnScrollToPosition(newPos, false);
		}
	}
#endif

	RenderHighlightHoverOverBrowsedItem();
}

void ListBoxComponent::SetList (const std::vector<BaseGuiDataElement*>& newList)
{
	ClearList(); //Deselect and delete old list.
	List = newList;
	UpdateTextContent();
	RefreshReadOnlyColorComps();
}

void ListBoxComponent::SetMaxNumSelected (Int newMaxNumSelected)
{
	MaxNumSelected = newMaxNumSelected;
	if (MaxNumSelected > 0 && MaxNumSelected < SelectedItems.size())
	{
		ClearSelection();
	}
}

void ListBoxComponent::SetMinNumSelected (Int newMinNumSelected)
{
	MinNumSelected = newMinNumSelected;
}

void ListBoxComponent::SetAutoDeselect (bool bNewAutoDeselect)
{
	if (SelectedItems.size() > 1 && bNewAutoDeselect)
	{
		ClearSelection();
	}

	bAutoDeselect = bNewAutoDeselect;
}

void ListBoxComponent::SetAutoSizeVertically (bool bNewAutoSizeVertically)
{
	if (bAutoSizeVertically == bNewAutoSizeVertically)
	{
		return;
	}

	bAutoSizeVertically = bNewAutoSizeVertically;
	if (bAutoSizeVertically)
	{
		RefreshVerticalSize();
	}
}

void ListBoxComponent::SetReadOnly (bool bNewReadOnly)
{
	bReadOnly = bNewReadOnly;
}

void ListBoxComponent::SetBackground (FrameComponent* newBackground)
{
	if (Background.IsValid())
	{
		Background->Destroy();
	}

	Background = newBackground;
	if (Background.IsValid())
	{
		AddComponent(Background);
	}
}

void ListBoxComponent::SetItemListText (LabelComponent* newItemListText)
{
	if (ItemListText.IsValid())
	{
		ItemListText->Destroy();
	}

	ItemListText = newItemListText;
	if (ItemListText.IsValid())
	{
		AddComponent(ItemListText);
	}

	ClearSelection();
	UpdateTextContent();
}

void ListBoxComponent::SetSelectionColor (Color newSelectionColor)
{
	SelectionColor = newSelectionColor;

	for (size_t i = 0; i < HighlightSelections.size(); ++i)
	{
		HighlightSelections.at(i).RenderComp->SolidColor = SelectionColor;
	}
}

void ListBoxComponent::SetHoverColor (Color newHoverColor)
{
	HoverColor = newHoverColor;

	if (HighlightHoverRender != nullptr)
	{
		HighlightHoverRender->SolidColor = HoverColor;
	}
}

void ListBoxComponent::SetReadOnlyColor (Color newReadOnlyColor)
{
	ReadOnlyColor = newReadOnlyColor;

	for (size_t i = 0; i < ReadOnlySelections.size(); ++i)
	{
		ReadOnlySelections.at(i).RenderComp->SolidColor = ReadOnlyColor;
	}
}

void ListBoxComponent::SetFilter (const DString& newFilter)
{
	DString oldFilter = Filter;
	Filter = newFilter;

	//Adjust the filter to make the searches faster
	Filter.ReplaceInline(TXT(" "), DString::EmptyString, DString::CC_CaseSensitive);
	Filter.ToUpper(); //case insensitive

	if (oldFilter.Compare(Filter, DString::CC_CaseSensitive) != 0) //If something changed
	{
		UpdateTextContent();
		RefreshHighlightSelectionComps();
		RefreshReadOnlyColorComps();
		SetBrowseIndex(-1);
	}
}

void ListBoxComponent::InitializeBackground ()
{
	Background = FrameComponent::CreateObject();
	if (AddComponent(Background))
	{
		Background->SetSize(Vector2(1.f, 1.f));
		Background->SetLockedFrame(true);
	}
}

void ListBoxComponent::InitializeItemListText ()
{
	CHECK(Background.IsValid())

	ItemListText = LabelComponent::CreateObject();
	if (Background->AddComponent(ItemListText))
	{
		ItemListText->SetClampText(true);
		ItemListText->SetWrapText(false);
		ItemListText->SetAnchorTop(Background->GetBorderThickness());
		ItemListText->SetAnchorRight(Background->GetBorderThickness());
		ItemListText->SetAnchorBottom(Background->GetBorderThickness());
		ItemListText->SetAnchorLeft(Background->GetBorderThickness());
		ItemListText->SetAutoSizeVertical(true);

		UpdateTextContent();
	}
}

void ListBoxComponent::InitializeHighlightBar ()
{
	CHECK(ItemListText.IsValid())

	HighlightOwner = PlanarTransformComponent::CreateObject();
	if (ItemListText->AddComponent(HighlightOwner))
	{
		HighlightOwner->SetSize(Vector2::ZERO_VECTOR);
		HighlightHoverRender = ColorRenderComponent::CreateObject();
		if (HighlightOwner->AddComponent(HighlightHoverRender))
		{
			HighlightHoverRender->SolidColor = HoverColor;
		}
	}
}

void ListBoxComponent::RefreshHighlightSelectionComps ()
{
	size_t numHighlightComps = 0;
	std::vector<size_t> highlightIndices;
	//Translate from SelectedItems to the current displayed DisplayedListIndices
	for (size_t i = 0; i < SelectedItems.size(); ++i)
	{
		size_t matchIdx = GetDisplayedIndex(SelectedItems.at(i));
		if (matchIdx != INDEX_NONE)
		{
			highlightIndices.push_back(matchIdx);
		}
	}

	//Sort in ascending order since the highlighting render components would group together.
	std::sort(highlightIndices.begin(), highlightIndices.end(), [](size_t a, size_t b)
	{
		return (a < b);
	});

	for (size_t i = 0; i < highlightIndices.size(); ++i)
	{
		//Check if this is adjacent to another.
		if (i > 0 && (highlightIndices.at(i) - highlightIndices.at(i - 1)) == 1)
		{
			//Instead of creating another render component, reuse the previous one by increasing the size height.
			CHECK(ContainerUtils::IsValidIndex(HighlightSelections, numHighlightComps - 1))
			SHighlightSelectionInfo& highlight = HighlightSelections.at(numHighlightComps - 1);
			highlight.RenderOwner->SetSize(highlight.RenderOwner->GetSize() + Vector2(0.f, ItemListText->GetLineHeight()));
			continue;
		}

		SHighlightSelectionInfo* highlightSelection = nullptr;
		if (numHighlightComps >= HighlightSelections.size())
		{
			HighlightSelections.push_back(SHighlightSelectionInfo());
			highlightSelection = &HighlightSelections.at(HighlightSelections.size() - 1);
			highlightSelection->RenderOwner = PlanarTransformComponent::CreateObject();
			if (ItemListText->AddComponent(highlightSelection->RenderOwner))
			{
				highlightSelection->RenderComp = ColorRenderComponent::CreateObject();
				if (highlightSelection->RenderOwner->AddComponent(highlightSelection->RenderComp))
				{
					highlightSelection->RenderComp->SolidColor = SelectionColor;
				}
			}
		}
		else
		{
			highlightSelection = &HighlightSelections.at(numHighlightComps);
		}

		++numHighlightComps;
		highlightSelection->RenderOwner->SetPosition(Vector2(0.f, ItemListText->GetLineHeight() * Int(highlightIndices.at(i)).ToFloat()));
		highlightSelection->RenderOwner->SetSize(Vector2(1.f, ItemListText->GetLineHeight()));
	}

	//Destroy all unused components
	if (numHighlightComps < HighlightSelections.size())
	{
		for (size_t i = numHighlightComps; i < HighlightSelections.size(); ++i)
		{
			HighlightSelections.at(i).RenderOwner->Destroy();
		}

		HighlightSelections.resize(numHighlightComps);
	}
}

void ListBoxComponent::RefreshReadOnlyColorComps ()
{
	size_t numReadOnlyComps = 0;
	for (size_t i = 0; i < DisplayedListIndices.size(); ++i)
	{
		size_t curListIdx = DisplayedListIndices.at(i);
		if (!List.at(curListIdx)->bReadOnly)
		{
			continue;
		}
		
		if (i > 0)
		{
			//Check if this is adjacent to another.
			size_t prevListIdx = DisplayedListIndices.at(i-1);
			if (List.at(prevListIdx)->bReadOnly)
			{
				//Instead of creating another render component, reuse the previous one by increasing the size height.
				CHECK(ContainerUtils::IsValidIndex(ReadOnlySelections, numReadOnlyComps - 1))
				SHighlightSelectionInfo& highlight = ReadOnlySelections.at(numReadOnlyComps - 1);
				highlight.RenderOwner->EditSize().Y += ItemListText->GetLineHeight();
				continue;
			}
		}

		SHighlightSelectionInfo* readOnlyRegion = nullptr;
		if (numReadOnlyComps >= ReadOnlySelections.size())
		{
			ReadOnlySelections.push_back(SHighlightSelectionInfo());
			readOnlyRegion = &ReadOnlySelections.at(ReadOnlySelections.size() - 1);
			readOnlyRegion->RenderOwner = PlanarTransformComponent::CreateObject();
			if (ItemListText->AddComponent(readOnlyRegion->RenderOwner))
			{
				readOnlyRegion->RenderComp = ColorRenderComponent::CreateObject();
				if (readOnlyRegion->RenderOwner->AddComponent(readOnlyRegion->RenderComp))
				{
					readOnlyRegion->RenderComp->SolidColor = ReadOnlyColor;
				}
			}
		}
		else
		{
			readOnlyRegion = &ReadOnlySelections.at(numReadOnlyComps);
		}

		++numReadOnlyComps;
		readOnlyRegion->RenderOwner->SetPosition(Vector2(0.f, ItemListText->GetLineHeight() * Int(i).ToFloat()));
		readOnlyRegion->RenderOwner->SetSize(Vector2(1.f, ItemListText->GetLineHeight()));
	}

	//Destroy all unused components
	if (numReadOnlyComps < ReadOnlySelections.size())
	{
		for (size_t i = numReadOnlyComps; i < ReadOnlySelections.size(); ++i)
		{
			ReadOnlySelections.at(i).RenderOwner->Destroy();
		}

		ReadOnlySelections.resize(numReadOnlyComps);
	}
}

void ListBoxComponent::RenderHighlightHoverOverBrowsedItem ()
{
	CHECK(HighlightOwner.IsValid() && ItemListText.IsValid())
	if (BrowseIndex == INT_INDEX_NONE)
	{
		//Set size to be zero instead of setting render visibility to avoid conflict with GuiComponent's SetVisibility.
		HighlightOwner->SetSize(Vector2::ZERO_VECTOR);
		return;
	}

	HighlightOwner->SetSize(Vector2(1.f, ItemListText->GetLineHeight()));
	HighlightOwner->SetPosition(Vector2(0.f, ItemListText->GetLineHeight() * BrowseIndex.ToFloat()));
}

Int ListBoxComponent::GetLineIdxFromCoordinates (Float xPos, Float yPos) const
{
	CHECK(ItemListText.IsValid())

	Vector2 relativeCoords(xPos, yPos);
	relativeCoords -= ItemListText->ReadCachedAbsPosition();

	//Did user click within bounds
	if (relativeCoords.X < 0 || relativeCoords.X > ItemListText->ReadCachedAbsSize().X)
	{
		return INT_INDEX_NONE;
	}
	else if (relativeCoords.Y < 0 || relativeCoords.Y > ItemListText->ReadCachedAbsSize().Y)
	{
		return INT_INDEX_NONE;
	}

	Int lineIdx = (relativeCoords.Y / ItemListText->GetLineHeight()).ToInt();

	//Min to handle case when user clicked beyond the last item, but still within component boundaries.
	return Utils::Min<Int>(lineIdx, List.size() - 1);
}

Rectangle ListBoxComponent::GetItemRegion (Int lineIdx) const
{
	CHECK(ItemListText.IsValid())

	Vector2 center = Vector2(ItemListText->ReadCachedAbsSize().X * 0.5f, (lineIdx.ToFloat() * ItemListText->GetLineHeight()) + (ItemListText->GetLineHeight() * 0.5f));
	center += ItemListText->ReadCachedAbsPosition();

	return Rectangle(ItemListText->ReadCachedAbsSize().X, ItemListText->GetLineHeight(), center);
}

void ListBoxComponent::RefreshVerticalSize ()
{
	if (ItemListText.IsValid())
	{
		Float lineHeight = ItemListText->GetLineHeight();
		Float newVertSize = lineHeight * Float(Utils::Max<size_t>(1, DisplayedListIndices.size()));
		newVertSize += ItemListText->GetLineSpacing(); //Add a little space for the last item to give it a margin.
		SetSize(ReadSize().X, newVertSize);
	}
}
SD_END