/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TestMenuFocusInterface.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "DropdownComponent.h"
#include "FocusComponent.h"
#include "FrameComponent.h"
#include "GuiTheme.h"
#include "ListBoxComponent.h"
#include "SingleSpriteButtonState.h"
#include "TestMenuFocusInterface.h"
#include "TextFieldComponent.h"
#include "TreeListComponent.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::TestMenuFocusInterface, SD::GuiEntity)
SD_BEGIN

void TestMenuFocusInterface::InitProps ()
{
	Super::InitProps();

	PageIdx = 0;

	MenuFrame = nullptr;
	NextPage = nullptr;
	PrevPage = nullptr;

	FocusButton = nullptr;
	FocusDropdown = nullptr;
	FocusTextField = nullptr;
	FocusListBox = nullptr;
	FocusTreeList = nullptr;

	ListBoxEntries.push_back(TXT("Red"));
	ListBoxEntries.push_back(TXT("Orange"));
	ListBoxEntries.push_back(TXT("Yellow"));
	ListBoxEntries.push_back(TXT("Green"));
	ListBoxEntries.push_back(TXT("Blue"));
	ListBoxEntries.push_back(TXT("Indigo"));
	ListBoxEntries.push_back(TXT("Violet"));
	ListBoxEntries.push_back(TXT("White"));
	ListBoxEntries.push_back(TXT("Black"));
	ListBoxEntries.push_back(TXT("Invisible"));

	DropdownEntries.push_back(TXT("First Option"));
	DropdownEntries.push_back(TXT("Some other option"));
	DropdownEntries.push_back(TXT("DString entries"));
	DropdownEntries.push_back(TXT("Press Up and Down"));
	DropdownEntries.push_back(TXT("to browse to an option"));
	DropdownEntries.push_back(TXT("Enter characters to"));
	DropdownEntries.push_back(TXT("jump to an option"));
	DropdownEntries.push_back(TXT("Press enter to expand"));
	DropdownEntries.push_back(TXT("and again to select item"));
	DropdownEntries.push_back(TXT("Testing Search"));
	DropdownEntries.push_back(TXT("Testing Search Entries"));
	DropdownEntries.push_back(TXT("Testing Search Fruits"));
	DropdownEntries.push_back(TXT("Testing Search Apples"));
	DropdownEntries.push_back(TXT("TESTING SEARCH BANANAS"));
	DropdownEntries.push_back(TXT("testing search oranges"));
	DropdownEntries.push_back(TXT("Testing search pears"));
	DropdownEntries.push_back(TXT("Testing search grapes"));
	DropdownEntries.push_back(TXT("Testing search Avocados"));
	DropdownEntries.push_back(TXT("Last option"));

	TreeListEntries.push_back(TXT("Root"));
	TreeListEntries.push_back(TXT("A"));
	TreeListEntries.push_back(TXT("A1"));
	TreeListEntries.push_back(TXT("A2"));
	TreeListEntries.push_back(TXT("A2a"));
	TreeListEntries.push_back(TXT("A2b"));
	TreeListEntries.push_back(TXT("A2bi"));
	TreeListEntries.push_back(TXT("A2bii"));
	TreeListEntries.push_back(TXT("A2biii"));
	TreeListEntries.push_back(TXT("A2biv"));
	TreeListEntries.push_back(TXT("A2c"));
	TreeListEntries.push_back(TXT("A2d"));
	TreeListEntries.push_back(TXT("A3"));
	TreeListEntries.push_back(TXT("A4"));
	TreeListEntries.push_back(TXT("A4a"));
	TreeListEntries.push_back(TXT("A4b"));
	TreeListEntries.push_back(TXT("A4bi"));
	TreeListEntries.push_back(TXT("A4bii"));
	TreeListEntries.push_back(TXT("B"));
	TreeListEntries.push_back(TXT("B1"));
	TreeListEntries.push_back(TXT("B1a"));
	TreeListEntries.push_back(TXT("B1b"));
	TreeListEntries.push_back(TXT("B1bi"));
	TreeListEntries.push_back(TXT("B1bii"));
	TreeListEntries.push_back(TXT("B1biii"));
	TreeListEntries.push_back(TXT("B1c"));
	TreeListEntries.push_back(TXT("B2"));
	TreeListEntries.push_back(TXT("B3"));
	TreeListEntries.push_back(TXT("C"));
	TreeListEntries.push_back(TXT("CA"));
}

void TestMenuFocusInterface::LoseFocus ()
{
	Super::LoseFocus();

	UnitTestLog.Log(LogCategory::LL_Debug, TXT("%s has lost focus."), ToString());
}

void TestMenuFocusInterface::GainFocus ()
{
	Super::GainFocus();

	UnitTestLog.Log(LogCategory::LL_Debug, TXT("%s has gained focus."), ToString());
}

void TestMenuFocusInterface::ConstructUI ()
{
	PageData.resize(3); //Add 3 pages

	MenuFrame = FrameComponent::CreateObject();
	if (AddComponent(MenuFrame))
	{
		MenuFrame->SetLockedFrame(true);
		MenuFrame->SetSize(Vector2(1.f, 1.f));
		MenuFrame->SetPosition(Vector2::ZERO_VECTOR);

		NextPage = ButtonComponent::CreateObject();
		if (!MenuFrame->AddComponent(NextPage))
		{
			return;
		}

		CHECK(NextPage->GetRenderComponent() != nullptr)
		NextPage->SetSize(Vector2(96, 16));
		NextPage->SetPosition(Vector2(MenuFrame->ReadCachedAbsSize().X - NextPage->ReadCachedAbsSize().X - MenuFrame->GetBorderThickness(), MenuFrame->ReadCachedAbsSize().Y - NextPage->ReadCachedAbsSize().Y - MenuFrame->GetBorderThickness()));
		NextPage->GetBackground()->SetCenterTexture(GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get());
		NextPage->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		NextPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleNextPageReleased, void, ButtonComponent*));
		NextPage->SetCaptionText(TXT("Next"));

		PrevPage = ButtonComponent::CreateObject();
		if (!MenuFrame->AddComponent(PrevPage))
		{
			return;
		}

		CHECK(PrevPage->GetRenderComponent() != nullptr)
		PrevPage->SetSize(Vector2(96, 16));
		PrevPage->SetPosition(Vector2(MenuFrame->GetBorderThickness(), MenuFrame->ReadCachedAbsSize().Y - PrevPage->ReadCachedAbsSize().Y - MenuFrame->GetBorderThickness()));
		PrevPage->GetBackground()->SetCenterTexture(GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get());
		PrevPage->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		PrevPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandlePrevPageReleased, void, ButtonComponent*));
		PrevPage->SetCaptionText(TXT("Previous"));

		Vector2 posOffset = Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetBorderThickness());

		FocusButton = ButtonComponent::CreateObject();
		if (!MenuFrame->AddComponent(FocusButton))
		{
			return;
		}

		CHECK(FocusButton->GetRenderComponent() != nullptr)
		FocusButton->SetSize(Vector2(128, 16));
		FocusButton->SetPosition(posOffset);
		posOffset.Y += FocusButton->ReadSize().Y;
		posOffset.Y += 8.f;
		FocusButton->GetBackground()->SetCenterTexture(GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get());
		FocusButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		FocusButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleButtonReleased, void, ButtonComponent*));
		FocusButton->SetCaptionText(TXT("Focus Button"));
		PageData.at(0).Components.push_back(FocusButton.Get());

		FocusDropdown = DropdownComponent::CreateObject();
		if (!MenuFrame->AddComponent(FocusDropdown))
		{
			return;
		}

		FocusDropdown->SetSize(Vector2(200, 200));
		FocusDropdown->SetPosition(posOffset);
		posOffset.Y += FocusDropdown->GetCollapsedHeight();
		posOffset.Y += 8.f;
		FocusDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, TestMenuFocusInterface, HandleDropdownOptionSelected, void, DropdownComponent*, Int);
		FocusDropdown->SetNoSelectedItemText(TXT("<Nothing selected>"));
		PageData.at(0).Components.push_back(FocusDropdown.Get());
		for (UINT_TYPE i = 0; i < DropdownEntries.size(); i++)
		{
			FocusDropdown->GetExpandMenuList()->AddOption<DString>(GuiDataElement<DString>(DropdownEntries.at(i), DropdownEntries.at(i)));
		}

		FocusTextField = TextFieldComponent::CreateObject();
		if (!MenuFrame->AddComponent(FocusTextField))
		{
			return;
		}

		FocusTextField->SetSize(Vector2(200, 96));
		FocusTextField->SetPosition(posOffset);
		posOffset.Y += FocusTextField->ReadSize().Y;
		FocusTextField->SetWrapText(true);
		FocusTextField->SetEditable(true);
		FocusTextField->SetSelectable(true);
		FocusTextField->SetText(TXT("When focused, the cursor position will be placed at the beginning.  Press Escape to deselect cursor position.  While deselected, you can either press tab to focus next UI element, or press enter again to make more edits to the text field."));
		PageData.at(0).Components.push_back(FocusTextField.Get());

		//Page 2
		FocusListBox = ListBoxComponent::CreateObject();
		if (!MenuFrame->AddComponent(FocusListBox))
		{
			return;
		}
		else
		{
			posOffset = Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetBorderThickness());
			FocusListBox->SetSize(Vector2(200, 148));
			FocusListBox->SetPosition(posOffset);
			posOffset.Y += FocusListBox->ReadSize().Y;
			posOffset.Y += 8.f;
			FocusListBox->OnItemSelected = SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleListBoxItemSelected, void, Int);
			FocusListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleListBoxItemDeselected, void, Int);

			std::vector<GuiDataElement<DString>> listBoxItems;
			for (DString listBoxItem : ListBoxEntries)
			{
				listBoxItems.push_back(GuiDataElement<DString>(listBoxItem, listBoxItem));
			}
			FocusListBox->SetList(listBoxItems);
			PageData.at(1).Components.push_back(FocusListBox.Get());
		}

		//Page 3
		FocusTreeList = TreeListComponent::CreateObject();
		if (!MenuFrame->AddComponent(FocusTreeList))
		{
			return;
		}

		FocusTreeList->SetPosition(Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetBorderThickness()));
		FocusTreeList->SetSize(MenuFrame->ReadCachedAbsSize() - Vector2(MenuFrame->GetBorderThickness() * 2, (MenuFrame->GetBorderThickness() * 2) + NextPage->ReadCachedAbsSize().Y));
		FocusTreeList->OnOptionSelected = SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleTreeListOptionSelected, void, Int);
		PageData.at(2).Components.push_back(FocusTreeList.Get());
		std::vector<TreeListComponent::SDataBranch*> branches;
		for (UINT_TYPE i = 0; i < TreeListEntries.size(); i++)
		{
			branches.push_back(new TreeListComponent::SDataBranch(new GuiDataElement<DString>(TreeListEntries.at(i), TreeListEntries.at(i))));
		}
		/*
		Manually link branches to this format
		[01]A
		[02]	A1
		[03]	A2
		[04]		A2a
		[05]		A2b
		[06]			A2bi
		[07]			A2bii
		[08]			A2biii
		[09]			A2biv
		[10]		A2c
		[11]		A2d
		[12]	A3
		[13]	A4
		[14]		A4a
		[15]		A4b
		[16]			A4bi
		[17]			A4bii
		[18]B
		[19]	B1
		[20]		B1a
		[21]		B1b
		[22]			B1bi
		[23]			B1bii
		[24]			B1biii
		[25]		B1c
		[26]	B2
		[27]	B3
		[28]C
		[29]	CA
		*/
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(1));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(2));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(3));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(4));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(5));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(6));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(7));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(8));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(9));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(10));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(11));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(12));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(13));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(13), *branches.at(14));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(13), *branches.at(15));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(15), *branches.at(16));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(15), *branches.at(17));

		TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(18));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(19));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(20));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(21));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(22));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(23));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(24));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(25));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(26));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(27));

		TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(28));
		TreeListComponent::SDataBranch::LinkBranches(*branches.at(28), *branches.at(29));
		FocusTreeList->SetTreeList(branches);
	}

	RefreshPageVisibility();
}

void TestMenuFocusInterface::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(FocusButton.Get());
	Focus->TabOrder.push_back(FocusDropdown.Get());
	Focus->TabOrder.push_back(FocusTextField.Get());
	Focus->TabOrder.push_back(PrevPage.Get());
	Focus->TabOrder.push_back(NextPage.Get());
	Focus->TabOrder.push_back(FocusListBox.Get());
	Focus->TabOrder.push_back(FocusTreeList.Get());
}

void TestMenuFocusInterface::RefreshPageVisibility ()
{
	for (UINT_TYPE i = 0; i < PageData.size(); i++)
	{
		bool bIsVisible = (PageIdx == i);
		for (UINT_TYPE j = 0; j < PageData.at(i).Components.size(); j++)
		{
			PageData.at(i).Components.at(j)->SetVisibility(bIsVisible);
		}
	}
}

void TestMenuFocusInterface::HandleNextPageReleased (ButtonComponent* uiComponent)
{
	if (++PageIdx >= PageData.size())
	{
		PageIdx = 0;
	}

	RefreshPageVisibility();
}

void TestMenuFocusInterface::HandlePrevPageReleased (ButtonComponent* uiComponent)
{
	if (--PageIdx < 0)
	{
		PageIdx = PageData.size() - 1;
	}

	RefreshPageVisibility();
}

void TestMenuFocusInterface::HandleButtonReleased (ButtonComponent* uiComponent)
{
	UnitTestLog.Log(LogCategory::LL_Debug, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
}

void TestMenuFocusInterface::HandleDropdownOptionSelected (DropdownComponent* comp, Int newIdx)
{
	UnitTestLog.Log(LogCategory::LL_Debug, TXT("Focus Dropdown selected new item:  %s"), FocusDropdown->GetSelectedGuiDataElement<DString>()->GetLabelText());
}

void TestMenuFocusInterface::HandleListBoxItemSelected (Int selectedIdx)
{
	UnitTestLog.Log(LogCategory::LL_Debug, TXT("Focused List Box selected new item:  %s"), FocusListBox->ReadList().at(selectedIdx.ToUnsignedInt())->GetLabelText());
}

void TestMenuFocusInterface::HandleListBoxItemDeselected (Int deselectedIdx)
{
	UnitTestLog.Log(LogCategory::LL_Debug, TXT("Focused List Box deselected item:  %s"), FocusListBox->ReadList().at(deselectedIdx.ToUnsignedInt())->GetLabelText());
}

void TestMenuFocusInterface::HandleTreeListOptionSelected (Int optionIdx)
{
	TreeListComponent::SDataBranch* selectedItem = FocusTreeList->GetBranchFromIdx(optionIdx);
	if (selectedItem != nullptr && selectedItem->Data != nullptr)
	{
		UnitTestLog.Log(LogCategory::LL_Debug, TXT("Focus TreeList selected new item:  %s"), selectedItem->Data->GetLabelText());
	}
	else
	{
		UnitTestLog.Log(LogCategory::LL_Debug, TXT("Focus TreeList cleared item selection."));
	}
}
SD_END

#endif