/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ButtonStateComponent.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "ButtonStateComponent.h"
#include "FrameComponent.h"

IMPLEMENT_ABSTRACT_CLASS(SD::ButtonStateComponent, SD::EntityComponent)
SD_BEGIN

void ButtonStateComponent::InitProps ()
{
	Super::InitProps();

	OwningButton = nullptr;
}

void ButtonStateComponent::BeginObject ()
{
	Super::BeginObject();

	SetDefaultAppearance();
}

bool ButtonStateComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	if (ownerCandidate == nullptr)
	{
		return true;
	}

	return (dynamic_cast<ButtonComponent*>(ownerCandidate) != nullptr);
}

CopiableObjectInterface* ButtonStateComponent::CreateCopiableInstanceOfMatchingType () const
{
	if (const DClass* duneClass = StaticClass())
	{
		if (const ButtonStateComponent* cdo = dynamic_cast<const ButtonStateComponent*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void ButtonStateComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningButton = dynamic_cast<ButtonComponent*>(newOwner);
	SetDefaultAppearance();
}

void ButtonStateComponent::SetDefaultAppearance ()
{

}

void ButtonStateComponent::SetDisableAppearance ()
{

}

void ButtonStateComponent::SetHoverAppearance ()
{

}

void ButtonStateComponent::SetDownAppearance ()
{

}

void ButtonStateComponent::RefreshState ()
{
	if (OwningButton.IsNullptr())
	{
		return;
	}

	if (!OwningButton->GetEnabled())
	{
		SetDisableAppearance();
	}
	else if (OwningButton->GetPressedDown())
	{
		SetDownAppearance();
	}
	else if (OwningButton->GetHovered())
	{
		SetHoverAppearance();
	}
	else
	{
		SetDefaultAppearance();
	}
}

ButtonComponent* ButtonStateComponent::GetOwningButton () const
{
	return OwningButton.Get();
}
SD_END