/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ContextMenuContainer.cpp
=====================================================================
*/

#include "ContextMenuComponent.h"
#include "ContextMenuContainer.h"
#include "GuiDrawLayer.h"
#include "GuiEntity.h"

IMPLEMENT_CLASS(SD::ContextMenuContainer, SD::GuiEntity)
SD_BEGIN

void ContextMenuContainer::Destroyed ()
{
	RemoveMenuInstance();

	Super::Destroyed();
}

GuiEntity* ContextMenuContainer::InstantiateMenu (const DClass* menuClass)
{
	if (RevealedMenu != nullptr)
	{
		RemoveMenuInstance();
	}

	if (menuClass == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("ContextMenuContainer is unable to instantiate a menu instance without specifying a class."));
		return nullptr;
	}
	
	if (!menuClass->IsA(GuiEntity::SStaticClass()))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("ContextMenuContainer is unable to instantiate a menu since %s is not a GuiEntity."), menuClass->ToString());
		return nullptr;
	}

	if (menuClass->IsAbstract() || menuClass->GetDefaultObject() == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("ContextMenuContainer is unable to instantiate a menu since %s is an abstract class."), menuClass->ToString());
		return nullptr;
	}

	RevealedMenu = dynamic_cast<GuiEntity*>(menuClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	if (RevealedMenu == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("ContextMenuContainer is unable to instantiate %s"), menuClass->ToString());
	}

	//Many menus (like the DynamicListMenu) relies on its size to run some calculations. Attach the GuiEntity to this container so that it knows how much space it can work with.
	RevealedMenu->SetRelativeTo(this);

	return RevealedMenu.Get();
}

void ContextMenuContainer::RemoveMenuInstance ()
{
	if (RevealedMenu != nullptr)
	{
		RevealedMenu->Destroy();
		RevealedMenu = nullptr;
	}
}

void ContextMenuContainer::RevealContextMenu (ContextMenuComponent* comp, MousePointer* mouse)
{
	if (RevealedMenu != nullptr && comp != nullptr && DrawLayer != nullptr)
	{
		RevealedMenu->SetDepth(GetDepth());
		RevealedMenu->ComputeAbsTransform(); //The size needs to be computed to identify where it should be drawn.
		Vector2 newPosition = comp->CalcRevealLocation(RevealedMenu->ReadCachedAbsSize(), mouse->ToWindowCoordinates(), ReadCachedAbsSize());
		RevealedMenu->SetPosition(newPosition);

		if (Input != nullptr)
		{
			RevealedMenu->SetupInputComponent(Input->GetInputMessenger(), Input->GetInputPriority());
		}

		DrawLayer->RegisterMenu(RevealedMenu.Get());
	}
}
SD_END