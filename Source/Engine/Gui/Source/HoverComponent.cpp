/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HoverComponent.cpp
=====================================================================
*/

#include "HoverComponent.h"

IMPLEMENT_CLASS(SD::HoverComponent, SD::GuiComponent)
SD_BEGIN

void HoverComponent::InitProps ()
{
	Super::InitProps();

	bIsHoveringOwner = false;
}

void HoverComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ProcessMouseMove(mouse, sfmlEvent, deltaMove);

	EvaluateHoverState(mouse, sfmlEvent);
}

bool HoverComponent::GetIsHovering () const
{
	return bIsHoveringOwner;
}

void HoverComponent::EvaluateHoverState (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	bool bOldHovering = bIsHoveringOwner;
	bIsHoveringOwner = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));

	if (bOldHovering != bIsHoveringOwner)
	{
		//Hover state change detected, invoke callback
		if (bIsHoveringOwner && OnRollOver.IsBounded())
		{
			OnRollOver(mouse, GetOwner());
		}
		else if (!bIsHoveringOwner && OnRollOut.IsBounded())
		{
			OnRollOut(mouse, GetOwner());
		}
	}
}
SD_END