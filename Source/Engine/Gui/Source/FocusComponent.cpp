/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FocusComponent.cpp
=====================================================================
*/

#include "FocusComponent.h"
#include "FocusInterface.h"

IMPLEMENT_CLASS(SD::FocusComponent, SD::EntityComponent)
SD_BEGIN

void FocusComponent::InitProps ()
{
	Super::InitProps();

	SelectedEntityIdx = INT_INDEX_NONE;
	Input = nullptr;
}

void FocusComponent::BeginObject ()
{
	Super::BeginObject();

	InitializeInput();
}

void FocusComponent::SelectNextEntity ()
{
	if (TabOrder.size() <= 0)
	{
		ClearSelection();
		return;
	}

	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->LoseFocus();
	}

	do
	{
		if ((++SelectedEntityIdx).ToUnsignedInt() >= TabOrder.size())
		{
			SelectedEntityIdx = 0;
		}

		FocusInterface* newSelectedEntity = GetSelectedEntity();
		if (newSelectedEntity == selectedEntity)
		{
			break; //Went full loop (all other entities are currently not focusable), reselect old entity
		}
	}
	while (!GetSelectedEntity()->CanBeFocused());

	selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->GainFocus();
	}
}

bool FocusComponent::SelectObject (FocusInterface* target)
{
	if (ContainerUtils::IsEmpty(TabOrder) || !target->CanBeFocused())
	{
		return false;
	}

	FocusInterface* oldSelectedEntity = GetSelectedEntity();
	if (oldSelectedEntity == target)
	{
		//Already focused. Do nothing.
		return true;
	}

	size_t idx = 0;
	while (idx < TabOrder.size())
	{
		if (TabOrder.at(idx) == target)
		{
			break;
		}

		++idx;
	}

	if (idx >= TabOrder.size())
	{
		//target doesn't exist in TabOrder
		return false;
	}

	if (oldSelectedEntity != nullptr)
	{
		oldSelectedEntity->LoseFocus();
	}

	SelectedEntityIdx = idx;
	TabOrder.at(idx)->GainFocus();

	return true;
}

void FocusComponent::ClearSelection ()
{
	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->LoseFocus();
	}

	SelectedEntityIdx = INT_INDEX_NONE;
}

FocusInterface* FocusComponent::GetSelectedEntity () const
{
	size_t idx = SelectedEntityIdx.ToUnsignedInt();
	if (SelectedEntityIdx >= 0 && idx < TabOrder.size())
	{
		return TabOrder.at(idx);
	}

	return nullptr;
}

InputComponent* FocusComponent::GetInput () const
{
	return Input.Get();
}

void FocusComponent::InitializeInput ()
{
	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->OnInput = SDFUNCTION_1PARAM(this, FocusComponent, HandleInput, bool, const sf::Event&);
		Input->OnText = SDFUNCTION_1PARAM(this, FocusComponent, HandleText, bool, const sf::Event&);
	}
}

bool FocusComponent::HandleInput (const sf::Event& keyEvent)
{
	if (keyEvent.type == sf::Event::KeyPressed && keyEvent.key.code == sf::Keyboard::Tab)
	{
		if (GetSelectedEntity() == nullptr || GetSelectedEntity()->CanTabOut())
		{
			SelectNextEntity();
			return true;
		}
	}

	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		return selectedEntity->CaptureFocusedInput(keyEvent);
	}

	return false;
}

bool FocusComponent::HandleText (const sf::Event& keyEvent)
{
	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		return selectedEntity->CaptureFocusedText(keyEvent);
	}

	return false;
}
SD_END