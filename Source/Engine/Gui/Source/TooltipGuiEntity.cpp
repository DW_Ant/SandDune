/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TooltipGuiEntity.cpp
=====================================================================
*/

#include "FrameComponent.h"
#include "GuiDrawLayer.h"
#include "LabelComponent.h"
#include "ScrollbarComponent.h"
#include "TooltipComponent.h"
#include "TooltipGuiEntity.h"

IMPLEMENT_CLASS(SD::TooltipGuiEntity, SD::GuiEntity)
SD_BEGIN

void TooltipGuiEntity::InitProps ()
{
	Super::InitProps();

	AutoSizeHorizontal = false;
	AutoSizeVertical = false;

	RevealFadeTime = 0.5f;
	HideFadeTime = 0.5f;

	TopMargin = 2.f;
	RightMargin = 8.f;
	BottomMargin = 2.f;
	LeftMargin = 8.f;

	TooltipShowPosition = Vector2(-1.f, -1.f);
	StartRevealTimeDelay = -1.f;

	TooltipFrame = nullptr;
	TooltipText = nullptr;
	RevealTick = nullptr;
	FadingTick = nullptr;
	
	StartFadeTime = -1.f;
	DefaultBorderAlpha = 64;

	LastMousePosition = Vector2(-1.f, -1.f);
}

void TooltipGuiEntity::BeginObject ()
{
	Super::BeginObject();

	RevealTick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(RevealTick))
	{
		RevealTick->SetTickHandler(SDFUNCTION_1PARAM(this, TooltipGuiEntity, HandleRevealTick, void, Float));
		RevealTick->SetTicking(false);
	}

	FadingTick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(FadingTick))
	{
		FadingTick->SetTickHandler(SDFUNCTION_1PARAM(this, TooltipGuiEntity, HandleFadingTick, void, Float));
		FadingTick->SetTicking(false);
	}
}

void TooltipGuiEntity::ConstructUI ()
{
	Super::ConstructUI();

	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	CHECK(localTheme != nullptr)
	SetVisibility(false);

	TooltipFrame = FrameComponent::CreateObject();
	if (AddComponent(TooltipFrame))
	{
		TooltipFrame->SetLockedFrame(true);
		TooltipFrame->SetEnableFractionScaling(false); //always use absolute coordinates
		TooltipFrame->SetBorderThickness(2.f);
		TooltipFrame->SetCenterTexture(localTheme->TooltipBackground.Get());

		if (BorderRenderComponent* borderComp = TooltipFrame->GetBorderComp())
		{
			borderComp->SetBorderColor(Color(255, 255, 255, DefaultBorderAlpha));
		}

		TooltipText = LabelComponent::CreateObject();
		if (TooltipFrame->AddComponent(TooltipText))
		{
			TooltipText->SetAutoRefresh(false);
			TooltipText->SetPosition(Vector2::ZERO_VECTOR);
			TooltipText->SetSize(Vector2(1.f, 1.f));
			TooltipText->SetHorizontalAlignment(LabelComponent::HA_Center);
			TooltipText->SetVerticalAlignment(LabelComponent::VA_Center);
			TooltipText->SetAutoSizeHorizontal(true);
			TooltipText->SetAutoSizeVertical(true);
		}
	}
}

void TooltipGuiEntity::InitializeFocusComponent ()
{
	//Noop - Don't call super since this entity should never be focused
}

void TooltipGuiEntity::HandlePassiveInput (const sf::Event& evnt)
{
	Super::HandlePassiveInput(evnt);

	//Any key press would hide the tooltip.
	if (IsVisible())
	{
		HideTooltip();
	}
}

void TooltipGuiEntity::ProcessTooltipMouseMove (MousePointer* mouse, bool isWithinBounds, TooltipComponent* tooltip)
{
	if (mouse->ReadPosition() == LastMousePosition || tooltip == nullptr)
	{
		//This mouse event is already processed. Ignore this event.
		return;
	}

	if (!isWithinBounds && TooltipOwner != tooltip)
	{
		//Ignore irrelevant out of bounds tooltips
		return;
	}

	LastMousePosition = mouse->GetPosition(); //Block all other mouse events for this event.
	if (isWithinBounds && TooltipOwner != tooltip && TooltipOwner.IsValid())
	{
		//A different tooltip of higher priority is hovered. Hide old tooltip
		HideTooltip();
		return;
	}

	TooltipOwner = tooltip;

	//If showing tooltip while not fading out
	if (IsVisible() && FadeDestination != 0)
	{
		//Moved out of bounds or move far enough to hide the tooltip
		Float moveThresSquared(TooltipOwner->HideMoveThreshold);
		moveThresSquared.PowInline(2.f);
		if (!isWithinBounds || (mouse->ReadCachedAbsPosition() - TooltipShowPosition).CalcDistSquared() >= moveThresSquared)
		{
			HideTooltip();
		}
	}
	else if (isWithinBounds) //Not showing tooltip and hovering over something
	{
		//Reset counter and position since the mouse moved
		mouse->ComputeAbsTransform();
		TooltipShowPosition = mouse->ToWindowCoordinates();
		StartRevealTimeDelay = TooltipOwner->RevealDelay;
		if (StartRevealTimeDelay <= 0 || TooltipOwner->bTooltipBlocker)
		{
			RevealTooltip();
		}
		else
		{
			RevealTick->SetTicking(true);
		}
	}
	else if (RevealTick->IsTicking()) //Not visible and no longer within bounds, but it's about to reveal its tooltip
	{
		TooltipOwner = nullptr;
		RevealTick->SetTicking(false);
	}
}

void TooltipGuiEntity::HideTooltip ()
{
	TooltipOwner = nullptr;
	if (HideFadeTime <= 0.f)
	{
		//Instantly hide
		SetVisibility(false);
		return;
	}

	if (FadingTick != nullptr && (!FadingTick->IsTicking() || FadeDestination != 0))
	{
		FadeDestination = 0;
		FadeBorderDestination = 0;
		GetAlpha(OUT FadeSource, OUT FadeSourceBorder);

		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		StartFadeTime = localEngine->GetElapsedTime();

		FadingTick->SetTicking(true);
	}

	if (RevealTick != nullptr)
	{
		RevealTick->SetTicking(false);
	}
}

void TooltipGuiEntity::SetTopMargin (Float newTopMargin)
{
	TopMargin = newTopMargin;
}

void TooltipGuiEntity::SetRightMargin (Float newRightMargin)
{
	RightMargin = newRightMargin;
}

void TooltipGuiEntity::SetBottomMargin (Float newBottomMargin)
{
	BottomMargin = newBottomMargin;
}

void TooltipGuiEntity::SetLeftMargin (Float newLeftMargin)
{
	LeftMargin = newLeftMargin;
}

void TooltipGuiEntity::RevealTooltip ()
{
	if (TooltipOwner.IsNullptr())
	{
		return; //Tooltip component must have been destroyed or cleared during ticking.
	}

	CHECK(TooltipFrame != nullptr)
	TooltipFrame->SetVisibility(!TooltipOwner->bTooltipBlocker);
	if (TooltipOwner->bTooltipBlocker)
	{
		//Set visible to notify this entity its reveal state. However everything else is hidden since the tooltip frame is invisible.
		SetVisibility(true);
		return;
	}

	CHECK(TooltipText != nullptr)
	TooltipText->SetText(TooltipOwner->ReadTooltipText());
	TooltipText->RefreshText(); //Needed to calculate the size of the frame
	if (TooltipFrame != nullptr)
	{
		Vector2 newSize(TooltipText->ReadSize());
		newSize.X += (LeftMargin + RightMargin);
		newSize.Y += (TopMargin + BottomMargin);
		TooltipFrame->SetSize(newSize);

		TooltipText->SetAnchorTop(TopMargin);
		TooltipText->SetAnchorRight(RightMargin);
		TooltipText->SetAnchorBottom(BottomMargin);
		TooltipText->SetAnchorLeft(LeftMargin);

		TooltipFrame->SetPosition(CalcTooltipFramePosition(TooltipShowPosition));

		//Needed to horizontally align the text within the frame
		TooltipText->ComputeAbsTransform();
		TooltipText->RefreshText();
	}

	SetVisibility(true);
	if (RevealFadeTime <= 0.f)
	{
		//Instantly show
		ApplyAlpha(255, DefaultBorderAlpha);
	}
	else if (FadingTick != nullptr)
	{
		FadeDestination = 255;
		FadeBorderDestination = DefaultBorderAlpha;
		GetAlpha(OUT FadeSource, OUT FadeSourceBorder);

		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		StartFadeTime = localEngine->GetElapsedTime();

		FadingTick->SetTicking(true);
	}

	if (RevealTick != nullptr)
	{
		RevealTick->SetTicking(false);
	}
}

Vector2 TooltipGuiEntity::CalcTooltipFramePosition (const Vector2& desiredPosition)
{
	CHECK(TooltipFrame != nullptr)

	Vector2 result(desiredPosition);

	//True if there's enough space to place the frame above desiredPosition. Otherwise, try placing below it
	bool bDestinationAbove = (result.Y - TooltipFrame->ReadSize().Y >= 0.f);

	Int horizontalPlacement = 0; //-1 = snap left, 0 = keep centered, 1 = snap right
	if (result.X - (TooltipFrame->ReadSize().X * 0.5f) < 0.f)
	{
		horizontalPlacement = 1; //snap right	
	}
	else if (result.X + (TooltipFrame->ReadSize().X * 0.5f) > ReadCachedAbsSize().X)
	{
		horizontalPlacement = -1; //snap left
	}

	if (bDestinationAbove)
	{
		result.Y -= TooltipFrame->ReadSize().Y;
	}
	else
	{
		result.Y += 8.f; //Slightly below mouse cursor
	}

	switch (horizontalPlacement.Value)
	{
		case(-1): //snap left
			result.X = ReadCachedAbsSize().X - TooltipFrame->ReadSize().X;
			break;

		case(0): //center align
			result.X -= (TooltipFrame->ReadSize().X * 0.5f);
			break;

		case(1): //snap right
			result.X = 0.f;
			break;
	}

	return result;
}

void TooltipGuiEntity::ApplyAlpha (sf::Uint8 newAlpha, sf::Uint8 newBorderAlpha)
{
	if (TooltipText != nullptr && TooltipText->GetRenderComponent() != nullptr)
	{
		sf::Color newFontColor = TooltipText->GetRenderComponent()->GetFontColor();
		newFontColor.a = newAlpha;
		TooltipText->GetRenderComponent()->SetFontColor(newFontColor);
	}

	if (TooltipFrame != nullptr)
	{
		if (SpriteComponent* spriteComp = TooltipFrame->GetCenterCompAs<SpriteComponent>())
		{
			if (spriteComp->GetSprite() != nullptr)
			{
				sf::Color newColor = spriteComp->GetSprite()->getColor();
				newColor.a = newAlpha;
				spriteComp->GetSprite()->setColor(newColor);
			}
		}
		else if (ColorRenderComponent* colorComp = TooltipFrame->GetCenterCompAs<ColorRenderComponent>())
		{
			colorComp->SolidColor.Source.a = newAlpha;
		}

		if (BorderRenderComponent* borderComp = TooltipFrame->GetBorderComp())
		{
			Color newColor = borderComp->GetBorderColor();
			newColor.Source.a = newBorderAlpha;
			borderComp->SetBorderColor(newColor);
		}
	}
}

void TooltipGuiEntity::GetAlpha (sf::Uint8& outAlpha, sf::Uint8& outBorderAlpha) const
{
	outAlpha = 0;
	outBorderAlpha = 0;

	if (TooltipFrame != nullptr)
	{
		if (SpriteComponent* spriteComp = TooltipFrame->GetCenterCompAs<SpriteComponent>())
		{
			if (spriteComp->GetSprite() != nullptr)
			{
				outAlpha = spriteComp->GetSprite()->getColor().a;
			}
		}
		else if (ColorRenderComponent* colorComp = TooltipFrame->GetCenterCompAs<ColorRenderComponent>())
		{
			outAlpha = colorComp->SolidColor.Source.a;
		}

		if (BorderRenderComponent* borderComp = TooltipFrame->GetBorderComp())
		{
			outBorderAlpha = borderComp->GetBorderColor().Source.a;
		}
	}
}

void TooltipGuiEntity::HandleRevealTick (Float deltaSec)
{
	if (TooltipOwner.IsNullptr())
	{
		CHECK(RevealTick != nullptr)

		//TooltipComponent must have been destroyed or cleared. Cancel ticking.
		RevealTick->SetTicking(false);
		return;
	}

	StartRevealTimeDelay -= deltaSec;
	if (StartRevealTimeDelay <= 0)
	{
		RevealTooltip(); //<-- Also disables this tick component
	}
}

void TooltipGuiEntity::HandleFadingTick (Float deltaSec)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	
	Float fadeDuration = (FadeDestination == 0) ? HideFadeTime : RevealFadeTime;
	Float ratio = (localEngine->GetElapsedTime() - StartFadeTime) / fadeDuration;

	ratio = Utils::Min<Float>(ratio, 1.f);
	sf::Uint8 newAlpha = Utils::Lerp(ratio.Value, FadeSource, FadeDestination);
	sf::Uint8 newBorderAlpha = Utils::Lerp(ratio.Value, FadeSourceBorder, FadeBorderDestination);
	ApplyAlpha(newAlpha, newBorderAlpha);

	if (ratio >= 1.f)
	{
		//Finish fading
		if (FadeDestination == 0)
		{
			SetVisibility(false);
		}

		if (FadingTick != nullptr)
		{
			FadingTick->SetTicking(false);
		}
	}
}
SD_END