/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DropdownGuiEntity.cpp
=====================================================================
*/

#include "DropdownComponent.h"
#include "DropdownGuiEntity.h"
#include "ScrollableInterface.h"
#include "ScrollbarComponent.h"

IMPLEMENT_CLASS(SD::DropdownGuiEntity, SD::GuiEntity)
SD_BEGIN

void DropdownGuiEntity::InitProps ()
{
	Super::InitProps();

	TransformOwner = nullptr;
}

void DropdownGuiEntity::ConstructUI ()
{
	TransformOwner = GuiComponent::CreateObject();
	if (AddComponent(TransformOwner))
	{
		//The ExpandMenu shouldn't change its style based on this Entity's style. Instead they should remain what they have currently with the DropdownComponent.
		TransformOwner->PropagateStyle = false;
	}

	SetVisibility(false);
}

void DropdownGuiEntity::HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::HandlePassiveMouseClick(mouse, sfmlEvent, eventType);

	if (ExpandedDropdown == nullptr || eventType != sf::Event::MouseButtonReleased || TransformOwner == nullptr)
	{
		return;
	}

	CHECK(mouse != nullptr)

	//Collapse this menu if the user clicks outside of menu
	if (!TransformOwner->IsWithinBounds(mouse->ReadPosition()))
	{
		ExpandedDropdown->Collapse();
	}
}

void DropdownGuiEntity::Destroyed ()
{
	if (ExpandedDropdown != nullptr)
	{
		//Return the expand menu back to the original DropdownComponent
		HideDropdown(ExpandedDropdown.Get());
	}

	Super::Destroyed();
}

void DropdownGuiEntity::DisplayDropdown (DropdownComponent* dropdown)
{
	if (ExpandedDropdown == dropdown)
	{
		//Already revealed
		return;
	}

	if (ExpandedDropdown != nullptr)
	{
		//Return the expand menu back to the DropdownComponent.
		ExpandedDropdown->Collapse();
	}

	ListBoxComponent* listComp = dropdown->GetExpandMenuList();

	//Dropdown component must have a valid list box component. Note: If the ListBoxComponent doesn't have any items, its size is probably not yet computed.
	//The size is used to identify this component's draw location. If this list box doesn't have any items, best not to draw it.
	if (listComp == nullptr || ContainerUtils::IsEmpty(listComp->ReadList()))
	{
		return;
	}

	ExpandComp = dropdown->GetExpandComponent();
	if (ExpandComp != nullptr)
	{
		OriginalOwner = dynamic_cast<GuiComponent*>(ExpandComp->GetOwner());
		if (OriginalOwner != nullptr)
		{
			ExpandComp->DetachSelfFromOwner();
			if (!TransformOwner->AddComponent(ExpandComp, false))
			{
				OriginalOwner->AddComponent(ExpandComp);
				OriginalOwner = nullptr;
				ExpandComp = nullptr;
				return;
			}

			TransformOwner->SetSize(OriginalOwner->ReadCachedAbsSize());
			TransformOwner->SetPosition(FindDrawLocation(dropdown, true));
			ExpandedDropdown = dropdown;
			SetVisibility(true);
		}
	}
}

void DropdownGuiEntity::HideDropdown (DropdownComponent* dropdown)
{
	if (ExpandedDropdown != dropdown)
	{
		//This is not the dropdown being showed.
		return;
	}

	//Needed since the RegisteredTransformDelegates vector may point to garbage if the transforms are destroyed before unregistering themselves from this Entity.
	CHECK_INFO(ExpandComp != nullptr, "A DropdownComponent's ExpandMenu was destroyed without unregistering itself from the DropdownGuiEntity. All DropdownComponents are expected to remove themselves from the DropdownGuiEntity before destroying themselves.")

	SetVisibility(false);
	if (ExpandComp == nullptr)
	{
		//There's nothing to move. Something destroyed it.
		ExpandedDropdown = nullptr;
		OriginalOwner = nullptr;
		return;
	}

	if (ExpandedDropdown == nullptr || OriginalOwner == nullptr)
	{
		//The dropdown component or the transform owner were destroyed. Destroy the ExpandComp since that would have been destroyed, too.
		if (ExpandComp != nullptr)
		{
			ExpandComp->Destroy();
			ExpandComp = nullptr;
		}

		ExpandedDropdown = nullptr;
		OriginalOwner = nullptr;
		return;
	}

	//Move expand menu back to the dropdown component
	ExpandComp->DetachSelfFromOwner();
	OriginalOwner->AddComponent(ExpandComp);

	for (Transformation* registeredTransform : RegisteredTransformDelegates)
	{
		registeredTransform->OnTransformChanged.UnregisterHandler(SDFUNCTION(this, DropdownGuiEntity, HandleExpandTransformChanged, void));
	}
	ContainerUtils::Empty(OUT RegisteredTransformDelegates);

	for (PlanarCamera* cam : RegisteredZoomDelegates)
	{
		cam->OnZoomChanged.UnregisterHandler(SDFUNCTION_1PARAM(this, DropdownGuiEntity, HandleZoomChanged, void, Float));
	}
	ContainerUtils::Empty(OUT RegisteredZoomDelegates);

	ExpandComp = nullptr;
	OriginalOwner = nullptr;
	ExpandedDropdown = nullptr;
}

Vector2 DropdownGuiEntity::FindDrawLocation (DropdownComponent* dropdown, bool registerDelegates) const
{
	CHECK(dropdown != nullptr)
	Vector2 result(dropdown->ReadCachedAbsPosition());

	SDFunction<void> transformChanged(SDFUNCTION(this, DropdownGuiEntity, HandleExpandTransformChanged, void));

	if (registerDelegates)
	{
		dropdown->OnTransformChanged.RegisterHandler(transformChanged);
		RegisteredTransformDelegates.push_back(dropdown);
	}

	GuiComponent* curComp = dropdown;
	while (true)
	{
		ScrollableInterface* scrollInterface = dynamic_cast<ScrollableInterface*>(curComp->GetRootEntity());
		if (scrollInterface == nullptr)
		{
			break;
		}

		ScrollbarComponent* scrollbar = scrollInterface->GetScrollbarOwner();
		if (scrollbar == nullptr)
		{
			break;
		}

		if (PlanarCamera* cam = scrollbar->GetFrameCamera())
		{
			result -= cam->ReadPosition() - (cam->GetZoomedExtents() * 0.5f);

			if (registerDelegates)
			{
				cam->OnTransformChanged.RegisterHandler(transformChanged);
				RegisteredTransformDelegates.push_back(cam);

				cam->OnZoomChanged.RegisterHandler(SDFUNCTION_1PARAM(this, DropdownGuiEntity, HandleZoomChanged, void, Float));
				RegisteredZoomDelegates.push_back(cam);
			}
		}

		if (PlanarTransformComponent* transform = scrollbar->GetFrameSpriteTransform())
		{
			result += transform->ReadCachedAbsPosition();

			if (registerDelegates)
			{
				transform->OnTransformChanged.RegisterHandler(transformChanged);
				RegisteredTransformDelegates.push_back(transform);
			}
		}

		curComp = scrollbar;
	}

	if (ExpandComp != nullptr && TransformOwner->ReadParentAbsSize().CalcDistSquared() > 0.f)
	{
		//Clamp the result so that it cannot go off the viewport.
		result.X = Utils::Clamp<Float>(result.X, 0.f, TransformOwner->ReadParentAbsSize().X - ExpandComp->ReadCachedAbsSize().X);
		result.Y = Utils::Clamp<Float>(result.Y, 0.f, TransformOwner->ReadParentAbsSize().Y - ExpandComp->ReadCachedAbsSize().Y);
	}

	return result;
}

void DropdownGuiEntity::HandleExpandTransformChanged () const
{
	CHECK(ExpandedDropdown != nullptr && TransformOwner != nullptr)
	TransformOwner->SetPosition(FindDrawLocation(ExpandedDropdown.Get(), false));
}

void DropdownGuiEntity::HandleZoomChanged (Float newZoom) const
{
	CHECK(ExpandedDropdown != nullptr)
	ExpandedDropdown->Collapse();
}
SD_END