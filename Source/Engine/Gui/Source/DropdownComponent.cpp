/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DropdownComponent.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "DropdownComponent.h"
#include "DropdownGuiEntity.h"
#include "FrameComponent.h"
#include "GuiEntity.h"
#include "GuiUtils.h"
#include "LabelComponent.h"
#include "ScrollbarComponent.h"
#include "SingleSpriteButtonState.h"
#include "TextFieldComponent.h"

IMPLEMENT_CLASS(SD::DropdownComponent, SD::GuiComponent)
SD_BEGIN

void DropdownComponent::InitProps ()
{
	Super::InitProps();

	ItemSelectionHeight = 0.2f;
	NoSelectedItemText = DString::EmptyString;
	bExpanded = false;
	bEnabled = true;
	bShowExpandMenuOnTop = true;
	ExpandTimestamp = -1.f;
	DragSelectThreshold = 0.15f;

	ExpandMenuScrollbar = nullptr;
	ExpandMenu = nullptr;
	ExpandMenuList = nullptr;
	ExpandButton = nullptr;
	ButtonTextureCollapsed = nullptr;
	ButtonTextureExpanded = nullptr;
	SelectedObjBackground = nullptr;
	SelectedItemLabel = nullptr;
	bIgnoreEnterRelease = false;
	SearchString = DString::EmptyString;
	SearchStringTimestamp = 0.f;
	SearchStringClearTime = 1.f;
	PrevSelectIdx = INDEX_NONE;
}

void DropdownComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const DropdownComponent* dropdownTemplate = dynamic_cast<const DropdownComponent*>(objTemplate);
	if (dropdownTemplate != nullptr)
	{
		SetItemSelectionHeight(dropdownTemplate->ItemSelectionHeight);
		SetShowExpandMenuOnTop(dropdownTemplate->bShowExpandMenuOnTop);
		SetDragSelectThreshold(dropdownTemplate->DragSelectThreshold);

		bool bCreatedObj;
		ScrollbarComponent* newScrollbar = ReplaceTargetWithObjOfMatchingClass(ExpandMenuScrollbar.Get(), dropdownTemplate->ExpandMenuScrollbar.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			ExpandMenuScrollbar = newScrollbar;
		}

		if (ExpandMenuScrollbar.IsValid())
		{
			ExpandMenuScrollbar->CopyPropertiesFrom(dropdownTemplate->ExpandMenuScrollbar.Get());
			ExpandMenuScrollbar->SetViewedObject(ExpandMenu);
		}

		ListBoxComponent* newExpandMenu = ReplaceTargetWithObjOfMatchingClass(ExpandMenuList.Get(), dropdownTemplate->GetExpandMenuList(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetExpandMenu(newExpandMenu);
		}

		if (ExpandMenuList.IsValid())
		{
			ExpandMenuList->CopyPropertiesFrom(dropdownTemplate->GetExpandMenuList());
		}

		ButtonComponent* newExpandButton = ReplaceTargetWithObjOfMatchingClass(ExpandButton.Get(), dropdownTemplate->GetExpandButton(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetExpandButton(newExpandButton);
		}

		if (ExpandButton.IsValid())
		{
			ExpandButton->CopyPropertiesFrom(dropdownTemplate->GetExpandButton());
		}

		SetButtonTextureCollapsed(dropdownTemplate->GetButtonTextureCollapsed());
		SetButtonTextureExpanded(dropdownTemplate->GetButtonTextureExpanded());

		FrameComponent* newSelectedObjBackground = ReplaceTargetWithObjOfMatchingClass(SelectedObjBackground.Get(), dropdownTemplate->GetSelectedObjBackground(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetSelectedObjBackground(newSelectedObjBackground);
		}

		if (SelectedObjBackground.IsValid())
		{
			SelectedObjBackground->CopyPropertiesFrom(dropdownTemplate->GetSelectedObjBackground());
		}

		LabelComponent* newSelectedItemLabel = ReplaceTargetWithObjOfMatchingClass(SelectedItemLabel.Get(), dropdownTemplate->GetSelectedItemLabel(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetSelectedItemLabel(newSelectedItemLabel);
		}

		if (SelectedItemLabel.IsValid())
		{
			SelectedItemLabel->CopyPropertiesFrom(dropdownTemplate->GetSelectedItemLabel());
		}
	}
}

bool DropdownComponent::CanBeFocused () const
{
	return (FocusInterface::CanBeFocused() && IsVisible() && IsEnabled());
}

void DropdownComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	Collapse(); //clear selection
}

bool DropdownComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (ExpandMenuList == nullptr || !IsEnabled())
	{
		return false;
	}

	if (keyEvent.key.code == sf::Keyboard::Return)
	{
		if (!bExpanded)
		{
			if (ExpandButton.IsValid())
			{
				if (keyEvent.type == sf::Event::KeyPressed)
				{
					ExpandButton->SetButtonDown(true, true);
					bIgnoreEnterRelease = true; //Ignore next release key to prevent it from immediately collapsing the menu.
				}
			}

			return true;
		}
		else //Expanded
		{
			if (keyEvent.type == sf::Event::KeyReleased && ExpandMenuList->GetBrowseIndex() != INT_INDEX_NONE && !bIgnoreEnterRelease)
			{
				ExpandMenuList->SetItemSelectionByIdx(ExpandMenuList->GetBrowseIndex(), !ExpandMenuList->IsItemSelectedByIndex(ExpandMenuList->GetBrowseIndex()));
				if (ExpandButton.IsValid())
				{
					ExpandButton->SetButtonUp(true, true);
				}
			}

			bIgnoreEnterRelease = false;
			return true;
		}
	}

	if (bExpanded && keyEvent.type == sf::Event::KeyPressed)
	{
		if (keyEvent.key.code == sf::Keyboard::Up)
		{
			Int newIndex = ExpandMenuList->GetBrowseIndex() - 1;
			if (newIndex < 0)
			{
				newIndex = ExpandMenuList->ReadList().size() - 1;
			}

			ExpandMenuList->SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Down)
		{
			Int newIndex = ExpandMenuList->GetBrowseIndex() + 1;
			if (newIndex >= ExpandMenuList->ReadList().size())
			{
				newIndex = 0;
			}

			ExpandMenuList->SetBrowseIndex(newIndex);
			return true;
		}
	}

	return false;
}

bool DropdownComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	if (!IsEnabled())
	{
		return false;
	}

	if (keyEvent.text.unicode == '\r' || //Don't conflict with CaptureFocusInput (The SetSelectedItem later in this function would cause the dropdown to collapse)
			keyEvent.text.unicode == '\t')
	{
		return false;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	if (localEngine->GetElapsedTime() - SearchStringTimestamp > SearchStringClearTime)
	{
		SearchString = DString::EmptyString;
	}

	DString newEntry = DString(sf::String(keyEvent.text.unicode));

	SearchString += newEntry;
	SearchStringTimestamp = localEngine->GetElapsedTime();

	size_t bestIndex = 0;
	Int mostMatches = 1;

	for (size_t i = 0; i < ExpandMenuList->ReadList().size(); i++)
	{
		Int numMatches = ExpandMenuList->ReadList().at(i)->GetLabelText().FindFirstMismatchingIdx(SearchString, DString::CC_IgnoreCase);

		if (numMatches == INT_INDEX_NONE) //Found a matching string
		{
			bestIndex = i;
			break;
		}

		if (Int(numMatches) > mostMatches)
		{
			mostMatches = numMatches;
			bestIndex = i;
		}
	}

	Int selectedIdx = GetSelectedItemByIdx();
	if (bestIndex != selectedIdx.ToUnsignedInt())
	{
		SetSelectedItemByIdx(bestIndex);
	}

	return true;
}

Vector2 DropdownComponent::GetMinimumSize () const
{
	Vector2 result(ReadSize());

	result.Y = ItemSelectionHeight;

	return result;
}

void DropdownComponent::ExecuteInput (const sf::Event& evnt)
{
	Super::ExecuteInput(evnt);

	if (IsExpanded() && evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		Collapse();
	}
}

bool DropdownComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	DetectedMouse = mouse;

	if (eventType != sf::Event::MouseButtonPressed || sfmlEvent.button != sf::Mouse::Left || IsExpanded() || !IsEnabled())
	{
		return false;
	}

	//Expose the list menu if the user clicked over the selected item frame
	if (SelectedObjBackground.IsValid() && SelectedObjBackground->IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y))))
	{
		Expand();
		return true;
	}

	return false;
}

void DropdownComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	ButtonTextureCollapsed = GuiTheme::GetGuiTheme()->DropdownCollapsedButton;
	ButtonTextureExpanded = GuiTheme::GetGuiTheme()->DropdownExpandedButton;

	InitializeSelectedObjBackground();
	InitializeExpandButton();
	InitializeSelectedItemLabel();
	InitializeExpandMenu();
}

void DropdownComponent::Destroyed ()
{
	ClearList(); //Need to deallocate the list of pointers

	if (bExpanded && ExpandHolder != nullptr)
	{
		ExpandHolder->HideDropdown(this);
	}

	if (ExpandMenu != nullptr)
	{
		if (ExpandMenuScrollbar != nullptr && ExpandMenuScrollbar->GetViewedObject() == ExpandMenu)
		{
			//Expand menu is already destroyed. No need to clear it a second time.
			ExpandMenuScrollbar->bDestroyViewedObjectOnDestruction = false;
		}

		ExpandMenu->Destroy();
		ExpandMenu = nullptr;
	}

	Super::Destroyed();
}

void DropdownComponent::Expand ()
{
	if (ExpandMenuList.IsNullptr() || ExpandMenuScrollbar.IsNullptr())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s cannot be expanded since it does not have a ListBoxComponent."), ToString());
		return;
	}

	if (ContainerUtils::IsEmpty(ExpandMenuList->ReadList()))
	{
		return; //There's nothing to expand.
	}

	bExpanded = true;
	ExpandMenuList->SetBrowseIndex(0);
	ExpandMenuScrollbar->SetVisibility(true);

	if (bShowExpandMenuOnTop)
	{
		ExpandHolder = GuiUtils::FindEntityInSameWindowAs<DropdownGuiEntity>(this);
		if (ExpandHolder == nullptr && DetectedMouse != nullptr)
		{
			ExpandHolder = GuiUtils::FindEntityInSameWindowAs<DropdownGuiEntity>(DetectedMouse.Get());
		}

		if (ExpandHolder != nullptr)
		{
			ExpandHolder->DisplayDropdown(this);
		}

		if (ExpandButton != nullptr && ExpandButton->GetBackground() != nullptr && ButtonTextureExpanded != nullptr)
		{
			ExpandButton->GetBackground()->SetCenterTexture(ButtonTextureExpanded.Get());
		}
	}

	if (SearchBar != nullptr)
	{
		ExpandMenuList->SetFilter(DString::EmptyString);
		SearchBar->SetText(DString::EmptyString);
		SearchBar->SetVisibility(true);
		SearchBar->SetCursorPosition(0);

		if (SelectedItemLabel != nullptr)
		{
			SelectedItemLabel->SetVisibility(false);
		}
	}

	if (Engine* localEngine = Engine::FindEngine())
	{
		ExpandTimestamp = localEngine->GetElapsedTime();
	}

	if (OnExpanded.IsBounded())
	{
		OnExpanded();
	}
}

void DropdownComponent::Collapse ()
{
	if (ExpandMenuList.IsNullptr() || ExpandMenuScrollbar.IsNullptr())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s cannot be collapsed since it does not have a ListBoxComponent."), ToString());
		return;
	}

	bExpanded = false;
	ExpandMenuList->SetBrowseIndex(INT_INDEX_NONE);
	ExpandMenuScrollbar->SetVisibility(false);

	if (bShowExpandMenuOnTop && ExpandHolder != nullptr)
	{
		ExpandHolder->HideDropdown(this);
		ExpandHolder = nullptr;
	}

	if (ExpandButton != nullptr && ExpandButton->GetBackground() != nullptr && ButtonTextureCollapsed != nullptr)
	{
		ExpandButton->GetBackground()->SetCenterTexture(ButtonTextureCollapsed.Get());
	}

	if (SearchBar != nullptr)
	{
		SearchBar->SetCursorPosition(-1);
		SearchBar->SetVisibility(false);

		if (SelectedItemLabel != nullptr)
		{
			SelectedItemLabel->SetVisibility(true);
		}
	}

	if (OnCollapsed.IsBounded())
	{
		OnCollapsed();
	}
}

bool DropdownComponent::IsExpanded () const
{
	return bExpanded;
}

Float DropdownComponent::GetCollapsedHeight () const
{
	if (SelectedObjBackground.IsNullptr())
	{
		return 0.f;
	}

	return SelectedObjBackground->ReadCachedAbsSize().Y;
}

void DropdownComponent::SetSelectedItemByIdx (Int itemIndex)
{
	if (ExpandMenuList != nullptr)
	{
		//The HandleExpandItemSelected callback will update the dropdown selection if the ExpandMenuList changed selections.
		ExpandMenuList->ReplaceItemSelectionByIdx(itemIndex);
	}
}

void DropdownComponent::SetNoSelectedItemText (const DString& newNoSelectedItemText)
{
	NoSelectedItemText = newNoSelectedItemText;

	Int selectedIdx = GetSelectedItemByIdx();
	if (selectedIdx == INDEX_NONE && SelectedItemLabel.IsValid())
	{
		SelectedItemLabel->SetText(NoSelectedItemText);
	}
}

void DropdownComponent::ClearList ()
{
	if (ExpandMenuList.IsValid())
	{
		ExpandMenuList->ClearList();
	}
}

Int DropdownComponent::GetSelectedItemByIdx () const
{
	if (ExpandMenuList != nullptr)
	{
		const std::vector<Int>& selectedItems = ExpandMenuList->ReadSelectedItems();
		if (selectedItems.size() > 0)
		{
			return selectedItems.at(0);
		}
	}

	return INDEX_NONE;
}

void DropdownComponent::SetEnableSearchBar (bool bEnabled)
{
	if ((SearchBar != nullptr) == bEnabled)
	{
		//If (SearchBar != nullptr && bEnabled) || (SearchBar == nullptr && !bEnabled)
		return; //SearchBar is already in the correct state.
	}

	if (!bEnabled)
	{
		SearchBar->Destroy();
		FilterExpandList();

		//Ensure the item label is visible just in case this function was called while this dropdown is expanded (since this component is hidden while the search bar is visible).
		if (SelectedItemLabel != nullptr)
		{
			SelectedItemLabel->SetVisibility(false);
		}

		return;
	}

	if (SelectedObjBackground == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to setup Dropdown Component's search bar since its SelectedObjBackground is destroyed."));
		return;
	}

	SearchBar = TextFieldComponent::CreateObject();
	if (SelectedObjBackground->AddComponent(SearchBar))
	{
		SearchBar->SetVisibility(false);
		SearchBar->SetWrapText(false);
		SearchBar->bAutoSelect = true;

		//add a bit of a left margin to prevent the blinking cursor to appear right on the border.
		SearchBar->SetAnchorLeft(5.f);

		SearchBar->OnEdit = SDFUNCTION_1PARAM(this, DropdownComponent, HandleSearchEdit, bool, const sf::Event&);
		SearchBar->OnReturn = SDFUNCTION_1PARAM(this, DropdownComponent, HandleSearchReturn, void, TextFieldComponent*);
	}
}

void DropdownComponent::SetItemSelectionHeight (Float newItemSelectionHeight)
{
	ItemSelectionHeight = newItemSelectionHeight;
	if (ExpandButton.IsValid())
	{
		ExpandButton->SetSize(Vector2(ExpandButton->ReadSize().X, ItemSelectionHeight));
	}

	if (SelectedObjBackground.IsValid())
	{
		SelectedObjBackground->SetSize(Vector2(SelectedObjBackground->ReadSize().X, ItemSelectionHeight));
	}

	if (ExpandMenuScrollbar.IsValid())
	{
		ExpandMenuScrollbar->SetAnchorTop(ItemSelectionHeight);
	}
}

void DropdownComponent::SetEnabled (bool newEnabled)
{
	if (bEnabled == newEnabled)
	{
		return;
	}

	bEnabled = newEnabled;
	if (ExpandButton != nullptr)
	{
		ExpandButton->SetEnabled(bEnabled);
	}
}

void DropdownComponent::SetShowExpandMenuOnTop (bool newShowExpandMenuOnTop)
{
	if (IsExpanded())
	{
		Collapse();
	}

	bShowExpandMenuOnTop = newShowExpandMenuOnTop;
}

void DropdownComponent::SetDragSelectThreshold (Float newDragSelectThreshold)
{
	DragSelectThreshold = newDragSelectThreshold;
}

void DropdownComponent::SetExpandMenu (ListBoxComponent* newExpandMenuList)
{
	if (ExpandMenuList.IsValid())
	{
		ExpandMenuList->Destroy();
	}

	if (newExpandMenuList != nullptr && AddComponent(newExpandMenuList))
	{
		ExpandMenuList = newExpandMenuList;
		ExpandMenuList->OnItemSelected = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemSelected, void, Int);
		ExpandMenuList->OnItemClicked = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemClicked, void, Int);
	}
}

void DropdownComponent::SetExpandButton (ButtonComponent* newExpandButton)
{
	if (ExpandButton.IsValid())
	{
		ExpandButton->Destroy();
	}

	if (newExpandButton != nullptr && AddComponent(newExpandButton))
	{
		ExpandButton = newExpandButton;
		ExpandButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandButtonClicked, void, ButtonComponent*));
	}
}

void DropdownComponent::SetButtonTextureCollapsed (Texture* newButtonTextureCollapsed)
{
	ButtonTextureCollapsed = newButtonTextureCollapsed;
	if (ExpandButton != nullptr && ExpandButton->GetBackground() != nullptr && !bExpanded)
	{
		ExpandButton->GetBackground()->SetCenterTexture(ButtonTextureCollapsed.Get());
	}
}

void DropdownComponent::SetButtonTextureExpanded (Texture* newButtonTextureExpanded)
{
	ButtonTextureExpanded = newButtonTextureExpanded;
	if (ExpandButton != nullptr && ExpandButton->GetBackground() && bExpanded)
	{
		ExpandButton->GetBackground()->SetCenterTexture(ButtonTextureExpanded.Get());
	}
}

void DropdownComponent::SetSelectedObjBackground (FrameComponent* newSelectedObjBackground)
{
	if (SelectedObjBackground.IsValid())
	{
		SelectedObjBackground->Destroy();
	}

	if (newSelectedObjBackground != nullptr && AddComponent(newSelectedObjBackground))
	{
		SelectedObjBackground = newSelectedObjBackground;
	}
}

void DropdownComponent::SetSelectedItemLabel (LabelComponent* newSelectedItemLabel)
{
	if (SelectedItemLabel.IsValid())
	{
		SelectedItemLabel->Destroy();
	}

	if (newSelectedItemLabel != nullptr && SelectedObjBackground.IsValid() && SelectedObjBackground->AddComponent(newSelectedItemLabel))
	{
		SelectedItemLabel = newSelectedItemLabel;
	}
}

GuiComponent* DropdownComponent::GetExpandComponent () const
{
	if (ExpandMenuScrollbar != nullptr)
	{
		return ExpandMenuScrollbar.Get();
	}

	return ExpandMenuList.Get();
}

DString DropdownComponent::GetNoSelectedItemText () const
{
	return NoSelectedItemText;
}

ButtonComponent* DropdownComponent::GetExpandButton () const
{
	return ExpandButton.Get();
}

Texture* DropdownComponent::GetButtonTextureCollapsed () const
{
	return ButtonTextureCollapsed.Get();
}

Texture* DropdownComponent::GetButtonTextureExpanded () const
{
	return ButtonTextureExpanded.Get();
}

FrameComponent* DropdownComponent::GetSelectedObjBackground () const
{
	return SelectedObjBackground.Get();
}

LabelComponent* DropdownComponent::GetSelectedItemLabel () const
{
	return SelectedItemLabel.Get();
}

TextFieldComponent* DropdownComponent::GetSearchBar () const
{
	return SearchBar.Get();
}

void DropdownComponent::InitializeExpandButton ()
{
	ExpandButton = ButtonComponent::CreateObject();
	if (!AddComponent(ExpandButton))
	{
		return;
	}

	ExpandButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
	if (ExpandButton->GetBackground() != nullptr)
	{
		ExpandButton->GetBackground()->SetCenterTexture(ButtonTextureCollapsed.Get());
	}

	Vector2 buttonSize = Vector2(0.1f, ItemSelectionHeight);
	ExpandButton->SetSize(buttonSize);
	ExpandButton->SetAnchorTop(0.f);
	ExpandButton->SetAnchorRight(0.f);

	if (ExpandButton->GetCaptionComponent() != nullptr)
	{
		ExpandButton->GetCaptionComponent()->Destroy();
	}

	ExpandButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandButtonClicked, void, ButtonComponent*));
}

void DropdownComponent::InitializeSelectedObjBackground ()
{
	SelectedObjBackground = FrameComponent::CreateObject();
	if (!AddComponent(SelectedObjBackground))
	{
		return;
	}

	SelectedObjBackground->SetLockedFrame(true);
	SelectedObjBackground->SetSize(0.9f, ItemSelectionHeight);
	SelectedObjBackground->SetAnchorTop(0.f);
	SelectedObjBackground->SetAnchorLeft(0.f);
}

void DropdownComponent::InitializeSelectedItemLabel ()
{
	CHECK(SelectedObjBackground.IsValid() && ExpandButton.IsValid())

	SelectedItemLabel = LabelComponent::CreateObject();
	if (!SelectedObjBackground->AddComponent(SelectedItemLabel))
	{
		return;
	}

	SelectedItemLabel->SetAutoRefresh(false);
	SelectedItemLabel->SetClampText(true);
	SelectedItemLabel->SetWrapText(false);
	SelectedItemLabel->SetSize(Vector2(1.f, 1.f));
	SelectedItemLabel->SetVerticalAlignment(LabelComponent::VA_Center);
	SelectedItemLabel->SetText(NoSelectedItemText);
	SelectedItemLabel->SetAutoRefresh(true);
}

void DropdownComponent::InitializeExpandMenu ()
{
	ExpandMenuScrollbar = ScrollbarComponent::CreateObject();
	if (AddComponent(ExpandMenuScrollbar))
	{
		ExpandMenuScrollbar->SetSize(1.f, 1.f);
		ExpandMenuScrollbar->SetAnchorTop(ItemSelectionHeight);
		ExpandMenuScrollbar->SetAnchorBottom(0.f);
		ExpandMenuScrollbar->SetHideControlsWhenFull(true);
		ExpandMenuScrollbar->SetZoomEnabled(false);
		ExpandMenuScrollbar->SetVisibility(IsExpanded());
		if (ExpandMenuScrollbar->GetFrameTexture() != nullptr)
		{
			ExpandMenuScrollbar->GetFrameTexture()->ResetColor = Color::INVISIBLE;
		}

		ExpandMenu = GuiEntity::CreateObject();
		ExpandMenu->SetAutoSizeVertical(true);
		ExpandMenu->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));

		ExpandMenuList = ListBoxComponent::CreateObject();
		if (ExpandMenu->AddComponent(ExpandMenuList))
		{
			ExpandMenuList->SetAutoDeselect(true);
			ExpandMenuList->SetMinNumSelected(1); //Prevent users from deselecting the current option.
			ExpandMenuList->OnItemSelected = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemSelected, void, Int);
			ExpandMenuList->OnItemClicked = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemClicked, void, Int); //Allows for the DropdownComponent to collapse even if the user selected the same item.

			ExpandMenuScrollbar->SetViewedObject(ExpandMenu);
		}
	}
}

void DropdownComponent::FilterExpandList ()
{
	DString filter = DString::EmptyString;
	if (SearchBar != nullptr)
	{
		filter = SearchBar->GetContent();
	}

	if (ExpandMenuList != nullptr)
	{
		ExpandMenuList->SetFilter(filter);
	}
}

void DropdownComponent::UpdateSelectionText (size_t selectedIdx)
{
	if (SelectedItemLabel != nullptr && ExpandMenuList != nullptr)
	{
		if (selectedIdx > ExpandMenuList->ReadList().size())
		{
			SelectedItemLabel->SetText(NoSelectedItemText);
		}
		else
		{
			SelectedItemLabel->SetText(ExpandMenuList->ReadList().at(selectedIdx)->GetLabelText());
		}
	}
}

void DropdownComponent::HandleExpandButtonClicked (ButtonComponent* uiComponent)
{
	(bExpanded) ? Collapse() : Expand();
}

void DropdownComponent::HandleExpandItemSelected (Int selectedItemIdx)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	if (localEngine->GetElapsedTime() - ExpandTimestamp < DragSelectThreshold)
	{
		//Ignore this release event since the user recently expanded the menu.
		if (ExpandMenuList != nullptr && ContainerUtils::IsValidIndex(ExpandMenuList->ReadList(), PrevSelectIdx) && selectedItemIdx != PrevSelectIdx)
		{
			std::vector<Int> oldItems{PrevSelectIdx};
			ExpandMenuList->SelectItemsByIdx(oldItems);
		}

		return;
	}

	PrevSelectIdx = selectedItemIdx;
	UpdateSelectionText(selectedItemIdx.ToUnsignedInt());

	if (OnOptionSelected.IsBounded())
	{
		OnOptionSelected(this, selectedItemIdx);
	}
}

void DropdownComponent::HandleExpandItemClicked (Int itemIdx)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	if (localEngine->GetElapsedTime() - ExpandTimestamp >= DragSelectThreshold)
	{
		Collapse();
	}
}

bool DropdownComponent::HandleSearchEdit (const sf::Event& sfmlEvent)
{
	FilterExpandList();
	return false;
}

void DropdownComponent::HandleSearchReturn (TextFieldComponent* textComp)
{
	if (ExpandMenuList != nullptr)
	{
		const std::vector<size_t>& filteredList = ExpandMenuList->ReadDisplayedListIndices();
		if (ContainerUtils::IsEmpty(filteredList))
		{
			return;
		}

		std::vector<Int> newSelection{filteredList.at(0)};
		ExpandMenuList->SelectItemsByIdx(newSelection);
	}
}
SD_END