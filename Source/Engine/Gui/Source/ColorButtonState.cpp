/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorButtonState.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "ColorButtonState.h"
#include "FrameComponent.h"

IMPLEMENT_CLASS(SD::ColorButtonState, SD::ButtonStateComponent)
SD_BEGIN

void ColorButtonState::CopyPropertiesFrom (const CopiableObjectInterface* cpy)
{
	const ColorButtonState* castCpyObj = dynamic_cast<const ColorButtonState*>(cpy);
	if (castCpyObj != nullptr)
	{
		DefaultColor = castCpyObj->DefaultColor;
		DisabledColor = castCpyObj->DisabledColor;
		HoverColor = castCpyObj->HoverColor;
		DownColor = castCpyObj->DownColor;
		RefreshState();
	}
}

void ColorButtonState::SetDefaultAppearance ()
{
	Super::SetDefaultAppearance();

	SetButtonColor(DefaultColor);
}

void ColorButtonState::SetDisableAppearance ()
{
	Super::SetDisableAppearance();

	SetButtonColor(DisabledColor);
}

void ColorButtonState::SetHoverAppearance ()
{
	Super::SetHoverAppearance();

	SetButtonColor(HoverColor);
}

void ColorButtonState::SetDownAppearance ()
{
	Super::SetDownAppearance();

	SetButtonColor(DownColor);
}

void ColorButtonState::SetDefaultColor (Color newDefaultColor)
{
	DefaultColor = newDefaultColor;

	RefreshState();
}

void ColorButtonState::SetDisabledColor (Color newDisabledColor)
{
	DisabledColor = newDisabledColor;

	RefreshState();
}

void ColorButtonState::SetHoverColor (Color newHoverColor)
{
	HoverColor = newHoverColor;

	RefreshState();
}

void ColorButtonState::SetDownColor (Color newDownColor)
{
	DownColor = newDownColor;

	RefreshState();
}

void ColorButtonState::SetButtonColor (Color newColorFill)
{
	if (OwningButton != nullptr)
	{
		if (FrameComponent* buttonBackground = OwningButton->GetBackground())
		{
			buttonBackground->SetCenterColor(newColorFill);
		}
	}
}

SD_END