/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScrollbarComponent.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "FakeMouse.h"
#include "FrameComponent.h"
#include "GuiDrawLayer.h"
#include "LabelComponent.h"
#include "ScrollableInterface.h"
#include "ScrollbarComponent.h"
#include "SingleSpriteButtonState.h"

IMPLEMENT_CLASS(SD::ScrollbarComponent, SD::GuiComponent)
SD_BEGIN

void ScrollbarComponent::InitProps ()
{
	Super::InitProps();

	ScrollToPositionTime = 0.25f;
	InitialPanDistance = 24.f;
	ShiftSpeedMultiplier = 2.f;
	AltSpeedMultiplier = 0.5f;
	ScrollSpeed = 450.f;
	TravelTrackSpeed = 1500.f;
	WheelSpeedAccumulationRate = 24.f;
	AnchorPanSpeed.Min = 4.f;
	AnchorPanSpeed.Max = 2048.f;
	AnchorDistanceLimits.Min = 8.f;
	AnchorDistanceLimits.Max = 512.f;
	MinToggleTime = 0.25f;
	HoldToDragTime = 0.5f;
	ZoomRange.Min = 0.1f;
	ZoomRange.Max = 10.f;
	ZoomSnapValues = {0.25f, 0.5f, 0.75f};
	ZoomSnapTolerance = 0.1f;
	ZoomSpeed = 0.25f;
	ZoomSensitivity = 0.1f;
	bDestroyViewedObjectOnDestruction = true;

	ViewedObject = nullptr;
	FrameTexture = nullptr;
	bFreezeTextureSize = false;
	FrameCamera = nullptr;
	FrameSpriteTransform = nullptr;
	FrameSprite = nullptr;
	ScrollUpButton = nullptr;
	ScrollDownButton = nullptr;
	ScrollLeftButton = nullptr;
	ScrollRightButton = nullptr;
	VerticalTrackTransform = nullptr;
	HorizontalTrackTransform = nullptr;
	VerticalTrack = nullptr;
	HorizontalTrack = nullptr;
	TravelTrackTransform = nullptr;
	TravelTrack = nullptr;
	VerticalThumbTransform = nullptr;
	HorizontalThumbTransform = nullptr;
	VerticalThumb = nullptr;
	HorizontalThumb = nullptr;
	MiddleMouseAnchorTransform = nullptr;
	MiddleMouseAnchor = nullptr;
	ZoomButton = nullptr;
	ZoomFeedbackTransform = nullptr;
	ZoomLabel = nullptr;
	Mouse = nullptr;
	Tick = nullptr;

	AnchorPosition = Vector2(-1.f, -1.f);
	AnchoredMouseIcon = nullptr;
	AnchorIconRefreshTimeInterval = 0.1f;
	AnchorIconTime = -1.f;
	StartAnchorTime = 1.f;
	HideControlsWhenFull = false;
	EnabledTrackColor = Color(64, 64, 64, 255);
	EnabledThumbColor = Color(150, 150, 150, 255);
	DisabledThumbColor = Color(48, 48, 48, 255);
	ThumbDragColor = Color(128, 128, 128, 255);
	ZoomEnabled = true;
	HorizontalControlState = CS_Uninitialized;
	VerticalControlState = CS_Uninitialized;
	CameraPanVelocity = Vector2::ZERO_VECTOR;
	CameraDestination = Vector2(-1.f, -1.f);
	DesiredZoom = -1.f;
	TravelTrackDirection = TTD_None;
	OriginalCamPanVelocity = Vector2::ZERO_VECTOR;
	TransformDetectionThreshold = 2.f;

	LastScrollToPositionTime = 0.f;
	ScrollbarThickness = 18.f;
	IsHoldingScrollButton = false;
	VerticalThumbHoldOffset = -1.f;
	HorizontalThumbHoldOffset = -1.f;
	SettingMousePosition = false;
	bUpdateFrameTextureSize = false;

	CamLatestPos = Vector2(-1.f, -1.f);
	SpriteLatestSize = Vector2(-1.f, -1.f);
	ObjLatestSize = Vector2(-1.f, -1.f);
	CamLatestZoom = -1.f;
}

void ScrollbarComponent::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleTick, void, Float));
	}
}

void ScrollbarComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarComponent* scrollbarTemplate = dynamic_cast<const ScrollbarComponent*>(objTemplate);
	if (scrollbarTemplate != nullptr)
	{
		ScrollToPositionTime = scrollbarTemplate->ScrollToPositionTime;
		InitialPanDistance = scrollbarTemplate->InitialPanDistance;
		ShiftSpeedMultiplier = scrollbarTemplate->ShiftSpeedMultiplier;
		AltSpeedMultiplier = scrollbarTemplate->AltSpeedMultiplier;
		ScrollSpeed = scrollbarTemplate->ScrollSpeed;
		TravelTrackSpeed = scrollbarTemplate->TravelTrackSpeed;
		WheelSpeedAccumulationRate = scrollbarTemplate->WheelSpeedAccumulationRate;
		AnchorPanSpeed = scrollbarTemplate->AnchorPanSpeed;
		MinToggleTime = scrollbarTemplate->MinToggleTime;
		HoldToDragTime = scrollbarTemplate->HoldToDragTime;
		ZoomRange = scrollbarTemplate->ZoomRange;
		ZoomSnapValues = scrollbarTemplate->ZoomSnapValues;
		ZoomSnapTolerance = scrollbarTemplate->ZoomSnapTolerance;
		ZoomSpeed = scrollbarTemplate->ZoomSpeed;
		ZoomSensitivity = scrollbarTemplate->ZoomSensitivity;
		bDestroyViewedObjectOnDestruction = scrollbarTemplate->bDestroyViewedObjectOnDestruction;

		bool createdObj;
		//Up button
		ScrollUpButton = ReplaceTargetWithObjOfMatchingClass(ScrollUpButton.Get(), scrollbarTemplate->ScrollUpButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollUpButton);
		}

		if (ScrollUpButton.IsValid())
		{
			ScrollUpButton->CopyPropertiesFrom(scrollbarTemplate->ScrollUpButton.Get());
		}

		//Down button
		ScrollDownButton = ReplaceTargetWithObjOfMatchingClass(ScrollDownButton.Get(), scrollbarTemplate->ScrollDownButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollDownButton);
		}

		if (ScrollDownButton.IsValid())
		{
			ScrollDownButton->CopyPropertiesFrom(scrollbarTemplate->ScrollDownButton.Get());
		}

		//Left button
		ScrollLeftButton = ReplaceTargetWithObjOfMatchingClass(ScrollLeftButton.Get(), scrollbarTemplate->ScrollLeftButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollLeftButton);
		}

		if (ScrollLeftButton.IsValid())
		{
			ScrollLeftButton->CopyPropertiesFrom(scrollbarTemplate->ScrollLeftButton.Get());
		}

		//Right button
		ScrollRightButton = ReplaceTargetWithObjOfMatchingClass(ScrollRightButton.Get(), scrollbarTemplate->ScrollRightButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollRightButton);
		}

		if (ScrollRightButton.IsValid())
		{
			ScrollRightButton->CopyPropertiesFrom(scrollbarTemplate->ScrollRightButton.Get());
		}

		//Vertical Track
		VerticalTrack = ReplaceTargetWithObjOfMatchingClass(VerticalTrack.Get(), scrollbarTemplate->VerticalTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(VerticalTrackTransform != nullptr)
			VerticalTrackTransform->AddComponent(VerticalTrack);
		}

		if (VerticalTrack.IsValid())
		{
			VerticalTrack->CopyPropertiesFrom(scrollbarTemplate->VerticalTrack.Get());
		}

		//Horizontal Track
		HorizontalTrack = ReplaceTargetWithObjOfMatchingClass(HorizontalTrack.Get(), scrollbarTemplate->HorizontalTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(HorizontalTrackTransform != nullptr)
			HorizontalTrackTransform->AddComponent(HorizontalTrack);
		}

		if (HorizontalTrack.IsValid())
		{
			HorizontalTrack->CopyPropertiesFrom(scrollbarTemplate->HorizontalTrack.Get());
		}

		//Travel Track
		TravelTrack = ReplaceTargetWithObjOfMatchingClass(TravelTrack.Get(), scrollbarTemplate->TravelTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(TravelTrackTransform != nullptr)
			TravelTrackTransform->AddComponent(TravelTrack);
		}

		if (TravelTrack.IsValid())
		{
			TravelTrack->CopyPropertiesFrom(scrollbarTemplate->TravelTrack.Get());
		}

		//Vertical Thumb
		VerticalThumb = ReplaceTargetWithObjOfMatchingClass(VerticalThumb.Get(), scrollbarTemplate->VerticalThumb.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(VerticalThumbTransform != nullptr)
			VerticalThumbTransform->AddComponent(VerticalThumb);
		}

		if (VerticalThumb.IsValid())
		{
			VerticalThumb->CopyPropertiesFrom(scrollbarTemplate->VerticalThumb.Get());
		}

		//Horizontal Thumb
		HorizontalThumb = ReplaceTargetWithObjOfMatchingClass(HorizontalThumb.Get(), scrollbarTemplate->HorizontalThumb.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(HorizontalThumbTransform != nullptr)
			HorizontalThumbTransform->AddComponent(HorizontalThumb);
		}

		if (HorizontalThumb.IsValid())
		{
			HorizontalThumb->CopyPropertiesFrom(scrollbarTemplate->HorizontalThumb.Get());
		}

		//Middle Mouse Anchor
		MiddleMouseAnchor = ReplaceTargetWithObjOfMatchingClass(MiddleMouseAnchor.Get(), scrollbarTemplate->MiddleMouseAnchor.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(MiddleMouseAnchorTransform != nullptr)
			MiddleMouseAnchorTransform->AddComponent(MiddleMouseAnchor);
		}

		if (MiddleMouseAnchor.IsValid())
		{
			MiddleMouseAnchor->CopyPropertiesFrom(scrollbarTemplate->MiddleMouseAnchor.Get());
		}

		//Zoom Button
		ZoomButton = ReplaceTargetWithObjOfMatchingClass(ZoomButton.Get(), scrollbarTemplate->ZoomButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ZoomButton);
		}

		if (ZoomButton.IsValid())
		{
			ZoomButton->CopyPropertiesFrom(scrollbarTemplate->ZoomButton.Get());
		}

		//Zoom Label
		ZoomLabel = ReplaceTargetWithObjOfMatchingClass(ZoomLabel.Get(), scrollbarTemplate->ZoomLabel.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(ZoomFeedbackTransform != nullptr)
			ZoomFeedbackTransform->AddComponent(ZoomLabel);
		}

		if (ZoomLabel.IsValid())
		{
			ZoomLabel->CopyPropertiesFrom(scrollbarTemplate->ZoomLabel.Get());
		}

		AnchorIconRefreshTimeInterval = scrollbarTemplate->AnchorIconRefreshTimeInterval;
		SetHideControlsWhenFull(scrollbarTemplate->HideControlsWhenFull);
		EnabledTrackColor = scrollbarTemplate->EnabledTrackColor;
		EnabledThumbColor = scrollbarTemplate->EnabledThumbColor;
		DisabledThumbColor = scrollbarTemplate->DisabledThumbColor;
		ThumbDragColor = scrollbarTemplate->ThumbDragColor;
		SetZoomEnabled(scrollbarTemplate->IsZoomEnabled());
		HorizontalControlState = scrollbarTemplate->HorizontalControlState;
		VerticalControlState = scrollbarTemplate->VerticalControlState;
		TransformDetectionThreshold = scrollbarTemplate->TransformDetectionThreshold;

		CamLatestPos = scrollbarTemplate->CamLatestPos;
		SpriteLatestSize = scrollbarTemplate->SpriteLatestSize;
		ObjLatestSize = scrollbarTemplate->ObjLatestSize;
		CamLatestZoom = scrollbarTemplate->CamLatestZoom;
	}
}

void ScrollbarComponent::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	/*
	We don't actually reconstruct the frame texture here since it's very probable this is called in a middle of a render cycle.
	We shouldn't destroy frame textures in a middle of a render cycle. Instead wait until the next Gui Tick to reconstruct it.
	*/
	bUpdateFrameTextureSize = true;
}

void ScrollbarComponent::ExecuteInput (const sf::Event& evnt)
{
	Super::ExecuteInput(evnt);

	//Relay event to ViewedObject
	if (ViewedObject != nullptr)
	{
		ViewedObject->ExecuteScrollableInterfaceInput(evnt);
	}
}

void ScrollbarComponent::ExecuteText (const sf::Event& evnt)
{
	Super::ExecuteText(evnt);

	//Relay event to ViewedObject
	if (ViewedObject != nullptr)
	{
		ViewedObject->ExecuteScrollableInterfaceText(evnt);
	}
}

void ScrollbarComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	Mouse = mouse;
	CHECK(FrameMouse.IsValid())
	FrameMouse->SetOriginalMouse(mouse);

	const Vector2 mousePosition(Float(sfmlEvent.x), Float(sfmlEvent.y));

	//Relay mouse event to ViewedObject
	{
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			const Vector2 frameMousePos = CalcViewedFramePosition(mousePosition);
			FrameMouse->SetPosition(frameMousePos);

			sf::Event::MouseMoveEvent frameMouseMove;
			frameMouseMove.x = frameMousePos.X.ToInt().ToInt32();
			frameMouseMove.y = frameMousePos.Y.ToInt().ToInt32();
			ViewedObject->ExecuteScrollableInterfaceMouseMove(FrameMouse.Get(), frameMouseMove, deltaMove);
		}
		else
		{
			FrameMouse->SetPosition(FakeMouse::OUT_OF_BOUNDS);
		}
	}

	CHECK(FrameCamera.IsValid())
	if (ViewedObject == nullptr)
	{
		return;
	}

	if (VerticalThumbHoldOffset >= 0.f)
	{
		//Vertical thumb is held.  Set the camera's vertical position.
		CHECK(VerticalTrackTransform != nullptr && VerticalThumbTransform != nullptr)

		//Identify the min/max values the camera can be placed vertically.
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);

		//Identify a value between 0 and 1 where the mouse is positioned relative to the vertial bar.
		Float translationAlpha = (mousePosition.Y - VerticalThumbHoldOffset - VerticalTrackTransform->ReadCachedAbsPosition().Y) / (VerticalTrackTransform->ReadCachedAbsSize().Y - VerticalThumbTransform->ReadCachedAbsSize().Y);
		translationAlpha = Utils::Clamp<Float>(translationAlpha, 0.f, 1.f);

		//Compute where the camera should be placed based on the translationAlpha.
		Float vertPos = Utils::Lerp(translationAlpha, minPos.Y, maxPos.Y);

		FrameCamera->SetPosition(FrameCamera->ReadPosition().X, vertPos);
	}
	else if (HorizontalThumbHoldOffset >= 0.f)
	{
		//Horizontal thumb is held.  Set the camera's horizontal position.
		CHECK(HorizontalTrackTransform != nullptr && HorizontalThumbTransform != nullptr)

		//Identify the min/max values the camera can be placed horizontally.
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);

		//Identify a value between 0 and 1 where the mouse is positioned relative to the horizontal bar.
		Float translationAlpha = (mousePosition.X - HorizontalThumbHoldOffset - HorizontalTrackTransform->ReadCachedAbsPosition().X) / (HorizontalTrackTransform->ReadCachedAbsSize().X - HorizontalThumbTransform->ReadCachedAbsSize().X);
		translationAlpha = Utils::Clamp<Float>(translationAlpha, 0.f, 1.f);

		//Compute where the camera should be placed based on the translationAlpha.
		Float horPos = Utils::Lerp(translationAlpha, minPos.X, maxPos.X);

		FrameCamera->SetPosition(horPos, FrameCamera->ReadPosition().Y);
	}

	if (AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
	{
		//The middle mouse panning is active.  Adjust the camera velocity based on relative position to anchor.
		Vector2 relPos = mousePosition - AnchorPosition;

		//Compute X speed
		if (Float::Abs(relPos.X) < AnchorDistanceLimits.Min)
		{
			CameraPanVelocity.X = 0.f;
		}
		else
		{
			//Compute the magnitude
			if (AnchorDistanceLimits.Min != AnchorDistanceLimits.Max)
			{
				Float anchorRatio = Utils::Lerp(Float::Abs(relPos.X) / AnchorDistanceLimits.Difference(), AnchorDistanceLimits.Min, AnchorDistanceLimits.Max);
				anchorRatio = Utils::Min(anchorRatio, AnchorPanSpeed.Max);
				anchorRatio /= AnchorDistanceLimits.Difference();
				CameraPanVelocity.X = Utils::Lerp(anchorRatio, AnchorPanSpeed.Min, AnchorPanSpeed.Max);
			}
			else
			{
				CameraPanVelocity.X = AnchorPanSpeed.Max; //No distance difference between min and max.  Assume max velocity.
			}

			if (relPos.X < 0.f)
			{
				//Reverse direction
				CameraPanVelocity.X *= -1.f;
			}
		}

		//Compute Y speed
		if (Float::Abs(relPos.Y) < AnchorDistanceLimits.Min)
		{
			CameraPanVelocity.Y = 0.f;
		}
		else
		{
			//Compute the magnitude
			if (AnchorDistanceLimits.Min != AnchorDistanceLimits.Max)
			{
				Float anchorRatio = Utils::Lerp(Float::Abs(relPos.Y) / AnchorDistanceLimits.Difference(), AnchorDistanceLimits.Min, AnchorDistanceLimits.Max);
				anchorRatio = Utils::Min(anchorRatio, AnchorPanSpeed.Max);
				anchorRatio /= AnchorDistanceLimits.Difference();
				CameraPanVelocity.Y = Utils::Lerp(anchorRatio, AnchorPanSpeed.Min, AnchorPanSpeed.Max);
			}
			else
			{
				CameraPanVelocity.Y = AnchorPanSpeed.Max; //No distance difference between min and max.  Assume max velocity.
			}

			if (relPos.Y < 0.f)
			{
				//Reverse direction
				CameraPanVelocity.Y *= -1.f;
			}
		}
	}
}

void ScrollbarComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	const Vector2 mousePosition(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
	//Relay mouse event to ViewedObject.
	{
		if (ViewedObject != nullptr)
		{
			Vector2 frameMousePos = CalcViewedFramePosition(mousePosition);

			sf::Event::MouseButtonEvent frameEvent;
			frameEvent.button = sfmlEvent.button;
			frameEvent.x = frameMousePos.X.ToInt().ToInt32();
			frameEvent.y = frameMousePos.Y.ToInt().ToInt32();
			ViewedObject->ExecuteScrollableInterfaceMouseClick(FrameMouse.Get(), frameEvent, eventType);
		}
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		IsHoldingScrollButton = false;

		if (TravelTrackDirection != TTD_None)
		{
			//Release travel track movement
			TravelTrackDirection = TTD_None;
			CameraPanVelocity = Vector2::ZERO_VECTOR;

			if (TravelTrack.IsValid())
			{
				TravelTrack->SetVisibility(false);
			}
		}
		else if (VerticalThumbHoldOffset >= 0.f || HorizontalThumbHoldOffset >= 0.f)
		{
			//Release thumb movement
			VerticalThumbHoldOffset = -1.f;
			HorizontalThumbHoldOffset = -1.f;
			CameraPanVelocity = Vector2::ZERO_VECTOR;

			if (VerticalThumb.IsValid() && VerticalThumbTransform != nullptr)
			{
				VerticalThumb->SolidColor = (VerticalThumbTransform->ReadSize().Y < 1.f) ? EnabledThumbColor : DisabledThumbColor;
			}

			if (HorizontalThumb.IsValid() && HorizontalThumbTransform != nullptr)
			{
				HorizontalThumb->SolidColor = (HorizontalThumbTransform->ReadSize().X < 1.f) ? EnabledThumbColor : DisabledThumbColor;
			}
		}
		else if (AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)

			//Identify if middle mouse should toggle or release.
			if (localEngine->GetElapsedTime() - StartAnchorTime > MinToggleTime)
			{
				//Held long enough.  Release anchor.
				AnchorPosition.X = -1.f;
				AnchorPosition.Y = -1.f;
				MiddleMouseAnchor->SetVisibility(false);

				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}
			else
			{
				//Not held long enough.  It's now a toggle.  User must click middle mouse wheel again to release.
			}
		}
		else if (ZoomFeedbackTransform != nullptr && ZoomFeedbackTransform->IsVisible())
		{
			//Release zoom controls.
			ZoomFeedbackTransform->SetVisibility(false);
		}
	}
}

void ScrollbarComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	Super::ExecuteMouseWheelMove(mouse, sfmlEvent);

	Vector2 mousePosition = Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
	if (ViewedObject != nullptr)
	{
		//Relay mouse event to the ViewedObject
		Vector2 framePosition = CalcViewedFramePosition(mousePosition);

		sf::Event::MouseWheelScrollEvent frameEvent;
		frameEvent.wheel = sfmlEvent.wheel;
		frameEvent.delta = sfmlEvent.delta;
		frameEvent.x = framePosition.X.ToInt().ToInt32();
		frameEvent.y = framePosition.Y.ToInt().ToInt32();
		ViewedObject->ExecuteScrollableInterfaceMouseWheelMove(FrameMouse.Get(), frameEvent);
	}
}

bool ScrollbarComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableInput(evnt))
	{
		return true;
	}

	//Replay event to ViewedObject
	if (ViewedObject != nullptr)
	{
		if (ViewedObject->ExecuteScrollableInterfaceConsumableInput(evnt))
		{
			return true;
		}
	}

	return false;
}

bool ScrollbarComponent::ExecuteConsumableText (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableText(evnt))
	{
		return true;
	}

	//Replay event to ViewedObject
	if (ViewedObject != nullptr)
	{
		if (ViewedObject->ExecuteScrollableInterfaceConsumableText(evnt))
		{
			return true;
		}
	}

	return false;
}

bool ScrollbarComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	Vector2 mousePosition = Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));

	//Relay mouse event to ViewedObject.
	{
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			Vector2 frameMousePos = CalcViewedFramePosition(mousePosition);

			sf::Event::MouseButtonEvent frameEvent;
			frameEvent.button = sfmlEvent.button;
			frameEvent.x = frameMousePos.X.ToInt().ToInt32();
			frameEvent.y = frameMousePos.Y.ToInt().ToInt32();
			if (ViewedObject->ExecuteScrollableInterfaceConsumableMouseClick(FrameMouse.Get(), frameEvent, eventType))
			{
				return true;
			}
		}
	}

	if (!IsWithinBounds(mousePosition))
	{
		return false;
	}

	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonPressed)
	{
		//Thumbs take priority over the track.
		if (VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f && VerticalThumbTransform->IsWithinBounds(mousePosition))
		{
			//User clicked on vertical thumb
			VerticalThumbHoldOffset = mousePosition.Y - VerticalThumbTransform->ReadCachedAbsPosition().Y;
			VerticalThumb->SolidColor = ThumbDragColor;
			CameraDestination = Vector2(-1.f, -1.f); //Interrupt camera destination since mouse is dictating movement.
			CameraPanVelocity = Vector2::ZERO_VECTOR;
			return true;
		}
		else if (HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f && HorizontalThumbTransform->IsWithinBounds(mousePosition))
		{
			//User clicked on horizontal thumb
			HorizontalThumbHoldOffset = mousePosition.X - HorizontalThumbTransform->ReadCachedAbsPosition().X;
			HorizontalThumb->SolidColor = ThumbDragColor;
			CameraDestination = Vector2(-1.f, -1.f); //Interrupt camera destination since mouse is dictating movement.
			CameraPanVelocity = Vector2::ZERO_VECTOR;
			return true;
		}
		//Test if the track is clicked.
		else if (VerticalTrackTransform != nullptr && VerticalThumbTransform != nullptr &&
			VerticalThumbTransform->ReadSize().Y < 1.f && VerticalTrackTransform->IsWithinBounds(mousePosition))
		{
			CHECK(FrameCamera.IsValid())

			//Vertical track is pressed
			TravelTrackDirection = (mousePosition.Y < VerticalThumbTransform->ReadCachedAbsPosition().Y) ? TTD_Up : TTD_Down;
			CameraPanVelocity = Vector2(0.f, TravelTrackSpeed);
			Vector2 initialPanDistMultiplier(0.f, 1.f);
			if (TravelTrackDirection == TTD_Up)
			{
				CameraPanVelocity.Y *= -1.f;
				initialPanDistMultiplier.Y *= -1.f;
			}

			OriginalCamPanVelocity = CameraPanVelocity;
			CameraDestination = FrameCamera->ReadPosition() + (initialPanDistMultiplier * InitialPanDistance);
			ClampCamPosition(OUT CameraDestination);
			TravelTrack->SetVisibility(true);

			//Set the travel track to be over the vertical bar.  The vertical position/scale is computed on Tick.
			TravelTrackTransform->SetPosition(VerticalTrackTransform->ReadPosition().X, TravelTrackTransform->ReadPosition().Y);
			TravelTrackTransform->SetSize(VerticalTrackTransform->ReadCachedAbsSize().X, TravelTrackTransform->ReadSize().Y);
			TravelTrackTransform->SetAnchorRight(0.f);
			TravelTrackTransform->SetAnchorBottom(-1.f);

			return true;
		}
		else if (HorizontalTrackTransform != nullptr && HorizontalThumbTransform != nullptr &&
			HorizontalThumbTransform->ReadSize().X < 1.f && HorizontalTrackTransform->IsWithinBounds(mousePosition))
		{
			CHECK(FrameCamera.IsValid())

			//Horizontal track is pressed
			TravelTrackDirection = (mousePosition.X < HorizontalThumbTransform->ReadCachedAbsPosition().X) ? TTD_Left : TTD_Right;
			CameraPanVelocity = Vector2(TravelTrackSpeed, 0.f);
			Vector2 initialPanDistMultiplier(1.f, 0.f);
			if (TravelTrackDirection == TTD_Left)
			{
				CameraPanVelocity.X *= -1.f;
				initialPanDistMultiplier.X *= -1.f;
			}

			OriginalCamPanVelocity = CameraPanVelocity;
			CameraDestination = FrameCamera->ReadPosition() + (initialPanDistMultiplier * InitialPanDistance);
			ClampCamPosition(OUT CameraDestination);
			TravelTrack->SetVisibility(true);

			//Set the travel track to be over the horizontal bar.  The horizontal position/scale is computed on Tick.
			TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, HorizontalTrackTransform->ReadPosition().Y);
			TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, HorizontalTrackTransform->ReadCachedAbsSize().Y);
			TravelTrackTransform->SetAnchorRight(-1.f);
			TravelTrackTransform->SetAnchorBottom(0.f);

			return true;
		}
	}

	if (sfmlEvent.button == sf::Mouse::Middle && eventType == sf::Event::MouseButtonPressed && IsScrollingEnabled())
	{
		//If not already anchored
		if (AnchorPosition.X < 0.f && AnchorPosition.Y < 0.f && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			//Set mouse anchor
			AnchorPosition = mousePosition;

			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)
			StartAnchorTime = localEngine->GetElapsedTime();

			bool isAlreadyRegistered = mouse->IsIconOverrideRegistered(SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			if (!isAlreadyRegistered)
			{
				//Update mouse icon
				AnchoredMouseIcon = CalculateAnchoredMouseIcon(mousePosition);
				AnchorIconTime = localEngine->GetElapsedTime();
				mouse->PushMouseIconOverride(AnchoredMouseIcon.Get(), SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			}

			if (MiddleMouseAnchor.IsValid())
			{
				MiddleMouseAnchor->SetVisibility(true);
			}

			if (MiddleMouseAnchorTransform != nullptr)
			{
				MiddleMouseAnchorTransform->SetPosition(mousePosition - ReadCachedAbsPosition());

				if (!isAlreadyRegistered)
				{
					OriginalMouseSize = mouse->GetSize();
					mouse->SetSize(MiddleMouseAnchorTransform->ReadSize());
				}
			}

			return true;
		}
	}

	return false;
}

bool ScrollbarComponent::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::ExecuteConsumableMouseWheelMove(mouse, sfmlEvent))
	{
		return true;
	}

	Vector2 mousePosition = Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y));
	//Relay mouse event to ViewedObject.
	{
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			//Relay mouse event to the ViewedObject
			Vector2 framePosition = CalcViewedFramePosition(mousePosition);

			sf::Event::MouseWheelScrollEvent frameEvent;
			frameEvent.wheel = sfmlEvent.wheel;
			frameEvent.delta = sfmlEvent.delta;
			frameEvent.x = framePosition.X.ToInt().ToInt32();
			frameEvent.y = framePosition.Y.ToInt().ToInt32();
			if (ViewedObject->ExecuteScrollableInterfaceConsumableMouseWheelMove(FrameMouse.Get(), frameEvent))
			{
				return true;
			}
		}
	}

	if (FrameSpriteTransform.IsNullptr() || !FrameSpriteTransform->IsWithinBounds(mousePosition))
	{
		//Not scrolling over the frame
		return false;
	}

	if (FrameCamera.IsNullptr())
	{
		return false;
	}

	if (VerticalThumbHoldOffset >= 0.f || HorizontalThumbHoldOffset >= 0.f)
	{
		//User is dragging the thumbs.  Ignore mouse wheel, but still consume the event.
		return true;
	}

	Float sizeYRatio = (ObjLatestSize.Y / SpriteLatestSize.Y);
	if (sizeYRatio <= 1.f)
	{
		//There's nothing to scroll
		return false;
	}

	//Process mouse wheel input
	if (InputBroadcaster::GetCtrlHeld() && ZoomEnabled) //zoom the camera
	{
		bool zoomingIn = (sfmlEvent.delta > 0);
		if (DesiredZoom > 0.f)
		{
			//Check if the current movement should be interrupted
			bool cameraZoomingIn = (FrameCamera->GetZoom() > DesiredZoom);
			if (cameraZoomingIn != zoomingIn)
			{
				DesiredZoom = FrameCamera->GetZoom();
			}
		}
		else
		{
			DesiredZoom = FrameCamera->GetZoom();
		}

		Float multiplier = (zoomingIn) ? 1.f : -1.f;
		DesiredZoom += (sqrt(DesiredZoom.Value) * ZoomSensitivity * multiplier);
	}
	else //Pan the camera
	{
		bool shouldResetMomentum = (CameraPanVelocity == Vector2::ZERO_VECTOR); //Reset if not moving
		if (CameraPanVelocity != Vector2::ZERO_VECTOR) //If moving
		{
			//Identify if we're changing directions
			Vector2 desiredVelocity(0.f, -sfmlEvent.delta * ScrollSpeed);
			shouldResetMomentum = (CameraPanVelocity.Dot(desiredVelocity) < 0.f);
		}

		if (shouldResetMomentum) //If not moving or changing directions
		{
			//Interrupt current movement and reset velocity/destination
			CameraPanVelocity = Vector2(0.f, -sfmlEvent.delta * ScrollSpeed);
			const Vector2 deltaMove(0.f, -sfmlEvent.delta * (InitialPanDistance / FrameCamera->GetZoom()));
			CameraDestination = FrameCamera->ReadPosition() + deltaMove;
			ClampCamPosition(OUT CameraDestination);
		}
		else //Already moving and moving in same direction as mouse wheel input
		{
			//Slightly increase speed and expand destination
			CameraPanVelocity.Y += (WheelSpeedAccumulationRate * -sfmlEvent.delta);
			CameraDestination.Y += (-sfmlEvent.delta * (InitialPanDistance / FrameCamera->GetZoom()));
			ClampCamPosition(OUT CameraDestination);
		}
	}

	return true;
}

Vector2 ScrollbarComponent::FakeMouseToInnerCoordinates (const Vector2& outerMousePos) const
{
	if (FrameCamera != nullptr && FrameSpriteTransform != nullptr)
	{
		Vector2 deltaPos = outerMousePos - FrameSpriteTransform->ReadCachedAbsPosition();
		return deltaPos + (FrameCamera->ReadPosition() - (FrameCamera->GetZoomedExtents() * 0.5f));
	}

	return Vector2::ZERO_VECTOR;
}

Vector2 ScrollbarComponent::FakeMouseToOuterCoordinates (const Vector2& innerMousePos) const
{
	if (FrameCamera != nullptr && FrameSpriteTransform != nullptr)
	{
		Vector2 deltaPos = innerMousePos - (FrameCamera->ReadPosition() - (FrameCamera->GetZoomedExtents() * 0.5f));
		return FrameSpriteTransform->ReadCachedAbsPosition() + deltaPos;
	}

	return Vector2::ZERO_VECTOR;
}

void ScrollbarComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	CHECK(localTheme != nullptr)

	FrameTexture = RenderTexture::CreateObject();

	//Create a very tiny FrameTexture so that it can be initialized immediately after construction. These properties will transfer over to the real frame texture whenever the abs transform is computed. See: ReconstructFrameTexture
	FrameTexture->CreateResource(2, 2, false);

	FrameCamera = PlanarCamera::CreateObject();
	FrameCamera->OnZoomChanged.RegisterHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomChanged, void, Float));

	FrameMouse = FakeMouse::CreateObject();
	FrameMouse->SetMouseOwner(this);

	//Most of the cases the Scrollbar will be rendering another GuiEntity.  Create a GuiDrawLayer.
	//If there's a need to use a non-GuiEntity for the viewed object, then the user can edit the draw layer.
	GuiDrawLayer* guiDrawLayer = GuiDrawLayer::CreateObject();
	guiDrawLayer->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
	FrameTexture->EditDrawLayers().push_back(RenderTarget::SDrawLayerCamera(guiDrawLayer, FrameCamera.Get()));
	FrameTexture->SetDestroyCamerasOnDestruction(true);
	FrameTexture->SetDestroyDrawLayersOnDestruction(true);

	FrameSpriteTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(FrameSpriteTransform))
	{
#if 0
		//Disable taking full component size regardless if it's hiding controls or not.
		//It's too slow to reconstruct the frame texture.
		if (HideControlsWhenFull)
		{
			//Sprite should fill entire component
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(-1.f);
			FrameSpriteTransform->SetAnchorBottom(-1.f);
			FrameSpriteTransform->SetSize(1.f, 1.f);
		}
		else
		{
#endif
			//Set sprite to not hide under scrollbars since the scrollbars are always visible.
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(ScrollbarThickness);
			FrameSpriteTransform->SetAnchorBottom(ScrollbarThickness);
//		}

		FrameSprite = SpriteComponent::CreateObject();
		if (FrameSpriteTransform->AddComponent(FrameSprite))
		{
			FrameSprite->SetSpriteTexture(FrameTexture.Get());
		}
	}

	//Scroll buttons
	{
		ScrollUpButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollUpButton))
		{
			if (ScrollUpButton->GetCaptionComponent() != nullptr)
			{
				ScrollUpButton->GetCaptionComponent()->Destroy();
			}

			ScrollUpButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap top right
			ScrollUpButton->SetAnchorTop(0.f);
			ScrollUpButton->SetAnchorRight(0.f);
			ScrollUpButton->SetEnableFractionScaling(false);
			ScrollUpButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

			ScrollUpButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollUpButtonPressed, void, ButtonComponent*));

			if (FrameComponent* background = ScrollUpButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->ScrollbarUpButton.Get());
			}
		}

		ScrollDownButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollDownButton))
		{
			if (ScrollDownButton->GetCaptionComponent() != nullptr)
			{
				ScrollDownButton->GetCaptionComponent()->Destroy();
			}

			ScrollDownButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom right but leaving enough space for zoom button in corner
			ScrollDownButton->SetAnchorBottom(ScrollbarThickness);
			ScrollDownButton->SetAnchorRight(0.f);
			ScrollDownButton->SetEnableFractionScaling(false);
			ScrollDownButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

			ScrollDownButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollDownButtonPressed, void, ButtonComponent*));

			if (FrameComponent* background = ScrollDownButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->ScrollbarDownButton.Get());
			}
		}

		ScrollLeftButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollLeftButton))
		{
			if (ScrollLeftButton->GetCaptionComponent() != nullptr)
			{
				ScrollLeftButton->GetCaptionComponent()->Destroy();
			}

			ScrollLeftButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom left
			ScrollLeftButton->SetAnchorBottom(0.f);
			ScrollLeftButton->SetAnchorLeft(0.f);
			ScrollLeftButton->SetEnableFractionScaling(false);
			ScrollLeftButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

			ScrollLeftButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollLeftButtonPressed, void, ButtonComponent*));

			if (FrameComponent* background = ScrollLeftButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->ScrollbarLeftButton.Get());
			}
		}

		ScrollRightButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollRightButton))
		{
			if (ScrollRightButton->GetCaptionComponent() != nullptr)
			{
				ScrollRightButton->GetCaptionComponent()->Destroy();
			}

			ScrollRightButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom right but leaving enough space for zoom button in corner
			ScrollRightButton->SetAnchorBottom(0.f);
			ScrollRightButton->SetAnchorRight(ScrollbarThickness);
			ScrollRightButton->SetEnableFractionScaling(false);
			ScrollRightButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

			ScrollRightButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollRightButtonPressed, void, ButtonComponent*));

			if (FrameComponent* background = ScrollRightButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->ScrollbarRightButton.Get());
			}
		}
	}

	//Track related components
	{
		VerticalTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(VerticalTrackTransform))
		{
			VerticalTrackTransform->SetSize(ScrollbarThickness, ScrollbarThickness); //Anchor properties will set the height.
			VerticalTrackTransform->SetAnchorTop(ScrollbarThickness);
			VerticalTrackTransform->SetAnchorRight(0.f);
			VerticalTrackTransform->SetAnchorBottom(ScrollbarThickness * 2.f);
			VerticalTrackTransform->SetEnableFractionScaling(false);
		}

		HorizontalTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(HorizontalTrackTransform))
		{
			HorizontalTrackTransform->SetSize(ScrollbarThickness, ScrollbarThickness); //Anchor properties will set the width.
			HorizontalTrackTransform->SetAnchorLeft(ScrollbarThickness);
			HorizontalTrackTransform->SetAnchorRight(ScrollbarThickness * 2.f);
			HorizontalTrackTransform->SetAnchorBottom(0.f);
			HorizontalTrackTransform->SetEnableFractionScaling(false);
		}

		CHECK(VerticalTrackTransform != nullptr)
		CHECK(HorizontalTrackTransform != nullptr)

		VerticalTrack = ColorRenderComponent::CreateObject();
		if (VerticalTrackTransform->AddComponent(VerticalTrack))
		{
			VerticalTrack->SolidColor = EnabledTrackColor;
		}

		HorizontalTrack = ColorRenderComponent::CreateObject();
		if (HorizontalTrackTransform->AddComponent(HorizontalTrack))
		{
			HorizontalTrack->SolidColor = EnabledTrackColor;
		}

		TravelTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(TravelTrackTransform))
		{
			TravelTrackTransform->SetEnableFractionScaling(false);
			//The travel track transform is computed based on mouse movements and thumb position.
		}

		CHECK(TravelTrackTransform != nullptr)

		TravelTrack = ColorRenderComponent::CreateObject();
		if (TravelTrackTransform->AddComponent(TravelTrack))
		{
			TravelTrack->SetVisibility(false);
			TravelTrack->SolidColor = Color(48, 48, 48, 255);
		}

		VerticalThumbTransform = PlanarTransformComponent::CreateObject();
		if (VerticalTrackTransform->AddComponent(VerticalThumbTransform))
		{
			//Fill the track
			VerticalThumbTransform->SetSize(1.f, 1.f);
			VerticalThumbTransform->SetPosition(0.f, 0.f);
		}

		HorizontalThumbTransform = PlanarTransformComponent::CreateObject();
		if (HorizontalTrackTransform->AddComponent(HorizontalThumbTransform))
		{
			//Fill the track
			HorizontalThumbTransform->SetSize(1.f, 1.f);
			HorizontalThumbTransform->SetPosition(0.f, 0.f);
		}

		CHECK(VerticalThumbTransform != nullptr)
		CHECK(HorizontalThumbTransform != nullptr)

		VerticalThumb = ColorRenderComponent::CreateObject();
		if (VerticalThumbTransform->AddComponent(VerticalThumb))
		{
			VerticalThumb->SolidColor = EnabledThumbColor;
		}

		HorizontalThumb = ColorRenderComponent::CreateObject();
		if (HorizontalThumbTransform->AddComponent(HorizontalThumb))
		{
			HorizontalThumb->SolidColor = EnabledThumbColor;
		}
	}

	MiddleMouseAnchorTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(MiddleMouseAnchorTransform))
	{
		MiddleMouseAnchorTransform->SetSize(32.f, 32.f);
	}

	CHECK(MiddleMouseAnchorTransform != nullptr)
	MiddleMouseAnchor = SpriteComponent::CreateObject();
	if (MiddleMouseAnchorTransform->AddComponent(MiddleMouseAnchor))
	{
		MiddleMouseAnchor->SetVisibility(false);
		MiddleMouseAnchor->EditPivot() = Vector2(0.5f, 0.5f);
		MiddleMouseAnchor->SetSpriteTexture(localTheme->ScrollbarMiddleMouseScrollAnchor.Get());
	}

	//Zoom related components
	{
		ZoomButton = ButtonComponent::CreateObject();
		if (AddComponent(ZoomButton))
		{
			ZoomButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			ZoomButton->SetVisibility(IsZoomEnabled());

			//Snap bottom right
			ZoomButton->SetAnchorRight(0.f);
			ZoomButton->SetAnchorBottom(0.f);
			ZoomButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			if (ZoomButton->GetCaptionComponent() != nullptr && ZoomButton->GetCaptionComponent()->GetRenderComponent() != nullptr)
			{
				ZoomButton->SetCaptionText(FrameCamera->GetZoom().ToFormattedString(1, 2, Float::TM_RoundUp));
				ZoomButton->GetCaptionComponent()->SetVerticalAlignment(LabelComponent::VA_Center);
				ZoomButton->GetCaptionComponent()->SetCharacterSize(8);
				ZoomButton->GetCaptionComponent()->GetRenderComponent()->SetFontColor(sf::Color::Black);
			}

			ZoomButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonPressed, void, ButtonComponent*));
			ZoomButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonReleased, void, ButtonComponent*));
			ZoomButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonRightClickReleased, void, ButtonComponent*));

			if (FrameComponent* background = ZoomButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->ScrollbarZoomButton.Get());
			}
		}

		ZoomFeedbackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(ZoomFeedbackTransform))
		{
			ZoomFeedbackTransform->SetSize(64.f, 64.f);

			//Snap to top left corner of zoom button
			ZoomFeedbackTransform->SetAnchorRight(ScrollbarThickness);
			ZoomFeedbackTransform->SetAnchorBottom(ScrollbarThickness);
			ZoomFeedbackTransform->SetVisibility(false);
		}

		CHECK(ZoomFeedbackTransform != nullptr)

		ZoomLabel = LabelComponent::CreateObject();
		if (ZoomFeedbackTransform->AddComponent(ZoomLabel))
		{
			ZoomLabel->SetAutoRefresh(false);
			ZoomLabel->SetWrapText(false);
			ZoomLabel->SetClampText(true);
			ZoomLabel->SetText(TXT("%s"));
			ZoomLabel->SetVerticalAlignment(LabelComponent::eVerticalAlignment::VA_Center);
			ZoomLabel->SetHorizontalAlignment(LabelComponent::eHorizontalAlignment::HA_Center);
			ZoomLabel->SetAutoRefresh(true);
		}
	}
}

void ScrollbarComponent::Destroyed ()
{
	if (bDestroyViewedObjectOnDestruction)
	{
		DestroyViewedObject();
	}

	if (FrameCamera.IsValid())
	{
		FrameCamera->Destroy();

		if (FrameTexture != nullptr)
		{
			FrameTexture->SetDestroyCamerasOnDestruction(false); //no need to destroy the camera twice
		}
	}

	if (FrameMouse.IsValid())
	{
		FrameMouse->Destroy();
	}

	if (FrameTexture.IsValid())
	{
		FrameTexture->Destroy();
	}

	if (Mouse.IsValid() && AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
	{
		Mouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		Mouse->SetSize(OriginalMouseSize);
	}

	Super::Destroyed();
}

void ScrollbarComponent::DestroyViewedObject ()
{
	if (ViewedObject != nullptr)
	{
		ViewedObject->RemoveScrollableObject();
		ViewedObject = nullptr;
	}
}

bool ScrollbarComponent::IsScrollingEnabled () const
{
	if (VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		return true;
	}

	if (HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		return true;
	}

	return false;
}

void ScrollbarComponent::SetViewedObject (ScrollableInterface* newViewedObject)
{
	if (ViewedObject != nullptr)
	{
		ViewedObject->OnScrollToPosition.ClearFunction();

		//Unregister the ViewedObject from the Scrollbar's DrawLayer
		ViewedObject->UnregisterFromDrawLayer(FrameTexture.Get());
		ViewedObject->ClearScrollbarOwner(this);
	}

	ViewedObject = newViewedObject;
	if (ViewedObject != nullptr)
	{
		ViewedObject->RegisterToDrawLayer(this, FrameTexture.Get());
		ViewedObject->OnScrollToPosition = SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleScrollToPosition, void, const Vector2&, bool);
		ViewedObject->InitializeScrollableInterface(this);
	}
}

void ScrollbarComponent::SetFreezeTextureSize (bool bNewFreezeTextureSize)
{
	bFreezeTextureSize = bNewFreezeTextureSize;
}

void ScrollbarComponent::SetHideControlsWhenFull (bool newHideControlsWhenFull)
{
	HideControlsWhenFull = newHideControlsWhenFull;

#if 0
	//Disable readjusting the frame size since it's very costly to reconstruct the render texture.
	if (FrameSpriteTransform.IsValid())
	{
		if (HideControlsWhenFull)
		{
			//Disable anchoring and set the sprite to fill the entire component (can hide behind bars when invisible)
			FrameSpriteTransform->SetSize(1.f, 1.f);
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(-1.f);
			FrameSpriteTransform->SetAnchorBottom(-1.f);
		}
		else
		{
			//The scroll bars will never hide.  Ensure the sprite is not behind those bars.
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(ScrollbarThickness);
			FrameSpriteTransform->SetAnchorBottom(ScrollbarThickness);
		}
	}
#endif

	//Set all controls to be visible and enabled.  The RefreshScrollButtons will conditionally reset their flags
	if (ScrollUpButton.IsValid())
	{
		ScrollUpButton->SetVisibility(true);
		ScrollUpButton->SetEnabled(true);
	}

	if (ScrollDownButton.IsValid())
	{
		ScrollDownButton->SetVisibility(true);
		ScrollDownButton->SetEnabled(true);
	}

	if (VerticalTrack.IsValid())
	{
		VerticalTrack->SetVisibility(true);
	}

	if (VerticalThumb.IsValid())
	{
		VerticalThumb->SetVisibility(true);
	}

	if (ScrollLeftButton.IsValid())
	{
		ScrollLeftButton->SetVisibility(true);
		ScrollLeftButton->SetEnabled(true);
	}

	if (ScrollRightButton.IsValid())
	{
		ScrollRightButton->SetVisibility(true);
		ScrollRightButton->SetEnabled(true);
	}

	if (HorizontalTrack.IsValid())
	{
		HorizontalTrack->SetVisibility(true);
	}

	if (HorizontalThumb.IsValid())
	{
		HorizontalThumb->SetVisibility(true);
	}

	//Force recompute visibility/enabledness
	HorizontalControlState = CS_Uninitialized;
	VerticalControlState = CS_Uninitialized;
	const Vector2 sizeRatio = (SpriteLatestSize / ObjLatestSize);
	RefreshScrollButtonConditions((sizeRatio.Y < 1.f), (sizeRatio.X < 1.f));
}

void ScrollbarComponent::SetEnabledTrackColor (Color newEnabledTrackColor)
{
	EnabledTrackColor = newEnabledTrackColor;
	if (VerticalTrack.IsValid())
	{
		VerticalTrack->SolidColor = EnabledTrackColor;
	}

	if (HorizontalTrack.IsValid())
	{
		HorizontalTrack->SolidColor = EnabledTrackColor;
	}
}

void ScrollbarComponent::SetEnabledThumbColor (Color newEnabledThumbColor)
{
	EnabledThumbColor = newEnabledThumbColor;

	//If thumb is enabled and not dragged
	if (VerticalThumb.IsValid() && VerticalThumbHoldOffset < 0.f &&
		VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		VerticalThumb->SolidColor = EnabledThumbColor;
	}

	if (HorizontalThumb.IsValid() && HorizontalThumbHoldOffset < 0.f &&
		HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		HorizontalThumb->SolidColor = EnabledThumbColor;
	}
}

void ScrollbarComponent::SetDisabledThumbColor (Color newDisabledThumbColor)
{
	DisabledThumbColor = newDisabledThumbColor;

	//If thumb is disabled
	if (VerticalThumb.IsValid() && VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y >= 1.f)
	{
		VerticalThumb->SolidColor = DisabledThumbColor;
	}

	if (HorizontalThumb.IsValid() && HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X >= 1.f)
	{
		HorizontalThumb->SolidColor = DisabledThumbColor;
	}
}

void ScrollbarComponent::SetThumbDragColor (Color newThumbDragColor)
{
	ThumbDragColor = newThumbDragColor;

	//If thumb is enabled and dragged
	if (VerticalThumb.IsValid() && VerticalThumbHoldOffset >= 0.f &&
		VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		VerticalThumb->SolidColor = ThumbDragColor;
	}

	if (HorizontalThumb.IsValid() && HorizontalThumbHoldOffset >= 0.f &&
		HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		HorizontalThumb->SolidColor = ThumbDragColor;
	}
}

void ScrollbarComponent::SetZoomEnabled (bool newZoomEnabled)
{
	if (newZoomEnabled != ZoomEnabled)
	{
		ZoomEnabled = newZoomEnabled;
		if (ZoomButton.IsValid())
		{
			ZoomButton->SetVisibility(ZoomEnabled);
		}
	}
}

ScrollableInterface* ScrollbarComponent::GetViewedObject () const
{
	return ViewedObject;
}

RenderTexture* ScrollbarComponent::GetFrameTexture () const
{
	return FrameTexture.Get();
}

PlanarCamera* ScrollbarComponent::GetFrameCamera () const
{
	return FrameCamera.Get();
}

SpriteComponent* ScrollbarComponent::GetFrameSprite () const
{
	return FrameSprite.Get();
}

PlanarTransformComponent* ScrollbarComponent::GetFrameSpriteTransform () const
{
	return FrameSpriteTransform.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollUpButton () const
{
	return ScrollUpButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollDownButton () const
{
	return ScrollDownButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollLeftButton () const
{
	return ScrollLeftButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollRightButton () const
{
	return ScrollRightButton.Get();
}

ColorRenderComponent* ScrollbarComponent::GetVerticalTrack () const
{
	return VerticalTrack.Get();
}

ColorRenderComponent* ScrollbarComponent::GetHorizontalTrack () const
{
	return HorizontalTrack.Get();
}

ColorRenderComponent* ScrollbarComponent::GetTravelTrack () const
{
	return TravelTrack.Get();
}

ColorRenderComponent* ScrollbarComponent::GetVerticalThumb () const
{
	return VerticalThumb.Get();
}

ColorRenderComponent* ScrollbarComponent::GetHorizontalThumb () const
{
	return HorizontalThumb.Get();
}

SpriteComponent* ScrollbarComponent::GetMiddleMouseAnchor () const
{
	return MiddleMouseAnchor.Get();
}

ButtonComponent* ScrollbarComponent::GetZoomButton () const
{
	return ZoomButton.Get();
}

LabelComponent* ScrollbarComponent::GetZoomLabel () const
{
	return ZoomLabel.Get();
}

void ScrollbarComponent::UpdateZoom (Float deltaSec)
{
	if (DesiredZoom <= 0.f || FrameCamera.IsNullptr())
	{
		return;
	}

	Float curZoom = FrameCamera->GetZoom();

	//Compute where the camera zoom resides in the x^2 graph.
	Float graphPos = sqrt(FrameCamera->GetZoom().Value);

	Float multiplier = 1.f;
	if (curZoom > DesiredZoom)
	{
		multiplier *= -1.f; //Move left along X-axis instead
	}

	//Move along x-axis
	graphPos += (deltaSec * multiplier);
	if (graphPos <= 0.f)
	{
		DesiredZoom = -1.f; //Stop Zooming
		FrameCamera->SetZoom(ZoomRange.Min);
		return;
	}

	//Identify the new Zoom value based on the x position
	Float newZoom = pow(graphPos.Value, 2);
	if (Float::Abs(DesiredZoom - newZoom) <= ZoomSnapTolerance)
	{
		newZoom = DesiredZoom;
		DesiredZoom = -1.f; //Stop Zooming
	}

	if (!ZoomRange.ContainsValue(newZoom))
	{
		DesiredZoom = -1.f; //Stop Zooming
		newZoom = ZoomRange.GetClampedValue(newZoom);
	}

	FrameCamera->SetZoom(newZoom);
}

Vector2 ScrollbarComponent::CalcViewedFramePosition (const Vector2& absPosition) const
{
	CHECK(FrameSpriteTransform.IsValid() && FrameCamera.IsValid())

	Vector2 frameCoordinates(absPosition);

	//Set coordinates relative to frame's top left corner.  The TopLeft corner is (0,0) in frame space.
	frameCoordinates -= FrameSpriteTransform->ReadCachedAbsPosition();

	frameCoordinates /= FrameCamera->GetZoom();
	frameCoordinates += (FrameCamera->ReadCachedAbsPosition() - (FrameCamera->GetZoomedExtents() * 0.5f));

	return frameCoordinates;
}

void ScrollbarComponent::GetCamPositionLimits (Vector2& outMinPos, Vector2& outMaxPos) const
{
	CHECK(FrameCamera.IsValid() && ViewedObject != nullptr)

	Vector2 viewSize;
	ViewedObject->GetTotalSize(OUT viewSize);

	const Vector2 camHalfExtents = FrameCamera->GetZoomedExtents() * 0.5f;

	outMinPos = camHalfExtents;
	outMaxPos = viewSize - camHalfExtents;
}

void ScrollbarComponent::ClampCamPosition (Vector2& outClampedPosition) const
{
	if (FrameCamera.IsValid() && ViewedObject != nullptr)
	{
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);

		outClampedPosition.ClampInline(minPos, maxPos);
	}
}

void ScrollbarComponent::UpdateTravelTrackVelocity (const Vector2& mousePos)
{
	//Restore velocity in case all of the conditions below fail.
	CameraPanVelocity = OriginalCamPanVelocity;

	//Identify if the mouse cursor is at the oposite direction the travel track is travelling.  If so, stop the camera's movement.
	switch (TravelTrackDirection)
	{
		case(TTD_Up):
			CHECK(VerticalThumbTransform != nullptr)
			//If below vertical thumb's top edge
			if (mousePos.Y > VerticalThumbTransform->ReadCachedAbsPosition().Y)
			{
				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}

			break;

		case(TTD_Down):
			CHECK(VerticalThumbTransform != nullptr)
			//If above vertical thumb's bottom edge
			if (mousePos.Y < VerticalThumbTransform->ReadCachedAbsPosition().Y + VerticalThumbTransform->ReadCachedAbsSize().Y)
			{
				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}

			break;

		case(TTD_Left):
			CHECK(HorizontalThumbTransform != nullptr)
			//If right of horizontal thumb's left edge
			if (mousePos.X > HorizontalThumbTransform->ReadCachedAbsPosition().X)
			{
				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}

			break;

		case(TTD_Right):
			CHECK(HorizontalThumbTransform != nullptr)
			//If left of horizontal thumb's right edge
			if (mousePos.X < HorizontalThumbTransform->ReadCachedAbsPosition().X + HorizontalThumbTransform->ReadCachedAbsSize().X)
			{
				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}

			break;
	}
}

void ScrollbarComponent::CalculateTravelTrackTransform (const Vector2& mousePos)
{
	CHECK(TravelTrackTransform != nullptr)
	Vector2 relativePos(mousePos - ReadCachedAbsPosition());

	//Ensure the position is within scrollable view bounds.
	relativePos.X = Utils::Clamp<Float>(relativePos.X, ScrollbarThickness, ReadCachedAbsSize().X - (ScrollbarThickness * 2.f));
	relativePos.Y = Utils::Clamp<Float>(relativePos.Y, ScrollbarThickness, ReadCachedAbsSize().Y - (ScrollbarThickness * 2.f));

	switch (TravelTrackDirection)
	{
		case(TTD_Up):
		{
			CHECK(VerticalThumbTransform != nullptr)

			//Reference abs coordinates since the thumb position is using scalar values (0-1).
			const Float thumbTopEdge = VerticalThumbTransform->ReadCachedAbsPosition().Y - ReadCachedAbsPosition().Y;
			if (relativePos.Y < thumbTopEdge)
			{
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, thumbTopEdge - relativePos.Y);
				TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, relativePos.Y);
			}
			else
			{
				//Mouse is below the top border while the thumb is moving up.  Hide travel track.
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, 0.f);
			}

			break;
		}
		case(TTD_Down):
		{
			CHECK(VerticalThumbTransform != nullptr)

			//Reference abs coordinates since the thumb position is using scalar values (0-1).
			const Float thumbBottomEdge = VerticalThumbTransform->ReadCachedAbsPosition().Y - ReadCachedAbsPosition().Y + VerticalThumbTransform->ReadCachedAbsSize().Y;
			if (relativePos.Y > thumbBottomEdge)
			{
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, relativePos.Y - thumbBottomEdge);
				TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, thumbBottomEdge);
			}
			else
			{
				//Mouse is above bottom border while the thumb is moving down.  Hide the travel track transform.
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, 0.f);
			}

			break;
		}
		case(TTD_Left):
		{
			CHECK(HorizontalThumbTransform != nullptr)

			const Float thumbLeftEdge = HorizontalThumbTransform->ReadCachedAbsPosition().X - ReadCachedAbsPosition().X;
			if (relativePos.X < thumbLeftEdge)
			{
				TravelTrackTransform->SetSize(thumbLeftEdge - relativePos.X, TravelTrackTransform->ReadSize().Y);
				TravelTrackTransform->SetPosition(relativePos.X, TravelTrackTransform->ReadPosition().Y);
			}
			else
			{
				//Mouse is right of thumb while it's moving left.  Hide travel track.
				TravelTrackTransform->SetSize(0.f, TravelTrackTransform->ReadSize().Y);
			}

			break;
		}
		case(TTD_Right):
		{
			CHECK(HorizontalThumbTransform != nullptr)

			const Float thumbRightEdge = HorizontalThumbTransform->ReadCachedAbsPosition().X - ReadCachedAbsPosition().X + HorizontalThumbTransform->ReadCachedAbsSize().X;
			if (relativePos.X > thumbRightEdge)
			{
				TravelTrackTransform->SetSize(relativePos.X - thumbRightEdge, TravelTrackTransform->ReadSize().Y);
				TravelTrackTransform->SetPosition(thumbRightEdge, TravelTrackTransform->ReadPosition().Y);
			}
			else
			{
				//Mouse is left of thumb while it's moving right.  Hide the travel track.
				TravelTrackTransform->SetSize(0.f, TravelTrackTransform->ReadSize().Y);
			}

			break;
		}

	}
}

void ScrollbarComponent::CalculateThumbTransforms ()
{
	//Check if any changes are detected
	if (FrameCamera.IsNullptr() || FrameSpriteTransform.IsNullptr() || ViewedObject == nullptr)
	{
		return;
	}

	if (FrameCamera->GetViewExtents() == Vector2::ZERO_VECTOR)
	{
		return;
	}

	const std::function<bool(const Vector2&, const Vector2&)> isChangeDetected = [&](const Vector2& a, const Vector2& b)
	{
		return (Float::Abs(a.X - b.X) >= TransformDetectionThreshold ||
			Float::Abs(a.Y - b.Y) >= TransformDetectionThreshold);
	};

	const Vector2& camPos = FrameCamera->ReadCachedAbsPosition();
	const Vector2& spriteSize = FrameSpriteTransform->ReadCachedAbsSize();
	Vector2 objSize;
	ViewedObject->GetTotalSize(OUT objSize);
	if (FrameCamera->GetZoom().IsCloseTo(CamLatestZoom, FLOAT_TOLERANCE) && !isChangeDetected(CamLatestPos, camPos) &&
		!isChangeDetected(SpriteLatestSize, spriteSize) && !isChangeDetected(objSize, ObjLatestSize))
	{
		//Nothing changed.  Don't bother recomputing transforms.
		return;
	}

	//A change is detected.  Update transforms.
	CamLatestPos = camPos;
	SpriteLatestSize = spriteSize;
	ObjLatestSize = objSize;
	CamLatestZoom = FrameCamera->GetZoom();

	//Calculate thumb positions and sizes
	const Vector2 sizeRatio = (FrameCamera->GetZoomedExtents() / objSize);

	//Find top left corner of viewable cam region.  Divide by objSize to get a value between 0 to objSize-regionSize.
	const Vector2 thumbPos = ((camPos - (FrameCamera->GetZoomedExtents() * 0.5f)) / objSize);
	if (VerticalThumbTransform != nullptr)
	{
		VerticalThumbTransform->SetSize(VerticalThumbTransform->ReadSize().X, Utils::Min<Float>(sizeRatio.Y, 1.f));
		VerticalThumbTransform->SetPosition(VerticalThumbTransform->ReadPosition().X, thumbPos.Y);

		//Update colors based on if there's something to scroll or not
		if (VerticalThumb.IsValid() && VerticalThumbHoldOffset < 0.f) //Don't update color if user is dragging thumb.
		{
			VerticalThumb->SolidColor = (sizeRatio.Y >= 1.f) ? DisabledThumbColor : EnabledThumbColor;
		}
	}

	if (HorizontalThumbTransform != nullptr)
	{
		HorizontalThumbTransform->SetSize(Utils::Min<Float>(sizeRatio.X, 1.f), HorizontalThumbTransform->ReadSize().Y);
		HorizontalThumbTransform->SetPosition(thumbPos.X, HorizontalThumbTransform->ReadPosition().Y);

		//Update colors based on if there's something to scroll or not
		if (HorizontalThumb.IsValid() && HorizontalThumbHoldOffset < 0.f)
		{
			HorizontalThumb->SolidColor = (sizeRatio.X >= 1.f) ? DisabledThumbColor : EnabledThumbColor;
		}
	}

	//Either enable or disable panning controls based on if there are content to scroll.
	RefreshScrollButtonConditions((sizeRatio.Y < 1.f), (sizeRatio.X < 1.f));
}

void ScrollbarComponent::RefreshScrollButtonConditions (bool isVertScrollingEnabled, bool isHorScrollingEnabled)
{
	bool updateHorizontal = (HorizontalControlState == CS_Uninitialized ||
		(HorizontalControlState == CS_Disabled && isHorScrollingEnabled) ||
		(HorizontalControlState == CS_Enabled && !isHorScrollingEnabled));

	bool updateVertical = (VerticalControlState == CS_Uninitialized ||
		(VerticalControlState == CS_Disabled && isVertScrollingEnabled) ||
		(VerticalControlState == CS_Enabled && !isVertScrollingEnabled));

	HorizontalControlState = (isHorScrollingEnabled) ? CS_Enabled : CS_Disabled;
	VerticalControlState = (isVertScrollingEnabled) ? CS_Enabled : CS_Disabled;

	if (HideControlsWhenFull)
	{
		if (updateVertical)
		{
			if (ScrollUpButton.IsValid())
			{
				ScrollUpButton->SetVisibility(isVertScrollingEnabled);
			}

			if (ScrollDownButton.IsValid())
			{
				ScrollDownButton->SetVisibility(isVertScrollingEnabled);
			}

			if (VerticalTrack.IsValid())
			{
				VerticalTrack->SetVisibility(isVertScrollingEnabled);
			}

			if (VerticalThumb.IsValid())
			{
				VerticalThumb->SetVisibility(isVertScrollingEnabled);
			}

#if 0
			if (FrameSpriteTransform.IsValid())
			{
				const Float rightAnchor = (isVertScrollingEnabled) ? ScrollbarThickness : 0.f;
				FrameSpriteTransform->SetAnchorRight(rightAnchor);
			}
#endif
		}

		if (updateHorizontal)
		{
			if (ScrollLeftButton.IsValid())
			{
				ScrollLeftButton->SetVisibility(isHorScrollingEnabled);
			}

			if (ScrollRightButton.IsValid())
			{
				ScrollRightButton->SetVisibility(isHorScrollingEnabled);
			}

			if (HorizontalTrack.IsValid())
			{
				HorizontalTrack->SetVisibility(isHorScrollingEnabled);
			}

			if (HorizontalThumb.IsValid())
			{
				HorizontalThumb->SetVisibility(isHorScrollingEnabled);
			}

#if 0
			if (FrameSpriteTransform.IsValid())
			{
				const Float bottomAnchor = (isHorScrollingEnabled) ? ScrollbarThickness : 0.f;
				FrameSpriteTransform->SetAnchorBottom(bottomAnchor);
			}
#endif
		}
	}
	else //Disable controls when full
	{
		if (updateVertical)
		{
			if (ScrollUpButton.IsValid())
			{
				ScrollUpButton->SetEnabled(isVertScrollingEnabled);
			}

			if (ScrollDownButton.IsValid())
			{
				ScrollDownButton->SetEnabled(isVertScrollingEnabled);
			}
		}

		if (updateHorizontal)
		{
			if (ScrollLeftButton.IsValid())
			{
				ScrollLeftButton->SetEnabled(isHorScrollingEnabled);
			}

			if (ScrollRightButton.IsValid())
			{
				ScrollRightButton->SetEnabled(isHorScrollingEnabled);
			}
		}
	}
}

void ScrollbarComponent::RefreshSpriteTextureRegion ()
{
	if (FrameSpriteTransform.IsNullptr() || FrameSprite.IsNullptr())
	{
		return;
	}

	//Don't set draw coordinates if the size is not yet computed.
	if (FrameSpriteTransform->ReadCachedAbsSize().X <= 0 || FrameSpriteTransform->ReadCachedAbsSize().Y <= 0)
	{
		return;
	}

	//Ensure the camera's ViewExtents is equal to the size of the sprite size to help with alignment.
	if (FrameCamera.IsValid() && FrameCamera->GetZoomedExtents() != FrameSpriteTransform->ReadCachedAbsSize())
	{
		Vector2 oldSize = FrameCamera->GetZoomedExtents();
		FrameCamera->SetViewExtents(FrameSpriteTransform->ReadCachedAbsSize());
		if (ViewedObject != nullptr)
		{
			ViewedObject->HandleScrollbarFrameSizeChange(oldSize, FrameCamera->GetZoomedExtents());
		}
	}
}

void ScrollbarComponent::ReconstructFrameTexture ()
{
	if (FrameSpriteTransform.IsNullptr() || FrameTexture.IsNullptr())
	{
		return;
	}

	const Vector2& desiredAbsSize = FrameSpriteTransform->ReadCachedAbsSize();
	if (desiredAbsSize.X <= 0.f || desiredAbsSize.Y <= 0.f)
	{
		//Component is not yet initialized
		return;
	}

	bUpdateFrameTextureSize = false;
	if (FrameTexture->GetSize() == desiredAbsSize)
	{
		//Already the desireable size
		return;
	}

	RenderTexture* newFrameTexture = RenderTexture::CreateObject();
	newFrameTexture->CreateResource(desiredAbsSize.X.ToInt(), desiredAbsSize.Y.ToInt(), false);

	newFrameTexture->EditDrawLayers() = FrameTexture->ReadDrawLayers();

	//Transfer ownership to newFrameTexture
	newFrameTexture->SetDestroyCamerasOnDestruction(FrameTexture->GetDestroyCamerasOnDestruction());
	newFrameTexture->SetDestroyDrawLayersOnDestruction(FrameTexture->GetDestroyDrawLayersOnDestruction());
	newFrameTexture->ResetColor = FrameTexture->ResetColor;
#ifdef DEBUG_MODE
	newFrameTexture->DebugName = FrameTexture->DebugName;
#endif

	//The draw layers and camera are transferred. Make sure to set these flags in order to prevent the old texture from destroying them.
	FrameTexture->SetDestroyCamerasOnDestruction(false);
	FrameTexture->SetDestroyDrawLayersOnDestruction(false);

	if (ViewedObject != nullptr)
	{
		ViewedObject->UnregisterFromDrawLayer(FrameTexture.Get());
		ViewedObject->RegisterToDrawLayer(this, newFrameTexture);
	}

	RenderTexture* oldFrameTexture = FrameTexture.Get();
	FrameTexture = newFrameTexture;
	if (FrameSprite.IsValid())
	{
		FrameSprite->SetSpriteTexture(newFrameTexture);
	}

	oldFrameTexture->Destroy();
}

Texture* ScrollbarComponent::CalculateAnchoredMouseIcon (const Vector2& mousePos) const
{
	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	CHECK(localTheme != nullptr);

	const Vector2 relativeDist(mousePos - AnchorPosition);
	if (Float::Abs(relativeDist.X) <= AnchorDistanceLimits.Min && Float::Abs(relativeDist.Y) <= AnchorDistanceLimits.Min)
	{
		return localTheme->ScrollbarMouseAnchorStationary.Get();
	}

	//0 = stationary.  -1 = left/up.  1 = right/down
	int verticalDirection = 0;
	int horizontalDirection = 0;

	//Check vertical direction
	if (relativeDist.Y < -AnchorDistanceLimits.Min)
	{
		verticalDirection = -1; //up
	}
	else if (relativeDist.Y > AnchorDistanceLimits.Min)
	{
		verticalDirection = 1; //Down
	}

	//Check horizontal direction
	if (relativeDist.X < -AnchorDistanceLimits.Min)
	{
		horizontalDirection = -1; //left
	}
	else if (relativeDist.X > AnchorDistanceLimits.Min)
	{
		horizontalDirection = 1; //right
	}

	if (verticalDirection < 0 && horizontalDirection == 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUp.Get();
	}
	else if (verticalDirection < 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUpRight.Get();
	}
	else if (verticalDirection == 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanRight.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDownRight.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection == 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDown.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDownLeft.Get();
	}
	else if (verticalDirection == 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanLeft.Get();
	}
	else if (verticalDirection < 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUpLeft.Get();
	}

	return nullptr;
}

void ScrollbarComponent::HandleScrollUpButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera.IsValid())
	{
		CameraPanVelocity = Vector2(0.f, -ScrollSpeed);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(0, -InitialPanDistance);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollDownButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera.IsValid())
	{
		CameraPanVelocity = Vector2(0.f, ScrollSpeed);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(0, InitialPanDistance);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollLeftButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera.IsValid())
	{
		CameraPanVelocity = Vector2(-ScrollSpeed, 0.f);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(-InitialPanDistance, 0.f);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollRightButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera.IsValid())
	{
		CameraPanVelocity = Vector2(ScrollSpeed, 0.f);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(InitialPanDistance, 0.f);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleZoomButtonPressed (ButtonComponent* uiComponent)
{
	HandleZoomButtonRightClickReleased(uiComponent);

	//Disabled until TextFieldComponents are working
#if 0
	if (ZoomFeedbackTransform.IsValid())
	{
		ZoomFeedbackTransform->SetVisibility(true);

		SettingMousePosition = true; //Ignore this mouse jump
		MousePointer::SetAbsMousePosition(ZoomFeedbackTransform->ReadCachedAbsPosition() + (ZoomFeedbackTransform->ReadCachedAbsSize() * 0.5f));
		SettingMousePosition = false;
	}

	if (ZoomLabel != nullptr && FrameCamera != nullptr)
	{
		const Float camZoom = FrameCamera->GetZoom() * 100.f; //Convert decimal to percentage
		ZoomLabel->SetText(camZoom.ToString() + TXT("%"));
	}
#endif
}

void ScrollbarComponent::HandleZoomButtonReleased (ButtonComponent* uiComponent)
{
	//Noop
}

void ScrollbarComponent::HandleZoomButtonRightClickReleased (ButtonComponent* uiComponent)
{
	if (FrameCamera.IsValid())
	{
		FrameCamera->SetZoom(1.f);
	}
}

bool ScrollbarComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvnt)
{
	if (AnchorPosition.X < 0.f || AnchorPosition.Y < 0.f)
	{
		AnchoredMouseIcon = nullptr;
	}
	else
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (localEngine->GetElapsedTime() - AnchorIconTime >= AnchorIconRefreshTimeInterval)
		{
			Vector2 mousePos(Float(mouseEvnt.x), Float(mouseEvnt.y));
			Texture* newAnchorMouseIcon = CalculateAnchoredMouseIcon(mousePos);
			if (AnchoredMouseIcon != newAnchorMouseIcon)
			{
				AnchoredMouseIcon = newAnchorMouseIcon;
				mouse->UpdateMouseIconOverride(newAnchorMouseIcon, SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			}

			AnchorIconTime = localEngine->GetElapsedTime();
		}
	}

	if (AnchoredMouseIcon.IsNullptr())
	{
		//About to remove mouse override, restore mouse pointer size.
		mouse->SetSize(OriginalMouseSize);
	}

	return (AnchoredMouseIcon.IsValid());
}

void ScrollbarComponent::HandleZoomChanged (Float newZoom)
{
	if (ZoomButton.IsValid())
	{
		ZoomButton->SetCaptionText(newZoom.ToFormattedString(1, 2, Float::TM_RoundUp));
	}
}

void ScrollbarComponent::HandleTick (Float deltaSec)
{
	if (bUpdateFrameTextureSize && !bFreezeTextureSize)
	{
		ReconstructFrameTexture();
	}

	if (FrameCamera.IsValid())
	{
		if (LastScrollToPositionTime > 0.f)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)

			Float alpha = (localEngine->GetElapsedTime() - LastScrollToPositionTime) / ScrollToPositionTime;
			if (alpha < 1.f)
			{
				FrameCamera->SetPosition(Vector2::Lerp(alpha, ScrollToPositionSrc, ScrollToPositionDest));
			}
			else
			{
				FrameCamera->SetPosition(ScrollToPositionDest);
				LastScrollToPositionTime = 0.f;
			}
		}

		Vector2 newPos = FrameCamera->ReadPosition();
		if (newPos == CameraDestination && !IsHoldingScrollButton)
		{
			CameraPanVelocity = Vector2::ZERO_VECTOR; //Arrived at destination, stop moving camera
			CameraDestination = Vector2(-1.f, -1.f);
		}
		else if (CameraPanVelocity != Vector2::ZERO_VECTOR)
		{
			//Apply camera velocity
			Float velocityMultiplier = 1.f;
			if (InputBroadcaster::GetShiftHeld())
			{
				velocityMultiplier *= ShiftSpeedMultiplier;
			}

			if (InputBroadcaster::GetAltHeld())
			{
				velocityMultiplier *= AltSpeedMultiplier;
			}

			newPos += ((CameraPanVelocity * velocityMultiplier * deltaSec) / FrameCamera->GetZoom());
		}

		UpdateZoom(deltaSec);

		//Always clamp position in case ViewedObject changed size or Sprite changed dimensions.
		ClampCamPosition(OUT newPos);
		FrameCamera->SetPosition(newPos);
		FrameCamera->ComputeAbsTransform(); //Needed for thumb transforms later in this Tick function

		//Identify if the camera has arrived to destination
		if (!IsHoldingScrollButton && TravelTrackDirection == TTD_None &&
			CameraPanVelocity != Vector2::ZERO_VECTOR && CameraDestination.X >= 0.f && CameraDestination.Y >= 0.f)
		{
			Vector2 direction = CameraPanVelocity;
			direction.NormalizeInline();

			Vector2 relativeDest = CameraDestination - FrameCamera->ReadCachedAbsPosition();
			if (!relativeDest.IsNearlyEqual(Vector2::ZERO_VECTOR, 0.0001f))
			{
				relativeDest.NormalizeInline();
			}

			//If the camera is near/past the destination or suddenly changed direction.  Stop moving and cancel destination.
			if (direction.Dot(relativeDest) < 0.f)
			{
				CameraDestination = Vector2(-1.f, -1.f);
				CameraPanVelocity = Vector2::ZERO_VECTOR;
			}
		}
	}

	CalculateThumbTransforms();

	if (TravelTrackDirection != TTD_None && Mouse.IsValid())
	{
		CalculateTravelTrackTransform(Mouse->ReadPosition());
		UpdateTravelTrackVelocity(Mouse->ReadPosition());
	}

	if (!bFreezeTextureSize)
	{
		RefreshSpriteTextureRegion();
	}
}

void ScrollbarComponent::HandleScrollToPosition (const Vector2& newScrollPosition, bool bDeltaOffset)
{
	if (FrameCamera.IsNullptr())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("ScrollToPosition callback ignored since the scrollbar component doesn't have FrameCamera."));
		return;
	}

	Vector2 destination(newScrollPosition);
	if (bDeltaOffset)
	{
		destination += FrameCamera->ReadPosition();
	}

	if (ScrollToPositionTime <= 0.f)
	{
		FrameCamera->SetPosition(destination);
	}
	else
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		LastScrollToPositionTime = localEngine->GetElapsedTime();
		ScrollToPositionSrc = FrameCamera->GetPosition();
		ScrollToPositionDest = destination;
	}
}
SD_END