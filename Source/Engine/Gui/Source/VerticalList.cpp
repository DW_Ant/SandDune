/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VerticalList.cpp
=====================================================================
*/

#include "VerticalList.h"

IMPLEMENT_CLASS(SD::VerticalList, SD::GuiComponent)
SD_BEGIN

void VerticalList::InitProps ()
{
	Super::InitProps();

	ComponentSpacing = 2.f;
}

void VerticalList::BeginObject ()
{
	Super::BeginObject();

	RefreshingTick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(RefreshingTick))
	{
		RefreshingTick->SetTickInterval(0.4f);
		RefreshingTick->SetTickHandler(SDFUNCTION_1PARAM(this, VerticalList, HandleRefreshTick, void, Float));
	}
}

void VerticalList::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const VerticalList* vertListTemplate = dynamic_cast<const VerticalList*>(objTemplate))
	{
		ComponentSpacing = vertListTemplate->ComponentSpacing;

		if (vertListTemplate->RefreshingTick.IsNullptr() && RefreshingTick.IsValid())
		{
			RefreshingTick->Destroy();
		}
		else if (vertListTemplate->RefreshingTick.IsValid())
		{
			if (RefreshingTick.IsNullptr())
			{
				RefreshingTick = TickComponent::CreateObject(TICK_GROUP_GUI);
				if (!AddComponent(RefreshingTick))
				{
					GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to copy from a vertical list template since it was unable to attach a new tick component to the destination."));
					return;
				}

				RefreshingTick->SetTickHandler(SDFUNCTION_1PARAM(this, VerticalList, HandleRefreshTick, void, Float));
			}

			RefreshingTick->SetTickInterval(vertListTemplate->RefreshingTick->GetTickInterval());
			RefreshingTick->SetTicking(vertListTemplate->RefreshingTick->IsTicking());
		}
	}
}

bool VerticalList::AddComponent_Implementation (EntityComponent* newComponent)
{
	if (!Super::AddComponent_Implementation(newComponent))
	{
		return false;
	}

	if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(newComponent))
	{
		ListOrder.push_back(guiComp);
	}

	return true;
}

bool VerticalList::RemoveComponent (EntityComponent* target)
{
	if (!Super::RemoveComponent(target))
	{
		return false;
	}

	if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(target))
	{
		for (size_t i = 0; i < ListOrder.size(); ++i)
		{
			if (ListOrder.at(i) == guiComp)
			{
				ListOrder.erase(ListOrder.begin() + i);
				break;
			}
		}
	}
	
	return true;
}

void VerticalList::Destroyed ()
{
	//Performance boost - Clear the ListOrder now before the components are destroyed. Destroying components would cause RemoveComponent to be called. No need to iterate through ListOrder for each removed component.
	ContainerUtils::Empty(OUT ListOrder);

	Super::Destroyed();
}

void VerticalList::RefreshComponentTransforms ()
{
	Float vertPos = ComponentSpacing;
	Float horSize = 0.f;

	size_t i = 0;
	//Position all UI Components sequentially
	while (i < ListOrder.size())
	{
		if (ListOrder.at(i).IsNullptr())
		{
			ListOrder.erase(ListOrder.begin() + i);
			continue;
		}

		//If visible and not using normalized scaling in Y direction.
		if (ListOrder.at(i)->IsVisible() && (!ListOrder.at(i)->GetEnableFractionScaling() || ListOrder.at(i)->ReadSize().Y > 1.f))
		{
			Vector2 compSize = ListOrder.at(i)->GetMinimumSize();

			ListOrder.at(i)->SetPosition(ListOrder.at(i)->ReadPosition().X, vertPos);
			vertPos += compSize.Y + ComponentSpacing;

			horSize = Utils::Max(horSize, compSize.X + ListOrder.at(i)->ReadPosition().X);
		}

		++i;
	}

	if (horSize <= 1.f)
	{
		//Everything is using normalized coordinates on the X axis. The vert list's horizontal size should not be affected.
		horSize = ReadSize().X;
	}

	//Scale this component horizontally and vertically to fit all components.
	SetSize(horSize, vertPos);
}

void VerticalList::MoveComponentToTop (GuiComponent* comp)
{
	if (ContainerUtils::IsEmpty(ListOrder))
	{
		return;
	}

	//Iterate backwards since it's more likely that comp is near the end of the list.
	size_t i = ListOrder.size() - 1;
	while (true)
	{
		if (ListOrder.at(i) == comp)
		{
			ListOrder.erase(ListOrder.begin() + i);
			ListOrder.insert(ListOrder.begin(), comp);
			return;
		}

		if (i == 1) //1 because 0 is already at the top of the list.
		{
			return;
		}

		--i;
	}
}

void VerticalList::MoveComponentToBottom (GuiComponent* comp)
{
	if (ContainerUtils::IsEmpty(ListOrder))
	{
		return;
	}

	for (size_t i = 0; i < ListOrder.size() - 1; ++i) //-1 since there's no need to check for the last element.
	{
		if (ListOrder.at(i) == comp)
		{
			ListOrder.erase(ListOrder.begin() + i);
			ListOrder.push_back(comp);
			return;
		}
	}
}

void VerticalList::HandleRefreshTick (Float deltaSec)
{
	RefreshComponentTransforms();
}
SD_END