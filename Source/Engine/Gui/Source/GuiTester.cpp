/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GuiTester.cpp
=====================================================================
*/

#include "GuiClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::GuiTester, SD::GuiEntity)
SD_BEGIN

void GuiTester::InitProps ()
{
	Super::InitProps();

	StageFrame = nullptr;
	PanelButtonCounter = 0;

	MaxButtonsPerPage = 9;
	ButtonBarPageIdx = 0;
	NextButtonBarPage = nullptr;
	PrevButtonBarPage = nullptr;

	TestWindowHandle = nullptr;
	TestDrawLayer = nullptr;
	TestWindowInput = nullptr;
	TestWindowMouse = nullptr;

	ListBoxFilterField = nullptr;
	ListBoxStrings.push_back(TXT("Apple"));
	ListBoxStrings.push_back(TXT("Banana"));
	ListBoxStrings.push_back(TXT("Grape"));
	ListBoxStrings.push_back(TXT("Orange"));
	ListBoxStrings.push_back(TXT("Pear"));
	ListBoxStrings.push_back(TXT("Strawberry"));

	for (Int i = 0; i < 20; i++)
	{
		DropdownNumbers.push_back(i);
	}
}

void GuiTester::ConstructUI ()
{
	InitializeStageFrame();
	PanelButtonCounter = 0;
	CreateBaseButtons();
}

void GuiTester::Destroyed ()
{
	ClearStageComponents(); //Destroy any external entities such as GuiEntities.

	//The Gui Window destroyed the draw layer

	if (TestWindowInput.IsValid())
	{
		TestWindowInput->Destroy();
	}

	if (TestWindowMouse.IsValid())
	{
		TestWindowMouse->Destroy();
	}

	Super::Destroyed();
}

void GuiTester::BeginUnitTest ()
{
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr)

	//Create external window that'll display and broadcast input for this test
	TestWindowHandle = Window::CreateObject();
	TestWindowHandle->SetResource(new sf::RenderWindow(sf::VideoMode(800, 600), "Gui Unit Test"));
	TestWindowHandle->GetResource()->setMouseCursorVisible(false);
	TestWindowHandle->GetTick()->SetTickInterval(0.0166f); //~60 frames per second
	TestWindowHandle->GetPollingTickComponent()->SetTickInterval(0.0166f);
	TestWindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GuiTester, HandleWindowEvent, void, const sf::Event&));

	TestWindowInput = InputBroadcaster::CreateObject();
	TestWindowMouse = MousePointer::CreateObject();
	InputUtils::SetupInputForWindow(TestWindowHandle.Get(), TestWindowInput.Get(), TestWindowMouse.Get());

	//Create DrawLayer for the external window
	TestDrawLayer = GuiDrawLayer::CreateObject();
	TestDrawLayer->SetDrawPriority(0);
	TestDrawLayer->RegisterMenu(this);
	TestDrawLayer->InitCommonEntities(GuiDrawLayer::SCommonGuiClasses(true), TestWindowInput.Get());
	TestWindowHandle->RegisterDrawLayer(TestDrawLayer.Get());
	TestDrawLayer->RegisterPlanarObject(TestWindowMouse.Get()); //Register this mouse to the test window

	SetupInputComponent(TestWindowInput.Get(), 100);

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTick, void, Float));
	}
}

void GuiTester::InitializeStageFrame ()
{
	if (StageFrame.IsValid())
	{
		return;
	}

	StageFrame = FrameComponent::CreateObject();
	if (!AddComponent(StageFrame))
	{
		return;
	}

	StageFrame->SetLockedFrame(true);
	StageFrame->SetBorderThickness(1.f);
	StageFrame->SetCenterColor(Color(48, 48, 48));
	StageFrame->SetPosition(Vector2(192.f, 32.f));
	StageFrame->SetSize(Vector2(600.f, 512.f));
}

void GuiTester::CreateBaseButtons ()
{
	ButtonComponent* frameButton = AddPanelButton();
	if (frameButton != nullptr)
	{
		frameButton->SetCaptionText(TXT("Frame Component"));
		frameButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleFrameButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* labelButton = AddPanelButton();
	if (labelButton != nullptr)
	{
		labelButton->SetCaptionText(TXT("Label Component"));
		labelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleLabelButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* autoSizeLabelButton = AddPanelButton();
	if (autoSizeLabelButton != nullptr)
	{
		autoSizeLabelButton->SetCaptionText(TXT("Auto-sized Label Component"));
		autoSizeLabelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleAutoSizeLabelButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* checkboxButton = AddPanelButton();
	if (checkboxButton != nullptr)
	{
		checkboxButton->SetCaptionText(TXT("Checkbox Component"));
		checkboxButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* contextButton = AddPanelButton();
	if (contextButton != nullptr)
	{
		contextButton->SetCaptionText(TXT("Context Menu Component"));
		contextButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleContextMenuButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* buttonButton = AddPanelButton();
	if (buttonButton != nullptr)
	{
		buttonButton->SetCaptionText(TXT("Button Component"));
		buttonButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* scrollbarButton = AddPanelButton();
	if (scrollbarButton != nullptr)
	{
		scrollbarButton->SetCaptionText(TXT("Scrollbar Component"));
		scrollbarButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* textFieldButton = AddPanelButton();
	if (textFieldButton != nullptr)
	{
		textFieldButton->SetCaptionText(TXT("Text Field Component"));
		textFieldButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* listBoxButton = AddPanelButton();
	if (listBoxButton != nullptr)
	{
		listBoxButton->SetCaptionText(TXT("List Box Component"));
		listBoxButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleListBoxButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* dropdownButton = AddPanelButton();
	if (dropdownButton != nullptr)
	{
		dropdownButton->SetCaptionText(TXT("Dropdown Component"));
		dropdownButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleDropdownButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* verticalListButton = AddPanelButton();
	if (verticalListButton != nullptr)
	{
		verticalListButton->SetCaptionText(TXT("Vertical List Component"));
		verticalListButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleVerticalListButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* tooltipButton = AddPanelButton();
	if (tooltipButton != nullptr)
	{
		tooltipButton->SetCaptionText(TXT("Tooltips"));
		tooltipButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTooltipButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* guiEntityButton = AddPanelButton();
	if (guiEntityButton != nullptr)
	{
		guiEntityButton->SetCaptionText(TXT("Gui Entities"));
		guiEntityButton->SetEnabled(false); //Disabled until fixed
		guiEntityButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleGuiEntityButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* hoverButton = AddPanelButton();
	if (hoverButton != nullptr)
	{
		hoverButton->SetCaptionText(TXT("Hover Component"));
		hoverButton->SetEnabled(false); //Disabled until fixed
		hoverButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleHoverButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* treeButton = AddPanelButton();
	if (treeButton != nullptr)
	{
		treeButton->SetCaptionText(TXT("Tree List Component"));
		treeButton->SetEnabled(false); //Disabled until fixed
		treeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTreeListButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* closeButton = AddPanelButton();
	if (closeButton != nullptr)
	{
		closeButton->SetCaptionText(TXT("End UI Test"));
		closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCloseClicked, void, ButtonComponent*));
	}

	if (ButtonBar.size() <= MaxButtonsPerPage)
	{
		//No need to create prev/next button since there aren't enough buttons for a second page.
		return;
	}

	//Create next and prev button page
	Vector2 buttonSize = Vector2(0.04f, 0.038f);
	Vector2 buttonPosition = Vector2(0.02f, 0.f);
	PrevButtonBarPage = ButtonComponent::CreateObject();
	if (AddComponent(PrevButtonBarPage))
	{
		PrevButtonBarPage->SetPosition(buttonPosition);
		PrevButtonBarPage->SetAnchorBottom(0.05f);
		PrevButtonBarPage->SetSize(buttonSize);
		PrevButtonBarPage->GetBackground()->SetBorderThickness(1.f);
		PrevButtonBarPage->SetCaptionText(TXT("<"));
		PrevButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandlePrevPageClicked, void, ButtonComponent*));

		//Prepare button position for next page
		buttonPosition.X += buttonSize.X + 0.01f;
	}


	NextButtonBarPage = ButtonComponent::CreateObject();
	if (AddComponent(NextButtonBarPage))
	{
		NextButtonBarPage->SetPosition(buttonPosition);
		NextButtonBarPage->SetAnchorBottom(0.05f);
		NextButtonBarPage->SetSize(buttonSize);
		NextButtonBarPage->GetBackground()->SetBorderThickness(1.f);
		NextButtonBarPage->SetCaptionText(TXT(">"));
		NextButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleNextPageClicked, void, ButtonComponent*));
	}
}

void GuiTester::ClearStageComponents ()
{
	for (size_t i = 0; i < StageComponents.size(); i++)
	{
		if (StageComponents.at(i).IsValid())
		{
			StageComponents.at(i)->Destroy();
		}
	}

	for (size_t i = 0; i < GuiEntities.size(); i++)
	{
		if (GuiEntities.at(i).IsValid())
		{
			GuiEntities.at(i)->Destroy();
		}
	}

	ToggleButtons[0] = nullptr;
	ToggleButtons[1] = nullptr;
	ButtonTestCounter = 0;
	EnableToggleCheckbox = nullptr;
	VisibilityToggleCheckbox = nullptr;
	TargetTestCheckbox = nullptr;
	ScrollbarTester = nullptr;
	ScrollbarTesterViewedObject = nullptr;
	ScrollbarTesterLabelTest = nullptr;
	ScrollbarTesterButtonTest = nullptr;
	ScrollbarTesterSizeChangeTest = nullptr;
	ScrollbarTesterDifferentFrameSizeTest = nullptr;
	ScrollbarTesterOuterSizeChangeTest = nullptr;
	ScrollbarTesterNestedScrollbars = nullptr;
	ScrollbarTesterComplexUi = nullptr;
	ScrollbarTesterSizeChangeFrame = nullptr;
	ScrollbarTesterDecreaseSizeButton = nullptr;
	ScrollbarTesterIncreaseSizeButton = nullptr;
	ListBoxFilterField = nullptr;
	MaxNumSelectableListBox = nullptr;
	ButtonBarDropdown = nullptr;
	SetFocusDropdown = nullptr;
	VerticalListMainList = nullptr;
	ContainerUtils::Empty(OUT VerticalListCheckboxes);
	ContainerUtils::Empty(OUT VerticalListButtons);
	HoverLabel = nullptr;
	HoverButton = nullptr;
	TreeListClassBrowser = nullptr;

	ContainerUtils::Empty(OUT StageComponents);
	ContainerUtils::Empty(OUT GuiEntities);

	//All Scrollbar test GUI Components are already destroyed when their associated owners were destroyed.
	ContainerUtils::Empty(ScrollbarTestObjects);
}

void GuiTester::ResetScrollbarTest ()
{
	for (UINT_TYPE i = 0; i < ScrollbarTestObjects.size(); ++i)
	{
		ScrollbarTestObjects.at(i)->Destroy();
	}
	ContainerUtils::Empty(ScrollbarTestObjects);

	if (ScrollbarTester.IsValid())
	{
		ScrollbarTester->SetHideControlsWhenFull(false);
		ScrollbarTester->SetAnchorTop(ScrollbarTesterAnchorTop);
		ScrollbarTester->SetAnchorRight(ScrollbarTesterAnchorRight);
		ScrollbarTester->SetAnchorBottom(ScrollbarTesterAnchorBottom);
		ScrollbarTester->SetAnchorLeft(ScrollbarTesterAnchorLeft);

		if (ScrollbarTester->GetFrameCamera() != nullptr)
		{
			//Snap back to top left corner
			ScrollbarTester->GetFrameCamera()->SetPosition(Vector2::ZERO_VECTOR);

			//Reset zoom
			ScrollbarTester->GetFrameCamera()->SetZoom(1.f);
		}
	}

	if (ScrollbarTesterViewedObject.IsValid())
	{
		ScrollbarTesterViewedObject->SetGuiSizeToOwningScrollbar(Vector2(-1.f, -1.f));
	}
}

ButtonComponent* GuiTester::AddPanelButton ()
{
	ButtonComponent* result = ButtonComponent::CreateObject();
	if (!AddComponent(result))
	{
		return nullptr;
	}

	Vector2 buttonPosition = Vector2(0.02f, 0.05f);
	Vector2 buttonSize = Vector2(0.16f, 0.075f);
	buttonPosition.Y += (0.0886f * PanelButtonCounter.ToFloat());
	result->SetPosition(buttonPosition);
	result->SetSize(buttonSize);
	ButtonBar.push_back(result);
	if (++PanelButtonCounter >= MaxButtonsPerPage)
	{
		//Reset the counter to 0 so that the next button appears on top
		PanelButtonCounter = 0;
	}

	if ((Int(ButtonBar.size() - 1) / MaxButtonsPerPage) != ButtonBarPageIdx)
	{
		//Hide this button since the current button bar page is not viewing this button.
		result->SetVisibility(false);
	}

	return result;
}

void GuiTester::UpdateButtonBarPageIdx ()
{
	for (UINT_TYPE i = 0; i < ButtonBar.size(); i++)
	{
		bool bViewingButton = ((Int(i) / MaxButtonsPerPage) == ButtonBarPageIdx);
		ButtonBar.at(i)->SetVisibility(bViewingButton);
	}
}

void GuiTester::HandleTick (Float deltaSec)
{
	//Noop
}

void GuiTester::HandleWindowEvent (const sf::Event& newWindowEvent)
{
	if (newWindowEvent.type == sf::Event::Closed)
	{
		Destroy();
	}
}

void GuiTester::HandleFrameButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Frame Components!"));
	ClearStageComponents();

	//Simple locked frame with fill texture and borders
	FrameComponent* frame1 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame1))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		frame1->SetLockedFrame(true);
		frame1->SetPosition(Vector2(8.f, 8.f));
		frame1->SetSize(Vector2(48.f, 48.f));
		frame1->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Graphics.DebuggingTexture")));
		StageComponents.push_back(frame1);
	}

	//Simple moveable frame with fill texture and thick borders
	FrameComponent* frame2 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame2))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		frame2->SetLockedFrame(false);
		frame2->SetBorderThickness(16.f);
		frame2->SetPosition(Vector2(80.f, 8.f));
		frame2->SetSize(Vector2(48.f, 48.f));
		frame2->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Graphics.DebuggingTexture")));
		StageComponents.push_back(frame2);
	}

	//locked frame with a moveable frame inside it
	FrameComponent* frame3 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame3))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		frame3->SetLockedFrame(true);
		frame3->SetPosition(Vector2(8.f, 80.f));
		frame3->SetSize(Vector2(96.f, 96.f));
		StageComponents.push_back(frame3);

		FrameComponent* frame4 = FrameComponent::CreateObject();
		if (!frame3->AddComponent(frame4))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach %s to %s (frame4 to frame3)."), frame4->ToString(), frame3->ToString());
		}
		else
		{
			frame4->SetLockedFrame(false);
			frame4->SetPosition(Vector2(0.25f, 0.25f));
			frame4->SetSize(Vector2(0.5f, 0.5f));
			StageComponents.push_back(frame4);
		}
	}

	//Test Min/Max Drag Size
	{
		FrameComponent* dragFrameBorder = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(dragFrameBorder))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach drag frame border to stage frame."));
		}
		else
		{
			dragFrameBorder->SetPosition(Vector2(0.55f, 0.02f));
			dragFrameBorder->SetSize(Vector2(0.4f, 0.3f));
			dragFrameBorder->SetLockedFrame(true);
			dragFrameBorder->SetCenterColor(Color(128, 128, 128, 32));
			StageComponents.push_back(dragFrameBorder);

			FrameComponent* dragSizeLimitTester = FrameComponent::CreateObject();
			if (!dragFrameBorder->AddComponent(dragSizeLimitTester))
			{
				UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach drag size limit tester frame component to the frame border."));
			}
			else
			{
				dragSizeLimitTester->SetPosition(Vector2(0.15f, 0.15f));
				dragSizeLimitTester->SetSize(Vector2(0.7f, 0.7f));
				dragSizeLimitTester->SetLockedFrame(false);
				StageComponents.push_back(dragSizeLimitTester);

				LabelComponent* dragSizeLabel = LabelComponent::CreateObject();
				if (!dragSizeLimitTester->AddComponent(dragSizeLabel))
				{
					UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach label component to drag size limit tester."));
				}
				else
				{
					dragSizeLabel->SetPosition(Vector2(dragSizeLimitTester->GetBorderThickness(), dragSizeLimitTester->GetBorderThickness()));
					dragSizeLabel->SetSize(Vector2(1.f, 1.f));
					DString dragText = TXT("Drag Size Limit Tester");
					dragSizeLabel->SetText(dragText);
					dragSizeLabel->SetClampText(true);
					dragSizeLabel->SetWrapText(false);

					//Set drag size limits based on label length
					Vector2 minDragSize(dragSizeLabel->GetFont()->CalculateStringWidth(dragText, dragSizeLabel->GetCharacterSize()), dragSizeLabel->GetCharacterSize().ToFloat() + 24.f);
					dragSizeLimitTester->SetMinDragSize(minDragSize);
					dragSizeLimitTester->SetMaxDragSize(minDragSize + Vector2(32.f, 64.f));

					StageComponents.push_back(dragSizeLabel);
				}
			}
		}
	}

	//Moveable frame with relative (locked) frames within it.
	{
		FrameComponent* nestedFrameContainer = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(nestedFrameContainer))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach nestedframe container to the stage."));
		}
		else
		{
			nestedFrameContainer->SetLockedFrame(true);
			nestedFrameContainer->SetCenterColor(Color(128, 128, 128, 32));
			nestedFrameContainer->SetPosition(Vector2(0.05f, 0.45f));
			nestedFrameContainer->SetSize(Vector2(200.f, 200.f));
			StageComponents.push_back(nestedFrameContainer);

			FrameComponent* nestedFrame = FrameComponent::CreateObject();
			if (!nestedFrameContainer->AddComponent(nestedFrame))
			{
				UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach nestedframe to the container."));
			}
			else
			{
				nestedFrame->SetPosition(Vector2::ZERO_VECTOR);
				nestedFrame->SetSize(Vector2(1.f, 1.f));
				nestedFrame->SetLockedFrame(false);
				nestedFrame->SetMinDragSize(Vector2(80.f, 80.f));

				FrameComponent* topLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topLeft))
				{
					topLeft->SetPosition(Vector2::ZERO_VECTOR);
					topLeft->SetSize(Vector2(0.2f, 0.2f));
					topLeft->SetLockedFrame(true);
				}

				FrameComponent* topCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topCenter))
				{
					topCenter->SetPosition(Vector2(0.4f, 0.f));
					topCenter->SetSize(Vector2(0.2f, 0.2f));
					topCenter->SetLockedFrame(true);
				}

				FrameComponent* topRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topRight))
				{
					topRight->SetPosition(Vector2(0.8f, 0.f));
					topRight->SetSize(Vector2(0.2f, 0.2f));
					topRight->SetLockedFrame(true);
				}

				FrameComponent* middleLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleLeft))
				{
					middleLeft->SetPosition(Vector2(0.f, 0.4f));
					middleLeft->SetSize(Vector2(0.2f, 0.2f));
					middleLeft->SetLockedFrame(true);
				}

				FrameComponent* middleCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleCenter))
				{
					middleCenter->SetPosition(Vector2(0.4f, 0.4f));
					middleCenter->SetSize(Vector2(0.2f, 0.2f));
					middleCenter->SetLockedFrame(true);
				}

				FrameComponent* middleRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleRight))
				{
					middleRight->SetPosition(Vector2(0.8f, 0.4f));
					middleRight->SetSize(Vector2(0.2f, 0.2f));
					middleRight->SetLockedFrame(true);
				}

				FrameComponent* bottomLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomLeft))
				{
					bottomLeft->SetPosition(Vector2(0.f, 0.8f));
					bottomLeft->SetSize(Vector2(0.2f, 0.2f));
					bottomLeft->SetLockedFrame(true);
				}

				FrameComponent* bottomCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomCenter))
				{
					bottomCenter->SetPosition(Vector2(0.4f, 0.8f));
					bottomCenter->SetSize(Vector2(0.2f, 0.2f));
					bottomCenter->SetLockedFrame(true);
				}

				FrameComponent* bottomRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomRight))
				{
					bottomRight->SetPosition(Vector2(0.8f, 0.8f));
					bottomRight->SetSize(Vector2(0.2f, 0.2f));
					bottomRight->SetLockedFrame(true);
				}
			}
		}
	}

	//Test anchored frames
	LabelComponent* anchorLabel = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(anchorLabel))
	{
		anchorLabel->SetPosition(Vector2(0.55f, 0.5f));
		anchorLabel->SetSize(Vector2(200.f, 16.f));
		anchorLabel->SetText(TXT("Anchored Frame Components"));
		StageComponents.push_back(anchorLabel);
	}

	FrameComponent* anchorContainer = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(anchorContainer))
	{
		anchorContainer->SetLockedFrame(true);
		anchorContainer->SetPosition(Vector2(0.55f, 0.55f));
		anchorContainer->SetSize(Vector2(200.f, 200.f));
		anchorContainer->SetBorderThickness(2.f);
		StageComponents.push_back(anchorContainer);

		FrameComponent* anchorTester = FrameComponent::CreateObject();
		if (anchorContainer->AddComponent(anchorTester))
		{
			anchorTester->SetPosition(Vector2::ZERO_VECTOR);
			anchorTester->SetSize(Vector2(1.f, 1.f));
			anchorTester->SetLockedFrame(false);
			anchorTester->SetMinDragSize(Vector2(150.f, 150.f));

			FrameComponent* topLeft = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(topLeft))
			{
				topLeft->SetPosition(Vector2::ZERO_VECTOR);
				topLeft->SetSize(Vector2(0.2f, 0.2f));
				topLeft->SetLockedFrame(true);
				topLeft->SetAnchorLeft(24.f);
				topLeft->SetAnchorTop(24.f);
			}

			FrameComponent* topRight = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(topRight))
			{
				topRight->SetPosition(Vector2(0.8f, 0.f));
				topRight->SetSize(Vector2(0.2f, 0.2f));
				topRight->SetLockedFrame(true);
				topRight->SetAnchorRight(24.f);
				topRight->SetAnchorTop(24.f);
			}

			FrameComponent* bottomLeft = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(bottomLeft))
			{
				bottomLeft->SetPosition(Vector2(0.f, 0.8f));
				bottomLeft->SetSize(Vector2(0.2f, 0.2f));
				bottomLeft->SetLockedFrame(true);
				bottomLeft->SetAnchorLeft(24.f);
				bottomLeft->SetAnchorBottom(24.f);
			}

			FrameComponent* bottomRight = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(bottomRight))
			{
				bottomRight->SetPosition(Vector2(0.8f, 0.8f));
				bottomRight->SetSize(Vector2(0.2f, 0.2f));
				bottomRight->SetLockedFrame(true);
				bottomRight->SetAnchorRight(24.f);
				bottomRight->SetAnchorBottom(24.f);
			}

			FrameComponent* verticalCenter = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(verticalCenter))
			{
				verticalCenter->SetLockedFrame(true);
				verticalCenter->SetAnchorLeft(68.f);
				verticalCenter->SetAnchorRight(68.f);
				verticalCenter->SetAnchorTop(24.f);
				verticalCenter->SetAnchorBottom(24.f);
			}

			FrameComponent* horizontalCenter = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(horizontalCenter))
			{
				horizontalCenter->SetLockedFrame(true);
				horizontalCenter->SetAnchorLeft(24.f);
				horizontalCenter->SetAnchorRight(24.f);
				horizontalCenter->SetAnchorTop(68.f);
				horizontalCenter->SetAnchorBottom(68.f);
			}
		}
	}
}

void GuiTester::HandleLabelButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Label Components!"));
	ClearStageComponents();

	LabelComponent* topLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top left label component to stage frame."));
	}
	else
	{
		topLeft->SetSize(Vector2(1.f, 1.f));
		topLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		topLeft->SetVerticalAlignment(LabelComponent::VA_Top);
		topLeft->SetText(TXT("Top Left"));
		StageComponents.push_back(topLeft);
	}

	LabelComponent* topCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top center label component to stage frame."));
	}
	else
	{
		topCenter->SetSize(Vector2(1.f, 1.f));
		topCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		topCenter->SetVerticalAlignment(LabelComponent::VA_Top);
		topCenter->SetText(TXT("Top Center"));
		StageComponents.push_back(topCenter);
	}


	LabelComponent* topRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top right label component to stage frame."));
	}
	else
	{
		topRight->SetSize(Vector2(1.f, 1.f));
		topRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		topRight->SetVerticalAlignment(LabelComponent::VA_Top);
		topRight->SetText(TXT("Top Right"));
		StageComponents.push_back(topRight);
	}

	LabelComponent* centerRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach center right label component to stage frame."));
	}
	else
	{
		centerRight->SetSize(Vector2(1.f, 1.f));
		centerRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		centerRight->SetVerticalAlignment(LabelComponent::VA_Center);
		centerRight->SetText(TXT("Center Right"));
		StageComponents.push_back(centerRight);
	}

	LabelComponent* bottomRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom right label component to stage frame."));
	}
	else
	{
		bottomRight->SetSize(Vector2(1.f, 1.f));
		bottomRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		bottomRight->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomRight->SetText(TXT("Bottom Right"));
		StageComponents.push_back(bottomRight);
	}

	LabelComponent* bottomCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom center label component to stage frame."));
	}
	else
	{
		bottomCenter->SetSize(Vector2(1.f, 1.f));
		bottomCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		bottomCenter->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomCenter->SetText(TXT("Bottom Center"));
		StageComponents.push_back(bottomCenter);
	}

	LabelComponent* bottomLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom left label component to stage frame."));
	}
	else
	{
		bottomLeft->SetSize(Vector2(1.f, 1.f));
		bottomLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		bottomLeft->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomLeft->SetText(TXT("Bottom Left"));
		StageComponents.push_back(bottomLeft);
	}

	LabelComponent* centerLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach center left label component to stage frame."));
	}
	else
	{
		centerLeft->SetSize(Vector2(1.f, 1.f));
		centerLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		centerLeft->SetVerticalAlignment(LabelComponent::VA_Center);
		centerLeft->SetText(TXT("Center Left"));
		StageComponents.push_back(centerLeft);
	}

	LabelComponent* centerCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach centered label component to stage frame."));
	}
	else
	{
		centerCenter->SetSize(Vector2(1.f, 1.f));
		centerCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		centerCenter->SetVerticalAlignment(LabelComponent::VA_Center);
		centerCenter->SetText(TXT("Centered"));
		StageComponents.push_back(centerCenter);
	}

	LabelComponent* largeText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(largeText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach large text label component to stage frame."));
	}
	else
	{
		largeText->SetSize(Vector2(256, 96));
		largeText->SetPosition(Vector2(48, 64));
		largeText->SetHorizontalAlignment(LabelComponent::HA_Left);
		largeText->SetVerticalAlignment(LabelComponent::VA_Top);
		largeText->SetText(TXT("Large Text (32)"));
		largeText->SetCharacterSize(32);
		StageComponents.push_back(largeText);
	}

	LabelComponent* mediumText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(mediumText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach medium text label component to stage frame."));
	}
	else
	{
		mediumText->SetSize(Vector2(384, 96));
		mediumText->SetPosition(Vector2(48, 110));
		mediumText->SetHorizontalAlignment(LabelComponent::HA_Left);
		mediumText->SetVerticalAlignment(LabelComponent::VA_Top);
		mediumText->SetText(TXT("Medium Text (16)"));
		mediumText->SetCharacterSize(16);
		StageComponents.push_back(mediumText);
	}

	LabelComponent* smallText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(smallText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach small text label component to stage frame."));
	}
	else
	{
		smallText->SetSize(Vector2(256, 96));
		smallText->SetPosition(Vector2(48, 144));
		smallText->SetHorizontalAlignment(LabelComponent::HA_Left);
		smallText->SetVerticalAlignment(LabelComponent::VA_Top);
		smallText->SetText(TXT("Small Text (8)"));
		smallText->SetCharacterSize(8);
		StageComponents.push_back(smallText);
	}

	LabelComponent* clampedText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(clampedText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach clamped text label component to stage frame."));
	}
	else
	{
		Float clampedTextWidth = 96.f;
		clampedText->SetSize(Vector2(clampedTextWidth, 96));
		clampedText->SetPosition(Vector2(StageFrame->ReadCachedAbsSize().X - clampedTextWidth, 64));
		clampedText->SetHorizontalAlignment(LabelComponent::HA_Left);
		clampedText->SetVerticalAlignment(LabelComponent::VA_Top);
		clampedText->SetWrapText(false);
		clampedText->SetClampText(true);
		clampedText->SetText(TXT("Clamped Text - Clamped Text - Clamped Text - Clamped Text - Clamped Text"));
		StageComponents.push_back(clampedText);
	}

	FrameComponent* autoRefreshFrame = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(autoRefreshFrame))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto refreshing frame component to stage frame."));
	}
	else
	{
		const Float borderThickness = 6.f;
		autoRefreshFrame->SetSize(Vector2(150.f, 96.f));
		autoRefreshFrame->SetPosition(Vector2(0.2f, 0.3f));
		autoRefreshFrame->SetLockedFrame(false);
		autoRefreshFrame->SetBorderThickness(borderThickness);
		StageComponents.push_back(autoRefreshFrame);

		LabelComponent* autoRefreshingText = LabelComponent::CreateObject();
		if (!autoRefreshFrame->AddComponent(autoRefreshingText))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto refreshing text label component to the auto refresh frame component."));
		}
		else
		{
			autoRefreshingText->SetAutoRefresh(false);
			autoRefreshingText->SetSize(1.f, 1.f);
			autoRefreshingText->SetAnchorTop(borderThickness);
			autoRefreshingText->SetAnchorRight(borderThickness);
			autoRefreshingText->SetAnchorBottom(borderThickness);
			autoRefreshingText->SetAnchorLeft(borderThickness);
			autoRefreshingText->SetClampText(true);
			autoRefreshingText->SetWrapText(true);
			autoRefreshingText->SetText(TXT("This text should wrap and clamp every time owning frame component changes size."));
			autoRefreshingText->SetAutoRefresh(true);
		}
	}

	LabelComponent* singleSpacedLine = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(singleSpacedLine))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach single spaced clamped text label component to stage frame."));
	}
	else
	{
		Float clampedTextWidth = 192.f;
		singleSpacedLine->SetSize(Vector2(clampedTextWidth, 96));
		singleSpacedLine->SetPosition(Vector2(StageFrame->ReadCachedAbsSize().X - clampedTextWidth, 32.f + (StageFrame->ReadCachedAbsSize().Y * 0.5f)));
		singleSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
		singleSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
		singleSpacedLine->SetWrapText(true);
		singleSpacedLine->SetClampText(true);
		singleSpacedLine->SetLineSpacing(0.f);
		singleSpacedLine->SetText(TXT("Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing."));
		StageComponents.push_back(singleSpacedLine);
	}

	LabelComponent* multiSpacedLine = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(multiSpacedLine))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach multi spaced unclamped text label component to stage frame."));
	}
	else
	{
		Float clampedTextWidth = 160.f;
		multiSpacedLine->SetSize(Vector2(clampedTextWidth, 128));
		multiSpacedLine->SetPosition(Vector2((StageFrame->ReadCachedAbsSize().X * 0.5f) - clampedTextWidth, 32.f + (StageFrame->ReadCachedAbsSize().Y * 0.5f)));
		multiSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
		multiSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
		multiSpacedLine->SetWrapText(true);
		multiSpacedLine->SetClampText(false);
		multiSpacedLine->SetLineSpacing(multiSpacedLine->GetCharacterSize().ToFloat());
		multiSpacedLine->SetText(TXT("Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing."));
		StageComponents.push_back(multiSpacedLine);
	}

	LabelComponent* newLineLabel = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(newLineLabel))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach new line character test label component to stage frame."));
	}
	else
	{
		newLineLabel->SetSize(Vector2(96, 128));
		newLineLabel->SetPosition(Vector2((StageFrame->ReadCachedAbsSize().X * 0.67f), (StageFrame->ReadCachedAbsSize().Y * 0.25f)));
		newLineLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
		newLineLabel->SetVerticalAlignment(LabelComponent::VA_Top);
		newLineLabel->SetWrapText(false);
		newLineLabel->SetClampText(false);
		newLineLabel->SetText(TXT("New Line \n character.\n"));
		StageComponents.push_back(newLineLabel);
	}
}

void GuiTester::HandleAutoSizeLabelButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Auto Sizing Label Components!"));
	ClearStageComponents();

	std::function<void(LabelComponent*)> addFrameTo = [](LabelComponent* labelToAddFrameTo)
	{
		FrameComponent* newFrame = FrameComponent::CreateObject();
		if (labelToAddFrameTo->AddComponent(newFrame))
		{
			newFrame->SetPosition(Vector2(0.f, 0.f));
			newFrame->SetSize(Vector2(1.f, 1.f));
			newFrame->SetLockedFrame(true);
			newFrame->SetCenterColor(Color(16, 16, 16, 128));
			newFrame->SetBorderThickness(0.f);

			if (BorderRenderComponent* borderComp = newFrame->GetBorderComp())
			{
				borderComp->Destroy();
			}
		}
	};

	Float numLines = 0.f;
	const Float lineHeight = dynamic_cast<const LabelComponent*>(LabelComponent::SStaticClass()->GetDefaultObject())->GetCharacterSize().ToFloat();

	LabelComponent* description = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(description))
	{
		description->SetAutoRefresh(false);
		description->SetPosition(Vector2(0.f, lineHeight * numLines));
		description->SetSize(Vector2(0.f, lineHeight * 2));
		description->SetAutoSizeHorizontal(true);
		description->SetClampText(false);
		description->SetText(TXT("These label components are auto sized where their size is\nautomatically adjusted based on the text."));
		numLines += 2.f;
		description->SetAutoRefresh(true);
		addFrameTo(description);
		StageComponents.push_back(description);
	}

	++numLines; //Add a blank line

	LabelComponent* horizontalTest = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(horizontalTest))
	{
		horizontalTest->SetAutoRefresh(false);
		horizontalTest->SetPosition(Vector2(0.f, lineHeight * numLines));
		horizontalTest->SetSize(Vector2(0.f, lineHeight * 3.f));
		horizontalTest->SetAutoSizeHorizontal(true);
		horizontalTest->SetClampText(true);
		horizontalTest->SetText(TXT("This label is scaled horizontally automatically based on it's longest line.\nThe shorter lines do not influence the scale.\nThe fourth line is clamped.\nYou should not be able to read this line."));
		numLines += 3.f;
		horizontalTest->SetAutoRefresh(true);
		addFrameTo(horizontalTest);
		StageComponents.push_back(horizontalTest);
	}

	++numLines; //Add a blank line

	LabelComponent* unclampedText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(unclampedText))
	{
		unclampedText->SetAutoRefresh(false);
		unclampedText->SetPosition(Vector2(0.f, lineHeight * numLines));
		unclampedText->SetSize(Vector2(0.f, lineHeight * 2.f));
		unclampedText->SetAutoSizeHorizontal(true);
		unclampedText->SetClampText(false);
		unclampedText->SetText(TXT("This label is scaled horizontally.\nThis is also unclamped, which would make the line below visible.\nEven though this line is outside range, the unclamped property allows overflow."));
		numLines += 3.f; //Size is 2 to test text overflow, but set numLines to 3 to prevent the next LabelComponent from overlapping with this label.
		unclampedText->SetAutoRefresh(true);
		addFrameTo(unclampedText);
		StageComponents.push_back(unclampedText);
	}

	++numLines; //Add a blank line

	LabelComponent* verticalTest = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(verticalTest))
	{
		verticalTest->SetAutoRefresh(false);
		verticalTest->SetPosition(Vector2(0.f, lineHeight * numLines));
		verticalTest->SetSize(Vector2(0.5f, 0.f));
		verticalTest->SetAutoSizeVertical(true);
		verticalTest->SetWrapText(false);
		verticalTest->SetHorizontalAlignment(LabelComponent::HA_Center);
		verticalTest->SetText(TXT("This label is scaled vertically.\nThis label also has wrap text to false\nwhich may cause text to go beyond the width of the component.\nHorizontal alignment is based on component width.\nLine 5."));
		numLines += 5.f;
		verticalTest->SetAutoRefresh(true);
		addFrameTo(verticalTest);
		StageComponents.push_back(verticalTest);
	}

	++numLines; //Add a blank line

	LabelComponent* verticalWrapText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(verticalWrapText))
	{
		verticalWrapText->SetAutoRefresh(false);
		verticalWrapText->SetPosition(Vector2(0.f, lineHeight * numLines));
		verticalWrapText->SetSize(Vector2(0.4f, 0.f));
		verticalWrapText->SetAutoSizeVertical(true);
		verticalWrapText->SetWrapText(true);
		verticalWrapText->SetHorizontalAlignment(LabelComponent::HA_Right);
		verticalWrapText->SetText(TXT("This label is scaled vertically.\nThis label also has wrap text to true which may cause text to move to the next line.\nThis is right aligned.\nLine 4."));
		numLines += 6.f;
		verticalWrapText->SetAutoRefresh(true);
		addFrameTo(verticalWrapText);
		StageComponents.push_back(verticalWrapText);
	}

	++numLines; //Add a blank line

	LabelComponent* autoSizeText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(autoSizeText))
	{
		autoSizeText->SetAutoRefresh(false);
		autoSizeText->SetPosition(Vector2(0.f, lineHeight * numLines));
		autoSizeText->SetSize(Vector2(0.f, 0.f));
		autoSizeText->SetAutoSizeVertical(true);
		autoSizeText->SetAutoSizeHorizontal(true);
		autoSizeText->SetHorizontalAlignment(LabelComponent::HA_Center);
		autoSizeText->SetText(TXT("This label is automically scaled vertically and horizontally.\nIt's a combination where the label width is determined by the widest line such as this one,\nand the height of the label component is determined based on the number of lines.\nWrapping and clamping does not apply."));
		numLines += 4.f;
		autoSizeText->SetAutoRefresh(true);
		addFrameTo(autoSizeText);
		StageComponents.push_back(autoSizeText);
	}
}

void GuiTester::HandleButtonButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Button Components!"));
	ClearStageComponents();

	Texture* textureStates = GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get();
	Float buttonHeight = 24.f;
	Float buttonWidth = 164.f;
	Float buttonInterval = 8.f;
	Float buttonPosition = 16.f;

	ButtonComponent* basicButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(basicButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic button component to stage frame."));
	}
	else
	{
		basicButton->SetSize(Vector2(buttonWidth, buttonHeight));
		basicButton->SetPosition(Vector2(16, buttonPosition));
		basicButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		basicButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		basicButton->ReplaceStateComponent(nullptr);
		buttonPosition += buttonHeight + buttonInterval;
		basicButton->SetCaptionText("No State Button");
		StageComponents.push_back(basicButton);
	}

	Vector2 noCaptionPosition(16, buttonPosition);
	ButtonComponent* noCaption = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(noCaption))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach no caption button component to stage frame."));
	}
	else
	{
		noCaption->SetSize(Vector2(buttonWidth, buttonHeight));
		noCaption->SetPosition(noCaptionPosition);
		noCaption->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		noCaption->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		noCaption->GetCaptionComponent()->Destroy();
		StageComponents.push_back(noCaption);
	}

	LabelComponent* tip = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(tip))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach label component about the captionless button component to stage frame."));
	}
	else
	{
		tip->SetSize(Vector2(buttonWidth, buttonHeight));
		tip->SetPosition(Vector2(noCaptionPosition.X + buttonWidth + 16, noCaptionPosition.Y));
		tip->SetText(TXT("<---- No Caption"));
		StageComponents.push_back(tip);
	}

	ButtonComponent* multiStateButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(multiStateButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach multi-state button component to stage frame."));
	}
	else
	{
		multiStateButton->SetSize(Vector2(buttonWidth, buttonHeight));
		multiStateButton->SetPosition(Vector2(16, buttonPosition));
		multiStateButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		multiStateButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		multiStateButton->SetCaptionText(TXT("Multi-state Button"));
		multiStateButton->GetBackground()->SetCenterTexture(textureStates);
		multiStateButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(multiStateButton);
	}

	ButtonComponent* enabledButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(enabledButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach enabled button component to stage frame."));
	}
	else
	{
		enabledButton->SetSize(Vector2(buttonWidth, buttonHeight));
		enabledButton->SetPosition(Vector2(16, buttonPosition));
		enabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		enabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		enabledButton->SetCaptionText(TXT("Enabled Button"));
		enabledButton->SetEnabled(true);
		enabledButton->GetBackground()->SetCenterTexture(textureStates);
		enabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(enabledButton);
	}

	ButtonComponent* disabledButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(disabledButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach disabled button component to stage frame."));
	}
	else
	{
		disabledButton->SetSize(Vector2(buttonWidth, buttonHeight));
		disabledButton->SetPosition(Vector2(16, buttonPosition));
		disabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		disabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		disabledButton->SetCaptionText(TXT("Disabled Button"));
		disabledButton->SetEnabled(false);
		disabledButton->GetBackground()->SetCenterTexture(textureStates);
		disabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(disabledButton);
	}

	ButtonComponent* toggledButton1 = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(toggledButton1))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggleable button component to stage frame."));
	}
	else
	{
		toggledButton1->SetSize(Vector2(buttonWidth, buttonHeight));
		toggledButton1->SetPosition(Vector2(16, buttonPosition));
		toggledButton1->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		toggledButton1->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestToggleReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		toggledButton1->SetCaptionText(TXT("Toggleable Button"));
		toggledButton1->SetEnabled(true);
		toggledButton1->GetBackground()->SetCenterTexture(textureStates);
		toggledButton1->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		ToggleButtons[0] = toggledButton1;
		StageComponents.push_back(toggledButton1);
	}

	ButtonComponent* toggledButton2 = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(toggledButton2))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggleable button component to stage frame."));
	}
	else
	{
		toggledButton2->SetSize(Vector2(buttonWidth, buttonHeight));
		toggledButton2->SetPosition(Vector2(16, buttonPosition));
		toggledButton2->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		toggledButton2->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestToggleReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		toggledButton2->SetCaptionText(TXT("Toggleable Button"));
		toggledButton2->SetEnabled(false);
		toggledButton2->GetBackground()->SetCenterTexture(textureStates);
		toggledButton2->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		ToggleButtons[1] = toggledButton2;
		StageComponents.push_back(toggledButton2);
	}

	ButtonComponent* outerButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(outerButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach outer button component to stage frame."));
	}
	else
	{
		outerButton->SetSize(Vector2(buttonWidth, buttonHeight * 3));
		outerButton->SetPosition(Vector2(16, buttonPosition));
		outerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		outerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += (buttonHeight * 3) + buttonInterval;
		outerButton->SetCaptionText(TXT("Outer Button"));
		outerButton->GetCaptionComponent()->SetVerticalAlignment(LabelComponent::VA_Top);
		outerButton->GetBackground()->SetCenterTexture(textureStates);
		outerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(outerButton);

		ButtonComponent* innerButton = ButtonComponent::CreateObject();
		if (!outerButton->AddComponent(innerButton))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach inner button component to outer button."));
		}
		else
		{
			innerButton->SetSize(Vector2(buttonWidth - 12, buttonHeight));
			innerButton->SetPosition(Vector2(8.f, buttonHeight));
			innerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
			innerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
			innerButton->SetCaptionText(TXT("Inner Button"));
			innerButton->GetBackground()->SetCenterTexture(textureStates);
			innerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			StageComponents.push_back(innerButton);
		}
	}

	ButtonComponent* colorChangeButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(colorChangeButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach color changing button component to the stage."));
	}
	else
	{
		colorChangeButton->SetSize(Vector2(buttonWidth, buttonHeight));
		colorChangeButton->SetPosition(Vector2(16.f, buttonPosition));
		buttonPosition += buttonHeight + buttonInterval;
		colorChangeButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		colorChangeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		colorChangeButton->SetCaptionText(TXT("Solid Color State"));
		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(colorChangeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		StageComponents.push_back(colorChangeButton);
	}

	ButtonComponent* rightClickButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(rightClickButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach right click button component to stage frame."));
	}
	else
	{
		ButtonTestCounter = 0;
		rightClickButton->SetSize(Vector2(buttonWidth, buttonHeight));
		const Vector2 rightClickButtonPos(16.f, buttonPosition);
		rightClickButton->SetPosition(rightClickButtonPos);
		rightClickButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		rightClickButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestIncrementCounter, void, ButtonComponent*));
		rightClickButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestDecrementCounter, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		rightClickButton->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
		StageComponents.push_back(rightClickButton);

		LabelComponent* rightClickTip = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(rightClickTip))
		{
			rightClickTip->SetSize(Vector2(buttonWidth * 3.f, buttonHeight));
			rightClickTip->SetPosition(Vector2(rightClickButtonPos.X + buttonWidth + 16, rightClickButtonPos.Y));
			rightClickTip->SetText(TXT("<--- Left click to increment. Right click to decrement."));
			StageComponents.push_back(rightClickTip);
		}
	}
}

void GuiTester::HandleCheckboxButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Checkbox Components!"));
	ClearStageComponents();

	CheckboxComponent* basicCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(basicCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic checkbox component to stage frame."));
	}
	else
	{
		basicCheckbox->SetSize(Vector2(200.f, 32.f));
		basicCheckbox->SetPosition(Vector2(8.f, 8.f));
		basicCheckbox->GetCaptionComponent()->SetText(TXT("Basic Checkbox"));
		basicCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(basicCheckbox);
	}

	CheckboxComponent* reversedCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(reversedCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach reversed checkbox component to stage frame."));
	}
	else
	{
		reversedCheckbox->SetSize(Vector2(200.f, 32.f));
		reversedCheckbox->SetPosition(Vector2(8.f, 80.f));
		reversedCheckbox->GetCaptionComponent()->SetText(TXT("Reversed Checkbox"));
		reversedCheckbox->SetCheckboxRightSide(false);
		reversedCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(reversedCheckbox);
	}

	EnableToggleCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(EnableToggleCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggle enableness checkbox component to stage frame."));
	}
	else
	{
		EnableToggleCheckbox->SetSize(Vector2(200.f, 32.f));
		EnableToggleCheckbox->SetPosition(Vector2(8.f, 120.f));
		EnableToggleCheckbox->GetCaptionComponent()->SetText(TXT("Set Enabled"));
		EnableToggleCheckbox->SetChecked(true);
		EnableToggleCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleToggleEnableCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(EnableToggleCheckbox.Get());
	}

	VisibilityToggleCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(VisibilityToggleCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggle visibility checkbox component to stage frame."));
	}
	else
	{
		VisibilityToggleCheckbox->SetSize(Vector2(200.f, 32.f));
		VisibilityToggleCheckbox->SetPosition(Vector2(8.f, 160.f));
		VisibilityToggleCheckbox->GetCaptionComponent()->SetText(TXT("Set Visibility"));
		VisibilityToggleCheckbox->SetChecked(true);
		VisibilityToggleCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleToggleVisibilityCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(VisibilityToggleCheckbox.Get());
	}

	TargetTestCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(TargetTestCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach target checkbox component to stage frame."));
	}
	else
	{
		TargetTestCheckbox->SetSize(Vector2(200.f, 32.f));
		TargetTestCheckbox->SetPosition(Vector2(8.f, 200.f));
		TargetTestCheckbox->GetCaptionComponent()->SetText(TXT("Target Checkbox"));
		TargetTestCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(TargetTestCheckbox.Get());
	}

	FrameComponent* checkboxContainer = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(checkboxContainer))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach checkboxContainer to StageFrame."));
	}
	else
	{
		checkboxContainer->SetSize(Vector2(200.f, 32.f));
		checkboxContainer->SetPosition(Vector2(8.f, 240.f));
		checkboxContainer->SetLockedFrame(true);
		checkboxContainer->SetBorderThickness(1.f);
		StageComponents.push_back(checkboxContainer);

		CheckboxComponent* containedCheckbox = CheckboxComponent::CreateObject();
		if (!checkboxContainer->AddComponent(containedCheckbox))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach containedCheckbox to checkboxContainer."));
		}
		else
		{
			containedCheckbox->SetPosition(Vector2::ZERO_VECTOR);
			containedCheckbox->SetSize(Vector2(1.f, 1.f));
			containedCheckbox->GetCaptionComponent()->SetText(TXT("Testing Contained Checkbox"));
			containedCheckbox->GetCaptionComponent()->SetCharacterSize(12);
		}
	}
}

void GuiTester::HandleContextMenuButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Context Menu Components!"));
	ClearStageComponents();

	FrameComponent* directionTestFrame = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(directionTestFrame))
	{
		directionTestFrame->SetPosition(Vector2::ZERO_VECTOR);
		directionTestFrame->SetSize(Vector2(0.5f, 0.5f)); //Really large to test against open directions
		directionTestFrame->SetBorderThickness(1.f);
		StageComponents.push_back(directionTestFrame);

		LabelComponent* directionTest = LabelComponent::CreateObject();
		if (directionTestFrame->AddComponent(directionTest))
		{
			directionTest->SetAutoRefresh(false);
			directionTest->SetPosition(Vector2::ZERO_VECTOR);
			directionTest->SetSize(Vector2(1.f, 1.f));
			directionTest->SetHorizontalAlignment(LabelComponent::HA_Center);
			directionTest->SetVerticalAlignment(LabelComponent::VA_Center);
			directionTest->SetWrapText(true);
			directionTest->SetClampText(false);
			directionTest->SetText(TXT("DIRECTION TEST:\n\
Right click anywhere here to reveal a context menu.\n\
This ContextMenuComponent will attempt to open it towards the top left direction.\n\
If there is not enough space, it'll flip over to a different direction.\n\
This context menu is also configured to only close if the user\n\
clicked on the X button."));
			directionTest->SetAutoRefresh(true);
		}

		ContextMenuComponent* contextComp = ContextMenuComponent::CreateObject();
		if (directionTestFrame->AddComponent(contextComp))
		{
			contextComp->SetContextClass(GuiEntity::SStaticClass());
			contextComp->SetRevealDirection(ContextMenuComponent::RD_TopLeft);
			contextComp->OnInitializeMenu = [&](GuiEntity* menu, ContextMenuComponent* contextComp, MousePointer* mouse)
			{
				menu->SetSize(Vector2(384.f, 128.f));

				FrameComponent* contextFrame = FrameComponent::CreateObject();
				if (menu->AddComponent(contextFrame))
				{
					contextFrame->SetCenterColor(Color(64, 64, 64, 255));
				}

				LabelComponent* contextDescription = LabelComponent::CreateObject();
				if (menu->AddComponent(contextDescription))
				{
					contextDescription->SetAutoRefresh(false);
					contextDescription->SetSize(Vector2(1.f, 0.95f));
					contextDescription->SetAnchorBottom(0.f);
					contextDescription->SetHorizontalAlignment(LabelComponent::HA_Center);
					contextDescription->SetVerticalAlignment(LabelComponent::VA_Center);
					contextDescription->SetWrapText(true);
					contextDescription->SetText(TXT("This oversized context menu will close\n\
when the user clicks on the X in the corner of this menu,\n\
or when the user reveals another context menu."));
					contextDescription->SetAutoRefresh(true);
				}

				ButtonComponent* closeContext = ButtonComponent::CreateObject();
				if (menu->AddComponent(closeContext))
				{
					closeContext->SetPosition(Vector2(0.85f, 0.05f));
					closeContext->SetSize(Vector2(0.1f, 0.2f));
					closeContext->SetCaptionText(TXT("X"));
					closeContext->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCloseContextMenu, void, ButtonComponent*));
				}

				return true;
			};
		}
	}

	//Scrollbar context menu test
	ScrollbarComponent* scrollbarFrame = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(scrollbarFrame))
	{
		scrollbarFrame->SetPosition(Vector2(0.f, 0.6f));
		scrollbarFrame->SetSize(Vector2(0.4f, 0.33f));
		StageComponents.push_back(scrollbarFrame);

		GuiEntity* viewedObj = GuiEntity::CreateObject();
		viewedObj->SetSize(Vector2(512.f, 512.f));
		viewedObj->SetGuiSizeToOwningScrollbar(Vector2(-1.f, -1.f));
		scrollbarFrame->SetViewedObject(viewedObj);

		LabelComponent* scrollbarText = LabelComponent::CreateObject();
		if (viewedObj->AddComponent(scrollbarText))
		{
			scrollbarText->SetAutoRefresh(false);
			scrollbarText->SetPosition(Vector2::ZERO_VECTOR);
			scrollbarText->SetSize(1.f, 1.f);
			scrollbarText->SetWrapText(true);
			scrollbarText->SetText(TXT("Scrollbar Test:\n\
This LabelComponent is within a scrollbar component.\n\
However, the context menu should appear above the scrollbar component.\n\
\n\
Also the context menu, itself, will have its own scrollbar.\n\
Click here with the left mouse button to open the context menu."));
			scrollbarText->SetAutoRefresh(true);
		}

		ContextMenuComponent* contextMenu = ContextMenuComponent::CreateObject();
		if (viewedObj->AddComponent(contextMenu))
		{
			contextMenu->SetContextClass(GuiEntity::SStaticClass());
			contextMenu->SetRevealMenuButton(sf::Mouse::Left);
			contextMenu->OnInitializeMenu = [&](GuiEntity* newMenu, ContextMenuComponent* contextComp, MousePointer* mouse)
			{
				newMenu->SetSize(Vector2(110.f, 160.f));

				ScrollbarComponent* contextScrollbar = ScrollbarComponent::CreateObject();
				if (newMenu->AddComponent(contextScrollbar))
				{
					contextScrollbar->SetPosition(Vector2::ZERO_VECTOR);
					contextScrollbar->SetSize(Vector2(1.f, 1.f));

					GuiEntity* buttonListEntity = GuiEntity::CreateObject();
					buttonListEntity->SetSize(Vector2(150.f, 150.f));
					buttonListEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
					buttonListEntity->SetAutoSizeVertical(true);
					contextScrollbar->SetViewedObject(buttonListEntity);

					for (size_t i = 0; i < 10; ++i)
					{
						ButtonComponent* closeButton = ButtonComponent::CreateObject();
						if (buttonListEntity->AddComponent(closeButton))
						{
							closeButton->SetPosition(Vector2(0.1f, (i * 24.f) + 8.f));
							closeButton->SetSize(Vector2(0.8f, 16.f));
							closeButton->SetCaptionText(TXT("Close"));
							closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCloseContextMenu, void, ButtonComponent*));
						}
					}
				}

				return true;
			};
		}
	}

	//Giant DynamicListMenu
	FrameComponent* listFrame = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(listFrame))
	{
		listFrame->SetPosition(Vector2(0.55f, 0.05f));
		listFrame->SetSize(Vector2(0.4f, 0.9f));
		listFrame->SetBorderThickness(1.f);
		StageComponents.push_back(listFrame);

		LabelComponent* listInstructions = LabelComponent::CreateObject();
		if (listFrame->AddComponent(listInstructions))
		{
			listInstructions->SetAutoRefresh(false);
			listInstructions->SetPosition(Vector2::ZERO_VECTOR);
			listInstructions->SetSize(1.f, 1.f);
			listInstructions->SetAnchorTop(8.f);
			listInstructions->SetAnchorRight(8.f);
			listInstructions->SetAnchorBottom(8.f);
			listInstructions->SetAnchorLeft(8.f);
			listInstructions->SetHorizontalAlignment(LabelComponent::HA_Center);
			listInstructions->SetVerticalAlignment(LabelComponent::VA_Center);
			listInstructions->SetWrapText(true);
			listInstructions->SetClampText(false);
			listInstructions->SetText(TXT("DynamicListMenu test:\n\n\
This context menu uses the DynamicListMenu.\n\
This component determines what options appear in this menu.\n\
Even though this component doesn't explicitly use the DynamicListMenu (via SetContextClass), the default behavior is that it'll reference \
the GuiTheme, and the default GuiTheme uses a DynamicListMenu as its default context class.\n\
\n\
The DynamicListMenu will take up the entire overlay size, allowing the its frame components (its visible menus) to be placed anywhere the GuiDrawLayer permits.\n\
Right click anywhere in this frame component component to see the DynamicListMenu."));
			listInstructions->SetAutoRefresh(true);
		}

		ContextMenuComponent* context = ContextMenuComponent::CreateObject();
		if (listFrame->AddComponent(context))
		{
			//No need to set ContextClass since that is found through the GuiTheme. A custom GuiTheme may override this, however.
			context->OnInitializeMenu = [&](GuiEntity* menu, ContextMenuComponent* contextComp, MousePointer* mouse)
			{
				DynamicListMenu* listMenu = dynamic_cast<DynamicListMenu*>(menu);
				if (listMenu == nullptr)
				{
					GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize DynamicListMenu. The given GuiEntity is not a DynamicListMenu instance. Instead it's a %s."), menu->ToString());
					return false;
				}

				std::vector<DynamicListMenu::SInitMenuOption> initOptions
				{
					DynamicListMenu::SInitMenuOption(TXT("Command 1"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command 1 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Command 2"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command 2 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Command 3"),[](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command 3 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(DynamicListMenu::DIVIDER),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 1"), sf::Keyboard::Num1, KS_None, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 1 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 2"), sf::Keyboard::Num2, KS_Ctrl, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 2 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 3"), sf::Keyboard::Num3, KS_Alt, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 3 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 4"), sf::Keyboard::Num4, KS_Shift, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 4 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 5"), sf::Keyboard::Num5, KS_Ctrl | KS_Alt, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 5 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 6"), sf::Keyboard::Num6, KS_Ctrl | KS_Shift, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 6 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 7"), sf::Keyboard::Num7, KS_Alt | KS_Shift, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 7 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Test>Keybind Cmd 8"), sf::Keyboard::Num8, KS_Ctrl | KS_Alt | KS_Shift, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind Cmd 8 is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Another Menu>Command a"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command a is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Another Menu>Command b"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command b is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Even if commands are inserted out of order>Command c will automatically move back up to Another Menu due to string match."), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Matching Sub Menu Instruction command is executed from DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Another Menu>Command c"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Command c is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Narrow Menu Test>l"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("l command from the narrow menu test is executed."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu>Second Narrow Menu Test>i"), sf::Keyboard::I, KS_Ctrl, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("i command from the second narrow menu test is executed."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Command x"), sf::Keyboard::X, KS_None, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("x command is executed from the the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Keybinds in the top menu should take priority."), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Keybind instruction command is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Priority Binds>Command x"), sf::Keyboard::X, KS_None, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("x command from the higher priority menu is executed."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Priority Binds>Command y"), sf::Keyboard::Y, KS_Ctrl, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("y command from the higher priority menu is executed."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Command y"), sf::Keyboard::Y, KS_None, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("y command is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Command z"), sf::Keyboard::Z, KS_Ctrl, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("z command is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Keybind Sub Menu>Priority Binds>Command z"), sf::Keyboard::Z, KS_None, [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("z command from the higher priority menu is executed."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Many Sub Menus>This Test Will>Expand to many menus.>The menus will choose>to either expand>left>or right>based on the available space.>Eventually you will reach>to a command.>Also menus should pop off>one-by-one>when hovering the mouse>out of bounds>End"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Nested menu test command is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Many Sub Menus>This test Will>Expand to many menus.>The menus will choose>to either expand>left>or right>based on the available space.>Eventually you will reach>to a command.>Also pressing escape>will pop the top menu.>Pressing escape repeatedly>will eventually close>the context menu"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Nested menu test with escape instruction is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu Spacing Test>You can use escape characters to display \\> characters in menu."), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Escape character instruction is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu Spacing Test>") + DynamicListMenu::DIVIDER),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu Spacing Test>Dividers can also exist>in sub menus"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Sub Menu Divider instruction is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu Spacing Test>") + DynamicListMenu::DIVIDER),
					DynamicListMenu::SInitMenuOption(TXT("Sub Menu Spacing Test>Menus can also fade out>Only when executing a command"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("Fade out instruction is executed from the DynamicListMenu."));
					}),
					DynamicListMenu::SInitMenuOption(DynamicListMenu::DIVIDER),
					DynamicListMenu::SInitMenuOption(TXT("The last command"), [](BaseGuiDataElement* data)
					{
						GuiLog.Log(LogCategory::LL_Debug, TXT("The last command is executed from the DynamicListMenu."));
					})
				};

				//Insert GuiDataElements right above the last command
				size_t insertIdx = initOptions.size() - 2;
				for (Int i = 0; i < 10; ++i)
				{
					DynamicListMenu::SInitMenuOption newOption(TXT("DataElementTest>Command ") + i.ToString(), SDFUNCTION_1PARAM(this, GuiTester, HandleDynamicListMenuCmd, void, BaseGuiDataElement*));
					DString textData = TXT("Cmd ") + i.ToString();
					newOption.Data = new GuiDataElement<DString>(textData, textData); //The DynamicListMenu will take ownership over the new GuiDataElement object. No need to delete it here.

					initOptions.insert(initOptions.begin() + insertIdx, newOption);
					++insertIdx;
				}

				listMenu->InitializeList(contextComp, mouse, initOptions);

				return true; //End callback to initialize context menu
			};
		}
	}
}

void GuiTester::HandleScrollbarButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar Components!"));
	ClearStageComponents();

	//Create the button components
	const Vector2 buttonSize(96.f, 48.f);
	Int buttonCounter = 0;
	ScrollbarTesterLabelTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterLabelTest))
	{
		ScrollbarTesterLabelTest->SetAnchorLeft(8.f);
		ScrollbarTesterLabelTest->SetAnchorTop(8.f + (buttonCounter.ToFloat() * buttonSize.Y));
		ScrollbarTesterLabelTest->SetSize(buttonSize);
		ScrollbarTesterLabelTest->SetCaptionText(TXT("Scrollable Label"));
		ScrollbarTesterLabelTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterLabelTestClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterLabelTest.Get());
	}

	ScrollbarTesterButtonTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterButtonTest))
	{
		ScrollbarTesterButtonTest->SetAnchorLeft(8.f);
		ScrollbarTesterButtonTest->SetAnchorTop(8.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterButtonTest->SetSize(buttonSize);
		ScrollbarTesterButtonTest->SetCaptionText(TXT("Scrollable Buttons"));
		ScrollbarTesterButtonTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterButtonTestClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterButtonTest.Get());
	}

	ScrollbarTesterSizeChangeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterSizeChangeTest))
	{
		ScrollbarTesterSizeChangeTest->SetAnchorLeft(8.f);
		ScrollbarTesterSizeChangeTest->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterSizeChangeTest->SetSize(buttonSize);
		ScrollbarTesterSizeChangeTest->SetCaptionText(TXT("Adjustable Size Test"));
		ScrollbarTesterSizeChangeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterSizeChangeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterSizeChangeTest.Get());
	}

	ScrollbarTesterDifferentFrameSizeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterDifferentFrameSizeTest))
	{
		ScrollbarTesterDifferentFrameSizeTest->SetAnchorLeft(8.f);
		ScrollbarTesterDifferentFrameSizeTest->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterDifferentFrameSizeTest->SetSize(buttonSize);
		ScrollbarTesterDifferentFrameSizeTest->SetCaptionText(TXT("Adjustable Frame Test"));
		ScrollbarTesterDifferentFrameSizeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterDifferentFrameSizeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterDifferentFrameSizeTest.Get());
	}

#if 0
	//Disabled since there's a better test FramedScrollbar. If there's a need to enable this, add a callback to freeze the scrollbar on frame drag.
	ScrollbarTesterOuterSizeChangeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterOuterSizeChangeTest))
	{
		ScrollbarTesterOuterSizeChangeTest->SetAnchorLeft(8.f);
		ScrollbarTesterOuterSizeChangeTest->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterOuterSizeChangeTest->SetSize(buttonSize);
		ScrollbarTesterOuterSizeChangeTest->SetCaptionText(TXT("Adjustable Scrollbar Size Test"));
		ScrollbarTesterOuterSizeChangeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterOuterSizeChangeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterOuterSizeChangeTest.Get());
	}
#endif

	ScrollbarTesterNestedScrollbars = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterNestedScrollbars))
	{
		ScrollbarTesterNestedScrollbars->SetAnchorLeft(8.f);
		ScrollbarTesterNestedScrollbars->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterNestedScrollbars->SetSize(buttonSize);
		ScrollbarTesterNestedScrollbars->SetCaptionText(TXT("Nested Scrollbars"));
		ScrollbarTesterNestedScrollbars->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterNestedScrollbarsClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterNestedScrollbars.Get());
	}

	ScrollbarTesterLaunchFramedScrollbarTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterLaunchFramedScrollbarTest))
	{
		ScrollbarTesterLaunchFramedScrollbarTest->SetAnchorLeft(8.f);
		ScrollbarTesterLaunchFramedScrollbarTest->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterLaunchFramedScrollbarTest->SetSize(buttonSize);
		ScrollbarTesterLaunchFramedScrollbarTest->SetCaptionText(TXT("Framed Scrollbar"));
		ScrollbarTesterLaunchFramedScrollbarTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterFramedScrollbarClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterLaunchFramedScrollbarTest.Get());
	}

	ScrollbarTesterComplexUi = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterComplexUi))
	{
		ScrollbarTesterComplexUi->SetAnchorLeft(8.f);
		ScrollbarTesterComplexUi->SetAnchorTop(16.f + (buttonCounter.ToFloat() * (buttonSize.Y + 8.f)));
		ScrollbarTesterComplexUi->SetSize(buttonSize);
		ScrollbarTesterComplexUi->SetCaptionText(TXT("Complex UI"));
		ScrollbarTesterComplexUi->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterComplexUiClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterComplexUi.Get());
	}

	//Setup the ViewedObject and scrollbar
	ScrollbarTester = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTester))
	{
		ScrollbarTesterAnchorTop = 8.f;
		ScrollbarTesterAnchorRight = 8.f;
		ScrollbarTesterAnchorBottom = 8.f;
		ScrollbarTesterAnchorLeft = buttonSize.X + 16.f;

		ScrollbarTester->SetAnchorTop(ScrollbarTesterAnchorTop);
		ScrollbarTester->SetAnchorRight(ScrollbarTesterAnchorRight);
		ScrollbarTester->SetAnchorBottom(ScrollbarTesterAnchorBottom);
		ScrollbarTester->SetAnchorLeft(ScrollbarTesterAnchorLeft);
		RenderTexture* renderTexture = ScrollbarTester->GetFrameTexture();
		CHECK(renderTexture != nullptr)
		renderTexture->ResetColor = Color(24, 24, 24, 255);
		StageComponents.push_back(ScrollbarTester.Get());

		ScrollbarTesterViewedObject = GuiEntity::CreateObject();
		ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
		ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
		ScrollbarTesterViewedObject->SetEnableFractionScaling(true);
		ScrollbarTester->SetViewedObject(ScrollbarTesterViewedObject.Get());
	}
}

void GuiTester::HandleTextFieldButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Text Field Components!"));
	ClearStageComponents();
	ContainerUtils::Empty(OUT TextFields);

	Vector2 buttonSize(0.2f, 0.05f);
	ButtonComponent* leftAlignButton = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(leftAlignButton))
	{
		leftAlignButton->SetPosition(Vector2(0.01f, 0.01f));
		leftAlignButton->SetSize(buttonSize);
		leftAlignButton->SetCaptionText(TXT("Left Align"));
		leftAlignButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldLeftAlignButtonClicked, void, ButtonComponent*));
		StageComponents.push_back(leftAlignButton);
	}

	ButtonComponent* centerAlignButton = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(centerAlignButton))
	{
		centerAlignButton->SetPosition(Vector2(0.5f - (buttonSize.X * 0.5f), 0.01f));
		centerAlignButton->SetSize(buttonSize);
		centerAlignButton->SetCaptionText(TXT("Center Align"));
		centerAlignButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldCenterAlignButtonClicked, void, ButtonComponent*));
		StageComponents.push_back(centerAlignButton);
	}

	ButtonComponent* rightAlignButton = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(rightAlignButton))
	{
		rightAlignButton->SetPosition(Vector2(0.99f - buttonSize.X, 0.01f));
		rightAlignButton->SetSize(buttonSize);
		rightAlignButton->SetCaptionText(TXT("Right Align"));
		rightAlignButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldRightAlignButtonClicked, void, ButtonComponent*));
		StageComponents.push_back(rightAlignButton);
	}

	const Int smallCharacterSize = 12;

	TextFieldComponent* uninteractableField = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(uninteractableField))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach uninteractable text field to stage frame."));
	}
	else
	{
		uninteractableField->SetAutoRefresh(false);
		uninteractableField->SetSize(Vector2(0.4f, 0.15f));
		uninteractableField->SetPosition(Vector2(0.01f, 0.15f));
		uninteractableField->SetEditable(false);
		uninteractableField->SetSelectable(false);
		uninteractableField->SetCharacterSize(smallCharacterSize);
		uninteractableField->SetText(TXT("This text is not selectable or editable.\n\nLine break test."));
		uninteractableField->SetAutoRefresh(true);
		TextFields.push_back(uninteractableField);
		StageComponents.push_back(uninteractableField);
	}

	TextFieldComponent* selectableField = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(selectableField))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach selectable text field to stage frame."));
	}
	else
	{
		selectableField->SetAutoRefresh(false);
		selectableField->SetSize(Vector2(0.4f, 0.15f));
		selectableField->SetPosition(Vector2(0.55f, 0.15f));
		selectableField->SetEditable(false);
		selectableField->SetSelectable(true);
		selectableField->SetCharacterSize(smallCharacterSize);
		selectableField->SetText(TXT("This text is selectable, but this is not editable.  You should be able to set cursor and highlight positions to this field, and press Ctrl+C to copy the selected text, and be able to paste the text to an external program."));
		selectableField->SetAutoRefresh(true);
		TextFields.push_back(selectableField);
		StageComponents.push_back(selectableField);
	}

	TextFieldComponent* editableText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(editableText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach editable text field to stage frame."));
	}
	else
	{
		editableText->SetAutoRefresh(false);
		editableText->SetSize(Vector2(0.4f, 0.15f));
		editableText->SetPosition(Vector2(0.01f, 0.35f));
		editableText->SetEditable(true);
		editableText->SetSelectable(true);
		editableText->SetCharacterSize(smallCharacterSize);
		editableText->SetText(TXT("This text is selectable and editable.  You can click on the text field to set the cursor position.  You can use the arrows to navigate the cursor around.  You also have copy, paste, and cut functionality."));
		editableText->SetAutoRefresh(true);
		TextFields.push_back(editableText);
		StageComponents.push_back(editableText);
	}

	TextFieldComponent* limitedText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(limitedText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach limited text field to stage frame."));
	}
	else
	{
		limitedText->SetAutoRefresh(false);
		limitedText->SetSize(Vector2(0.4f, 0.15f));
		limitedText->SetPosition(Vector2(0.55f, 0.35f));
		limitedText->SetEditable(true);
		limitedText->SetSelectable(true);
		limitedText->MaxNumCharacters = 64;
		limitedText->SetText(TXT("This editable Text Field has a 64 character limit."));
		limitedText->SetAutoRefresh(true);
		TextFields.push_back(limitedText);
		StageComponents.push_back(limitedText);
	}

	TextFieldComponent* constraintText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(constraintText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach constraint text field to stage frame."));
	}
	else
	{
		constraintText->SetAutoRefresh(false);
		constraintText->SetSize(Vector2(0.4f, 0.1f));
		constraintText->SetPosition(Vector2(0.01f, 0.75f));
		constraintText->SetEditable(true);
		constraintText->SetSelectable(true);
		constraintText->SetText(TXT("Letters spaces and periods only."));
		constraintText->MaxNumCharacters = 48;
		constraintText->OnAllowTextInput = SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldLimitedInput, bool, const DString&);
		constraintText->OnTextChanged = SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldTextChanged, void, TextFieldComponent*);
		constraintText->SetAutoRefresh(true);
		TextFields.push_back(constraintText);
		StageComponents.push_back(constraintText);
	}

	TextFieldComponent* singleLineText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(singleLineText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach single line text field to stage frame."));
	}
	else
	{
		singleLineText->SetAutoRefresh(false);
		singleLineText->SetSize(Vector2(0.4f, 0.075f));
		singleLineText->SetPosition(Vector2(0.01f, 0.875f));
		singleLineText->SetEditable(true);
		singleLineText->SetSelectable(true);
		singleLineText->SetSingleLine(true);
		singleLineText->SetText(TXT("Single line text field. Text will scroll horizontally without a scrollbar."));
		singleLineText->OnReturn = SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldSingleLineReturn, void, TextFieldComponent*);
		singleLineText->SetAutoRefresh(true);
		TextFields.push_back(singleLineText);
		StageComponents.push_back(singleLineText);
	}

	ScrollbarComponent* textScrollbar = ScrollbarComponent::CreateObject();
	if (!StageFrame->AddComponent(textScrollbar))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach scrollbar for the text field component to stage frame."));
	}
	else
	{
		textScrollbar->SetPosition(Vector2(0.01f, 0.55f));
		textScrollbar->SetSize(Vector2(0.4f, 0.15f));
		textScrollbar->SetZoomEnabled(true);
		textScrollbar->SetHideControlsWhenFull(true);
		StageComponents.push_back(textScrollbar);

		GuiEntity* textViewedObject = GuiEntity::CreateObject();
		textViewedObject->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
		textViewedObject->SetAutoSizeVertical(true);
		textScrollbar->SetViewedObject(textViewedObject);

		TextFieldComponent* scrollableField = TextFieldComponent::CreateObject();
		if (textViewedObject->AddComponent(scrollableField))
		{
			scrollableField->SetAutoRefresh(false);
			scrollableField->SetSize(Vector2(1.f, -1.f));
			scrollableField->SetAutoSizeVertical(true);
			scrollableField->SetEditable(true);
			scrollableField->SetSelectable(true);
			scrollableField->bAutoSelect = false;
			scrollableField->SetText(TXT("This text field is editable inside a scrollbar component.\nUnicode test: \u03ba\u1f79\u03c3\u03bc\u03b5 = cosmos!\nAlso this text doesn't automatically select all when clicked."));
			scrollableField->SetAutoRefresh(true);
			TextFields.push_back(scrollableField);
		}
	}

	ScrollbarComponent* stressTestScrollbar = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(stressTestScrollbar))
	{
		stressTestScrollbar->SetSize(Vector2(0.4f, 0.3f));
		stressTestScrollbar->SetPosition(Vector2(0.55f, 0.55f));
		stressTestScrollbar->SetHideControlsWhenFull(false);
		StageComponents.push_back(stressTestScrollbar);

		GuiEntity* viewObj = GuiEntity::CreateObject();
		viewObj->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
		viewObj->SetAutoSizeVertical(true);
		stressTestScrollbar->SetViewedObject(viewObj);

		TextFieldComponent* stressTest = TextFieldComponent::CreateObject();
		if (!viewObj->AddComponent(stressTest))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach stress test text field to stage frame."));
		}
		else
		{
			stressTest->SetAutoRefresh(false);
			stressTest->SetPosition(Vector2::ZERO_VECTOR);
			stressTest->SetSize(Vector2(1.f, 0.f));
			stressTest->SetAutoSizeVertical(true);
			stressTest->SetEditable(true);
			stressTest->SetSelectable(true);
			stressTest->bAutoSelect = false;
			stressTest->SetText(TXT("\tFour score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. \
\n\n\tNow we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this. \
\n\n\tBut, in a larger sense, we can not dedicate-we can not consecrate-we can not hallow-this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us-that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion-that we here highly resolve that these dead shall not have died in vain-that this nation, under God, shall have a new birth of freedom-and that government of the people, by the people, for the people, shall not perish from the earth."));
			stressTest->SetAutoRefresh(true);
			TextFields.push_back(stressTest);
		}
	}
}

void GuiTester::HandleListBoxButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying List Box Components!"));
	ClearStageComponents();

	//Add text field component that'll apply a filter to all ListBoxComponents.
	{
		LabelComponent* filterLabel = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(filterLabel))
		{
			filterLabel->SetAutoRefresh(false);
			filterLabel->SetPosition(Vector2(0.f, 0.01f));
			filterLabel->SetSize(Vector2(0.225f, 0.05f));
			filterLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
			filterLabel->SetVerticalAlignment(LabelComponent::VA_Center);
			filterLabel->SetWrapText(false);
			filterLabel->SetText(TXT("Filter:"));
			filterLabel->SetAutoRefresh(true);
			StageComponents.push_back(filterLabel);
		}

		ListBoxFilterField = TextFieldComponent::CreateObject();
		if (StageFrame->AddComponent(ListBoxFilterField))
		{
			ListBoxFilterField->SetAutoRefresh(false);
			ListBoxFilterField->SetPosition(Vector2(0.25f, 0.01f));
			ListBoxFilterField->SetSize(Vector2(0.225f, 0.05f));
			ListBoxFilterField->OnEdit = SDFUNCTION_1PARAM(this, GuiTester, HandleListBoxFilterEdit, bool, const sf::Event&);
			ListBoxFilterField->SetAutoRefresh(true);
			StageComponents.push_back(ListBoxFilterField);
		}
	}

	Float textVertDisplacement = 0.05f;

	std::vector<GuiDataElement<DString>> listContent;
	for (UINT_TYPE i = 0; i < ListBoxStrings.size(); i++)
	{
		listContent.push_back(GuiDataElement<DString>(ListBoxStrings.at(i), ListBoxStrings.at(i)));
	}

	ListBoxComponent* basicListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(basicListBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic list box to stage frame."));
	}
	else
	{
		basicListBox->SetSize(Vector2(0.25f, 2.f));
		basicListBox->SetPosition(Vector2(0.01f, 0.075f));

		basicListBox->SetList(listContent);
		basicListBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleBasicListBoxOptionSelected, void, Int);
		basicListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleBasicListBoxOptionDeselected, void, Int);
		StageComponents.push_back(basicListBox);

		//Test List Box selections
		{
			std::vector<DString> selectedItems;
			selectedItems.push_back(ListBoxStrings.at(1));
			selectedItems.push_back(ListBoxStrings.at(3));
			basicListBox->SetSelectedItems(selectedItems);

			std::vector<DString> actualSelectedItems;
			basicListBox->GetSelectedItems(OUT actualSelectedItems);

			if (selectedItems.size() != actualSelectedItems.size())
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("GuiTester:  ListBox component test failed.  After selecting %s in the ListBox, the accessor to obtain them returned %s items."), Int(selectedItems.size()), Int(actualSelectedItems.size())));
				Destroy();
				return;
			}

			for (size_t i = 0; i < selectedItems.size(); ++i)
			{
				if (selectedItems.at(i) != actualSelectedItems.at(i))
				{
					Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("GuiTester:  ListBox component test failed.  After selecting items, retrieving the selected items does not return the original match. At index %s, it returned %s instead of %s."), Int(i), actualSelectedItems.at(i), selectedItems.at(i)));
					Destroy();
					return;	
				}
			}

			DString actualItem = selectedItems.at(0);
			if (!basicListBox->IsItemSelected(actualItem))
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("GuiTester:  ListBox component test failed.  The \"%s\" should be a selected item, but IsItemSelected returns otherwise."), actualItem));
				Destroy();
				return;
			}

			actualItem = ListBoxStrings.at(0);
			if (basicListBox->IsItemSelected(actualItem))
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("GuiTester:  ListBox component test failed.  The \"%s\" should not be a selected item, but IsItemSelected returns otherwise."), actualItem));
				Destroy();
				return;
			}

			Int testIdx = 3;
			DString expected = ListBoxStrings.at(testIdx.ToUnsignedInt());
			bool bFound = basicListBox->GetItemByIdx(testIdx, OUT actualItem);
			if (!bFound || expected != actualItem)
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("GuiTester:  ListBox component test failed.  Failed to find item at index %s. It should have found \"%s\" instead."), testIdx, expected));
				Destroy();
				return;
			}
		}
	}

	ScrollbarComponent* scrollbar = ScrollbarComponent::CreateObject();
	if (!StageFrame->AddComponent(scrollbar))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach scrollbar to the stage frame."));
	}
	else
	{
		scrollbar->SetSize(Vector2(0.25f, 0.33f));
		scrollbar->SetPosition(Vector2(0.33f, 0.075f));
		scrollbar->SetHideControlsWhenFull(true);
		StageComponents.push_back(scrollbar);

		GuiEntity* listOwner = GuiEntity::CreateObject();
		listOwner->SetAutoSizeHorizontal(false);
		listOwner->SetAutoSizeVertical(true);
		listOwner->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
		scrollbar->SetViewedObject(listOwner);

		ListBoxComponent* scrollbarListBox = ListBoxComponent::CreateObject();
		if (!listOwner->AddComponent(scrollbarListBox))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach scrollbar list box to stage frame."));
		}
		else
		{
			scrollbarListBox->SetSize(Vector2(1.f, 1.f));
			scrollbarListBox->SetPosition(Vector2::ZERO_VECTOR);

			std::vector<GuiDataElement<Int>> listContent;
			for (Int i = 0; i < 50; i++)
			{
				listContent.push_back(GuiDataElement<Int>(i, i.ToString()));
			}
			scrollbarListBox->SetList(listContent);
		}
	}

	ListBoxComponent* readOnlyBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(readOnlyBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach read only list box to stage frame."));
	}
	else
	{
		readOnlyBox->SetSize(Vector2(0.25f, 0.01f));
		readOnlyBox->SetPosition(Vector2(0.66f, 0.075f));

		readOnlyBox->SetList(listContent);
		readOnlyBox->SetReadOnly(true);

		readOnlyBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleReadOnlyBoxOptionSelected, void, Int);
		readOnlyBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleReadOnlyBoxOptionDeselected, void, Int);
		StageComponents.push_back(readOnlyBox);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach read only List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(0.25f, 16.f));
			description->SetPosition(readOnlyBox->ReadPosition() - Vector2(0.f, textVertDisplacement));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(TXT("Read Only"));
			StageComponents.push_back(description);
		}
	}

	ListBoxComponent* someReadOnlyBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(someReadOnlyBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach some read only list box to stage frame."));
	}
	else
	{
		std::vector<GuiDataElement<DString>> foodList;
		GuiDataElement<DString> newElement(TXT("Apple"), TXT("Apple"));
		foodList.push_back(newElement);
		newElement.Data = TXT("Banana");
		newElement.LabelText = newElement.Data;
		foodList.push_back(newElement);
		newElement.Data = TXT("Carrot");
		newElement.LabelText = newElement.Data;
		newElement.bReadOnly = true;
		foodList.push_back(newElement);
		newElement.Data = TXT("Pea");
		newElement.LabelText = newElement.Data;
		foodList.push_back(newElement);
		newElement.Data = TXT("Orange");
		newElement.LabelText = newElement.Data;
		newElement.bReadOnly = false;
		foodList.push_back(newElement);
		newElement.Data = TXT("Green Bean");
		newElement.LabelText = newElement.Data;
		newElement.bReadOnly = true;
		foodList.push_back(newElement);
		newElement.Data = TXT("Pear");
		newElement.LabelText = newElement.Data;
		newElement.bReadOnly = false;
		foodList.push_back(newElement);

		someReadOnlyBox->SetSize(Vector2(0.25f, 0.01f));
		someReadOnlyBox->SetPosition(Vector2(0.66f, 0.4f));
		someReadOnlyBox->SetList(foodList);
		StageComponents.push_back(someReadOnlyBox);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach some read only List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(0.25f, 16.f));
			description->SetPosition(someReadOnlyBox->ReadPosition() - Vector2(0.f, textVertDisplacement));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(TXT("Vegetables Not Selectable"));
			description->SetCharacterSize(12);
			StageComponents.push_back(description);
		}
	}

	MaxNumSelectableListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(MaxNumSelectableListBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach list box with a limit of selectable items to stage frame."));
	}
	else
	{
		MaxNumSelectableListBox->SetSize(Vector2(0.25f, 0.01f));
		MaxNumSelectableListBox->SetPosition(Vector2(0.01f, 0.55f));

		MaxNumSelectableListBox->SetList(listContent);
		MaxNumSelectableListBox->SetMaxNumSelected(3);

		MaxNumSelectableListBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleMaxNumSelectedBoxOptionSelected, void, Int);
		MaxNumSelectableListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleMaxNumSelectedBoxOptionDeselected, void, Int);
		StageComponents.push_back(MaxNumSelectableListBox.Get());

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach max num selectable items List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(0.25f, 16.f));
			description->SetPosition(MaxNumSelectableListBox->ReadPosition() - Vector2(0, textVertDisplacement));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(DString::CreateFormattedString(TXT("Max selections = %s"), MaxNumSelectableListBox->GetMaxNumSelected()));
			StageComponents.push_back(description);
		}
	}

	ListBoxComponent* minNumSelectableListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(minNumSelectableListBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach list box with a min limit of selectable items to stage frame."));
	}
	else
	{
		minNumSelectableListBox->SetSize(Vector2(0.25f, 0.01f));
		minNumSelectableListBox->SetPosition(Vector2(0.33f, 0.55f));

		minNumSelectableListBox->SetList(listContent);
		minNumSelectableListBox->SetMinNumSelected(2);

		minNumSelectableListBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleMinNumSelectedBoxOptionSelected, void, Int);
		minNumSelectableListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleMinNumSelectedBoxOptionDeselected, void, Int);
		StageComponents.push_back(minNumSelectableListBox);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach min num selectable items List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(0.25f, 16.f));
			description->SetPosition(minNumSelectableListBox->ReadPosition() - Vector2(0.f, textVertDisplacement));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(DString::CreateFormattedString(TXT("Min Selections = %s"), minNumSelectableListBox->GetMinNumSelected()));
			StageComponents.push_back(description);
		}
	}

	ListBoxComponent* autoDeselect = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(autoDeselect))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto deselect list box to stage frame."));
	}
	else
	{
		autoDeselect->SetSize(Vector2(0.25f, 0.01f));
		autoDeselect->SetPosition(Vector2(0.66f, 0.75f));

		autoDeselect->SetList(listContent);
		autoDeselect->SetAutoDeselect(true);

		autoDeselect->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleAutoDeselectBoxOptionSelected, void, Int);
		autoDeselect->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleAutoDeselectBoxOptionDeselected, void, Int);
		StageComponents.push_back(autoDeselect);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto deselect List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(0.25f, 16.f));
			description->SetPosition(autoDeselect->ReadPosition() - Vector2(0.f, textVertDisplacement));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(TXT("Auto deselects"));
			StageComponents.push_back(description);
		}
	}
}

void GuiTester::HandleDropdownButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Dropdown Components!"));
	ClearStageComponents();

	DropdownComponent* basicDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(basicDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic dropdown to stage frame."));
	}
	else
	{
		basicDropdown->SetSize(Vector2(256, 128));
		basicDropdown->SetPosition(Vector2(48, 48));
		basicDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, GuiTester, HandleBasicDropdownOptionClicked, void, DropdownComponent*, Int);
		basicDropdown->SetNoSelectedItemText(TXT("<Nothing selected>"));

		//Only add a few options since the basic dropdown is not testing the scrollbar component
		for (UINT_TYPE i = 0; i < 4 && i < DropdownNumbers.size(); i++)
		{
			GuiDataElement<Int> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			basicDropdown->GetExpandMenuList()->AddOption(newEntry);
		}

		StageComponents.push_back(basicDropdown);
	}

	DropdownComponent* fullBasicDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(fullBasicDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach full basic dropdown to stage frame."));
	}
	else
	{
		fullBasicDropdown->SetSize(Vector2(256, 128));
		fullBasicDropdown->SetPosition(Vector2(48, 175));
		fullBasicDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, GuiTester, HandleFullBasicDropdownOptionClicked, void, DropdownComponent*, Int);

		for (UINT_TYPE i = 0; i < DropdownNumbers.size(); i++)
		{
			GuiDataElement<Int> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			fullBasicDropdown->GetExpandMenuList()->AddOption(newEntry);
		}

		fullBasicDropdown->SetSelectedItemByIdx(4);
		StageComponents.push_back(fullBasicDropdown);

		FrameComponent* overlapTest = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(overlapTest))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach a FrameComponent to the stage frame for the overlap dropdown test."));
		}
		else
		{
			overlapTest->SetPosition(fullBasicDropdown->ReadPosition() + (fullBasicDropdown->ReadSize() * Vector2(0.15f, 0.1f)));
			overlapTest->SetSize(Vector2(200.f, 160.f));
			overlapTest->SetCenterColor(Color(16, 16, 16, 96));
			if (BorderRenderComponent* borderComp = overlapTest->GetBorderComp())
			{
				borderComp->Destroy();
			}

			StageComponents.push_back(overlapTest);

			LabelComponent* overlapLabel = LabelComponent::CreateObject();
			if (!overlapTest->AddComponent(overlapLabel))
			{
				UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach a LabelComponent to the stage frame for the overlap dropdown test."));
			}
			else
			{
				overlapLabel->SetAutoRefresh(false);
				overlapLabel->SetPosition(Vector2::ZERO_VECTOR);
				overlapLabel->SetSize(Vector2(1.f, 1.f));
				overlapLabel->SetWrapText(true);
				overlapLabel->SetAutoSizeVertical(true);
				overlapLabel->SetText(TXT("Overlapping Component Test: Dropdown components move their expand components to the foreground when revealed. Even if other components in the same draw layer are rendered over the dropdown, the expand component should render above it."));
				overlapLabel->SetAutoRefresh(true);
			}
		}
	}

	ButtonBarDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(ButtonBarDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach button bar dropdown to stage frame."));
	}
	else
	{
		ButtonBarDropdown->SetSize(Vector2(256, 128));
		ButtonBarDropdown->SetPosition(Vector2(48, 384));
		ButtonBarDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, GuiTester, HandleButtonBarDropdownOptionClicked, void, DropdownComponent*, Int);

		for (size_t i = 0; i < ButtonBar.size(); ++i)
		{
			GuiDataElement<ButtonComponent*> newEntry;
			newEntry.Data = ButtonBar.at(i).Get();
			newEntry.LabelText = ButtonBar.at(i)->GetCaptionText();
			ButtonBarDropdown->GetExpandMenuList()->AddOption(newEntry);
		}

		ButtonBarDropdown->SetSelectedItemByIdx(0);
		StageComponents.push_back(ButtonBarDropdown.Get());
	}

	DropdownComponent* disabledDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(disabledDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach disabled dropdown to stage frame."));
	}
	else
	{
		disabledDropdown->SetSize(Vector2(256, 128));
		disabledDropdown->SetPosition(Vector2(48, 450));
		disabledDropdown->SetEnabled(false);

		GuiDataElement<DString> option1;
		option1.Data = TXT("Disabled Dropdown");
		option1.LabelText = option1.Data;
		disabledDropdown->GetExpandMenuList()->AddOption(option1);

		GuiDataElement<DString> option2;
		option2.Data = TXT("You should not be able to see this.");
		option2.LabelText = option2.Data;
		disabledDropdown->GetExpandMenuList()->AddOption(option2);

		disabledDropdown->SetSelectedItemByIdx(0);
		StageComponents.push_back(disabledDropdown);
	}

	//Testing if the dropdown shrinks in size for having very few options.
	DropdownComponent* shortDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(shortDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach short dropdown to stage frame."));
	}
	else
	{
		shortDropdown->SetSize(Vector2(256, 128));
		shortDropdown->SetPosition(Vector2(basicDropdown->ReadPosition().X + basicDropdown->ReadSize().X + 16.f, basicDropdown->ReadPosition().Y));

		for (size_t i = 0; i < 2; ++i)
		{
			GuiDataElement<Int> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			shortDropdown->GetExpandMenuList()->AddOption(newEntry);
		}

		shortDropdown->SetSelectedItemByIdx(0);
		StageComponents.push_back(shortDropdown);
	}

	//Testing searchable DropdownComponent
	DropdownComponent* searchableDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(searchableDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach searchable DropdownComponent to the stage frame."));
	}
	else
	{
		searchableDropdown->SetSize(Vector2(256.f, 128.f));
		searchableDropdown->SetPosition(Vector2(fullBasicDropdown->ReadPosition().X + fullBasicDropdown->ReadSize().X + 16.f, fullBasicDropdown->ReadPosition().Y));
		searchableDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, GuiTester, HandleSearchableDropdownOptionClicked, void, DropdownComponent*, Int);
		searchableDropdown->SetEnableSearchBar(true);

		std::vector<DString> options
		{
			TXT("Searchable Dropdown"),
			TXT("SearchableDropdown"),
			TXT("Search able Drop Down"),
			TXT("Sea arch Ha Abl edrop op Do own"),
			TXT("Search"),
			TXT("Sea"),
			TXT("See"),
			TXT("archable"),
			TXT(" search able"),
			TXT("Drop Searchable down"),
			TXT("downdrop"),
			TXT("search down"),
			TXT("dropsearch"),
			TXT("down down down search search"),
			TXT("s e a r c h a b l e d r o p d o w n"),
			TXT("Artorias"),
			TXT("Artorias Duskstride"),
			TXT("Art Dusk"),
			TXT("Art Stride"),
			TXT("Duskstride"),
			TXT("Dyllus Duskstorm"),
			TXT("Dyllus"),
			TXT("Storm")
		};

		for (size_t i = 0; i < options.size(); i++)
		{
			GuiDataElement<DString> newEntry;
			newEntry.Data = options.at(i);
			newEntry.LabelText = options.at(i);
			searchableDropdown->GetExpandMenuList()->AddOption(newEntry);
		}

		searchableDropdown->SetSelectedItemByIdx(0);
		
		StageComponents.push_back(searchableDropdown);
	}
}

void GuiTester::HandleVerticalListButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Displaying Vertical List!"));
	ClearStageComponents();

	ScrollbarComponent* scrollComp = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(scrollComp))
	{
		scrollComp->SetSize(Vector2(0.45f, 1.f));
		scrollComp->SetAnchorTop(0.01f);
		scrollComp->SetAnchorBottom(0.01f);
		scrollComp->SetAnchorLeft(0.01f);
		scrollComp->SetHideControlsWhenFull(true);
		StageComponents.push_back(scrollComp);
		
		GuiEntity* scrollableEntity = GuiEntity::CreateObject();
		scrollableEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
		scrollableEntity->SetAutoSizeVertical(true);
		scrollComp->SetViewedObject(scrollableEntity);

		VerticalListMainList = VerticalList::CreateObject();
		if (scrollableEntity->AddComponent(VerticalListMainList))
		{
			VerticalListMainList->SetPosition(Vector2::ZERO_VECTOR);
			VerticalListMainList->SetAnchorLeft(0.f);
			VerticalListMainList->SetAnchorRight(0.f);
			VerticalListMainList->ComponentSpacing = 6.f;

			if (TickComponent* refreshTick = VerticalListMainList->GetRefreshingTick())
			{
				//Disabled since we know the sub components will not adjust their sizes. Instead we can explicitly call refresh whenever a component is added or removed.
				refreshTick->SetTicking(false);
			}

			for (Int i = 1; i < 4; ++i)
			{
				ButtonComponent* button = ButtonComponent::CreateObject();
				if (VerticalListMainList->AddComponent(button))
				{
					button->SetAnchorLeft(0.05f);
					button->SetAnchorRight(0.05f);
					button->SetSize(Vector2(1.f, 16.f));
					button->SetCaptionText(TXT("Button ") + i.ToString());
					VerticalListButtons.push_back(button);
				}

				CheckboxComponent* checkbox = CheckboxComponent::CreateObject();
				if (VerticalListMainList->AddComponent(checkbox))
				{
					checkbox->SetAnchorLeft(0.05f);
					checkbox->SetAnchorRight(0.05f);
					checkbox->SetSize(Vector2(1.f, 16.f));
					checkbox->SetChecked(true);
					if (checkbox->GetCaptionComponent() != nullptr)
					{
						checkbox->GetCaptionComponent()->SetHorizontalAlignment(LabelComponent::HA_Center);
						checkbox->GetCaptionComponent()->SetText(TXT("Checkbox ") + i.ToString());
					}
					
					VerticalListCheckboxes.push_back(checkbox);
				}
			}

			VerticalListMainList->RefreshComponentTransforms();
		}
	}

	Float compSpacing = 0.01f;
	Vector2 compPos(Vector2(0.f, compSpacing));
	Vector2 compSize(0.33f, 0.075f);
	CheckboxComponent* toggleButtonVis = CheckboxComponent::CreateObject();
	if (StageFrame->AddComponent(toggleButtonVis))
	{
		toggleButtonVis->SetPosition(compPos);
		toggleButtonVis->SetSize(compSize);
		toggleButtonVis->SetAnchorRight(compSpacing);
		toggleButtonVis->SetChecked(true);
		toggleButtonVis->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleVertListToggleButtonVisibility, void, CheckboxComponent*);
		if (toggleButtonVis->GetCaptionComponent() != nullptr)
		{
			toggleButtonVis->GetCaptionComponent()->SetText(TXT("Toggle Button Visibility"));
		}

		StageComponents.push_back(toggleButtonVis);
		compPos.Y += compSize.Y + compSpacing;
	}

	CheckboxComponent* toggleCheckboxVis = CheckboxComponent::CreateObject();
	if (StageFrame->AddComponent(toggleCheckboxVis))
	{
		toggleCheckboxVis->SetPosition(compPos);
		toggleCheckboxVis->SetSize(compSize);
		toggleCheckboxVis->SetAnchorRight(compSpacing);
		toggleCheckboxVis->SetChecked(true);
		toggleCheckboxVis->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleVertListToggleCheckboxVisibility, void, CheckboxComponent*);
		if (toggleCheckboxVis->GetCaptionComponent() != nullptr)
		{
			toggleCheckboxVis->GetCaptionComponent()->SetText(TXT("Toggle Checkbox Visibility"));
		}

		StageComponents.push_back(toggleCheckboxVis);
		compPos.Y += compSize.Y + compSpacing;
	}

	compPos.Y += compSpacing;

	{
		LabelComponent* buttonPrompt = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(buttonPrompt))
		{
			buttonPrompt->SetAutoRefresh(false);
			buttonPrompt->SetPosition(compPos);
			buttonPrompt->SetSize(compSize);
			buttonPrompt->SetAnchorRight(compSpacing);
			buttonPrompt->SetHorizontalAlignment(LabelComponent::HA_Center);
			buttonPrompt->SetWrapText(true);
			buttonPrompt->SetClampText(true);
			buttonPrompt->SetText(TXT("Add/Remove Button"));
			buttonPrompt->SetAutoRefresh(true);

			StageComponents.push_back(buttonPrompt);
			compPos.Y += compSize.Y + compSpacing;
		}

		ButtonComponent* addButton = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(addButton))
		{
			addButton->SetPosition(compPos);
			addButton->SetSize(compSize * Vector2(0.25f, 1.f));
			addButton->SetAnchorRight((compSpacing * 2.f) + addButton->ReadSize().X);
			addButton->SetCaptionText(TXT("+"));
			addButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleVertListAddButton, void, ButtonComponent*));
			StageComponents.push_back(addButton);
		}

		ButtonComponent* removeButton = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(removeButton))
		{
			removeButton->SetPosition(compPos);
			removeButton->SetSize(compSize * Vector2(0.25f, 1.f));
			removeButton->SetAnchorRight(compSpacing);
			removeButton->SetCaptionText(TXT("-"));
			removeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleVertListRemoveButton, void, ButtonComponent*));
			StageComponents.push_back(removeButton);
			compPos.Y += compSize.Y + (compSpacing * 2.f);
		}
	}

	{
		LabelComponent* checkboxPrompt = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(checkboxPrompt))
		{
			checkboxPrompt->SetAutoRefresh(false);
			checkboxPrompt->SetPosition(compPos);
			checkboxPrompt->SetSize(compSize);
			checkboxPrompt->SetAnchorRight(compSpacing);
			checkboxPrompt->SetHorizontalAlignment(LabelComponent::HA_Center);
			checkboxPrompt->SetWrapText(true);
			checkboxPrompt->SetClampText(true);
			checkboxPrompt->SetText(TXT("Add/Remove Checkbox"));
			checkboxPrompt->SetAutoRefresh(true);

			StageComponents.push_back(checkboxPrompt);
			compPos.Y += compSize.Y + compSpacing;
		}

		ButtonComponent* addCheckbox = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(addCheckbox))
		{
			addCheckbox->SetPosition(compPos);
			addCheckbox->SetSize(compSize * Vector2(0.25f, 1.f));
			addCheckbox->SetAnchorRight((compSpacing * 2.f) + addCheckbox->ReadSize().X);
			addCheckbox->SetCaptionText(TXT("+"));
			addCheckbox->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleVertListAddCheckbox, void, ButtonComponent*));
			StageComponents.push_back(addCheckbox);
		}

		ButtonComponent* removeCheckbox = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(removeCheckbox))
		{
			removeCheckbox->SetPosition(compPos);
			removeCheckbox->SetSize(compSize * Vector2(0.25f, 1.f));
			removeCheckbox->SetAnchorRight(compSpacing);
			removeCheckbox->SetCaptionText(TXT("-"));
			removeCheckbox->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleVertListRemoveCheckbox, void, ButtonComponent*));
			StageComponents.push_back(removeCheckbox);
			compPos.Y += compSize.Y + (compSpacing * 2.f);
		}
	}
}

void GuiTester::HandleTooltipButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Tooltips!"));
	ClearStageComponents();

	Vector2 buttonSize(0.2f, 0.2f);
	Float centerAnchor = 0.5f - (buttonSize.X * 0.5f);
	std::function<ButtonComponent*()> createButton([&]()
	{
		ButtonComponent* newButton = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(newButton))
		{
			newButton->SetSize(buttonSize);
			if (newButton->GetCaptionComponent() != nullptr)
			{
				newButton->GetCaptionComponent()->Destroy();
			}

			StageComponents.push_back(newButton);
		}

		return newButton;
	});

	Float revealDelay = 0.67f;
	if (ButtonComponent* top = createButton())
	{
		top->SetAnchorLeft(centerAnchor);
		top->SetAnchorTop(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (top->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This tooltip should appear below the mouse.\n\
since this tooltip\n\
has many lines.\n\
Many lines would cause\n\
it to scale vertically.\n\
And when it scales\n\
vertically, it may\n\
have to shift below\n\
since there's not enough\n\
space to render above it."));
		}
	}
	
	if (ButtonComponent* topRight = createButton())
	{
		topRight->SetAnchorRight(0.f);
		topRight->SetAnchorTop(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (topRight->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This tooltip has a low move threshold.\n\
Slightly moving the mouse would cause\n\
the tooltip to hide."));
			tooltip->HideMoveThreshold = 2.f;
		}
	}

	if (ButtonComponent* right = createButton())
	{
		right->SetAnchorRight(0.f);
		right->SetAnchorTop(centerAnchor);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (right->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This wide tooltip should shift to the left since there isn't enough space to render the text normally."));
		}
	}

	if (ButtonComponent* bottomRight = createButton())
	{
		bottomRight->SetAnchorRight(0.f);
		bottomRight->SetAnchorBottom(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (bottomRight->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This tooltip has a high move threshold.\n\
Once the tooltip shows itself, it will not hide\n\
until the mouse is no longer hovering over the button."));
			tooltip->HideMoveThreshold = 9999.f;

		}
	}

	if (ButtonComponent* bottom = createButton())
	{
		bottom->SetAnchorLeft(centerAnchor);
		bottom->SetAnchorBottom(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (bottom->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This is a tall tooltip\n\
that should render\n\
above the mouse pointer.\n\
Line A\n\
Line B\n\
Line C\n\
\n\
This should also be\n\
centered horizontally\n\
over the mouse."));
		}
	}

	if (ButtonComponent* bottomLeft = createButton())
	{
		bottomLeft->SetAnchorLeft(0.f);
		bottomLeft->SetAnchorBottom(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (bottomLeft->AddComponent(tooltip))
		{
			tooltip->SetTooltipText(TXT("This tooltip should\n\
reveal itself as soon as the\n\
mouse hovers over it."));
			tooltip->RevealDelay = 0.f;
			tooltip->ShowMoveThreshold = 0.f;
		}
	}

	if (ButtonComponent* left = createButton())
	{
		left->SetAnchorLeft(0.f);
		left->SetAnchorTop(centerAnchor);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (left->AddComponent(tooltip))
		{
			tooltip->RevealDelay = revealDelay;
			tooltip->SetTooltipText(TXT("This wide tooltip should shift to the right since there isn't enough space to render horizontally centered."));
		}
	}

	if (ButtonComponent* topLeft = createButton())
	{
		topLeft->SetAnchorLeft(0.f);
		topLeft->SetAnchorTop(0.f);

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (topLeft->AddComponent(tooltip))
		{
			tooltip->SetTooltipText(TXT("This tooltip has a long\n\
delay before it reveals itself\n\
(2 second reveal delay)."));
			tooltip->RevealDelay = 2.f;
		}
	}

	//Run scrollbar test
	ScrollbarComponent* scrollbar = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(scrollbar))
	{
		scrollbar->SetPosition(Vector2(0.33f, 0.33f));
		scrollbar->SetSize(Vector2(0.33f, 0.33f));
		StageComponents.push_back(scrollbar);

		GuiEntity* viewedObj = GuiEntity::CreateObject();
		viewedObj->SetAutoSizeHorizontal(true);
		viewedObj->SetAutoSizeVertical(true);
		scrollbar->SetViewedObject(viewedObj);

		Vector2 scrollbarButtonSize(128.f, 96.f);
		Float buttonSpacing = 32.f;
		Vector2 pos(Vector2::ZERO_VECTOR);
		ButtonComponent* topLeft = ButtonComponent::CreateObject();
		if (viewedObj->AddComponent(topLeft))
		{
			topLeft->SetPosition(pos);
			topLeft->SetSize(scrollbarButtonSize);
			if (topLeft->GetCaptionComponent() != nullptr)
			{
				topLeft->GetCaptionComponent()->Destroy();
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (topLeft->AddComponent(tooltip))
			{
				tooltip->RevealDelay = revealDelay;
				tooltip->SetTooltipText(TXT("Each component may have their own tooltip component\n\
to specify the text, reveal duration, and move thresholds.\n\
However the tooltips are rendered in an external GuiEntity\n\
so it could be rendered beyond the borders and above others.\n\
\n\
The GuiDrawLayer has a special variable to manage the tooltip entity."));
			}
		}

		ButtonComponent* topRight = ButtonComponent::CreateObject();
		if (viewedObj->AddComponent(topRight))
		{
			topRight->SetPosition(Vector2(scrollbarButtonSize.X + buttonSpacing, 0.f));
			topRight->SetSize(scrollbarButtonSize);
			if (topRight->GetCaptionComponent() != nullptr)
			{
				topRight->GetCaptionComponent()->Destroy();
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (topRight->AddComponent(tooltip))
			{
				tooltip->RevealDelay = revealDelay;
				tooltip->SetTooltipText(TXT("Tooltip components are weird. They do not act like standalone instances primarily\n\
because these can be rendered outside the owning component's boundaries.\n\
And there should only be at most one tooltip visible at a time."));
			}
		}

		ButtonComponent* bottomLeft = ButtonComponent::CreateObject();
		if (viewedObj->AddComponent(bottomLeft))
		{
			bottomLeft->SetPosition(Vector2(0.f, scrollbarButtonSize.Y + buttonSpacing));
			bottomLeft->SetSize(scrollbarButtonSize);
			if (bottomLeft->GetCaptionComponent() != nullptr)
			{
				bottomLeft->GetCaptionComponent()->Destroy();
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (bottomLeft->AddComponent(tooltip))
			{
				tooltip->RevealDelay = revealDelay;
				tooltip->SetTooltipText(TXT("Tooltips even render outside the scrollbar's rendered textures.\n\
For this to work, each tooltip component climbs up the entity owner chain.\n\
From there, they search for the RenderTarget that is the Window handle, itself."));
			}
		}

		ButtonComponent* bottomRight = ButtonComponent::CreateObject();
		if (viewedObj->AddComponent(bottomRight))
		{
			bottomRight->SetPosition(Vector2(scrollbarButtonSize + Vector2(buttonSpacing, buttonSpacing)));
			bottomRight->SetSize(scrollbarButtonSize);
			if (bottomRight->GetCaptionComponent() != nullptr)
			{
				bottomRight->GetCaptionComponent()->Destroy();
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (bottomRight->AddComponent(tooltip))
			{
				tooltip->RevealDelay = revealDelay;
				tooltip->SetTooltipText(TXT("Typically each GuiDrawLayer instance that is registered to a window\n\
should initialize their own TooltipGuiEntity in order for each\n\
TooltipComponent to be able to find the correct instance."));
			}
		}
	}
}

void GuiTester::HandleGuiEntityButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying GuiEntities!"));
	ClearStageComponents();

	TestMenuButtonList* buttonTest = TestMenuButtonList::CreateObject();
	CHECK(buttonTest != nullptr)
	buttonTest->SetPosition(Vector2(16, 64));
	buttonTest->SetSize(Vector2(256, 256));
	GuiEntities.push_back(buttonTest);

	TestMenuFocusInterface* focusTest = TestMenuFocusInterface::CreateObject();
	CHECK(focusTest != nullptr)
	focusTest->SetSize(Vector2(256, 256));
	focusTest->SetPosition(Vector2(256, 96));
	GuiEntities.push_back(focusTest);

	//Create UI elements that adjusts the focus
	LabelComponent* setFocusLabel = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(setFocusLabel))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach set focus label component to stage frame."));
	}
	else
	{
		setFocusLabel->SetSize(Vector2(75.f, 16.f));
		setFocusLabel->SetPosition(Vector2::ZERO_VECTOR);
		setFocusLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
		setFocusLabel->SetVerticalAlignment(LabelComponent::VA_Top);
		setFocusLabel->SetText(TXT("Set Focus"));
		StageComponents.push_back(setFocusLabel);

		SetFocusDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(SetFocusDropdown))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach set focus dropdown component to stage frame."));
		}
		else
		{
			SetFocusDropdown->SetSize(Vector2(256.f, 256.f));
			SetFocusDropdown->SetPosition(setFocusLabel->ReadPosition() + Vector2(setFocusLabel->ReadCachedAbsSize().X + 16.f, 0.f));
			SetFocusDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, GuiTester, HandleFocusChange, void, DropdownComponent*, Int);
			for (UINT_TYPE i = 0; i < GuiEntities.size(); i++)
			{
				GuiDataElement<GuiEntity*> dropdownEntry(GuiEntities.at(i).Get(), GuiEntities.at(i)->GetName());
				SetFocusDropdown->GetExpandMenuList()->AddOption(dropdownEntry);
			}

			FocusedGuiEntityIdx = 0;
			SetFocusDropdown->SetSelectedItemByIdx(FocusedGuiEntityIdx);
			GuiEntities.at(FocusedGuiEntityIdx)->GainFocus();
			StageComponents.push_back(SetFocusDropdown.Get());
		}
	}
}

void GuiTester::HandleHoverButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying components with Hover Components!"));
	ClearStageComponents();

	FrameComponent* frame = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(frame))
	{
		frame->SetPosition(Vector2::ZERO_VECTOR);
		frame->SetSize(Vector2(128, 128));
		frame->SetLockedFrame(false);
		StageComponents.push_back(frame);

		HoverComponent* frameHover = HoverComponent::CreateObject();
		if (frame->AddComponent(frameHover))
		{
			frameHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverFrameRolledOver, void, MousePointer*, Entity*);
			frameHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverFrameRolledOut, void, MousePointer*, Entity*);
		}
	}

	HoverLabel = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(HoverLabel))
	{
		HoverLabel->SetSize(Vector2(128, 16));
		//HoverLabel->SnapBottom();
		//HoverLabel->SnapCenterHorizontally();
		HoverLabel->SetText(TXT("Hover over me"));
		StageComponents.push_back(HoverLabel.Get());

		HoverComponent* labelHover = HoverComponent::CreateObject();
		if (HoverLabel->AddComponent(labelHover))
		{
			labelHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverLabelRolledOver, void, MousePointer*, Entity*);
			labelHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverLabelRolledOut, void, MousePointer*, Entity*);
		}
	}

	HoverButton = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(HoverButton))
	{
		HoverButton->SetSize(Vector2(128, 16));
		//HoverButton->SnapRight();
		//HoverButton->SnapTop();
		HoverButton->SetCaptionText(TXT("Hover over me"));
		StageComponents.push_back(HoverButton.Get());

		HoverComponent* buttonHover = HoverComponent::CreateObject();
		if (HoverButton->AddComponent(buttonHover))
		{
			buttonHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverButtonRolledOver, void, MousePointer*, Entity*);
			buttonHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverButtonRolledOut, void, MousePointer*, Entity*);
		}
	}
}

void GuiTester::HandleTreeListButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Tree List Components!"));
	ClearStageComponents();

	TreeListClassBrowser = TreeListComponent::CreateObject();
	if (StageFrame->AddComponent(TreeListClassBrowser))
	{
		TreeListClassBrowser->SetPosition(Vector2::ZERO_VECTOR);
		TreeListClassBrowser->SetSize(Vector2(1.f, 1.f));
		TreeListClassBrowser->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleTreeListClassSelected, void, Int);

		//Populate tree data
		std::vector<TreeListComponent::SDataBranch*> branches;
		for (ClassIterator iter(Object::SStaticClass()); iter.SelectedClass != nullptr; iter++)
		{
			TreeListComponent::SDataBranch* curBranch = new TreeListComponent::SDataBranch(new GuiDataElement<const DClass*>(iter.SelectedClass, iter.SelectedClass->ToString()));

			//Find parent branch
			if (iter.SelectedClass->GetSuperClass() != nullptr)
			{
				for (UINT_TYPE i = 0; i < branches.size(); i++)
				{
					GuiDataElement<const DClass*>* otherBranch = dynamic_cast<GuiDataElement<const DClass*>*>(branches.at(i)->Data);
					CHECK(otherBranch != nullptr)
					if (otherBranch->Data == iter.SelectedClass->GetSuperClass())
					{
						TreeListComponent::SDataBranch::LinkBranches(*branches.at(i), *curBranch);
						break;
					}
				}
			}

			branches.push_back(curBranch);
		}

		//Swap the first 10 elements with the last 10 elements to briefly test the sorting function
		for (UINT_TYPE i = 0; i < 10; i++)
		{
			TreeListComponent::SDataBranch* otherBranch = branches.at(branches.size() - 1 - i);
			branches.at(branches.size() - 1 - i) = branches.at(i);
			branches.at(i) = otherBranch;
		}

		//If this function does not correctly sort this tree list, then the tree will not correctly display on UI
		TreeListComponent::SortDataList(branches);

		TreeListClassBrowser->SetTreeList(branches);
		TreeListClassBrowser->ExpandAll();

		//Select own class
		//TreeListClassBrowser->SetSelectedItem<const DClass*>(GuiTester::SStaticClass()); //TODO: uncomment after resolving circular dependency between TreeListComponent and TreeListIterator.
		StageComponents.push_back(TreeListClassBrowser.Get());
	}
}

void GuiTester::HandleCloseClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("End UI button was clicked.  Concluding UI test."));

	if (TestWindowHandle.IsValid())
	{
		TestWindowHandle->Destroy();
	}

	Destroy();
}

void GuiTester::HandleNextPageClicked (ButtonComponent* uiComponent)
{
	if (++ButtonBarPageIdx > (Int(ButtonBar.size()) / MaxButtonsPerPage))
	{
		ButtonBarPageIdx = 0;
	}

	UpdateButtonBarPageIdx();
}

void GuiTester::HandlePrevPageClicked (ButtonComponent* uiComponent)
{
	if (--ButtonBarPageIdx < 0)
	{
		ButtonBarPageIdx = Int(ButtonBar.size()) / MaxButtonsPerPage;
	}

	UpdateButtonBarPageIdx();
}

void GuiTester::HandleButtonTestPressed (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("Button \"%s\" was pressed."), uiComponent->GetCaptionText());
}

void GuiTester::HandleButtonTestReleased (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
}

void GuiTester::HandleButtonTestToggleReleased (ButtonComponent* uiComponent)
{
	if (ToggleButtons[0] == uiComponent)
	{
		ToggleButtons[0]->SetEnabled(false);
		ToggleButtons[1]->SetEnabled(true);
	}
	else
	{
		ToggleButtons[0]->SetEnabled(true);
		ToggleButtons[1]->SetEnabled(false);
	}

	HandleButtonTestReleased(uiComponent);
}

void GuiTester::HandleButtonTestIncrementCounter (ButtonComponent* uiComponent)
{
	++ButtonTestCounter;
	if (uiComponent != nullptr)
	{
		uiComponent->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
	}

	HandleButtonTestReleased(uiComponent);
}

void GuiTester::HandleButtonTestDecrementCounter (ButtonComponent* uiComponent)
{
	--ButtonTestCounter;
	if (uiComponent != nullptr)
	{
		uiComponent->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
	}
}

void GuiTester::HandleCheckboxToggle (CheckboxComponent* uiComponent)
{
	if (uiComponent->GetChecked())
	{
		UnitTester::TestLog(TestFlags, TXT("The Checkbox (%s) is checked."), uiComponent->GetCaptionComponent()->GetContent());
	}
	else
	{
		UnitTester::TestLog(TestFlags, TXT("The Checkbox (%s) is no longer checked."), uiComponent->GetCaptionComponent()->GetContent());
	}
}

void GuiTester::HandleToggleEnableCheckboxToggle (CheckboxComponent* uiComponent)
{
	CHECK(EnableToggleCheckbox.IsValid() && TargetTestCheckbox.IsValid())
	TargetTestCheckbox->SetEnabled(EnableToggleCheckbox->GetChecked());
}

void GuiTester::HandleToggleVisibilityCheckboxToggle (CheckboxComponent* uiComponent)
{
	CHECK(VisibilityToggleCheckbox.IsValid() && TargetTestCheckbox.IsValid())
	TargetTestCheckbox->SetVisibility(VisibilityToggleCheckbox->GetChecked());
}

void GuiTester::HandleCloseContextMenu (ButtonComponent* uiComponent)
{
	GuiEntity* rootEntity = dynamic_cast<GuiEntity*>(uiComponent->GetRootEntity());

	while (rootEntity != nullptr)
	{
		if (ScrollbarComponent* owningScrollbar = rootEntity->GetScrollbarOwner())
		{
			rootEntity = dynamic_cast<GuiEntity*>(owningScrollbar->GetRootEntity());
			continue;
		}

		break;
	}

	if (rootEntity != nullptr)
	{
		rootEntity->Destroy();
	}
}

void GuiTester::HandleDynamicListMenuCmd (BaseGuiDataElement* data)
{
	if (GuiDataElement<DString>* stringData = dynamic_cast<GuiDataElement<DString>*>(data))
	{
		GuiLog.Log(LogCategory::LL_Debug, TXT("The Dynamic List Menu executed a command associated with a GuiDataElement. The data element reads: \"%s\"."), stringData->Data);
	}
}

void GuiTester::HandleScrollbarTesterLabelTestClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid() && ScrollbarTester.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > label test!"));
	ResetScrollbarTest();

	LabelComponent* testLabel = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(testLabel))
	{
		ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
		ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
		ScrollbarTesterViewedObject->SetSize(1.f, 1.f);
		ScrollbarTesterViewedObject->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));

		testLabel->SetAutoRefresh(false);
		testLabel->SetSize(Vector2(1.f, 1.f));
		testLabel->SetAutoSizeVertical(true);
		testLabel->SetCharacterSize(24);
		testLabel->SetLineSpacing(12.f);
		testLabel->SetWrapText(true);
		testLabel->SetClampText(false);
		testLabel->SetText(TXT("He was...a hero. Not the hero we deserved - the hero we needed. Nothing less than a knight, shining... \
\nThey'll have to hunt you. \
\nYou'll hunt me.  You'll condemn me.  Set the dogs on me.  Because that what needs to happen.  Because sometimes truth isn't good enough.  Sometimes people deserve more.  Sometimes people deserve their faith rewarded. \
\n \
\n \
\nWhy is he running, Dad? \
\nBecause we have to chase him. \
\nHe didn't anything wrong. \
\nBecause he's the hero Gotham deserves, but not the one it needs right now. So we'll hunt him. Because he can take it. Because he's not our hero. He's a silent guardian. A watchful protector. A Dark Knight. \
\n \
\n\t-The Dark Knight"));
		testLabel->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(testLabel);
	}
}

void GuiTester::HandleScrollbarTesterButtonTestClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > button test!"));
	ResetScrollbarTest();

	const Vector2 buttonSize(165.f, 32.f);
	const Float buttonSpacing(8.f);
	const Int numRows(12);
	const Int numCols(8);

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(((buttonSize.X + buttonSpacing) * numCols.ToFloat() + buttonSpacing), ((buttonSize.Y + buttonSpacing) * numRows.ToFloat()) + buttonSpacing);

	Int buttonIdx = 0;
	for (Int row = 0; row < numRows; row++)
	{
		for (Int col = 0; col < numCols; col++)
		{
			ButtonComponent* newButton = ButtonComponent::CreateObject();
			if (ScrollbarTesterViewedObject->AddComponent(newButton))
			{
				newButton->SetPosition((buttonSize.X * col.ToFloat()) + buttonSpacing * (col + 1).ToFloat(), (buttonSize.Y * row.ToFloat()) + buttonSpacing * (row + 1).ToFloat());
				newButton->SetSize(buttonSize);
				newButton->SetCaptionText(TXT("Scrollable Button ") + buttonIdx.ToString());
				newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterTempButtonClicked, void, ButtonComponent*));
				ScrollbarTestObjects.push_back(newButton);
				buttonIdx++;
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterTempButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  \"%s\" release event invoked."), uiComponent->GetCaptionText());
}

void GuiTester::HandleScrollbarTesterSizeChangeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > adjustable size test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);

	CheckboxComponent* hideWhenFullCheckbox = CheckboxComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(hideWhenFullCheckbox))
	{
		hideWhenFullCheckbox->SetAnchorLeft(8.f);
		hideWhenFullCheckbox->SetAnchorTop(8.f);
		hideWhenFullCheckbox->SetSize(Vector2(256.f, 24.f));

		if (hideWhenFullCheckbox->GetCaptionComponent() != nullptr)
		{
			hideWhenFullCheckbox->GetCaptionComponent()->SetText(TXT("Hide Scrollbar When Full"));
		}

		if (ScrollbarTester.IsValid())
		{
			hideWhenFullCheckbox->SetChecked(ScrollbarTester->GetHideControlsWhenFull());
		}

		hideWhenFullCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHideWhenFull, void, CheckboxComponent*);
		ScrollbarTestObjects.push_back(hideWhenFullCheckbox);
	}

	ScrollbarTesterSizeChangeFrame = FrameComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterSizeChangeFrame))
	{
		ScrollbarTesterSizeChangeFrame->SetAnchorLeft(8.f);
		ScrollbarTesterSizeChangeFrame->SetAnchorTop(96.f);
		ScrollbarTesterSizeChangeFrame->SetLockedFrame(true);
		ScrollbarTesterSizeChangeFrame->SetBorderThickness(4.f);
		ScrollbarTesterSizeChangeFrame->SetSize(512.f, 512.f);
		ScrollbarTesterSizeChangeFrame->SetEnableFractionScaling(false);
		ScrollbarTestObjects.push_back(ScrollbarTesterSizeChangeFrame.Get());

		TickComponent* frameTick = TickComponent::CreateObject(TICK_GROUP_GUI);
		if (ScrollbarTesterSizeChangeFrame->AddComponent(frameTick))
		{
			frameTick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterSizeChangerTick, void, Float));
		}
	}

	ScrollbarTesterDecreaseSizeButton = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterDecreaseSizeButton))
	{
		ScrollbarTesterDecreaseSizeButton->SetPosition(8.f, 40.f);
		ScrollbarTesterDecreaseSizeButton->SetSize(Vector2(32.f, 32.f));
		ScrollbarTesterDecreaseSizeButton->SetCaptionText(TXT("-"));
		ScrollbarTesterDecreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterDecreaseSizeClicked, void, ButtonComponent*));

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(ScrollbarTesterDecreaseSizeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		ScrollbarTestObjects.push_back(ScrollbarTesterDecreaseSizeButton.Get());
	}

	ScrollbarTesterIncreaseSizeButton = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterIncreaseSizeButton))
	{
		ScrollbarTesterIncreaseSizeButton->SetPosition(48.f, 40.f);
		ScrollbarTesterIncreaseSizeButton->SetSize(Vector2(32.f, 32.f));
		ScrollbarTesterIncreaseSizeButton->SetCaptionText(TXT("+"));
		ScrollbarTesterIncreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterIncreaseSizeClicked, void, ButtonComponent*));

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(ScrollbarTesterIncreaseSizeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		ScrollbarTestObjects.push_back(ScrollbarTesterIncreaseSizeButton.Get());
	}
}

void GuiTester::HandleScrollbarTesterToggleHideWhenFull (CheckboxComponent* uiComponent)
{
	if (ScrollbarTester.IsValid())
	{
		ScrollbarTester->SetHideControlsWhenFull(uiComponent->GetChecked());
	}
}

void GuiTester::HandleScrollbarTesterDecreaseSizeClicked (ButtonComponent* uiComponent)
{
	const Float sizeDecreaseAmount(8.f);
	if (ScrollbarTesterSizeChangeFrame.IsValid())
	{
		Vector2 newSize = ScrollbarTesterSizeChangeFrame->GetSize();
		newSize.X -= sizeDecreaseAmount;
		newSize.Y -= sizeDecreaseAmount;

		newSize.X = Utils::Max(newSize.X, sizeDecreaseAmount);
		newSize.Y = Utils::Max(newSize.Y, sizeDecreaseAmount);

		ScrollbarTesterSizeChangeFrame->SetSize(newSize);
	}
}

void GuiTester::HandleScrollbarTesterIncreaseSizeClicked (ButtonComponent* uiComponent)
{
	const Float sizeIncreaseAmount(8.f);
	if (ScrollbarTesterSizeChangeFrame.IsValid())
	{
		Vector2 newSize = ScrollbarTesterSizeChangeFrame->GetSize();
		newSize.X += sizeIncreaseAmount;
		newSize.Y += sizeIncreaseAmount;

		ScrollbarTesterSizeChangeFrame->SetSize(newSize);
	}
}

void GuiTester::HandleScrollbarTesterSizeChangerTick (Float deltaSec)
{
	if (ScrollbarTester.IsNullptr() || ScrollbarTester->GetFrameCamera() == nullptr)
	{
		return;
	}

	const Vector2 frameTopLeftCorner = (ScrollbarTester->GetFrameCamera()->ReadPosition() - (ScrollbarTester->GetFrameCamera()->ReadViewExtents() * 0.5f));

	//Reposition the buttons to be attach to the top left corner of camera
	if (ScrollbarTesterDecreaseSizeButton.IsValid())
	{
		ScrollbarTesterDecreaseSizeButton->SetPosition(Vector2(8.f, 40.f) + frameTopLeftCorner);
	}

	if (ScrollbarTesterIncreaseSizeButton.IsValid())
	{
		ScrollbarTesterIncreaseSizeButton->SetPosition(Vector2(48.f, 40.f) + frameTopLeftCorner);
	}
}

void GuiTester::HandleScrollbarTesterDifferentFrameSizeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid() && ScrollbarTester.IsValid() && ScrollbarTester->GetFrameCamera() != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > Different Frame Size test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameCamera()->ReadViewExtents());

	ButtonComponent* toggleHorizontal = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(toggleHorizontal))
	{
		toggleHorizontal->SetAnchorTop(8.f);
		toggleHorizontal->SetAnchorLeft(8.f);
		toggleHorizontal->SetSize(Vector2(128.f, 48.f));
		toggleHorizontal->SetCaptionText(TXT("Toggle Horizontal Size"));
		toggleHorizontal->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHalfHorizontalButtonClicked, void, ButtonComponent*));
		ScrollbarTestObjects.push_back(toggleHorizontal);
	}

	ButtonComponent* toggleVertical = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(toggleVertical))
	{
		CHECK(toggleHorizontal != nullptr)
		toggleVertical->SetAnchorTop(8.f);
		toggleVertical->SetAnchorLeft(16.f + toggleHorizontal->ReadSize().X);
		toggleVertical->SetSize(Vector2(128.f, 48.f));
		toggleVertical->SetCaptionText(TXT("Toggle Vertical Size"));
		toggleVertical->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHalfVertialButtonClicked, void, ButtonComponent*));
		ScrollbarTestObjects.push_back(toggleVertical);
	}

	LabelComponent* label = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(label))
	{
		label->SetAutoRefresh(false);
		label->SetSize(ScrollbarTester->GetFrameSpriteTransform()->ReadCachedAbsSize());
		label->SetAutoSizeHorizontal(false);
		label->SetAutoSizeVertical(true);
		label->SetPosition(Vector2(0.f, 64.f));
		label->SetWrapText(true);
		label->SetClampText(false);
		label->SetText(TXT("Look at the state we're in \n \
This docile apathy \n \
We fight and stumble aimlessly \n \
\n \
We broke our promises \n \
One too many times \n \
\n \
When hope is a ghost \n \
Fear survive \n \
\n \
I'm just gonna sit here \n \
I'm just gonna stay here for awhile. \n \
Need to catch my breath here \n \
What do we do now \n \
\n \
Look at the empty stares \n \
of once mighty men \n \
Fallen and fragile \n \
so unsure \n \
\n \
Who do we look to now \n \
How can we trust again \n \
When hope is a ghost \n \
Fear survives \n \
\n \
I'm just gonna sit here \n \
I'm just gonna stay here for awhile \n \
Need to catch my breath here \n \
What do we do now \n \
\n \
\n \
Hope Is a Ghost - Angelflare\n "));
		label->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(label);
	}

	//Clear anchors so its size may be adjusted
	ScrollbarTester->SetAnchorTop(-1.f);
	ScrollbarTester->SetAnchorRight(-1.f);
	ScrollbarTester->SetAnchorBottom(-1.f);
	ScrollbarTester->SetSize(0.75f, 0.8f);
	ScrollbarTester->SetHideControlsWhenFull(true);
}

void GuiTester::HandleScrollbarTesterToggleHalfHorizontalButtonClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTester.IsValid())

	Vector2 newSize(ScrollbarTester->ReadSize());
	newSize.X = (ScrollbarTester->ReadSize().X >= 0.75f) ? 0.375f : 0.75f;

	ScrollbarTester->SetSize(newSize);
}

void GuiTester::HandleScrollbarTesterToggleHalfVertialButtonClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTester.IsValid())

	Vector2 newSize(ScrollbarTester->ReadSize());
	newSize.Y = (ScrollbarTester->ReadSize().Y >= 0.8f) ? 0.4f : 0.8f;

	ScrollbarTester->SetSize(newSize);
}

void GuiTester::HandleScrollbarTesterOuterSizeChangeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid() && ScrollbarTester.IsValid() && ScrollbarTester->GetFrameCamera() != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > outer size change test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameCamera()->ReadViewExtents());

	FrameComponent* lockedFrame = FrameComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(lockedFrame))
	{
		lockedFrame->SetPosition(Vector2::ZERO_VECTOR);
		lockedFrame->SetSize(Vector2(1.f, 1.f));
		lockedFrame->SetBorderThickness(1.f);
		lockedFrame->SetLockedFrame(true);
		ScrollbarTestObjects.push_back(lockedFrame);

		FrameComponent* adjustableFrame = FrameComponent::CreateObject();
		if (lockedFrame->AddComponent(adjustableFrame))
		{
			adjustableFrame->SetPosition(Vector2(0.25f, 0.25f));
			adjustableFrame->SetSize(Vector2(0.5f, 0.5f));
			adjustableFrame->SetBorderThickness(6.f);
			adjustableFrame->SetMinDragSize(Vector2(48.f, 48.f));
			adjustableFrame->SetLockedFrame(false);

			ScrollbarComponent* innerScrollbar = ScrollbarComponent::CreateObject();
			if (adjustableFrame->AddComponent(innerScrollbar))
			{
				innerScrollbar->SetAnchorTop(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorRight(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorBottom(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorLeft(adjustableFrame->GetBorderThickness());

				GuiEntity* innerEntity = GuiEntity::CreateObject();
				innerEntity->SetAutoSizeHorizontal(false);
				innerEntity->SetAutoSizeHorizontal(false);
				innerEntity->SetPosition(Vector2::ZERO_VECTOR);
				innerEntity->SetSize(1024.f, 1024.f);
				innerScrollbar->SetViewedObject(innerEntity);

				PlanarTransformComponent* innerSpriteTransform = PlanarTransformComponent::CreateObject();
				if (innerEntity->AddComponent(innerSpriteTransform))
				{
					innerSpriteTransform->SetPosition(Vector2::ZERO_VECTOR);
					innerSpriteTransform->SetSize(Vector2(1.f, 1.f));

					SpriteComponent* innerSprite = SpriteComponent::CreateObject();
					if (innerSpriteTransform->AddComponent(innerSprite))
					{
						TexturePool* localTexturePool = TexturePool::FindTexturePool();
						CHECK(localTexturePool != nullptr)

						innerSprite->SetSpriteTexture(localTexturePool->GetTexture(HashedString(TXT("Engine.Gui.FrameComponentFill"))));
					}
				}
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterNestedScrollbarsClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > nested scrollbar test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(Vector2(1.f, 1.f));
	ScrollbarTesterViewedObject->SetGuiSizeToOwningScrollbar(Vector2(1.f, 1.f));

	ScrollbarComponent* leftScrollbar = ScrollbarComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(leftScrollbar))
	{
		leftScrollbar->SetAnchorTop(32.f);
		leftScrollbar->SetAnchorLeft(32.f);
		leftScrollbar->SetAnchorBottom(32.f);
		leftScrollbar->SetSize(0.4f, 1.f);
		ScrollbarTestObjects.push_back(leftScrollbar);

		GuiEntity* leftViewedObject = GuiEntity::CreateObject();
		leftViewedObject->SetAutoSizeHorizontal(true);
		leftViewedObject->SetAutoSizeVertical(true);
		leftScrollbar->SetViewedObject(leftViewedObject);

		LabelComponent* leftLabel = LabelComponent::CreateObject();
		if (leftViewedObject->AddComponent(leftLabel))
		{
			leftLabel->SetAutoRefresh(false);
			leftLabel->SetAutoSizeHorizontal(true);
			leftLabel->SetAutoSizeVertical(true);
			leftLabel->SetLineSpacing(6.f);
			leftLabel->SetHorizontalAlignment(LabelComponent::HA_Center);

			DString labelText = TXT("This is a long line to test horizontal scrolling.  The Scrollbar on the right is testing input redirection to see even if a nested Scrollbar can relay input to its ViewedObject in the correct coordinate space.");
			for (Int i = 0; i < 32; ++i)
			{
				labelText += TXT("\nLine ") + i.ToString();
			}

			leftLabel->SetText(labelText);
			leftLabel->SetAutoRefresh(true);
		}
	}

	ScrollbarComponent* rightScrollbar = ScrollbarComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(rightScrollbar))
	{
		rightScrollbar->SetAnchorTop(32.f);
		rightScrollbar->SetAnchorRight(32.f);
		rightScrollbar->SetAnchorBottom(32.f);
		rightScrollbar->SetSize(0.4f, 1.f);
		ScrollbarTestObjects.push_back(rightScrollbar);

		GuiEntity* rightViewedObject = GuiEntity::CreateObject();
		rightViewedObject->SetAutoSizeHorizontal(false);
		rightViewedObject->SetAutoSizeVertical(true);
		rightViewedObject->SetSize(108.f, 1.f);
		rightScrollbar->SetViewedObject(rightViewedObject);

		std::vector<DString> foodList({TXT("Spaghetti"), TXT("Pizza"), TXT("Cereal"), TXT("Taco"), TXT("Hamburger"),
			TXT("Hotdog"), TXT("Bacon"), TXT("Cheese"), TXT("Potato"), TXT("Carrot"), TXT("Salad"), TXT("Bread"),
			TXT("Chili Pepper"), TXT("Omnomberry")});
		const Vector2 checkboxSize(150.f, 32.f);
		const Float checkboxSpacing(12.f);

		for (UINT_TYPE i = 0; i < foodList.size(); ++i)
		{
			CheckboxComponent* subCheckbox = CheckboxComponent::CreateObject();
			if (rightViewedObject->AddComponent(subCheckbox))
			{
				subCheckbox->SetPosition(checkboxSpacing, (checkboxSpacing * Float::MakeFloat(i+1)) + (checkboxSize.Y * Float::MakeFloat(i)));
				subCheckbox->SetSize(checkboxSize);

				if (subCheckbox->GetCaptionComponent() != nullptr)
				{
					subCheckbox->GetCaptionComponent()->SetText(foodList.at(i));
				}

				subCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleFoodItem, void, CheckboxComponent*);
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterToggleFoodItem (CheckboxComponent* uiComponent)
{
	if (uiComponent == nullptr || uiComponent->GetCaptionComponent() == nullptr)
	{
		UnitTestLog.Log(LogCategory::LL_Warning, TXT("A Checkbox is checked in nested scrollbar test.  However, the callback's uiComponent is either null, or its caption component is null."));
		return;
	}

	DString checkedSuffix = (uiComponent->GetChecked()) ? TXT("now checked.") : TXT("no longer checked.");
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  %s is %s"), uiComponent->GetCaptionComponent()->GetContent(), checkedSuffix);
}

void GuiTester::HandleScrollbarTesterFramedScrollbarClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > framed Scrollbar test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);

	//Fix the size for the main viewed object. The main test is the inner viewed object since that one is within a adjustable frame size.
	//Leaving a little room for the owning scrollbar to test dragging a frame component within a scrollbar at a scroll offset.
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->ReadCachedAbsSize() * Vector2(1.1f, 1.1f));

	FrameComponent* owningFrame = FrameComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(owningFrame))
	{
		owningFrame->SetPosition(Vector2(0.25f, 0.25f));
		owningFrame->SetSize(Vector2(0.5f, 0.5f));
		owningFrame->SetLockedFrame(false);
		owningFrame->SetBorderThickness(8.f);
		owningFrame->SetMinDragSize(Vector2(112.f, 112.f));
		owningFrame->OnBorderGrip = SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterFrameGripped, void, bool);
		ScrollbarTestObjects.push_back(owningFrame);

		ScrollbarTesterFramedScrollbar = ScrollbarComponent::CreateObject();
		if (owningFrame->AddComponent(ScrollbarTesterFramedScrollbar))
		{
			ScrollbarTesterFramedScrollbar->SetPosition(Vector2(0.05f, 0.05f));
			ScrollbarTesterFramedScrollbar->SetSize(Vector2(0.9f, 0.9f));
			ButtonComponent* buttonComp = ScrollbarTesterFramedScrollbar->GetZoomButton();
			if (buttonComp != nullptr && buttonComp->GetCaptionComponent() != nullptr)
			{
				//Destroy the caption component so it would be easier to debug the label component inside this scrollbar.
				//The caption component here would be recalculating its content every few frames the inner scrollbar changed dimensions.
				buttonComp->GetCaptionComponent()->Destroy();
			}

			GuiEntity* innerEntity = GuiEntity::CreateObject();
			innerEntity->SetAutoSizeHorizontal(false);
			innerEntity->SetAutoSizeVertical(true);
			innerEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			ScrollbarTesterFramedScrollbar->SetViewedObject(innerEntity);

			if (ScrollbarTesterFramedScrollbar->GetFrameTexture() != nullptr)
			{
				ScrollbarTesterFramedScrollbar->GetFrameTexture()->ResetColor = Color(0, 0, 0, 75);
			}

			LabelComponent* subLabel = LabelComponent::CreateObject();
			if (innerEntity->AddComponent(subLabel))
			{
				subLabel->SetAutoRefresh(false);
				subLabel->SetPosition(Vector2::ZERO_VECTOR);
				subLabel->SetSize(Vector2(1.f, 1.f));
				subLabel->SetAnchorLeft(0.125f);
				subLabel->SetAnchorRight(0.125f);
				subLabel->SetAutoSizeHorizontal(false);
				subLabel->SetAutoSizeVertical(true);
				subLabel->SetWrapText(true);
				subLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				subLabel->SetText(TXT("This tests the following:\n\n\
1. The scrollbar size changes based on the owning frame component's size.\n\n\
2. The scrollbar should automatically reconstruct its frame texture as soon as the user releases the frame's borders.\n\n\
3. This label component within the adjusting scrollbar should also adapt. Using normalized anchor coordinates, it should always have its left and right margins set to 12.5% of the scrollbar width.\n\n\
4. The vertical size of the label component is configured to be auto sized based on the number of lines, and this label instructs the scrollbar's maximum vertical scroll position based on that.\n\n\
5. The scrollbar's frame texture is also transparent. The frame component rendered behind the scrollbar should still be visible.\n"));
				subLabel->SetAutoRefresh(true);
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterFrameGripped (bool isGripped)
{
	if (ScrollbarTesterFramedScrollbar.IsValid())
	{
		ScrollbarTesterFramedScrollbar->SetFreezeTextureSize(isGripped);
	}
}

void GuiTester::HandleScrollbarTesterComplexUiClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject.IsValid())

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > complex UI test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
	ScrollbarTesterViewedObject->SetSize(1.f, 1.f);

	Float posY = 16.f;
	const Float uiPadding = 16.f;
	LabelComponent* title = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(title))
	{
		title->SetAutoRefresh(false);
		title->SetPosition(Vector2(posY, posY));
		title->SetSize(Vector2(512.f, 48.f));
		title->SetCharacterSize(48);
		title->SetClampText(true);
		title->SetWrapText(false);
		title->SetText(TXT("Complicated UI Test"));
		title->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(title);

		posY += title->ReadSize().Y + uiPadding;
	}

	const std::vector<DString> labelTexts(
	{
		TXT("This test displays the scrollbar rendering a GuiEntity that contains numerous UI components of various types such as labels, buttons, nested components, and other scrollbars."),
		TXT("These UI components repeat a few times to test UI components in various scrolling positions."),
		TXT("Some tips regarding scrollbar controls.\nHolding the ctrl, shift, and alt keys multiplies the scrolling speed.\nYou can scroll the scrollbar by clicking on the arrow buttons on the sides.\nYou can also scroll it by dragging the thumb, or clicking on the tracks between the buttons.\nYou can also use the mouse wheel to scroll of course.\nLastly, you can also scroll the scrollbar using the middle mouse button.")
	});
	const UINT_TYPE numRepeats = labelTexts.size();

	for (UINT_TYPE i = 0; i < numRepeats; ++i)
	{
		LabelComponent* description = LabelComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(description))
		{
			description->SetAutoRefresh(false);
			description->SetPosition(Vector2(0.f, posY));
			description->SetSize(256.f, 1.f);
			description->SetAutoSizeVertical(true);
			description->SetWrapText(true);
			description->SetClampText(false);
			description->SetText(labelTexts.at(i));
			description->SetAutoRefresh(true);
			ScrollbarTestObjects.push_back(description);

			posY += description->ReadSize().Y + uiPadding;
		}

		ButtonComponent* testButton = ButtonComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(testButton))
		{
			testButton->SetPosition(Vector2(8.f, posY));
			testButton->SetSize(Vector2(96.f, 32.f));
			testButton->SetCaptionText(TXT("Button that does nothing"));
			ScrollbarTestObjects.push_back(testButton);

			posY += testButton->ReadSize().Y + uiPadding;
		}

		FrameComponent* testFrame = FrameComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(testFrame))
		{
			testFrame->SetPosition(0.f, posY);
			testFrame->SetSize(768.f, 256.f);
			testFrame->SetLockedFrame(true);
			testFrame->SetBorderThickness(2.f);
			ScrollbarTestObjects.push_back(testFrame);

			posY += testFrame->ReadSize().Y + uiPadding;

			FrameComponent* adjustableFrame = FrameComponent::CreateObject();
			if (testFrame->AddComponent(adjustableFrame))
			{
				adjustableFrame->SetPosition(Vector2::ZERO_VECTOR);
				adjustableFrame->SetSize(testFrame->ReadSize() * 0.5f);
				adjustableFrame->SetBorderThickness(6.f);
				adjustableFrame->SetLockedFrame(false);
				adjustableFrame->SetCenterColor(Color(25, 50, 0));

				ButtonComponent* innerButton = ButtonComponent::CreateObject();
				if (adjustableFrame->AddComponent(innerButton))
				{
					innerButton->SetPosition(0.4f, 0.4f);
					innerButton->SetSize(0.2f, 0.2f);
					innerButton->SetCaptionText("Inner Button");

					ColorButtonState* buttonState = dynamic_cast<ColorButtonState*>(innerButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
					if (buttonState != nullptr)
					{
						buttonState->SetDefaultColor(Color(100, 50, 50));
						buttonState->SetHoverColor(Color(50, 100, 50));
						buttonState->SetDownColor(Color(50, 50, 100));
					}
				}
			}
		}

		ScrollbarComponent* scrollbarLabel = ScrollbarComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(scrollbarLabel))
		{
			scrollbarLabel->SetPosition(0.f, posY);
			scrollbarLabel->SetSize(256.f, 256.f);
			scrollbarLabel->SetHideControlsWhenFull(true);

			GuiEntity* scrollbarLabelViewedObject = nullptr;
			LabelComponent* scrollbarLabelContent = nullptr;
			LabelComponent::CreateLabelWithinScrollbar(scrollbarLabel, OUT scrollbarLabelViewedObject, OUT scrollbarLabelContent);
			CHECK(scrollbarLabelViewedObject != nullptr && scrollbarLabelContent != nullptr)

			scrollbarLabelContent->SetAutoRefresh(false);
			scrollbarLabelContent->SetText(TXT("Line 1\nLine 2\nLine 3\nLine 4\nLine 5\nLine 6\nLine 7\nLine 8\nLine 9\nLine 10\nLine 11\nLine 12\nLine 13\nLine 14\nLine 15\nLine 16"));
			scrollbarLabelContent->SetLineSpacing(4.f);
			scrollbarLabelContent->SetCharacterSize(16);
			scrollbarLabelContent->SetAutoRefresh(true);

			posY += scrollbarLabel->ReadSize().Y + uiPadding;
			ScrollbarTestObjects.push_back(scrollbarLabel);
		}
	}
}

void GuiTester::HandleTextFieldLeftAlignButtonClicked (ButtonComponent* uiComponent)
{
	for (TextFieldComponent* field : TextFields)
	{
		field->SetHorizontalAlignment(LabelComponent::HA_Left);
	}
}

void GuiTester::HandleTextFieldCenterAlignButtonClicked (ButtonComponent* uiComponent)
{
	for (TextFieldComponent* field : TextFields)
	{
		field->SetHorizontalAlignment(LabelComponent::HA_Center);
	}
}

void GuiTester::HandleTextFieldRightAlignButtonClicked (ButtonComponent* uiComponent)
{
	for (TextFieldComponent* field : TextFields)
	{
		field->SetHorizontalAlignment(LabelComponent::HA_Right);
	}
}

void GuiTester::HandleTextFieldTextChanged (TextFieldComponent* uiComp)
{
	if (uiComp != nullptr)
	{
		GuiLog.Log(LogCategory::LL_Debug, TXT("The TextField broadcasted a OnTextChanged delegate. Its content is now \"%s\"."), uiComp->GetContent());
	}
}

bool GuiTester::HandleTextFieldLimitedInput (const DString& txt)
{
	return txt.HasRegexMatch(TXT("[a-zA-Z .]"));
}

void GuiTester::HandleTextFieldSingleLineReturn (TextFieldComponent* textField)
{
	CHECK(textField != nullptr)
	GuiLog.Log(LogCategory::LL_Debug, TXT("The Single Line Text Field now reads: \"%s\"."), textField->GetContent());
}

bool GuiTester::HandleListBoxFilterEdit (const sf::Event& sfEvent)
{
	DString filterText = ListBoxFilterField->GetContent();
	for (ComponentIterator iter(StageFrame.Get(), true); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (ListBoxComponent* listComp = dynamic_cast<ListBoxComponent*>(iter.GetSelectedComponent()))
		{
			listComp->SetFilter(filterText);
		}
		else if (ScrollbarComponent* scrollbar = dynamic_cast<ScrollbarComponent*>(iter.GetSelectedComponent()))
		{
			if (GuiEntity* viewedObj = dynamic_cast<GuiEntity*>(scrollbar->GetViewedObject()))
			{
				if (ListBoxComponent* listComp = dynamic_cast<ListBoxComponent*>(viewedObj->FindComponent(ListBoxComponent::SStaticClass(), true)))
				{
					listComp->SetFilter(filterText);
				}
			}
		}
	}

	return false;
}

void GuiTester::HandleBasicListBoxOptionSelected (Int selectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Selected new item from basic List Box component:  %s"), ListBoxStrings.at(selectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleBasicListBoxOptionDeselected (Int deselectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Deselected item from basic List Box component:  %s"), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleReadOnlyBoxOptionSelected (Int selectedIdx)
{
	Engine::FindEngine()->FatalError(TXT("GuiTester:  List Box component test failed.  User was still able to select an item from a read only List Box."));
	Destroy();
}

void GuiTester::HandleReadOnlyBoxOptionDeselected (Int deselectedIdx)
{
	Engine::FindEngine()->FatalError(TXT("GuiTester:  List Box component test failed.  User was still able to deselect an item from a read only List Box."));
	Destroy();
}

void GuiTester::HandleMaxNumSelectedBoxOptionSelected (Int selectedIdx)
{
	CHECK(MaxNumSelectableListBox.IsValid())
	UnitTester::TestLog(TestFlags, TXT("Selected item from List Box with max selection limit:  %s.  Total number of selected items:  %s"), ListBoxStrings.at(selectedIdx.ToUnsignedInt()), Int(MaxNumSelectableListBox->ReadSelectedItems().size()));

	if (MaxNumSelectableListBox->ReadSelectedItems().size() > MaxNumSelectableListBox->GetMaxNumSelected())
	{
		Engine::FindEngine()->FatalError(TXT("GuiTester:  User was able to select more items than the List Box's item selection limit (") + MaxNumSelectableListBox->GetMaxNumSelected().ToString() + TXT(")."));
		Destroy();
	}
}

void GuiTester::HandleMaxNumSelectedBoxOptionDeselected (Int deselectedIdx)
{
	CHECK(MaxNumSelectableListBox.IsValid())
	UnitTester::TestLog(TestFlags, TXT("Deselected item from List Box with max selection limit:  %s."), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleMinNumSelectedBoxOptionSelected (Int selectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Min Selection List Box selected new item:  %s."), ListBoxStrings.at(selectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleMinNumSelectedBoxOptionDeselected (Int deselectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Min Selection List Box deselected item:  %s."), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleAutoDeselectBoxOptionSelected (Int selectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Auto deselecting List Box selected new item:  %s."), ListBoxStrings.at(selectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleAutoDeselectBoxOptionDeselected (Int deselectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Auto deselecting List Box deselected item:  %s."), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleBasicDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Basic dropdown component selected new option index:  %s"), newOptionIdx);
}

void GuiTester::HandleFullBasicDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Full basic dropdown component selected new option index:  %s"), newOptionIdx);
}

void GuiTester::HandleButtonBarDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx)
{
	CHECK(ButtonBarDropdown.IsValid())

	GuiDataElement<ButtonComponent*>* selectedData = ButtonBarDropdown->GetSelectedGuiDataElement<ButtonComponent*>();
	ButtonComponent* selectedButton = selectedData->Data;
	if (selectedButton == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("GuiTester:  Dropdown component test failed.  Failed to retrieve button component instance after selecting a new item."));
		Destroy();
		return;
	}

	UnitTester::TestLog(TestFlags, TXT("Button bar dropdown component selected a button component (%s) with text caption equal to \"%s\"."), selectedButton->ToString(), selectedButton->GetCaptionText());
}

void GuiTester::HandleSearchableDropdownOptionClicked (DropdownComponent* comp, Int newOptionIdx)
{
	CHECK(comp != nullptr)

	LabelComponent* label = comp->GetSelectedItemLabel();
	CHECK(label != nullptr)

	DString selectedOption = label->GetContent();
	UnitTester::TestLog(TestFlags, TXT("Dropdown component selected option \"%s\""), selectedOption);
}

void GuiTester::HandleVertListToggleCheckboxVisibility (CheckboxComponent* uiComponent)
{
	CHECK(VerticalListMainList != nullptr)

	for (CheckboxComponent* checkbox : VerticalListCheckboxes)
	{
		checkbox->SetVisibility(uiComponent->IsChecked());
	}

	VerticalListMainList->RefreshComponentTransforms();
}

void GuiTester::HandleVertListToggleButtonVisibility (CheckboxComponent* uiComponent)
{
	CHECK(VerticalListMainList != nullptr)

	for (ButtonComponent* button : VerticalListButtons)
	{
		button->SetVisibility(uiComponent->IsChecked());
	}

	VerticalListMainList->RefreshComponentTransforms();
}

void GuiTester::HandleVertListAddButton (ButtonComponent* uiComponent)
{
	if (VerticalListMainList != nullptr)
	{
		ButtonComponent* newButton = ButtonComponent::CreateObject();
		if (VerticalListMainList->AddComponent(newButton))
		{
			newButton->SetAnchorLeft(0.05f);
			newButton->SetAnchorRight(0.05f);
			newButton->SetSize(Vector2(1.f, 16.f));
			newButton->SetCaptionText(TXT("Button ") + Int(VerticalListButtons.size() + 1).ToString());
			VerticalListButtons.push_back(newButton);
		}

		VerticalListMainList->RefreshComponentTransforms();
	}
}

void GuiTester::HandleVertListRemoveButton (ButtonComponent* uiComponent)
{
	if (!ContainerUtils::IsEmpty(VerticalListButtons) && VerticalListMainList != nullptr)
	{
		ContainerUtils::GetLast(VerticalListButtons)->Destroy();
		VerticalListButtons.pop_back();
		VerticalListMainList->RefreshComponentTransforms();
	}
}

void GuiTester::HandleVertListAddCheckbox (ButtonComponent* uiComponent)
{
	if (VerticalListMainList != nullptr)
	{
		CheckboxComponent* newCheckbox = CheckboxComponent::CreateObject();
		if (VerticalListMainList->AddComponent(newCheckbox))
		{
			newCheckbox->SetAnchorLeft(0.05f);
			newCheckbox->SetAnchorRight(0.05f);
			newCheckbox->SetSize(Vector2(1.f, 16.f));
			newCheckbox->SetChecked(true);
			if (newCheckbox->GetCaptionComponent() != nullptr)
			{
				newCheckbox->GetCaptionComponent()->SetHorizontalAlignment(LabelComponent::HA_Center);
				newCheckbox->GetCaptionComponent()->SetText(TXT("Checkbox ") + Int(VerticalListCheckboxes.size() + 1).ToString());
			}
			
			VerticalListCheckboxes.push_back(newCheckbox);
		}

		VerticalListMainList->RefreshComponentTransforms();
	}
}

void GuiTester::HandleVertListRemoveCheckbox (ButtonComponent* uiComponent)
{
	if (!ContainerUtils::IsEmpty(VerticalListCheckboxes) && VerticalListMainList != nullptr)
	{
		ContainerUtils::GetLast(VerticalListCheckboxes)->Destroy();
		VerticalListCheckboxes.pop_back();
		VerticalListMainList->RefreshComponentTransforms();
	}
}

void GuiTester::HandleFocusChange (DropdownComponent* comp, Int newOptionIdx)
{
	CHECK(SetFocusDropdown.IsValid())

	if (FocusedGuiEntityIdx != UINT_INDEX_NONE)
	{
		//Deselect the previous GuiEntity
		GuiEntities.at(FocusedGuiEntityIdx)->LoseFocus();
	}

	GuiDataElement<GuiEntity*>* selectedData = SetFocusDropdown->GetSelectedGuiDataElement<GuiEntity*>();
	GuiEntity* selectedGui = selectedData->Data;
	if (selectedGui == nullptr)
	{
		UnitTestLog.Log(LogCategory::LL_Warning, TXT("Failed to select GuiEntity from Set Focus dropdown component."));
		return;
	}

	selectedGui->GainFocus();
}

void GuiTester::HandleHoverFrameRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Frame hover component rolled over."));
}

void GuiTester::HandleHoverFrameRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Frame hover component rolled out."));
}

void GuiTester::HandleHoverLabelRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Label hover component rolled over."));
	HoverLabel->SetText(TXT("Rolled Over"));
}

void GuiTester::HandleHoverLabelRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Label hover component rolled out."));
	HoverLabel->SetText(TXT("Rolled Out"));
}

void GuiTester::HandleHoverButtonRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Button hover component rolled over."));
	HoverButton->SetCaptionText(TXT("Rolled Over"));
}

void GuiTester::HandleHoverButtonRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Button hover component rolled out."));
	HoverButton->SetCaptionText(TXT("Rolled Out"));
}

void GuiTester::HandleTreeListClassSelected (Int newOptionIdx)
{
	CHECK(TreeListClassBrowser.IsValid())

	GuiDataElement<const DClass*>* selectedItem = TreeListClassBrowser->GetSelectedDataElement<const DClass*>();
	if (selectedItem != nullptr && selectedItem->Data != nullptr)
	{
		UnitTester::TestLog(TestFlags, TXT("Selected class:  %s"), selectedItem->Data->ToString());
	}
	else
	{
		UnitTester::TestLog(TestFlags, TXT("Cleared class selection."));
	}
}
SD_END
#endif