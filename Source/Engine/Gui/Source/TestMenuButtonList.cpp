/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TestMenuButtonList.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "FocusComponent.h"
#include "FrameComponent.h"
#include "SingleSpriteButtonState.h"
#include "TestMenuButtonList.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::TestMenuButtonList, SD::GuiEntity)
SD_BEGIN

void TestMenuButtonList::InitProps ()
{
	Super::InitProps();

	MenuFrame = nullptr;
}

void TestMenuButtonList::LoseFocus ()
{
	Super::LoseFocus();

	GuiLog.Log(LogCategory::LL_Debug, TXT("%s has lost focus."), ToString());
}

void TestMenuButtonList::GainFocus ()
{
	Super::GainFocus();

	GuiLog.Log(LogCategory::LL_Debug, TXT("%s has gained focus."), ToString());
}

void TestMenuButtonList::ConstructUI ()
{
	MenuFrame = FrameComponent::CreateObject();
	if (AddComponent(MenuFrame))
	{
		MenuFrame->SetPosition(Vector2::ZERO_VECTOR);
		MenuFrame->SetSize(Vector2(1.f, 1.f));
		MenuFrame->SetLockedFrame(true);

		for (UINT_TYPE i = 0; i < 10; i++)
		{
			ButtonComponent* newButton = InitializeButton();
			CHECK(newButton != nullptr)

			newButton->SetCaptionText(TXT("Button ") + Int(i + 1).ToString());

			//Disable button 5 to test if the focus component will skip it
			if (i == 4)
			{
				newButton->SetEnabled(false);
			}
		}

		Vector2 buttonSize = Vector2(0.6f, 16.f);
		Float buttonSpacing = 8.f;
		for (UINT_TYPE i = 0; i < ButtonList.size(); i++)
		{
			ButtonList.at(i)->SetSize(buttonSize);
			ButtonList.at(i)->SetPosition(Vector2(0.f, ((buttonSize.Y + buttonSpacing) * Float::MakeFloat(i)) + MenuFrame->GetBorderThickness()));
		}
	}
}

void TestMenuButtonList::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	CHECK(Focus.IsValid())

	for (UINT_TYPE i = 0; i < ButtonList.size(); i++)
	{
		Focus->TabOrder.push_back(ButtonList.at(i).Get());
	}
}

ButtonComponent* TestMenuButtonList::InitializeButton ()
{
	ButtonComponent* newButton = ButtonComponent::CreateObject();
	if (!MenuFrame->AddComponent(newButton))
	{
		return nullptr;
	}

	if (newButton->GetBackground() != nullptr)
	{
		newButton->GetBackground()->SetCenterTexture(GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get());
	}

	newButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
	newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuButtonList, HandleButtonReleased, void, ButtonComponent*));
	ButtonList.push_back(newButton);

	return newButton;
}

void TestMenuButtonList::HandleButtonReleased (ButtonComponent* uiComponent)
{
	GuiLog.Log(LogCategory::LL_Debug, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
}
SD_END

#endif