/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicListMenu.cpp
=====================================================================
*/

#include "ContextMenuComponent.h"
#include "DynamicListMenu.h"
#include "FrameComponent.h"
#include "LabelComponent.h"

IMPLEMENT_CLASS(SD::DynamicListMenu, SD::GuiEntity)
SD_BEGIN

const DString DynamicListMenu::DIVIDER(TXT("--= D I V I D E R =--"));

DynamicListMenu::SInitMenuOption::SInitMenuOption (const DString& pathToDivider) :
	FullCommandPath(pathToDivider),
	Data(nullptr),
	Keybind(sf::Keyboard::Unknown),
	RequiredState(KS_None)
{
#ifdef DEBUG_MODE
	bool pathValidated = true;
	if (FullCommandPath.Length() > DIVIDER.Length())
	{
		//The length is greater than the divider, that implies there's a path to this divider. Ensure the '>' character is immediately following DIVIDER. This checks for accidental spaces before DIVIDER.
		if (!FullCommandPath.EndsWith(TXT(">") + DIVIDER, DString::CC_CaseSensitive))
		{
			pathValidated = false;
		}

		//It's possible to bypass this check using the escape character (eg: "\\>DIVIDER"), but doing something like that implies you're searching for trouble. Not bothering to check that for performance's sake.
	}
	else if (FullCommandPath.Compare(DIVIDER, DString::CC_CaseSensitive) != 0)
	{
		pathValidated = false;
	}

	CHECK_INFO(pathValidated, "Failed to initialize DynamicListMenu::SInitMenuOption. The constructor with only DString in the parameter is expected to end with the divider string since it's not bound to a command.")
#endif
}

DynamicListMenu::SInitMenuOption::SInitMenuOption (const DString& inFullCommandPath, const std::function<void(BaseGuiDataElement*)>& inOnCommand) :
	FullCommandPath(inFullCommandPath),
	Data(nullptr),
	OnCommand(inOnCommand),
	Keybind(sf::Keyboard::Unknown),
	RequiredState(KS_None)
{
	//Noop
}

DynamicListMenu::SInitMenuOption::SInitMenuOption (const DString& inFullCommandPath, sf::Keyboard::Key inKeybind, EKeyboardState inRequiredState, const std::function<void(BaseGuiDataElement*)>& inOnCommand) :
	FullCommandPath(inFullCommandPath),
	Data(nullptr),
	OnCommand(inOnCommand),
	Keybind(inKeybind),
	RequiredState(inRequiredState)
{
	//Noop
}

DynamicListMenu::SMenuOption::SMenuOption () :
	DisplayText(DString::EmptyString),
	DisplaySuffix(DString::EmptyString),
	bIsDivider(false),
	Data(nullptr),
	ParentOption(INDEX_NONE),
	Keybind(sf::Keyboard::Unknown),
	RequiredState(KS_None)
{
	//Noop
}

DynamicListMenu::SMenuBlock::SMenuBlock () :
	Frame(nullptr),
	ColorTransform(nullptr),
	HoverIdx(INT_INDEX_NONE),
	ExpandedOption(INT_INDEX_NONE)
{
	//Noop
}

DynamicListMenu::SMenuBlock::~SMenuBlock ()
{
	//Noop
}

void DynamicListMenu::InitProps ()
{
	Super::InitProps();

	HoverActivationDelay = 0.35f;
	HoveredCommandTimestamp = -1.f;
	CollapseMenuDelay = 0.5f;
	UnhoveredMenuTimestamp = -1.f;
	FadeOutDuration = 0.65f;
	FadeOutTimestamp = -1.f;
	OptionSize = 16.f;
	MinSuffixDistance = 12.f;
	MinMenuWidth = 18.f;
	OptionSpacing = 8.f;
	SuffixPadding = 0.015f;
	DividerHeightMultiplier = 0.6f;
	DividerVisibleHeight = 0.15f;
	DividerColor = Color(64, 64, 64, 96);
	BackgroundColor = Color(252, 252, 208);
	HighlightColor = Color(96, 96, 225, 175);
	PrimaryTextColor = Color::BLACK;
	SecondaryTextColor = Color(102, 109, 16, 220);

	LatestHoveredMenu = INDEX_NONE;
}

void DynamicListMenu::BeginObject ()
{
	Super::BeginObject();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, DynamicListMenu, HoverMonitorTick, void, Float));
	}
}

void DynamicListMenu::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::HandleMouseMove(mouse, sfmlEvent, deltaMove);

	if (ContainerUtils::IsEmpty(VisibleMenus) || FadeOutTimestamp >= 0.f)
	{
		return;
	}

	SMenuBlock* topHoveredBlock = nullptr;
	size_t blockIdx = VisibleMenus.size() - 1;
	while (true)
	{
		if (VisibleMenus.at(blockIdx)->Frame != nullptr && VisibleMenus.at(blockIdx)->Frame->IsWithinBounds(mouse->ReadCachedAbsPosition()))
		{
			topHoveredBlock = VisibleMenus.at(blockIdx);
			break;
		}

		if (blockIdx == 0)
		{
			break;
		}

		--blockIdx;
	}

	if (topHoveredBlock != nullptr)
	{
		LatestHoveredMenu = blockIdx;
		bool bIsVeryTopMenu = (blockIdx == VisibleMenus.size() - 1);
		if (bIsVeryTopMenu)
		{
			UnhoveredMenuTimestamp = -1.f; //Clear the unhover delay if the user hovered anywhere in the top menu.
		}

		Int newHoverIdx = CalcHoveredOption(mouse->ReadCachedAbsPosition(), *topHoveredBlock);
		if (newHoverIdx != INT_INDEX_NONE && newHoverIdx != topHoveredBlock->HoverIdx)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)

			if (blockIdx > 0)
			{
				//Ensure all menus under the current hovered menu have their highlighted options snap back to the option that expands to this hovered menu.
				//This could happen if the user moves their mouse to a lower menu briefly before jumping it back to the top menu. The highlight of that lower menu should revert back when doing so.
				size_t subMenuIdx = blockIdx - 1;
				while (true)
				{
					if (VisibleMenus.at(subMenuIdx)->HoverIdx != VisibleMenus.at(subMenuIdx)->ExpandedOption)
					{
						HighlightIdxInBlock(*VisibleMenus.at(subMenuIdx), VisibleMenus.at(subMenuIdx)->ExpandedOption);
					}

					if (subMenuIdx == 0)
					{
						break;
					}

					--subMenuIdx;
				}
			}

			HoveredCommandTimestamp = -1.f; //Default to not hovering over an option
			bool isHoveringOverExpandMenu = HighlightIdxInBlock(*topHoveredBlock, newHoverIdx);
			if (isHoveringOverExpandMenu)
			{
				HoveredCommandTimestamp = localEngine->GetElapsedTime();
			}

			//If there is at least one expanded menu, and the user has hovered over the highest menu before
			SMenuBlock* veryTopMenu = ContainerUtils::GetLast(VisibleMenus);
			if (VisibleMenus.size() > 1 && veryTopMenu->HoverIdx != INT_INDEX_NONE)
			{
				UnhoveredMenuTimestamp = localEngine->GetElapsedTime();
			}
		}
	}
	else if (UnhoveredMenuTimestamp < 0.f && VisibleMenus.size() > 1) //Not hovering anything and there are more than the root menu showing
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		UnhoveredMenuTimestamp = localEngine->GetElapsedTime();
		LatestHoveredMenu = INDEX_NONE;
	}
}

void DynamicListMenu::HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::HandlePassiveMouseClick(mouse, sfmlEvent, eventType);

	if (FadeOutTimestamp >= 0.f)
	{
		return;
	}

	bool clickedWithinBounds = false;

	//If the user clicked out of bounds from all components, destroy this list.
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (PlanarTransform* planarComp = dynamic_cast<PlanarTransform*>(iter.GetSelectedComponent()))
		{
			if (planarComp->IsWithinBounds(mouse->ReadCachedAbsPosition()))
			{
				clickedWithinBounds = true;
				break;
			}
		}
	}

	if (!clickedWithinBounds)
	{
		Destroy();
	}
}

bool DynamicListMenu::HandleConsumableInput (const sf::Event& evnt)
{
	if (Super::HandleConsumableInput(evnt))
	{
		return true;
	}

	if (FadeOutTimestamp >= 0.f)
	{
		return false;
	}

	if (evnt.type != sf::Event::KeyPressed && evnt.type != sf::Event::KeyReleased)
	{
		return false;
	}

	//Check if a keybind is executed
	if (!ContainerUtils::IsEmpty(VisibleMenus))
	{
		size_t i = VisibleMenus.size() - 1;
		while (true)
		{
			for (size_t cmdIdx : VisibleMenus.at(i)->Options)
			{
				if (cmdIdx == INDEX_NONE || AllOptions.at(cmdIdx).Keybind == sf::Keyboard::Unknown)
				{
					continue; //Either a divider or a command without a keybind. Skip this command.
				}

				if (evnt.key.code != AllOptions.at(cmdIdx).Keybind)
				{
					continue; //This input event is not related to this option
				}

				EKeyboardState reqKeys = AllOptions.at(cmdIdx).RequiredState;

				//'deduct' the required keys based on the keyboard state
				reqKeys &= ~InputBroadcaster::GetKeyboardState();

				if (reqKeys == KS_None)
				{
					//Collapse any menus between this menu and the top menu
					while (i+1 < VisibleMenus.size())
					{
						RemoveLastVisibleMenu();
					}

					//Select the option for pressed and released
					VisibleMenus.at(i)->HoverIdx = cmdIdx;
					HoveredCommandTimestamp = -1.f;
					UnhoveredMenuTimestamp = -1.f;
					UpdateHoverTransform(*VisibleMenus.at(i));

					if (evnt.type == sf::Event::KeyReleased)
					{
						//Execute the option
						if (AllOptions.at(cmdIdx).OnCommand.IsBounded())
						{
							AllOptions.at(cmdIdx).OnCommand(AllOptions.at(cmdIdx).Data);
						}

						BeginFadeOut();
					}

					return true;
				}
			}

			if (i == 0)
			{
				break;
			}
			--i;
		}
	}

	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		if (VisibleMenus.size() > 1)
		{
			RemoveLastVisibleMenu();
		}
		else
		{
			Destroy();
		}

		return true;
	}

	return false;
}

bool DynamicListMenu::HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::HandleConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (FadeOutTimestamp >= 0.f)
	{
		return false;
	}

	if (eventType != sf::Event::MouseButtonReleased || sfmlEvent.button != sf::Mouse::Button::Left)
	{
		//If clicking over any of the visible menus, consume the input event even if it's not doing anything to it (preventing the user from clicking buttons under this list).
		for (SMenuBlock* menu : VisibleMenus)
		{
			if (menu->Frame != nullptr && menu->Frame->IsWithinBounds(mouse->ReadCachedAbsPosition()))
			{
				return true;
			}
		}

		return false;
	}

	//Identify where they clicked. Menus on top takes input priority.
	bool consumeInput = false;
	if (!ContainerUtils::IsEmpty(VisibleMenus))
	{
		size_t i = VisibleMenus.size() - 1;
		while (true)
		{
			SMenuBlock* menu = VisibleMenus.at(i);
			if (menu->Frame == nullptr || !menu->Frame->IsWithinBounds(mouse->ReadCachedAbsPosition()))
			{
				if (i == 0)
				{
					break;
				}

				--i;
				continue;
			}

			consumeInput = true;
			Int hoveredIdx = CalcHoveredOption(mouse->ReadCachedAbsPosition(), *menu);

			//Collapse any menus between this menu and the top menu
			while (i+1 < VisibleMenus.size())
			{
				RemoveLastVisibleMenu();
			}

			if (hoveredIdx != menu->HoverIdx)
			{
				menu->HoverIdx = hoveredIdx;
				UpdateHoverTransform(*menu);
			}

			if (ContainerUtils::IsValidIndex(AllOptions, hoveredIdx)) //If not clicking on a divider
			{
				SMenuOption& execOption = AllOptions.at(hoveredIdx.ToUnsignedInt());
				if (!ContainerUtils::IsEmpty(execOption.SubOptions))
				{
					Vector2 primaryLocation;
					Vector2 secondaryLocation;
					if (PlanarTransformComponent* colorTransform = menu->ColorTransform)
					{
						secondaryLocation = colorTransform->ReadCachedAbsPosition();
						primaryLocation = secondaryLocation;
						primaryLocation.X += colorTransform->ReadCachedAbsSize().X;
					}

					//Expand this menu
					RevealMenuBlock(execOption.SubOptions, primaryLocation, secondaryLocation);
					menu->ExpandedOption = hoveredIdx;
				}
				else if (execOption.bIsDivider)
				{
					//Noop
				}
				else if (execOption.OnCommand.IsBounded())
				{
					execOption.OnCommand(execOption.Data);
					BeginFadeOut();
				}
				else
				{
					GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot execute %s since that option is not bound to a delegate."), execOption.DisplayText);
				}
			}

			break;
		}
	}

	return consumeInput;
}

void DynamicListMenu::Destroyed ()
{
	ClearAllOptions();

	for (SMenuBlock* menu : VisibleMenus)
	{
		delete menu;
		menu = nullptr;
	}
	ContainerUtils::Empty(OUT VisibleMenus);

	Super::Destroyed();
}

void DynamicListMenu::InitializeList (ContextMenuComponent* contextComp, MousePointer* mouse, const std::vector<SInitMenuOption>& inOptions)
{
	if (!ContainerUtils::IsEmpty(AllOptions))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize %s since that menu entity is already initialized."), ToString());

		for (const SInitMenuOption& option : inOptions)
		{
			if (option.Data != nullptr)
			{
				delete option.Data;
				option.Data = nullptr;
			}
		}

		return;
	}

	if (ContainerUtils::IsEmpty(inOptions))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize %s without specifying any options."), ToString());
		return;
	}

	for (const SInitMenuOption& initOption : inOptions)
	{
		CHECK(!initOption.FullCommandPath.IsEmpty())

		bool hasTrailingSymbol = initOption.FullCommandPath.EndsWith('>');

		std::vector<DString> path;
		initOption.FullCommandPath.ParseString('>', OUT path, true);
		
		//Ensure none of the > characters are delimited
		{
			size_t i = 0;
			while (i < path.size() - 1)
			{
				if (path.at(i).EndsWith('\\'))
				{
					//Remove the escape character
					path.at(i).PopBack();

					//Bring the next path to this line.
					path.at(i) += TXT(">") + path.at(i+1);
					path.erase(path.begin() + i+1);
					continue;
				}

				++i;
			}
		}

		//Bring the trailing symbol back since ParseString would have removed it.
		if (hasTrailingSymbol && !ContainerUtils::IsEmpty(path))
		{
			path.at(path.size() - 1) += TXT(">");
		}

		//Handle moving the path to sub menus except for the last item.
		size_t expectedParentIdx = INDEX_NONE;
		bool bCreatedSubMenu = false;
		for (size_t pathIdx = 0; pathIdx < path.size() - 1; ++pathIdx)
		{
			bool bFoundSubMenu = false;

			//check if the menu already exists.
			if (!bCreatedSubMenu) //Only check if we haven't created a sub menu. If we created a sub menu, then it implies none of the sub paths also haven't been created yet.
			{
				for (size_t optionIdx = 0; optionIdx < AllOptions.size(); ++optionIdx)
				{
					SMenuOption& otherOption = AllOptions.at(optionIdx);
					if (expectedParentIdx != otherOption.ParentOption)
					{
						continue;
					}

					if (otherOption.DisplayText.Compare(path.at(pathIdx), DString::CC_IgnoreCase) == 0)
					{
						//This expand menu already exists
						expectedParentIdx = optionIdx;
						bFoundSubMenu = true;
						break;
					}
				}
			}

			if (bFoundSubMenu)
			{
				continue;
			}

			//Create sub menu
			if (expectedParentIdx != INDEX_NONE)
			{
				AllOptions.at(expectedParentIdx).SubOptions.push_back(AllOptions.size());
			}

			SMenuOption& subMenu = AllOptions.emplace_back();
			subMenu.DisplayText = path.at(pathIdx);
			subMenu.DisplaySuffix = TXT(">");
			subMenu.ParentOption = expectedParentIdx;
			expectedParentIdx = AllOptions.size() - 1;
			bCreatedSubMenu = true;
		}

		//Create command
		if (expectedParentIdx != INDEX_NONE)
		{
			AllOptions.at(expectedParentIdx).SubOptions.push_back(AllOptions.size());
		}

		SMenuOption& newCmd = AllOptions.emplace_back();
		newCmd.DisplayText = ContainerUtils::GetLast(path);
		newCmd.ParentOption = expectedParentIdx;
		newCmd.bIsDivider = (newCmd.DisplayText.Compare(DIVIDER, DString::CC_CaseSensitive) == 0);
		if (newCmd.bIsDivider)
		{
			if (initOption.Data != nullptr)
			{
				GuiLog.Log(LogCategory::LL_Warning, TXT("Divider context menu options should not have Data associated with it since they don't execute a command delegate."));

				//Delete the data ahead of time since the SMenuOption wont be deleting it.
				delete initOption.Data;
				initOption.Data = nullptr;
			}

			continue; //Dividers cannot have a keybind or command associated with it.
		}

		if (initOption.Keybind != sf::Keyboard::Unknown)
		{
			newCmd.DisplaySuffix = TXT("(");

			if ((initOption.RequiredState & KS_Ctrl) > 0)
			{
				newCmd.DisplaySuffix += TXT("Ctrl + ");
			}

			if ((initOption.RequiredState & KS_Alt) > 0)
			{
				newCmd.DisplaySuffix += TXT("Alt + ");
			}

			if ((initOption.RequiredState & KS_Shift) > 0)
			{
				newCmd.DisplaySuffix += TXT("Shft + ");
			}

			newCmd.DisplaySuffix += InputUtils::TranslateToHumanReadableText(initOption.Keybind) + TXT(")");
			newCmd.Keybind = initOption.Keybind;
			newCmd.RequiredState = initOption.RequiredState;
		}

		newCmd.Data = initOption.Data;
		newCmd.OnCommand = initOption.OnCommand;
	}

	//The size is needed to determine the root menu's location.
	ComputeAbsTransform();

	//Reveal the root menu
	std::vector<size_t> rootOptions;
	for (size_t i = 0; i < AllOptions.size(); ++i)
	{
		if (AllOptions.at(i).ParentOption == INDEX_NONE)
		{
			rootOptions.push_back(i);
		}
	}

	RevealMenuBlock(rootOptions, contextComp, mouse->ToWindowCoordinates());
}

void DynamicListMenu::SetHoverActivationDelay (Float newHoverActivationDelay)
{
	HoverActivationDelay = newHoverActivationDelay;
}

void DynamicListMenu::SetCollapseMenuDelay (Float newCollapseMenuDelay)
{
	CollapseMenuDelay = newCollapseMenuDelay;
}

void DynamicListMenu::SetFadeOutDuration (Float newFadeOutDuration)
{
	FadeOutDuration = newFadeOutDuration;
}

void DynamicListMenu::SetOptionSize (Float newOptionSize)
{
	OptionSize = newOptionSize;
}

void DynamicListMenu::SetMinSuffixDistance (Float newMinSuffixDistance)
{
	MinSuffixDistance = newMinSuffixDistance;
}

void DynamicListMenu::SetMinMenuWidth (Float newMinMenuWidth)
{
	MinMenuWidth = newMinMenuWidth;
}

void DynamicListMenu::SetOptionSpacing (Float newOptionSpacing)
{
	OptionSpacing = newOptionSpacing;
}

void DynamicListMenu::SetSuffixPadding (Float newSuffixPadding)
{
	SuffixPadding = newSuffixPadding;
}

void DynamicListMenu::SetDividerHeightMultiplier (Float newDividerHeightMultiplier)
{
	DividerHeightMultiplier = newDividerHeightMultiplier;
}

void DynamicListMenu::SetDividerVisibleHeight (Float newDividerVisibleHeight)
{
	DividerVisibleHeight = newDividerVisibleHeight;
}

void DynamicListMenu::SetDividerColor (Color newDividerColor)
{
	DividerColor = newDividerColor;
}

void DynamicListMenu::SetBackgroundColor (Color newBackgroundColor)
{
	BackgroundColor = newBackgroundColor;
}

void DynamicListMenu::SetHighlightColor (Color newHighlightColor)
{
	HighlightColor = newHighlightColor;
}

void DynamicListMenu::SetPrimaryTextColor (Color newPrimaryTextColor)
{
	PrimaryTextColor = newPrimaryTextColor;
}

void DynamicListMenu::SetSecondaryTextColor (Color newSecondaryTextColor)
{
	SecondaryTextColor = newSecondaryTextColor;
}

Int DynamicListMenu::CalcHoveredOption (const Vector2& mousePos, const SMenuBlock& block) const
{
	CHECK(block.Frame != nullptr)

	Float curY = block.Frame->ReadCachedAbsPosition().Y;
	curY += GetTopMargin();
	for (size_t optionIdx : block.Options)
	{
		Float additionalHeight = OptionSize + OptionSpacing;
		if (AllOptions.at(optionIdx).bIsDivider)
		{
			additionalHeight *= DividerHeightMultiplier;
		}

		if (mousePos.Y <= curY + additionalHeight)
		{
			return optionIdx;
		}

		curY += additionalHeight;
	}

	return INT_INDEX_NONE;
}

bool DynamicListMenu::HighlightIdxInBlock (SMenuBlock& outBlock, Int newHoverIdx)
{
	outBlock.HoverIdx = newHoverIdx;
	if (ContainerUtils::IsValidIndex(AllOptions, outBlock.HoverIdx))
	{
		SMenuOption& hoveredOption = AllOptions.at(newHoverIdx.ToUnsignedInt());
		outBlock.ColorTransform->SetVisibility(!hoveredOption.bIsDivider); //Hide highlighted line if hovering over a divider
		if (!hoveredOption.bIsDivider)
		{
			UpdateHoverTransform(OUT outBlock);
		}

		//If this menu can expand to other menus
		return (!ContainerUtils::IsEmpty(hoveredOption.SubOptions));
	}

	return false;
}

void DynamicListMenu::UpdateHoverTransform (const SMenuBlock& block)
{
	CHECK(block.ColorTransform != nullptr)

	if (!ContainerUtils::IsValidIndex(AllOptions, block.HoverIdx))
	{
		block.ColorTransform->SetVisibility(false);
		return;
	}

	block.ColorTransform->SetVisibility(true);
	size_t expectedIdx = block.HoverIdx.ToUnsignedInt();
	Float yPos = GetTopMargin();
	for (size_t i : block.Options)
	{
		if (i == expectedIdx)
		{
			break;
		}

		Float delta = OptionSize + OptionSpacing;
		if (i == INDEX_NONE || AllOptions.at(i).bIsDivider)
		{
			delta *= DividerHeightMultiplier;
		}

		yPos += delta;
	}
	
	block.ColorTransform->EditPosition().Y = yPos;
}

void DynamicListMenu::RevealMenuBlock (const std::vector<size_t>& commandIndices, const Vector2& primaryLocation, const Vector2& secondaryLocation)
{
	FrameComponent* frame = InitializeNewMenuBlock(commandIndices);
	if (frame == nullptr)
	{
		return;
	}

	//Now that the size is computed, need to identify where this menu should be placed.
	Vector2 newLocation(primaryLocation);
	if (primaryLocation.X + frame->ReadCachedAbsSize().X <= ReadCachedAbsSize().X)
	{
		//Noop
	}
	else if (secondaryLocation.X - frame->ReadCachedAbsSize().X >= 0.f)
	{
		//Since using secondaryLocation, have the right border of this menu touch the secondaryLocation.
		newLocation.X = (secondaryLocation.X - frame->ReadCachedAbsSize().X);
		newLocation.Y = secondaryLocation.Y;
	}
	else
	{
		newLocation.X = 0.f;
	}

	if (newLocation.Y + frame->ReadCachedAbsSize().Y > ReadCachedAbsSize().Y)
	{
		//Move the menu up so that its bottom border aligns with this GuiEntity's bottom border.
		newLocation.Y = (ReadCachedAbsSize().Y - frame->ReadCachedAbsSize().Y);

		if (newLocation.Y < 0.f)
		{
			//This menu is to too tall. Best we can do is top align at this point.
			newLocation.Y = 0.f;
		}
	}

	frame->SetPosition(newLocation);
}

void DynamicListMenu::RevealMenuBlock (const std::vector<size_t>& commandIndices, ContextMenuComponent* contextComp, const Vector2& revealLocation)
{
	FrameComponent* frame = InitializeNewMenuBlock(commandIndices);
	if (frame != nullptr && contextComp != nullptr)
	{
		Vector2 newPos = contextComp->CalcRevealLocation(frame->ReadCachedAbsSize(), revealLocation, ReadCachedAbsSize());
		frame->SetPosition(newPos);
	}
}

FrameComponent* DynamicListMenu::InitializeNewMenuBlock (const std::vector<size_t>& commandIndices)
{
	SMenuBlock* newBlock = new SMenuBlock();
	VisibleMenus.push_back(newBlock);
	newBlock->Options = commandIndices;

	newBlock->Frame = FrameComponent::CreateObject();
	if (AddComponent(newBlock->Frame))
	{
		newBlock->Frame->SetCenterColor(BackgroundColor);
		if (BorderRenderComponent* borderComp = newBlock->Frame->GetBorderComp())
		{
			borderComp->Destroy();
		}
		newBlock->Frame->SetBorderThickness(0.f);

		if (!ContainerUtils::IsEmpty(VisibleMenus) && VisibleMenus.at(0)->Frame != nullptr)
		{
			newBlock->Frame->SetVisibility(VisibleMenus.at(0)->Frame->IsVisible());
		}

		const Font* usedFont = nullptr;

		//Add hover components first to render it behind the text.
		newBlock->ColorTransform = PlanarTransformComponent::CreateObject();
		if (newBlock->Frame->AddComponent(newBlock->ColorTransform))
		{
			newBlock->ColorTransform->SetPosition(Vector2::ZERO_VECTOR);
			newBlock->ColorTransform->SetSize(Vector2(1.f, OptionSize + OptionSpacing));
			newBlock->ColorTransform->SetVisibility(false); //Invisible until the user hovers over an option.

			ColorRenderComponent* colorComp = ColorRenderComponent::CreateObject();
			if (newBlock->ColorTransform->AddComponent(colorComp))
			{
				colorComp->SolidColor = HighlightColor;
			}
		}

		Float curY = GetTopMargin();
		size_t nextIdx = 0;

		while (nextIdx < commandIndices.size())
		{
			Int numLines = 0;
			DString labelTxt;
			DString suffixes;
			for (size_t i = nextIdx; i < commandIndices.size(); ++i)
			{
				if (commandIndices.at(i) == INDEX_NONE)
				{
					if (numLines == 0)
					{
						++nextIdx; //Prevents infinite recursion
					}

					break;
				}

				SMenuOption& option = AllOptions.at(commandIndices.at(i));
				if (option.bIsDivider)
				{
					if (numLines == 0)
					{
						++nextIdx; //Prevents infinite recursion
					}

					break;
				}

				++nextIdx;
				++numLines;
				labelTxt += option.DisplayText + TXT("\n");
				suffixes += option.DisplaySuffix + TXT("\n");
			}

			if (numLines <= 0)
			{
				//This is a divider, use a render component instead of label components
				Float dividerHeight = (OptionSize + OptionSpacing) * DividerHeightMultiplier;
				PlanarTransformComponent* dividerTransform = PlanarTransformComponent::CreateObject();
				if (newBlock->Frame->AddComponent(dividerTransform))
				{
					dividerTransform->SetPosition(Vector2(1.f, curY + (dividerHeight*0.5f) ));
					dividerTransform->SetSize(Vector2(1.f, dividerHeight * DividerVisibleHeight));
					dividerTransform->SetAnchorLeft(0.15f);
					dividerTransform->SetAnchorRight(0.15f);

					ColorRenderComponent* color = ColorRenderComponent::CreateObject();
					if (dividerTransform->AddComponent(color))
					{
						color->SolidColor = DividerColor;
						color->SetPivot(0.f, 0.5f);
					}
				}
				
				curY += dividerHeight;
			}
			else
			{
				LabelComponent* label = LabelComponent::CreateObject();
				if (newBlock->Frame->AddComponent(label))
				{
					if (TextRenderComponent* textRender = label->GetRenderComponent())
					{
						textRender->SetFontColor(PrimaryTextColor.Source);
					}

					label->SetAutoRefresh(false);
					label->SetPosition(Vector2(0.f, curY));
					label->SetSize(Vector2(1.f, (OptionSize + OptionSpacing) * numLines.ToFloat()));
					label->SetWrapText(false);
					label->SetClampText(true);
					label->SetCharacterSize(OptionSize.ToInt());
					label->SetLineSpacing(OptionSpacing);
					label->SetText(labelTxt);
					label->SetAutoRefresh(true);
					usedFont = label->GetFont();
				}

				LabelComponent* suffixLabel = LabelComponent::CreateObject();
				if (newBlock->Frame->AddComponent(suffixLabel))
				{
					if (TextRenderComponent* textRender = suffixLabel->GetRenderComponent())
					{
						textRender->SetFontColor(SecondaryTextColor.Source);
					}

					suffixLabel->SetAutoRefresh(false);
					suffixLabel->SetPosition(Vector2(0.f, curY));
					suffixLabel->SetSize(Vector2(1.f - SuffixPadding, (OptionSize + OptionSpacing) * numLines.ToFloat()));
					suffixLabel->SetWrapText(false);
					suffixLabel->SetClampText(true);
					suffixLabel->SetCharacterSize(OptionSize.ToInt());
					suffixLabel->SetLineSpacing(OptionSpacing);
					suffixLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
					suffixLabel->SetText(suffixes);
					suffixLabel->SetAutoRefresh(true);
				}

				curY += (OptionSize + OptionSpacing) * numLines.ToFloat();
			}
		}

		//Need to identify the widest line
		Float widestLine = MinMenuWidth;

		if (usedFont != nullptr)
		{
			for (size_t commandIdx : commandIndices)
			{
				if (!ContainerUtils::IsValidIndex(AllOptions, commandIdx))
				{
					//Ignore widest line
					continue;
				}

				DString fullText = AllOptions.at(commandIdx).DisplayText + AllOptions.at(commandIdx).DisplaySuffix;

				Float additionalWidth = 0.f;
				if (!AllOptions.at(commandIdx).DisplaySuffix.IsEmpty())
				{
					additionalWidth += SuffixPadding;
				}

				widestLine = Utils::Max(widestLine, usedFont->CalculateStringWidth(fullText, OptionSize.ToInt()) + additionalWidth);
			}
		}

		newBlock->Frame->SetSize(Vector2(widestLine + MinSuffixDistance, curY + GetTopMargin()));
		newBlock->Frame->ComputeAbsTransform();
	}

	return newBlock->Frame;
}

void DynamicListMenu::RemoveLastVisibleMenu ()
{
	if (VisibleMenus.size() <= 1)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to remove the last visible menu from DynamicListMenu since there %s visible menus. To remove the root menu, call Destroy instead."), Int(VisibleMenus.size()));
		Destroy();
		return;
	}

	SMenuBlock* lastBlock = ContainerUtils::GetLast(VisibleMenus);
	if (lastBlock->Frame != nullptr)
	{
		lastBlock->Frame->Destroy(); //This also destroys all components attached to this
		lastBlock->Frame = nullptr;
		delete lastBlock;
	}
	VisibleMenus.pop_back();

	//Notify the next top menu that it's no longer expanded.
	SMenuBlock* newTopMenu = ContainerUtils::GetLast(VisibleMenus);
	newTopMenu->ExpandedOption = INT_INDEX_NONE;
}

void DynamicListMenu::BeginFadeOut ()
{
	if (FadeOutDuration <= 0.f)
	{
		//No fade out. Destroy this Entity immediately.
		Destroy();
		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	FadeOutTimestamp = localEngine->GetElapsedTime();
	HoveredCommandTimestamp = -1.f;
	UnhoveredMenuTimestamp = -1.f;
}

void DynamicListMenu::ClearAllOptions ()
{
	for (size_t i = 0; i < AllOptions.size(); ++i)
	{
		if (AllOptions.at(i).Data != nullptr)
		{
			delete AllOptions.at(i).Data;
			AllOptions.at(i).Data = nullptr;
		}
	}

	ContainerUtils::Empty(OUT AllOptions);
}

void DynamicListMenu::HoverMonitorTick (Float deltaSec)
{
	if (FadeOutTimestamp >= 0.f)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (localEngine->GetElapsedTime() - FadeOutTimestamp >= FadeOutDuration)
		{
			Destroy();
		}
		else
		{
			Float ratio = (localEngine->GetElapsedTime() - FadeOutTimestamp) / FadeOutDuration;
			sf::Uint8 alpha = Utils::Lerp<sf::Uint8>(ratio.Value, 255, 0);

			for (SMenuBlock* block : VisibleMenus)
			{
				if (block->Frame == nullptr)
				{
					continue;
				}

				if (ColorRenderComponent* colorComp = dynamic_cast<ColorRenderComponent*>(block->Frame->GetCenterComp()))
				{
					Color newColor = colorComp->SolidColor;
					newColor.Source.a = alpha;
					colorComp->SolidColor = newColor;
				}

				for (ComponentIterator iter(block->Frame, true); iter.GetSelectedComponent() != nullptr; ++iter)
				{
					if (ColorRenderComponent* colorComp = dynamic_cast<ColorRenderComponent*>(iter.GetSelectedComponent()))
					{
						colorComp->SolidColor.Source.a = alpha;
					}
					else if (LabelComponent* labelComp = dynamic_cast<LabelComponent*>(iter.GetSelectedComponent()))
					{
						if (TextRenderComponent* textRender = labelComp->GetRenderComponent())
						{
							sf::Color newColor = textRender->GetFontColor();
							newColor.a = alpha;
							textRender->SetFontColor(newColor);
						}
					}
				}
			}
		}

		//Don't bother calculating the rest of the latent operations if this entity is in the middle of fading out.
		return;
	}
	
	if (HoveredCommandTimestamp >= 0.f && VisibleMenus.size() > 0)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (localEngine->GetElapsedTime() - HoveredCommandTimestamp >= HoverActivationDelay)
		{
			HoveredCommandTimestamp = -1.f;
			if (ContainerUtils::IsValidIndex(VisibleMenus, LatestHoveredMenu))
			{
				size_t i = LatestHoveredMenu;

				//Collapse any menus between this menu and the top menu
				while (i+1 < VisibleMenus.size())
				{
					RemoveLastVisibleMenu();
				}

				SMenuBlock* targetMenu = VisibleMenus.at(LatestHoveredMenu);
				if (ContainerUtils::IsValidIndex(AllOptions, targetMenu->HoverIdx))
				{
					SMenuOption& hoveredOption = AllOptions.at(targetMenu->HoverIdx.ToUnsignedInt());
					if (hoveredOption.SubOptions.size() > 0)
					{
						Vector2 primaryLocation;
						Vector2 secondaryLocation;
						if (PlanarTransformComponent* colorTransform = targetMenu->ColorTransform)
						{
							secondaryLocation = colorTransform->ReadCachedAbsPosition();
							primaryLocation = secondaryLocation;
							primaryLocation.X += colorTransform->ReadCachedAbsSize().X;
						}

						RevealMenuBlock(hoveredOption.SubOptions, primaryLocation, secondaryLocation);
						targetMenu->ExpandedOption = targetMenu->HoverIdx;
						UnhoveredMenuTimestamp = -1.f; //Don't immediately collapse this new menu.
					}
				}
			}
		}
	}

	if (UnhoveredMenuTimestamp >= 0.f && VisibleMenus.size() > 1)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (localEngine->GetElapsedTime() - UnhoveredMenuTimestamp >= CollapseMenuDelay)
		{
			UnhoveredMenuTimestamp = localEngine->GetElapsedTime(); //Reset the timer to add another delay before popping the next menu
			RemoveLastVisibleMenu();

			if (VisibleMenus.size() <= 1)
			{
				//Disable collapsing last visible menu if only the root menu is showing.
				UnhoveredMenuTimestamp = -1.f;
			}

			if (LatestHoveredMenu != INDEX_NONE && (LatestHoveredMenu + 1) >= VisibleMenus.size())
			{
				//The user is currently hovering over the new top menu, stop the timer.
				UnhoveredMenuTimestamp = -1.f;
			}
		}
	}
}
SD_END