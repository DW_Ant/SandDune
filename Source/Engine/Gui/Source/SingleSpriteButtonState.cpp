/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SingleSpriteButtonState.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "FrameComponent.h"
#include "SingleSpriteButtonState.h"

IMPLEMENT_CLASS(SD::SingleSpriteButtonState, SD::ButtonStateComponent)
SD_BEGIN

void SingleSpriteButtonState::CopyPropertiesFrom (const CopiableObjectInterface* cpy)
{
	//Noop
}

void SingleSpriteButtonState::SetDefaultAppearance ()
{
	SpriteComponent* buttonSprite = GetSpriteComp();
	if (buttonSprite != nullptr)
	{
		buttonSprite->SetSubDivision(1, 4, 0, 0);
	}
}

void SingleSpriteButtonState::SetDisableAppearance ()
{
	SpriteComponent* buttonSprite = GetSpriteComp();
	if (buttonSprite != nullptr)
	{
		buttonSprite->SetSubDivision(1, 4, 0, 1);
	}
}

void SingleSpriteButtonState::SetHoverAppearance ()
{
	SpriteComponent* buttonSprite = GetSpriteComp();
	if (buttonSprite != nullptr)
	{
		buttonSprite->SetSubDivision(1, 4, 0, 2);
	}
}

void SingleSpriteButtonState::SetDownAppearance ()
{
	SpriteComponent* buttonSprite = GetSpriteComp();
	if (buttonSprite != nullptr)
	{
		buttonSprite->SetSubDivision(1, 4, 0, 3);
	}
}

SpriteComponent* SingleSpriteButtonState::GetSpriteComp () const
{
	if (OwningButton != nullptr && OwningButton->GetBackground() != nullptr)
	{
		return OwningButton->GetBackground()->GetCenterCompAs<SpriteComponent>();
	}

	return nullptr;
}
SD_END