/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ButtonComponent.cpp
=====================================================================
*/

#include "ButtonComponent.h"
#include "ButtonStateComponent.h"
#include "FrameComponent.h"
#include "LabelComponent.h"

IMPLEMENT_CLASS(SD::ButtonComponent, SD::GuiComponent)
SD_BEGIN

void ButtonComponent::InitProps ()
{
	Super::InitProps();

	bEnabled = true;
	bPressedDown = false;
	bHovered = false;

	bPendingReleaseButton = false;
}

void ButtonComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ButtonComponent* buttonTemplate = dynamic_cast<const ButtonComponent*>(objTemplate);
	if (buttonTemplate != nullptr)
	{
		bool bCreatedObject;
		CaptionComponent = ReplaceTargetWithObjOfMatchingClass(CaptionComponent.Get(), buttonTemplate->CaptionComponent.Get(), OUT bCreatedObject);
		if (bCreatedObject)
		{
			AddComponent(CaptionComponent);
		}

		if (CaptionComponent.IsValid())
		{
			CaptionComponent->CopyPropertiesFrom(buttonTemplate->CaptionComponent.Get());
		}

		Background = ReplaceTargetWithObjOfMatchingClass(GetBackground(), buttonTemplate->GetBackground(), OUT bCreatedObject);
		if (bCreatedObject && AddComponent(Background))
		{
			if (SpriteComponent* centerSprite = Background->GetCenterCompAs<SpriteComponent>())
			{
				centerSprite->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, ButtonComponent, HandleSpriteTextureChanged, void));
			}
		}

		if (Background.IsValid())
		{
			Background->CopyPropertiesFrom(buttonTemplate->GetBackground());
		}

		if (GetStateComponent() == nullptr && buttonTemplate->GetStateComponent() != nullptr)
		{
			//This component is missing a state component and the template has one
			ButtonStateComponent* newState = ReplaceStateComponent(buttonTemplate->GetStateComponent()->StaticClass());
			newState->CopyPropertiesFrom(buttonTemplate->GetStateComponent());
		}
		else if (buttonTemplate->GetStateComponent() == nullptr)
		{
			//This component has a button state component but template does not.
			ReplaceStateComponent(nullptr);
		}
		else if (buttonTemplate->GetStateComponent()->StaticClass() != GetStateComponent()->StaticClass())
		{
			//Both have state components but the classes are different.
			ButtonStateComponent* newState = ReplaceStateComponent(buttonTemplate->GetStateComponent()->StaticClass());
			newState->CopyPropertiesFrom(buttonTemplate->GetStateComponent());
		}
	}
}

bool ButtonComponent::CanBeFocused () const
{
	return FocusInterface::CanBeFocused() && GetEnabled() && IsVisible();
}

void ButtonComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	if (bPressedDown) //Cancel out of selection (Pressing tab while Return key is held)
	{
		SetButtonUp(false, false);
	}
}

bool ButtonComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (keyEvent.key.code == sf::Keyboard::Return)
	{
		if (keyEvent.type == sf::Event::KeyPressed)
		{
			SetButtonDown(true, true);
			return true;
		}
		else if (keyEvent.type == sf::Event::KeyReleased)
		{
			SetButtonUp(true, true);
			return true;
		}
	}

	return false;
}

bool ButtonComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return false;
}

void ButtonComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	bool bNewHovered = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));

	if (bNewHovered != bHovered)
	{
		bHovered = bNewHovered;
		if (StateComponent.IsValid())
		{
			if (bHovered)
			{
				(bPressedDown) ? StateComponent->SetDownAppearance() : StateComponent->SetHoverAppearance();
			}
			else
			{
				StateComponent->SetDefaultAppearance();
			}
		}
	}
}

void ButtonComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	bool isRelevant = (sfmlEvent.button == sf::Mouse::Left || (sfmlEvent.button == sf::Mouse::Right && HasRightClickHandler()));
	if (!isRelevant || !IsVisible())
	{
		return;
	}

	if (bPressedDown && eventType == sf::Event::MouseButtonReleased)
	{
		//Don't invoke delegate yet. That should be handled in the consume mouse click function so that other components listening to nonconsumable events may hear the event before the delegate does things.
		bPendingReleaseButton = true;
	}
}

bool ButtonComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//Buttons only care for left mouse button (or right mouse button if there's a function bound to it)
	bool isRelevant = (sfmlEvent.button == sf::Mouse::Left || (sfmlEvent.button == sf::Mouse::Right && HasRightClickHandler()));
	if (!isRelevant || !IsVisible())
	{
		return false;
	}

	bool isWithinBounds = IsWithinBounds(Vector2(Float::MakeFloat(sfmlEvent.x), Float::MakeFloat(sfmlEvent.y)));
	if (bPendingReleaseButton)
	{
		SetButtonUp(isWithinBounds, sfmlEvent.button == sf::Mouse::Left);
		bPendingReleaseButton = false;
		return isWithinBounds;
	}

	if (!bPressedDown && eventType == sf::Event::MouseButtonPressed && isWithinBounds)
	{
		SetButtonDown(true, sfmlEvent.button == sf::Mouse::Left);
		return true;
	}

	return false;
}

bool ButtonComponent::AcceptsMouseEvents (EInputEvent inputEvent, const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(inputEvent, mousePosX, mousePosY));
}

void ButtonComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeBackground();
	InitializeCaptionComponent();
}

void ButtonComponent::SetButtonDown (bool bInvokeDelegate, bool isLeftClick)
{
	if (StateComponent.IsValid())
	{
		StateComponent->SetDownAppearance();
	}

	if (bInvokeDelegate)
	{
		if (isLeftClick && OnButtonPressed.IsBounded())
		{
			OnButtonPressed(this);
		}
		else if (!isLeftClick && OnButtonPressedRightClick.IsBounded())
		{
			OnButtonPressedRightClick(this);
		}
	}

	bPressedDown = true;
}

void ButtonComponent::SetButtonUp (bool bInvokeDelegate, bool isLeftClick)
{
	if (StateComponent.IsValid())
	{
		//Restore button state appearance regardless if the button was released beyond bounds or not
		(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
	}

	bPressedDown = false;

	if (bInvokeDelegate)
	{
		if (isLeftClick && OnButtonReleased.IsBounded())
		{
			OnButtonReleased(this);
		}
		else if (!isLeftClick && OnButtonReleasedRightClick.IsBounded())
		{
			OnButtonReleasedRightClick(this);
		}
	}
}

void ButtonComponent::SetButtonPressedHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonPressed = newHandler;
}

void ButtonComponent::SetButtonReleasedHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonReleased = newHandler;
}

void ButtonComponent::SetButtonPressedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonPressedRightClick = newHandler;
}

void ButtonComponent::SetButtonReleasedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonReleasedRightClick = newHandler;
}

void ButtonComponent::SetCaptionText (DString newText)
{
	if (CaptionComponent.IsValid())
	{
		CaptionComponent->SetText(newText);
	}
}

void ButtonComponent::SetBackground (FrameComponent* newBackground)
{
	if (Background.IsValid())
	{
		Background->Destroy();
	}

	Background = newBackground;
	if (Background.IsValid())
	{
		AddComponent(Background);
	}
}

void ButtonComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;
	bHovered = false;
	bPressedDown = false;

	if (StateComponent.IsValid())
	{
		if (bEnabled)
		{
			(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
		}
		else
		{
			StateComponent->SetDisableAppearance();
		}
	}
}

ButtonStateComponent* ButtonComponent::ReplaceStateComponent (const DClass* newButtonStateClass)
{
	if (newButtonStateClass == nullptr)
	{
		//Simply destroy the old
		if (StateComponent.IsValid())
		{
			StateComponent->Destroy();
			StateComponent = nullptr;
		}

		return nullptr;
	}

	const ButtonStateComponent* defaultButtonState = dynamic_cast<const ButtonStateComponent*>(newButtonStateClass->GetDefaultObject());
	if (!defaultButtonState)
	{
		return nullptr;
	}

	ButtonStateComponent* oldButtonState = StateComponent.Get();
	StateComponent = defaultButtonState->CreateObjectOfMatchingClass();
	if (StateComponent.IsNullptr())
	{
		StateComponent = oldButtonState;
		return nullptr;
	}

	if (!AddComponent(StateComponent))
	{
		StateComponent = oldButtonState;
		return nullptr;
	}

	if (oldButtonState != nullptr)
	{
		RemoveComponent(oldButtonState);
		oldButtonState->Destroy();
	}

	StateComponent->RefreshState();
	return StateComponent.Get();
}

bool ButtonComponent::GetEnabled () const
{
	return bEnabled;
}

bool ButtonComponent::GetHovered () const
{
	return bHovered;
}

bool ButtonComponent::GetPressedDown () const
{
	return bPressedDown;
}

DString ButtonComponent::GetCaptionText () const
{
	if (CaptionComponent.IsValid())
	{
		return CaptionComponent->GetContent();
	}

	return DString::EmptyString;
}

RenderComponent* ButtonComponent::GetRenderComponent() const
{
	if (Background != nullptr)
	{
		return Background->GetCenterComp();
	}

	return nullptr;
}

void ButtonComponent::InitializeBackground ()
{
	Background = FrameComponent::CreateObject();

	if (AddComponent(Background))
	{
		//Remove borders
		if (BorderRenderComponent* borderComp = Background->GetBorderComp())
		{
			borderComp->Destroy();
		}

		Background->SetBorderThickness(0.f);
		Background->SetLockedFrame(true);

		Background->SetCenterTexture(GuiTheme::GetGuiTheme()->ButtonFill.Get());
		if (SpriteComponent* spriteComp = Background->GetCenterCompAs<SpriteComponent>())
		{
			spriteComp->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, ButtonComponent, HandleSpriteTextureChanged, void));
		}
	}
}

void ButtonComponent::InitializeCaptionComponent ()
{
	Float borderThickness = (Background.IsValid()) ? Background->GetBorderThickness() : 0;

	CaptionComponent = LabelComponent::CreateObject();
	GuiComponent* owningComponent = this;
	if (Background.IsValid())
	{
		//The text's owning component should be the background (if any) for draw priority reasons.
		owningComponent = Background.Get();
	}

	if (owningComponent->AddComponent(CaptionComponent))
	{
		CaptionComponent->SetAutoRefresh(false);

		CaptionComponent->SetSize(Vector2(1.f, 1.f));
		CaptionComponent->SetPosition(Vector2::ZERO_VECTOR);

		CaptionComponent->SetLineSpacing(0);
		CaptionComponent->SetHorizontalAlignment(LabelComponent::HA_Center);
		CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Center);
		CaptionComponent->SetAutoRefresh(true);

		SetCaptionText(TXT("Button"));
	}
}

void ButtonComponent::HandleSpriteTextureChanged ()
{
	if (StateComponent.IsValid())
	{
		StateComponent->RefreshState();
	}
}
SD_END