/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderComponent.h
  A component class responsible for drawing content to a RenderTarget.
=====================================================================
*/

#pragma once

#include "Aabb.h"
#include "Graphics.h"

SD_BEGIN
class Camera;
class RenderTarget;
class ScreenCapture;
class Transformation;

class GRAPHICS_API RenderComponent : public EntityComponent, public CopiableObjectInterface
{
	DECLARE_CLASS(RenderComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
#ifdef DEBUG_MODE
	/* Number of scene draw layers this render component is registered to. This is used for debugging purposes. Otherwise this variable serves no purpose. */
	Int NumRegisteredSceneLayers;
#endif

private:
	/* Transform object that influences where this Entity should draw in the RenderTarget. */
	Transformation* OwnerTransform;

	/* The latest computed transformed Aabb. If it's empty, then it's marked for dirty where either the owner transform
	changed its or if this render component adjusted its size/position. */
	mutable Aabb CachedTransformedAabb;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Implementation that draws something to the given renderTarget from the camera perspective.
	 */
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) = 0;

	/**
	 * Returns the axis aligned bounding box that encompasses this render component without any transformation data.
	 * This assumes this render component resides at the origin with a scalar value of 1.
	 *
	 * For 2D render components that share Planar and Scene Transforms, the returned Aabb should assume the height is zero
	 * so it may be used in both coordinate spaces (there is no Z in planar transforms).
	 */
	virtual Aabb GetAabb () const = 0;

	/**
	 * Returns the axis aligned bounding box that encompasses this render component that is adjusted based on the owner
	 * transformation.
	 */
	virtual Aabb GetTransformedAabb () const;

	/**
	 * Forces this RenderComponent to recompute its TransformedAabb next time GetTransformedAabb is called.
	 */
	virtual void MarkTransformedAabbDirty ();

	/**
	 * Returns true if the entity should even be added to the RenderTarget's draw order array.
	 */
	virtual bool IsRelevant (const RenderTarget* renderTarget, const Camera* camera) const;

	/**
	 * Returns true if this component should always be relevant when the given ScreenCapture object is capturing a snapshot.
	 * Example for when this should be true, if a sprite component is rendering a RenderTexture, it should return true so that the components within the RenderTexture are also captured.
	 */
	virtual bool IsAlwaysScreenCapRelevant (ScreenCapture* screenCap) const;

	/**
	 * Quick utility to register this render component to all default scene draw layers found in the GraphicsEngineComponent.
	 */
	virtual void RegisterToDefaultSceneLayers ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Transformation* GetOwnerTransform () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTransformChanged ();
};
SD_END