/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorTester.h
  Object that'll be testing the ColorRenderComponent.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef DEBUG_MODE
#include "PlanarTransform.h"

SD_BEGIN
class ColorRenderComponent;

class GRAPHICS_API ColorTester : public Entity, public PlanarTransform
{
	DECLARE_CLASS(ColorTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<ColorRenderComponent> ColorComponent;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual bool BeginTest (UnitTester::EUnitTestFlags testFlags);
};
SD_END

#endif