/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteComponent.h
  A render component that's responsible for drawing a single sprite.
=====================================================================
*/

#pragma once

#include "Aabb.h"
#include "RenderComponent.h"

SD_BEGIN
class RenderTexture;
class ScreenCapture;
class Shader;
class Texture;

class GRAPHICS_API SpriteComponent : public RenderComponent
{
	DECLARE_CLASS(SpriteComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EDrawMode
	{
		/* Draw the sprite so that its edges stretches to the edge of the sprite. */
		DM_Stretch,

		/* Preserves the texture's aspect ratio regardless of the sprite size. It keeps repeating the texture until it fits the size.
		By default, it'll start with the top left corner, but use the SetDrawCoordinates function to override. */
		DM_Tiled
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of callbacks to invoke whenever the texture is updated. */
	MulticastDelegate<> OnTextureChangedCallbacks;

protected:
	/* Specifies how the texture fills the sprite instance. */
	EDrawMode DrawMode;

	/* The instanced SFML's object that'll handle drawing the actual sprite. */
	sf::Sprite* Sprite;

	/* Special affect that's applied over the sprite. */
	DPointer<Shader> SpriteShader;

	/* Render state that'll is used how the sprite is drawn to the render target. */
	sf::RenderStates RenderState;

	/* Base size of the SpriteComponent. Used to scale the sprite without scaling any components attached to it. */
	Vector2 BaseSize;

	/* Determines the draw offset for this sprite using normalized coordinates where (0,0) would draw from the top left corner. (0.5, 0.5) would draw from the center. (1,1) would draw from bottom right corner. */
	Vector2 Pivot;

	/* Scale multipliers based on displaying subdivision or draw coordinates of a sprite.
	Formula:   SubDivisionScale = TextureSize / UserSpecifiedTextureRegion (in pixels) */
	Float SubDivideScaleX;
	Float SubDivideScaleY;

	/* Becomes true if this component should not render the Sprite object.  Setting the sprite's texture to nullptr
	leads to undefined behavior.  The component will check this flag before attempting to render it. */
	bool InvisibleTexture;

	/* Only valid when rendering a texture that came from a RenderTexture.  This is used to notify the RenderTexture
	where its viewport is drawn on the window. */
	DPointer<RenderTexture> RenderTextureOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;
	virtual bool IsAlwaysScreenCapRelevant (ScreenCapture* screenCap) const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the sfml's sprite sub rectangle based on the split and display values.
	 * numHorizontal/VerticalSegments determine the number of equal parts.  (2 = splitting in half, 4 = splitting in quarters)
	 * horizontal/verticalIndex determines which segment to display.  Reads from left->right and top->bottom.
	 * 	If horizontalIndex is 4, then it'll display the fourth segment from the left.
	 * 	If verticalIndex is 0, then it'll display the top segment.
	 * Function returns false if the specified parameters are not valid and no changes were applied to the sprite.
	 * Note:  Calling this will be overriding SetDrawCoordinates if it returns true.
	 */
	virtual bool SetSubDivision (Int numHorizontalSegments, Int numVerticalSegments, Int horizontalIndex, Int verticalIndex);

	/**
	 * Scales the draw coordinates using multipliers relative to the sprite size.
	 * @Param ScaleMultipliers:  Determines how much of the texture is drawn.  Setting 0.5 will draw half the texture.
	 * @Param posMultipliers:  Determines draw position offset.  Setting to 0.5 will move the center of the texture to top left corner.
	 * Note:  Calling this will be overriding SetSubDivision.
	 */
	virtual void SetDrawCoordinatesMultipliers (Float xScaleMultiplier, Float yScaleMultiplier, Float xPosMultiplier = 0.f, Float yPosMultiplier = 0.f);

	/**
	 * Same as SetDrawCoordinates(Floats), but this uses absolute coordinates instead of multipliers.
	 * U = x position, V = y position, UL = x Length, VL = y Length.
	 * Note:  Calling this will be overriding SetSubDivision.
	 */
	virtual void SetDrawCoordinates (Int u, Int v, Int ul, Int vl);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDrawMode (EDrawMode newDrawMode);

	/**
	 * Updates the texture to render for this sprite.  Note:  You should update the DrawCoordinates or SubDivision values after changing the texture.
	 */
	virtual void SetSpriteTexture (const sf::Texture* newTexture);
	virtual void SetSpriteTexture (const Texture* newTexture);
	virtual void SetSpriteTexture (RenderTexture* renderTextureOwner);
	virtual void ClearSpriteTexture ();

	/**
	 * Replaces the sprite's shader with the specified shader.  This will also update the new shader's base texture resource
	 * to reflect the sprite's current texture.
	 */
	virtual void SetSpriteShader (Shader* newSpriteShader);

	virtual void SetPivot (const Vector2& newPivot);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EDrawMode GetDrawMode () const
	{
		return DrawMode;
	}

	virtual sf::Sprite* GetSprite () const;
	virtual Shader* GetSpriteShader () const;

	/**
	 * Returns the base size of the texture (without transform scalars).
	 */
	virtual void GetTextureSize (Int& outWidth, Int& outHeight) const;
	virtual Vector2 GetTextureSize () const;

	inline const Vector2& ReadBaseSize () const
	{
		return BaseSize;
	}

	inline Vector2& EditBaseSize ()
	{
		return BaseSize;
	}

	inline const Vector2& ReadPivot () const
	{
		return Pivot;
	}

	inline Vector2& EditPivot ()
	{
		return Pivot;
	}

	inline RenderTexture* GetRenderTextureOwner () const
	{
		return RenderTextureOwner.Get();
	}

	/**
	 * Returns the draw coordinates of the texture this sprite is rendering.
	 * See SetDrawCoordinates and SetSubDivision.
	 * outU = xPosition of rectangle from the left border of texture.
	 * outV = yPosition of rectangle from the top border of texture.
	 * outUL = The width of the rectangle.
	 * outUV = The height of the rectangle.
	 */
	virtual void GetDrawCoordinates (Int& outU, Int& outV, Int& outUl, Int& outVl) const;
};
SD_END