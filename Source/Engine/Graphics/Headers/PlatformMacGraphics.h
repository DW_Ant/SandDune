/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacGraphics.h
  Utilities containing Mac-specific functions the Graphics module depends on.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef PLATFORM_MAC

SD_BEGIN
class Window;

/**
 * Retrieves the system's current Dots Per Inch setting.
 * Returns a negative value if it's not defined or if there's an error.
 */
Int GRAPHICS_API OS_GetSystemDpi (const Window* windowHandle);

/**
 * Returns coordinates of the given window within the OS's title bar and borders.
 * To get the whole window coordinates (with the titlebar), please refer to Window::GetWindowPosition.
 * The Rectangle uses X-Right and Y-Down coordinate system.
 */
Rectangle GRAPHICS_API OS_GetClientWindowPos (const Window* windowHandle);
SD_END

#endif //PLATFORM_MAC