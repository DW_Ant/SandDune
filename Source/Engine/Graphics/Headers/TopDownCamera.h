/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TopDownCamera.h
  An Orthographic camera that's able to view SceneTransformable Entities.
  Its view frustum is perpendicular to the X-Y plane.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API TopDownCamera : public SceneCamera
{
	DECLARE_CLASS(TopDownCamera)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the view frustum. This multiplier essentially acts like the camera's zoom level.
	The greater the value, the more 'zoomed in' it appears. A value of 1 renders everything with 1-to-1 scaling.
	A value of 2 makes everything render twice as large.
	When left at 1, the camera will render match 1-to-1 (1 cm on the screen corresponds to 1 cm in scene space). */
	Float Zoom;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const override;
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const override;
	virtual Float GetDistSquaredToPoint (const Vector3& worldLocation) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Computes the multiplier that converts from "pixel units" to "scene units"
	 */
	virtual Float GetScalarProjection () const;
};
SD_END