/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Texture.h
  An object responsible for storing a reference to a texture resource.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class TexturePool;

class GRAPHICS_API Texture : public Object
{
	DECLARE_CLASS(Texture)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	sf::Texture* TextureResource;

	/* The texture pool that is managing this texture. */
	TexturePool* OwningTexturePool;

	/* List of tags this texture is categorized. */
	std::vector<HashedString> Tags;

	Int Width;
	Int Height;
	DString TextureName;

	/* Becomes true when this texture generated mipmap. */
	bool bHasMipMap;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Release ();

	/**
	 * Overrides pixel data of the texture.
	 * @param newTexelData The vector of each texel data. The vector size must exactly match the size
	 * of this texture x 4. Each element in the vector corresponds to a single color channel in a texel.
	 *
	 * Texel[0]
	 *		newTexelData[0]=Red
	 *		newTexelData[1]=Green
	 *		newTexelData[2]=Blue
	 *		newTexelData[3]=Alpha
	 * Texel[1]
	 *		newTexelData[4]=Red
	 *		newTexelData[5]=Green
	 *		newTexelData[6]=Blue
	 *		newTexelData[7]=Alpha
	 * Texel[2]
	 * ...
	 *
	 * Each channel is a 8-bit value (from 0-255, where 0 corresponds to black. 255 corresponds to white).
	 * Returns true on success.
	 */
	virtual bool SetTexelData (const std::vector<sf::Uint8>& newTexelData);
	virtual bool SetTexelData (const std::vector<sf::Uint8>& newTexelData, Int width, Int height);

	virtual void SetSmooth (bool bSmooth);

	virtual void SetRepeat (bool bRepeating);

	/**
	 * Attempts to generate mipmap. Returns true on success or if it already has mipmap.
	 */
	virtual bool GenerateMipMap ();

	/**
	 * Destroys the previous resource, and sets the texture resource to the given params.
	 */
	virtual void SetResource (sf::Texture* newResource);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningTexturePool (TexturePool* newOwningTexturePool)
	{
		OwningTexturePool = newOwningTexturePool;
	}

	virtual void SetTextureName (const DString& newTextureName)
	{
		TextureName = newTextureName;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual sf::Texture* EditTextureResource ();

	inline TexturePool* GetOwningTexturePool () const
	{
		return OwningTexturePool;
	}

	inline const std::vector<HashedString>& ReadTags () const
	{
		return Tags;
	}

	inline std::vector<HashedString>& EditTags ()
	{
		return Tags;
	}

	virtual void GetDimensions (Int& outWidth, Int& outHeight) const;
	virtual void GetDimensions (Vector2& outDimensions) const;
	virtual Vector2 GetDimensions () const;
	virtual Int GetWidth () const;
	virtual Int GetHeight () const;
	virtual DString GetTextureName () const;

	/**
	 * Returns the SFML's texture resource.
	 */
	virtual const sf::Texture* GetTextureResource () const;

	/**
	 * Returns a copy of the raw image data that make up this texture. This is generally
	 * useful for manipulating textures dynamically.
	 *
	 * This is a slow operation since it downloads the texture data from the graphics
	 * card and copies them to a new image.
	 *
	 * Returns true if there is a valid image.
	 */
	virtual bool GetImage (sf::Image& outImage) const;

	inline bool HasMipMap () const
	{
		return bHasMipMap;
	}
};
SD_END