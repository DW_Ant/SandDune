/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResolutionComponent.h
  A component that'll broadcast its callback whenever its window handle
  changes resolution.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API ResolutionComponent : public EntityComponent
{
	DECLARE_CLASS(ResolutionComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever the window handle changes resolution.  Params:  newWidth, newHeight*/
	SDFunction<void, Int, Int> OnResolutionChange;

protected:
	/* The window this component will poll events from to detect resolution changes. */
	DPointer<Window> WindowHandle;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetWindowHandle (Window* newWindowHandle);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Window* GetWindowHandle () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWindowEvent (const sf::Event& newEvent);
};
SD_END