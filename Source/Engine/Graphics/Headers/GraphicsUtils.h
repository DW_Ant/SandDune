/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsUtils.h
  An object containing utilities related to the Graphics library.
  This also contains SFML utility functions.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
#pragma region "External Operators"
template <typename T>
sf::Vector2<T> operator* (const sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	return sf::Vector2<T>(left.x * right.x, left.y * right.y);
}

template <typename T>
sf::Vector2<T>& operator*= (sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	left.x *= right.x;
	left.y *= right.y;
	return left;
}

template <typename T>
sf::Vector2<T> operator/ (const sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	CHECK(right.x != 0.f && right.y != 0.f)
	if (right.x == 0.f || right.y == 0.f)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide by zero! Dividing sf::Vector (%s,%s) by (%s,%s)"), Float(left.x), Float(left.y), Float(right.x), Float(right.y));
		return left;
	}

	return sf::Vector2<T>(left.x / right.x, left.y / right.y);
}

template <typename T>
sf::Vector2<T>& operator/= (sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	CHECK(right.x != 0.f && right.y != 0.f)
	if (right.x == 0.f || right.y == 0.f)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide by zero! Dividing sf::Vector (%s,%s) by (%s,%s)"), Float(left.x), Float(left.y), Float(right.x), Float(right.y));
		return left;
	}

	left.x /= right.x;
	left.y /= right.y;
	return left;
}
#pragma endregion
SD_END