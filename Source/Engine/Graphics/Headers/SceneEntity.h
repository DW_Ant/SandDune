/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneEntity.h
  An empty Entity that also derives from a SceneTransform.
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "SceneTransform.h"

SD_BEGIN
class GRAPHICS_API SceneEntity : public Entity, public SceneTransform
{
	DECLARE_CLASS(SceneEntity)
};
SD_END