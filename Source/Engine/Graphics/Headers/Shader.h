/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Shader.h
  A wrapper to the SFML's shader object where this will contain a handle to a shader resource
  that will be implementing either a vertex or fragment shader.

  NOTE:  Shaders do not use ResourcePools like textures and fonts since recycled shaders may have influences
  on other shared shaders.  For example, a shader with a texture uniform may be set based on the sprite component it's associated with, but
  associating a specific texture to a shader should not affect other shaders that's running the same shader script.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#define FRAGMENT_SHADER_EXTENSION TXT("frag")
#define VERTEX_SHADER_EXTENSION TXT("vert")

//Sand-Dune specific shader uniform names
#define SHADER_BASE_TEXTURE TXT("SDBaseTexture")
#define SHADER_GLOBAL_TIME TXT("SDGlobalTime")
#define SHADER_DELTA_TIME TXT("SDDeltaTime")

SD_BEGIN
class GRAPHICS_API Shader : public Object
{
	DECLARE_CLASS(Shader)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	sf::Shader* SFShader;

protected:
	/* Name of the file handle that's implementing the shader. */
	DString ShaderName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Opens the specified file, and attempts to create and initialize a shader with that file.
	 * The path to the file is relative to the folder matching the SHADER_LOCATION macro value.
	 * Returns a pointer of the created shader.  Returns null if it's unsuccessful or the file is not found.
	 */
	static Shader* CreateShader (const FileAttributes& fileName, sf::Shader::Type shaderType);
	static Shader* CreateShader (const FileAttributes& vertexFileName, const FileAttributes& fragmentFileName);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void SetSFShader (sf::Shader* newSFShader);
};
SD_END