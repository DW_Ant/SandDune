/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorRenderComponent.h
  A render component that's responsible for drawing solid colors.
=====================================================================
*/

#pragma once

#include "Color.h"
#include "RenderComponent.h"

SD_BEGIN
class GRAPHICS_API ColorRenderComponent : public RenderComponent
{
	DECLARE_CLASS(ColorRenderComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum ShapeType
	{
		ST_Rectangle,
		ST_Circle
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Color SolidColor;

protected:
	ShapeType Shape;

	/* Used to determine the base size of this Entity (in absolute units).
	This behaves differently rather than simply setting the transform's scale.
	This unit will not affect any transforms relative to this component. Contrast to transform's scale,
	Entities relative to a transform with scale other than one also has their translation affected based on its parent's scale.
	For circle shapes, only the X-axis is considered for the diameter. */
	Vector2 BaseSize;

	/* Determines the draw origin of the shape where (0,0) draws the shape from the top left corner, and (1,1) draws the shape from bottom right corner. */
	Vector2 Pivot;

private:
	/* The original intended BaseSize.Y (since BaseSize.Y can be lost for circle shapes). */
	Float OriginalBaseSizeY;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetShape (ShapeType newShape);
	void SetBaseSize (const Vector2& newBaseSize);
	void SetBaseSize (Float newBaseSizeX, Float newBaseSizeY);
	void SetPivot (const Vector2& newPivot);
	void SetPivot (Float pivotX, Float pivotY);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ShapeType GetShape () const
	{
		return Shape;
	}

	inline Vector2 GetBaseSize () const
	{
		return BaseSize;
	}

	inline const Vector2& ReadBaseSize () const
	{
		return BaseSize;
	}

	inline Vector2 GetPivot () const
	{
		return Pivot;
	}

	inline const Vector2& ReadPivot () const
	{
		return Pivot;
	}

	inline Vector2& EditPivot ()
	{
		return Pivot;
	}
};
SD_END