/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Graphics.h
  Contains important file includes and definitions for the Graphics module.

  The Graphics module is responsible for handling the graphics pipeline,
  and define the resources needed for rendering such as Cameras, RenderComponents,
  Draw Layers, and Render Targets.

  The Graphics module also defines the fundamental concept of the Scene where
  Entities may implement a transform interface to help define where they are
  in relation to other Entities.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"

#include <SFML/Graphics.hpp>

#define TICK_GROUP_RENDER "Render" //TickGroup that'll be iterating through render components
#define TICK_GROUP_PRIORITY_RENDER 500

#define TICK_GROUP_TRANSFORM_UPDATE "Transform" //TickGroup that'll iterate through transformations to update their absolute attributes
#define TICK_GROUP_PRIORITY_TRANSFORM_UPDATE 510 //Update transformations just before rendering them.

#ifdef PLATFORM_WINDOWS
	#ifdef GRAPHICS_EXPORT
		#define GRAPHICS_API __declspec(dllexport)
	#else
		#define GRAPHICS_API __declspec(dllimport)
	#endif
#else
	#define GRAPHICS_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsGraphics.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacGraphics.h"
#endif

SD_BEGIN
extern LogCategory GRAPHICS_API GraphicsLog;
SD_END