/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveRenderComponent.h
  A render component that generates triangles of a solid color to follow
  the chain of 2D points specified by the user.
=====================================================================
*/

#pragma once

#include "RenderComponent.h"

SD_BEGIN
class Texture;

class GRAPHICS_API CurveRenderComponent : public RenderComponent
{
	DECLARE_CLASS(CurveRenderComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	sf::Color CurveColor;

	/* Texture to use for the line. If null, then it'll render the line as a solid color. The texture is stretched across the entire curve. */
	DPointer<const Texture> CurveTexture;

	sf::RenderStates RenderStates;

	/* List of points that make up the curve. Each point is relative to this render component's pivot point. */
	std::vector<Vector2> Points;

	/* Determines the distance between the vertices when crossing over the point. The distance between the point and the edge of the curve is equal to half of this value. */
	Float CurveThickness;

	/* List of all vertices that make up each triangle. These are generated based on the Points. */
	sf::VertexArray Vertices;

private:
	/* The most recent Aabb calculated from GetAabb. If empty, it'll iterate through the vertices to recompute its Aabb. */
	mutable Aabb CachedBoundingBox;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Generates vertices that follow a bezier curve.
	 * @param segmentLength Determines the length for each segment. This determines how smooth the curve will be (in pixels). A smoother line requires more vertices.
	 * @param step When evaluating points on the curve, it'll step through the curve at this interval. If the point is greater or equal to the segment length, it'll place a vertex there.
	 * The steps iterates through the curve ranging from 0-1. The smaller the value, the more consistent/accurate the segment lengths are, but the more cycles required to generate the curve.
	 * In general, you'll want to set steps to a small value for curves with tight corners. The default value 0.01 implies it'll run through 100 iterations.
	 */
	virtual void GenerateBezierCurve (const Vector2& endPt1, const Vector2& endPt2, const Vector2& ctrlPt1, const Vector2& ctrlPt2, Float segmentLength, Float inCurveThickness, Float step = 0.01f);

	/**
	 * Generates this component's vertex list based on the given points.
	 */
	virtual void GenerateCurve (const std::vector<Vector2>& inPoints, Float inCurveThickness);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCurveColor (sf::Color newCurveColor);
	virtual void SetCurveTexture (const Texture* newCurveTexture);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline sf::Color GetCurveColor () const
	{
		return CurveColor;
	}

	inline const Texture* GetCurveTexture () const
	{
		return CurveTexture.Get();
	}

	inline std::vector<Vector2> GetPoints () const
	{
		return Points;
	}

	inline const std::vector<Vector2>& ReadPoints () const
	{
		return Points;
	}

	inline Float GetCurveThickness () const
	{
		return CurveThickness;
	}

	inline sf::VertexArray GetVertices () const
	{
		return Vertices;
	}

	inline const sf::VertexArray& ReadVertices () const
	{
		return Vertices;
	}

	inline sf::VertexArray& EditVertices ()
	{
		return Vertices;
	}
};
SD_END