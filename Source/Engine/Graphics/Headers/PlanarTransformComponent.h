/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransformComponent.h
  An empty EntityComponent that also derives from a PlanarTransform.
  This is used to give non Planar components their own transforms.

  If attached to other Entities with PlanarTransforms, this component will automatically
  be relative to the owner.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#include "PlanarTransform.h"

SD_BEGIN
class GRAPHICS_API PlanarTransformComponent : public EntityComponent, public PlanarTransform
{
	DECLARE_CLASS(PlanarTransformComponent)


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
};
SD_END