/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsUnitTester.h
  Tests various aspects of the Graphics Module.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef DEBUG_MODE
SD_BEGIN
class GRAPHICS_API GraphicsUnitTester : public UnitTester
{
	DECLARE_CLASS(GraphicsUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestColor (EUnitTestFlags testFlags) const;
	virtual bool TestSfVector (EUnitTestFlags testFlags) const;
	virtual bool TestAabb (UnitTester::EUnitTestFlags testFlags) const;
	virtual bool TestPlanarTransforms (UnitTester::EUnitTestFlags testFlags) const;
	virtual bool TestSceneTransforms (UnitTester::EUnitTestFlags testFlags) const;
	virtual bool TestSceneTransformUpdater (UnitTester::EUnitTestFlags testFlags) const;
	virtual bool TestSolidColor (EUnitTestFlags testFlags) const;
	virtual bool TestRenderTexture (EUnitTestFlags testFlags) const;
	virtual bool TestTopDownCamera (EUnitTestFlags testFlags) const;
};
SD_END

#endif