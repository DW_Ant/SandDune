/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsEngineComponent.h
  EngineComponent responsible for launching the application's window, and
  running the render pipeline.

  This is essentially the GraphicsCore.
=====================================================================
*/

#pragma once

#include "Color.h"
#include "Graphics.h"
#include "RenderTarget.h"

SD_BEGIN
class DrawLayer;
class PlanarDrawLayer;
class PlanarTransformUpdater;
class SceneEntity;
class SceneDrawLayer;
class SceneTransformUpdater;

class GRAPHICS_API GraphicsEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(GraphicsEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Default DPI setting to fallback to if the system does not have it yet defined (in dots per inch). */
	static Int DEFAULT_DPI_SCALE;

	/* Latest DPI setting obtained from the primary window handle (in dots per inch). */
	static Int DpiScale;

	/* The window handle that'll typically render the main scene. */
	DPointer<Window> PrimaryWindow;

	/* Entity responsible for updating dirty PlanarTransforms. */
	PlanarTransformUpdater* PlanarUpdater;

	/* Entity responsible for updating SceneTransformations efficiently. */
	SceneTransformUpdater* SceneUpdater;

	/* Primitive DrawLayer used to render 'loose' Entities such as MousePointer. */
	DPointer<PlanarDrawLayer> CanvasDrawLayer;

	/* List of SceneDrawLayers all render components can subscribe to through their RegisterToDefaultSceneLayers functions.
	It's not required to register SceneDrawLayers to this list. It's merely to serve as a convenience to quickly add render components
	to the application's common scene draw layers. */
	std::vector<SceneDrawLayer*> DefaultSceneLayers;

	unsigned int RenderComponentHashNumber;

private:
	/* If true, then this component will instantiate and initialize PrimaryWindow and CanvasDrawLayer during initialization. This flag must be set before the engine launches its components. */
	bool bInitializeDefaultViewport;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GraphicsEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RegisterObjectHash () override;
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

	/**
	 * Obtains the current system's DPI settings (in dots per inch).
	 */
	static Int GetDpiScale ();

	/**
	 * Converts the given millimeters to pixels on the display in accordance to the system's DPI scale.
	 */
	static Float ConvertMillimetersToPixels (Float millimeters);
	static Float ConvertPixelsToMillimeters (Float pixels);

	/**
	 * Quick utility to create an Entity responsible for drawing a line between the two end points.
	 * The RenderComponent will automatically register to the default scene draw layers.
	 * If the lifespan is not positive, then this Entity will not automatically expire, and the caller is responsible for managing the Entity.
	 * If the lifespan param is positive, then this Entity will automatically be destroyed in that many seconds.
	 * If the drawLayer parameter is nullptr, then it'll register the line to all default scene layers. Otherwise, it'll only render the line on that specific draw layer.
	 * Returns the owning Entity that owns the render component.
	 */
	static SceneEntity* DrawLine (const Vector3& endPointA, const Vector3& endPointB, Float lineThickness, Color lineColor, Float lifeSpan, SceneDrawLayer* drawLayer = nullptr);

	/**
	 * Quick utility to create an Entity responsible for drawing a circle over the given point.
	 * The RenderComponent will automatically register to the default scene draw layers.
	 * If the lifespan is not positive, then this Entity will not automatically expire, and the caller is responsible for managing the Entity.
	 * If the lifespan param is positive, then this Entity will automatically be destroyed in that many seconds.
	 * If the drawLayer parameter is nullptr, then it'll register the line to all default scene layers. Otherwise, it'll only render the line on that specific draw layer.
	 * Returns the owning Entity that owns the render component.
	 */
	static SceneEntity* DrawCircle (const Vector3& center, Float radius, Color lineColor, Float lifeSpan, SceneDrawLayer* drawLayer = nullptr);

	virtual void AddDefaultSceneDrawLayer (SceneDrawLayer* newLayer);
	virtual void RemoveDefaultSceneDrawLayer (SceneDrawLayer* targetLayer);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetInitializeDefaultViewport (bool bNewInitializeDefaultViewport);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Window* GetPrimaryWindow () const
	{
		return PrimaryWindow.Get();
	}

	inline PlanarTransformUpdater* GetPlanarUpdater () const
	{
		return PlanarUpdater;
	}

	inline SceneTransformUpdater* GetSceneUpdater () const
	{
		return SceneUpdater;
	}

	inline PlanarDrawLayer* GetCanvasDrawLayer () const
	{
		return CanvasDrawLayer.Get();
	}

	inline const std::vector<SceneDrawLayer*>& ReadDefaultSceneLayers () const
	{
		return DefaultSceneLayers;
	}

	inline unsigned int GetRenderComponentHashNumber () const
	{
		return RenderComponentHashNumber;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports various essential textures.
	 */
	virtual void ImportEngineTextures ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMainWindowEvent (const sf::Event& newEvent);
};
SD_END