/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Camera.h
  The Camera contains defines the angle to take perspective from when identifying
  where objects should draw themselves to render targets.  This Entity also contains
  lense settings that may influence how objects appear.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API Camera : public Entity
{
	DECLARE_CLASS(Camera)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum EViewMode
	{
		VM_Normal //Normal view:  textured components, supports shaders, lighting, etc...
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The "lense" of the camera that may influence how objects appear.*/
	EViewMode ViewMode;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Calculates the starting location and a direction from this camera.
	 * This function converts a screen coordinate to scene dimensions.
	 * This direction and location essentially informs where that pixelPos is found in the camera coordinate space.
	 *
	 * @param viewportSize The total width and height of the viewport in screen space (num pixels).
	 * @param pixelPos The point of interest on the viewport.
	 * @param outDirection The rotation the ray is facing.
	 * @param outLocation The location in scene space that determines the starting location of the ray.
	 */
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const;
};
SD_END