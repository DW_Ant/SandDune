/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarCamera.h
  A specific camera that simply determines the draw offset on a 2D plane.  This camera is
  stuck on a plane, and it can only move in two directions.
=====================================================================
*/

#pragma once

#include "Camera.h"
#include "PlanarTransform.h"

SD_BEGIN
class GRAPHICS_API PlanarCamera : public Camera, public PlanarTransform
{
	DECLARE_CLASS(PlanarCamera)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate that's broadcasted whenever the Zoom property changes. */
	MulticastDelegate<Float> OnZoomChanged;

protected:
	/* Determines the scalar multiplier for all objects within view.  2.0 implies that objects are rendered twice as large. */
	Float Zoom;

	/* Determines the min and max range this camera can zoom. */
	Range<Float> ZoomRange;

	/* Draw extents of this camera.  Note this doesn't always correspond 1-to-1 to the size of the viewport that's using this
	camera for drawing.  This is primarily used to help position the camera if a system has control over viewport size, and
	the camera.  For example:  ScrollbarComponents align the camera so that it's top left extents meets the top left corner of sprite.
	ViewExtents are the width and height rectangular region.  Left edge is determined by camera's position minus half of extents.X.
	If no extents are specified, then drawn rendered components are placed relative to this camera's center position.
	Zoom does not affect the ViewExtents. */
	Vector2 ViewExtents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the view extents with the zoom applied.
	 */
	inline Vector2 GetZoomedExtents () const
	{
		return ViewExtents / Zoom;
	}


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetZoom (Float newZoom);
	virtual void SetZoomRange (const Range<Float>& newZoomRange);
	virtual void SetViewExtents (const Vector2& newViewExtents);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetZoom () const
	{
		return Zoom;
	}

	inline Range<Float> GetZoomRange () const
	{
		return ZoomRange;
	}

	inline const Range<Float>& ReadZoomRange () const
	{
		return ZoomRange;
	}

	inline Vector2 GetViewExtents () const
	{
		return ViewExtents;
	}

	inline const Vector2& ReadViewExtents () const
	{
		return ViewExtents;
	}
};
SD_END