/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformUpdaterTester.h
  An Entity specifically designed to test the SceneTransformUpdater.
  This will abort the tests on first failure.
  This Entity will automatically terminate upon finishing its last test.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef DEBUG_MODE

SD_BEGIN
class SceneTransform;

class GRAPHICS_API SceneTransformUpdaterTester : public Entity
{
	DECLARE_CLASS(SceneTransformUpdaterTester)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct STestInstance
	{
		/* Function to execute when launching this test. The test fails if this returns false. */
		std::function<bool()> StartTest;

		/* Function to execute on the second frame. The test fails if this returns false. */
		std::function<bool()> SecondFrame;

		/* The function to execute on the last (third) frame. This test fails if this returns false. */
		std::function<bool()> EndTest;

		/* Human readable name description for this test, used when printing out logs related to this test. */
		DString TestName;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Lists of tests to execute sequentially. The test completes if all test cases passes. */
	std::vector<STestInstance> Tests;

	UnitTester::EUnitTestFlags TestFlags;

	/* Frame number in the current test instance. 0 = started first frame, 1 = second frame, 2 and up means the last frame of test. */
	Int TestFrameNum;

	/* The index of Tests that is currently being executed in the test. */
	size_t ActiveTestIndex;

	/* List of Scene Entities instantiated from the current test. Clears in each new test run. */
	std::vector<SceneTransform*> TestTransforms;

	/* List of expected absolute transforms that must correspond 1-to-1 in the correct order of TestTransforms.
	If any abs translations are mismatched from the TestTransforms' abs translation, then the test validation will fail. */
	std::vector<Vector3> ExpectedAbsTranslations;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Launches the test sequence, and destroys this entity when failed or finished.
	 */
	virtual bool LaunchTests (UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Shortcut to create a singular SceneTransform with the given translation and update behavior.
	 */
	virtual SceneTransform* CreateTransform (const Vector3& translation, SceneTransform::EUpdateBehavior updateBehavior) const;

	/**
	 * Iterates through all expected translations to see if they align with all transformations' abs translations.
	 * Returns true if everything matches.
	 */
	virtual bool CheckTransformations () const;

	/**
	 * Destroys all objects associated with the current test.
	 */
	virtual void RemoveTestObjects ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END
#endif