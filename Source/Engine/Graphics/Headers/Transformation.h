/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Transformation.h
  A base class that defines utility functions that converts from one coordinate
  space to another to help determine how one Entity is related to another.
=====================================================================
*/

#pragma once

#include "Aabb.h"
#include "Graphics.h"
#include "GraphicsUtils.h"

SD_BEGIN
class Camera;
class RenderTarget;

class GRAPHICS_API Transformation
{


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SScreenProjectionData
	{
		SScreenProjectionData () :
			Position(0.f, 0.f),
			BaseSize(1.f, 1.f),
			Scale(1.f, 1.f),
			Rotation(0.f)
		{

		}

		SScreenProjectionData (const SScreenProjectionData& cpy) :
			Position(cpy.Position),
			BaseSize(cpy.BaseSize),
			Scale(cpy.Scale),
			Rotation(cpy.Rotation)
		{

		}

		/* Resulting SFML coordinates the entity's origin is found on the screen. */
		sf::Vector2f Position;

		/* The object's size in coordinate space. For PlanarTransforms, this is simply the absolute size.
		This could be the base size in pixels or cms. */
		sf::Vector2f BaseSize;

		/* Multipliers that scales the drawn object.  This is not the base size of the entity.  This is simply the scalar multiplier to the BaseSize.
		For example, this is the camera's has a zoom multiplier, or the distance modifier for a 3D perspective camera. */
		sf::Vector2f Scale;

		/* The rotation value to apply to the drawn object.  Generally this is a reflection to the transform's rotation properties, but for
		oriented cameras could influence this value based on the view's orientation. */
		float Rotation;

		inline sf::Vector2f GetFinalSize () const
		{
			return BaseSize * Scale;
		}

		bool operator== (const SScreenProjectionData& other) const
		{
			return (Position == other.Position && BaseSize == other.BaseSize && Scale == other.Scale && Rotation == other.Rotation);
		}

		bool operator!= (const SScreenProjectionData& other) const
		{
			return !(*this == other);
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Broadcasted whenever any of the absolute attributes of this transform changes. */
	MulticastDelegate<> OnTransformChanged;

private:
	/* Becomes true whenever the ProjectionData needs to be recomputed next time the ProjectionData is accessed. */
	mutable bool ProjectionDataDirty;
	mutable Transformation::SScreenProjectionData LatestProjectionData;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Transformation ();
	Transformation (const Transformation& cpy);
	virtual ~Transformation ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if any of this Entities' surface is visible from the given camera perspective.
	 * @param renderTarget the resource that defines the stage entities plan to test against.
	 * @param camera the angle the perspective is taking place from.
	 */
	virtual bool IsWithinView (const RenderTarget* renderTarget, const Camera* camera) const = 0;

	/**
	 * Calculates the position, rotation, and scale in screen space.  The screen space coordinates
	 * are computed based on this transformation relative to the camera's transform.
	 *
	 * The resulting transform is in SFML coordinate space where orgin is top left corner of screen,
	 * and rotation is in degrees clockwise.
	 *
	 * Subclass transforms may cast the camera to specific sub classes since it only makes sense to use
	 * cameras with matching transform coordinate spaces.  For example, a PlanarCamera doesn't exist in a SceneTransform coordinate space.
	 * If camera is null, then the transform may assume that the entity coordinates are already in screen space coordinates.
	 *
	 * @param target The render target these coordinates are for.  This is typically used to compute the amount of screenspace available.
	 * @param cam The entity that determines the view's perspective.  The resulting transform is relative to this camera's position and orientation.
	 * @param outProjectionData The resulting data struct that contains the transformation of the projected rendered object.
	 */
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const = 0;

	/**
	 * Uses the absolute transformation to manipulate the given bounding box.
	 */
	virtual void TransformAabb (Aabb& outAabb) const = 0;

	/**
	 * Called whenever the ScreenProjection data needs to be recomputed whenever it's accessed.
	 */
	virtual void MarkProjectionDataDirty () const;


	/*
	=====================
	  Accessors
	=====================
	*/

	/**
	 * Obtains the latest screen projection data. If it's dirty, it'll recompute the screen projection before returning the results.
	 */
	virtual const SScreenProjectionData& GetProjectionData (const RenderTarget* renderTarget, const Camera* camera) const;

	/**
	 * Retrieves the latest screen projection regardless if it was marked for dirty or not.
	 * Use GetProjectionData to recompute projection data if it's stale.
	 */
	inline const SScreenProjectionData& ReadProjectionData () const
	{
		return LatestProjectionData;
	}

	bool inline IsProjectionDataDirty () const
	{
		return ProjectionDataDirty;
	}
};
SD_END