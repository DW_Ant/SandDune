/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EmptyRenderTarget.h
  A RenderTarget that doesn't do anything with draw calls.  This class is primarily
  used for testing.
=====================================================================
*/

#pragma once

#include "RenderTarget.h"

#ifdef DEBUG_MODE

SD_BEGIN
class GRAPHICS_API EmptyRenderTarget : public RenderTarget
{
	DECLARE_CLASS(EmptyRenderTarget)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Vector2 Size;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void Reset () override;
	virtual void GetSize (Int& outWidth, Int& outHeight) const override;
	virtual Vector2 GetSize () const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSize (const Vector2& newSize);
};
SD_END

#endif