/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Window.h
  An object representing a Window.  Contains various delegates other
  objects could register to for event handling.
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API Window : public RenderTarget
{
	DECLARE_CLASS(Window)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Color to use when this object needs to reset. */
	Color DefaultColor;

	/* If true, then this Window event will invoke the close event on destruction. */
	bool InvokeCloseOnDestruction;

protected:
	/* Mutex used to protect which window resource is active or not. This is primarily to notify OpenGL
	which context it should use. Only one window can be active within a process. */
	static std::mutex WindowResourceMutex;

	/* Actual handle that's broadcasting the events, and interfaces with the OS. */
	sf::RenderWindow* Resource;

	/* Tick component that allows this object to poll window events. */
	DPointer<TickComponent> PollingTickComponent;

	/* List of event handlers that may be interested to any of this window handle's events. */
	MulticastDelegate<const sf::Event&> PollingDelegates;

	/* List of callbacks that may prevent the window handle from broadcasting the close event.
	If any callback return false, then the close event is ignored.
	It is not safe to write to this vector in the middle of handling this event. */
	std::vector<SDFunction<bool>> OnCanClose;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Reset () override;
	virtual void GenerateScene () override;
	virtual void Draw (RenderComponent* comp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState = sf::RenderStates::Default) override;
	virtual void Display () override;
	virtual void GetSize (Int& outWidth, Int& outHeight) const override;
	virtual Vector2 GetSize () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the owning objects event handler to receive notifications from window events.
	 * The external object is responsible for calling UnregisterPollingDelegate to avoid dangling pointers.
	 */
	virtual void RegisterPollingDelegate (SDFunction<void, const sf::Event&> newCallback);
	virtual void UnregisterPollingDelegate (SDFunction<void, const sf::Event&> targetCallback);

	virtual void RegisterCanCloseDelegate (SDFunction<bool> newCallback);
	virtual void UnregisterCanCloseDelegate (SDFunction<bool> targetCallback);

	/**
	 * Notifies this object to check all pending window events to broadcast delegates.
	 * The Window does not automatically poll its own events since this object could be external from main loop.
	 */
	virtual void PollWindowEvents ();

	/**
	 * Retrieves the OS's coordinates of the current window.  If bIncludeTitlebar is false, then it'll retrieve
	 * the coordinates within the view.
	 */
	virtual void GetWindowPosition (Int& outPosX, Int& outPosY, bool bIncludeTitlebar = false) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSize (const Vector2& newSize);
	virtual void SetResource (sf::RenderWindow* newResource);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual sf::RenderWindow* GetResource () const;
	virtual void GetWindowSize (Int& outWidth, Int& outHeight) const;
	virtual Vector2 GetWindowSize () const;
	virtual TickComponent* GetPollingTickComponent () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePollingTick (Float deltaSec);
};
SD_END