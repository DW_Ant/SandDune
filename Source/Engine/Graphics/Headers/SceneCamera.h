/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneCamera.h
  A Camera that's able to project an Entity with 3D coordinates into 2D screenspace.

  This camera also implements the SceneTransform interface so that relative positions
  to drawn Entities can be computed since this camera shares the same coordinate system.
=====================================================================
*/

#pragma once

#include "Camera.h"
#include "SceneTransform.h"

SD_BEGIN
class GRAPHICS_API SceneCamera : public Camera, public SceneTransform
{
	DECLARE_CLASS(SceneCamera)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * The opposite of CalculatePixelProjection, this function projects a 3D transform into 2D screen coordinates.
	 *
	 * @param viewportSize The number of pixels available to draw in the viewport. Used to determine the view frustum limits.
	 * @param transform The transform the camera is using to view. It'll use this transform relative to this camera's transform to compute the screen projection.
	 * @param outProjectionData The resulting transformation of objects projected to the screen from this camera's perspective.
	 */
	virtual void ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const = 0;

	/**
	 * Determines the distance the given location is relative to the closest screen point.
	 * For perspective cameras, this is simply the distance from the camera to the point.
	 * For orthographic cameras, this is the distance from the given location to the closest point on the near plane.
	 */
	virtual Float GetDistSquaredToPoint (const Vector3& worldLocation) const = 0;

	inline Float GetDistToPoint (const Vector3& worldLocation) const
	{
		return std::sqrtf(GetDistSquaredToPoint(worldLocation).Value);
	}
};
SD_END