/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransform.h
  A Transform that defines a 2D Entities' position relative to other 2D Entities.

  This transform assumes the coordinate space relative to some arbitrary origin (in pixels).
  When projected to the screen without a camera, the origin automatically snaps to the top
  left corner of the RenderTarget.

  Entities can determine their position and size relative to the other Entities.
  For example, the size and positional values between 0-1 is a fraction of their Owner's
  positional and size attributes.

  Unit values (in pixels) for coordinates and size can be specified when
  values greater than 1 is given.


  +-----------+
  | ANCHORING |
  +-----------+
  Planar Transforms have the ability to anchor themselves to their parent transform (the
  transform this transform is relative to).  The dev can specify borders (top, right, bottom,
  and left) or any combination.  When anchored, this transform's relative position and size
  will automatically be adjusted whenever the parent transform changes size.  The transform's
  border distance to the parent transform's border will always be the same until SetAnchor
  is reapplied.

  When anchored, this transform's Position and Size may automatically be converted from
  relative coordinates (when 0-1 is specified) to coordinates in pixels (greater than 1).
  This is because the relative coordinates are irrelevant as soon as the parent transform
  changes size.  These values are computed on the every render cycle since this may be dependent
  on the size of the RenderTarget.

  Anchoring may provide inconsistent behaviors to transforms that are registered to multiple
  different-sized RenderTargets and if their root transforms are in relative size.  It's
  recommended to only apply anchoring to planar transforms for Entities that are only rendered
  in one RenderTarget (such as UI).

  When realigning transforms to fit the anchor constraints, the system may to overwrite the
  Position values only.  If setting position isn't sufficient enough (when both opposite borders
  are anchored), then it'll overwrite the transform's size, too.

  Anchoring only works for axis-aligned transforms (no rotations).

  Due to accessing the parent size, the Anchoring system relies on the parent transforms to
  compute their absolute size before this transform anchors to parent.  Otherwise, the anchoring
  may take a few frames to apply.
=====================================================================
*/

#pragma once

#include "Transformation.h"
#include "Camera.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API PlanarTransform : public Transformation
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Specifies the translation of this transform (units are in pixels).  Values between 0-1
	are interpretted as fractions relative to the Owner or RenderTarget where 0 is left/top aligned,
	0.5 is centered, and 1 is right/bottom aligned.  Values greater than 1 are in absolute units
	(in pixels) relative to the Owners (if any).
	If anchored then the Position values are automatically updated and converted to units in pixels.
	This is because the parent transforms (relativeTo) could change size at any time, and
	this transform will need to realign itself to abide to the anchor's restrictions.
	When realigning, the relative location (when less than 1 is specified) is no longer relevant. */
	Vector2 Position;

	/* Depth value that determines this object's 'distance' from all cameras.  Objects with lesser depth values
	are rendered in front of other objects with greater depth (distance) values. */
	Float Depth;

	/* Scalar data where relative to the Owner/RenderTarget where 0.1 is 10% the size of Owner, and 1.0
	is the Owner's full size.  Anything greater than 1 is absolute size in cm regardless of Owner scalar values.
	If anchored then the Size values may be automatically updated and converted to units in pixels.
	This is because the parent transforms (relativeTo) could change size at any time, and this transform
	will need to realign itself to abide to the anchor's restrictions.  When realigning, the relative size
	(when less than 1 is specified) is no longer relevant. */
	Vector2 Size;

	/* If true, then this object may scale position and size if the coordinates specified are between 0 and 1. */
	bool EnableFractionScaling;

private:
	/* Transform this object is relative to.  If none is specified, then this transform is relative to the RenderTarget. */
	PlanarTransform* RelativeTo;

	/* List of all Transforms relative to this object. */
	std::vector<PlanarTransform*> AttachedTransforms;

	/* If anchored, these values store the distances between this transform's border and the parent transform's border.
	This transform is not anchored if it's negative. */
	Float AnchorLeftDist;
	Float AnchorTopDist;
	Float AnchorRightDist;
	Float AnchorBottomDist;

	/* Becomes true when one of the attributes changed and it needs to recompute its absolute transform next update.
	This flag propagates to all transforms relative to this object. */
	bool bTransformDirty;

	/* Computed at each Render call, this is the latest absolute position and size (in pixels).
	It's the accumulated transform data with relative owners considered.
	By default, these values are updated every frame, but should the frame delay cause issues, call the
	ComputeAbsTransform method to refresh these values.

	If this object depends on the size of the RenderTarget, then these values are still in pixels,
	but they are computed based on the latest RenderTarget they're drawn to. If there isn't a RenderTarget,
	then they assume the RenderTarget size uses normalized coordinates (between -1 to 1). */
	Vector2 CachedPosition;
	Vector2 CachedSize;

	/* Size of the parent bounds.  This is either the CachedSize of RelativeTo or
	the size of the RenderTarget.  This is primarily used for anchor calculations. */
	Vector2 ParentAbsSize;

	/* The RenderTarget this Entity was most recently rendered in.
	This is used for Planar objects that have their scale and position dependent on the render target's size.
	Should ComputeAbsTransform is called, it refers to the most recently drawn RenderTarget.

	Entities that are rendered in multiple RenderTargets should absolutely NOT have position and scalar values
	relative to an Entity, who's rendered in multiple RenderTargets. That just doesn't make sense.
	This property is updated every time this transform is computing its screen coordinates.
	This pointer is only valid on root transforms. */
	mutable DPointer<const RenderTarget> RootRenderTarget;

	/* Camera used for rendering this object on the RootRenderTarget. Only applicable on the root transform. */
	mutable DPointer<const Camera> ExternalCamera;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PlanarTransform ();
	PlanarTransform (const Vector2& inPosition, const Vector2& inSize, Float inDepth = 0.f, PlanarTransform* inRelativeTo = nullptr);
	PlanarTransform (const PlanarTransform& copyObj);
	virtual ~PlanarTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsWithinView (const RenderTarget* renderTarget, const Camera* camera) const override;
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;
	virtual void TransformAabb (Aabb& outAabb) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Recursively steps through this transform and everything attached to it to compute its absolute position and size.
	 * Doing so also clears the dirty flag. It's recommended to call this on the root transform.
	 */
	virtual void ComputeAbsTransform ();

	/**
	 * Invoked whenever this transform's absolute size or position has changed since last frame.
	 * Typically this is called automatically from the PlanarTransformUpdater after everything else finished computing their abs transform.
	 */
	virtual void PostAbsTransformUpdate ();

	/**
	 * Returns true if the given point is within this Entity's bounds.
	 * @param point Position relative to the top left corner of the RenderTarget.
	 */
	virtual bool IsWithinBounds (const Vector2& point) const;

	/**
	 * Recomputes Position and Size to consider anchor restrictions.  If anchored, the two vectors
	 * will automatically be converted from scalar to units in pixels.  This function assumes
	 * the absolute transform was just computed.  This function may also update the CachedAbs Vectors
	 * if the anchors influenced this transform's dimensions.
	 */
	virtual void ApplyAnchors ();

	/**
	 * Copies the planar transform properties from the given transform.
	 */
	virtual void CopyPlanarTransform (const PlanarTransform* copyFrom);

	/**
	 * Obtains the RenderTarget from the root transform.
	 */
	const RenderTarget* FindRootRenderTarget () const;
	const Camera* FindExternalCamera () const;


	/*
	=====================
	  Mutators
	=====================
	*/

	void SetPosition (const Vector2& newPosition);
	void SetPosition (Float x, Float y);
	void SetDepth (Float newDepth);
	void SetSize (const Vector2& newSize);
	void SetSize (Float width, Float height);
	void SetEnableFractionScaling (bool newEnableFractionScaling);
	void SetRelativeTo (PlanarTransform* newRelativeTo);

	/**
	 * Specifies the distance this transform's border(s) should be from its parent transform's borders.
	 * Anchoring should NOT be used for transforms that expects to be positioned/scaled relative to their parent transforms.
	 * Anchoring should NOT be used for transforms that are used in multiple different-sized RenderTargets.
	 * Anchoring should NOT be used for transforms with rotations.
	 * See class description for details on these restrictions.
	 *
	 * @param borderDist The number of pixels this border should be from the parent transform's border.
	 * Set to negative values to disable anchoring.
	 */
	void SetAnchorLeft (Float leftDist);
	void SetAnchorTop (Float topDist);
	void SetAnchorRight (Float rightDist);
	void SetAnchorBottom (Float bottomDist);

	virtual void SetRootRenderTarget (const RenderTarget* newRootRenderTarget);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector2 GetPosition () const
	{
		return Position;
	}

	inline const Vector2& ReadPosition () const
	{
		return Position;
	}

	inline Vector2& EditPosition ()
	{
		MarkTransformDirty();
		return Position;
	}

	inline Float GetDepth () const
	{
		return Depth;
	}

	inline Vector2 GetSize () const
	{
		return Size;
	}

	inline const Vector2& ReadSize () const
	{
		return Size;
	}

	inline Vector2& EditSize ()
	{
		MarkTransformDirty();
		return Size;
	}

	inline bool GetEnableFractionScaling () const
	{
		return EnableFractionScaling;
	}

	inline PlanarTransform* GetRelativeTo () const
	{
		return RelativeTo;
	}

	inline Vector2 GetCachedAbsPosition () const
	{
		return CachedPosition;
	}

	inline const Vector2& ReadCachedAbsPosition () const
	{
		return CachedPosition;
	}

	inline Vector2 GetCachedAbsSize () const
	{
		return CachedSize;
	}

	inline const Vector2& ReadCachedAbsSize () const
	{
		return CachedSize;
	}

	inline const Vector2& ReadParentAbsSize () const
	{
		return ParentAbsSize;
	}

	inline Float GetAnchorLeftDist  () const
	{
		return AnchorLeftDist;
	}

	inline Float GetAnchorTopDist   () const
	{
		return AnchorTopDist;
	}

	inline Float GetAnchorRightDist () const
	{
		return AnchorRightDist;
	}

	inline Float GetAnchorBottomDist() const
	{
		return AnchorBottomDist;
	}

	inline bool IsAnchoredLeft () const
	{
		return (AnchorLeftDist >= 0.f);
	}

	inline bool IsAnchoredTop () const
	{
		return (AnchorTopDist >= 0.f);
	}

	inline bool IsAnchoredRight () const
	{
		return (AnchorRightDist >= 0.f);
	}

	inline bool IsAnchoredBottom () const
	{
		return (AnchorBottomDist >= 0.f);
	}

	inline bool IsTransformDirty () const
	{
		return bTransformDirty;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Marks this transform dirty and it needs to recompute its absolute transformation.
	 * This also registers this function to the local planar transform updater if the parent transform is not dirty.
	 */
	virtual void MarkTransformDirty ();
};
SD_END