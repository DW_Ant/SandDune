/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarDrawLayer.h
  A DrawLayer that is designed to render a 2D scene to the RenderTarget.  This DrawLayer
  corresponds to PlanarTransform objects.

  It's recommended to use specialized subclasses instead of this one for performance and
  maintainability reasons.  Freely registered render components may become difficult to manage
  draw order for large projects.
=====================================================================
*/

#pragma once

#include "DrawLayer.h"

SD_BEGIN
class RenderComponent;
class PlanarCamera;
class PlanarTransform;

class GRAPHICS_API PlanarDrawLayer : public DrawLayer
{
	DECLARE_CLASS(PlanarDrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Draw Layer priority relative to other DrawLayers.  The Canvas Draw Layer uses a simple free RenderComponent registration, where
	RenderComponents that is a component for a planar transformable object can register themselves to this layer.
	It's recommended to keep component registration to a minimum for this layer due to slow performance and maintainability reasons.
	Large projects may be difficult to maintain render depth (draw order). */
	static const Int CANVAS_LAYER_PRIORITY;

protected:
	/* List of Entities to render. This draw layer will iterate through each render component and draw them relative to where they appear in
	the component tree. The registered object must be an Entity that implements a PlanarTransform interface. It must also have RenderComponents, too.
	This list is automatically sorted by that transform's depth value. */
	std::vector<PlanarTransform*> EntitiesToDraw;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void RegisterPlanarObject (PlanarTransform* newObj);
	virtual void UnregisterPlanarObject (PlanarTransform* target);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void RenderComponents (RenderTarget* renderTarget, Camera* cam);
	virtual void PurgeExpiredObjs ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END