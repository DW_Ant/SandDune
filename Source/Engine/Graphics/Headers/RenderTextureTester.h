/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTextureTester.h
  A test class that creates a rendered texture resource, and displays that texture within a frame.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef DEBUG_MODE

SD_BEGIN
class RenderTexture;
class ColorRenderComponent;

class GRAPHICS_API RenderTextureTester : public Entity
{
	DECLARE_CLASS(RenderTextureTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Window that'll be rendering the split screen RenderTextures. */
	Window* SplitScreenWindow;

	/* Draw layer for rendering contents in the split screen window. */
	DPointer<PlanarDrawLayer> WindowDrawLayer;

	/* Determines the size of the frame.  This also happens to determine the size of the render textures. */
	Vector2 FrameSize;

	/* Entity that'll be owning the RenderTexture objects. */
	Entity* RenderTextureOwner;

	/* The texture that'll be displaying the contents within the frame. */
	DPointer<RenderTexture> TopRenderTexture;
	DPointer<RenderTexture> BottomRenderTexture;

	/* Cameras that determines the location of the borders. */
	DPointer<PlanarCamera> TopCamera;
	DPointer<PlanarCamera> BottomCamera;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWindowEvent (const sf::Event& evnt);
};
SD_END
#endif