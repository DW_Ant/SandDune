/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DrawLayer.h
  A DrawLayer defines a collection of Entities that should render in a certain order relative
  to each other.

  A DrawLayer is registered to RenderTargets, and their DrawPriority determines which
  DrawLayer should render over others.
  A common example is the UI Draw Layer render over the Scene Draw Layer.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class RenderTarget;
class Camera;

class GRAPHICS_API DrawLayer : public Object
{
	DECLARE_CLASS(DrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Draw priority of this DrawLayer where higher values will cause this DrawLayer to render over
	lesser DrawLayers. */
	Int DrawPriority;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Kicks off the render process for this DrawLayer.
	 * This function should execute all registered render component's Render function in a defined order.
	 */
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) = 0;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Changes the DrawLayer's priority.  This can only be changed once, and it should be set
	 * before it's registered to any RenderTarget.
	 */
	virtual void SetDrawPriority (Int newDrawPriority);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetDrawPriority () const
	{
		return DrawPriority;
	}
};
SD_END