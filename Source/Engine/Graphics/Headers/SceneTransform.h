/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransform.h
  An object containing a series of properties that defines the object's local scaling, position,
  rotation, and which coordinate system it's relative to.

  Although this is a 2D application, a 3D transformation is still used primarily for perspective
  projections where distant objects are rendered smaller than the close objects.  Also
  this would make it easier to transition to 3D if the need for it comes.

  The coordinate system is a left handed coordinate system where X is forward, R is right, and Z is up.
=====================================================================
*/

#pragma once

#include "Transformation.h"

SD_BEGIN

class GRAPHICS_API SceneTransform : public Transformation
{

	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EUpdateBehavior
	{
		UB_Manual, //This transformation does not register to the SceneTransformUpdater. This means its absolute transformation must be manually invoked for updates.
		UB_Seldom, //This transformation is registered to the SceneTransformUpdater whenever it's dirty. This is used for transforms that change periodically.
		UB_Continuous //This transformation is registered to the SceneTransformUpdater permanently. This is used for transformations that changes very often.
	};

protected:
	enum EBroadcastState
	{
		BS_Ready, //This transformation is ready to broadcast the OnTransformChanged whenever the absolute transform is calculated.
		BS_Locked, //This transformation is not allowed to broadcast the OnTransformChanged delegate. This is typically set because it's being calculated in a separate thread.
		BS_Pending //This transformation is locked (forbidden to broadcast), but the transformation did change while it is locked. The multicast delegate will broadcast as soon as it unlocks.
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Vector3 Translation;
	Vector3 Scale;

	/* Scales the scalar vector in all three axis uniformly. */
	Float GlobalScale;

	/* Rotates this object about its pivot point. Being a left handed coordinate system, all axis rotates clockwise. */
	Rotator Rotation;

	/* Translation, Scale, and Rotation in world coordinates. This should be recomputed every time this transform is rendered, and
	it may become stale quickly. Use ComputeAbsoluteTransform to refresh these variables. */
	mutable TransformMatrix AbsTransform;
	mutable Vector3 AbsTranslation;
	mutable Vector3 AbsScale;
	mutable Rotator AbsRotation;

private:
	/* The SceneTransform this object is relative to.  If this is null, then this transform matrix is in absolute coordinates
	where it's relative to the universe's transform (arbitrary origin and no scaling/rotation data). */
	SceneTransform* RelativeTo;

	/* List of transform components that are relative to this transform. */
	std::vector<SceneTransform*> AttachedTransforms;

	/* Indicates when this transformation's absolute attributes are recomputed. */
	EUpdateBehavior UpdateBehavior;

	/* Becomes true if the absolute attributes are stale, and ComputeAbsoluteTransform needs to be invoked for an update.
	If this is true, then it's safe to assume all transforms relative to this are also stale. */
	bool AbsTransformDirty;

	/* State that determines when the OnTransformChanged delegate may broadcast. */
	EBroadcastState TransformBroadcastState;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SceneTransform ();
	SceneTransform (const Vector3& inTranslation, const Vector3& inScale, Float inGlobalScale, Rotator inRotation, SceneTransform* inRelativeTo, EUpdateBehavior inUpdateBehavior);
	SceneTransform (const SceneTransform& copyData);
	virtual ~SceneTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsWithinView (const RenderTarget* renderTarget, const Camera* camera)  const override;
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;
	virtual void TransformAabb (Aabb& outAabb) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Resets this transform where it's positioned at origin, and
	 * it contains no rotation, and all scaling axis is 1.
	 */
	void ResetTransform ();

	/**
	 * Generates a 4x4 matrix of this Transformation.
	 */
	TransformMatrix ToTransformMatrix () const;

	/**
	 * Apply this scene's transform to the given matrix in the order of Translation, Scaling, then Rotation.
	 */
	void ApplyTransformTo (TransformMatrix& outMatrix) const;

	/**
	 * Recomputes the absolute transform variables. This also ensures everything it's relative to is also up to date.
	 * Does nothing if this transform is not dirty.
	 */
	virtual void CalculateAbsoluteTransform ();

	/**
	 * Utility to iterate through this transform and all sub transforms relative to this.
	 * This function invokes the given lambda on each iterated transform.
	 */
	virtual void ForEachTransformInBranch (const std::function<void(SceneTransform* /*curTransform*/)>& toExecute);

	/**
	 * Marks the absolute attributes as stale, and forces the engine to recompute the absolute transform for this and
	 * all transforms relative to this.
	 *
	 * This cannot be a virtual function since it's called from a constructor.
	 */
	void MarkAbsTransformDirty ();

	/**
	 * Determines if the OnTransformChanged multicast delegate can be broadcasted or not.
	 * If the transform is calculated while this is locked, the multicast delegate will broadcast whenever this is unlocked.
	 */
	virtual void SetTransformChangedBroadcastLock (bool bLocked);


	/*
	=====================
	  Mutators
	=====================
	*/

	void SetTranslation (const Vector3& newTranslation);
	void SetScale (const Vector3& newScale);
	void SetGlobalScale (Float newGlobalScale);
	void SetRotation (const Rotator& newRotation);
	void SetRelativeTo (SceneTransform* newRelativeTo);
	void SetUpdateBehavior (EUpdateBehavior newUpdateBehavior);


	/*
	=====================
	  Accessors
	=====================
	*/

public:

	inline TransformMatrix GetAbsTransform () const
	{
		return AbsTransform;
	}

	inline Vector3 GetAbsTranslation () const
	{
		return AbsTranslation;
	}

	inline Vector3 GetAbsScale () const
	{
		return AbsScale;
	}

	inline Rotator GetAbsRotation () const
	{
		return AbsRotation;
	}

	inline Vector3 GetTranslation () const
	{
		return Translation;
	}

	inline const Vector3& ReadTranslation () const
	{
		return Translation;
	}

	inline Vector3& EditTranslation ()
	{
		MarkAbsTransformDirty();
		return Translation;
	}

	inline Vector3 GetScale () const
	{
		return Scale;
	}

	inline const Vector3& ReadScale () const
	{
		return Scale;
	}

	inline Vector3& EditScale ()
	{
		MarkAbsTransformDirty();
		return Scale;
	}

	inline Float GetGlobalScale () const
	{
		return GlobalScale;
	}

	inline const Rotator& ReadRotation () const
	{
		return Rotation;
	}

	inline Rotator& EditRotation ()
	{
		MarkAbsTransformDirty();
		return Rotation;
	}

	inline const TransformMatrix& ReadAbsTransform () const
	{
		return AbsTransform;
	}

	inline const Vector3& ReadAbsTranslation () const
	{
		return AbsTranslation;
	}

	inline const Vector3& ReadAbsScale () const
	{
		return AbsScale;
	}

	inline Rotator GetRotation () const
	{
		return Rotation;
	}

	inline const Rotator& ReadAbsRotation () const
	{
		return AbsRotation;
	}

	inline SceneTransform* GetRelativeTo () const
	{
		return RelativeTo;
	}

	inline const std::vector<SceneTransform*>& ReadAttachedTransforms () const
	{
		return AttachedTransforms;
	}

	inline EUpdateBehavior GetUpdateBehavior () const
	{
		return UpdateBehavior;
	}

	inline bool IsAbsTransformDirty () const
	{
		return AbsTransformDirty;
	}
};
SD_END