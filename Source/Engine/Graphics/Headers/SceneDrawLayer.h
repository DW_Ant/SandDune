/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneDrawLayer.h
  A DrawLayer used to render any SceneTransform Entity.
=====================================================================
*/

#pragma once

#include "DrawLayer.h"

SD_BEGIN
class RenderComponent;
class SceneCamera;

class GRAPHICS_API SceneDrawLayer : public DrawLayer
{
	DECLARE_CLASS(SceneDrawLayer)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/* Simple data struct used to cache information about a registered component. */
	struct SRegisteredComponent
	{
		RenderComponent* Component;

		/* This component's relative distance to the most recent camera. This is cached in order to avoid
		calling the DistSquared function every time this Entity changes position during the sort function. */
		Float DistSquared;

		SRegisteredComponent (RenderComponent* inComponent) :
			Component(inComponent),
			DistSquared(-1.f)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of RenderComponents to render.  These RenderComponents must be a component for a SceneTransform object. */
	std::vector<SRegisteredComponent> ComponentsToDraw;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers a single render component to the render list. The component will be rejected if it's
	 * not a render component for an Entity with a SceneTransform.
	 * Returns true on success.
	 */
	virtual bool RegisterSingleComponent (RenderComponent* newRenderComp);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Renders all registered components to the specified renderTarget.
	 */
	virtual void RenderComponents (RenderTarget* renderTarget, SceneCamera* cam);

	/**
	 * Iterates through the component list and sorts them based on their position relative to the given camera.
	 * This actually edits the component list to reduce the amount of shuffling next time this is called.
	 */
	virtual void SortRenderComponents (SceneCamera* cam);

	/**
	 * Iterates through the component list and removes anything that are pending destruction.
	 * NOTE: This must be called before the actual pointers are purged since the Components are not DPointers.
	 */
	virtual void PurgeExpiredComponents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END