/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InstancedSpriteComponent.h
  A render component that specializes in rendering numerous sprites of the same texture
  effeciently.

  This improves performance compared to the ordinary SpriteComponent by doing the following:

  * All sprites are grouped as one Entity. The render pipeline only needs to sort them once.
  * Avoid creating unecessary data for numerous sprite components. Only stores transform data
  and sprite rendering for each sprite rather than the alternative where a SpriteComponent must
  be maintained in a Components vector as well as the Engine's object table.
  * Sprite instances are rendered using a VertexArray instead of a series of Sprite objects, which
  means everything can be rendered using one draw call.
=====================================================================
*/

#pragma once

#include "RenderComponent.h"

SD_BEGIN
class Texture;

class GRAPHICS_API InstancedSpriteComponent : public RenderComponent
{
	DECLARE_CLASS(InstancedSpriteComponent)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SSpriteInstance
	{
		SSpriteInstance () :
			Position(0.f, 0.f),
			Size(1.f, 1.f),
			Rotation(Rotator::ZERO_ROTATOR),
			SubDivisionIdxX(0),
			SubDivisionIdxY(0)
		{

		}

		SSpriteInstance(const SSpriteInstance& cpy) :
			Position(cpy.Position),
			Size(cpy.Size),
			Rotation(cpy.Rotation),
			SubDivisionIdxX(cpy.SubDivisionIdxX),
			SubDivisionIdxY(cpy.SubDivisionIdxY)
		{

		}

		/* Sprite's screen position offset relative to this component's screen position. */
		Vector2 Position;

		/* The base size of this sprite without any multipliers from the owning transform or screen projection. */
		Vector2 Size;

		Rotator Rotation;

		/* Determines which index in the subdivision table this sprite should render in. */
		int SubDivisionIdxX;
		int SubDivisionIdxY;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The number of elements in the Vertices array for each SpriteInstance. */
	static const size_t NUM_VERTICES_PER_SPRITE;

protected:
	/* The instanced Texture all sprite instances within this component will be using. */
	DPointer<const Texture> SpriteTexture;

	/* Render state that'll is used how the sprite is drawn to the render target. */
	sf::RenderStates RenderState;

	/* Scale multipliers based on displaying subdivision of a sprite. */
	Int NumSubDivisionsX;
	Int NumSubDivisionsY;

	/* Data used to populate the vertex data. */
	std::vector<SSpriteInstance> SpriteInstances;

	/* Actual sf::Vertex array that is used for rendering. */
	sf::VertexArray Vertices;

private:
	/*This is the smallest bounding box that encompasses all sprites.

	This is cached for performance reasons to prevent recalculating	the bounding box
	every time GetAabb is called. If it's an empty box, then it's considered dirty, where
	it will have to iterate through	the vertices next time GetAabb is invoked. */
	mutable Aabb BaseBoundingBox;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Replaces the instances with a randomly populated list based on the given params.
	 * @param numInstancesPerCell - Determines how many instances this function will create in each cell.
	 * If a densityMap is not provided, this is essentially the total number of instances. Must be a positive number.
	 * @param cellSize - Determines how many texels in the densityMap makes up a single cell. 1 = texel, 2 = 2x2 texels, 3 = 3x3 texels.
	 * Not used if a densityMap is not provided. Must be a positive number.
	 * @params dimensions - Determines the width (X) and height (Y) extents that could potentially determine the sprite instance location relative to this component position.
	 * Both axis must be positive.
	 * @params sizeRange - Determines the size variation for each sprite instance.
	 * @param uniformSize - If true, then the x and y scale is guaranteed to be equal.
	 * @param randomRotation - If true, then a random rotation value is chosen. Otherwise, it'll default to zero rotation.
	 * @params densityMap - Determines the density population using this texture as a map. White texels means fully populated while black yields no instances.
	 * The formula is simply the average brightness of the cell x numInstancesPerCell
	 * If a texture is not provided, then it'll assume even full density within the entire region. Fills the region with numInstancesPerCell.
	 */
	virtual void PopulateInstances (Int numInstancesPerCell, Int cellSize, const Vector2& dimensions, Range<Float> sizeRange, bool uniformSize, bool randomRotation, Texture* densityMap = nullptr);

	/**
	 * Updates the vertices based on the current SpriteInstance vector. This function will also update the vertex array to ensure each sprite
	 * instance has their own corresponding vertices.
	 *
	 * @param spriteInstanceIndices - The indices to the SpriteInstance vector to notify which vertices needs to be updated.
	 * If this vector is empty, then it'll update all vertices.
	 */
	virtual void RegenerateVertices (const std::vector<size_t>& spriteInstanceIndices);
	virtual void RegenerateVertices ();

	/**
	 * Clears the cached version of the Aabb in order for it to recompute the Aabb.
	 */
	virtual void MarkAabbAsDirty ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSpriteTexture (const Texture* newTexture);
	virtual void SetNumSubDivisions (Int newNumSubDivisionsX, Int newNumSubDivisionsY);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const Texture* GetSpriteTexture () const
	{
		return SpriteTexture.Get();
	}

	inline Int GetNumSubDivisionsX () const
	{
		return NumSubDivisionsX;
	}

	inline Int GetNumSubDivisionsY () const
	{
		return NumSubDivisionsY;
	}

	const std::vector<SSpriteInstance>& ReadSpriteInstances () const
	{
		return SpriteInstances;
	}

	/**
	 * After manipulating the SpriteInstance array, be sure to call
	 * RegenerateVertices to apply the changes.
	 */
	std::vector<SSpriteInstance>& EditSpriteInstances ()
	{
		return SpriteInstances;
	}

	/**
	 * Edits the raw vertex data, itself. There's no need to regenerate vertices after manipulating this vector.
	 * When editing vertices beyond the bounding box, be sure to mark the bounding box as dirty.
	 */
	sf::VertexArray& EditVertices ()
	{
		return Vertices;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the corresponding vertices to the given spriteInstanceIdx if it is in range.
	 */
	virtual void UpdateVertexArray (size_t spriteInstanceIdx);

	/**
	 * Refreshes the texture coordinates for the specified sprite index so that it renders the entire subdivision based
	 * on the corresponding sprite's sub divide index and current texture size.
	 *
	 * @param subDivisionLengths - The size of the texture region that is used to determine the sub division sections.
	 * Normally this would be a local variable within UpdateTextureCoordinates, but it's a parameter to prevent calculating
	 * the lengths for each vertex. It should be computed once for all vertices for performance reasons.
	 */
	virtual void UpdateTextureCoordinates (size_t spriteInstanceIdx, const Vector2& subDivisionLengths);
};
SD_END