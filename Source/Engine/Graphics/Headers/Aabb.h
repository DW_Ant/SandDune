/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Aabb.h
  A datatype that's a 3D rectangular Axis Aligned Bounding Box.

  This type is very similar to the SD::Rectangle except that it's in three dimensions.
  Although these two types could converge into one class either making the third dimension optional
  or through templates, it was decided to keep these separate to specialize functions that may depend
  on the circumstance. For example: Area vs SurfaceArea. Not requiring a 2D rectangle to have a depth of 1
  in order to produce an area/volume that's not zero (yet not wanting to mess with the surface area by setting
  depth to 1). It'll also keep the system consistent with other data types (eg: Vector2 vs Vector3), and
  it makes the datatypes strongly typed since we must use explicit conversion functions when converting from 2D to 3D
  rectangles, making it impossible to accidentally mix between 2D and 3D rectangles.

  Unlike 2D rectangles, the Aabb assumes the same coordinate system as Sand Dune's 3D coordinate system.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API Aabb : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The number of centimeters between the two borders that intersect the Y-axis. */
	Float Width;

	/* The number of centimeters between the two borders that intersect the Z-axis. */
	Float Height;

	/* The number of centimeters between the two borders that intersect the X-axis. */
	Float Depth;

	Vector3 Center;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Aabb ();
	Aabb (Float inWidth, Float inHeight, Float inDepth, const Vector3& inCenter);
	Aabb (Float inForward, Float inBackward, Float inRight, Float inLeft, Float inUp, Float inDown);
	Aabb (const Vector3& cornerA, const Vector3& cornerB);
	Aabb (const Aabb& copy);


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Aabb& copy);
	bool operator== (const Aabb& otherRect) const;
	bool operator!= (const Aabb& otherRect) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Generates the smallest Aabb that encompasses all given points.
	 */
	static Aabb GenerateEncompassingAabb (const std::vector<Vector3>& points);

	inline Float GetSurfaceArea () const
	{
		return (Width * Height * 2.f) + (Depth * Height * 2.f) + (Depth * Width * 2.f);
	}

	inline Float GetVolume () const
	{
		return (Width * Height * Depth);
	}

	/**
	 * Returns true if all sides of this Aabb are equal.
	 */
	inline bool IsCubed () const
	{
		return (Width == Height && Width == Depth);
	}

	/**
	 * Returns true if all surfaces of this AABB are zero.
	 */
	inline bool IsEmpty () const
	{
		return (Width == 0.f && Height == 0.f && Depth == 0.f);
	}

	/**
	 * Returns true if none of the surfaces converged to 0.
	 */
	inline bool Is3d () const
	{
		return (Width > 0.f && Height > 0.f && Depth > 0.f);
	}

	/**
	 * Calculates the overlapping AABB.
	 * Returns an empty rectangle if the two AABBs do not overlap.
	 */
	Aabb GetOverlappingAabb (const Aabb& otherAabb) const;

	/**
	 * Returns the smallest possible Aabb that encompasses both bounding boxes.
	 */
	Aabb CombineAabb (const Aabb& combineWith) const;

	/**
	 * Returns true if this Aabb overlaps with other Aabb.
	 */
	bool Overlaps (const Aabb& otherAabb) const;

	/**
	 * Returns true if the given point is inside this bounds.
	 */
	bool EncompassesPoint (const Vector3& targetPoint) const;


	/*
	=====================
	  Accessors
	=====================
	*/

	/**
	 * Returns aliases to Width, Height, and Depth that maps to a particular dimension.
	 */
	inline Float GetXLength () const
	{
		return Depth;
	}

	inline Float GetYLength () const
	{
		return Width;
	}

	inline Float GetZLength () const
	{
		return Height;
	}

	/**
	 * Various accessors to obtain the plane that intersects the specified border. This assumes Sand Dune's 3D coordinate space.
	 * This also asssumes the perspective where the camera is facing down positive X.
	 */
	inline Float GetForward () const
	{
		return Center.X + (Depth * 0.5f);
	}

	inline Float GetBackward () const
	{
		return Center.X - (Depth * 0.5f);
	}

	inline Float GetRight () const
	{
		return Center.Y + (Width * 0.5f);
	}

	inline Float GetLeft () const
	{
		return Center.Y - (Width * 0.5f);
	}

	inline Float GetUp () const
	{
		return Center.Z + (Height * 0.5f);
	}

	inline Float GetDown () const
	{
		return Center.Z - (Height * 0.5f);
	}
};
SD_END