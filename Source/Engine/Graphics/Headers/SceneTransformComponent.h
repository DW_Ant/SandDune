/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformComponent.h
  An empty EntityComponent that also derives from a SceneTransform.
  This is used to give other components their own transforms.

  If attached to other Entities with SceneTransforms, this component will automatically
  be relative to the owner.
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "SceneTransform.h"

SD_BEGIN
#ifdef DEBUG_MODE
class GraphicsUnitTester;
#endif

class GRAPHICS_API SceneTransformComponent : public EntityComponent, public SceneTransform
{
	DECLARE_CLASS(SceneTransformComponent)


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleOwnerChange (EntityComponent* compThatChangedOwners, Entity* prevOwner);

#ifdef DEBUG_MODE
	//Used to verify HandleOwnerChange during unit tests.
	friend class GraphicsUnitTester;
#endif
};
SD_END