/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BorderRenderComponent.h
  A component that is capable of drawing a color or a texture (either stretched
  or tiled) around a perimeter. Nothing is drawn in the center.

  The borders will be rendered at a specified thickness regardless of the transform chain.
  The border will automatically scale up/down to fill the size.
=====================================================================
*/

#pragma once

#include "Color.h"
#include "Graphics.h"
#include "RenderComponent.h"

SD_BEGIN
class Texture;

class GRAPHICS_API BorderRenderComponent : public RenderComponent
{
	DECLARE_CLASS(BorderRenderComponent)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EDrawType
	{
		DT_Stretched, //The texture is stretched between border-to-border.
		DT_Tiled //The texture is repeated between border-to-border.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked whenever this component's border thickness has changed. */
	SDFunction<void, Float /*newBorderThickness*/> OnThicknessChanged;

protected:
	EDrawType DrawType;

	/* Used to determine the base size of this Entity (in absolute units).
	This behaves differently rather than simply setting the transform's scale.
	This unit will not affect any transforms relative to this component. Contrast to transform's scale,
	Entities relative to a transform with scale other than one also has their translation affected based on its parent's scale. */
	Vector2 BaseSize;

	/* Determines the origin of this border component. (0,0) will cause top left corner to be at the origin. (0.5, 0.5) will cause the origin to be at the center.
	(1, 1) will cause the bottom right corner to be at the origin. */
	Vector2 Pivot;

	/* Multipliers used when applying draw types. */
	Vector2 TextureScale;

	/* Reference to the texture that will be drawn around all borders and corners.
	The top of the texture is on the outside of the border. The bottom is on the inside of the border.
	If nullptr, it'll render a solid color around the borders instead. */
	DPointer<const Texture> BorderTexture;

	/* Color to apply to the texture. If there is no texture, then this is the solid color to use for all borders. */
	Color BorderColor;

	/* Determines how thick to draw the borders. */
	Float BorderThickness;

	/* Vertices that make up the borders. */
	sf::VertexArray Vertices;
	sf::RenderStates RenderStates;

	/* If true, then this component will automatically generate vertices whenever any of this component's properties changes attributes. */
	bool bAutoGenerateVerts;

private:
	/* The last (transform scale x BaseSize) used when the vertices were generated. */
	Vector2 LatestSize;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * When rendering a texture, this method sets the draw mode to tile type.
	 * The size multipliers will scale the textures relative to the texture size.
	 */
	virtual void ApplyDrawModeTiled (const Vector2& tileSizeMultiplier);

	/**
	 * When rendering a texture, this method sets the draw mode to stretched.
	 * The multipliers determines how often the texture will repeat along a single border.
	 * Vector(1, 2) implies there will be one texture tile horizontally and two tiles vertically for a single border.
	 */
	virtual void ApplyDrawModeStretched (const Vector2& numRepeats);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetBaseSize (const Vector2& newBaseSize);
	virtual void SetPivot (const Vector2& newPivot);
	virtual void SetBorderTexture (const Texture* newBorderTexture);
	virtual void SetBorderColor (Color newBorderColor);
	virtual void SetBorderThickness (Float newBorderThickness);
	virtual void SetAutoGenerateVerts (bool newAutoGenerateVerts);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector2 GetBaseSize () const
	{
		return BaseSize;
	}

	inline const Vector2& ReadBaseSize () const
	{
		return BaseSize;
	}

	inline Vector2 GetPivot () const
	{
		return Pivot;
	}

	inline const Vector2& ReadPivot () const
	{
		return Pivot;
	}

	inline const Texture* GetBorderTexture () const
	{
		return BorderTexture.Get();
	}

	inline Color GetBorderColor () const
	{
		return BorderColor;
	}

	inline Float GetBorderThickness () const
	{
		return BorderThickness;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void GenerateBorderVertices ();
};
SD_END