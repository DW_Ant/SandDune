/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TexturePool.h
  TexturePool is responsible for maintaining all imported Textures for
  recycling.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class Texture;

class GRAPHICS_API TexturePool : public ResourcePool
{
	DECLARE_CLASS(TexturePool)
	DECLARE_RESOURCE_POOL(Texture)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of tags each resource could reference to make it easier to find when searching.
	Only manipulate this vector through ResourceTag::AddTag and ResourceTag::RemoveTag since textures reference this vector by index.
	Directly manipulating this vector will cause the texture indices to be displaced. */
	std::vector<ResourceTag> Tags;

protected:
	/* List of all textures imported in this pool. */
	std::unordered_map<HashedString, Texture*> ImportedTextures;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;

protected:
	virtual void ReleaseResources () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a Texture object, imports file to resource, and registers the object to the TexturePool.
	 */
	virtual Texture* CreateAndImportTexture (const FileAttributes& file, const HashedString& textureName);

	/**
	 * Creates a Texture object from the given file. The file must be in '.txtr' format.
	 * If it imports successfully, then the texture asset will be registered to this TexturePool where texture name is the same name as
	 * the directory of the file (relative to the Content directory), and the file name itself. eg: "Path", "To", "File". File extension is excluded.
	 */
	virtual Texture* ImportTexture (const FileAttributes& file);

	/**
	 * Adds the given texture to this pool. Returns true on success.
	 */
	virtual bool AddTexture (Texture* newTexture, const HashedString& textureName);

	/**
	 * Populates the given vector with all imported textures with no order in particular.
	 */
	virtual void GetAllTextures (std::vector<Texture*>& outAllTextures);

	/**
	 * Retrieves the Texture instance associated with the given string.
	 */
	virtual const Texture* GetTexture (const HashedString& textureName, bool bLogOnMissing = true) const;
	virtual Texture* EditTexture (const HashedString& textureName, bool bLogOnMissing = true);

	/**
	 * Destroys and removes the Texture instance associated with the given string.
	 */
	virtual void DestroyTexture (const HashedString& textureName);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void SetTextureAttributes (Texture* target, const DString& textureName);
};
SD_END