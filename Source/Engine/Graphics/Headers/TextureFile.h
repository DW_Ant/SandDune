/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureFile.h
  A binary file that is able to read and write from .txtr files and produce a Texture object.

  This binary file contains the following data in this order:
  * [int] Width
  * [int] Height
  * [vector<int>] Pixels (each color is an uncompressed RGBA where each color channel is 8 bits.). For 64-bit files, the last 32 bits are ignored.
  * [vector<string>] Tags
  * [bool] Wrapping/Repeating
  * [bool] Smoothing
  * [bool] Mipmap
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class Texture;
class TexturePool;

class GRAPHICS_API TextureFile : public BinaryFile
{
	DECLARE_CLASS(TextureFile)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* File extension used for texture binary files. */
	static const DString FILE_EXTENSION;

protected:
	/* The texture resource this file is either reading from or writing to.
	When opening a texture file, this variable is assigned.
	When writing to a texture file, this variable must be assigned before writing.
	This file does not assume ownership and will not destroy this texture. */
	Texture* TargetTexture;

	/* The TexturePool is the TargetTexture should register to when opening. This variable must be assigned before opening the file.
	The location where the texture is stored in this pool is based on the file directory relative to the Content directory.
	This variable is not used when saving the texture. */
	TexturePool* TargetTexturePool;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual bool ReadContents (const DataBuffer& incomingData) override;
	virtual bool SaveContents (DataBuffer& outBuffer) const override;
	

	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTargetTexture (Texture* newTargetTexture);
	virtual void SetTargetTexturePool (TexturePool* newTargetTexturePool);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Texture* GetTargetTexture () const
	{
		return TargetTexture;
	}
};
SD_END