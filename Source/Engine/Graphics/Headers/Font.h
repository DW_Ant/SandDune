/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Font.h
  An object responsible for storing a reference to a font resource.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API Font : public Object
{
	DECLARE_CLASS(Font)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reads the number of spaces that are equal to one tab (in terms of character width).  This variable actually doesn't
	configure the tab width, it only records the width of SFML's tabs. */
	static const Int NUM_SPACES_PER_TAB;

	DString FontName;

protected:
	sf::Font* FontResource;

	/* If all characters are equal width, this is the number of pixels each character takes. Becomes negative for fonts with variable sizes. */
	Float UniformedCharWidth;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Release ();
	virtual void SetResource (sf::Font* newResource);

	/**
	 * Computes the total width in pixels of the specified character.
	 * @Param character:  The string that contains the character.  Reasoning for a string param instead of char is because some
	 * character encodings (such as UTF-8 or UTF-16) may require more than one 'character' to represent a single character.
	 */
	virtual Float GetCharacterWidth (const DString& character, Int charSize) const;

	/**
	 * Computes the total width in pixels of the whole string considering the text size.
	 */
	virtual Float CalculateStringWidth (const DString& line, Int charSize) const;

	/**
	 * Computes the total width (in pixels) of the characters (starting from startingIdx) within given string.
	 * If numChars is not negative, then it'll tally the total width of numChars starting from startingIdx.
	 * Otherwise, it'll count all following characters of startingIdx to the end of the string.
	 */
	virtual Float CalculateStringWidth (const DString& line, Int charSize, Int startingIdx, Int numChars = -1) const;

	/**
	 * Returns the character index (of the specified string) that is closest to the maxWidth limit (never returns characters beyond the limit).
	 * Returns -1 if the first character is beyond the width limit.
	 */
	virtual Int FindCharNearWidthLimit (const DString& line, Int charSize, Float maxWidth) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual DString GetFontName () const;

	/**
	 * Returns the SFML's font resource.
	 */
	virtual const sf::Font* GetFontResource () const;

	inline Float GetUniformedCharWidth () const
	{
		return UniformedCharWidth;
	}
};
SD_END