/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTarget.h
  An object that RenderComponents can draw to.  Common examples include window
  handles and procedurally generated textures.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class Camera;
class DrawLayer;
class RenderComponent;
class ScreenCapture;

class GRAPHICS_API RenderTarget : public Entity
{
	DECLARE_CLASS(RenderTarget)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/**
	 * Simple pair that pairs a draw layer with a camera to generate a scene from.
	 */
	struct SDrawLayerCamera
	{
		DrawLayer* Layer;
		Camera* Cam;

		SDrawLayerCamera (DrawLayer* inLayer, Camera* inCam) :
			Layer(inLayer),
			Cam(inCam)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of DrawLayers this RenderTarget will iterate through when generating a scene.
	Each DrawLayer has an associated camera to view from.  If it's not paired with a camera,
	then it assumes that the coordinates in the draw layer are already in screenspace.
	The RenderTarget will not destroy the cameras and Draw Layers in destruction since there
	are use cases where those Entities should persist beyond the RenderTarget. */
	std::vector<SDrawLayerCamera> DrawLayers;

	/* RenderTarget this object will inherit draw layers from.  This is used to ensure DrawLayers from the parent is
	also registered to this object's draw layer list. */
	DPointer<RenderTarget> ParentRenderTarget;

	/* Tick Component that determines when this RenderTarget may auto-generate its scene. */
	DPointer<TickComponent> Tick;

	/* If true, then this RenderTarget will destroy the cameras associated with each DrawLayer when this is destroyed. */
	bool DestroyCamerasOnDestruction;

	/* If true, then this RenderTarget will destroy all registered DrawLayers when this is destroyed. */
	bool DestroyDrawLayersOnDestruction;

	/* If not nullptr, then the next render cycle will process all render components to this screen capture. Automatically clears after the render cycle. */
	DPointer<ScreenCapture> RenderCapture;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Clears all drawn images from this object.
	 */
	virtual void Reset () = 0;

	/**
	 * Iterates through all DrawLayers to cause their RenderComponents to draw to this RenderTarget.
	 */
	virtual void GenerateScene ();

	/**
	 * Draws the SFML object to this resource.
	 */
	virtual void Draw (RenderComponent* renderComp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState = sf::RenderStates::Default);

	/**
	 * Invoked whenever the render pipeline finished drawing entities to this object.
	 * This function presents this object to the resource.
	 */
	virtual void Display ();

	/**
	 * Registers the DrawLayer to the render pipeline.
	 *
	 * @param drawLayer the new layer to register.  This function will reject the request if this layer is already registered.
	 * @param associatedCamera The camera that'll correspond to the draw layer.  If null, then the Entities drawn in this layer uses absolute coordinates.
	 */
	virtual void RegisterDrawLayer (DrawLayer* drawLayer, Camera* associatedCamera = nullptr);

	/**
	 * Removes the specified DrawLayer from the render pipeline.
	 */
	virtual void RemoveDrawLayer (DrawLayer* target);

	/**
	 * Iterates through the DrawLayer camera mapping, and assigns the DrawLayer to the specified Camera.
	 * Returns true if the DrawLayer was found and assigned the new camera map.
	 */
	virtual bool AssignCamera (DrawLayer* targetDrawLayer, Camera* newAssociatedCamera);
	virtual bool AssignCamera (Int drawLayerPriority, Camera* newAssociatedCamera);

	/**
	 * Copies the DrawLayer list from the other RenderTarget.
	 */
	virtual void SetDrawLayersFrom (RenderTarget* targetToCopyFrom);

	/**
	 * Returns the 2D Coordinates this RenderTarget was viewed in the viewport.
	 * This is useful to determine where entities are drawn in window space.
	 * For example, if this a RenderTexture that a Sprite is drawn in the center of the window,
	 * then this function returns the coordinates of that sprite relative to the window.
	 * For RenderTargets that are drawn multiple times with different transforms, this function only
	 * considers the latest transform.
	 */
	virtual Vector2 GetWindowCoordinates () const;

	/**
	 * Transfer DrawLayer references and ownership to the given RenderTarget.
	 * This RenderTarget will no longer be responsible for clearing the DrawLayers and their associated Cameras.
	 */
	virtual void TransferDrawLayersTo (RenderTarget* transferTo);

	/**
	 * If true, then this render target will automatically regenerate its scene at the specified update rate.
	 */
	virtual void SetAutoDraw (bool bAutoGenerateScene, Float updateRate = -1.f);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetParentRenderTarget (RenderTarget* newParentRenderTarget);
	virtual void SetDestroyCamerasOnDestruction (bool newDestroyCamerasOnDestruction);
	virtual void SetDestroyDrawLayersOnDestruction (bool newDestroyDrawLayersOnDestruction);
	virtual void SetScreenCapture (ScreenCapture* newScreenCapture);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual void GetSize (Int& outWidth, Int& outHeight) const = 0;
	virtual Vector2 GetSize () const = 0;

	inline RenderTarget* GetParentRenderTarget () const
	{
		return ParentRenderTarget.Get();
	}

	inline const std::vector<SDrawLayerCamera>& ReadDrawLayers () const
	{
		return DrawLayers;
	}

	inline std::vector<SDrawLayerCamera>& EditDrawLayers ()
	{
		return DrawLayers;
	}

	inline TickComponent* GetTick () const
	{
		return Tick.Get();
	}

	inline bool GetDestroyCamerasOnDestruction () const
	{
		return DestroyCamerasOnDestruction;
	}

	inline bool GetDestroyDrawLayersOnDestruction () const
	{
		return DestroyDrawLayersOnDestruction;
	}

	inline ScreenCapture* GetRenderCapture () const
	{
		return RenderCapture.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitTick ();

	/**
	 * Iterates through the DrawLayer list, and removes any from the list that are marked for destruction.
	 */
	virtual void RemovePurgedDrawLayers ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
	virtual void HandleGarbageCollection ();
};
SD_END