/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformUpdater.h
  An Entity that efficiently and continuously calculates the absolute transformation for
  every dirty SceneTransformation.

  There are two main ways to register SceneTransforms to the updater: Temporarily and Permanently

  Temporarily
  These transforms are pushed to the vector unchecked, and as soon as the updater computes the abs transform,
  they are removed from the list. Temporary transforms are computed synchronously.
  PRO: It's fast to add/remove transforms to the list. Computed only once before it's removed.
  CON: Computed synchronously.

  Permanently
  These transforms are pushed to another vector. This vector is checked where there aren't any duplicates.
  The transforms remain in this vector even after the updater computed its transform. Since the vector is checked,
  these transforms are computed in separate threads (if enabled).
  PRO: It's fast to iterate through the transforms. Transforms remain in this list until it's explicitly removed. Computed asynchronously.
  CON: It's slow to add transforms to the list.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class SceneTransform;

#ifndef SD_TRANSFORM_UPDATER_MAX_THREADS
#define SD_TRANSFORM_UPDATER_MAX_THREADS 8
#endif

class GRAPHICS_API SceneTransformUpdater : public Entity
{
	DECLARE_CLASS(SceneTransformUpdater)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of SceneTransforms that will continuously checked for updating their absolute tranformation.
	This vector should only contain the root transformations since the transforms relative to these transforms
	will also update their absolute transformations. */
	std::vector<SceneTransform*> PermanentTransforms;

	/* List of SceneTransforms that will be removed from the list as soon as their absolute transformation is calculated.
	This vector does not shrink automatically. Instead the first empty item indicates the rest of the vector is null.
	This vector should only have root transformations since transforms relative to them will also update their abs transform.

	Note: It's possible for relative transforms to be found in this list when the sub transform is updated
	before their root transform. For performance reasons, it does not iterate through this list to keep it clean.
	It'll end up being one extra call to calculate the absolute transformation (which should do nothing due to the dirty flag). */
	std::vector<SceneTransform*> TemporaryTransforms;

	/* The number of transforms from the permanent transform vector each thread will compute.
	If 0, then both vectors (temporary and permanent) are computed synchronously on the same thread.
	Note: The TemporaryTransforms are not handled in multiple threads since transforms are not guaranteed to be unique,
	which may introduce race conditions. */
	size_t NumTransformsPerThread;

	/* Worker threads responsible for calculating the absolute transforms in the PermanentTransforms vector. */
	std::thread* WorkerThreads[SD_TRANSFORM_UPDATER_MAX_THREADS];

	/* The number of threads that were allocated in WorkerThreads. */
	int NumWorkerThreads;

	/* Determines the number of worker threads that are currently computing transforms. */
	std::atomic<int> NumActiveThreads;

	/* Increments every calculation cycle. Used to detect a new update from the main thread. Wrapping around is harmless. */
	std::atomic<unsigned char> FrameCycle;

	/* Condition variable that used to determine which threads are active or not. */
	std::condition_variable CalcFinishedCondition;
	std::mutex CalcFinishedConditionMutex;

	/* If true, then all worker threads need to return. */
	std::atomic<bool> bTerminateWorkerThreads;

private:
	/* The next index in TemporaryTransforms that is nullptr. This is used for quickly accessing the next available slot when
	adding a new temporary transform. */
	size_t NextAvailableTemporarySlot;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if one of the transforms the given transform is relative to is already registered to the permanent transform list.
	 * Complexity of this function is the length of the relative transform x the length of the permanent vector list.
	 */
	virtual bool IsAlreadyPermanentTransform (SceneTransform* transform) const;

	/**
	 * Adds the given transformation to the permanent transform list. This function ensures there aren't any duplicates.
	 *
	 * Returns true if successfully registered.
	 */
	virtual bool RegisterPermanentTransform (SceneTransform* newTransform);

	/**
	 * Adds the transformation to the temporary transform list where it will calculate its transformation once before it's
	 * removed from the list.
	 *
	 * This function does not check for duplicates or root transformations for performance reasons.
	 *
	 * WARNING: The given transformation should not reside in the permanent transformation vector. If it is, then a race
	 * condition may occur when the permanent transforms are computing their abs transform in a separate thread while the temporary
	 * transforms are updating.
	 */
	virtual void RegisterTemporaryTransform (SceneTransform* newTransform);

	virtual void RemovePermanentTransform (SceneTransform* oldTransform);

	/**
	 * Although temporary transforms are automatically removed from the list whenever they're updated,
	 * this function is for transformations that are about to be purged.
	 * This function is slower than being removed normally since this will iterate through the list and remove
	 * the item from the middle of the vector. This doesn't set it to null since null will indicate that
	 * all following elements are also null.
	 */
	virtual void RemoveTemporaryTransform (SceneTransform* oldTransform);

	/**
	 * Collapses the size of the TemporaryTransform vector to remove all elements that are nulled out.
	 */
	virtual void ShrinkTemporaryTransformVector ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetNumTransformsPerThread (size_t newNumTransformsPerThread);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline size_t GetNumTransformsPerThread () const
	{
		return NumTransformsPerThread;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through all permanent transformations to ensure their and their sub transforms' absolute attributes
	 * are up to date.
	 */
	virtual void UpdatePermanentTransforms ();

	/**
	 * Iterates through all temporary transformations to ensure their and their sub transforms' absolute attributes
	 * are up to date. It'll then clear all transforms from the temporary vector.
	 */
	virtual void UpdateTemporaryTransforms ();

	/**
	 * Computes the absolute transformations for entities in the PermanentTransforms vector. This function may be processed in multiple threads.
	 * This only computes up to NumTransformsPerThread starting from the specified index value.
	 */
	virtual void CalcPermanentTransformBatch (size_t startIdx);

	/**
	 * Evaluates the worker threads load based on the number of registered PermanentTransformers.
	 * This function may allocate new worker threads if available. If there are no more workers available, it'll increase the work load for each worker thread.
	 */
	virtual void EvaluateWorkerThreadLoad ();

	/**
	 * Function to run for each worker thread. This function will not return until this updater is destroyed.
	 */
	virtual void RunWorkerThread (size_t workerIdx);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END