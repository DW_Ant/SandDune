/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransformUpdater.h
  An Entity that continuously calculates the absolute transformation for every dirty
  PlanarTransformation.

  PlanarTransforms are strange in the sense where the size of the parent can adjust
  transform of its children (eg: using normalized coordinates). Sometimes, the
  children can influence the size of the parent (eg: GuiEntity::AutoSize properties).
  
  Because it can go both ways, this Updater processes runs two steps.
  Step 1:
  It'll iterate through every dirty transform and compute their absolute transforms.
  It'll calculate the root transform before moving down to its children. Every transform
  that changes will be pushed to a queue for a callback.

  Step 2:
  It'll then iterate through that queue to notify the subclasses that their transforms
  changed, allowing them to react or restructure things based on their new transforms.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class PlanarTransform;

class GRAPHICS_API PlanarTransformUpdater : public Entity
{
	DECLARE_CLASS(PlanarTransformUpdater)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of transforms that needs to calculate their absolute transforms. All transforms relative to these trnasforms
	will also be updated. No need to remove components from this list as this list clears after every update. */
	std::vector<PlanarTransform*> DirtyTransforms;

	/* List of transforms that were updated, and are currently waiting for a callback. */
	std::vector<PlanarTransform*> PendingCallbackTransforms;

	/* List of transforms that attempted to register during an update cycle. This list will migrate to the DirtyTransforms
	after the update cycle completes. */
	std::vector<PlanarTransform*> LockedDirtyTransforms;

private:
	/* Becomes true when this updater is in the middle of updating transforms. */
	bool bLocked;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds this transform to the list of dirty transforms.
	 * For expediency purposes, this function will not do a unique check. This will also not
	 * check if one of its parents already exists in the list. This simply adds it to the end.
	 */
	virtual void AddDirtyTransform (PlanarTransform* newTransform);

	/**
	 * Searches through the dirty transform list to remove all instances of the old transform from it.
	 * There's no need to call this unless the transform object is being purged. Otherwise, vectors
	 * automatically empty themselves every update.
	 */
	virtual void RemoveDirtyTransform (PlanarTransform* oldTransform);

	/**
	 * Registers this planar transform component to the PendingCallbackTransforms vector.
	 * This function does not conduct any unique checks. Registering a transform multiple times would result in multiple callbacks.
	 */
	virtual void AddPendingCallbackTransform (PlanarTransform* transform);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each dirty transform chain to recompute their absolute transforms.
	 * It'll then notify each transform that they changed before clearing both vectors.
	 */
	virtual void UpdateTransforms ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END