/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScreenCapture.h
  This object is responsible for listening to render calls for a single frame to produce
  a RenderTexture that is also associated with a pixel map where each pixel contains
  a pointer to a RenderComponent responsible for drawing on that pixel.

  The ScreenCapture is a very slow process, and it should only be used when performance
  is not essential.
=====================================================================
*/

#pragma once

#include "Color.h"
#include "Graphics.h"
#include "Transformation.h"

SD_BEGIN
class Camera;
class RenderComponent;
class RenderTarget;
class RenderTexture;

class GRAPHICS_API ScreenCapture : public Object
{
	DECLARE_CLASS(ScreenCapture)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EPixelMapType
	{
		PMT_None, //Does not generate a pixel map.
		PMT_AABB, //Uses the render component's AABB. Does not consider transparency and rotation. Although this method is much faster, it is an approximation.
		PMT_PerPixel //Very slow method. Supports transparency at a per pixel level. Also considers the render component's rotation.
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If bound, then only RenderComponents where this delegate returns true are considered when generating the captured texture. */
	SDFunction<bool, RenderComponent*> OnIsRelevant;

protected:
	/* The RenderTarget this capture will generate the pointer map for. */
	RenderTarget* CaptureTarget;

	/* Determines if a pixel map should be generated or not. If it should, this determines which method should be used. */
	EPixelMapType PixelMapType;

	/* The opacity of the render object must reach at least this threshold to be considered for the pixel map.
	For example, if the AlphaThreshold is 25%, color components with an alpha channel below 64 (quarter of 255) will not update the pixel map. */
	Float AlphaThreshold;

	/* If true, then ScreenCaptures are recursively calculated for sprites that are rendering other RenderTextures, allowing the capture to detect rendered
	components within its viewports. Otherwise, it'll only detect the sprite component rendering the RenderTexture. */
	bool bDetectCompsWithinRenderTextures;

	Color DefaultColor;

	/* The generated texture when producing this capture. The texture is destroyed when the ScreenCapture is destroyed. */
	DPointer<RenderTexture> CapturedTexture;

	/**
	 * Temporary RenderTexture used to populate the pixel map.
	 *
	 * Each RenderComponent will also draw to this texture, however, between each render call, the texture is generated and reset. Although that would be extremely slow
	 * to do, it would allow the system to be agnostic to the type of RenderComponent used to draw. For example, instead of remapping each pixel on a texture transformed relative to
	 * the component's transform and camera used, it'll be simpler and more consistent when using an extra RenderTexture.
	 */
	RenderTexture* PixelMapGeneratorTexture;

	/* Pointer that points to the start of the pointer map. The size of this buffer is the width x height of the CapturedTexture.
	Each element in this array corresponds to a single pixel to the CapturedTexture. 
	The map is row-major (climbs along the X-axis of the texture before moving onto the next row of pixels).
	This object does not take ownership over any of these components. */
	RenderComponent** PixelMap;

	/* Returns the number of RenderComponents that affected the PixelMap. This includes components found within RenderTextures if bDetectCompsWithinRenderTextures is true. */
	Int NumCapturedComponents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Launches the process to capture the scene.
	 */
	virtual bool GenerateCaptureTexture (RenderTarget* inCaptureTarget, Color defaultColor);

	/**
	 * Accesses the pointer map to find the RenderComponent that drawn to the specific texture dimensions.
	 */
	virtual RenderComponent* GetMappedPixel (Int texturePosX, Int texturePosY) const;

	/**
	 * Adds the given RenderTexture to be drawn at the specified coordinates on top of the CaptureTexture.
	 * This function assumes that this ScreenCapture is in the middle of generating a screen capture.
	 */
	virtual void AddRenderTexture (RenderTexture* renderTex, RenderComponent* renderComp, const Transformation::SScreenProjectionData& projection);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPixelMapType (EPixelMapType newPixelMapType);
	virtual void SetAlphaThreshold (Float newAlphaThreshold);
	virtual void SetDetectCompsWithinRenderTextures (bool newDetectCompsWithinRenderTextures);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EPixelMapType GetPixelMapType () const
	{
		return PixelMapType;
	}

	inline Float GetAlphaThreshold () const
	{
		return AlphaThreshold;
	}

	inline bool IsDetectingCompsWithinRenderTextures () const
	{
		return bDetectCompsWithinRenderTextures;
	}

	inline Color GetDefaultColor () const
	{
		return DefaultColor;
	}

	inline RenderTexture* GetCapturedTexture () const
	{
		return CapturedTexture.Get();
	}

	inline Int GetNumCapturedComponents () const
	{
		return NumCapturedComponents;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Replaces chunks of this capture's pixel map data with the otherCapture's pixel map data projected onto this capture.
	 * The renderComp's AABB and the given projection data is used to translate otherCapture's pixel map to this capture's pixel map.
	 */
	virtual void OverwritePixelMapFrom (const ScreenCapture& otherCapture, RenderComponent* renderComp, const Transformation::SScreenProjectionData& projection);

	virtual void ClearCapturedData ();

	virtual void ProcessRenderComponent (RenderComponent* comp, const Camera* cam);
	virtual void FinishRenderCycle ();

	friend class RenderTarget;
};
SD_END