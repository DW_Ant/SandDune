/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextRenderComponent.h
  A component that communicates with SFML to render
  text with specified parameters.
=====================================================================
*/

#pragma once

#include "RenderComponent.h"

SD_BEGIN
class Font;
class Shader;

class GRAPHICS_API TextRenderComponent : public RenderComponent
{
	DECLARE_CLASS(TextRenderComponent)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct STextData
	{
		sf::Text* SfmlInstance;
		sf::Vector2f DrawOffset;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to all instantiated SFML Text objects.  Each instance represents a new line.
	   Editing this text's font size and font should be handled through this TextRenderComponent instead
	   so that its shader may adapt appropriately. */
	std::vector<STextData> Texts;

	/* Special affect that's applied over the text. */
	DPointer<Shader> TextShader;

	sf::Color FontColor;

	/* Render state that'll is used how the text is drawn to the render target. */
	sf::RenderStates RenderState;

	/* Font that's applied to sf Text instances. */
	DPointer<const Font> TextFont;

	/* Character size applied to the sf Text instances. */
	Int FontSize;

	/* List of callbacks to invoke whenever this component updates its shader. */
	std::vector<SDFunction<void>> OnShaderChangedCallbacks;

private:
	/* The most recent Aabb calculated from GetAabb. If empty, it'll iterate through the text instances to recompute its Aabb. */
	mutable Aabb CachedBoundingBox;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Constructs a new Text instance that is initialized with the given parameters, and registers the text instance to the DrawOffset list.
	 * Returns a reference to the newly created text data.
	 */
	virtual STextData& CreateTextInstance (const DString& textContent, const Font* font, uint32 fontSize);

	/**
	 * Deletes all text pointers and clears the Texts vector.
	 * This also clears their corresponding DrawOffsets.
	 */
	virtual void DeleteAllText ();

	/**
	 * Deletes the text data associated with the specified line.
	 */
	virtual void DeleteTextData (size_t instanceIdx);

	virtual void RegisterOnShaderChangedCallback (SDFunction<void> newOnShaderChangedCallback);
	virtual void UnregisterOnShaderChangedCallback (SDFunction<void> oldOnShaderChangedCallback);

	virtual void SetTextShader (Shader* newTextShader);
	virtual void SetFontColor (sf::Color newFontColor);
	virtual void SetTextFont (const Font* newTextFont);
	virtual void SetFontSize (Int newFontSize);

	/**
	 * Clears bounding box information so that next time GetAabb is called, it'll recompute it.
	 */
	virtual void MarkAabbAsDirty ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline std::vector<STextData>& EditTexts ()
	{
		MarkAabbAsDirty();
		return Texts;
	}

	inline const std::vector<STextData>& ReadTexts () const
	{
		return Texts;
	}

	inline Shader* GetTextShader () const
	{
		return TextShader.Get();
	}

	inline sf::Color GetFontColor () const
	{
		return FontColor;
	}

	inline const Font* GetTextFont () const
	{
		return TextFont.Get();
	}

	inline Int GetFontSize () const
	{
		return FontSize;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates any shader parameters for the current applied shader, and may update the
	 * texts' render states based on current shader, font, and character size.
	 */
	virtual void RefreshTextShader ();
};
SD_END