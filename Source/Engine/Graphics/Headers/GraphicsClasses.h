/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsClasses.h
  Contains all header includes for the Graphics module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the GraphicsClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_GRAPHICS
#include "Aabb.h"
#include "BorderRenderComponent.h"
#include "Camera.h"
#include "Color.h"
#include "ColorRenderComponent.h"
#include "ColorTester.h"
#include "CurveRenderComponent.h"
#include "DrawLayer.h"
#include "EmptyRenderTarget.h"
#include "Font.h"
#include "FontPool.h"
#include "Graphics.h"
#include "GraphicsEngineComponent.h"
#include "GraphicsUnitTester.h"
#include "GraphicsUtils.h"
#include "InstancedSpriteComponent.h"
#include "PlanarCamera.h"
#include "PlanarDrawLayer.h"
#include "PlanarTransform.h"
#include "PlanarTransformComponent.h"
#include "PlanarTransformUpdater.h"
#include "PlatformMacGraphics.h"
#include "PlatformWindowsGraphics.h"
#include "RenderComponent.h"
#include "RenderTarget.h"
#include "RenderTexture.h"
#include "RenderTextureTester.h"
#include "ResolutionComponent.h"
#include "SceneCamera.h"
#include "SceneDrawLayer.h"
#include "SceneEntity.h"
#include "SceneTransform.h"
#include "SceneTransformComponent.h"
#include "SceneTransformUpdater.h"
#include "SceneTransformUpdaterTester.h"
#include "ScreenCapture.h"
#include "Shader.h"
#include "SpriteComponent.h"
#include "TextRenderComponent.h"
#include "Texture.h"
#include "TextureFile.h"
#include "TexturePool.h"
#include "TopDownCamera.h"
#include "Transformation.h"
#include "Window.h"

#endif
