/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Color.h
  An object representing a color for the SFML's color object.  This
  object is treated as a datatype.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API Color : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Common colors */
	static const Color WHITE;
	static const Color BLACK;
	static const Color RED;
	static const Color YELLOW;
	static const Color GREEN;
	static const Color CYAN;
	static const Color BLUE;
	static const Color MAGENTA;
	static const Color INVISIBLE;

	/* The color object actually implementing this datatype. */
	sf::Color Source;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Color ();
	Color (const Color& otherColor);
	Color (const sf::Color& otherColor);
	Color (uint8 red, uint8 green, uint8 blue, uint8 alpha = 255);

	/**
	 * Constructor that creates a color from a single 32-bit int.
	 * The Int's bits are divided into 4 uint8s where each segment represents a color component.
	 * The leading bits define the Red color, followed by the Green, Blue, and Alpha channels.
	 */
	Color (uint32 colors);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ResetToDefaults () override;
	virtual DString ToString () const override;
	virtual void ParseString (const DString& str) override;
	virtual size_t GetMinBytes () const override;
	virtual void Serialize (DataBuffer& outData) const override;
	virtual bool Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Linear interpolation between min and max based on the given ratio.
	 * Ratio ranges from [0,1] to return a value between min and max.  0 being equal to min, and 1 being equal to max.
	 */
	static Color Lerp (Float ratio, Color min, Color max);
};

#pragma region "Operators"
//Same as sf::Color operators.  Please refer to SFML documentation to see what these operators do
//http://www.sfml-dev.org/documentation/2.3.2/classsf_1_1Color.php
GRAPHICS_API bool operator== (const Color &left, const Color &right);
GRAPHICS_API bool operator!= (const Color &left, const Color &right);
GRAPHICS_API Color operator+ (const Color &left, const Color &right);
GRAPHICS_API Color operator- (const Color &left, const Color &right);
GRAPHICS_API Color operator* (const Color &left, const Color &right);
GRAPHICS_API Color& operator+= (Color &left, const Color &right);
GRAPHICS_API Color& operator-= (Color &left, const Color &right);
GRAPHICS_API Color& operator*= (Color &left, const Color &right);
#pragma endregion
SD_END