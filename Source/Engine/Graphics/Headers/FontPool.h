/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FontPool.h
  FontPool is responsible for maintaining all imported Fonts for
  recycling.
=====================================================================
*/

#pragma once

#include "Font.h"
#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API FontPool : public ResourcePool
{
	DECLARE_CLASS(FontPool)
	DECLARE_RESOURCE_POOL(Font)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all fonts imported to this pool. */
	std::unordered_map<HashedString, Font*> ImportedFonts;

	DPointer<Font> DefaultFont;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ReleaseResources () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a Font object, imports file to resource, and registers the object to the FontPool.
	 */
	virtual Font* CreateAndImportFont (const FileAttributes& file, const HashedString& fontName);

	/**
	 * Populates the given vector with all imported fonts with no order in particular.
	 */
	virtual void GetAllFonts (std::vector<Font*>& outAllFonts);

	/**
	 * Retrieves the Font instance associated with the given string.
	 */
	virtual const Font* GetFont (const HashedString& fontName, bool bLogOnMissing = true) const;
	virtual Font* EditFont (const HashedString& fontName, bool bLogOnMissing = true);

	/**
	 * Removes and destroys the Font instance associated with the given string.
	 */
	virtual void DestroyFont (const HashedString& fontName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Font* GetDefaultFont () const;
};
SD_END