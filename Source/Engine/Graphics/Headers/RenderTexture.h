/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTexture.h
  A wrapper to the SFML's RenderTexture.  This is a 2D texture that permits RenderComponents
  to draw themselves to this texture.

  Drawing to a texture is useful for generating images relative to a frame.  This is useful for
  split screen and drawing a scene within a sub component such as a scrollbar.

  Essentially this class is a wrapper to SFML's RenderTexture.  The purpose behind this wrapper is
  to "register" this class in Sand Dune's framework to allow RenderComponents to draw contents to this
  class as a SD::RenderTarget and to generate meta data of this class (such as CDOs and class trees).
=====================================================================
*/

#pragma once

#include "Color.h"
#include "Graphics.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API RenderTexture : public RenderTarget
{
	DECLARE_CLASS(RenderTexture)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The color to use when clearing the texture resource. */
	Color ResetColor;

protected:
	/* The actual render texture implementation.  Note:  a pointer is used here since the class default object is instantiated at startup time,
	and that would cause a freeze.  Potentially because of accessing a mutex GlResource constructor.
	See:  https://github.com/SFML/SFML/issues/741
	A work around is to make this variable a pointer, and only assign the Resource variable on BeginObject instead of constructor since
	CDO's do not invoke BeginObject. */
	sf::RenderTexture* Resource;

	/* The RenderTarget responsible for drawing an Entity displaying this RenderTexture. Use this to recursively climb up the render targets to find the window
	handle containing this texture. */
	RenderTarget* OuterRenderTarget;

	/* The window coordinates where this texture is last drawn relative to the top left corner of the Window. */
	Vector2 LatestDrawCoordinates;

private:
	bool CreatedResource;

	/* Number of draw calls invoked within a single cycle. Resets every time it generates a new scene. */
	Int NumDrawCalls;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Reset () override;
	virtual void GenerateScene () override;
	virtual void Draw (RenderComponent* comp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState = sf::RenderStates::Default) override;
	virtual void Display () override;
	virtual Vector2 GetWindowCoordinates () const override;
	virtual void GetSize (Int& outWidth, Int& outHeight) const override;
	virtual Vector2 GetSize () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a SFML Render Texture with the specified dimensions.
	 * Returns true on success.
	 * See sf::RenderTexture::create
	 */
	bool CreateResource (Int width, Int height, bool hasDepthBuffer);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOuterRenderTarget (RenderTarget* newOuterRenderTarget);
	void SetLatestDrawCoordinates (const Vector2& newLatestDrawCoordinates);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const sf::RenderTexture* GetResource () const
	{
		return Resource;
	}

	inline sf::RenderTexture* EditResource ()
	{
		return Resource;
	}

	inline RenderTarget* GetOuterRenderTarget () const
	{
		return OuterRenderTarget;
	}
};
SD_END