/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TexturePool.cpp
=====================================================================
*/

#include "Texture.h"
#include "TextureFile.h"
#include "TexturePool.h"

IMPLEMENT_CLASS(SD::TexturePool, SD::ResourcePool)
SD_BEGIN
IMPLEMENT_RESOURCE_POOL(Texture)

void TexturePool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();
}

void TexturePool::ReleaseResources ()
{
	for (std::pair<HashedString, Texture*> texture : ImportedTextures)
	{
		texture.second->Destroy();
	}
	ImportedTextures.clear();
}

void TexturePool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

Texture* TexturePool::CreateAndImportTexture (const FileAttributes& file, const HashedString& textureName)
{
	if (ImportedTextures.contains(textureName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import texture from file \"%s\" since the texture name \"%s\" is already in use."), file.GetName(true, true), textureName.ReadString());
		return nullptr;
	}

	sf::Texture* newTexture = new sf::Texture();
	{
		SfmlOutputStream sfmlOutput;

		//Import texture
		if (!newTexture->loadFromFile(file.GetName(true, true).ToCString()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create texture.  Could not load %s.  %s"), file.GetName(true, true), sfmlOutput.ReadOutput());
			delete newTexture;
			return nullptr;
		}
	}

	Texture* result = Texture::CreateObject();
	if (result == nullptr)
	{
		delete newTexture;
		return nullptr;
	}

	result->SetResource(newTexture);

#ifdef REQPOWEROF2
	Int width;
	Int height;
	result->GetDimensions(OUT width, OUT height);

	//Typically sprites are automatically padded so that they are in powers of 2.
	if (!Utils::IsPowerOf2(width) || !Utils::IsPowerOf2(height))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import texture %s.  The dimensions of that image is not in powers of two (%sx%s)"), file.GetName(true, true), width, height);
		result->Destroy();
		return nullptr;
	}
#endif

	SetTextureAttributes(result, textureName.ReadString());
	ImportedTextures.insert({textureName, result});
	result->SetOwningTexturePool(this);
	return result;
}

Texture* TexturePool::ImportTexture (const FileAttributes& file)
{
	if (!OS_CheckIfFileExists(file))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import %s since that file does not exist."), file.GetName(true, true));
		return nullptr;
	}

	if (file.ReadFileExtension().Compare(TextureFile::FILE_EXTENSION, DString::CC_IgnoreCase) != 0)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import %s since only %s extensions are supported."), file.GetName(true, true), TextureFile::FILE_EXTENSION);
		return nullptr;
	}

	TextureFile* textureFile = TextureFile::CreateObject();
	textureFile->SetTargetTexturePool(this);
	if (!textureFile->OpenFile(file))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import %s. See the log for details."), file.GetName(true, true));
	}

	Texture* result = textureFile->GetTargetTexture();
	textureFile->Destroy();
	return result;
}

bool TexturePool::AddTexture (Texture* newTexture, const HashedString& textureName)
{
	if (ImportedTextures.contains(textureName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to add texture \"%s\" to the texture pool since \"%s\" is already in use."), newTexture->ToString(), textureName.ReadString());
		return false;
	}

	ImportedTextures.insert({textureName, newTexture});
	return true;
}

void TexturePool::GetAllTextures (std::vector<Texture*>& outAllTextures)
{
	for (std::pair<HashedString, Texture*> texture : ImportedTextures)
	{
		outAllTextures.push_back(texture.second);
	}
}

const Texture* TexturePool::GetTexture (const HashedString& textureName, bool bLogOnMissing) const
{
	if (!ImportedTextures.contains(textureName))
	{
		if (bLogOnMissing)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("The Texture \"%s\" does not exist in the TexturePool."), textureName.ReadString());
		}

		return nullptr;
	}

	return ImportedTextures.at(textureName);
}

Texture* TexturePool::EditTexture (const HashedString& textureName, bool bLogOnMissing)
{
	if (!ImportedTextures.contains(textureName))
	{
		if (bLogOnMissing)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("The Texture \"%s\" does not exist in the TexturePool."), textureName.ReadString());
		}

		return nullptr;
	}

	return ImportedTextures.at(textureName);
}

void TexturePool::DestroyTexture (const HashedString& textureName)
{
	if (ImportedTextures.contains(textureName))
	{
		ImportedTextures.at(textureName)->Destroy();
		ImportedTextures.erase(textureName);
	}
}

void TexturePool::SetTextureAttributes (Texture* target, const DString& textureName)
{
	target->SetTextureName(textureName);
	target->SetRepeat(true); //By default:  textures should be repeating since smeared textures are typically not desired.
}
SD_END