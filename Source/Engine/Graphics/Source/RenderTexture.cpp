/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTexture.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::RenderTexture, SD::RenderTarget)
SD_BEGIN

void RenderTexture::InitProps ()
{
	Super::InitProps();

	ResetColor = Color(0, 0, 0, 255);
	Resource = nullptr;
	OuterRenderTarget = nullptr;
	LatestDrawCoordinates = Vector2::ZERO_VECTOR;

	CreatedResource = false;
	NumDrawCalls = 0;
}

void RenderTexture::BeginObject ()
{
	Super::BeginObject();

	Resource = new sf::RenderTexture();
}

void RenderTexture::Reset ()
{
	CHECK(Resource != nullptr)
	Resource->clear(ResetColor.Source);
}

void RenderTexture::GenerateScene ()
{
	NumDrawCalls = 0;
	Super::GenerateScene();
}

void RenderTexture::Draw (RenderComponent* comp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState)
{
	Super::Draw(comp, cam, drawable, renderState);

	CHECK(Resource != nullptr)
	Resource->draw(drawable, renderState);
	++NumDrawCalls;
}

void RenderTexture::Display ()
{
	Super::Display();

	CHECK(Resource != nullptr)

	if (NumDrawCalls <= 0)
	{
		//RenderTextures should draw at least something to prevent conflicts with other RenderTextures.
		sf::RectangleShape color(sf::Vector2f(64.f, 64.f));
		color.setFillColor(sf::Color(0, 0, 0, 0));
		Resource->draw(color);
		++NumDrawCalls;
	}

	Resource->display();
}

Vector2 RenderTexture::GetWindowCoordinates () const
{
	return LatestDrawCoordinates;
}

void RenderTexture::GetSize (Int& outWidth, Int& outHeight) const
{
	CHECK(Resource != nullptr)
	sf::Vector2u textureSize = Resource->getTexture().getSize();
	outWidth = textureSize.x;
	outHeight = textureSize.y;
}

Vector2 RenderTexture::GetSize () const
{
	Int width;
	Int height;
	GetSize(OUT width, OUT height);

	return Vector2(width.ToFloat(), height.ToFloat());
}

void RenderTexture::Destroyed ()
{
	if (Resource != nullptr)
	{
		delete Resource;
		Resource = nullptr;
	}

	Super::Destroyed();
}

bool RenderTexture::CreateResource (Int width, Int height, bool hasDepthBuffer)
{
	CHECK_INFO(!CreatedResource, "Cannot create another resource on a RenderTexture that already has created one.")

	CreatedResource = Resource->create(width.ToUnsignedInt32(), height.ToUnsignedInt32(), hasDepthBuffer);
	return CreatedResource;
}

void RenderTexture::SetOuterRenderTarget (RenderTarget* newOuterRenderTarget)
{
	OuterRenderTarget = newOuterRenderTarget;
}

void RenderTexture::SetLatestDrawCoordinates (const Vector2& newLatestDrawCoordinates)
{
	LatestDrawCoordinates = newLatestDrawCoordinates;
}
SD_END