/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextRenderComponent.cpp
=====================================================================
*/

#include "Aabb.h"
#include "Font.h"
#include "RenderTarget.h"
#include "Shader.h"
#include "TextRenderComponent.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::TextRenderComponent, SD::RenderComponent)
SD_BEGIN

void TextRenderComponent::InitProps ()
{
	Super::InitProps();

	TextShader = nullptr;
	FontColor = sf::Color::White;
	RenderState = sf::RenderStates::Default;
	TextFont = nullptr;
	FontSize = 12;
}

void TextRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextRenderComponent* textTemplate = dynamic_cast<const TextRenderComponent*>(objTemplate);
	if (textTemplate != nullptr)
	{
		SetTextShader(textTemplate->GetTextShader());
		SetFontColor(textTemplate->GetFontColor());
		SetTextFont(textTemplate->GetTextFont());
		SetFontSize(textTemplate->GetFontSize());
	}
}

void TextRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	const Vector2 renderTargetSize = renderTarget->GetSize();
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	for (size_t i = 0; i < Texts.size(); ++i)
	{
		const sf::Vector2f drawPosition = (Texts.at(i).DrawOffset * projectionData.Scale) + projectionData.Position;
		if (Texts.at(i).SfmlInstance != nullptr && drawPosition.y + (FontSize.ToFloat() * projectionData.Scale.y) >= 0.f)
		{
			if (drawPosition.y >= renderTargetSize.Y)
			{
				break; //Beyond viewable region
			}

			CHECK(Texts.at(i).SfmlInstance != nullptr)
			if (Texts.at(i).SfmlInstance->getString().getSize() > 0)
			{
				Texts.at(i).SfmlInstance->setPosition(drawPosition);
				Texts.at(i).SfmlInstance->setScale(projectionData.Scale); //Note: We only use scale instead of size since the sf objects have their own base size through the font size.
				renderTarget->Draw(this, camera, *(Texts.at(i).SfmlInstance), RenderState);
			}
		}
	}
}

Aabb TextRenderComponent::GetAabb () const
{
	if (CachedBoundingBox.IsEmpty() && Texts.size() > 0)
	{
		float top = MAX_FLOAT; //Top maps to smallest Y
		float right = -MAX_FLOAT; //Right maps to greatest X
		float bottom = -MAX_FLOAT; //Bottom maps to greatest Y
		float left = MAX_FLOAT; //Left maps to smallest X

		for (size_t i = 0; i < Texts.size(); ++i)
		{
			sf::FloatRect textBounds = Texts.at(i).SfmlInstance->getLocalBounds();

			top = Utils::Min(top, textBounds.top);
			right = Utils::Max(right, textBounds.left + textBounds.width);
			bottom = Utils::Max(bottom, textBounds.top + textBounds.height);
			left = Utils::Min(left, textBounds.left);
		}

		//Convert from Sf to Sd bounding box
		CachedBoundingBox = Aabb(right, left, bottom, top, 0.f, 0.f);
	}

	return CachedBoundingBox;
}

void TextRenderComponent::Destroyed ()
{
	if (TextShader.IsValid())
	{
		TextShader->Destroy();
		TextShader = nullptr;
	}

	DeleteAllText();

	Super::Destroyed();
}

TextRenderComponent::STextData& TextRenderComponent::CreateTextInstance (const DString& textContent, const Font* font, uint32 fontSize)
{
	sf::Text* newLine = new sf::Text(textContent.ToSfmlString(), *(font->GetFontResource()), fontSize);
	newLine->setFillColor(FontColor);

	STextData newData;
	newData.SfmlInstance = newLine;
	newData.DrawOffset = sf::Vector2f(0.f, 0.f);
	Texts.push_back(newData);
	MarkAabbAsDirty();

	return Texts.at(Texts.size() - 1);
}

void TextRenderComponent::DeleteAllText ()
{
	for (size_t i = 0; i < Texts.size(); ++i)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			delete Texts.at(i).SfmlInstance;
		}
	}

	ContainerUtils::Empty(OUT Texts);
	MarkAabbAsDirty();
}

void TextRenderComponent::DeleteTextData (size_t instanceIdx)
{
	if (ContainerUtils::IsValidIndex(Texts, instanceIdx))
	{
		delete Texts.at(instanceIdx).SfmlInstance;
		Texts.erase(Texts.begin() + instanceIdx);
		MarkAabbAsDirty();
	}

}

void TextRenderComponent::RegisterOnShaderChangedCallback (SDFunction<void> newOnShaderChangedCallback)
{
#ifdef DEBUG_MODE
	for (size_t i = 0; i < OnShaderChangedCallbacks.size(); ++i)
	{
		if (OnShaderChangedCallbacks.at(i) == newOnShaderChangedCallback)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("%s is already registered to %s.  Having duplicate entries would cause unreliable callback unregistration.  This check is only checked in debug builds, and may produce different results in release builds."), newOnShaderChangedCallback, ToString());
			return;
		}
	}
#endif

	OnShaderChangedCallbacks.push_back(newOnShaderChangedCallback);
}

void TextRenderComponent::UnregisterOnShaderChangedCallback (SDFunction<void> oldOnShaderChangedCallback)
{
	for (size_t i = 0; i < OnShaderChangedCallbacks.size(); ++i)
	{
		if (OnShaderChangedCallbacks.at(i) == oldOnShaderChangedCallback)
		{
			OnShaderChangedCallbacks.erase(OnShaderChangedCallbacks.begin() + i);
			return;
		}
	}

	GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to unregister %s from %s since %s was not found in the callbacks vector."), oldOnShaderChangedCallback, ToString(), oldOnShaderChangedCallback);
}

void TextRenderComponent::SetTextShader (Shader* newTextShader)
{
	if (TextShader.IsValid())
	{
		TextShader->Destroy();
	}

	TextShader = newTextShader;
	RefreshTextShader();

	for (size_t i = 0; i < OnShaderChangedCallbacks.size(); ++i)
	{
		OnShaderChangedCallbacks.at(i).Execute();
	}
}

void TextRenderComponent::SetFontColor (sf::Color newFontColor)
{
	if (FontColor == newFontColor)
	{
		return;
	}

	FontColor = newFontColor;
	for (size_t i = 0; i < Texts.size(); ++i)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setFillColor(FontColor);
		}
	}
}

void TextRenderComponent::SetTextFont (const Font* newTextFont)
{
	if (TextFont == newTextFont)
	{
		return;
	}

	TextFont = newTextFont;
	for (size_t i = 0; i < Texts.size(); ++i)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setFont(*TextFont->GetFontResource());
		}
	}

	RefreshTextShader();
	MarkAabbAsDirty();
}

void TextRenderComponent::SetFontSize (Int newFontSize)
{
	if (FontSize == newFontSize)
	{
		return;
	}

	FontSize = newFontSize;
	for (size_t i = 0; i < Texts.size(); ++i)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setCharacterSize(FontSize.ToUnsignedInt32());
		}
	}

	RefreshTextShader();
	MarkAabbAsDirty();
}

void TextRenderComponent::MarkAabbAsDirty ()
{
	CachedBoundingBox = Aabb();
}

void TextRenderComponent::RefreshTextShader ()
{
	if (TextShader.IsValid() && TextFont.IsValid())
	{
		CHECK(TextShader->SFShader != nullptr)
		TextShader->SFShader->setUniform(SHADER_BASE_TEXTURE, TextFont->GetFontResource()->getTexture(FontSize.ToUnsignedInt32()));
		RenderState = TextShader->SFShader;
	}
	else
	{
		RenderState = sf::RenderStates::Default;
	}
}
SD_END