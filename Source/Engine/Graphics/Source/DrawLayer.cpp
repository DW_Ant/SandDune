/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DrawLayer.cpp
=====================================================================
*/

#include "DrawLayer.h"

IMPLEMENT_ABSTRACT_CLASS(SD::DrawLayer, SD::Object)
SD_BEGIN

void DrawLayer::InitProps ()
{
	Super::InitProps();

	DrawPriority = -1;
}

void DrawLayer::SetDrawPriority (Int newDrawPriority)
{
	const DrawLayer* defaultObject = dynamic_cast<const DrawLayer*>(GetDefaultObject());

	//Check if the draw priority was assigned already or not.
	if (defaultObject != nullptr && GetDrawPriority() != defaultObject->GetDrawPriority())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot reassign a DrawLayer's priority value after it has been established."));
		return;
	}

	DrawPriority = newDrawPriority;
}
SD_END