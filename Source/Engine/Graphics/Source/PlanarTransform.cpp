/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransform.cpp
=====================================================================
*/

#include "Camera.h"
#include "GraphicsEngineComponent.h"
#include "PlanarCamera.h"
#include "PlanarTransform.h"
#include "PlanarTransformUpdater.h"
#include "RenderTarget.h"

SD_BEGIN
PlanarTransform::PlanarTransform () : Transformation(),
	Position(0.00001f, 0.00001f), //Hack: Slightly nudge from the origin so that when objects initialize this transform to 0,0, it'll mark dirty. Can't mark for dirty in this constructor since CDOs also invoke transform constructors.
	Depth(0.f),
	Size(1.f, 1.f),
	EnableFractionScaling(true),
	RelativeTo(nullptr),
	AnchorLeftDist(-1.f),
	AnchorTopDist(-1.f),
	AnchorRightDist(-1.f),
	AnchorBottomDist(-1.f),
	bTransformDirty(false),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{

}

PlanarTransform::PlanarTransform (const Vector2& inPosition, const Vector2& inSize, Float inDepth, PlanarTransform* inRelativeTo) : Transformation(),
	Position(inPosition),
	Depth(inDepth),
	Size(inSize),
	EnableFractionScaling(true),
	AnchorLeftDist(-1.f),
	AnchorTopDist(-1.f),
	AnchorRightDist(-1.f),
	AnchorBottomDist(-1.f),
	bTransformDirty(false),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{
	SetRelativeTo(inRelativeTo);
}

PlanarTransform::PlanarTransform (const PlanarTransform& copyObj) : Transformation(copyObj),
	Position(copyObj.Position),
	Depth(copyObj.Depth),
	Size(copyObj.Size),
	EnableFractionScaling(copyObj.EnableFractionScaling),
	AnchorLeftDist(copyObj.AnchorLeftDist),
	AnchorTopDist(copyObj.AnchorTopDist),
	AnchorRightDist(copyObj.AnchorRightDist),
	AnchorBottomDist(copyObj.AnchorBottomDist),
	bTransformDirty(copyObj.bTransformDirty),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{
	SetRelativeTo(copyObj.RelativeTo);
}

PlanarTransform::~PlanarTransform ()
{
	if (IsTransformDirty())
	{
		GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
		if (localGraphicsEngine != nullptr && localGraphicsEngine->GetPlanarUpdater() != nullptr)
		{
			localGraphicsEngine->GetPlanarUpdater()->RemoveDirtyTransform(this);
		}
	}

	if (RelativeTo != nullptr)
	{
		//Detach from RelativeTo without calling SetRelativeTo (don't want this destroyed object to register to the Updater).
		ContainerUtils::RemoveItem(OUT RelativeTo->AttachedTransforms, this);
		RelativeTo = nullptr;
	}

	while (!ContainerUtils::IsEmpty(AttachedTransforms))
	{
		AttachedTransforms.at(0)->SetRelativeTo(nullptr); //Calling this will have them remove themselves from this transform's AttachedTransforms.
	}
}

bool PlanarTransform::IsWithinView (const RenderTarget* renderTarget, const Camera* camera) const
{
	const SScreenProjectionData& projectionData = GetProjectionData(renderTarget, camera);
	return true;

	sf::Vector2f finalSize = projectionData.GetFinalSize();
	Vector2 renderSize = renderTarget->GetSize();

	//TODO: Fix bug where this doesn't consider the SpriteComponent's Pivot and size scale. The problem is that the transform is unaware of what kind of RenderComponent is using it.
	//There is a many to one relationship between transform and render component.
	//The proper fix is to consolidate RenderComponents and Transforms as one. To resolve the Planar and Scene transform issue, the SpriteComponents can only be used for PlanarTransforms.
	//2D billboards will be used for SceneTransforms to treat SpriteComponents in a 3D environment.
	return (projectionData.Position.x + finalSize.x >= 0.f && projectionData.Position.y + finalSize.y >= 0.f && projectionData.Position.x <= renderSize.X && projectionData.Position.y <= renderSize.Y);
}

void PlanarTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	CHECK(target != nullptr)
	if (RelativeTo == nullptr)
	{
		RootRenderTarget = target;
		ExternalCamera = cam;

		if (RootRenderTarget->GetSize() != ParentAbsSize)
		{
			//Detected that the RenderTarget changed size, mark this dirty since it may need to recompute attributes based on the render target size.
			PlanarTransform* constHack = const_cast<PlanarTransform*>(this);
			constHack->MarkTransformDirty();
		}
	}

	Vector2 relPos(ReadCachedAbsPosition());
	outProjectionData.BaseSize = Vector2::SDtoSFML(ReadCachedAbsSize());
	outProjectionData.Scale = sf::Vector2f(1.f, 1.f);

	const PlanarCamera* planarCam = dynamic_cast<const PlanarCamera*>(cam);
	if (planarCam != nullptr)
	{
		Vector2 topLeft = planarCam->ReadCachedAbsPosition() - (planarCam->GetZoomedExtents() * 0.5f);
		relPos = (relPos - topLeft) * planarCam->GetZoom();

		outProjectionData.Scale *= planarCam->GetZoom().Value;
	}
	else if (cam != nullptr) //PlanarCamera cast failed, but a camera is specified.
	{
		//Give warning if there is a camera but it's not using the correct coordinate space
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("The camera associated with a PlanarTransform is not a PlanarCamera.  The transform will assume that the coordinates are in screen space."));
	}

	//Convert to SFML and assign its results to the out parameters.
	outProjectionData.Position = Vector2::SDtoSFML(relPos);
	outProjectionData.Rotation = 0.f;
}

void PlanarTransform::TransformAabb (Aabb& outAabb) const
{
	outAabb.Depth *= ReadCachedAbsSize().X;
	outAabb.Width *= ReadCachedAbsSize().Y;

	outAabb.Center.X += (ReadCachedAbsPosition().X * ReadCachedAbsSize().X);
	outAabb.Center.Y += (ReadCachedAbsPosition().Y * ReadCachedAbsSize().Y);
}

void PlanarTransform::ComputeAbsTransform ()
{
	//Ensure the parent transform is computed first
	if (RelativeTo != nullptr && RelativeTo->IsTransformDirty())
	{
		RelativeTo->ComputeAbsTransform();
	}

	if (!IsTransformDirty())
	{
		return;
	}

	//Fallback to 1,1 in case the root render target is not yet assigned. This may be true for Entities such as the PlanarCamera that has size set to 1.
	ParentAbsSize = Vector2(1.f, 1.f);
	if (RelativeTo != nullptr)
	{
		ParentAbsSize = RelativeTo->GetCachedAbsSize();
	}
	else if (RootRenderTarget.IsValid())
	{
		ParentAbsSize = RootRenderTarget->GetSize();
	}
	CachedPosition = (RelativeTo != nullptr) ? RelativeTo->GetCachedAbsPosition() : Vector2::ZERO_VECTOR;

	CachedPosition.X += (Float::Abs(Position.X) <= 1 && EnableFractionScaling) ? ParentAbsSize.X * Position.X : Position.X;
	CachedPosition.Y += (Float::Abs(Position.Y) <= 1 && EnableFractionScaling) ? ParentAbsSize.Y * Position.Y : Position.Y;

	CachedSize.X = (Float::Abs(Size.X) <= 1 && EnableFractionScaling) ? ParentAbsSize.X * Size.X : Size.X;
	CachedSize.Y = (Float::Abs(Size.Y) <= 1 && EnableFractionScaling) ? ParentAbsSize.Y * Size.Y : Size.Y;

	ApplyAnchors();

	bTransformDirty = false;

	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	if (localGraphicsEngine != nullptr && localGraphicsEngine->GetPlanarUpdater() != nullptr)
	{
		localGraphicsEngine->GetPlanarUpdater()->AddPendingCallbackTransform(this);
	}

	//Recursively update children transforms
	for (PlanarTransform* child : AttachedTransforms)
	{
		child->ComputeAbsTransform();
	}
}

void PlanarTransform::PostAbsTransformUpdate ()
{
	OnTransformChanged.Broadcast();
}

bool PlanarTransform::IsWithinBounds (const Vector2& point) const
{
	return (point.X >= CachedPosition.X && point.Y >= CachedPosition.Y &&
		point.X <= (CachedPosition.X + CachedSize.X) && point.Y <= CachedPosition.Y + CachedSize.Y);
}

void PlanarTransform::ApplyAnchors ()
{
	Vector2 parentPos = (RelativeTo != nullptr) ? RelativeTo->GetCachedAbsPosition() : Vector2::ZERO_VECTOR;

	Float absAnchorTop(AnchorTopDist);
	Float absAnchorRight(AnchorRightDist);
	Float absAnchorBottom(AnchorBottomDist);
	Float absAnchorLeft(AnchorLeftDist);
	if (EnableFractionScaling)
	{
		if (absAnchorTop >= 0.f && absAnchorTop <= 1.f)
		{
			absAnchorTop *= ParentAbsSize.Y;
		}

		if (absAnchorRight >= 0.f && absAnchorRight <= 1.f)
		{
			absAnchorRight *= ParentAbsSize.X;
		}

		if (absAnchorBottom >= 0.f && absAnchorBottom <= 1.f)
		{
			absAnchorBottom *= ParentAbsSize.Y;
		}

		if (absAnchorLeft >= 0.f && absAnchorLeft <= 1.f)
		{
			absAnchorLeft *= ParentAbsSize.X;
		}
	}

	//Calculate horizontal position
	if (AnchorLeftDist >= 0.f)
	{
		CachedPosition.X = parentPos.X + absAnchorLeft;
	}

	if (AnchorRightDist >= 0.f)
	{
		if (AnchorLeftDist >= 0.f)
		{
			//Need to adjust size instead of position since the left border is also locked in place.
			CachedSize.X = ParentAbsSize.X - (absAnchorLeft + absAnchorRight);
		}
		else
		{
			CachedPosition.X = parentPos.X + (ParentAbsSize.X - (CachedSize.X + absAnchorRight));
		}
	}

	//Calculate vertical position
	if (AnchorTopDist >= 0.f)
	{
		CachedPosition.Y = parentPos.Y + absAnchorTop;
	}

	if (AnchorBottomDist >= 0.f)
	{
		if (AnchorTopDist >= 0.f)
		{
			//Need to adjust size instead of position since the top border is also locked in place.
			CachedSize.Y = ParentAbsSize.Y - (absAnchorTop + absAnchorBottom);
		}
		else
		{
			CachedPosition.Y = parentPos.Y + (ParentAbsSize.Y - (CachedSize.Y + absAnchorBottom));
		}
	}
}

void PlanarTransform::CopyPlanarTransform (const PlanarTransform* copyFrom)
{
	SetPosition(copyFrom->Position);
	SetDepth(copyFrom->Depth);
	SetSize(copyFrom->Size);
	SetEnableFractionScaling(copyFrom->EnableFractionScaling);
	SetAnchorLeft(copyFrom->AnchorLeftDist);
	SetAnchorTop(copyFrom->AnchorTopDist);
	SetAnchorRight(copyFrom->AnchorRightDist);
	SetAnchorBottom(copyFrom->AnchorBottomDist);
}

const RenderTarget* PlanarTransform::FindRootRenderTarget () const
{
	for (const PlanarTransform* transform = this; transform != nullptr; transform = transform->GetRelativeTo())
	{
		if (transform->RootRenderTarget.IsValid())
		{
			return transform->RootRenderTarget.Get();
		}
	}

	return nullptr;
}

const Camera* PlanarTransform::FindExternalCamera () const
{
	for (const PlanarTransform* transform = this; transform != nullptr; transform = transform->GetRelativeTo())
	{
		if (transform->ExternalCamera.IsValid())
		{
			return transform->ExternalCamera.Get();
		}
	}

	return nullptr;
}

void PlanarTransform::SetPosition (const Vector2& newPosition)
{
	if (Position != newPosition)
	{
		Position = newPosition;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetPosition (Float x, Float y)
{
	if (Position.X != x || Position.Y != y)
	{
		Position.X = x;
		Position.Y = y;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetDepth (Float newDepth)
{
	Depth = newDepth;
}

void PlanarTransform::SetSize (const Vector2& newSize)
{
	if (Size != newSize)
	{
		Size = newSize;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetSize (Float width, Float height)
{
	if (Size.X != width || Size.Y != height)
	{
		Size.X = width;
		Size.Y = height;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetEnableFractionScaling (bool newEnableFractionScaling)
{
	if (EnableFractionScaling != newEnableFractionScaling)
	{
		EnableFractionScaling = newEnableFractionScaling;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetRelativeTo (PlanarTransform* newRelativeTo)
{
	CHECK(newRelativeTo != this) //Should not attach transform to itself
#if ENABLE_COMPLEX_CHECKING
	//Ensure this isn't setting a circular relativeTo
	const PlanarTransform* relativeTo = newRelativeTo;
	while (relativeTo != nullptr)
	{
		CHECK_INFO(relativeTo != this, "Circular relative transformation detected.  Cannot set a PlanarTransform object relative to itself.")
		relativeTo = relativeTo->GetRelativeTo();
	}
#endif

	if (RelativeTo == newRelativeTo)
	{
		return;
	}

	//Check if this transform needs to be added to the updater.
	//This transform is not registered to the updater if they're marked for dirty while its relativeTo is already registered.
	if ((RelativeTo != nullptr && RelativeTo->IsTransformDirty()) &&
		(newRelativeTo == nullptr || !newRelativeTo->IsTransformDirty())) //If the newRelativeTo is already dirty, no need to register since it would inevitably be computed anyways.
	{
		GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
		if (localGraphicsEngine != nullptr && localGraphicsEngine->GetPlanarUpdater() != nullptr)
		{
			localGraphicsEngine->GetPlanarUpdater()->AddDirtyTransform(this);
		}
	}
	
	//Detach from old parent
	if (RelativeTo != nullptr)
	{
		ContainerUtils::RemoveItem(OUT RelativeTo->AttachedTransforms, this);
	}

	RelativeTo = newRelativeTo;
	if (RelativeTo != nullptr)
	{
		RelativeTo->AttachedTransforms.push_back(this);
	}

	//This pointer should only be set on Root Transforms. If SetRelativeTo(nullptr) is called, the root transform will be set on next screen projection.
	RootRenderTarget = nullptr;
	ExternalCamera = nullptr;

	MarkTransformDirty();
}

void PlanarTransform::SetAnchorLeft (Float leftDist)
{
	if (AnchorLeftDist != leftDist)
	{
		AnchorLeftDist = leftDist;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetAnchorTop (Float topDist)
{
	if (AnchorTopDist != topDist)
	{
		AnchorTopDist = topDist;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetAnchorRight (Float rightDist)
{
	if (AnchorRightDist != rightDist)
	{
		AnchorRightDist = rightDist;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetAnchorBottom (Float bottomDist)
{
	if (AnchorBottomDist != bottomDist)
	{
		AnchorBottomDist = bottomDist;
		MarkTransformDirty();
	}
}

void PlanarTransform::SetRootRenderTarget (const RenderTarget* newRootRenderTarget)
{
	RootRenderTarget = newRootRenderTarget;
	MarkTransformDirty();
}

void PlanarTransform::MarkTransformDirty ()
{
	if (bTransformDirty)
	{
		return;
	}

	bTransformDirty = true;
	if (RelativeTo == nullptr || !RelativeTo->IsTransformDirty())
	{
		//This transform is the outermost transform that is now dirty. Send to the updater to request an abs transform update.
		GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
		if (localGraphicsEngine != nullptr && localGraphicsEngine->GetPlanarUpdater() != nullptr)
		{
			localGraphicsEngine->GetPlanarUpdater()->AddDirtyTransform(this);
		}
		//If there isn't a planar updater, then assume something else is going to call compute abs transform.
		//Note: It's also possible those conditions would fail since the static classes could be initializing at this point (which don't need transform updates).
	}

	//Propagate this flag to subcomponents
	for (PlanarTransform* attachedTransform : AttachedTransforms)
	{
		attachedTransform->MarkTransformDirty();
	}
}
SD_END