/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsGraphics.cpp
=====================================================================
*/


#include "GraphicsClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
Int OS_GetSystemDpi (const Window* windowHandle)
{
	if (windowHandle == nullptr)
	{
		return -1;
	}

	UINT winDpi = GetDpiForWindow(windowHandle->GetResource()->getSystemHandle());

	return Int(winDpi);
}

Rectangle OS_GetClientWindowPos (const Window* windowHandle)
{
	RECT clientWindow;
	if (!GetClientRect(windowHandle->GetResource()->getSystemHandle(), &clientWindow))
	{
		return Rectangle();
	}

	RECT window;
	if (!GetWindowRect(windowHandle->GetResource()->getSystemHandle(), &window))
	{
		return Rectangle();
	}

	Int borderThickness = ((window.right - window.left) - clientWindow.right) / 2;
	Int titlebarThickness = ((window.bottom - window.top) - clientWindow.bottom - borderThickness);

	//Convert to SD Rectangle
	Float width = ((window.right - window.left) - (borderThickness * 2)).ToFloat();
	Float height = ((window.bottom - window.top) - (borderThickness + titlebarThickness)).ToFloat();
	Vector2 center;
	center.X = (window.left + borderThickness).ToFloat() + (width * 0.5f);
	center.Y = (window.top + titlebarThickness).ToFloat() + (height * 0.5f);

	return Rectangle(width, height, center);
}
SD_END

#endif