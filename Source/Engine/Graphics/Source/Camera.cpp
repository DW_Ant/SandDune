/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Camera.cpp
=====================================================================
*/

#include "Camera.h"

IMPLEMENT_CLASS(SD::Camera, SD::Entity)
SD_BEGIN

void Camera::InitProps ()
{
	Super::InitProps();

	ViewMode = VM_Normal;
}

void Camera::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	outDirection = Rotator::ZERO_ROTATOR;
	outLocation = Vector3::ZERO_VECTOR;
}
SD_END