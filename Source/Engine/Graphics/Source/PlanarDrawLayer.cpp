/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarDrawLayer.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::PlanarDrawLayer, SD::DrawLayer)
SD_BEGIN

const Int PlanarDrawLayer::CANVAS_LAYER_PRIORITY = 1000;

void PlanarDrawLayer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, PlanarDrawLayer, HandleGarbageCollection, void));
	}
}

void PlanarDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	RenderComponents(renderTarget, cam);
}

void PlanarDrawLayer::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, PlanarDrawLayer, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

void PlanarDrawLayer::RegisterPlanarObject (PlanarTransform* newObj)
{
	if (dynamic_cast<Entity*>(newObj) == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to register planar object to the PlanarDrawLayer since it must be an Entity with RenderComponents."));
		return;
	}

#if ENABLE_COMPLEX_CHECKING
	//Make sure the component is registered at most once
	CHECK(ContainerUtils::FindInVector(EntitiesToDraw, newObj) == UINT_INDEX_NONE)
#endif

	EntitiesToDraw.push_back(newObj);
}

void PlanarDrawLayer::UnregisterPlanarObject (PlanarTransform* target)
{
	ContainerUtils::RemoveItem(OUT EntitiesToDraw, target);
}

void PlanarDrawLayer::RenderComponents (RenderTarget* renderTarget, Camera* cam)
{
	//Early out if there aren't any components to render (since this function is slow)
	if (ContainerUtils::IsEmpty(EntitiesToDraw))
	{
		return;
	}

	PurgeExpiredObjs();
	std::sort(EntitiesToDraw.begin(), EntitiesToDraw.end(), [](PlanarTransform* a, PlanarTransform* b)
	{
		return (a->GetDepth() < b->GetDepth());
	});

	for (size_t i = 0; i < EntitiesToDraw.size(); ++i)
	{
		EntitiesToDraw.at(i)->MarkProjectionDataDirty();
		EntitiesToDraw.at(i)->SetRootRenderTarget(renderTarget);

		if (Entity* obj = dynamic_cast<Entity*>(EntitiesToDraw.at(i)))
		{
			for (ComponentIterator iter(obj, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				if (PlanarTransform* transform = dynamic_cast<PlanarTransform*>(iter.GetSelectedComponent()))
				{
					transform->MarkProjectionDataDirty();
					continue;
				}

				if (RenderComponent* rendComp = dynamic_cast<RenderComponent*>(iter.GetSelectedComponent()))
				{
					if (rendComp->IsRelevant(renderTarget, cam))
					{
						rendComp->Render(renderTarget, cam);
					}
				}
			}
		}
	}
}

void PlanarDrawLayer::PurgeExpiredObjs ()
{
	//Remove empty/destroyed components from list
	size_t i = 0;
	while (i < EntitiesToDraw.size())
	{
		Entity* obj = dynamic_cast<Entity*>(EntitiesToDraw.at(i));
		if (obj == nullptr)
		{
			EntitiesToDraw.erase(EntitiesToDraw.begin() + i);
			continue;
		}

		if (!VALID_OBJECT(obj))
		{
			EntitiesToDraw.erase(EntitiesToDraw.begin() + i);
			continue;
		}

		++i;
	}
}

void PlanarDrawLayer::HandleGarbageCollection ()
{
	PurgeExpiredObjs();
}
SD_END