/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsUnitTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::GraphicsUnitTester, SD::UnitTester)
SD_BEGIN

bool GraphicsUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bPassed = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bPassed &= (TestColor(testFlags) && TestSfVector(testFlags) && TestAabb(testFlags) &&
			TestPlanarTransforms(testFlags) && TestSceneTransforms(testFlags) && TestTopDownCamera(testFlags));
	}

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		bPassed &= TestSceneTransformUpdater(testFlags);
	}

	if (bPassed && (testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Manual) > 0 && (testFlags & UTF_Asynchronous) > 0 && (testFlags & UTF_NeverFails) > 0)
	{
		bPassed &= (TestSolidColor(testFlags) && TestRenderTexture(testFlags));
	}

	return bPassed;
}

bool GraphicsUnitTester::TestColor (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Color"));
	SetTestCategory(testFlags, TXT("Constructors"));
	Color red(255, 0, 0, 255);
	Color blue(0, 0, 255, 255);
	Color blueCopy(blue);
	uint32 greenBits = (255 << 16);
	greenBits += 255; //Add alpha
	Color green(greenBits);
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		if (red == blue)
		{
			UnitTestError(testFlags, TXT("The color red (%s) should not be equal to blue (%s)."), red, blue);
			return false;
		}

		if (blue != blueCopy)
		{
			UnitTestError(testFlags, TXT("The blue color (%s) should be equal to its copy constructed blue (%s)."), blue, blueCopy);
			return false;
		}

		if (green.Source.r != 0 || green.Source.g != 255 || green.Source.b != 0 || green.Source.a != 255)
		{
			UnitTestError(testFlags, TXT("The green color constructed from this integer (%s) should have the RGBA channels at 0,255,0,255.  Instead it's %s"), DString::MakeString(greenBits), green);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Color white(255, 255, 255, 255);
		Color greyish(120, 131, 127, 200);
		Color black(0, 0, 0, 215);
		Color readWhite;
		Color readGreyish;
		Color readBlack;
		DataBuffer colorBuffer;

		colorBuffer << white;
		colorBuffer << greyish;
		colorBuffer << black;
		colorBuffer >> readWhite;
		colorBuffer >> readGreyish;
		colorBuffer >> readBlack;

		if (white != readWhite)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing white (%s) to data buffer, it read %s from that data buffer."), white, readWhite);
			return false;
		}

		if (greyish != readGreyish)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing greyish (%s) to data buffer, it read %s from that data buffer."), greyish, readGreyish);
			return false;
		}

		if (black != readBlack)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing black (%s) to data buffer, it read %s from that data buffer."), black, readBlack);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Color expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Color actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Color test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Color(24, 32, 78, 255);
		testStr = TXT("(R=24, G=32, B=78, A=255)");
		if (!testParsing())
		{
			return false;
		}

		expected = Color(78, 42, 55, 100);
		testStr = TXT("(B = 55, A = 100, R=78, G = 42)");
		if (!testParsing())
		{
			return false;
		}

		expected = Color(123, 58, 204, 175);
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utilities"));
	{
		Color white = sf::Color::White;
		Color black = sf::Color::Black;
		Color grey(127, 127, 127, 255);
		UnitTester::TestLog(testFlags, TXT("Testing color lerping."));
		Color lerpedColor = Color::Lerp(0.f, black, white);
		if (lerpedColor != black)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0 between %s and %s should have resulted in %s.  Instead it returned %s."), black, white, black, lerpedColor);
			return false;
		}

		lerpedColor = Color::Lerp(1.f, black, white);
		if (lerpedColor != white)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 1 between %s and %s should have resulted in %s.  Instead it returned %s."), black, white, white, lerpedColor);
			return false;
		}

		lerpedColor = Color::Lerp(0.5f, black, white);
		if (lerpedColor != grey)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0.5 between %s and %s should have resulted in %s.  Instead it returned %s."), black, white, grey, lerpedColor);
			return false;
		}

		Color expectedColor(191, 191, 191, 255);
		lerpedColor = Color::Lerp(0.75f, black, white);
		if (lerpedColor != expectedColor)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0.75 between %s and %s should have resulted in %s.  Instead it returned %s."), black, white, expectedColor, lerpedColor);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Color"));

	return true;
}

bool GraphicsUnitTester::TestSfVector (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("SFML Vector"));

	const std::function<DString(const sf::Vector2f&)> vecToString([](const sf::Vector2f& v)
	{
		return (TXT("(") + Float(v.x).ToString() + TXT(", ") + Float(v.y).ToString() + TXT(")"));
	});

	SetTestCategory(testFlags, TXT("Math"));
	{
		sf::Vector2f vecA(2.f, 4.f);
		sf::Vector2f vecB(-6.f, 8.f);

		sf::Vector2f result = vecA * vecB;
		sf::Vector2f expected(-12.f, 32.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Multiplying %s with %s should have resulted in %s. Instead it's %s."), vecToString(vecA), vecToString(vecB), vecToString(expected), vecToString(result));
			return false;
		}

		result *= vecB;
		expected = sf::Vector2f(72.f, 256.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Vector multiplication should have resulted in %s instead of %s."), vecToString(expected), vecToString(result));
			return false;
		}

		result /= vecB;
		expected = sf::Vector2f(-12.f, 32.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Vector division should have resulted in %s instead of %s."), vecToString(expected), vecToString(result));
			return false;
		}

		result = vecB / vecA;
		expected = sf::Vector2f(-3.f, 2.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Dividing %s with %s should have resulted in %s. Instead it's %s."), vecToString(vecB), vecToString(vecA), vecToString(expected), vecToString(result));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("SFML Vector"));
	return true;
}

bool GraphicsUnitTester::TestAabb (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Aabb"));

	SetTestCategory(testFlags, TXT("Construction"));
	{
		Aabb blank;
		Aabb init(8.f, 16.f, 24.f, Vector3(-2.f, -32.f, 6.f));
		Aabb borders(-1.f, -3.f, 4.f, 0.f, 32.f, 10.f);
		Aabb copyInit(init);
		Aabb copyBorders(borders);

		if (!blank.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The default constructor of an Aabb should be a blank bounding box."));
			return false;
		}

		if (init.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s should not be considered an empty bounding box."), init);
			return false;
		}

		Float expected = 24.f;
		if (init.Depth != expected || init.GetXLength() != init.Depth)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s depth should have been %s. Instead it's %s."), init, expected, init.Depth);
			return false;
		}

		expected = 8.f;
		if (init.Width != expected || init.GetYLength() != init.Width)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s width should have been %s. Instead it's %s."), init, expected, init.Width);
			return false;
		}

		expected = 16.f;
		if (init.Height != expected || init.GetZLength() != init.Height)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s height should have been %s. Instead it's %s."), init, expected, init.Height);
			return false;
		}

		Vector3 expectedCenter(-2.f, -32.f, 6.f);
		if (init.Center != expectedCenter)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s center should have been %s. Instead it's %s."), init, expectedCenter, init.Center);
			return false;
		}

		expected = 10.f;
		if (init.GetForward() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s forward border should have been %s. Instead it's %s."), init, expected, init.GetForward());
			return false;
		}

		expected = -14.f;
		if (init.GetBackward() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s backward border should have been %s. Instead it's %s."), init, expected, init.GetBackward());
			return false;
		}

		expected = -28.f;
		if (init.GetRight() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s right border should have been %s. Instead it's %s."), init, expected, init.GetRight());
			return false;
		}

		expected = -36.f;
		if (init.GetLeft() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s left border should have been %s. Instead it's %s."), init, expected, init.GetLeft());
			return false;
		}

		expected = 14.f;
		if (init.GetUp() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s upper border should have been %s. Instead it's %s."), init, expected, init.GetUp());
			return false;
		}

		expected = -2.f;
		if (init.GetDown() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized Aabb %s lower border should have been %s. Instead it's %s."), init, expected, init.GetDown());
			return false;
		}

		//Test the Aabb that is initialized by borders instead
		expected = 2.f;
		if (borders.Depth != expected || borders.Depth != borders.GetXLength())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s depth should have been %s. Instead it's %s."), borders, expected, borders.Depth);
			return false;
		}

		expected = 4.f;
		if (borders.Width != expected || borders.Width != borders.GetYLength())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s width should have been %s. Instead it's %s."), borders, expected, borders.Width);
			return false;
		}

		expected = 22.f;
		if (borders.Height != expected || borders.Height != borders.GetZLength())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s height should have been %s. Instead it's %s."), borders, expected, borders.Height);
			return false;
		}

		expectedCenter = Vector3(-2.f, 2.f, 21.f);
		if (borders.Center != expectedCenter)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s center should have been %s. Instead it's %s."), borders, expectedCenter, borders.Center);
			return false;
		}

		expected = -1.f;
		if (borders.GetForward() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s forward border should have been %s. Instead it's %s."), borders, expected, borders.GetForward());
			return false;
		}

		expected = -3.f;
		if (borders.GetBackward() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s backward border should have been %s. Instead it's %s."), borders, expected, borders.GetBackward());
			return false;
		}

		expected = 4.f;
		if (borders.GetRight() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s right border should have been %s. Instead it's %s."), borders, expected, borders.GetRight());
			return false;
		}

		expected = 0.f;
		if (borders.GetLeft() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s left border should have been %s. Instead it's %s."), borders, expected, borders.GetLeft());
			return false;
		}

		expected = 32.f;
		if (borders.GetUp() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s upper border should have been %s. Instead it's %s."), borders, expected, borders.GetUp());
			return false;
		}

		expected = 10.f;
		if (borders.GetDown() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The initialized by borders Aabb %s lower border should have been %s. Instead it's %s."), borders, expected, borders.GetDown());
			return false;
		}

		if (init != copyInit)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The copy constructor from %s should be equal. Instead it's %s."), init, copyInit);
			return false;
		}

		if (borders != copyBorders)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The copy constructor from %s should be equal. Instead it's %s."), borders, copyBorders);
			return false;
		}

		//Test Aabb created from corners
		Aabb corners(Vector3(25.f, -30.f, 0.5f), Vector3(20.f, 40.f, -1.f));
		expected = 5.f;
		if (corners.Depth != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The constructor created from two points should have created an Aabb with a depth of %s. Instead the depth is %s."), expected, corners.Depth);
			return false;
		}

		expected = 70.f;
		if (corners.Width != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The constructor created from two points should have created an Aabb with a width of %s. Instead the width is %s."), expected, corners.Width);
			return false;
		}

		expected = 1.5f;
		if (corners.Height != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The constructor created from two points should have created an Aabb with a height of %s. Instead the height is %s."), expected, corners.Height);
			return false;
		}

		expectedCenter = Vector3(22.5f, 5.f, -0.25f);
		if (corners.Center != expectedCenter)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The constructor created from two points should have created an Aabb with a center of %s. Instead the center is %s."), expectedCenter, corners.Center);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Serialization"));
	{
		Aabb originalAabb(55.f, 42.f, 92.f, Vector3(-2912.2f, 0.4f, 928.25f));
		DataBuffer buffer;
		Aabb writtenAabb;

		buffer << originalAabb;
		buffer >> writtenAabb;
		if (originalAabb != writtenAabb)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The original Aabb %s does not equal to a copy of it after serializing and deserializing its data. Instead it's %s."), originalAabb, writtenAabb);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Parsing"));
	{
		Aabb expected;
		DString testStr;
		std::function<bool()> testParsing([&]()
		{
			Aabb actual;
			actual.ParseString(testStr);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Aabb test failed. Parsing the string \"%s\" should have resulted in %s. Instead it generated %s."), testStr, expected, actual);
				return false;
			}

			return true;
		});

		expected = Aabb(4.f, 8.f, 12.f, Vector3(6.f, 15.f, 20.f));
		testStr = TXT("(Width = 4, Height = 8, Depth = 12, Center = (X = 6, Y = 15, Z = 20))");
		if (!testParsing())
		{
			return false;
		}

		expected = Aabb(2.45f, 15.15f, 32.98f, Vector3(18.83f, 25.602f, 10.08f));
		testStr = TXT("(Depth=32.98, Center=(Z=10.08, X=18.8300, Y=25.602), Height=15.15, Width=2.45)");
		if (!testParsing())
		{
			return false;
		}

		expected = Aabb(12.24f, 30.08f, 10.f, Vector3(89.f, -115.26f, 70.5f));
		testStr = TXT("Width = 12.24, Height = 30.08, Depth = 10, Center=(X = 89, Y     =  -115.26, Z = 70.5))");
		if (!testParsing())
		{
			return false;
		}

		expected = Aabb(53.4f, 23.3f, 80.22f, Vector3(-501.4f, -24.4f, 40.2f));
		testStr = expected.ToString();
		if (!testParsing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Basic Methods"));
	{
		Aabb testAabb(2.f, 6.f, 14.f, Vector3(3.f, 11.f, 25.f));

		Float expected = 248.f;
		if (testAabb.GetSurfaceArea() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The surface area of %s should have been %s. Instead it's %s."), testAabb, expected, testAabb.GetSurfaceArea());
			return false;
		}

		expected = 168.f;
		if (testAabb.GetVolume() != expected)
		{
			UnitTestError(testFlags, TXT("Aabb test failed. The volume of %s should have been %s. Instead it's %s."), testAabb, expected, testAabb.GetVolume());
			return false;
		}

		if (testAabb.IsCubed())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. %s should not have been considered a cube."), testAabb);
			return false;
		}

		Aabb cube(4.f, 4.f, 4.f, Vector3(23.f, -82.f, 0.2f));
		if (!cube.IsCubed())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. %s should have been considered a cube."), cube);
			return false;
		}

		if (!cube.Is3d() || !testAabb.Is3d())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. %s and %s should be considered 3 dimensional."), cube, testAabb);
			return false;
		}

		Aabb a(cube);
		Aabb b(cube);
		Aabb c(cube);
		a.Depth = 0.f;
		b.Width = 0.f;
		c.Height = 0.f;
		if (a.Is3d() || b.Is3d() || c.Is3d())
		{
			UnitTestError(testFlags, TXT("Aabb test failed. %s, %s, and %s should not be considered 3 dimensional rectangles since one of their dimensions have converged to 0."), a, b, c);
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Generate Aabb Over Points"));
	{
		std::function<bool(const std::vector<Vector3>&, const Aabb&)> testGenerateAabb([&](const std::vector<Vector3>& points, const Aabb& expected)
		{
			Aabb generatedAabb = Aabb::GenerateEncompassingAabb(points);
			if (generatedAabb != expected)
			{
				UnitTestError(testFlags, TXT("Aabb test failed. The generated Aabb %s from a series of points does not match the expected Aabb %s."), generatedAabb, expected);
				return false;
			}

			return true;
		});

		std::vector<Vector3> points;
		Aabb expected;

		points.push_back(Vector3::ZERO_VECTOR);
		Float forward = 0.f;
		Float backward = 0.f;
		Float right = 0.f;
		Float left = 0.f;
		Float up = 0.f;
		Float down = 0.f;
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(10.f, 5.f, 0.f));
		forward = 10.f;
		right = 5.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(5.f, 2.f, 0.f));
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(5.f, -4.f, 0.f));
		left = -4.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(-100.f, 13.f, 0.f));
		backward = -100.f;
		right = 13.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(4.f, 6.f, 8.f));
		up = 8.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(-48.f, -15.f, 2.f));
		left = -15.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(22.f, 2.5f, -20.f));
		forward = 22.f;
		down = -20.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(8.f, -8.f, 4.f));
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}

		points.push_back(Vector3(70.f, 40.f, -35.f));
		forward = 70.f;
		right = 40.f;
		down = -35.f;
		expected = Aabb(forward, backward, right, left, up, down);
		if (!testGenerateAabb(points, expected))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlaps"));
	{
		std::function<bool(const Aabb&, const Aabb&, bool)> testOverlaps([&](const Aabb& a, const Aabb& b, bool expectsOverlap)
		{
			bool overlaps = a.Overlaps(b);
			if (overlaps != expectsOverlap)
			{
				if (expectsOverlap)
				{
					UnitTestError(testFlags, TXT("Aabb test failed. %s should be considered overlapping with %s, but it is not."), a, b);
				}
				else
				{
					UnitTestError(testFlags, TXT("Aabb test failed. %s should not be considered overlapping with %s, but it is."), a, b);
				}
			}

			return (overlaps == expectsOverlap);
		});

		Aabb a;
		Aabb b;

		a.Width = 10.f;
		a.Height = 10.f;
		a.Depth = 10.f;
		a.Center = Vector3::ZERO_VECTOR;

		b = a;
		b.Center = Vector3(5.f, 0.f, 0.f);

		//Aabb should always overlap with itself if it's 3D.
		if (!testOverlaps(a, a, true))
		{
			return false;
		}

		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(-5.f, 0.f, 0.f);
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(0.f, 5.f, 0.f);
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(0.f, -5.f, 0.f);
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(0.f, 0.f, 5.f);
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(0.f, 0.f, -5.f);
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center = Vector3(15.f, 0.f, 0.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center = Vector3(-15.f, 0.f, 0.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center = Vector3(0.f, 15.f, 0.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center = Vector3(0.f, -15.f, 0.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center = Vector3(0.f, 0.f, 15.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center = Vector3(0.f, 0.f, -15.f);
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		a.Width = 5.f;
		a.Height = 10.f;
		a.Depth = 15.f;
		a.Center = Vector3(10.f, 100.f, 1000.f);

		b.Width = 500.f;
		b.Height = 1000.f;
		b.Depth = 1500.f;
		b.Center = a.Center;

		//Test Depth overlap
		b.Center.X = -1005.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center.X = -50.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.X = 717.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.X = 770.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		//Test Width overlap
		b.Center.X = a.Center.X;
		b.Center.Y = -160.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center.Y = -140.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.Y = 352.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.Y = 353.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		//Test Height overlap
		b.Center.Y = a.Center.Y;
		b.Center.Z = 494.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}

		b.Center.Z = 496.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.Z = 1504.f;
		if (!testOverlaps(a, b, true))
		{
			return false;
		}

		b.Center.Z = 1506.f;
		if (!testOverlaps(a, b, false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlapping Aabb"));
	{
		std::function<bool(const Aabb&, const Aabb&, const Aabb&)> testOverlappingAabb([&](const Aabb& a, const Aabb& b, const Aabb& expected)
		{
			Aabb actual = a.GetOverlappingAabb(b);
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Aabb test failed. The overlapping Aabb between %s and %s should have been %s. Instead it's %s."), a, b, expected, actual);
			}

			return (actual == expected);
		});

		Aabb a;
		Aabb b;
		Aabb expected;

		a.Width = 10.f;
		a.Height = 10.f;
		a.Depth = 10.f;
		a.Center = Vector3::ZERO_VECTOR;

		b.Width = 10.f;
		b.Height = 10.f;
		b.Depth = 10.f;
		b.Center = Vector3(5.f, 5.f, 5.f);

		expected.Width = 5.f;
		expected.Height = 5.f;
		expected.Depth = 5.f;
		expected.Center = Vector3(2.5f, 2.5f, 2.5f);
		if (!testOverlappingAabb(a, b, expected))
		{
			return false;
		}

		a.Width = 10.f;
		a.Height = 20.f;
		a.Depth = 30.f;
		a.Center = Vector3(4.f, -8.f, 16.f);

		b.Width = 30.f;
		b.Height = 20.f;
		b.Depth = 10.f;
		b.Center = Vector3(6.f, -12.f, 25.f);

		expected.Width = 10.f;
		expected.Height = 11.f;
		expected.Depth = 10.f;
		expected.Center = Vector3(6.f, -8.f, 20.5f);
		if (!testOverlappingAabb(a, b, expected))
		{
			return false;
		}

		a.Width = 10.f;
		a.Height = 12.f;
		a.Depth = 14.f;
		a.Center = Vector3(-100.f, 50.f, -25.f);

		b.Width = 18.f;
		b.Height = 20.f;
		b.Depth = 12.f;
		b.Center = Vector3(-92.f, 57.f, -34.f);

		expected.Width = 7.f;
		expected.Height = 7.f;
		expected.Depth = 5.f;
		expected.Center = Vector3(-95.5f, 51.5f, -27.5f);
		if (!testOverlappingAabb(a, b, expected))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	//CombineAabb
	SetTestCategory(testFlags, TXT("Combine Aabb"));
	{
		std::function<bool(const Aabb&, const Aabb&, const Aabb&)> testCombineAabb([&](const Aabb& a, const Aabb& b, const Aabb& expected)
		{
			Aabb result = a.CombineAabb(b);
			if (result != expected)
			{
				UnitTestError(testFlags, TXT("Aabb test failed. Combining %s and %s together should have resulted in %s. Instead it's %s."), a, b, expected, result);
			}

			return (result == expected);
		});

		Aabb a;
		Aabb b;
		Aabb expected;

		a.Width = 2.f;
		a.Height = 2.f;
		a.Depth = 2.f;
		a.Center = Vector3::ZERO_VECTOR;

		b = a;
		b.Center = Vector3(10.f, -20.f, 20.f);

		expected.Width = 22.f;
		expected.Height = 22.f;
		expected.Depth = 12.f;
		expected.Center = Vector3(5.f, -10.f, 10.f);
		if (!testCombineAabb(a, b, expected))
		{
			return false;
		}

		a.Width = 14.f;
		a.Height = 18.f;
		a.Depth = 24.f;
		a.Center = Vector3(18.f, 24.f, 32.f);

		b.Width = 2.f;
		b.Height = 4.f;
		b.Depth = 8.f;
		b.Center = Vector3(17.f, 21.f, 35.f);

		expected = a;
		if (!testCombineAabb(a, b, expected))
		{
			return false;
		}

		b.Width = 45.f;
		b.Height = 6.f;
		b.Depth = 12.f;
		b.Center = Vector3(30.f, 15.f, 31.f);

		expected.Width = 45.f;
		expected.Height = 18.f;
		expected.Depth = 30.f;
		expected.Center = Vector3(21.f, 15.f, 32.f);
		if (!testCombineAabb(a, b, expected))
		{
			return false;
		}

		//Test case where the Aabb do not overlap
		a.Width = 100.f;
		a.Height = 25.f;
		a.Depth = 50.f;
		a.Center = Vector3(1000.f, 1500.f, 2000.f);

		b.Width = 200.f;
		b.Height = 400.f;
		b.Depth = 800.f;
		b.Center = Vector3(-2500.f, -3300.f, -60500.f);

		expected.Width = 4950.f;
		expected.Height = 62712.5f;
		expected.Depth = 3925.f;
		expected.Center = Vector3(-937.5f, -925.f, -29343.75f);
		if (!testCombineAabb(a, b, expected))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		std::function<bool(const Aabb&, const Vector3&, bool)> testEncompassesPoint([&](const Aabb& box, const Vector3& testPoint, bool expectsEncompassesPoint)
		{
			bool actual = box.EncompassesPoint(testPoint);
			if (actual != expectsEncompassesPoint)
			{
				if (expectsEncompassesPoint)
				{
					UnitTestError(testFlags, TXT("Aabb test failed. %s should be encompassing %s. But it is not."), box, testPoint);
				}
				else
				{
					UnitTestError(testFlags, TXT("Aabb test failed. %s should NOT be encompassing %s. But is is."), box, testPoint);
				}
			}

			return (actual == expectsEncompassesPoint);
		});

		Aabb box(10.f, 20.f, 100.f, Vector3(-50.f, 100.f, 5000.f));
		Vector3 point(box.Center);

		//Test Depth
		point.X = -101.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}

		point.X = -99.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.X = -1.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.X = 1.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}

		//Test Width
		point = box.Center;
		point.Y = 94.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}

		point.Y = 96.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.Y = 104.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.Y = 106.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}

		//Test Height
		point = box.Center;
		point.Z = 4989.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}

		point.Z = 4991.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.Z = 5009.f;
		if (!testEncompassesPoint(box, point, true))
		{
			return false;
		}

		point.Z = 5011.f;
		if (!testEncompassesPoint(box, point, false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Aabb"));
	return true;
}

bool GraphicsUnitTester::TestPlanarTransforms (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Planar Transforms"));

	std::vector<Entity*> entitiesToDestroy;
	std::function<void()> clearEntities = [&entitiesToDestroy]
	{
		for (UINT_TYPE i = 0; i < entitiesToDestroy.size(); ++i)
		{
			entitiesToDestroy.at(i)->Destroy();
		}
		ContainerUtils::Empty(entitiesToDestroy);
	};

	TestLog(testFlags, TXT("Setting up a few PlanarTransformed Entities to be relative to each other."));

	//Need to setup a render target since PlanarTransform's root size may depend on the RenderTarget it's drawn to.
	EmptyRenderTarget* blankRender = EmptyRenderTarget::CreateObject();
	Vector2 rootSize(100.f, 100.f);
	blankRender->SetSize(rootSize);

	PlanarTransformComponent* rootEntity = PlanarTransformComponent::CreateObject();
	PlanarTransformComponent* childEntity = PlanarTransformComponent::CreateObject();
	PlanarTransformComponent* leafEntity = PlanarTransformComponent::CreateObject();
	childEntity->SetRelativeTo(rootEntity);
	leafEntity->SetRelativeTo(childEntity);

	{
		//The RootEntity's size depends on a viewport size. Call CalculateScreenSpaceTransform to pair up the root with a RenderTarget.
		//Normally this will automatically be called when it actually renders the Entity to a viewport. But for an isolated test, we force it here.
		Transformation::SScreenProjectionData unusedProjectionData;
		rootEntity->CalculateScreenSpaceTransform(blankRender, nullptr, OUT unusedProjectionData);
	}

	entitiesToDestroy.push_back(blankRender);
	entitiesToDestroy.push_back(rootEntity);
	entitiesToDestroy.push_back(childEntity);
	entitiesToDestroy.push_back(leafEntity);

	std::function<void()> computeAbsCoordinates = [&]
	{
		rootEntity->ComputeAbsTransform();
		childEntity->ComputeAbsTransform();
		leafEntity->ComputeAbsTransform();
	};

	SetTestCategory(testFlags, TXT("Unit Coordinates"));
	{
		TestLog(testFlags, TXT("Testing Unit Coordinates where leafEntity is relative to childEntity.  ChildEntity is relative to rootEntity."));
		rootEntity->SetPosition(Vector2(16.f, 16.f));
		rootEntity->SetSize(Vector2(64.f, 64.f));

		childEntity->SetPosition(Vector2(16.f, 32.f));
		childEntity->SetSize(Vector2(36.f, 48.f));

		leafEntity->SetPosition(Vector2(8.f, 12.f));
		leafEntity->SetSize(Vector2(16.f, 24.f));

		computeAbsCoordinates();

		//Test Position
		Vector2 expected(16.f, 16.f);
		if (rootEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the root Entity at position %s should have been %s.  Instead it's %s."), rootEntity->ReadPosition(), expected, rootEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		expected += Vector2(16.f, 32.f);
		if (childEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the child Entity at position %s should have been %s.  Instead it's %s."), childEntity->ReadPosition(), expected, childEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		expected += Vector2(8.f, 12.f);
		if (leafEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the leaf Entity at position %s should have been %s.  Instead it's %s."), leafEntity->ReadPosition(), expected, leafEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		//Test Size
		expected = Vector2(64.f, 64.f);
		if (rootEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the root Entity with size %s should have been %s.  Instead it's %s."), rootEntity->ReadSize(), expected, rootEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}

		expected = Vector2(36.f, 48.f);
		if (childEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the child Entity with size %s should have been %s.  Instead it's %s."), childEntity->ReadSize(), expected, childEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}

		expected = Vector2(16.f, 24.f);
		if (leafEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the leaf Entity with size %s should have been %s.  Instead it's %s."), leafEntity->ReadSize(), expected, leafEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("IsWithinBounds"));
	{
		rootEntity->SetPosition(Vector2(10.f, 20.f));
		rootEntity->SetSize(Vector2(80.f, 60.f));

		childEntity->SetPosition(Vector2(20.f, 5.f));
		childEntity->SetSize(Vector2(40.f, 50.f));
		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*tester*/, const Vector2& /* testPoint */, bool /* isPointInBounds */)> testWithinBounds = [&](PlanarTransformComponent* tester, const Vector2& testPoint, bool isPointInBounds)
		{
			if (tester->IsWithinBounds(testPoint) != isPointInBounds)
			{
				if (isPointInBounds)
				{
					UnitTestError(testFlags, TXT("Planar Transform test failed.  The IsWithinBounds test should have returned true when testing point %s on an Entity at abs position %s and abs size %s.  IsWithinBounds returned false instead."), testPoint, tester->ReadCachedAbsPosition(), tester->ReadCachedAbsSize());
				}
				else
				{
					UnitTestError(testFlags, TXT("Planar Transform test failed.  The IsWithinBounds test should have returned false when testing point %s on an Entity at abs position %s and abs size %s.  IsWithinBounds returned true instead."), testPoint, tester->ReadCachedAbsPosition(), tester->ReadCachedAbsSize());
				}

				clearEntities();
				return false;
			}

			return true;
		};

		//Test RootEntity
		if (!testWithinBounds(rootEntity, Vector2(2.f, 4.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(30.f, 5.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(95.f, 32.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(32.f, 95.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(4.f, 32.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(24.f, 24.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(82.f, 24.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(82.f, 75.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(24.f, 75.f), true))
		{
			return false;
		}

		//Test LeafEntity
		//AbsPosition(30, 25)
		//AbsSize(40, 50)
		//Valid bounds rectangle in abs coordinates (Left: 30, Top: 25, Right: 70, Bottom: 75)
		if (!testWithinBounds(childEntity, Vector2(15.f, 24.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(16.f, 48.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(48.f, 24.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(84.f, 50.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(48.f, 80.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(32.f, 32.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(64.f, 32.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(64.f, 64.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(32.f, 64.f), true))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Relative Coordinates"));
	{
		TestLog(testFlags, TXT("Testing Relative Coordinates where leafEntity is relative to childEntity.  ChildEntity is relative to rootEntity."));
		rootEntity->SetPosition(Vector2(0.f, 0.f));
		rootEntity->SetSize(Vector2(1.f, 1.f));

		childEntity->SetPosition(Vector2(0.1f, 0.2f));
		childEntity->SetSize(Vector2(0.8f, 0.6f));

		leafEntity->SetPosition(Vector2(0.5f, 0.5f));
		leafEntity->SetSize(Vector2(0.5f, 0.5f));
		computeAbsCoordinates();

		//Test Position
		Vector2 expected(0.f, 0.f);
		if (rootEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the root entity with relative Position set to %s should have been %s.  It's %s instead."), rootEntity->ReadPosition(), expected, rootEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		expected = Vector2(10.f, 20.f);
		if (childEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the child entity with relative Position set to %s should have been %s.  It's %s instead."), childEntity->ReadPosition(), expected, childEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		expected = Vector2(50.f, 50.f);
		if (leafEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the leaf entity with relative Position set to %s should have been %s.  It's %s instead."), leafEntity->ReadPosition(), expected, leafEntity->ReadCachedAbsPosition());
			clearEntities();
			return false;
		}

		//Test Size
		expected = Vector2(100.f, 100.f);

		//tolerance added due to accumulated float precision errors with CachedAbsSize
		float vectorLengthTolerance = 0.00001f;
		if ((rootEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the root entity with relative Size set to %s should have been %s.  It's %s instead."), rootEntity->ReadSize(), expected, rootEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}

		expected = Vector2(80.f, 60.f);
		if ((childEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the child entity with relative Size set to %s should have been %s.  It's %s instead."), childEntity->ReadSize(), expected, childEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}

		expected = Vector2(40.f, 30.f);
		if ((leafEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the leaf entity with relative Size set to %s should have been %s.  It's %s instead."), leafEntity->ReadSize(), expected, leafEntity->ReadCachedAbsSize());
			clearEntities();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Anchoring"));
	{
		std::function<void()> resetAttributes = [&]
		{
			rootEntity->SetAnchorLeft(-1.f);
			rootEntity->SetAnchorTop(-1.f);
			rootEntity->SetAnchorRight(-1.f);
			rootEntity->SetAnchorBottom(-1.f);

			childEntity->SetAnchorLeft(-1.f);
			childEntity->SetAnchorTop(-1.f);
			childEntity->SetAnchorRight(-1.f);
			childEntity->SetAnchorBottom(-1.f);

			leafEntity->SetAnchorLeft(-1.f);
			leafEntity->SetAnchorTop(-1.f);
			leafEntity->SetAnchorRight(-1.f);
			leafEntity->SetAnchorBottom(-1.f);

			rootEntity->SetSize(Vector2(90.f, 85.f));
			rootEntity->SetPosition(Vector2(5.f, 10.f));

			childEntity->SetSize(Vector2(60.f, 64.f));
			childEntity->SetPosition(Vector2(5.f, 10.f));

			leafEntity->SetSize(Vector2(40.f, 48.f));
			leafEntity->SetPosition(Vector2(5.f, 10.f));
		};

		TestLog(testFlags, TXT("Testing Left Anchor"));
		resetAttributes();
		rootEntity->SetAnchorLeft(8.f);
		childEntity->SetAnchorLeft(32.f);
		leafEntity->SetAnchorLeft(16.f);
		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*entity*/, const Vector2& /*expectedPos*/)> testAnchoring = [&](PlanarTransformComponent* entity, const Vector2& expectedPos)
		{
			if (entity->ReadCachedAbsPosition() != expectedPos)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of an anchored Entity should have been %s.  Instead it's %s."), expectedPos, entity->ReadCachedAbsPosition());
				clearEntities();
				return false;
			}

			return true;
		};

		Vector2 expected(8.f, 10.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(40.f, 20.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(56.f, 30.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Top Anchor"));
		resetAttributes();
		rootEntity->SetAnchorTop(12.f);
		childEntity->SetAnchorTop(16.f);
		leafEntity->SetAnchorTop(8.f);
		computeAbsCoordinates();

		expected = Vector2(5.f, 12.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(10.f, 28.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(15.f, 36.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Right Anchor"));
		resetAttributes();
		rootEntity->SetAnchorRight(12.f);
		childEntity->SetAnchorRight(16.f);
		leafEntity->SetAnchorRight(8.f);
		computeAbsCoordinates();

		expected = Vector2(-2.f, 10.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(12.f, 20.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(24.f, 30.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Bottom Anchor"));
		resetAttributes();
		rootEntity->SetAnchorBottom(12.f);
		childEntity->SetAnchorBottom(16.f);
		leafEntity->SetAnchorBottom(8.f);
		computeAbsCoordinates();

		expected = Vector2(5.f, 3.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(10.f, 8.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(15.f, 16.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Multi Anchor"));
		resetAttributes();
		rootEntity->SetAnchorLeft(2.f);
		rootEntity->SetAnchorTop(4.f);
		rootEntity->SetAnchorRight(6.f);
		rootEntity->SetAnchorBottom(8.f);

		childEntity->SetAnchorLeft(3.f);
		childEntity->SetAnchorTop(7.f);
		childEntity->SetAnchorRight(13.f);
		childEntity->SetAnchorBottom(17.f);

		leafEntity->SetAnchorLeft(12.f);
		leafEntity->SetAnchorTop(15.f);
		leafEntity->SetAnchorRight(18.f);
		leafEntity->SetAnchorBottom(21.f);

		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*entity*/, const Vector2& /*expectedPos*/, const Vector2& /*expectedSize*/)> testMultiAnchor = [&](PlanarTransformComponent* entity, const Vector2& expectedPos, const Vector2& expectedSize)
		{
			if (entity->ReadCachedAbsPosition() != expectedPos)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute position of a multi-anchored Entity should have been %s.  It's actually %s instead."), expectedPos, entity->ReadCachedAbsPosition());
				clearEntities();
				return false;
			}

			if (entity->ReadCachedAbsSize() != expectedSize)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size of a multi-anchored Entity should have been %s.  It's actually %s instead."), expectedSize, entity->ReadCachedAbsSize());
				clearEntities();
				return false;
			}

			return true;
		};

		Vector2 expectedPos(2, 4);
		Vector2 expectedSize(92.f, 88.f);
		if (!testMultiAnchor(rootEntity, expectedPos, expectedSize))
		{
			return false;
		}

		expectedPos = Vector2(5.f, 11.f);
		expectedSize = Vector2(76.f, 64.f);
		if (!testMultiAnchor(childEntity, expectedPos, expectedSize))
		{
			return false;
		}

		expectedPos = Vector2(17.f, 26.f);
		expectedSize = Vector2(46.f, 28.f);
		if (!testMultiAnchor(leafEntity, expectedPos, expectedSize))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Normalized Anchoring"));
	{
		rootEntity->SetSize(Vector2(100.f, 100.f));
		rootEntity->SetPosition(Vector2::ZERO_VECTOR);
		rootEntity->SetAnchorTop(-1.f);
		rootEntity->SetAnchorRight(-1.f);
		rootEntity->SetAnchorBottom(-1.f);
		rootEntity->SetAnchorLeft(-1.f);

		childEntity->SetPosition(Vector2::ZERO_VECTOR);
		childEntity->SetSize(1.f, 1.f);
		childEntity->SetAnchorTop(-1.f);
		childEntity->SetAnchorRight(-1.f);
		childEntity->SetAnchorBottom(-1.f);
		childEntity->SetAnchorLeft(-1.f);
		Vector2 expectedPosition(Vector2::ZERO_VECTOR);
		Vector2 expectedSize(rootEntity->ReadSize());

		std::function<bool()> testAnchors([&]()
		{
			computeAbsCoordinates();
			if (childEntity->ReadCachedAbsPosition() != expectedPosition)
			{
				UnitTestError(testFlags, TXT("Planar test failed. The actual position %s does not match the expected position %s."), childEntity->ReadCachedAbsPosition(), expectedPosition);
				clearEntities();
				return false;
			}

			if (childEntity->ReadCachedAbsSize() != expectedSize)
			{
				UnitTestError(testFlags, TXT("Planar test failed. The actual size %s does not match the expected size %s"), childEntity->ReadCachedAbsSize(), expectedSize);
				clearEntities();
				return false;
			}

			return true;
		});

		childEntity->SetAnchorLeft(0.25f);
		expectedPosition.X = 25.f;
		if (!testAnchors())
		{
			return false;
		}

		childEntity->SetAnchorRight(0.25f);
		expectedSize.X = 50.f;
		if (!testAnchors())
		{
			return false;
		}

		childEntity->SetAnchorTop(0.1f);
		expectedPosition.Y = 10.f;
		if (!testAnchors())
		{
			return false;
		}

		childEntity->SetAnchorBottom(0.8f);
		expectedSize.Y = 10.f;
		if (!testAnchors())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	clearEntities();
	ExecuteSuccessSequence(testFlags, TXT("Planar Transforms"));
	return true;
}

bool GraphicsUnitTester::TestSceneTransforms (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Scene Transforms"));

	TestLog(testFlags, TXT("Spawning 3 Entities that implements a SceneTransform.  1 Entity is the root, and the other 2 Entities are relative to the root Entity."));
	std::vector<SceneEntity*> entitiesToDestroy;
	std::function<void()> clearEntities = [&entitiesToDestroy]
	{
		for (UINT_TYPE i = 0; i < entitiesToDestroy.size(); ++i)
		{
			entitiesToDestroy.at(i)->Destroy();
		}
		ContainerUtils::Empty(entitiesToDestroy);
	};

	SceneEntity* rootEntity = SceneEntity::CreateObject();
	rootEntity->DebugName = TXT("Root Entity");
	entitiesToDestroy.push_back(rootEntity);

	//All Entities should initialize with transform matrices equal to their identity matrix.
	TransformMatrix transformMatrix = rootEntity->ToTransformMatrix();
	if (!transformMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Entity transform test failed.  The transform matrix should initialize to the identity matrix.  The Entity instead initialized with this matrix data %s"), transformMatrix);
		clearEntities();
		return false;
	}

	SceneEntity* subEntity1 = SceneEntity::CreateObject();
	SceneEntity* subEntity2 = SceneEntity::CreateObject();
	subEntity1->DebugName = TXT("Sub Entity 1");
	subEntity2->DebugName = TXT("Sub Entity 2");
	subEntity1->SetRelativeTo(rootEntity);
	subEntity2->SetRelativeTo(rootEntity);
	entitiesToDestroy.push_back(subEntity1);
	entitiesToDestroy.push_back(subEntity2);

	SetTestCategory(testFlags, TXT("Translation Tests"));
	{
		//At this point everything should still be at the origin.  Verify that.
		Vector3 expectedAbsPosition(0.f, 0.f, 0.f);
		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		subEntity2->CalculateAbsoluteTransform();
		Vector3 actualAbsPosition = rootEntity->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After spawning an entity that's not relative to anything, it's absolute translation wasn't %s.  Instead it's %s."), expectedAbsPosition, actualAbsPosition);
			clearEntities();
			return false;
		}

		actualAbsPosition = subEntity1->GetAbsTranslation();
		bool bPassed = (actualAbsPosition == expectedAbsPosition);
		actualAbsPosition = subEntity2->GetAbsTranslation();
		bPassed &= (actualAbsPosition == expectedAbsPosition);
		if (!bPassed)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After spawning an entity that's relative to %s, it's translation wasn't %s."), rootEntity->ToString(), expectedAbsPosition);
			clearEntities();
			return false;
		}

		TestLog(testFlags, TXT("Verified that all entities have their default position in absolute space are at the origin."));

		Vector3 deltaMove(5.f, 50.f, 0.f);
		rootEntity->EditTranslation() += deltaMove;
		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		actualAbsPosition = rootEntity->GetAbsTranslation();
		expectedAbsPosition = deltaMove;
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity by %s the root entity absolute translation should be %s.  Instead it's %s."), deltaMove, expectedAbsPosition, actualAbsPosition);
			clearEntities();
			return false;
		}

		actualAbsPosition = subEntity1->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity by %s and with the sub entity's transform having no translation modifications, the sub entity's absolute position should be %s.  Instead it's %s."), deltaMove, expectedAbsPosition, actualAbsPosition);
			clearEntities();
			return false;
		}

		deltaMove = Vector3(-10.f, 100.f, 30.f);
		subEntity1->EditTranslation() += deltaMove;
		expectedAbsPosition += deltaMove;
		subEntity1->CalculateAbsoluteTransform();
		actualAbsPosition = subEntity1->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the sub entity by %s, it's absolute position should have been %s.  Intead it's %s."), deltaMove, expectedAbsPosition, actualAbsPosition);
			clearEntities();
			return false;
		}

		//Spawn a chain of Entities relative to each other, and test their coordinates
		{
			SceneEntity* superSubEntityA = SceneEntity::CreateObject();
			SceneEntity* superSubEntityB = SceneEntity::CreateObject();
			SceneEntity* superSubEntityC = SceneEntity::CreateObject();
			SceneEntity* superSubEntityD = SceneEntity::CreateObject();

			std::vector<SceneEntity*> superSubEntities;
			superSubEntities.push_back(superSubEntityA);
			superSubEntities.push_back(superSubEntityB);
			superSubEntities.push_back(superSubEntityC);
			superSubEntities.push_back(superSubEntityD);
			std::function<void()> clearSubEntities = [&]
			{
				for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
				{
					superSubEntities.at(i)->Destroy();
				}

				ContainerUtils::Empty(superSubEntities);
			};

			superSubEntityA->SetRelativeTo(subEntity2);
			superSubEntityB->SetRelativeTo(superSubEntityA);
			superSubEntityC->SetRelativeTo(superSubEntityB);
			superSubEntityD->SetRelativeTo(superSubEntityC);

			std::vector<Vector3> expectedAbsCoordinates;
			std::vector<Vector3> deltaMoves;

			expectedAbsCoordinates.push_back(rootEntity->GetAbsTranslation()); //subEntity2

			Vector3 deltaMove(182.5f, 28.1f, -45.6f);
			deltaMoves.push_back(deltaMove);
			superSubEntityA->EditTranslation() += deltaMove;
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + deltaMove); //superSubEntityA

			deltaMove = Vector3(24.f, -57.f, 115.5f);
			deltaMoves.push_back(deltaMove);
			superSubEntityB->EditTranslation() += deltaMove;
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + deltaMove); //superSubEntityB

			//Add three movement actions for the C
			{
				Vector3 totalMove = Vector3::ZERO_VECTOR;
				deltaMove = Vector3(15.f, -64.5f, 119.15f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				deltaMove = Vector3(-32.7f, 82.5f, -120.5f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				deltaMove = Vector3(71.f, -48.2f, 44.44f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + totalMove); //superSubEntityC
			}

			//D did not move, so it's expected to have the same coordinates as C
			superSubEntityD->SetTranslation(Vector3::ZERO_VECTOR);
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1)); //superSubEntityD

			std::vector<Vector3> actualAbsCoordinates;
			subEntity2->CalculateAbsoluteTransform();
			actualAbsCoordinates.push_back(subEntity2->GetAbsTranslation());
			for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
			{
				superSubEntities.at(i)->CalculateAbsoluteTransform();
				actualAbsCoordinates.push_back(superSubEntities.at(i)->GetAbsTranslation());
			}
			CHECK(actualAbsCoordinates.size() == expectedAbsCoordinates.size())

			//Verify the sub entity coordinates
			for (UINT_TYPE i = 0; i < actualAbsCoordinates.size(); ++i)
			{
				actualAbsCoordinates.at(i).X.RoundInline(4);
				actualAbsCoordinates.at(i).Y.RoundInline(4);
				actualAbsCoordinates.at(i).Z.RoundInline(4);

				expectedAbsCoordinates.at(i).X.RoundInline(4);
				expectedAbsCoordinates.at(i).Y.RoundInline(4);
				expectedAbsCoordinates.at(i).Z.RoundInline(4);

				if (actualAbsCoordinates.at(i) != expectedAbsCoordinates.at(i))
				{
					UnitTestError(testFlags, TXT("Entity transform test failed.  There's a translation mismatch from expected %s compared to the actual %s at index %s."), expectedAbsCoordinates.at(i), actualAbsCoordinates.at(i), DString::MakeString(i));
					clearSubEntities();
					clearEntities();
					return false;
				}
			}

			TestLog(testFlags, TXT("Entity transform test instantiated 4 Entities relative to each other, and verified that their translation adjustments align with expected coordinates."));

			deltaMove = Vector3(100.f, 200.f, -150.f);
			rootEntity->EditTranslation() += deltaMove;
			for (UINT_TYPE i = 0; i < expectedAbsCoordinates.size(); ++i)
			{
				expectedAbsCoordinates.at(i) += deltaMove;
			}

			subEntity2->CalculateAbsoluteTransform();
			actualAbsCoordinates.at(0) = subEntity2->GetAbsTranslation();
			for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
			{
				superSubEntities.at(i)->CalculateAbsoluteTransform();
				actualAbsCoordinates.at(i + 1) = superSubEntities.at(i)->GetAbsTranslation();
			}

			//Verify sub entity coordinates
			for (UINT_TYPE i = 0; i < expectedAbsCoordinates.size(); ++i)
			{
				actualAbsCoordinates.at(i).X.RoundInline(4);
				actualAbsCoordinates.at(i).Y.RoundInline(4);
				actualAbsCoordinates.at(i).Z.RoundInline(4);

				expectedAbsCoordinates.at(i).X.RoundInline(4);
				expectedAbsCoordinates.at(i).Y.RoundInline(4);
				expectedAbsCoordinates.at(i).Z.RoundInline(4);

				if (actualAbsCoordinates.at(i) != expectedAbsCoordinates.at(i))
				{
					UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity, there's a translation mismatch from expected %s compared to the actual coordinates %s at index %s."), expectedAbsCoordinates.at(i), actualAbsCoordinates.at(i), DString::MakeString(i));
					clearSubEntities();
					clearEntities();
					return false;
				}
			}

			TestLog(testFlags, TXT("After moving the root entity, the sub entities did update their absolute coordinates as well."));
			clearSubEntities();
		}
	}
	CompleteTestCategory(testFlags);

	for (SceneEntity* subEntity : entitiesToDestroy)
	{
		subEntity->ResetTransform();
	}

	SetTestCategory(testFlags, TXT("Scaling Tests"));
	{
		Vector3 rootScaling(2.f, 1.f, 4.f);
		rootEntity->SetScale(rootScaling);
		Vector3 actualScaling = rootEntity->GetScale();

		if (rootScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the root entity by %s, the transform scaling attributes reads %s instead of %s."), rootScaling, actualScaling, rootScaling);
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved scaling information from root Entity."));

		//Sub Entities (currently at identity matrix) should have the same scaling info.
		//Getting the scale value by accessing certain elements doesn't make sense unless the matrix doesn't have rotation.  Ensure there isn't a rotation.
		subEntity1->CalculateAbsoluteTransform();
		TransformMatrix absMatrix = subEntity1->GetAbsTransform();
		actualScaling = absMatrix.GetScale();
		if (rootScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the root entity by %s, the sub entity's transform matrix, which should be the identity matrix (%s), computed its absolute scaling to be %s instead of %s."), rootScaling, subEntity1->ToTransformMatrix(), actualScaling, rootScaling);
			clearEntities();
			return false;
		}

		Vector3 subEntityScale(8.f, -3.f, 0.5f);
		subEntity1->SetScale(subEntityScale);
		Vector3 expectedScaling = subEntityScale * rootScaling;
		subEntity1->CalculateAbsoluteTransform();
		absMatrix = subEntity1->GetAbsTransform();
		actualScaling = absMatrix.GetScale();

		for (UINT_TYPE i = 0; i < 3; ++i)
		{
			expectedScaling[i].RoundInline(4);
			actualScaling[i].RoundInline(4);
		}

		if (expectedScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the sub entity by %s, the sub entity's absolute scale computed to be %s instead of %s."), subEntityScale, actualScaling, expectedScaling);
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity scaling information."));

		//Apply translations to both sub entities and see if their abs positions are affected by RelativeTo's scaling.
		SceneEntity* subEntity3 = SceneEntity::CreateObject();
		entitiesToDestroy.push_back(subEntity3);

		Vector3 subEntityTranslation(500.f, -100.f, 0.5f);
		subEntity1->SetTranslation(subEntityTranslation);
		subEntity2->SetTranslation(subEntityTranslation);
		subEntity2->SetRelativeTo(subEntity1);
		subEntity3->SetTranslation(subEntityTranslation);
		subEntity3->SetRelativeTo(subEntity2);

		subEntity1->CalculateAbsoluteTransform();
		Vector3 actualPosition = subEntity1->GetAbsTranslation();
		Vector3 expectedPosition = (rootScaling * subEntityTranslation);

		std::function<void(Int /*numDecimals*/)> roundTheComparisonVectors = [&](Int numDecimals)
		{
			for (UINT_TYPE i = 0; i < 3; ++i)
			{
				actualPosition[i].RoundInline(numDecimals);
				expectedPosition[i].RoundInline(numDecimals);
			}
		};
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s, and the sub entity's translation set to %s, the expected absolute translation of the sub entity should have been %s.  Instead it computed %s."), rootScaling, subEntityTranslation, expectedPosition, actualPosition);
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity absolute coordinates even with the root entity scaled to %s."), rootScaling);

		Vector3 rootShift(200.f, 400.f, -350.f);
		rootEntity->SetTranslation(rootShift);
		subEntity1->CalculateAbsoluteTransform();
		actualPosition = subEntity1->GetAbsTranslation();
		expectedPosition += rootShift;
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s and shifted to %s, and the sub entity's translation set to %s, the expected absolute translation of the sub entity should have been %s.  Instead it computed %s."), rootScaling, rootShift, subEntityTranslation, expectedPosition, actualPosition);
			clearEntities();
			return false;
		}

		subEntity2->CalculateAbsoluteTransform();
		actualPosition = subEntity2->GetAbsTranslation();
		expectedPosition = (rootShift) + (rootScaling * subEntityTranslation) + (rootScaling * subEntity1->ReadScale() * subEntityTranslation);
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s and shifted to %s, and the immediate parent entity scaled to %s and translated to %s, the expected absolute translation of the sub sub entity (with relative translation of %s) should have been %s.  Instead it computed %s."), rootScaling, rootShift, subEntity1->ReadScale(), subEntityTranslation, subEntityTranslation, expectedPosition, actualPosition);
			clearEntities();
			return false;
		}

		subEntity3->CalculateAbsoluteTransform();
		actualPosition = subEntity3->GetAbsTranslation();
		expectedPosition += (rootScaling * subEntity1->ReadScale() * subEntity2->ReadScale() * subEntityTranslation);
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  The sub entity of a sub entity with no scale modifiers should have a simple translation shift relative to the parent sub entity.  Its absolute translation is %s instead of %s."), expectedPosition, actualPosition);
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity absolute coordinates after shifting the scaled root entity."));

		subEntity2->SetRelativeTo(rootEntity);
		subEntity3->Destroy();
		entitiesToDestroy.erase(entitiesToDestroy.begin() + entitiesToDestroy.size() - 1);
	}
	CompleteTestCategory(testFlags);

	for (SceneEntity* subEntity : entitiesToDestroy)
	{
		subEntity->ResetTransform();
	}

	std::function<bool(const Vector3&, const Vector3&, const Rotator&, const Vector3&, const Vector3&, const Rotator&, const Vector3&, const Vector3&, const Rotator&)> runTransformTest
		([&](const Vector3& rootTranslation, const Vector3& rootScale, const Rotator& rootRotation, const Vector3& subEntityTranslation, const Vector3& subEntityScale, const Rotator& subEntityRotation, const Vector3& expectedSubAbsPosition, const Vector3& expectedSubScale, const Rotator& expectedSubRotation)
		{
			const float tolerance = 0.01f;

			rootEntity->ResetTransform();
			subEntity1->ResetTransform();

			TestLog(testFlags, TXT("Translating Root Entity by %s. Scaling Root Entity by %s. Rotating Root Entity by %s."), rootTranslation, rootScale, rootRotation);
			TestLog(testFlags, TXT("Translating Sub  Entity by %s. Scaling Sub  Entity by %s. Rotating Sub  Entity by %s."), subEntityTranslation, subEntityScale, subEntityRotation);

			rootEntity->EditTranslation() = rootTranslation;
			rootEntity->EditScale() = rootScale;
			rootEntity->EditRotation() = rootRotation;
			subEntity1->EditTranslation() = subEntityTranslation;
			subEntity1->EditScale() = subEntityScale;
			subEntity1->EditRotation() = subEntityRotation;

			rootEntity->CalculateAbsoluteTransform();
			subEntity1->CalculateAbsoluteTransform();

			if (!subEntity1->ReadAbsTranslation().IsNearlyEqual(expectedSubAbsPosition, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute position of the sub entity should have been %s. Instead it's %s."), rootTranslation, rootScale, rootRotation, subEntityTranslation, subEntityScale, subEntityRotation, expectedSubAbsPosition, subEntity1->ReadAbsTranslation());
				clearEntities();
				return false;
			}

			if (!subEntity1->ReadAbsScale().IsNearlyEqual(expectedSubScale, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute scale of the sub entity should have been %s. Instead it's %s."), rootTranslation, rootScale, rootRotation, subEntityTranslation, subEntityScale, subEntityRotation, expectedSubScale, subEntity1->ReadAbsScale());
				clearEntities();
				return false;
			}

			//Convert to directional vectors since there are multiple ways for rotations to point to the same direction.
			Vector3 subEntityDirection = subEntity1->ReadAbsRotation().GetDirectionalVector();
			Vector3 expectedDirection = expectedSubRotation.GetDirectionalVector();
			if (!subEntityDirection.IsNearlyEqual(expectedDirection, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute rotation of the sub entity should have been %s. Instead it's %s. The directional vectors of those rotators are not equal: %s != %s"), rootTranslation, rootScale, rootRotation, subEntityTranslation, subEntityScale, subEntityRotation, expectedSubRotation, subEntity1->ReadAbsRotation(), subEntityDirection, expectedDirection);
				clearEntities();
				return false;
			}

			return true;
		});

	std::function<bool(const TransformMatrix&, const TransformMatrix&)> verifyTransformMatrix([&](const TransformMatrix& givenMatrix, const TransformMatrix& expectedMatrix)
	{
		if (!givenMatrix.IsNearlyEqual(expectedMatrix, 0.01f))
		{
			UnitTestError(testFlags, TXT("Scene Transform test failed. The given matrix does not match the expected matrix."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Given Matrix"));
				givenMatrix.LogMatrix();

				TestLog(testFlags, TXT("-========-"));
				TestLog(testFlags, TXT("Expected Matrix"));
				expectedMatrix.LogMatrix();
			}

			clearEntities();
			return false;
		}

		return true;
	});

	SetTestCategory(testFlags, TXT("Rotation Tests"));
	{
		//Constants that'll be the same for all tests for this section
		const Vector3 rootEntityTranslation(Vector3::ZERO_VECTOR);
		const Vector3 rootEntityScale(1.f, 1.f, 1.f);
		const Vector3 subEntityTranslation(1.f, 0.f, 0.f);
		const Vector3 subEntityScale(1.f, 1.f, 1.f);
		const Vector3 expectedScale(1.f, 1.f, 1.f);
		const unsigned int fortyFiveDegreeSpin = static_cast<unsigned int>(static_cast<float>(Rotator::QUARTER_REVOLUTION) * 0.5f);

		if (!runTransformTest(rootEntityTranslation, rootEntityScale, Rotator::ZERO_ROTATOR, subEntityTranslation, subEntityScale, Rotator::ZERO_ROTATOR, Vector3(1.f, 0.f, 0.f), expectedScale, Rotator::ZERO_ROTATOR))
		{
			return false;
		}

		//Spin 45 degrees along yaw each iteration.
		Rotator spin = Rotator::ZERO_ROTATOR;
		for (int i = 0; i < 8; ++i)
		{
			Vector3 expectedAbsPosition = spin.GetDirectionalVector() * subEntityTranslation.VSize();

			//Sub Entity should be orbiting around root
			if (!runTransformTest(rootEntityTranslation, rootEntityScale, spin, subEntityTranslation, subEntityScale, Rotator::ZERO_ROTATOR, expectedAbsPosition, subEntityScale, spin))
			{
				return false;
			}

			spin.Yaw += fortyFiveDegreeSpin;
		}

		std::vector<Rotator> expectedAbsRotation
		{
			/* [0] */ Rotator(0, 0, 0),
			/* [1] */ Rotator(180.f, 45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [2] */ Rotator(-90.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [3] */ Rotator(0.f, 45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [4] */ Rotator(0.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [5] */ Rotator(0.f, -45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [6] */ Rotator(90.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [7] */ Rotator(-180.f, -45.f, 0.f, Rotator::ERotationUnit::RU_Degrees)
		};

		//Spin 45 degrees along pitch each iteration. Meanwhile, the sub entity will also be spinning 45 degrees yaw.
		spin = Rotator::ZERO_ROTATOR;
		Rotator subSpin = Rotator::ZERO_ROTATOR;
		for (size_t i = 0; i < expectedAbsRotation.size(); ++i)
		{
			Vector3 expectedAbsPosition = spin.GetDirectionalVector() * subEntityTranslation.VSize();

			//Sub Entity should be orbiting around root. While the sub entity should also have their own spin.
			if (!runTransformTest(rootEntityTranslation, rootEntityScale, spin, subEntityTranslation, subEntityScale, subSpin, expectedAbsPosition, subEntityScale, expectedAbsRotation.at(i)))
			{
				return false;
			}

			spin.Pitch += fortyFiveDegreeSpin;
			subSpin.Yaw += fortyFiveDegreeSpin;
			subSpin.Pitch += Rotator::QUARTER_REVOLUTION;
			subSpin.Roll += Rotator::HALF_REVOLUTION; //woah dizzy Entity
		}
	}
	CompleteTestCategory(testFlags);

	//This category will be verifying the sub entity's position, scale, and rotation based on its local transform and the root's transform.
	SetTestCategory(testFlags, TXT("Relative Transform Tests"));
	{
		Vector3 rootTranslation(1.f, 2.f, 3.f);
		Vector3 rootScale(1.f, 1.f, 1.f);
		Rotator rootRotation(Rotator::ZERO_ROTATOR);
		Vector3 subTranslation(10.f, 20.f, 30.f);
		Vector3 subScale(1.f, 1.f, 1.f);
		Rotator subRotation(Rotator::ZERO_ROTATOR);
		Vector3 expectedTranslation(11.f, 22.f, 33.f);
		Vector3 expectedScale(1.f, 1.f, 1.f);
		Rotator expectedRotation(Rotator::ZERO_ROTATOR);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		rootRotation = Rotator(45.f, 0.f, 180.f, Rotator::RU_Degrees);
		expectedRotation = Rotator(45.f, 0.f, 180.f, Rotator::RU_Degrees);
		expectedTranslation = Vector3(-6.0711f, -19.2132f, -27.0000f);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		subRotation = Rotator(-45.f, 90.f, 0.f, Rotator::RU_Degrees);
		expectedRotation = Rotator(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		subScale.Y = 2.f;
		expectedScale.Y = 2.f;
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		rootScale.X = 2.f;
		rootScale.Y = 3.f;
		rootEntity->EditScale() = rootScale; //Assign this early for the ToTransformMatrix tests.

		//Verify the transform matrices are valid before verifying its sub components
		TransformMatrix expectedRootTransform(Vector3(2.f, 3.f, 1.f), Matrix(4, 4,
			{
				1.414f,		-1.414f,	0.f,	1.f,
				-2.121f,	-2.121f,	0.f,	2.f,
				0.f,		0.f,		-1.f,	3.f,
				0.f,		0.f,		0.f,	1.f
			}));

		if (!verifyTransformMatrix(rootEntity->ToTransformMatrix(), expectedRootTransform))
		{
			return false;
		}

		TransformMatrix expectedSubTransform(Vector3(1.f, 2.f, 1.f), Matrix(4, 4,
			{
				0.f,	-0.707f,	-0.707f,	10.f,
				0.f,	1.414f,		-1.414f,	20.f,
				1.f,	0.f,		0.f,		30.f,
				0.f,	0.f,		0.f,		1.f
			}));

		if (!verifyTransformMatrix(subEntity1->ToTransformMatrix(), expectedSubTransform))
		{
			return false;
		}

		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		TransformMatrix expectedAbsTransform(Vector3(2.f, 6.f, 1.f), Matrix(4, 4,
			{
				0.f,	-3.f,		1.f,		-13.14f,
				0.f,	-1.5f,		4.499f,		-61.63f,
				-1.f,	0.f,		0.f,		-27.f,
				0.f,	0.f,		0.f,		1.f
			}));

		if (!verifyTransformMatrix(subEntity1->GetAbsTransform(), expectedAbsTransform))
		{
			return false;
		}

		expectedScale.X = 2.f;
		expectedScale.Y = 6.f;
		expectedTranslation = Vector3(-13.14f, -61.63f, -27.f);
		//TODO: Reenable this test. For some reason, scaling the root entity changes the orientation of the Sub Entity.
		/*
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}
		*/
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Scene Transform Components"));
	{
		SceneEntity* transformTestRootEntity = SceneEntity::CreateObject();
		entitiesToDestroy.push_back(transformTestRootEntity);

		/*
		Create this component tree:

		transformTestRootEntity
			TickA
				SceneTransformA
					SceneTransformB
						TickB
				TickC
			SceneTransformC
				TickD
		*/

		SceneTransformComponent* sceneTransformA = nullptr;
		SceneTransformComponent* sceneTransformB = nullptr;
		SceneTransformComponent* sceneTransformC = nullptr;
		TickComponent* tickA = nullptr;
		TickComponent* tickB = nullptr;
		TickComponent* tickC = nullptr;
		TickComponent* tickD = nullptr;

		tickA = TickComponent::CreateObject(TICK_GROUP_DEBUG);
		if (transformTestRootEntity->AddComponent(tickA))
		{
			sceneTransformA = SceneTransformComponent::CreateObject();
			if (tickA->AddComponent(sceneTransformA))
			{
				sceneTransformB = SceneTransformComponent::CreateObject();
				if (sceneTransformA->AddComponent(sceneTransformB))
				{
					tickB = TickComponent::CreateObject(TICK_GROUP_DEBUG);
					if (sceneTransformB->AddComponent(tickB))
					{
						//Noop
					}
				}

				tickC = TickComponent::CreateObject(TICK_GROUP_DEBUG);
				if (tickA->AddComponent(tickC))
				{
					//Noop
				}
			}
		}

		sceneTransformC = SceneTransformComponent::CreateObject();
		if (transformTestRootEntity->AddComponent(sceneTransformC))
		{
			tickD = TickComponent::CreateObject(TICK_GROUP_DEBUG);
			if (sceneTransformC->AddComponent(tickD))
			{
				//Noop
			}
		}

		SceneTransformComponent* testComp = nullptr;
		SceneTransform* expectedRelative = nullptr;
		std::function<bool(Entity*)> runComponentTest([&](Entity* attachTo)
		{
			//If attachTo is null then assume that the test component is already attached to the correct component.
			if (attachTo != nullptr)
			{
				testComp->DetachSelfFromOwner();
				if (!attachTo->AddComponent(testComp))
				{
					UnitTestError(testFlags, TXT("Scene Transform Component test failed. Unable to attach a SceneTransformComponent to a %s."), attachTo->ToString());
					clearEntities();
					return false;
				}
			}

			if (expectedRelative != testComp->GetRelativeTo())
			{
				UnitTestError(testFlags, TXT("Scene Transform Component test failed. After attaching a SceneTransformComponent to %s, the expected RelativeTo does not match the actual RelativeTo."), testComp->GetOwner()->ToString());
				clearEntities();
				return false;
			}

			//Need to verify the OnOwnerChanged callbacks are correct.
			//Generate a list of components that should have the callback registered.
			std::vector<EntityComponent*> expectedRegistered;
			for (Entity* owner = testComp->GetOwner(); owner != nullptr; /* Noop */ )
			{
				if (dynamic_cast<SceneTransform*>(owner) != nullptr)
				{
					break;
				}

				if (EntityComponent* comp = dynamic_cast<EntityComponent*>(owner))
				{
					expectedRegistered.push_back(comp);
					owner = comp->GetOwner();
					continue;
				}

				break;
			}

			for (ComponentIterator iter(transformTestRootEntity, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				bool expectCallback = (ContainerUtils::FindInVector(expectedRegistered, iter.GetSelectedComponent()) != UINT_INDEX_NONE);
				bool hasCallback = iter.GetSelectedComponent()->OnOwnerChanged.IsRegistered(SDFUNCTION_2PARAM(testComp, SceneTransformComponent, HandleOwnerChange, void, EntityComponent*, Entity*), true);

				if (expectCallback != hasCallback)
				{
					DString expectedConditionStr = expectCallback ? DString::EmptyString : TXT(" NOT ");
					DString actualConditionStr = hasCallback ? DString::EmptyString : TXT(" NOT ");
					UnitTestError(testFlags, TXT("Scene Transform Component test failed. When verifying the OnOwnerChanged callbacks, the %s is expected to %s have the callback. But it does %s have the callback."), iter.GetSelectedComponent()->ToString(), expectedConditionStr, actualConditionStr);
					clearEntities();
					return false;
				}
			}

			return true;
		});

		testComp = SceneTransformComponent::CreateObject();
		expectedRelative = transformTestRootEntity;
		if (!runComponentTest(transformTestRootEntity))
		{
			return false;
		}

		if (!runComponentTest(tickA))
		{
			return false;
		}

		expectedRelative = sceneTransformA;
		if (!runComponentTest(sceneTransformA))
		{
			return false;
		}

		expectedRelative = sceneTransformB;
		if (!runComponentTest(tickB))
		{
			return false;
		}

		expectedRelative = sceneTransformC;
		if (!runComponentTest(tickD))
		{
			return false;
		}

		expectedRelative = transformTestRootEntity;
		if (!runComponentTest(tickC))
		{
			return false;
		}

		//Move tickC to tickB
		tickC->DetachSelfFromOwner();
		tickB->AddComponent(tickC);
		expectedRelative = sceneTransformB;
		if (!runComponentTest(nullptr))
		{
			return false;
		}

		//Move tickC to tickD (tickB should unregister its callback).
		tickC->DetachSelfFromOwner();
		tickD->AddComponent(tickC);
		expectedRelative = sceneTransformC;
		if (!runComponentTest(nullptr))
		{
			return false;
		}

		testComp->DetachSelfFromOwner();
		expectedRelative = nullptr;
		if (!runComponentTest(nullptr))
		{
			testComp->Destroy();
			return false;
		}

		transformTestRootEntity->AddComponent(testComp); //Reattach component so that it'll be destroyed when the root entity is destroyed.
	}
	CompleteTestCategory(testFlags);

	clearEntities();
	ExecuteSuccessSequence(testFlags, TXT("Scene Transforms"));
	return true;
}

bool GraphicsUnitTester::TestSceneTransformUpdater (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Scene Transform Updater"));

	SceneTransformUpdaterTester* tester = SceneTransformUpdaterTester::CreateObject();
	tester->LaunchTests(testFlags);
	TestLog(testFlags, TXT("SceneTransformUpdater test created. The Entities associated with that test will perish after it fails or completes."));

	return true;
}

bool GraphicsUnitTester::TestSolidColor (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Solid Color Component"));

	ColorTester* colorTester = ColorTester::CreateObject();
	return (colorTester->BeginTest(testFlags)); //The ColorTester will automatically expire when test finishes.
}

bool GraphicsUnitTester::TestRenderTexture (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Render Texture"));
	RenderTextureTester* tester = RenderTextureTester::CreateObject();
	TestLog(testFlags, TXT("RenderTextureTester created.  Close the Render Texture window to terminate the test."));

	return true;
}

bool GraphicsUnitTester::TestTopDownCamera (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Top Down Camera"));

	SetTestCategory(testFlags, TXT("Pixel to Scene"));
	{
		GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
		CHECK(graphicsEngine != nullptr)

		TopDownCamera* cam = TopDownCamera::CreateObject();
		cam->SetTranslation(Vector3(400.f, -500.f, 0.f));
		cam->Zoom = 0.5f;
		cam->CalculateAbsoluteTransform();

		std::function<bool(const Vector2&, const Vector2&, const Vector2&)> testPixelProjection ([&](const Vector2& viewportSize, const Vector2& pixelPos, const Vector2& expectedLocation)
		{
			Rotator rotation;
			Vector3 actualLocation;
			cam->CalculatePixelProjection(viewportSize, pixelPos, OUT rotation, OUT actualLocation);

			bool bPassed = (actualLocation.ToVector2().IsNearlyEqual(expectedLocation, 0.001f));
			if (!bPassed)
			{
				UnitTestError(testFlags, TXT("Top Down camera test failed. The pixel projection to scene does not return the expected location: %s. It returned %s instead."), expectedLocation, actualLocation.ToVector2());
			}

			return bPassed;
		});

		if (!testPixelProjection(Vector2(100.f, 100.f), Vector2(50.f, 50.f), Vector2(400.f, -500.f)))
		{
			cam->Destroy();
			return false;
		}

		if (!testPixelProjection(Vector2(100.f, 100.f), Vector2(0.f, 0.f), Vector2(397.354f, -502.645f)))
		{
			cam->Destroy();
			return false;
		}

		if (!testPixelProjection(Vector2(200.f, 100.f), Vector2(200.f, 80.f), Vector2(405.292f, -498.413f)))
		{
			cam->Destroy();
			return false;
		}

		cam->Destroy();
		return true;
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Top Down Camera"));
	return true;
}

SD_END
#endif