/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformUpdater.cpp
=====================================================================
*/

#include "SceneTransform.h"
#include "SceneTransformUpdater.h"

IMPLEMENT_CLASS(SD::SceneTransformUpdater, SD::Entity)
SD_BEGIN

void SceneTransformUpdater::InitProps ()
{
	Super::InitProps();

	//No strong opinion on this number. Without any data to back this claim, it seems to be a decent number of transforms for each thread to handle.
	//Too small of a number would create too many threads to handle one small operation.
	//Too big of a number would take too long of an operation to handle all of those transforms.
	NumTransformsPerThread = 8;
	for (size_t i = 0; i < SD_TRANSFORM_UPDATER_MAX_THREADS; ++i)
	{
		WorkerThreads[i] = nullptr;
	}

	NumWorkerThreads = 0;
	NumActiveThreads = 0;
	FrameCycle = 0;
	bTerminateWorkerThreads = false;

	NextAvailableTemporarySlot = 0;
}

void SceneTransformUpdater::BeginObject ()
{
	Super::BeginObject();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_TRANSFORM_UPDATE);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, SceneTransformUpdater, HandleTick, void, Float));
	}
}

void SceneTransformUpdater::Destroyed ()
{
	//Notify all worker threads to terminate.
	bTerminateWorkerThreads = true;
	CalcFinishedCondition.notify_all();

	//Block this thread until all worker threads terminate.
	for (size_t i = 0; i < SD_TRANSFORM_UPDATER_MAX_THREADS && WorkerThreads[i] != nullptr; ++i)
	{
		WorkerThreads[i]->join();
		delete WorkerThreads[i];
		WorkerThreads[i] = nullptr;
	}

	Super::Destroyed();
}

bool SceneTransformUpdater::IsAlreadyPermanentTransform (SceneTransform* transform) const
{
	for (SceneTransform* curTransform = transform; curTransform != nullptr; curTransform = curTransform->GetRelativeTo())
	{
		if (ContainerUtils::FindInVector(PermanentTransforms, curTransform) != UINT_INDEX_NONE)
		{
			return true;
		}
	}

	return false;
}

bool SceneTransformUpdater::RegisterPermanentTransform (SceneTransform* newTransform)
{
	if (newTransform->GetUpdateBehavior() != SceneTransform::UB_Continuous)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Only continuously updated SceneTransforms are allowed to register to the SceneTransformUpdater's PermanentTransform vector. Registering other transform types run the risk of race conditions."));
		return false;
	}

	//It's important to check for duplicates not only for performance reasons, but also to prevent race conditions.
	bool alreadyAttached = false;

	//Ensure none of the sub transforms are already registered to this list.
	newTransform->ForEachTransformInBranch([&](SceneTransform* curTransform)
	{
		if (alreadyAttached)
		{
			//already failed
			return;
		}

		if (!ContainerUtils::IsEmpty(curTransform->ReadAttachedTransforms()))
		{
			//Do nothing. This function should only execute on leaf transforms since each leaf will check up the transform chain anyways.
			return;
		}

		if (IsAlreadyPermanentTransform(curTransform))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to register new transformation since it or one of the transforms it's relative to is already registered to the permanent transform vector."));
			alreadyAttached = true;
		}
	});

	if (alreadyAttached)
	{
		return false;
	}

	PermanentTransforms.push_back(newTransform);

	//Check if the worker threads need to expand
	EvaluateWorkerThreadLoad();

	return true;
}

void SceneTransformUpdater::RegisterTemporaryTransform (SceneTransform* newTransform)
{
	if (newTransform->GetUpdateBehavior() == SceneTransform::UB_Continuous)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Registering a continuously updating SceneTransform to a temporary vector in the SceneTransformUpdater is not supported due to the risk of race conditions between worker threads and this thread."));
		return;
	}

	if (ContainerUtils::IsEmpty(TemporaryTransforms) || NextAvailableTemporarySlot >= TemporaryTransforms.size())
	{
		TemporaryTransforms.push_back(newTransform);
		NextAvailableTemporarySlot = TemporaryTransforms.size();
		return;
	}

	TemporaryTransforms.at(NextAvailableTemporarySlot) = newTransform;
	++NextAvailableTemporarySlot;
}

void SceneTransformUpdater::RemovePermanentTransform (SceneTransform* oldTransform)
{
	ContainerUtils::RemoveItem(OUT PermanentTransforms, oldTransform);
}

void SceneTransformUpdater::RemoveTemporaryTransform (SceneTransform* oldTransform)
{
	size_t removedIdx = ContainerUtils::RemoveItem(OUT TemporaryTransforms, oldTransform);
	if (removedIdx != UINT_INDEX_NONE && removedIdx < NextAvailableTemporarySlot)
	{
		--NextAvailableTemporarySlot;
	}
}

void SceneTransformUpdater::ShrinkTemporaryTransformVector ()
{
	size_t firstEmpty = ContainerUtils::FindInVector<SceneTransform*>(TemporaryTransforms, nullptr);
	if (firstEmpty != UINT_INDEX_NONE && firstEmpty < TemporaryTransforms.size())
	{
		TemporaryTransforms.resize(firstEmpty);
		NextAvailableTemporarySlot = firstEmpty;
	}
}

void SceneTransformUpdater::SetNumTransformsPerThread (size_t newNumTransformsPerThread)
{
	NumTransformsPerThread = newNumTransformsPerThread;
	EvaluateWorkerThreadLoad();
}

void SceneTransformUpdater::UpdatePermanentTransforms ()
{
	if (ContainerUtils::IsEmpty(PermanentTransforms))
	{
		return;
	}

	for (SceneTransform* transform : PermanentTransforms)
	{
		//Don't broadcast delegates in a separate thread since the handlers may assume it's processed in the same thread as the GraphicsEngineComponent.
		transform->SetTransformChangedBroadcastLock(true);
	}

	if (NumTransformsPerThread <= 0 || PermanentTransforms.size() <= NumTransformsPerThread)
	{
		//Just do everything in this thread
		CalcPermanentTransformBatch(0);
	}
	else
	{
		{
			std::lock_guard<std::mutex> lock(CalcFinishedConditionMutex);
			FrameCycle.fetch_add(1); //Increment the frame cycle to indicate the worker threads that they'll need to run through their loops.
			NumActiveThreads.store(NumWorkerThreads);
		}

		CalcFinishedCondition.notify_all();

		{
			std::unique_lock<std::mutex> lock(CalcFinishedConditionMutex);

			//Freeze current thread until all workers finished.
			CalcFinishedCondition.wait(lock, [&]{return (NumActiveThreads.load() <= 0);});
		}
	}

	//Broadcast OnTransformChanged if the transforms changed.
	for (SceneTransform* transform : PermanentTransforms)
	{
		transform->SetTransformChangedBroadcastLock(false);
	}
}

void SceneTransformUpdater::UpdateTemporaryTransforms ()
{
	for (size_t i = 0; i < TemporaryTransforms.size(); ++i)
	{
		if (TemporaryTransforms.at(i) == nullptr)
		{
			//Early exit since TemporaryTransforms doesn't shrink each loop. First nullptr indicates everything else is also null
			break;
		}

		TemporaryTransforms.at(i)->ForEachTransformInBranch([](SceneTransform* curTransform)
		{
			curTransform->CalculateAbsoluteTransform();
		});

		TemporaryTransforms.at(i) = nullptr;
	}

	NextAvailableTemporarySlot = 0;
}

void SceneTransformUpdater::CalcPermanentTransformBatch (size_t startIdx)
{
	//If multiple threads enabled, then select the next group of transforms. Otherwise do the whole vector.
	size_t lastIdx = (NumTransformsPerThread > 0) ? Utils::Min(startIdx + NumTransformsPerThread - 1, PermanentTransforms.size() - 1) : PermanentTransforms.size() - 1;
	for (size_t i = startIdx; i <= lastIdx; ++i)
	{
		PermanentTransforms.at(i)->ForEachTransformInBranch([](SceneTransform* curTransform)
		{
			curTransform->CalculateAbsoluteTransform();
		});
	}
}

void SceneTransformUpdater::EvaluateWorkerThreadLoad ()
{
	if (NumTransformsPerThread <= 0)
	{
		return;
	}

	while (true)
	{
		float threadsRequired = static_cast<float>(PermanentTransforms.size()) / static_cast<float>(NumTransformsPerThread);
		int maxNumThreads = static_cast<int>(std::ceilf(threadsRequired));
		if (maxNumThreads > SD_TRANSFORM_UPDATER_MAX_THREADS)
		{
			//There are more transforms than there are threads. Increase the load for each thread.
			NumTransformsPerThread++;
			continue;
		}

		//Allocate threads
		for (int i = maxNumThreads - 1; WorkerThreads[i] == nullptr && i >= 0; --i)
		{
			WorkerThreads[i] = new std::thread(&SceneTransformUpdater::RunWorkerThread, this, i);
			NumWorkerThreads++;
		}

		break;
	}
}

void SceneTransformUpdater::RunWorkerThread (size_t workerIdx)
{
	//Reason for the +127: Pick a frame cycle that is 'opposite' from the FrameCycle just in case the main thread incremented it and is waiting for this thread to process before the main thread continues avoiding a deadlock.
	unsigned char prevFrameCycle = FrameCycle.load() + 127;

	while (!bTerminateWorkerThreads)
	{
		{
			//Lock current thread until NumActiveThreads variable is positive.
			std::unique_lock<std::mutex> lock(CalcFinishedConditionMutex);
			CalcFinishedCondition.wait(lock, [&]{return ((NumActiveThreads.load() > 0 && FrameCycle.load() != prevFrameCycle) || bTerminateWorkerThreads);});
		}

		if (bTerminateWorkerThreads)
		{
			break;
		}

		size_t startIdx = NumTransformsPerThread * workerIdx;
		if (startIdx < PermanentTransforms.size())
		{
			CalcPermanentTransformBatch(startIdx);
		}

		{
			std::lock_guard<std::mutex> lock(CalcFinishedConditionMutex);
			//This thread finished its operation. Deduct from NumActiveThreads.
			prevFrameCycle = FrameCycle.load();
			NumActiveThreads.fetch_sub(1);
		}

		CalcFinishedCondition.notify_all();
	}
}

void SceneTransformUpdater::HandleTick (Float deltaSec)
{
	UpdatePermanentTransforms();
	UpdateTemporaryTransforms();
}
SD_END