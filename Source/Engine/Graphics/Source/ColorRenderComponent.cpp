/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorRenderComponent.cpp
=====================================================================
*/

#include "ColorRenderComponent.h"
#include "RenderTarget.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::ColorRenderComponent, SD::RenderComponent)
SD_BEGIN

void ColorRenderComponent::InitProps ()
{
	Super::InitProps();

	SolidColor = sf::Color(0, 0, 0, 255);
	Shape = ST_Rectangle;
	BaseSize = Vector2(1.f, 1.f);
	Pivot = Vector2::ZERO_VECTOR;

	OriginalBaseSizeY = BaseSize.Y;
}

void ColorRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ColorRenderComponent* colorTemplate = dynamic_cast<const ColorRenderComponent*>(objTemplate);
	if (colorTemplate != nullptr)
	{
		SolidColor = colorTemplate->SolidColor;
		SetShape(colorTemplate->Shape);
		SetBaseSize(colorTemplate->BaseSize);
		SetPivot(colorTemplate->Pivot);
	}
}

void ColorRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	Transformation::SScreenProjectionData projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	switch (Shape)
	{
		case (ST_Rectangle):
		{
			sf::RectangleShape renderedShape;
			renderedShape.setPosition(projectionData.Position);
			renderedShape.setScale(projectionData.GetFinalSize());
			renderedShape.setRotation(projectionData.Rotation);
			renderedShape.setOrigin(Vector2::SDtoSFML(Pivot * BaseSize));
			renderedShape.setSize(Vector2::SDtoSFML(BaseSize));

			renderedShape.setFillColor(SolidColor.Source);
			renderTarget->Draw(this, camera, renderedShape);

			break;
		}
		case (ST_Circle):
		{
			sf::CircleShape renderedShape;
			renderedShape.setPosition(projectionData.Position);
			renderedShape.setScale(projectionData.GetFinalSize());
			renderedShape.setRotation(projectionData.Rotation);
			renderedShape.setOrigin(Vector2::SDtoSFML(Pivot * BaseSize));
			renderedShape.setRadius(BaseSize.X.Value * 0.5f);

			renderedShape.setFillColor(SolidColor.Source);
			renderTarget->Draw(this, camera, renderedShape);

			break;
		}
	}
}

Aabb ColorRenderComponent::GetAabb () const
{
	return Aabb(BaseSize.X, 0.f, BaseSize.Y, Vector3::ZERO_VECTOR);
}

void ColorRenderComponent::SetShape (ShapeType newShape)
{
	Shape = newShape;

	if (Shape == ST_Circle)
	{
		BaseSize.Y = BaseSize.X;
	}
	else
	{
		BaseSize.Y = OriginalBaseSizeY;
	}
}

void ColorRenderComponent::SetBaseSize (const Vector2& newBaseSize)
{
	BaseSize = newBaseSize;
	OriginalBaseSizeY = BaseSize.Y;

	if (Shape == ST_Circle)
	{
		BaseSize.Y = BaseSize.X; //Only the X axis is considered for circles.
	}
}

void ColorRenderComponent::SetBaseSize (Float newBaseSizeX, Float newBaseSizeY)
{
	BaseSize.X = newBaseSizeX;
	BaseSize.Y = newBaseSizeY;
	OriginalBaseSizeY = BaseSize.Y;

	if (Shape == ST_Circle)
	{
		BaseSize.Y = BaseSize.X;
	}
}

void ColorRenderComponent::SetPivot (const Vector2& newPivot)
{
	Pivot = newPivot;
}

void ColorRenderComponent::SetPivot (Float pivotX, Float pivotY)
{
	Pivot.X = pivotX;
	Pivot.Y = pivotY;
}
SD_END