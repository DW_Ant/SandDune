/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveRenderComponent.cpp
=====================================================================
*/

#include "Aabb.h"
#include "CurveRenderComponent.h"
#include "RenderTarget.h"
#include "Texture.h"
#include "TexturePool.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::CurveRenderComponent, SD::RenderComponent)
SD_BEGIN

void CurveRenderComponent::InitProps ()
{
	Super::InitProps();

	CurveColor = sf::Color::White;
	CurveThickness = 0.f;
}

void CurveRenderComponent::BeginObject ()
{
	Super::BeginObject();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (localTexturePool != nullptr)
	{
		SetCurveTexture(localTexturePool->GetTexture(HashedString("Engine.Graphics.VerticalGradient")));
	}
}

void CurveRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const CurveRenderComponent* curveTemplate = dynamic_cast<const CurveRenderComponent*>(objTemplate);
	if (curveTemplate != nullptr)
	{
		SetCurveColor(curveTemplate->GetCurveColor());
		SetCurveTexture(curveTemplate->GetCurveTexture());
	}
}

void CurveRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	CHECK(GetOwnerTransform() != nullptr)

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	RenderStates.transform = sf::Transform::Identity;
	RenderStates.transform.translate(projectionData.Position);
	RenderStates.transform.scale(projectionData.Scale);
	RenderStates.transform.rotate(projectionData.Rotation);

	renderTarget->Draw(this, camera, Vertices, RenderStates);
}

Aabb CurveRenderComponent::GetAabb () const
{
	if (CachedBoundingBox.IsEmpty() && !ContainerUtils::IsEmpty(Points))
	{
		sf::FloatRect bounds = Vertices.getBounds();
		CachedBoundingBox = Aabb(Vector3(bounds.left, bounds.top, 0.f), Vector3(bounds.left + bounds.width, bounds.top + bounds.height, 0.f));
	}

	return CachedBoundingBox;
}

void CurveRenderComponent::GenerateBezierCurve (const Vector2& endPt1, const Vector2& endPt2, const Vector2& ctrlPt1, const Vector2& ctrlPt2, Float segmentLength, Float inCurveThickness, Float step)
{
	Float segSquared = segmentLength * segmentLength;
	std::vector<Vector2> newPoints;

	newPoints.push_back(endPt1);

	for (Float t = step; t < 1.f; t += step)
	{
		//Point between [E]ndPt1, [C]trl[P]t[1]
		Vector2 ecp1 = (ctrlPt1 - endPt1) * t;
		ecp1 += endPt1;

		//Point between [C]trlPt1, [C]trl[P]t2
		Vector2 ccp = (ctrlPt2 - ctrlPt1) * t;
		ccp += ctrlPt1;

		//Point between [C]trlPt2, [E]nd[P]t[2]
		Vector2 cep2 = (endPt2 - ctrlPt2) * t;
		cep2 += ctrlPt2;

		//Point between ecp1 and ccp
		Vector2 p1 = (ccp - ecp1) * t;
		p1 += ecp1;

		//Point between ccp and cep2
		Vector2 p2 = (cep2 - ccp) * t;
		p2 += ccp;

		//The actual point on the curve
		Vector2 result = (p2 - p1) * t;
		result += p1;

		if ((result - ContainerUtils::GetLast(newPoints)).CalcDistSquared() >= segSquared)
		{
			newPoints.push_back(result);
		}
	}

	newPoints.push_back(endPt2);
	GenerateCurve(newPoints, inCurveThickness);
}

void CurveRenderComponent::GenerateCurve (const std::vector<Vector2>& inPoints, Float inCurveThickness)
{
	CurveThickness = inCurveThickness;
	CachedBoundingBox = Aabb();
	Points = inPoints;
	Vertices.clear();

	if (ContainerUtils::IsEmpty(Points))
	{
		return;
	}

	float halfThickness = CurveThickness.Value * 0.5f;

	if (CurveThickness <= 0.f)
	{
		//Have the vertex array correspond to the points 1-by-1 since the line has no thickness (no need to generate triangles).
		Vertices.setPrimitiveType(sf::LineStrip);
		for (const Vector2& point : Points)
		{
			Vertices.append(sf::Vertex(sf::Vector2f(point.X.Value, point.Y.Value), CurveColor));
		}
	}
	else if (Points.size() == 1)
	{
		Vertices.setPrimitiveType(sf::TriangleFan);

		sf::Vector2f center(Points.at(0).X.Value, Points.at(0).Y.Value);

		//Draw four vertices around the one and only point.
		Vertices.append(sf::Vertex(center, CurveColor));
		Vertices.append(sf::Vertex(center + sf::Vector2f(halfThickness, 0.f), CurveColor));
		Vertices.append(sf::Vertex(center + sf::Vector2f(0.f, halfThickness), CurveColor));
		Vertices.append(sf::Vertex(center + sf::Vector2f(-halfThickness, 0.f), CurveColor));
		Vertices.append(sf::Vertex(center + sf::Vector2f(0.f, -halfThickness), CurveColor));
		Vertices.append(sf::Vertex(center + sf::Vector2f(halfThickness, 0.f), CurveColor)); //connect back to second vertex to complete the circle.
	}
	else
	{
		Vertices.setPrimitiveType(sf::TriangleStrip);
		sf::Vector2f textureDimensions(0.f, 0.f);
		if (CurveTexture != nullptr)
		{
			Int width;
			Int height;
			CurveTexture->GetDimensions(OUT width, OUT height);
			textureDimensions.x = width.ToFloat().Value;
			textureDimensions.y = height.ToFloat().Value;
		}

		for (size_t i = 0; i < Points.size(); ++i)
		{
			Vector2 newVertex;
			float rotateAmount = PI_FLOAT * 0.5f; //90 degrees
			Vector2 rotatedPt = (i == 0) ? (Points.at(i+1) - Points.at(i)) : (Points.at(i) - Points.at(i-1));
			if (rotatedPt.IsNearlyEqual(Vector2::ZERO_VECTOR, 0.001f))
			{
				rotatedPt = Vector2(1.f, 0.f);
			}
			else
			{
				rotatedPt.NormalizeInline();
			}

			rotatedPt *= halfThickness;

			//Rotate next point about the origin by angleAmount
			newVertex.X = (rotatedPt.X * cos(rotateAmount)) - (rotatedPt.Y * sin(rotateAmount));
			newVertex.Y = (rotatedPt.X * sin(rotateAmount)) + (rotatedPt.Y * cos(rotateAmount));

			if (i > 1)
			{
				//Need to create two more vertices at the previous point that are perpendicular to this current point. The order of these vertices don't matter since the triangles overlap anyways.
				Vector2 prevVert = newVertex + Points.at(i-1);
				Vertices.append(sf::Vertex(sf::Vector2f(prevVert.X.Value, prevVert.Y.Value), CurveColor, sf::Vector2f(0.f, 0.f)));

				prevVert = -newVertex + Points.at(i-1);
				Vertices.append(sf::Vertex(sf::Vector2f(prevVert.X.Value, prevVert.Y.Value), CurveColor, sf::Vector2f(0.f, textureDimensions.y)));
			}

			Vector2 inverse(-newVertex + Points.at(i));
			newVertex += Points.at(i);

			sf::Vertex nextVert(sf::Vector2f(newVertex.X.Value, newVertex.Y.Value), CurveColor, sf::Vector2f(textureDimensions.x, 0.f));
			sf::Vertex invertVert(sf::Vector2f(inverse.X.Value, inverse.Y.Value), CurveColor, textureDimensions);
			if (i >= 1)
			{
				//Always connect the furthest vertex first to prevent the triangle fan from inverting itself.
				const sf::Vertex& prev = Vertices[Vertices.getVertexCount() - 1];

				//The vertex order may vary, always connect to closest vertex from the previous point.
				float distNext = (std::powf(prev.position.x - nextVert.position.x, 2.f) + std::powf(prev.position.y - nextVert.position.y, 2.f));
				float distInvert = (std::powf(prev.position.x - invertVert.position.x, 2.f) + std::powf(prev.position.y - invertVert.position.y, 2.f));

				if (distInvert > distNext)
				{
					//Swap the vertices since the triangle fan would have crossed over itself otherwise.
					std::swap(nextVert, invertVert);
				}
			}

			Vertices.append(nextVert);
			Vertices.append(invertVert);
		}
	}
}

void CurveRenderComponent::SetCurveColor (sf::Color newCurveColor)
{
	if (CurveColor == newCurveColor)
	{
		return;
	}

	CurveColor = newCurveColor;
	for (size_t i = 0; i < Vertices.getVertexCount(); ++i)
	{
		Vertices[i].color = CurveColor;
	}
}

void CurveRenderComponent::SetCurveTexture (const Texture* newCurveTexture)
{
	if (CurveTexture == newCurveTexture)
	{
		return;
	}

	CurveTexture = newCurveTexture;
	if (CurveTexture == nullptr)
	{
		RenderStates.texture = nullptr;
	}
	else
	{
		RenderStates.texture = CurveTexture->GetTextureResource();
	}

	//If vertices are already generated, update their UVs to reflect the new texture dimensions.
	if (CurveTexture != nullptr && CurveThickness > 0.f)
	{
		Vector2 dimensions = CurveTexture->GetDimensions();

		/*
		The last two vertices should render the right side of the texture.
		0---------------3
		|  ____...---"" |
		1---------------2
		*/
		for (size_t i = 0; i+3 < Vertices.getVertexCount(); i += 4)
		{
			//Update the normals for the next four vertices
			Vertices[i].texCoords = sf::Vector2f(0.f, 0.f);
			Vertices[i+1].texCoords = sf::Vector2f(0.f, dimensions.Y.Value);
			Vertices[i+2].texCoords = sf::Vector2f(dimensions.X.Value, 0.f);
			Vertices[i+3].texCoords = sf::Vector2f(dimensions.X.Value, dimensions.Y.Value);
		}
	}
}
SD_END