/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EmptyRenderTarget.cpp
=====================================================================
*/

#include "EmptyRenderTarget.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::EmptyRenderTarget, SD::RenderTarget)
SD_BEGIN

void EmptyRenderTarget::InitProps ()
{
	Super::InitProps();

	Size = Vector2::ZERO_VECTOR;
}

void EmptyRenderTarget::Reset ()
{
	//Noop
}

void EmptyRenderTarget::GetSize (Int& outWidth, Int& outHeight) const
{
	outWidth = Size.X.ToInt();
	outHeight = Size.Y.ToInt();
}

Vector2 EmptyRenderTarget::GetSize () const
{
	return Size;
}

void EmptyRenderTarget::SetSize (const Vector2& newSize)
{
	Size = newSize;
}
SD_END

#endif