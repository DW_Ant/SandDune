/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BorderRenderComponent.cpp
=====================================================================
*/

#include "BorderRenderComponent.h"
#include "RenderTarget.h"
#include "Texture.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::BorderRenderComponent, SD::RenderComponent)
SD_BEGIN

void BorderRenderComponent::InitProps ()
{
	Super::InitProps();

	DrawType = DT_Tiled;
	BaseSize = Vector2(1.f, 1.f);
	Pivot = Vector2::ZERO_VECTOR;
	TextureScale = Vector2(1.f, 1.f);
	BorderColor = Color::WHITE;
	BorderThickness = 8.f;
	bAutoGenerateVerts = true;

	LatestSize = Vector2::ZERO_VECTOR;
}

void BorderRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const BorderRenderComponent* borderTemplate = dynamic_cast<const BorderRenderComponent*>(objTemplate))
	{
		SetAutoGenerateVerts(false);
		DrawType = borderTemplate->DrawType;
		SetBaseSize(borderTemplate->ReadBaseSize());
		SetPivot(borderTemplate->ReadPivot());
		TextureScale = borderTemplate->TextureScale;
		BorderTexture = borderTemplate->BorderTexture;
		BorderColor = borderTemplate->BorderColor;
		BorderThickness = borderTemplate->BorderThickness;
		SetAutoGenerateVerts(true);
	}
}

void BorderRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	if (bAutoGenerateVerts)
	{
		Vector2 curScale = (Vector2::SFMLtoSD(projectionData.BaseSize) * BaseSize);
		if (!curScale.IsNearlyEqual(LatestSize))
		{
			LatestSize = curScale;
			GenerateBorderVertices();
		}
	}

	RenderStates.transform = sf::Transform::Identity;
	RenderStates.transform.translate(projectionData.Position);
	RenderStates.transform.scale(projectionData.Scale);
	RenderStates.transform.rotate(projectionData.Rotation);

	renderTarget->Draw(this, camera, Vertices, RenderStates);
}

Aabb BorderRenderComponent::GetAabb () const
{
	return Aabb(BaseSize.X, 0.f, BaseSize.Y, Vector3::ZERO_VECTOR);
}

void BorderRenderComponent::ApplyDrawModeTiled (const Vector2& tileSizeMultiplier)
{
	DrawType = DT_Tiled;
	TextureScale = tileSizeMultiplier;

	if (bAutoGenerateVerts)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::ApplyDrawModeStretched (const Vector2& numRepeats)
{
	DrawType = DT_Stretched;
	TextureScale = numRepeats;

	if (bAutoGenerateVerts)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::SetBaseSize (const Vector2& newBaseSize)
{
	if (BaseSize.IsNearlyEqual(newBaseSize))
	{
		return;
	}

	BaseSize = newBaseSize;
}

void BorderRenderComponent::SetPivot (const Vector2& newPivot)
{
	if (Pivot.IsNearlyEqual(newPivot))
	{
		return;
	}

	Pivot = newPivot;

	if (bAutoGenerateVerts)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::SetBorderTexture (const Texture* newBorderTexture)
{
	if (newBorderTexture == BorderTexture)
	{
		return;
	}

	bool changedDimensions = true;
	if (BorderTexture != nullptr && newBorderTexture != nullptr)
	{
		//If the texture sizes are equal, there's no need to generate vertices.
		changedDimensions = (!BorderTexture->GetDimensions().IsNearlyEqual(newBorderTexture->GetDimensions()));
	}

	BorderTexture = newBorderTexture;

	if (BorderTexture == nullptr)
	{
		RenderStates.texture = nullptr;
	}
	else
	{
		RenderStates.texture = BorderTexture->GetTextureResource();
	}

	//There's no need to regenerate vertices if the texture is cleared since solid colors don't care about the vertices' uv.
	if (bAutoGenerateVerts && changedDimensions && BorderTexture != nullptr)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::SetBorderColor (Color newBorderColor)
{
	if (BorderColor == newBorderColor)
	{
		return;
	}

	BorderColor = newBorderColor;

	if (bAutoGenerateVerts)
	{
		for (size_t vertIdx = 0; vertIdx < Vertices.getVertexCount(); ++vertIdx)
		{
			Vertices[vertIdx].color = BorderColor.Source;
		}
	}
}

void BorderRenderComponent::SetBorderThickness (Float newBorderThickness)
{
	if (BorderThickness.IsCloseTo(newBorderThickness))
	{
		return;
	}

	BorderThickness = newBorderThickness;
	if (OnThicknessChanged.IsBounded())
	{
		OnThicknessChanged(BorderThickness);
	}

	if (bAutoGenerateVerts)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::SetAutoGenerateVerts (bool newAutoGenerateVerts)
{
	bAutoGenerateVerts = newAutoGenerateVerts;
	
	if (bAutoGenerateVerts)
	{
		GenerateBorderVertices();
	}
}

void BorderRenderComponent::GenerateBorderVertices ()
{
	if (LatestSize.IsEmpty())
	{
		return;
	}

	Vertices.setPrimitiveType(sf::TriangleStrip);

	float colA = BorderThickness.Value;
	sf::Vector2f colB((LatestSize.X - BorderThickness).Value, (LatestSize.Y - BorderThickness).Value);
	sf::Vector2f textureSize(1.f, 1.f); //Default to 0-1 uv coordinates if no texture is chosen.

	/*
	Same as textureSize.x, but this coordinates is used for the vertical borders. This is an important distinction to make for DT_Tiled draw types.
	Otherwise the vertical borders' texture uv would be based on the size of the x dimensions.
	*/
	float altTextureSizeX = textureSize.x;

	if (BorderTexture != nullptr)
	{
		textureSize = Vector2::SDtoSFML(BorderTexture->GetDimensions());
	}

	if (TextureScale.X != 0.f && TextureScale.Y != 0.f)
	{
		if (DrawType == DT_Tiled)
		{
			textureSize.x = ((LatestSize.X - (BorderThickness * 2.f)) / TextureScale.X).Value;
			altTextureSizeX = ((LatestSize.Y - (BorderThickness * 2.f)) / TextureScale.X).Value;
			textureSize.y = (BorderThickness / TextureScale.Y).Value;
		}
		else //stretched
		{
			textureSize.x *= TextureScale.X.Value;
			textureSize.y *= TextureScale.Y.Value;
			altTextureSizeX = textureSize.x;
		}
	}
	
	/*
	Construct this vertex mesh
	   1---3--------------5---6&7
	   | \ |""""---...__  | / |
	22&0---2&23-----------4&9-8
	   |\  |              |\  |
	   | | |              | | |
	   | | |              | | |
	   |  \|              |  \|
	20&19--17&21------15&11---10
	   | / | __..---"""   | \ |
	   18--16---------14&13---12
	*/
	Vertices.resize(24);

	//First construct vertices where the origin is related to the top left corner.

	//Top left corner
	Vertices[0] = sf::Vertex(sf::Vector2f(0.f, colA), BorderColor.Source, sf::Vector2f(0.f, 0.f));
	Vertices[1] = sf::Vertex(sf::Vector2f(0.f, 0.f), BorderColor.Source, sf::Vector2f(textureSize.x * 0.5f, 0.f));
	Vertices[2] = sf::Vertex(sf::Vector2f(colA, colA), BorderColor.Source, sf::Vector2f(0.f, textureSize.y));
	Vertices[3] = sf::Vertex(sf::Vector2f(colA, 0.f), BorderColor.Source, sf::Vector2f(0.f, 0.f));

	//Top right corner
	Vertices[4] = sf::Vertex(sf::Vector2f(colB.x, colA), BorderColor.Source, textureSize);
	Vertices[5] = sf::Vertex(sf::Vector2f(colB.x, 0.f), BorderColor.Source, sf::Vector2f(textureSize.x, 0.f));
	Vertices[6] = sf::Vertex(sf::Vector2f(LatestSize.X.Value, 0.f), BorderColor.Source, sf::Vector2f(textureSize.x * 0.5f, 0.f));
	Vertices[7] = Vertices[6];
	Vertices[7].texCoords.x *= -1.f;
	Vertices[8] = sf::Vertex(sf::Vector2f(LatestSize.X.Value, colA), BorderColor.Source, sf::Vector2f(0.f, 0.f));
	Vertices[9] = Vertices[4];
	Vertices[9].texCoords.x = 0.f;

	//Bottom right corner
	Vertices[10] = sf::Vertex(sf::Vector2f(LatestSize.X.Value, colB.y), BorderColor.Source, sf::Vector2f(altTextureSizeX, 0.f));
	Vertices[11] = sf::Vertex(sf::Vector2f(colB.x, colB.y), BorderColor.Source, sf::Vector2f(altTextureSizeX, textureSize.y));
	Vertices[12] = sf::Vertex(sf::Vector2f(LatestSize.X.Value, LatestSize.Y.Value), BorderColor.Source, sf::Vector2f(textureSize.x * 0.5f, 0.f));
	Vertices[13] = sf::Vertex(sf::Vector2f(colB.x, LatestSize.Y.Value), BorderColor.Source, sf::Vector2f(textureSize.x, 0.f));
	Vertices[14] = Vertices[13];
	Vertices[14].texCoords = sf::Vector2f(0.f, 0.f);
	Vertices[15] = Vertices[11];
	Vertices[15].texCoords.x = 0.f;

	//Bottom left corner
	Vertices[16] = sf::Vertex(sf::Vector2f(colA, LatestSize.Y.Value), BorderColor.Source, sf::Vector2f(textureSize.x, 0.f));
	Vertices[17] = sf::Vertex(sf::Vector2f(colA, colB.y), BorderColor.Source, sf::Vector2f(textureSize.x, textureSize.y));
	Vertices[18] = sf::Vertex(sf::Vector2f(0.f, LatestSize.Y.Value), BorderColor.Source, sf::Vector2f(textureSize.x * 0.5f, 0.f));
	Vertices[19] = sf::Vertex(sf::Vector2f(0.f, colB.y), BorderColor.Source, sf::Vector2f(textureSize.x, 0.f));
	Vertices[20] = Vertices[19];
	Vertices[20].texCoords.x = 0.f;
	Vertices[21] = Vertices[17];
	Vertices[21].texCoords.x = 0.f;

	//Connecting back to top left corner
	Vertices[22] = Vertices[0];
	Vertices[22].texCoords.x = altTextureSizeX;
	Vertices[23] = Vertices[2];
	Vertices[23].texCoords.x = altTextureSizeX;

	//Next apply Pivot
	if (!Pivot.IsEmpty())
	{
		sf::Vector2f offset = Vector2::SDtoSFML(Pivot * LatestSize);
		for (size_t i = 0; i < Vertices.getVertexCount(); ++i)
		{
			Vertices[i].position -= offset;
		}
	}
}
SD_END