/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphicsEngineComponent.cpp
=====================================================================
*/

#include "ColorRenderComponent.h"
#include "FontPool.h"
#include "GraphicsEngineComponent.h"
#include "PlanarDrawLayer.h"
#include "PlanarTransformUpdater.h"
#include "SceneDrawLayer.h"
#include "SceneEntity.h"
#include "SceneTransformUpdater.h"
#include "TextureFile.h"
#include "TexturePool.h"
#include "Window.h"

IMPLEMENT_ENGINE_COMPONENT(SD::GraphicsEngineComponent)
SD_BEGIN

Int GraphicsEngineComponent::DEFAULT_DPI_SCALE = 96; //Window's default DPI scaling factor.
Int GraphicsEngineComponent::DpiScale = 96;

GraphicsEngineComponent::GraphicsEngineComponent () : Super()
{
	bTickingComponent = true;

	PlanarUpdater = nullptr;
	SceneUpdater = nullptr;
	RenderComponentHashNumber = 0;

	bInitializeDefaultViewport = true;
}

void GraphicsEngineComponent::RegisterObjectHash ()
{
	Super::RegisterObjectHash();

	RenderComponentHashNumber = Engine::FindEngine()->RegisterObjectHash(TXT("Render Component"));
}

void GraphicsEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

#ifdef PLATFORM_WINDOWS
	//Windows should be notified as soon as possible before anything is constructed that depends on DPI.
	static bool notifiedWindowsDpiAwareness = false;
	if (!notifiedWindowsDpiAwareness)
	{
		notifiedWindowsDpiAwareness = true;
		if (!SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_SYSTEM_AWARE))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to notify Windows of this application's DPI awareness. This may introduce scaling issues. Error code: %s"), DString::MakeString(GetLastError()));
			//Fallthrough - try to start the system anyways.
		}
	}
#endif

#ifdef DEBUG_MODE
	GraphicsLog.Log(LogCategory::LL_Log, TXT("Initializing Graphics Engine. . ."));
#endif

	if (!sf::Shader::isAvailable())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, (TXT("Your system does not support shaders.")));
	}

#ifdef DEBUG_MODE
	//Display an early crash for not running the asset exporter.
	FileAttributes reqTexture(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("64-Grid.") + TextureFile::FILE_EXTENSION);
	if (!OS_CheckIfFileExists(reqTexture))
	{
		Engine::FindEngine()->FatalError(TXT("64-Grid.txtr file is missing from Content/Engine/Graphics directory. Be sure to run the Asset Exporter to generate content files."));
	}
#endif

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_TRANSFORM_UPDATE, TICK_GROUP_PRIORITY_TRANSFORM_UPDATE);
	localEngine->CreateTickGroup(TICK_GROUP_RENDER, TICK_GROUP_PRIORITY_RENDER);

	//Create the various resource pools
	if (TexturePool::FindTexturePool() == nullptr)
	{
		TexturePool::CreateObject();
	}

	if (FontPool::FindFontPool() == nullptr)
	{
		FontPool::CreateObject();
	}

	PlanarUpdater = PlanarTransformUpdater::CreateObject();
	SceneUpdater = SceneTransformUpdater::CreateObject();

	if (bInitializeDefaultViewport)
	{
		//Setup the primary render target and its camera
		PrimaryWindow = Window::CreateObject();
		PrimaryWindow->SetResource(new sf::RenderWindow(sf::VideoMode(800, 600), ProjectName.ToCString()));
		PrimaryWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GraphicsEngineComponent, HandleMainWindowEvent, void, const sf::Event&));
		PrimaryWindow->SetDestroyCamerasOnDestruction(false);
		PrimaryWindow->SetDestroyDrawLayersOnDestruction(false);
		DpiScale = OS_GetSystemDpi(PrimaryWindow.Get());

		//Create base DrawLayers
		CanvasDrawLayer = PlanarDrawLayer::CreateObject();
		CanvasDrawLayer->SetDrawPriority(PlanarDrawLayer::CANVAS_LAYER_PRIORITY);
		PrimaryWindow->RegisterDrawLayer(CanvasDrawLayer.Get());
	}

	if (DpiScale <= 0)
	{
		DpiScale = DEFAULT_DPI_SCALE;
	}
}

void GraphicsEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	ImportEngineTextures();
}

void GraphicsEngineComponent::ShutdownComponent ()
{
	if (PrimaryWindow.IsValid())
	{
		PrimaryWindow->Destroy();
	}

	if (CanvasDrawLayer.IsValid())
	{
		CanvasDrawLayer->Destroy();
	}

	if (PlanarUpdater != nullptr)
	{
		PlanarUpdater->Destroy();
		PlanarUpdater = nullptr;
	}

	if (SceneUpdater != nullptr)
	{
		SceneUpdater->Destroy();
		SceneUpdater = nullptr;
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (localTexturePool != nullptr)
	{
		localTexturePool->Destroy();
	}

	FontPool* localFontPool = FontPool::FindFontPool();
	if (localFontPool != nullptr)
	{
		localFontPool->Destroy();
	}

	Super::ShutdownComponent();
}

void GraphicsEngineComponent::SetInitializeDefaultViewport (bool bNewInitializeDefaultViewport)
{
	if (OwningEngine != nullptr && OwningEngine->IsInitialized())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot assign GraphicsEngineComponent::bInitializeDefaultViewport after the engine is initialized."));
		return;
	}

	bInitializeDefaultViewport = bNewInitializeDefaultViewport;
}

Int GraphicsEngineComponent::GetDpiScale ()
{
	return DpiScale;
}

Float GraphicsEngineComponent::ConvertMillimetersToPixels (Float millimeters)
{
	static const Float inchesPerMm = 0.039370079f;
	return (millimeters * (DpiScale.ToFloat() * inchesPerMm));
}

Float GraphicsEngineComponent::ConvertPixelsToMillimeters (Float pixels)
{
	static const Float mmPerInch = 25.4f;
	return (pixels * mmPerInch) / DpiScale.ToFloat();
}

SceneEntity* GraphicsEngineComponent::DrawLine (const Vector3& endPointA, const Vector3& endPointB, Float lineThickness, Color lineColor, Float lifeSpan, SceneDrawLayer* drawLayer)
{
	SceneEntity* result = SceneEntity::CreateObject();
	if (lifeSpan > 0.f)
	{
		LifeSpanComponent* expireComp = LifeSpanComponent::CreateObject();
		if (result->AddComponent(expireComp))
		{
			expireComp->LifeSpan = lifeSpan;
		}
		else
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to draw a line because it's unable to attach a LifeSpan component to the Entity."));
			result->Destroy(); //Destroy here since the caller probably assumes that this entity will auto expire since they specified a positive lifeSpan in param.
			return nullptr;
		}
	}

	ColorRenderComponent* line = ColorRenderComponent::CreateObject();
	if (result->AddComponent(line))
	{
		line->SolidColor = lineColor;
		line->SetShape(ColorRenderComponent::ST_Rectangle);
		line->SetBaseSize((endPointB - endPointA).VSize(), lineThickness);

		if (drawLayer == nullptr)
		{
			line->RegisterToDefaultSceneLayers();
		}
		else
		{
			drawLayer->RegisterSingleComponent(line);
		}

		result->SetTranslation(endPointA);
		result->SetRotation(Rotator(endPointB - endPointA));
	}

	return result;
}

SceneEntity* GraphicsEngineComponent::DrawCircle (const Vector3& center, Float radius, Color lineColor, Float lifeSpan, SceneDrawLayer* drawLayer)
{
	SceneEntity* result = SceneEntity::CreateObject();
	if (lifeSpan > 0.f)
	{
		LifeSpanComponent* expireComp = LifeSpanComponent::CreateObject();
		if (result->AddComponent(expireComp))
		{
			expireComp->LifeSpan = lifeSpan;
		}
		else
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to draw a circle at %s because it's unable to attach a LifeSpan component to the Entity."), center);
			result->Destroy(); //Destroy here since the caller probably assumes that this entity will auto expire since they specified a positive lifeSpan in param.
			return nullptr;
		}
	}

	ColorRenderComponent* circle = ColorRenderComponent::CreateObject();
	if (result->AddComponent(circle))
	{
		circle->SolidColor = lineColor;
		circle->SetShape(ColorRenderComponent::ST_Circle);
		circle->SetBaseSize(radius * 2.f, 1.f);
		circle->SetPivot(Vector2(0.5f, 0.5f));

		if (drawLayer == nullptr)
		{
			circle->RegisterToDefaultSceneLayers();
		}
		else
		{
			drawLayer->RegisterSingleComponent(circle);
		}

		result->SetTranslation(center);
	}

	return result;
}

void GraphicsEngineComponent::AddDefaultSceneDrawLayer (SceneDrawLayer* newLayer)
{
	ContainerUtils::AddUnique(OUT DefaultSceneLayers, newLayer);
}

void GraphicsEngineComponent::RemoveDefaultSceneDrawLayer (SceneDrawLayer* targetLayer)
{
	ContainerUtils::RemoveItem(OUT DefaultSceneLayers, targetLayer);
}

void GraphicsEngineComponent::ImportEngineTextures ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("64-Grid.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("BorderTexture.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("DebuggingTexture.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("DottedCircle.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("VerticalGradient.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("WhiteFrame.txtr")));
}

void GraphicsEngineComponent::HandleMainWindowEvent (const sf::Event& newEvent)
{
	if (newEvent.type == sf::Event::Closed)
	{
		Engine::FindEngine()->Shutdown();
	}
}
SD_END