/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Color.cpp
=====================================================================
*/

#include "Color.h"

SD_BEGIN
const Color Color::WHITE(255, 255, 255, 255);
const Color Color::BLACK(0, 0, 0, 255);
const Color Color::RED(255, 0, 0, 255);
const Color Color::YELLOW(255, 255, 0, 255);
const Color Color::GREEN(0, 255, 0, 255);
const Color Color::CYAN(0, 255, 255, 255);
const Color Color::BLUE(0, 0, 255, 255);
const Color Color::MAGENTA(255, 0, 255, 255);
const Color Color::INVISIBLE(0, 0, 0, 0);

Color::Color ()
{
	Source = sf::Color();
}

Color::Color (const Color& otherColor)
{
	Source = otherColor.Source;
}

Color::Color (const sf::Color& otherColor)
{
	Source = otherColor;
}

Color::Color (uint8 red, uint8 green, uint8 blue, uint8 alpha)
{
	sf::Uint8 sfRed = red;
	sf::Uint8 sfGreen = green;
	sf::Uint8 sfBlue = blue;
	sf::Uint8 sfAlpha = alpha;

	Source = sf::Color(sfRed, sfGreen, sfBlue, sfAlpha);
}

Color::Color (uint32 colors)
{
	Source = sf::Color(colors);
}

void Color::ResetToDefaults ()
{
	Source = sf::Color::Black;
}

DString Color::ToString () const
{
	return DString::CreateFormattedString(TXT("(R=%s, G=%s, B=%s, A=%s)"), Int(Source.r), Int(Source.g), Int(Source.b), Int(Source.a));
}

void Color::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("R"), CommonPropertyRegex::Int);
	if (!varData.IsEmpty())
	{
		Source.r = Int(varData).Value;
	}

	varData = ParseVariable(str, TXT("G"), CommonPropertyRegex::Int);
	if (!varData.IsEmpty())
	{
		Source.g = Int(varData).Value;
	}

	varData = ParseVariable(str, TXT("B"), CommonPropertyRegex::Int);
	if (!varData.IsEmpty())
	{
		Source.b = Int(varData).Value;
	}

	varData = ParseVariable(str, TXT("A"), CommonPropertyRegex::Int);
	if (!varData.IsEmpty())
	{
		Source.a = Int(varData).Value;
	}
}

size_t Color::GetMinBytes () const
{
	return sizeof(Source);
}

void Color::Serialize (DataBuffer& outData) const
{
	//Push an unsigned int32 from data buffer instead of an Int to avoid signed/unsigned mismatch with conversion.
	const int numBytes = sizeof(uint32);
	char charArray[numBytes];
	uint32 sourceNum = Source.toInteger();
	memcpy(charArray, &sourceNum, numBytes);

	//Reverse bytes if needed
	if (outData.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	outData.AppendBytes(charArray, numBytes);
}

bool Color::Deserialize (const DataBuffer& dataBuffer)
{
	//Pull an unsigned int32 from data buffer instead of an Int to avoid signed/unsigned mismatch with conversion.
	const int numBytes = sizeof(uint32);
	char rawData[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	uint32 allChannels;
	memcpy(&allChannels, rawData, numBytes);

	Source = sf::Color(allChannels);
	return true;
}

Color Color::Lerp (Float ratio, Color min, Color max)
{
	Int r = ((ratio * static_cast<float>((max.Source.r - min.Source.r))) + min.Source.r).ToInt();
	Int g = ((ratio * static_cast<float>((max.Source.g - min.Source.g))) + min.Source.g).ToInt();
	Int b = ((ratio * static_cast<float>((max.Source.b - min.Source.b))) + min.Source.b).ToInt();
	Int a = ((ratio * static_cast<float>((max.Source.a - min.Source.a))) + min.Source.a).ToInt();

	r = Utils::Clamp<Int>(r, 0, 255);
	g = Utils::Clamp<Int>(g, 0, 255);
	b = Utils::Clamp<Int>(b, 0, 255);
	a = Utils::Clamp<Int>(a, 0, 255);

	return Color(static_cast<sf::Uint8>(r.Value), static_cast<sf::Uint8>(g.Value), static_cast<sf::Uint8>(b.Value), static_cast<sf::Uint8>(a.Value));
}

bool operator== (const Color &left, const Color &right)
{
	return left.Source == right.Source;
}

bool operator!= (const Color &left, const Color &right)
{
	return left.Source != right.Source;
}

Color operator+ (const Color &left, const Color &right)
{
	return Color(left.Source + right.Source);
}

Color operator- (const Color &left, const Color &right)
{
	return Color(left.Source - right.Source);
}

Color operator* (const Color &left, const Color &right)
{
	return Color(left.Source * right.Source);
}

Color& operator+= (Color &left, const Color &right)
{
	left.Source += right.Source;
	return left;
}

Color& operator-= (Color &left, const Color &right)
{
	left.Source -= right.Source;
	return left;
}

Color& operator*= (Color &left, const Color &right)
{
	left.Source *= right.Source;
	return left;
}
SD_END