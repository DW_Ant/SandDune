/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScreenCapture.cpp
=====================================================================
*/

#include "RenderComponent.h"
#include "RenderTarget.h"
#include "RenderTexture.h"
#include "ScreenCapture.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::ScreenCapture, SD::Object)
SD_BEGIN

void ScreenCapture::InitProps ()
{
	Super::InitProps();

	CaptureTarget = nullptr;
	PixelMapType = PMT_PerPixel;
	AlphaThreshold = 0.25f;
	bDetectCompsWithinRenderTextures = true;
	DefaultColor = Color::BLACK;
	PixelMapGeneratorTexture = nullptr;
	PixelMap = nullptr;
	NumCapturedComponents = 0;
}

void ScreenCapture::Destroyed ()
{
	ClearCapturedData();

	Super::Destroyed();
}

bool ScreenCapture::GenerateCaptureTexture (RenderTarget* inCaptureTarget, Color defaultColor)
{
	ClearCapturedData();

	DefaultColor = defaultColor;
	CaptureTarget = inCaptureTarget;
	if (CaptureTarget == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot generate texture capture data without specifying a RenderTarget to capture data from."));
		return false;
	}

	Int width;
	Int height;
	CaptureTarget->GetSize(OUT width, OUT height);
	CapturedTexture = RenderTexture::CreateObject();
	CapturedTexture->SetAutoDraw(false);
	CapturedTexture->ResetColor = defaultColor;

#ifdef DEBUG_MODE
	CapturedTexture->DebugName = TXT("ScreenCapture's CaptureTexture");
#endif

	if (!CapturedTexture->CreateResource(width, height, false))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to generate screen capture. Unable to create a %sx%s texture."), width, height);
		ClearCapturedData();
		return false;
	}

	if (PixelMapType != PMT_None)
	{
		if (AlphaThreshold <= 0.f)
		{
			//Switch to a faster algorithm since it can't distinguish invisible pixels from unused pixels with the PerPixel approach.
			PixelMapType = PMT_AABB;
		}

		if (PixelMapType == PMT_PerPixel)
		{
			PixelMapGeneratorTexture = RenderTexture::CreateObject();
			PixelMapGeneratorTexture->ResetColor = Color::INVISIBLE;
			PixelMapGeneratorTexture->SetAutoDraw(false);

#ifdef DEBUG_MODE
			PixelMapGeneratorTexture->DebugName = TXT("ScreenCapture's Pixel Map Generator Texture");
#endif

			if (!PixelMapGeneratorTexture->CreateResource(width, height, false))
			{
				GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to generate screen capture since the PixelMapGeneratorTexture is unable to be created."));
				ClearCapturedData();
				return false;
			}
		}

		size_t numPixels = (width * height).ToUnsignedInt();
		PixelMap = new RenderComponent*[numPixels];
		for (size_t i = 0; i < numPixels; ++i)
		{
			PixelMap[i] = nullptr;
		}
	}

	NumCapturedComponents = 0;
	CapturedTexture->Reset(); //Apply default color before the CaptureTarget draws components onto the texture.
	CaptureTarget->SetScreenCapture(this);
	CaptureTarget->GenerateScene();

	return true;
}

RenderComponent* ScreenCapture::GetMappedPixel (Int texturePosX, Int texturePosY) const
{
	if (PixelMap == nullptr || CapturedTexture == nullptr)
	{
		return nullptr;
	}

	Int texWidth;
	Int texHeight;
	CapturedTexture->GetSize(OUT texWidth, OUT texHeight);

	size_t idx = ((texturePosY * texWidth) + texturePosX).ToUnsignedInt();

	if (idx >= (texWidth * texHeight))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to get mapped pixel since the pixel at coordinates (%s, %s) is beyond the range of a %sx%s texture."), texturePosX, texturePosY, texWidth, texHeight);
		return nullptr;
	}

	return PixelMap[idx];
}

void ScreenCapture::AddRenderTexture (RenderTexture* renderTex, RenderComponent* renderComp, const Transformation::SScreenProjectionData& projection)
{
	if (renderComp == nullptr || renderTex == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to add render texture to the screen capture since the RenderComponent or the RenderTexture is not specified."));
		return;
	}

	Int width;
	Int height;
	renderTex->GetSize(OUT width, OUT height);

	ScreenCapture* innerCapture = ScreenCapture::CreateObject();
	innerCapture->OnIsRelevant = OnIsRelevant;
	innerCapture->SetPixelMapType(GetPixelMapType());
	innerCapture->SetAlphaThreshold(GetAlphaThreshold());
	innerCapture->SetDetectCompsWithinRenderTextures(true);

	if (innerCapture->GenerateCaptureTexture(renderTex, Color::INVISIBLE))
	{
		if (innerCapture->GetNumCapturedComponents() > 0)
		{
			//Draw the generated render texture on top of the outer capture. It's done this way to exclude render components that were filtered out.
			RenderTexture* innerTexture = innerCapture->GetCapturedTexture();
			CHECK(innerTexture->GetResource() != nullptr)

			sf::Sprite sprite;
			sprite.setTexture(innerTexture->GetResource()->getTexture());
			sprite.setPosition(projection.Position);
			sprite.setScale(projection.Scale);
			CapturedTexture->Draw(renderComp, nullptr, sprite);

			OverwritePixelMapFrom(*innerCapture, renderComp, projection);
		}
	}

	innerCapture->Destroy();
}

void ScreenCapture::SetPixelMapType (EPixelMapType newPixelMapType)
{
	PixelMapType = newPixelMapType;
}

void ScreenCapture::SetAlphaThreshold (Float newAlphaThreshold)
{
	AlphaThreshold = Utils::Clamp<Float>(newAlphaThreshold, 0.f, 1.f);
}

void ScreenCapture::SetDetectCompsWithinRenderTextures (bool newDetectCompsWithinRenderTextures)
{
	bDetectCompsWithinRenderTextures = newDetectCompsWithinRenderTextures;
}

void ScreenCapture::OverwritePixelMapFrom (const ScreenCapture& otherCapture, RenderComponent* renderComp, const Transformation::SScreenProjectionData& projection)
{
	//Note: The projection is rendComp's projection. Not the inner render component's projection.
	if (otherCapture.GetCapturedTexture() == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to overwrite pixel map from %s since the other screen capture has not yet generated a capture texture."), otherCapture.ToString());
		return;
	}

	if (renderComp == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to overwrite pixel map from %s since a render component is not specified."), otherCapture.ToString());
		return;
	}

	//Identify which pixels may be affected by the given component drawn on this capture.
	Aabb boundingBox = renderComp->GetAabb();
	sf::Vector2f projectionScale = projection.GetFinalSize();
	boundingBox.Depth *= projectionScale.x;
	boundingBox.Width *= projectionScale.y;
	boundingBox.Center.X += projection.Position.x + (boundingBox.Depth * 0.5f);
	boundingBox.Center.Y += projection.Position.y + (boundingBox.Width * 0.5f);

	Int intLeftCol = Int(boundingBox.GetBackward());
	Int intRightCol = Int(boundingBox.GetForward());
	Int intTopRow = Int(boundingBox.GetLeft());
	Int intBottomRow = Int(boundingBox.GetRight());

	//Shift all borders one pixel inward to handle float rounding issues. Rounding errors may cause the pixel override to be out of range by one pixel.
	++intLeftCol;
	--intRightCol;
	++intTopRow;
	--intBottomRow;

	Int width;
	Int height;
	CaptureTarget->GetSize(OUT width, OUT height);
	
	size_t leftCol = Utils::Max<Int>(intLeftCol, 0).ToUnsignedInt();
	size_t rightCol = Utils::Min(intRightCol, width).ToUnsignedInt();
	size_t topRow = Utils::Max<Int>(intTopRow, 0).ToUnsignedInt();
	size_t bottomRow = Utils::Min(intBottomRow, height).ToUnsignedInt();

	Vector2 innerToOuterRatio(1.f / projection.Scale.x, 1.f / projection.Scale.y);
	Float xOffset = 0.f;
	Float yOffset = 0.f;
	if (intLeftCol < 0)
	{
		//The sprite's left border is clamped. Need to start counting from the new x pixel.
		xOffset = intLeftCol.ToFloat() * innerToOuterRatio.X;
	}

	if (intTopRow < 0)
	{
		//The sprite's top border is clamped. Need to start counting from the new y pixel.
		yOffset = intTopRow.ToFloat() * innerToOuterRatio.Y; 
	}

	std::vector<RenderComponent*> foundComps;

	size_t maxIdx = (width * height).ToUnsignedInt();
	for (size_t rowIdx = topRow; rowIdx < bottomRow; ++rowIdx)
	{
		for (size_t colIdx = leftCol; colIdx < rightCol; ++colIdx)
		{
			Vector2 pixelPos(Float::MakeFloat(colIdx-leftCol) * innerToOuterRatio.X, Float::MakeFloat(rowIdx-topRow) * innerToOuterRatio.Y);
			pixelPos.X -= xOffset;
			pixelPos.Y -= yOffset;

			size_t writeIdx = ((rowIdx * width) + colIdx).ToUnsignedInt();
			if (writeIdx >= maxIdx)
			{
				GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to overwrite pixel map beyond the pixel map's range. Accessing index %s of a pixel map of size %s. rowIdx=%s, colIdx=%s. PixelMap dimensions: %sx%s."), Int(writeIdx), Int(maxIdx), Int(rowIdx), Int(colIdx), width, height);
				continue;
			}

			PixelMap[writeIdx] = otherCapture.GetMappedPixel(pixelPos.X.ToInt(), pixelPos.Y.ToInt());
			if (PixelMap[writeIdx] != nullptr && ContainerUtils::FindInVector(foundComps, PixelMap[writeIdx]) == UINT_INDEX_NONE)
			{
				foundComps.push_back(PixelMap[writeIdx]);
			}
		}
	}

	NumCapturedComponents += Int(foundComps.size());
}

void ScreenCapture::ClearCapturedData ()
{
	if (CapturedTexture != nullptr)
	{
		CapturedTexture->Destroy();
		CapturedTexture = nullptr;
	}

	if (PixelMapGeneratorTexture != nullptr)
	{
		PixelMapGeneratorTexture->Destroy();
		PixelMapGeneratorTexture = nullptr;
	}

	if (PixelMap != nullptr)
	{
		delete[] PixelMap;
		PixelMap = nullptr;
	}
}

void ScreenCapture::ProcessRenderComponent (RenderComponent* comp, const Camera* cam)
{
	CHECK(comp != nullptr)
	if (!comp->IsAlwaysScreenCapRelevant(this) && OnIsRelevant.IsBounded() && !OnIsRelevant(comp))
	{
		return;
	}

	if (CapturedTexture != nullptr)
	{
		comp->Render(CapturedTexture.Get(), cam);
	}

	bool isMapUpdated = false;
	if (PixelMap != nullptr)
	{
		Int width;
		Int height;
		CapturedTexture->GetSize(OUT width, OUT height);
		size_t numPixels = (width * height).ToUnsignedInt();
		if (PixelMapType == PMT_PerPixel && PixelMapGeneratorTexture != nullptr)
		{
			sf::Uint8 transparencyThreshold = (255.f * AlphaThreshold).ToInt().Value;

			PixelMapGeneratorTexture->Reset();
			comp->Render(PixelMapGeneratorTexture, cam);
			PixelMapGeneratorTexture->Display();

			const sf::Texture& texture = PixelMapGeneratorTexture->GetResource()->getTexture();
			sf::Image textureData = texture.copyToImage();

			if (const sf::Uint8* pixelPtr = textureData.getPixelsPtr())
			{
				for (size_t i = 0; i < numPixels; ++i)
				{
					sf::Uint8 alphaChannel = pixelPtr[(i*4) + 3];
					if (alphaChannel >= transparencyThreshold)
					{
						PixelMap[i] = comp;
						isMapUpdated = true;
					}
				}
			}
		}
		else
		{
			//Use the faster AABB approach. Ignores rotation and transparency
			Aabb boundingBox = comp->GetAabb();
			const Transformation::SScreenProjectionData& projectionData = comp->GetOwnerTransform()->GetProjectionData(CapturedTexture.Get(), cam);
			sf::Vector2f projectionScale = projectionData.GetFinalSize();
			boundingBox.Depth *= projectionScale.x;
			boundingBox.Width *= projectionScale.y;
			boundingBox.Center.X += projectionData.Position.x + (boundingBox.Depth * 0.5f);
			boundingBox.Center.Y += projectionData.Position.y + (boundingBox.Width * 0.5f);

			size_t leftCol = Int(boundingBox.GetBackward()).ToUnsignedInt();
			size_t rightCol = Int(boundingBox.GetForward()).ToUnsignedInt();
			size_t topRow = Int(boundingBox.GetLeft()).ToUnsignedInt();
			size_t bottomRow = Int(boundingBox.GetRight()).ToUnsignedInt();
			size_t maxIdx = (width * height).ToUnsignedInt();

			leftCol = Utils::Max<size_t>(leftCol, 0);
			rightCol = Utils::Min(rightCol, width.ToUnsignedInt());
			topRow = Utils::Max<size_t>(topRow, 0);
			bottomRow = Utils::Min(bottomRow, height.ToUnsignedInt());

			for (size_t rowIdx = topRow; rowIdx < bottomRow; ++rowIdx)
			{
				for (size_t colIdx = leftCol; colIdx < rightCol; ++colIdx)
				{
					size_t pixelIdx = ((rowIdx * width) + colIdx).ToUnsignedInt();
					if (pixelIdx >= maxIdx)
					{
						break;
					}

					PixelMap[pixelIdx] = comp;
					isMapUpdated = true;
				}
			}
		}
	}

	if (isMapUpdated)
	{
		++NumCapturedComponents;
	}
}

void ScreenCapture::FinishRenderCycle ()
{
	if (CapturedTexture != nullptr)
	{
		CapturedTexture->Display();
	}
}
SD_END