/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TopDownCamera.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::TopDownCamera, SD::SceneCamera)
SD_BEGIN

void TopDownCamera::InitProps ()
{
	Super::InitProps();

	Zoom = 0.01f; //1 meter in the scene consumes about 1 cm of screen space.
}

void TopDownCamera::ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const
{
	Vector3 relativeDist = ReadAbsTranslation() - transform.ReadAbsTranslation();

	Float scalarMultiplier = GetScalarProjection();
	outProjectionData.Position.x = ((viewportSize.X * 0.5f) - (relativeDist.X * scalarMultiplier)).Value;
	outProjectionData.Position.y = ((viewportSize.Y * 0.5f) - (relativeDist.Y * scalarMultiplier)).Value;

	outProjectionData.BaseSize = Vector2::SDtoSFML(transform.ReadAbsScale().ToVector2());
	outProjectionData.Scale = sf::Vector2f(scalarMultiplier.Value, scalarMultiplier.Value);

	outProjectionData.Rotation = -transform.ReadAbsRotation().GetYaw(Rotator::RU_Degrees).Value;
}

void TopDownCamera::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	Vector2 deltaMousePos = pixelPos - (viewportSize * 0.5f);
	deltaMousePos /= GetScalarProjection();
	outLocation.X = AbsTranslation.X + deltaMousePos.X;
	outLocation.Y = AbsTranslation.Y + deltaMousePos.Y;
	outDirection = Rotator(0, Rotator::QUARTER_REVOLUTION, 0); //Look straight down
}

Float TopDownCamera::GetDistSquaredToPoint (const Vector3& worldLocation) const
{
	//This camera is aligned to Z-axis with its projection perpendicular to XY plane. The distance is simply the Z distances between point and camera.
	return std::powf((worldLocation.Z - AbsTranslation.Z).Value, 2.f);
}

Float TopDownCamera::GetScalarProjection () const
{
	Float dpi = GraphicsEngineComponent::GetDpiScale().ToFloat();
	const Float inchesPerCentimeter = 0.3937008f;

	return dpi * inchesPerCentimeter * Zoom;
}
SD_END