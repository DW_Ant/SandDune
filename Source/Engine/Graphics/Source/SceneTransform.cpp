/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransform.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
SceneTransform::SceneTransform () : Transformation(),
	Translation(Vector3::ZERO_VECTOR),
	Scale(1.f, 1.f, 1.f),
	GlobalScale(1.f),
	Rotation(),
	AbsTranslation(Vector3::ZERO_VECTOR),
	AbsScale(1.f, 1.f, 1.f),
	AbsRotation(),
	RelativeTo(nullptr),
	UpdateBehavior(UB_Seldom),
	AbsTransformDirty(false),
	TransformBroadcastState(BS_Ready)
{

}

SceneTransform::SceneTransform (const Vector3& inTranslation, const Vector3& inScale, Float inGlobalScale, Rotator inRotation, SceneTransform* inRelativeTo, EUpdateBehavior inUpdateBehavior) : Transformation(),
	Translation(inTranslation),
	Scale(inScale),
	GlobalScale(inGlobalScale),
	Rotation(inRotation),
	AbsTranslation(Vector3::ZERO_VECTOR),
	AbsScale(1.f, 1.f, 1.f),
	AbsRotation(),
	RelativeTo(nullptr),
	UpdateBehavior(UB_Manual), //Initialize it to something to produce defined behavior when SetUpdateBehavior is called.
	AbsTransformDirty(false), //Set to false here since we may need this to register to the updater if update behavior becomes seldom later.
	TransformBroadcastState(BS_Ready)
{
	SetUpdateBehavior(inUpdateBehavior);
	if (inRelativeTo != nullptr)
	{
		SetRelativeTo(inRelativeTo);
	}

	MarkAbsTransformDirty();
}

SceneTransform::SceneTransform (const SceneTransform& copyData) : Transformation(copyData),
	Translation(copyData.Translation),
	Scale(copyData.Scale),
	GlobalScale(copyData.GlobalScale),
	Rotation(copyData.Rotation),
	AbsTranslation(copyData.AbsTranslation),
	AbsScale(copyData.AbsScale),
	AbsRotation(copyData.AbsRotation),
	RelativeTo(copyData.RelativeTo),
	UpdateBehavior(UB_Manual), //Initialize it to something to produce defined behavior when SetUpdateBehavior is called.
	AbsTransformDirty(copyData.AbsTransformDirty),
	TransformBroadcastState(copyData.TransformBroadcastState)
{
	SetUpdateBehavior(copyData.UpdateBehavior);
}

SceneTransform::~SceneTransform ()
{
	if (UpdateBehavior == UB_Continuous)
	{
		GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
		if (localGraphicsEngine != nullptr)
		{
			if (SceneTransformUpdater* updater = localGraphicsEngine->GetSceneUpdater())
			{
				updater->RemovePermanentTransform(this);
			}
		}
	}
	else if (IsAbsTransformDirty() && UpdateBehavior == UB_Seldom)
	{
		GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
		if (localGraphicsEngine != nullptr)
		{
			if (SceneTransformUpdater* updater = localGraphicsEngine->GetSceneUpdater())
			{
				updater->RemoveTemporaryTransform(this);
			}
		}
	}
}

bool SceneTransform::IsWithinView (const RenderTarget* renderTarget, const Camera* camera)  const
{
	//TODO:  Implement SceneTransform::IsWithinView
	return true;
}

void SceneTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	const SceneCamera* sceneCam = dynamic_cast<const SceneCamera*>(cam);
	if (sceneCam == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to compute SceneTransform's Screen Space position since it doesn't know its relative position to a camera using a different coordinate system."));

		//Default to identity since we don't know which axis converges where in screen space.
		outProjectionData.Position = sf::Vector2f(0.f, 0.f);
		outProjectionData.BaseSize = sf::Vector2f(1.f, 1.f);
		outProjectionData.Scale = sf::Vector2f(1.f, 1.f);
		outProjectionData.Rotation = 0.f;
		return;
	}

	//The cameras will compute the relative coordinates due to various projections.
	sceneCam->ProjectToScreenCoordinates(target->GetSize(), *this, OUT outProjectionData);
}

void SceneTransform::TransformAabb (Aabb& outAabb) const
{
#if 0
	//Need to figure out scaling based on all degrees of rotation.
	Vector3 scaleByRotation(1.f, 1.f, 1.f);
	if (AbsRotation != Rotator::ZERO_ROTATOR)
	{
		float yawRadians = AbsRotation.GetYaw(Rotator::RU_Radians).Value;
		float pitchRadians = AbsRotation.GetPitch(Rotator::RU_Radians).Value;

		//The resulting Aabb scalar bounds are going to be slightly adjusted based on the rotation.

		//yaw
		scaleByRotation.X = Utils::Max(std::cos(yawRadians) * outAabb.Depth * 0.5f, std::sin(yawRadians) * outAabb.Width * 0.5f);
		scaleByRotation.Y = Utils::Max(std::sin(yawRadians) * outAabb.Depth * 0.5f, std::cos(yawRadians) * outAabb.Width * 0.5f);

		//Pitch

		//Roll
	}

	outAabb.Width *= AbsScale.Y;
	outAabb.Height *= AbsScale.Z;
	outAabb.Depth *= AbsScale.X;
	outAabb.Center += AbsTranslation;
#endif

	if (AbsRotation == Rotator::ZERO_ROTATOR)
	{
		//Use faster/simple method to stretch the bounding box
		outAabb.Width *= AbsScale.Y;
		outAabb.Height *= AbsScale.Z;
		outAabb.Depth *= AbsScale.X;
		outAabb.Center += AbsTranslation;
		return;
	}

	std::vector<Vector3> points;
	points.resize(8);
	//forward surface starting from top right corner
	points.at(0) = Vector3(outAabb.Depth * 0.5f, outAabb.Width * 0.5f, outAabb.Height * 0.5f);
	points.at(1) = Vector3(outAabb.Depth * 0.5f, outAabb.Width * 0.5f, -outAabb.Height * 0.5f);
	points.at(2) = Vector3(outAabb.Depth * 0.5f, -outAabb.Width * 0.5f, -outAabb.Height * 0.5f);
	points.at(3) = Vector3(outAabb.Depth * 0.5f, -outAabb.Width * 0.5f, outAabb.Height * 0.5f);

	//Back surface
	for (int i = 0; i < 4; ++i)
	{
		points.at(i+4) = points.at(i);
		points.at(i+4).X *= -1.f;
	}

	//Transform each point
	for (size_t i = 0; i < points.size(); ++i)
	{
		TransformMatrix matrix;
		matrix.Translate(points.at(i));

		//Scales and rotates this vertex about the origin before applying the given Aabb's translation
		matrix *= AbsTransform;
		points.at(i) = matrix.GetTranslation() + outAabb.Center;
	}

	//Look at each vertex to generate a new bounding box that encompasses all vertices.
	outAabb = Aabb::GenerateEncompassingAabb(points);
}

void SceneTransform::ResetTransform ()
{
	Translation = Vector3::ZERO_VECTOR;
	Scale = Vector3(1.f, 1.f, 1.f);
	GlobalScale = 1.f;
	Rotation = Rotator::ZERO_ROTATOR;
	MarkAbsTransformDirty();
}

TransformMatrix SceneTransform::ToTransformMatrix () const
{
	TransformMatrix transformMatrix; //Generate identity matrix
	ApplyTransformTo(OUT transformMatrix);

	return transformMatrix;
}

void SceneTransform::ApplyTransformTo (TransformMatrix& outMatrix) const
{
	outMatrix.Translate(Translation);
	outMatrix.Scale(Scale * GlobalScale);
	outMatrix.Rotate(Rotation);
}

void SceneTransform::CalculateAbsoluteTransform ()
{
	if (!IsAbsTransformDirty())
	{
		return;
	}

	if (RelativeTo != nullptr && RelativeTo->IsAbsTransformDirty())
	{
		//Normally this shouldn't happen unless CalculateAbsoluteTransform was called in the middle of the transform chain.
		//This will recursively call CalculateAbsoluteTransform on the root transforms first.
		RelativeTo->CalculateAbsoluteTransform();

		//Check the flag is cleared since this transform is going to rely on the parent transform being up to date.
		CHECK_INFO(!RelativeTo->IsAbsTransformDirty(), "Computing the absolute transform should have cleared the transform's dirty flag. Did the callback for OnTransformChanged change the transforms?")
	}

	AbsTransform = TransformMatrix::IDENTITY_TRANSFORM;
	if (RelativeTo != nullptr)
	{
		AbsTransform *= RelativeTo->AbsTransform;
	}
	AbsTransform *= ToTransformMatrix();

	CHECK(AbsTransform.IsValid())
	AbsTransform.CalcTransformAttributes(OUT AbsTranslation, OUT AbsScale, OUT AbsRotation);
	AbsTransformDirty = false;

	if (TransformBroadcastState == BS_Ready)
	{
		OnTransformChanged.Broadcast();
	}
	else
	{
		TransformBroadcastState = BS_Pending;
	}
}

void SceneTransform::ForEachTransformInBranch (const std::function<void(SceneTransform*)>& toExecute)
{
	toExecute(this);

	for (SceneTransform* curTransform : AttachedTransforms)
	{
		//Recursively execute the lambda in sub branches
		curTransform->ForEachTransformInBranch(toExecute);
	}
}

void SceneTransform::MarkAbsTransformDirty ()
{
	if (AbsTransformDirty)
	{
		return;
	}

	if (UpdateBehavior == UB_Seldom)
	{
		//Only register if the relative to is not already registered.
		if (RelativeTo == nullptr || (RelativeTo->UpdateBehavior != UB_Seldom) || !RelativeTo->IsAbsTransformDirty())
		{
			GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
			if (localGraphicsEngine != nullptr && localGraphicsEngine->GetSceneUpdater() != nullptr)
			{
				localGraphicsEngine->GetSceneUpdater()->RegisterTemporaryTransform(this);
			}
		}
	}

	AbsTransformDirty = true;
	for (SceneTransform* subTransform : AttachedTransforms)
	{
		if (!subTransform->IsAbsTransformDirty())
		{
			//Recursively mark sub transforms dirty
			subTransform->MarkAbsTransformDirty();
		}
	}
}

void SceneTransform::SetTransformChangedBroadcastLock (bool bLocked)
{
	//locking
	if (bLocked)
	{
		if (TransformBroadcastState == BS_Ready)
		{
			TransformBroadcastState = BS_Locked;
		}

		return;
	}

	//unlocking
	if (TransformBroadcastState == BS_Pending)
	{
		OnTransformChanged.Broadcast();
	}
	TransformBroadcastState = BS_Ready;
}

void SceneTransform::SetTranslation (const Vector3& newTranslation)
{
	Translation = newTranslation;
	MarkAbsTransformDirty();
}

void SceneTransform::SetScale (const Vector3& newScale)
{
	Scale = newScale;
	MarkAbsTransformDirty();
}

void SceneTransform::SetGlobalScale (Float newGlobalScale)
{
	GlobalScale = newGlobalScale;
	MarkAbsTransformDirty();
}

void SceneTransform::SetRotation (const Rotator& newRotation)
{
	Rotation = newRotation;
	MarkAbsTransformDirty();
}

void SceneTransform::SetRelativeTo (SceneTransform* newRelativeTo)
{
	if (RelativeTo == newRelativeTo)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("A scene transformation is already relative to the specified transform."));
		return;
	}

	if (RelativeTo != nullptr)
	{
		UINT_TYPE removedIdx = ContainerUtils::RemoveItem(OUT RelativeTo->AttachedTransforms, this);
		CHECK_INFO(removedIdx != INT_INDEX_NONE, "Something went wrong. A scene transform should not be able to be relative to another transform without being registered to its AttachedTransforms vector.")
	}

#if ENABLE_COMPLEX_CHECKING
	//Ensure this isn't setting a circular relativeTo
	const SceneTransform* checkRelativeTo = newRelativeTo;
	while (checkRelativeTo != nullptr)
	{
		CHECK_INFO(checkRelativeTo != this, "Circular relative transformation detected.  Cannot set a Transformation object relative to itself.")

		checkRelativeTo = checkRelativeTo->GetRelativeTo();
	}
#endif

	if (UpdateBehavior == UB_Seldom)
	{
		//Check if this transform needs to be added to the updater.
		//For seldomly updated transforms, they are automatically registered to the updater when their transform changed while their relativeTo is not dirty.
		if ((RelativeTo != nullptr && RelativeTo->UpdateBehavior == UB_Seldom && RelativeTo->IsAbsTransformDirty()) && //Checks if the relativeTo is already registered.
			(newRelativeTo == nullptr || (newRelativeTo->UpdateBehavior == UB_Seldom && !newRelativeTo->IsAbsTransformDirty()))) //If the newRelativeTo is already dirty, no need to register since that, too, is also registered.
		{
			GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
			if (localGraphicsEngine != nullptr && localGraphicsEngine->GetSceneUpdater() != nullptr)
			{
				localGraphicsEngine->GetSceneUpdater()->RegisterTemporaryTransform(this);
			}
		}
	}

	RelativeTo = newRelativeTo;
	if (RelativeTo != nullptr)
	{
		RelativeTo->AttachedTransforms.push_back(this);
	}

	MarkAbsTransformDirty();

	if (UpdateBehavior == UB_Continuous)
	{
		if (GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find())
		{
			SceneTransformUpdater* updater = localGraphicsEngine->GetSceneUpdater();
			if (updater == nullptr)
			{
				GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to register continuously updated scene transform to the updater since there isn't an instance in the graphics engine component."));
			}
			else if (RelativeTo != nullptr && updater->IsAlreadyPermanentTransform(RelativeTo))
			{
				//It's attached to a transform that's already registered.
				updater->RemovePermanentTransform(this);
			}
			else if (!updater->IsAlreadyPermanentTransform(this)) //handle case where continuous transform that was attached to a manual transform, moved to another manual transform
			{
				updater->RegisterPermanentTransform(this);
			}
		}
	}
}

void SceneTransform::SetUpdateBehavior (EUpdateBehavior newUpdateBehavior)
{
	if (newUpdateBehavior == UpdateBehavior)
	{
		return;
	}

	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr)
	SceneTransformUpdater* updater = localGraphicsEngine->GetSceneUpdater();
	if (updater == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to set SceneTransforms' update behavior since there isn't an instance of the SceneTransformUpdater in the local graphics engine component."));
		return;
	}

	if (UpdateBehavior == UB_Continuous)
	{
		updater->RemovePermanentTransform(this);
	}

	UpdateBehavior = newUpdateBehavior;

	//Only register this transform if none of the transforms this is relative to is already registered.
	if (UpdateBehavior == UB_Continuous && !updater->IsAlreadyPermanentTransform(this))
	{
		updater->RegisterPermanentTransform(this);
	}
}
SD_END