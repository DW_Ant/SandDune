/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Transformation.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
Transformation::Transformation () :
	ProjectionDataDirty(true)
{

}

Transformation::Transformation (const Transformation& cpy) :
	ProjectionDataDirty(true)
{

}

Transformation::~Transformation ()
{

}

void Transformation::MarkProjectionDataDirty () const
{
	ProjectionDataDirty = true;
}

const Transformation::SScreenProjectionData& Transformation::GetProjectionData (const RenderTarget* renderTarget, const Camera* camera) const
{
	if (!ProjectionDataDirty)
	{
		return LatestProjectionData;
	}

	CalculateScreenSpaceTransform(renderTarget, camera, OUT LatestProjectionData);
	ProjectionDataDirty = false;

	return LatestProjectionData;
}
SD_END