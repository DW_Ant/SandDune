/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransformComponent.cpp
=====================================================================
*/

#include "PlanarTransformComponent.h"

IMPLEMENT_CLASS(SD::PlanarTransformComponent, SD::EntityComponent)
SD_BEGIN

void PlanarTransformComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (PlanarTransform* ownerTransform = dynamic_cast<PlanarTransform*>(newOwner))
	{
		SetRelativeTo(ownerTransform);
	}
}

void PlanarTransformComponent::ComponentDetached ()
{
	SetRelativeTo(nullptr);

	Super::ComponentDetached();
}
SD_END