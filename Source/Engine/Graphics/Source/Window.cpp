/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Window.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::Window, SD::RenderTarget)
SD_BEGIN

std::mutex Window::WindowResourceMutex;

void Window::InitProps ()
{
	Super::InitProps();

	DefaultColor = sf::Color::Black;
	InvokeCloseOnDestruction = true;

	Resource = nullptr;
	PollingTickComponent = nullptr;
}

void Window::BeginObject ()
{
	Super::BeginObject();

	PollingTickComponent = TickComponent::CreateObject(TICK_GROUP_SYSTEM);
	if (AddComponent(PollingTickComponent))
	{
		PollingTickComponent->SetTickHandler(SDFUNCTION_1PARAM(this, Window, HandlePollingTick, void, Float));
	}
}

void Window::Reset ()
{
	if (Resource != nullptr)
	{
		Resource->clear(DefaultColor.Source);
	}
}

void Window::GenerateScene ()
{
	if (Resource != nullptr)
	{
		const std::lock_guard<std::mutex> lock(WindowResourceMutex);

		Resource->setActive(true);
		Super::GenerateScene();
		Resource->setActive(false);
	}
}

void Window::Draw (RenderComponent* comp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState)
{
	Super::Draw(comp, cam, drawable, renderState);

	Resource->draw(drawable, renderState);
}

void Window::Display ()
{
	Super::Display();

	if (Resource != nullptr)
	{
		Resource->display();
	}
}

void Window::GetSize (Int& outWidth, Int& outHeight) const
{
	if (Resource != nullptr)
	{
		sf::Vector2u windowSize = Resource->getSize();
		outWidth = windowSize.x;
		outHeight = windowSize.y;
	}
	else
	{
		outWidth = 0;
		outHeight = 0;
	}
}

Vector2 Window::GetSize () const
{
	Int width;
	Int height;
	GetSize(OUT width, OUT height);

	return Vector2(width.ToFloat(), height.ToFloat());
}

void Window::Destroyed ()
{
	if (InvokeCloseOnDestruction)
	{
		//Notify all polling delegates that this window is closing
		sf::Event closeEvent;
		closeEvent.type = sf::Event::Closed;
		PollingDelegates.Broadcast(closeEvent);
	}

	if (Resource != nullptr)
	{
		delete Resource;
		Resource = nullptr;
	}

	Super::Destroyed();
}

void Window::RegisterPollingDelegate (SDFunction<void, const sf::Event&> newCallback)
{
	PollingDelegates.RegisterHandler(newCallback);
}

void Window::UnregisterPollingDelegate (SDFunction<void, const sf::Event&> targetCallback)
{
	PollingDelegates.UnregisterHandler(targetCallback);
}

void Window::RegisterCanCloseDelegate (SDFunction<bool> newCallback)
{
	ContainerUtils::AddUnique(OUT OnCanClose, newCallback);
}

void Window::UnregisterCanCloseDelegate (SDFunction<bool> targetCallback)
{
	ContainerUtils::RemoveItem(OUT OnCanClose, targetCallback);
}

void Window::PollWindowEvents ()
{
	//Mouse move events are recorded here and only consider the latest mouse move position since multiple mouse move events could be executed within a single frame.
	Vector2 finalMousePos = Vector2::ZERO_VECTOR;
	bool bPendingDestruction = false;

	/* List of events to broadcast after the polling loop. This is handled after the loop to minimize the mutex lock, and to ensure
	None of the event handlers destroy or invoke more event during the loop. */
	std::vector<sf::Event> events;

	if (Resource != nullptr)
	{
		sf::Event sfEvent;
		while (Resource->pollEvent(OUT sfEvent))
		{
			switch(sfEvent.type)
			{
				case(sf::Event::MouseMoved) :
					finalMousePos.X = Float::MakeFloat(sfEvent.mouseMove.x);
					finalMousePos.Y = Float::MakeFloat(sfEvent.mouseMove.y);
					continue; //Don't notify polling delegates yet.  There may be other pending mouse moves.

				case(sf::Event::Closed) :
				{
					//Ensure nothing it preventing the close event
					bool bCanClose = true;
					for (size_t i = 0; i < OnCanClose.size(); ++i)
					{
						if (!OnCanClose.at(i).Execute())
						{
							bCanClose = false;
							break;
						}
					}

					if (bCanClose)
					{
						//The Closed event is automatically invoked upon destruction.
						//Make sure the other PollingDelegates are invoked before closed is called.
						//The reason why Closed is broadcasted on Destroy instead of here is to handle cases where the window handle is destroyed without the user explicitly closing window.
						bPendingDestruction = true;
					}

					continue;
				}

				case(sf::Event::Resized) :
					// update the view to the new size of the window
					sf::FloatRect visibleArea(0.f, 0.f, static_cast<float>(sfEvent.size.width), static_cast<float>(sfEvent.size.height));
					Resource->setView(sf::View(visibleArea));
					break;
			}

			events.push_back(sfEvent);
		}
	}

	if (!finalMousePos.IsEmpty())
	{
		sf::Event sfEvent;
		sfEvent.type = sf::Event::MouseMoved;
		sfEvent.mouseMove.x = finalMousePos.X.ToInt().ToInt32();
		sfEvent.mouseMove.y = finalMousePos.Y.ToInt().ToInt32();
		events.push_back(sfEvent);
	}

	for (const sf::Event& sfEvent : events)
	{
		PollingDelegates.Broadcast(sfEvent);
	}

	if (bPendingDestruction)
	{
		//This will notify all PollingDelegates about the closed event
		Destroy();
	}
}

void Window::GetWindowPosition (Int& outPosX, Int& outPosY, bool bIncludeTitlebar) const
{
	if (!bIncludeTitlebar)
	{
		Rectangle clientWindow = OS_GetClientWindowPos(this);

		outPosX = clientWindow.GetLesserVBorder().ToInt();
		outPosY = clientWindow.GetLesserHBorder().ToInt();
		return;
	}

	sf::Vector2i windowPos = Resource->getPosition();

	outPosX = windowPos.x;
	outPosY = windowPos.y;
}

void Window::SetSize (const Vector2& newSize)
{
	if (Resource != nullptr)
	{
		Resource->setSize(sf::Vector2u(newSize.X.ToInt().ToUnsignedInt32(), newSize.Y.ToInt().ToUnsignedInt32()));
	}
}

void Window::SetResource (sf::RenderWindow* newResource)
{
	if (Resource != nullptr)
	{
		Resource->setActive(false);
		delete Resource;
		Resource = nullptr;
	}

	Resource = newResource;
}

sf::RenderWindow* Window::GetResource () const
{
	return Resource;
}

void Window::GetWindowSize (Int& outWidth, Int& outHeight) const
{
	sf::Vector2u size = Resource->getSize();

	outWidth = size.x;
	outHeight = size.y;
}

Vector2 Window::GetWindowSize () const
{
	sf::Vector2u size = Resource->getSize();

	return Vector2(Float::MakeFloat(size.x), Float::MakeFloat(size.y));
}

TickComponent* Window::GetPollingTickComponent () const
{
	return PollingTickComponent.Get();
}

void Window::HandlePollingTick (Float deltaSec)
{
	PollWindowEvents();
}
SD_END