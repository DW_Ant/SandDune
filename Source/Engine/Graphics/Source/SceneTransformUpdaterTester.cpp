/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformUpdaterTester.cpp
=====================================================================
*/

#include "SceneTransform.h"
#include "SceneTransformUpdaterTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::SceneTransformUpdaterTester, SD::Entity)
SD_BEGIN

void SceneTransformUpdaterTester::InitProps ()
{
	Super::InitProps();

	TestFlags = UnitTester::EUnitTestFlags::UTF_None;
	TestFrameNum = 0;
	ActiveTestIndex = UINT_INDEX_NONE;
}

void SceneTransformUpdaterTester::Destroyed ()
{
	RemoveTestObjects();

	Super::Destroyed();
}

bool SceneTransformUpdaterTester::LaunchTests (UnitTester::EUnitTestFlags testFlags)
{
	TestFlags = testFlags;
	ActiveTestIndex = 0;
	ContainerUtils::Empty(Tests);
	RemoveTestObjects();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, SceneTransformUpdaterTester, HandleTick, void, Float));
	}
	else
	{
		UnitTester::UnitTestError_Static(TestFlags, TXT("Unable to launch SceneTransformUpdaterTester since it was unable to attach a tick component to the entity."));
		Destroy();
		return false;
	}

	//Create test instances
	//Simple manual test
	{
		STestInstance test;
		test.TestName = TXT("Simple Manual Test");
		test.StartTest = [&]()
		{
			TestTransforms.push_back(CreateTransform(Vector3(10.f, 0.f, 0.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(5.f, 0.f, 0.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 5.f, 0.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, -10.f, 0.f), SceneTransform::UB_Manual));
			return true;
		};

		test.SecondFrame = [&]()
		{
			//Everything should read zero since manual transforms should not be automatically updated
			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				ExpectedAbsTranslations.push_back(Vector3::ZERO_VECTOR);
			}

			if (!CheckTransformations())
			{
				return false;
			}

			//Now update transforms
			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				TestTransforms.at(i)->CalculateAbsoluteTransform();
				ExpectedAbsTranslations.at(i) = TestTransforms.at(i)->ReadTranslation();
			}

			//These should update immediately since manual transforms don't need to wait for the updater
			if (!CheckTransformations())
			{
				return false;
			}

			CHECK(TestTransforms.size() >= 4)
			TestTransforms.at(1)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(2)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(3)->SetRelativeTo(TestTransforms.at(1)); //test nested relative to

			//The abs transforms should not have updated
			if (!CheckTransformations())
			{
				return false;
			}

			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				TestTransforms.at(i)->CalculateAbsoluteTransform();
			}

			ExpectedAbsTranslations.at(1) = Vector3(15.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(2) = Vector3(10.f, 5.f, 0.f);
			ExpectedAbsTranslations.at(3) = Vector3(15.f, -10.f, 0.f);

			if (!CheckTransformations())
			{
				return false;
			}

			return true;
		};

		test.EndTest = [&]()
		{
			//No need to run on the third frame since everything is essentially synchronous on the manual test.
			return true;
		};

		Tests.push_back(test);
	}

	//Run simple seldom test
	{
		STestInstance test;
		test.TestName = TXT("Seldom test");
		test.StartTest = [&]()
		{
			TestTransforms.push_back(CreateTransform(Vector3(5.f, 0.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 5.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 5.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(15.f, 0.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 15.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 15.f), SceneTransform::UB_Seldom));

			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				//These should not be set yet
				ExpectedAbsTranslations.push_back(Vector3::ZERO_VECTOR);
			}

			return CheckTransformations();
		};

		test.SecondFrame = [&]()
		{
			//Abs translations should have been computed by now
			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				ExpectedAbsTranslations.at(i) = TestTransforms.at(i)->GetTranslation();
			}

			if (!CheckTransformations())
			{
				return false;
			}

			//Run relative to tests
			CHECK(TestTransforms.size() >= 6)
			TestTransforms.at(1)->SetRelativeTo(TestTransforms.at(0)); //breadth test
			TestTransforms.at(2)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(3)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(4)->SetRelativeTo(TestTransforms.at(1)); //depth test
			TestTransforms.at(5)->SetRelativeTo(TestTransforms.at(4));

			//Note: The abs translations should be computed next frame since changing relative to should have marked them as dirty.
			return true;
		};

		test.EndTest = [&]()
		{
			CHECK(TestTransforms.size() >= 6)
			ExpectedAbsTranslations.at(1) = Vector3(5.f, 5.f, 0.f);
			ExpectedAbsTranslations.at(2) = Vector3(5.f, 0.f, 5.f);
			ExpectedAbsTranslations.at(3) = Vector3(20.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(4) = Vector3(5.f, 20.f, 0.f);
			ExpectedAbsTranslations.at(5) = Vector3(5.f, 20.f, 15.f);

			return CheckTransformations();
		};

		Tests.push_back(test);
	}

	//Test continuous transforms
	{
		STestInstance test;
		test.TestName = TXT("Continuous test");
		test.StartTest = [&]()
		{
			TestTransforms.push_back(CreateTransform(Vector3(5.f, 0.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 5.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 5.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(15.f, 0.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 15.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 15.f), SceneTransform::UB_Continuous));

			TestTransforms.at(3)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(4)->SetRelativeTo(TestTransforms.at(0));
			TestTransforms.at(5)->SetRelativeTo(TestTransforms.at(3));

			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				//The abs transforms should not have been computed yet.
				ExpectedAbsTranslations.push_back(Vector3::ZERO_VECTOR);
			}

			return CheckTransformations();
		};

		test.SecondFrame = [&]()
		{
			CHECK(TestTransforms.size() >= 6)
			ExpectedAbsTranslations.at(0) = Vector3(5.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(1) = Vector3(0.f, 5.f, 0.f);
			ExpectedAbsTranslations.at(2) = Vector3(0.f, 0.f, 5.f);
			ExpectedAbsTranslations.at(3) = Vector3(20.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(4) = Vector3(5.f, 15.f, 0.f);
			ExpectedAbsTranslations.at(5) = Vector3(20.f, 0.f, 15.f);

			if (!CheckTransformations())
			{
				return false;
			}

			//Test if dirty transforms update
			TestTransforms.at(0)->EditTranslation() = Vector3(-1.f, 0.f, 0.f);
			TestTransforms.at(1)->EditTranslation() = Vector3(0.f, -1.f, 0.f);

			//Test changing relative to transforms
			TestTransforms.at(2)->SetRelativeTo(TestTransforms.at(3));
			TestTransforms.at(3)->SetRelativeTo(TestTransforms.at(1));
			TestTransforms.at(4)->SetRelativeTo(TestTransforms.at(1));
			TestTransforms.at(5)->SetRelativeTo(nullptr);

			return true;
		};

		test.EndTest = [&]()
		{
			CHECK(TestTransforms.size() >= 6)
			ExpectedAbsTranslations.at(0) = Vector3(-1.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(1) = Vector3(0.f, -1.f, 0.f);
			ExpectedAbsTranslations.at(2) = Vector3(15.f, -1.f, 5.f);
			ExpectedAbsTranslations.at(3) = Vector3(15.f, -1.f, 0.f);
			ExpectedAbsTranslations.at(4) = Vector3(0.f, 14.f, 0.f);
			ExpectedAbsTranslations.at(5) = Vector3(0.f, 0.f, 15.f);

			return CheckTransformations();
		};

		Tests.push_back(test);
	}

	//Test the dirty flag with all three behaviors
	{
		STestInstance test;
		test.TestName = TXT("Dirty flag test");
		test.StartTest = [&]()
		{
			//Manual transforms
			TestTransforms.push_back(CreateTransform(Vector3(500.f, 500.f, 500.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(500.f, 0.f, 0.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 500.f, 0.f), SceneTransform::UB_Manual));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 500.f), SceneTransform::UB_Manual));

			//Seldom transforms
			TestTransforms.push_back(CreateTransform(Vector3(50.f, 50.f, 50.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(50.f, 0.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 50.f, 0.f), SceneTransform::UB_Seldom));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 50.f), SceneTransform::UB_Seldom));

			//Continuous transforms
			TestTransforms.push_back(CreateTransform(Vector3(5.f, 5.f, 5.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(5.f, 0.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 5.f, 0.f), SceneTransform::UB_Continuous));
			TestTransforms.push_back(CreateTransform(Vector3(0.f, 0.f, 5.f), SceneTransform::UB_Continuous));

			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				//Nothing should be set yet
				ExpectedAbsTranslations.push_back(Vector3::ZERO_VECTOR);
			}

			if (!CheckTransformations())
			{
				return false;
			}

			//First set simply translates (testing the dirty flag). This will be set next frame after initial test.
			//Second set simply translates but also acts like the root transform for others. This will be set next frame after initial test.
			//Third set transforms are relative to next UpdateBehavior set (manual attaches to seldom, seldom attaches to continuous, continuous attaches to manual).
			TestTransforms.at(2)->SetRelativeTo(TestTransforms.at(5));
			TestTransforms.at(6)->SetRelativeTo(TestTransforms.at(9));
			TestTransforms.at(10)->SetRelativeTo(TestTransforms.at(1));

			//Last set of transforms are relative to the previous UpdateBehavior set (manual attaches to continuous, seldom attaches to manual, continuous attaches to seldom).
			TestTransforms.at(3)->SetRelativeTo(TestTransforms.at(9));
			TestTransforms.at(7)->SetRelativeTo(TestTransforms.at(1));
			TestTransforms.at(11)->SetRelativeTo(TestTransforms.at(5));

			ExpectedAbsTranslations.at(0) = Vector3::ZERO_VECTOR; //The manual transform should not be set yet.

			//Note: All other manual transforms should automatically update since seldom and continuous transforms are attached to it (or manual transforms are attached to auto transforms).
			//Subtransforms should automatically ensure their parents are up to date.
			//SceneUpdaters should automatically update the root transforms and everything relative to it.
			ExpectedAbsTranslations.at(1) = Vector3(500.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(2) = Vector3(50.f, 500.f, 0.f);
			ExpectedAbsTranslations.at(3) = Vector3(5.f, 0.f, 500.f);
			ExpectedAbsTranslations.at(4) = Vector3(50.f, 50.f, 50.f);
			ExpectedAbsTranslations.at(5) = Vector3(50.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(6) = Vector3(5.f, 50.f, 0.f);
			ExpectedAbsTranslations.at(7) = Vector3(500.f, 0.f, 50.f);
			ExpectedAbsTranslations.at(8) = Vector3(5.f, 5.f, 5.f);
			ExpectedAbsTranslations.at(9) = Vector3(5.f, 0.f, 0.f);
			ExpectedAbsTranslations.at(10) = Vector3(500.f, 5.f, 0.f);
			ExpectedAbsTranslations.at(11) = Vector3(50.f, 0.f, 5.f);

			return true;
		};

		test.SecondFrame = [&]()
		{
			if (!CheckTransformations())
			{
				return false;
			}

			//Update the one manual transform
			TestTransforms.at(0)->CalculateAbsoluteTransform();
			ExpectedAbsTranslations.at(0) = Vector3(500.f, 500.f, 500.f);

			if (!CheckTransformations())
			{
				return false;
			}

			//Invert everything to test the dirty flag
			for (size_t i = 0; i < TestTransforms.size(); ++i)
			{
				TestTransforms.at(i)->EditTranslation() *= -1.f;
			}

			//Nothing should be updated yet
			if (!CheckTransformations())
			{
				return false;
			}

			//Since all cases don't have two axis combined, we can simply invert the expected values as well rather than taking the sum of the relative to chain.
			//Invert everything except for the first transform since that is the only manual transform that's not relative to or attached to an automatic transform (it doesn't get an automatic update).
			for (size_t i = 1; i < ExpectedAbsTranslations.size(); ++i)
			{
				ExpectedAbsTranslations.at(i) *= -1.f;
			}

			//Wait for the next frame to get an update
			return true;
		};

		test.EndTest = [&]()
		{
			if (!CheckTransformations())
			{
				return false;
			}

			TestTransforms.at(0)->CalculateAbsoluteTransform();
			ExpectedAbsTranslations.at(0) *= -1.f;

			return CheckTransformations();
		};

		Tests.push_back(test);
	}

	//Test multithreaded update. Essentially this will be a stress test to test running multiple threads for a lot of transform updates.
	if ((TestFlags & UnitTester::EUnitTestFlags::UTF_StressTest) > 0)
	{
		//Run this test multiple times to test against race conditions.
		for (size_t testNum = 0; testNum < 16; ++testNum)
		{
			STestInstance test;
			test.TestName = TXT("Multithread Stress Test ") + Int(testNum).ToString();

			test.StartTest = [&]()
			{
				//Generate 128 transforms to kick off a lot of threads
				float numTransforms = 128.f;
				//The first 64 transforms are seldom transforms while the last 64 are continuous transforms
				for (float i = 0; i < numTransforms; i += 1.f)
				{
					Vector3 translation(10.f * i, 12.f * i, 3.f * i);
					TestTransforms.push_back(CreateTransform(translation, (i < numTransforms * 0.5f) ? SceneTransform::UB_Seldom : SceneTransform::UB_Continuous));
					ExpectedAbsTranslations.push_back(translation);
				}

				return true;
			};

			test.SecondFrame = [&]()
			{
				if (!CheckTransformations())
				{
					return false;
				}

				//Force another update
				Vector3 deltaTranslation(2.f, 3.f, 5.f);
				for (size_t i = 0; i < TestTransforms.size(); ++i)
				{
					//Call set translation to ensure SetTranslation is also marking the transform as dirty
					TestTransforms.at(i)->SetTranslation(TestTransforms.at(i)->ReadTranslation() + deltaTranslation);
					ExpectedAbsTranslations.at(i) += deltaTranslation;

					if ((i%2) == 0)
					{
						TestTransforms.at(i)->EditTranslation() *= -1.f;
						ExpectedAbsTranslations.at(i) *= -1.f;
					}
				}

				return true;
			};

			test.EndTest = [&]()
			{
				return CheckTransformations();
			};

			Tests.push_back(test);
		}
	}

	return true;
}

SceneTransform* SceneTransformUpdaterTester::CreateTransform (const Vector3& translation, SceneTransform::EUpdateBehavior updateBehavior) const
{
	SceneTransform* result = new SceneTransform(translation, Vector3(1.f, 1.f, 1.f), 1.f, Rotator::ZERO_ROTATOR, nullptr, updateBehavior);
	return result;
}

bool SceneTransformUpdaterTester::CheckTransformations () const
{
	CHECK(ContainerUtils::IsValidIndex(Tests, ActiveTestIndex));
	if (TestTransforms.size() != ExpectedAbsTranslations.size())
	{
		UnitTester::UnitTestError_Static(TestFlags, TXT("Scene Transform Updater test failed. The %s produced a size mismatch with the transform instances and the expected translations vector. The number of transforms = %s. The number of expected translations %s."), Tests.at(ActiveTestIndex).TestName, Int(TestTransforms.size()), Int(ExpectedAbsTranslations.size()));
		return false;
	}

	for (size_t i = 0; i < TestTransforms.size(); ++i)
	{
		if (TestTransforms.at(i)->ReadAbsTranslation() != ExpectedAbsTranslations.at(i))
		{
			UnitTester::UnitTestError_Static(TestFlags, TXT("Scene Transform Updater test failed. During the %s, the SceneTransformation at index %s has an absolute translation that does not match the expected translation. The expected translation is %s. The actual absolute translation is %s."), Tests.at(ActiveTestIndex).TestName, Int(i), ExpectedAbsTranslations.at(i), TestTransforms.at(i)->ReadAbsTranslation());
			return false;
		}
	}

	return true;
}

void SceneTransformUpdaterTester::RemoveTestObjects ()
{
	for (size_t i = 0; i < TestTransforms.size(); ++i)
	{
		delete TestTransforms.at(i);
	}

	ContainerUtils::Empty(OUT TestTransforms);
	ContainerUtils::Empty(OUT ExpectedAbsTranslations);
}

void SceneTransformUpdaterTester::HandleTick (Float deltaSec)
{
	if (TestFrameNum >= 3)
	{
		if (ActiveTestIndex >= Tests.size() - 1)
		{
			//Finish the tests
			UnitTester::TestLog(TestFlags, TXT("SceneTransformUpdater test has passed! All entities associated with this test will be destroyed."));
			Destroy();
			return;
		}
		else
		{
			//Advance to the next test
			++ActiveTestIndex;
			UnitTester::TestLog(TestFlags, TXT("The SceneTransformUpdater test has launched the %s test."), Tests.at(ActiveTestIndex).TestName);
			RemoveTestObjects();
			TestFrameNum = 0;
		}
	}

	//Execute a single portion of the test
	switch (TestFrameNum.Value)
	{
		case(0):
			if (!Tests.at(ActiveTestIndex).StartTest())
			{
				Destroy();
			}

			break;

		case(1):
			if (!Tests.at(ActiveTestIndex).SecondFrame())
			{
				Destroy();
			}

			break;

		case(2):
			if (!Tests.at(ActiveTestIndex).EndTest())
			{
				Destroy();
			}

			break;
	}

	++TestFrameNum;
}
SD_END
#endif