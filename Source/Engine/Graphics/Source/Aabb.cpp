/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Aabb.cpp
=====================================================================
*/

#include "Aabb.h"

SD_BEGIN
Aabb::Aabb () :
	Width(0.f),
	Height(0.f),
	Depth(0.f),
	Center(Vector3::ZERO_VECTOR)
{
	//Noop
}

Aabb::Aabb (Float inWidth, Float inHeight, Float inDepth, const Vector3& inCenter) :
	Width(inWidth),
	Height(inHeight),
	Depth(inDepth),
	Center(inCenter)
{
	//Noop
}

Aabb::Aabb (Float inForward, Float inBackward, Float inRight, Float inLeft, Float inUp, Float inDown)
{
	Width = (inRight - inLeft);
	Height = (inUp - inDown);
	Depth = (inForward - inBackward);
	Center = Vector3((inForward + inBackward) * 0.5f, (inRight + inLeft) * 0.5f, (inUp + inDown) * 0.5f);
}

Aabb::Aabb (const Vector3& cornerA, const Vector3& cornerB) :
	Width(Float::Abs(cornerA.Y - cornerB.Y)),
	Height(Float::Abs(cornerA.Z - cornerB.Z)),
	Depth(Float::Abs(cornerA.X - cornerB.X)),
	Center((cornerA + cornerB) * 0.5f)
{
	//Noop
}

Aabb::Aabb (const Aabb& copy) :
	Width(copy.Width),
	Height(copy.Height),
	Depth(copy.Depth),
	Center(copy.Center)
{
	//Noop
}

void Aabb::operator= (const Aabb& copy)
{
	Width = copy.Width;
	Height = copy.Height;
	Depth = copy.Depth;
	Center = copy.Center;
}

bool Aabb::operator== (const Aabb& otherRect) const
{
	return (Width == otherRect.Width && Height == otherRect.Height && Depth == otherRect.Depth && Center == otherRect.Center);
}

bool Aabb::operator!= (const Aabb& otherRect) const
{
	return !(*this == otherRect);
}

void Aabb::ResetToDefaults ()
{
	Width = 0.f;
	Height = 0.f;
	Depth = 0.f;
	Center = Vector3::ZERO_VECTOR;
}

DString Aabb::ToString () const
{
	return DString::CreateFormattedString(TXT("(Width=%s, Height=%s, Depth=%s, Center=%s)"), Width, Height, Depth, Center);
}

void Aabb::ParseString (const DString& str)
{
	DString varData = ParseVariable(str, TXT("Width"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Aabb();
		return;
	}
	Width = Float(varData);

	varData = ParseVariable(str, TXT("Height"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Aabb();
		return;
	}
	Height = Float(varData);

	varData = ParseVariable(str, TXT("Depth"), CommonPropertyRegex::Float);
	if (varData.IsEmpty())
	{
		*this = Aabb();
		return;
	}
	Depth = Float(varData);

	varData = ParseStruct(str, TXT("Center"));
	if (varData.IsEmpty())
	{
		*this = Aabb();
		return;
	}
	Center.ParseString(varData);
}

size_t Aabb::GetMinBytes () const
{
	//(Width, Height, Depth) + Center
	return (Float::SGetMinBytes() * 3) + Vector3::SGetMinBytes();
}

void Aabb::Serialize (DataBuffer& outData) const
{
	outData << Width << Height << Depth << Center;
}

bool Aabb::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Width >> Height >> Depth >> Center;
	return !dataBuffer.HasReadError();
}

Aabb Aabb::GenerateEncompassingAabb (const std::vector<Vector3>& points)
{
	if (ContainerUtils::IsEmpty(points))
	{
		return Aabb();
	}

	Float forward = -MAX_FLOAT;
	Float backward = MAX_FLOAT;
	Float right = -MAX_FLOAT;
	Float left = MAX_FLOAT;
	Float up = -MAX_FLOAT;
	Float down = MAX_FLOAT;

	for (size_t i = 0; i < points.size(); ++i)
	{
		forward = Utils::Max(forward, points[i].X);
		backward = Utils::Min(backward, points[i].X);
		right = Utils::Max(right, points[i].Y);
		left = Utils::Min(left, points[i].Y);
		up = Utils::Max(up, points[i].Z);
		down = Utils::Min(down, points[i].Z);
	}

	return Aabb(forward, backward, right, left, up, down);
}

Aabb Aabb::GetOverlappingAabb (const Aabb& otherAabb) const
{
	if (!Overlaps(otherAabb))
	{
		return Aabb();
	}

	Float forward = Utils::Min(GetForward(), otherAabb.GetForward());
	Float backward = Utils::Max(GetBackward(), otherAabb.GetBackward());
	Float right = Utils::Min(GetRight(), otherAabb.GetRight());
	Float left = Utils::Max(GetLeft(), otherAabb.GetLeft());
	Float up = Utils::Min(GetUp(), otherAabb.GetUp());
	Float down = Utils::Max(GetDown(), otherAabb.GetDown());

	return Aabb(forward, backward, right, left, up, down);
}

Aabb Aabb::CombineAabb (const Aabb& combineWith) const
{
	Float forward = Utils::Max(GetForward(), combineWith.GetForward());
	Float backward = Utils::Min(GetBackward(), combineWith.GetBackward());
	Float right = Utils::Max(GetRight(), combineWith.GetRight());
	Float left = Utils::Min(GetLeft(), combineWith.GetLeft());
	Float up = Utils::Max(GetUp(), combineWith.GetUp());
	Float down = Utils::Min(GetDown(), combineWith.GetDown());

	return Aabb(forward, backward, right, left, up, down);
}

bool Aabb::Overlaps (const Aabb& otherAabb) const
{
	Float deltaDepth = Center.X - otherAabb.Center.X;
	deltaDepth.AbsInline();
	if (deltaDepth > (Depth * 0.5f) + (otherAabb.Depth * 0.5f))
	{
		//Either too far ahead or behind
		return false;
	}

	Float deltaWidth = Center.Y - otherAabb.Center.Y;
	deltaWidth.AbsInline();
	if (deltaWidth > (Width * 0.5f) + (otherAabb.Width * 0.5f))
	{
		//Either too far right or left
		return false;
	}

	Float deltaHeight = Center.Z - otherAabb.Center.Z;
	deltaHeight.AbsInline();
	if (deltaHeight > (Height * 0.5f) + (otherAabb.Height * 0.5f))
	{
		//Either too far above or below
		return false;
	}

	return true;
}

bool Aabb::EncompassesPoint (const Vector3& targetPoint) const
{
	if (targetPoint.X > GetForward() || targetPoint.X < GetBackward())
	{
		return false;
	}

	if (targetPoint.Y > GetRight() || targetPoint.Y < GetLeft())
	{
		return false;
	}

	if (targetPoint.Z > GetUp() || targetPoint.Z < GetDown())
	{
		return false;
	}

	return true;
}
SD_END