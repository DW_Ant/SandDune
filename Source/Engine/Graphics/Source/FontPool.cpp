/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FontPool.cpp
=====================================================================
*/

#include "FontPool.h"

IMPLEMENT_CLASS(SD::FontPool, SD::ResourcePool)
SD_BEGIN
IMPLEMENT_RESOURCE_POOL(Font)

void FontPool::InitProps ()
{
	Super::InitProps();

	DefaultFont = nullptr;
}

void FontPool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();

	DefaultFont = EditFont(HashedString("DefaultFont"), false);
	if (DefaultFont.IsNullptr())
	{
		DefaultFont = CreateAndImportFont(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("Thryromanes.ttf")), HashedString("DefaultFont"));
	}
}

void FontPool::ReleaseResources ()
{
	for (std::pair<HashedString, Font*> font : ImportedFonts)
	{
		font.second->Destroy();
	}
	ImportedFonts.clear();

	DefaultFont = nullptr;
}

void FontPool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

Font* FontPool::CreateAndImportFont (const FileAttributes& file, const HashedString& fontName)
{
	if (ImportedFonts.contains(fontName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create font \"%s\" since the font name \"%s\" is already in use."), file.GetName(true, true), fontName.ReadString());
		return nullptr;
	}

	sf::Font* newFont = new sf::Font();
	if (!newFont->loadFromFile(file.GetName(true, true).ToCString()))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create font \"%s\".  Could not find \"%s\""), fontName.ReadString(), file.GetName(true, true));
		delete newFont;
		return nullptr;
	}

	Font* result = Font::CreateObject();
	if (result == nullptr)
	{
		delete newFont;
		return nullptr;
	}

	newFont->setSmooth(false); //Disable font smoothing for a more crisp look to it since majority of the text in this engine is small.
	result->SetResource(newFont);
	result->FontName = fontName.ReadString();
	ImportedFonts.insert({fontName, result});

	return result;
}

void FontPool::GetAllFonts (std::vector<Font*>& outAllFonts)
{
	for (std::pair<HashedString, Font*> font : ImportedFonts)
	{
		outAllFonts.push_back(font.second);
	}
}

const Font* FontPool::GetFont (const HashedString& fontName, bool bLogOnMissing) const
{
	if (!ImportedFonts.contains(fontName))
	{
		if (bLogOnMissing)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("The font \"%s\" does not exist in the font pool."), fontName.ReadString());
		}

		return nullptr;
	}

	return ImportedFonts.at(fontName);
}

Font* FontPool::EditFont (const HashedString& fontName, bool bLogOnMissing)
{
	if (!ImportedFonts.contains(fontName))
	{
		if (bLogOnMissing)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("The font \"%s\" does not exist in the font pool."), fontName.ReadString());
		}

		return nullptr;
	}

	return ImportedFonts.at(fontName);
}

void FontPool::DestroyFont (const HashedString& fontName)
{
	if (ImportedFonts.contains(fontName))
	{
		ImportedFonts.at(fontName)->Destroy();
		ImportedFonts.erase(fontName);
	}
}

Font* FontPool::GetDefaultFont () const
{
	return DefaultFont.Get();
}
SD_END