/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneCamera.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_ABSTRACT_CLASS(SD::SceneCamera, SD::Camera)
SD_BEGIN
SD_END