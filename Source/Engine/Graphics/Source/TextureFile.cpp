/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureFile.cpp
=====================================================================
*/

#include "Color.h"
#include "Texture.h"
#include "TextureFile.h"
#include "TexturePool.h"

IMPLEMENT_CLASS(SD::TextureFile, SD::BinaryFile)
SD_BEGIN

const DString TextureFile::FILE_EXTENSION(TXT("txtr"));

void TextureFile::InitProps ()
{
	Super::InitProps();

	TargetTexture = nullptr;
	TargetTexturePool = nullptr;
}

bool TextureFile::ReadContents (const DataBuffer& incomingData)
{
	if (!Super::ReadContents(incomingData))
	{
		return false;
	}

	if (TargetTexturePool == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. The TargetTexturePool must be assigned before a texture is loaded."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	bool readBytes;
	Int width = ReadIntFromBuffer(incomingData, OUT readBytes);
	if (!readBytes)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. Could not read width from data buffer."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	Int height = ReadIntFromBuffer(incomingData, OUT readBytes);
	if (!readBytes)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. Could not read height from data buffer."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	std::vector<Int> pixels;
	incomingData.DeserializeArray(OUT pixels);
	Int expectedSize = height * width;

#ifdef PLATFORM_64BIT
	//for 64 bit ints, there are 2 pixels per int.
	if (expectedSize.IsOdd())
	{
		++expectedSize;
	}
	expectedSize /= 2;
#endif

	if (Int(pixels.size()) != expectedSize)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. The number of pixels (%s) does not fit in a %s by %s texture."), LatestAccessedFile.GetName(true, true), Int(pixels.size()), width, height);
		return false;
	}

	std::vector<sf::Uint8> colorChannels;
	colorChannels.resize((height * width).ToUnsignedInt() * 4);

#ifdef PLATFORM_64BIT
	size_t colIdx = 0;
	for (size_t i = 0; i < pixels.size(); ++i)
	{
		Int leadingBits = pixels.at(i);
		leadingBits >>= 32; //Moves the first 32 bits to the beginning of the int.
		Color firstPixel(leadingBits.ToUnsignedInt32(false));
		colorChannels.at(colIdx) = firstPixel.Source.r;
		colorChannels.at(colIdx + 1) = firstPixel.Source.g;
		colorChannels.at(colIdx + 2) = firstPixel.Source.b;
		colorChannels.at(colIdx + 3) = firstPixel.Source.a;

		if (colIdx + 7 < colorChannels.size()) //Handles odd number of pixels
		{
			Int trailingBits = pixels.at(i);
			trailingBits &= 0xFFFFFFFF; //Drop the leading zeros beyond 32 bits
			Color secondPixel(trailingBits.ToUnsignedInt32(false));
			colorChannels.at(colIdx + 4) = secondPixel.Source.r;
			colorChannels.at(colIdx + 5) = secondPixel.Source.g;
			colorChannels.at(colIdx + 6) = secondPixel.Source.b;
			colorChannels.at(colIdx + 7) = secondPixel.Source.a;
		}

		colIdx += 8;
	}
#else
	for (size_t i = 0; i < pixels.size(); ++i)
	{
		Color pixel(pixels.at(i).ToUnsignedInt32(false));
		colorChannels.at((i*4)) = pixel.Source.r;
		colorChannels.at((i*4) + 1) = pixel.Source.g;
		colorChannels.at((i*4) + 2) = pixel.Source.b;
		colorChannels.at((i*4) + 3) = pixel.Source.a;
	}
#endif

	TargetTexture = Texture::CreateObject();
	if (!TargetTexture->SetTexelData(colorChannels, width, height))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to open texture file %s. Failed to set the texture's texel data."), LatestAccessedFile.GetName(true, true));
		TargetTexture->Destroy();
		TargetTexture = nullptr;
		return false;
	}
	
	//Import texture to TexturePool
	HashedString textureName;
	DString strTextureName = LatestAccessedFile.ReadPath().GetAbsPath().ToString();
	if (Directory::CONTENT_DIRECTORY.ReadDirectoryPath().Length() > strTextureName.Length())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to open texture file %s. All textures are required to reside in: %s"), LatestAccessedFile.GetName(true, true), Directory::CONTENT_DIRECTORY.ReadDirectoryPath());
		TargetTexture->Destroy();
		TargetTexture = nullptr;
		return false;
	}

	strTextureName = strTextureName.SubString(Directory::CONTENT_DIRECTORY.GetAbsPath().ToString().Length());

	//Replace slashes with periods to follow general naming convention.
	strTextureName.ReplaceInline(Directory::DIRECTORY_SEPARATOR, TXT("."), DString::CC_CaseSensitive);
	if (!strTextureName.EndsWith('.'))
	{
		strTextureName += '.';
	}
	strTextureName += LatestAccessedFile.GetName(false, false);
	TargetTexture->SetTextureName(strTextureName);
	textureName.SetString(strTextureName);

	if (!TargetTexturePool->AddTexture(TargetTexture, textureName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to import %s to the specified texture pool (the slot is possibly in use)."), strTextureName);
		TargetTexture->Destroy();
		TargetTexture = nullptr;
		return false;
	}

	//Set texture attributes
	TargetTexture->SetOwningTexturePool(TargetTexturePool);

	std::vector<DString> tags;
	incomingData.DeserializeArray(OUT tags);
	for (size_t i = 0; i < tags.size(); ++i)
	{
		const HashedString& newTag = TargetTexture->EditTags().emplace_back(tags.at(i));
		ResourceTag::AddTag(newTag, OUT TargetTexturePool->Tags);
	}

	{
		if (!incomingData.CanReadBytes(Bool::SGetMinBytes()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. Not enough bytes to read a bool from buffer."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		Bool wrapping;
		if ((incomingData >> wrapping).HasReadError())
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. The data is malformed, preventing the data buffer from reading the wrapping Bool."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		TargetTexture->SetRepeat(wrapping);
	}

	{
		if (!incomingData.CanReadBytes(Bool::SGetMinBytes()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. Not enough bytes to read a bool from buffer."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		Bool smoothing;
		if ((incomingData >> smoothing).HasReadError())
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. The data is malformed, preventing the data buffer from reading the smoothing Bool."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		TargetTexture->SetSmooth(smoothing);
	}

	{
		if (!incomingData.CanReadBytes(Bool::SGetMinBytes()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. Not enough bytes to read a bool from buffer."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		Bool mipMap;
		if ((incomingData >> mipMap).HasReadError())
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to load texture from %s. The data is malformed, preventing the data buffer from reading the mip map Bool."), LatestAccessedFile.GetName(true, true));
			TargetTexturePool->DestroyTexture(textureName);
			TargetTexture = nullptr;
			return false;
		}

		if (mipMap)
		{
			TargetTexture->GenerateMipMap();
		}
	}
	
	return true;
}

bool TextureFile::SaveContents (DataBuffer& outBuffer) const
{
	if (!Super::SaveContents(OUT outBuffer))
	{
		return false;
	}

	if (TargetTexture == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot save texture to %s since the TargetTexture is not yet assigned."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	if (TargetTexture->GetOwningTexturePool() == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot save texture to %s since the TargetTexture is not associated with a texture pool. Cannot identify the tags without the texture pool reference."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	Int width;
	Int height;
	TargetTexture->GetDimensions(OUT width, OUT height);
	outBuffer << width;
	outBuffer << height;

	sf::Image textureImg;
	if (!TargetTexture->GetImage(OUT textureImg))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot save texture to %s since the TargetTexture is unable to copy to an image object."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	const sf::Uint8* colors = textureImg.getPixelsPtr();
	std::vector<Int> pixels;
	size_t numPixels = (width * height).ToUnsignedInt();
	size_t vectSize = numPixels;

#ifdef PLATFORM_64BIT
	//For 64-bit ints, there are two pixels per Int.
	if ((vectSize % 2) != 0)
	{
		//Odd number. Add one extra one extra Int to the end to store that extra pixel.
		vectSize++;
	}
	vectSize /= 2;
#endif

	pixels.resize(vectSize);
	for (size_t colIdx = 0, pixIdx = 0; colIdx < numPixels; ++colIdx, ++pixIdx)
	{
		Int pixel = 0;
		pixel |= (Int(colors[colIdx*4]) << 24); //r
		pixel |= (Int(colors[(colIdx*4) + 1]) << 16); //g
		pixel |= (Int(colors[(colIdx*4) + 2]) << 8); //b
		pixel |= (Int(colors[(colIdx*4) + 3])); //a

#ifdef PLATFORM_64BIT
		pixel <<= 32; //Shift everything to the first 32 bits.
		++colIdx;

		if (colIdx < numPixels) //Handles cases with odd number of pixels
		{
			//Repeat process
			pixel |= (Int(colors[colIdx*4]) << 24); //r
			pixel |= (Int(colors[(colIdx*4) + 1]) << 16); //g
			pixel |= (Int(colors[(colIdx*4) + 2]) << 8); //b
			pixel |= (Int(colors[(colIdx*4) + 3])); //a
		}
#endif

		pixels.at(pixIdx) = pixel;
	}

	outBuffer.SerializeArray(pixels);

	std::vector<DString> tags;
	for (const HashedString& tag : TargetTexture->ReadTags())
	{
		tags.push_back(tag.ToString());
	}
	outBuffer.SerializeArray(tags);

	outBuffer << Bool(TargetTexture->GetTextureResource()->isRepeated());
	outBuffer << Bool(TargetTexture->GetTextureResource()->isSmooth());
	outBuffer << Bool(TargetTexture->HasMipMap());

	return true;
}

void TextureFile::SetTargetTexture (Texture* newTargetTexture)
{
	TargetTexture = newTargetTexture;
}

void TextureFile::SetTargetTexturePool (TexturePool* newTargetTexturePool)
{
	TargetTexturePool = newTargetTexturePool;
}
SD_END