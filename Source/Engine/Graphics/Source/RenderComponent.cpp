/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_ABSTRACT_CLASS(SD::RenderComponent, SD::EntityComponent)
SD_BEGIN

void RenderComponent::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	NumRegisteredSceneLayers = 0;
#endif

	OwnerTransform = nullptr;
}

CopiableObjectInterface* RenderComponent::CreateCopiableInstanceOfMatchingType () const
{
	if (const DClass* duneClass = StaticClass())
	{
		if (const RenderComponent* cdo = dynamic_cast<const RenderComponent*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void RenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	CHECK(objTemplate != this) //It's pointless for an object to copy properties from self.
}

bool RenderComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	//Owners must derive from a Transformation object.
	return (Super::CanBeAttachedTo(ownerCandidate) && dynamic_cast<Transformation*>(ownerCandidate) != nullptr);
}

unsigned int RenderComponent::CalculateHashID () const
{
	return (GraphicsEngineComponent::Find()->GetRenderComponentHashNumber());
}

void RenderComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (OwnerTransform = dynamic_cast<Transformation*>(newOwner))
	{
		OwnerTransform->OnTransformChanged.RegisterHandler(SDFUNCTION(this, RenderComponent, HandleTransformChanged, void));
	}

	CHECK(OwnerTransform != nullptr)
}

void RenderComponent::ComponentDetached ()
{
	if (OwnerTransform != nullptr)
	{
		OwnerTransform->OnTransformChanged.UnregisterHandler(SDFUNCTION(this, RenderComponent, HandleTransformChanged, void));
		OwnerTransform = nullptr;
	}

	Super::ComponentDetached();
}

Aabb RenderComponent::GetTransformedAabb () const
{
	if (CachedTransformedAabb.IsEmpty())
	{
		CachedTransformedAabb = GetAabb();
		OwnerTransform->TransformAabb(OUT CachedTransformedAabb);
	}

	return CachedTransformedAabb;
}

void RenderComponent::MarkTransformedAabbDirty ()
{
	CachedTransformedAabb = Aabb();
}

bool RenderComponent::IsRelevant (const RenderTarget* renderTarget, const Camera* camera) const
{
	if (!IsVisible())
	{
		return false;
	}

	const Transformation* ownerTransform = GetOwnerTransform();
	CHECK(ownerTransform != nullptr)
	return ownerTransform->IsWithinView(renderTarget, camera);
}

bool RenderComponent::IsAlwaysScreenCapRelevant (ScreenCapture* screenCap) const
{
	return false;
}

void RenderComponent::RegisterToDefaultSceneLayers ()
{
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr)

	const std::vector<SceneDrawLayer*>& defaultSceneLayers = localGraphicsEngine->ReadDefaultSceneLayers();
	for (SceneDrawLayer* sceneLayer : defaultSceneLayers)
	{
		sceneLayer->RegisterSingleComponent(this);
	}
}

Transformation* RenderComponent::GetOwnerTransform () const
{
	return OwnerTransform;
}

void RenderComponent::HandleTransformChanged ()
{
	MarkTransformedAabbDirty();
}
SD_END