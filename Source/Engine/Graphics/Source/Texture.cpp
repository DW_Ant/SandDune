/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Texture.cpp
=====================================================================
*/

#include "Texture.h"
#include "TexturePool.h"

IMPLEMENT_CLASS(SD::Texture, SD::Object)
SD_BEGIN

void Texture::InitProps ()
{
	Super::InitProps();

	TextureResource = nullptr;
	OwningTexturePool = nullptr;
	Width = 0;
	Height = 0;
	TextureName = TXT("UnknownTexture");
	bHasMipMap = false;
}

DString Texture::GetFriendlyName () const
{
	if (!TextureName.IsEmpty())
	{
		return TXT("Texture:  ") + TextureName;
	}

	return Super::GetFriendlyName();
}

void Texture::Destroyed ()
{
	Release();

	if (OwningTexturePool != nullptr)
	{
		for (const HashedString& tag : Tags)
		{
			ResourceTag::RemoveTag(tag, OUT OwningTexturePool->Tags);
		}
		ContainerUtils::Empty(OUT Tags);
	}

	Super::Destroyed();
}

void Texture::Release ()
{
	if (TextureResource != nullptr)
	{
		delete TextureResource;
		TextureResource = nullptr;

		Width = 0;
		Height = 0;
		TextureName = TXT("Unknown Texture");
		bHasMipMap = false;
	}
}

bool Texture::SetTexelData (const std::vector<sf::Uint8>& newTexelData)
{
	if (newTexelData.size() != Width * Height * 4)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to set texel data since the length of the texel data does not match the size of the texture (%sx%s).  Length of texel data is %s. Expected size is %s"), Width, Height, Int(newTexelData.size()), Int(newTexelData.size() * 4));
		return false;
	}

	sf::Uint8* textureData = new (std::nothrow) sf::Uint8[Width.Value * Height.Value * 4];
	if (textureData == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to allocate memory to update pixel data for %s."), TextureName);
		return false;
	}

	// Copy pixelData to textureData
	for (size_t i = 0; i < newTexelData.size(); i++)
	{
		textureData[i] = newTexelData.at(i);
	}

	TextureResource->update(textureData);

	delete[] textureData;
	return true;
}

bool Texture::SetTexelData (const std::vector<sf::Uint8>& newTexelData, Int width, Int height)
{
	Release(); //Clear old texture data since this function would completely replace it.

	sf::Uint8* textureData = new (std::nothrow) sf::Uint8[width.Value * height.Value * 4];
	if (textureData == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to allocate memory to update pixel data for %s."), TextureName);
		return false;
	}

	// Copy pixelData to textureData
	for (size_t i = 0; i < newTexelData.size(); i++)
	{
		textureData[i] = newTexelData.at(i);
	}

	TextureResource = new sf::Texture();
	{
		SfmlOutputStream sfmlOutput;

		if (!TextureResource->create(width.ToUnsignedInt32(), height.ToUnsignedInt32()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create a %s x %s texture."), width, height);
			delete[] textureData;
			return false;
		}

		TextureResource->update(textureData);
	}

	Width = width;
	Height = height;

	delete[] textureData;
	return true;
}

void Texture::SetSmooth (bool bSmooth)
{
	TextureResource->setSmooth(bSmooth);
}

void Texture::SetRepeat (bool bRepeating)
{
	TextureResource->setRepeated(bRepeating);
}

bool Texture::GenerateMipMap ()
{
	if (bHasMipMap)
	{
		return true;
	}

	if (TextureResource != nullptr && TextureResource->generateMipmap())
	{
		bHasMipMap = true;
	}

	return bHasMipMap;
}

void Texture::SetResource (sf::Texture* newResource)
{
	if (TextureResource != nullptr)
	{
		Release();
	}

	TextureResource = newResource;
	if (TextureResource != nullptr)
	{
		sf::Vector2u dimensions = TextureResource->getSize();
		Width = dimensions.x;
		Height = dimensions.y;
	}
}

sf::Texture* Texture::EditTextureResource ()
{
	return TextureResource;
}

void Texture::GetDimensions (Int &outWidth, Int &outHeight) const
{
	outWidth = Width;
	outHeight = Height;
}

void Texture::GetDimensions (Vector2& outDimensions) const
{
	outDimensions.X = Width.ToFloat();
	outDimensions.Y = Height.ToFloat();
}

Vector2 Texture::GetDimensions () const
{
	return Vector2(Width.ToFloat(), Height.ToFloat());
}

Int Texture::GetWidth () const
{
	return Width;
}

Int Texture::GetHeight () const
{
	return Height;
}

DString Texture::GetTextureName () const
{
	return TextureName;
}

const sf::Texture* Texture::GetTextureResource () const
{
	return TextureResource;
}

bool Texture::GetImage (sf::Image& outImage) const
{
	if (TextureResource == nullptr)
	{
		return false;
	}

	outImage = TextureResource->copyToImage();
	return true;
}
SD_END