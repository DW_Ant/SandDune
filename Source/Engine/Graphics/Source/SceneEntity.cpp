/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneEntity.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::SceneEntity, SD::Entity)
SD_BEGIN
SD_END