/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacGraphics.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef PLATFORM_MAC

SD_BEGIN
Int GRAPHICS_API OS_GetSystemDpi (const Window* windowHandle)
{
#error Implement this function 'OS_GetSystemDpi'
}

Rectangle OS_GetClientWindowPos (const Window* windowHandle)
{
#error Implement this function 'OS_GetClientWindowPos'
}
SD_END

#endif