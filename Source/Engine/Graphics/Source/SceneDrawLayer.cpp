/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneDrawLayer.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::SceneDrawLayer, SD::DrawLayer)
SD_BEGIN

void SceneDrawLayer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, SceneDrawLayer, HandleGarbageCollection, void));
	}
}

void SceneDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	SceneCamera* sceneCam = dynamic_cast<SceneCamera*>(cam);
	if (sceneCam != nullptr)
	{
		RenderComponents(renderTarget, sceneCam);
	}
	else
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to render SceneDrawLayer since the given camera is not a SceneCamera."));
	}
}

void SceneDrawLayer::Destroyed ()
{
#ifdef DEBUG_MODE
	for (SRegisteredComponent comp : ComponentsToDraw)
	{
		comp.Component->NumRegisteredSceneLayers--;
	}
#endif

	//Remove this draw layer from the graphics engine in case this is a default scene draw layer.
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	if (localGraphicsEngine != nullptr)
	{
		localGraphicsEngine->RemoveDefaultSceneDrawLayer(this);
	}

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, SceneDrawLayer, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

bool SceneDrawLayer::RegisterSingleComponent (RenderComponent* newRenderComp)
{
#if ENABLE_COMPLEX_CHECKING
	//Make sure the component is registered at most once
	for (UINT_TYPE i = 0; i < ComponentsToDraw.size(); ++i)
	{
		CHECK(ComponentsToDraw.at(i).Component != newRenderComp)
	}
#endif

	if (dynamic_cast<const SceneTransform*>(newRenderComp->GetOwnerTransform()) == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot register the component %s to the SceneDrawLayer since the RenderComponent's owner does not does not implement the SceneTransform."), newRenderComp->ToString());
		return false;
	}

	ComponentsToDraw.push_back(newRenderComp); //Add to end anyways. This component will be sorted before its rendered.
#ifdef DEBUG_MODE
	newRenderComp->NumRegisteredSceneLayers++;
#endif

	return true;
}

void SceneDrawLayer::RenderComponents (RenderTarget* renderTarget, SceneCamera* cam)
{
	//Early out if there aren't any components to render (since this function is slow)
	if (ContainerUtils::IsEmpty(ComponentsToDraw) || cam == nullptr || renderTarget == nullptr)
	{
		return;
	}

	PurgeExpiredComponents();
	SortRenderComponents(cam);

	for (UINT_TYPE i = 0; i < ComponentsToDraw.size(); ++i)
	{
		CHECK(ComponentsToDraw.at(i).Component->GetOwnerTransform() != nullptr)

		ComponentsToDraw.at(i).Component->GetOwnerTransform()->MarkProjectionDataDirty();
		if (ComponentsToDraw.at(i).Component->IsRelevant(renderTarget, cam))
		{
			ComponentsToDraw.at(i).Component->Render(renderTarget, cam);
		}
	}
}

void SceneDrawLayer::SortRenderComponents (SceneCamera* cam)
{
	//Compute the relative distances once before moving to the sort function.
	for (size_t i = 0; i < ComponentsToDraw.size(); ++i)
	{
		SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(ComponentsToDraw.at(i).Component->GetOwnerTransform());
		CHECK(ownerTransform != nullptr)

		ComponentsToDraw.at(i).DistSquared = cam->GetDistSquaredToPoint(ownerTransform->ReadAbsTranslation());
	}

	//Objects earlier in the rendered list are drawn before others, which makes objects later in the list render over others.
	//Sort based on their relative distance to the camera.
	std::sort(ComponentsToDraw.begin(), ComponentsToDraw.end(), [](const SRegisteredComponent& a, const SRegisteredComponent& b)
	{
		//Returns true if the element should appear earlier in the list.
		return (a.DistSquared > b.DistSquared);
	});
}

void SceneDrawLayer::PurgeExpiredComponents ()
{
	//Remove empty/destroyed components from list
	UINT_TYPE i = 0;
	while (i < ComponentsToDraw.size())
	{
		if (!VALID_OBJECT(ComponentsToDraw.at(i).Component))
		{
			ComponentsToDraw.erase(ComponentsToDraw.begin() + i);
			continue;
		}

		//Make sure the components are still a component of a scene transform object (this could happen if they changed owners after registration).
		if (dynamic_cast<const SceneTransform*>(ComponentsToDraw.at(i).Component->GetOwnerTransform()) == nullptr)
		{
#ifdef DEBUG_MODE
			ComponentsToDraw.at(i).Component->NumRegisteredSceneLayers--;
#endif

			ComponentsToDraw.erase(ComponentsToDraw.begin() + i);
			continue;
		}

		++i;
	}
}

void SceneDrawLayer::HandleGarbageCollection ()
{
	PurgeExpiredComponents();
}
SD_END