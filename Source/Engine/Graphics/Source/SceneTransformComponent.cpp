/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTransformComponent.cpp
=====================================================================
*/

#include "SceneTransformComponent.h"

IMPLEMENT_CLASS(SD::SceneTransformComponent, SD::EntityComponent)
SD_BEGIN

void SceneTransformComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	//Climb up the component chain until it finds a SceneTransform.
	for (Entity* curEntity = Owner.Get(); curEntity != nullptr; /* Noop */)
	{
		if (SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(curEntity))
		{
			SetRelativeTo(ownerTransform);
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(curEntity))
		{
			comp->OnOwnerChanged.RegisterHandler(SDFUNCTION_2PARAM(this, SceneTransformComponent, HandleOwnerChange, void, EntityComponent*, Entity*));
			curEntity = comp->GetOwner();
			continue;
		}

		break;
	}
}

void SceneTransformComponent::ComponentDetached ()
{
	//Unregister callbacks if any
	for (Entity* curEntity = Owner.Get(); curEntity != nullptr; /* Noop */)
	{
		if (dynamic_cast<SceneTransform*>(curEntity) != nullptr)
		{
			//Found a scene transform. This Entity and anything above it (even if it's a component) would not have the callback.
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(curEntity))
		{
			comp->OnOwnerChanged.UnregisterHandler(SDFUNCTION_2PARAM(this, SceneTransformComponent, HandleOwnerChange, void, EntityComponent*, Entity*));
			curEntity = comp->GetOwner();
			continue;
		}

		break;
	}

	SetRelativeTo(nullptr);

	Super::ComponentDetached();
}

void SceneTransformComponent::HandleOwnerChange (EntityComponent* compThatChangedOwners, Entity* prevOwner)
{
	SceneTransform* newRelativeTo = nullptr;

	//Unbind anything between prevOwner to the scene transform it's relative to (or the root Entity if none).
	for (Entity* curEntity = prevOwner; curEntity != nullptr; /* Noop */ )
	{
		if (dynamic_cast<SceneTransform*>(curEntity) != nullptr)
		{
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(curEntity))
		{
			comp->OnOwnerChanged.UnregisterHandler(SDFUNCTION_2PARAM(this, SceneTransformComponent, HandleOwnerChange, void, EntityComponent*, Entity*));
			curEntity = comp->GetOwner();
			continue;
		}

		break;
	}

	//Bind anything between compThatChanged to any EntityComponents until it finds root or SceneTransform.
	for (Entity* curEntity = compThatChangedOwners->GetOwner(); curEntity != nullptr; /* Noop */ )
	{
		if (newRelativeTo = dynamic_cast<SceneTransform*>(curEntity))
		{
			break;
		}

		if (EntityComponent* comp = dynamic_cast<EntityComponent*>(curEntity))
		{
			comp->OnOwnerChanged.RegisterHandler(SDFUNCTION_2PARAM(this, SceneTransformComponent, HandleOwnerChange, void, EntityComponent*, Entity*));
			curEntity = comp->GetOwner();
			continue;
		}

		break;
	}

	//Note: It's possible that newRelativeTo is nullptr. This will cause this SceneTransformComponent to assume the root transform.
	if (newRelativeTo != GetRelativeTo())
	{
		SetRelativeTo(newRelativeTo);
	}
}
SD_END