/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InstancedSpriteComponent.cpp
=====================================================================
*/

#include "Camera.h"
#include "Color.h"
#include "InstancedSpriteComponent.h"
#include "RenderTarget.h"
#include "Texture.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::InstancedSpriteComponent, SD::RenderComponent)
SD_BEGIN

const size_t InstancedSpriteComponent::NUM_VERTICES_PER_SPRITE = 6;

void InstancedSpriteComponent::InitProps ()
{
	Super::InitProps();

	SpriteTexture = nullptr;
	RenderState = sf::RenderStates::Default;
	NumSubDivisionsX = 1;
	NumSubDivisionsY = 1;
	Vertices.setPrimitiveType(sf::PrimitiveType::Triangles);
}

void InstancedSpriteComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const InstancedSpriteComponent* copyObj = dynamic_cast<const InstancedSpriteComponent*>(objTemplate))
	{
		SpriteTexture = copyObj->SpriteTexture;
		RenderState = copyObj->RenderState;
		NumSubDivisionsX = copyObj->NumSubDivisionsX;
		NumSubDivisionsY = copyObj->NumSubDivisionsY;

		ContainerUtils::Empty(SpriteInstances);
		for (size_t i = 0; i < copyObj->SpriteInstances.size(); ++i)
		{
			SpriteInstances.push_back(SSpriteInstance(copyObj->SpriteInstances.at(i)));
		}
	}
}

void InstancedSpriteComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	CHECK(GetOwnerTransform() != nullptr && SpriteTexture.IsValid())

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	RenderState.transform = sf::Transform::Identity;
	RenderState.transform.translate(projectionData.Position);
	RenderState.transform.scale(projectionData.Scale);
	RenderState.transform.rotate(projectionData.Rotation);

	renderTarget->Draw(this, camera, Vertices, RenderState);
}

Aabb InstancedSpriteComponent::GetAabb () const
{
	if (BaseBoundingBox.IsEmpty())
	{
		sf::FloatRect sfBounds = Vertices.getBounds();

		Vector3 center(sfBounds.left + (sfBounds.width * 0.5f), sfBounds.top + (sfBounds.height * 0.5f), 0.f);
		BaseBoundingBox = Aabb(sfBounds.width, 0.f, sfBounds.height, center);
	}

	return BaseBoundingBox;
}

void InstancedSpriteComponent::PopulateInstances (Int numInstancesPerCell, Int cellSize, const Vector2& dimensions, Range<Float> sizeRange, bool uniformSize, bool randomRotation, Texture* densityMap)
{
	CHECK(NumSubDivisionsX > 0 && NumSubDivisionsY > 0)

	if (numInstancesPerCell <= 0)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to populate sprite instances using a non positive numInstancesPerCell (%s). Setting minimum value to 1."), numInstancesPerCell);
		numInstancesPerCell = 1;
	}

	if (cellSize <= 0)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to populate sprite instances using a nonpositive cellSize (%s). Setting it to the minimum value of 1."), cellSize);
		cellSize = 1;
	}

	if (dimensions.X <= 0.f || dimensions.Y <= 0.f)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to populate sprite instances using a nonpositive width or height (%s)."), dimensions);
		return;
	}

	ContainerUtils::Empty(SpriteInstances);

	std::function<void(SSpriteInstance&)> initializeSpriteInstance([&](SSpriteInstance& outNewInstance)
	{
		outNewInstance.Size.X = RandomUtils::RandRange(sizeRange.Min, sizeRange.Max);
		outNewInstance.Size.Y = (uniformSize) ? outNewInstance.Size.X : RandomUtils::RandRange(sizeRange.Min, sizeRange.Max);

		outNewInstance.Rotation = (randomRotation) ? Rotator(static_cast<unsigned int>(RandomUtils::Rand(Rotator::FULL_REVOLUTION)), 0, 0) : Rotator::ZERO_ROTATOR;
		outNewInstance.SubDivisionIdxX = RandomUtils::Rand(NumSubDivisionsX).ToInt32();
		outNewInstance.SubDivisionIdxY = RandomUtils::Rand(NumSubDivisionsY).ToInt32();
	});

	//Handle simple case without a density mapping
	if (densityMap == nullptr)
	{
		for (size_t i = 0; i < numInstancesPerCell; ++i)
		{
			SpriteInstances.push_back(SSpriteInstance());

			SSpriteInstance& newInstance = SpriteInstances[SpriteInstances.size() - 1];
			newInstance.Position.X = RandomUtils::RandRange(dimensions.X * -0.5f, dimensions.X * 0.5f);
			newInstance.Position.Y = RandomUtils::RandRange(dimensions.Y * -0.5f, dimensions.Y * 0.5f);
			initializeSpriteInstance(newInstance);
		}
	}
	else
	{
		Int textureWidth;
		Int textureHeight;
		densityMap->GetDimensions(OUT textureWidth, OUT textureHeight);

		CHECK(densityMap->GetTextureResource() != nullptr)
		sf::Image img = densityMap->GetTextureResource()->copyToImage();

		//texelSize: How many units in planar space does each texel consume
		Float texelSizeX = dimensions.X / textureWidth.ToFloat();
		Float texelSizeY = dimensions.Y / textureHeight.ToFloat();

		//CellPosition is the top left corner of the SpriteInstance location range for this particular cell.
		Vector2 cellPosition(0.f, dimensions.Y * -0.5f);
		Float floatCellSize = cellSize.ToFloat();

		//Displace a bit since the beginning of the loops increments this by cellSize.
		cellPosition.Y -= floatCellSize * texelSizeY;

		//Iterate through each cell and populate instances within each cell.
		for (Int texelIdxY = 0; texelIdxY < textureHeight; texelIdxY += cellSize)
		{
			cellPosition.Y += (floatCellSize * texelSizeY);

			//Reset xPos back to the left.
			cellPosition.X = (dimensions.X * -0.5f) - (floatCellSize * texelSizeX);
			for (Int texelIdxX = 0; texelIdxX < textureWidth; texelIdxX += cellSize)
			{
				cellPosition.X += (floatCellSize * texelSizeX);
				Float numInstances = numInstancesPerCell.ToFloat();
				{
					std::vector<sf::Color> cellPixels;

					//Find the average color of all pixels within this cell
					for (Int cellHeight = 0; cellHeight < cellSize && (cellHeight + texelIdxY) < textureHeight; ++cellHeight)
					{
						for (Int cellWidth = 0; cellWidth < cellSize && (cellWidth + texelIdxX) < textureWidth; ++cellWidth)
						{
							cellPixels.push_back(img.getPixel((cellWidth + texelIdxX).ToUnsignedInt32(), (cellHeight + texelIdxY).ToUnsignedInt32()));
						}
					}
					CHECK(!ContainerUtils::IsEmpty(cellPixels));

					//Divide number of instances based on this cell size (eg: If this cell is cut off a bit early due to being over to the texture's edge)
					numInstances *= (Float::MakeFloat(cellPixels.size()) / (cellSize * cellSize).ToFloat());

					//Compute the average color brightness (ignoring alpha channels)
					Int avgColor = 0;
					for (size_t i = 0; i < cellPixels.size(); ++i)
					{
						avgColor += cellPixels.at(i).r;
						avgColor += cellPixels.at(i).g;
						avgColor += cellPixels.at(i).b;
					}
					avgColor /= cellPixels.size();

					const Float maxBrightness = 765.f; //3 channels (rgb) that can be 255 for each channel.
					numInstances *= (avgColor.ToFloat() / maxBrightness);
					numInstances.RoundInline();
				}

				for (Int spriteInstance = 0; spriteInstance < numInstances.ToInt(); ++spriteInstance)
				{
					SpriteInstances.push_back(SSpriteInstance());

					SSpriteInstance& newInstance = SpriteInstances[SpriteInstances.size() - 1];

					newInstance.Position.X = RandomUtils::RandRange(cellPosition.X, cellPosition.X + (texelSizeX * floatCellSize));
					newInstance.Position.Y = RandomUtils::RandRange(cellPosition.Y, cellPosition.Y + (texelSizeY * floatCellSize));

					newInstance.Position += dimensions * 0.5f; //The sprite should be relative to the center rather than top left corner.

					initializeSpriteInstance(newInstance);
				}
			}
		}
	}

	RegenerateVertices();
}

void InstancedSpriteComponent::RegenerateVertices (const std::vector<size_t>& spriteInstanceIndices)
{
	if (Vertices.getVertexCount() != SpriteInstances.size() * NUM_VERTICES_PER_SPRITE)
	{
		Vertices.resize(SpriteInstances.size() * NUM_VERTICES_PER_SPRITE);
	}

	CHECK(SpriteTexture.IsValid())
	Vector2 textureSize = SpriteTexture->GetDimensions();
	Vector2 subDivisionLengths(textureSize.X / NumSubDivisionsX.ToFloat(), textureSize.Y / NumSubDivisionsY.ToFloat());

	if (ContainerUtils::IsEmpty(spriteInstanceIndices))
	{
		//Update everything
		for (size_t i = 0; i < SpriteInstances.size(); ++i)
		{
			UpdateVertexArray(i);
			UpdateTextureCoordinates(i, subDivisionLengths);
		}
	}
	else
	{
		//Update specified instances
		for (size_t i = 0; i < spriteInstanceIndices.size(); ++i)
		{
			if (!ContainerUtils::IsValidIndex(SpriteInstances, spriteInstanceIndices.at(i)))
			{
				GraphicsLog.Log(LogCategory::LL_Warning, TXT("Invalid sprite index %s. Out of range for the SpriteInstance vector of size %s."), Int(i), Int(SpriteInstances.size()));
				continue;
			}

			UpdateVertexArray(spriteInstanceIndices.at(i));
			UpdateTextureCoordinates(spriteInstanceIndices.at(i), subDivisionLengths);
		}
	}

	MarkAabbAsDirty();
}

void InstancedSpriteComponent::RegenerateVertices ()
{
	//Don't initialize sprite instance vector to notify the function to initialize everything.
	std::vector<size_t> unusedSpriteInstanceIndices;
	RegenerateVertices(unusedSpriteInstanceIndices);
}

void InstancedSpriteComponent::MarkAabbAsDirty ()
{
	BaseBoundingBox = Aabb();
}

void InstancedSpriteComponent::SetSpriteTexture (const Texture* newTexture)
{
	if (newTexture == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Can't assign null textures to sprite instances."));
		return;
	}

	bool regenerateTextureCoordinates = (SpriteTexture.IsNullptr() || newTexture->GetDimensions() != SpriteTexture->GetDimensions());
	SpriteTexture = newTexture;
	RenderState.texture = SpriteTexture->GetTextureResource();

	if (regenerateTextureCoordinates)
	{
		Vector2 textureSize = SpriteTexture->GetDimensions();
		Vector2 subDivisionLengths(textureSize.X / NumSubDivisionsX.ToFloat(), textureSize.Y / NumSubDivisionsY.ToFloat());

		for (size_t i = 0; i < SpriteInstances.size(); ++i)
		{
			UpdateTextureCoordinates(i, subDivisionLengths);
		}
	}
}

void InstancedSpriteComponent::SetNumSubDivisions (Int newNumSubDivisionsX, Int newNumSubDivisionsY)
{
	if (newNumSubDivisionsX <= 0 || newNumSubDivisionsY <= 0)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to set a non positive integer for number of sub divisions (%s and %s). Setting parameters to the minimum value of 1."), newNumSubDivisionsX, newNumSubDivisionsY);
		newNumSubDivisionsX = Utils::Max<Int>(newNumSubDivisionsX, 1);
		newNumSubDivisionsY = Utils::Max<Int>(newNumSubDivisionsY, 1);
	}

	NumSubDivisionsX = newNumSubDivisionsX;
	NumSubDivisionsY = newNumSubDivisionsY;
}

void InstancedSpriteComponent::UpdateVertexArray (size_t spriteInstanceIdx)
{
	CHECK(ContainerUtils::IsValidIndex(SpriteInstances, spriteInstanceIdx))

	size_t vertexIdx = spriteInstanceIdx * NUM_VERTICES_PER_SPRITE;

	/*
	Each sprite will generate six vertices to form two triangles.
	2---------0&3
	|        /|
	|       / |
	|      /  |
	|     /   |
	|    +    |
	|   /     |
	|  /      |
	| /       |
	1&5_______4

	The plus sign is the center point/pivot of the sprite.
	*/
	Vector2 spriteVertices[NUM_VERTICES_PER_SPRITE];
	spriteVertices[0] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * 0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * -0.5f);
	spriteVertices[1] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * -0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * 0.5f);
	spriteVertices[2] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * -0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * -0.5f);
	spriteVertices[3] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * 0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * -0.5f);
	spriteVertices[4] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * 0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * 0.5f);
	spriteVertices[5] = Vector2(SpriteInstances[spriteInstanceIdx].Size.X * -0.5f, SpriteInstances[spriteInstanceIdx].Size.Y * 0.5f);

	const float radians = SpriteInstances[spriteInstanceIdx].Rotation.GetYaw(Rotator::RU_Radians).Value;

	//Rotate each vertex about the center. Then displace it by the sprite's position offset.
	for (size_t i = 0; i < NUM_VERTICES_PER_SPRITE; ++i)
	{
		Float newXPos = (spriteVertices[i].X * std::cos(radians)) - (spriteVertices[i].Y * std::sin(radians));
		spriteVertices[i].Y = (spriteVertices[i].X * std::sin(radians)) + (spriteVertices[i].Y * std::cos(radians));
		spriteVertices[i].X = newXPos;
		spriteVertices[i] += SpriteInstances[spriteInstanceIdx].Position;
	}

	for (size_t i = 0; i < NUM_VERTICES_PER_SPRITE; ++i)
	{
		Vertices[vertexIdx + i].position = Vector2::SDtoSFML(spriteVertices[i]);
		Vertices[vertexIdx + i].color = Color::WHITE.Source;
	}
}

void InstancedSpriteComponent::UpdateTextureCoordinates (size_t spriteInstanceIdx, const Vector2& subDivisionLengths)
{
	CHECK(ContainerUtils::IsValidIndex(SpriteInstances, spriteInstanceIdx))

	size_t vertexIdx = spriteInstanceIdx * NUM_VERTICES_PER_SPRITE;
	Vertices[vertexIdx].texCoords.x = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxX + 1) * subDivisionLengths.X).Value;
	Vertices[vertexIdx].texCoords.y = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxY) * subDivisionLengths.Y).Value;

	Vertices[vertexIdx+1].texCoords.x = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxX) * subDivisionLengths.X).Value;
	Vertices[vertexIdx+1].texCoords.y = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxY + 1) * subDivisionLengths.Y).Value;

	Vertices[vertexIdx+2].texCoords.x = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxX) * subDivisionLengths.X).Value;
	Vertices[vertexIdx+2].texCoords.y = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxY) * subDivisionLengths.Y).Value;

	Vertices[vertexIdx+3].texCoords = Vertices[vertexIdx].texCoords;

	Vertices[vertexIdx+4].texCoords.x = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxX + 1) * subDivisionLengths.X).Value;
	Vertices[vertexIdx+4].texCoords.y = (Float::MakeFloat(SpriteInstances[spriteInstanceIdx].SubDivisionIdxY + 1) * subDivisionLengths.Y).Value;

	Vertices[vertexIdx+5].texCoords = Vertices[vertexIdx+1].texCoords;
}
SD_END