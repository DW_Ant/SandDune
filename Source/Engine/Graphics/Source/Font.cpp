/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Font.cpp
=====================================================================
*/

#include "Font.h"

IMPLEMENT_CLASS(SD::Font, SD::Object)
SD_BEGIN

const Int Font::NUM_SPACES_PER_TAB = 4;

void Font::InitProps ()
{
	Super::InitProps();

	FontName = TXT("UnknownFont");
	FontResource = nullptr;
	UniformedCharWidth = -1.f;
}

DString Font::GetFriendlyName () const
{
	if (!FontName.IsEmpty())
	{
		return TXT("Font:  ") + FontName;
	}

	return Super::GetFriendlyName();
}

void Font::Destroyed ()
{
	Release();

	Super::Destroyed();
}

void Font::Release ()
{
	if (FontResource != nullptr)
	{
		delete FontResource;
	}

	FontResource = nullptr;
}

void Font::SetResource (sf::Font* newResource)
{
	FontResource = newResource;
}

Float Font::GetCharacterWidth (const DString& character, Int charSize) const
{
	if (FontResource == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate character width for \"%s\" since a font resource is not yet assigned to %s"), DString(character), ToString());
		return 0.f;
	}

	Float glyphWidth(UniformedCharWidth);
	if (glyphWidth <= 0.f)
	{
		CHECK(character.Length() == 1)
		sf::Uint32 codePoint = character.ToUTF32().at(0);
		if (!FontResource->hasGlyph(codePoint))
		{
			//Special case for fonts that don't have glyphs for tabs
			if (character[0] == '\t')
			{
				return GetCharacterWidth(' ', charSize) * Float(NUM_SPACES_PER_TAB);
			}

			return 0.f;
		}

		const sf::Glyph& glyph = FontResource->getGlyph(codePoint, charSize.ToUnsignedInt32(), false);
		glyphWidth = Float(glyph.advance);
	}

	switch (character[0])
	{
		case('\t'):
			return glyphWidth * Float(NUM_SPACES_PER_TAB);

		case('\n'):
		case('\r'):
			return 0.f;
	}

	return glyphWidth;
}

Float Font::CalculateStringWidth (const DString& line, Int charSize) const
{
	return CalculateStringWidth(line, charSize, 0);
}

Float Font::CalculateStringWidth (const DString& line, Int charSize, Int startingIdx, Int numChars) const
{
	if (numChars < 0)
	{
		numChars = line.Length() - startingIdx;
	}

	CHECK(numChars + startingIdx <= line.Length())
	Float result = 0.f;
	StringIterator iter(&line);
	for (Int i = 0; i < startingIdx; i++)
	{
		++iter;
	}

	for (Int i = 0; i < numChars; i++)
	{
		result += GetCharacterWidth(*iter, charSize);
		++iter;
	}

	return result;
}

Int Font::FindCharNearWidthLimit (const DString& line, Int charSize, Float maxWidth) const
{
	Float curWidth = 0.f;

	Int charIdx = 0;
	for (StringIterator iter(&line); !iter.IsAtEnd(); ++iter, charIdx++)
	{
		Float newWidth = GetCharacterWidth(*iter, charSize);
		if (curWidth + newWidth > maxWidth)
		{
			return (charIdx - 1); //return previous character that fits within bounds.
		}

		curWidth += newWidth;
	}

	return line.Length() - 1; //The whole string fits, return the last character.
}

DString Font::GetFontName () const
{
	return FontName;
}

const sf::Font* Font::GetFontResource () const
{
	return FontResource;
}
SD_END