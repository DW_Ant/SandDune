/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarTransformUpdater.cpp
=====================================================================
*/

#include "PlanarTransform.h"
#include "PlanarTransformUpdater.h"

IMPLEMENT_CLASS(SD::PlanarTransformUpdater, SD::Entity)
SD_BEGIN

void PlanarTransformUpdater::InitProps ()
{
	Super::InitProps();

	bLocked = false;
}

void PlanarTransformUpdater::BeginObject ()
{
	Super::BeginObject();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_TRANSFORM_UPDATE);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, PlanarTransformUpdater, HandleTick, void, Float));
	}
}

void PlanarTransformUpdater::AddDirtyTransform (PlanarTransform* newTransform)
{
	if (bLocked)
	{
		LockedDirtyTransforms.push_back(newTransform);
	}
	else
	{
		DirtyTransforms.push_back(newTransform);
	}
}

void PlanarTransformUpdater::RemoveDirtyTransform (PlanarTransform* oldTransform)
{
	for (size_t i = 0; i < LockedDirtyTransforms.size(); ++i)
	{
		if (LockedDirtyTransforms.at(i) == oldTransform)
		{
			LockedDirtyTransforms.at(i) = nullptr;
			//Continue in case this transform is added multiple times.
		}
	}

	for (size_t i = 0; i < DirtyTransforms.size(); ++i)
	{
		if (DirtyTransforms.at(i) == oldTransform)
		{
			DirtyTransforms.at(i) = nullptr;
			//Continue in case this transform is added multiple times.
		}
	}

	//Remove it from the callback vector
	for (size_t i = 0; i < PendingCallbackTransforms.size(); ++i)
	{
		if (PendingCallbackTransforms.at(i) == oldTransform)
		{
			PendingCallbackTransforms.at(i) = nullptr; //Don't erase from vector in case this is currently accessed.
			break; //Transforms are only added to this vector once per update. Safe to break.
		}
	}
}

void PlanarTransformUpdater::AddPendingCallbackTransform (PlanarTransform* transform)
{
	if (transform != nullptr)
	{
		PendingCallbackTransforms.push_back(transform);
	}
}

void PlanarTransformUpdater::UpdateTransforms ()
{
	bLocked = true;

	for (size_t i = 0; i < DirtyTransforms.size(); ++i)
	{
		if (DirtyTransforms.at(i) != nullptr)
		{
			DirtyTransforms.at(i)->ComputeAbsTransform();
		}
	}

	//Invoke the callback after everything is computed since subclasses may care about their child transforms' absolute attributes.
	for (size_t i = 0; i < PendingCallbackTransforms.size(); ++i)
	{
		if (PendingCallbackTransforms.at(i) != nullptr)
		{
			PendingCallbackTransforms.at(i)->PostAbsTransformUpdate();
		}
	}
	ContainerUtils::Empty(OUT DirtyTransforms);
	ContainerUtils::Empty(OUT PendingCallbackTransforms);

	//Move all locked transforms to the dirty list.
	for (PlanarTransform* lockedTransform : LockedDirtyTransforms)
	{
		DirtyTransforms.push_back(lockedTransform);
	}
	ContainerUtils::Empty(OUT LockedDirtyTransforms);
	bLocked = false;
}

void PlanarTransformUpdater::HandleTick (Float deltaSec)
{
	UpdateTransforms();
}
SD_END