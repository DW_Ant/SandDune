/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlanarCamera.cpp
=====================================================================
*/

#include "PlanarCamera.h"

IMPLEMENT_CLASS(SD::PlanarCamera, SD::Camera)
SD_BEGIN

void PlanarCamera::InitProps ()
{
	Super::InitProps();

	Zoom = 1.f;
	ZoomRange = Range<Float>(0.01f, 20.f);
	ViewExtents = Vector2::ZERO_VECTOR;
}

void PlanarCamera::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	Vector2 planeLocation = GetCachedAbsPosition();

	Float zoomReciprocal = 1 / Zoom;

	//pixelPos is relative to the top left corner of viewport, but the viewport's center draws from the camera.  Offset pixelPos by half the viewport size.
	planeLocation -= ((viewportSize * zoomReciprocal) * 0.5f);
	planeLocation += (pixelPos * zoomReciprocal);

	outLocation = planeLocation.ToVector3();

	//Planar cameras don't have height and direction.
	outDirection = Rotator::ZERO_ROTATOR;
}

void PlanarCamera::SetZoom (Float newZoom)
{
	newZoom = ZoomRange.GetClampedValue(newZoom);
	if (Zoom != newZoom)
	{
		Zoom = newZoom;
		OnZoomChanged.Broadcast(Zoom);
	}
}

void PlanarCamera::SetZoomRange (const Range<Float>& newZoomRange)
{
	ZoomRange = newZoomRange;

	if (ZoomRange.Min <= 0.f || ZoomRange.Max <= 0.f)
	{
		ZoomRange = Range<Float>(0.01f, 20.f);
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to set a PlanarCamera's ZoomRange to %s. Clamping zoom to %s"), newZoomRange, ZoomRange);
	}

	ZoomRange.FixRange();
	if (!ZoomRange.ContainsValue(Zoom))
	{
		SetZoom(ZoomRange.GetClampedValue(Zoom));
	}
}

void PlanarCamera::SetViewExtents (const Vector2& newViewExtents)
{
	ViewExtents = newViewExtents;
}
SD_END