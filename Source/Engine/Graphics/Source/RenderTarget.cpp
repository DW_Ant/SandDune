/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTarget.cpp
=====================================================================
*/

#include "Camera.h"
#include "DrawLayer.h"
#include "RenderTarget.h"
#include "ScreenCapture.h"

IMPLEMENT_ABSTRACT_CLASS(SD::RenderTarget, SD::Entity)
SD_BEGIN

void RenderTarget::InitProps ()
{
	Super::InitProps();

	ParentRenderTarget = nullptr;
	Tick = nullptr;

	DestroyCamerasOnDestruction = true;
	DestroyDrawLayersOnDestruction = true;
}

void RenderTarget::BeginObject ()
{
	Super::BeginObject();

	InitTick();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, RenderTarget, HandleGarbageCollection, void));
}

void RenderTarget::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, RenderTarget, HandleGarbageCollection, void));

	if (DestroyCamerasOnDestruction)
	{
		for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
		{
			if (DrawLayers.at(i).Cam != nullptr)
			{
				DrawLayers.at(i).Cam->Destroy();
				DrawLayers.at(i).Cam = nullptr;
			}
		}
	}

	if (DestroyDrawLayersOnDestruction)
	{
		for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
		{
			if (DrawLayers.at(i).Layer != nullptr)
			{
				DrawLayers.at(i).Layer->Destroy();
				DrawLayers.at(i).Layer = nullptr;
			}
		}
	}

	if (RenderCapture != nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Destroyed a RenderTarget while it's in the process of capturing a scene."));
		RenderCapture->FinishRenderCycle();
		RenderCapture = nullptr;
	}

	Super::Destroyed();
}

void RenderTarget::GenerateScene ()
{
	RemovePurgedDrawLayers();
	Reset();

	if (ParentRenderTarget.IsValid())
	{
		for (UINT_TYPE i = 0; i < ParentRenderTarget->DrawLayers.size(); ++i)
		{
			ParentRenderTarget->DrawLayers.at(i).Layer->RenderDrawLayer(this, ParentRenderTarget->DrawLayers.at(i).Cam);
		}
	}

	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		DrawLayers.at(i).Layer->RenderDrawLayer(this, DrawLayers.at(i).Cam);
	}

	Display();
}

void RenderTarget::Draw (RenderComponent* renderComp, const Camera* cam, const sf::Drawable& drawable, const sf::RenderStates& renderState)
{
	if (RenderCapture != nullptr)
	{
		RenderCapture->ProcessRenderComponent(renderComp, cam);
	}
}

void RenderTarget::Display ()
{
	if (RenderCapture != nullptr)
	{
		RenderCapture->FinishRenderCycle();
		RenderCapture = nullptr;
	}
}

void RenderTarget::RegisterDrawLayer (DrawLayer* drawLayer, Camera* associatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == drawLayer)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot register %s to %s more than once."), drawLayer->ToString(), ToString());
			return;
		}
	}

	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer->GetDrawPriority() > drawLayer->GetDrawPriority())
		{
			DrawLayers.insert(DrawLayers.begin() + i, SDrawLayerCamera(drawLayer, associatedCamera));
			return;
		}
	}

	//This draw layer has highest priority since there aren't any layers with greater priority values.  Add to end of list to make it render over others.
	DrawLayers.push_back(SDrawLayerCamera(drawLayer, associatedCamera));
}

void RenderTarget::RemoveDrawLayer (DrawLayer* target)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == target)
		{
			DrawLayers.erase(DrawLayers.begin() + i);
			break;
		}
	}
}

bool RenderTarget::AssignCamera (DrawLayer* targetDrawLayer, Camera* newAssociatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == targetDrawLayer)
		{
			DrawLayers.at(i).Cam = newAssociatedCamera;
			return true;
		}
	}

	return false;
}

bool RenderTarget::AssignCamera (Int drawLayerPriority, Camera* newAssociatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer->GetDrawPriority() == drawLayerPriority)
		{
			DrawLayers.at(i).Cam = newAssociatedCamera;
			return true;
		}
	}

	return false;
}

void RenderTarget::SetDrawLayersFrom (RenderTarget* targetToCopyFrom)
{
	CHECK(targetToCopyFrom != this) //Doesn't make sense to copy from self
	DrawLayers = targetToCopyFrom->DrawLayers;
}

Vector2 RenderTarget::GetWindowCoordinates () const
{
	return Vector2::ZERO_VECTOR;
}

void RenderTarget::TransferDrawLayersTo (RenderTarget* transferTo)
{
	if (transferTo == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot transfer draw layers to a null render target."));
		return;
	}

	for (size_t i = 0; i < DrawLayers.size(); ++i)
	{
		transferTo->RegisterDrawLayer(DrawLayers.at(i).Layer, DrawLayers.at(i).Cam);
	}
	ContainerUtils::Empty(OUT DrawLayers);
}

void RenderTarget::SetAutoDraw (bool bAutoGenerateScene, Float updateRate)
{
	if (!bAutoGenerateScene)
	{
		if (Tick != nullptr)
		{
			Tick->Destroy();
			Tick = nullptr;
		}
	}
	else if (Tick == nullptr)
	{
		InitTick();

		CHECK(Tick != nullptr)
		Tick->SetTickInterval(updateRate);
	}
}

void RenderTarget::SetParentRenderTarget (RenderTarget* newParentRenderTarget)
{
	ParentRenderTarget = newParentRenderTarget;
}

void RenderTarget::SetDestroyCamerasOnDestruction (bool newDestroyCamerasOnDestruction)
{
	DestroyCamerasOnDestruction = newDestroyCamerasOnDestruction;
}

void RenderTarget::SetDestroyDrawLayersOnDestruction (bool newDestroyDrawLayersOnDestruction)
{
	DestroyDrawLayersOnDestruction = newDestroyDrawLayersOnDestruction;
}

void RenderTarget::SetScreenCapture (ScreenCapture* newScreenCapture)
{
	CHECK_INFO(RenderCapture == nullptr, "The RenderTarget is already associated with a ScreenCapture.")
	RenderCapture = newScreenCapture;
}

void RenderTarget::InitTick ()
{
	CHECK(Tick == nullptr)

	Tick = TickComponent::CreateObject(TICK_GROUP_RENDER);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, RenderTarget, HandleTick, void, Float));
	}
}

void RenderTarget::RemovePurgedDrawLayers ()
{
	UINT_TYPE i = 0;
	while (i < DrawLayers.size())
	{
		if (!VALID_OBJECT(DrawLayers.at(i).Layer))
		{
			DrawLayers.erase(DrawLayers.begin() + i);
			continue; //Don't increment i since the DrawLayer vector changed size.
		}
		else if (!VALID_OBJECT(DrawLayers.at(i).Cam))
		{
			//Camera was destroyed while a DrawLayer is viewing from that camera.
			//Simply set the cam to null so that the rendered objects are now in screen space.
			DrawLayers.at(i).Cam = nullptr;
		}

		++i;
	}
}

void RenderTarget::HandleTick (Float deltaSec)
{
	GenerateScene();
}

void RenderTarget::HandleGarbageCollection ()
{
	RemovePurgedDrawLayers();
}
SD_END