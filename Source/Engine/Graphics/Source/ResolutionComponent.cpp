/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ResolutionComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

IMPLEMENT_CLASS(SD::ResolutionComponent, SD::EntityComponent)
SD_BEGIN

void ResolutionComponent::InitProps ()
{
	Super::InitProps();

	WindowHandle = nullptr;
}

void ResolutionComponent::Destroyed ()
{
	if (WindowHandle.IsValid())
	{
		SetWindowHandle(nullptr); //Unregister polling delegate
	}

	Super::Destroyed();
}

void ResolutionComponent::SetWindowHandle (Window* newWindowHandle)
{
	if (WindowHandle.IsValid())
	{
		WindowHandle->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, ResolutionComponent, HandleWindowEvent, void, const sf::Event&));
	}

	WindowHandle = newWindowHandle;

	if (WindowHandle.IsValid())
	{
		WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, ResolutionComponent, HandleWindowEvent, void, const sf::Event&));
	}
}

Window* ResolutionComponent::GetWindowHandle () const
{
	return WindowHandle.Get();
}

void ResolutionComponent::HandleWindowEvent (const sf::Event& newEvent)
{
	if (!OnResolutionChange.IsBounded())
	{
		return;
	}

	if (newEvent.type == sf::Event::Resized)
	{
		OnResolutionChange.Execute(Int(newEvent.size.width), Int(newEvent.size.height));
	}
}
SD_END