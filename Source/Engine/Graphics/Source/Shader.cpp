/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Shader.cpp
=====================================================================
*/

#include "Shader.h"

IMPLEMENT_CLASS(SD::Shader, SD::Object)
SD_BEGIN

void Shader::InitProps ()
{
	Super::InitProps();

	SFShader = nullptr;
	ShaderName = DString::EmptyString;
}

DString Shader::GetFriendlyName () const
{
	if (!ShaderName.IsEmpty())
	{
		return TXT("Shader:  ") + ShaderName;
	}

	return Super::GetFriendlyName();
}

void Shader::Destroyed ()
{
	if (SFShader != nullptr)
	{
		delete SFShader;
		SFShader = nullptr;
	}

	Super::Destroyed();
}

Shader* Shader::CreateShader (const FileAttributes& fileName, sf::Shader::Type shaderType)
{
	if (!sf::Shader::isAvailable())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to create shader since the system doesn't support shaders."));
		return nullptr;
	}

	sf::Shader* newShader = new sf::Shader();
	{
		SfmlOutputStream sfOutput;
		if (!newShader->loadFromFile(fileName.GetName(true, true).ToCString(), shaderType))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("There was an error in loading shader %s:  %s"), fileName.GetName(false, true), sfOutput.ReadOutput());
			delete newShader;
			return nullptr;
		}
	}

	Shader* result = Shader::CreateObject();
	CHECK(result != nullptr)

	result->SetSFShader(newShader);
	result->ShaderName = fileName.GetName(false, false);
	return result;
}

Shader* Shader::CreateShader (const FileAttributes& vertexFileName, const FileAttributes& fragmentFileName)
{
	if (!sf::Shader::isAvailable())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to create shader since the system doesn't support shaders."));
		return nullptr;
	}

	sf::Shader* newShader = new sf::Shader();
	{
		SfmlOutputStream sfmlOutput;
		if (!newShader->loadFromFile(vertexFileName.GetName(true, true).ToCString(), fragmentFileName.GetName(true, true).ToCString()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Error in loading shader(s) %s-%s:  %s"), vertexFileName.GetName(false, true), fragmentFileName.GetName(false, true), sfmlOutput.ReadOutput());
			delete newShader;
			return nullptr;
		}
	}

	Shader* result = Shader::CreateObject();
	CHECK(result != nullptr)

	result->SetSFShader(newShader);
	return result;
}

void Shader::SetSFShader (sf::Shader* newSFShader)
{
	SFShader = newSFShader;
}
SD_END