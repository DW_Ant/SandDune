/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteComponent.cpp
=====================================================================
*/

#include "RenderTarget.h"
#include "RenderTexture.h"
#include "ScreenCapture.h"
#include "Shader.h"
#include "SpriteComponent.h"
#include "Texture.h"
#include "Transformation.h"

IMPLEMENT_CLASS(SD::SpriteComponent, SD::RenderComponent)
SD_BEGIN

void SpriteComponent::InitProps ()
{
	Super::InitProps();

	DrawMode = DM_Stretch;
	Sprite = nullptr;
	SpriteShader = nullptr;
	RenderState = sf::RenderStates::Default;

	BaseSize = Vector2(1.f, 1.f);
	Pivot = Vector2::ZERO_VECTOR;
	SubDivideScaleX = 1.f;
	SubDivideScaleY = 1.f;

	InvisibleTexture = true; //Sprite texture not yet assigned
	RenderTextureOwner = nullptr;
}

void SpriteComponent::BeginObject ()
{
	Super::BeginObject();

	Sprite = new sf::Sprite();
}

void SpriteComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const SpriteComponent* spriteTemplate = dynamic_cast<const SpriteComponent*>(objTemplate);
	if (spriteTemplate != nullptr)
	{
		SetDrawMode(spriteTemplate->DrawMode);

		if (spriteTemplate->Sprite->getTexture() != nullptr)
		{
			SetSpriteTexture(spriteTemplate->Sprite->getTexture());
		}
		else
		{
			ClearSpriteTexture();
		}

		SetSpriteShader(spriteTemplate->GetSpriteShader());

		BaseSize = spriteTemplate->BaseSize;
		Pivot = spriteTemplate->Pivot;

		if (Sprite != nullptr && spriteTemplate->Sprite != nullptr)
		{
			Sprite->setTextureRect(spriteTemplate->Sprite->getTextureRect());
		}
		SubDivideScaleX = spriteTemplate->SubDivideScaleX;
		SubDivideScaleY = spriteTemplate->SubDivideScaleY;

		InvisibleTexture = spriteTemplate->InvisibleTexture;
	}
}

void SpriteComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	if (Sprite == nullptr || InvisibleTexture)
	{
		return;
	}

	CHECK(GetOwnerTransform() != nullptr)
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	if (RenderTextureOwner != nullptr)
	{
		//If the renderTarget is capturing the screen, may need to check if it should update recursively draw its render texture into the screen capture.
		if (ScreenCapture* capture = renderTarget->GetRenderCapture())
		{
			if (capture->IsDetectingCompsWithinRenderTextures())
			{
				capture->AddRenderTexture(RenderTextureOwner.Get(), this, projectionData);
				return; //Don't bother drawing the sprite itself, since the screen capture will handle drawing the render texture (in order to avoid drawing irrelevant components within the texture).
			}
		}
	}

	Sprite->setPosition(projectionData.Position);
	Sprite->setRotation(projectionData.Rotation);

	Vector2 textureSize = GetTextureSize();
	Sprite->setOrigin(Vector2::SDtoSFML(Pivot * textureSize));

	//Apply sprite scale multipliers
	if (DrawMode == DM_Stretch)
	{
		sf::Vector2f spriteSize(SubDivideScaleX.Value, SubDivideScaleY.Value);
		spriteSize /= textureSize;
		spriteSize *= projectionData.GetFinalSize() * BaseSize;
		Sprite->setScale(spriteSize);
	}
	else if (DrawMode == DM_Tiled)
	{
		sf::IntRect textRect(Sprite->getTextureRect());
		Vector2 denom(textureSize.X * SubDivideScaleX, textureSize.Y * SubDivideScaleY);

		//numTiles = the number of times the texture must repeat in order to fill the space. The space is equal to BaseSize * projectionData.BaseSize.
		Vector2 numTiles = ((BaseSize * projectionData.BaseSize) / denom);
		textRect.width = (textureSize.X * numTiles.X).ToInt().Value;
		textRect.height = (textureSize.Y * numTiles.Y).ToInt().Value;
		Sprite->setTextureRect(textRect);

		//Camera zoom and distance should not affect the number of tiles.
		sf::Vector2f spriteSize(SubDivideScaleX.Value, SubDivideScaleY.Value);
		spriteSize *= projectionData.Scale;
		Sprite->setScale(spriteSize);
	}

	renderTarget->Draw(this, camera, *Sprite, RenderState);

	//Special case for RenderTextures.  This is used to notify the RenderTexture where it's being drawn in relation to the window.
	if (RenderTextureOwner != nullptr)
	{
		RenderTextureOwner->SetOuterRenderTarget(renderTarget);
		RenderTextureOwner->SetLatestDrawCoordinates(renderTarget->GetWindowCoordinates() + Vector2::SFMLtoSD(projectionData.Position));
	}
}

Aabb SpriteComponent::GetAabb () const
{
	return Aabb(BaseSize.Y, 0.f, BaseSize.X, Vector3::ZERO_VECTOR);
}

bool SpriteComponent::IsAlwaysScreenCapRelevant (ScreenCapture* screenCap) const
{
	if (RenderTextureOwner != nullptr && screenCap != nullptr && screenCap->IsDetectingCompsWithinRenderTextures())
	{
		//Sprites drawing render textures are always relevant so that components within the render texture are detected.
		return true;
	}

	return Super::IsAlwaysScreenCapRelevant(screenCap);
}

void SpriteComponent::Destroyed ()
{
	if (SpriteShader.IsValid())
	{
		SpriteShader->Destroy();
	}

	if (Sprite != nullptr)
	{
		delete Sprite;
		Sprite = nullptr;
	}

	Super::Destroyed();
}

bool SpriteComponent::SetSubDivision (Int numHorizontalSegments, Int numVerticalSegments, Int horizontalIndex, Int verticalIndex)
{
	if (Sprite->getTexture() == nullptr)
	{
		return false;
	}

	if (numHorizontalSegments <= 0 || numVerticalSegments <= 0 || horizontalIndex < 0 || verticalIndex < 0)
	{
		return false;
	}

	if (horizontalIndex >= numHorizontalSegments || verticalIndex >= numVerticalSegments)
	{
		return false;
	}

	sf::Vector2u totalSize = Sprite->getTexture()->getSize();

	sf::IntRect sfRect;
	sfRect.width = totalSize.x/numHorizontalSegments.ToUnsignedInt32();
	sfRect.height = totalSize.y/numVerticalSegments.ToUnsignedInt32();
	sfRect.left = horizontalIndex.ToUnsignedInt32() * sfRect.width;
	sfRect.top = verticalIndex.ToUnsignedInt32() * sfRect.height;

	Sprite->setTextureRect(sfRect);

	SubDivideScaleX = numHorizontalSegments.ToFloat();
	SubDivideScaleY = numVerticalSegments.ToFloat();

	return true;
}

void SpriteComponent::SetDrawCoordinatesMultipliers (Float xScaleMultiplier, Float yScaleMultiplier, Float xPosMultiplier, Float yPosMultiplier)
{
	if (Sprite == nullptr || Sprite->getTexture() == nullptr)
	{
		return;
	}

	sf::Vector2f totalSize = sf::Vector2f(Sprite->getTexture()->getSize());
	sf::FloatRect sfRect;
	sfRect.width = totalSize.x * xScaleMultiplier.Value;
	sfRect.height = totalSize.y * yScaleMultiplier.Value;
	sfRect.left = totalSize.x * xPosMultiplier.Value;
	sfRect.top = totalSize.y * yPosMultiplier.Value;

	Sprite->setTextureRect(sf::IntRect(sfRect));

	SubDivideScaleX = 1/xScaleMultiplier;
	SubDivideScaleY = 1/yScaleMultiplier;
}

void SpriteComponent::SetDrawCoordinates (Int u, Int v, Int ul, Int vl)
{
	sf::IntRect sfRect;
	sfRect.width = ul.ToInt32();
	sfRect.height = vl.ToInt32();
	sfRect.left = u.ToInt32();
	sfRect.top = v.ToInt32();

	Sprite->setTextureRect(sfRect);

	if (Sprite != nullptr && Sprite->getTexture() != nullptr)
	{
		sf::Vector2u totalSize = Sprite->getTexture()->getSize();
		SubDivideScaleX = Int(totalSize.x).ToFloat() / ul.ToFloat();
		SubDivideScaleY = Int(totalSize.y).ToFloat() / vl.ToFloat();
	}
}

void SpriteComponent::SetDrawMode (EDrawMode newDrawMode)
{
	DrawMode = newDrawMode;
}

void SpriteComponent::SetSpriteTexture (const sf::Texture* newTexture)
{
	CHECK(Sprite != nullptr)

	if (newTexture == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Texture resource is null when attempting to set it on a SpriteComponent.  If nullptr is intended, use ClearSpriteTexture instead."));
		return;
	}

	if (SpriteShader.IsValid() && SpriteShader->SFShader != nullptr)
	{
		SpriteShader->SFShader->setUniform(SHADER_BASE_TEXTURE, newTexture);
	}

	Sprite->setTexture(*newTexture);
	InvisibleTexture = false;
	RenderTextureOwner = nullptr;

	//Need to update draw coordinates since setting the texture initially would update the the sprite's draw rectangle, but subsequent texture updates will not.
	//For example, if a sprite initializes to use a 256x256 texture, the sprite's textureRect value is 256,256,0,0 (zero being positions).  Later if that same
	//	sprite changed to a 128x128 texture, the sprite's textureRect is still 256,256,0,0.
	Vector2 newTextureSize = GetTextureSize();
	sf::IntRect curRect = Sprite->getTextureRect();
	SetDrawCoordinates(curRect.left, curRect.top, (newTextureSize.X/SubDivideScaleX).ToInt(), (newTextureSize.Y/SubDivideScaleY).ToInt());

	OnTextureChangedCallbacks.Broadcast();
}

void SpriteComponent::SetSpriteTexture (const Texture* newTexture)
{
	const sf::Texture* newSfTexture = nullptr;
	if (newTexture != nullptr)
	{
		newSfTexture = newTexture->GetTextureResource();
	}

	SetSpriteTexture(newSfTexture);
}

void SpriteComponent::SetSpriteTexture (RenderTexture* renderTextureOwner)
{
	if (renderTextureOwner->GetResource() == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to assign sprite texture since the RenderTexture does not have a resource associated with it."));
		return;
	}

	SetSpriteTexture(&renderTextureOwner->GetResource()->getTexture());
	RenderTextureOwner = renderTextureOwner;
}

void SpriteComponent::ClearSpriteTexture ()
{
	InvisibleTexture = true;
}

void SpriteComponent::SetSpriteShader (Shader* newSpriteShader)
{
	CHECK(Sprite != nullptr)

	if (SpriteShader.IsValid())
	{
		SpriteShader->Destroy();
	}

	SpriteShader = newSpriteShader;
	if (SpriteShader.IsNullptr())
	{
		RenderState = sf::RenderStates::Default;
		return;
	}

	if (SpriteShader->SFShader == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Setting sprite shader (%s) to %s before binding a SF Shader instance upon the shader, itself, will cause the sprite component to render incorrectly."), SpriteShader->GetFriendlyName(), ToString());
		return;
	}

	SpriteShader->SFShader->setUniform(SHADER_BASE_TEXTURE, *Sprite->getTexture());
	RenderState = sf::RenderStates(SpriteShader->SFShader);
}

void SpriteComponent::SetPivot (const Vector2& newPivot)
{
	Pivot = newPivot;
}

sf::Sprite* SpriteComponent::GetSprite () const
{
	return Sprite;
}

Shader* SpriteComponent::GetSpriteShader () const
{
	return SpriteShader.Get();
}

void SpriteComponent::GetTextureSize (Int& outWidth, Int& outHeight) const
{
	CHECK(Sprite != nullptr)

	outWidth = 0;
	outHeight = 0;

	if (Sprite->getTexture() != nullptr)
	{
		sf::Vector2u textureSize = Sprite->getTexture()->getSize();
		outWidth = textureSize.x;
		outHeight = textureSize.y;
	}
}

Vector2 SpriteComponent::GetTextureSize () const
{
	Int width;
	Int height;
	GetTextureSize(OUT width, OUT height);

	return Vector2(width.ToFloat(), height.ToFloat());
}

void SpriteComponent::GetDrawCoordinates (Int& outU, Int& outV, Int& outUl, Int& outVl) const
{
	if (Sprite != nullptr)
	{
		const sf::IntRect& region = Sprite->getTextureRect();
		outU = region.left;
		outV = region.top;
		outUl = region.width;
		outVl = region.height;
	}
}
SD_END