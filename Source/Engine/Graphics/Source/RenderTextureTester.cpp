/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RenderTextureTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::RenderTextureTester, SD::Entity)
SD_BEGIN

void RenderTextureTester::InitProps ()
{
	Super::InitProps();

	SplitScreenWindow = nullptr;
	WindowDrawLayer = nullptr;
	FrameSize = Vector2(384.f, 256.f);
	RenderTextureOwner = nullptr;
	TopRenderTexture = nullptr;
	BottomRenderTexture = nullptr;
	TopCamera = nullptr;
	BottomCamera = nullptr;
}

void RenderTextureTester::BeginObject ()
{
	Super::BeginObject();

	const Int width = FrameSize.X.ToInt();
	const Int height = FrameSize.Y.ToInt();
	const Float frameRate = 0.033333f; //~30 frames per second

	SplitScreenWindow = Window::CreateObject();
	SplitScreenWindow->SetResource(new sf::RenderWindow(sf::VideoMode(width.ToUnsignedInt32(), height.ToUnsignedInt32() * 2), "Render Texture Test"));
	SplitScreenWindow->GetTick()->SetTickInterval(frameRate);
	SplitScreenWindow->GetPollingTickComponent()->SetTickInterval(frameRate);
	SplitScreenWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, RenderTextureTester, HandleWindowEvent, void, const sf::Event&));

	//Setup draw layer for split screen window
	WindowDrawLayer = PlanarDrawLayer::CreateObject();
	SplitScreenWindow->RegisterDrawLayer(WindowDrawLayer.Get());

	//Setup camera
	TopCamera = PlanarCamera::CreateObject();
	TopCamera->SetViewExtents(FrameSize);
	BottomCamera = PlanarCamera::CreateObject();
	BottomCamera->SetViewExtents(FrameSize);
	BottomCamera->SetZoom(0.5f);

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	CHECK(mainWindow != nullptr)

	//Setup RenderTextures
	TopRenderTexture = RenderTexture::CreateObject();
	TopRenderTexture->CreateResource(width, height, false);
	TopRenderTexture->GetTick()->SetTickInterval(frameRate);
	TopRenderTexture->SetDrawLayersFrom(mainWindow);
	TopRenderTexture->SetDestroyDrawLayersOnDestruction(false); //Don't destroy draw layers since the mainWindow still uses them
	TopRenderTexture->AssignCamera(PlanarDrawLayer::CANVAS_LAYER_PRIORITY, TopCamera.Get());

	BottomRenderTexture = RenderTexture::CreateObject();
	BottomRenderTexture->CreateResource(width, height, false);
	BottomRenderTexture->GetTick()->SetTickInterval(frameRate);
	BottomRenderTexture->SetDrawLayersFrom(mainWindow);
	BottomRenderTexture->SetDestroyDrawLayersOnDestruction(false); //Don't destroy draw layers since the mainWindow still uses them
	BottomRenderTexture->AssignCamera(PlanarDrawLayer::CANVAS_LAYER_PRIORITY, BottomCamera.Get());

	//Setup Sprite transforms
	RenderTextureOwner = Entity::CreateObject();
	PlanarTransformComponent* topTransform = PlanarTransformComponent::CreateObject();
	if (RenderTextureOwner->AddComponent(topTransform))
	{
		topTransform->SetSize(Vector2(1.f, 0.5f));
		topTransform->SetPosition(Vector2::ZERO_VECTOR);
	}

	PlanarTransformComponent* bottomTransform = PlanarTransformComponent::CreateObject();
	if (RenderTextureOwner->AddComponent(bottomTransform))
	{
		bottomTransform->SetSize(Vector2(1.f, 0.5f));
		bottomTransform->SetPosition(Vector2(0.f, 0.5f));
	}

	CHECK(topTransform != nullptr && bottomTransform != nullptr)

	//Setup Sprite components that'll be rendering a RenderTexture on the external window.
	SpriteComponent* topSprite = SpriteComponent::CreateObject();
	if (topTransform->AddComponent(topSprite))
	{
		WindowDrawLayer->RegisterPlanarObject(topTransform);
		topSprite->SetSpriteTexture(&TopRenderTexture->GetResource()->getTexture());
	}

	SpriteComponent* bottomSprite = SpriteComponent::CreateObject();
	if (bottomTransform->AddComponent(bottomSprite))
	{
		WindowDrawLayer->RegisterPlanarObject(bottomTransform);
		bottomSprite->SetSpriteTexture(&BottomRenderTexture->GetResource()->getTexture());
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	Texture* whiteFrame = localTexturePool->EditTexture(HashedString("Engine.Graphics.WhiteFrame"));
	CHECK(whiteFrame != nullptr)

	//Setup frames rendered in main window
	//Top frame
	PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
	CHECK(TopCamera.IsValid() && canvasLayer != nullptr)
	{
		TopCamera->SetPosition(FrameSize * 0.5f); //Place the view in top left corner by placing the camera at center of frame
		TopCamera->SetSize(FrameSize);

		PlanarTransformComponent* cameraSpriteTransform = PlanarTransformComponent::CreateObject();
		if (TopCamera->AddComponent(cameraSpriteTransform))
		{
			//The border sprite represents the view boundaries the camera can see from.
			cameraSpriteTransform->SetPosition(FrameSize * -0.5f);

			SpriteComponent* borderSprite = SpriteComponent::CreateObject();
			if (cameraSpriteTransform->AddComponent(borderSprite))
			{
				borderSprite->SetSpriteTexture(whiteFrame);
				canvasLayer->RegisterPlanarObject(cameraSpriteTransform);

				Shader* colorShift = Shader::CreateShader(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("ColorShift.frag")), sf::Shader::Type::Fragment);
				if (colorShift != nullptr)
				{
					colorShift->SFShader->setUniform("ColorMultiplier", sf::Glsl::Vec4(1.f, 0.f, 0.f, 1.f));
					borderSprite->SetSpriteShader(colorShift);
				}
			}
		}
	}

	//Bottom frame
	CHECK(BottomCamera.IsValid())
	{
		BottomCamera->SetPosition(FrameSize * Vector2(1.5f, 1.5f));
		BottomCamera->SetSize(FrameSize);

		PlanarTransformComponent* cameraSpriteTransform = PlanarTransformComponent::CreateObject();
		if (BottomCamera->AddComponent(cameraSpriteTransform))
		{
			cameraSpriteTransform->SetPosition(FrameSize * -0.5f);

			SpriteComponent* borderSprite = SpriteComponent::CreateObject();
			if (cameraSpriteTransform->AddComponent(borderSprite))
			{
				borderSprite->SetSpriteTexture(whiteFrame);
				canvasLayer->RegisterPlanarObject(cameraSpriteTransform);

				Shader* colorShift = Shader::CreateShader(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), TXT("ColorShift.frag")), sf::Shader::Type::Fragment);
				if (colorShift != nullptr)
				{
					colorShift->SFShader->setUniform("ColorMultiplier", sf::Glsl::Vec4(0.f, 0.f, 1.f, 1.f));
					borderSprite->SetSpriteShader(colorShift);
				}
			}
		}
	}
}

void RenderTextureTester::Destroyed ()
{
	if (SplitScreenWindow != nullptr)
	{
		//Don't invoke RenderTextureTester::Destroy when the window is destroyed
		SplitScreenWindow->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, RenderTextureTester, HandleWindowEvent, void, const sf::Event&));
		SplitScreenWindow->Destroy();
	}

	//No need to destroy DrawLayer since the Window is configured to destroy DrawLayers on Destruction.

	if (TopRenderTexture.IsValid())
	{
		TopRenderTexture->Destroy();
	}

	if (BottomRenderTexture.IsValid())
	{
		BottomRenderTexture->Destroy();
	}

	if (RenderTextureOwner != nullptr)
	{
		RenderTextureOwner->Destroy();
	}

	//No need to destroy cameras since the RenderTextures destroyed them.

	//No need to destroy frames since they will be automatically destroyed since they are sub components of this Entity.

	Super::Destroyed();
}

void RenderTextureTester::HandleWindowEvent (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::Closed)
	{
		//The window is already marked for destruction.  Clear nullptr before RenderTextureTester::Destroyed calls destroy on a destroyed object.
		SplitScreenWindow = nullptr;
		Destroy();
	}
}

SD_END
#endif