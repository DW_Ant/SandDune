/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ColorTester.cpp
=====================================================================
*/

#include "ColorRenderComponent.h"
#include "ColorTester.h"
#include "GraphicsEngineComponent.h"
#include "PlanarDrawLayer.h"
#include "PlanarTransformComponent.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::ColorTester, SD::Entity)
SD_BEGIN

void ColorTester::InitProps ()
{
	Super::InitProps();

	ColorComponent = nullptr;
}

void ColorTester::BeginObject ()
{
	Super::BeginObject();

	SetSize(Vector2(256.f, 256.f));
	SetDepth(10.f);
}

bool ColorTester::BeginTest (UnitTester::EUnitTestFlags testFlags)
{
	PlanarTransformComponent* colorTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(colorTransform))
	{
		colorTransform->SetPosition(Vector2(0.5f, 0.5f));
		colorTransform->SetSize(Vector2(0.25f, 0.25f));
	}
	else
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed. Unable to attach a PlanarTransformComponent to the ColorTester."));
		Destroy();
		return false;
	}

	ColorComponent = ColorRenderComponent::CreateObject();
	if (colorTransform->AddComponent(ColorComponent))
	{
		ColorComponent->SolidColor = sf::Color::Cyan;
	}
	else
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  Unable to attach ColorRenderComponent to the a PlanarTransformComponent."));
		Destroy();
		return false;
	}

	LifeSpanComponent* lifeSpan = LifeSpanComponent::CreateObject();
	if (!AddComponent(lifeSpan))
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  Unable to attach LifeSpanComponent to the ColorTester."));
		Destroy();
		return false;
	}

	PlanarDrawLayer* canvasLayer = GraphicsEngineComponent::Find()->GetCanvasDrawLayer();
	if (canvasLayer == nullptr)
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  There isn't a CanvasDrawLayer to register the component for rendering."));
		Destroy();
		return false;

	}

	canvasLayer->RegisterPlanarObject(colorTransform);
	lifeSpan->LifeSpan = 3.f;
	UnitTester::TestLog(testFlags, TXT("Solid Color test launched.  A cyan component created, and it will automatically perish in 3 seconds."));
	return true;
}
SD_END

#endif