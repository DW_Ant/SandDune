/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsClasses.h
  Contains all header includes for the Physics module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the PhysicsClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_PHYSICS
#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionShape.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry2dUtils.h"
#include "Geometry3dUtils.h"
#include "Physics.h"
#include "PhysicsComponent.h"
#include "PhysicsEngineComponent.h"
#include "PhysicsUnitTest.h"
#include "PhysicsUtils.h"
#include "QuadTree.h"
#include "QuadTreeInterface.h"
#include "QuadTreeVisualizer.h"

#endif
