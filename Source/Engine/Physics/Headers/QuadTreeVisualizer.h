/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QuadTreeVisualizer.h

  An Object that helps debug and visualize a Quad Tree.
  Typically this implementation would be handled on the QuadTree, itself.
  The main reason why it's separated into its own class is because a QuadTree is not an Entity.
=====================================================================
*/

#pragma once

#include "Physics.h"

#ifdef DEBUG_MODE
SD_BEGIN
class QuadTree;

class PHYSICS_API QuadTreeVisualizer : public Entity, public SceneTransform
{
	DECLARE_CLASS(QuadTreeVisualizer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	QuadTree* RootTree;

	/* The DrawLayer this Entity is drawing to. */
	DPointer<SceneDrawLayer> DrawLayer;

	/* The color to use when rendering the borders for each QuadTree region. */
	Color BorderColor;

	/* Determines the line thickness for each border. */
	Float BorderThickness;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Hooks up this visualizer to the given QuadTree and DrawLayer (to render to).
	 */
	virtual void SetupVisualizer (QuadTree* inRootTree, SceneDrawLayer* inDrawLayer, Color inBorderColor, Float inBorderThickness);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SceneDrawLayer* GetDrawLayer () const
	{
		return DrawLayer.Get();
	}

	inline Color GetBorderColor () const
	{
		return BorderColor;
	}

	inline Float GetBorderThickness () const
	{
		return BorderThickness;
	}
};
SD_END
#endif