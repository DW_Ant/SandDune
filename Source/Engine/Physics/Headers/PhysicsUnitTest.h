/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsUnitTest.h

  A unit test that tests the Physics module.
=====================================================================
*/

#pragma once

#include "Physics.h"
#include "QuadTreeInterface.h"

#ifdef DEBUG_MODE

SD_BEGIN
//Simple dummy class used for testing the Quad Tree
class QuadTreeTester : public QuadTreeInterface
{
public:
	Aabb BoundingBox;
	QuadTreeTester (Float width, Float height, Float depth, const Vector3& center);
	QuadTreeTester (Aabb inBoundingBox);
	virtual const Aabb& ReadBoundingBox () const override;
};

class PHYSICS_API PhysicsUnitTest : public UnitTester
{
	DECLARE_CLASS(PhysicsUnitTest)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool RunQuadTreeTest (EUnitTestFlags testFlags) const;
	virtual bool RunGeometry2dUtils (EUnitTestFlags testFlags) const;
	virtual bool RunGeometry3dUtils (EUnitTestFlags testFlags) const;
	virtual bool RunSegmentTests (EUnitTestFlags testFlags) const;
	virtual bool RunSphereTests (EUnitTestFlags testFlags) const;
	virtual bool RunCapsuleTests (EUnitTestFlags testFlags) const;
	virtual bool RunExtrudePolygonTests (EUnitTestFlags testFlags) const;
	virtual bool RunCollisionUtilsTests (EUnitTestFlags testFlags) const;
	virtual bool RunPhysicsUtilsTests (EUnitTestFlags testFlags) const;
};
SD_END
#endif