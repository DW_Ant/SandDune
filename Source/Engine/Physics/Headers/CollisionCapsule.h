/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionCapsule.h

  A 3D geometric collision shape that is defined by its center point, radius, and height.

  A capsule has two spheres above and below the center point. The distance between the centers of these
  spheres are defined by the height.

  The height is always parallel to the Z-axis.

  The center is always at the shape's Position point.
=====================================================================
*/

#pragma once

#include "CollisionShape.h"

SD_BEGIN
class CollisionSegment;

class PHYSICS_API CollisionCapsule : public CollisionShape
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The vertical distance between spheres' centers. */
	Float Height;

	/* The distance between the spheres' centers and their end points. This is also the distance from the
	cylinder (that's between the two spheres) center to the edge. */
	Float Radius;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionCapsule ();
	CollisionCapsule (Float inScale, const Vector3& inPosition);
	CollisionCapsule (Float inScale, const Vector3& inPosition, Float inHeight, Float inRadius);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RefreshBoundingBox () override;
	virtual bool OverlapsWith (const CollisionShape* otherShape) const override;
	virtual bool EncompassesPoint (const Vector3& point) const override;
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the distance between the Position.Z value and the top & bottom of the cylinder's edge.
	 * (Height/2 * Scale)
	 */
	virtual Float GetCylinderZEdge () const;

	/**
	 * Returns the distance between the Position.Z value and the upper tip of the top sphere.
	 * (Height/2 + Radius) * Scale.
	 */
	virtual Float GetCapsuleZLimit () const;

	/**
	 * Returns a CollisionSegment that represents this capsule.
	 * The segment essentially has its end points at this capsule's spheres' centers.
	 */
	virtual CollisionSegment ToSegment () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetHeight (Float inHeight);
	virtual void SetRadius (Float inRadius);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetHeight () const
	{
		return Height;
	}

	inline Float GetRadius () const
	{
		return Radius;
	}
};
SD_END