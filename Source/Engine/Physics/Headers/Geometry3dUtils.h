/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Geometry3dUtils.h

  Defines a bunch of useful geometric functions in 3D space.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class PHYSICS_API Geometry3dUtils : public BaseUtils
{
	DECLARE_CLASS(Geometry3dUtils)


	/*
	=====================
	  Struct
	=====================
	*/

public:
	/* A data struct representing a 3D infinite plane defined by a point and a normal. */
	struct SPlane
	{
	public:
		/* A point that resides on this infinite plane. */
		Vector3 Position;

	private:
		/* Directional vector that is perpendicular to the plane. This vector is assumed to be normalized. */
		Vector3 Normal;

	public:
		SPlane ();
		SPlane (const Vector3& inPosition, const Vector3& inNormal);
		void SetNormal (const Vector3& newNormal); //Normal will automatically be normalized

		inline const Vector3& ReadNormal () const
		{
			return Normal;
		}

		inline Vector3 GetNormal () const
		{
			return Normal;
		}
	};


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if both directional vectors are parallel.
	 */
	static inline bool IsParallel (const Vector3& dirA, const Vector3& dirB, Float tolerance = FLOAT_TOLERANCE)
	{
		return (dirA.CrossProduct(dirB).IsNearlyEqual(Vector3::ZERO_VECTOR, tolerance));
	}

	/**
	 * Returns true if all three points are collinear.
	 */
	static bool AreCollinear (const Vector3& a, const Vector3& b, const Vector3& c, Float tolerance = FLOAT_TOLERANCE);

	/**
	 * Returns true if the test point resides on the segment defined by two end points. The end points are in absolute coordinates.
	 */
	static bool IsOnSegment (const Vector3& endPtA, const Vector3& endPtB, const Vector3& testPt, Float tolerance = FLOAT_TOLERANCE);
	
	/**
	 * Assuming that the three points are already collinear, this returns true if the middle point is between the two end points.
	 * This is essentially the same as IsOnSegment but the collinear assumption allows this function to perform this check faster.
	 */
	static bool IsBetweenCollinearPts (const Vector3& endPtA, const Vector3& endPtB, const Vector3& testPt);

	/**
	 * Calculates the distance from the given point to the line defined by its point and direction from that point.
	 * If this function returns nearly zero, then it's considered that the point resides on the line.
	 * @param bBoundedLine If true, then the line is considered to be finite where its length is defined by the magnitude of the directional vector.
	 * Otherwise it'll assume this line is infinite. For bounded lines it'll return the distance between the testPoint and the line if its within the range. If it's out of range, it'll return the distance between the testPoint and one of the end points of the line.
	 * @param outIntersectingPoint The point on the line that marks the intersecting point between the given line and the perpendicular segment that marks the distance between the line and the testPoint.
	 */
	static Float CalcDistBetweenPointAndLine (const Vector3& linePt, const Vector3& lineDir, const Vector3& testPoint, bool bBoundedLine, Vector3& outIntersectingPoint);
	static Float CalcSquaredDistBetweenPointAndLine (const Vector3& linePt, const Vector3& lineDir, const Vector3& testPoint, bool bBoundedLine, Vector3& outIntersectingPoint);

	/**
	 * Returns true if the two given lines intersect at least once.
	 * A line is defined by a point and a directional vector from that point (vector form).
	 * @param bBoundedLines If true, then the two lines are bounded by end points where the end points are defined by a point and the length of the direction from that point.
	 * If bounded, lines will not be considered intersecting if the intersection point is beyond their end points. Otherwise, it's assumed that the lines are infinite length.
	 * If it does intersect, it'll assign the outIntersectingPoint param to a point that resides in both lines. This param is assigned even if the lines are not intersecting due to bound limits.
	 */
	static bool IsIntersecting (const Vector3& ptA, const Vector3& dirA, const Vector3& ptB, const Vector3& dirB, bool bBoundedLines, Vector3& outIntersectingPoint, Float tolerance = FLOAT_TOLERANCE);

	/**
	 * Calculates and returns the shortest distance between the two given lines defined by a point and a direction (vector form).
	 * @param bBoundedLines If true, then the two lines are bounded by their end points where the end points are defined by a point and the length of the direction vector.
	 * @param outDistEndPtA The end point of the shortest segment connecting the two lines. This end point resides on the first line in absolute coordinates.
	 * @param outDistEndPtB The end point of the shortest segment connecting the two lines. This end point resides on the second line in absolute coordinates.
	 */
	static Float CalcDistBetweenLines (const Vector3& linePtA, const Vector3& lineDirA, const Vector3& linePtB, const Vector3& lineDirB, bool bBoundedLines, Vector3& outDistEndPtA, Vector3& outDistEndPtB);

	/**
	 * Calculates the point when the line intersects the infinite plane. Returns true if there's at least one intersection.
	 * @param bBoundedLine If true, then the line's end points are determined by its start point and the length of its directional vector.
	 * @param outIntersection If there is at least one intersection, this parameter will indicate where the intersection resides. For infinite intersection cases, this param is equal to
	 * the start position of the vector. This parameter is assigned even if the segment doesn't intersect the plane because of its end points not reaching it.
	 */
	static bool CalcLinePlaneIntersection (const SPlane& plane, const Vector3& linePt, const Vector3& lineDir, bool bBoundedLine, Vector3& outIntersection);

	/**
	 * Calculates and returns the given directional vector projected onto the specified plane.
	 * The plane's displacement is not considered when making these calculations since the resulting directional vector is always relative to the plane.
	 */
	static Vector3 ProjectLineOnPlane (const SPlane& plane, const Vector3& lineDir);

	/**
	 * Calculates the angle between the two directional vectors in radians.
	 * For perpendicular lines, this returns pi halves. Parallel lines return 0.
	 * Directional vectors are not required to be normalized.
	 */
	static Float CalcAngleBetweenLines (const Vector3& dirA, const Vector3& dirB);

	/**
	 * Calculates the intersection points where the line meets the sphere.
	 * If bBoundedLine is true, then the length of the segment is defined by the magnitude of the directional vector.
	 * Returns true if there is at least one intersection.
	 */
	static bool CalcSphereLineIntersections (const Vector3& sphereCenter, Float radius, const Vector3& linePt, const Vector3& lineDir, bool bBoundedLine, std::vector<Vector3>& outIntersections);
};
SD_END