/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsComponent.h

  Defines the owning Entities' movement and collision properties.

  If it has momentum, this component is responsible for changing its owner's local translation
  every frame. This component interacts with other PhysicsComponents, and reacts based on their
  collision properties.

  This component specifies the collision shapes, collision channels, and response types on collisions.

  Regarding collision channels:
  Sand Dune doesn't preset collision channels. It's up to the game modules or the projects, themselves, to
  define their own set of collision channels. It's recommended to use preprocessor macros, static const ints, or
  enums to map an int to a symbol. The collision channels should also be in powers of two so entities may use
  use a blend of channels.

  For example:
  The player entity may have the following collision channels.
  COLLISION_CHANNEL_WORLD_ENTITY = 2
  COLLISION_CHANNEL_CHARACTER = 4
  COLLISION_CHANNEL_BLUE_TEAM = 8

  Using bit wise flags, this would allow collision queries to test by specific channel(s). For example if there's a projectile
  that ignore friendlies, certain queries may set collision channels to: WORLD_ENTITY | CHARACTER | RED_TEAM | ENVIRONMENTAL or
  ~COLLISION_CHANNEL_BLUE_TEAM (essentially collides against all except blue team channel).

  Regarding the Physics Cycle:
  There are three stages in the Physics Cycle.
  The first stage will update the Velocity and the shape positions. Knowing that the TransformOwner's absolute transform can be
  dirty, only the shapes are used for collision detection.

  The second stage iterates through all updated shapes and updates the overlapping shapes. It'll mark which components are pending
  a callback for the last stage.

  The last stage will invoke the Overlapping callbacks. This is after all the shapes simulated physics, and the handlers are safe to update
  the TransformOwner's and PhysicsComponent's properties.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class CollisionShape;
class QuadTree;

class PHYSICS_API PhysicsComponent : public EntityComponent
{
	DECLARE_CLASS(PhysicsComponent)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/* Simple struct that maps a collision shape to a quad tree node it resides in. */
	struct SCollisionShape
	{
		/* Pointer of the shape that acts as this component's collision detection. The reason for a pointer is for polymorphism purposes.
		Each shape Position is in world space and is not related to the TransformOwner's translation. */
		CollisionShape* Shape = nullptr;

		/* The delta vector between The Shape Position and the transform owner's local translation. If (0,0,0), then the TransformOwner's
		local Translation will snap to the Shape's Position at the end of the Physics cycle. Otherwise, this will displace the TransformOwner's
		translation relative to the Shape's position. */
		Vector3 ShapeDisplacement = Vector3::ZERO_VECTOR;

		SCollisionShape ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Broadcasted whenever this component has started/stopped overlapping against a PhysicsComponent with at least one collision channel
	that matches one of this component's OverlappingChannels. */
	SDFunction<void, PhysicsComponent* /*delegateOwner*/, PhysicsComponent* /*otherComp*/> OnBeginOverlap;
	SDFunction<void, PhysicsComponent* /*delegateOwner*/, PhysicsComponent* /*otherComp*/> OnEndOverlap;

	/* Broadcasted whenever this component was blocked by another component. */
	SDFunction<void, PhysicsComponent* /*delegateOwner*/, PhysicsComponent* /*otherComp*/> OnBlocked;

	/* Determines how many units per second this component travels in world space. */
	Vector3 Velocity;

	/* Determines how much the velocity should change every second. */
	Vector3 Acceleration;

	/* A composition of all collision channels that make up this component. All channels should be in powers of two since it consolidates
	all channels using the bitwise AND operation. */
	Int CollisionChannels;

	/* A composition of all collision channels that will invoke OnBeginOverlap and OnEndOverlap if the other PhysicsComponent has any of
	these channels (via CollisionChannels). The channels should be in powers of two since it uses a bitwise OR operation. */
	Int OverlappingChannels;

	/* A composition of all collision channels this PhysicsComponent will block if the other PhysicsComponent also
	blocks any of these channels.
	If the other physics component does not have ANY of these channels, then these components will not block each other. */
	Int BlockingChannels;

	/* Determines how much Velocity is conserved when colliding against physical objects.
	1 does not reduce the velocity magnitude. 0.5 cuts velocity in half. 0 has no bounce. */
	Float CollisionDampening;

	/* List of all PhysicsComponents this component is currently overlapping. */
	std::vector<DPointer<PhysicsComponent>> OverlappingComponents;

	/*  List of shapes that comprises this physics component. If any of these shapes blocks or overlap other physics component's shapes,
	then the collision responses ensue. Two shapes with the same PhysisComponent owner cannot collide against each other.
	All shapes' Position are relative to this component's transform in absolute space. */
	std::vector<SCollisionShape> CollisionShapes;

	/* The QuadTree instance its collision shapes are registered to. Defaults to the PhysicsEngineComponent's root quad tree. */
	QuadTree* RootQuadTree;

	/* The SceneTransformation this component is responsible for changing every time it moves. */
	SceneTransform* TransformOwner;

	/* The Component responsible for updating this component every frame for physics simulation. */
	TickComponent* PrePhysicsTick;
	TickComponent* PhysicsTick;
	TickComponent* PostPhysicsTick;

private:
	/* The delta Vector that converts the Owner Transform's local translation to world position.
	Updated every time the transform updated its absolute attributes so that minor local translations does not
	displace the transform local space to Shape's world space in the middle of the Tick cycle. */
	Vector3 TransformLocalToWorld;

	/* Becomes false if the owning transform has not yet initialized or computed its absolute transform yet.
	When false, this would prevent the shapes from registering to the QuadTree, and this component will not simulate
	any physics until this is true. In addition to that, callbacks are not invoked to prevent false positives during init.*/
	bool bTransformIsReady;


	/* List of callbacks that are waiting to invoke Begin and End overlap at the end of the physics cycle.
	This is primarily used at the end of the cycle to ensure the callback handlers doesn't change the physics state
	(such as transform changes or object destruction) in the middle of an update. */
	std::vector<PhysicsComponent*> PendingBeginOverlap;
	std::vector<PhysicsComponent*> PendingEndOverlap;
	std::vector<PhysicsComponent*> PendingBlockCallbacks;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps ();
	virtual void BeginObject ();
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void Destroyed ();
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the collision shape to this component if it's not registered already.
	 * This component will assume ownership where if this component is destroyed, it'll also destroy
	 * its associated collision shapes.
	 */
	virtual void AddShape (CollisionShape* newShape);

	/**
	 * Attempts to remove the specified shape from this component's list of shapes.
	 * The component will no longer assume ownership of the collision shape, and it will not delete the removed
	 * shape when this component is destroyed.
	 * Returns true if the shape was found and removed from QuadTree.
	 */
	virtual bool RemoveShape (CollisionShape* target);

	/**
	 * Returns true if this PhysicsComponent can detect overlap events with the given collision channel(s).
	 */
	virtual bool CanOverlapWith (Int collisionChannels) const;
	virtual bool CanOverlapWith (PhysicsComponent* otherComp) const;

	/**
	 * Returns true if this PhysicsComponent can block against the specified collision channel.
	 */
	virtual bool CanBlockAgainst (Int collisionChannels) const;

	/**
	 * Returns true if this PhysicsComponent can block against the other PhysicsComponent.
	 * NOTE: Even if the PhysicComponent matches this component's blocking channels, this component cannot block
	 * the other PhysicsComponent if the other component doesn't block this component.
	 * Both components must block each other.
	 */
	virtual bool CanBlockAgainst (PhysicsComponent* otherComp) const;

	/**
	 * Returns true if this component is able to simulate physics.
	 */
	virtual bool IsSimulatingPhysics () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetBeginOverlap (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newBeginOverlap);
	virtual void SetEndOverlap (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newEndOverlap);
	virtual void SetBlocked (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newBlocked);
	virtual void SetVelocity (const Vector3& newVelocity);
	virtual void SetAcceleration (const Vector3& newAcceleration);
	virtual void SetCollisionChannels (Int newCollisionChannels);
	virtual void SetOverlappingChannels (Int newOverlappingChannels);
	virtual void SetBlockingChannels (Int newBlockingChannels);
	virtual void SetCollisionDampening (Float newCollisionDampening);

	/**
	 * Sets the RootQuadTree to the new pointer.
	 * If the shapes are already registered to another quad tree, they are removed from that tree before inserted to this new tree.
	 */
	virtual void SetRootQuadTree (QuadTree* newRootQuadTree);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector3 GetVelocity () const
	{
		return Velocity;
	}

	inline const Vector3& ReadVelocity () const
	{
		return Velocity;
	}

	inline Vector3 GetAcceleration () const
	{
		return Acceleration;
	}

	inline const Vector3& ReadAcceleration () const
	{
		return Acceleration;
	}

	inline Int GetCollisionChannels () const
	{
		return CollisionChannels;
	}

	inline Int GetOverlappingChannels () const
	{
		return OverlappingChannels;
	}

	inline Int GetBlockingChannels () const
	{
		return BlockingChannels;
	}

	inline const std::vector<DPointer<PhysicsComponent>>& ReadOverlappingComponents () const
	{
		return OverlappingComponents;
	}

	void GetShapes (std::vector<CollisionShape*>& outShapes) const;
	inline const std::vector<SCollisionShape>& ReadCollisionShapes () const
	{
		return CollisionShapes;
	}

	inline QuadTree* GetRootQuadTree () const
	{
		return RootQuadTree;
	}

	inline SceneTransform* GetTransformOwner () const
	{
		return TransformOwner;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the callback ticking component's enabledness based on this component's condition.
	 */
	virtual void UpdateCallbackTicker ();

	/**
	 * The first stage in the physics cycle. This function is responsible for finding a new position where it doesn't
	 * overlap with blocking components.
	 * It'll run at most two passes.
	 * The first pass will attempt to move each shape in the new position, and if none of the shapes overlap with a blocking component
	 * Then this pass is done. Otherwise, it'll move the shape in the way it glides along the blocking component (suggesting a new position).
	 * If it glides along another component, then it'll run the second pass. The second pass will test if any shapes overlap with a blocking
	 * component. If the second pass fails, then this component will move back to the original position.
	 *
	 * Note: If this component started off the physics cycle overlapping a blocking component, then that blocking component is
	 * ignored for the update loop to prevent those two components from getting stuck.
	 */
	virtual void SimulateVelocity (Float deltaSec);

	/**
	 * The second stage in the physics cycle. This function is responsible for updating the overlapping shapes, and
	 * register pending callbacks based on changes on overlapping callbacks.
	 */
	virtual void UpdateOverlappingShapes ();

	/**
	 * The last stage in the physics cycle where all pending callbacks are invoked.
	 * Here it's safe for the handlers to update transforms, add, and remove objects since the physics objects are no longer referenced for this frame.
	 */
	virtual void InvokePendingCallbacks ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTransformChanged ();
	virtual void HandlePrePhysicsTick (Float deltaSec);
	virtual void HandlePhysicsTick (Float deltaSec);
	virtual void HandlePostPhysicsTick (Float deltaSec);
};
SD_END