/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QuadTree.h
  A recursive datatype that allows for efficient space partitioning.

  This assumes to use SD's coordinate system on the XY plane. The Z-axis is only considered
  when querying for overlapping shapes. Otherwise only the XY plane is considered when dividing
  rectangles in the quad tree.
=====================================================================
*/

#pragma once

#include "Physics.h"
#include "QuadTreeInterface.h"

SD_BEGIN
#ifdef DEBUG_MODE
class PhyicsUnitTest;
class QuadTreeVisualizer;
#endif

class PHYSICS_API QuadTree
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The minimum number of objects that resides in a single quad before it considers subdivision. */
	static const size_t MIN_OBJS;

	/* The XY coordinate where this quad's center resides. */
	Vector2 Center;

	/* Specified the distance from edge-to-edge. Only squared quad trees are supported. */
	Float Length;

	/* The recursive quads that are relative to the center. These may be null if the region is not sub divided.
	Order for these nodes matter for quick indexing. See FindSubRegion in how these are accessed.
	[0] = PositiveX, PositiveY
	[1] = PositiveX, NegativeY
	[2] = NegativeX, PositiveY
	[3] = NegativeX, NegativeY*/
	QuadTree* SubNodes[4];

	/* The QuadTree node that owns this node. If null, then this assumes to be the root. */
	QuadTree* OwningNode;

	/* List of objects residing in this node. Objects are registered in this node only if this is a node, or the object's Aabb does not fit within one of the
	sub nodes (or its edges overlap at least two quads). */
	std::vector<QuadTreeInterface*> Objects;

#ifdef DEBUG_MODE
	/* If not nullptr then the visualizer associated with this QuadTree network. */
	QuadTreeVisualizer* VisualTree;

	/* The TransformComponent that is the parent component of the two render components for the horizontal and vertical lines. */
	SceneTransformComponent* VisualLineTransform;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	QuadTree (const Vector2& inCenter, Float inLength, QuadTree* inOwningNode);
	virtual ~QuadTree ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the given object either in this QuadTree or in one of the sub quad trees depending on the object's Aabb.
	 * Returns the node instance the object was inserted to (could be either this instance or one of its sub nodes).
	 */
	virtual QuadTree* InsertObject (QuadTreeInterface* newObj);

	/**
	 * Searches and removes the specified object from this QuadTree.
	 * Returns true if the object was found and removed.
	 */
	virtual bool RemoveObject (QuadTreeInterface* target);

	/**
	 * This function assumes that the specified target has changed its position. With this assumption, the function
	 * will reposition the target to a new node within the tree if there's a more suitable node for the target.
	 *
	 * This function assumes the target resides in this exact node and will not search in owning or sub nodes. Returns false if not found.
	 * The outNewNode is the current quad tree instance the object resides in after the move.
	 */
	virtual bool MoveObject (QuadTreeInterface* target, QuadTree*& outNewNode);

	/**
	 * Generates a list of all objects that overlap with the given Aabb.
	 */
	virtual void FindOverlappingObjects (const Aabb& searchRegion, std::vector<QuadTreeInterface*>& outOverlappingObjects) const;

	/**
	 * Generates a list of all objects that encompasses the given point.
	 */
	virtual void FindEncompassingObjects (const Vector3& point, std::vector<QuadTreeInterface*>& outEncompassingObjects) const;

	/**
	 * Returns true if there are no QuadTrees dividing this node.
	 */
	virtual bool IsLeaf () const;

	/**
	 * Returns true if this is the outer most node, and nothing owns this node.
	 */
	virtual bool IsRoot () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector2 GetCenter () const
	{
		return Center;
	}

	inline Float GetLength () const
	{
		return Length;
	}

	inline const std::vector<QuadTreeInterface*>& ReadObjects () const
	{
		return Objects;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the given squared region (defined by center and length) completely encompasses the given Aabb. This ignores the object's Aabb height.
	 */
	static bool EncompassesAabb (const Vector2& regionCenter, Float regionLength, const Aabb& boundingBox);

	/**
	 * Returns true if this node overlaps with the given bounding box.
	 * This function ignores Z-axis.
	 */
	virtual bool OverlapsWith (const Aabb& target) const;

	/**
	 * Returns the index of the SubNodes array where the given Aabb may possibly reside.
	 * This does not search recursively, and only returns one of the four children from this node.
	 * This will return the index values of the SubNodes regardless if they're nullptrs or not.
	 * Returns -1 if the given target does not fit in any of the four quads.
	 * If the return value is not negative, then the outSubRegionCenter will be assigned.
	 */
	virtual int GetSubRegion (const Aabb& target, Vector2& outSubRegionCenter) const;

	/**
	 * Creates and initializes a sub region. This will also move any objects the sub region encompasses
	 * to the new sub region.
	 */
	virtual void InitializeSubRegion (int subRegionIdx, const Vector2& subRegionCenter);

	/**
	 * If this leaf node does not contain enough objects, it'll move its objects to the parent node before deleting self.
	 * This function assumes this node is a leaf and not a root.
	 * It's not safe to access anything from this instance after calling this function!
	 */
	virtual void CollapseLeaf ();

#ifdef DEBUG_MODE
	/**
	 * Recursively iterates through each sub node to instantiate RenderComponents for each region.
	 * The RenderComponents are attached to the given Visualizer.
	 */
	virtual void InitializeVisualizer (QuadTreeVisualizer* inVisualizer);

	/**
	 * Recursively clears any pointers to the tree visualizer. Since the QuadTree does not take ownership, it will
	 * not destroy the components.
	 */
	virtual void ClearVisualizerReferences ();
#endif

#ifdef DEBUG_MODE
	//Used for verifying the data structure during the unit tests.
	friend class PhysicsUnitTest;
	friend class QuadTreeVisualizer;
#endif
};
SD_END