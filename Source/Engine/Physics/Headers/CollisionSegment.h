/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionSegment.h

  A 3D geometric collision shape that is defined by its two end points.
  The Position of this shape is always at the shape's start point.
=====================================================================
*/

#pragma once

#include "CollisionShape.h"

SD_BEGIN
class PHYSICS_API CollisionSegment : public CollisionShape
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The 3D position of the other end point in relative to the Position. */
	Vector3 EndPoint;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionSegment ();
	CollisionSegment (Float inScale, const Vector3& inPosition);
	CollisionSegment (const Vector3& inEndPoint);
	CollisionSegment (Float inScale, const Vector3& inPosition, const Vector3& inEndPoint);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RefreshBoundingBox () override;
	virtual bool OverlapsWith (const CollisionShape* otherShape) const override;
	virtual bool EncompassesPoint (const Vector3& point) const override;
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetEndPoint (const Vector3& inEndPoint);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector3 GetEndPoint () const
	{
		return EndPoint;
	}

	inline const Vector3& ReadEndPoint () const
	{
		return EndPoint;
	}

	//Retrieves the directional vector
	inline Vector3 GetDirection () const
	{
		return (EndPoint * Scale);
	}

	//Retrieves the EndPoint in absolute coordinates
	inline Vector3 GetAbsEndPoint () const
	{
		return ((EndPoint * Scale) + Position);
	}
};
SD_END