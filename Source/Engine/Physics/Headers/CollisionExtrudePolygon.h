/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionExtrudePolygon.h

  A 3D geometric collision shape that is defined by a series of 2D line segments on the XY plane. Each
  line segment is defined by an X,Y coordinate relative to this shape's Position.

  All line segments are connected to each other to form a polygon. The last vertex connects to the first vertex.

  Finally, the polygon extrudes upwards along the Z-axis to form the 3D prism. The position value resides at the bottom
  of the shape.

  Running intersection tests for each line segment is generally slow. It's generally a good idea to break down large
  polygons into smaller ones to take advantage of their Aabb bounding box checks to reduce the amount of segments to check.
=====================================================================
*/

#pragma once

#include "CollisionShape.h"

SD_BEGIN
class PHYSICS_API CollisionExtrudePolygon : public CollisionShape
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The distance between the bottom and the top of this shape. */
	Float ExtrudeHeight;

	/* A series of vertices that compose the polygon.
	Every vertex resides on the XY plane that intersects the shape's Z position.
	The segments are joined together by each vertex. The first vertex connects to the last vertex.
	The coordinates are relative to the shape's Position. */
	std::vector<Vector2> Vertices;

	/* List of cached vertices that makes up the segments in world space.
	This is essentially a duplicated variable of Vertices with the exception that Vertices[0] is added at the end to connect the last and first vertex.
	Also each vertex is Vertices[i] + (Position*Scale) since they're translated in world space. */
	std::vector<Vector2> WorldSpaceSegments;

	/* Container of normals that correspond to each side facing plane of this mesh. Normals always face outward. */
	std::vector<Vector2> Normals;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionExtrudePolygon ();
	CollisionExtrudePolygon (Float inScale, const Vector3& inPosition);
	CollisionExtrudePolygon (Float inScale, const Vector3& inPosition, Float inExtrudeHeight, const std::vector<Vector2>& inVertices);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RefreshBoundingBox () override;
	virtual bool OverlapsWith (const CollisionShape* otherShape) const override;
	virtual bool EncompassesPoint (const Vector3& point) const override;
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const override;
	virtual void SetPosition (const Vector3& newPosition) override;
	virtual void Translate (const Vector3& delta) override;
	virtual void SetScale (Float newScale) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true specified point resides inside this polygon regardless of the shape's extrude height and Z-position.
	 * The given point should be in world space.
	 */
	virtual bool IsWithinPolygon (const Vector2& point) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetExtrudeHeight (Float newExtrudeHeight);
	virtual void SetVertices (const std::vector<Vector2>& newVertices);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetExtrudeHeight () const
	{
		return ExtrudeHeight;
	}

	inline const std::vector<Vector2>& ReadVertices () const
	{
		return Vertices;
	}

	inline const std::vector<Vector2>& ReadWorldSpaceSegments () const
	{
		return WorldSpaceSegments;
	}

	inline const std::vector<Vector2>& ReadNormals () const
	{
		return Normals;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the vertices to ensure they're correct. If not valid, then the vertex array clears.
	 * Returns true if the vertices are fine.
	 */
	bool ValidateVertices ();

	/**
	 * Reads the Vertices vector and updates the WorldSpaceSegments to reflect the new position and vertices.
	 */
	void UpdateWorldSpaceSegments ();

	/**
	 * Iterates through each vertex and replaces the Normals vector with new values reflecting each border.
	 */
	void UpdateNormals ();

	/**
	 * Given the 2D start point in world space, this function will find a ray direction
	 * so that it doesn't intersect any vertex on this polygon.
	 * Returns false if it was unable to find a direction since there are just WAY too many vertices in this polygon.
	 */
	virtual bool FindRayDir (const Vector2& startPt, Vector2& outRayDir) const;
};
SD_END