/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Geometry2dUtils.h

  Defines a bunch of useful geometric functions in 2D space.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class PHYSICS_API Geometry2dUtils : public BaseUtils
{
	DECLARE_CLASS(Geometry2dUtils)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EIntersectionType
	{
		IT_None, //The geometric shapes do not intersect
		IT_Point, //The geometric shapes intersect at a single point
		IT_Segment, //The overlapping portion between the geometric shapes forms a segment with two distinct end points
		IT_Ray, //The overlapping portion between the geometric shapes forms a ray where it starts from one point and moves to infinity from its direction
		IT_Line, //The overlapping portion between the geometric shapes forms a line where it has no end points
		IT_Plane //The overlapping portion between the geometric shapes forms a plane with an area greater than zero
	};


	/*
	=====================
	  Struct
	=====================
	*/

public:
	struct SIntersectResults
	{
		EIntersectionType IntersectionType;

		/* If the IntersectionType is:
		IT_None then this variable is not assigned.
		IT_Point then this variable is equal to that point of intersection.
		IT_Segment then this variable is one of the two end points in the segment.
		IT_Ray then this variable is the end point of the ray.
		IT_Line then this variable is one of the two points the line intersects.*/
		Vector2 Pt1;

		/* If the IntersectionType is:
		IT_None or IT_Point then this variable is not assigned.
		IT_Segment then this variable is the other end point of the segment.
		IT_Ray then this variable is a point on the ray that is not on the end point.
		IT_Line then this variable is the other point that the line intersects. */
		Vector2 Pt2;
	};


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the three points are collinear.
	 */
	static bool ArePointsCollinear (const Vector2& a, const Vector2& b, const Vector2& c, Float tolerance = FLOAT_TOLERANCE);

	/**
	 * Returns true if the given point is within the segment.
	 */
	static bool IsOnSegment (const Vector2& segPtA, const Vector2& segPtB, const Vector2& testPt, Float tolerance = FLOAT_TOLERANCE);

	/**
	 * Assuming that the three points are already collinear, this returns true if the middle point is between the two end points.
	 * This is essentially the same as IsOnSegment but the collinear assumption allows this function to perform this check faster.
	 */
	static bool IsBetweenCollinearPts (const Vector2& endPtA, const Vector2& endPtB, const Vector2& testPt);

	/**
	 * Calculates the point's distance to the line defined by 2 points it intersects.
	 */
	static Float CalcDistToLine (const Vector2& pt, const Vector2& linePt1, const Vector2& linePt2);

	/**
	 * Calculates the point's distance to the segment defined by its 2 end points.
	 */
	static Float CalcDistToSegment (const Vector2& pt, const Vector2& segPt1, const Vector2& segPt2);

	/**
	 * Calculates the closest point to the target that is also on the specified line.
	 */
	static Vector2 CalcClosestPointOnLine (const Vector2& target, const Vector2& linePt1, const Vector2& linePt2);

	/**
	 * Calculates the closest point to the target that is also on the specified segment.
	 */
	static Vector2 CalcClosestPointOnSegment (const Vector2& target, const Vector2& segPt1, const Vector2& segPt2);

	/**
	 * Calculates where the two segments intersect.
	 * Segments are defined by a line connecting and ending at the two points.
	 */
	static void CalcSegmentIntersection (const Vector2& seg1PtA, const Vector2& seg1PtB, const Vector2& seg2PtA, const Vector2& seg2PtB, SIntersectResults& outIntersection);

	/**
	 * Calculates where the two rays intersect.
	 * Rays are defined by line extending from PointA that intersects PointB.
	 */
	static void CalcRayIntersection (const Vector2& ray1PtA, const Vector2& ray1PtB, const Vector2& ray2PtA, const Vector2& ray2PtB, SIntersectResults& outIntersection);

	/**
	 * Calculate where two lines intersect.
	 * A line extends to infinity that intersects through both points.
	 */
	static void CalcLineIntersection (const Vector2& line1PtA, const Vector2& line1PtB, const Vector2& line2PtA, const Vector2& line2PtB, SIntersectResults& outIntersection);

	/**
	 * Calculates where the ray intersects the line.
	 */
	static void CalcLineRayIntersection (const Vector2& linePtA, const Vector2& linePtB, const Vector2& rayPtA, const Vector2& rayPtB, SIntersectResults& outIntersection);

	/**
	 * Calculates where the segment intersects the line.
	 */
	static void CalcLineSegIntersection (const Vector2& linePtA, const Vector2& linePtB, const Vector2& segPtA, const Vector2& segPtB, SIntersectResults& outIntersection);

	/**
	 * Calculates where the segment intersects the ray.
	 */
	static void CalcRaySegIntersection (const Vector2& rayPtA, const Vector2& rayPtB, const Vector2& segPtA, const Vector2& segPtB, SIntersectResults& outIntersection);

	/**
	 * Calculates the intersection points where the line meets the circle.
	 * Returns true if there are at least one intersection.
	 */
	static bool CalcCircleSegIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& segPtA, const Vector2& segPtB, std::vector<Vector2>& outIntersections);
	static bool CalcCircleRayIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& rayStartPt, const Vector2& rayPtB, std::vector<Vector2>& outIntersections);
	static bool CalcCircleLineIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& linePtA, const Vector2& linePtB, std::vector<Vector2>& outIntersections);



	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Defines the magnitude of the resulting vector of the cross product between the two 2D vectors.
	 * This would have been the magnitude of the Z-component had both of these 2D vectors been in 3D space.
	 * See: https://stackoverflow.com/questions/243945/calculating-a-2d-vectors-cross-product
	 */
	static Float CrossProduct2d (const Vector2& a, const Vector2& b);

	/**
	 * Identifies the slope and Y-intercept of the line that also intersects the two points.
	 * Returns false for undefined slopes (where the line is parallel to the Y-axis).
	 * The out parameters are not assigned if this function returns false.
	 */
	static bool CalcLineFormula (const Vector2& pt1, const Vector2& pt2, Float& outSlope, Float& outYIntercept);

	/**
	 * Returns true if the given ray encompasses the test point.
	 * This function assumes that all three points are collinear.
	 */
	static bool IsRayEncompassingPoint (const Vector2& testPt, const Vector2& rayStart, const Vector2& rayPt);
};
SD_END