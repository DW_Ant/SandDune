/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsUtils.h

  Defines a group of functions related to interfacing with PhysicsComponents within a QuadTree.

  This class defines the following:
  * Trace/sweep functions
  * Locating all physics components within a region
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class PhysicsComponent;
class QuadTree;

class PHYSICS_API PhysicsUtils : public BaseUtils
{
	DECLARE_CLASS(PhysicsUtils)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct STraceParams
	{
		/* Determines which collision channels are relevant for this trace. Objects that doesn't have any of these channels are ignored in the trace.
		By default all collision channels are relevant. */
		Int CollisionChannels = ~0; //Everything is relevant

		/* List of PhysicsComponents to ignore when conducting the trace. */
		std::vector<PhysicsComponent*> ExceptionList;

		/* Determines the behavior if the trace starts from inside a solid object. If true, this trace will return the start trace position as if the trace is 'shooting at point blank'.
		Otherwise the trace will continue. And if the end trace pierces through the solid object, the EndPoint will return point where the trace exits the shape. */
		bool bCanCollideAtStart = false;

		//For reasons unknown, I'm getting unresolved symbol errors when implementing a manual constructor (unlike STraceResults). To workaround, ended up inlining default variables in the declaration instead. Possible bug in Visual Studio?
	};

	struct STraceResults
	{
		/* The point on the trace where it first collided with an object. If it doesn't collide with anything, it'll simply be the end point where the trace ended. */
		Vector3 EndPoint;

		/* The physics component the trace collided against. */
		PhysicsComponent* CollidedAgainst;

	private:
		/* The normal of the surface this ray trace collided against. This vector is always normalized. */
		Vector3 Normal;

	public:
		STraceResults ();

		//This setter automatically normalizes the parameter
		void SetNormal (const Vector3& inNormal);

		inline Vector3 GetNormal () const
		{
			return Normal;
		}

		inline const Vector3& ReadNormal () const
		{
			return Normal;
		}

		//Returns true if this trace collided against something.
		inline bool HasImpact () const
		{
			return (CollidedAgainst != nullptr);
		}
	};

	struct SRelevanceParams
	{
		/* Determines which channels are relevant for this check. If the overlapping shape contains any of these channels, they are considered relevant for the results. */
		Int CollisionChannels = ~0;

		/* List of PhysicsComponents this check will ignore. */
		std::vector<PhysicsComponent*> ExceptionList;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines the maximum length of each vector when conducting traces. Long traces are divided into fragments to reduce iteration time by reducing the size of the AABB,
	and sorting collision order where closer shapes to the start trace will be checked before the shapes near the end.
	This value determines the limit of each segment (in cms). */
	static Float TraceStepLength;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Conducts a 3D trace from starting point to the end point (in absolute coordinates), and return the information about its collision with a PhysicsComponent.
	 */
	static STraceResults RayTrace (QuadTree* rootTree, const Vector3& startTrace, const Vector3& endTrace, const STraceParams& traceParams);

	/**
	 * Conducts a 3D trace to identify if any objects overlaps with the ray, similar to the RayTrace method.
	 * Returns true if there is at least one physics component overlapping with the ray trace. 
	 *
	 * Advantages:
	 * Faster - Breaks early upon first intersection. Uses faster algorithms (Overlaps rather than CalcCollision). Doesn't need to calculate impact point nor the closest shape to the start position.
	 * Simpler results - Returns a simple bool instead of a data struct
	 *
	 * Disadvantages:
	 * Does not return the PhysicsComponent that collided against this ray.
	 * Does not return the location or normal of the impact.
	 */
	static bool FastTrace (QuadTree* rootTree, const Vector3& startTrace, const Vector3& endTrace, const STraceParams& traceParams);

	/**
	 * Performs a line trace and figures all shapes that overlaps with that line.
	 */
	static std::vector<PhysicsComponent*> Sweep (QuadTree* rootTree, const Vector3& startSweep, const Vector3& endSweep, const SRelevanceParams& sweepParams);

	/**
	 * Returns all PhysicsComponents that overlap the specified coordinate.
	 */
	static std::vector<PhysicsComponent*> FindEncompassingComps (QuadTree* rootTree, const Vector3& point, const SRelevanceParams& overlapParams);

	/**
	 * Returns all PhysicsComponents that are within range from the specified point.
	 * @param point The coordinate in world space to search from.
	 * @param maxDist The maximum allowable distance to be considered from the point to the closest point on the PhysicsComponent's CollisionShape.
	 */
	static std::vector<PhysicsComponent*> FindNearbyComps (QuadTree* rootTree, const Vector3& point, Float maxDist, const SRelevanceParams& overlapParams);
};
SD_END