/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsComponent.h

  EngineComponent responsible for managing the PhysicsComponents.

  This component houses the QuadTree that is used for tracking PhysicsComponents, and with this,
  collision queries (such as ray traces) will go through this component.

=====================================================================
*/

#pragma once

#include "QuadTree.h"
#include "Physics.h"

SD_BEGIN
class PHYSICS_API PhysicsEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(PhysicsEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The root QuadTree that tracks CollisionShapes instantiated in this thread. */
	QuadTree RootPhysTree;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PhysicsEngineComponent (); //Defaults the world length to 10 kilometers

	/**
	 * Creates an PhysicsEngineComponent instance that initializes its root QuadTree with its maximum length equal
	 * to the given maxWorldLength (in centimeters).
	 */
	PhysicsEngineComponent (Float maxWorldLength);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline QuadTree* GetRootPhysTree ()
	{
		return &RootPhysTree;
	}
};
SD_END