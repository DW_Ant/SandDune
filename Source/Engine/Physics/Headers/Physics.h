/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Physics.h

  Contains important file includes and definitions for the Physics module.

  The Physics module adds primitive movement and collision detection for Entities in
  3D space. Entities with PhysicsComponents may move around every frame and collide with
  other PhysicsComponents and collision queries (such as ray traces).

  This module uses very simple but fast physics systems. For a robust physics system, use the Box 2D
  module instead of this module.

  This module supports the following:
  * Simple collision handling (block or overlap on collision)
  * Simple but fast shapes
  * Collision channels
  * Ray tracing
  * Direct manipulation to the owner's scene transformation using its native coordinate space.

=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef PHYSICS_EXPORT
		#define PHYSICS_API __declspec(dllexport)
	#else
		#define PHYSICS_API __declspec(dllimport)
	#endif
#else
	#define PHYSICS_API
#endif

//Update physics before rendering and box 2d physics.
#define TICK_GROUP_PRE_PHYSICS "PrePhysics"
#define TICK_GROUP_PRIORITY_PRE_PHYSICS 602

#define TICK_GROUP_PHYSICS "Physics"
#define TICK_GROUP_PRIORITY_PHYSICS 600

#define TICK_GROUP_POST_PHYSICS "PostPhysics"
#define TICK_GROUP_PRIORITY_POST_PHYSICS 598

SD_BEGIN
extern PHYSICS_API LogCategory PhysicsLog;
SD_END