/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionShape.h

  Parent class of all 3D geometric shapes PhysicsComponents use to define their collision bounds.

  This declares a series of functions the physics simulation will use to detect collision.
  For performance reasons, these shapes are not very malleable. It can only scale uniformly,
  and the origin is often at a fixed point. Lastly these shapes cannot be rotated.
=====================================================================
*/

#pragma once

#include "Physics.h"
#include "QuadTreeInterface.h"

SD_BEGIN
class PhysicsComponent;

class PHYSICS_API CollisionShape : public QuadTreeInterface
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/**
	 * In order to avoid a series of type casting to subclasses, each subclass has their own type
	 * to quickly access subclasses. That way special function overloads may be called to handle more
	 * efficient collision calculations rather than treating each shape generically.
	 */
	enum EShapeType
	{
		ST_Segment,
		ST_Sphere,
		ST_Capsule,
		ST_ExtrudePolygon,
		ST_Unknown
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The Component that is responsible for this shape.
	This can be a nullptr if nothing is managing this shape. */
	PhysicsComponent* OwningComponent;

	/* The minimum sized AABB that encompasses this geometric shape.
	The dimensions of this box includes this shape's scale and translation. */
	Aabb BoundingBox;

	/* The scalar multiplier for the size of this shape. This does not multiply its translation. */
	Float Scale;

	/* The absolute translation of this shape. This specifies where this shape's origin resides in world space. */
	Vector3 Position;

	EShapeType Shape;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionShape ();
	CollisionShape (Float inScale, const Vector3& inPosition);
	virtual ~CollisionShape ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual const Aabb& ReadBoundingBox () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Generates the minimum sized bounding box that encompasses this shape.
	 * This function considers the shape's current scale and position.
	 */
	virtual void RefreshBoundingBox () = 0;

	/**
	 * Returns true if any part of this shape is overlapping with the other shape.
	 */
	virtual bool OverlapsWith (const CollisionShape* otherShape) const;

	/**
	 * Returns true if this shape encompasses the given point.
	 */
	virtual bool EncompassesPoint (const Vector3& point) const;

	/**
	 * Returns a point on this shape that is the closest to the desired point.
	 */
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const = 0;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningComponent (PhysicsComponent* newOwningComponent);

	/**
	 * Moves the origin of this shape to the new world position. This also updates the Aabb accordingly.
	 */
	virtual void SetPosition (const Vector3& newPosition);

	/**
	 * Displaces the current position with the given vector.
	 */
	virtual void Translate (const Vector3& delta);

	/**
	 * Sets the scale and adjusts the Aabb accordingly.
	 */
	virtual void SetScale (Float newScale);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline PhysicsComponent* GetOwningComponent () const
	{
		return OwningComponent;
	}

	inline Float GetScale () const
	{
		return Scale;
	}

	inline Vector3 GetPosition () const
	{
		return Position;
	}

	inline const Vector3& ReadPosition () const
	{
		return Position;
	}

	inline EShapeType GetShape () const
	{
		return Shape;
	}
};
SD_END