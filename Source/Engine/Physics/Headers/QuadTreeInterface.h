/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QuadTreeInterface.h

  A simple interface that allows an object to be inserted to a Quad Tree.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class QuadTree;

class PHYSICS_API QuadTreeInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The node this object resides in the QuadTree. The QuadTree, itself, updates this pointer. */
	QuadTree* TreeNode = nullptr;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the Aabb reference that encompasses this object.
	 */
	virtual const Aabb& ReadBoundingBox () const = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline QuadTree* GetTreeNode () const
	{
		return TreeNode;
	}

friend class QuadTree;
};
SD_END