/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionUtils.h

  Defines a series of functions that handles special cases that can quickly calculate
  collision detection.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class CollisionCapsule;
class CollisionExtrudePolygon;
class CollisionSegment;
class CollisionSphere;

class PHYSICS_API CollisionUtils : public BaseUtils
{
	DECLARE_CLASS(CollisionUtils)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SCollisionInfo
	{
		/* Deterines a point in location where the collision occured. For surface-to-surface collisions, this would be the center point of the colliding surfaces. */
		Vector3 ImpactLocation;

	private:
		/* Determines the angle off incident. If an object collide against another perpendicularly, the angle of incident is the reverse direction of the moving object.
		This vector is always normalized. */
		Vector3 Normal;

	public:
		SCollisionInfo ();
		SCollisionInfo (const Vector3& inImpactLocation, const Vector3& inNormal);
		SCollisionInfo (const SCollisionInfo& other);

		/* Automatically normalizes the given vector before assigning it to Normal. */
		void SetNormal (const Vector3& newNormal);

		inline Vector3 GetNormal () const
		{
			return Normal;
		}

		inline const Vector3& ReadNormal () const
		{
			return Normal;
		}
	};


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the two shapes overlap each other.
	 * These functions assume the shapes' Aabb are already overlapping.
	 * In general Overlaps is faster than CalcCollision, but it doesn't retrieve the collision impact location and normal.
	 */
	static bool Overlaps (const CollisionSegment* a, const CollisionSegment* b);
	static bool Overlaps (const CollisionSegment* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionSegment* a, const CollisionCapsule* b);
	static bool Overlaps (const CollisionSegment* a, const CollisionExtrudePolygon* b);

	static bool Overlaps (const CollisionSphere* a, const CollisionSegment* b);
	static bool Overlaps (const CollisionSphere* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionSphere* a, const CollisionCapsule* b);
	static bool Overlaps (const CollisionSphere* a, const CollisionExtrudePolygon* b);

	static bool Overlaps (const CollisionCapsule* a, const CollisionSegment* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionCapsule* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionExtrudePolygon* b);

	static bool Overlaps (const CollisionExtrudePolygon* a, const CollisionSegment* b);
	static bool Overlaps (const CollisionExtrudePolygon* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionExtrudePolygon* a, const CollisionCapsule* b);

	/**
	 * This is a slow function! Use sparingly or use on very simple polygons.
	 * It'll be much faster to break large polygons into a bunch of smaller ones to leverage Aabb filtering.
	 * Complexity is O(2n+n^2)
	 */
	static bool Overlaps (const CollisionExtrudePolygon* a, const CollisionExtrudePolygon* b);

	/**
	 * Returns true if the two shapes collided against each other. These functions already assume that both of the shapes' AABB are overlapping.
	 * If they collide, it'll populate the outCollision struct with information about the collision from the perspective of A colliding into B.
	 */
	static bool CalcCollision (const CollisionSegment* a, const CollisionSegment* b, SCollisionInfo& outCollision);
	static bool CalcCollision (const CollisionSegment* a, const CollisionSphere* b, SCollisionInfo& outCollision);
	static bool CalcCollision (const CollisionSegment* a, const CollisionCapsule* b, SCollisionInfo& outCollision);
	static bool CalcCollision (const CollisionSegment* a, const CollisionExtrudePolygon* b, SCollisionInfo& outCollision);

	//TODO: Writing unit test cases for these is pretty exhausting. Finish writing CalcCollision for sphere-sphere, sphere-capsule, sphere-poly, capsule-capsule, capsulse-poly, and poly-poly.
	static bool CalcCollision (const CollisionSphere* a, const CollisionSegment* b, SCollisionInfo& outCollision);

	static bool CalcCollision (const CollisionCapsule* a, const CollisionSegment* b, SCollisionInfo& outCollision);

	static bool CalcCollision (const CollisionExtrudePolygon* a, const CollisionSegment* b, SCollisionInfo& outCollision);
};
SD_END