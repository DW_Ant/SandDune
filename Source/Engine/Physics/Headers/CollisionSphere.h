/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionSphere.h

  A 3D geometric collision shape that is defined by its center point and its radius.
  The center is always at the shape's Position point.
=====================================================================
*/

#pragma once

#include "CollisionShape.h"

SD_BEGIN
class PHYSICS_API CollisionSphere : public CollisionShape
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The distance from the center to the edge of the sphere. */
	Float Radius;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionSphere ();
	CollisionSphere (Float inScale, const Vector3& inPosition);
	CollisionSphere (Float inRadius);
	CollisionSphere (Float inScale, const Vector3& inPosition, Float inRadius);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RefreshBoundingBox () override;
	virtual bool OverlapsWith (const CollisionShape* otherShape) const override;
	virtual bool EncompassesPoint (const Vector3& point) const override;
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetRadius (Float newRadius);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetRadius () const
	{
		return Radius;
	}
};
SD_END