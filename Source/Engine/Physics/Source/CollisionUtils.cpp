/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionUtils.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry2dUtils.h"
#include "Geometry3dUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::CollisionUtils, SD::BaseUtils)
SD_BEGIN

CollisionUtils::SCollisionInfo::SCollisionInfo () :
	ImpactLocation(Vector3::ZERO_VECTOR),
	Normal(Vector3(1.f, 0.f, 0.f)) //default to forward direction
{
	//Noop
}

CollisionUtils::SCollisionInfo::SCollisionInfo (const Vector3& inImpactLocation, const Vector3& inNormal) :
	ImpactLocation(inImpactLocation)
{
	SetNormal(inNormal);
}

CollisionUtils::SCollisionInfo::SCollisionInfo (const SCollisionInfo& other) :
	ImpactLocation(other.ImpactLocation),
	Normal(other.Normal)
{
	//Noop
}

void CollisionUtils::SCollisionInfo::SetNormal (const Vector3& newNormal)
{
	Normal = Vector3::Normalize(newNormal);
}

bool CollisionUtils::Overlaps (const CollisionSegment* a, const CollisionSegment* b)
{
	Vector3 intersectionPt;
	return Geometry3dUtils::IsIntersecting(a->ReadPosition(), a->GetDirection(), b->ReadPosition(), b->GetDirection(), true, OUT intersectionPt);
}

bool CollisionUtils::Overlaps (const CollisionSegment* a, const CollisionSphere* b)
{
	Vector3 intersectionPoint;
	Float dist = Geometry3dUtils::CalcDistBetweenPointAndLine(a->ReadPosition(), a->GetDirection(), b->ReadPosition(), true, OUT intersectionPoint);
	return (dist < (b->GetRadius() * b->GetScale()));
}

bool CollisionUtils::Overlaps (const CollisionSegment* a, const CollisionCapsule* b)
{
	Vector3 intersectionA;
	Vector3 intersectionB;

	//Treat a capsule as a vertical segment.
	Vector3 bottomSphereCenter = b->GetPosition();
	bottomSphereCenter.Z -= b->GetCylinderZEdge();

	Float dist = Geometry3dUtils::CalcDistBetweenLines(a->ReadPosition(), a->GetDirection(), bottomSphereCenter, Vector3(0.f, 0.f, b->GetHeight() * b->GetScale()), true, OUT intersectionA, OUT intersectionB);

	return (dist <= (b->GetRadius() * b->GetScale()));
}

bool CollisionUtils::Overlaps (const CollisionSegment* a, const CollisionExtrudePolygon* b)
{
	Vector3 linePt = a->ReadPosition();
	Vector3 lineDir = a->GetDirection();
	const std::vector<Vector2>& vertices = b->ReadWorldSpaceSegments();
	for (size_t idx = 0; (idx+1) < vertices.size(); ++idx)
	{
		CHECK(idx < b->ReadNormals().size())
		Geometry3dUtils::SPlane plane(Vector3(vertices[idx].X, vertices[idx].Y, b->ReadPosition().Z), b->ReadNormals().at(idx).ToVector3());

		Vector3 intersection;
		if (Geometry3dUtils::CalcLinePlaneIntersection(plane, linePt, lineDir, true, OUT intersection))
		{
			//Test Z-axis.
			if (intersection.Z < b->ReadPosition().Z || intersection.Z > b->ReadPosition().Z + (b->GetExtrudeHeight() * b->GetScale()))
			{
				//Intersection is beyond the extruded polygon length
				continue;
			}

			//Ensure the intersection point is within the plane boundaries.
			if (Geometry2dUtils::IsBetweenCollinearPts(vertices[idx], vertices[idx+1], intersection.ToVector2()))
			{
				//Valid intersection detected. This segment overlaps with the polygon.
				return true;
			}
		}
	}

	//Handle case where the segment is completely within the extruded polygon
	if (b->EncompassesPoint(a->ReadPosition()))
	{
		return true;
	}

	//Handle case where the segment intersects the polygon through its top or bottom surfaces.
	if (a->ReadEndPoint().Z != 0.f)
	{
		Float bottomSeg;
		Float topSeg;
		if (a->ReadEndPoint().Z > 0.f) //segment is moving up
		{
			bottomSeg = a->ReadPosition().Z;
			topSeg = a->GetAbsEndPoint().Z;
		}
		else //segment is moving down
		{
			bottomSeg = a->GetAbsEndPoint().Z;
			topSeg = a->ReadPosition().Z;
		}

		Range<Float> zRange(bottomSeg, topSeg);
		bool bTestTopPlane = true;

		//If segment intersects the bottom plane.
		if (zRange.ContainsValue(b->ReadPosition().Z))
		{
			Vector3 intersection;

			//Check where the segment intersects the bottom plane.
			if (Geometry3dUtils::CalcLinePlaneIntersection(Geometry3dUtils::SPlane(b->ReadPosition(), Vector3(0.f, 0.f, -1.f)), a->ReadPosition(), a->ReadEndPoint(), true, OUT intersection))
			{
				if (b->IsWithinPolygon(intersection.ToVector2()))
				{
					return true;
				}
				else
				{
					//If the polygon does not encompass the bottom plane intersection, it is not possible to encompass the top plane's intersection without crossing one of the walls. The walls are already checked earlier in this function.
					//Skip the top plane check.
					bTestTopPlane = false;
				}
			}
		}

		Float topPlane = b->ReadPosition().Z + (b->GetExtrudeHeight() * b->GetScale());
		if (bTestTopPlane && zRange.ContainsValue(topPlane))
		{
			Vector3 intersection;
			if (Geometry3dUtils::CalcLinePlaneIntersection(Geometry3dUtils::SPlane(Vector3(b->ReadPosition().X, b->ReadPosition().Y, topPlane), Vector3(0.f, 0.f, 1.f)), a->ReadPosition(), a->ReadEndPoint(), true, OUT intersection))
			{
				if (b->IsWithinPolygon(intersection.ToVector2()))
				{
					return true;
				}
			}
		}
	}

	return false;
}

//============================

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionSegment* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionSphere* b)
{
	//Simple distance check
	Float distSquared = (a->ReadPosition() - b->ReadPosition()).CalcDistSquared();

	return (distSquared <= std::pow((a->GetRadius() * a->GetScale()).Value + (b->GetRadius() * b->GetScale()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionCapsule* b)
{
	//If the sphere is vertically between the capsule's end points...
	Float cylinderZEdge = b->GetCylinderZEdge();
	if (Float::Abs(a->ReadPosition().Z - b->ReadPosition().Z) <= cylinderZEdge)
	{
		//...Then only check the distance between the two centers on the XY plane.
		Vector3 delta = (a->ReadPosition() - b->ReadPosition());
		return (delta.CalcDistSquared(false) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
	}

	//Otherwise, compare the sphere's distance to one of the two end points.
	Vector3 endPoint(b->GetPosition());

	endPoint.Z += (a->ReadPosition().Z < endPoint.Z) ? -cylinderZEdge : cylinderZEdge;
	return ((a->ReadPosition() - endPoint).CalcDistSquared(true) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionExtrudePolygon* b)
{
	//Return true if the sphere encompasses the polygon's closest point towards the sphere's center.
	return a->EncompassesPoint(b->CalcClosestPoint(a->ReadPosition()));
}

//============================

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionSegment* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionSphere* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionCapsule* b)
{
	//Identify if the two cylinders intersect along the Z-axis
	Float deltaHeight = Float::Abs(a->ReadPosition().Z - b->ReadPosition().Z);
	Float aCylinderHeight = a->GetCylinderZEdge();
	Float bCylinderHeight = b->GetCylinderZEdge();
	if (deltaHeight <= aCylinderHeight + bCylinderHeight)
	{
		Vector3 delta = (a->ReadPosition() - b->ReadPosition());

		//These overlap if the dist between their centers along the XY plane is within the sum of their radii.
		return (delta.CalcDistSquared(false) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
	}

	//Otherwise compare the two spherical end points
	Vector3 lowerSphere;
	Vector3 upperSphere;

	//Figure out which two of the four spheres are closest to each other.
	if (a->ReadPosition().Z < b->ReadPosition().Z)
	{
		lowerSphere = a->GetPosition();
		lowerSphere.Z += aCylinderHeight;
		upperSphere = b->GetPosition();
		upperSphere.Z -= bCylinderHeight;
	}
	else
	{
		lowerSphere = b->GetPosition();
		lowerSphere.Z += bCylinderHeight;
		upperSphere = a->GetPosition();
		upperSphere.Z -= aCylinderHeight;
	}

	return ((upperSphere - lowerSphere).CalcDistSquared() < std::pow((a->GetRadius() * a->GetScale()).Value + (b->GetRadius() * b->GetScale()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionExtrudePolygon* b)
{
	Float clampedZ = Utils::Clamp(a->ReadPosition().Z, b->ReadPosition().Z, b->ReadPosition().Z + (b->GetExtrudeHeight() * b->GetScale()));
	Vector3 targetPt(a->ReadPosition().X, a->ReadPosition().Y, clampedZ);

	return a->EncompassesPoint(b->CalcClosestPoint(targetPt));
}

//============================

bool CollisionUtils::Overlaps (const CollisionExtrudePolygon* a, const CollisionSegment* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionExtrudePolygon* a, const CollisionSphere* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionExtrudePolygon* a, const CollisionCapsule* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionExtrudePolygon* a, const CollisionExtrudePolygon* b)
{
	//NOTE: These functions are assuming the Aabbs are already overlapping. There's no need to check for Z.

	if (a->ReadVertices().size() < 2 || b->ReadVertices().size() < 2)
	{
		return false;
	}

	//Check if any lines intersect each other
	Geometry2dUtils::SIntersectResults intersection;
	for (size_t aIdx = 0; (aIdx+1) < a->ReadWorldSpaceSegments().size(); ++aIdx)
	{
		for (size_t bIdx = 0; (bIdx+1) < b->ReadWorldSpaceSegments().size(); ++bIdx)
		{
			Geometry2dUtils::CalcSegmentIntersection(a->ReadWorldSpaceSegments()[aIdx], a->ReadWorldSpaceSegments()[aIdx+1], b->ReadWorldSpaceSegments()[bIdx], b->ReadWorldSpaceSegments()[bIdx+1], OUT intersection);
			if (intersection.IntersectionType != Geometry2dUtils::IT_None)
			{
				return true;
			}
		}
	}

	//Check if one polygon encompasses the other
	if (a->EncompassesPoint(b->ReadPosition()) || b->EncompassesPoint(a->ReadPosition()))
	{
		return true;
	}

	return false;
}

//============================

bool CollisionUtils::CalcCollision (const CollisionSegment* a, const CollisionSegment* b, SCollisionInfo& outCollision)
{
	Vector3 intersectionPt;
	bool bResult = Geometry3dUtils::IsIntersecting(a->ReadPosition(), a->GetDirection(), b->ReadPosition(), b->GetDirection(), true, OUT intersectionPt, 0.0001f);
	if (bResult)
	{
		outCollision.ImpactLocation = intersectionPt;

		//There are infinite number of perpendicular normals to a line. Pick a normal of a plane containing both segments. Not sure how useful this would be though.
		outCollision.SetNormal(a->ReadEndPoint().CrossProduct(b->ReadEndPoint()));
	}

	return bResult;
}

bool CollisionUtils::CalcCollision (const CollisionSegment* a, const CollisionSphere* b, SCollisionInfo& outCollision)
{
	std::vector<Vector3> intersections;
	if (Geometry3dUtils::CalcSphereLineIntersections(b->ReadPosition(), b->GetRadius() * b->GetScale(), a->ReadPosition(), a->GetDirection(), true, OUT intersections))
	{
		CHECK(!ContainerUtils::IsEmpty(intersections))
		outCollision.ImpactLocation = intersections.at(0);
		if (intersections.size() > 1 && (outCollision.ImpactLocation - a->ReadPosition()).CalcDistSquared() > (intersections.at(1) - a->ReadPosition()).CalcDistSquared())
		{
			//Two intersections found. Only consider the intersection closest to the segment's start position.
			outCollision.ImpactLocation = intersections.at(1);
		}

		outCollision.SetNormal(outCollision.ImpactLocation - b->ReadPosition());
	}
	else if ((a->ReadPosition() - b->ReadPosition()).CalcDistSquared() < b->GetRadius() * b->GetScale())
	{
		//The sphere completely encompasses the segment. Collide immediately where the segment starts.
		outCollision.ImpactLocation = a->GetPosition();
		outCollision.SetNormal(-a->GetEndPoint());
	}
	else
	{
		//No intersection
		return false;
	}

	return true;
}

//There must be a cleaner way to implement this.
bool CollisionUtils::CalcCollision (const CollisionSegment* a, const CollisionCapsule* b, SCollisionInfo& outCollision)
{
	/*
	Algorithm:
	* Treat this as a top down 2D problem (2D line approaching circle).
	* If the circle encompasses both end points of the 2D line, then run a vertical test. Segment must collide against at least one of the spheres.
	* Otherwise, calculate the intersections between the 2D line and the circle. Look at the point closest to the segment's start point.
	* Get a ratio from intersection point and the length of the segment. Use that ratio to find where that point resides in the 3D line.
	* If the intersection point is either above or below the capsule's cylinder, then run sphere-segment collision check. Only need to check against one of the two spheres.
	*/

	Float scaledRadius = b->GetRadius() * b->GetScale();
	Vector3 absEndPt = a->GetAbsEndPoint();
	Vector2 startPos2d(a->ReadPosition().ToVector2());
	Vector2 endPos2d(absEndPt.ToVector2());

	//Look at the topdown perspective to see where the line overlaps the circle.
	std::vector<Vector2> topdownIntersections;
	if (!Geometry2dUtils::CalcCircleSegIntersections(b->ReadPosition().ToVector2(), scaledRadius, startPos2d, endPos2d, OUT topdownIntersections))
	{
		//No intersections. Check if the circle encompasses the point.
		if ((startPos2d - b->ReadPosition().ToVector2()).CalcDistSquared() >= Float::Pow(scaledRadius, 2.f))
		{
			//It does not. No collision
			return false;
		}

		//The 2D circle completely encompasses the 2D segment.

		//Only possible intersections are either above or below (or both). Run the intersection tests between both spheres.
		std::vector<Vector3> topIntersections;
		Vector3 sphereCenter(b->ReadPosition() + Vector3(0.f, 0.f, b->GetCylinderZEdge()));
		if (Geometry3dUtils::CalcSphereLineIntersections(sphereCenter, scaledRadius, a->ReadPosition(), a->GetDirection(), true, OUT topIntersections))
		{
			//Remove any intersections underneathe the sphere center since those intersections are inside the cylinder part of the capsule.
			size_t i = 0;
			while (i < topIntersections.size())
			{
				if (topIntersections.at(i).Z < sphereCenter.Z)
				{
					topIntersections.erase(topIntersections.begin() + i);
					continue;
				}

				++i;
			}
		}

		//Check bottom sphere
		std::vector<Vector3> bottomIntersections;
		sphereCenter.Z -= (b->GetHeight() * b->GetScale());
		if (Geometry3dUtils::CalcSphereLineIntersections(sphereCenter, scaledRadius, a->ReadPosition(), a->GetDirection(), true, OUT bottomIntersections))
		{
			//Remove any intersections above the sphere center since those intersections are inside the cylinder part of the capsule.
			size_t i = 0;
			while (i < bottomIntersections.size())
			{
				if (bottomIntersections.at(i).Z > sphereCenter.Z)
				{
					bottomIntersections.erase(bottomIntersections.begin() + i);
					continue;
				}

				++i;
			}
		}

		//Find the point of intersection closest to the segment's starting position.
		Float bestDist = MAX_FLOAT;
		Vector3 bestIntersection;
		Vector3 bestSphereCenter;
		bool bHasIntersection = false;
		for (const Vector3& intersection : topIntersections)
		{
			Float dist = (intersection - a->ReadPosition()).CalcDistSquared();
			if (dist < bestDist)
			{
				bestIntersection = intersection;
				bestSphereCenter = sphereCenter;
				bestSphereCenter.Z += b->GetHeight() * b->GetScale();
				bestDist = dist;
				bHasIntersection = true;
			}
		}

		for (const Vector3& intersection : bottomIntersections)
		{
			Float dist = (intersection - a->ReadPosition()).CalcDistSquared();
			if (dist < bestDist)
			{
				bestIntersection = intersection;
				bestSphereCenter = sphereCenter;
				bestDist = dist;
				bHasIntersection = true;
			}
		}

		if (bHasIntersection)
		{
			outCollision.ImpactLocation = bestIntersection;
			outCollision.SetNormal(bestIntersection - bestSphereCenter);
			return true;
		}

		//Check if the capsule encompasses the segment's starting point. If so, then assume that the capsule encompasses the entire segment.
		if (b->EncompassesPoint(a->ReadPosition()))
		{
			//The cylinder completely encompasses the segment. Collide immediately where the segment starts.
			outCollision.ImpactLocation = a->GetPosition();
			outCollision.SetNormal(-a->ReadEndPoint());

			return true;
		}

		//No collision
		return false;
	}

	CHECK(!ContainerUtils::IsEmpty(topdownIntersections))

	//The segment is horizontally crossing over the 2D circle.
	if (topdownIntersections.size() > 1)
	{
		//Two intersections found. Remove the one further away from the segment's start position. NOTE: It's not possible for the start position to be inside the capsule otherwise there would be at most one intersection.
		if ((topdownIntersections.at(0) - startPos2d).CalcDistSquared() < (topdownIntersections.at(1) - startPos2d).CalcDistSquared())
		{
			topdownIntersections.pop_back();
		}
		else
		{
			topdownIntersections.erase(topdownIntersections.begin()); //Remove element 0
		}
	}

	//Find the ratio where the intersection point divides the segment
	Float intersectionRatio = (topdownIntersections.at(0) - startPos2d).VSize() / (endPos2d - startPos2d).VSize();

	CHECK(intersectionRatio >= 0.f && intersectionRatio <= 1.f)

	Vector3 intersection = a->ReadPosition() + (a->GetDirection() * intersectionRatio);

	if (Float::Abs(intersection.Z - b->ReadPosition().Z) <= b->GetHeight() * 0.5f * b->GetScale())
	{
		//The segment is intersecting the capsule on the side of the cylinder part.
		outCollision.ImpactLocation = intersection;
		outCollision.SetNormal((intersection - b->ReadPosition()) * Vector3(1.f, 1.f, 0.f)); //Flatten Z-axis since this is hitting the side of the capsule.
		return true;
	}

	//The intersection point is colliding against either the top or bottom part of the capsule.

	//Identify which sphere the line is possibly colliding against (at this point, it's not possible for the segment to collide against both capsule end points).
	Vector3 sphereCenter(b->GetPosition());
	sphereCenter.Z += (intersection.Z > b->ReadPosition().Z) ? b->GetHeight() * 0.5f * b->GetScale() : b->GetHeight() * -0.5f * b->GetScale();
	std::vector<Vector3> intersections;
	if (!Geometry3dUtils::CalcSphereLineIntersections(sphereCenter, scaledRadius, a->ReadPosition(), a->GetDirection(), true, OUT intersections))
	{
		//No intersections
		return false;
	}

	if (intersections.size() > 1)
	{
		//Prioritize on the intersection that is on the outside of the capsule (not on the inside of the cylinder). If both intersections are on the outside, prioritize the one closer to the segment's start position.

		//The segment could be moving into the capsule or moving out from the capsule.
		Vector3 priorityEndPt = ((a->ReadPosition() - sphereCenter).CalcDistSquared() > scaledRadius) ? a->GetPosition() : absEndPt;
		if ((intersections.at(0) - priorityEndPt).CalcDistSquared() > (intersections.at(1) - priorityEndPt).CalcDistSquared())
		{
			intersections.erase(intersections.begin()); //Remove the first element.
		}

		//Assume intersections[0] contains the correct intersection.
	}

	outCollision.ImpactLocation = intersections.at(0);
	outCollision.SetNormal(outCollision.ImpactLocation - sphereCenter);
	return true;
}

bool CollisionUtils::CalcCollision (const CollisionSegment* a, const CollisionExtrudePolygon* b, SCollisionInfo& outCollision)
{
	//All possible intersections. First element is the intersection location. Second element is the normal.
	std::vector<std::pair<Vector3, Vector3>> allIntersections;

	//Check if the line collides against the top or bottom planes.
	if (!a->ReadEndPoint().Z.IsCloseTo(0.f))
	{
		Geometry3dUtils::SPlane plane(b->ReadPosition() + Vector3(0.f, 0.f, b->GetExtrudeHeight() * b->GetScale()), Vector3(0.f, 0.f, 1.f));
		Vector3 intersection;

		//Top intersection
		if (Geometry3dUtils::CalcLinePlaneIntersection(plane, a->ReadPosition(), a->GetDirection(), true, OUT intersection))
		{
			if (b->EncompassesPoint(intersection - Vector3(0.f, 0.f, 0.00001f))) //This conducts an AABB check
			{
				allIntersections.push_back({intersection, plane.ReadNormal()});
			}
		}

		//Bottom intersection
		plane.SetNormal(Vector3(0.f, 0.f, -1.f));
		plane.Position = b->GetPosition();
		if (Geometry3dUtils::CalcLinePlaneIntersection(plane, a->ReadPosition(), a->GetDirection(), true, OUT intersection))
		{
			if (b->EncompassesPoint(intersection + Vector3(0.f, 0.f, 0.00001f))) //This conducts an AABB check
			{
				allIntersections.push_back({intersection, plane.ReadNormal()});
			}
		}
	}

	//Find all borders this segment collides against.
	Vector3 lineDir = a->GetDirection();
	const std::vector<Vector2>& vertices = b->ReadWorldSpaceSegments();
	for (size_t idx = 0; (idx+1) < vertices.size(); ++idx)
	{
		CHECK(idx < b->ReadNormals().size())
		Vector3 norm = b->ReadNormals().at(idx).ToVector3();
		Geometry3dUtils::SPlane plane(Vector3(vertices[idx].X, vertices[idx].Y, b->ReadPosition().Z), norm);

		Vector3 intersection;
		if (Geometry3dUtils::CalcLinePlaneIntersection(plane, a->ReadPosition(), lineDir, true, OUT intersection))
		{
			//Test Z-axis.
			if (intersection.Z < b->ReadPosition().Z || intersection.Z > b->ReadPosition().Z + (b->GetExtrudeHeight() * b->GetScale()))
			{
				//Intersection is beyond the extruded polygon length
				continue;
			}

			//Ensure the intersection point is within the plane boundaries.
			if (Geometry2dUtils::IsBetweenCollinearPts(vertices[idx], vertices[idx+1], intersection.ToVector2()))
			{
				//Valid intersection detected.
				allIntersections.push_back({intersection, norm});
			}
		}
	}

	//Find the closest intersection to start position
	Float bestDist = MAX_FLOAT;
	size_t bestIdx = UINT_INDEX_NONE;
	for (size_t i = 0; i < allIntersections.size(); ++i)
	{
		Float dist = (a->ReadPosition() - allIntersections.at(i).first).CalcDistSquared();
		if (dist < bestDist)
		{
			bestIdx = i;
			bestDist = dist;
		}
	}

	if (bestIdx != UINT_INDEX_NONE)
	{
		outCollision.ImpactLocation = allIntersections.at(bestIdx).first;
		outCollision.SetNormal(allIntersections.at(bestIdx).second);
		return true;
	}
	else if (b->EncompassesPoint(a->ReadPosition()))
	{
		//The mesh completely encompasses the segment. Collide immediately where the segment starts.
		outCollision.ImpactLocation = a->GetPosition();
		outCollision.SetNormal(-a->GetEndPoint());
		return true;
	}

	return false;
}

bool CollisionUtils::CalcCollision (const CollisionSphere* a, const CollisionSegment* b, SCollisionInfo& outCollision)
{
	bool bResult = CalcCollision(b, a, OUT outCollision);
	if (bResult)
	{
		//Reverse the normal since the sphere is colliding into the segment
		outCollision.SetNormal(outCollision.GetNormal() * -1.f);
	}

	return bResult;
}

bool CollisionUtils::CalcCollision (const CollisionCapsule* a, const CollisionSegment* b, SCollisionInfo& outCollision)
{
	bool bResult = CalcCollision(b, a, OUT outCollision);
	if (bResult)
	{
		//Reverse the normal since the capsule is colliding into the segment
		outCollision.SetNormal(outCollision.GetNormal() * -1.f);
	}

	return bResult;
}

bool CollisionUtils::CalcCollision (const CollisionExtrudePolygon* a, const CollisionSegment* b, SCollisionInfo& outCollision)
{
	bool bResult = CalcCollision(b, a, OUT outCollision);
	if (bResult)
	{
		//Reverse the normal since the capsule is colliding into the segment
		outCollision.SetNormal(outCollision.GetNormal() * -1.f);
	}

	return bResult;
}
SD_END