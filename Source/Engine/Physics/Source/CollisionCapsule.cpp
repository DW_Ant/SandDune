/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionCapsule.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry3dUtils.h"

SD_BEGIN
CollisionCapsule::CollisionCapsule () :
	CollisionShape(),
	Height(1.f),
	Radius(0.5f)
{
	Shape = ST_Capsule;
}

CollisionCapsule::CollisionCapsule (Float inScale, const Vector3& inPosition) :
	CollisionShape(inScale, inPosition),
	Height(1.f),
	Radius(0.5f)
{
	Shape = ST_Capsule;
}

CollisionCapsule::CollisionCapsule (Float inScale, const Vector3& inPosition, Float inHeight, Float inRadius) :
	CollisionShape(inScale, inPosition),
	Height(inHeight),
	Radius(inRadius)
{
	Shape = ST_Capsule;
}

void CollisionCapsule::RefreshBoundingBox ()
{
	BoundingBox.Center = Position;

	Float size = Scale * (Radius * 2.f);
	BoundingBox.Depth = size;
	BoundingBox.Width = size;
	BoundingBox.Height = GetCapsuleZLimit() * 2.f;
}

bool CollisionCapsule::OverlapsWith (const CollisionShape* otherShape) const
{
	if (!CollisionShape::OverlapsWith(otherShape))
	{
		return false;
	}

	switch (otherShape->GetShape())
	{
		case(ST_Segment):
		{
			const CollisionSegment* otherSegment = dynamic_cast<const CollisionSegment*>(otherShape);
			CHECK(otherSegment != nullptr)
			return CollisionUtils::Overlaps(this, otherSegment);
		}

		case(ST_Sphere):
		{
			const CollisionSphere* otherSphere = dynamic_cast<const CollisionSphere*>(otherShape);
			CHECK(otherSphere != nullptr)
			return CollisionUtils::Overlaps(this, otherSphere);
		}

		case(ST_Capsule):
		{
			const CollisionCapsule* otherCapsule = dynamic_cast<const CollisionCapsule*>(otherShape);
			CHECK(otherCapsule != nullptr)
			return CollisionUtils::Overlaps(this, otherCapsule);
		}

		case(ST_ExtrudePolygon):
		{
			const CollisionExtrudePolygon* otherPoly = dynamic_cast<const CollisionExtrudePolygon*>(otherShape);
			CHECK(otherPoly != nullptr)
			return CollisionUtils::Overlaps(this, otherPoly);
		}
	}

	//Fallback to something generic. This may fail if the other object is concave.
	Vector3 closestPoint = otherShape->CalcClosestPoint(Position);
	return EncompassesPoint(closestPoint);
}

bool CollisionCapsule::EncompassesPoint (const Vector3& point) const
{
	if (!CollisionShape::EncompassesPoint(point))
	{
		return false;
	}

	CollisionSegment segment = ToSegment();
	Vector3 intersection;
	Float squaredDist = Geometry3dUtils::CalcSquaredDistBetweenPointAndLine(segment.ReadPosition(), segment.GetDirection(), point, true, OUT intersection);
	return squaredDist <= Float::Pow(Radius * Scale, 2.f);
}

Vector3 CollisionCapsule::CalcClosestPoint (const Vector3& desiredPoint) const
{
	if (CollisionShape::EncompassesPoint(desiredPoint))
	{
		return desiredPoint;
	}

	Float cylinderZEdge = GetCylinderZEdge();
	if (desiredPoint.Z <= Position.Z + cylinderZEdge && desiredPoint.Z >= Position.Z - cylinderZEdge)
	{
		//Get point within the cylinder (work with the XY plane only)
		Vector2 dir = (desiredPoint.ToVector2() - Position.ToVector2());
		dir.NormalizeInline();

		//Convert back to 3D where the height is equal to the desiredPoint
		Vector3 closestPoint = (dir * Scale * Radius).ToVector3();
		closestPoint.X += Position.X;
		closestPoint.Y += Position.Y;
		closestPoint.Z = desiredPoint.Z;

		return closestPoint;
	}

	Vector3 sphereCenter(Position);
	sphereCenter.Z += (desiredPoint.Z > Position.Z) ? cylinderZEdge : -cylinderZEdge; //Identify which sphere is closer to point

	//Find the closest point from the sphere to the desired point
	Vector3 dir = desiredPoint - sphereCenter;
	dir.NormalizeInline();
	return (sphereCenter + (dir * (Scale * Radius)));
}

Float CollisionCapsule::GetCylinderZEdge () const
{
	return (Height * 0.5f * Scale);
}

Float CollisionCapsule::GetCapsuleZLimit () const
{
	return ((Height * 0.5f) + Radius) * Scale;
}

CollisionSegment CollisionCapsule::ToSegment () const
{
	return CollisionSegment(Scale, Vector3(Position.X, Position.Y, Position.Z - (Height * 0.5f * Scale)), Vector3(0.f, 0.f, Height));
}

void CollisionCapsule::SetHeight (Float inHeight)
{
	Height = inHeight;
	RefreshBoundingBox();
}

void CollisionCapsule::SetRadius (Float inRadius)
{
	Radius = inRadius;
	RefreshBoundingBox();
}
SD_END