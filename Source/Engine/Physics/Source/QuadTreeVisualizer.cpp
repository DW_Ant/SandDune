/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QuadTreeVisualizer.cpp
=====================================================================
*/

#include "QuadTree.h"
#include "QuadTreeVisualizer.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::QuadTreeVisualizer, SD::Entity)
SD_BEGIN

void QuadTreeVisualizer::InitProps ()
{
	Super::InitProps();

	RootTree = nullptr;
	BorderColor = Color(24, 24, 24, 96);
	BorderThickness = 12.f;
}

void QuadTreeVisualizer::Destroyed ()
{
	if (RootTree != nullptr)
	{
		RootTree->ClearVisualizerReferences();
		RootTree = nullptr;
	}

	Super::Destroyed();
}

void QuadTreeVisualizer::SetupVisualizer (QuadTree* inRootTree, SceneDrawLayer* inDrawLayer, Color inBorderColor, Float inBorderThickness)
{
	if (inRootTree == nullptr || inDrawLayer == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize a QuadTreeVisualizer with null QuadTree or DrawLayer."));
		return;
	}

	if (inRootTree->VisualTree != nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize a QuadTreeVisualizer to a QuadTree that already has a visualizer associated with it."));
		return;
	}

	DrawLayer = inDrawLayer;
	BorderColor = inBorderColor;
	BorderThickness = inBorderThickness;
	RootTree = inRootTree;
	inRootTree->InitializeVisualizer(this);
}
SD_END
#endif