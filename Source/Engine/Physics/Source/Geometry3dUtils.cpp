/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Geometry3dUtils.cpp
=====================================================================
*/

#include "Geometry3dUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::Geometry3dUtils, SD::BaseUtils)
SD_BEGIN

Geometry3dUtils::SPlane::SPlane () :
	Position(Vector3::ZERO_VECTOR),
	Normal(1.f, 0.f, 0.f)
{
	//Noop
}

Geometry3dUtils::SPlane::SPlane (const Vector3& inPosition, const Vector3& inNormal) :
	Position(inPosition)
{
	SetNormal(inNormal);
}

void Geometry3dUtils::SPlane::SetNormal (const Vector3& newNormal)
{
	if (newNormal.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot generate a Geometry3dUtils::SPlane object from a zero normal."));

		//Default to facing along X-axis instead.
		Normal = Vector3(1.f, 0.f, 0.f);
		return;
	}

	Normal = Vector3::Normalize(newNormal);
}

bool Geometry3dUtils::AreCollinear (const Vector3& a, const Vector3& b, const Vector3& c, Float tolerance)
{
	return ((a-b).CrossProduct(c-a).IsNearlyEqual(Vector3::ZERO_VECTOR, tolerance));
}

bool Geometry3dUtils::IsOnSegment (const Vector3& endPtA, const Vector3& endPtB, const Vector3& testPt, Float tolerance)
{
	//First test if they're collinear
	if (!AreCollinear(endPtA, endPtB, testPt, tolerance))
	{
		return false;
	}

	//Test distance sizes to see if it's beyond the length of the segment.
	if ((testPt - endPtA).CalcDistSquared() > (endPtB - endPtA).CalcDistSquared())
	{
		//Beyond length
		return false;
	}

	//Test that the test point is not the reversed direction of the vector.
	Vector3 multipliers = (testPt - endPtA) * (endPtB - endPtA);

	//If any axis is negative, then it assumes that the vector is facing the opposite direction.
	return (multipliers.X >= 0.f && multipliers.Y >= 0.f && multipliers.Z >= 0.f);
}

bool Geometry3dUtils::IsBetweenCollinearPts (const Vector3& endPtA, const Vector3& endPtB, const Vector3& testPt)
{
	//Assuming all three points are collinear, all we need to do is check their relative distances.
	Float segLengthSqrd = (endPtB - endPtA).CalcDistSquared();

	return ((testPt - endPtA).CalcDistSquared() <= segLengthSqrd && (testPt - endPtB).CalcDistSquared() <= segLengthSqrd);
}

Float Geometry3dUtils::CalcDistBetweenPointAndLine (const Vector3& linePt, const Vector3& lineDir, const Vector3& testPoint, bool bBoundedLine, Vector3& outIntersectingPoint)
{
	Float dist = CalcSquaredDistBetweenPointAndLine(linePt, lineDir, testPoint, bBoundedLine, OUT outIntersectingPoint);
	if (dist > 0.f)
	{
		dist.PowInline(0.5f);
	}

	return dist;
}

Float Geometry3dUtils::CalcSquaredDistBetweenPointAndLine (const Vector3& linePt, const Vector3& lineDir, const Vector3& testPoint, bool bBoundedLine, Vector3& outIntersectingPoint)
{
	if (lineDir.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate distance between a point and a line. Cannot define a line by a point (%s) and direction (%s) when the direction vector is zero."), linePt, lineDir);
		return -1.f;
	}

	//Project testPoint vector onto the line using the dot product.
	Vector3 normDir = Vector3::Normalize(lineDir);
	Float scalarProjection = (testPoint - linePt).Dot(normDir);

	if (bBoundedLine)
	{
		if (scalarProjection < 0.f)
		{
			outIntersectingPoint = linePt;

			//It's the reverse direction from lineDir. Return the distance between test point and the line's starting point.
			return (testPoint - linePt).CalcDistSquared();
		}
		else if (scalarProjection > 1.f)
		{
			Vector3 boundedDir(normDir * scalarProjection); //Assume unbounded lines
			if (boundedDir.CalcDistSquared() > lineDir.CalcDistSquared())
			{
				//lineDir must be a nonNormalized directional vector, and the scalar projection is beyond the lineDir's limits. Clamp it down to lineDir.
				boundedDir = lineDir;
			}
			
			outIntersectingPoint = (linePt + boundedDir);
			return (testPoint - outIntersectingPoint).CalcDistSquared();
		}
	}

	outIntersectingPoint = linePt + (normDir * scalarProjection);
	return (outIntersectingPoint - testPoint).CalcDistSquared();
}

/*
Although it's easier to solve using a system of equations. This function implements this answer: https://math.stackexchange.com/a/270991 since it's easier
to implement this solution in C++ rather than implementing a generic system of equations.
*/
bool Geometry3dUtils::IsIntersecting (const Vector3& ptA, const Vector3& dirA, const Vector3& ptB, const Vector3& dirB, bool bBoundedLines, Vector3& outIntersectingPoint, Float tolerance)
{
	Vector3 dirCrossProd(dirA.CrossProduct(dirB));
	//If the two lines are not parallel...
	if (!dirCrossProd.IsNearlyEqual(Vector3::ZERO_VECTOR, tolerance))
	{
		//The two non-parallel lines intersect only if the dot product of their cross product is zero.
		if (!dirCrossProd.Dot(ptA - ptB).IsCloseTo(0.f, tolerance))
		{
			return false;
		}
	}
	else
	{
		//Handle parallel lines
		if (!AreCollinear(ptA, ptA + dirA, ptB, tolerance))
		{
			//Parallel but they are not aligned.
			return false;
		}

		outIntersectingPoint = ptA;
		if (bBoundedLines)
		{
			Vector3 endPtA(ptA + dirA);
			Vector3 endPtB(ptB + dirB);
			if (IsOnSegment(ptA, endPtA, ptB, tolerance))
			{
				outIntersectingPoint = ptB;
				return true;
			}

			if (IsOnSegment(ptA, endPtA, endPtB, tolerance))
			{
				outIntersectingPoint = endPtB;
				return true;
			}

			if (IsOnSegment(ptB, endPtB, ptA, tolerance)) //Handles case where segment B completely encompasses segment A
			{
				outIntersectingPoint = ptA;
				return true;
			}

			return false;
		}

		return true;
	}

	Vector3 crossProdBetweenLines = (ptA.CrossProduct(ptB));

	//The dot product between the shared product and both points must be the same to consider intersecting.
	Float dotA = crossProdBetweenLines.Dot(ptA);
	Float dotB = crossProdBetweenLines.Dot(ptB);
	if (!dotA.IsCloseTo(dotB, tolerance))
	{
		//The two lines do not intersect
		return false;
	}

	Vector3 crossProd = dirA.CrossProduct(crossProdBetweenLines);
	Float constDotA = crossProd.Dot(ptA);

	//Find the point on the second line where the point on the line satisfies constant from the first line.
	//General point on second line is: ptB + (dirB * time)
	//Find time so that: crossProd dot (ptB + (dirB * time)) = constDotA
	Float crossDotPtB = crossProd.Dot(ptB);
	Float crossDotDirB = crossProd.Dot(dirB);
	Float time = (constDotA - crossDotPtB) / crossDotDirB;

	//Plug in time along the second line to find the point of intersection.
	outIntersectingPoint = ptB + (dirB * time);

	if (bBoundedLines)
	{
		//The time value is beyond the length of line B
		if (time < 0.f || time > 1.f)
		{
			return false;
		}

		//Check if the intersection point is beyond the length A
		//Formula: ptA + (dirA * time) = outIntersectingPoint. Solve for time
		size_t idx = 2; //Z
		if (dirA.X != 0.f)
		{
			idx = 0; //X-axis
		}
		else if (dirA.Y != 0.f)
		{
			idx = 1; //Y-axis
		}

		time = (outIntersectingPoint[idx] - ptA[idx]) / dirA[idx];
		return (time >= 0.f && time <= 1.f);
	}

	return true;
}

/*
Although it's easier to calculate via: Dist = Float::Abs[(DirA.CrossProduct(DirB).DotProduct(linePt2 - linePt1)] / (DirA.CrossProduct(DirB).VSize())]
The problem is that that algorithm doesn't describe where the shortest line resides.
To calculate the shortest line and the length of that shortest line, a different formula is used.
Function implemented is based on: https://developer.rhino3d.com/guides/rhinoscript/shortest-line/ 
*/
Float Geometry3dUtils::CalcDistBetweenLines (const Vector3& linePtA, const Vector3& lineDirA, const Vector3& linePtB, const Vector3& lineDirB, bool bBoundedLines, Vector3& outDistEndPtA, Vector3& outDistEndPtB)
{
	if (lineDirA.IsEmpty() || lineDirB.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate the distance between two lines without specifying a direction for both lines. Line A direction = %s, line B direction = %s."), lineDirA, lineDirB);
		return -1.f;
	}

	if (IsParallel(lineDirA, lineDirB))
	{
		//Run special case for parallel lines since there are infinite many 'shortest dist segments' connecting the two lines.
		Float shortestDist = CalcDistBetweenPointAndLine(linePtA, lineDirA, linePtB, false, OUT outDistEndPtA);
		outDistEndPtB = linePtB;

		if (bBoundedLines)
		{
			Vector3 deltaDistPt = outDistEndPtA - linePtA;
			Vector3 deltaDistDir = outDistEndPtA - (linePtA + lineDirA);

			Float dot = deltaDistPt.Dot(deltaDistDir);

			//If both dot products are facing the opposite directions (negative * positive), assume the dist end point resides within segment A
			if (dot < 0.f)
			{
				return shortestDist;
			}

			//Handle case where the point resides outside of segment A
			Vector3 prevDeltaDistPt = deltaDistPt;

			//Positive dot product implies both end points of segment A is facing the same direction. This means outDistEndPtA is out of range.
			//Jump ahead by length of segmentB
			outDistEndPtA += lineDirB;
			deltaDistPt += outDistEndPtA - linePtA;
			deltaDistDir += outDistEndPtA - (linePtA + lineDirA);
			dot = deltaDistPt.Dot(deltaDistDir);

			if (dot > 0.f) //If outDistEndPtA is still out of range from segment A.
			{
				Float dotPrevDot = deltaDistPt.Dot(prevDeltaDistPt);
				if (dotPrevDot > 0.f)
				{
					//Even after jumping the length of the segment B, all dot products are facing the same direction, which suggests that there is no segment that is perpendicular to both segments.
					//Simply retrieve the shortest segment that connects two end points.
					const Vector3 linePtC(linePtA + lineDirA);
					const Vector3 linePtD(linePtB + lineDirB);
					const Vector3* bestPtA = &linePtA;
					const Vector3* bestPtB = &linePtB;
					Float bestDist = (linePtA - linePtB).CalcDistSquared();
					Float dist = (linePtC - linePtB).CalcDistSquared();
					if (dist < bestDist)
					{
						bestPtA = &linePtC;
						bestPtB = &linePtB;
						bestDist = dist;
					}

					dist = (linePtA - linePtD).CalcDistSquared();
					if (dist < bestDist)
					{
						bestPtA = &linePtA;
						bestPtB = &linePtD;
						bestDist = dist;
					}

					dist = (linePtC - linePtD).CalcDistSquared();
					if (dist < bestDist)
					{
						bestPtA = &linePtC;
						bestPtB = &linePtD;
						bestDist = dist;
					}

					outDistEndPtA = *bestPtA;
					outDistEndPtB = *bestPtB;
					return Float::Pow(bestDist, 0.5f);
				}
				else
				{
					//Although outDistEndPtA is still beyond segment A's length, the end point is on the opposite side this time. This means segment A is shorter than segment B and it's within segment B.
					Vector3 deltaMove = (outDistEndPtA - linePtA);
					outDistEndPtA = linePtA;
					outDistEndPtB += deltaMove;
				}
			}	
		} //bBoundedLines

		return shortestDist;
	} //parallel lines

	//The dot product of the vector between linePtA, linePtB and vector lineDirB
	Float abDotB = (linePtA - linePtB).Dot(lineDirB);

	//The dot product of vector lineDirB and vector lineDirA
	Float BDotA = lineDirB.Dot(lineDirA);

	//The doct product of the vector linePtA, linePtB and vector lineDirA
	Float abDotA = (linePtA - linePtB).Dot(lineDirA);

	//The squared magnitude of the two directional vectors.
	Float squaredMagA = lineDirA.CalcDistSquared();
	Float squaredMagB = lineDirB.CalcDistSquared();

	Float denominator = (squaredMagA * squaredMagB) - Float::Pow(BDotA, 2.f);
	if (denominator == 0.f)
	{
		//This should never happen since the parallel lines case is handled earlier in this function.
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate the distance between two lines since the denominator is zero. Line A = Point(%s), Direction(%s). Line B = Point(%s), Direction(%s)."), linePtA, lineDirA, linePtB, lineDirB);
		return -1.f;
	}

	Float numerator = (abDotB * BDotA) - (abDotA * squaredMagB);

	//The "time-step" down line A
	Float ta = numerator / denominator;

	//The "time-step" down line B
	Float tb = (abDotB + (BDotA * ta)) / squaredMagB;

	outDistEndPtA = linePtA + (ta * lineDirA);
	outDistEndPtB = linePtB + (tb * lineDirB);

	//Check if the shortest line is out of bounds
	if (bBoundedLines)
	{
		Range<Float> limits(0.f, 1.f);
		if (!limits.ContainsValue(ta) || !limits.ContainsValue(tb))
		{
			//Since the shortest distance is beyond the length of one of the two lines, the shortest distance must connect to one of the end points.
			//Take the distance between the four points and return the shortest value.
			Float dist = CalcSquaredDistBetweenPointAndLine(linePtA, lineDirA, linePtB, true, OUT outDistEndPtA);
			Float bestDist = dist;
			outDistEndPtB = linePtB;
			
			Vector3 intersection;
			dist = CalcSquaredDistBetweenPointAndLine(linePtA, lineDirA, linePtB + lineDirB, true, OUT intersection);
			if (dist < bestDist)
			{
				bestDist = dist;
				outDistEndPtA = intersection;
				outDistEndPtB = (linePtB + lineDirB);
			}

			dist = CalcSquaredDistBetweenPointAndLine(linePtB, lineDirB, linePtA, true, OUT intersection);
			if (dist < bestDist)
			{
				bestDist = dist;
				outDistEndPtA = linePtA;
				outDistEndPtB = intersection;
			}

			dist = CalcSquaredDistBetweenPointAndLine(linePtB, lineDirB, linePtA + lineDirA, true, OUT intersection);
			if (dist < bestDist)
			{
				bestDist = dist;
				outDistEndPtA = (linePtA + lineDirA);
				outDistEndPtB = intersection;
			}

			bestDist.PowInline(0.5f);
			return bestDist;
		}
	}

	return (outDistEndPtA - outDistEndPtB).VSize();
}

//Function is constructed based on: https://math.stackexchange.com/questions/83990/line-and-plane-intersection-in-3d
bool Geometry3dUtils::CalcLinePlaneIntersection (const SPlane& plane, const Vector3& linePt, const Vector3& lineDir, bool bBoundedLine, Vector3& outIntersection)
{
	if (lineDir.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot calculate line-plane intersection when the directional vector for the line is empty. Line point = %s, Line Direction = %s."), linePt, lineDir);
		return false;
	}

	Vector3 normDir = Vector3::Normalize(lineDir);
	Float normDotDir = plane.ReadNormal().Dot(normDir);
	if (normDotDir.IsCloseTo(0.f))
	{
		//The line is parallel to the plane. Now it's either no intersection or infinite intersections.
		//The dot product of (Any point on the plane, minus its position point) is zero for the normal.
		if (plane.ReadNormal().Dot(linePt - plane.Position) == 0.f)
		{
			//Line resides on the plane
			outIntersection = linePt;
			return true;
		}

		return false;
	}

	Float dist = plane.ReadNormal().Dot(plane.Position); //Distance the plane is from the origin.
	Float normDotPt = plane.ReadNormal().Dot(linePt);
	outIntersection = linePt + ((dist - normDotPt) / normDotDir) * normDir;
	
	return (!bBoundedLine || IsBetweenCollinearPts(linePt, linePt + lineDir, outIntersection));
}

Vector3 Geometry3dUtils::ProjectLineOnPlane (const SPlane& plane, const Vector3& lineDir)
{
	return (lineDir - ((lineDir.Dot(plane.ReadNormal())) * plane.ReadNormal()));
}

Float Geometry3dUtils::CalcAngleBetweenLines (const Vector3& dirA, const Vector3& dirB)
{
	Float dot = dirA.Dot(dirB);
	Float denom = dirA.CalcDistSquared() * dirB.CalcDistSquared();

	if (denom == 0.f)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate angle between two lines when at least one of the directional vectors is a zero vector. DirA = %s, DirB = %s."), dirA, dirB);
		return 0.f;
	}
	else if (denom != 1.f) //If either directional vector is not normalized
	{
		denom.PowInline(0.5f); //Take the square root of the squared distances. These vectors must be normalized since arccos expects a range between -1 to 1.
	}

	return std::acosf((dot / denom).Value);
}

bool Geometry3dUtils::CalcSphereLineIntersections (const Vector3& sphereCenter, Float radius, const Vector3& linePt, const Vector3& lineDir, bool bBoundedLine, std::vector<Vector3>& outIntersections)
{
	if (lineDir.IsEmpty())
	{
		//This could only intersect if the point resides exactly on the sphere.
		if ((linePt - sphereCenter).CalcDistSquared().IsCloseTo(Float::Pow(radius, 2.f)))
		{
			outIntersections.push_back(linePt);
		}

		return !(ContainerUtils::IsEmpty(outIntersections));
	}

	Vector3 normDir(Vector3::Normalize(lineDir));

	//time along the segment that marks the closest point towards the center of the sphere.
	Float time = (sphereCenter - linePt).Dot(normDir);
	Vector3 closestPt = (normDir * time) + linePt;

	//x represents the distance between the closest point and the point where the segment meets the edge of the sphere
	//y represents the distance between closestPt and sphereCenter
	//Formula:  x = +-sqrt(radius^2 - y^2)
	Float sqrtBase = Float::Pow(radius, 2.f) - (sphereCenter - closestPt).CalcDistSquared();
	if (sqrtBase < 0.f)
	{
		//Negative numbers means y is greater than the radius (radius - y). This means the closestPoint is beyond the sphere's radius. No intersection.
		return false;
	}
	else if (sqrtBase.IsCloseTo(0.f))
	{
		//Closest point is right on the edge of the sphere. Only one point of intersection, and it's on the edge.
		if (!bBoundedLine || IsBetweenCollinearPts(linePt, lineDir + linePt, closestPt))
		{
			outIntersections.push_back(closestPt);
			return true;
		}

		return false;
	}

	Float intercept = Float::Pow(sqrtBase, 0.5f);
	Float dirSquared = lineDir.CalcDistSquared();

	//Check which direction. 'Sqrting' something requires to check the positive and negative side.
	if (!bBoundedLine || intercept < time) //If unbounded or within segment range (not 'behind' start position)
	{
		Vector3 intersection((normDir * (time - intercept)) + linePt);

		if (!bBoundedLine || (intersection - linePt).CalcDistSquared() <= lineDir.CalcDistSquared()) //If unbounded or within range reach of the segment
		{
			outIntersections.push_back(intersection);
		}
	}

	if (!bBoundedLine || Float::Pow(time + intercept, 2.f) < dirSquared) //If within segment range (direction's reach)
	{
		Vector3 intersection((normDir * (time + intercept)) + linePt);

		Vector3 endPt(lineDir + linePt);
		//If unbounded or NOT behind 'behind' the segment.
		if (!bBoundedLine || (intersection - endPt).CalcDistSquared() <= (linePt - endPt).CalcDistSquared())
		{
			outIntersections.push_back(intersection);
		}
	}
	
	return !ContainerUtils::IsEmpty(outIntersections);
}
SD_END