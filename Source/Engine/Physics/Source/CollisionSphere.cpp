/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionSphere.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"

SD_BEGIN
CollisionSphere::CollisionSphere () :
	CollisionShape(),
	Radius(1.f)
{
	Shape = ST_Sphere;
}

CollisionSphere::CollisionSphere (Float inScale, const Vector3& inPosition) :
	CollisionShape(inScale, inPosition),
	Radius(1.f)
{
	Shape = ST_Sphere;
}

CollisionSphere::CollisionSphere (Float inRadius) :
	CollisionShape(),
	Radius(inRadius)
{
	Shape = ST_Sphere;
}

CollisionSphere::CollisionSphere (Float inScale, const Vector3& inPosition, Float inRadius) :
	CollisionShape(inScale, inPosition),
	Radius(inRadius)
{
	Shape = ST_Sphere;
}

void CollisionSphere::RefreshBoundingBox ()
{
	BoundingBox.Center = Position;
	Float size = Scale * (Radius * 2.f);
	BoundingBox.Depth = size;
	BoundingBox.Width = size;
	BoundingBox.Height = size;
}

bool CollisionSphere::OverlapsWith (const CollisionShape* otherShape) const
{
	if (!CollisionShape::OverlapsWith(otherShape))
	{
		return false;
	}

	switch (otherShape->GetShape())
	{
		case(ST_Segment):
		{
			const CollisionSegment* otherSegment = dynamic_cast<const CollisionSegment*>(otherShape);
			CHECK(otherSegment != nullptr)
			return CollisionUtils::Overlaps(this, otherSegment);
		}

		case (ST_Sphere):
		{
			const CollisionSphere* otherSphere = dynamic_cast<const CollisionSphere*>(otherShape);
			CHECK(otherSphere != nullptr)
			return CollisionUtils::Overlaps(this, otherSphere);
		}

		case (ST_Capsule):
		{
			const CollisionCapsule* otherCapsule = dynamic_cast<const CollisionCapsule*>(otherShape);
			CHECK(otherCapsule != nullptr)
			return CollisionUtils::Overlaps(this, otherCapsule);
		}

		case (ST_ExtrudePolygon):
		{
			const CollisionExtrudePolygon* otherPoly = dynamic_cast<const CollisionExtrudePolygon*>(otherShape);
			CHECK(otherPoly != nullptr)
			return CollisionUtils::Overlaps(this, otherPoly);
		}
	}

	//Handle generic case
	Vector3 closestPoint = otherShape->CalcClosestPoint(Position);

	//If the closestPoint on the other shape is within the radius of this shape, then it's safe to assume it overlaps.
	return EncompassesPoint(closestPoint);
}

bool CollisionSphere::EncompassesPoint (const Vector3& point) const
{
	//Skip the Aabb bounds check. It's very fast to check distance instead.
	return ((point - Position).CalcDistSquared() <= std::pow((Radius * Scale).Value, 2.f));
}

Vector3 CollisionSphere::CalcClosestPoint (const Vector3& desiredPoint) const
{
	if (EncompassesPoint(desiredPoint))
	{
		return desiredPoint;
	}

	Vector3 dir = desiredPoint - Position;
	dir.NormalizeInline();

	return Position + (dir * (Radius * Scale));
}

void CollisionSphere::SetRadius (Float newRadius)
{
	Radius = newRadius;
	RefreshBoundingBox();
}
SD_END