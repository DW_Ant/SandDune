/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsUnitTest.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry2dUtils.h"
#include "Geometry3dUtils.h"
#include "QuadTree.h"
#include "QuadTreeInterface.h"
#include "PhysicsComponent.h"
#include "PhysicsUnitTest.h"
#include "PhysicsUtils.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD::PhysicsUnitTest, SD::UnitTester)
SD_BEGIN

QuadTreeTester::QuadTreeTester (Float width, Float height, Float depth, const Vector3& center) :
	BoundingBox(width, height, depth, center)
{
	//Noop
}

QuadTreeTester::QuadTreeTester (Aabb inBoundingBox) :
	BoundingBox(inBoundingBox)
{
	//Noop
}

const Aabb& QuadTreeTester::ReadBoundingBox () const
{
	return BoundingBox;
}

bool PhysicsUnitTest::RunTests (EUnitTestFlags testFlags) const
{
	bool result = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		result &= RunQuadTreeTest(testFlags) && RunGeometry2dUtils(testFlags) && RunGeometry3dUtils(testFlags) &&
			RunSegmentTests(testFlags) && RunSphereTests(testFlags) && RunCapsuleTests(testFlags) && RunExtrudePolygonTests(testFlags) &&
			RunCollisionUtilsTests(testFlags) && RunPhysicsUtilsTests(testFlags);
	}

	return result;
}

bool PhysicsUnitTest::RunQuadTreeTest (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Quad Tree"));

	QuadTree root(Vector2(0.f, 0.f), 512.f, nullptr);

	std::vector<QuadTreeTester*> rectList;
	//Rects added in the Back Left corner
	QuadTreeTester blSquareA(5.f, 5.f, 5.f, Vector3(-49.f, -49.f, 0.f));
	rectList.push_back(&blSquareA);
	QuadTreeTester blSquareB(4.f, 4.f, 4.f, Vector3(-50.f, -48.f, 0.f));
	rectList.push_back(&blSquareB);
	QuadTreeTester blSquareC(6.f, 6.f, 6.f, Vector3(-48.f, -49.f, 0.f));
	rectList.push_back(&blSquareC);
	QuadTreeTester blSquareD(3.f, 3.f, 3.f, Vector3(-51.f, -51.f, 0.f));
	rectList.push_back(&blSquareD);
	QuadTreeTester blSquareE(4.f, 4.f, 4.f, Vector3(-46.f, 47.f, 0.f));
	rectList.push_back(&blSquareE);
	QuadTreeTester blSquareF(blSquareE);
	rectList.push_back(&blSquareF);
	QuadTreeTester blRectA(2.f, 30.f, 10.f, Vector3(-45.f, -49.f, 0.f));
	rectList.push_back(&blRectA);
	QuadTreeTester blRectB(125.f, 1.f, 5.f, Vector3(-49.f, -50.f, 0.f));
	rectList.push_back(&blRectB);
	QuadTreeTester blRectC(64.f, 75.f, 5.f, Vector3(-45.f, -52.f, 0.f));
	rectList.push_back(&blRectC);

	//Rects added in the Forward Right corner
	QuadTreeTester frSquareA(8.f, 8.f, 8.f, Vector3(49.f, 49.f, 0.f));
	rectList.push_back(&frSquareA);
	QuadTreeTester frSquareB(24.f, 24.f, 24.f, Vector3(64.f, 55.f, 0.f));
	rectList.push_back(&frSquareB);
	QuadTreeTester frSquareC(4.f, 4.f, 4.f, Vector3(51.f, 51.f, 0.f));
	rectList.push_back(&frSquareC);
	QuadTreeTester frRectA(12.f, 32.f, 5.f, Vector3(50.f, 48.f, 0.f));
	rectList.push_back(&frRectA);

	//Rects around the Center
	QuadTreeTester cSquareA(5.f, 5.f, 5.f, Vector3(15.f, 15.f, 0.f));
	rectList.push_back(&cSquareA);
	QuadTreeTester cSquareB(2.f, 2.f, 2.f, Vector3(-5.f, -5.f, 0.f));
	rectList.push_back(&cSquareB);
	QuadTreeTester cSquareC(2.f, 2.f, 2.f, Vector3(5.f, 5.f, 0.f));
	rectList.push_back(&cSquareC);
	QuadTreeTester cSquareD(2.f, 2.f, 2.f, Vector3::ZERO_VECTOR);
	rectList.push_back(&cSquareD);

	//Rects added in the forward left corner
	QuadTreeTester flSquareA(5.f, 5.f, 5.f, Vector3(50.f, -49.f, 0.f));
	rectList.push_back(&flSquareA);

	SetTestCategory(testFlags, TXT("Insertion"));
	{
		for (QuadTreeTester* rect : rectList)
		{
			root.InsertObject(rect);
		}
	}
	CompleteTestCategory(testFlags);

	std::vector<QuadTreeTester*> expectedRects; //List of all rectangles that are expected to encompass the test point
	SetTestCategory(testFlags, TXT("Get Encompassing Objects"));
	{
		std::function<bool(const Vector3&)> testEncompassingPoint([&](const Vector3& testPoint)
		{
			//Ensure the expected list makes sense
			for (QuadTreeTester* expected : expectedRects)
			{
				if (!expected->ReadBoundingBox().EncompassesPoint(testPoint))
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. Invalid test detected. It's testing for encompassing points, but the expected Aabb %s does not encompass %s."), expected->ReadBoundingBox(), testPoint);
					return false;
				}
			}

			std::vector<QuadTreeInterface*> actualOverlapping;
			root.FindEncompassingObjects(testPoint, OUT actualOverlapping);
			for (QuadTreeInterface* found : actualOverlapping)
			{
				QuadTreeTester* treeTester = dynamic_cast<QuadTreeTester*>(found);
				if (treeTester == nullptr)
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. Obtaining the overlapping objects from the test quad tree returned an instance that's not a Quad Tree tester."));
					return false;
				}

				//Ensure it's a valid test
				if (!treeTester->ReadBoundingBox().EncompassesPoint(testPoint))
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. An Aabb %s is returned, but it doesn't encompass the point %s."), treeTester->ReadBoundingBox(), testPoint);
					return false;
				}

				size_t expectedIdx = ContainerUtils::FindInVector(expectedRects, treeTester);
				if (expectedIdx == UINT_INDEX_NONE)
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. Found a Aabb %s when it wasn't suppose to be found when testing against point %s."), treeTester->ReadBoundingBox(), testPoint);
					return false;
				}
			}

			//Ensure it found every expected Aabb
			for (QuadTreeTester* expected : expectedRects)
			{
				size_t foundIdx = 0;
				for ( ; foundIdx < actualOverlapping.size(); ++foundIdx)
				{
					QuadTreeTester* found = dynamic_cast<QuadTreeTester*>(actualOverlapping.at(foundIdx));
					if (found == expected)
					{
						break;
					}
				}

				if (foundIdx >= actualOverlapping.size())
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. Could not find expected Aabb %s when retrieving all Aabbs that encompasses %s."), expected->ReadBoundingBox(), testPoint);
					return false;
				}
			}

			return true;
		});

		//Generates a list of rects that should encompass the given point (uses linear list instead of the quad tree).
		std::function<void(const Vector3&)> populateExpectedList([&](const Vector3& testPoint)
		{
			ContainerUtils::Empty(OUT expectedRects);
			for (QuadTreeTester* rect : rectList)
			{
				if (rect->ReadBoundingBox().EncompassesPoint(testPoint))
				{
					expectedRects.push_back(rect);
				}
			}
		});

		Vector3 testPoint(Vector3::ZERO_VECTOR);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(16.f, -32.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(-50.f, -50.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(50.f, -50.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(50.f, 50.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(-50.f, 50.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(25.f, 25.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(-25.f, 25.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(25.f, -25.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}

		testPoint = Vector3(-25.f, -25.f, 0.f);
		populateExpectedList(testPoint);
		if (!testEncompassingPoint(testPoint))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Get Overlapping Objects"));
	{
		std::function<bool(const Aabb&)> testOverlappingRects([&](const Aabb& testRegion)
		{
			std::vector<QuadTreeInterface*> actualRects;
			root.FindOverlappingObjects(testRegion, OUT actualRects);

			//Ensure each found Aabb is in the expected list
			for (QuadTreeInterface* actual : actualRects)
			{
				size_t foundIdx = 0;
				QuadTreeTester* tester = dynamic_cast<QuadTreeTester*>(actual);
				for ( ; foundIdx < actualRects.size(); ++foundIdx)
				{
					if (tester == expectedRects.at(foundIdx))
					{
						break;
					}
				}

				if (foundIdx >= actualRects.size())
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. FindOverlappingAabbs found a Aabb %s when it was not suppose to when testing the region %s."), actual->ReadBoundingBox(), testRegion);
					return false;
				}
			}

			//ensure every expected Aabb is found.
			for (QuadTreeTester* expected : expectedRects)
			{
				size_t foundIdx = 0;
				for ( ; foundIdx < actualRects.size(); ++foundIdx)
				{
					QuadTreeTester* tester = dynamic_cast<QuadTreeTester*>(actualRects.at(foundIdx));
					if (tester == expected)
					{
						break;
					}
				}

				if (foundIdx >= actualRects.size())
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. FindOverlappingAabb did not find the expected %s when it was testing the region %s."), expected->ReadBoundingBox(), testRegion);
					return false;
				}
			}

			return true;
		});

		std::function<void(const Aabb&)> populateExpectedList([&](const Aabb& testRegion)
		{
			ContainerUtils::Empty(OUT expectedRects);
			for (QuadTreeTester* rect : rectList)
			{
				if (rect->ReadBoundingBox().Overlaps(testRegion))
				{
					expectedRects.push_back(rect);
				}
			}
		});

		Aabb testRegion(5.f, 5.f, 5.f, Vector3::ZERO_VECTOR);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Width = 100.f;
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Width = 5.f;
		testRegion.Depth = 100.f;
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Width = 15.f;
		testRegion.Depth = 10.f;
		testRegion.Center = Vector3(25.f, 25.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(-25.f, 25.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(-25.f, -25.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(25.f, -25.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Width = 15.f;
		testRegion.Depth = 5.f;
		testRegion.Center = Vector3(60.f, 60.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(-60.f, 60.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(-60.f, -60.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(60.f, -60.f, 0.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Width = 500.f;
		testRegion.Depth = 500.f;
		testRegion.Center = Vector3::ZERO_VECTOR;
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Center = Vector3(0.f, 0.f, 1000.f);
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}

		testRegion.Height = 5000.f;
		populateExpectedList(testRegion);
		if (!testOverlappingRects(testRegion))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Moving Object"));
	{
		QuadTreeTester testRegion(0.f, 0.f, 0.f, Vector3::ZERO_VECTOR);
		QuadTree* curNode;
		//Searches through the tree and returns the node the Aabb suppose to reside in. Returns nullptr if the node doesn't exist.
		std::function<QuadTree*()> findExpectedSubNode([&]() -> QuadTree*
		{
			Vector2 regionCenter;
			QuadTree* node = &root;
			while (node != nullptr)
			{
				int subRegionIdx = node->GetSubRegion(testRegion.ReadBoundingBox(), OUT regionCenter);
				if (subRegionIdx < 0 || node->SubNodes[subRegionIdx] == nullptr)
				{
					return node;
				}

				node = node->SubNodes[subRegionIdx];
			}

			return nullptr;
		});

		std::function<bool()> testMoveAabb([&]()
		{
			if (curNode == nullptr)
			{
				UnitTestError(testFlags, TXT("Quad Tree test failed. An Aabb should always belong in a quad tree regardless where it moves."));
				return false;
			}

			Vector2 prevNodeCenter(curNode->GetCenter());
			QuadTree* newNode;
			if (!curNode->MoveObject(&testRegion, OUT newNode))
			{
				UnitTestError(testFlags, TXT("Quad Tree test failed. Could not move the Aabb within the Quad Tree. The test region was not found in the current node."));
				return false;
			}
			curNode = newNode;

			QuadTree* expectedNode = findExpectedSubNode();
			if (curNode != expectedNode)
			{
				UnitTestError(testFlags, TXT("Quad Tree test failed. After moving the Aabb within the Quad Tree, the resulting node does not match the expected node."));
				return false;
			}

			//Ensure the new node contains the region
			size_t findIdx = 0;
			for ( ; findIdx < curNode->ReadObjects().size(); ++findIdx)
			{
				QuadTreeTester* tester = dynamic_cast<QuadTreeTester*>(curNode->ReadObjects().at(findIdx));
				if (tester == &testRegion)
				{
					break;
				}
			}

			if (findIdx >= curNode->ReadObjects().size())
			{
				UnitTestError(testFlags, TXT("Quad Tree test failed. After moving a Aabb within a QuadTree, the expected new node does not contain the Aabb."));
				return false;
			}

			//It's possible that the previous node collapsed. Step into the tree until we can't any further or until we found a node with matching center.
			Aabb prevCenterBox(0.1f, 0.1f, 0.1f, prevNodeCenter.ToVector3());
			QuadTree* subNode = &root;
			while (subNode != nullptr)
			{
				Vector2 unused;
				int subNodeIdx = subNode->GetSubRegion(prevCenterBox, OUT unused);

				//Either the center does not fit in sub region, or the sub region doesn't exist yet (due to leaf), or the centers already match (suggesting we're already in the prev node).
				if (subNodeIdx < 0 || subNode->SubNodes[subNodeIdx] == nullptr || curNode->GetCenter() == prevNodeCenter)
				{
					if (subNode != curNode)
					{
						//Verify that the previous node no longer has a reference to the Aabb.
						size_t oldFindIdx = 0;
						for ( ; oldFindIdx < subNode->ReadObjects().size(); ++oldFindIdx)
						{
							QuadTreeTester* tester = dynamic_cast<QuadTreeTester*>(subNode->ReadObjects().at(oldFindIdx));
							if (tester == &testRegion)
							{
								break;
							}
						}

						if (oldFindIdx < subNode->ReadObjects().size())
						{
							UnitTestError(testFlags, TXT("Quad Tree test failed. After moving a Aabb within a QuadTree to a new node, the old node still has a reference to the Aabb at index %s."), Int(oldFindIdx));
							return false;
						}
					}

					break;
				}

				subNode = subNode->SubNodes[subNodeIdx];
			}

			//Ensure the new node encompasses the Aabb. Test against its center and all eight corners.
			const Aabb& testRegionBox = testRegion.ReadBoundingBox();
			std::vector<Vector3> testPoints(
			{
				testRegionBox.Center,
				Vector3(testRegionBox.GetForward(), testRegionBox.GetRight(), testRegionBox.GetUp()),
				Vector3(testRegionBox.GetBackward(), testRegionBox.GetRight(), testRegionBox.GetUp()),
				Vector3(testRegionBox.GetForward(), testRegionBox.GetLeft(), testRegionBox.GetUp()),
				Vector3(testRegionBox.GetBackward(), testRegionBox.GetLeft(), testRegionBox.GetUp()),
				Vector3(testRegionBox.GetForward(), testRegionBox.GetRight(), testRegionBox.GetDown()),
				Vector3(testRegionBox.GetBackward(), testRegionBox.GetRight(), testRegionBox.GetDown()),
				Vector3(testRegionBox.GetForward(), testRegionBox.GetLeft(), testRegionBox.GetDown()),
				Vector3(testRegionBox.GetBackward(), testRegionBox.GetLeft(), testRegionBox.GetDown())
			});

			std::vector<QuadTreeInterface*> encompassedAabbs;
			for (const Vector3& testPt : testPoints)
			{
				ContainerUtils::Empty(OUT encompassedAabbs);
				curNode->FindEncompassingObjects(testPt, OUT encompassedAabbs);
				findIdx = 0;
				for ( ; findIdx < encompassedAabbs.size(); ++findIdx)
				{
					QuadTreeTester* tester = dynamic_cast<QuadTreeTester*>(encompassedAabbs.at(findIdx));
					if (tester == &testRegion)
					{
						break;
					}
				}

				if (findIdx >= encompassedAabbs.size())
				{
					UnitTestError(testFlags, TXT("Quad Tree test failed. After moving an Aabb, the new node does not completely encompasses the Aabb. It doesn't encompass point %s."), testPt);
					return false;
				}
			}

			return true;
		});

		testRegion = Aabb(5.f, 5.f, 5.f, Vector3::ZERO_VECTOR);
		root.InsertObject(&testRegion);
		curNode = &root;

		testRegion.BoundingBox.Center = Vector3(20.f, 20.f, 0.f);
		if (!testMoveAabb())
		{
			return false;
		}

		testRegion.BoundingBox.Center = Vector3(40.f, 20.f, 0.f);
		if (!testMoveAabb())
		{
			return false;
		}

		Vector3 newCenter = Vector3(100.f, 40.f, 0.f);
		for (Float i = 0.f; i < 20.f; i += 1.f)
		{
			newCenter.X -= 10.f;
			newCenter.Z = i * 2.f;
			testRegion.BoundingBox.Center = newCenter;
			if (!testMoveAabb())
			{
				return false;
			}
		}

		testRegion.BoundingBox.Center = Vector3(45.f, -30.f, 10.f);
		if (!testMoveAabb())
		{
			return false;
		}

		testRegion.BoundingBox.Center = Vector3(45.f, -32.f, 10.f);
		if (!testMoveAabb())
		{
			return false;
		}

		//test big jump
		testRegion.BoundingBox.Center = Vector3(90.f, 68.f, 80.f);
		if (!testMoveAabb())
		{
			return false;
		}

		//Test small jump
		testRegion.BoundingBox.Center = Vector3(90.5f, 68.f, 80.f);
		if (!testMoveAabb())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Removal"));
	{
		for (QuadTreeInterface* rect : rectList)
		{
			if (!root.RemoveObject(rect))
			{
				UnitTestError(testFlags, TXT("Quad Tree test failed. Failed to remove Aabb %s from the Quad tree."), rect->ReadBoundingBox());
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Quad Tree"));
	return true;
}

bool PhysicsUnitTest::RunGeometry2dUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Geometry 2D Utils"));
	Float tolerance = 0.00001f;

	//Compares the two intersections and returns true if all relevant variables are within tolerance.
	std::function<bool(const Geometry2dUtils::SIntersectResults&, const Geometry2dUtils::SIntersectResults&)> compareIntersections([tolerance](const Geometry2dUtils::SIntersectResults& a, const Geometry2dUtils::SIntersectResults& b)
	{
		if (a.IntersectionType != b.IntersectionType)
		{
			return false;
		}

		switch (a.IntersectionType)
		{
			case(Geometry2dUtils::IT_None): return true; //No points to compare
			case(Geometry2dUtils::IT_Point):
			{
				return ((a.Pt1 - b.Pt1).VSize() <= tolerance);
			}

			case(Geometry2dUtils::IT_Segment):
			{
				//End points must be exact, but the ordering could be any order
				if ((a.Pt1 - b.Pt1).VSize() <= tolerance)
				{
					return ((a.Pt2 - b.Pt2).VSize() <= tolerance);
				}
				else if ((a.Pt1 - b.Pt2).VSize() <= tolerance)
				{
					return ((a.Pt2 - b.Pt1).VSize() <= tolerance);
				}
				else
				{
					return false;
				}
			}

			case(Geometry2dUtils::IT_Ray):
			{
				//First point must be exact since that's the ray's starting point. But the second point must reside somewhere in the ray.
				if ((a.Pt1 - b.Pt1).VSize() > tolerance)
				{
					return false;
				}

				Vector2 aDir = Vector2::Normalize(a.Pt2 - a.Pt1);
				Vector2 bDir = Vector2::Normalize(b.Pt2 - b.Pt1);
				return ((aDir - bDir).VSize() <= tolerance);
			}

			case(Geometry2dUtils::IT_Line):
			{
				//All four points must reside on the same line.

				//Check for direction
				Vector2 aDir = Vector2::Normalize(a.Pt2 - a.Pt1);
				Vector2 bDir = Vector2::Normalize(b.Pt2 - b.Pt1);
				if ((aDir + bDir).IsNearlyEqual(Vector2::ZERO_VECTOR, tolerance))
				{
					//Vectors are facing the opposite directions. Reverse direction for one of them for easier comparisons
					aDir *= -1.f;
				}

				if ((aDir - bDir).VSize() > tolerance)
				{
					return false;
				}

				//Ensure the translation between both lines are the same
				if (aDir.Y == 1.f || aDir.Y == -1.f)
				{
					//Handle vertical lines
					return (a.Pt1.X == b.Pt1.X);
				}
				else
				{
					Float aSlope = (a.Pt1.Y - a.Pt2.Y) / (a.Pt1.X - a.Pt2.X);
					Float bSlope = (b.Pt1.Y - b.Pt2.Y) / (b.Pt1.X - b.Pt2.X);
					if (Float::Abs(aSlope - bSlope) > tolerance)
					{
						//This should never happen since we checked for direction earlier
						return false;
					}

					//Point slope form: (y - y1) = m(x - x1)
					//If the lines are equal, they should cross the y intercept at the same place. Solve for y when x is 0.
					//y = -mx1 + y1
					Float yInterA = (-aSlope * a.Pt1.X) + a.Pt1.Y;
					Float yInterB = (-bSlope * b.Pt1.X) + b.Pt1.Y;
					return (Float::Abs(yInterA - yInterB) <= tolerance);
				}
			}

			//Planes are not implemented
		}

		return true;
	});

	//Returns a string that prints out the given intersection struct.
	std::function<DString(const Geometry2dUtils::SIntersectResults&)> intersectionToString([](const Geometry2dUtils::SIntersectResults& intersection)
	{
		DString result(TXT("IntersectionType="));
		switch(intersection.IntersectionType)
		{
			case(Geometry2dUtils::IT_None):
				result += TXT("None");
				return result;

			case(Geometry2dUtils::IT_Point):
				result += TXT("Point");
				break;

			case(Geometry2dUtils::IT_Segment):
				result += TXT("Segment");
				break;

			case(Geometry2dUtils::IT_Ray):
				result += TXT("Ray");
				break;

			case(Geometry2dUtils::IT_Line):
				result += TXT("Line");
				break;

			case(Geometry2dUtils::IT_Plane):
				result += TXT("Plane");
				return result;
		}

		result += TXT(" at ") + intersection.Pt1.ToString();
		if (intersection.IntersectionType == Geometry2dUtils::IT_Point)
		{
			return result;
		}

		result += TXT(" and ") + intersection.Pt2.ToString();
		return result;
	});

	SetTestCategory(testFlags, TXT("Points Collinear"));
	{
		Vector2 a;
		Vector2 b;
		Vector2 c;
		bool expected;
		std::function<bool()> testCollinear([&]()
		{
			bool actual = Geometry2dUtils::ArePointsCollinear(a, b, c);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. The following points %s, %s, %s should %s be collinear. The function suggests otherwise."), a, b, c, conditionalStr);
				return false;
			}

			bool orderA = (Geometry2dUtils::ArePointsCollinear(a, c, b));
			bool orderB = Geometry2dUtils::ArePointsCollinear(b, a, c);
			bool orderC = Geometry2dUtils::ArePointsCollinear(b, c, a);
			bool orderD = Geometry2dUtils::ArePointsCollinear(c, a, b);
			bool orderE = Geometry2dUtils::ArePointsCollinear(c, b, a);
			if (actual != orderA || actual != orderB || actual != orderC || actual != orderD || actual != orderE)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating if the points %s, %s, %s should be collinear or not should be consistent regardless of the order they're passed into the function."), a, b, c);
				return false;
			}

			return true;
		});

		a = Vector2(0.f, 0.f);
		b = Vector2(-1.f, 0.f);
		c = Vector2(1.f, 0.f);
		expected = true;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector2(1.f, 0.f);
		b = Vector2(1.f, 1.f);
		c = Vector2(1.f, -1.f);
		if (!testCollinear())
		{
			return false;
		}

		a = Vector2(5.f, 6.f);
		b = Vector2(6.f, 7.f);
		c = Vector2(7.f, 8.f);
		if (!testCollinear())
		{
			return false;
		}

		a = Vector2(-4.f, -2.f);
		b = Vector2(-6.5f, -1.f);
		c = Vector2(-9.f, 0.f);
		if (!testCollinear())
		{
			return false;
		}

		c.Y += 1.f;
		expected = false;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector2(-1.f, 1.f);
		b = Vector2(0.f, 2.f);
		c = Vector2(1.f, 1.f);
		if (!testCollinear())
		{
			return false;
		}

		a = Vector2(50.f, -45.f);
		b = a;
		c = a;
		expected = true;
		if (!testCollinear())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Is On Segment"));
	{
		Vector2 segPtA;
		Vector2 segPtB;
		Vector2 testPt;
		bool expected;
		std::function<bool()> testIsOnSegment([&]()
		{
			bool actual = Geometry2dUtils::IsOnSegment(segPtA, segPtB, testPt);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When checking if the point %s resides on the segment [%s, %s], the point should %s be considered inside the segment. The function suggests otherwise."), testPt, segPtA, segPtB, conditionalStr);
				return false;
			}

			if (Geometry2dUtils::ArePointsCollinear(segPtA, segPtB, testPt, 0.00001f))
			{
				//Run extra collinear test to ensure it's consistent with the IsOnSegment.
				bool bCollinearTest = Geometry2dUtils::IsBetweenCollinearPts(segPtA, segPtB, testPt);
				if (actual != bCollinearTest)
				{
					UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. The IsBetweenCollinearPts test returned different results when testing if %s resides on the segment [%s, %s]. IsOnSegment returned %s. IsBetweenCollinearPts returned %s."), testPt, segPtA, segPtB, Bool(actual), Bool(bCollinearTest));
					return false;
				}
			}

			return true;
		});

		segPtA = Vector2(4.f, 8.f);
		segPtB = Vector2(8.f, 8.f);
		testPt = Vector2(6.f, 8.f);
		expected = true;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(3.f, 8.f);
		expected = false;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(10.f, 8.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(6.f, 7.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(6.f, 9.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		segPtA = Vector2(-4.f, -2.f);
		segPtB = Vector2(-4.f, 2.f);
		testPt = Vector2(-4.f, -3.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(-4.f, 1.f);
		expected = true;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(-4.f, 3.f);
		expected = false;
		if (!testIsOnSegment())
		{
			return false;
		}

		segPtA = Vector2(8.f, -4.f);
		segPtB = Vector2(-6.f, 0.f);
		testPt = Vector2(14.f, -8.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = segPtA;
		expected = true;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(4.5f, -3.f);
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt.X += 0.1f;
		expected = false;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = segPtB;
		expected = true;
		if (!testIsOnSegment())
		{
			return false;
		}

		testPt = Vector2(-20.f, 4.f);
		expected = false;
		if (!testIsOnSegment())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Closest Point"));
	{
		Vector2 linePt1;
		Vector2 linePt2;
		Vector2 testPt;
		Vector2 expected;
		std::function<bool()> testClosestPtLine([&]()
		{
			Vector2 actual = Geometry2dUtils::CalcClosestPointOnLine(testPt, linePt1, linePt2);
			if ((actual - expected).VSize() > tolerance)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating the closest point to %s on a line that intersects points %s and %s should have returned %s. Instead it returned %s."), testPt, linePt1, linePt2, expected, actual);
				return false;
			}

			return true;
		});

		std::function<bool()> testClosestPtSegment([&]()
		{
			Vector2 actual = Geometry2dUtils::CalcClosestPointOnSegment(testPt, linePt1, linePt2);
			if ((actual - expected).VSize() > tolerance)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating the closest point to %s on a segment with end points %s and %s should have returned %s. Instead it returned %s."), testPt, linePt1, linePt2, expected, actual);
				return false;
			}

			return true;
		});

		//Simple test cases on vertical line
		linePt1 = Vector2(2.f, 4.f);
		linePt2 = Vector2(2.f, 8.f);
		testPt = Vector2(2.f, 5.f);
		expected = testPt;
		if (!testClosestPtLine() || !testClosestPtSegment())
		{
			return false;
		}

		testPt = Vector2(3.f, 4.f);
		expected = Vector2(2.f, 4.f);
		if (!testClosestPtLine() || !testClosestPtSegment())
		{
			return false;
		}

		testPt = Vector2(800.f, 10.f);
		expected = Vector2(2.f, 10.f);
		if (!testClosestPtLine())
		{
			return false;
		}

		expected = Vector2(2.f, 8.f);
		if (!testClosestPtSegment())
		{
			return false;
		}

		testPt = Vector2(-40.f, -1.f);
		expected = Vector2(2.f, -1.f);
		if (!testClosestPtLine())
		{
			return false;
		}

		expected = Vector2(2.f, 4.f);
		if (!testClosestPtSegment())
		{
			return false;
		}

		//test against a sloped line
		linePt1 = Vector2(8.f, -2.f);
		linePt2 = Vector2(9.f, -4.f);
		testPt = Vector2(8.5f, -3.f);
		expected = testPt;
		if (!testClosestPtLine() || !testClosestPtSegment())
		{
			return false;
		}

		testPt = Vector2(2.f, -10.f);
		expected = Vector2(10.f, -6.f);
		if (!testClosestPtLine())
		{
			return false;
		}

		expected = linePt2;
		if (!testClosestPtSegment())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Distance"));
	{
		Vector2 linePt1;
		Vector2 linePt2;
		Vector2 testPt;
		Float expectedDist;
		std::function<bool()> testDistToLine([&]()
		{
			Float actualDist = Geometry2dUtils::CalcDistToLine(testPt, linePt1, linePt2);
			if (!actualDist.IsCloseTo(expectedDist, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating the distance between point %s and the line that intersects %s and %s should have returned %s. Instead it calculated %s."), testPt, linePt1, linePt2, expectedDist, actualDist);
				return false;
			}

			return true;
		});

		std::function<bool()> testDistToSegment([&]()
		{
			Float actualDist = Geometry2dUtils::CalcDistToSegment(testPt, linePt1, linePt2);
			if (!actualDist.IsCloseTo(expectedDist, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating the distance between point %s and the segment with end points %s and %s should have returned %s. Instead it calculated %s."), testPt, linePt1, linePt2, expectedDist, actualDist);
				return false;
			}

			return true;
		});

		//Run simple tests along a horizontal line
		linePt1 = Vector2::ZERO_VECTOR;
		linePt2 = Vector2(5.f, 0.f);
		testPt = Vector2::ZERO_VECTOR;
		expectedDist = 0.f;
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt.X = 2.5f;
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt = linePt2;
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(-2.f, 0.f);
		if (!testDistToLine())
		{
			return false;
		}

		expectedDist = 2.f;
		if (!testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(7.f, 0.f);
		expectedDist = 0.f;
		if (!testDistToLine())
		{
			return false;
		}

		expectedDist = 2.f;
		if (!testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(0.f, 2.f);
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(4.f, -2.f);
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(50.f, 40.f);
		expectedDist = 40.f;
		if (!testDistToLine())
		{
			return false;
		}

		expectedDist = std::sqrtf(3625.f);
		if (!testDistToSegment())
		{
			return false;
		}

		//Run tests on an angled line
		//y=0.25x+3.25
		linePt1 = Vector2(-5.f, 2.f);
		linePt2 = Vector2(-1.f, 3.f);
		testPt = Vector2(-3.f, 2.5f);
		expectedDist = 0.f;
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt = Vector2(-2.375f, 0.f); //forms the perpendicular line: -4x-9.5 that intersects at point (-3, 2.5)
		expectedDist = std::sqrtf(6.640625f);
		if (!testDistToLine() || !testDistToSegment())
		{
			return false;
		}

		testPt += Vector2(40.f, 10.f); //Shift up the line parallel to the test line. Testing bounds for segment
		if (!testDistToLine())
		{
			return false;
		}

		expectedDist = std::sqrtf(1540.890625f); //dist from testPt to linePt2.
		if (!testDistToSegment())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	//Test line-to-line intersection first since all of the other intersection functions depends on it.
	SetTestCategory(testFlags, TXT("Line-Line Intersection"));
	{
		Vector2 line1PtA;
		Vector2 line1PtB;
		Vector2 line2PtA;
		Vector2 line2PtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testLineIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcLineIntersection(line1PtA, line1PtB, line2PtA, line2PtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. Calculating the intersection between the lines [%s:%s] and [%s:%s] should have resulted in %s. Instead it's %s."), line1PtA, line1PtB, line2PtA, line2PtB, intersectionToString(expected), intersectionToString(actual));
				return false;
			}

			return true;
		});

		//Simple cases, axis aligned lines
		line1PtA = Vector2(5.f, 5.f);
		line1PtB = Vector2(10.f, 5.f);
		line2PtA = Vector2(-30.f, -30.f);
		line2PtB = Vector2(-30.f, 30.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(-30.f, 5.f);
		if (!testLineIntersection())
		{
			return false;
		}

		line2PtA = Vector2(2.f, 5.f);
		line2PtB = Vector2(-100.f, 5.f);
		expected.IntersectionType = Geometry2dUtils::IT_Line;
		expected.Pt1 = Vector2(0.f, 5.f);
		expected.Pt2 = Vector2(100.f, 5.f);
		if (!testLineIntersection())
		{
			return false;
		}

		line2PtA = Vector2(2.f, 6.f);
		line2PtB = Vector2(-50.f, 6.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineIntersection())
		{
			return false;
		}

		//LineA = 0.5x+8
		//LineB = -3x+1
		line1PtA = Vector2(0.f, 8.f);
		line1PtB = Vector2(8.f, 12.f);
		line2PtA = Vector2(3.f, -8.f);
		line2PtB = Vector2(13.f, -38.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(-2.f, 7.f);
		if (!testLineIntersection())
		{
			return false;
		}

		//LineB = 0.5x+8
		line2PtA = Vector2(-16.f, 0.f);
		line2PtB = Vector2(-28.f, -6.f);
		expected.IntersectionType = Geometry2dUtils::IT_Line;
		expected.Pt1 = Vector2(-48.f, -16.f);
		expected.Pt2 = Vector2(400.f, 208.f);
		if (!testLineIntersection())
		{
			return false;
		}

		//LineB = 0.5x+7
		line2PtA.Y -= 1.f;
		line2PtB.Y -= 1.f;
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Segment-Segment Intersection"));
	{
		Vector2 seg1PtA;
		Vector2 seg1PtB;
		Vector2 seg2PtA;
		Vector2 seg2PtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testSegmentIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcSegmentIntersection(seg1PtA, seg1PtB, seg2PtA, seg2PtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. Calculating the intersection between segments [%s:%s] and [%s:%s] should have resulted in %s. Instead it computed %s."), seg1PtA, seg1PtB, seg2PtA, seg2PtB, intersectionToString(expected), intersectionToString(actual));
				return false;
			}

			return true;
		});

		//Segments that are axis aligned
		seg1PtA = Vector2(1.f, 1.f);
		seg1PtB = Vector2(5.f, 1.f);
		seg2PtA = Vector2(3.f, 10.f);
		seg2PtB = Vector2(3.f, -0.5f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(3.f, 1.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Make sure each segment end points are checked
		Vector2 delta(-10.f, 0.f);
		seg1PtA -= delta;
		seg1PtB -= delta;
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg1PtA += (delta * 2.f);
		seg1PtB += (delta * 2.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg1PtA -= delta;
		seg1PtB -= delta;
		delta = Vector2(0.f, 20.f);
		seg2PtA -= delta;
		seg2PtB -= delta;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA += (delta * 2.f);
		seg2PtB += (delta * 2.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Test parallel segments
		//One segment encompassing the other
		seg1PtA = Vector2::ZERO_VECTOR;
		seg1PtB = Vector2(0.f, 5.f);
		seg2PtA = Vector2(0.f, -10.f);
		seg2PtB = Vector2(0.f, 10.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = seg1PtA;
		expected.Pt2 = seg1PtB;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA = Vector2(0.f, 1.f);
		seg2PtB = Vector2(0.f, 1.5f);
		expected.Pt1 = seg2PtA;
		expected.Pt2 = seg2PtB;
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Partially overlapping
		seg2PtA = Vector2(0.f, -3.f);
		seg2PtB = Vector2(0.f, 3.f);
		expected.Pt1 = seg1PtA;
		expected.Pt2 = seg2PtB;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA = Vector2(0.f, 2.f);
		seg2PtB = Vector2(0.f, 10.f);
		expected.Pt1 = seg2PtA;
		expected.Pt2 = seg1PtB;
		if (!testSegmentIntersection())
		{
			return false;
		}

		//On same line, but no overlap
		seg2PtA = Vector2(0.f, -10.f);
		seg2PtB = Vector2(0.f, -5.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA = Vector2(0.f, 30.f);
		seg2PtB = Vector2(0.f, 45.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Parallel but not overlapping
		seg2PtA = Vector2(4.f, -2.f);
		seg2PtB = Vector2(4.f, 15.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Quick vertical line test
		seg1PtA = Vector2(4.f, 5.f);
		seg1PtB = Vector2(4.f, -5.f);
		seg2PtA = Vector2(4.f, 10.f);
		seg2PtB = Vector2(4.f, 0.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = Vector2(4.f, 5.f);
		expected.Pt2 = Vector2(4.f, 0.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Test angled segments
		//Segment1=0.1x-5
		//Segment2=0.2x-6
		seg1PtA = Vector2(9.f, -4.1f);
		seg1PtB = Vector2(12.f, -3.8f);
		seg2PtA = Vector2(9.f, -4.2f);
		seg2PtB = Vector2(11.f, -3.8f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(10.f, -4.f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Shift segments up and down the lines to test bounds
		seg1PtB = expected.Pt1;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg1PtA = Vector2(8.f, -4.2f);
		seg1PtB = Vector2(9.f, -4.1f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg1PtA = Vector2(11.f, -3.9f);
		seg1PtB = Vector2(13.f, -3.7f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg1PtA = Vector2(9.f, -4.1f);
		seg1PtB = Vector2(12.f, -3.8f);
		seg2PtA = Vector2(6.f, -4.8f);
		seg2PtB = Vector2(8.f, -4.4f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA = Vector2(11.f, -3.8f);
		seg2PtB = Vector2(12.f, -3.6f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		//Overlap the angled segments
		seg2PtA = Vector2(8.f, -4.2f);
		seg2PtB = Vector2(11.f, -3.9f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = Vector2(9.f, -4.1f);
		expected.Pt2 = Vector2(11.f, -3.9f);
		if (!testSegmentIntersection())
		{
			return false;
		}

		seg2PtA = Vector2(6.f, -4.4f);
		seg2PtB = Vector2(7.f, -4.3f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testSegmentIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Ray-Ray Intersection"));
	{
		Vector2 ray1PtA;
		Vector2 ray1PtB;
		Vector2 ray2PtA;
		Vector2 ray2PtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testRayIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcRayIntersection(ray1PtA, ray1PtB, ray2PtA, ray2PtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. Calculating the intersection between rays [%s:%s] and [%s:%s] should have resulted in %s. Instead it computed %s."), ray1PtA, ray1PtB, ray2PtA, ray2PtB, intersectionToString(expected), intersectionToString(actual));
				return false;
			}

			return true;
		});

		//Axis aligned rays
		ray1PtA = Vector2(1.f, 0.f);
		ray1PtB = Vector2(5.f, 0.f);
		ray2PtA = Vector2(0.f, 1.f);
		ray2PtB = Vector2(0.f, 5.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray1PtA, ray1PtB);
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray2PtA, ray2PtB);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2::ZERO_VECTOR;
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray1PtA, ray1PtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRayIntersection())
		{
			return false;
		}

		//Rays on the same line
		//y=2x+4
		ray1PtA = Vector2(2.f, 8.f);
		ray1PtB = Vector2(4.f, 12.f);
		ray2PtA = Vector2(3.f, 10.f);
		ray2PtB = Vector2(5.f, 14.f);
		expected.IntersectionType = Geometry2dUtils::IT_Ray;
		expected.Pt1 = Vector2(3.f, 10.f);
		expected.Pt2 = Vector2(6.f, 16.f);
		if (!testRayIntersection())
		{
			return false;
		}

		ray2PtA = Vector2(1.f, 6.f);
		ray2PtB = Vector2(3.f, 10.f);
		expected.Pt1 = Vector2(2.f, 8.f);
		if (!testRayIntersection())
		{
			return false;
		}

		ray2PtA = Vector2(0.f, 4.f);
		ray2PtB = Vector2(1.f, 6.f);
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray2PtA, ray2PtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray1PtA, ray1PtB);
		expected.IntersectionType = Geometry2dUtils::IT_Ray;
		expected.Pt1 = Vector2(1.f, 6.f);
		expected.Pt2 = Vector2(-2.f, 0.f);
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray2PtA, ray2PtB);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = Vector2(0.f, 4.f);
		expected.Pt2 = Vector2(4.f, 12.f);
		if (!testRayIntersection())
		{
			return false;
		}

		//Test sloped rays intersecting at a single point
		//ray1=0.5x+3
		//ray2=0.4x+2
		ray1PtA = Vector2(-12.5f, -3.f);
		ray1PtB = Vector2(-11.f, -2.4f);
		ray2PtA = Vector2(-12.f, -3.f);
		ray2PtB = Vector2(-10.6f, -2.3f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(-10.f, -2.f);
		if (!testRayIntersection())
		{
			return false;
		}

		std::swap(ray1PtA, ray1PtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRayIntersection())
		{
			return false;
		}

		ray1PtA = Vector2(-9.f, -1.6f);
		ray1PtB = Vector2(-9.5f, -1.8f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(-10.f, -2.f);
		if (!testRayIntersection())
		{
			return false;
		}

		ray2PtA = Vector2(-9.f, -1.5f);
		ray2PtB = Vector2(-7.f, -0.5f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRayIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Line-Ray Intersection"));
	{
		Vector2 linePtA;
		Vector2 linePtB;
		Vector2 rayPtA;
		Vector2 rayPtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testLineRayIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcLineRayIntersection(linePtA, linePtB, rayPtA, rayPtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. Calculating the intersection between the line [%s:%s] and ray [%s:%s] should have resulted in %s. Instead it calculated %s."), linePtA, linePtB, rayPtA, rayPtB, intersectionToString(expected), intersectionToString(actual));
				return false;
			}

			return true;
		});

		//Axis aligned
		linePtA = Vector2(20.f, 2.f);
		linePtB = Vector2(50.f, 2.f);
		rayPtA = Vector2(2.f, 10.f);
		rayPtB = Vector2(2.f, 5.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(2.f, 2.f);
		if (!testLineRayIntersection())
		{
			return false;
		}

		std::swap(linePtA, linePtB); //should have no effect
		if (!testLineRayIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineRayIntersection())
		{
			return false;
		}

		rayPtA = Vector2(2.f, 3.f);
		rayPtB = Vector2(2.f, 1.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(2.f, 2.f);
		if (!testLineRayIntersection())
		{
			return false;
		}

		rayPtA += Vector2(0.f, -5.f);
		rayPtB += Vector2(0.f, -5.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineRayIntersection())
		{
			return false;
		}

		rayPtA = Vector2(-14.f, 2.f);
		rayPtB = Vector2(-7.f, 2.f);
		expected.IntersectionType = Geometry2dUtils::IT_Ray;
		expected.Pt1 = rayPtA;
		expected.Pt2 = linePtA;
		if (!testLineRayIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.Pt1 = Vector2(-7.f, 2.f);
		expected.Pt2 = Vector2(-25.f, 2.f);
		if (!testLineRayIntersection())
		{
			return false;
		}

		//Test angled intersections
		//line=10x-30
		//ray=4x-12
		linePtA = Vector2(0.f, -12.f);
		linePtB = Vector2(33.f, 120.f);
		rayPtA = Vector2(6.f, 30.f);
		rayPtB = Vector2(5.f, 20.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(3.f, 0.f);
		if (!testLineRayIntersection())
		{
			return false;
		}

		std::swap(linePtA, linePtB);
		if (!testLineRayIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineRayIntersection())
		{
			return false;
		}

		rayPtA = Vector2(5.f, 8.f);
		rayPtB = Vector2(7.f, 16.f);
		expected.IntersectionType = Geometry2dUtils::IT_Ray;
		expected.Pt1 = rayPtA;
		expected.Pt2 = rayPtB;
		if (!testLineRayIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Line-Segment Intersection"));
	{
		Vector2 linePtA;
		Vector2 linePtB;
		Vector2 segPtA;
		Vector2 segPtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testLineSegmentIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcLineSegIntersection(linePtA, linePtB, segPtA, segPtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. Calculating the intersection between line [%s:%s] and segment [%s:%s] should have resulted in %s. Instead it calculated %s."), linePtA, linePtB, segPtA, segPtB, intersectionToString(actual), intersectionToString(expected));
				return false;
			}

			return true;
		});

		//Axis aligned tests
		linePtA = Vector2(4.f, 8.f);
		linePtB = Vector2(4.f, -8.f);
		segPtA = Vector2(-2.f, -2.f);
		segPtB = Vector2(2.f, -2.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(2.f, -2.f);
		segPtB = Vector2(6.f, -2.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(4.f, -2.f);
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(6.f, -2.f);
		segPtB = Vector2(10.f, -2.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(5.f, 5.f);
		segPtB = Vector2(5.f, 6.f);
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(4.f, 5.f);
		segPtB = Vector2(4.f, 6.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = segPtA;
		expected.Pt2 = segPtB;
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		//Angled test
		//line = 0.1x+4
		//segment = 0.4x-2
		linePtA = Vector2(0.f, 4.f);
		linePtB = Vector2(10.f, 5.f);
		segPtA = Vector2(0.f, -2.f);
		segPtB = Vector2(5.f, 0.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(5.f, 0.f);
		segPtB = Vector2(30.f, 10.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(20.f, 6.f);
		if (!testLineSegmentIntersection())
		{
			return false;
		}

		//Make line parallel to segment
		linePtA = Vector2(0.f, -2.f);
		linePtB = Vector2(5.f, 0.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = segPtA;
		expected.Pt2 = segPtB;
		if (!testLineSegmentIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Ray-Segment Intersection"));
	{
		Vector2 rayPtA;
		Vector2 rayPtB;
		Vector2 segPtA;
		Vector2 segPtB;
		Geometry2dUtils::SIntersectResults expected;
		std::function<bool()> testRaySegmentIntersection([&]()
		{
			Geometry2dUtils::SIntersectResults actual;
			Geometry2dUtils::CalcRaySegIntersection(rayPtA, rayPtB, segPtA, segPtB, OUT actual);
			if (!compareIntersections(actual, expected))
			{
				UnitTestError(testFlags, TXT("Geometry 2d Utils test failed. Calculating the intersection between ray [%s:%s] and segment [%s:%s] should have resulted in %s. Instead it calculated %s."), rayPtA, rayPtB, segPtA, segPtB, intersectionToString(expected), intersectionToString(actual));
				return false;
			}

			return true;
		});

		//Axis aligned tests
		rayPtA = Vector2(-2.f, 1.f);
		rayPtB = Vector2(-8.f, 1.f);
		segPtA = Vector2(1.f, 8.f);
		segPtB = Vector2(1.f, -8.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(1.f, 1.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		segPtA = Vector2(-4.f, 8.f);
		segPtB = Vector2(-4.f, -8.f);
		expected.Pt1 = Vector2(-4.f, 1.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-4.f, 16.f);
		segPtB = Vector2(-4.f, 8.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-4.f, -8.f);
		segPtB = Vector2(-4.f, -16.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-12.f, 8.f);
		segPtB = Vector2(-12.f, -8.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(-12.f, 1.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-24.f, 1.f);
		segPtB = Vector2(-12.f, 1.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = segPtA;
		expected.Pt2 = segPtB;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-8.f, 1.f);
		segPtB = Vector2(-4.f, 1.f);
		expected.IntersectionType = Geometry2dUtils::IT_Segment;
		expected.Pt1 = Vector2(-8.f, 1.f);
		expected.Pt2 = segPtB;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA += Vector2(0.f, 1.f);
		segPtB += Vector2(0.f, 1.f);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		//Run angled tests
		//Ray=2x-4
		//Segment=4x-6
		rayPtA = Vector2(-2.f, -8.f);
		rayPtB = Vector2(0.f, -4.f);
		segPtA = Vector2(0.f, -6.f);
		segPtB = Vector2(1.5f, 0.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(1.f, -2.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		expected.IntersectionType = Geometry2dUtils::IT_None;
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		std::swap(rayPtA, rayPtB);
		segPtA = Vector2(1.5f, 0.f);
		segPtB = Vector2(2.f, 2.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(0.f, -6.f);
		segPtB = Vector2(-1.f, -10.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}

		segPtA = Vector2(-1.f, -10.f);
		segPtB = Vector2(4.f, 10.f);
		expected.IntersectionType = Geometry2dUtils::IT_Point;
		expected.Pt1 = Vector2(1.f, -2.f);
		if (!testRaySegmentIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Circle-Line/Ray/Segment intersections"));
	{
		Vector2 circleCenter;
		Float radius;
		Vector2 linePtA;
		Vector2 linePtB;
		std::vector<Vector2> expectedLineIntersections;
		std::vector<Vector2> expectedRayIntersections;
		std::vector<Vector2> expectedSegIntersections;
		Float tolerance = 0.005f; //The graphing calculator used to verify the intersections rounds to 3 decimal places.

		std::function<bool(const std::vector<Vector2>&, const std::vector<Vector2>&, const DString&)> verifyVectors([&](const std::vector<Vector2>& actual, const std::vector<Vector2>& expected, const DString& lineType)
		{
			if (actual.size() != expected.size())
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-%s intersection (circle pos: %s, radius %s, linePts: %s, %s), the number of expected intersections, does not match the number of actual intersections. Expected: %s, Actual: %s."), lineType, circleCenter, radius, linePtA, linePtB, DString::ListToString(expected), DString::ListToString(actual));
				return false;
			}

			std::vector<Vector2> searchFor(expected);
			while (!ContainerUtils::IsEmpty(searchFor))
			{
				bool bFoundMatch = false;

				for (const Vector2& intersection : actual)
				{
					if (intersection.IsNearlyEqual(ContainerUtils::GetLast(searchFor), tolerance))
					{
						bFoundMatch = true;
						searchFor.pop_back();
						break;
					}
				}

				if (bFoundMatch)
				{
					continue;
				}

				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-%s intersection (circle pos: %s, radius %s, linePts: %s, %s), the function should have returned the following intersections: %s. Instead it returned: %s."), lineType, circleCenter, radius, linePtA, linePtB, DString::ListToString(expected), DString::ListToString(actual));
				return false;
			}

			if (!ContainerUtils::IsEmpty(searchFor))
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-%s intersection (circle pos: %s, radius %s, linePts: %s, %s), the function did not produce the following expected intersections: %s. Instead it only returned: %s."), lineType, circleCenter, radius, linePtA, linePtB, DString::ListToString(searchFor), DString::ListToString(actual));
				return false;
			}

			return true;
		});

		std::function<bool()> testIntersections([&]()
		{
			//Run line test
			std::vector<Vector2> intersections;
			bool bResult = Geometry2dUtils::CalcCircleLineIntersections(circleCenter, radius, linePtA, linePtB, OUT intersections);
			bool bHasIntersection = !ContainerUtils::IsEmpty(intersections);
			if (bResult != bHasIntersection)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-line intersection (circle pos: %s, radius: %s, linePts: %s, %s), the function should return true if there's at least one intersection. It returned %s when it has %s intersection(s)."), circleCenter, radius, linePtA, linePtB, Bool(bResult), Int(intersections.size()));
				return false;
			}

			if (!verifyVectors(intersections, expectedLineIntersections, TXT("line")))
			{
				return false;
			}

			//Run ray test
			ContainerUtils::Empty(OUT intersections);
			bResult = Geometry2dUtils::CalcCircleRayIntersections(circleCenter, radius, linePtA, linePtB, OUT intersections);
			bHasIntersection = !ContainerUtils::IsEmpty(intersections);
			if (bResult != bHasIntersection)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-ray intersection (circle pos: %s, radius: %s, linePts: %s, %s), the function should return true if there's at least one intersection. It returned %s when it has %s intersection(s)."), circleCenter, radius, linePtA, linePtB, Bool(bResult), Int(intersections.size()));
				return false;
			}

			if (!verifyVectors(intersections, expectedRayIntersections, TXT("ray")))
			{
				return false;
			}

			//Run segment test
			ContainerUtils::Empty(OUT intersections);
			bResult = Geometry2dUtils::CalcCircleSegIntersections(circleCenter, radius, linePtA, linePtB, OUT intersections);
			bHasIntersection = !ContainerUtils::IsEmpty(intersections);
			if (bResult != bHasIntersection)
			{
				UnitTestError(testFlags, TXT("Geometry 2D Utils test failed. When calculating the circle-segment intersection (circle pos: %s, radius: %s, linePts: %s, %s), the function should return true if there's at least one intersection. It returned %s when it has %s intersection(s)."), circleCenter, radius, linePtA, linePtB, Bool(bResult), Int(intersections.size()));
				return false;
			}

			if (!verifyVectors(intersections, expectedSegIntersections, TXT("segment")))
			{
				return false;
			}

			return true;
		});

		//Run simple perpendicular cases where the circle is on the origin.
		circleCenter = Vector2::ZERO_VECTOR;
		radius = 4.f;
		linePtA = Vector2(5.f, 0.f);
		linePtB = Vector2(-5.f, 0.f);
		expectedLineIntersections = {Vector2(4.f, 0.f), Vector2(-4.f, 0.f)};
		expectedRayIntersections = expectedLineIntersections;
		expectedSegIntersections = expectedLineIntersections;
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2::ZERO_VECTOR;
		expectedSegIntersections = {Vector2(4.f, 0.f)};
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(4.5f, 0.f);
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(10.f, 0.f);
		ContainerUtils::Empty(OUT expectedRayIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtA = Vector2(5.f, 10.f);
		linePtB = Vector2(5.f, 0.f);
		ContainerUtils::Empty(OUT expectedLineIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Test line starting inside circle, moving out.
		linePtA = Vector2(0.f, 2.f);
		linePtB = Vector2(0.f, -2.f);
		expectedLineIntersections = {Vector2(0.f, 4.f), Vector2(0.f, -4.f)};
		expectedRayIntersections = {Vector2(0.f, -4.f)};
		if (!testIntersections())
		{
			return false;
		}

		//Test tangent line
		linePtA = Vector2(5.f, 4.f);
		linePtB = Vector2(4.f, 4.f);
		expectedLineIntersections = {Vector2(0.f, 4.f)};
		expectedRayIntersections = expectedLineIntersections;
		if (!testIntersections())
		{
			return false;
		}

		//Displace the circle
		circleCenter = Vector2(4.f, -8.f);
		radius = 10.f;
		linePtA = Vector2(4.f, 10.f);
		linePtB = Vector2(4.f, -20.f);
		expectedLineIntersections = {Vector2(4.f, 2.f), Vector2(4.f, -18.f)};
		expectedRayIntersections = expectedLineIntersections;
		expectedSegIntersections = expectedLineIntersections;
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(4.f, -10.f);
		expectedSegIntersections = {Vector2(4.f, 2.f)};
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(4.f, 5.f);
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(4.f, 15.f);
		ContainerUtils::Empty(OUT expectedRayIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Test the circle encompassesing segment
		linePtA = Vector2(1.f, -8.f);
		linePtB = Vector2(8.f, -8.f);
		expectedLineIntersections = {Vector2(-6.f, -8.f), Vector2(14.f, -8.f)};
		expectedRayIntersections = {Vector2(14.f, -8.f)};
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Run tests where the line intersects the circle at an angle.
		//Tests are verified using a graphing calculator: https://www.desmos.com/calculator

		//Equation of circle: (x - 2.2)^2 + (y + 1.44)^2 = 36
		circleCenter = Vector2(2.2f, -1.44f);
		radius = 6.f;

		//Equation of line: y = -x - 2
		linePtA = Vector2(-10.f, 8.f);
		linePtB = Vector2(-5.f, 3.f);

		expectedLineIntersections = {Vector2(-3.192f, 1.192f), Vector2(4.832f, -6.832f)};
		expectedRayIntersections = expectedLineIntersections;
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtA = Vector2(0.f, -2.f);
		expectedRayIntersections = {Vector2(-3.192f, 1.192f)};
		expectedSegIntersections = expectedRayIntersections;
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(-2.f, 0.f);
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Equation of circle: (x+5.5)^2 + (y-3.14)^2 = 49
		circleCenter = Vector2(-5.5f, 3.14f);
		radius = 7.f;

		//Equation of line: y = -4x + 4
		linePtA = Vector2(-2.f, 12.f);
		linePtB = Vector2(-1.5f, 10.f);
		expectedLineIntersections = {Vector2(-1.158f, 8.63f), Vector2(0.915f, 0.339f)};
		expectedRayIntersections = expectedLineIntersections;
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtB = Vector2(1.f, 0.f);
		expectedSegIntersections = expectedLineIntersections;
		if (!testIntersections())
		{
			return false;
		}

		linePtA = linePtB;
		linePtB = Vector2(2.f, -4.f);
		ContainerUtils::Empty(OUT expectedRayIntersections);
		ContainerUtils::Empty(OUT expectedSegIntersections);
		if (!testIntersections())
		{
			return false;
		}

		linePtA = Vector2(0.f, 4.f);
		expectedRayIntersections = {Vector2(0.915f, 0.339f)};
		expectedSegIntersections = expectedRayIntersections;
		if (!testIntersections())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Geometry 2D Utils"));
	return true;
}

bool PhysicsUnitTest::RunGeometry3dUtils (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Geometry 3D Utils"));

	SetTestCategory(testFlags, TXT("Is Parallel"));
	{
		Vector3 dirA;
		Vector3 dirB;
		bool bExpected;
		std::function<bool()> testParallel([&]()
		{
			bool bActual = Geometry3dUtils::IsParallel(dirA, dirB);
			if (bExpected != bActual)
			{
				DString expectedPrefix = (bExpected) ? DString::EmptyString : TXT(" NOT");
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. The directional vectors %s and %s are%s expected to be parallel, but the IsParallel function suggests otherwise."), dirA, dirB, expectedPrefix);
				return false;
			}

			return true;
		});

		dirA = Vector3(1.f, 0.f, 0.f);
		dirB = dirA;
		bExpected = true;
		if (!testParallel())
		{
			return false;
		}

		dirB.Z += 0.1f;
		bExpected = false;
		if (!testParallel())
		{
			return false;
		}

		dirA = Vector3(0.f, 1.f, 0.f);
		dirB = Vector3(0.f, -1.f, 0.f);
		bExpected = true;
		if (!testParallel())
		{
			return false;
		}

		dirB.X -= 0.1f;
		bExpected = false;
		if (!testParallel())
		{
			return false;
		}

		dirA = Vector3(0.f, 0.f, -1.f);
		dirB = Vector3(0.f, 0.f, 8.f);
		bExpected = true;
		if (!testParallel())
		{
			return false;
		}

		dirA *= 100.f;
		if (!testParallel())
		{
			return false;
		}

		dirA = Vector3(0.5f, 1.5f, 4.f);
		dirB = dirA;
		if (!testParallel())
		{
			return false;
		}

		dirB *= -1.f;
		if (!testParallel())
		{
			return false;
		}

		dirB.Y += 0.01f;
		bExpected = false;
		if (!testParallel())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Collinear"));
	{
		Vector3 a;
		Vector3 b;
		Vector3 c;
		bool bExpected;
		std::function<bool()> testCollinear([&]()
		{
			bool bActual = Geometry3dUtils::AreCollinear(a, b, c);
			if (bActual != bExpected)
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. The points %s, %s, %s in a collinear test should have returned %s. Instead it returned %s."), a, b, c, Bool(bExpected), Bool(bActual));
				return false;
			}

			return true;
		});

		a = Vector3::ZERO_VECTOR;
		b = Vector3(1.f, 0.f, 0.f);
		c = Vector3(5.f, 0.f, 0.f);
		bExpected = true;
		if (!testCollinear())
		{
			return false;
		}

		c = Vector3(-5.f, 0.f, 0.f);
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3(10.f, 0.f, 0.f);
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3::ZERO_VECTOR;
		b = Vector3::ZERO_VECTOR;
		c = Vector3::ZERO_VECTOR;
		if (!testCollinear())
		{
			return false;
		}

		b = Vector3(1.f, 0.f, 0.f);
		c = Vector3(0.f, 1.f, 0.f);
		bExpected = false;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3(0.5f, 0.5f, 0.f);
		bExpected = true;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3(0.5f, 0.5f, 0.25f);
		bExpected = false;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3(2.f, 4.f, 8.f);
		b = Vector3(1.f, 5.f, 7.f);
		c = Vector3(-2.f, 8.f, 4.f);
		bExpected = true;
		if (!testCollinear())
		{
			return false;
		}

		c.X += 0.2f;
		bExpected = false;
		if (!testCollinear())
		{
			return false;
		}

		a = Vector3(0.f, 1.f, 0.f);
		b = Vector3(0.f, 3.f, 0.f);
		c = Vector3(0.f, 3.1f, 0.f);
		bExpected = true;
		if (!testCollinear())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Is On Segment"));
	{
		Vector3 a;
		Vector3 b;
		Vector3 testPt;
		bool bExpected;
		std::function<bool()> testOnSegment([&]()
		{
			bool bActual = Geometry3dUtils::IsOnSegment(a, b, testPt);
			if (bActual != bExpected)
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. Is On Segment did not return the expected value. Segment is defined by end points %s and %s. The point it was testing against is %s. Expected=%s. Actual=%s."), a, b, testPt, Bool(bExpected), Bool(bActual));
				return false;
			}

			//Ensure the faster algorithm works as well if the points are collinear.
			if (Geometry3dUtils::AreCollinear(a, b, testPt))
			{
				bool bFastCheck = Geometry3dUtils::IsBetweenCollinearPts(a, b, testPt);
				if (bFastCheck != bActual)
				{
					UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. IsBetweenCollinearPts did not return the same results for the three collinear points: %s, %s, %s. The IsOnSegment returned %s. IsBetweenCollinearPts returned %s."), a, b, testPt, Bool(bActual), Bool(bFastCheck));
					return false;
				}
			}

			return true;
		});

		//Test simple perpendicular cases
		a = Vector3(0.f, 0.f, 1.f);
		b = Vector3(0.f, 0.f, 4.f);
		testPt = Vector3::ZERO_VECTOR;
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.f, 0.f, 1.f);
		bExpected = true;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.f, 0.f, 2.f);
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.2f, 0.f, 2.f);
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.f, -1.f, 2.f);
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.f, 0.f, 4.f);
		bExpected = true;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(0.f, 0.f, 5.f);
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}
		
		//Test against angular segments
		a = Vector3(1.f, 1.f, 1.f);
		b = Vector3(10.f, 10.f, 10.f);
		testPt = Vector3::ZERO_VECTOR;
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(2.f, 2.f, 2.f);
		bExpected = true;
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(8.1f, 8.1f, 8.1f);
		if (!testOnSegment())
		{
			return false;
		}

		testPt.X += 0.1f;
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}

		a = Vector3(-5.f, -10.f, 4.f);
		b = Vector3(-15.f, -20.f, 40.f);
		if (!testOnSegment())
		{
			return false;
		}

		testPt = Vector3(-10.f, -15.f, 22.f);
		bExpected = true;
		if (!testOnSegment())
		{
			return false;
		}

		testPt.Y += 0.1f;
		bExpected = false;
		if (!testOnSegment())
		{
			return false;
		}

		testPt.Y -= 0.1f;
		testPt.X += 0.1f;
		if (!testOnSegment())
		{
			return false;
		}

		testPt.X -= 0.1f;
		testPt.Z -= 0.1f;
		if (!testOnSegment())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Distance Between Point and Line"));
	{
		Vector3 linePt;
		Vector3 lineDir;
		Vector3 testPoint;
		bool bBoundedLine;
		Vector3 expectedIntersection;
		Float expectedDist;
		std::function<bool()> testDist([&]()
		{
			Vector3 actualIntersection;
			Float actualDist = Geometry3dUtils::CalcDistBetweenPointAndLine(linePt, lineDir, testPoint, bBoundedLine, OUT actualIntersection);
			if (!actualDist.IsCloseTo(expectedDist, 0.0001f))
			{
				DString boundedStr = (bBoundedLine) ? TXT("bounded") : TXT("infinite");
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. Failed to calculate the distance between a point and a %s line. The line starts at point %s with directional vector %s. The test point is %s. The calculated distance is %s. The expected distance is %s."), boundedStr, linePt, lineDir, testPoint, actualDist, expectedDist);
				return false;
			}

			if (!expectedIntersection.IsNearlyEqual(actualIntersection, 0.0001f))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between point (%s) and line (point = %s, direction = %s), the expected point of intersection is %s. Instead it calculated %s."), testPoint, linePt, lineDir, expectedIntersection, actualIntersection);
				return false;
			}

			//Ensure the results makes sense
			Float testDist = (testPoint - actualIntersection).VSize();
			if (!testDist.IsCloseTo(actualDist))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between point (%s) and line (point = %s, direction = %s), the point of intersection is %s, however the vector size between test point and intersection does not match the distance. Distance = %s, Expected Distance = %s."), testPoint, linePt, lineDir, actualIntersection, actualDist, testDist);
				return false;
			}

			if (!Geometry3dUtils::AreCollinear(linePt, lineDir + linePt, actualIntersection, 0.0001f))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between point (%s) and line (point = %s, direction = %s), the point of intersection is %s. That intersection point should reside on the line, but the three points are not collinear."), testPoint, linePt, lineDir, actualIntersection);
				return false;
			}

			//Ensure the squared dist variation aligns with the other CalcDist function.
			Vector3 squaredIntersection;
			Float squaredDist = Geometry3dUtils::CalcSquaredDistBetweenPointAndLine(linePt, lineDir, testPoint, bBoundedLine, OUT squaredIntersection);
			if (!squaredIntersection.IsNearlyEqual(actualIntersection))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the squared distance between point (%s) and line (point = %s, direction = %s), the intersection point (%s) is different from the original intersection point (%s)."), testPoint, linePt, lineDir, squaredIntersection, actualIntersection);
				return false;
			}

			if (!Float::Pow(actualDist, 2.f).IsCloseTo(squaredDist, 0.001f))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the squared distance between point (%s) and line (point = %s, direction = %s), the squared distance (%s) is not actually the square of the original distance (%s)."), testPoint, linePt, lineDir, squaredDist, actualDist);
				return false;
			}

			return true;
		});

		//Run simple perpendicular cases that are easy to visualize and calculate.
		linePt = Vector3::ZERO_VECTOR;
		lineDir = Vector3(1.f, 0.f, 0.f);
		testPoint = Vector3(1.f, 0.f, 0.f);
		bBoundedLine = false;
		expectedDist = 0.f;
		expectedIntersection = testPoint;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(10.f, 0.f, 0.f);
		expectedIntersection = testPoint;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(-10.f, 0.f, 0.f);
		expectedIntersection = testPoint;
		if (!testDist())
		{
			return false;
		}

		bBoundedLine = true;
		expectedDist = 10.f;
		expectedIntersection = Vector3::ZERO_VECTOR;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(100.f, 0.f, 0.f);
		expectedDist = 99.f;
		expectedIntersection = lineDir;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(0.5f, 1.f, 0.f);
		expectedDist = 1.f;
		expectedIntersection = Vector3(0.5f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(0.5f, 0.f, -1.f);
		if (!testDist())
		{
			return false;
		}

		lineDir = Vector3(-1.f, 0.f, 0.f);
		testPoint = Vector3(-0.5f, -1.f, 0.f);
		expectedIntersection = Vector3(-0.5f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(-0.5f, 0.f, 5.f);
		expectedDist = 5.f;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(500.f, 0.f, 5.f);
		bBoundedLine = false;
		expectedDist = 5.f;
		expectedIntersection = Vector3(500.f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(-500.f, 0.f, 0.f);
		bBoundedLine = true;
		expectedDist = 499.f;
		expectedIntersection = Vector3(-1.f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		lineDir = Vector3(-100.f, 0.f, 0.f);
		expectedDist = 400.f;
		expectedIntersection = lineDir;
		if (!testDist())
		{
			return false;
		}

		linePt = Vector3::ZERO_VECTOR;
		lineDir = Vector3(1.f, 0.f, 0.f);
		testPoint = Vector3(1.f, 1.f, 0.f);
		bBoundedLine = false;
		expectedDist = 1.f;
		expectedIntersection = Vector3(1.f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		lineDir = Vector3(2.f, 0.f, 0.f);
		testPoint = Vector3(1.75f, 0.f, 0.f); //test against nonnormalized directional vectors
		bBoundedLine = true;
		expectedDist = 0.f;
		expectedIntersection = Vector3(1.75f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		bBoundedLine = false;
		if (!testDist())
		{
			return false;
		}

		testPoint = Vector3(2.5f, 0.f, 0.f);
		expectedIntersection = testPoint;
		if (!testDist())
		{
			return false;
		}

		bBoundedLine = true;
		expectedDist = 0.5f;
		expectedIntersection = Vector3(2.f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		//Run simple angular test cases
		lineDir = Vector3(1.f, 0.f, 0.f);
		testPoint = Vector3(1.f, 1.f, 1.f);
		bBoundedLine = false;
		expectedDist = 1.4142136f;
		expectedIntersection = Vector3(1.f, 0.f, 0.f);
		if (!testDist())
		{
			return false;
		}

		//Run angular test cases using random hard coded numbers.
		//expectedIntersection is calculated from: https://www.123calculus.com/en/projection-point-line-page-7-60-180.html
		linePt = Vector3(8.f, 4.f, 12.f);
		lineDir = Vector3(1.f, 0.f, 1.f);
		testPoint = Vector3(1.f, -1.f, 1.f);
		expectedDist = 5.744563f;
		expectedIntersection = Vector3(-1.f, 4.f, 3.f);
		if (!testDist())
		{
			return false;
		}

		linePt = Vector3(2.f, 4.f, 8.f);
		lineDir = Vector3(4.f, 8.f, 2);
		testPoint = Vector3(5.f, 6.f, 2.f);
		bBoundedLine = false;
		expectedDist = 6.778819f;
		expectedIntersection = Vector3(2.7619f, 5.52381f, 8.38095f);
		if (!testDist())
		{
			return false;
		}

		linePt = Vector3(1.f, 9.f, -1.f);
		lineDir = Vector3(-8.f, -10.f, 8.f);
		testPoint = Vector3(7.f, 12.f, 13.f);
		expectedDist = 15.360007f;
		expectedIntersection = Vector3(-0.19298f, 7.50877f, 0.19298f);
		if (!testDist())
		{
			return false;
		}

		linePt = Vector3(7.f, -4.f, 15.f);
		lineDir = Vector3(1.f, 8.f, -8.f);
		testPoint = Vector3(8.f, 3.f, 10.f);
		expectedDist = 1.435972f;
		expectedIntersection = Vector3(7.75194f, 2.0155f, 8.9845f);
		if (!testDist())
		{
			return false;
		}

		linePt = Vector3(-4.5f, 22.2f, -16.4f);
		lineDir = Vector3(8.1f, -15.64f, 11.9f);
		testPoint = Vector3(38.4f, 24.8f, -33.5f);
		expectedDist = 45.999418f;
		expectedIntersection = Vector3(-2.64748f, 18.62304f, -13.6784f);
		if (!testDist())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Line Intersection"));
	{
		Vector3 lineAPt;
		Vector3 lineADir;

		Vector3 lineBPt;
		Vector3 lineBDir;

		bool bBoundedLines;
		bool bShouldIntersect;
		Vector3 expected; //If expected is a zero vector, it'll disable the intersection test (for parallel cases that have infinite intersections)
		std::function<bool()> testIntersection([&]()
		{
			Vector3 intersection;
			bool bIntersecting = Geometry3dUtils::IsIntersecting(lineAPt, lineADir, lineBPt, lineBDir, bBoundedLines, OUT intersection);
			if (bIntersecting != bShouldIntersect)
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. Mismatch in expected and actual intersection. Line A is defined by point %s and direction %s. Line B is defined by point %s and direction %s. Are Lines bounded? %s. IsIntersecting=%s. ExpectedIntersecting=%s."), lineAPt, lineADir, lineBPt, lineBDir, Bool(bBoundedLines), Bool(bIntersecting), Bool(bShouldIntersect));
				return false;
			}

			if (bIntersecting && !expected.IsEmpty() && !expected.IsNearlyEqual(intersection))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. Although the two lines are intersecting, the intersection point does not match the expected point. Line A is defined by point %s and direction %s. Line B is defined by point %s and direction %s. Are lines bounded? %s. Intersection point is %s. Expected intersection point is %s."), lineAPt, lineADir, lineBPt, lineBDir, Bool(bBoundedLines), intersection, expected);
				return false;
			}

			return true;
		});

		//Simple case where the vertical line intersects the middle point of the line parallel to the XY plane.
		lineAPt = Vector3(6.f, 8.f, 4.f);
		lineADir = Vector3(6.f, 7.f, 0.f);
		lineBPt = Vector3(9.f, 11.5f, 10.f);
		lineBDir = Vector3(0.f, 0.f, -20.f);
		bBoundedLines = true;
		bShouldIntersect = true;
		expected = Vector3(9.f, 11.5f, 4.f);
		if (!testIntersection())
		{
			return false;
		}

		//Test end points
		lineBPt.Z += 50.f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = false;
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt.Z -= 150.f;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Test displacement
		lineAPt = Vector3(6.f, 4.f, 4.f);
		lineBPt.Z = 10.f;
		if (!testIntersection())
		{
			return false;
		}

		//The reversed params should produce the same results
		lineAPt = Vector3(9.f, 11.5f, 10.f);
		lineADir = Vector3(0.f, 0.f, -20.f);
		lineBPt = Vector3(6.f, 8.f, 4.f);
		lineBDir = Vector3(6.f, 7.f, 0.f);
		bBoundedLines = true;
		bShouldIntersect = true;
		expected = Vector3(9.f, 11.5f, 4.f);
		if (!testIntersection())
		{
			return false;
		}

		//Test end points
		lineAPt.Z += 50.f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = false;
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		lineAPt.Z -= 150.f;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Test displacement
		lineAPt.Z = 10.f;
		lineBPt = Vector3(3.f, 4.f, 4.f);
		if (!testIntersection())
		{
			return false;
		}

		//Test parallel lines
		lineAPt = Vector3(5.f, 6.f, 7.f);
		lineADir = Vector3(-3.f, 0.f, 0.f);
		lineBPt = Vector3(50.f, 6.f, 7.f);
		lineBDir = Vector3(4.f, 0.f, 0.f);
		bBoundedLines = false;
		bShouldIntersect = true;
		expected = Vector3::ZERO_VECTOR;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt = Vector3(7.f, 6.f, 7.f);
		lineBDir = Vector3(-4.f, 0.f, 0.f);
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt.X = 3.f;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt.Z += 0.1f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt = Vector3(4.f, 6.1f, 7.f);
		if (!testIntersection())
		{
			return false;
		}

		lineAPt = Vector3(3.f, 4.f, 5.f);
		lineADir = Vector3(1.f, -1.f, 2.f);
		lineBPt = Vector3(5.f, 2.f, 9.f);
		lineBDir = lineADir;
		bBoundedLines = false;
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		lineBDir *= -1.f;
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLines = false;
		if (!testIntersection())
		{
			return false;
		}

		lineBPt.Y += 0.1f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Run this example: https://www.youtube.com/watch?v=N-qUfr-rz_Y
		lineAPt = Vector3(-2.f, -1.f, 0.f);
		lineADir = Vector3(1.f, 1.f, 1.f);
		lineBPt = Vector3(8.f, -6.f, -11.f);
		lineBDir = Vector3(-2.f, 3.f, 5.f);
		bBoundedLines = true;
		bShouldIntersect = false;
		expected = Vector3(2.f, 3.f, 4.f);
		if (!testIntersection())
		{
			return false;
		}

		lineADir *= 20.f;
		lineBDir *= 20.f;
		bShouldIntersect = true;
		if (!testIntersection())
		{
			return false;
		}

		lineBDir.Y *= -1.f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Run examples from: https://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines
		lineAPt = Vector3(5.f, 5.f, 4.f);
		lineADir = Vector3(5.f, 5.f, 2.f);
		lineBPt = Vector3(5.f, 5.f, 5.f);
		lineBDir = Vector3(5.f, 5.f, -2.f);
		bBoundedLines = true;
		bShouldIntersect = true;
		expected = Vector3(6.25f, 6.25f, 4.5f);
		if (!testIntersection())
		{
			return false;
		}

		lineAPt = Vector3(6.f, 8.f, 4.f);
		lineADir = Vector3(6.f, 7.f, 0.f);
		lineBPt = Vector3(6.f, 8.f, 2.f);
		lineBDir = Vector3(6.f, 7.f, 4.f);
		bShouldIntersect = true;
		expected = Vector3(9.f, 11.5f, 4.f);
		if (!testIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Distance Between Lines"));
	{
		Float tolerance = 0.00001f;
		Vector3 linePtA;
		Vector3 lineDirA;
		Vector3 linePtB;
		Vector3 lineDirB;
		bool bBoundedLines;
		Float expectedDist;

		//If the expected distance vector is empty, then this test is skipped since there are some cases where there may be infinite solutions for parallel lines.
		Vector3 expectedDistEndPtA; 
		Vector3 expectedDistEndPtB;

		std::function<bool()> testLineDist([&]()
		{
			Vector3 actualDistEndPtA;
			Vector3 actualDistEndPtB;
			Float actualDist = Geometry3dUtils::CalcDistBetweenLines(linePtA, lineDirA, linePtB, lineDirB, bBoundedLines, OUT actualDistEndPtA, OUT actualDistEndPtB);
			DString lineDescription = bBoundedLines ? TXT("segment") : TXT("line");
			if (!expectedDist.IsCloseTo(actualDist, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between %s A (pt = %s, dir = %s) and %s B (pt = %s, dir = %s), the distance is %s instead of %s."), lineDescription, linePtA, lineDirA, lineDescription, linePtB, lineDirB, actualDist, expectedDist);
				return false;
			}

			if (!expectedDistEndPtA.IsEmpty() && !expectedDistEndPtA.IsNearlyEqual(actualDistEndPtA, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between %s A (pt = %s, dir = %s) and %s B (pt = %s, dir = %s), the expected end point for the distance intersection on A is %s instead of %s."), lineDescription, linePtA, lineDirA, lineDescription, linePtB, lineDirB, expectedDistEndPtA, actualDistEndPtA);
				return false;
			}

			if (!expectedDistEndPtB.IsEmpty() && !expectedDistEndPtB.IsNearlyEqual(actualDistEndPtB, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between %s A (pt = %s, dir = %s) and %s B (pt = %s, dir = %s), the expected end point for the distance intersection on B is %s instead of %s."), lineDescription, linePtA, lineDirA, lineDescription, linePtB, lineDirB, expectedDistEndPtB, actualDistEndPtB);
				return false;
			}

			//The end points must reside in the correct lines regardless if the end point test is disabled.
			if (!Geometry3dUtils::AreCollinear(linePtA, linePtA + lineDirA, actualDistEndPtA, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between %s A (pt = %s, dir = %s) and %s B (pt = %s, dir = %s), the end point (%s) for the distance intersection on A does not reside on %s A."), lineDescription, linePtA, lineDirA, lineDescription, linePtB, lineDirB, actualDistEndPtA, lineDescription);
				return false;
			}

			if (!Geometry3dUtils::AreCollinear(linePtB, linePtB + lineDirB, actualDistEndPtB, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between %s A (pt = %s, dir = %s) and %s B (pt = %s, dir = %s), the end point (%s) for the distance intersection on B does not reside on %s B."), lineDescription, linePtA, lineDirA, lineDescription, linePtB, lineDirB, actualDistEndPtB, lineDescription);
				return false;
			}

			//For segments, the distance end points must reside within the bounds of the segments.
			if (bBoundedLines)
			{
				if (!Geometry3dUtils::IsOnSegment(linePtA, linePtA + lineDirA, actualDistEndPtA, tolerance))
				{
					UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between segment A (pt = %s, dir = %s) and segment B (pt = %s, dir = %s), the end point (%s) for the distance intersection on A does not reside within the bounds of segment A."), linePtA, lineDirA, linePtB, lineDirB, actualDistEndPtA);
					return false;
				}

				if (!Geometry3dUtils::IsOnSegment(linePtB, linePtB + lineDirB, actualDistEndPtB, tolerance))
				{
					UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the distance between segment A (pt = %s, dir = %s) and segment B (pt = %s, dir = %s), the end point (%s) for the distance intersection on B does not reside within the bounds of segment B."), linePtA, lineDirA, linePtB, lineDirB, actualDistEndPtB);
					return false;
				}
			}

			return true;
		});
		
		//Run simple perpendicular cases
		linePtA = Vector3::ZERO_VECTOR;
		lineDirA = Vector3(1.f, 0.f, 0.f);
		linePtB = Vector3(1.f, 1.f, 0.f);
		lineDirB = Vector3(0.f, 0.f, 1.f);
		bBoundedLines = false;
		expectedDist = 1.f;
		expectedDistEndPtA = Vector3(1.f, 0.f, 0.f); 
		expectedDistEndPtB = Vector3(1.f, 1.f, 0.f);
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(0.f, -1.f, 0.f);
		bBoundedLines = false;
		expectedDist = 2.f;
		expectedDistEndPtA = Vector3(1.f, -1.f, 0.f);
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		if (!testLineDist())
		{
			return false;
		}

		linePtA.X = -100.f;
		bBoundedLines = false;
		if (!testLineDist())
		{
			return false;
		}

		linePtA.X = 100.f;
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		expectedDist = 99.0202f;
		expectedDistEndPtA = Vector3(100.f, -1.f, 0.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtA.X = -100.f;
		expectedDist = 100.02f;
		expectedDistEndPtA = Vector3(-99.f, -1.f, 0.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(0.f, -1.f, 0.f);
		linePtB = Vector3(1.f, 1.f, -100.f);
		expectedDist = 99.0202f;
		expectedDistEndPtA = Vector3(1.f, -1.f, 0.f);
		expectedDistEndPtB = Vector3(1.f, 1.f, -99.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtB.Z = 100.f;
		expectedDist = 100.019998f;
		expectedDistEndPtB = Vector3(1.f, 1.f, 100.f);
		if (!testLineDist())
		{
			return false;
		}

		//Run simple parallel cases
		linePtA = Vector3(1.f, 1.f, 1.f);
		lineDirA = Vector3(1.f, 1.f, 1.f);
		linePtB = Vector3(5.f, 5.f, 5.f);
		lineDirB = Vector3(1.f, 1.f, 1.f);
		bBoundedLines = false;
		expectedDist = 0.f; //Lines are on top of the each other
		expectedDistEndPtA = Vector3::ZERO_VECTOR; //Disable test since there are infinite solutions
		expectedDistEndPtB = Vector3::ZERO_VECTOR; //Disable test since there are infinite solutions
		if (!testLineDist())
		{
			return false;
		}

		lineDirB *= -1.f;
		if (!testLineDist())
		{
			return false;
		}

		lineDirA = Vector3(10.f, 0.f, 0.f);
		lineDirB = Vector3(1.f, 0.f, 0.f);
		expectedDist = 5.656854f;
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		if (!testLineDist())
		{
			return false;
		}

		linePtB = Vector3(20.f, 5.f, 5.f);
		expectedDist = 10.630146f;
		expectedDistEndPtA = Vector3(11.f, 1.f, 1.f);
		expectedDistEndPtB = Vector3(20.f, 5.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		lineDirB *= -1.f;
		expectedDist = 9.797959f;
		expectedDistEndPtB = Vector3(19.f, 5.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtB = Vector3(-20.f, 5.f, 5.f);
		expectedDist = 21.748563f;
		expectedDistEndPtA = Vector3(1.f, 1.f, 1.f);
		expectedDistEndPtB = Vector3(-20.f, 5.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		lineDirB *= -1.f;
		expectedDist = 20.784609f;
		expectedDistEndPtB = Vector3(-19.f, 5.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(1.f, 1.f, 1.f);
		lineDirA = Vector3(0.f, 1.f, 0.f);
		linePtB = Vector3(5.f, 0.f, 5.f);
		lineDirB = Vector3(0.f, 10.f, 0.f);
		expectedDist = 5.656854f;
		expectedDistEndPtA = Vector3::ZERO_VECTOR; //disable test
		expectedDistEndPtB = Vector3::ZERO_VECTOR; //disable test
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(1.f, 20.f, 1.f);
		expectedDist = 11.489125f;
		expectedDistEndPtA = Vector3(linePtA);
		expectedDistEndPtB = Vector3(5.f, 10.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		lineDirA *= -1.f;
		expectedDist = 10.630146f;
		expectedDistEndPtA = Vector3(1.f, 19.f, 1.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(1.f, -20.f, 1.f);
		expectedDist = 20.78461f;
		expectedDistEndPtA = linePtA;
		expectedDistEndPtB = Vector3(5.f, 0.f, 5.f);
		if (!testLineDist())
		{
			return false;
		}

		lineDirA *= -1.f;
		expectedDist = 19.824228f;
		expectedDistEndPtA = Vector3(1.f, -19.f, 1.f);
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(8.f, -1.f, 4.f);
		lineDirA = Vector3(5.f, 5.7f, 13.f);
		linePtB = Vector3(8.4f, 6.3f, -7.f);
		lineDirB = lineDirA * -1.f;
		bBoundedLines = false;
		expectedDist = 11.438311f;
		expectedDistEndPtA = Vector3::ZERO_VECTOR; //disable test
		expectedDistEndPtB = Vector3::ZERO_VECTOR; //disable test
		if (!testLineDist())
		{
			return false;
		}

		//Run skewed line tests
		//Run test case from: https://www.youtube.com/watch?v=HC5YikQxwZA
		linePtA = Vector3(0.f, 2.f, -1.f);
		lineDirA = Vector3(1.f, 1.f, 2.f);
		linePtB = Vector3(1.f, 0.f, -1.f);
		lineDirB = Vector3(1.f, 1.f, 3.f);
		bBoundedLines = false;
		expectedDist = 2.12132f;
		expectedDistEndPtA = Vector3(-1.5f, 0.5f, -4.f);
		expectedDistEndPtB = Vector3(0.f, -1.f, -4.f);
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		expectedDist = 2.215646838f;
		expectedDistEndPtA = linePtA;
		expectedDistEndPtB = Vector3(1.09091f, 0.09091f, -0.72727f);
		if (!testLineDist())
		{
			return false;
		}

		/*
		Disabled this test since when I attempt to do the math manually, it doesn't add up. Possibly because my function is wrong?
		When doing it manually, I would end up getting a system of equations with:
		Point on line A = -12u-21t+18=0
		Point on line B = 21u+12t-36=0
		Solving it would end up getting: t=-0.181818, u=1.81818
		However when placing them back in the generic line equations:
		(-2t+5, 4t+2, t-1) and (2u, -u-4, -4u+7)

		Got fed up. Wasted too many days on this. Couldn't find a distance calculator that can also retrieve the end points of this line. Unit tests for skewed lines will be lacking.
		At least we got one test case working.

		linePtA = Vector3(5.f, 2.f, -1.f);
		lineDirA = Vector3(-2.f, 4.f, 1.f);
		linePtB = Vector3(0.f, -4.f, 7.f);
		lineDirB = Vector3(2.f, -1.f, -4.f);
		bBoundedLines = false;
		if (!testLineDist())
		{
			return false;
		}
		*/

		//Only test for distance since I suck at math
		linePtA = Vector3(5.f, 2.f, -1.f);
		lineDirA = Vector3(1.f, 3.f, 0.5f);
		linePtB = Vector3(4.f, -6.f, 4.f);
		lineDirB = Vector3(1.f, -1.f, -3.f);
		bBoundedLines = false;
		expectedDist = 3.940162f;
		expectedDistEndPtA = Vector3::ZERO_VECTOR; //disable test
		expectedDistEndPtB = Vector3::ZERO_VECTOR; //disable test
		if (!testLineDist())
		{
			return false;
		}

		linePtA = Vector3(-3.f, 1.f, 4.f);
		lineDirA = Vector3(0.5f, -2.1f, 1.5f);
		linePtB = Vector3(-3.3f, -5.f, 2.7f);
		lineDirB = Vector3(1.1f, -3.6f, 0.2f);
		expectedDist = 2.186235f;
		if (!testLineDist())
		{
			return false;
		}

		//Test against lines that intersect
		linePtA = Vector3(1.f, 4.f, -7.f);
		lineDirA = Vector3(-2.f, 3.f, 2.f);
		linePtB = Vector3(5.f, -2.f, 3.f);
		lineDirB = Vector3(-6.f, 9.f, -8.f);
		bBoundedLines = false;
		expectedDist = 0.f;
		expectedDistEndPtA = Vector3(-1.f, 7.f, -5.f);
		expectedDistEndPtB = expectedDistEndPtA;
		if (!testLineDist())
		{
			return false;
		}

		lineDirA *= -1.f;
		if (!testLineDist())
		{
			return false;
		}

		bBoundedLines = true;
		expectedDist = 3.751974f;
		expectedDistEndPtA = linePtA;
		expectedDistEndPtB = Vector3(-0.23757f, 5.85635f, -3.98343f);
		if (!testLineDist())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Line-Plane Intersection"));
	{
		Geometry3dUtils::SPlane plane;
		Vector3 linePt;
		Vector3 lineDir;
		bool bBoundedLine;
		bool bShouldIntersect;
		Vector3 expectedIntersection;

		std::function<bool()> testIntersection([&]()
		{
			Vector3 intersection;
			bool bIsIntersecting = Geometry3dUtils::CalcLinePlaneIntersection(plane, linePt, lineDir, bBoundedLine, OUT intersection);

			if (bIsIntersecting != bShouldIntersect)
			{
				DString intersectStr = (bShouldIntersect) ? DString::EmptyString : TXT(" NOT");
				UnitTestError(testFlags, TXT("Geometry 3D test failed. When calculating the intersection point between plane (point = %s, normal = %s) and line (point = %s, direction = %s, boundedLine = %s), the two shapes should%s have intersected, but the function suggests otherwise."), plane.Position, plane.ReadNormal(), linePt, lineDir, Bool(bBoundedLine), intersectStr);
				return false;
			}

			if (bIsIntersecting && !expectedIntersection.IsNearlyEqual(intersection, 0.01f)) //The calculator used to verify the expected intersections only had three decimal places.
			{
				UnitTestError(testFlags, TXT("Geometry 3D test failed. When calculating the intersection point between plane (point = %s, normal = %s) and line (point = %s, direction = %s), the intersection point should have been %s. Instead it's %s."), plane.Position, plane.ReadNormal(), linePt, lineDir, expectedIntersection, intersection);
				return false;
			}

			return true;
		});

		//Simple perpendicular test cases
		plane.SetNormal(Vector3(0.f, 0.f, 1.f));
		plane.Position = Vector3::ZERO_VECTOR;
		linePt = Vector3(0.f, 0.f, 5.f);
		lineDir = Vector3(0.f, 0.f, -1.f);
		bBoundedLine = false;
		bShouldIntersect = true;
		expectedIntersection = Vector3::ZERO_VECTOR;
		if (!testIntersection())
		{
			return false;
		}

		lineDir *= -1.f;
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLine = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		plane.SetNormal(Vector3(1.f, 1.f, 0.f));
		plane.Position = Vector3(2.f, 2.f, 2.f);
		linePt = Vector3(4.f, 4.f, 4.f);
		lineDir = Vector3(-1.f, -1.f, 0.f);
		bBoundedLine = false;
		bShouldIntersect = true;
		expectedIntersection = Vector3(2.f, 2.f, 4.f);
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLine = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Run parallel test cases
		plane.SetNormal(Vector3(1.f, 0.f, 0.f));
		plane.Position = Vector3(-3.f, 1005.f, -4829.f);
		linePt = Vector3(-5.f, 45.f, 64.f);
		lineDir = Vector3(0.f, 0.f, 1.f);
		bBoundedLine = false;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		linePt.X = -3.f;
		bShouldIntersect = true;
		expectedIntersection = linePt;
		if (!testIntersection())
		{
			return false;
		}

		lineDir = Vector3(0.f, 4.f, -6.f);
		bBoundedLine = true;
		if (!testIntersection())
		{
			return false;
		}

		linePt.X += 2.f;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}

		//Run random test cases
		plane.SetNormal(Vector3(0.5f, -2.f, 4.f));
		plane.Position = Vector3(6.5f, -3.1f, 4.2f);
		linePt = Vector3(3.f, 7.f, -2.f);
		lineDir = Vector3(4.f, 5.f, -1.f);
		bBoundedLine = false;
		bShouldIntersect = true;
		expectedIntersection = Vector3(-12.583f, -12.479f, 1.896f);
		if (!testIntersection())
		{
			return false;
		}

		plane.SetNormal(Vector3(2.f, 1.f, -0.25f));
		plane.Position = Vector3(9.f, 7.2f, -2.5f);
		linePt = Vector3(3.2f, 4.3f, -2.f);
		lineDir = Vector3(1.5f, -1.5f, 2.2f);
		expectedIntersection = Vector3(26.292f, -18.792f, 31.868f);
		if (!testIntersection())
		{
			return false;
		}

		plane.SetNormal(Vector3(0.1f, 0.4f, -0.25f));
		plane.Position = Vector3(4.f, 7.f, -3.5f);
		linePt = Vector3(205.f, 315.f, 400.f);
		lineDir = Vector3(-25.f, -30.f, -32.f);
		expectedIntersection = Vector3(41.827f, 119.192f, 191.138f);
		if (!testIntersection())
		{
			return false;
		}

		bBoundedLine = true;
		bShouldIntersect = false;
		if (!testIntersection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Projecting Line to Plane"));
	{
		Geometry3dUtils::SPlane plane;
		Vector3 lineDir;
		Vector3 expected;
		std::function<bool()> testProjection([&]()
		{
			Vector3 actual = Geometry3dUtils::ProjectLineOnPlane(plane, lineDir);
			if (!expected.IsNearlyEqual(actual, 0.00001f))
			{
				UnitTestError(testFlags, TXT("Geometry 3D test failed. When projecting vector %s onto plane %s, the expected projected line should have been %s instead of %s."), lineDir, plane.ReadNormal(), expected, actual);
				return false;
			}

			return true;
		});

		plane.Position = Vector3::ZERO_VECTOR;
		plane.SetNormal(Vector3(0.f, 0.f, 1.f));
		lineDir = Vector3(1.f, 2.f, 3.f);
		expected = Vector3(1.f, 2.f, 0.f);
		if (!testProjection())
		{
			return false;
		}

		plane.Position = Vector3(5.f, 6.f, 80.f); //Shouldn't matter what the plane displacement is since the projected vector is always relative to the plane.
		if (!testProjection())
		{
			return false;
		}

		//Test against non normalized lines
		plane.SetNormal(Vector3(1.f, 1.f, 0.f));
		lineDir = Vector3(0.f, 0.f, 3.f);
		expected = lineDir;
		if (!testProjection())
		{
			return false;
		}

		lineDir = Vector3(1.f, -1.f, 3.f);
		expected = lineDir;
		if (!testProjection())
		{
			return false;
		}

		plane.SetNormal(Vector3(1.f, 1.f, 1.f));
		lineDir = Vector3(1.f, 1.f, 1.f);
		expected = Vector3::ZERO_VECTOR; //lineDir is perpendicular to the plane. Its projection should be a point.
		if (!testProjection())
		{
			return false;
		}

		plane.SetNormal(Vector3(1.f, 1.f, 1.f));
		lineDir = Vector3(3.f, -4.f, 8.f);
		expected = Vector3(0.666667f, -6.333333f, 5.666667f);
		if (!testProjection())
		{
			return false;
		}

		plane.SetNormal(Vector3(4.f, 1.5f, -2.f));
		lineDir = Vector3(-6.f, 8.f, 2.f);
		expected = Vector3(-3.123596f, 9.078652f, 0.561798f);
		if (!testProjection())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Angle Between Lines"));
	{
		Vector3 dirA;
		Vector3 dirB;
		Float expected;
		Float tolerance = 0.01f; //The calculator used to verify the results only had two decimal places.

		std::function<bool()> testAngle([&]()
		{
			Float actual = Geometry3dUtils::CalcAngleBetweenLines(dirA, dirB);
			if (!actual.IsCloseTo(expected, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D test failed. Calculating the angle between directional vector %s and directional vector %s should have returned %s instead of %s."), dirA, dirB, expected, actual);
				return false;
			}

			if (expected.IsCloseTo(0.f) && actual.IsCloseTo(PI_FLOAT))
			{
				expected = PI_FLOAT; //For parallel angles, + or - pi values are ambiguous since directional parallel vectors can face either direction. Change expected to align with actual.
			}

			//The angle shouldn't be affected based on the order.
			Float reversedOrder = Geometry3dUtils::CalcAngleBetweenLines(dirB, dirA);
			if (!actual.IsCloseTo(reversedOrder, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D test failed. The results from calculating the angle between directional vector %s and directional vector %s shouldn't vary based on the parameter order. The original angle is %s. The reversed parameter angle is %s."), dirA, dirB, actual, reversedOrder);
				return false;
			}

			//The angle shouldn't be affected by the magnitude of the vectors.
			Float scaled = Geometry3dUtils::CalcAngleBetweenLines(dirA * 4.5f, dirB * 0.1f);
			if (!actual.IsCloseTo(scaled, tolerance))
			{
				UnitTestError(testFlags, TXT("Geometry 3D test failed. Calculating the angle between directional vector %s and directional vector %s shouldn't be affected when the directional vectors are scaled. Original angle is %s. When the vectors are scaled, the angle is %s."), dirA, dirB, actual, scaled);
				return false;
			}

			return true;
		});

		dirA = Vector3(1.f, 0.f, 0.f);
		dirB = Vector3(1.f, 0.f, 0.f); //parallel
		expected = 0.f;
		if (!testAngle())
		{
			return false;
		}

		dirB = Vector3(0.f, 1.f, 0.f); //perpendicular
		expected = PI_FLOAT * 0.5f;
		if (!testAngle())
		{
			return false;
		}

		dirB = Vector3(0.f, 0.f, -1.f); //Also perpendicular
		if (!testAngle())
		{
			return false;
		}

		dirB = Vector3(0.5f, 0.5f, 0.f); //45 degrees
		expected = 0.785398f;
		if (!testAngle())
		{
			return false;
		}

		dirA = Vector3(1.f, -0.5f, 0.33f);
		dirB = Vector3(10.f, 3.4f, -2.5f);
		expected = 0.9388f;
		if (!testAngle())
		{
			return false;
		}

		dirA = Vector3(100.f, 2.f, -4.f);
		dirB = Vector3(3.5f, -80.f, 6.2f);
		expected = 1.5502f;
		if (!testAngle())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Sphere-Line Intersection"));
	{
		Vector3 sphereCenter;
		Float radius;
		Vector3 linePt;
		Vector3 lineDir;
		bool bBoundedLine;
		bool bExpected;
		std::vector<Vector3> expectedIntersections;

		std::function<bool()> testIntersections([&]()
		{
			std::vector<Vector3> actualIntersections;
			bool bActual = Geometry3dUtils::CalcSphereLineIntersections(sphereCenter, radius, linePt, lineDir, bBoundedLine, OUT actualIntersections);
			if (bActual != bExpected)
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the intersection between sphere (pt: %s, radius: %s) and line (pt: %s, dir: %s, bounded: %s), the function should have returned %s. Instead it returned %s."), sphereCenter, radius, linePt, lineDir, Bool(bBoundedLine), Bool(bExpected), Bool(bActual));
				return false;
			}

			if (expectedIntersections.size() != actualIntersections.size())
			{
				UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the intersection between sphere (pt: %s, radius: %s) and line (pt: %s, dir: %s, bounded: %s), the function should have found %s intersections. Instead it found %s intersections."), sphereCenter, radius, linePt, lineDir, Bool(bBoundedLine), Int(expectedIntersections.size()), Int(actualIntersections.size()));
				return false;
			}

			std::vector<Vector3> search(expectedIntersections);
			while (search.size() > 0)
			{
				bool bFound = false;
				for (const Vector3& actual : actualIntersections)
				{
					if (ContainerUtils::GetLast(search).IsNearlyEqual(actual))
					{
						search.pop_back();
						bFound = true;
						break;
					}
				}

				if (!bFound)
				{
					UnitTestError(testFlags, TXT("Geometry 3D Utils test failed. When calculating the intersection between sphere (pt: %s, radius: %s) and line (pt: %s, dir: %s, bounded: %s), the function should have found intersection %s. Instead it found the following intersections: %s."), sphereCenter, radius, linePt, lineDir, Bool(bBoundedLine), ContainerUtils::GetLast(search), DString::ListToString(actualIntersections));
					return false;
				}
			}

			return true;
		});

		//Run simple perpendicular tests
		sphereCenter = Vector3::ZERO_VECTOR;
		radius = 4.f;
		linePt = Vector3(5.f, 0.f, 0.f);
		lineDir = Vector3(-10.f, 0.f, 0.f);
		bBoundedLine = false;
		bExpected = true;
		expectedIntersections = {Vector3(-4.f, 0.f, 0.f), Vector3(4.f, 0.f, 0.f)};
		if (!testIntersections())
		{
			return false;
		}

		bBoundedLine = true;
		if (!testIntersections())
		{
			return false;
		}

		lineDir = Vector3(-8.f, 0.f, 0.f);
		expectedIntersections = {Vector3(4.f, 0.f, 0.f)};
		if (!testIntersections())
		{
			return false;
		}

		bBoundedLine = false;
		expectedIntersections = {Vector3(-4.f, 0.f, 0.f), Vector3(4.f, 0.f, 0.f)};
		if (!testIntersections())
		{
			return false;
		}

		lineDir = Vector3(1.f, 0.f, 0.f);
		if (!testIntersections())
		{
			return false;
		}

		bBoundedLine = true;
		bExpected = false;
		ContainerUtils::Empty(OUT expectedIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Test tangent line
		linePt = Vector3(5.f, 0.f, 4.f);
		bExpected = false;
		if (!testIntersections())
		{
			return false;
		}

		bBoundedLine = false;
		bExpected = true;
		expectedIntersections = {Vector3(0.f, 0.f, 4.f)};
		if (!testIntersections())
		{
			return false;
		}

		lineDir = Vector3(-5.f, 0.f, 0.f);
		bBoundedLine = true;
		if (!testIntersections())
		{
			return false;
		}

		//Test with displaced sphere
		sphereCenter = Vector3(10.f, -12.f, 15.f);
		radius = 2.f;
		linePt = Vector3(10.f, -8.f, 15.f);
		lineDir = Vector3(0.f, -1.f, 0.f);
		bExpected = false;
		ContainerUtils::Empty(OUT expectedIntersections);
		if (!testIntersections())
		{
			return false;
		}

		bBoundedLine = false;
		bExpected = true;
		expectedIntersections = {Vector3(10.f, -10.f, 15.f), Vector3(10.f, -14.f, 15.f)};
		if (!testIntersections())
		{
			return false;
		}

		//Test the line missing the sphere
		linePt = Vector3(14.f, -12.f, 15.f);
		lineDir = Vector3(0.f, 0.f, 1.f);
		bExpected = false;
		ContainerUtils::Empty(OUT expectedIntersections);
		if (!testIntersections())
		{
			return false;
		}

		//Run angled tests
		//Equation of sphere: (x-4)^2 + (y+3)^2 + (z-3)^2 = 16
		sphereCenter = Vector3(4.f, -3.f, 3.f);
		radius = 4.f;
		//Equation of line: (-8, -9, 10) + p(-1, -1, -1)
		linePt = Vector3(-8.f, -9.f, 10.f);
		lineDir = Vector3(-1.f, -1.f, -1.f);
		bBoundedLine = false;
		bExpected = false;
		ContainerUtils::Empty(OUT expectedIntersections); //Line misses sphere
		if (!testIntersections())
		{
			return false;
		}

		//Move the line to barely intersect with the sphere
		//Equation of line: (-8, -9, 10) + p(1, 0.5, -1)
		linePt = Vector3(-8.f, -9.f, 10.f);
		lineDir = Vector3(1.f, 0.5f, -1.f);
		bExpected = true;
		expectedIntersections = {Vector3(0.8091336f, -4.595433f, 1.190866f), Vector3(2.746422f, -3.626789f, -0.746422f)};
		if (!testIntersections())
		{
			return false;
		}

		lineDir *= 100.f;
		bBoundedLine = true;
		if (!testIntersections())
		{
			return false;
		}

		lineDir.NormalizeInline();
		bBoundedLine = true;
		bExpected = false;
		ContainerUtils::Empty(OUT expectedIntersections);
		if (!testIntersections())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Geometry 3D Utils"));
	return true;
}

bool PhysicsUnitTest::RunSegmentTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Collision Segment Tests"));

	std::function<DString(const CollisionSegment&)> segmentToString([](const CollisionSegment& segment)
	{
		return DString::CreateFormattedString(TXT("Segment[Pos:%s, EndPoint:%s, Scale:%s]"), segment.ReadPosition(), segment.ReadEndPoint(), segment.GetScale());
	});

	SetTestCategory(testFlags, TXT("Aabb"));
	{
		CollisionSegment testSegment;
		Aabb expected;
		std::function<bool()> testAabb([&]()
		{
			const Aabb& actual = testSegment.ReadBoundingBox();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Collision Segment test failed. The %s should have a Aabb of %s. Instead it's %s."), segmentToString(testSegment), expected, actual);
				return false;
			}

			return true;
		});

		testSegment.SetPosition(Vector3(1.f, 0.f, 5.f));
		testSegment.SetScale(1.f);
		testSegment.SetEndPoint(Vector3(3.f, 0.f, 0.f));
		expected.Center = Vector3(2.5f, 0.f, 5.f);
		expected.Depth = 3.f;
		expected.Width = 0.f;
		expected.Height = 0.f;
		if (!testAabb())
		{
			return false;
		}

		testSegment.SetEndPoint(Vector3(3.f, -6.f, 0.f));
		expected.Center = Vector3(2.5f, -3.f, 5.f);
		expected.Depth = 3.f;
		expected.Width = 6.f;
		expected.Height = 0.f;
		if (!testAabb())
		{
			return false;
		}

		testSegment.SetPosition(Vector3(1.f, 0.f, -5.f));
		expected.Center = Vector3(2.5f, -3.f, -5.f);
		if (!testAabb())
		{
			return false;
		}

		testSegment.SetEndPoint(Vector3(3.f, -6.f, 10.f));
		expected.Center = Vector3(2.5f, -3.f, 0.f);
		expected.Height = 10.f;
		if (!testAabb())
		{
			return false;
		}

		testSegment.SetScale(2.f);
		expected.Center = Vector3(4.f, -6.f, 5.f);
		expected.Depth *= 2.f;
		expected.Width *= 2.f;
		expected.Height *= 2.f;
		if (!testAabb())
		{
			return false;
		}

		testSegment.SetPosition(Vector3(14.f, -16.f, 8.f));
		testSegment.SetScale(3.f);
		testSegment.SetEndPoint(Vector3(5.f, 13.f, -2.f));
		expected.Center = Vector3(21.5f, 3.5f, 5.f);
		expected.Depth = 15.f;
		expected.Width = 39.f;
		expected.Height = 6.f;
		if (!testAabb())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		CollisionSegment testSegment;
		Vector3 testPoint;
		bool bExpected;
		std::function<bool()> testEncompassesPoint([&]()
		{
			bool bActual = testSegment.EncompassesPoint(testPoint);
			if (bExpected != bActual)
			{
				DString notText = (bExpected) ? DString::EmptyString : TXT(" NOT");
				UnitTestError(testFlags, TXT("Collision Segment unit test failed! When testing if %s encompasses the point %s, the test should%s have returned true. It suggests otherwise."), segmentToString(testSegment), testPoint, notText);
				return false;
			}

			return true;
		});

		testSegment.SetPosition(Vector3::ZERO_VECTOR);
		testSegment.SetEndPoint(Vector3(1.f, 0.f, 0.f));
		testSegment.SetScale(1.f);
		testPoint = Vector3::ZERO_VECTOR;
		bExpected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(0.5f, 0.f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(1.f, 0.f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(1.1f, 0.f, 0.f);
		bExpected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(-0.1f, 0.f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(0.5f, 0.1f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(0.5f, 0.f, -0.1f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testSegment.SetPosition(Vector3(5.f, -3.f, 2.5f));
		testSegment.SetEndPoint(Vector3(10.f, 5.f, -5.f));
		testSegment.SetScale(2.f);
		testPoint = Vector3(5.f, -3.f, 2.5f);
		bExpected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(15.f, 2.f, -2.5f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(25.f, 7.f, -7.5f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(26.f, 7.f, -7.5f);
		bExpected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(26.f, 7.5f, -8.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(4.f, -3.5f, 3.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(14.f, 2.f, -2.5f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(15.f, 2.5f, -2.5f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPoint = Vector3(15.f, 2.f, -2.f);
		if (!testEncompassesPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Closest Point"));
	{
		CollisionSegment testSegment;
		Vector3 testPoint;
		Vector3 expected;
		Float tolerance = 0.0001f;
		std::function<bool()> testCalcPoint([&]()
		{
			Vector3 actual = testSegment.CalcClosestPoint(testPoint);
			if (!actual.IsNearlyEqual(expected, tolerance))
			{
				UnitTestError(testFlags, TXT("Collision segment test failed! When calculating the closest point on %s, the closest point residing on the segment to %s should have been %s. Instead the closest point was computed to be %s."), segmentToString(testSegment), testPoint, expected, actual);
				return false;
			}

			//The results must reside on the segment.
			if (!Geometry3dUtils::IsOnSegment(testSegment.ReadPosition(), testSegment.GetAbsEndPoint(), actual, tolerance))
			{
				UnitTestError(testFlags, TXT("Collision segment test failed! When calculating the closest point on %s to point %s, the results from that function %s does not reside on the collision segment."), segmentToString(testSegment), testPoint, actual);
				return false;
			}

			return true;
		});

		//Simple test cases
		testSegment.SetPosition(Vector3::ZERO_VECTOR);
		testSegment.SetEndPoint(Vector3(1.f, 0.f, 0.f));
		testSegment.SetScale(2.f);
		testPoint = Vector3(1.f, 0.f, 0.f);
		expected = testPoint;
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(1.75f, 0.f, 0.f);
		expected = testPoint;
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(2.25f, 0.f, 0.f);
		expected = Vector3(2.f, 0.f, 0.f);
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(-1.f, 0.f, 0.f);
		expected = Vector3::ZERO_VECTOR;
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(0.25f, 4.f, 180.f);
		expected = Vector3(0.25f, 0.f, 0.f);
		if (!testCalcPoint())
		{
			return false;
		}

		testSegment.SetPosition(Vector3(1.f, 5.f, -5.f));
		testPoint = Vector3(1.25f, -8.f, 48.f);
		expected = Vector3(1.25f, 5.f, -5.f);
		if (!testCalcPoint())
		{
			return false;
		}

		//Run sloped tests
		testSegment.SetPosition(Vector3(3.f, 2.5f, -4.f));
		testSegment.SetEndPoint(Vector3(2.f, 1.f, 3.f));
		testSegment.SetScale(10.f);
		testPoint = Vector3(13.f, 7.5f, 11.f);
		expected = testPoint;
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(-25.f, -40.f, -35.f);
		expected = testSegment.GetPosition();
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(100.f, 100.f, 100.f);
		expected = Vector3(23.f, 12.5f, 26.f);
		if (!testCalcPoint())
		{
			return false;
		}

		testPoint = Vector3(15.f, 6.f, 20.f);
		expected = Vector3(17.21429, 9.60714f, 17.32143f);
		if (!testCalcPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Collision Segment Tests"));
	return true;
}

bool PhysicsUnitTest::RunSphereTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Collision Sphere Tests"));

	std::function<DString(const CollisionSphere&)> sphereToString([](const CollisionSphere& sphere)
	{
		return DString::CreateFormattedString(TXT("Sphere[Pos:%s, Scale:%s, Radius:%s]"), sphere.ReadPosition(), sphere.GetScale(), sphere.GetRadius());
	});

	SetTestCategory(testFlags, TXT("Aabb"));
	{
		CollisionSphere testSphere;
		Aabb expected;
		std::function<bool()> testAabb([&]()
		{
			testSphere.RefreshBoundingBox();
			const Aabb& actual = testSphere.ReadBoundingBox();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Collision Sphere Test failed. The Aabb for %s does not match the expected Aabb [%s]. Instead it's [%s]."), sphereToString(testSphere), expected, actual);
				return false;
			}

			return true;
		});

		testSphere = CollisionSphere(2.f, Vector3(8.f, -16.f, 12.f), 4.f);
		expected = Aabb(16.f, 16.f, 16.f, Vector3(8.f, -16.f, 12.f));
		if (!testAabb())
		{
			return false;
		}

		Vector3 translation(-1.f, 5.f, 6.f);
		expected.Center += translation;
		testSphere.Translate(translation);
		if (!testAabb())
		{
			return false;
		}

		translation = Vector3(8.f, 20.f, 14.5f);
		expected.Center += translation;
		testSphere.Translate(translation);
		if (!testAabb())
		{
			return false;
		}

		Float scale = 0.5f;
		testSphere.SetRadius(testSphere.GetRadius() * scale);
		expected.Height *= scale;
		expected.Width *= scale;
		expected.Depth *= scale;
		if (!testAabb())
		{
			return false;
		}

		scale = 2.5f;
		testSphere.SetScale(testSphere.GetScale() * scale);
		expected.Height *= scale;
		expected.Width *= scale;
		expected.Depth *= scale;
		if (!testAabb())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		CollisionSphere testSphere;
		Vector3 testPt;
		bool expected;
		std::function<bool()> testEncompassesPoint([&]()
		{
			bool actual = testSphere.EncompassesPoint(testPt);
			if (actual != expected)
			{
				if (expected)
				{
					UnitTestError(testFlags, TXT("Collision Sphere test failed. The %s should encompassing the point %s, but the function says otherwise."), sphereToString(testSphere), testPt);
				}
				else
				{
					UnitTestError(testFlags, TXT("Collision Sphere test failed. The %s should NOT encompassing the point %s, but the function says otherwise."), sphereToString(testSphere), testPt);
				}
			}

			return (actual == expected);
		});

		testSphere = CollisionSphere(10.f, Vector3(4.f, -10.f, 2.5f), 2.f);
		testPt = testSphere.ReadPosition();
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(4.f, 4.f, 4.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(15.f, 0.f, 14.5f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = testSphere.ReadPosition();
		testPt += Vector3(19.f, 0.f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(2.f, 0.f, 0.f);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = testSphere.ReadPosition();
		testPt += Vector3(19.f, 19.f, -19.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(2.f, 2.f, -2.f);
		if (!testEncompassesPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Closest Point"));
	{
		CollisionSphere testSphere;
		Vector3 testPt;
		Vector3 expected;
		std::function<bool()> testCalcClosestPoint([&]()
		{
			Vector3 actual = testSphere.CalcClosestPoint(testPt);
			if (!actual.IsNearlyEqual(expected))
			{
				UnitTestError(testFlags, TXT("Collision Sphere test failed. Caluclating the closest point on %s towards point %s should have been %s. Instead it returned %s."), sphereToString(testSphere), testPt, expected, actual);
				return false;
			}

			return true;
		});

		testSphere = CollisionSphere(4.f, Vector3(1.f, 2.f, -4.f), 2.f);
		testPt = Vector3(-1.f, 1.f, -2.f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(100.f, 2.f, -4.f);
		expected = Vector3(9.f, 2.f, -4.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(20.f, 30.f, 60.f);
		//deltaVectMagnitude = 72.39475119
		//NormalizedDir = (0.262449967, 0.386768371, 0.884041991)
		expected = Vector3(2.0996f, 3.094147f, 7.072336f);
		expected += testSphere.ReadPosition();
		if (!testCalcClosestPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Collision Sphere Tests"));
	return true;
}

bool PhysicsUnitTest::RunCapsuleTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Collision Capsule Tests"));

	std::function<DString(const CollisionCapsule&)> capsuleToString([](const CollisionCapsule& capsule)
	{
		return DString::CreateFormattedString(TXT("Capsule [Pos:%s, Scale:%s, Height:%s, Radius:%s]"), capsule.ReadPosition(), capsule.GetScale(), capsule.GetHeight(), capsule.GetRadius());
	});

	SetTestCategory(testFlags, TXT("Capsule Z Edges"));
	{
		CollisionCapsule testCapsule;
		Float expectedCapsuleLimit;
		Float expectedCylinderLimit;
		std::function<bool()> testZLimit([&]()
		{
			Float actualCapsuleLimit = testCapsule.GetCapsuleZLimit();
			if (!expectedCapsuleLimit.IsCloseTo(actualCapsuleLimit))
			{
				UnitTestError(testFlags, TXT("Collision Capsule test failed! The capsule Z limit for %s should have been %s. Instead it returned %s."), capsuleToString(testCapsule), expectedCapsuleLimit, actualCapsuleLimit);
				return false;
			}

			Float actualCylinderLimit = testCapsule.GetCylinderZEdge();
			if (!expectedCylinderLimit.IsCloseTo(actualCylinderLimit))
			{
				UnitTestError(testFlags, TXT("Collision Capsule test failed! The capsule cylinder's Z limit for %s should have been %s. Instead it returned %s."), capsuleToString(testCapsule), expectedCylinderLimit, actualCylinderLimit);
				return false;
			}

			return true;
		});

		testCapsule.SetPosition(Vector3(45.f, -32.f, 11.8f));
		testCapsule.SetScale(1.f);
		testCapsule.SetRadius(1.f);
		testCapsule.SetHeight(4.f);
		expectedCapsuleLimit = 3.f;
		expectedCylinderLimit = 2.f;
		if (!testZLimit())
		{
			return false;
		}

		testCapsule.SetRadius(6.f);
		expectedCapsuleLimit = 8.f;
		if (!testZLimit())
		{
			return false;
		}

		testCapsule.SetScale(3.f);
		expectedCapsuleLimit = 24.f;
		expectedCylinderLimit = 6.f;
		if (!testZLimit())
		{
			return false;
		}

		testCapsule.SetHeight(2.f);
		expectedCapsuleLimit = 21.f;
		expectedCylinderLimit = 3.f;
		if (!testZLimit())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Capsule to Segment"));
	{
		std::function<DString(const CollisionSegment&)> segmentToString([](const CollisionSegment& segment)
		{
			return DString::CreateFormattedString(TXT("segment [pos:%s, scale:%s, endpt: %s]"), segment.ReadPosition(), segment.GetScale(), segment.ReadEndPoint());
		});

		CollisionCapsule testCapsule;
		CollisionSegment expectedSegment;
		std::function<bool()> testConversion([&]()
		{
			CollisionSegment actualSegment = testCapsule.ToSegment();
			if (!actualSegment.ReadPosition().IsNearlyEqual(expectedSegment.ReadPosition()) || !actualSegment.GetScale().IsCloseTo(expectedSegment.GetScale()) || !actualSegment.ReadEndPoint().IsNearlyEqual(expectedSegment.ReadEndPoint()))
			{
				UnitTestError(testFlags, TXT("Collision Capsule unit test failed! When converting %s to a segment, it should have generated %s instead of %s."), capsuleToString(testCapsule), segmentToString(expectedSegment), segmentToString(actualSegment));
				return false;
			}

			return true;
		});

		testCapsule.SetPosition(Vector3(3.f, -4.f, 5.f));
		testCapsule.SetScale(1.f);
		testCapsule.SetHeight(2.f);
		testCapsule.SetRadius(1.f);
		expectedSegment.SetPosition(Vector3(3.f, -4.f, 4.f));
		expectedSegment.SetScale(1.f);
		expectedSegment.SetEndPoint(Vector3(0.f, 0.f, 2.f));
		if (!testConversion())
		{
			return false;
		}

		testCapsule.SetHeight(4.f);
		expectedSegment.SetPosition(Vector3(3.f, -4.f, 3.f));
		expectedSegment.SetEndPoint(Vector3(0.f, 0.f, 4.f));
		if (!testConversion())
		{
			return false;
		}

		testCapsule.SetRadius(150.f);
		if (!testConversion())
		{
			return false;
		}

		testCapsule.SetScale(3.f);
		expectedSegment.SetPosition(Vector3(3.f, -4.f, -1.f));
		expectedSegment.SetScale(3.f);
		if (!testConversion())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Aabb"));
	{
		CollisionCapsule testCapsule;
		Aabb expected;
		std::function<bool()> testAabb([&]()
		{
			testCapsule.RefreshBoundingBox();
			const Aabb& actual = testCapsule.ReadBoundingBox();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Collision Capsule test failed. The %s bounding box should have been %s. Instead it's %s."), capsuleToString(testCapsule), expected, actual);
				return false;
			}

			return true;
		});

		testCapsule = CollisionCapsule(4.f, Vector3(12.f, -8.f, 16.f), 8.f, 6.f);
		expected = Aabb(48.f, 80.f, 48.f, Vector3(12.f, -8.f, 16.f));
		if (!testAabb())
		{
			return false;
		}

		Vector3 translation(82.2f, -91.6f, 202.25f);
		testCapsule.Translate(translation);
		expected.Center += translation;
		if (!testAabb())
		{
			return false;
		}

		Float scale = 3.5f;
		testCapsule.SetScale(testCapsule.GetScale() * scale);
		expected.Depth *= scale;
		expected.Width *= scale;
		expected.Height *= scale;
		if (!testAabb())
		{
			return false;
		}

		testCapsule.SetHeight(testCapsule.GetHeight() * scale);
		expected.Height = 560.f;
		if (!testAabb())
		{
			return false;
		}

		scale = 0.5f;
		testCapsule.SetRadius(testCapsule.GetRadius() * scale);
		expected.Height = 476.f; //Since radius does affect the total height.
		expected.Depth *= scale;
		expected.Width *= scale;
		if (!testAabb())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		CollisionCapsule testCapsule;
		Vector3 testPt;
		bool expected;
		std::function<bool()> testEncompassesPoint([&]()
		{
			testCapsule.RefreshBoundingBox();
			bool actual = testCapsule.EncompassesPoint(testPt);
			if (actual != expected)
			{
				if (expected)
				{
					UnitTestError(testFlags, TXT("Collision Capsule test failed. The %s should be encompassing %s. The function suggests otherwise."), capsuleToString(testCapsule), testPt);
				}
				else
				{
					UnitTestError(testFlags, TXT("Collision Capsule test failed. The %s should NOT be encompassing %s. The function suggests otherwise."), capsuleToString(testCapsule), testPt);
				}
			}

			return (actual == expected);
		});

		testCapsule = CollisionCapsule(2.f, Vector3(1.f, 2.f, -4.f), 8.f, 4.f);
		testPt = testCapsule.GetPosition();
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(6.f, 0.f, 0.f);
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(3.f, 0.f, 0.f);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = testCapsule.GetPosition();
		testPt += Vector3(0.f, 0.f, 15.f);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(0.f, 0.f, 2.f);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Test right outside the corner of the capsule
		testPt = testCapsule.GetPosition();
		testPt += Vector3(0.f, 0.f, -8); //Set relative to the bottom sphere

		Vector3 offset(1.f, 1.f, -1.f);
		offset.NormalizeInline();
		offset *= 7.f; //Slightly less than the sphere's radius, to move right next to the edge of the capsule
		testPt += offset;
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt += Vector3(2.f, 2.f, -2.f);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Closest Point"));
	{
		CollisionCapsule testCapsule;
		Vector3 testPt;
		Vector3 expected;
		std::function<bool()> testCalcClosestPoint([&]()
		{
			testCapsule.RefreshBoundingBox();
			Vector3 actual = testCapsule.CalcClosestPoint(testPt);
			if (!actual.IsNearlyEqual(expected, 0.001f))
			{
				UnitTestError(testFlags, TXT("Collision Capsule test failed. Calculating the closest point on %s towards point %s should have returned %s. Instead it calculated %s."), capsuleToString(testCapsule), testPt, expected, actual);
				return false;
			}

			return true;
		});

		testCapsule = CollisionCapsule(2.f, Vector3(-2.f, -4.f, -8.f), 12.f, 4.f);
		testPt = testCapsule.GetPosition();
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt += Vector3(3.f, -2.f, 1.5f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = testCapsule.GetPosition();
		testPt += Vector3(-5000.f, 0.f, 0.f);
		expected = Vector3(-10.f, -4.f, -8.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = testCapsule.GetPosition();
		testPt += Vector3(0.f, 0.f, 800.f);
		expected = Vector3(-2.f, -4.f, 12.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		//Find a point on the cylinder
		testPt = testCapsule.GetPosition();
		testPt += Vector3(24.f, 6.f, 5.f);
		expected = Vector3(5.761339f, -2.059715f, -3.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		//Find a point on one of the spheres
		testPt = testCapsule.GetPosition();
		Float cylinderZEdge = testCapsule.GetCylinderZEdge();
		testPt.Z += cylinderZEdge; //set point relative to top sphere
		testPt += Vector3(-32.f, 32.f, 8.f);
		/*
		Uncomment for debugging
		expected = Vector3(-0.696311f, 0.696311f, 0.174078f);
		expected *= (testCapsule.GetRadius() * testCapsule.GetScale()); //Point on the edge of the sphere
		expected += Vector3(0.f, 0.f, cylinderZEdge) + testCapsule.ReadPosition();
		*/
		expected = Vector3(-7.570488f, 1.570488f, 5.392623f);
		if (!testCalcClosestPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Collision Capsule Tests"));
	return true;
}

bool PhysicsUnitTest::RunExtrudePolygonTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Collision Extrude Polygon Tests"));

	std::function<DString(const CollisionExtrudePolygon&)> polygonToString([](const CollisionExtrudePolygon& polygon)
	{
		return DString::CreateFormattedString(TXT("Extrude Polygon[Pos:%s, Scale:%s, Height:%s, Verts:{%s}]"), polygon.ReadPosition(), polygon.GetScale(), polygon.GetExtrudeHeight(), DString::ListToString(polygon.ReadVertices()));
	});

	SetTestCategory(testFlags, TXT("World Space Vertices"));
	{
		CollisionExtrudePolygon testPoly;
		std::vector<Vector2> expected;
		std::function<bool()> testWorldVerts([&]()
		{
			testPoly.RefreshBoundingBox();
			const std::vector<Vector2>& worldVerts = testPoly.ReadWorldSpaceSegments();
			if (expected.size() != worldVerts.size())
			{
				UnitTestError(testFlags, TXT("Collision Extrude Polygon tests failed. The world vertices vector length (%s) does not match the expected length (%s) for %s"), Int(worldVerts.size()), Int(expected.size()), polygonToString(testPoly));
				return false;
			}

			for (size_t i = 0; i < worldVerts.size(); ++i)
			{
				if (!worldVerts.at(i).IsNearlyEqual(expected.at(i)))
				{
					UnitTestError(testFlags, TXT("Collision Extrude Polygon tests failed. The world vertices at index %s does not match the expected value for %s. It should be equal to %s instead of %s."), Int(i), polygonToString(testPoly), expected.at(i), worldVerts.at(i));
					return false;
				}
			}

			return true;
		});

		//Test simple box
		testPoly = CollisionExtrudePolygon(2.f, Vector3(1.f, 2.f, 3.f), 4.f, {Vector2(2.f, 2.f), Vector2(-2.f, 2.f), Vector2(-2.f, -2.f), Vector2(2.f, -2.f)});
		expected = {Vector2(5.f, 6.f), Vector2(-3.f, 6.f), Vector2(-3.f, -2.f), Vector2(5.f, -2.f), Vector2(5.f, 6.f)};
		if (!testWorldVerts())
		{
			return false;
		}

		/*
		P = Position
		9-------0
		|        \
		|         \
		8----7     \
		     |      P------2
		5----6            /
		|                /
		4---------------3
		*/
		testPoly = CollisionExtrudePolygon(2.f, Vector3(-4.f, 10.f, 8.f), 8.f,
		{
			Vector2(-2.f, -5.f), Vector2::ZERO_VECTOR, Vector2(5.f, 0.f), Vector2(3.f, 5.f),
			Vector2(-8.f, 5.f), Vector2(-8.f, 1.f), Vector2(-5.f, 1.f), Vector2(-5.f, -1.f),
			Vector2(-8.f, -1.f), Vector2(-8.f, -5.f)
		});

		expected =
		{
			Vector2(-8.f, 0.f), Vector2(-4.f, 10.f), Vector2(6.f, 10.f), Vector2(2.f, 20.f),
			Vector2(-20.f, 20.f), Vector2(-20.f, 12.f), Vector2(-14.f, 12.f), Vector2(-14.f, 8.f),
			Vector2(-20.f, 8.f), Vector2(-20.f, 0.f), Vector2(-8.f, 0.f)
		};

		if (!testWorldVerts())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Normals"));
	{
		CollisionExtrudePolygon testPoly;
		std::vector<Vector2> expected;
		Float tolerance = 0.0001f;
		std::function<bool()> testNormals([&]()
		{
			const std::vector<Vector2>& actual = testPoly.ReadNormals();
			if (actual.size() != expected.size())
			{
				UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. The size of its Normal vector should have been %s. Instead its size is %s."), Int(expected.size()), Int(actual.size()));
				return false;
			}

			//The Normals' size should always be equal to the size of vertices.size(), and WorldSpaceVertices.size() - 1
			if (actual.size() != testPoly.ReadVertices().size() || actual.size() != testPoly.ReadWorldSpaceSegments().size() - 1)
			{
				UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. The size of its normals (%s) should be equal to its number of vertices (%s). In addition to that, the number of its normals should also be equal to its WorldSpaceSegments' size minus 1 (%s)."), Int(actual.size()), Int(testPoly.ReadVertices().size()), Int(testPoly.ReadWorldSpaceSegments().size() - 1));
				return false;
			}

			//Order matters with this normal vector since it maps to each face in the polygon.
			for (size_t i = 0; i < expected.size(); ++i)
			{
				if (!actual.at(i).CalcDistSquared().IsCloseTo(1.f, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. At index %s, the extrude polygon doesn't contain a normalized normal (%s)."), Int(i), actual.at(i));
					return false;
				}

				if (!expected.at(i).IsNearlyEqual(actual.at(i), tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. At index %s, the extruded polygon's normal is expected to be %s. Instead it's %s."), Int(i), expected.at(i), actual.at(i));
					return false;
				}
			}

			return true;
		});

		/*
		A-------B
		|       |
		|   P   |
		|       |
		D-------C
		*/
		testPoly.SetPosition(Vector3::ZERO_VECTOR);
		testPoly.SetExtrudeHeight(3.f);
		testPoly.SetScale(2.f);
		testPoly.SetVertices(
		{
			Vector2(-10.f, -10.f),
			Vector2(10.f, -10.f),
			Vector2(10.f, 10.f),
			Vector2(-10.f, 10.f)
		});

		expected =
		{
			Vector2(0.f, -1.f),
			Vector2(1.f, 0.f),
			Vector2(0.f, 1.f),
			Vector2(-1.f, 0.f)
		};

		if (!testNormals())
		{
			return false;
		}

		//Have the vertices defined counter clockwise to ensure the Normals still face outward.
		/*
		A-------D
		|       |
		|   P   |
		|       |
		B-------C
		*/
		testPoly.SetVertices(
		{
			Vector2(-10.f, -10.f),
			Vector2(-10.f, 10.f),
			Vector2(10.f, 10.f),
			Vector2(10.f, -10.f)
		});

		expected =
		{
			Vector2(-1.f, 0.f),
			Vector2(0.f, 1.f),
			Vector2(1.f, 0.f),
			Vector2(0.f, -1.f)
		};

		if (!testNormals())
		{
			return false;
		}

		//Test against normals that aren't always axis aligned
		/*
		A
		|\__
		|   \__
		|    P \__
		|          \
		C------------B
		*/
		testPoly.SetVertices(
		{
			Vector2(-10.f, -5.f),
			Vector2(10.f, 5.f),
			Vector2(-10.f, 5.f)
		});

		expected = 
		{
			Vector2::Normalize(Vector2(1.f, -2.f)),
			Vector2(0.f, 1.f),
			Vector2(-1.f, 0.f)
		};

		if (!testNormals())
		{
			return false;
		}

		//Test against a complicated shape.
		/*
		    A-------B
		   /         \
		  /           \
		 I      P      C
		 |      F      |
		 |     / \     |
		 |    /   \    |
		 H---G     E---D
		*/
		testPoly.SetVertices(
		{
			Vector2(-8.f, -10.f), //A
			Vector2(8.f, -10.f),
			Vector2(10.f, -8.f), //C
			Vector2(10.f, 10.f),
			Vector2(5.f, 10.f), //E
			Vector2(0.f, 5.f),
			Vector2(-5.f, 10.f), //G
			Vector2(-10.f, 10.f),
			Vector2(-10.f, -8.f) //I
		});

		expected =
		{
			Vector2(0.f, -1.f),
			Vector2::Normalize(Vector2(1.f, -1.f)),
			Vector2(1.f, 0.f),
			Vector2(0.f, 1.f),
			Vector2::Normalize(Vector2(-1.f, 1.f)),
			Vector2::Normalize(Vector2(1.f, 1.f)),
			Vector2(0.f, 1.f),
			Vector2(-1.f, 0.f),
			Vector2::Normalize(Vector2(-1.f, -1.f))
		};

		if (!testNormals())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Aabb"));
	{
		CollisionExtrudePolygon testPoly;
		Aabb expected;
		std::function<bool()> testAabb([&]()
		{
			testPoly.RefreshBoundingBox();
			const Aabb& actual = testPoly.ReadBoundingBox();
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Collision Extrude Polygon tests failed. The bounding box for %s should have been %s. Instead it's %s."), polygonToString(testPoly), expected, actual);
				return false;
			}

			return true;
		});

		//Polygon that's a 'rotated' cube
		testPoly = CollisionExtrudePolygon(2.f, Vector3(2.f, 4.f, 6.f), 6.f, {Vector2(5.f, 0.f), Vector2(0.f, 5.f), Vector2(-5.f, 0.f), Vector2(0.f, -5.f)});
		expected = Aabb(20.f, 12.f, 20.f, Vector3(2.f, 4.f, 12.f));
		if (!testAabb())
		{
			return false;
		}

		Vector3 deltaTranslation(Vector3(6.f, -2.5f, -1.1f));
		testPoly.Translate(deltaTranslation);
		expected.Center += deltaTranslation;
		if (!testAabb())
		{
			return false;
		}

		testPoly.SetExtrudeHeight(testPoly.GetExtrudeHeight() + 3.f);
		expected.Height += 6.f;
		expected.Center.Z += 3.f;
		if (!testAabb())
		{
			return false;
		}

		/*
		1------2
		|      |
		|      |
		|      P------4
		|              \
		|               \
		0----------------5
		*/
		testPoly.SetVertices({Vector2(-5.f, 5.f), Vector2(-5.f, -5.f), Vector2(0.f, -5.f), Vector2::ZERO_VECTOR, Vector2(5.f, 0.f), Vector2(8.f, 5.f)});
		testPoly.SetPosition(Vector3(12.f, 6.f, 2.f));
		expected.Depth = 26.f;
		expected.Width = 20.f;
		expected.Center = Vector3(15.f, 6.f, 11.f);
		if (!testAabb())
		{
			return false;
		}

		/*
		1------2
		|      |
		|      |
		|      P------4
		|              \
		|               \
		0-----8   6------5
		       \ /
			    7
		*/
		std::vector<Vector2> verts = testPoly.ReadVertices();
		//Add the three new verts
		verts.push_back(Vector2(2.5f, 5.f));
		verts.push_back(Vector2(1.f, 6.f));
		verts.push_back(Vector2(-1.f, 5.f));
		testPoly = CollisionExtrudePolygon(3.f, Vector3(-4.f, 6.f, 5.f), 4.f, verts);
		expected.Depth = 39.f;
		expected.Width = 33.f;
		expected.Height = 12.f;
		expected.Center = Vector3(0.5f, 7.5f, 11.f);
		if (!testAabb())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encompasses Point"));
	{
		CollisionExtrudePolygon testPoly;
		Vector3 testPt;
		bool expected;
		std::function<bool()> testEncompassesPoint([&]()
		{
			testPoly.RefreshBoundingBox();
			bool actual = testPoly.EncompassesPoint(testPt);
			if (actual != expected)
			{
				if (expected)
				{
					UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. The %s should be encompassing %s. The function returned otherwise."), polygonToString(testPoly), testPt);
				}
				else
				{
					UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. The %s should NOT be encompassing %s. The function returned otherwise."), polygonToString(testPoly), testPt);
				}
			}

			return (actual == expected);
		});

		//simple rect test
		testPoly = CollisionExtrudePolygon(2.f, Vector3(4.f, -5.f, 8.f), 8.f, {Vector2(4.f, 6.f), Vector2(-4.f, 6.f), Vector2(-4.f, -6.f), Vector2(4.f, -6.f)});
		testPt = testPoly.GetPosition();
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, -5.f, 7.f);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, -5.f, 23.f);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt.Z += 1.5f;
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(11.f, -5.f, 8.f);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt.X += 1.5f;
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, -16.f, 8.f);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt.Y -= 1.5f;
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		/*
		P = Position
		A----B     2-----3
		|    |     |      \
		|    0-----1       \
		9------P            \
		       |             4
		7------8             |
		|                    |
		6--------------------5
		*/

		testPoly = CollisionExtrudePolygon(2.f, Vector3(8.f, 10.f, -5.f), 4.f,
		{
			//Each row will define 4 vertices
			Vector2(-2.f, -2.f), Vector2(3.f, -2.f), Vector2(3.f, -5.f), Vector2(5.f, -5.f),
			Vector2(10.f, 1.f), Vector2(10.f, 5.f), Vector2(-5.f, 5.f), Vector2(-5.f, 2.f),
			Vector2(0.f, 2.f), Vector2(0.f, 0.f), Vector2(-5.f, 0.f), Vector2(-5.f, -5.f),
			Vector2(-2.f, -5.f)
		});

		Float height = 2.f;
		testPt = Vector3(1.f, 2.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Test moving the point horizontally that intersects segments 9A, B0, 21, & 34
		testPt = Vector3(-12.f, 4.f, height);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, 4.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(8.f, 4.f, height);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(16.f, 4.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(20.f, 4.f, height);
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Test against double counting. Pick a point that's horizontally aligned to Pt 4 and vertically aligned to Pt 3.
		testPt = Vector3(18.f, 12.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Move the point along Y-axis. It should move between the following segments: AB, 9P, 78, 65
		testPt = Vector3(2.f, -10.f, height);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, 4.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, 11.f, height);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, 16.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, 22.f, height);
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Test point residing on a vertical segment 45
		testPt = Vector3(28.f, 16.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt.X += 0.5f;
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}

		//Test point on horizontal segment 23
		testPt = Vector3(18.f, 0.f, height);
		expected = true;
		if (!testEncompassesPoint())
		{
			return false;
		}

		testPt.Y -= 0.5f;
		expected = false;
		if (!testEncompassesPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calculate Closest Point"));
	{
		CollisionExtrudePolygon testPoly;
		Vector3 testPt;
		Vector3 expected;
		std::function<bool()> testCalcClosestPoint([&]()
		{
			testPoly.RefreshBoundingBox();
			Vector3 actual = testPoly.CalcClosestPoint(testPt);
			if (!actual.IsNearlyEqual(expected, 0.01f))
			{
				UnitTestError(testFlags, TXT("Collision Extrude Polygon test failed. Calculating the closest point from %s to %s should have resulted in %s. Instead it returned %s."), testPt, polygonToString(testPoly), expected, actual);
				return false;
			}

			return true;
		});

		//Simple rect test
		testPoly = CollisionExtrudePolygon(2.f, Vector3(4.f, 6.f, -10.f), 6.f, {Vector2(5.f, 8.f), Vector2(-5.f, 8.f), Vector2(-5.f, -8.f), Vector2(5.f, -8.f)});
		testPt = testPoly.GetPosition();
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, 6.f, -5000.f);
		expected = Vector3(4.f, 6.f, -10.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, 6.f, 10000.f);
		expected = Vector3(4.f, 6.f, 2.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(13.f, 6.f, 2.f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.X += 2.f;
		expected = Vector3(14.f, 6.f, 2.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, 21.f, 2.f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.Y += 2.f;
		expected = Vector3(4.f, 22.f, 2.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(-5.f, 6.f, 2.f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.X -= 2.f;
		expected = Vector3(-6.f, 6.f, 2.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, -9.f, 2.f);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(4.f, -11.f, 2.f);
		expected = Vector3(4.f, -10.f, 2.f);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		/*
		0----1       4----5
		|    |       |    |
		|    |       |    |
		|    2-------3    |
		|        P        |
		|       / \       |
		9------8   7------6
		*/

		testPoly = CollisionExtrudePolygon(2.f, Vector3(2.f, -2.f, 5.f), 4.f,
		{
			//Each row has four vertices
			Vector2(-10.f, -10.f), Vector2(-5.f, -10.f), Vector2(-5.f, -5.f), Vector2(5.f, -5.f),
			Vector2(5.f, -10.f), Vector2(10.f, -10.f), Vector2(10.f, 10.f), Vector2(2.f, 10.f),
			Vector2(0.f, 0.f), Vector2(-2.f, 10.f), Vector2(-10.f, 10.f)
		});
		Float height = 8.f;
		testPt = Vector3(3.f, -4.f, height);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.Z = -100.f;
		expected.Z = testPoly.ReadPosition().Z;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.Z = 100.f;
		expected.Z = 13.f;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		//Move the testPt from left to right, crossing the segments: 90, 12, 34, 56
		testPt = Vector3(-20.f, -20.f, height);
		expected = Vector3(-18.f, -20.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		//Test height one more time, this time the point being outside the polygon.
		testPt.Z = -100.f;
		expected.Z = testPoly.ReadPosition().Z;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt.Z = 100.f;
		expected.Z = 13.f;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(-14.f, -18.f, height);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(-6.f, -18.f, height);
		expected = Vector3(-8.f, -18.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(2.f, -14.f, height);
		expected = Vector3(2.f, -12.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(8.f, -18.f, height);
		expected = Vector3(12.f, -18.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}


		testPt = Vector3(18.f, -18.f, height);
		expected = testPt;
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(26.f, -18.f, height);
		expected = Vector3(22.f, -18.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		testPt = Vector3(12.f, 20.f, height);
		expected = Vector3(12.f, 18.f, height);
		if (!testCalcClosestPoint())
		{
			return false;
		}

		//Test point that is closest to the sloped segment 7P
		//Segment on polygon: -5x
		//Segment that connects the testPt to the segment: 0.2x-8.2
		testPt = Vector3(4.f, 14.f, height);
		expected = Vector3(5.154f, 13.77f, height); //Intersects at point in local space and no scale: (1.577, 7.885)
		if (!testCalcClosestPoint())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Collision Extrude Polygon Tests"));
	return true;
}

bool PhysicsUnitTest::RunCollisionUtilsTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Collision Utils Tests"));

	std::function<DString(const CollisionSegment&)> segmentToString([](const CollisionSegment& segment)
	{
		return DString::CreateFormattedString(TXT("Segment [pos: %s, scale: %s, end pt: %s]"), segment.ReadPosition(), segment.GetScale(), segment.ReadEndPoint());
	});

	std::function<DString(const CollisionSphere&)> sphereToString([](const CollisionSphere& sphere)
	{
		return DString::CreateFormattedString(TXT("Sphere [pos: %s, scale: %s, radius: %s]"), sphere.ReadPosition(), sphere.GetScale(), sphere.GetRadius());
	});

	std::function<DString(const CollisionCapsule&)> capsuleToString([](const CollisionCapsule& capsule)
	{
		return DString::CreateFormattedString(TXT("Capsule [pos: %s, scale: %s, height: %s, radius: %s]"), capsule.ReadPosition(), capsule.GetScale(), capsule.GetHeight(), capsule.GetRadius());
	});

	std::function<DString(const CollisionExtrudePolygon&)> polygonToString([](const CollisionExtrudePolygon& polygon)
	{
		return DString::CreateFormattedString(TXT("Polygon [pos: %s, scale: %s, height: %s, vertices: %s]"), polygon.ReadPosition(), polygon.GetScale(), polygon.GetExtrudeHeight(), DString::ListToString(polygon.ReadVertices()));
	});

	SetTestCategory(testFlags, TXT("Overlap: Segment-Segment"));
	{
		CollisionSegment a;
		CollisionSegment b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The segments %s and %s should %s be overlapping. The function suggests otherwise."), segmentToString(a), segmentToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should return the same result regardless of instance owner."));
				return false;
			}

			return true;
		});

		//Simple perpendicular tests
		a.SetPosition(Vector3(-1.f, 0.f, 0.f));
		a.SetScale(2.f);
		a.SetEndPoint(Vector3(0.75f, 0.f, 0.f));
		b.SetPosition(Vector3(0.f, -1.f, 0.f));
		b.SetScale(2.f);
		b.SetEndPoint(Vector3(0.f, 0.75f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, -1.f, 0.1f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, -2.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 0.1f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		//Parallel tests
		b.SetPosition(Vector3(-1.f, 0.f, 0.f));
		b.SetEndPoint(Vector3(0.75f, 0.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(-1.1f, 0.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		b.SetEndPoint(Vector3(-0.75f, 0.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(1.6f, 0.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetEndPoint(Vector3(0.75f, 0.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Run an intersection test
		//b intersects a at a's half point (6, -4, 5)
		a.SetPosition(Vector3(5.f, -3.f, 4.f));
		a.SetScale(2.f);
		a.SetEndPoint(Vector3(1.f, -1.f, 1.f));
		b.SetPosition(Vector3(3.f, -1.f, 6.f));
		b.SetScale(4.f);
		b.SetEndPoint(Vector3(3.f, -3.f, -1.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(1.f);
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(0.9f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(2.f);
		b.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(0.9f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(-2.f);
		b.SetScale(4.f);
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(2.f);
		b.SetPosition(Vector3(3.1f, -1.f, 6.f));
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(3.f, -1.f, 6.f));
		b.SetEndPoint(Vector3(3.f, -3.1f, -1.f));
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Segment-Sphere"));
	{
		CollisionSegment a;
		CollisionSphere b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The %s and %s should %s be overlapping. The function suggests otherwise."), segmentToString(a), sphereToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should return the same result regardless of instance owner."));
				return false;
			}

			return true;
		});

		//Move the sphere around the segment
		a.SetPosition(Vector3(-1.f, 0.f, 0.f));
		a.SetScale(1.f);
		a.SetEndPoint(Vector3(2.f, 0.f, 0.f));
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetScale(1.f);
		b.SetRadius(2.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 3.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(2.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 4.5f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(3.5f, 0.f, 0.f));
		b.SetScale(1.f);
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(2.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(-3.f, 0.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test the sphere moving around an angled segment away from the origin.
		a.SetPosition(Vector3(5.f, -8.f, 10.f));
		a.SetScale(2.f);
		a.SetEndPoint(Vector3(-1.f, 1.f, -2.f));
		b.SetPosition(Vector3(4.f, -6.5f, 7.f)); //dist is ~0.456
		b.SetRadius(0.2f);
		b.SetScale(2.f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(2.5f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(20.f);
		b.SetPosition(Vector3(3.f, -2.f, 4.f)); //dist is ~3.05
		b.SetRadius(3.f);
		b.SetScale(1.f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetRadius(3.1f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetRadius(2.f);
		b.SetScale(2.f);
		if (!testOverlap())
		{
			return false;
		}

		//Test against segment end points
		a.SetPosition(Vector3(4.f, -3.f, 2.f));
		a.SetEndPoint(Vector3(2.f, 4.f, -1.f));
		a.SetScale(2.f);
		//A's end points are:  (4, -3, 2), (8, 5, 0)
		b.SetPosition(Vector3(4.f, -3.f, 2.f));
		b.SetRadius(1.f);
		b.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(5.f, -5.f, 2.5f)); //Dist ~= 2.29
		b.SetScale(1.f);
		b.SetRadius(2.3f);
		if (!testOverlap())
		{
			return false;
		}

		b.SetRadius(0.75f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(8.f, 5.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(10.f, 9.f, -1.f)); //Dist ~= 4.583
		b.SetScale(1.f);
		b.SetRadius(4.5f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(1.1f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Segment-Capsule"));
	{
		CollisionSegment a;
		CollisionCapsule b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The %s and %s should %s be overlapping. The function suggests otherwise."), segmentToString(a), capsuleToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should return the same result regardless of instance owner."));
				return false;
			}

			return true;
		});

		//Have segment around the center, move capsule around segment.
		a.SetPosition(Vector3(0.f, -1.f, 0.f));
		a.SetEndPoint(Vector3(0.f, 1.f, 0.f));
		a.SetScale(2.f);
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetHeight(2.f);
		b.SetRadius(1.f);
		b.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(1.5f, 0.f, 0.f));
		b.SetHeight(4.f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 1.5f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 2.5f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 0.f, 1.5f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 0.f, -2.5f));
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(0.f, 0.f, -3.5f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Position the capsule at a random point, move the segment around the capsule.
		b.SetPosition(Vector3(4.f, 3.f, -5.f));
		b.SetScale(2.f);
		b.SetRadius(2.f); //X-Y end points: (8, 3), (4, 7), (-4, 3), (4, -1)
		b.SetHeight(4.f); //vertical end points (4, 3, 3), (4, 3, -13)
		a.SetPosition(Vector3(6.f, 5.f, -1.f));
		a.SetEndPoint(Vector3(3.f, 5.f, 10.f));
		a.SetScale(0.01f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(9.f, 10.f, 11.f));
		a.SetEndPoint(Vector3(-3.f, -5.f, -10.f));
		a.SetScale(0.25f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(1.f, 0.5f, -8.f));
		a.SetEndPoint(Vector3(-2.f, 2.f, 4.f));
		a.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Nudge segment so that it's now out of range from the capsule.
		a.SetPosition(Vector3(1.f, 0.3f, -8.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Have a horizontal segment orbit around the capsule.
		a.SetPosition(Vector3(7.9f, 3.f, -5.f));
		a.SetEndPoint(Vector3(1.f, 0.f, 0.f));
		a.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(8.1f, 3.f, -5.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 3.f, 2.9f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 3.f, 3.1f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(-1.1f, 3.f, -5.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(-0.9f, 3.f, -5.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 3.f, -12.9f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 3.f, -13.1f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Y-axis
		a.SetPosition(Vector3(3.9f, 7.1f, -2.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 6.9f, -1.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, 6.9f, 1.f)); //Be on the outside corner of the upper sphere
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, -0.9f, -7.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(3.9f, -1.1f, -7.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Segment-Extrude Polygon"));
	{
		CollisionSegment a;
		CollisionExtrudePolygon b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The %s and %s should %s be overlapping. The function suggests otherwise."), segmentToString(a), polygonToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should return the same result regardless of instance owner."));
				return false;
			}

			return true;
		});

		b.SetPosition(Vector3(1.f, 2.f, 3.f));
		b.SetScale(1.f);
		b.SetExtrudeHeight(10.f);
		/*
		      A
		    /   \
		  /       \
		 D    P    B
		  \       /
		    \   /
		      C
		*/
		//A squared polygon at an angle to test against segment-box without relying on AABB.
		b.SetVertices(
		{
			Vector2(0.f, -10.f),
			Vector2(10.f, 0.f),
			Vector2(0.f, 10.f),
			Vector2(-10.f, 0.f)
		});

		a.SetPosition(Vector3(0.f, 2.f, 3.5f));
		a.SetScale(1.f);
		a.SetEndPoint(Vector3(2.f, 0.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Move the segment around the square. Test each edge and clip each corner.
		//Clip corner 1
		a.SetPosition(Vector3(0.f, -7.f, 3.5f));
		a.SetEndPoint(Vector3(3.f, 0.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.Translate(Vector3(0.f, -2.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test segment AB
		a.SetPosition(Vector3(7.f, -4.f, 3.5f));
		a.SetEndPoint(Vector3(5.f, 5.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(-2.f, 2.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test clipping B
		a.SetPosition(Vector3(10.5f, 3.f, 3.5f));
		a.SetEndPoint(Vector3(0.f, -2.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(0.f, 2.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test against segment BC
		a.SetPosition(Vector3(7.f, 8.f, 3.5f));
		a.SetEndPoint(Vector3(-1.f, -1.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(1.5f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Clip vertex C
		a.SetPosition(Vector3(2.f, 11.5f, 3.5f));
		a.SetEndPoint(Vector3(-2.f, 0.f, 4.f));
		a.SetScale(1.f);
		if (!testOverlap())
		{
			return false;
		}

		a.Translate(Vector3(0.f, 0.f, 11.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test against segment CD
		a.SetPosition(Vector3(-4.5f, 9.5f, 6.f));
		a.SetEndPoint(Vector3(0.5f, -2.f, -2.f));
		if (!testOverlap())
		{
			return false;
		}

		a.Translate(Vector3(0.f, -1.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Clip against vertex D
		a.SetPosition(Vector3(-8.5f, 3.f, 3.5f));
		a.SetEndPoint(Vector3(0.f, -2.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetScale(0.2f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test against segment DA
		a.SetPosition(Vector3(-4.5f, -3.5f, 4.f));
		a.SetEndPoint(Vector3(1.f, -1.f, -1.f));
		a.SetScale(1.f);
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(1.f, 1.f, -1.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test clipping the top of the polygon
		a.SetPosition(Vector3(-2.f, 1.f, 15.f));
		a.SetEndPoint(Vector3(5.f, 5.f, -5.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(5.f, 5.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test the segment clipping under the polygon
		a.SetPosition(Vector3(4.f, 5.f, 2.f));
		a.SetEndPoint(Vector3(-2.f, -2.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(-2.f, -2.f, 1.5f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test the segment skewing through the top and bottom planes. None of its end points are inside the polygon
		a.SetPosition(Vector3(4.f, 5.f, -1.f));
		a.SetEndPoint(Vector3(-2.f, -2.f, 30.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(4.f, 5.f, 14.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(2.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Ensure the vertices are scaled. Clip against vertex B
		a.SetPosition(Vector3(20.5f, 3.f, 4.f));
		a.SetEndPoint(Vector3(0.f, -2.f, 0.f));
		b.SetScale(2.f);
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(1.75f);
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Test against a complicated polygon.
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetScale(1.f);
		b.SetExtrudeHeight(10.f);
		/*
		 A--B    D-E
		 | H \   | |
		 | |\ \  | |
		 | | \P\ | |
		 | |  \ \| |
		 | |   \ C |
		 | |    \  |
		 J-I     G-F
		 */
		b.SetVertices(
		{
			Vector2(-200.f, -200.f), //A
			Vector2(-150.f, -200.f), //B
			Vector2(150.f, 150.f), //C
			Vector2(150.f, -200.f), //D
			Vector2(200.f, -200.f), //E
			Vector2(200.f, 200.f), //F
			Vector2(150.f, 200.f), //G
			Vector2(-150.f, -150.f), //H
			Vector2(-150.f, 200.f), //I
			Vector2(-200.f, 200.f) //J
		});

		//Create vertical segment, intersecting the bottom of the left side of N.
		a.SetPosition(Vector3(-175.f, 0.f, -10.f));
		a.SetEndPoint(Vector3(0.f, 0.f, 15.f));
		a.SetScale(1.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Shift to the gap between left side and diagonal.
		a.SetPosition(Vector3(-100.f, 100.f, -10.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Shift segment to vertically cross through the center of the N
		a.SetPosition(Vector3(0.f, 0.f, -10.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(0.f, 0.f, 1000.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(0.f, 0.f, 5.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(0.f, 0.f, 1.f));
		if (!testOverlap())
		{
			return false;
		}

		//Shift the segment to the gap between the diagonal and the right side of the N
		a.SetPosition(Vector3(100.f, -100.f, 5.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Shift to the right side of the N
		a.SetPosition(Vector3(175.f, -100.f, 5.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(0.f, 0.f, 32.f));
		if (!testOverlap())
		{
			return false;
		}

		a.Translate(Vector3(0.f, 0.f, 10.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Diagonal segment intersect segment HI
		a.SetPosition(Vector3(-125.f, 0.f, 4.f));
		a.SetEndPoint(Vector3(-50.f, -25.f, 2.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(-24.f, -25.f, 2.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(-26.f, -25.f, 4.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(-26.f, -25.f, 100.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Diagonal segment intersect segments BC and GH
		a.SetPosition(Vector3(-100.f, 100.f, 8.f));
		a.SetEndPoint(Vector3(200.f, -200.f, -4.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(200.f, 200.f, -4.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Clip the top left corner of segment EF
		a.SetPosition(Vector3(190.f, 0.f, 11.f));
		a.SetEndPoint(Vector3(20.f, 0.f, -2.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetEndPoint(Vector3(20.f, 0.f, -0.9f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Intersect segment DE
		a.SetPosition(Vector3(175.f, -220.f, 8.f));
		a.SetEndPoint(Vector3(0.f, 40.f, -4.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetScale(1.5f); //Causing vertex D to be at position (225, -300)
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Sphere-Sphere"));
	{
		CollisionSphere a;
		CollisionSphere b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			a.RefreshBoundingBox();
			b.RefreshBoundingBox();
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The spheres %s and %s should %s be overlapping. The function suggests otherwise."), sphereToString(a), sphereToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should return the same result regardless of instance owner."));
				return false;
			}

			return true;
		});

		a = CollisionSphere(2.f, Vector3::ZERO_VECTOR, 4.f);
		b = CollisionSphere(4.f, Vector3::ZERO_VECTOR, 8.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(10.f, 0.f, 0.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(10.f, 0.f, 39.f));
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(10.f, 0.f, 40.5f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(128.28f, -128.28f, 100.f));
		b.SetPosition(Vector3(100.f, -100.f, 100.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		a.SetPosition(Vector3(129.f, -129.f, 100.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Sphere-Capsule"));
	{
		CollisionSphere sphere;
		CollisionCapsule capsule;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			sphere.RefreshBoundingBox();
			capsule.RefreshBoundingBox();
			bool actual = sphere.OverlapsWith(&capsule);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The shapes %s and %s should %s be overlapping. The function suggests otherwise."), sphereToString(sphere), capsuleToString(capsule), conditionalStr);
				return false;
			}

			bool otherCheck = capsule.OverlapsWith(&sphere);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should not depend on the parameter order."));
				return false;
			}

			return true;
		});

		sphere = CollisionSphere(4.f, Vector3::ZERO_VECTOR, 2.f);
		capsule = CollisionCapsule(2.f, Vector3::ZERO_VECTOR, 8.f, 4.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		capsule.SetPosition(Vector3(20.f, -30.f, 50.f));
		sphere.SetPosition(Vector3(35.f, -30.f, 50.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(37.f, -30.f, 50.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(8.687f, -18.687f, 55.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(8.f, -18.f, 55.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(9.f, -19.f, 55.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(20.f, -41.3137f, 30.6863f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.Translate(Vector3(0.f, 0.f, -1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(20.f, -41.3137f, 69.3137f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.Translate(Vector3(0.f, 0.f, 1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Sphere-Extrude Polygon"));
	{
		CollisionSphere sphere;
		CollisionExtrudePolygon polygon;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			sphere.RefreshBoundingBox();
			polygon.RefreshBoundingBox();
			bool actual = sphere.OverlapsWith(&polygon);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The shapes %s and %s should %s be overlapping. The function suggests otherwise."), sphereToString(sphere), polygonToString(polygon), conditionalStr);
				return false;
			}

			bool otherCheck = polygon.OverlapsWith(&sphere);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should not depend on the parameter order."));
				return false;
			}

			return true;
		});

		//Simple rect test
		sphere = CollisionSphere(4.f, Vector3(5.f, -5.f, 3.f), 2.f);
		polygon = CollisionExtrudePolygon(2.f, Vector3(5.f, -5.f, 3.f), 4.f, {Vector2(5.f, 8.f), Vector2(-5.f, 8.f), Vector2(-5.f, -8.f), Vector2(5.f, -8.f)});
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(5.f, -5.f, 18.95f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.Translate(Vector3(0.f, 0.f, 1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(5.f, -5.f, -4.95f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		sphere.Translate(Vector3(0.f, 0.f, -1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//For each rect edge, test the sphere position that's just barely in range, and barely out of range. On top of that (pun intended), test Z edges.
		Float midHeight = 7.f;
		std::vector<Vector3> testSpots({Vector3(22.95f, -5.f, midHeight), Vector3(5.f, 18.95f, midHeight), Vector3(-12.95f, -5.f, midHeight), Vector3(5.f, -28.95f, midHeight)});
		for (size_t i = 0; i < testSpots.size(); ++i)
		{
			sphere.SetPosition(testSpots.at(i));
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			sphere.SetPosition(sphere.ReadPosition() * 1.1f);
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			sphere.SetPosition(testSpots.at(i));
			Vector3 newPos = sphere.GetPosition();
			newPos.Z = 3.f;
			sphere.SetPosition(newPos);
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			sphere.Translate(Vector3(0.f, 0.f, -1.f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			newPos = sphere.GetPosition();
			newPos.Z = 11.f;
			sphere.SetPosition(newPos);
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			sphere.Translate(Vector3(0.f, 0.f, 1.f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}
		}

		/*
		0------1
		|      |
		|   c  |b  a
		|      |
		|      P------2
		|             |
		|             |
		|             |
		4-------------3
		*/
		polygon = CollisionExtrudePolygon(2.f, Vector3(1.f, 2.f, 3.f), 4.f,
		{
			Vector2(-5.f, -5.f), Vector2(0.f, -5.f), Vector2(0.f, 0.f),
			Vector2(5.f, 0.f), Vector2(5.f, 5.f), Vector2(-5.f, 5.f)
		});
		sphere = CollisionSphere(1.f, Vector3(4.f, -1.f, 6.f), 2.f); //Test the concave spot at point a
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		sphere.SetPosition(Vector3(2.f, -1.f, 6.f)); //Test at spot b
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test at spot c
		sphere.SetPosition(Vector3(-1.5f, -0.5f, 6.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Capsule-Capsule"));
	{
		CollisionCapsule a;
		CollisionCapsule b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			a.RefreshBoundingBox();
			b.RefreshBoundingBox();
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The capsules %s and %s should %s be overlapping. The function suggests otherwise."), capsuleToString(a), capsuleToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should not depend on the parameter order."));
				return false;
			}

			return true;
		});

		a = CollisionCapsule(2.f, Vector3(2.f, 3.f, 4.f), 4.f, 8.f);
		b = CollisionCapsule(4.f, Vector3(2.f, 3.f, 4.f), 4.f, 2.f);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(25.95f, 3.f, 4.f));
		if (!testOverlap())
		{
			return false;
		}

		b.Translate(Vector3(0.f, 2.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(25.95f, 3.f, 16.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.Translate(Vector3(0.f, 0.f, 2.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.SetPosition(Vector3(2.f, 3.f, 39.75f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.Translate(Vector3(0.f, 0.f, 1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Capsule-Extrude Polygon"));
	{
		CollisionCapsule capsule;
		CollisionExtrudePolygon polygon;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			capsule.RefreshBoundingBox();
			polygon.RefreshBoundingBox();
			bool actual = capsule.OverlapsWith(&polygon);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The shapes %s and %s should %s be overlapping. The function suggests otherwise."), capsuleToString(capsule), polygonToString(polygon), conditionalStr);
				return false;
			}

			bool otherCheck = polygon.OverlapsWith(&capsule);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should not depend on the parameter order."));
				return false;
			}

			return true;
		});

		//Simple rect test
		capsule = CollisionCapsule(2.f, Vector3(2.f, 3.f, -2.f), 8.f, 4.f);
		polygon = CollisionExtrudePolygon(2.f, Vector3(2.f, 3.f, -2.f), 4.f, {Vector2(4.f, 6.f), Vector2(-4.f, 6.f), Vector2(-4.f, -6.f), Vector2(4.f, -6.f)});
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		Float height = 0.f;
		//Test against each XY edge
		std::vector<Vector3> testPos({Vector3(17.95f, 3.f, height), Vector3(2.f, 22.95f, height), Vector3(-13.95f, 3.f, height), Vector3(2.f, -16.95f, height)});
		for (size_t i = 0; i < testPos.size(); ++i)
		{
			capsule.SetPosition(testPos.at(i));
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			capsule.SetPosition(testPos.at(i) * 1.1f);
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			//Test z-pos
			capsule.SetPosition(testPos.at(i) + Vector3(0.f, 0.f, 14.1f));
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			capsule.Translate(Vector3(0.f, 0.f, 1.5f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			capsule.SetPosition(testPos.at(i) + Vector3(0.f, 0.f, -10.1f));
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			capsule.Translate(Vector3(0.f, 0.f, -1.5f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}
		}

		/*
		  0----1
		 / 5--4 \
		D /    3-2
		|P
		C \    8-9
		 \ 6--7  /
		  B-----A
		*/
		polygon = CollisionExtrudePolygon(2.f, Vector3(1.f, 2.f, 3.f), 4.f,
		{
			Vector2(2.f, -5.f), Vector2(8.f, -5.f), Vector2(10.f, -2.f), Vector2(8.f, -2.f),
			Vector2(6.f, -4.f), Vector2(4.f, -4.f), Vector2(0.f, 0.f), Vector2(4.f, 4.f),
			Vector2(6.f, 4.f), Vector2(8.f, 2.f), Vector2(10.f, 2.f), Vector2(8.f, 5.f),
			Vector2(2.f, 5.f), Vector2(-2.f, 2.f), Vector2(-2.f, -2.f)
		});
		capsule = CollisionCapsule(0.25f, Vector3::ZERO_VECTOR, 8.f, 4.f);
		height = 5.f;

		//Test the spot inside the C
		capsule.SetPosition(Vector3(6.f, 2.f, height));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Move up where it's just out of range between segment 45
		capsule.SetPosition(Vector3(11.f, -4.5f, height));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		capsule.Translate(Vector3(0.f, -1.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Move capsule in between segments 23, and 89
		capsule.SetPosition(Vector3(19.f, 4.5f, height));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		capsule.Translate(Vector3(0.f, 1.f, 0.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Move capsule between points DPC
		capsule.SetPosition(Vector3(-3.f, 2.f, height));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		capsule.Translate(Vector3(-1.5f, 0.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		//Move capsule in middle of plane 67AB
		capsule.SetPosition(Vector3(11.f, 11.f, height));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test the vertical tips of the capsule.
		Vector3 newPos(capsule.GetPosition());
		newPos.Z = 12.95f;
		capsule.SetPosition(newPos);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		capsule.Translate(Vector3(0.f, 0.f, 1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		newPos = capsule.GetPosition();
		newPos.Z = 1.f;
		capsule.SetPosition(newPos);
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		capsule.Translate(Vector3(0.f, 0.f, -1.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Overlap: Extrude Polygon-Extrude Polygon"));
	{
		CollisionExtrudePolygon a;
		CollisionExtrudePolygon b;
		bool expected;
		std::function<bool()> testOverlap([&]()
		{
			a.RefreshBoundingBox();
			b.RefreshBoundingBox();
			bool actual = a.OverlapsWith(&b);
			if (actual != expected)
			{
				DString conditionalStr = (expected) ? TXT("") : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. The Extrude Polygons %s and %s should %s be overlapping. The function suggests otherwise."), polygonToString(a), polygonToString(b), conditionalStr);
				return false;
			}

			bool otherCheck = b.OverlapsWith(&a);
			if (actual != otherCheck)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. The OverlapsWith function should not depend on the parameter order."));
				return false;
			}

			return true;
		});

		//Simple rect tests
		a = CollisionExtrudePolygon(2.f, Vector3(3.f, 6.f, 9.f), 4.f, {Vector2(2.f, 6.f), Vector2(-2.f, 6.f), Vector2(-2.f, -6.f), Vector2(2.f, -6.f)});
		b = CollisionExtrudePolygon(4.f, Vector3(3.f, 6.f, 9.f), 10.f, {Vector2(3.f, 1.f), Vector2(-3.f, 1.f), Vector2(-3.f, -1.f), Vector2(3.f, -1.f)});
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		//Test each edge-to-edge intersection
		std::vector<Vector3> testPos({Vector3(18.95f, 6.f, 9.f), Vector3(3.f, 21.95f, 9.f), Vector3(-12.95f, 6.f, 9.f), Vector3(3.f, -9.95f, 9.f)});
		for (const Vector3& pos : testPos)
		{
			//First test the XY plane
			b.SetPosition(pos);
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			b.SetPosition(b.ReadPosition() * 1.1f);
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			//Next test the polygons on top of the other
			Vector3 newPos(pos);
			newPos.Z = 16.95f;
			b.SetPosition(newPos);
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			b.Translate(Vector3(0.f, 0.f, 1.f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}

			newPos = b.GetPosition();
			newPos.Z = -30.95f;
			b.SetPosition(newPos);
			expected = true;
			if (!testOverlap())
			{
				return false;
			}

			b.Translate(Vector3(0.f, 0.f, -1.f));
			expected = false;
			if (!testOverlap())
			{
				return false;
			}
		}

		//Complex polygon test
		/*
		A
		0--------1
		|        |
		|  3-----2
		| P
		|  4-----5
		|        |
		7--------6
		*/
		a = CollisionExtrudePolygon(2.f, Vector3(-1.f, 1.f, 2.f), 4.f,
		{
			Vector2(-4.f, -10.f), Vector2(10.f, -10.f), Vector2(10.f, -5.f), Vector2(4.f, -5.f),
			Vector2(0.f, 0.f), Vector2(4.f, 5.f), Vector2(10.f, 5.f), Vector2(10.f, 10.f),
			Vector2(-4.f, 10.f)
		});

		/*
		B
		            I
			       /|
			      / J
		  G------H   \
		 F    P       A
		  E------D   /
		          \ B
			       \|
			        C
		*/
		b = CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 6.f,
		{
			Vector2(10.f, 0.f), Vector2(8.f, 2.f), Vector2(8.f, 10.f), Vector2(5.f, 5.f),
			Vector2(-15.f, 5.f), Vector2(-20.f, 0.f), Vector2(-15.f, -5.f), Vector2(5.f, -5.f),
			Vector2(8.f, -10.f), Vector2(8.f, -2.f)
		});

		//Move B in the middle of the concave section of A.
		b.SetPosition(Vector3(20.5f, 0.f, 2.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}

		b.Translate(Vector3(-1.5f, 0.f, 0.f));
		expected = true; //F should be encompassed
		if (!testOverlap())
		{
			return false;
		}

		//Move B behind A so that vertex A is encompassed
		b.SetPosition(Vector3(-18.95f, 1.f, 2.f));
		expected = true;
		if (!testOverlap())
		{
			return false;
		}

		b.Translate(Vector3(-1.5f, 0.f, 0.f));
		expected = false;
		if (!testOverlap())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	//Run CalcCollision util functions

	SetTestCategory(testFlags, TXT("Calc Collision: Segment-Segment"));
	{
		CollisionSegment a;
		CollisionSegment b;
		bool bExpectCollision;
		Vector3 expectedImpact;
		std::function<bool()> testCollision([&]()
		{
			CollisionUtils::SCollisionInfo collisionInfo;
			bool bActualCollision = CollisionUtils::CalcCollision(&a, &b, OUT collisionInfo);
			if (bActualCollision != bExpectCollision)
			{
				DString conditionalStr = (bExpectCollision) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. When calculating the collision between %s and %s, they should %s have collided, but the function suggests otherwise."), segmentToString(a), segmentToString(b), conditionalStr);
				return false;
			}

			if (bActualCollision)
			{
				if (!expectedImpact.IsNearlyEqual(collisionInfo.ImpactLocation))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision location %s does not match the expected impact %s."), segmentToString(a), segmentToString(b), collisionInfo.ImpactLocation, expectedImpact);
					return false;
				}

				//Don't test against normals since segment-segment collision normals are arbitrary.
			}

			CollisionUtils::SCollisionInfo reversedCollision;
			bool bReversed = CollisionUtils::CalcCollision(&b, &a, OUT reversedCollision);
			if (bReversed != bActualCollision)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should detect the collision accurate regardless of the order of parameters."), segmentToString(a), segmentToString(b));
				return false;
			}

			//Impact location should be the same regardless of the order.
			if (bActualCollision && !reversedCollision.ImpactLocation.IsNearlyEqual(collisionInfo.ImpactLocation))
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the same impact location regardless of the parameter order. The original impact location %s, the reversed location is %s."), segmentToString(a), segmentToString(b), collisionInfo.ImpactLocation, reversedCollision.ImpactLocation);
				return false;
			}

			return true;
		});

		a.SetPosition(Vector3(-1.f, 0.f, 0.f));
		a.SetEndPoint(Vector3(2.f, 0.f, 0.f));
		b.SetPosition(Vector3(0.f, -1.f, 0.f));
		b.SetEndPoint(Vector3(0.f, 2.f, 0.f));
		bExpectCollision = true;
		expectedImpact = Vector3::ZERO_VECTOR;
		if (!testCollision())
		{
			return false;
		}

		b.Translate(Vector3(0.f, 0.f, 0.1f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		b.Translate(Vector3(0.25f, 0.f, -0.1f));
		bExpectCollision = true;
		expectedImpact = Vector3(0.25f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(0.25f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		a.SetPosition(Vector3(3.f, 4.f, 2.f));
		a.SetEndPoint(Vector3(1.f, 1.f, 1.f)); //Move by 2 units to expectedImpact
		a.SetScale(1.f);
		b.SetPosition(Vector3(13.f, -6.f, 0.f));
		b.SetEndPoint(Vector3(-2.f, 3.f, 1.f)); //Move by 4 units to expectedImpact
		expectedImpact = Vector3(5.f, 6.f, 4.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(1.9f);
		b.SetScale(3.9f);
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(2.1f);
		b.SetScale(4.1f);
		bExpectCollision = true;
		if (!testCollision())
		{
			return false;
		}

		a.Translate(Vector3(0.1f, 0.f, 0.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calc Collision: Segment-Sphere"));
	{
		CollisionSegment a;
		CollisionSphere b;
		bool bExpectCollision;
		Vector3 expectedImpact;
		Vector3 expectedNormal;
		Float tolerance = 0.0001f; //Rounded to the fifth decimal place
		std::function<bool()> testCollision([&]()
		{
			CollisionUtils::SCollisionInfo collisionInfo;
			bool bActualCollision = CollisionUtils::CalcCollision(&a, &b, OUT collisionInfo);
			if (bActualCollision != bExpectCollision)
			{
				DString conditionalStr = (bExpectCollision) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. When calculating the collision between %s and %s, they should %s have collided, but the function suggests otherwise."), segmentToString(a), sphereToString(b), conditionalStr);
				return false;
			}

			if (bActualCollision)
			{
				if (!expectedImpact.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision location %s does not match the expected impact %s."), segmentToString(a), sphereToString(b), collisionInfo.ImpactLocation, expectedImpact);
					return false;
				}

				if (!expectedNormal.IsNearlyEqual(collisionInfo.ReadNormal(), tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision normal %s does not match the expected normal %s."), segmentToString(a), sphereToString(b), collisionInfo.ReadNormal(), expectedNormal);
					return false;
				}
			}

			CollisionUtils::SCollisionInfo reversedCollision;
			bool bReversed = CollisionUtils::CalcCollision(&b, &a, OUT reversedCollision);
			if (bReversed != bActualCollision)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should detect the collision accurate regardless of the order of parameters."), segmentToString(a), sphereToString(b));
				return false;
			}

			if (bActualCollision)
			{
				//Impact location should be the same regardless of the order.
				if (!reversedCollision.ImpactLocation.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the same impact location regardless of the parameter order. The original impact location %s, the reversed location is %s."), segmentToString(a), sphereToString(b), collisionInfo.ImpactLocation, reversedCollision.ImpactLocation);
					return false;
				}

				//Normals should be reversed when swapping param order
				Vector3 reversedNormal = collisionInfo.GetNormal() * -1.f;
				if (!reversedCollision.ReadNormal().IsNearlyEqual(reversedNormal, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the reversed impact normal when swapping parameter order. The calculated impact normal %s, the expected normal is %s."), segmentToString(a), sphereToString(b), reversedCollision.ReadNormal(), reversedNormal);
					return false;
				}
			}

			return true;
		});

		a.SetPosition(Vector3(-5.f, 0.f, 0.f));
		a.SetEndPoint(Vector3(10.f, 0.f, 0.f));
		a.SetScale(1.f);
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetRadius(2.f);
		b.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(-2.f, 0.f, 0.f);
		expectedNormal = Vector3(-1.f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(0.5f);
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(0.25f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		a.SetPosition(Vector3(-2.5f, 0.f, 2.f)); //Tangent collision
		a.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(0.f, 0.f, 2.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		a.SetPosition(Vector3(-1.f, 0.f, 0.f)); //start inside the sphere, end point is outside
		expectedImpact = Vector3(2.f, 0.f, 0.f);
		expectedNormal = Vector3(1.f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		a.SetPosition(Vector3(-1.f, 0.f, -0.1f));
		a.SetScale(0.2f); //Completely inside the sphere
		expectedImpact = a.GetPosition();
		expectedNormal = Vector3::Normalize(-a.GetEndPoint());
		if (!testCollision())
		{
			return false;
		}

		//Have angled segment approach the sphere, but doesn't collide (passes AABB test, but fails sphere segment test)
		a.SetPosition(Vector3(4.f, 4.f, 4.f));
		a.SetEndPoint(Vector3::Normalize(Vector3(-1.f, -1.f, -1.f)));
		a.SetScale(4.8f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		a.SetScale(5.f);
		bExpectCollision = true;
		expectedImpact = Vector3(1.1547f, 1.1547f, 1.1547f);
		expectedNormal = Vector3::Normalize(expectedImpact - b.ReadPosition());
		if (!testCollision())
		{
			return false;
		}

		a.Translate(Vector3(3.f, 2.f, 1.f));
		b.Translate(Vector3(3.f, 2.f, 1.f));
		expectedImpact += Vector3(3.f, 2.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		//Run case where the segment intersects the sphere from its end point, but it doesn't cross the mid section due to length limits.
		a.SetPosition(Vector3(0.f, 0.f, 5.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -3.f));
		a.SetScale(1.f);
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetRadius(3.f);
		b.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(0.f, 0.f, 3.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		//Run case where the segment intersects the sphere from its end point, but the start position is beyond its 'closest point'.
		a.SetPosition(Vector3(0.f, 0.f, -1.f));
		expectedImpact = Vector3(0.f, 0.f, -3.f);
		expectedNormal = Vector3(0.f, 0.f, -1.f);
		if (!testCollision())
		{
			return false;
		}

		//This is the bets 3D calculator I've seen!! https://www.geogebra.org/3d   I'm using that to verify the test results!

		//Equation of line: (10,0,0) + p(0.5, 2, 1)
		a.SetPosition(Vector3(5.f, -20.f, -10.f));
		a.SetEndPoint(Vector3(0.5f, 2.f, 1.f));
		a.SetScale(10.f);
		//Equation of sphere: (x-10)^2 + y^2 + (z-2)^2 = 16
		b.SetPosition(Vector3(10.f, 0.f, 2.f));
		b.SetRadius(4.f);
		b.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(9.41092f, -2.35632f, -1.17816f);
		expectedNormal = Vector3::Normalize(expectedImpact - b.ReadPosition());
		if (!testCollision())
		{
			return false;
		}

		//Move segment to be inside the sphere. The impact should be the exit point now.
		a.SetPosition(Vector3(10.f, 0.f, 0.f));
		expectedImpact = Vector3(10.97003f, 3.88013f, 1.94007f);
		expectedNormal = Vector3::Normalize(expectedImpact - b.ReadPosition());
		if (!testCollision())
		{
			return false;
		}

		//Move the segment to be outside the sphere
		a.SetPosition(Vector3(15.f, 20.f, 10.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Equation of line: (3.04719, -3.27817, -0.91549) + p(-5.33333, 4.0404, -12.12121)
		a.SetPosition(Vector3(3.04719f, -3.27817f, -0.91549f));
		a.SetEndPoint(Vector3(5.33333f, -4.0404f, 12.12121f)); //Reverse direction to have the ray point towards the sphere
		a.SetScale(1.f);
		//Equation of sphere: (x-10)^2 + (y+5)^2 + (z-15)^2 = 36
		b.SetPosition(Vector3(10.f, -5.f, 15.f));
		b.SetRadius(6.f);
		b.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(7.74122f, -6.83426f, 9.7527f);
		expectedNormal = Vector3::Normalize(expectedImpact - b.ReadPosition());
		if (!testCollision())
		{
			return false;
		}

		//Reverse the direction since we want to point towards the sphere.
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Point back to the sphere
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		a.SetScale(0.5f); //Reduce the magnitude of direction so the sphere is out of reach.
		if (!testCollision())
		{
			return false;
		}
		
		//Move the segment to inside the sphere. This time the impact point should be on the other side of the sphere.
		a.SetPosition(Vector3(9.713853f, -8.32867f, 14.236023f));
		a.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(11.53831f, -9.71084f, 18.38253f);
		expectedNormal = Vector3::Normalize(expectedImpact - b.ReadPosition());
		if (!testCollision())
		{
			return false;
		}

		//Move the segment beyond the sphere
		a.SetPosition(Vector3(12.38052f, -10.34887f, 20.29663f)); //Position + (Direction * 1.75)
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calc Collision: Segment-Capsule"));
	{
		CollisionSegment a;
		CollisionCapsule b;
		bool bExpectCollision;
		Vector3 expectedImpact;
		Vector3 expectedNormal;
		Float tolerance = 0.003f; //Rounded to the third decimal place
		std::function<bool()> testCollision([&]()
		{
			CollisionUtils::SCollisionInfo collisionInfo;
			bool bActualCollision = CollisionUtils::CalcCollision(&a, &b, OUT collisionInfo);
			if (bActualCollision != bExpectCollision)
			{
				DString conditionalStr = (bExpectCollision) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. When calculating the collision between %s and %s, they should %s have collided, but the function suggests otherwise."), segmentToString(a), capsuleToString(b), conditionalStr);
				return false;
			}

			if (bActualCollision)
			{
				if (!expectedImpact.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision location %s does not match the expected impact %s."), segmentToString(a), capsuleToString(b), collisionInfo.ImpactLocation, expectedImpact);
					return false;
				}

				if (!expectedNormal.IsNearlyEqual(collisionInfo.ReadNormal(), tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision normal %s does not match the expected normal %s."), segmentToString(a), capsuleToString(b), collisionInfo.ReadNormal(), expectedNormal);
					return false;
				}
			}

			CollisionUtils::SCollisionInfo reversedCollision;
			bool bReversed = CollisionUtils::CalcCollision(&b, &a, OUT reversedCollision);
			if (bReversed != bActualCollision)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should detect the collision accurate regardless of the order of parameters."), segmentToString(a), capsuleToString(b));
				return false;
			}

			if (bActualCollision)
			{
				//Impact location should be the same regardless of the order.
				if (!reversedCollision.ImpactLocation.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the same impact location regardless of the parameter order. The original impact location %s, the reversed location is %s."), segmentToString(a), capsuleToString(b), collisionInfo.ImpactLocation, reversedCollision.ImpactLocation);
					return false;
				}

				//Normals should be reversed when swapping param order
				Vector3 reversedNormal = collisionInfo.GetNormal() * -1.f;
				if (!reversedCollision.ReadNormal().IsNearlyEqual(reversedNormal, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the reversed impact normal when swapping parameter order. The calculated impact normal %s, the expected normal is %s."), segmentToString(a), capsuleToString(b), reversedCollision.ReadNormal(), reversedNormal);
					return false;
				}
			}

			return true;
		});

		//Run simple perpendicular tests
		a.SetPosition(Vector3(15.f, 2.f, 3.f));
		a.SetEndPoint(Vector3(-1.f, 0.f, 0.f));
		a.SetScale(20.f);
		b.SetPosition(Vector3(1.f, 2.f, 3.f));
		b.SetRadius(4.f);
		b.SetHeight(5.f);
		b.SetScale(2.f);
		bExpectCollision = true;
		expectedImpact = Vector3(9.f, 2.f, 3.f);
		expectedNormal = Vector3(1.f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		a.Translate(Vector3(0.f, 0.f, 2.f));
		expectedImpact = Vector3(9.f, 2.f, 5.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment starts inside moving out from the capsule
		a.SetPosition(Vector3(1.f, 2.f, 3.f));
		expectedImpact = Vector3(-7.f, 2.f, 3.f);
		expectedNormal = Vector3(-1.f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment starts outside the capsule, moving away
		a.SetPosition(Vector3(-8.f, 2.f, 3.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Segment approaching cylinder but no intersection
		a.SetPosition(Vector3(9.f, 10.f, -1.f));
		a.SetEndPoint(Vector3(-2.f, -2.f, 3.f));
		a.SetScale(1.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Long segment misses the cylinder
		a.SetPosition(Vector3(0.f, 15.f, 4.f));
		a.SetEndPoint(Vector3(15.f, -15.f, -2.f));
		if (!testCollision())
		{
			return false;
		}

		//Segment starts at the bottom sphere, moving out
		a.SetPosition(Vector3(-1.f, 2.f, -3.f));
		a.SetEndPoint(Vector3(-1.f, 0.f, 0.f));
		a.SetScale(20.f);
		bExpectCollision = true;
		expectedImpact = Vector3(-6.937f, 2.f, -3.f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(1.f, 2.f, -2.f));
		if (!testCollision())
		{
			return false;
		}

		a.SetPosition(Vector3(1.f, 10.f, -3.f));
		a.SetEndPoint(Vector3(0.f, -1.f, 0.f));
		expectedImpact = Vector3(1.f, 9.937f, -3.f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(1.f, 2.f, -2.f));
		if (!testCollision())
		{
			return false;
		}

		//Approaching bottom sphere, but no intersection
		a.SetPosition(Vector3(-7.f, -6.f, -10.f));
		a.SetEndPoint(Vector3(2.f, 2.f, 2.f));
		a.SetScale(1.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Tangent to the bottom sphere
		a.SetPosition(Vector3(-7.f,-6.f, -10.f));
		a.SetEndPoint(Vector3(1.f, 1.f, 0.f));
		a.SetScale(20.f);
		bExpectCollision = true;
		expectedImpact = Vector3(1.f, 2.f, -10.f);
		expectedNormal = Vector3(0.f, 0.f, -1.f);
		if (!testCollision())
		{
			return false;
		}

		a.Translate(Vector3(0.f, 0.f, -0.25f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Segment approaching top sphere, but doesn't intersect
		a.SetPosition(Vector3(-7.f, 10.f, 15.f));
		a.SetEndPoint(Vector3(4.f, -4.f, 0.f));
		a.SetScale(1.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Segment is a tangent to the top sphere.
		a.SetPosition(Vector3(-7.f, 10.f, 16.f));
		a.SetScale(20.f);
		bExpectCollision = true;
		expectedImpact = Vector3(1.f, 2.f, 16.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		//Pierce through the top sphere.
		a.SetPosition(Vector3(1.f, -8.f, 9.f));
		a.SetEndPoint(Vector3(0.f, 20.f, 0.f));
		a.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(1.f, -5.937f, 9.f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(1.f, 2.f, 8.f));
		if (!testCollision())
		{
			return false;
		}

		//Reverse the segment
		a.SetPosition(Vector3(1.f, 11.f, 9.f));
		a.SetEndPoint(Vector3(0.f, -20.f, 0.f));
		expectedImpact = Vector3(1.f, 9.937f, 9.f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(1.f, 2.f, 8.f));
		if (!testCollision())
		{
			return false;
		}

		//Start inside the top sphere, moving out
		a.SetPosition(Vector3(1.f, 5.f, 9.f));
		a.SetEndPoint(Vector3(0.f, 10.f, 0.f));
		if (!testCollision())
		{
			return false;
		}

		a.SetEndPoint(Vector3(0.f, -20.f, 0.f));
		expectedImpact = Vector3(1.f, -5.937f, 9.f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(1.f, 2.f, 8.f));
		if (!testCollision())
		{
			return false;
		}

		//Test capsule encompassing the segment
		a.SetPosition(Vector3(3.f, 2.f, -2.f));
		a.SetEndPoint(Vector3(-2.f, -1.f, 5.f));
		expectedImpact = a.GetPosition();
		expectedNormal = Vector3::Normalize(-a.GetEndPoint());
		if (!testCollision())
		{
			return false;
		}

		//Segment over the capsule, approaches but not intersection.
		a.SetPosition(Vector3(6.f, 7.f, 17.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -2.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Segment over the capsule, pierces through the top, end inside the sphere.
		a.SetPosition(Vector3(1.f, 2.f, 17.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -2.f));
		bExpectCollision = true;
		expectedImpact = Vector3(1.f, 2.f, 16.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment over capsule, end segment inside the cylinder.
		a.SetPosition(Vector3(1.f, 2.f, 5.f));
		a.SetEndPoint(Vector3(0.f, 0.f, 12.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment over capsule, end segment inside the bottom sphere
		a.SetPosition(Vector3(1.f, 2.f, 17.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -25.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment over capsule, and completely pierce through the capsule through top and bottom
		a.SetPosition(Vector3(1.f, 2.f, 17.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -28.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		expectedImpact = Vector3(1.f, 2.f, -10.f);
		expectedNormal = Vector3(0.f, 0.f, -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment under the capsule, end segment inside top sphere
		a.SetPosition(Vector3(1.f, 2.f, -11.f));
		a.SetEndPoint(Vector3(0.f, 0.f, 19.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment under the capsule, end segment inside the cylinder
		a.SetPosition(Vector3(1.f, 2.f, -11.f));
		a.SetEndPoint(Vector3(0.f, 0.f, 15.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment under the capsule, end segment inside the bottom sphere
		a.SetPosition(Vector3(1.f, 2.f, -11.f));
		a.SetEndPoint(Vector3(0.f, 0.f, 3.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment under the capsule. It approaches the bottom sphere, but it does not intersect it.
		a.SetPosition(Vector3(-5.f, 8.f, -10.f));
		a.SetEndPoint(Vector3(0.f, 0.f, -2.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Run angled tests (verified using the 3D calculator)
		//Equation of the line: (11, -5, 10) + p(-19.5, 13, -12)
		a.SetPosition(Vector3(11.f, -5.f, 10.f));
		a.SetEndPoint(Vector3(-19.5f, 13.f, -12.f));
		a.SetScale(1.f);
		//Equation of the cylinder part of the capsule: (x+3.5)^2 + (y-4)^2 = 9
		//Equation of top sphere: (x+3.5)^2 + (y-4)^2 + (z+6)^2 = 9
		//Equation of bottom sphere (x+3.5)^2 + (y-4)^2 + (z-10)^2 = 9
		b.SetPosition(Vector3(-3.5f, 4.f, 2.f));
		b.SetScale(1.f);
		b.SetRadius(3.f);
		b.SetHeight(16.f);
		bExpectCollision = true;
		expectedImpact = Vector3(-0.7392f, 2.82613f, 2.77588f);
		expectedNormal = Vector3::Normalize((expectedImpact - b.GetPosition()) * Vector3(1.f, 1.f, 0.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		expectedImpact = Vector3(-5.64542f, 6.09695f, -0.24333f);
		expectedNormal = Vector3::Normalize((expectedImpact - b.GetPosition()) * Vector3(1.f, 1.f, 0.f));
		if (!testCollision())
		{
			return false;
		}

		//Shorten length of segment (ends inside the cylinder)
		a.SetScale(0.2823f);
		if (!testCollision())
		{
			return false;
		}

		//Shorten length again, this time not reaching the cylinder.
		a.SetScale(0.1f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}
		
		//Raise segment so that it intersects through the top of the capsule.
		a.SetPosition(Vector3(-8.5f, 8.f, 12.f));
		a.SetEndPoint(Vector3(19.5f, -13.f, -2.f));
		a.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(-5.21772f, 5.81182f, 11.66336f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(-3.5f, 4.f, 10.f));
		if (!testCollision())
		{
			return false;
		}

		//Invert segment
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		expectedImpact = Vector3(-0.96167f, 2.97445f, 11.22684f);
		expectedNormal = Vector3::Normalize(expectedImpact - Vector3(-3.5f, 4.f, 10.f));
		if (!testCollision())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Calc Collision: Segment-Extrude Polygon"));
	{
		CollisionSegment a;
		CollisionExtrudePolygon b;
		bool bExpectCollision;
		Vector3 expectedImpact;
		Vector3 expectedNormal;
		Float tolerance = 0.003f; //Rounded to the third decimal place
		std::function<bool()> testCollision([&]()
		{
			CollisionUtils::SCollisionInfo collisionInfo;
			bool bActualCollision = CollisionUtils::CalcCollision(&a, &b, OUT collisionInfo);
			if (bActualCollision != bExpectCollision)
			{
				DString conditionalStr = (bExpectCollision) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Collision Utils test failed. When calculating the collision between %s and %s, they should %s have collided, but the function suggests otherwise."), segmentToString(a), polygonToString(b), conditionalStr);
				return false;
			}

			if (bActualCollision)
			{
				if (!expectedImpact.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision location %s does not match the expected impact %s."), segmentToString(a), polygonToString(b), collisionInfo.ImpactLocation, expectedImpact);
					return false;
				}

				if (!expectedNormal.IsNearlyEqual(collisionInfo.ReadNormal(), tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Although it successfully detected a collision between %s and %s, the resulting collision normal %s does not match the expected normal %s."), segmentToString(a), polygonToString(b), collisionInfo.ReadNormal(), expectedNormal);
					return false;
				}
			}

			CollisionUtils::SCollisionInfo reversedCollision;
			bool bReversed = CollisionUtils::CalcCollision(&b, &a, OUT reversedCollision);
			if (bReversed != bActualCollision)
			{
				UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should detect the collision accurate regardless of the order of parameters."), segmentToString(a), polygonToString(b));
				return false;
			}

			if (bActualCollision)
			{
				//Impact location should be the same regardless of the order.
				if (!reversedCollision.ImpactLocation.IsNearlyEqual(collisionInfo.ImpactLocation, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the same impact location regardless of the parameter order. The original impact location %s, the reversed location is %s."), segmentToString(a), polygonToString(b), collisionInfo.ImpactLocation, reversedCollision.ImpactLocation);
					return false;
				}

				//Normals should be reversed when swapping param order
				Vector3 reversedNormal = collisionInfo.GetNormal() * -1.f;
				if (!reversedCollision.ReadNormal().IsNearlyEqual(reversedNormal, tolerance))
				{
					UnitTestError(testFlags, TXT("Collision Utils test failed. Calculating the collision between %s and %s should produce the reversed impact normal when swapping parameter order. The calculated impact normal %s, the expected normal is %s."), segmentToString(a), polygonToString(b), reversedCollision.ReadNormal(), reversedNormal);
					return false;
				}
			}

			return true;
		});

		/*
		A-------B
		|       |
		|       |
		|       P------C
		|              |
		|              |
		E--------------D
		*/
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetScale(2.f);
		b.SetExtrudeHeight(5.f);
		b.SetVertices(
		{
			Vector2(-5.f, -5.f), //A
			Vector2(0.f, -5.f), //B
			Vector2::ZERO_VECTOR, //Position
			Vector2(5.f, 0.f), //C
			Vector2(5.f, 5.f), //D
			Vector2(-5.f, 5.f), //E
		});

		//Segment is above plane PCDE. Moving down, but doesn't intersect the mesh.
		a.SetPosition(Vector3(5.f, 5.f, 15.f));
		a.SetEndPoint(Vector3(-2.f, 0.f, -1.f));
		a.SetScale(3.f);
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		//Segment is above plane PCDE. Moving down, it intersects through the top plane.
		a.SetEndPoint(Vector3(-2.f, 0.f, -4.f));
		a.SetScale(1.5f);
		bExpectCollision = true;
		expectedImpact = Vector3(2.5f, 5.f, 10.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return true;
		}

		//Extend segment so that it pierces through the extruded polygon.
		a.SetScale(5.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment under the polygon, intersects the bottom plane ABP
		a.SetPosition(Vector3(-5.f, -5.f, -5.f));
		a.SetEndPoint(Vector3(2.f, -2.f, 10.f));
		b.SetPosition(Vector3(0.f, 0.f, 10.f));
		expectedImpact = Vector3(-2.f, -8.f, 10.f);
		expectedNormal = Vector3(0.f, 0.f, -1.f);
		if (!testCollision())
		{
			return false;
		}

		//Intersect through AB and top plane corner
		a.SetPosition(Vector3(-5.f, -11.f, 18.f));
		a.SetEndPoint(Vector3(0.f, 1.f, 1.f));
		a.SetScale(3.f);
		expectedImpact = Vector3(-5.f, -10.f, 19.f);
		expectedNormal = Vector3(0.f, -1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//Invert segment (impact will be at the top plane instead)
		a.SetPosition(a.GetAbsEndPoint());
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		expectedImpact = Vector3(-5.f, -9.f, 20.f);
		expectedNormal = Vector3(0.f, 0.f, 1.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment intersects BP and AB
		a.SetPosition(Vector3(1.f, -4.f, 11.f));
		a.SetEndPoint(Vector3(-1.f, -1.f, 1.f));
		expectedImpact = Vector3(0.f, -5.f, 12.f);
		expectedNormal = Vector3(1.f, 0.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//Invert segment. This time the impact should be on AB.
		a.SetPosition(Vector3(-6.f, -11.f, 18.f));
		a.SetEndPoint(a.GetEndPoint() * -1.f);
		expectedImpact = Vector3(-5.f, -10.f, 17.f);
		expectedNormal = Vector3(0.f, -1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//The mesh completely encompass the segment
		a.SetPosition(Vector3(2.f, 2.f, 12.f));
		a.SetEndPoint(Vector3(-2.f, 0.f, 4.f));
		a.SetScale(1.f);
		expectedImpact = a.GetPosition();
		expectedNormal = Vector3::Normalize(-a.ReadEndPoint());
		if (!testCollision())
		{
			return false;
		}

		//Vertical segment in the gap between vertices BPC. Shouldn't collide with anything.
		a.SetPosition(Vector3(2.f, -8.f, 23.f));
		a.SetEndPoint(Vector3(6.f, 6.f, -20.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}

		/*
		A--------B
		|        |
		|    D---C
		|     \
		|    P  >E
		|     /
		|    F---G
		|        |
		I--------H
		*/
		b.SetVertices(
		{
			Vector2(-10.f, -10.f), //A
			Vector2(10.f, -10.f),
			Vector2(10.f, -5.f), //C
			Vector2(0.f, -5.f),
			Vector2(8.f, 0.f), //E
			Vector2(0.f, 5.f),
			Vector2(10.f, 5.f), //G
			Vector2(10.f, 10.f),
			Vector2(-10.f, 10.f) //I
		});
		b.SetPosition(Vector3::ZERO_VECTOR);
		b.SetScale(1.f);
		b.SetExtrudeHeight(10.f);

		//Intersect lines: HI, FG, FE, DE, DC, and AB. The impact should return HI's intersection.
		a.SetPosition(Vector3(6.5f, 12.f, 5.f));
		a.SetEndPoint(Vector3(0.f, -50.f, 0.f));
		a.SetScale(1.f);
		bExpectCollision = true;
		expectedImpact = Vector3(6.5f, 10.f, 5.f);
		expectedNormal = Vector3(0.f, 1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//The segment should start inside plane FGHI. The impact should return FG's intersection.
		a.SetPosition(Vector3(6.5f, 8.f, 5.f));
		expectedImpact = Vector3(6.5f, 5.f, 5.f);
		expectedNormal = Vector3(0.f, -1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//The segment is starting in the gap between segments FE and FG. The impact should return FE's intersection.
		a.SetPosition(Vector3(6.5f, 4.f, 5.f));
		expectedImpact = Vector3(6.5f, 0.9375f, 5.f);
		expectedNormal = Vector3::Normalize(Vector3(5.f, 8.f, 0.f));
		if (!testCollision())
		{
			return false;
		}

		//Segment is starting in the plane DEF. The impact should return DE's intersection.
		a.SetPosition(Vector3(6.5f, 0.f, 5.f));
		expectedImpact = Vector3(6.5f, -0.9375f, 5.f);
		expectedNormal = Vector3::Normalize(Vector3(5.f, -8.f, 0.f));
		if (!testCollision())
		{
			return false;
		}

		//Segment is starting in the gap between EDC. The impact should return DC's intersection.
		a.SetPosition(Vector3(6.5f, -4.f, 5.f));
		expectedImpact = Vector3(6.5f, -5.f, 5.f);
		expectedNormal = Vector3(0.f, 1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//Segment is starting in the plane ABCD. The impact should return AB's intersection.
		a.SetPosition(Vector3(6.5f, -8.f, 6.f));
		expectedImpact = Vector3(6.5f, -10.f, 6.f);
		expectedNormal = Vector3(0.f, -1.f, 0.f);
		if (!testCollision())
		{
			return false;
		}

		//Vertical segment in the gap DCE
		a.SetPosition(Vector3(6.5f, -4.f, 11.f));
		a.SetEndPoint(Vector3(3.f, 2.f, -15.f));
		bExpectCollision = false;
		if (!testCollision())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Collision Utils Tests"));
	return true;
}

bool PhysicsUnitTest::RunPhysicsUtilsTests (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Physics Utils"));

	QuadTree rootTree(Vector2::ZERO_VECTOR, 2000.f, nullptr);
	SceneEntity* testObj = SceneEntity::CreateObject();

	//Collision channel for physics components represented as a number in the diagram below.
	Int numChannel = 1;

	//Collision channel for physics components represented as a letter in the diagram below.
	Int letterChannel = 2;

	//Letter collision channels
	Int vowelChannel = 4;
	Int consonantChannel = 8;

	//Number collision channels
	Int evenChannel = 16;
	Int oddChannel = 32;

	Int allChannels(~0);

	/*
	+-------------------------------------------+
	|  |   / |/|            o o o o o o o o o o |
	|  |  /  |\|            0 1 2 3 4 5 6 7 8 9 |
	|  | /   |/|                                |
	|  A  B  CDE                                |
	|            /\                             |
	|           /F \    P                       |
	|     /\   /___ \                           |
	|  __/G \__                                 |
	|  \______/            I                    |
	|                      HO                   |
	+-------------------------------------------+
	*/

	//Create line segments in top left region
	PhysicsComponent* segA = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(segA))
	{
		segA->SetRootQuadTree(&rootTree);
		segA->SetCollisionChannels(letterChannel | vowelChannel);
		segA->AddShape(new CollisionSegment(1.f, Vector3(-950.f, -750.f, 0.f), Vector3(0.f, -200.f, 0.f)));
	}

	PhysicsComponent* segB = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(segB))
	{
		segB->SetRootQuadTree(&rootTree);
		segB->SetCollisionChannels(letterChannel | consonantChannel);
		segB->AddShape(new CollisionSegment(1.f, Vector3(-900.f, -750.f, 0.f), Vector3(100.f, -200.f, 0.f)));
	}

	PhysicsComponent* segC = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(segC))
	{
		segC->SetRootQuadTree(&rootTree);
		segC->SetCollisionChannels(letterChannel | consonantChannel);
		segC->AddShape(new CollisionSegment(1.f, Vector3(-850.f, -750.f, 0.f), Vector3(0.f, -200.f, 0.f)));
	}
	
	PhysicsComponent* segD = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(segD))
	{
		segD->SetRootQuadTree(&rootTree);
		segD->SetCollisionChannels(letterChannel | consonantChannel);
		segD->AddShape(new CollisionSegment(1.f, Vector3(-800.f, -750.f, 0.f), Vector3(50.f, -50.f, 0.f)));
		segD->AddShape(new CollisionSegment(1.f, Vector3(-750.f, -800.f, 0.f), Vector3(-50.f, -50.f, 0.f)));
		segD->AddShape(new CollisionSegment(1.f, Vector3(-800.f, -850.f, 0.f), Vector3(50.f, -50.f, 0.f)));
	}

	PhysicsComponent* segE = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(segE))
	{
		segE->SetRootQuadTree(&rootTree);
		segE->SetCollisionChannels(letterChannel | vowelChannel);
		segE->AddShape(new CollisionSegment(1.f, Vector3(-750.f, -750.f, 0.f), Vector3(0.f, -200.f, 0.f)));
	}

	//Create triangle near center
	PhysicsComponent* triangleF = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(triangleF))
	{
		triangleF->SetRootQuadTree(&rootTree);
		triangleF->SetCollisionChannels(letterChannel | consonantChannel);
		triangleF->AddShape(new CollisionExtrudePolygon(1.f, Vector3(-250.f, 0.f, 0.f), 10.f,
		{
			Vector2(0.f, -50.f),
			Vector2(50.f, 50.f),
			Vector2(-50.f, 50.f)
		}));
	}

	//Create 'boat shape' in the bottom left corner
	PhysicsComponent* polyG = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(polyG))
	{
		polyG->SetRootQuadTree(&rootTree);
		polyG->SetCollisionChannels(letterChannel | consonantChannel);

		/*
			  A
			 / \
		 F--G   B--C
		  \   P   /
		   E-----D
		*/
		polyG->AddShape(new CollisionExtrudePolygon(1.f, Vector3(-850.f, 750.f, 0.f), 10.f,
		{
			Vector2(0.f, -100.f), //A
			Vector2(50.f, -50.f),
			Vector2(100.f, -50.f), //C
			Vector2(75.f, 50.f),
			Vector2(-75.f, 50.f), //E
			Vector2(-100.f, -50.f),
			Vector2(-50.f, -50.f) //G
		}));
	}

	//Create overlapping capsules H and I (I is above H)
	PhysicsComponent* capsuleH = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(capsuleH))
	{
		capsuleH->SetRootQuadTree(&rootTree);
		capsuleH->SetCollisionChannels(letterChannel | consonantChannel);
		capsuleH->AddShape(new CollisionCapsule(1.f, Vector3(100.f, 900.f, -75.f), 100.f, 50.f));
	}

	PhysicsComponent* capsuleI = PhysicsComponent::CreateObject();
	if (testObj->AddComponent(capsuleI))
	{
		capsuleI->SetRootQuadTree(&rootTree);
		capsuleI->SetCollisionChannels(letterChannel | vowelChannel);
		capsuleI->AddShape(new CollisionCapsule(1.f, Vector3(100.f, 900.f, 75.f), 100.f, 50.f));
	}

	//Create spheres in upper right corner
	std::vector<PhysicsComponent*> spheres;
	Vector3 sphereCenter(0.f, -750.f, 0.f);
	for (size_t i = 0; i < 10; ++i)
	{
		PhysicsComponent* sphere = PhysicsComponent::CreateObject();
		if (testObj->AddComponent(sphere))
		{
			sphere->SetRootQuadTree(&rootTree);

			Int channel = ((i%2) == 0) ? evenChannel : oddChannel;
			sphere->SetCollisionChannels(numChannel | channel);
			sphere->AddShape(new CollisionSphere(1.f, sphereCenter, 10.f));
			spheres.push_back(sphere);
			sphereCenter.X += 100.f;
		}
	}

	//Force absolute transform to find this object's position in the quad tree. Otherwise, the PhysicsComponents would wait for a tick update to simulate physics.
	//See PhysicsComponent::bTransformReady
	testObj->MarkAbsTransformDirty();
	testObj->CalculateAbsoluteTransform();

	SetTestCategory(testFlags, TXT("Ray Trace"));
	{
		PhysicsUtils::STraceParams traceParams;
		Vector3 startTrace;
		Vector3 endTrace;
		PhysicsUtils::STraceResults expected;
		bool bRunNormalTest = true;

		std::function<DString()> paramsToString([&]()
		{
			return DString::CreateFormattedString(TXT("[start=%s, end=%s, channels=%s, exception list size=%s]"), startTrace, endTrace, traceParams.CollisionChannels, Int(traceParams.ExceptionList.size()));
		});

		std::function<bool()> testTrace([&]()
		{
			PhysicsUtils::STraceResults actual = PhysicsUtils::RayTrace(&rootTree, startTrace, endTrace, traceParams);
			if (actual.HasImpact() != expected.HasImpact())
			{
				DString conditionalStr = (expected.HasImpact()) ? DString::EmptyString : TXT("NOT");
				UnitTestError(testFlags, TXT("Ray Trace test failed. When tracing with params %s, it should %s have collided with something. The function suggests otherwise."), paramsToString(), conditionalStr);
				testObj->Destroy();
				return false;
			}

			if (actual.CollidedAgainst != expected.CollidedAgainst)
			{
				UnitTestError(testFlags, TXT("Ray Trace test failed. When tracing with params %s, it should have collided against %s instead of %s."), paramsToString(), expected.CollidedAgainst->ToString(), actual.CollidedAgainst->ToString());
				testObj->Destroy();
				return false;
			}

			//Ensure the FastTrace is consistent with the regular trace.
			bool bFastTrace = PhysicsUtils::FastTrace(&rootTree, startTrace, endTrace, traceParams);
			if (bFastTrace != actual.HasImpact())
			{
				UnitTestError(testFlags, TXT("Ray Trace test failed. When tracing with params %s, it should have produced results that are consistent with FastTrace. Regular trace produced %s. Fast trace produced %s."), paramsToString(), Bool(actual.HasImpact()), Bool(bFastTrace));
				testObj->Destroy();
				return false;
			}

			if (actual.HasImpact())
			{
				if (!actual.EndPoint.IsNearlyEqual(expected.EndPoint, 0.002f)) //Graphing calculator only has 3 decimal places
				{
					UnitTestError(testFlags, TXT("Ray Trace test failed. When tracing with params %s, the impact location should have been %s instead of %s."), paramsToString(), expected.EndPoint, actual.EndPoint);
					testObj->Destroy();
					return false;
				}

				if (bRunNormalTest && !actual.ReadNormal().IsNearlyEqual(expected.ReadNormal(), 0.001f))
				{
					UnitTestError(testFlags, TXT("Ray Trace test failed. When tracing with params %s, the impact normal should have been %s instead of %s."), paramsToString(), expected.ReadNormal(), actual.ReadNormal());
					testObj->Destroy();
					return false;
				}
			}

			return true;
		});

		//Trace that collides with nothing in lower right corner.
		startTrace = Vector3(50.f, 50.f, 0.f);
		endTrace = Vector3(950.f, 950.f, 0.f);
		traceParams.CollisionChannels = allChannels;
		traceParams.bCanCollideAtStart = true;
		expected.CollidedAgainst = nullptr;
		if (!testTrace())
		{
			return false;
		}

		//Trace that starts left of A and intersects all segments A-E.
		startTrace = Vector3(-1000.f, -800.f, 0.f);
		endTrace = Vector3(-100.f, -800.f, 0.f);
		expected.CollidedAgainst = segA;
		expected.EndPoint = Vector3(-950.f, -800.f, 0.f);
		bRunNormalTest = false; //Normals are not well defined for segment-segment collisions.
		if (!testTrace())
		{
			return false;
		}

		//Ignore vowels
		traceParams.CollisionChannels = consonantChannel;
		expected.CollidedAgainst = segB;
		expected.EndPoint = Vector3(-875.f, -800.f, 0.f);
		if (!testTrace())
		{
			return false;
		}

		//Ignore B
		traceParams.ExceptionList.push_back(segB);
		expected.CollidedAgainst = segC;
		expected.EndPoint = Vector3(-850.f, -800.f, 0.f);
		if (!testTrace())
		{
			return false;
		}

		//Ray that collides against F & G. Should return F since it's closer to ray's start position.
		startTrace = Vector3(0.f, -200.f, 5.f);
		endTrace = Vector3(-1000.f, 800.f, 5.f);
		ContainerUtils::Empty(OUT traceParams.ExceptionList);
		traceParams.CollisionChannels = allChannels;
		expected.CollidedAgainst = triangleF;
		expected.EndPoint = Vector3(-216.667f, 16.667f, 5.f);
		expected.SetNormal(Vector3(2.f, -1.f, 0.f));
		bRunNormalTest = true;
		if (!testTrace())
		{
			return false;
		}

		//Ray trace that collides against G against segment FE
		//E is at point (-925, 800)
		//F is at point (-950, 700)
		startTrace = Vector3(-935.f, 785.f, 8.f);
		endTrace = Vector3(-925.f, 775.f, 8.f);
		expected.CollidedAgainst = polyG;
		expected.EndPoint = Vector3(-930.f, 780.f, 8.f);
		expected.SetNormal(Vector3::Normalize(Vector3(-4.f, 1.f, 0.f)));
		if (!testTrace())
		{
			return false;
		}

		//Run a trace completely inside polyG
		startTrace = Vector3(-850.f, 760.f, 3.f);
		endTrace = startTrace + Vector3(0.f, 0.f, 100.f);
		expected.EndPoint = startTrace;
		expected.SetNormal(Vector3(0.f, 0.f, -1.f));
		if (!testTrace())
		{
			return false;
		}

		//Run the same trace, but this time without immediately colliding against encompassing shapes.
		traceParams.bCanCollideAtStart = false;
		expected.EndPoint = Vector3(-850.f, 760.f, 10.f); //Should be the point the trace exits the polygon
		expected.SetNormal(Vector3(0.f, 0.f, 1.f));
		if (!testTrace())
		{
			return false;
		}

		//Trace against the spheres. Evens only.
		startTrace = Vector3(-150.f, -750.f, 0.f);
		endTrace = Vector3(1000.f, -750.f, 0.f);
		traceParams.CollisionChannels = evenChannel;
		expected.CollidedAgainst = spheres.at(0);
		expected.EndPoint = Vector3(-10.f, -750.f, 0.f);
		expected.SetNormal(Vector3(-1.f, 0.f, 0.f));
		if (!testTrace())
		{
			return false;
		}

		//Trace against the spheres. Odds only.
		traceParams.CollisionChannels = oddChannel;
		expected.CollidedAgainst = spheres.at(1);
		expected.EndPoint = Vector3(90.f, -750.f, 0.f);
		if (!testTrace())
		{
			return false;
		}

		//Trace against spheres. Odds only. Ignoring sphere_1.
		traceParams.ExceptionList.push_back(spheres.at(1));
		expected.CollidedAgainst = spheres.at(3);
		expected.EndPoint = Vector3(290.f, -750.f, 0.f);
		if (!testTrace())
		{
			return false;
		}

		//Perform a distant trace. It should still find the 9th sphere even though the ray trace algorithm breaks the trace into fragments.
		CHECK(spheres.size() >= 9)
		startTrace = Vector3(25000.f, -750.f, 0.f);
		endTrace = Vector3(0.f, -750.f, 0.f);
		ContainerUtils::Empty(OUT traceParams.ExceptionList);
		traceParams.CollisionChannels = allChannels;
		expected.CollidedAgainst = spheres.at(9);
		expected.EndPoint = Vector3(910.f, -750.f, 0.f);
		expected.SetNormal(Vector3(1.f, 0.f, 0.f));
		if (!testTrace())
		{
			return false;
		}

		//Long distance trace that should not intersect any spheres.
		endTrace = Vector3(975.f, -750.f, 0.f);
		expected.CollidedAgainst = nullptr;
		if (!testTrace())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Sweep"));
	{
		Vector3 startSweep;
		Vector3 endSweep;
		PhysicsUtils::SRelevanceParams params;
		std::vector<PhysicsComponent*> expected;

		std::function<DString()> paramsToString([&]()
		{
			return DString::CreateFormattedString(TXT("[start=%s, end=%s, channels=%s, exception list size=%s]"), startSweep, endSweep, params.CollisionChannels, Int(params.ExceptionList.size()));
		});

		std::function<bool()> testSweep([&]()
		{
			std::vector<PhysicsComponent*> actual = PhysicsUtils::Sweep(&rootTree, startSweep, endSweep, params);
			if (actual.size() != expected.size())
			{
				UnitTestError(testFlags, TXT("Sweep test failed. When sweeping with the params %s, it's expected to find %s physics components. Instead it found %s."), paramsToString(), Int(expected.size()), Int(actual.size()));
				testObj->Destroy();
				return false;
			}

			std::vector<PhysicsComponent*> searchFor = expected;
			while (searchFor.size() > 0)
			{
				bool bFound = false;
				for (PhysicsComponent* phys : actual)
				{
					if (ContainerUtils::GetLast(searchFor) == phys)
					{
						bFound = true;
						searchFor.pop_back();
						break;
					}
				}

				if (!bFound)
				{
					UnitTestError(testFlags, TXT("Sweep test failed. When sweeping with the params %s, it was unable to find %s."), paramsToString(), ContainerUtils::GetLast(searchFor)->ToString());
					testObj->Destroy();
					return false;
				}
			}

			return true;
		});

		//Sweep should find nothing
		startSweep = Vector3(50.f, 50.f, 0.f);
		endSweep = Vector3(250.f, 512.f, 0.f);
		expected = {};
		if (!testSweep())
		{
			return false;
		}

		//Sweep should find A
		startSweep = Vector3(-925.f, -800.f, 0.f);
		endSweep = Vector3(-975.f, -815.f, 0.f);
		params.CollisionChannels = letterChannel;
		expected = {segA};
		if (!testSweep())
		{
			return false;
		}

		//Sweep should find nothing
		params.CollisionChannels = numChannel;
		expected = {};
		if (!testSweep())
		{
			return false;
		}

		//Sweep should collide against all segments A-E
		startSweep = Vector3(-1000.f, -800.f, 0.f);
		endSweep = Vector3(-50.f, -800.f, 0.f);
		params.CollisionChannels = allChannels;
		expected = {segA, segB, segC, segD, segE};
		if (!testSweep())
		{
			return false;
		}

		//consonants only
		params.CollisionChannels = consonantChannel;
		expected = {segB, segC, segD};
		if (!testSweep())
		{
			return false;
		}

		//vowels only
		params.CollisionChannels = vowelChannel;
		expected = {segA, segE};
		if (!testSweep())
		{
			return false;
		}

		//Sweep should find F and G
		startSweep = Vector3(-200.f, 50.f, 5.f);
		endSweep = Vector3(-950.f, 725.f, 5.f);
		params.CollisionChannels = allChannels;
		expected = {triangleF, polyG};
		if (!testSweep())
		{
			return false;
		}

		//Test exception list
		params.ExceptionList.push_back(triangleF);
		expected = {polyG};
		if (!testSweep())
		{
			return false;
		}

		//Test a sweep completely inside the polygon.
		startSweep = Vector3(-850.f, 725.f, 3.f);
		endSweep = Vector3(-855.f, 775.f, 4.f);
		ContainerUtils::Empty(OUT params.ExceptionList);
		expected = {polyG};
		if (!testSweep())
		{
			return false;
		}

		//Long distance sweeping should find all spheres
		startSweep = Vector3(20000.f, -750.f, 4.f);
		endSweep = Vector3(0.f, -750.f, 4.f);
		expected = spheres;
		if (!testSweep())
		{
			return false;
		}

		//Long distance sweeping that should only find the last sphere
		endSweep = Vector3(900.f, -750.f, 0.f);
		expected = {ContainerUtils::GetLast(spheres)};
		if (!testSweep())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Find Encompassing Components"));
	{
		Vector3 testPoint;
		PhysicsUtils::SRelevanceParams params;
		std::vector<PhysicsComponent*> expected;

		std::function<DString()> paramsToString([&]()
		{
			return DString::CreateFormattedString(TXT("[testPt=%s, channels=%s, exception list size=%s]"), testPoint, params.CollisionChannels, Int(params.ExceptionList.size()));
		});

		std::function<bool()> testEncompassing([&]()
		{
			std::vector<PhysicsComponent*> actual = PhysicsUtils::FindEncompassingComps(&rootTree, testPoint, params);
			if (actual.size() != expected.size())
			{
				UnitTestError(testFlags, TXT("Encompassing Components test failed. When performing the action with the params %s, it's expected to find %s physics components. Instead it found %s."), paramsToString(), Int(expected.size()), Int(actual.size()));
				testObj->Destroy();
				return false;
			}

			std::vector<PhysicsComponent*> searchFor = expected;
			while (searchFor.size() > 0)
			{
				bool bFound = false;
				for (PhysicsComponent* phys : actual)
				{
					if (ContainerUtils::GetLast(searchFor) == phys)
					{
						bFound = true;
						searchFor.pop_back();
						break;
					}
				}

				if (!bFound)
				{
					UnitTestError(testFlags, TXT("Encompassing Components test failed. When performing the action with the params %s, it was unable to find %s."), paramsToString(), ContainerUtils::GetLast(searchFor)->ToString());
					testObj->Destroy();
					return false;
				}
			}

			return true;
		});

		//Point should overlap with nothing
		testPoint = Vector3(50.f, 50.f, 0.f);
		ContainerUtils::Empty(OUT expected);
		if (!testEncompassing())
		{
			return false;
		}

		//Polygon G should encompass point
		testPoint = Vector3(-810.f, 710.f, 5.f);
		expected = {polyG};
		if (!testEncompassing())
		{
			return false;
		}

		//Test point should be inside polyG's AABB, but should not be encompassed.
		testPoint = Vector3(-790.f, 690.f, 5.f);
		ContainerUtils::Empty(OUT expected);
		if (!testEncompassing())
		{
			return false;
		}

		//Both capsules should encompass the test point
		testPoint = Vector3(100.f, 900.f, 0.f);
		expected = {capsuleH, capsuleI};
		if (!testEncompassing())
		{
			return false;
		}

		//Ensure the params can filter out shapes.
		params.CollisionChannels = consonantChannel;
		expected = {capsuleH};
		if (!testEncompassing())
		{
			return false;
		}

		params.ExceptionList = {capsuleI};
		params.CollisionChannels = ~0;
		expected = {capsuleH};
		if (!testEncompassing())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Find Nearby Components"));
	{
		Vector3 position;
		Float distance;
		PhysicsUtils::SRelevanceParams params;
		std::vector<PhysicsComponent*> expected;

		std::function<DString()> paramsToString([&]()
		{
			return DString::CreateFormattedString(TXT("[pos=%s, dist=%s, channels=%s, exception list size=%s]"), position, distance, params.CollisionChannels, Int(params.ExceptionList.size()));
		});

		std::function<bool()> testNearbyComps([&]()
		{
			std::vector<PhysicsComponent*> actual = PhysicsUtils::FindNearbyComps(&rootTree, position, distance, params);
			if (actual.size() != expected.size())
			{
				UnitTestError(testFlags, TXT("Find Nearby Components test failed. When performing the action with the params %s, it's expected to find %s physics components. Instead it found %s."), paramsToString(), Int(expected.size()), Int(actual.size()));
				testObj->Destroy();
				return false;
			}

			std::vector<PhysicsComponent*> searchFor = expected;
			while (searchFor.size() > 0)
			{
				bool bFound = false;
				for (PhysicsComponent* phys : actual)
				{
					if (ContainerUtils::GetLast(searchFor) == phys)
					{
						bFound = true;
						searchFor.pop_back();
						break;
					}
				}

				if (!bFound)
				{
					UnitTestError(testFlags, TXT("Find Nearby Components test failed. When performing the action with the params %s, it was unable to find %s."), paramsToString(), ContainerUtils::GetLast(searchFor)->ToString());
					testObj->Destroy();
					return false;
				}
			}

			return true;
		});

		//Test the region inside the empty space.
		position = Vector3(250.f, 250.f, 0.f);
		distance = 50.f;
		ContainerUtils::Empty(OUT expected);
		if (!testNearbyComps())
		{
			return false;
		}

		//Test the region that overlaps all segments
		position = Vector3(-850.f, -800.f, 0.f);
		distance = 400.f;
		expected = {segA, segB, segC, segD, segE};
		if (!testNearbyComps())
		{
			return false;
		}

		params.CollisionChannels = consonantChannel;
		expected = {segB, segC, segD};
		if (!testNearbyComps())
		{
			return false;
		}

		params.CollisionChannels = vowelChannel;
		expected = {segA, segE};
		if (!testNearbyComps())
		{
			return false;
		}

		//Test the gap AGF near polygon G
		position = Vector3(-925.f, 685.f, 2.f);
		distance = 10.f;
		params.CollisionChannels = allChannels;
		ContainerUtils::Empty(OUT expected);
		if (!testNearbyComps())
		{
			return false;
		}

		distance = 20.f;
		expected = {polyG};
		if (!testNearbyComps())
		{
			return false;
		}

		//Test overlapping multiple spheres
		position = Vector3(400.f, -750.f, 0.f);
		distance = 125.f;
		expected = {spheres.at(3), spheres.at(4), spheres.at(5)};
		if (!testNearbyComps())
		{
			return false;
		}

		params.ExceptionList.push_back(spheres.at(3));
		expected = {spheres.at(4), spheres.at(5)};
		if (!testNearbyComps())
		{
			return false;
		}

		position.Z += 100.f;
		ContainerUtils::Empty(OUT params.ExceptionList);
		expected = {spheres.at(4)};
		if (!testNearbyComps())
		{
			return false;
		}

		//Test distant sphere
		position = Vector3(3000.f, -750.f, 0.f);
		distance = 2110.f;
		expected = {spheres.at(9)};
		if (!testNearbyComps())
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	testObj->Destroy();
	ExecuteSuccessSequence(testFlags, TXT("Physics Utils"));
	return true;
}
SD_END
#endif