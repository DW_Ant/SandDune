/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionExtrudePolygon.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry2dUtils.h"

SD_BEGIN
CollisionExtrudePolygon::CollisionExtrudePolygon () :
	CollisionShape(),
	ExtrudeHeight(1.f)
{
	Shape = ST_ExtrudePolygon;
}

CollisionExtrudePolygon::CollisionExtrudePolygon (Float inScale, const Vector3& inPosition) :
	CollisionShape(inScale, inPosition),
	ExtrudeHeight(1.f)
{
	Shape = ST_ExtrudePolygon;
}

CollisionExtrudePolygon::CollisionExtrudePolygon (Float inScale, const Vector3& inPosition, Float inExtrudeHeight, const std::vector<Vector2>& inVertices) :
	CollisionShape(inScale, inPosition),
	ExtrudeHeight(inExtrudeHeight),
	Vertices(inVertices)
{
	Shape = ST_ExtrudePolygon;
	ValidateVertices();
	UpdateWorldSpaceSegments();
	UpdateNormals();
}

void CollisionExtrudePolygon::RefreshBoundingBox ()
{
	if (ContainerUtils::IsEmpty(Vertices))
	{
		BoundingBox = Aabb();
		return;
	}

	Range<Float> xRange(MAX_FLOAT, -MAX_FLOAT);
	Range<Float> yRange(MAX_FLOAT, -MAX_FLOAT);
	for (size_t i = 0; i < Vertices.size(); ++i)
	{
		xRange.Min = Utils::Min(xRange.Min, Vertices[i].X);
		xRange.Max = Utils::Max(xRange.Max, Vertices[i].X);
		yRange.Min = Utils::Min(yRange.Min, Vertices[i].Y);
		yRange.Max = Utils::Max(yRange.Max, Vertices[i].Y);
	}

	BoundingBox.Depth = (xRange.Max - xRange.Min) * Scale;
	BoundingBox.Width = (yRange.Max - yRange.Min) * Scale;
	BoundingBox.Height = ExtrudeHeight * Scale;

	BoundingBox.Center = Vector3(xRange.Center() * Scale, yRange.Center() * Scale, BoundingBox.Height * 0.5f) + Position;
}

bool CollisionExtrudePolygon::OverlapsWith (const CollisionShape* otherShape) const
{
	if (!CollisionShape::OverlapsWith(otherShape))
	{
		return false;
	}

	//Attempt to use the faster algorithms when checking against specific shapes
	switch (otherShape->GetShape())
	{
		case(ST_Segment):
		{
			const CollisionSegment* otherSegment = dynamic_cast<const CollisionSegment*>(otherShape);
			CHECK(otherSegment != nullptr)
			return CollisionUtils::Overlaps(this, otherSegment);
		}

		case(ST_Sphere):
		{
			const CollisionSphere* otherSphere = dynamic_cast<const CollisionSphere*>(otherShape);
			CHECK(otherSphere != nullptr)
			return CollisionUtils::Overlaps(this, otherSphere);
		}

		case(ST_Capsule):
		{
			const CollisionCapsule* otherCapsule = dynamic_cast<const CollisionCapsule*>(otherShape);
			CHECK(otherCapsule != nullptr)
			return CollisionUtils::Overlaps(this, otherCapsule);
		}

		case(ST_ExtrudePolygon):
		{
			const CollisionExtrudePolygon* otherPoly = dynamic_cast<const CollisionExtrudePolygon*>(otherShape);
			CHECK(otherPoly != nullptr)
			return CollisionUtils::Overlaps(this, otherPoly);
		}
	}

	//Unknown shapes are not supported
	return false;
}

bool CollisionExtrudePolygon::EncompassesPoint (const Vector3& point) const
{
	if (!CollisionShape::EncompassesPoint(point)) //Aabb check
	{
		return false;
	}

	/*
	NOTE: Since the top and bottom surfaces are parallel to the XY plane, there's no need to check for point's Z-position since
	the Aabb check would have tested against that.
	Only check if the point's XY coordinate is within the polygon.
	*/
	return IsWithinPolygon(point.ToVector2());
}

Vector3 CollisionExtrudePolygon::CalcClosestPoint (const Vector3& desiredPoint) const
{
	//Check if the point is already inside the polygon
	if (IsWithinPolygon(desiredPoint.ToVector2()))
	{
		Vector3 result(desiredPoint);
		result.Z = Utils::Clamp(result.Z, Position.Z, Position.Z + (ExtrudeHeight * Scale));
		return result;
	}

	Vector2 target = desiredPoint.ToVector2();
	Float bestDist = MAX_FLOAT;
	Vector2 bestPt;
	for (size_t i = 0; (i+1) < WorldSpaceSegments.size(); ++i)
	{
		Vector2 curPt = Geometry2dUtils::CalcClosestPointOnSegment(target, WorldSpaceSegments.at(i), WorldSpaceSegments.at(i+1));
		Float curDist = (target - curPt).VSize();
		if (curDist < bestDist)
		{
			bestPt = curPt;
			bestDist = curDist;
		}
	}

	Vector3 result(bestPt.ToVector3());
	result.Z = Utils::Clamp<Float>(desiredPoint.Z, Position.Z, Position.Z + (ExtrudeHeight * Scale));

	return result;
}

void CollisionExtrudePolygon::SetPosition (const Vector3& newPosition)
{
	Vector3 deltaPosition = newPosition - Position;
	CollisionShape::SetPosition(newPosition);

	//It would be faster to simply translate each vertex than reconstructing it
	for (Vector2& vertex : WorldSpaceSegments)
	{
		vertex.X += deltaPosition.X;
		vertex.Y += deltaPosition.Y;
	}
}

void CollisionExtrudePolygon::Translate (const Vector3& delta)
{
	CollisionShape::Translate(delta);

	for (Vector2& vertex : WorldSpaceSegments)
	{
		vertex.X += delta.X;
		vertex.Y += delta.Y;
	}
}

void CollisionExtrudePolygon::SetScale (Float newScale)
{
	Float deltaScale = newScale / Scale;
	CollisionShape::SetScale(newScale);

	//Update the world segments
	for (Vector2& vertex : WorldSpaceSegments)
	{
		vertex.X *= deltaScale;
		vertex.Y *= deltaScale;
	}
}

bool CollisionExtrudePolygon::IsWithinPolygon (const Vector2& point) const
{
	//AABB check (only if the bound box exist)
	if (!BoundingBox.IsEmpty())
	{
		//Check bounding box while ignoring Z
		if (BoundingBox.GetForward() < point.X || BoundingBox.GetBackward() > point.X ||
			BoundingBox.GetRight() < point.Y || BoundingBox.GetLeft() > point.Y)
		{
			return false;
		}
	}

	CHECK(Vertices.size() >= 3)

	//Test if the point resides on any segment (since the algorithm relies on finding a ray dir that doesn't intersect a vertex).
	for (size_t i = 0; i < WorldSpaceSegments.size() - 1; ++i) //Excluding the last vertex is intended since that is always equal to WorldSpaceSegments[0].
	{
		if (WorldSpaceSegments[i] == point)
		{
			return true;
		}
	}

	Vector2 rayStart = point;
	Vector2 rayDir;

	//Find a direction that doesn't intersect a vertex since intersecting through a vertex causes problems such as double counting a vertex since it technically resides in two segments.
	if (!FindRayDir(rayStart, OUT rayDir))
	{
		//Default to true since chances are that this point is inside the shape since there's an intersection all around it unless this shape is some strange hollowed out shape.
		//Either way, FindRayDir will issue a log warning, and the developers should either break up the shape into multiple pieces or use fewer vertices.
		return true;
	}

	Int numIntersections = 0;
	Geometry2dUtils::SIntersectResults intersection;
	for (size_t i = 0; (i+1) < WorldSpaceSegments.size(); ++i)
	{
		Geometry2dUtils::CalcRaySegIntersection(rayStart, rayDir, WorldSpaceSegments.at(i), WorldSpaceSegments.at(i+1), OUT intersection);
		if (intersection.IntersectionType == Geometry2dUtils::IT_Point)
		{
			//Note: Parallel intersections (when intersection type is equal to segment) are intentionally ignored. The end points from adjacent edges are considered instead.
			++numIntersections;
		}
	}

	return numIntersections.IsOdd();
}

void CollisionExtrudePolygon::SetExtrudeHeight (Float newExtrudeHeight)
{
	ExtrudeHeight = newExtrudeHeight;

	//Update the height of the Aabb
	BoundingBox.Height = (ExtrudeHeight * Scale);
	BoundingBox.Center.Z = Position.Z + (ExtrudeHeight * Scale * 0.5f);
}

void CollisionExtrudePolygon::SetVertices (const std::vector<Vector2>& newVertices)
{
	Vertices = newVertices;
	ValidateVertices();
	UpdateWorldSpaceSegments();
	RefreshBoundingBox();
	UpdateNormals();
}

bool CollisionExtrudePolygon::ValidateVertices ()
{
	if (Vertices.size() < 3)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Invalid extruded polygon detected. A polygon must have at least 3 vertices!"));
		ContainerUtils::Empty(OUT Vertices);
		return false;
	}

	return true;
}

void CollisionExtrudePolygon::UpdateWorldSpaceSegments ()
{
	WorldSpaceSegments = Vertices;
	if (ContainerUtils::IsEmpty(WorldSpaceSegments))
	{
		return;
	}

	//Convert each vertex in world space
	for (Vector2& vertex : WorldSpaceSegments)
	{
		vertex *= Scale;
		vertex.X += Position.X;
		vertex.Y += Position.Y;
	}

	//Push the first vertex again to create a segment connecting the last and the first.
	WorldSpaceSegments.push_back(WorldSpaceSegments.at(0));
}

void CollisionExtrudePolygon::UpdateNormals ()
{
	ContainerUtils::Empty(OUT Normals);

	//Although it may be simpler and faster to simply swap axis (set X to Y, and Y into X) for the Normals.
	//The problem is knowing which direction is correct (negative X or negative Y points 'outside' of the polygon).
	//The solution is to treat each border as a Rotator. Add 90 degrees for each angle. Convert it back to a directional vector. This also adds the extra benefit to return a Normalized vector.
	//That way, we can only test one of the normals to see if its point is inside the polygon or not. If it is inverted, then simply invert all vectors rather needing to test every normal.
	for (size_t i = 0; (i+1) < WorldSpaceSegments.size(); ++i)
	{
		Rotator rotation((WorldSpaceSegments[i] - WorldSpaceSegments[i+1]).ToVector3());
		rotation.Yaw += Rotator::QUARTER_REVOLUTION;
		Normals.push_back(rotation.GetDirectionalVector().ToVector2());

		//The resulting Normal should be Normalized since the Rotator from above shouldn't have any pitch since there should be no Z-axis when converting between 2D and 3D vectors.
		CHECK(ContainerUtils::GetLast(Normals).CalcDistSquared().IsCloseTo(1.f))
	}

	//If one of the Normals are facing towards the polygon, invert all Normals.
	if (WorldSpaceSegments.size() >= 2 && IsWithinPolygon(((WorldSpaceSegments.at(0) + WorldSpaceSegments.at(1)) * 0.5f) + (Normals.at(0) * 0.01f)))
	{
		for (Vector2& normal : Normals)
		{
			normal *= -1.f;
		}
	}
}

bool CollisionExtrudePolygon::FindRayDir (const Vector2& startPt, Vector2& outRayDir) const
{
	//NOTE: We don't check for ray directions for performance reasons. We only check if the points are collinear instead.

	//First try easy cases (vertical and horizontal)
	bool bFoundVert = false;
	for (const Vector2& vert : WorldSpaceSegments)
	{
		if (vert.Y == startPt.Y)
		{
			bFoundVert = true;
			break;
		}
	}

	if (!bFoundVert)
	{
		outRayDir = startPt;
		outRayDir.X += 1.f;
		return true;
	}

	bFoundVert = false;
	for (const Vector2& vert : WorldSpaceSegments)
	{
		if (vert.X == startPt.X)
		{
			bFoundVert = true;
			break;
		}
	}

	if (!bFoundVert)
	{
		outRayDir = startPt;
		outRayDir.Y += 1.f;
		return true;
	}

	//Now try using incremental directions
	Rotator dir(Rotator::ZERO_ROTATOR);
	const unsigned int MAX_ITERATIONS(20); //9 degrees per iteration.
	const unsigned int INCREMENT_AMOUNT = Rotator::HALF_REVOLUTION / MAX_ITERATIONS;

	Rotator rotation(2, 0, 0); //Add a slight nudge to misalign from horizontal and vertical rays since we already tested against axis-aligned rays.
	for (Int iterCounter = 0; iterCounter < MAX_ITERATIONS; ++iterCounter)
	{
		outRayDir = rotation.GetDirectionalVector().ToVector2();
		outRayDir += startPt;
		bFoundVert = false;
		for (size_t i = 0; i < WorldSpaceSegments.size() - 1; ++i) //Excluding the last segment is intended since the last is always equal to the first.
		{
			if (Geometry2dUtils::ArePointsCollinear(startPt, outRayDir, WorldSpaceSegments[i]))
			{
				bFoundVert = true;
				break;
			}
		}

		if (!bFoundVert)
		{
			//Found acceptable direction
			return true;
		}

		rotation.Yaw += INCREMENT_AMOUNT;
	}

	PhysicsLog.Log(LogCategory::LL_Warning, TXT("Unable to find Ray Direction for Extrude Polygon. Found a vertex intersection against %s directions. Recommend reducing the number of vertices!"), Int(MAX_ITERATIONS + 2));
	return false;
}
SD_END