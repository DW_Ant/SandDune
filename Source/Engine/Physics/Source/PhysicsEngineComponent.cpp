/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsEngineComponent.cpp
=====================================================================
*/

#include "PhysicsEngineComponent.h"

IMPLEMENT_ENGINE_COMPONENT(SD::PhysicsEngineComponent)
SD_BEGIN

PhysicsEngineComponent::PhysicsEngineComponent () : EngineComponent(),
	RootPhysTree(Vector2::ZERO_VECTOR, 100000.f, nullptr)
{
	//Noop
}

PhysicsEngineComponent::PhysicsEngineComponent (Float maxWorldLength) : EngineComponent(),
	RootPhysTree(Vector2::ZERO_VECTOR, maxWorldLength, nullptr)
{
	//Noop
}

void PhysicsEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = GetOwningEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_PRE_PHYSICS, TICK_GROUP_PRIORITY_PRE_PHYSICS);
	localEngine->CreateTickGroup(TICK_GROUP_PHYSICS, TICK_GROUP_PRIORITY_PHYSICS);
	localEngine->CreateTickGroup(TICK_GROUP_POST_PHYSICS, TICK_GROUP_PRIORITY_POST_PHYSICS);
}
SD_END