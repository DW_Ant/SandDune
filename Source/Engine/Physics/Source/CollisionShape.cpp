/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionShape.cpp
=====================================================================
*/

#include "CollisionShape.h"

SD_BEGIN
CollisionShape::CollisionShape () :
	OwningComponent(nullptr),
	BoundingBox(),
	Scale(1.f),
	Position(Vector3::ZERO_VECTOR),
	Shape(ST_Unknown)
{
	//Noop
}

CollisionShape::CollisionShape (Float inScale, const Vector3& inPosition) :
	OwningComponent(nullptr),
	BoundingBox(),
	Scale(inScale),
	Position(inPosition),
	Shape(ST_Unknown)
{
	//Noop
}

CollisionShape::~CollisionShape ()
{
	//Noop
}

const Aabb& CollisionShape::ReadBoundingBox () const
{
	return BoundingBox;
}

bool CollisionShape::OverlapsWith (const CollisionShape* otherShape) const
{
#if ENABLE_COMPLEX_CHECKING
	CHECK(otherShape != nullptr)

	//Although it would make sense to call RefreshBoundingBox if the check failed, it's intentionally omitted since this needs to be a fast function.
	CHECK_INFO(!BoundingBox.IsEmpty() && !otherShape->BoundingBox.IsEmpty(), "Be sure to call Refresh Bounding Box before using the CollisionShape.")
#endif

	//Parent class will only check for Aabb overlap for fast checking. Subclasses are responsible for checking for details.
	return (BoundingBox.Overlaps(otherShape->BoundingBox));
}

bool CollisionShape::EncompassesPoint (const Vector3& point) const
{
#if ENABLE_COMPLEX_CHECKING
	//Although it would make sense to call RefreshBoundingBox if the check failed, it's intentionally omitted since this needs to be a fast function.
	CHECK_INFO(!BoundingBox.IsEmpty(), "Be sure to call Refresh Bounding Box before using the CollisionShape.")
#endif

	return (BoundingBox.EncompassesPoint(point));
}

void CollisionShape::SetOwningComponent (PhysicsComponent* newOwningComponent)
{
	OwningComponent = newOwningComponent;
}

void CollisionShape::SetPosition (const Vector3& newPosition)
{
	Vector3 deltaCenter = (BoundingBox.Center - Position);
	BoundingBox.Center = (newPosition + deltaCenter);
	Position = newPosition;
}

void CollisionShape::Translate (const Vector3& delta)
{
	BoundingBox.Center += delta;
	Position += delta;
}

void CollisionShape::SetScale (Float newScale)
{
	Scale = newScale;

	//It's uncertain how the Aabb scales since it's up to the subclasses to define where the origin resides. Simply reconstruct it entirely instead.
	RefreshBoundingBox();
}
SD_END