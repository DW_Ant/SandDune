/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsUtils.cpp
=====================================================================
*/

#include "CollisionShape.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionUtils.h"
#include "PhysicsComponent.h"
#include "PhysicsUtils.h"
#include "QuadTree.h"
#include "QuadTreeInterface.h"

IMPLEMENT_ABSTRACT_CLASS(SD::PhysicsUtils, SD::BaseUtils)
SD_BEGIN

Float PhysicsUtils::TraceStepLength(1000.f); //10 meters

PhysicsUtils::STraceResults::STraceResults () :
	EndPoint(Vector3::ZERO_VECTOR),
	CollidedAgainst(nullptr)
{
	SetNormal(Vector3(1.f, 0.f, 0.f));
}

void PhysicsUtils::STraceResults::SetNormal (const Vector3& inNormal)
{
	Normal = Vector3::Normalize(inNormal);
}

PhysicsUtils::STraceResults PhysicsUtils::RayTrace (QuadTree* rootTree, const Vector3& startTrace, const Vector3& endTrace, const STraceParams& traceParams)
{
	STraceResults results;
	results.EndPoint = endTrace;
	if (rootTree == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a ray trace without specifying which QuadTree to perform the trace in."));
		return results;
	}

	Vector3 dir(endTrace - startTrace);
	if (dir.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a ray trace using a single point. The startTrace and endTrace must be in two different coordinates."));
		return results;
	}
	dir.NormalizeInline();

	//Break traces into steps for the following reasons:
	// * Reduce size of AABB regions for traces not aligned with an axis.
	// * Only iterate through shapes in order from closest to start point to end point.
	// * Break early if a shape is found earlier in the trace rather than needing to iterate through each shape.
	Vector3 startPt(startTrace);
	Vector3 endPt(startPt);

	Float totalLength = (endTrace - startTrace).VSize();
	bool bCheckEncompassing = traceParams.bCanCollideAtStart;

	for (Float dist = 0.f; dist < totalLength; dist += TraceStepLength)
	{
		if (dist + TraceStepLength > totalLength)
		{
			//This step would have gone the trace limit. Clamp it to the end point.
			endPt = endTrace;
		}
		else
		{
			endPt += (dir * TraceStepLength);
		}

		Aabb region(startPt, endPt);
		std::vector<QuadTreeInterface*> overlappingObjs;
		rootTree->FindOverlappingObjects(region, OUT overlappingObjs);
		CollisionSegment segment(1.f, startPt, (endPt - startPt));
		segment.RefreshBoundingBox();

		CollisionUtils::SCollisionInfo bestCollision;
		PhysicsComponent* bestPhys = nullptr;
		Float bestDist = MAX_FLOAT;

		for (QuadTreeInterface* overlapObj : overlappingObjs)
		{
			if (CollisionShape* shape = dynamic_cast<CollisionShape*>(overlapObj))
			{
				if (PhysicsComponent* physComp = shape->GetOwningComponent())
				{
					//Check collision channels
					if ((physComp->GetCollisionChannels() & traceParams.CollisionChannels) == 0)
					{
						continue;
					}

					//Check if the physics component is in the exception list
					if (ContainerUtils::FindInVector(traceParams.ExceptionList, physComp) != UINT_INDEX_NONE)
					{
						continue;
					}

					if (bCheckEncompassing && shape->EncompassesPoint(startTrace))
					{
						bestCollision.ImpactLocation = startTrace;
						bestCollision.SetNormal((endTrace - startTrace) * -1.f);
						bestPhys = physComp;
						bestDist = 0.f;
						break;
					}

					bool bHasCollision = false;
					CollisionUtils::SCollisionInfo curCollision;

					//TODO: Replace switch statement with CollisionShape::CalcCollision whenever the other permutations are implementated.
					switch(shape->GetShape())
					{
						case(CollisionShape::ST_Segment):
						{
							if (CollisionSegment* other = dynamic_cast<CollisionSegment*>(shape))
							{
								bHasCollision = CollisionUtils::CalcCollision(&segment, other, OUT curCollision);
							}

							break;
						}

						case(CollisionShape::ST_Sphere):
						{
							if (CollisionSphere* other = dynamic_cast<CollisionSphere*>(shape))
							{
								bHasCollision = CollisionUtils::CalcCollision(&segment, other, OUT curCollision);
							}

							break;
						}

						case(CollisionShape::ST_Capsule):
						{
							if (CollisionCapsule* other = dynamic_cast<CollisionCapsule*>(shape))
							{
								bHasCollision = CollisionUtils::CalcCollision(&segment, other, OUT curCollision);
							}

							break;
						}

						case(CollisionShape::ST_ExtrudePolygon):
						{
							if (CollisionExtrudePolygon* other = dynamic_cast<CollisionExtrudePolygon*>(shape))
							{
								bHasCollision = CollisionUtils::CalcCollision(&segment, other, OUT curCollision);
							}

							break;
						}
					}

					if (bHasCollision)
					{
						Float curDist = (curCollision.ImpactLocation - startTrace).CalcDistSquared();
						if (bestPhys == nullptr || curDist < bestDist)
						{
							bestCollision = curCollision;
							bestPhys = physComp;
							bestDist = curDist;
						}
					}
				}
			} //shape

		} //foreach overlapping objects

		if (bestPhys != nullptr)
		{
			results.EndPoint = bestCollision.ImpactLocation;
			results.SetNormal(bestCollision.ReadNormal());
			results.CollidedAgainst = bestPhys;
			break;
		}

		//Advance the trace step
		startPt = endPt;
		bCheckEncompassing = false; //Ignore encompassing checks for later steps since the next starting point cannot be encompassed.
	} //foreach trace step

	return results;
}

bool PhysicsUtils::FastTrace (QuadTree* rootTree, const Vector3& startTrace, const Vector3& endTrace, const STraceParams& traceParams)
{
	if (rootTree == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a ray trace without specifying which QuadTree to perform the trace in."));
		return false;
	}

	Vector3 dir(endTrace - startTrace);
	if (dir.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a ray trace using a single point. The startTrace and endTrace must be in two different coordinates."));
		return false;
	}
	dir.NormalizeInline();

	Vector3 startPt(startTrace);
	Vector3 endPt(startPt);

	Float totalLength = (endTrace - startTrace).VSize();
	for (Float dist = 0.f; dist < totalLength; dist += TraceStepLength)
	{
		if (dist + TraceStepLength > totalLength)
		{
			//This step would have gone the trace limit. Clamp it to the end point.
			endPt = endTrace;
		}
		else
		{
			endPt += (dir * TraceStepLength);
		}

		Aabb region(startPt, endPt);
		std::vector<QuadTreeInterface*> overlappingObjs;
		rootTree->FindOverlappingObjects(region, OUT overlappingObjs);
		CollisionSegment segment(1.f, startPt, (endPt - startPt));
		segment.RefreshBoundingBox();

		for (QuadTreeInterface* overlapObj : overlappingObjs)
		{
			if (CollisionShape* shape = dynamic_cast<CollisionShape*>(overlapObj))
			{
				if (PhysicsComponent* physComp = shape->GetOwningComponent())
				{
					//Check collision channels
					if ((physComp->GetCollisionChannels() & traceParams.CollisionChannels) == 0)
					{
						continue;
					}

					//Check if the physics component is in the exception list
					if (ContainerUtils::FindInVector(traceParams.ExceptionList, physComp) != UINT_INDEX_NONE)
					{
						continue;
					}

					if (segment.OverlapsWith(shape))
					{
						return true;
					}
				}
			} //shape

		} //foreach overlapping objects

		//Advance the trace step
		startPt = endPt;
	} //foreach trace step

	return false;
}

std::vector<PhysicsComponent*> PhysicsUtils::Sweep (QuadTree* rootTree, const Vector3& startSweep, const Vector3& endSweep, const SRelevanceParams& sweepParams)
{
	std::vector<PhysicsComponent*> results;
	if (rootTree == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a sweep without specifying which QuadTree to perform the sweep in."));
		return results;
	}

	Vector3 dir(endSweep - startSweep);
	if (dir.IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform a sweep using a single point. The startSweep and endSweep must be in two different coordinates."));
		return results;
	}
	dir.NormalizeInline();

	//Break traces into steps to reduce size of AABB
	Vector3 startPt(startSweep);
	Vector3 endPt(startPt);

	Float totalLength = (endSweep - startSweep).VSize();
	for (Float dist = 0.f; dist < totalLength; dist += TraceStepLength)
	{
		if (dist + TraceStepLength > totalLength)
		{
			//This step would have gone the trace limit. Clamp it to the end point.
			endPt = endSweep;
		}
		else
		{
			endPt += (dir * TraceStepLength);
		}

		Aabb region(startPt, endPt);
		std::vector<QuadTreeInterface*> overlappingObjs;
		rootTree->FindOverlappingObjects(region, OUT overlappingObjs);
		CollisionSegment segment(1.f, startPt, (endPt - startPt));
		segment.RefreshBoundingBox();

		for (QuadTreeInterface* overlapObj : overlappingObjs)
		{
			if (CollisionShape* shape = dynamic_cast<CollisionShape*>(overlapObj))
			{
				if (PhysicsComponent* physComp = shape->GetOwningComponent())
				{
					if ((physComp->GetCollisionChannels() & sweepParams.CollisionChannels) == 0)
					{
						continue;
					}

					//Don't bother calculating for this shape if it's already in the results.
					if (ContainerUtils::FindInVector(results, physComp) != UINT_INDEX_NONE)
					{
						continue;
					}

					if (ContainerUtils::FindInVector(sweepParams.ExceptionList, physComp) != UINT_INDEX_NONE)
					{
						continue;
					}

					if (segment.OverlapsWith(shape))
					{
						results.push_back(physComp);
					}
				}
			}
		}

		//Advance the trace step
		startPt = endPt;
	}

	return results;
}

std::vector<PhysicsComponent*> PhysicsUtils::FindEncompassingComps (QuadTree* rootTree, const Vector3& point, const PhysicsUtils::SRelevanceParams& overlapParams)
{
	std::vector<PhysicsComponent*> results;
	if (rootTree == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform an overlap check without specifying which QuadTree to perform the check in."));
		return results;
	}

	std::vector<QuadTreeInterface*> overlappingObjs;
	rootTree->FindEncompassingObjects(point, OUT overlappingObjs);
	for (QuadTreeInterface* overlapObj : overlappingObjs)
	{
		if (CollisionShape* shape = dynamic_cast<CollisionShape*>(overlapObj))
		{
			if (PhysicsComponent* physComp = shape->GetOwningComponent())
			{
				if ((physComp->GetCollisionChannels() & overlapParams.CollisionChannels) == 0)
				{
					continue;
				}

				//Don't bother calculating for this shape if it's already in the results.
				if (ContainerUtils::FindInVector(results, physComp) != UINT_INDEX_NONE)
				{
					continue;
				}

				if (ContainerUtils::FindInVector(overlapParams.ExceptionList, physComp) != UINT_INDEX_NONE)
				{
					continue;
				}

				if (shape->EncompassesPoint(point))
				{
					results.push_back(physComp);
				}
			}
		}
	}

	return results;
}

std::vector<PhysicsComponent*> PhysicsUtils::FindNearbyComps (QuadTree* rootTree, const Vector3& point, Float maxDist, const SRelevanceParams& overlapParams)
{
	std::vector<PhysicsComponent*> results;
	if (rootTree == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot perform an overlap check without specifying which QuadTree to perform the check in."));
		return results;
	}

	CollisionSphere sphere(1.f, point, maxDist);
	sphere.RefreshBoundingBox();

	std::vector<QuadTreeInterface*> overlappingObjs;
	rootTree->FindOverlappingObjects(sphere.ReadBoundingBox(), OUT overlappingObjs);
	for (QuadTreeInterface* overlapObj : overlappingObjs)
	{
		if (CollisionShape* shape = dynamic_cast<CollisionShape*>(overlapObj))
		{
			if (PhysicsComponent* physComp = shape->GetOwningComponent())
			{
				if ((physComp->GetCollisionChannels() & overlapParams.CollisionChannels) == 0)
				{
					continue;
				}

				//Don't bother calculating for this shape if it's already in the results.
				if (ContainerUtils::FindInVector(results, physComp) != UINT_INDEX_NONE)
				{
					continue;
				}

				if (ContainerUtils::FindInVector(overlapParams.ExceptionList, physComp) != UINT_INDEX_NONE)
				{
					continue;
				}

				if (sphere.OverlapsWith(shape))
				{
					results.push_back(physComp);
				}
			}
		}
	}

	return results;
}
SD_END