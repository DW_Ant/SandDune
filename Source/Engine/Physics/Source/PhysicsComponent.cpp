/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsComponent.cpp
=====================================================================
*/

#include "CollisionShape.h"
#include "QuadTree.h"
#include "QuadTreeInterface.h"
#include "PhysicsComponent.h"
#include "PhysicsEngineComponent.h"

IMPLEMENT_CLASS(SD::PhysicsComponent, SD::EntityComponent)
SD_BEGIN

PhysicsComponent::SCollisionShape::SCollisionShape () :
	Shape(nullptr),
	ShapeDisplacement(Vector3::ZERO_VECTOR)
{
	//Noop
}

void PhysicsComponent::InitProps ()
{
	Super::InitProps();

	Velocity = Vector3::ZERO_VECTOR;
	Acceleration = Vector3::ZERO_VECTOR;
	CollisionChannels = 0;
	OverlappingChannels = 0;
	BlockingChannels = 0;
	CollisionDampening = 0.25f;
	RootQuadTree = nullptr;
	TransformOwner = nullptr;
	PrePhysicsTick = nullptr;
	PhysicsTick = nullptr;
	PostPhysicsTick = nullptr;

	TransformLocalToWorld = Vector3::ZERO_VECTOR;
	bTransformIsReady = false;
}

void PhysicsComponent::BeginObject ()
{
	Super::BeginObject();

	PrePhysicsTick = TickComponent::CreateObject(TICK_GROUP_PRE_PHYSICS);
	if (AddComponent(PrePhysicsTick))
	{
		PrePhysicsTick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePrePhysicsTick, void, Float));
	}

	PhysicsTick = TickComponent::CreateObject(TICK_GROUP_PHYSICS);
	if (AddComponent(PhysicsTick))
	{
		PhysicsTick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePhysicsTick, void, Float));
		PhysicsTick->SetTicking(OverlappingChannels != 0);
	}

	PostPhysicsTick = TickComponent::CreateObject(TICK_GROUP_POST_PHYSICS);
	if (AddComponent(PostPhysicsTick))
	{
		PostPhysicsTick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePostPhysicsTick, void, Float));
		UpdateCallbackTicker();
	}

	PhysicsEngineComponent* localPhysEngine = PhysicsEngineComponent::Find();
	CHECK(localPhysEngine != nullptr)
	RootQuadTree = localPhysEngine->GetRootPhysTree();
}

bool PhysicsComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<SceneTransform*>(ownerCandidate) != nullptr);
}

void PhysicsComponent::Destroyed ()
{
	for (SCollisionShape& shape : CollisionShapes)
	{
		if (shape.Shape != nullptr && shape.Shape->GetTreeNode() != nullptr)
		{
			if (!shape.Shape->GetTreeNode()->RemoveObject(shape.Shape))
			{
				PhysicsLog.Log(LogCategory::LL_Warning, TXT("Failed to find and remove Aabb from Quad Tree. %s has a shape with an unknown Quad Tree Node."), ToString());
			}

			delete shape.Shape;
		}
	}
	ContainerUtils::Empty(OUT CollisionShapes);

	Super::Destroyed();
}

void PhysicsComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (TransformOwner = dynamic_cast<SceneTransform*>(newOwner))
	{
		TransformOwner->OnTransformChanged.RegisterHandler(SDFUNCTION(this, PhysicsComponent, HandleTransformChanged, void));
		bTransformIsReady = (TransformOwner->ReadAbsTranslation() != Vector3::ZERO_VECTOR); //If at origin, assume the transform needs an update.
		if (!bTransformIsReady)
		{
			//Force a abs transform update just in case the owner is actually placed at the origin. With the transform callback, it would at least start simulating the component.
			TransformOwner->MarkAbsTransformDirty();
		}
	}

	CHECK(TransformOwner != nullptr)
}

void PhysicsComponent::ComponentDetached ()
{
	if (TransformOwner != nullptr)
	{
		TransformOwner->OnTransformChanged.UnregisterHandler(SDFUNCTION(this, PhysicsComponent, HandleTransformChanged, void));
		TransformOwner = nullptr;
	}

	bTransformIsReady = false; //Stop simulating

	Super::ComponentDetached();
}

void PhysicsComponent::AddShape (CollisionShape* newShape)
{
	if (newShape == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot add a null shape to a PhysicsComponent."));
		return;
	}

	for (const SCollisionShape& shape : CollisionShapes)
	{
		if (shape.Shape == newShape)
		{
			PhysicsLog.Log(LogCategory::LL_Log, TXT("%s already has that shape. The AddShape call is ignored."), ToString());
			return;
		}
	}

	newShape->SetOwningComponent(this);

	SCollisionShape newEntry;
	newEntry.Shape = newShape;
	newEntry.Shape->RefreshBoundingBox();
	newEntry.ShapeDisplacement = newShape->GetPosition();

	if (bTransformIsReady && RootQuadTree != nullptr) //If it's not ready, don't add to quad tree yet to prevent dozens of shapes from registering at the origin.
	{
		RootQuadTree->InsertObject(newShape);
	}

	CollisionShapes.push_back(newEntry);
}

bool PhysicsComponent::RemoveShape (CollisionShape* target)
{
	if (target == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot remove a null shape from a PhysicsComponent."));
		return false;
	}

	for (size_t i = 0; i < CollisionShapes.size(); ++i)
	{
		if (CollisionShapes.at(i).Shape == target)
		{
			if (CollisionShapes[i].Shape->GetTreeNode() != nullptr)
			{
				if (!CollisionShapes[i].Shape->GetTreeNode()->RemoveObject(CollisionShapes[i].Shape))
				{
					PhysicsLog.Log(LogCategory::LL_Warning, TXT("Failed to remove shape from QuadTree. The PhysicsComponent thinks the shape resides in a particular node that does not have a reference to the shape's bounding box."));
				}
			}

			CollisionShapes[i].Shape->SetOwningComponent(nullptr);
			CollisionShapes.erase(CollisionShapes.begin() + i);
			return true;
		}
	}

	return false; //target does not exist in this PhysicsComponent
}

bool PhysicsComponent::CanOverlapWith (Int collisionChannels) const
{
	return ((OverlappingChannels & collisionChannels) > 0);
}

bool PhysicsComponent::CanOverlapWith (PhysicsComponent* otherComp) const
{
	return (otherComp != nullptr && otherComp != this && CanOverlapWith(otherComp->CollisionChannels));
}

bool PhysicsComponent::CanBlockAgainst (Int collisionChannels) const
{
	return ((BlockingChannels & collisionChannels) > 0);
}

bool PhysicsComponent::CanBlockAgainst (PhysicsComponent* otherComp) const
{
	if (otherComp == nullptr || otherComp == this)
	{
		return false;
	}

	//Both components must block each other to pass.
	//Note: because it's symmetrical, we ignore the component's CollisionChannels and only look at what they block instead.
	return ((BlockingChannels & otherComp->BlockingChannels) > 0);
}

bool PhysicsComponent::IsSimulatingPhysics () const
{
	return bTransformIsReady;
}

void PhysicsComponent::SetBeginOverlap (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newBeginOverlap)
{
	OnBeginOverlap = newBeginOverlap;
	UpdateCallbackTicker();
}

void PhysicsComponent::SetEndOverlap (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newEndOverlap)
{
	OnEndOverlap = newEndOverlap;
	UpdateCallbackTicker();
}

void PhysicsComponent::SetBlocked (SDFunction<void, PhysicsComponent*, PhysicsComponent*> newBlocked)
{
	OnBlocked = newBlocked;
	UpdateCallbackTicker();
}

void PhysicsComponent::SetVelocity (const Vector3& newVelocity)
{
	Velocity = newVelocity;
}

void PhysicsComponent::SetAcceleration (const Vector3& newAcceleration)
{
	Acceleration = newAcceleration;
}

void PhysicsComponent::SetCollisionChannels (Int newCollisionChannels)
{
	CollisionChannels = newCollisionChannels;
}

void PhysicsComponent::SetOverlappingChannels (Int newOverlappingChannels)
{
	OverlappingChannels = newOverlappingChannels;
	if (PhysicsTick != nullptr)
	{
		PhysicsTick->SetTicking(OverlappingChannels != 0);
	}

	size_t i = 0;
	while (i < OverlappingComponents.size())
	{
		if (!CanOverlapWith(OverlappingComponents.at(i).Get()))
		{
			if (OnEndOverlap.IsBounded())
			{
				OnEndOverlap(this, OverlappingComponents.at(i).Get());
			}

			OverlappingComponents.erase(OverlappingComponents.begin() + i);
			continue;
		}

		++i;
	}
}

void PhysicsComponent::SetBlockingChannels (Int newBlockingChannels)
{
	BlockingChannels = newBlockingChannels;
}

void PhysicsComponent::SetCollisionDampening (Float newCollisionDampening)
{
	CollisionDampening = Float::Abs(newCollisionDampening);
}

void PhysicsComponent::SetRootQuadTree (QuadTree* newRootQuadTree)
{
	if (RootQuadTree != nullptr)
	{
		//Remove all shapes from the old RootQuadTree.
		for (SCollisionShape& shape : CollisionShapes)
		{
			RootQuadTree->RemoveObject(shape.Shape);
		}
	}

	RootQuadTree = newRootQuadTree;
	if (RootQuadTree != nullptr)
	{
		//Insert all shapes to the new RootQuadTree.
		for (SCollisionShape& shape : CollisionShapes)
		{
			RootQuadTree->InsertObject(shape.Shape);
		}
	}
}

void PhysicsComponent::GetShapes (std::vector<CollisionShape*>& outShapes) const
{
	for (const SCollisionShape& shape : CollisionShapes)
	{
		outShapes.push_back(shape.Shape);
	}
}

void PhysicsComponent::UpdateCallbackTicker ()
{
	if (PostPhysicsTick != nullptr)
	{
		bool bShouldTick = bTransformIsReady && (OnBeginOverlap.IsBounded() || OnEndOverlap.IsBounded() || OnBlocked.IsBounded());
		PostPhysicsTick->SetTicking(bShouldTick);
	}
}

void PhysicsComponent::SimulateVelocity (Float deltaSec)
{
	if (TransformOwner == nullptr || !IsSimulatingPhysics())
	{
		return;
	}

	Vector3 oldPosition = TransformOwner->GetAbsTranslation();
	Velocity += (Acceleration * deltaSec);

	for (SCollisionShape& shape : CollisionShapes)
	{
		Vector3 newShapePos(TransformLocalToWorld + TransformOwner->ReadTranslation() + shape.ShapeDisplacement);
		if (newShapePos != shape.Shape->ReadPosition() && shape.Shape->GetTreeNode() != nullptr)
		{
			//Update the new shape position relative to the owner's new translation.
			//This reposition should ignore collision since this translation was explicitly assigned outside the physics loop.
			shape.Shape->SetPosition(newShapePos);

			QuadTree* newNode = nullptr;
			shape.Shape->GetTreeNode()->MoveObject(shape.Shape, OUT newNode);
		}
	}

	if (Velocity == Vector3::ZERO_VECTOR)
	{
		//Nothing to move
		return;
	}

	if (RootQuadTree == nullptr)
	{
		//Can't simulate shapes without organizing shapes into quad trees. Don't run simulation.
		return;
	}

	std::vector<CollisionShape*> ignoreList; //List of blocking components this component must ignore.
	if (BlockingChannels > 0)
	{
		//Figure out everything that's overlapping this component.
		//Objects that are already overlapping this component are ignored to prevent objects that were teleported inside another from getting stuck.
		for (SCollisionShape& shape : CollisionShapes)
		{
			std::vector<QuadTreeInterface*> overlappingObjs;
			RootQuadTree->FindOverlappingObjects(shape.Shape->ReadBoundingBox(), OUT overlappingObjs);
			for (QuadTreeInterface* obj : overlappingObjs)
			{
				CollisionShape* overlappingObj = dynamic_cast<CollisionShape*>(obj);
				if (overlappingObj == nullptr || overlappingObj->GetOwningComponent() == nullptr)
				{
					PhysicsLog.Log(LogCategory::LL_Warning, TXT("Invalid object detected in the PhysicsEngineComponent's quad tree. All objects in that quad tree should be CollisionShapes that are associated with a PhysicsComponent."));
					continue;
				}

				if (!CanBlockAgainst(overlappingObj->GetOwningComponent()))
				{
					continue;
				}

				if (ContainerUtils::FindInVector(ignoreList, overlappingObj) != UINT_INDEX_NONE)
				{
					//This shape is already in the ignore list
					continue;
				}

				if (shape.Shape->OverlapsWith(overlappingObj))
				{
					ignoreList.push_back(overlappingObj);
				}
			}
		}
	}

	//Move each shape to the new position and check for collision
	Vector3 newPosition(oldPosition + (Velocity * deltaSec)); //newPosition is the current position the move loop is testing against.
	Int blockedCounter = 0; //Number of tested positions that were blocked.
	size_t shapeIdx = 0;
	while (shapeIdx < CollisionShapes.size())
	{
		CollisionShapes.at(shapeIdx).Shape->SetPosition(TransformLocalToWorld + newPosition + CollisionShapes.at(shapeIdx).ShapeDisplacement);
		QuadTree* newNode = nullptr;
		if (!CollisionShapes[shapeIdx].Shape->GetTreeNode()->MoveObject(CollisionShapes[shapeIdx].Shape, OUT newNode))
		{
			PhysicsLog.Log(LogCategory::LL_Warning, TXT("Failed to move shape within a quad tree. The cached tree node did not have the shape registered. Removing and reinserting the shape in the quad tree to recover."));

			//Fallback to inefficent method to find and remove, then reinsert the shape to the quad tree
			RootQuadTree->RemoveObject(CollisionShapes[shapeIdx].Shape);
			RootQuadTree->InsertObject(CollisionShapes[shapeIdx].Shape);
		}

		//Check if this shape's new position is blocked.
		if (BlockingChannels == 0)
		{
			//Skip collision checking since this component cannot block against anything
			++shapeIdx;
			continue;
		}

		std::vector<QuadTreeInterface*> overlappingObjs;
		RootQuadTree->FindOverlappingObjects(CollisionShapes[shapeIdx].Shape->ReadBoundingBox(), OUT overlappingObjs);
		bool bPositionBlocked = false;

		for (QuadTreeInterface* overlappingObj : overlappingObjs)
		{
			CollisionShape* overlappingShape = dynamic_cast<CollisionShape*>(overlappingObj);
			if (overlappingShape == nullptr)
			{
				PhysicsLog.Log(LogCategory::LL_Warning, TXT("Invalid registered object in the PhysicsEngineComponent's quad tree. Only CollisionShapes are permitted in that quad tree."));
				continue;
			}

			PhysicsComponent* otherComp = overlappingShape->GetOwningComponent();
			if (otherComp == nullptr)
			{
				PhysicsLog.Log(LogCategory::LL_Warning, TXT("Found a registered shape in the PhysicsEngineComponent quad tree that is not associated with a PhysicsComponent. This shape will be ignored."));
				continue;
			}

			if (!CanBlockAgainst(otherComp))
			{
				//It's not possible for this shape to block this component
				continue;
			}

			if (ContainerUtils::FindInVector(ignoreList, overlappingShape) != UINT_INDEX_NONE)
			{
				//This particular shape is ignored
				continue;
			}

			if (CollisionShapes[shapeIdx].Shape->OverlapsWith(overlappingShape)) //high resolution collision detection
			{
				//The new space is blocked by another component.
				++blockedCounter;
				bPositionBlocked = true;
				if (blockedCounter < 2)
				{
					//First time getting blocked. Glide against this shape to find a new possible position.
					//Move this shape back to starting position since we will use that old Position for RubAgainst function.
					CollisionShapes[shapeIdx].Shape->SetPosition(oldPosition + CollisionShapes[shapeIdx].ShapeDisplacement);
					if (!CollisionShapes[shapeIdx].Shape->GetTreeNode()->MoveObject(CollisionShapes[shapeIdx].Shape, OUT newNode))
					{
						//This should never happen due to the MoveObject earlier in this loop
						CHECK_INFO(false, "Some how the QuadTree::MoveObject failed even though it was validated earlier in the SimulateVelocity loop.");
						RootQuadTree->InsertObject(CollisionShapes[shapeIdx].Shape);
					}

					//TODO: Implement CollisionUtils::DragAgainst
					//Without DragAgainst, shapes may have a tendency to 'stick' to surfaces.
					//newPosition = CollisionUtils::DragAgainst(CollisionShapes[shapeIdx].Shape, overlappingShape, (Velocity * deltaSec));

					//Cheap workaround in stopping momentum.
					//A real solution would require calculating the normal.
					Velocity *= -CollisionDampening;
					if (OnBlocked.IsBounded())
					{
						PendingBlockCallbacks.push_back(otherComp);
					}

					if (otherComp->OnBlocked.IsBounded())
					{
						otherComp->PendingBlockCallbacks.push_back(this);
					}
				}

				break;
			}
		}

		if (bPositionBlocked)
		{
			if (blockedCounter < 2)
			{
				//Try again with the recommended new position.
				shapeIdx = 0;
				continue;
			}
			else
			{
				//Couldn't find a valid position that's not blocked. Revert everything back to the original positions.
				for (SCollisionShape revertShape : CollisionShapes)
				{
					revertShape.Shape->SetPosition(oldPosition + revertShape.ShapeDisplacement);
					if (!revertShape.Shape->GetTreeNode()->MoveObject(revertShape.Shape, OUT newNode))
					{
						PhysicsLog.Log(LogCategory::LL_Warning, TXT("Failed to move shape within a quad tree since the collision shape is not registered to that exact node. Falling back to removing and reinserting the shape to the quad tree."));
						RootQuadTree->RemoveObject(revertShape.Shape);
						RootQuadTree->InsertObject(revertShape.Shape);
					}
				}

				//Since we're reverting to the old position, don't bother updating the owner transform.
				return;
			}
		}

		++shapeIdx;
	}

	//Found a valid position! Update the transform
	TransformOwner->SetTranslation(newPosition);
}

void PhysicsComponent::UpdateOverlappingShapes ()
{
	if (OverlappingChannels == 0)
	{
		if (PhysicsTick != nullptr)
		{
			PhysicsTick->SetTicking(false);
		}

		return;
	}

	if (!IsSimulatingPhysics())
	{
		return;
	}

	//First check for anything that is no longer overlapping this component.
	size_t i = 0;
	while (i < OverlappingComponents.size())
	{
		if (OverlappingComponents.at(i).IsNullptr() || !OverlappingComponents.at(i)->IsSimulatingPhysics())
		{
			//The component must have been destroyed recently
			OverlappingComponents.erase(OverlappingComponents.begin() + i);
			continue;
		}

		if (!CanOverlapWith(OverlappingComponents.at(i).Get()))
		{
			//One of the collision channels must have changed between frames.
			PendingEndOverlap.push_back(OverlappingComponents.at(i).Get());
			OverlappingComponents.erase(OverlappingComponents.begin() + i);
			continue;
		}

		bool overlapDetected = false;
		for (SCollisionShape& selfShape : CollisionShapes)
		{
			for (SCollisionShape& otherShape : OverlappingComponents.at(i)->CollisionShapes)
			{
				if (selfShape.Shape->OverlapsWith(otherShape.Shape))
				{
					overlapDetected = true;
					break;
				}
			}

			if (overlapDetected)
			{
				break;
			}
		}

		if (!overlapDetected)
		{
			PendingEndOverlap.push_back(OverlappingComponents.at(i).Get());
			OverlappingComponents.erase(OverlappingComponents.begin() + i);
			continue;
		}

		++i;
	}

	//Check for new components to overlap
	PhysicsEngineComponent* localPhysEngine = PhysicsEngineComponent::Find();
	CHECK(localPhysEngine != nullptr && localPhysEngine->GetRootPhysTree() != nullptr)

	for (SCollisionShape& shape : CollisionShapes)
	{
		std::vector<QuadTreeInterface*> overlappingObjs;
		localPhysEngine->GetRootPhysTree()->FindOverlappingObjects(shape.Shape->ReadBoundingBox(), OUT overlappingObjs);

		for (QuadTreeInterface* obj : overlappingObjs)
		{
			CollisionShape* otherShape = dynamic_cast<CollisionShape*>(obj);
			if (otherShape == nullptr || otherShape->GetOwningComponent() == nullptr)
			{
				PhysicsLog.Log(LogCategory::LL_Warning, TXT("Detected an invalid object in the PhysicsEngineComponent's quad tree. All objects in that tree must be collision shapes associated with a PhysicsComponent."));
				continue;
			}

			PhysicsComponent* otherComp = otherShape->GetOwningComponent();
			if (!CanOverlapWith(otherComp))
			{
				continue;
			}

			//Ignore shapes that are pending removal since we already tested against them earlier in this function.
			if (ContainerUtils::FindInVector(PendingEndOverlap, otherComp) != UINT_INDEX_NONE)
			{
				continue;
			}

			//Ignore shapes that are already overlapping
			if (ContainerUtils::FindInVector<DPointer<PhysicsComponent>>(OverlappingComponents, otherComp) != UINT_INDEX_NONE)
			{
				continue;
			}

			if (shape.Shape->OverlapsWith(otherShape))
			{
				OverlappingComponents.push_back(otherComp);
				PendingBeginOverlap.push_back(otherComp);
			}
		}
	}
}

void PhysicsComponent::InvokePendingCallbacks ()
{
	if (OnBeginOverlap.IsBounded())
	{
		for (PhysicsComponent* otherComp : PendingBeginOverlap)
		{
			OnBeginOverlap(this, otherComp);
		}
	}

	if (OnEndOverlap.IsBounded())
	{
		for (PhysicsComponent* otherComp : PendingEndOverlap)
		{
			OnEndOverlap(this, otherComp);
		}
	}

	if (OnBlocked.IsBounded())
	{
		for (PhysicsComponent* otherComp : PendingBlockCallbacks)
		{
			OnBlocked(this, otherComp);
		}
	}

	ContainerUtils::Empty(OUT PendingBeginOverlap);
	ContainerUtils::Empty(OUT PendingEndOverlap);
	ContainerUtils::Empty(OUT PendingBlockCallbacks);
}

void PhysicsComponent::HandleTransformChanged ()
{
	if (TransformOwner != nullptr)
	{
		TransformLocalToWorld = TransformOwner->ReadAbsTranslation() - TransformOwner->ReadTranslation();

		if (!bTransformIsReady && RootQuadTree != nullptr)
		{
			//All shapes need to be inserted to the QuadTree
			for (SCollisionShape& shape : CollisionShapes)
			{
				RootQuadTree->InsertObject(shape.Shape);
			}

			bTransformIsReady = true;
			ContainerUtils::Empty(OUT PendingBeginOverlap);
			ContainerUtils::Empty(OUT PendingEndOverlap);
			ContainerUtils::Empty(OUT PendingBlockCallbacks);
			UpdateCallbackTicker();
		}
	}
}

void PhysicsComponent::HandlePrePhysicsTick (Float deltaSec)
{
	SimulateVelocity(deltaSec);
}

void PhysicsComponent::HandlePhysicsTick (Float deltaSec)
{
	UpdateOverlappingShapes();
}

void PhysicsComponent::HandlePostPhysicsTick (Float deltaSec)
{
	InvokePendingCallbacks();
}
SD_END