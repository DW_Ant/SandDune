/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QuadTree.cpp
=====================================================================
*/

#include "QuadTree.h"
#include "QuadTreeVisualizer.h"

SD_BEGIN
const size_t QuadTree::MIN_OBJS(4);

QuadTree::QuadTree (const Vector2& inCenter, Float inLength, QuadTree* inOwningNode) :
	Center(inCenter),
	Length(inLength),
	OwningNode(inOwningNode)
{
	for (int i = 0; i < 4; ++i)
	{
		SubNodes[i] = nullptr;
	}

#ifdef DEBUG_MODE
	VisualTree = nullptr;
	VisualLineTransform = nullptr;
#endif
}

QuadTree::~QuadTree ()
{
	for (int i = 0; i < 4; ++i)
	{
		delete SubNodes[i];
		SubNodes[i] = nullptr;
	}
}

QuadTree* QuadTree::InsertObject (QuadTreeInterface* newObj)
{
	if (newObj == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot insert a null object to a quad tree."));
		return nullptr;
	}

	if (newObj->ReadBoundingBox().IsEmpty())
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Object with a bounding box of zero extent is inserted to a quad tree. If multiple zero extent boxes are inserted at the same spot between node borders, it would create an inefficient deep tree."));
	}

	if (IsLeaf() && Objects.size() < MIN_OBJS)
	{
		//Not enough objects to consider adding sub nodes
		Objects.push_back(newObj);
		newObj->TreeNode = this;
		return this;
	}

	if (newObj->ReadBoundingBox().Width >= Length || newObj->ReadBoundingBox().Depth >= Length)
	{
		//This object will not fit in the sub node.
		Objects.push_back(newObj);
		newObj->TreeNode = this;
		return this;
	}

	//Figure out which of the four quadrants this object can possibly belong in
	Vector2 subRegionCenter;
	int regionIdx = GetSubRegion(newObj->ReadBoundingBox(), OUT subRegionCenter);
	if (regionIdx >= 0)
	{
		if (SubNodes[regionIdx] == nullptr)
		{
			InitializeSubRegion(regionIdx, subRegionCenter);
		}

		return SubNodes[regionIdx]->InsertObject(newObj);
	}
	else
	{
		//This region overlaps one of the borders. Add this object to this node since it overlaps at least two sub nodes.
		Objects.push_back(newObj);
		newObj->TreeNode = this;
		return this;
	}
}

bool QuadTree::RemoveObject (QuadTreeInterface* target)
{
	if (target == nullptr)
	{
		PhysicsLog.Log(LogCategory::LL_Warning, TXT("Cannot remove a null object from a QuadTree."));
		return false;
	}

	bool isLeaf = IsLeaf();
	if (target->ReadBoundingBox().Width >= Length || target->ReadBoundingBox().Height >= Length)
	{
		size_t removedIdx = ContainerUtils::RemoveItem(OUT Objects, target);
		bool foundItem = (removedIdx != UINT_INDEX_NONE);
		if (foundItem && isLeaf && !IsRoot() && Objects.size() < MIN_OBJS)
		{
			target->TreeNode = nullptr;

			//This leaf has too few objects here. Move all remaining objects from this node to the parent node.
			CollapseLeaf();
		}

		return foundItem;
	}

	//Quickly search where the target may reside.
	Vector2 subRegionCenter;
	int regionIdx = GetSubRegion(target->ReadBoundingBox(), OUT subRegionCenter);
	if (regionIdx >= 0 && SubNodes[regionIdx] != nullptr)
	{
		return SubNodes[regionIdx]->RemoveObject(target);
	}

	size_t removedIdx = ContainerUtils::RemoveItem(OUT Objects, target);
	bool foundItem = (removedIdx != UINT_INDEX_NONE);
	if (foundItem)
	{
		target->TreeNode = nullptr;
	}

	return foundItem;
}

bool QuadTree::MoveObject (QuadTreeInterface* target, QuadTree*& outNewNode)
{
	size_t targetIdx = 0;
	for (; targetIdx < Objects.size(); ++targetIdx)
	{
		if (Objects.at(targetIdx) == target)
		{
			break;
		}
	}

	if (targetIdx >= Objects.size())
	{
		return false;
	}

	if (!IsLeaf() || Objects.size() >= MIN_OBJS)
	{
		//First check if it should belong in a sub node.
		Vector2 subRegionCenter;
		int subNodeIdx = GetSubRegion(target->ReadBoundingBox(), OUT subRegionCenter);
		if (subNodeIdx >= 0)
		{
			//Found a better sub node the Aabb belongs to
			//Moving target to sub node
			if (SubNodes[subNodeIdx] == nullptr)
			{
				InitializeSubRegion(subNodeIdx, subRegionCenter);
				outNewNode = SubNodes[subNodeIdx];
				//InitializeSubRegion already moved target to sub region
			}
			else
			{
				target->TreeNode = nullptr;
				outNewNode = SubNodes[subNodeIdx]->InsertObject(target);
				Objects.erase(Objects.begin() + targetIdx);
			}

			return true;
		}
	}

	if (IsRoot() || EncompassesAabb(Center, Length, target->ReadBoundingBox()))
	{
		//This is already the ideal node for the target. Don't change anything.
		outNewNode = this;
		return true;
	}

	//Climb up the tree to find the more suitable node to house the target
	for (QuadTree* parent = OwningNode; parent != nullptr; parent = parent->OwningNode)
	{
		if (parent->IsRoot() || EncompassesAabb(parent->Center, parent->Length, target->ReadBoundingBox()))
		{
			//Move the target to either the parent nodes or one of the sub nodes within parent.
			target->TreeNode = nullptr;
			outNewNode = parent->InsertObject(target);
			Objects.erase(Objects.begin() + targetIdx);

			if (IsLeaf()) //Root is already checked above
			{
				CollapseLeaf();
			}

			return true;
		}
	}

	PhysicsLog.Log(LogCategory::LL_Warning, TXT("Something went wrong when moving an object within a QuadTree. Somehow it failed to find a node to move the object to."));
	return false;
}

void QuadTree::FindOverlappingObjects (const Aabb& searchRegion, std::vector<QuadTreeInterface*>& outOverlappingObjects) const
{
	for (QuadTreeInterface* obj : Objects)
	{
		if (obj->ReadBoundingBox().Overlaps(searchRegion))
		{
			outOverlappingObjects.push_back(obj);
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		if (SubNodes[i] != nullptr && SubNodes[i]->OverlapsWith(searchRegion))
		{
			SubNodes[i]->FindOverlappingObjects(searchRegion, OUT outOverlappingObjects);
		}
	}
}

void QuadTree::FindEncompassingObjects (const Vector3& point, std::vector<QuadTreeInterface*>& outEncompassingObjects) const
{
	for (QuadTreeInterface* obj : Objects)
	{
		if (obj->ReadBoundingBox().EncompassesPoint(point))
		{
			outEncompassingObjects.push_back(obj);
		}
	}

	int regionIdx = 0;
	if (point.X < Center.X)
	{
		regionIdx = 2;
	}

	if (point.Y < Center.Y)
	{
		regionIdx++;
	}

	if (SubNodes[regionIdx] != nullptr)
	{
		SubNodes[regionIdx]->FindEncompassingObjects(point, OUT outEncompassingObjects);
	}
}

bool QuadTree::IsLeaf () const
{
	for (int i = 0; i < 4; ++i)
	{
		if (SubNodes[i] != nullptr)
		{
			return false;
		}
	}

	return true;
}

bool QuadTree::IsRoot () const
{
	return (OwningNode == nullptr);
}

bool QuadTree::EncompassesAabb (const Vector2& regionCenter, Float regionLength, const Aabb& boundingBox)
{
	Float halfLength = regionLength * 0.5f;

	//Check X-axis
	if (regionCenter.X - halfLength > boundingBox.GetBackward() || regionCenter.X + halfLength < boundingBox.GetForward())
	{
		return false;
	}

	//Check Y-axis
	if (regionCenter.Y - halfLength > boundingBox.GetLeft() || regionCenter.Y + halfLength < boundingBox.GetRight())
	{
		return false;
	}

	return true;
}

bool QuadTree::OverlapsWith (const Aabb& target) const
{
	Float halfLength = Length * 0.5f;

	//Check X-axis
	if (Center.X - halfLength > target.GetForward() || Center.X + halfLength < target.GetBackward())
	{
		return false;
	}

	//Check Y-axis
	if (Center.Y - halfLength > target.GetRight() || Center.Y + halfLength < target.GetLeft())
	{
		return false;
	}

	return true;
}

int QuadTree::GetSubRegion (const Aabb& target, Vector2& outSubRegionCenter) const
{
	outSubRegionCenter = Vector2(1.f, 1.f);
	int regionIdx = 0;
	if (target.Center.X < Center.X)
	{
		outSubRegionCenter.X *= -1.f;
		regionIdx = 2;
	}

	if (target.Center.Y < Center.Y)
	{
		outSubRegionCenter.Y *= -1.f;
		regionIdx++; //Either becomes 3 if x is negative, or 1 if x is positive
	}

	outSubRegionCenter *= (Length * 0.25f);
	outSubRegionCenter += Center;
	if (EncompassesAabb(outSubRegionCenter, Length * 0.5f, target))
	{
		return regionIdx;
	}

	return -1;
}

void QuadTree::InitializeSubRegion (int subRegionIdx, const Vector2& subRegionCenter)
{
	CHECK(SubNodes[subRegionIdx] == nullptr)

	Float halfLength = Length * 0.5f;
	SubNodes[subRegionIdx] = new QuadTree(subRegionCenter, halfLength, this);

#ifdef DEBUG_MODE
	if (VisualTree != nullptr && VisualTree->GetDrawLayer() != nullptr)
	{
		SubNodes[subRegionIdx]->VisualTree = VisualTree;

		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (VisualTree->AddComponent(transform))
		{
			transform->SetTranslation(subRegionCenter.ToVector3());
			SubNodes[subRegionIdx]->VisualLineTransform = transform;

			ColorRenderComponent* hLine = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(hLine))
			{
				hLine->SetBaseSize(halfLength, VisualTree->GetBorderThickness());
				hLine->SolidColor = VisualTree->GetBorderColor();
				hLine->SetPivot(0.5f, 0.5f);
				VisualTree->GetDrawLayer()->RegisterSingleComponent(hLine);
			}

			ColorRenderComponent* vLine = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(vLine))
			{
				vLine->SetBaseSize(VisualTree->GetBorderThickness(), halfLength);
				vLine->SolidColor = VisualTree->GetBorderColor();
				vLine->SetPivot(0.5f, 0.5f);
				VisualTree->GetDrawLayer()->RegisterSingleComponent(vLine);
			}
		}
	}
#endif

	//Check if any rectangle from this node should move to the new sub node
	size_t i = 0;
	while (i < Objects.size())
	{
		if (EncompassesAabb(subRegionCenter, halfLength, Objects.at(i)->ReadBoundingBox()))
		{
			Objects.at(i)->TreeNode = nullptr;
			QuadTree* newNode = SubNodes[subRegionIdx]->InsertObject(Objects.at(i));
			Objects.erase(Objects.begin() + i);
			continue;
		}
		++i;
	}
}

void QuadTree::CollapseLeaf ()
{
	CHECK(OwningNode != nullptr)

	//Move rects from self to parent node
	for (QuadTreeInterface* obj : Objects)
	{
		OwningNode->Objects.push_back(obj);
		obj->TreeNode = OwningNode;
	}
	ContainerUtils::Empty(OUT Objects);

	//Sever this node from the OwningNode
	for (int i = 0; i < 4; ++i)
	{
		if (OwningNode->SubNodes[i] == this)
		{
			OwningNode->SubNodes[i] = nullptr;
			break;
		}
	}

#ifdef DEBUG_MODE
	//Destroy the visual lines
	if (VisualTree != nullptr)
	{
		VisualLineTransform->Destroy(); //Also destroys the render components attached to this transform.
		VisualLineTransform = nullptr;
		VisualTree = nullptr;
	}
#endif

	//Check if the parent node needs to be collapsed, too.
	if (OwningNode->IsLeaf() && !OwningNode->IsRoot() && OwningNode->Objects.size() < MIN_OBJS)
	{
		OwningNode->CollapseLeaf();
	}

	delete this;
}

#ifdef DEBUG_MODE
void QuadTree::InitializeVisualizer (QuadTreeVisualizer* inVisualizer)
{
	CHECK(inVisualizer != nullptr && inVisualizer->GetDrawLayer() != nullptr)
	VisualTree = inVisualizer;

	VisualLineTransform = SceneTransformComponent::CreateObject();
	if (VisualTree->AddComponent(VisualLineTransform))
	{
		VisualLineTransform->SetTranslation(Center.ToVector3());

		ColorRenderComponent* hLine = ColorRenderComponent::CreateObject();
		if (VisualLineTransform->AddComponent(hLine))
		{
			hLine->SetBaseSize(Length, VisualTree->GetBorderThickness());
			hLine->SetPivot(0.5f, 0.5f);
			hLine->SolidColor = VisualTree->GetBorderColor();
			VisualTree->GetDrawLayer()->RegisterSingleComponent(hLine);
		}

		ColorRenderComponent* vLine = ColorRenderComponent::CreateObject();
		if (VisualLineTransform->AddComponent(vLine))
		{
			vLine->SetBaseSize(VisualTree->GetBorderThickness(), Length);
			vLine->SetPivot(0.5f, 0.5f);
			vLine->SolidColor = VisualTree->GetBorderColor();
			VisualTree->GetDrawLayer()->RegisterSingleComponent(vLine);
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		if (SubNodes[i] != nullptr)
		{
			SubNodes[i]->InitializeVisualizer(inVisualizer);
		}
	}
}

void QuadTree::ClearVisualizerReferences ()
{
	if (VisualTree != nullptr)
	{
		VisualTree = nullptr;
		VisualLineTransform = nullptr;

		for (int i = 0; i < 4; ++i)
		{
			if (SubNodes[i] != nullptr)
			{
				SubNodes[i]->ClearVisualizerReferences();
			}
		}
	}
}
#endif
SD_END