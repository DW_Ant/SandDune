/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CollisionSegment.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionExtrudePolygon.h"
#include "CollisionSegment.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"
#include "Geometry3dUtils.h"

SD_BEGIN
CollisionSegment::CollisionSegment () :
	CollisionShape(),
	EndPoint(Vector3::ZERO_VECTOR)
{
	Shape = ST_Segment;
}

CollisionSegment::CollisionSegment (Float inScale, const Vector3& inPosition) :
	CollisionShape(inScale, inPosition),
	EndPoint(Vector3::ZERO_VECTOR)
{
	Shape = ST_Segment;
}

CollisionSegment::CollisionSegment (const Vector3& inEndPoint) :
	CollisionShape(),
	EndPoint(inEndPoint)
{
	Shape = ST_Segment;
}

CollisionSegment::CollisionSegment (Float inScale, const Vector3& inPosition, const Vector3& inEndPoint) :
	CollisionShape(inScale, inPosition),
	EndPoint(inEndPoint)
{
	Shape = ST_Segment;
}

void CollisionSegment::RefreshBoundingBox ()
{
	BoundingBox.Center = (EndPoint * Scale * 0.5f) + Position;
	BoundingBox.Depth = Float::Abs(EndPoint.X) * Scale;
	BoundingBox.Width = Float::Abs(EndPoint.Y) * Scale;
	BoundingBox.Height = Float::Abs(EndPoint.Z) * Scale;
}

bool CollisionSegment::OverlapsWith (const CollisionShape* otherShape) const
{
	if (!CollisionShape::OverlapsWith(otherShape))
	{
		return false;
	}

	switch (otherShape->GetShape())
	{
		case (ST_Segment):
		{
			const CollisionSegment* otherSegment = dynamic_cast<const CollisionSegment*>(otherShape);
			CHECK(otherSegment != nullptr)
			return CollisionUtils::Overlaps(this, otherSegment);
		}

		case (ST_Sphere):
		{
			const CollisionSphere* otherSphere = dynamic_cast<const CollisionSphere*>(otherShape);
			CHECK(otherSphere != nullptr)
			return CollisionUtils::Overlaps(this, otherSphere);
		}

		case (ST_Capsule):
		{
			const CollisionCapsule* otherCapsule = dynamic_cast<const CollisionCapsule*>(otherShape);
			CHECK(otherCapsule != nullptr)
			return CollisionUtils::Overlaps(this, otherCapsule);
		}

		case (ST_ExtrudePolygon):
		{
			const CollisionExtrudePolygon* otherPoly = dynamic_cast<const CollisionExtrudePolygon*>(otherShape);
			CHECK(otherPoly != nullptr)
			return CollisionUtils::Overlaps(this, otherPoly);
		}
	}

	return false;
}

bool CollisionSegment::EncompassesPoint (const Vector3& point) const
{
	return Geometry3dUtils::IsOnSegment(Position, GetAbsEndPoint(), point);
}

Vector3 CollisionSegment::CalcClosestPoint (const Vector3& desiredPoint) const
{
	Vector3 intersection;
	Geometry3dUtils::CalcDistBetweenPointAndLine(Position, GetDirection(), desiredPoint, true, OUT intersection);

	return intersection;
}

void CollisionSegment::SetEndPoint (const Vector3& newEndPoint)
{
	EndPoint = newEndPoint;
	RefreshBoundingBox();
}
SD_END