/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Geometry2dUtils.cpp
=====================================================================
*/

#include "Geometry2dUtils.h"

IMPLEMENT_ABSTRACT_CLASS(SD::Geometry2dUtils, SD::BaseUtils)
SD_BEGIN

bool Geometry2dUtils::ArePointsCollinear (const Vector2& a, const Vector2& b, const Vector2& c, Float tolerance)
{
	return (CrossProduct2d(a - b, a - c).IsCloseTo(0.f, tolerance));
}

bool Geometry2dUtils::IsOnSegment (const Vector2& segPtA, const Vector2& segPtB, const Vector2& testPt, Float tolerance)
{
	if (!ArePointsCollinear(segPtA, segPtB, testPt, tolerance))
	{
		return false;
	}

	//Test distance sizes to see if it's beyond the length of the segment.
	if ((testPt - segPtA).CalcDistSquared() > (segPtA - segPtB).CalcDistSquared())
	{
		//Beyond length
		return false;
	}

	//Test that the test point is not the reversed direction of the vector.
	Vector2 multipliers = (testPt - segPtA) * (segPtB - segPtA);

	//If any axis is negative, then it assumes that the vector is facing the opposite direction.
	return (multipliers.X >= 0.f && multipliers.Y >= 0.f);
}

bool Geometry2dUtils::IsBetweenCollinearPts (const Vector2& endPtA, const Vector2& endPtB, const Vector2& testPt)
{
	Float delta;
	if (endPtA.X != endPtB.X)
	{
		delta = ((testPt.X - endPtA.X) / (endPtB.X - endPtA.X));
	}
	else
	{
		delta = ((testPt.Y - endPtA.Y) / (endPtB.Y - endPtA.Y));
	}

	return (delta >= 0.f && delta <= 1.f);
}

Float Geometry2dUtils::CalcDistToLine (const Vector2& pt, const Vector2& linePt1, const Vector2& linePt2)
{
	return (pt - CalcClosestPointOnLine(pt, linePt1, linePt2)).VSize();
}

Float Geometry2dUtils::CalcDistToSegment (const Vector2& pt, const Vector2& segPt1, const Vector2& segPt2)
{
	return (pt - CalcClosestPointOnSegment(pt, segPt1, segPt2)).VSize();
}

Vector2 Geometry2dUtils::CalcClosestPointOnLine (const Vector2& target, const Vector2& linePt1, const Vector2& linePt2)
{
	//Formula is pulled from: https://www.geometrictools.com/Documentation/DistancePointLine.pdf
	Vector2 dir = linePt2 - linePt1;
	Float ptProjection = dir.Dot(target - linePt1) / dir.Dot(dir); //Find the "percent" where pt resides between linePt1 and linePt2

	return (linePt1 + (dir * ptProjection));
}

Vector2 Geometry2dUtils::CalcClosestPointOnSegment (const Vector2& target, const Vector2& segPt1, const Vector2& segPt2)
{
	//Formula is pulled from: https://www.geometrictools.com/Documentation/DistancePointLine.pdf
	Vector2 dir = segPt2 - segPt1;
	Float ptProjection = dir.Dot(target - segPt1) / dir.Dot(dir); //Find the "percent" where pt resides between linePt1 and linePt2

	if (ptProjection >= 0.f && ptProjection <= 1.f)
	{
		//Point resides on the segment
		return (segPt1 + (dir * ptProjection));
	}
	else if (ptProjection < 0.f)
	{
		//Point is 'behind' the first point. The closest distance from point is to that end point
		return segPt1;
	}
	else
	{
		//The point is 'beyond' the second point.
		return segPt2;
	}
}

void Geometry2dUtils::CalcSegmentIntersection (const Vector2& seg1PtA, const Vector2& seg1PtB, const Vector2& seg2PtA, const Vector2& seg2PtB, SIntersectResults& outIntersection)
{
	CalcLineIntersection(seg1PtA, seg1PtB, seg2PtA, seg2PtB, OUT outIntersection);
	switch (outIntersection.IntersectionType)
	{
		case (IT_None): return;
		case (IT_Point):
		{
			//Double check if the point of intersection is between the two segments.
			if (!IsBetweenCollinearPts(seg1PtA, seg1PtB, outIntersection.Pt1) ||
				!IsBetweenCollinearPts(seg2PtA, seg2PtB, outIntersection.Pt1))
			{
				//The intersection point is beyond one of the segments.
				outIntersection.IntersectionType = IT_None;
			}

			return;
		}

		case (IT_Line):
		{
			//Check if one segment encompasses the other.
			if (IsBetweenCollinearPts(seg2PtA, seg2PtB, seg1PtA) && IsBetweenCollinearPts(seg2PtA, seg2PtB, seg1PtB))
			{
				outIntersection.IntersectionType = IT_Segment;
				outIntersection.Pt1 = seg1PtA;
				outIntersection.Pt2 = seg1PtB;
				return;
			}
			else if (IsBetweenCollinearPts(seg1PtA, seg1PtB, seg2PtA) && IsBetweenCollinearPts(seg1PtA, seg1PtB, seg2PtB))
			{
				outIntersection.IntersectionType = IT_Segment;
				outIntersection.Pt1 = seg2PtA;
				outIntersection.Pt2 = seg2PtB;
				return;
			}

			//Check for partially overlapping segments
			//All four segment points sorted in ascending order
			std::vector<const Vector2*> sortedPts{&seg1PtA, &seg1PtB, &seg2PtA, &seg2PtB};
			if (seg1PtA.X == seg1PtB.X)
			{
				std::sort(sortedPts.begin(), sortedPts.end(), [](const Vector2* a, const Vector2* b)
				{
					return (a->Y < b->Y);
				});
			}
			else
			{
				std::sort(sortedPts.begin(), sortedPts.end(), [](const Vector2* a, const Vector2* b)
				{
					return (a->X < b->X);
				});
			}

			if (Vector2::CalcDistBetween(seg1PtA, seg1PtB) + Vector2::CalcDistBetween(seg2PtA, seg2PtB) <= Vector2::CalcDistBetween(*sortedPts[0], *sortedPts[3]))
			{
				//The maximum possible size for the two segments to overlap is the sum of the combined vectors.
				//If the range between the two extreme pts are greater than the allowable size, then we know the segments do NOT overlap.
				outIntersection.IntersectionType = IT_None;
			}
			else
			{
				//The range between the two most extreme points are within the maximum allowable size for overlapping segments.
				outIntersection.IntersectionType = IT_Segment;

				//The points in between the two end extreme points is the intersection overlap.
				outIntersection.Pt1 = *sortedPts[1];
				outIntersection.Pt2 = *sortedPts[2];
			}

			return;
		}
	}
}

void Geometry2dUtils::CalcRayIntersection (const Vector2& ray1PtA, const Vector2& ray1PtB, const Vector2& ray2PtA, const Vector2& ray2PtB, SIntersectResults& outIntersection)
{
	CalcLineIntersection(ray1PtA, ray1PtB, ray2PtA, ray2PtB, OUT outIntersection);
	switch (outIntersection.IntersectionType)
	{
		case (IT_None): return; //These parallel lines never intersect
		case (IT_Point):
		{
			if (!IsRayEncompassingPoint(outIntersection.Pt1, ray1PtA, ray1PtB) ||
				!IsRayEncompassingPoint(outIntersection.Pt1, ray2PtA, ray2PtB))
			{
				//Detected a negative. The intersection point is in the wrong direction for one of the two rays.
				outIntersection.IntersectionType = IT_None;
			}

			return;
		}
		case (IT_Line):
		{
			//Figure out if the rays are facing opposite or the same directions
			Vector2 direction = (ray1PtB - ray1PtA) * (ray2PtB - ray2PtA);
			if (direction.X > 0.f && direction.Y > 0.f)
			{
				outIntersection.IntersectionType = IT_Ray;

				//Rays are facing towards the same direction
				//The overlap is starting from the ray that encompasses the other.
				direction = (ray1PtB - ray1PtA);
				if (direction.X > 0.f)
				{
					//Since the vector is heading towards the positive direction, use the starting point that is greater than the two vectors.
					outIntersection.Pt1 = (ray1PtA.X < ray2PtA.X) ? ray2PtA : ray1PtA;
				}
				else if (direction.X > 0.f)
				{
					//Since the vector is heading towards the negative direction, use the starting point that is less than the two vectors.
					outIntersection.Pt1 = (ray1PtA.X > ray2PtA.X) ? ray2PtA : ray1PtA;
				}
				else if (direction.Y > 0.f)
				{
					//It's a vertical line. Do the same checks but for Y-axis.
					outIntersection.Pt1 = (ray1PtA.Y < ray2PtA.Y) ? ray2PtA : ray1PtA;
				}
				else //direction.Y <= 0.f Note: If the rays are identical, then it doesn't matter which of the two end points it picks
				{
					outIntersection.Pt1 = (ray1PtA.Y > ray2PtA.Y) ? ray2PtA : ray1PtA;
				}

				outIntersection.Pt2 = outIntersection.Pt1 + direction;
				return;
			}
			else
			{
				//Nudge the ray1 starting point towards its direction and its reverse direction.
				//If the distance between ray 1's starting point is closer to ray 2's starting point compared to the distance when using the reverse direction, then we know the vectors are facing towards each other.
				direction = (ray1PtB - ray1PtA);
				if (((ray1PtA + direction) - ray2PtA).CalcDistSquared() < ((ray1PtA - direction) - ray2PtA).CalcDistSquared())
				{
					//The two rays are facing towards each other. The intersection is the segment between their end points.
					outIntersection.IntersectionType = IT_Segment;
					outIntersection.Pt1 = ray1PtA;
					outIntersection.Pt2 = ray2PtA;
				}
				else
				{
					//The two rays are facing away from each other. They don't intersect.
					outIntersection.IntersectionType = IT_None;
				}
			}

			return;
		}
	}
}

void Geometry2dUtils::CalcLineIntersection (const Vector2& line1PtA, const Vector2& line1PtB, const Vector2& line2PtA, const Vector2& line2PtB, SIntersectResults& outIntersection)
{
	//There are faster algorithms such as the one described here: https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
	//But my dumb brain couldn't quite comprehend it so I went with my own approach. Even if it's far messier with the if else clauses :(

	Float line1Slope;
	Float line1Intercept;
	bool line1ValidSlope = CalcLineFormula(line1PtA, line1PtB, OUT line1Slope, OUT line1Intercept);

	Float line2Slope;
	Float line2Intercept;
	bool line2ValidSlope = CalcLineFormula(line2PtA, line2PtB, OUT line2Slope, OUT line2Intercept);
	if (!line1ValidSlope && !line2ValidSlope)
	{
		if (line1PtA.X == line2PtA.X)
		{
			outIntersection.IntersectionType = IT_Line;
			outIntersection.Pt1 = line1PtA;
			outIntersection.Pt2 = line2PtA;
			return;
		}
		else
		{
			//Vertical parallel lines do not intersect
			outIntersection.IntersectionType = IT_None;
			return;
		}
	}
	else if (!line1ValidSlope)
	{
		outIntersection.IntersectionType = IT_Point;
		outIntersection.Pt1.X = line1PtA.X;
		outIntersection.Pt1.Y = (line1PtA.X * line2Slope) + line2Intercept;
		return;
	}
	else if (!line2ValidSlope)
	{
		outIntersection.IntersectionType = IT_Point;
		outIntersection.Pt1.X = line2PtA.X;
		outIntersection.Pt1.Y = (line2PtA.X * line1Slope) + line1Intercept;
		return;
	}
	else if (line1Slope == line2Slope)
	{
		if (line1Intercept == line2Intercept)
		{
			//Lines are equal
			outIntersection.IntersectionType = IT_Line;
			outIntersection.Pt1 = line1PtA;
			outIntersection.Pt2 = line2PtA;
			return;
		}
		else
		{
			//These parallel lines never intersect
			outIntersection.IntersectionType = IT_None;
			return;
		}
	}
	else
	{
		outIntersection.IntersectionType = IT_Point;
		outIntersection.Pt1.X = (line2Intercept - line1Intercept) / (line1Slope - line2Slope);
		outIntersection.Pt1.Y = (line1Slope * outIntersection.Pt1.X) + line1Intercept;
	}
}

void Geometry2dUtils::CalcLineRayIntersection (const Vector2& linePtA, const Vector2& linePtB, const Vector2& rayPtA, const Vector2& rayPtB, SIntersectResults& outIntersection)
{
	CalcLineIntersection(linePtA, linePtB, rayPtA, rayPtB, OUT outIntersection);
	switch (outIntersection.IntersectionType)
	{
		case (IT_None): return;
		case (IT_Point):
		{
			//Check if the point of intersection is behind the ray or not.
			if (!IsRayEncompassingPoint(outIntersection.Pt1, rayPtA, rayPtB))
			{
				//Ray is not pointing towards the intersection point.
				outIntersection.IntersectionType = IT_None;
			}

			return;
		}
		case (IT_Line):
		{
			//The intersection encompasses the entire ray
			outIntersection.IntersectionType = IT_Ray;
			outIntersection.Pt1 = rayPtA;
			outIntersection.Pt2 = rayPtB;
			return;
		}
	}
}

void Geometry2dUtils::CalcLineSegIntersection (const Vector2& linePtA, const Vector2& linePtB, const Vector2& segPtA, const Vector2& segPtB, SIntersectResults& outIntersection)
{
	CalcLineIntersection(linePtA, linePtB, segPtA, segPtB, OUT outIntersection);
	switch (outIntersection.IntersectionType)
	{
		case(IT_None): return;
		case(IT_Point):
		{
			//Ensure the intersection is within the segment range.
			if (!IsBetweenCollinearPts(segPtA, segPtB, outIntersection.Pt1))
			{
				outIntersection.IntersectionType = IT_None;
			}
			return;
		}
		case(IT_Line):
		{
			//The parallel line encompasses the entire segment.
			outIntersection.IntersectionType = IT_Segment;
			outIntersection.Pt1 = segPtA;
			outIntersection.Pt2 = segPtB;
			return;
		}
	}
}

void Geometry2dUtils::CalcRaySegIntersection (const Vector2& rayPtA, const Vector2& rayPtB, const Vector2& segPtA, const Vector2& segPtB, SIntersectResults& outIntersection)
{
	CalcLineIntersection(rayPtA, rayPtB, segPtA, segPtB, OUT outIntersection);
	switch (outIntersection.IntersectionType)
	{
		case (IT_None): return;
		case (IT_Point):
		{
			//Ensure it's within range of the segment
			if (!IsBetweenCollinearPts(segPtA, segPtB, outIntersection.Pt1))
			{
				outIntersection.IntersectionType = IT_None;
				return;
			}

			//Ensure the ray is pointing towards the point of intersection.
			if (!IsRayEncompassingPoint(outIntersection.Pt1, rayPtA, rayPtB))
			{
				outIntersection.IntersectionType = IT_None;
			}

			return;
		}
		case (IT_Line):
		{
			Int numEndPoints = 0; //How many end points the ray encompasses.
			const Vector2* segEnd = nullptr; //Useful for when the ray encompasses only one of the two end points. This indicates which end point it encompasses.
			if (IsRayEncompassingPoint(segPtA, rayPtA, rayPtB))
			{
				++numEndPoints;
				segEnd = &segPtA;
			}

			if (IsRayEncompassingPoint(segPtB, rayPtA, rayPtB))
			{
				++numEndPoints;
				segEnd = &segPtB;
			}

			switch (numEndPoints.Value)
			{
				case(0):
				{
					//Ray is facing away from parallel segment
					outIntersection.IntersectionType = IT_None;
					return;
				}
				case(1):
				{
					//The Ray's starting point is inside the segment. The overlap is between the ray starting point and the segment's encompassed end point.
					outIntersection.IntersectionType = IT_Segment;
					outIntersection.Pt1 = rayPtA;
					outIntersection.Pt2 = *segEnd;
					return;
				}
				case(2):
				{
					//The ray's starting point is outside the segment, but it's facing towards the parallel segment.
					outIntersection.IntersectionType = IT_Segment;
					outIntersection.Pt1 = segPtA;
					outIntersection.Pt2 = segPtB;
					return;
				}
				default:
					CHECK_INFO(false, "Error when calculating the intersection point between a ray parallel to a segment. Some how this function generated an invalid counter.")
			}
		}
	}
}

bool Geometry2dUtils::CalcCircleSegIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& segPtA, const Vector2& segPtB, std::vector<Vector2>& outIntersections)
{
	if (CalcCircleLineIntersections(circleCenter, circleRadius, segPtA, segPtB, OUT outIntersections))
	{
		size_t i = 0;
		while (i < outIntersections.size())
		{
			if (!IsBetweenCollinearPts(segPtA, segPtB, outIntersections[i]))
			{
				//Intersection is outside the segment range. Remove from the intersection list.
				outIntersections.erase(outIntersections.begin() + i);
				continue;
			}

			++i;
		}
	}

	return !ContainerUtils::IsEmpty(outIntersections);
}

bool Geometry2dUtils::CalcCircleRayIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& rayStartPt, const Vector2& rayPtB, std::vector<Vector2>& outIntersections)
{
	if (CalcCircleLineIntersections(circleCenter, circleRadius, rayStartPt, rayPtB, OUT outIntersections))
	{
		size_t i = 0;
		while (i < outIntersections.size())
		{
			if (!IsRayEncompassingPoint(outIntersections[i], rayStartPt, rayPtB))
			{
				//Intersection is outside the segment range. Remove from the intersection list.
				outIntersections.erase(outIntersections.begin() + i);
				continue;
			}

			++i;
		}
	}

	return !ContainerUtils::IsEmpty(outIntersections);
}

bool Geometry2dUtils::CalcCircleLineIntersections (const Vector2& circleCenter, Float circleRadius, const Vector2& linePtA, const Vector2& linePtB, std::vector<Vector2>& outIntersections)
{
	Vector2 lineDir = (linePtB - linePtA);
	if (lineDir.IsEmpty())
	{
		//This could only intersect if the point resides exactly on the circle.
		if ((linePtA - circleCenter).CalcDistSquared().IsCloseTo(Float::Pow(circleRadius, 2.f)))
		{
			outIntersections.push_back(linePtA);
		}

		return !(ContainerUtils::IsEmpty(outIntersections));
	}

	Vector2 lineDirNorm(Vector2::Normalize(lineDir));

	//Project line connecting the linePtA to circle center onto the line
	Float projectionFactor = lineDirNorm.Dot(circleCenter - linePtA);
	Vector2 t = linePtA + (projectionFactor * lineDirNorm);

	//t represents the point on the line where a line perpendicular to the line also intersects the center of the circle.
	//This forms a right triangle inside the circle. The sides being y, x, and r.
	//x is the segment of the right triangle that connects t and the edge of the circle. This line is also parallel to the line defined by this function's parameters.
	//y is the segment that connects t and the circle's center. The angle x and y forms a right angle since the segment is perpendicular.
	//r is the hypotenus. That's simply the radius of the circle. Since we know y and r, solve for x using pythagorean theorem.
	//x = +- sqrt(r^2 - y^2)
	Float ySquared = (circleCenter - t).CalcDistSquared();
	Float rSquared(circleRadius);
	rSquared.PowInline(2.f);

	if (ySquared > rSquared)
	{
		//The line doesn't intersect the circle.
		return false;
	}
	else if (ySquared.IsCloseTo(rSquared))
	{
		//The line only intersects the circle once since it's a tangent to the circle.
		outIntersections.push_back(t);
		return true;
	}

	Float x = Float::Pow(rSquared - ySquared, 0.5f);
	outIntersections.push_back(linePtA + ((projectionFactor + x) * lineDirNorm));
	outIntersections.push_back(linePtA + ((projectionFactor - x) * lineDirNorm));
	return true;
}

Float Geometry2dUtils::CrossProduct2d (const Vector2& a, const Vector2& b)
{
	return (a.X * b.Y) - (a.Y * b.X);
}

bool Geometry2dUtils::CalcLineFormula (const Vector2& pt1, const Vector2& pt2, Float& outSlope, Float& outYIntercept)
{
	if (pt1.X == pt2.X)
	{
		return false;
	}

	outSlope = (pt1.Y - pt2.Y) / (pt1.X - pt2.X);
	//y = mx + b
	//Solve for b
	outYIntercept = pt1.Y - (outSlope * pt1.X);
	return true;
}

bool Geometry2dUtils::IsRayEncompassingPoint (const Vector2& testPt, const Vector2& rayStart, const Vector2& rayPt)
{
	/*
	Multiply the deltas between both points. If both deltas' signage are equal then it resides in the ray.
	Formula: (PointB - PointA) * (Intersection - PointA).
	If either axis is negative, then that means the intersection is heading the wrong direction from the ray's end point.
	1D example:
	Say PointA = 5, PointB = 10.
	If the intercept is 100, the equation is: (10-5) * (100-5) = 475.
	The numeric value from the multiplication doesn't matter. What matters is that it's positive. Positive means it's in alignment (heading down the ray from PointA towards PointB, you'll eventually hit intercept).
	Had the intercept been 1, the equation is: (10-5) * (1-5) = -20
	Negative means it's outside the ray since it's heading towards the wrong direction.
	Another 1D example:
	Say PointA = 5, PointB = -5.
	If the intercept is 2, the equation is: (-5 - 5) * (2 - 5) = 30
	Positive means the intercept is within range.
	If the intercept is 6, the equation is: (-5 - 5) * (6 - 5) = -10
	Negative means the intercept is not in range.
	*/
	Vector2 direction = (rayPt - rayStart) * (testPt - rayStart);
	return (direction.X >= 0.f && direction.Y >= 0.f);
}
SD_END