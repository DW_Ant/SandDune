/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacInput.cpp
=====================================================================
*/


#include "PlatformMacInput.h"

#ifdef PLATFORM_MAC

SD_BEGIN
void OS_SetMousePosition (const Vector2& mousePosition)
{
#error Please implement OS_SetMousePosition for your platform.
}

Vector2 OS_GetMousePosition ()
{
#error Please implement OS_GetMousePosition for your platform.
}

void OS_ClampMousePointer (const Rectangle& region)
{
#error Please implement OS_ClampMousePointer for your platform.
}
SD_END

#endif