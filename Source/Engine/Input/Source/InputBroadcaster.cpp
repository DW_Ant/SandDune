/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputBroadcaster.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputComponent.h"
#include "InputEngineComponent.h"
#include "MousePointer.h"

IMPLEMENT_CLASS(SD::InputBroadcaster, SD::Object)
SD_BEGIN

EKeyboardState InputBroadcaster::KeyboardState = KS_None;

void InputBroadcaster::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	bDebugKeyEvent = false;
	bDebugTextEvent = false;
	bDebugMouseMove = false;
	bDebugMouseButton = false;
	bDebugMouseWheel = false;
#endif

	InputSource = nullptr;
	AssociatedMouse = nullptr;
}

void InputBroadcaster::BeginObject ()
{
	Super::BeginObject();

	Engine::FindEngine()->RegisterPreGarbageCollectEvent(SDFUNCTION(this, InputBroadcaster, HandleGarbageCollection, void));
	InputEngineComponent::Find()->RegisterInputBroadcaster(this);
}

void InputBroadcaster::Destroyed ()
{
	Engine::FindEngine()->RemovePreGarbageCollectEvent(SDFUNCTION(this, InputBroadcaster, HandleGarbageCollection, void));
	InputEngineComponent* localInputEngine = InputEngineComponent::Find();
	if (localInputEngine != nullptr) //This can be null if the InputEngine was removed before the InputBroadcaster.
	{
		localInputEngine->UnregisterInputBroadcaster(this);
	}

	if (AssociatedMouse.IsValid())
	{
		AssociatedMouse->Destroy();
	}

	//Unregister polling events from window source
	SetInputSource(nullptr);

	Super::Destroyed();
}

InputBroadcaster* InputBroadcaster::FindBroadcaster (Window* associatedWindow)
{
	InputEngineComponent* localInputEngine = InputEngineComponent::Find();
	if (localInputEngine == nullptr)
	{
		return nullptr;
	}

	return localInputEngine->FindBroadcasterForWindow(associatedWindow);
}

void InputBroadcaster::SetInputSource (Window* targetWindow)
{
	if (InputSource.IsValid())
	{
		InputSource->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, InputBroadcaster, HandleWindowEvent, void, const sf::Event&));
		InputSource = nullptr;
	}

	if (targetWindow != nullptr)
	{
		targetWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, InputBroadcaster, HandleWindowEvent, void, const sf::Event&));
		InputSource = targetWindow;
	}
}

void InputBroadcaster::AddInputComponent (InputComponent* newComponent, Int priority)
{
	if (newComponent->PendingInputMessenger.IsValid() && newComponent->PendingInputMessenger != this)
	{
		//Notify the other broadcaster not to include add this component to avoid having this broadcaster from overriding our new changes.
		InputBroadcaster* otherBroadcaster = newComponent->PendingInputMessenger.Get();
		for (size_t i = 0; i < otherBroadcaster->PendingRegisterComponents.size(); ++i)
		{
			if (otherBroadcaster->PendingRegisterComponents.at(i).Component == newComponent)
			{
				otherBroadcaster->PendingRegisterComponents.erase(otherBroadcaster->PendingRegisterComponents.begin() + i);
				break;
			}
		}

		newComponent->PendingInputMessenger = nullptr;
	}

	if (bLockedRegisteredComponents)
	{
		//See if this component is already pending
		for (size_t i = 0; i < PendingRegisterComponents.size(); ++i)
		{
			if (PendingRegisterComponents.at(i).Component == newComponent)
			{
				PendingRegisterComponents.at(i).Priority = priority;
				return; //already pending
			}
		}

		PendingRegisterComponents.push_back(SInputPriorityMapping(newComponent, priority));
		newComponent->PendingInputMessenger = this;
		return;
	}

	if (newComponent->InputMessenger == this && newComponent->GetInputPriority() == priority)
	{
		return; //already registered with the same input priority
	}

	if (newComponent->InputMessenger.IsValid())
	{
		//InputLog.Log(LogCategory::LL_Warning, TXT("Unregistering %s from %s since an InputComponent may only have a single input source."), newComponent->ToString(), newComponent->InputMessenger->ToString());
		newComponent->InputMessenger->RemoveInputComponent(newComponent);
	}

	SetInputMessengerFor(newComponent);

	size_t i = 0;
	while (i < RegisteredInputComponents.size())
	{
		if (RegisteredInputComponents.at(i).Priority < priority)
		{
			break;
		}

		++i;
	}

	newComponent->InputPriority = priority;
	RegisteredInputComponents.insert(RegisteredInputComponents.begin() + i, SInputPriorityMapping(newComponent, priority));
}

bool InputBroadcaster::SetInputPriority (InputComponent* targetComponent, Int newPriority)
{
	RemoveInputComponent(targetComponent);
	AddInputComponent(targetComponent, newPriority);

	return true;
}

bool InputBroadcaster::RemoveInputComponent (InputComponent* target)
{
	if (bLockedRegisteredComponents)
	{
		//See if target is already pending removal
		for (size_t i = 0; i < PendingUnregisterComponents.size(); ++i)
		{
			if (PendingUnregisterComponents.at(i) == target)
			{
				return false; //Already pending removal
			}
		}

		PendingUnregisterComponents.push_back(target);
		return false;
	}

	for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
	{
		if (RegisteredInputComponents.at(i).Component == target)
		{
			RegisteredInputComponents.erase(RegisteredInputComponents.begin() + i);
			ClearInputMessengerFor(target);
			return true;
		}
	}

	return false;
}

void InputBroadcaster::SetAssociatedMouse (MousePointer* newAssociatedMouse)
{
	AssociatedMouse = newAssociatedMouse;
}

#ifdef DEBUG_MODE
void InputBroadcaster::LogRegisteredInputComponents () const
{
	InputLog.Log(LogCategory::LL_Debug, TXT("-== Listing registered input components for %s ==-"), ToString());
	InputLog.Log(LogCategory::LL_Debug, TXT("    [ArrayIdx]:  ObjectName (Priority Value)"));

	for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
	{
		const DString friendlyName = (VALID_OBJECT(RegisteredInputComponents.at(i).Component->GetOwner())) ? RegisteredInputComponents.at(i).Component->GetOwner()->ToString() : RegisteredInputComponents.at(i).Component->ToString();
		InputLog.Log(LogCategory::LL_Debug, TXT("    [%s]:  %s (%s)"), DString::MakeString(i), friendlyName, RegisteredInputComponents.at(i).Priority);
	}

	InputLog.Log(LogCategory::LL_Debug, TXT("-== End debug list of input components ==-"));
}
#endif

void InputBroadcaster::BroadcastNewEvent (const sf::Event& newEvent)
{
	switch (newEvent.type)
	{
		case(sf::Event::KeyPressed) :
		case(sf::Event::KeyReleased) :
			NewKeyEvent(newEvent);
			break;

		case(sf::Event::TextEntered) :
			NewTextEvent(newEvent);
			break;

		case(sf::Event::MouseMoved) :
			NewMouseMoveEvent(newEvent.mouseMove);
			break;

		case(sf::Event::MouseButtonReleased) :
		case(sf::Event::MouseButtonPressed) :
			NewMouseButtonEvent(newEvent.mouseButton, newEvent.type);
			break;

		case(sf::Event::MouseWheelScrolled) :
			NewMouseWheelEvent(newEvent.mouseWheelScroll);
			break;

		case (sf::Event::Closed) : //Invoke if window is closed explicitly, or when engine destroys window
			if (!GetPendingDelete()) //If the owner of this broadcaster didn't already mark this Entity for destruction.
			{
				Destroy();
			}
			break;
	}
}

MousePointer* InputBroadcaster::GetAssociatedMouse () const
{
	return AssociatedMouse.Get();
}

EKeyboardState InputBroadcaster::GetKeyboardState ()
{
	return KeyboardState;
}

bool InputBroadcaster::GetCtrlHeld ()
{
	return (KeyboardState & KS_Ctrl) > 0;
}

bool InputBroadcaster::GetShiftHeld ()
{
	return (KeyboardState & KS_Shift) > 0;
}

bool InputBroadcaster::GetAltHeld ()
{
	return (KeyboardState & KS_Alt) > 0;
}

Window* InputBroadcaster::GetInputSource () const
{
	return InputSource.Get();
}

void InputBroadcaster::SetLockedRegisteredComponents (bool bNewLockedRegisteredComponents)
{
	bLockedRegisteredComponents = bNewLockedRegisteredComponents;

	if (bLockedRegisteredComponents)
	{
		return;
	}

	//Execute all pending input components
	for (size_t i = 0; i < PendingUnregisterComponents.size(); ++i)
	{
		RemoveInputComponent(PendingUnregisterComponents.at(i));
	}
	PendingUnregisterComponents.clear();

	for (const SInputPriorityMapping& curPriorityMapping : PendingRegisterComponents)
	{
		AddInputComponent(curPriorityMapping.Component, curPriorityMapping.Priority);
		curPriorityMapping.Component->PendingInputMessenger = nullptr;
	}
	PendingRegisterComponents.clear();
}

void InputBroadcaster::NewKeyEvent (sf::Event newEvent)
{
#ifdef DEBUG_MODE
	if (bDebugKeyEvent)
	{
		InputLog.Log(LogCategory::LL_Debug, TXT("%s received new key event (%s)"), ToString(), KeyEventToString(newEvent));
	}
#endif

	KeyboardState = KS_None;
	if (newEvent.key.control)
	{
		KeyboardState |= KS_Ctrl;
	}

	if (newEvent.key.shift)
	{
		KeyboardState |= KS_Shift;
	}

	if (newEvent.key.alt)
	{
		KeyboardState |= KS_Alt;
	}

	SetLockedRegisteredComponents(true);
	{
		//Process passive events first
		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				RegisteredInputComponents.at(i).Component->ProcessInput(newEvent);
			}
		}

		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (!RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				continue;
			}

			if (RegisteredInputComponents.at(i).Component->CaptureInput(newEvent))
			{
#ifdef DEBUG_MODE
				if (bDebugKeyEvent)
				{
					InputLog.Log(LogCategory::LL_Debug, TXT("    %s (Handler: %s) captured key event (%s)"), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Component->OnInput, KeyEventToString(newEvent));
				}
#endif

				break;
			}
		}
	}
	SetLockedRegisteredComponents(false);
}

void InputBroadcaster::NewTextEvent (sf::Event newEvent)
{
#ifdef DEBUG_MODE
	if (bDebugTextEvent)
	{
		InputLog.Log(LogCategory::LL_Debug, TXT("%s received new text event \"%s\""), ToString(), TextEventToString(newEvent));
	}
#endif

	SetLockedRegisteredComponents(true);
	{
		//Process passive events first
		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				RegisteredInputComponents.at(i).Component->ProcessText(newEvent);
			}
		}

		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (!RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				continue;
			}

			if (RegisteredInputComponents.at(i).Component->CaptureText(newEvent))
			{
#ifdef DEBUG_MODE
				if (bDebugTextEvent)
				{
					InputLog.Log(LogCategory::LL_Debug, TXT("    %s (Handler: %s) captured text event \"%s\""), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Component->OnText, TextEventToString(newEvent));
				}
#endif

				break;
			}
		}
	}
	SetLockedRegisteredComponents(false);
}

void InputBroadcaster::NewMouseMoveEvent (sf::Event::MouseMoveEvent sfmlEvent)
{
	if (AssociatedMouse.IsNullptr())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Failed to broadcast mouse move event since an associated mouse is not bound to %s"), ToString());
		return;
	}

	Vector2 deltaMove((float)sfmlEvent.x - OldMousePosition.X, (float)sfmlEvent.y - OldMousePosition.Y);
	OldMousePosition = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));

#ifdef DEBUG_MODE
	if (bDebugMouseMove)
	{
		InputLog.Log(LogCategory::LL_Debug, TXT("%s received new mouse move event %s.   Delta mouse move is:  %s"), ToString(), MouseMoveEventToString(sfmlEvent), deltaMove);
	}
#endif

	SetLockedRegisteredComponents(true);
	for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
	{
		if (RegisteredInputComponents.at(i).Component->CanReceiveInput())
		{
			RegisteredInputComponents.at(i).Component->MouseMove(AssociatedMouse.Get(), sfmlEvent, deltaMove);
		}
	}
	SetLockedRegisteredComponents(false);
}

void InputBroadcaster::NewMouseButtonEvent (sf::Event::MouseButtonEvent sfmlEvent, sf::Event::EventType eventType)
{
	if (AssociatedMouse.IsNullptr())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Failed to broadcast mouse button event since an associated mouse is not bound to %s"), ToString());
		return;
	}

#ifdef DEBUG_MODE
	if (bDebugMouseButton)
	{
		InputLog.Log(LogCategory::LL_Debug, TXT("%s received new mouse button event (%s)"), ToString(), MouseButtonEventToString(sfmlEvent));
	}
#endif

	SetLockedRegisteredComponents(true);
	{
		//Process passive events first
		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				RegisteredInputComponents.at(i).Component->ProcessMouseClick(AssociatedMouse.Get(), sfmlEvent, eventType);
			}
		}

		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (!RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				continue;
			}

			if (RegisteredInputComponents.at(i).Component->CaptureMouseClick(AssociatedMouse.Get(), sfmlEvent, eventType))
			{
#ifdef DEBUG_MODE
				if (bDebugMouseButton)
				{
					InputLog.Log(LogCategory::LL_Debug, TXT("    %s (Handler: %s) captured mouse button event (%s)"), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Component->OnMouseClick, MouseButtonEventToString(sfmlEvent));
				}
#endif

				break;
			}
		}
	}
	SetLockedRegisteredComponents(false);
}

void InputBroadcaster::NewMouseWheelEvent (sf::Event::MouseWheelScrollEvent sfmlEvent)
{
	if (AssociatedMouse.IsNullptr())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Failed to broadcast mouse wheel event since an associated mouse is not bound to %s"), ToString());
		return;
	}

#ifdef DEBUG_MODE
	if (bDebugMouseWheel)
	{
		InputLog.Log(LogCategory::LL_Debug, TXT("%s received new mouse wheel event (%s)"), ToString(), MouseWheelEventToString(sfmlEvent));
	}
#endif

	SetLockedRegisteredComponents(true);
	{
		//Process passive events first
		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				RegisteredInputComponents.at(i).Component->ProcessMouseWheel(AssociatedMouse.Get(), sfmlEvent);
			}
		}

		for (size_t i = 0; i < RegisteredInputComponents.size(); ++i)
		{
			if (!RegisteredInputComponents.at(i).Component->CanReceiveInput())
			{
				continue;
			}

			if (RegisteredInputComponents.at(i).Component->CaptureMouseWheel(AssociatedMouse.Get(), sfmlEvent))
			{
#ifdef DEBUG_MODE
				if (bDebugMouseWheel)
				{
					InputLog.Log(LogCategory::LL_Debug, TXT("    %s (Handler: %s) captured mouse wheel event (%s)"), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Component->OnMouseWheel, MouseWheelEventToString(sfmlEvent));
				}
#endif

				break;
			}
		}
	}
	SetLockedRegisteredComponents(false);
}

DString InputBroadcaster::KeyEventToString (sf::Event sfEvent) const
{
	DString keyType = TXT("Unknown");
	switch (sfEvent.type)
	{
		case(sf::Event::EventType::KeyPressed):
			keyType = TXT("Key Pressed");
			break;
		case(sf::Event::EventType::KeyReleased):
			keyType = TXT("Key Released");
			break;

		default:
			return TXT("Not a Key Event");
	}

	return DString::CreateFormattedString(TXT("Type:  %s | Key code:  %s"), keyType, Int(sfEvent.key.code));
}

DString InputBroadcaster::TextEventToString (sf::Event sfEvent) const
{
	if (sfEvent.type != sf::Event::TextEntered)
	{
		return TXT("Not a Text Event");
	}

	return DString(sf::String(sfEvent.text.unicode));
}

DString InputBroadcaster::MouseMoveEventToString (sf::Event::MouseMoveEvent sfEvent) const
{
	return DString::CreateFormattedString(TXT("(%s,%s)"), DString::MakeString(sfEvent.x), DString::MakeString(sfEvent.y));
}

DString InputBroadcaster::MouseButtonEventToString (sf::Event::MouseButtonEvent sfEvent) const
{
	DString buttonName = TXT("Unknown mouse button");
	switch (sfEvent.button)
	{
		case(sf::Mouse::Button::Left):
			buttonName = TXT("LMouse");
			break;
		case(sf::Mouse::Button::Middle):
			buttonName = TXT("MMouse");
			break;
		case(sf::Mouse::Button::Right):
			buttonName = TXT("RMouse");
			break;
		case(sf::Mouse::Button::XButton1):
			buttonName = TXT("FirstExtraMouseButton");
			break;
		case(sf::Mouse::Button::XButton2):
			buttonName = TXT("SecExtraMouseButton");
			break;
	}

	return buttonName;
}

DString InputBroadcaster::MouseWheelEventToString (sf::Event::MouseWheelScrollEvent sfEvent) const
{
	if (sfEvent.delta > 0)
	{
		return DString::CreateFormattedString(TXT("Mouse wheel up %s"), DString::MakeString(sfEvent.delta));
	}
	else
	{
		return DString::CreateFormattedString(TXT("Mouse wheel down %s"), DString::MakeString(sfEvent.delta * -1));
	}
}

void InputBroadcaster::SetInputMessengerFor (InputComponent* target)
{
	target->InputMessenger = this;
}

void InputBroadcaster::ClearInputMessengerFor (InputComponent* target)
{
	target->InputMessenger = nullptr;
}

void InputBroadcaster::HandleWindowEvent (const sf::Event& newEvent)
{
	BroadcastNewEvent(newEvent);
}

void InputBroadcaster::HandleGarbageCollection ()
{
	//Cycle through registered input components to identify if any needs to be removed
	size_t i = 0;
	while (i < RegisteredInputComponents.size())
	{
		if (RegisteredInputComponents.at(i).Component == nullptr || RegisteredInputComponents.at(i).Component->GetPendingDelete())
		{
			RegisteredInputComponents.erase(RegisteredInputComponents.begin() + i);
			continue;
		}

		++i;
	}

	i = 0;
	while (i < PendingRegisterComponents.size())
	{
		if (PendingRegisterComponents.at(i).Component == nullptr || PendingRegisterComponents.at(i).Component->GetPendingDelete())
		{
			PendingRegisterComponents.erase(PendingRegisterComponents.begin() + i);
			continue;
		}

		++i;
	}

	i = 0;
	while (i < PendingUnregisterComponents.size())
	{
		if (!VALID_OBJECT(PendingUnregisterComponents.at(i)))
		{
			PendingUnregisterComponents.erase(PendingUnregisterComponents.begin() + i);
			continue;
		}

		++i;
	}
}
SD_END