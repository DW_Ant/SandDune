/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsInput.cpp
=====================================================================
*/


#include "PlatformWindowsInput.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
void OS_SetMousePosition (const Vector2& mousePosition)
{
	if (!SetCursorPos(mousePosition.X.ToInt().ToInt32(), mousePosition.Y.ToInt().ToInt32()))
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to set the cursor position to %s.  Error code:  %s"), mousePosition, DString::MakeString(GetLastError()));
	}
}

Vector2 OS_GetMousePosition ()
{
	POINT mousePos;
	if (!GetCursorPos(OUT &mousePos))
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to get the cursor position.  Error code:  %s"), DString::MakeString(GetLastError()));
		return Vector2::ZERO_VECTOR;
	}

	return Vector2(Int(static_cast<int>(mousePos.x)).ToFloat(), Int(static_cast<int>(mousePos.y)).ToFloat());
}

void OS_ClampMousePointer (const Rectangle& region)
{
	if (region.IsEmpty())
	{
		ClipCursor(NULL);
		return;
	}

	RECT borders;

	//Note: MS's 'bottom' is the border closer to zero, which is the top part of the rectangle.
	borders.left = static_cast<LONG>(region.GetLesserVBorder().ToInt().Value);
	borders.top = static_cast<LONG>(region.GetLesserHBorder().ToInt().Value);
	borders.right = static_cast<LONG>(region.GetGreaterVBorder().ToInt().Value);
	borders.bottom = static_cast<LONG>(region.GetGreaterHBorder().ToInt().Value);

	ClipCursor(&borders);
}
SD_END

#endif