/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MousePointer.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputComponent.h"
#include "InputEngineComponent.h"
#include "MousePointer.h"

IMPLEMENT_CLASS(SD::MousePointer, SD::Entity)
SD_BEGIN

const Int MousePointer::INPUT_PRIORITY(1000000);
bool MousePointer::bEngineMovingMouse(false);

void MousePointer::InitProps ()
{
	Super::InitProps();

	SetEnableFractionScaling(false);

	bContinuouslyEvalClampPos = true;

	DoubleClickThreshold = 0.35f;
	bSetupInputComp = true;
	ShowMouseValue = 0;
	bLocked = false;
	MouseIcon = nullptr;
	MouseInput = nullptr;
	DefaultIcon = nullptr;
	OwningWindowHandle = nullptr;
	IconOverrideTick = nullptr;
}

void MousePointer::BeginObject ()
{
	Super::BeginObject();

	LoadMouseSettings();

	if (bSetupInputComp)
	{
		MouseInput = InputComponent::CreateObject();
		if (AddComponent(MouseInput))
		{
			MouseInput->OnMouseMove = SDFUNCTION_3PARAM(this, MousePointer, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);

			//This should be the first interface to receive mouse input (so that mouse positions are updated first).
			InputEngineComponent::Find()->GetMainBroadcaster()->SetInputPriority(MouseInput.Get(), INPUT_PRIORITY);
		}
	}

	MouseIcon = SpriteComponent::CreateObject();
	if (!AddComponent(MouseIcon))
	{
		Engine::FindEngine()->FatalError(TXT("Failed to attached SpriteComponent to a mouse pointer object."));
		return;
	}

	MouseIcon->SetPivot(Vector2(0.5f, 0.5f)); //Center sprite over center
	SetSize(GetDefaultMouseSize());
	SetDepth(MAX_FLOAT); //Ensure this mouse is drawn over anything in the Canvas layer

	IconOverrideTick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(IconOverrideTick))
	{
		IconOverrideTick->SetTicking(false);
		IconOverrideTick->SetTickHandler(SDFUNCTION_1PARAM(this, MousePointer, HandleIconOverrideTick, void, Float));
	}
}

void MousePointer::Destroyed ()
{
	SetOwningWindowHandle(nullptr);

	Super::Destroyed();
}

Vector2 MousePointer::GetAbsMousePosition ()
{
	return OS_GetMousePosition();
}

Vector2 MousePointer::GetRelativeMousePosition (Window* windowHandle)
{
	if (windowHandle == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Attempted to retrieve relative mouse position without passing in a window handle.  Absolute position is returned instead."));
		return GetAbsMousePosition();
	}

	Int windowPosX;
	Int windowPosY;
	windowHandle->GetWindowPosition(OUT windowPosX, OUT windowPosY, false);

	Vector2 absMousePos = OS_GetMousePosition();
	return Vector2(absMousePos.X - windowPosX.ToFloat(), absMousePos.Y - windowPosY.ToFloat());
}

Vector2 MousePointer::ToOuterCoordinates () const
{
	//Assume this mouse is already relative to the window handle.
	return GetPosition();
}

Vector2 MousePointer::ToWindowCoordinates () const
{
	//Assume this mouse is already relative to the window handle.
	return GetPosition();
}

void MousePointer::SetAbsMousePosition (const Vector2& newMousePos)
{
	bEngineMovingMouse = true;
	OS_SetMousePosition(newMousePos);
	bEngineMovingMouse = false;
}

void MousePointer::SetRelativeMousePosition (Window* windowHandle, const Vector2& newMousePos)
{
	if (windowHandle == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Attempted to set mouse position relative to a window without passing in the window, itself.  Coordinates will be set in desktop space instead."));
		SetAbsMousePosition(newMousePos);
		return;
	}

	Int windowPosX;
	Int windowPosY;
	windowHandle->GetWindowPosition(OUT windowPosX, OUT windowPosY, false);

	bEngineMovingMouse = true;
	OS_SetMousePosition(Vector2(newMousePos.X + windowPosX.ToFloat(), newMousePos.Y + windowPosY.ToFloat()));
	bEngineMovingMouse = false;
}

void MousePointer::PushPositionLimit (Rectangle newRegionLimit, SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> limitCallback)
{
	if (!limitCallback.IsBounded())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Cannot set a mouse limit region to (%s) without associating a callback for this region.  The role of the callback is notifiy the MousePointer when the region is no longer relevant."), newRegionLimit);
		return;
	}

	SMouseLimit newLimitEntry;
	newLimitEntry.LimitRegion = newRegionLimit;
	newLimitEntry.LimitCallback = limitCallback;
	ClampPositionsStack.push_back(newLimitEntry);

	Rectangle newRegion = newLimitEntry.LimitRegion;
	//Set the curLimit relative to the OS's borders.
	AddWindowOffsetTo(newRegion);

	OS_ClampMousePointer(newRegion);
}

bool MousePointer::RemovePositionLimit (SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback)
{
	for (UINT_TYPE i = 0; i < ClampPositionsStack.size(); i++) //This is the reason why ClampPositionsStack is a vector instead of a stack
	{
		if (targetCallback == ClampPositionsStack.at(i).LimitCallback)
		{
			ClampPositionsStack.erase(ClampPositionsStack.begin() + i);
			if (ClampPositionsStack.size() == 0)
			{
				//Last clamp was removed.  Remove clamped borders.
				OS_ClampMousePointer(Rectangle());
			}
			else if (i >= ClampPositionsStack.size())
			{
				//Last element was removed, reapply new limits
				EvaluateClampedMousePointer();
			}

			return true;
		}
	}

	return false;
}

void MousePointer::PushMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction)
{
	if (!callbackFunction.IsBounded())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Cannot set a mouse pointer icon (%s) without associating a callback for this override.  The role of the callback is notifiy the MousePointer when it can stop using this new icon."), newIcon->GetTextureName());
		return;
	}

	SIconOverride newOverride;
	newOverride.Icon = newIcon;
	newOverride.OverrideCallback = callbackFunction;
	MouseIconOverrideStack.emplace_back(newOverride);
	MouseIcon->SetSpriteTexture(newIcon);

	if (IconOverrideTick != nullptr)
	{
		IconOverrideTick->SetTicking(true);
	}
}

bool MousePointer::UpdateMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction)
{
	for (size_t i = 0; i < MouseIconOverrideStack.size(); ++i)
	{
		if (MouseIconOverrideStack.at(i).OverrideCallback == callbackFunction)
		{
			if (MouseIconOverrideStack.at(i).Icon != newIcon)
			{
				MouseIconOverrideStack.at(i).Icon = newIcon;
				if (i >= MouseIconOverrideStack.size() - 1)
				{
					//This icon is currently being viewed, update the mouse sprite.
					MouseIcon->SetSpriteTexture(newIcon);
				}
			}

			return true;
		}
	}

	return false;
}

bool MousePointer::RemoveIconOverride (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback)
{
	for (size_t i = 0; i < MouseIconOverrideStack.size(); i++)
	{
		if (targetCallback == MouseIconOverrideStack.at(i).OverrideCallback)
		{
			bool bIsLast = (i >= MouseIconOverrideStack.size() - 1);
			MouseIconOverrideStack.erase(MouseIconOverrideStack.begin() + i);
			if (bIsLast)
			{
				//Last element was removed, check for new icon
				EvaluateMouseIconOverrides();
			}

			return true;
		}
	}

	return false;
}

bool MousePointer::IsIconOverrideCurrentlyViewed (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback)
{
	if (ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		return false;
	}

	return (MouseIconOverrideStack.back().OverrideCallback == targetCallback);
}

bool MousePointer::IsIconOverrideRegistered (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback) const
{
	for (const SIconOverride& iconOverride : MouseIconOverrideStack)
	{
		if (iconOverride.OverrideCallback == targetCallback)
		{
			return true;
		}
	}

	return false;
}

void MousePointer::SetDefaultIcon (Texture* newIcon)
{
	DefaultIcon = newIcon;

	if (MouseIcon.IsValid() && MouseIconOverrideStack.size() <= 0)
	{
		MouseIcon->SetSpriteTexture(DefaultIcon.Get());
	}
}

void MousePointer::SetMousePosition (const Vector2& newPos)
{
	if (bLocked)
	{
		return;
	}

	SetPosition(newPos);

	if (bContinuouslyEvalClampPos)
	{
		EvaluateClampedMousePointer();
	}
}

void MousePointer::SetMouseVisibility (bool bVisible)
{
	ShowMouseValue = (bVisible) ? Utils::Max<Int>(ShowMouseValue - 1, 0) : ShowMouseValue + 1;

	if (MouseIcon.IsValid())
	{
		MouseIcon->SetVisibility(GetMouseVisibility());
	}
}

void MousePointer::SetLocked (bool bNewLocked)
{
	bLocked = bNewLocked;
}

void MousePointer::SetOwningWindowHandle (Window* newOwningWindowHandle)
{
	//Restore window's cursor visible state
	if (OwningWindowHandle.IsValid() && OwningWindowHandle->GetResource() != nullptr)
	{
		//Restore OS Mouse Pointer if there are no other instantiated MousePointer classes
		OwningWindowHandle->GetResource()->setMouseCursorVisible(true);

		for (ObjectIterator iter(GetObjectHash()); iter.SelectedObject; iter++)
		{
			MousePointer* mouse = dynamic_cast<MousePointer*>(iter.SelectedObject.Get());
			if (mouse != nullptr && mouse->OwningWindowHandle == OwningWindowHandle && iter.SelectedObject.Get() != this)
			{
				//Although it's not likely, there's another mouse pointer instance that is also drawing a cursor in this window.
				OwningWindowHandle->GetResource()->setMouseCursorVisible(false);
				break;
			}
		}
	}

	OwningWindowHandle = newOwningWindowHandle;
	if (OwningWindowHandle.IsValid() && OwningWindowHandle->GetResource() != nullptr)
	{
		OwningWindowHandle->GetResource()->setMouseCursorVisible(false);
	}
}

void MousePointer::EvaluateMouseIconOverrides ()
{
	sf::Event::MouseMoveEvent newMoveEvent;
	newMoveEvent.x = ReadCachedAbsPosition().X.ToInt().ToInt32();
	newMoveEvent.y = ReadCachedAbsPosition().Y.ToInt().ToInt32();

	while (!ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		const SIconOverride& iconOverride = MouseIconOverrideStack.at(MouseIconOverrideStack.size() - 1);
		if (iconOverride.Icon.IsNullptr() || !iconOverride.OverrideCallback.IsBounded() || !iconOverride.OverrideCallback(this, newMoveEvent))
		{
			MouseIconOverrideStack.pop_back();
			continue;
		}

		//Only update mouse sprite if the texture is different
		if (iconOverride.Icon->GetTextureResource() != MouseIcon->GetSprite()->getTexture())
		{
			MouseIcon->SetSpriteTexture(iconOverride.Icon.Get());
		}

		return;
	}

	//At this point we're assuming the stack is empty
	CHECK(ContainerUtils::IsEmpty(MouseIconOverrideStack));

	if (IconOverrideTick != nullptr)
	{
		IconOverrideTick->SetTicking(false);
	}

	//Restore default texture
	MouseIcon->SetSpriteTexture(DefaultIcon.Get());
}

void MousePointer::EvaluateClampedMousePointer ()
{
	if (ClampPositionsStack.size() > 0)
	{
		sf::Event::MouseMoveEvent newMoveEvent;
		Vector2 absCoordinates = ReadCachedAbsPosition();
		newMoveEvent.x = absCoordinates.X.ToInt().ToInt32();
		newMoveEvent.y = absCoordinates.Y.ToInt().ToInt32();

		SMouseLimit curLimit = ClampPositionsStack.at(ClampPositionsStack.size() - 1);

		if (!curLimit.LimitCallback.IsBounded() || !curLimit.LimitCallback(this, newMoveEvent))
		{
			//Remove the back element and try the next one
			ClampPositionsStack.erase(ClampPositionsStack.begin() + ClampPositionsStack.size() - 1);
			EvaluateClampedMousePointer(); //Recursively check the top of the stack until a callback returns true (or when there's nothing)

			if (ClampPositionsStack.size() <= 0)
			{
				//Remove clamped borders
				OS_ClampMousePointer(Rectangle());
			}
			else
			{
				Rectangle curLimitRect = curLimit.LimitRegion;
				//Set the curLimit relative to the OS's borders.
				AddWindowOffsetTo(curLimitRect);

				OS_ClampMousePointer(curLimitRect);
			}
		}
	}
}

void MousePointer::SetDoubleClickThreshold (Float newDoubleClickThreshold)
{
	DoubleClickThreshold = newDoubleClickThreshold;
}

MousePointer* MousePointer::GetMousePointer ()
{
	InputEngineComponent* inputEngine = InputEngineComponent::Find();
	if (inputEngine != nullptr)
	{
		return inputEngine->GetMouse();
	}

	return nullptr;
}

MousePointer* MousePointer::GetRootMouse ()
{
	return this;
}

Float MousePointer::GetDoubleClickThreshold () const
{
	return DoubleClickThreshold;
}

bool MousePointer::GetMouseVisibility () const
{
	return (ShowMouseValue == 0);
}

bool MousePointer::GetLocked () const
{
	return bLocked;
}

Int MousePointer::GetClampSize () const
{
	return Int(ClampPositionsStack.size());
}

Texture* MousePointer::GetDefaultIcon () const
{
	return DefaultIcon.Get();
}

SpriteComponent* MousePointer::GetMouseIcon () const
{
	return MouseIcon.Get();
}

InputComponent* MousePointer::GetMouseInput () const
{
	return MouseInput.Get();
}

Window* MousePointer::GetOwningWindowHandle () const
{
	return OwningWindowHandle.Get();
}

void MousePointer::LoadMouseSettings ()
{
	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Input.ini")), false))
	{
		DoubleClickThreshold = config->GetProperty<Float>(TXT("Mouse"), TXT("DoubleClickThreshold"));
	}

	config->Destroy();
}

void MousePointer::AddWindowOffsetTo (Rectangle& outRectangle) const
{
	if (OwningWindowHandle.IsNullptr())
	{
		return;
	}

	Int windowPosX;
	Int windowPosY;
	OwningWindowHandle->GetWindowPosition(windowPosX, windowPosY);

	outRectangle.Center += Vector2(windowPosX.ToFloat(), windowPosY.ToFloat());
}

Vector2 MousePointer::GetDefaultMouseSize () const
{
	return Vector2(30.f, 50.f);
}

void MousePointer::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	SetMousePosition(Vector2(Float(sfmlEvent.x), Float(sfmlEvent.y)));
}

void MousePointer::HandleIconOverrideTick (Float deltaSec)
{
	EvaluateMouseIconOverrides();
}
SD_END