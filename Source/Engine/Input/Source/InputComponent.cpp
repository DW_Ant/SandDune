/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputComponent.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputComponent.h"
#include "InputEngineComponent.h"
#include "MousePointer.h"

IMPLEMENT_CLASS(SD::InputComponent, SD::EntityComponent)
SD_BEGIN

void InputComponent::InitProps ()
{
	Super::InitProps();

	InputMessenger = nullptr;
	PendingInputMessenger = nullptr;
	bInputEnabled = true;
	InputPriority = 0;
}

void InputComponent::BeginObject ()
{
	Super::BeginObject();

	InputEngineComponent* localInputEngine = InputEngineComponent::Find();
	if (localInputEngine != nullptr && localInputEngine->GetMainBroadcaster() != nullptr)
	{
		//By default, this component will register to the main render window since most of the input components will be handled there
		localInputEngine->GetMainBroadcaster()->AddInputComponent(this);
	}
}

void InputComponent::Destroyed ()
{
	if (InputMessenger.IsValid())
	{
		InputMessenger->RemoveInputComponent(this);
		InputMessenger = nullptr;
	}

	Super::Destroyed();
}

bool InputComponent::CanReceiveInput () const
{
	return bInputEnabled;
}

void InputComponent::ProcessInput (const sf::Event& keyEvent)
{
	if (OnPassiveInput.IsBounded())
	{
		OnPassiveInput(keyEvent);
	}
}

bool InputComponent::CaptureInput (const sf::Event& keyEvent)
{
	if (ProcessKeybinds(keyEvent))
	{
		return true;
	}

	if (OnInput.IsBounded())
	{
		return OnInput(keyEvent);
	}

	return false;
}

void InputComponent::ProcessText (const sf::Event& keyEvent)
{
	if (OnPassiveText.IsBounded())
	{
		OnPassiveText(keyEvent);
	}
}

bool InputComponent::CaptureText (const sf::Event& keyEvent)
{
	if (OnText.IsBounded())
	{
		return OnText(keyEvent);
	}

	return false;
}

void InputComponent::MouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (OnMouseMove.IsBounded())
	{
		OnMouseMove(mouse, sfmlEvent, deltaMove);
	}
}

void InputComponent::ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (OnPassiveMouseClick.IsBounded())
	{
		OnPassiveMouseClick(mouse, sfmlEvent, eventType);
	}
}

bool InputComponent::CaptureMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (ProcessKeybinds(sfmlEvent, eventType))
	{
		return true;
	}

	if (OnMouseClick.IsBounded())
	{
		return OnMouseClick(mouse, sfmlEvent, eventType);
	}

	return false;
}

void InputComponent::ProcessMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (OnPassiveMouseWheel.IsBounded())
	{
		OnPassiveMouseWheel(mouse, sfmlEvent);
	}
}

bool InputComponent::CaptureMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (OnMouseWheel.IsBounded())
	{
		return OnMouseWheel(mouse, sfmlEvent);
	}

	return false;
}

void InputComponent::SetInputEnabled (bool bNewInputEnabled)
{
	bInputEnabled = bNewInputEnabled;
}

void InputComponent::SetInputPriority (Int newPriority)
{
	InputBroadcaster* broadcaster = (PendingInputMessenger.IsValid()) ? PendingInputMessenger.Get() : InputMessenger.Get();
	broadcaster->SetInputPriority(this, newPriority);
}

void InputComponent::AddKeybind (sf::Keyboard::Key keyboardKey, bool bOnPressedOnly, const SDFunction<bool>& OnExecution)
{
	AddKeybind(keyboardKey, bOnPressedOnly, KS_None, OnExecution);
}

void InputComponent::AddKeybind (sf::Keyboard::Key keyboardKey, bool bOnPressedOnly, EKeyboardState reqStates, const SDFunction<bool>& OnExecution)
{
	//Check if the bind exists already
	for (size_t i = 0; i < Keybinds.size(); ++i)
	{
		if (Keybinds.at(i).OnKeybind == OnExecution)
		{
			//Simply update the keybind
			Keybinds.at(i).ButtonType = SKeybind::BT_Keyboard;
			Keybinds.at(i).KeyboardKey = keyboardKey;
			Keybinds.at(i).bOnPressedOnly = bOnPressedOnly;
			return;
		}
	}

	SKeybind& newKeybind = Keybinds.emplace_back();
	newKeybind.ButtonType = SKeybind::BT_Keyboard;
	newKeybind.KeyboardKey = keyboardKey;
	newKeybind.bOnPressedOnly = bOnPressedOnly;
	newKeybind.RequiredStates = reqStates;
	newKeybind.OnKeybind = OnExecution;
}

void InputComponent::AddKeybind (sf::Mouse::Button mouseButton, bool bOnPressedOnly, const SDFunction<bool>& OnExecution)
{
	//Check if the bind exists already
	for (size_t i = 0; i < Keybinds.size(); ++i)
	{
		if (Keybinds.at(i).OnKeybind == OnExecution)
		{
			//Simply update the keybind
			Keybinds.at(i).ButtonType = SKeybind::BT_Mouse;
			Keybinds.at(i).MouseButton = mouseButton;
			Keybinds.at(i).bOnPressedOnly = bOnPressedOnly;
			return;
		}
	}

	SKeybind newKeybind;
	newKeybind.ButtonType = SKeybind::BT_Mouse;
	newKeybind.MouseButton = mouseButton;
	newKeybind.bOnPressedOnly = bOnPressedOnly;
	newKeybind.OnKeybind = OnExecution;
	Keybinds.emplace_back(newKeybind);
}

void InputComponent::RemoveKeybind (const SDFunction<bool>& OnExecution)
{
	for (size_t i = 0; i < Keybinds.size(); ++i)
	{
		if (Keybinds.at(i).OnKeybind == OnExecution)
		{
			Keybinds.erase(Keybinds.begin() + i);
			return;
		}
	}

	InputLog.Log(LogCategory::LL_Warning, TXT("Unable to remove keybind %s since it is not registered to %s."), OnExecution, ToString());
}

InputBroadcaster* InputComponent::GetInputMessenger () const
{
	return InputMessenger.Get();
}

Int InputComponent::GetInputPriority () const
{
	return InputPriority;
}

bool InputComponent::ProcessKeybinds (const sf::Event& keyboardEvent) const
{
	CHECK(keyboardEvent.type == sf::Event::KeyPressed || keyboardEvent.type == sf::Event::KeyReleased)

	for (const SKeybind& keybind : Keybinds)
	{
		if (keybind.ButtonType == SKeybind::BT_Keyboard && keyboardEvent.key.code == keybind.KeyboardKey && keybind.OnKeybind.IsBounded())
		{
			EKeyboardState reqStates = keybind.RequiredStates;

			//'Deduct' reqStates with the keyboard's global state
			reqStates &= ~InputBroadcaster::GetKeyboardState();

			//Check if the keyboard state requirements are met
			if (reqStates != KS_None)
			{
				//At least one of the button state requirements are not met.
				continue;
			}

			if ((keybind.bOnPressedOnly && keyboardEvent.type == sf::Event::KeyPressed) ||
				(!keybind.bOnPressedOnly && keyboardEvent.type == sf::Event::KeyReleased))
			{
				//Execute the keybind!
				if (keybind.OnKeybind.Execute())
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool InputComponent::ProcessKeybinds (const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType eventType) const
{
	CHECK(eventType == sf::Event::MouseButtonPressed || eventType == sf::Event::MouseButtonReleased)

	for (const SKeybind& keybind : Keybinds)
	{
		if (keybind.ButtonType == SKeybind::BT_Mouse && mouseEvent.button == keybind.MouseButton && keybind.OnKeybind.IsBounded())
		{
			if ((keybind.bOnPressedOnly && eventType == sf::Event::MouseButtonPressed) ||
				(!keybind.bOnPressedOnly && eventType == sf::Event::MouseButtonReleased))
			{
				//Execute the keybind!
				if (keybind.OnKeybind.Execute())
				{
					return true;
				}
			}
		}
	}

	return false;
}
SD_END