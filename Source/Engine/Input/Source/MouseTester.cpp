/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MouseTester.cpp
=====================================================================
*/

#include "InputComponent.h"
#include "InputEngineComponent.h"
#include "MousePointer.h"
#include "MouseTester.h"
#include "MouseTesterRender.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::MouseTester, SD::Entity)
SD_BEGIN

DPointer<Texture> MouseTester::Holding1Tx = nullptr;
DPointer<Texture> MouseTester::Holding2Tx = nullptr;
DPointer<Texture> MouseTester::Holding3Tx = nullptr;
DPointer<Texture> MouseTester::Holding4Tx = nullptr;

void MouseTester::InitProps ()
{
	Super::InitProps();

	bActive = true;
	bSettingLimitRegion = false;
	Input = nullptr;
	RegionRenderer = nullptr;

	bHolding1 = false;
	bHolding2 = false;
	bHolding3 = false;
	bHolding4 = false;
	bClampingMouse = false;
}

void MouseTester::BeginObject ()
{
	Super::BeginObject();

	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->OnInput = SDFUNCTION_1PARAM(this, MouseTester, HandleCaptureInput, bool, const sf::Event&);
		Input->OnMouseMove = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		Input->OnMouseClick = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	RegionRenderer = MouseTesterRender::CreateObject();
	if (AddComponent(RegionRenderer))
	{
		if (!IsDefaultObject())
		{
			GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
			CHECK(graphicsEngine != nullptr)
			PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
			CHECK(canvasLayer != nullptr)
			canvasLayer->RegisterPlanarObject(this);
			SetDepth(25.f);
		}
	}
}

void MouseTester::PostEngineInitialize () const
{
	Super::PostEngineInitialize();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (localTexturePool != nullptr)
	{
		Holding1Tx = localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPointer1.txtr")));
		Holding2Tx = localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPointer2.txtr")));
		Holding3Tx = localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPointer3.txtr")));
		Holding4Tx = localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPointer4.txtr")));
	}
}

void MouseTester::Destroyed ()
{
	MousePointer* pointer = InputEngineComponent::Find()->GetMouse();
	if (pointer != nullptr)
	{
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon1, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon2, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon3, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon4, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemovePositionLimit(SDFUNCTION_2PARAM(this, MouseTester, HandlePushLimit, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	Super::Destroyed();
}

#define HOLD_NUM_SWITCH(num) \
if (bHolding##num && keyEvent.type == sf::Event::KeyReleased) \
{ \
	bHolding##num = false; \
	mouse->EvaluateMouseIconOverrides(); \
} \
else if (!bHolding##num && keyEvent.type == sf::Event::KeyPressed) \
{ \
	bHolding##num = true; \
	mouse->PushMouseIconOverride(Holding##num##Tx.Get(), SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon##num##, bool, MousePointer*, const sf::Event::MouseMoveEvent&)); \
} \
else {}

bool MouseTester::HandleCaptureInput (const sf::Event& keyEvent)
{
	if (!bActive)
	{
		return false;
	}

	MousePointer* mouse = InputEngineComponent::Find()->GetMouse();
	CHECK(mouse != nullptr)

	switch (keyEvent.key.code)
	{
		case(sf::Keyboard::Num1):
			HOLD_NUM_SWITCH(1);
			return true;

		case(sf::Keyboard::Num2):
			HOLD_NUM_SWITCH(2);
			return true;

		case(sf::Keyboard::Num3):
			HOLD_NUM_SWITCH(3);
			return true;

		case(sf::Keyboard::Num4):
			HOLD_NUM_SWITCH(4);
			return true;

		case(sf::Keyboard::Escape):
			if (keyEvent.type == sf::Event::KeyReleased)
			{
				UnitTester::TestLog(TestFlags, TXT("Escape released.  Ending Mouse Tester."));
				Destroy();
			}
			return true;
	}

	return false;
}
#undef HOLD_NUM_SWITCH

void MouseTester::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (bActive && bSettingLimitRegion && RegionRenderer.IsValid())
	{
		RegionRenderer->FlexiblePoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
	}
}

bool MouseTester::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (!bActive)
	{
		return false;
	}

	if (sfmlEvent.button == sf::Mouse::Button::Left)
	{
		bSettingLimitRegion = (eventType == sf::Event::MouseButtonPressed);
		if (RegionRenderer.IsValid() && bSettingLimitRegion)
		{
			RegionRenderer->PivotPoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
			RegionRenderer->FlexiblePoint = RegionRenderer->PivotPoint;
		}

		if (eventType == sf::Event::MouseButtonReleased) //Apply limit
		{
			Rectangle limitRegion;
			limitRegion.Width = Float::Abs(RegionRenderer->FlexiblePoint.X - RegionRenderer->PivotPoint.X);
			limitRegion.Height = Float::Abs(RegionRenderer->FlexiblePoint.Y - RegionRenderer->PivotPoint.Y);

			//It's uncertain which points are the left/right, up/down. Gotta use utils to take the lesser of the two to figure out left/top.
			limitRegion.Center.X = Utils::Min(RegionRenderer->PivotPoint.X, RegionRenderer->FlexiblePoint.X);
			limitRegion.Center.Y = Utils::Min(RegionRenderer->PivotPoint.Y, RegionRenderer->FlexiblePoint.Y);
			limitRegion.Center.X += (limitRegion.Width * 0.5f);
			limitRegion.Center.Y += (limitRegion.Height * 0.5f);

			InputEngineComponent::Find()->GetMouse()->PushPositionLimit(limitRegion, SDFUNCTION_2PARAM(this, MouseTester, HandlePushLimit, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			bClampingMouse = true;
		}

		return true;
	}

	if (sfmlEvent.button == sf::Mouse::Button::Right)
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			bClampingMouse = false;
			InputEngineComponent::Find()->GetMouse()->EvaluateClampedMousePointer();
			if (RegionRenderer.IsValid())
			{
				//Stop rendering rectangle
				RegionRenderer->PivotPoint = Vector2::ZERO_VECTOR;
				RegionRenderer->FlexiblePoint = Vector2::ZERO_VECTOR;
			}
		}

		return true;
	}

	return false;
}

bool MouseTester::HandleMouseIcon1 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bHolding1;
}

bool MouseTester::HandleMouseIcon2 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bHolding2;
}

bool MouseTester::HandleMouseIcon3 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bHolding3;
}

bool MouseTester::HandleMouseIcon4 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bHolding4;
}

bool MouseTester::HandlePushLimit (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bClampingMouse;
}
SD_END

#endif