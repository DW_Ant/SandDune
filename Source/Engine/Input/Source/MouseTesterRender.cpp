/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MouseTesterRender.cpp
=====================================================================
*/

#include "MouseTesterRender.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::MouseTesterRender, SD::RenderComponent)
SD_BEGIN

void MouseTesterRender::InitProps ()
{
	Super::InitProps();

	BorderThickness = 4.f;
}

void MouseTesterRender::Render (RenderTarget* renderTarget, const Camera* camera)
{
	CHECK(renderTarget != nullptr)
	if (PivotPoint.IsEmpty() && FlexiblePoint.IsEmpty())
	{
		return;
	}

	Vector2 topLeft;
	topLeft.X = Utils::Min(PivotPoint.X, FlexiblePoint.X);
	topLeft.Y = Utils::Min(PivotPoint.Y, FlexiblePoint.Y);

	Vector2 bottomRight;
	bottomRight.X = Utils::Max(PivotPoint.X, FlexiblePoint.X);
	bottomRight.Y = Utils::Max(PivotPoint.Y, FlexiblePoint.Y);

	sf::RectangleShape outline;
	outline.setSize(Vector2::SDtoSFML(Vector2(bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y)));
	outline.setPosition(Vector2::SDtoSFML(topLeft));
	outline.setOutlineThickness(BorderThickness);
	outline.setOutlineColor(sf::Color::Red);
	outline.setFillColor(sf::Color::Transparent);
	renderTarget->Draw(this, camera, outline);
}

Aabb MouseTesterRender::GetAabb () const
{
	std::vector<Vector3> points;
	points.push_back(PivotPoint.ToVector3());
	points.push_back(FlexiblePoint.ToVector3());

	return Aabb::GenerateEncompassingAabb(points);
}
SD_END

#endif