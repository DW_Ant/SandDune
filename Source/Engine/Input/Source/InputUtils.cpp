/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputUtils.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputUtils.h"
#include "MousePointer.h"

IMPLEMENT_ABSTRACT_CLASS(SD::InputUtils, SD::BaseUtils)
SD_BEGIN

void InputUtils::SetupInputForWindow (Window* window, InputBroadcaster* inputBroadcaster, MousePointer* mouse)
{
	CHECK_INFO(window != nullptr && inputBroadcaster != nullptr && mouse != nullptr, TXT("Cannot setup input for a window.  Either the window, input broadcaster, or the mouse was not specified."))

	inputBroadcaster->SetInputSource(window);

	inputBroadcaster->AddInputComponent(mouse->GetMouseInput(), MousePointer::INPUT_PRIORITY);
	inputBroadcaster->SetAssociatedMouse(mouse);

	//Default icon should match the main mouse pointer's icon.
	Texture* defaultIcon = MousePointer::GetMousePointer()->GetDefaultIcon();
	mouse->SetDefaultIcon(defaultIcon);

	mouse->SetOwningWindowHandle(window);
}

#ifdef WITH_LOCALIZATION
DString InputUtils::TranslateToHumanReadableText (sf::Keyboard::Key keyboard)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	if (translator == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to translate keyboard key to human readable text since there isn't a text translator instance. Check if your application contains a LocalizationEngineComponent instance in this thread."));
#ifdef DEBUG_MODE
		return TXT("<Missing Input Translator>");
#else
		return DString::EmptyString;
#endif
	}

	//This mapping must exactly match sf::Keyboard::Key enum in the correct order.
	static const std::vector<DString> keyMapping(
	{
		TXT("A"),
		TXT("B"),
		TXT("C"),
		TXT("D"),
		TXT("E"),
		TXT("F"),
		TXT("G"),
		TXT("H"),
		TXT("I"),
		TXT("J"),
		TXT("K"),
		TXT("L"),
		TXT("M"),
		TXT("N"),
		TXT("N"),
		TXT("P"),
		TXT("Q"),
		TXT("R"),
		TXT("S"),
		TXT("T"),
		TXT("U"),
		TXT("V"),
		TXT("W"),
		TXT("X"),
		TXT("Y"),
		TXT("Z"),
		TXT("NUM0"),
		TXT("NUM1"),
		TXT("NUM2"),
		TXT("NUM3"),
		TXT("NUM4"),
		TXT("NUM5"),
		TXT("NUM6"),
		TXT("NUM7"),
		TXT("NUM8"),
		TXT("NUM9"),
		TXT("Escape"),
		TXT("LCtrl"),
		TXT("LShift"),
		TXT("LAlt"),
		TXT("LSystem"),
		TXT("RCtrl"),
		TXT("RShift"),
		TXT("RAlt"),
		TXT("RSystem"),
		TXT("Menu"),
		TXT("LBracket"),
		TXT("RBracket"),
		TXT("Semicolon"),
		TXT("Comma"),
		TXT("Period"),
		TXT("Quote"),
		TXT("Slash"),
		TXT("Backslash"),
		TXT("Tilde"),
		TXT("Equal"),
		TXT("Hyphen"),
		TXT("Space"),
		TXT("Enter"),
		TXT("Backspace"),
		TXT("Tab"),
		TXT("PageUp"),
		TXT("PageDown"),
		TXT("End"),
		TXT("Home"),
		TXT("Insert"),
		TXT("Delete"),
		TXT("Add"),
		TXT("Subtract"),
		TXT("Multiply"),
		TXT("Divide"),
		TXT("Left"),
		TXT("Right"),
		TXT("Up"),
		TXT("Down"),
		TXT("Numpad0"),
		TXT("Numpad1"),
		TXT("Numpad2"),
		TXT("Numpad3"),
		TXT("Numpad4"),
		TXT("Numpad5"),
		TXT("Numpad6"),
		TXT("Numpad7"),
		TXT("Numpad8"),
		TXT("Numpad9"),
		TXT("F1"),
		TXT("F2"),
		TXT("F3"),
		TXT("F4"),
		TXT("F5"),
		TXT("F6"),
		TXT("F7"),
		TXT("F8"),
		TXT("F9"),
		TXT("F10"),
		TXT("F11"),
		TXT("F12"),
		TXT("F13"),
		TXT("F14"),
		TXT("F15"),
		TXT("Pause")
	});

	CHECK(keyMapping.size() == sf::Keyboard::KeyCount) //Ensure this vector maps to the keyboard enum 1-to-1.

	Int keyIdx = static_cast<int>(keyboard);
	if (keyIdx >= keyMapping.size())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to translate keyboard key to human readable text since the key idx %s is out of range from the key mapping of size %s."), keyIdx, Int(keyMapping.size()));
		keyIdx = -1; //Default to unknown key
	}

	if (keyIdx < 0)
	{
		//Display unknown key message
		return translator->TranslateText(TXT("UnknownKey"), TXT("Input"), TXT("Keyboard"));
	}

	return translator->TranslateText(keyMapping.at(keyIdx.ToUnsignedInt()), TXT("Input"), TXT("Keyboard"));
}

DString InputUtils::TranslateToHumanReadableText (sf::Mouse::Button mouseButton)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	if (translator == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to translate mouse button to human readable text since there isn't a text translator instance. Check if your application contains a LocalizationEngineComponent instance in this thread."));
#ifdef DEBUG_MODE
		return TXT("<Missing Input Translator>");
#else
		return DString::EmptyString;
#endif
	}

	static const std::vector<DString> mouseMapping(
	{
		TXT("Left"),
		TXT("Right"),
		TXT("Middle"),
		TXT("XButton1"),
		TXT("XButton2")
	});
	
	Int mouseIdx = static_cast<int>(mouseButton);
	if (mouseIdx >= mouseMapping.size())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to translate mouse button to human readable text since the mouse button idx %s is out of range from the mapping of size %s."), mouseIdx, Int(mouseMapping.size()));
		mouseIdx = -1;
	}

	if (mouseIdx < 0)
	{
		return translator->TranslateText(TXT("UnknownKey"), TXT("Input"), TXT("Mouse"));
	}

	return translator->TranslateText(mouseMapping.at(mouseIdx.ToUnsignedInt()), TXT("Input"), TXT("Mouse"));
}
#endif
SD_END