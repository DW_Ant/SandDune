/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputUnitTester.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputEngineComponent.h"
#include "InputTester.h"
#include "InputUnitTester.h"
#include "MousePointer.h"
#include "MouseTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::InputUnitTester, SD::UnitTester)
SD_BEGIN

bool InputUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;
	if ((testFlags & UTF_Manual) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		bResult = TestMousePointer(testFlags);
	}

	if (bResult && (testFlags & UTF_Manual) > 0 && (testFlags & UTF_NeverFails) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		bResult = (TestInputBroadcaster(testFlags) && TestInputComponent(testFlags));
	}

	return bResult;
}

bool InputUnitTester::TestInputBroadcaster (EUnitTestFlags testFlags) const
{
	InputEngineComponent* inputEngine = InputEngineComponent::Find();
	if (inputEngine == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to test input broadcaster since the tester cannot find the local Input Engine Component."));
		return false;
	}

	InputBroadcaster* mainBroadcaster = inputEngine->GetMainBroadcaster();
	if (mainBroadcaster == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to test input broadcaster since the local input engine component does not have a broadcaster associated with it."));
		return false;
	}

	mainBroadcaster->bDebugKeyEvent = true;
	mainBroadcaster->bDebugTextEvent = true;
	mainBroadcaster->bDebugMouseMove = true;
	mainBroadcaster->bDebugMouseButton = true;
	mainBroadcaster->bDebugMouseWheel = true;
	return true;
}

bool InputUnitTester::TestInputComponent (EUnitTestFlags testFlags) const
{
	TestLog(testFlags, TXT("Begin Input Component unit test. . ."));
	InputTester* inputTester = InputTester::CreateObject(); //This object will destroy the TestBroadcaster and itself when the user presses the Esc character.
	inputTester->TestFlags = testFlags;
	TestLog(testFlags, TXT("This test does not automatically terminate.  Press the Escape character to end test."));

	return true;
}

bool InputUnitTester::TestMousePointer (EUnitTestFlags testFlags) const
{
	//MouseTester will destroy itself when escape is pressed
	TestLog(testFlags, TXT("Begin Mouse Test. . ."));

	SetTestCategory(testFlags, TXT("OS Mouse Coordinates"));
	{
		//Since the mouse pointer is a shared resource with OS, it's possible (although very unlikely)
		//for the mouse position to change between OS mouse function calls.
		const Int maxMouseAttempts = 50;
		bool hasPassedTest = false;

		//Various counters to identify which test(s) has failed
		Int relativeCoordTestsFailed = 0;
		Int absPosTestFailed = 0;
		Int newRelPosTestFailed = 0;
		for (Int i = 0; i < maxMouseAttempts; ++i)
		{
			Vector2 originalMousePos = MousePointer::GetAbsMousePosition();
			Window* mainWindow = GraphicsEngineComponent::Find()->GetPrimaryWindow();
			CHECK(mainWindow != nullptr)

			Int windowPosX;
			Int windowPosY;
			mainWindow->GetWindowPosition(OUT windowPosX, OUT windowPosY);
			Vector2 winPos(windowPosX.ToFloat(), windowPosY.ToFloat());

			Vector2 relativeMouseCoord = MousePointer::GetRelativeMousePosition(mainWindow);
			if (originalMousePos - winPos != relativeMouseCoord)
			{
				relativeCoordTestsFailed++;
				continue;
			}

			Vector2 newAbsPosition(64.f, 96.f);
			MousePointer::SetAbsMousePosition(newAbsPosition);
			Vector2 absPos = MousePointer::GetAbsMousePosition();
			if (newAbsPosition != absPos)
			{
				absPosTestFailed++;
				continue;
			}

			Vector2 newRelPos(32.f, 48.f);
			MousePointer::SetRelativeMousePosition(mainWindow, newRelPos);
			Vector2 relPos = MousePointer::GetRelativeMousePosition(mainWindow);
			if (newRelPos != relPos)
			{
				newRelPosTestFailed++;
				continue;
			}

			//Restore the mouse position to original position.
			MousePointer::SetAbsMousePosition(originalMousePos);
			hasPassedTest = true;
			break;
		}

		if (!hasPassedTest)
		{
			DString specificTestMsgs = DString::EmptyString;
			if (relativeCoordTestsFailed > 0)
			{
				specificTestMsgs += DString::CreateFormattedString(TXT("MousePointer::GetRelativeMousePosition does not retrieve a mouse position that's relative to the specified window handle.\nThis test has failed %s time(s).\n"), relativeCoordTestsFailed);
			}

			if (absPosTestFailed > 0)
			{
				specificTestMsgs += DString::CreateFormattedString(TXT("After setting the mouse position, the MousePointer::GetAbsMousePosition failed to retrieve the same mouse absolute position it was set to.\nThis test has failed %s time(s).\n"), absPosTestFailed);
			}

			if (newRelPosTestFailed > 0)
			{
				specificTestMsgs += DString::CreateFormattedString(TXT("After setting the mouse position, the MousePointer::GetRelativeMousePosition failed to retrieve the same mouse relative position it was set to.\nThis test has failed %s time(s).\n"), newRelPosTestFailed);
			}

			UnitTestError(testFlags, TXT("MousePointer test failed.  Since the mouse is a shared resource with the OS, the test was conducted multiple times.  However, the unit test has failed %s consecutive times.\n%s"), maxMouseAttempts, specificTestMsgs);
			return false;
		}
	}
	MouseTester* mouseTester = MouseTester::CreateObject();
	mouseTester->TestFlags = testFlags;
	mouseTester->bActive = false; //Disabled for now.  The InputTester will activate this tester when it's destroyed.
	TestLog(testFlags, TXT("This test does not automatically terminate.  Press the Escape character once to begin the test, and again to end it."));

	return true;
}
SD_END

#endif