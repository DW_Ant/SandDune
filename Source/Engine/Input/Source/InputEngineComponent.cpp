/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputEngineComponent.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputEngineComponent.h"
#include "MousePointer.h"

IMPLEMENT_ENGINE_COMPONENT(SD::InputEngineComponent)
SD_BEGIN

InputEngineComponent::InputEngineComponent () : Super()
{
	MainBroadcaster = nullptr;
	Mouse = nullptr;
}

void InputEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	MainBroadcaster = InputBroadcaster::CreateObject();
	CHECK(MainBroadcaster.IsValid())

	Mouse = MousePointer::CreateObject();
	CHECK(Mouse.IsValid())

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
	CHECK(canvasLayer != nullptr)
	canvasLayer->RegisterPlanarObject(Mouse.Get());
}

void InputEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	ImportMouseIcons(localTexturePool);

	HashedString defaultIconName("Engine.Input.CursorPointer");
	Texture* defaultIcon = localTexturePool->EditTexture(defaultIconName);
	if (defaultIcon == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("Unable to import essential texture for InputEngineComponent: ") + defaultIconName.ReadString());
		return;
	}

	Mouse->SetDefaultIcon(defaultIcon);
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	if (localGraphicsEngine != nullptr)
	{
		Mouse->SetOwningWindowHandle(localGraphicsEngine->GetPrimaryWindow());
		MainBroadcaster->SetInputSource(localGraphicsEngine->GetPrimaryWindow());
	}
	else
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Local engine does not have a GraphicsEngineComponent.  Local InputEngineComponent will not be able to register mouse pointer to a window handle."));
	}

	MainBroadcaster->SetAssociatedMouse(Mouse.Get());
}

void InputEngineComponent::ShutdownComponent ()
{
	Super::ShutdownComponent();

	if (MainBroadcaster.IsValid())
	{
		MainBroadcaster->Destroy();
		MainBroadcaster = nullptr;
	}

	if (Mouse.IsValid())
	{
		Mouse->Destroy();
		Mouse = nullptr;
	}

	//Remove clamped mouse position limits
	OS_ClampMousePointer(Rectangle());
}

void InputEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	//GraphicsEngine must initialize first since the mouse pointer needs to register to canvas draw layer.
	outDependencies.push_back(GraphicsEngineComponent::SStaticClass());
}

InputBroadcaster* InputEngineComponent::FindBroadcasterForWindow (Window* window) const
{
	for (InputBroadcaster* curBroadcaster : InputBroadcasters)
	{
		if (curBroadcaster->GetInputSource() == window)
		{
			return curBroadcaster;
		}
	}

	return nullptr;
}

void InputEngineComponent::RegisterInputBroadcaster (InputBroadcaster* newBroadcaster)
{
	InputBroadcasters.push_back(newBroadcaster);
}

void InputEngineComponent::UnregisterInputBroadcaster (InputBroadcaster* oldBroadcaster)
{
	for (UINT_TYPE i = 0; i < InputBroadcasters.size(); i++)
	{
		if (InputBroadcasters.at(i) == oldBroadcaster)
		{
			InputBroadcasters.erase(InputBroadcasters.begin() + i);
			return;
		}
	}
}

InputBroadcaster* InputEngineComponent::GetMainBroadcaster () const
{
	return MainBroadcaster.Get();
}

MousePointer* InputEngineComponent::GetMouse () const
{
	return Mouse.Get();
}

void InputEngineComponent::ImportMouseIcons (TexturePool* localTexturePool)
{
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorGrab.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorOpenHand.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPointer.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorPrecision.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorResizeBottomLeft.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorResizeHorizontal.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorResizeTopLeft.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorResizeVertical.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorScrollbarPan.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Input"), TXT("CursorText.txtr")));
}
SD_END