/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputTester.cpp
=====================================================================
*/

#include "InputBroadcaster.h"
#include "InputComponent.h"
#include "InputEngineComponent.h"
#include "InputTester.h"
#include "MouseTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::InputTester, SD::Entity)
SD_BEGIN

void InputTester::InitProps ()
{
	Super::InitProps();

	TestComponent = nullptr;
}

void InputTester::BeginObject ()
{
	Super::BeginObject();

	TestComponent = InputComponent::CreateObject();
	if (TestComponent.IsValid())
	{
		TestComponent->OnInput = SDFUNCTION_1PARAM(this, InputTester, HandleCaptureInput, bool, const sf::Event&);
		TestComponent->OnText = SDFUNCTION_1PARAM(this, InputTester, HandleCaptureText, bool, const sf::Event&);
		TestComponent->OnMouseMove = SDFUNCTION_3PARAM(this, InputTester, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		TestComponent->OnMouseClick = SDFUNCTION_3PARAM(this, InputTester, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		TestComponent->OnMouseWheel = SDFUNCTION_2PARAM(this, InputTester, HandleMouseWheelScroll, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
		AddComponent(TestComponent);

		InputEngineComponent* inputEngine = InputEngineComponent::Find();
		CHECK(inputEngine != nullptr)
		InputBroadcaster* mainBroadcaster = inputEngine->GetMainBroadcaster();
		CHECK(mainBroadcaster != nullptr)
		mainBroadcaster->AddInputComponent(TestComponent.Get());
	}
}

void InputTester::Destroyed ()
{
	//Reset debugging flags
	if (InputEngineComponent* inputEngine = InputEngineComponent::Find())
	{
		if (InputBroadcaster* mainBroadcaster = inputEngine->GetMainBroadcaster())
		{
			mainBroadcaster->bDebugKeyEvent = false;
			mainBroadcaster->bDebugMouseButton = false;
			mainBroadcaster->bDebugMouseMove = false;
			mainBroadcaster->bDebugMouseWheel = false;
			mainBroadcaster->bDebugTextEvent = false;
		}
	}

	//Activate MouseTester
	for (ObjectIterator iter; iter.SelectedObject.IsValid(); iter++)
	{
		if (dynamic_cast<MouseTester*>(iter.SelectedObject.Get()) != nullptr)
		{
			UnitTester::TestLog(TestFlags, TXT("Input Tester concluded.  Now launching MouseTester.  Press the escape key to terminate."));
			dynamic_cast<MouseTester*>(iter.SelectedObject.Get())->bActive = true;
			break;
		}
	}

	Super::Destroyed();
}

bool InputTester::HandleCaptureInput (const sf::Event& keyEvent)
{
	if (keyEvent.type == sf::Event::KeyPressed)
	{
		UnitTester::TestLog(TestFlags, TXT("Key event pressed."));
	}
	else if (keyEvent.type == sf::Event::KeyReleased)
	{
		UnitTester::TestLog(TestFlags, TXT("Key event released.  Press Escape to end input test."));
	}

	if (keyEvent.type == sf::Event::KeyReleased && keyEvent.key.code == sf::Keyboard::Escape)
	{
		UnitTester::TestLog(TestFlags, TXT("Escape key released.  Ending InputComponent tester."));
		Destroy();
		return true;
	}

	return false;
}

bool InputTester::HandleCaptureText (const sf::Event& keyEvent)
{
	UnitTester::TestLog(TestFlags, TXT("\"%s\" received."), DString(sf::String(keyEvent.text.unicode)));

	return false;
}

void InputTester::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	UnitTester::TestLog(TestFlags, TXT("Detected mouse movement.  Mouse coordinates (%s, %s).   Delta move (%s)"), Int(sfmlEvent.x), Int(sfmlEvent.y), deltaMove);
}

bool InputTester::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	DString buttonName = TXT("Unknown button");
	switch (sfmlEvent.button)
	{
		case(sf::Mouse::Left):
			buttonName = TXT("LMouse");
			break;

		case(sf::Mouse::Right):
			buttonName = TXT("RMouse");
			break;

		case(sf::Mouse::Middle):
			buttonName = TXT("Middle Mouse");
			break;

		case(sf::Mouse::XButton1):
			buttonName = TXT("Extra Button 1");
			break;

		case(sf::Mouse::XButton2):
			buttonName = TXT("Extra Button 2");
			break;
	}

	if (eventType == sf::Event::MouseButtonPressed)
	{
		UnitTester::TestLog(TestFlags, TXT("Mouse button pressed:  %s"), buttonName);
	}
	else if (eventType == sf::Event::MouseButtonReleased)
	{
		UnitTester::TestLog(TestFlags, TXT("Mouse button released:  %s"), buttonName);
	}

	return false;
}

bool InputTester::HandleMouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	UnitTester::TestLog(TestFlags, TXT("Mouse wheel scrolled:  %s"), Float(sfmlEvent.delta));
	return false;
}
SD_END

#endif