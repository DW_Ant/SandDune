/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputBroadcaster.h
  Binds delegates to a Window handle to receive input events.  Then
  it'll broadcast those events to registered InputComponents until the event
  is consumed, or if it reached to the end of the stack.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
class InputComponent;
class MousePointer;

class INPUT_API InputBroadcaster : public Object
{
	DECLARE_CLASS(InputBroadcaster)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SInputPriorityMapping
	{
		InputComponent* Component;

		/* Relatively speaking, the higher the Priority, the earlier this input component will receive events. */
		Int Priority;

		SInputPriorityMapping (InputComponent* inComponent, Int inPriority)
		{
			Component = inComponent;
			Priority = inPriority;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
#ifdef DEBUG_MODE
	/* Various debugging flags about a particular input event. */
	bool bDebugKeyEvent;
	bool bDebugTextEvent;
	bool bDebugMouseMove;
	bool bDebugMouseButton;
	bool bDebugMouseWheel;
#endif

protected:
	/* Various input state changes.  Any Broadcaster from any thread could set these flags. */
	static EKeyboardState KeyboardState;

	/* A list of registered InputComponents that may receive key events. */
	std::vector<SInputPriorityMapping> RegisteredInputComponents;

	/* Window handle this broadcaster is receiving events from. */
	DPointer<Window> InputSource;

	/* Previous mouse pointer position used to calculate the delta mouse move. */
	Vector2 OldMousePosition;

	DPointer<MousePointer> AssociatedMouse;

	/* If true, the the InputBroadcaster is in the middle of reading from RegisteredInputComponents (within a loop).
	If this is true, then new input components cannot be registered, and old components cannot be removed until this is turns false. */
	bool bLockedRegisteredComponents;

	/* List of components waiting to be registered when RegisteredInputComponents vector is available. */
	std::vector<SInputPriorityMapping> PendingRegisterComponents;

	/* List of components waiting to be unregistered when RegisteredInputComponents vector is available.
	It's imperative not to use DPointer in this array since InputComponents will unregister themselves from the broadcasters.
	However, broadcasters may be locked if the InputComponents destroyed themselves within an Input event.  With DPointers, if an Input
	Component unregistered itself while it's getting destroyed, the DPointer would remove the PendingUnregisterComponents' reference. */
	std::vector<InputComponent*> PendingUnregisterComponents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Searches and returns the InputBroadcaster that is pulling events from the given window handle.
	 */
	static InputBroadcaster* FindBroadcaster (Window* associatedWindow);

	/**
	 * Binds event callbacks to the given window.
	 */
	virtual void SetInputSource (Window* targetWindow);

	/**
	 * Adds an InputComponent to the RegisteredInputs array.
	 * The higher the priority, the earlier the InputComponent will appear in the RegisteredInputsArray (gets key events before other components).
	 */
	virtual void AddInputComponent (InputComponent* newComponent, Int priority = 0);

	/**
	 * Reorders the input priority for specified input component.
	 * Returns true if the targetComponent was found.
	 */
	virtual bool SetInputPriority (InputComponent* targetComponent, Int newPriority);

	/**
	 * Removes an InputComponent from the RegisteredInputs array.
	 * Returns true if the target was found and removed.
	 */
	virtual bool RemoveInputComponent (InputComponent* target);

	virtual void SetAssociatedMouse (MousePointer* newAssociatedMouse);

#ifdef DEBUG_MODE
	/**
	 * Logs out the list of registered InputComponents and their priorities.
	 */
	virtual void LogRegisteredInputComponents () const;
#endif

	/**
	 * Reads in the sf::Event, and if the event is input-related, it'll relay that event to subscribed InputComponents.
	 */
	virtual void BroadcastNewEvent (const sf::Event& newEvent);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual MousePointer* GetAssociatedMouse () const;
	static EKeyboardState GetKeyboardState ();
	static bool GetCtrlHeld ();
	static bool GetShiftHeld ();
	static bool GetAltHeld ();
	virtual Window* GetInputSource () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void SetLockedRegisteredComponents (bool bNewLockedRegisteredComponents);

	/**
	 * Various input events to broadcast to InputComponents whenever a window event message was received.
	 */
	virtual void NewKeyEvent (sf::Event newEvent);
	virtual void NewTextEvent (sf::Event newEvent);
	virtual void NewMouseMoveEvent (sf::Event::MouseMoveEvent sfmlEvent);
	virtual void NewMouseButtonEvent (sf::Event::MouseButtonEvent sfmlEvent, sf::Event::EventType eventType);
	virtual void NewMouseWheelEvent (sf::Event::MouseWheelScrollEvent sfmlEvent);

	/**
	 * Helper functions to convert various aspects of the sf::Event to DString (for logging).
	 */
	virtual DString KeyEventToString (sf::Event sfEvent) const;
	virtual DString TextEventToString (sf::Event sfEvent) const;
	virtual DString MouseMoveEventToString (sf::Event::MouseMoveEvent sfEvent) const;
	virtual DString MouseButtonEventToString (sf::Event::MouseButtonEvent sfEvent) const;
	virtual DString MouseWheelEventToString (sf::Event::MouseWheelScrollEvent sfEvent) const;

	/**
	 * Updates the Input Messenger variables for the given InputComponent.
	 * Not virtual since this function relies on InputComponent's friend class.
	 */
	void SetInputMessengerFor (InputComponent* target);
	void ClearInputMessengerFor (InputComponent* target);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * Invoked whenever any event was invoked to the window this broadcaster is bound to.
	 */
	virtual void HandleWindowEvent (const sf::Event& newEvent);

	virtual void HandleGarbageCollection ();
};
SD_END