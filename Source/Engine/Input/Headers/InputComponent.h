/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputComponent.h
  A component that receives events and may potentially capture
  that input event, which hinders other components further down the stack
  from receiving the key event.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
class MousePointer;
class InputBroadcaster;

class INPUT_API InputComponent : public EntityComponent
{
	DECLARE_CLASS(InputComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SKeybind
	{
		enum EButtonType
		{
			BT_Keyboard,
			BT_Mouse
		} ButtonType;

		union
		{
			/* The key the user must press to invoke the callback. */
			sf::Keyboard::Key KeyboardKey;

			/* The mouse button the user must press to invoke the callback. */
			sf::Mouse::Button MouseButton;
		};

		/* If true then this keybind is executed on button pressed. Otherwise, it'll be executed on button released. */
		bool bOnPressedOnly;

		/* Determines which buttons must be held down for this command to execute. */
		EKeyboardState RequiredStates;

		/* The delegate to execute from activating this keybind. If this delegate is bounded and returns true, then it'll consume the input event. */
		SDFunction<bool> OnKeybind;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various callbacks for the input events.
	Nonconsumeable events. All registered InputComponents will receive these callbacks before a consumeable delegate is broadcasted. */
	SDFunction<void, const sf::Event&> OnPassiveInput;
	SDFunction<void, const sf::Event&> OnPassiveText;
	SDFunction<void, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType> OnPassiveMouseClick;
	SDFunction<void, MousePointer*, const sf::Event::MouseWheelScrollEvent&> OnPassiveMouseWheel;

	SDFunction<void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2& /*deltaMove*/> OnMouseMove;

	/* Consumeable events. If they return true, it'll prevent other InputComponents of lower priority from receiving these events. */
	SDFunction<bool, const sf::Event&> OnInput;
	SDFunction<bool, const sf::Event&> OnText;
	SDFunction<bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType> OnMouseClick;
	SDFunction<bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&> OnMouseWheel;

protected:
	/* Broadcaster this component registered to.  This component will unregister itself from the
	broadcaster on destroy.  This is automatically set when (un)registering to a broadcaster.*/
	DPointer<InputBroadcaster> InputMessenger;

	/* If true, then this InputComponent may receive input events.  Otherwise the broadcaster may skip over this component until reenabled. */
	bool bInputEnabled;

	/* Input Priority this InputComponent was registered to its InputMessenger.  An InputBroadcaster directly assigns this variable whenever this component registers to it. */
	Int InputPriority;

	/* List of keybinds this input component could invoke. If any of the events return true, it'll consume the event, preventing it from passing the generic input callbacks.
	Keybinds takes precedent over this component's generic input events. It does not take priority over other InputComponent's with greater priority. */
	std::vector<SKeybind> Keybinds;

	/* Broadcaster this component is pending to.  Needed in case this component is moving to a new broadcaster while this one was already pending from another broadcaster. */
	DPointer<InputBroadcaster> PendingInputMessenger;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this InputComponent is allowed to receive input events.
	 */
	virtual bool CanReceiveInput () const;

	/**
	 * Invoked whenever a key press event was sent.
	 * Warning:  Keep a look out for overlaping keyEvents.  For example:  if bUnicode is unchecked, keyEvent.Text.unicode 54 will be true for both "6" and sf::Keyboard::Tilde
	 */
	virtual void ProcessInput (const sf::Event& keyEvent);

	/**
	 * Same as ProcessInput.
	 * Except if this returns true, it'll prevent other InputComponents further down the list from receiving this event.
	 */
	virtual bool CaptureInput (const sf::Event& keyEvent);

	/**
	 * Invoked whenever a text event was sent.
	 * Not to be confused with CaptureInput.  This function's keyEvent contains interpretted text from sequence of keys that was pressed (ie:  Shift + a = 'A').
	 */
	virtual void ProcessText (const sf::Event& keyEvent);

	/**
	 * Same as ProcessText.
	 * Except if this returns true, it'll prevent other InputComponents further down the list from receiving this event.
	 */
	virtual bool CaptureText (const sf::Event& keyEvent);

	/**
	 * Invoked whenever the mouse pointer moved.
	 * Use sfmlEvent.x to get the new mouse pointer cordinate relative to the left of the window.
	 * Use sfmlEvent.y to get the new mouse pointer cordinate relative to the top of the window.
	 * deltaMove is the distance traveled since last MouseMove event.
	 */
	virtual void MouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);

	/**
	 * Invoked whenever any mouse button was clicked.
	 *
	 * Supported buttons:
	 * Left, Right, Middle, XButton1, XButton2
	 *
	 * eventType will either MouseButtonPressed or MouseButtonReleased
	 */
	virtual void ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);

	/**
	 * Same as ProcessMouseClick.
	 * Except if this returns true, it'll prevent other InputComponents further down the list from receiving this event.
	 */
	virtual bool CaptureMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);

	/**
	 * Invoked whenever the mouse wheel moved.
	 * sfmlEvent.delta will be positive if the wheel moved up, negative when wheel moved down.
	 */
	virtual void ProcessMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Same as ProcessMouseWheel.
	 * Except if this returns true, it'll prevent other InputComponents further down the list from receiving this event.
	 */
	virtual bool CaptureMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	virtual void SetInputEnabled (bool bNewInputEnabled);
	virtual void SetInputPriority (Int newPriority);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Adds the keybind to this component. If the callback already exists, this function will simply update its keybinding.
	 * @param bOnPressedOnly - If true then this callback is executed when the button is immediately pressed. Otherwise it'll only execute when the button is released.
	 */
	virtual void AddKeybind (sf::Keyboard::Key keyboardKey, bool bOnPressedOnly, const SDFunction<bool>& OnExecution);
	virtual void AddKeybind (sf::Keyboard::Key keyboardKey, bool bOnPressedOnly, EKeyboardState reqStates, const SDFunction<bool>& OnExecution);
	virtual void AddKeybind (sf::Mouse::Button mouseButton, bool bOnPressedOnly, const SDFunction<bool>& OnExecution);

	virtual void RemoveKeybind (const SDFunction<bool>& OnExecution);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual InputBroadcaster* GetInputMessenger () const;
	virtual Int GetInputPriority () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through the keybinds to see if any one of them can process this particular event. Returns true if a keybind is executed AND consumed the event.
	 */
	virtual bool ProcessKeybinds (const sf::Event& keyboardEvent) const;
	virtual bool ProcessKeybinds (const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType eventType) const;

	friend class InputBroadcaster;
};
SD_END