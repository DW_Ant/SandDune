/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MouseTester.h
  Entity that helps the InputUnitTester to validate the MousePointer.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef DEBUG_MODE
SD_BEGIN
class InputComponent;
class InputBroadcaster;
class MouseTesterRender;

class INPUT_API MouseTester : public Entity, public PlanarTransform
{
	DECLARE_CLASS(MouseTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this entity will listen and may capture input events. */
	bool bActive;

	UnitTester::EUnitTestFlags TestFlags;

protected:
	/* Becomes true if the user is currently setting the limit region. */
	bool bSettingLimitRegion;

	DPointer<InputComponent> Input;

	/* Component responsible for rendering the rectangle for region limits. */
	DPointer<MouseTesterRender> RegionRenderer;

private:
	/* Various input flags that are pressed down. */
	bool bHolding1;
	bool bHolding2;
	bool bHolding3;
	bool bHolding4;

	/* Is true, if this tester is currently clamping mouse pointer. */
	bool bClampingMouse;

	/* Various mouse icons to use based on held numbers. */
	static DPointer<Texture> Holding1Tx;
	static DPointer<Texture> Holding2Tx;
	static DPointer<Texture> Holding3Tx;
	static DPointer<Texture> Holding4Tx;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void PostEngineInitialize () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleCaptureInput (const sf::Event& keyEvent);
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);

	/* Various event handlers that determine which mouse icon the mouse pointer should use. */
	virtual bool HandleMouseIcon1 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);
	virtual bool HandleMouseIcon2 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);
	virtual bool HandleMouseIcon3 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);
	virtual bool HandleMouseIcon4 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);

	virtual bool HandlePushLimit (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);
};
SD_END

#endif