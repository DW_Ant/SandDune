/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Input.h
  Contains important file includes and definitions for the Input module.

  The input module contains classes such as InputComponents and InputBroadcaster.
  The main broadcaster and default behavior for InputComponents will react to callbacks
  from the main application window; they could be listening to events from other windows.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"

#ifdef WITH_LOCALIZATION
#include "Engine\Localization\Headers\LocalizationClasses.h"
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef INPUT_EXPORT
		#define INPUT_API __declspec(dllexport)
	#else
		#define INPUT_API __declspec(dllimport)
	#endif
#else
	#define INPUT_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsInput.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacInput.h"
#endif

SD_BEGIN
extern LogCategory INPUT_API InputLog;

enum EKeyboardState
{
	KS_None = 0x0,
	KS_Ctrl = 0x1,
	KS_Alt = 0x2,
	KS_Shift = 0x4
};

DEFINE_ENUM_FUNCTIONS(EKeyboardState)
SD_END