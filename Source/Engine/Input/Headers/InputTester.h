/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputTester.h
  Entity that helps the InputUnitTester to validate the InputComponent.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef DEBUG_MODE
SD_BEGIN
class INPUT_API InputTester : public Entity
{
	DECLARE_CLASS(InputTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	UnitTester::EUnitTestFlags TestFlags;

protected:
	DPointer<InputComponent> TestComponent;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleCaptureInput (const sf::Event& keyEvent);
	virtual bool HandleCaptureText (const sf::Event& keyEvent);
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool HandleMouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

};
SD_END

#endif