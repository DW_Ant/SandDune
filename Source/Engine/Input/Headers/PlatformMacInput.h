/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMacInput.h
  Includes any Windows-specific libraries and definitions for the Input module.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef PLATFORM_MAC
SD_BEGIN


/*
=====================
  Methods
=====================
*/

/**
 * Sets the mouse coordinates relative to the top left corner of the desktop.
 */
void INPUT_API OS_SetMousePosition (const Vector2& mousePosition);

/**
 * Retrieves the OS mouse position relative to the top left corner of the desktop.
 */
Vector2 INPUT_API OS_GetMousePosition ();

/**
 * Clamps the operating system's mouse pointer to be within a rectangle.
 * If everything is 0, then no clamps are applied.
 * The Region assumes X-right and Y-down coordinate system.
 */
void INPUT_API OS_ClampMousePointer (const Rectangle& region);
SD_END

#endif //PLATFORM_MAC