/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MousePointer.h
  Object that's responsible for drawing the mouse cursor.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
//TODO:  Readjust current mouse limit region whenever the window changed positions.

class INPUT_API MousePointer : public Entity, public PlanarTransform
{
	DECLARE_CLASS(MousePointer)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SIconOverride
	{
		/* Texture icon to use while this override is active. */
		DPointer<const Texture> Icon;

		/* Function to callback that should return true if this struct should continue to override the mouse pointer. */
		SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> OverrideCallback;
	};

	struct SMouseLimit
	{
		/* Function callback that'll return false whenever this region is no longer applicable. */
		SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> LimitCallback;

		/* Limit region to apply while this MouseLimit is active. */
		Rectangle LimitRegion;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Int INPUT_PRIORITY;

	/* If true, the mouse pointer will continuously evaluate clamped mouse positions on mouse move.  Otherwise, external classes are expected to invoke EvaluateClampedMousePointer during relevant times. */
	bool bContinuouslyEvalClampPos;

protected:
	/* Becomes true if the mouse position is artifically being placed (eg: The program is setting the mouse position instead of the user input). Check against this flag
	if there's a need to check if the MouseMove functions need to make a distinction between user input and system input. */
	static bool bEngineMovingMouse;

	/* The user must click within this time (in seconds) for the mouse event to register as a double click.
	If an InputComponent needs to react to a double click event, they should internally keep track of their last click time and compare it against this value.
	Typically the double click event happens on mouse press (instead of release).
	Also it's typical to cancel the double click counter if a separate entity was clicked. For example if they clicked on component A, then B, then back to A all
	within the threshold, it shouldn't be a double click event. */
	Float DoubleClickThreshold;

	/* If true, then this mouse will initialize its InputComponent to react to mouse events. Otherwise, this assumes the mouse will receive events externally (eg: FakeMouse).
	This must be assigned before BeginObject. */
	bool bSetupInputComp;

	/* Counter that determines if the mouse should be visible or not.  It's hidden when this value is greater than 0. */
	Int ShowMouseValue;

	/* If true, then this mouse pointer cannot move. */
	bool bLocked;

	DPointer<SpriteComponent> MouseIcon;
	DPointer<InputComponent> MouseInput;

	/* Texture to use when displaying mouse pointer without any IconOverrides. */
	DPointer<Texture> DefaultIcon;

	/* Stack of clampped mouse positions.  The vector on top of the stack determines the clamp limits of the mouse pointer. */
	std::vector<SMouseLimit> ClampPositionsStack;

	/* Stack of icons that are overriding the mouse pointer icon.  Only the element on top of the stack is considered.
	It will only pop the stack if the callback function on top returned false. */
	std::vector<SIconOverride> MouseIconOverrideStack;

	/* Determines which window handle is this mouse pointer rendering to and capturing events for. */
	DPointer<Window> OwningWindowHandle;

	/* Tick Component responsible for evaluating the mouse icon override stack. */
	TickComponent* IconOverrideTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the OS's mouse pointer coordinates relative to the top left corner of the desktop.
	 */
	static Vector2 GetAbsMousePosition ();

	/**
	 * Returns the mouse pointer coordinates relative to the top left corner of the specified window.
	 */
	static Vector2 GetRelativeMousePosition (Window* windowHandle);

	/**
	 * Returns the displacement vector that can translate from local coordinate system to the next outer coordinate system.
	 */
	virtual Vector2 ToOuterCoordinates () const;

	/**
	 * Recursively climbs up the coordinate spaces until it's in window space. This function returns the Vector that converts
	 * from local to window coordinate space.
	 */
	virtual Vector2 ToWindowCoordinates () const;

	/**
	 * Sets the OS mouse position relative to the top left corner of the desktop.
	 */
	static void SetAbsMousePosition (const Vector2& newMousePos);

	/**
	 * Sets the OS mouse position relative to the top left corner of the given window.
	 */
	static void SetRelativeMousePosition (Window* windowHandle, const Vector2& newMousePos);

	/**
	 * Pushes a new mouse position limit to the clamped positions stack.
	 * The limitCallback is the function callback to refer to that should return false whenever the limit region should no longer apply.
	 * @param newLimitRegion The region in desktop space in pixels that determines the new limits the mouse can move within.  This assumes X-Right, Y-Down coordinates.
	 * @param limitCallback The function invoked every mouse move update.  When this function returns false, then the region restrictions are removed.
	 */
	virtual void PushPositionLimit (Rectangle newLimitRegion, SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> limitCallback);

	/**
	 * Removes the specified position limit.  Returns true if found and removed.
	 */
	virtual bool RemovePositionLimit (SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback);

	/**
	 * Pushes a new mouse icon override to the top of the stack.  Every mouse pointer icon evaluation
	 * will call the callback function to see if this element should continue overriding the mouse pointer or not.
	 * Only the element on top of the stack is evaluated.
	 */
	virtual void PushMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction);

	/**
	 * Changes the mouse icon override that's associated with the given callback.
	 * If the callback doesn't exist, then this function will do nothing.
	 * Returns true if the icon is updated.
	 */
	virtual bool UpdateMouseIconOverride (const Texture* newIcon, const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& callbackFunction);

	/**
	 * Removes the specified MouseIconOverride.  Returns true if found and removed.
	 */
	virtual bool RemoveIconOverride (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback);

	/**
	 * Returns true if the icon override is currently viewed (on top of stack).
	 */
	virtual bool IsIconOverrideCurrentlyViewed (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback);

	/**
	 * Returns true if the icon override exists somewhere in the stack.
	 */
	virtual bool IsIconOverrideRegistered (const SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&>& targetCallback) const;

	virtual void SetDefaultIcon (Texture* newIcon);

	virtual void SetMousePosition (const Vector2& newPos);

	/**
	 * Increments ShowMouseValue when bVisible is false.  Decrements ShowMouseValue when bVisible is true.
	 */
	virtual void SetMouseVisibility (bool bVisible);

	virtual void SetLocked (bool bNewLocked);

	virtual void SetOwningWindowHandle (Window* newOwningWindowHandle);

	/**
	 * Looks at the top of the MouseIconOverrides stack to see if a new icon needs to be applied or not.
	 */
	virtual void EvaluateMouseIconOverrides ();

	/**
	 * Looks at the top of the ClampPositions stack to see if a new mouse region limit needs to be applied or not.
	 */
	virtual void EvaluateClampedMousePointer ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDoubleClickThreshold (Float newDoubleClickThreshold);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static MousePointer* GetMousePointer ();

	static inline bool IsEngineMovingMouse ()
	{
		return bEngineMovingMouse;
	}

	/**
	 * Returns the mouse pointer instance that is receiving the input events.
	 * Returns self for most cases. For the FakeMouse, it'll recursively return the owning instance.
	 */
	virtual MousePointer* GetRootMouse ();

	virtual Float GetDoubleClickThreshold () const;

	virtual bool GetMouseVisibility () const;
	virtual bool GetLocked () const;

	/**
	 * Returns the size of ClampPositionsStack
	 */
	virtual Int GetClampSize () const;

	virtual Texture* GetDefaultIcon () const;

	virtual SpriteComponent* GetMouseIcon () const;
	virtual InputComponent* GetMouseInput () const;
	virtual Window* GetOwningWindowHandle () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads from the Input.ini to initialize mouse-related properties.
	 */
	virtual void LoadMouseSettings ();

	/**
	 * Offsets the rectangle by the main window's position coordinates.
	 * outRectangle is assumed to be in X-right Y-down coordinates.
	 */
	virtual void AddWindowOffsetTo (Rectangle& outRectangle) const;

	/**
	 * Returns the default mouse icon size (in pixels).
	 */
	virtual Vector2 GetDefaultMouseSize () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual void HandleIconOverrideTick (Float deltaSec);
};
SD_END