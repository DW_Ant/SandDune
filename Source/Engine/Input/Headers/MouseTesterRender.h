/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MouseTesterRender.h
  Specialized render component used to draw a rectangle representing
  the limit region the MouseTester establishes.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef DEBUG_MODE
SD_BEGIN
class INPUT_API MouseTesterRender : public RenderComponent
{
	DECLARE_CLASS(MouseTesterRender)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Rectangle corner coordinates relative to the upper left corner of window.  This corner is where the tester pressed down on. */
	Vector2 PivotPoint;

	/* Rectangle corner coordinates relative to the upper left corner of window.  This corner is the opposite corner of the pivot. */
	Vector2 FlexiblePoint;

	float BorderThickness;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
	virtual Aabb GetAabb () const override;
};
SD_END

#endif