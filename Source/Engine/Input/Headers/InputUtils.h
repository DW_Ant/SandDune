/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputUtils.h
  Contains miscellaneous static utility functions for the Input module.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
class MousePointer;
class InputBroadcaster;

class INPUT_API InputUtils : public BaseUtils
{
	DECLARE_CLASS(InputUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Convient function that sets up a InputBroadcaster and a MousePointer to the specified window handle.
	 *
	 * @param window The window handle that needs a InputBroadcaster and a MousePointer associated with it.
	 * @param input Registers this input to the specified window.
	 * @param mouse Mouse object that'll render itself to the specified window, and link it with the input broadcaster.
	 */
	static void SetupInputForWindow (Window* window, InputBroadcaster* input, MousePointer* mouse);

#ifdef WITH_LOCALIZATION
	/**
	 * Translates the given key to human readable text that is translated to their local language.
	 */
	static DString TranslateToHumanReadableText (sf::Keyboard::Key keyboard);
	static DString TranslateToHumanReadableText (sf::Mouse::Button mouseButton);
#endif
};
SD_END