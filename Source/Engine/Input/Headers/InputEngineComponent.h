/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputEngineComponent.h
  This component is responsible for instantiating the InputBroadcaster
  that listens for input messages from the main application's window.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
class InputBroadcaster;
class MousePointer;

class INPUT_API InputEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(InputEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Broadcast handler that'll be broadcasting message events from the main window. */
	DPointer<InputBroadcaster> MainBroadcaster;

	/* List of all instanced broadcasters. */
	std::vector<InputBroadcaster*> InputBroadcasters;

	/* Mouse pointer affiliated with the main application window (defined in GraphicsEngineComponent). */
	DPointer<MousePointer> Mouse;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	InputEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Finds an input broadcaster that's broadcasting input events from the specified window.
	 */
	virtual InputBroadcaster* FindBroadcasterForWindow (Window* window) const;

	/**
	 * Unconditionally appends the new broadcaster to the list of Broadcasters.
	 */
	virtual void RegisterInputBroadcaster (InputBroadcaster* newBroadcaster);

	/**
	 * Removes input broadcaster from the list of broadcasters.
	 */
	virtual void UnregisterInputBroadcaster (InputBroadcaster* oldBroadcaster);

	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual InputBroadcaster* GetMainBroadcaster () const;
	virtual MousePointer* GetMouse () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports numerous textures for the mouse pointer.
	 */
	virtual void ImportMouseIcons (TexturePool* localTexturePool);
};
SD_END