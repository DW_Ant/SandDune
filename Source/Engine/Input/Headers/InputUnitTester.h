/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputUnitTester.h
  Tests various aspects of the Input Module.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef DEBUG_MODE
SD_BEGIN
class InputBroadcaster;

class INPUT_API InputUnitTester : public UnitTester
{
	DECLARE_CLASS(InputUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestInputBroadcaster (EUnitTestFlags testFlags) const;
	virtual bool TestInputComponent (EUnitTestFlags testFlags) const;
	virtual bool TestMousePointer (EUnitTestFlags testFlags) const;

};
SD_END

#endif