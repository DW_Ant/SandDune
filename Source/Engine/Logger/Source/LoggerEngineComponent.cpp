/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LoggerEngineComponent.cpp
=====================================================================
*/

#include "Console.h"
#include "LoggerEngineComponent.h"

IMPLEMENT_ENGINE_COMPONENT(SD::LoggerEngineComponent)
SD_BEGIN

const Directory LoggerEngineComponent::LOG_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Logs"));

LoggerEngineComponent::LoggerEngineComponent () : Super(),
	BaseLogName(ProjectName),
	LogFile(nullptr),
	LogLifeSpan(-1), //Default to negative in case the config file was not opened, and doesn't cause archived logs to be deleted.
	TimestampFormat(TXT("%name-Backup %month-%day-%year %hour.%minute.%second")),
	LogHeaderFormat(TXT("[%type]:  ")),
	ConsoleHandler(nullptr),
	bLogging(false)
{
	//Noop
}

void LoggerEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	CHECK(OwningEngine != nullptr)
	if (OwningEngine->HasCmdLineSwitch(TXT("-log"), DString::CC_IgnoreCase))
	{
		CreateConsole();
	}
}

void LoggerEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	//Retrieve config files
	ConfigWriter* configWriter = ConfigWriter::CreateObject();
	if (configWriter != nullptr)
	{
		configWriter->OpenFile(Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + DString(TXT("Logger.ini")), false);
		if (!configWriter->IsFileOpened())
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to open configuration file for LoggerEngineComponent.  Using default values."));
		}
		else
		{
			LogLifeSpan = configWriter->GetProperty<Int>(TXT("LoggerEngineComponent"), TXT("LogLifeSpan"));
			TimestampFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("TimestampFormat"));
			LogHeaderFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("LogHeaderFormat"));
		}

		configWriter->Destroy();
	}

	PurgeOldLogFiles();
	SetupLogWriter();
	LoggerLog.Log(LogCategory::LL_Log, TXT("LoggerEngineComponent initialized.  SandDune version is %s."), Version::GetVersionNumber());
	LoggerLog.Log(LogCategory::LL_Log, TXT("System configuration. . ."));

#ifdef PLATFORM_WINDOWS
	LoggerLog.Log(LogCategory::LL_Log, TXT("    OS: Windows"));
#elif PLATFORM_MAC
	LoggerLog.Log(LogCategory::LL_Log, TXT("    OS: Mac"));
#else
	#error Please place a log statement for your platform.
#endif

#ifdef PLATFORM_64BIT
	LoggerLog.Log(LogCategory::LL_Log, TXT("    Platform: 64 bit"));
#else
	LoggerLog.Log(LogCategory::LL_Log, TXT("    Platform: 32 bit"));
#endif

#ifdef DEBUG_MODE
	LoggerLog.Log(LogCategory::LL_Log, TXT("    Configuration: Debug"));
#else
	LoggerLog.Log(LogCategory::LL_Log, TXT("    Configuration: Release"));
#endif

	if (DataBuffer::IsSystemLittleEndian())
	{
		LoggerLog.Log(LogCategory::LL_Log, TXT("    Byte Order: Little Endian"));
	}
	else
	{
		LoggerLog.Log(LogCategory::LL_Log, TXT("    Byte Order: Big Endian"));
	}
}

void LoggerEngineComponent::FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg)
{
	FormatLogHeader(category->GetLogTitle(), logLevel, msg);
}

void LoggerEngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{
	if (!bLogging)
	{
		bLogging = true;

		if ((category->GetUsageFlags() & LogCategory::FLAG_LOG_FILE) > 0)
		{
			//Record to file writer
			if (LogFile.IsValid())
			{
				LogFile->AddTextEntry(formattedMsg);
			}
			else
			{
				//Push log message to vector to be later recorded whenever LogFile was initialized.
				PendingLogMsgs.push_back(formattedMsg);
			}
		}

		if (ConsoleHandler != nullptr && (category->GetUsageFlags() & LogCategory::FLAG_OUTPUT_WINDOW) > 0)
		{
			ConsoleHandler->WriteOutput(formattedMsg, logLevel);
		}

		bLogging = false;
	}
}

void LoggerEngineComponent::ShutdownComponent ()
{
	if (LogFile.IsValid())
	{
		//Bid the user good well before departing. . .
		LoggerLog.Log(LogCategory::LL_Log, TXT("Engine is properly shutting down.  Closing log file - Good Bye!"));

		//Write out the remaining buffer to the file
		LogFile->WriteToFile(true);
		LogFile->Destroy();
		LogFile = nullptr;
		PreserveLogFile();
	}

	if (ConsoleHandler != nullptr)
	{
		FreeConsole();
	}

	Super::ShutdownComponent();
}

void LoggerEngineComponent::CreateConsole ()
{
	if (ConsoleHandler == nullptr)
	{
		ConsoleHandler = Console::CreateObject();
	}
}

void LoggerEngineComponent::FreeConsole ()
{
	if (ConsoleHandler != nullptr)
	{
		ConsoleHandler->Destroy();
		ConsoleHandler = nullptr;
	}
}

void LoggerEngineComponent::PurgeOldLogFiles ()
{
	if (LogLifeSpan <= 0)
	{
		return; //Files don't expire
	}

	DateTime today;
	today.SetToCurrentTime();
	Int todayDays = today.CalculateTotalDays().ToInt();

	std::vector<FileAttributes> pendingDeleteFiles;
	for (FileIterator fileIter(LOG_DIRECTORY, FileIterator::SF_IncludeFiles); !fileIter.FinishedIterating(); fileIter++)
	{
		AdvFileAttributes selectedAttributes(fileIter.GetSelectedAttributes());

		//Only consider .log files
		if (!selectedAttributes.GetFileExists() || !selectedAttributes.GetFileExtension().EndsWith(TXT("log"), DString::CC_IgnoreCase))
		{
			continue;
		}

		//Can't delete read only files
		if (selectedAttributes.GetReadOnly())
		{
			continue;
		}

		if (selectedAttributes.GetLastModifiedDate().CalculateTotalDays().ToInt() + LogLifeSpan < todayDays)
		{
			//Mark for deletion.  Don't delete actually here since iterator is still referencing file.
			pendingDeleteFiles.push_back(selectedAttributes);
		}
	}

	for (size_t i = 0; i < pendingDeleteFiles.size(); ++i)
	{
		LoggerLog.Log(LogCategory::LL_Log, TXT("Purging old log file:  %s"), pendingDeleteFiles.at(i).GetName(false, true));
		if (!OS_DeleteFile(pendingDeleteFiles.at(i)))
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Failed to purge old log file:  %s"), pendingDeleteFiles.at(i).GetName(false, true));
		}
	}
}

void LoggerEngineComponent::SetupLogWriter ()
{
	LogFile = TextFileWriter::CreateObject();

	if (LogFile.IsNullptr())
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate a TextFileWriter object.  The LoggerEngineComponent will be unable to write messages to the system log."));
		return;
	}

	if (!LogFile->OpenFile(LOG_DIRECTORY.ReadDirectoryPath() + BaseLogName + TXT(".Log"), true))
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to open:  %s%s.Log!"), Directory::BASE_DIRECTORY, BaseLogName);
		return;
	}

	for (size_t i = 0; i < PendingLogMsgs.size(); ++i)
	{
		LogFile->AddTextEntry(PendingLogMsgs.at(i));
	}
	PendingLogMsgs.clear();
}

void LoggerEngineComponent::FormatLogHeader (const DString& logHeader, LogCategory::ELogLevel logLevel, OUT DString& outFullMessage)
{
	DString header = LogHeaderFormat;

	Engine* localEngine = Engine::FindEngine();
	header.ReplaceInline(TXT("%type"), logHeader, DString::CC_CaseSensitive);
	header.ReplaceInline(TXT("%frame"), DString::MakeString(localEngine->GetFrameCounter()), DString::CC_CaseSensitive);
	header.ReplaceInline(TXT("%level"), LogCategory::LogLevelToString(logLevel), DString::CC_CaseSensitive);

	const clock_t& startTime = localEngine->GetStartTime();
	clock_t deltaTime = clock() - startTime;
	header.ReplaceInline(TXT("%time"), Float(deltaTime * 0.001f).ToFormattedString(1, 3, Float::TM_Round), DString::CC_CaseSensitive);

	if (header.Find(TXT("%"), 0, DString::CC_CaseSensitive) >= 0)
	{
		DateTime now;
		now.SetToCurrentTime();
		header = now.FormatStringPadded(header);
	}

	outFullMessage = header + outFullMessage;
}

void LoggerEngineComponent::PreserveLogFile ()
{
	DString newFileName;
	GenerateFileName(newFileName);

	if (!OS_CopyFile(FileAttributes(LOG_DIRECTORY, BaseLogName + TXT(".Log")), LOG_DIRECTORY, newFileName, true))
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to save a copy of %s.Log to %s.  The logs from this session will not be archived."), BaseLogName, newFileName);
	}
}

void LoggerEngineComponent::GenerateFileName (DString& outFileName)
{
	DateTime today;
	today.SetToCurrentTime();

	outFileName = TimestampFormat;
	outFileName = today.FormatStringPadded(outFileName);
	outFileName.ReplaceInline(TXT("%name"), BaseLogName, DString::CC_IgnoreCase);
}
SD_END