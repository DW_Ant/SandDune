/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsLogger.cpp
=====================================================================
*/


#include "Logger.h"
#include "ConsoleParams.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
bool OS_CreateConsole (SConsoleParams& outParams)
{
	if (!AllocConsole())
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to create a console. Error code: %s"), DString::MakeString(GetLastError()));
		return false;
	}

	if (outParams.bObtainOutput)
	{
		outParams.OutputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		if (outParams.OutputHandle == INVALID_HANDLE_VALUE || outParams.OutputHandle == nullptr)
		{
			OS_FreeConsole();
			outParams.OutputHandle = nullptr;
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to create console. Could not obtain the output handle. Error code: %s"), DString::MakeString(GetLastError()));
			return false;
		}

		//NOTE: According to ms documentation, there is no need to close the handle when done.
	}

	if (!outParams.ConsoleTitle.IsEmpty())
	{
		OS_SetConsoleTitle(outParams.ConsoleTitle);
	}

	return true;
}

bool OS_SetConsoleTitle (const DString& newTitle)
{
	LPCTSTR winTitle = newTitle.ToCString();
	return SetConsoleTitle(winTitle);
}

bool OS_FreeConsole ()
{
	if (!FreeConsole())
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to free a console instance. Error code: %s"), DString::MakeString(GetLastError()));
		return false;
	}

	return true;
}

bool OS_WriteConsole (SConsoleParams& outParams, const DString& msg)
{
	WORD oldColor;
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	if (outParams.TextColor != SConsoleParams::C_Default)
	{
		if (!GetConsoleScreenBufferInfo(outParams.OutputHandle, &consoleInfo))
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to write \"%s\" to the console since it was unable to obtain screen buffer info. Error code: %s"), msg, DString::MakeString(GetLastError()));
			return false;
		}

		oldColor = consoleInfo.wAttributes;
		WORD newColor;
		//Convert SConsoleParams::EColor to MS-specific color
		switch (outParams.TextColor)
		{
			default:
			case(SConsoleParams::C_Default):
				newColor = oldColor;
				break;

			case(SConsoleParams::C_Black):
				newColor = 0;
				break;

			case(SConsoleParams::C_Gray):
				newColor = FOREGROUND_INTENSITY;
				break;

			case(SConsoleParams::C_White):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
				break;

			case(SConsoleParams::C_Red):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_RED;
				break;

			case(SConsoleParams::C_Yellow):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN;
				break;

			case(SConsoleParams::C_Green):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_GREEN;
				break;

			case(SConsoleParams::C_Cyan):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE;
				break;

			case(SConsoleParams::C_Blue):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_BLUE;
				break;

			case(SConsoleParams::C_Magenta):
				newColor = FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_RED;
				break;
		}

		if (!SetConsoleTextAttribute(outParams.OutputHandle, newColor))
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to write \"%s\" to the console since it was unable to set the console text color to %s. Error code: %s"), msg, DString::MakeString(newColor), DString::MakeString(GetLastError()));
			return false;
		}
	}

	const TCHAR* buffer = msg.ToCString();
	DWORD numChars = msg.Length().Value;
	DWORD numCharsWritten;
	void* reserved = nullptr;
	if (WriteConsole(outParams.OutputHandle, buffer, numChars, &numCharsWritten, reserved) == 0)
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Failed to write \"%s\" to the console. Only %s characters were written. Error code: %s"), msg, DString::MakeString(numCharsWritten), DString::MakeString(GetLastError()));
		return false;
	}

	if (outParams.TextColor != SConsoleParams::C_Default)
	{
		//Restore color
		SetConsoleTextAttribute(outParams.OutputHandle, oldColor);
	}

	return true;
}
SD_END
#endif