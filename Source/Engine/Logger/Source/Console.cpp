/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Console.cpp
=====================================================================
*/

#include "Console.h"

IMPLEMENT_CLASS(SD::Console, SD::Object)
SD_BEGIN

void Console::InitProps ()
{
	Super::InitProps();

	bIsActive = false;
}

void Console::BeginObject ()
{
	Super::BeginObject();

	Params.bObtainOutput = true;
	bIsActive = OS_CreateConsole(OUT Params);
}

void Console::Destroyed ()
{
	if (bIsActive)
	{
		OS_FreeConsole();
		bIsActive = false;
	}

	Super::Destroyed();
}

void Console::WriteOutput (const DString& msg, LogCategory::ELogLevel logLevel)
{
	if (bIsActive && Params.OutputHandle != nullptr)
	{
		switch (logLevel)
		{
			default:
#ifdef DEBUG_MODE
			case(LogCategory::LL_Debug):
#endif
			case(LogCategory::LL_Verbose):
				Params.TextColor = SConsoleParams::C_Default;
				break;

			case(LogCategory::LL_Log):
				Params.TextColor = SConsoleParams::C_White;
				break;

			case(LogCategory::LL_Warning):
				Params.TextColor = SConsoleParams::C_Yellow;
				break;

			case(LogCategory::LL_Critical):
			case(LogCategory::LL_Fatal):
				Params.TextColor = SConsoleParams::C_Red;
				break;		
		}

		OS_WriteConsole(OUT Params, msg);
	}
}
SD_END