/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformMac.cpp
=====================================================================
*/

#include "Logger.h"

#ifdef PLATFORM_MAC

SD_BEGIN
bool OS_CreateConsole (SConsoleParams& outParams)
{
	#error Please implement OS_CreateConsole for your platform.
	return false;
}

bool OS_SetConsoleTitle (const DString& newTitle)
{
	#error Please implement OS_SetConsoleTitle for your platform.
	return false;
}

bool OS_FreeConsole ()
{
	#error Please implement OS_FreeConsole for your platform.
	return false;
}

bool OS_WriteConsole (SConsoleParams& outParams, const DString& msg)
{
	#error Please implement OS_WriteConsole for your platform.
	return false;
}
SD_END
#endif