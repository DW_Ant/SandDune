/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LoggerClasses.h
  Contains all header includes for the Logger module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the LoggerClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_LOGGER
#include "Console.h"
#include "ConsoleParams.h"
#include "Logger.h"
#include "LoggerEngineComponent.h"
#include "PlatformMacLogger.h"
#include "PlatformWindowsLogger.h"

#endif
