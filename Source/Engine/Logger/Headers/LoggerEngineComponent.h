/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LoggerEngineComponent.h
  This component is responsible for setting up the external window
  that'll capture and display log messages.  This component is also
  responsible for managing log files.
=====================================================================
*/

#pragma once

#include "Logger.h"

SD_BEGIN
class Console;

class LOGGER_API LoggerEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(LoggerEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Directory LOG_DIRECTORY;

	/* Log names applied to file names without extension. */
	DString BaseLogName;

protected:
	/* Handle to the object that's responsible writing data to the logs. */
	DPointer<TextFileWriter> LogFile;

	/* List of log statements to be recorded (that were logged before the LogFile was initialized). */
	std::vector<DString> PendingLogMsgs;

	/* Determines number of days a log file may reside before it expires (in days).
	If negative, then log files does not expire. */
	Int LogLifeSpan;

	/* Timestamp to display on the log file names when saving a copy of the log file.
	Available macros are:  %name (BaseLogName), %year, %month, %day, %hour, %minute, %second */
	DString TimestampFormat;

	/* Header to display before every log message.  The following macros are substituted:
	%type (Log Type), %frame (Frame count), %time (elapsed time), %year, %month, %day, %hour, %minute, %second */
	DString LogHeaderFormat;

	Console* ConsoleHandler;

private:
	/* Becomes true if this engine component is currently processing a log. Used to prevent infinite recursion from writing a log that was created from writing a different log. */
	bool bLogging;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	LoggerEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg) override;
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg) override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes an OS console window instance.
	 * The console window instance will automatically perish when this engine component is destroyed.
	 * If there's a need to free the console window before then, call FreeConsole.
	 */
	virtual void CreateConsole ();

	/**
	 * If this engine component created a console window instance, then this function will free it.
	 */
	virtual void FreeConsole ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Searches through all log files, and purges any old log files.
	 */
	virtual void PurgeOldLogFiles ();

	/**
	 * Setups the LogWriter to start writing entries to a particular log file.
	 */
	virtual void SetupLogWriter ();

	/**
	 * Formats the header message for the log message.
	 */
	virtual void FormatLogHeader (const DString& logHeader, LogCategory::ELogLevel logLevel, OUT DString& outFullMessage);

	/**
	 * Saves a copy of the log file with a timestamp in the file name.
	 */
	virtual void PreserveLogFile ();

	/**
	 * Generates a file name using the current timestamp.
	 */
	virtual void GenerateFileName (DString& outFileName);
};
SD_END