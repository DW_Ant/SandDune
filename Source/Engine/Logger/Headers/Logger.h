/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Logger.h
  Contains important file includes and definitions for the Logger module.

  The logger module sets up a file writer that'll capture and record engine
  log messages.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Time\Headers\TimeClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef LOGGER_EXPORT
		#define LOGGER_API __declspec(dllexport)
	#else
		#define LOGGER_API __declspec(dllimport)
	#endif
#else
	#define LOGGER_API
#endif

#define LOGGER_CONFIG TXT("Logger.ini")

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsLogger.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacLogger.h"
#endif

SD_BEGIN
extern LogCategory LOGGER_API LoggerLog;
SD_END