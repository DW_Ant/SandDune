/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlatformWindowsLogger.h
  Utilities containing Windows-specific functions related to the Logger module.
=====================================================================
*/

#pragma once

#include "Logger.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
struct SConsoleParams;

/**
 * Creates a OS console/terminal window and attaches the handle to this process.
 * This application's standard output will be displayed on the console window.
 * Returns true if the console is instantiated, and it should be freed at some point.
 *
 * Note: It's intended that this isn't DLL exported. Use LoggerEngineComponent to create a console.
 */
bool LOGGER_API OS_CreateConsole (SConsoleParams& outParams);

/**
 * Replaces the console title with the given string.
 * Returns true on success.
 */
bool LOGGER_API OS_SetConsoleTitle (const DString& newTitle);

/**
 * Detaches and terminates any console window instance associated with this process.
 * Returns true on success.
 *
 * Note: It's intended that this isn't DLL exported. Use LoggerEngineComponent to free the console.
 */
bool LOGGER_API OS_FreeConsole ();

/**
 * Writes the given string to the console.
 * The console params data struct must contain a pointer to the output handler.
 * Returns true if the text has successfully been written to the console.
 */
bool LOGGER_API OS_WriteConsole (SConsoleParams& outParams, const DString& msg);
SD_END

#endif //PLATFORM_WINDOWS