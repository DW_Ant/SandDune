/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Console.h
  An object that interfaces with the OS Console window.
=====================================================================
*/

#pragma once

#include "Logger.h"
#include "ConsoleParams.h"

SD_BEGIN
class LOGGER_API Console : public Object
{
	DECLARE_CLASS(Console)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Data struct containing the OS handles to the console. */
	SConsoleParams Params;

	/* Becomes true if the OS console is instantiated. */
	bool bIsActive;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Prints the given text on the console window.
	 */
	virtual void WriteOutput (const DString& msg, LogCategory::ELogLevel logLevel);
};
SD_END