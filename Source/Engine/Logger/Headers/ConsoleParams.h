/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ConsoleParams.h
  Data struct used to interface with the OS calls to the console.
=====================================================================
*/

#pragma once

#include "Logger.h"

SD_BEGIN
struct LOGGER_API SConsoleParams
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EColor
	{
		C_Default,
		C_Black,
		C_Gray,
		C_White,
		C_Red,
		C_Yellow,
		C_Green,
		C_Cyan,
		C_Blue,
		C_Magenta
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then it'll attempt to obtain the console's output buffer from the OS. */
	bool bObtainOutput;

	/* The pointer to the output buffer that can write to the OS console. */
	void* OutputHandle;

	/* The color to use when drawing text. */
	EColor TextColor;

	/* Changes the console title to this value. If left empty then it'll use the default title. */
	DString ConsoleTitle;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SConsoleParams () :
		bObtainOutput(true),
		OutputHandle(nullptr),
		TextColor(C_Default),
		ConsoleTitle(DString::EmptyString)
	{
		//Noop
	}
};
SD_END