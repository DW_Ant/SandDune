/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableInt.cpp
=====================================================================
*/

#include "EditableInt.h"

IMPLEMENT_CLASS(SD::EditableInt, SD::EditableField)
SD_BEGIN

EditableInt::SInspectorInt::SInspectorInt (const DString& inPropertyName, const DString& inTooltipText, Int* inAssociatedVariable, Int inDefaultValue) : SInspectorProperty(inPropertyName, inTooltipText),
	bHasMinValue(false),
	bHasMaxValue(false),
	ValueRange(0, 100),
	AssociatedVariable(inAssociatedVariable),
	DefaultValue(inDefaultValue),
	InitOverride(inDefaultValue)
{
	//Noop
}

void EditableInt::SInspectorInt::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString configStr = config->GetPropertyText(sectionName, PropertyName);
	if (configStr.IsEmpty())
	{
		*AssociatedVariable = DefaultValue;
	}
	else
	{
		AssociatedVariable->ParseString(configStr);
	}
}

void EditableInt::SInspectorInt::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to config since there isn't a variable associated with it."), PropertyName);
		return;
	}

	config->SaveProperty(sectionName, PropertyName, *AssociatedVariable);
}

bool EditableInt::SInspectorInt::LoadBinary (const DataBuffer& incomingData)
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorInt is not associated with a variable."), PropertyName);
		return false;
	}

	if (!incomingData.CanReadBytes(Int::SGetMinBytes()))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary. Not enough bytes in the data buffer."), PropertyName);
		return false;
	}

	if ((incomingData >> *AssociatedVariable).HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load %s from binary. The data is malformed, preventing the buffer from reading an Int."), PropertyName);
		return false;
	}

	return true;
}

void EditableInt::SInspectorInt::SaveBinary (DataBuffer& outData) const
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to binary since it's not associated with any variable."), PropertyName);
		return;
	}

	outData << *AssociatedVariable;
}

EditPropertyComponent* EditableInt::SInspectorInt::CreateBlankEditableComponent () const
{
	return EditableInt::CreateObject();
}

void EditableInt::SInspectorInt::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableInt* intComp = dynamic_cast<EditableInt*>(AssociatedComp))
	{
		Range<Int> actualIntRange(ValueRange);
		if (!bHasMinValue)
		{
			actualIntRange.Min = SD_MININT;
		}

		if (!bHasMaxValue)
		{
			actualIntRange.Max = SD_MAXINT;
		}

		intComp->SetMinValue(actualIntRange.Min);
		intComp->SetMaxValue(actualIntRange.Max);
		intComp->BindVariable(AssociatedVariable);
		intComp->SetDefaultValue(DefaultValue);

		if (AssociatedVariable == nullptr)
		{
			if (TextFieldComponent* textField = intComp->GetPropValueField())
			{
				if (InitType == IIV_UseOverride)
				{
					textField->SetText(InitOverride.ToString());
				}
				else
				{
					textField->SetText(DefaultValue.ToString());
				}
			}
		}
	}
}

void EditableInt::InitProps ()
{
	Super::InitProps();

	MinValue = SD_MININT;
	MaxValue = SD_MAXINT;
	EditedInt = nullptr;
	DefaultInt = 0;

	UpdateIntTick = nullptr;
}

void EditableInt::BeginObject ()
{
	Super::BeginObject();

	UpdateIntTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateIntTick))
	{
		UpdateIntTick->SetTickInterval(0.2f); //Not important to refresh continuously.
		UpdateIntTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableInt, HandleUpdateIntTick, void, Float));
	}
}

void EditableInt::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableInt* intTemplate = dynamic_cast<const EditableInt*>(objTemplate))
	{
		SetMinValue(intTemplate->MinValue);
		SetMaxValue(intTemplate->MaxValue);
		SetNormalBackgroundColor(intTemplate->NormalBackgroundColor);
		SetReadOnlyBackgroundColor(intTemplate->ReadOnlyBackgroundColor);
	}
}

void EditableInt::CopyToBuffer (DataBuffer& copyTo) const
{
	Super::CopyToBuffer(OUT copyTo);

	if (PropValueField != nullptr)
	{
		DString text = PropValueField->GetContent();
		copyTo << text.Atoi();
	}
}

bool EditableInt::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	Int intData;
	if ((incomingData >> intData).HasReadError())
	{
		return false;
	}

	if (PropValueField != nullptr)
	{
		PropValueField->SetText(intData.ToString());
	}

	if (EditedInt != nullptr)
	{
		(*EditedInt) = intData;
	}

	return true;
}

void EditableInt::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	LocalInt = 0;
	if (PropValueField != nullptr)
	{
		LocalInt = PropValueField->GetContent().Atoi();
	}

	EditedInt = &LocalInt;
}

EditPropertyComponent::SInspectorProperty* EditableInt::CreateDefaultInspector (const DString& propName) const
{
	return new SInspectorInt(propName, DString::EmptyString, nullptr);
}

void EditableInt::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateIntTick != nullptr)
	{
		UpdateIntTick->Destroy();
		UpdateIntTick = nullptr;
	}
}

void EditableInt::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (EditedInt != nullptr)
	{
		*EditedInt = DefaultInt;
	}
	else if (PropValueField.IsValid())
	{
		PropValueField->SetText(DefaultInt.ToString());
	}

	MarkPropertyTextDirty();
	ProcessApplyEdit(this);
}

void EditableInt::UnbindVariable ()
{
	Super::UnbindVariable();

	BindVariable(nullptr);
}

void EditableInt::InitializeComponents ()
{
	Super::InitializeComponents();

	if (PropValueField.IsValid())
	{
		PropValueField->MaxNumCharacters = Int(SD_MININT).NumDigits() + 1; //Plus 1 to include the negative sign
	}
}

void EditableInt::ApplyEdits ()
{
	if (PropValueField.IsValid() && !IsReadOnly())
	{
		Int newValue(PropValueField->GetContent());
		newValue = Utils::Clamp(newValue, MinValue, MaxValue);
		PropValueField->SetText(newValue.ToString());

		if (EditedInt != nullptr)
		{
			*EditedInt = newValue;
			if (ResetDefaultsButton.IsValid())
			{
				ResetDefaultsButton->SetEnabled(DefaultInt != *EditedInt);
				if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
				{
					tooltipEntity->HideTooltip();
				}
			}
		}
		else if (ResetDefaultsButton.IsValid())
		{
			Int newEdit;
			newEdit.ParseString(PropValueField->GetContent());
			ResetDefaultsButton->SetEnabled(DefaultInt != newEdit);
			if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
			{
				tooltipEntity->HideTooltip();
			}
		}
	}
}

DString EditableInt::GetPermittedInputChars () const
{
	DString permittedChars = TXT("[0-9");
	if (PropValueField != nullptr)
	{
		bool editingFirstPos = (PropValueField->GetCursorPosition() == 0 || PropValueField->GetHighlightEndPosition() == 0);
		bool isEmpty = ContainerUtils::IsEmpty(PropValueField->ReadContent()) || PropValueField->ReadContent().at(0).IsEmpty();

		if (editingFirstPos &&
			(isEmpty || PropValueField->GetHighlightEndPosition() >= 0 || PropValueField->ReadContent().at(0).At(0) != '-'))
		{
			//If inserting at the beginning of field and if the first character is not already a negative sign, permit the negative sign
			permittedChars += TXT("-");
		}
	}

	permittedChars += TXT("]");
	return permittedChars;
}

void EditableInt::BindVariable (Int* newEditedInt)
{
	EditedInt = newEditedInt;
	MarkPropertyTextDirty();

	if (PropValueField.IsValid())
	{
		if (EditedInt != nullptr)
		{
			//Clamps don't apply when reading from the int.
			PropValueField->SetText(EditedInt->ToString());
		}
		else
		{
			PropValueField->SetText(DString::EmptyString);
		}
	}
}

void EditableInt::SetMinValue (Int newMinValue)
{
	MinValue = newMinValue;

	if (PropValueField.IsValid())
	{
		Int intValue(PropValueField->GetContent());
		if (intValue < MinValue)
		{
			intValue = MinValue;
			MarkPropertyTextDirty();
			PropValueField->SetText(intValue.ToString());
		}
	}
}

void EditableInt::SetMaxValue (Int newMaxValue)
{
	MaxValue = newMaxValue;

	if (PropValueField.IsValid())
	{
		Int intValue(PropValueField->GetContent());
		if (intValue > MaxValue)
		{
			intValue = MaxValue;
			MarkPropertyTextDirty();
			PropValueField->SetText(intValue.ToString());
		}
	}
}

void EditableInt::SetDefaultValue (Int newDefaultInt)
{
	DefaultInt = newDefaultInt;

	if (ResetDefaultsButton.IsNullptr())
	{
		AddResetToDefaultsControl();
	}

	if (ResetDefaultsButton.IsValid() && EditedInt != nullptr)
	{
		ResetDefaultsButton->SetEnabled(!IsReadOnly() && DefaultInt != *EditedInt);
	}
}

void EditableInt::HandleUpdateIntTick (Float deltaSec)
{
	//Don't update the text field if the user is currently editing it.
	if (PropValueField.IsValid() && PropValueField->GetCursorPosition() < 0 && EditedInt != nullptr)
	{
		if (PropValueField->GetContent().Atoi() != *EditedInt)
		{
			MarkPropertyTextDirty();

			//Clamps don't apply when reading from value.
			PropValueField->SetText(EditedInt->ToString());
		}
	}
}
SD_END