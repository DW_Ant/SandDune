/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditPropertyComponent.cpp
=====================================================================
*/

#include "EditPropertyComponent.h"

IMPLEMENT_CLASS(SD::EditPropertyComponent, SD::GuiComponent)
SD_BEGIN

EditPropertyComponent::SInspectorProperty::SInspectorProperty (const DString& inPropertyName, const DString& inTooltipText) :
	PropertyName(inPropertyName),
	TooltipText(inTooltipText),
	bReadOnly(false),
	OwningStruct(nullptr),
	AssociatedComp(nullptr),
	PropertyId(-1),
	InitType(IIV_UseAssociatedVar)
{
	//Noop
}

void EditPropertyComponent::SInspectorProperty::EditPropComponent () const
{
	CHECK(AssociatedComp != nullptr)

	if (AssociatedComp->GetPropertyName() != nullptr)
	{
		AssociatedComp->GetPropertyName()->SetText(PropertyName);
	}

	AssociatedComp->PropertyId = PropertyId;
	AssociatedComp->SetReadOnly(bReadOnly);
	AssociatedComp->OnApplyEdit = OnApplyEdit;

	GuiComponent* tooltipOwner = AssociatedComp->GetComponentTooltipsShouldAttachTo();
	if (!TooltipText.IsEmpty() && tooltipOwner != nullptr)
	{
		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (tooltipOwner->AddComponent(tooltip))
		{
			tooltip->SetTooltipText(TooltipText);
		}
	}
}

EditPropertyComponent::SCaptionColors::SCaptionColors () :
	TextColor(SD::Color::WHITE),
	Background(SD::Color::BLACK)
{
	//Noop
}

EditPropertyComponent::SCaptionColors::SCaptionColors (const Color& inTextColor, const Color& inBackground) :
	TextColor(inTextColor),
	Background(inBackground)
{
	//Noop
}

void EditPropertyComponent::InitProps ()
{
	Super::InitProps();

	bInitPropName = true;
	PropertyId = -1;
	NormalFontColor = Color(225, 225, 225);
	ReadOnlyFontColor = Color(96, 96, 96);
	CaptionErrorColors = SCaptionColors(Color(255, 96, 85), Color(50, 50, 50));
	CaptionInfoColors = SCaptionColors(Color(135, 235, 255), Color(40, 70, 75));
	BarrierTransform = nullptr;
	CaptionBackground = nullptr;
	CaptionComponent = nullptr;
	bReadOnly = false;
	LineHeight = 32.f;
	LineHeightMultiplier = 1.f;
	VerticalSpace = -1.f;
	FirstSubPropPos = 0.f;
	SubPropIndent = 18.f;
	bPropertyTextChanged = true;
	CachedValueAsText = DString::EmptyString;
	bGrabbingBarrier = false;
	BarrierRange = Range<Float>(0.2f, 0.8f);
	FieldButtonSize = Vector2(24.f, LineHeight);
	bIsReplacingMouseIcon = false;
	ResizeTick = nullptr;
}

void EditPropertyComponent::BeginObject ()
{
	Super::BeginObject();

	ResizeTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(ResizeTick))
	{
		ResizeTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditPropertyComponent, HandleResizeTick, void, Float));
		ResizeTick->SetTicking(false);
	}
}

void EditPropertyComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditPropertyComponent* propTemplate = dynamic_cast<const EditPropertyComponent*>(objTemplate))
	{
		NormalFontColor = propTemplate->NormalFontColor;
		ReadOnlyFontColor = propTemplate->ReadOnlyFontColor;

		bool bCreatedObj;
		PropertyName = ReplaceTargetWithObjOfMatchingClass(PropertyName.Get(), propTemplate->GetPropertyName(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(PropertyName);
		}

		if (PropertyName.IsValid())
		{
			PropertyName->CopyPropertiesFrom(propTemplate->GetPropertyName());
		}

		BarrierComponent = ReplaceTargetWithObjOfMatchingClass(BarrierComponent.Get(), propTemplate->GetBarrierComponent(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(BarrierComponent);
		}

		if (BarrierComponent.IsValid())
		{
			BarrierComponent->CopyPropertiesFrom(propTemplate->GetBarrierComponent());
		}

		SetReadOnly(propTemplate->IsReadOnly());
		SetLineHeight(propTemplate->GetLineHeight());
		SetCaptionErrorColors(propTemplate->ReadCaptionErrorColors());
		SetCaptionInfoColors(propTemplate->ReadCaptionInfoColors());
		LineHeightMultiplier = propTemplate->LineHeightMultiplier;
		SetSubPropIndent(propTemplate->SubPropIndent);
		MarkPropertyTextDirty();
		SetBarrierRange(propTemplate->BarrierRange);
		SetFieldButtonSize(propTemplate->FieldButtonSize);
	}
}

bool EditPropertyComponent::AcceptsInputEvent (EInputEvent inputEvent, const sf::Event& evnt) const
{
	return (!IsReadOnly() && Super::AcceptsInputEvent(inputEvent, evnt));
}

void EditPropertyComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (BarrierTransform.IsNullptr())
	{
		return;
	}

	//Check if this needs to overwrite the mouse icon
	if (!bIsReplacingMouseIcon && BarrierTransform->IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y)))
	{
		//Is now hovering over barrier when it wasn't before.
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		Texture* dragBarrierIcon = localTexturePool->EditTexture(HashedString("Engine.Input.CursorResizeHorizontal"));
		if (dragBarrierIcon != nullptr)
		{
			bIsReplacingMouseIcon = true;
			mouse->PushMouseIconOverride(dragBarrierIcon, SDFUNCTION_2PARAM(this, EditPropertyComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}
	}

	if (!bGrabbingBarrier || deltaMove.X == 0.f || ReadCachedAbsSize().X == 0.f)
	{
		return;
	}

	Vector2 newPos = Vector2(sfmlEvent.x, 0.f);

	//Convert newPos from abs to normalized coordinates
	newPos.X -= ReadCachedAbsPosition().X;
	newPos.X /= ReadCachedAbsSize().X;
	newPos.X = BarrierRange.GetClampedValue(newPos.X);
	newPos.X -= (BarrierTransform->ReadSize().X * 0.5f);
	BarrierTransform->SetPosition(newPos);
	UpdatePropertyHorizontalTransforms();
}

void EditPropertyComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//Listen for mouse release event
	if (bGrabbingBarrier && sfmlEvent.button == sf::Mouse::Button::Left && eventType == sf::Event::MouseButtonReleased)
	{
		ReleaseBarrier();
	}
}

bool EditPropertyComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (bGrabbingBarrier || sfmlEvent.button != sf::Mouse::Button::Left || eventType != sf::Event::MouseButtonPressed)
	{
		return false;
	}

	//Check if click on the barrier
	if (BarrierTransform.IsValid() && BarrierTransform->IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y)))
	{
		GrabBarrier();
		return true;
	}

	return false;
}

void EditPropertyComponent::Destroyed ()
{
	if (bIsReplacingMouseIcon)
	{
		InputEngineComponent* localInputEngine = InputEngineComponent::Find();
		if (localInputEngine != nullptr && localInputEngine->GetMouse() != nullptr)
		{
			localInputEngine->GetMouse()->RemoveIconOverride(SDFUNCTION_2PARAM(this, EditPropertyComponent, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			bIsReplacingMouseIcon = false;
		}
	}

	Super::Destroyed();
}

bool EditPropertyComponent::AddComponent_Implementation (EntityComponent* newComponent)
{
	if (!Super::AddComponent_Implementation(newComponent))
	{
		return false;
	}

	if (EditPropertyComponent* subProp = dynamic_cast<EditPropertyComponent*>(newComponent))
	{
		SubPropertyComponents.push_back(subProp);

		subProp->SetAnchorLeft(SubPropIndent);
		subProp->SetAnchorRight(0.f);
	}

	return true;
}

bool EditPropertyComponent::RemoveComponent (EntityComponent* target)
{
	if (!Super::RemoveComponent(target))
	{
		return false;
	}

	if (EditPropertyComponent* subProp = dynamic_cast<EditPropertyComponent*>(target))
	{
		ContainerUtils::RemoveItem(OUT SubPropertyComponents, subProp);
	}

	if (ButtonComponent* button = dynamic_cast<ButtonComponent*>(target))
	{
		size_t removedIdx = ContainerUtils::RemoveItem(OUT FieldControls, button);
		if (removedIdx != UINT_INDEX_NONE)
		{
			for (size_t i = removedIdx; i < FieldControls.size(); ++i)
			{
				FieldControls.at(i)->SetAnchorRight(Float(i) * FieldButtonSize.X);
			}
		}
	}

	return true;
}

void EditPropertyComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	MarkVerticalSpaceDirty();
}

void EditPropertyComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	if (!bInitPropName)
	{
		return;
	}

	BarrierTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(BarrierTransform))
	{
		BarrierTransform->SetSize(Vector2(0.025f, 1.f));
		BarrierTransform->SetPosition(Vector2(0.5f - (BarrierTransform->ReadSize().X * 0.5f), 0.f));
		BarrierTransform->OnTransformChanged.RegisterHandler(SDFUNCTION(this, EditPropertyComponent, HandleBarrierTransformChanged, void));

		BarrierComponent = ColorRenderComponent::CreateObject();
		if (BarrierTransform->AddComponent(BarrierComponent))
		{
			BarrierComponent->SolidColor = Color(32, 32, 32);
			BarrierComponent->SetShape(ColorRenderComponent::ST_Rectangle);
		}
	}

	CHECK(BarrierTransform.IsValid())

	PropertyName = LabelComponent::CreateObject();
	if (AddComponent(PropertyName))
	{
		PropertyName->SetAutoRefresh(false);
		PropertyName->SetPosition(Vector2::ZERO_VECTOR);
		PropertyName->SetSize(Vector2(BarrierTransform->ReadPosition().X - (BarrierTransform->ReadSize().X * 0.5f), 1.f));
		PropertyName->SetWrapText(false);
		PropertyName->SetClampText(true);
		PropertyName->SetVerticalAlignment(LabelComponent::VA_Center);
		if (TextRenderComponent* textRender = PropertyName->GetRenderComponent())
		{
			textRender->SetFontColor(IsReadOnly() ? ReadOnlyFontColor.Source : NormalFontColor.Source);
		}

		PropertyName->SetAutoRefresh(true);
	}
}

void EditPropertyComponent::PostInitPropertyComponent ()
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->PostInitPropertyComponent();
	}
}

ButtonComponent* EditPropertyComponent::AddFieldControl ()
{
	ButtonComponent* newButton = ButtonComponent::CreateObject();
	if (AddComponent(newButton))
	{
		newButton->SetAnchorRight(Float(FieldControls.size()) * FieldButtonSize.X);
		newButton->SetSize(FieldButtonSize);
		if (newButton->GetCaptionComponent() != nullptr)
		{
			newButton->GetCaptionComponent()->Destroy();
		}

		FieldControls.push_back(newButton);
	}

	return newButton;
}

void EditPropertyComponent::CopyToBuffer (DataBuffer& copyTo) const
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->CopyToBuffer(OUT copyTo);
	}
}

bool EditPropertyComponent::CopyFromBuffer (const DataBuffer& incomingData)
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		if (!subProp->CopyFromBuffer(incomingData))
		{
			return false;
		}
	}

	return true;
}

void EditPropertyComponent::BindToLocalVariable ()
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->BindToLocalVariable();
	}
}

void EditPropertyComponent::UpdatePropertyHorizontalTransforms ()
{
	if (PropertyName.IsValid() && BarrierTransform.IsValid())
	{
		Float newSizeX = BarrierTransform->ReadPosition().X - (BarrierTransform->ReadSize().X * 0.5f);
		PropertyName->SetSize(newSizeX, PropertyName->ReadSize().Y);
	}

	//Move the barrier transforms for all sub properties
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		if (!subProp->IsVisible())
		{
			continue;
		}

		if (PlanarTransformComponent* subBarrier = subProp->GetBarrierTransform())
		{
			Float parentBarrier(BarrierTransform->ReadPosition().X);
			if (parentBarrier <= 1.f && BarrierTransform->GetEnableFractionScaling())
			{
				//Convert from normalized to pixels
				parentBarrier *= ReadCachedAbsSize().X;
			}

			Float subPos(subProp->GetAnchorLeftDist());

			//Update the barrier so that it aligns with the parent barrier regardless of size difference and indentation.
			Float newPos = (parentBarrier - subPos) / subProp->ReadCachedAbsSize().X;
			subBarrier->EditPosition().X = subProp->BarrierRange.GetClampedValue(newPos);
			subProp->UpdatePropertyHorizontalTransforms();
		}
	}
}

Float EditPropertyComponent::UpdatePropertyVerticalTransforms ()
{
	if (ResizeTick != nullptr)
	{
		ResizeTick->SetTicking(false);
	}

	Float totalSize = GetVerticalSpace();
	if (totalSize <= 0.f)
	{
		//Must be collapsed or invisible
		return totalSize;
	}

	EditSize().Y = totalSize;

	if (PropertyName.IsValid())
	{
		PropertyName->EditSize().Y = LineHeight * LineHeightMultiplier;
	}

	if (BarrierTransform.IsValid())
	{
		BarrierTransform->EditSize().Y = LineHeight * LineHeightMultiplier;
	}

	//Update subcomponent translations (this should handle collapsing and expanding).
	Float vertPos(FirstSubPropPos);
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->EditPosition().Y = vertPos;
		vertPos += subProp->UpdatePropertyVerticalTransforms();
	}

	return totalSize;
}

void EditPropertyComponent::MarkVerticalSpaceDirty ()
{
	//Only the root property component needs to be marked for dirty since calculating the vertical space is recursive and the size of the child prop affects the vertical size of the root.
	EditPropertyComponent* rootProp = GetRootEditableProperty();
	if (rootProp != this)
	{
		rootProp->MarkVerticalSpaceDirty();
		return;
	}

	VerticalSpace = -1.f;
	if (ResizeTick != nullptr)
	{
		ResizeTick->SetTicking(true);
	}
}

void EditPropertyComponent::MarkPropertyTextDirty ()
{
	bPropertyTextChanged = true;
	CachedValueAsText = DString::EmptyString;
}

void EditPropertyComponent::SetBarrierPosition (Float barrierPercent)
{
	if (BarrierTransform.IsValid())
	{
		barrierPercent = BarrierRange.GetClampedValue(barrierPercent);
		BarrierTransform->EditPosition().X = barrierPercent;
		UpdatePropertyHorizontalTransforms();
	}
}

DString EditPropertyComponent::GetValueAsText () const
{
	if (bPropertyTextChanged)
	{
		CachedValueAsText = ConstructValueAsText();
		bPropertyTextChanged = false;
	}
	
	return CachedValueAsText;
}

size_t EditPropertyComponent::FindSelfFromOwnerSubProperties () const
{
	EditPropertyComponent* editCompOwner = dynamic_cast<EditPropertyComponent*>(GetOwner());
	if (editCompOwner == nullptr)
	{
		return INDEX_NONE;
	}

	for (size_t i = 0; i < editCompOwner->ReadSubPropertyComponents().size(); ++i)
	{
		if (editCompOwner->ReadSubPropertyComponents().at(i) == this)
		{
			return i;
		}
	}

	return INDEX_NONE;
}

EditPropertyComponent* EditPropertyComponent::FindSubPropWithMatchingId (Int propId, bool bRecursiveSearch) const
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		if (subProp->GetPropertyId() == propId)
		{
			return subProp;
		}

		if (bRecursiveSearch)
		{
			if (EditPropertyComponent* result = subProp->FindSubPropWithMatchingId(propId, bRecursiveSearch))
			{
				return result;
			}
		}
	}

	return nullptr;
}

void EditPropertyComponent::RemoveVarUpdateTickComponent ()
{
	//Noop
}

void EditPropertyComponent::ResetToDefaults ()
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->ResetToDefaults();
	}
}

void EditPropertyComponent::UnbindVariable ()
{
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->UnbindVariable();
	}
}

void EditPropertyComponent::ShowCaption (const DString& text, const SCaptionColors& captionColors)
{
	if (CaptionBackground == nullptr)
	{
		CaptionBackground = ColorRenderComponent::CreateObject();
		if (AddComponent(CaptionBackground))
		{
			//Move the caption background to the front of the component list so it's rendered behind the other components.
			MoveComponentToFront(CaptionBackground);
			MarkVerticalSpaceDirty();
		}
	}

	CHECK(CaptionBackground != nullptr)
	CaptionBackground->SolidColor = captionColors.Background;

	if (CaptionComponent == nullptr)
	{
		CaptionComponent = LabelComponent::CreateObject();
		if (AddComponent(CaptionComponent))
		{
			CaptionComponent->SetAutoRefresh(false);
			CaptionComponent->SetAnchorBottom(6.f);
			CaptionComponent->SetAnchorLeft(12.f);
			CaptionComponent->SetAnchorRight(12.f);
			CaptionComponent->SetSize(Vector2(1.f, LineHeight));
			CaptionComponent->SetWrapText(true);
			CaptionComponent->SetClampText(true);
			CaptionComponent->SetCharacterSize((LineHeight * 0.475f).ToInt());
			CaptionComponent->SetLineSpacing(0.f);
			CaptionComponent->SetHorizontalAlignment(LabelComponent::HA_Center);
			CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Bottom);
			CaptionComponent->SetAutoRefresh(true);
		}
	}

	CHECK(CaptionComponent != nullptr)
	if (TextRenderComponent* renderComp = CaptionComponent->GetRenderComponent())
	{
		renderComp->SetFontColor(captionColors.TextColor.Source);
	}

	CaptionComponent->SetText(text);
}

void EditPropertyComponent::ShowCaption (const DString& text, const SD::Color& textColor, const SD::Color& backgroundColor)
{
	ShowCaption(text, SCaptionColors(textColor, backgroundColor));
}

void EditPropertyComponent::HideCaption ()
{
	if (CaptionBackground != nullptr)
	{
		CaptionBackground->Destroy();
		CaptionComponent->Destroy();
		CaptionBackground = nullptr;
		CaptionComponent = nullptr;
		MarkVerticalSpaceDirty();
	}
}

void EditPropertyComponent::SetPropertyId (Int newPropertyId)
{
	PropertyId = newPropertyId;
}

void EditPropertyComponent::SetNormalFontColor (Color newNormalFontColor)
{
	NormalFontColor = newNormalFontColor;

	if (PropertyName.IsValid() && PropertyName->GetRenderComponent() != nullptr && !IsReadOnly())
	{
		PropertyName->GetRenderComponent()->SetFontColor(NormalFontColor.Source);
	}

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetNormalFontColor(newNormalFontColor);
	}
}

void EditPropertyComponent::SetReadOnlyFontColor (Color newReadOnlyFontColor)
{
	ReadOnlyFontColor = newReadOnlyFontColor;

	if (PropertyName.IsValid() && PropertyName->GetRenderComponent() != nullptr && IsReadOnly())
	{
		PropertyName->GetRenderComponent()->SetFontColor(ReadOnlyFontColor.Source);
	}

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetReadOnlyFontColor(ReadOnlyFontColor);
	}
}

void EditPropertyComponent::SetCaptionErrorColors (const SCaptionColors& newCaptionErrorColors)
{
	CaptionErrorColors = newCaptionErrorColors;

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetCaptionErrorColors(CaptionErrorColors);
	}
}

void EditPropertyComponent::SetCaptionInfoColors (const SCaptionColors& newCaptionInfoColors)
{
	CaptionInfoColors = newCaptionInfoColors;

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetCaptionInfoColors(CaptionInfoColors);
	}
}

void EditPropertyComponent::SetReadOnly (bool bNewReadOnly)
{
	if (bReadOnly == bNewReadOnly)
	{
		return;
	}

	bReadOnly = bNewReadOnly;
	if (PropertyName.IsValid() && PropertyName->GetRenderComponent() != nullptr)
	{
		PropertyName->GetRenderComponent()->SetFontColor(IsReadOnly() ? ReadOnlyFontColor.Source : NormalFontColor.Source);
	}

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetReadOnly(IsReadOnly());
	}
}

void EditPropertyComponent::SetLineHeight (Float newLineHeight)
{
	LineHeight = newLineHeight;
	FieldButtonSize.Y = LineHeight;
	MarkVerticalSpaceDirty();

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetLineHeight(LineHeight);
	}
}

void EditPropertyComponent::SetSubPropIndent (Float newSubPropIndent)
{
	if (SubPropIndent == newSubPropIndent)
	{
		return;
	}

	SubPropIndent = newSubPropIndent;

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetSubPropIndent(SubPropIndent);
	}

	for (EditPropertyComponent* subComp : SubPropertyComponents)
	{
		subComp->SetAnchorLeft(SubPropIndent);
	}
}

void EditPropertyComponent::SetBarrierRange (const Range<Float>& newBarrierRange)
{
	BarrierRange = newBarrierRange;
	if (BarrierTransform.IsValid())
	{
		Float clampedPos = BarrierRange.GetClampedValue(BarrierTransform->ReadPosition().X);
		clampedPos -= (BarrierTransform->ReadSize().X * 0.5f);
		if (clampedPos != BarrierTransform->ReadPosition().X)
		{
			BarrierTransform->SetPosition(Vector2(clampedPos, BarrierTransform->ReadPosition().Y));
		}
	}
}

void EditPropertyComponent::SetFieldButtonSize (const Vector2& newFieldButtonSize)
{
	FieldButtonSize = newFieldButtonSize;

	for (size_t i = 0; i < FieldControls.size(); ++i)
	{
		FieldControls.at(i)->SetSize(FieldButtonSize);
		FieldControls.at(i)->SetAnchorRight(Float(i) * FieldButtonSize.X);
	}
}

Float EditPropertyComponent::GetVerticalSpace () const
{
	if (VerticalSpace < 0.f)
	{
		VerticalSpace = CalcVerticalSpace(true);
	}

	return VerticalSpace;
}

EditPropertyComponent::SInspectorProperty* EditPropertyComponent::CreateDefaultInspector (const DString& propName) const
{
	return nullptr;
}

GuiComponent* EditPropertyComponent::GetComponentTooltipsShouldAttachTo ()
{
	return this;
}

EditPropertyComponent* EditPropertyComponent::GetRootEditableProperty ()
{
	EditPropertyComponent* result = this;
	while (EditPropertyComponent* ownerComp = dynamic_cast<EditPropertyComponent*>(result->GetOwner()))
	{
		result = ownerComp;
	}

	return result;
}

void EditPropertyComponent::GrabBarrier ()
{
	bGrabbingBarrier = true;
}

void EditPropertyComponent::ReleaseBarrier ()
{
	bGrabbingBarrier = false;
}

Float EditPropertyComponent::CalcVerticalSpace (bool includeSubComp) const
{
	if (!IsVisible())
	{
		return 0.f;
	}

	Float result(LineHeight * LineHeightMultiplier);
	if (includeSubComp)
	{
		for (EditPropertyComponent* subComp : SubPropertyComponents)
		{
			Float vertSpace = subComp->CalcVerticalSpace(true);
			subComp->VerticalSpace = vertSpace;
			result += vertSpace;
		}
	}

	if (CaptionBackground != nullptr)
	{
		result += LineHeight;
	}

	return result;
}

DString EditPropertyComponent::ConstructValueAsText () const
{
	return DString::EmptyString;
}

void EditPropertyComponent::ProcessApplyEdit (EditPropertyComponent* editedComp)
{
	if (OnApplyEdit.IsBounded())
	{
		OnApplyEdit.Execute(editedComp);
	}

	if (EditPropertyComponent* owningProp = dynamic_cast<EditPropertyComponent*>(GetOwner()))
	{
		owningProp->ProcessApplyEdit(editedComp);
	}
}

void EditPropertyComponent::HandleBarrierTransformChanged ()
{
	if (PropertyName.IsValid() && BarrierTransform.IsValid())
	{
		PropertyName->SetSize(Vector2(BarrierTransform->ReadPosition().X, PropertyName->ReadSize().Y));
	}
}

bool EditPropertyComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvent)
{
	bool bResult = (bGrabbingBarrier || (BarrierTransform.IsValid() && BarrierTransform->IsWithinBounds(Vector2(Float(mouseEvent.x), Float(mouseEvent.y)))));
	if (!bResult)
	{
		bIsReplacingMouseIcon = false;
	}

	return bResult;
}

void EditPropertyComponent::HandleResizeTick (Float deltaSec)
{
	UpdatePropertyVerticalTransforms();
}
SD_END