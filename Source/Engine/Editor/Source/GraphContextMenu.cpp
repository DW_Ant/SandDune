/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphFunction.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "ExecNode.h"
#include "FunctionNode.h"
#include "GraphAsset.h"
#include "GraphContextMenu.h"
#include "GraphFunction.h"
#include "GraphEditor.h"
#include "GraphVariable.h"
#include "VariableNode.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::GraphContextMenu, SD::DynamicListMenu)
SD_BEGIN

const DString GraphContextMenu::SECTION_NAME(TXT("GraphContextMenu"));

std::vector<GraphFunction*> GraphContextMenu::AllGetFunctions;
std::vector<GraphFunction*> GraphContextMenu::AllSetFunctions;
std::vector<GraphFunction*> GraphContextMenu::AllEditFunctions;

GraphContextMenu::SCmdData::SCmdData () :
	Func(nullptr)
{
	//Noop
}

GraphContextMenu::SCmdData::SCmdData (GraphFunction* inFunc) :
	Func(inFunc)
{
	//Noop
}

GraphContextMenu::SCmdData::SCmdData (const DString& inVarId, GraphFunction* inFunc) :
	VarId(inVarId),
	Func(inFunc)
{
	//Noop
}

void GraphContextMenu::InitProps ()
{
	Super::InitProps();

	PlacementLocation = Vector2::ZERO_VECTOR;
	OwningEditor = nullptr;
	SearchField = nullptr;
	SearchResults = nullptr;
	SearchResultScroll = nullptr;
}

void GraphContextMenu::BeginObject ()
{
	Super::BeginObject();

	PopulateVarRelatedClasses();
	PopulateVarAccessFunctions();
}

void GraphContextMenu::InitializeList (ContextMenuComponent* contextComp, MousePointer* mouse, const std::vector<SInitMenuOption>& inOptions)
{
	Super::InitializeList(contextComp, mouse, inOptions);

	if (ContainerUtils::IsEmpty(VisibleMenus))
	{
		return;
	}

	FrameComponent* rootFrame = VisibleMenus.at(0)->Frame;
	CHECK(rootFrame != nullptr)

	//Snap the search results and search field to the top of the root menu.
	if (SearchField != nullptr)
	{
		SearchField->SetPosition(rootFrame->ReadPosition());
		SearchField->EditPosition().Y -= SearchField->ReadSize().Y;
		SearchField->EditSize().X = rootFrame->ReadSize().X;

		//If off screen, move the search field to the top of the screen, shift the root frame to be below the search field.
		if (SearchField->ReadPosition().Y < 0.f)
		{
			SearchField->EditPosition().Y = 0.f;
			rootFrame->EditPosition().Y = SearchField->ReadSize().Y;
		}

		if (SearchResultScroll != nullptr)
		{
			SearchResultScroll->SetPosition(rootFrame->ReadPosition());
			SearchResultScroll->SetSize(rootFrame->ReadSize());
			
			Float minSize = 128.f;
			if (SearchResultScroll->ReadSize().Y < minSize)
			{
				SearchResultScroll->EditSize().Y = minSize;
			}
		}
	}
}

void GraphContextMenu::ConstructUI ()
{
	Super::ConstructUI();

	SearchField = TextFieldComponent::CreateObject();
	if (AddComponent(SearchField))
	{
		SearchField->EditSize().Y = SearchField->GetCharacterSize().ToFloat();
		SearchField->SetAutoRefresh(false);
		SearchField->SetSingleLine(true);
		SearchField->bAutoSelect = true;
		SearchField->SetEnableFractionScaling(false);
		SearchField->SetCursorPosition(0);
		SearchField->OnTextChanged = SDFUNCTION_1PARAM(this, GraphContextMenu, HandleSearchEdit, void, TextFieldComponent*);
		SearchField->OnReturn = SDFUNCTION_1PARAM(this, GraphContextMenu, HandleSearchReturn, void, TextFieldComponent*);
		SearchField->SetAutoRefresh(true);

		if (FrameComponent* background = SearchField->GetBackgroundComponent())
		{
			if (BorderRenderComponent* borderComp = background->GetBorderComp())
			{
				borderComp->Destroy();
			}

			background->SetBorderThickness(0.f);
		}
	}

	SearchResultScroll = ScrollbarComponent::CreateObject();
	if (AddComponent(SearchResultScroll))
	{
		SearchResultScroll->SetVisibility(false);
		SearchResultScroll->SetEnableFractionScaling(false);
		SearchResultScroll->SetHideControlsWhenFull(true);
		SearchResultScroll->SetZoomEnabled(false);

		if (RenderTexture* background = SearchResultScroll->GetFrameTexture())
		{
			background->ResetColor = Color::INVISIBLE;
		}

		GuiEntity* subMenu = GuiEntity::CreateObject();
		subMenu->SetGuiSizeToOwningScrollbar(Vector2(-1.f, 1.f));
		subMenu->SetAutoSizeVertical(true);
		SearchResultScroll->SetViewedObject(subMenu);

		SearchResults = ListBoxComponent::CreateObject();
		if (subMenu->AddComponent(SearchResults))
		{
			SearchResults->SetPosition(Vector2::ZERO_VECTOR);
			SearchResults->SetSize(Vector2(1.f, 32.f));
			SearchResults->SetAutoDeselect(true);
			SearchResults->SetAutoSizeVertically(true);
			SearchResults->OnItemSelected = SDFUNCTION_1PARAM(this, GraphContextMenu, HandleItemSelected, void, Int);
		}
	}
}

void GraphContextMenu::BeginFadeOut ()
{
	if (OnGraphContextClosing.IsBounded() && OnGraphContextClosing())
	{
		OnGraphContextClosing.ClearFunction();
	}

	Super::BeginFadeOut();
}

void GraphContextMenu::Destroyed ()
{
	if (OnGraphContextClosing.IsBounded() && OnGraphContextClosing())
	{
		OnGraphContextClosing.ClearFunction();
	}

	Super::Destroyed();
}

void GraphContextMenu::InitGraphContextFromVar (ContextMenuComponent* contextComp, MousePointer* mouse, GraphEditor* inOwningEditor)
{
	if (inOwningEditor == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize GraphContext from a variable since the GraphEditor instance was not given."));
		return;
	}

	VariablePort* connectVar = dynamic_cast<VariablePort*>(inOwningEditor->GetDraggedPort());
	if (connectVar == nullptr)
	{
		InitGraphContext(contextComp, mouse, inOwningEditor);
		return;
	}

	OwningEditor = inOwningEditor;
	PlacementLocation = mouse->ReadPosition();
	std::vector<DynamicListMenu::SInitMenuOption> initOptions;

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	const DString& fileName = EditorEngineComponent::LOCALIZATION_FILE_NAME;

	GraphFunction* localVarFunction = nullptr; //Used to identify if it should create a getter or a setter function based on the direction of the graph variable.
	bool isInputVar = connectVar->GetPortType() == NodePort::PT_Input;
	if (isInputVar)
	{
		//If the dragged port is an input var, then this should create a getter function.
		localVarFunction = FindFunction(connectVar->GetVarType(), PD_Output, AllGetFunctions);
	}
	else
	{
		//If the dragged port is an output var, then this should create a setter function to initialize this local variable.
		localVarFunction = FindFunction(connectVar->GetVarType(), PD_Input, AllSetFunctions);
	}

	if (localVarFunction != nullptr)
	{
		//Add command to create a local variable
		DynamicListMenu::SInitMenuOption& createLocalVar = initOptions.emplace_back(translator->TranslateText(TXT("CreateLocalVariable"), fileName, SECTION_NAME), SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateLocalVar, void, BaseGuiDataElement*));
		createLocalVar.Data = new GuiDataElement<SCmdData>(SCmdData(localVarFunction), DString::EmptyString);
	}

	EPortDirection portDir = (isInputVar) ? PD_Output : PD_Input;
	AddLocalVariables(translator, connectVar->GetVarType(), portDir, OUT initOptions);

	DString headerText = translator->TranslateText(TXT("VariableFunctions"), fileName, SECTION_NAME) + TXT(">");
	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	
	for (const HashedString& assetName : VarRelatedClasses)
	{
		if (!editorEngine->ReadLoadedAssets().contains(assetName))
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to find variable-related functions from %s since that asset is not found."), assetName);
			continue;
		}

		//Compare, Convert, etc...
		if (GraphAsset* asset = editorEngine->ReadLoadedAssets().at(assetName))
		{
			for (GraphFunction* func : asset->ReadMemberFunctions())
			{
				if (!func->IsGlobal())
				{
					continue;
				}

				if (func->GetUniqueFlag() != GraphFunction::UF_None)
				{
					//Getters and setters are presented somewhere else in the context menu. Must reference a member or local variable instead to access these functions.
					continue;
				}

				const std::vector<GraphVariable*>* paramList = nullptr;
				if (isInputVar)
				{
					//If the dragged port is an input var, then this can only connect to a function with an output.
					paramList = &func->ReadOutParams();
				}
				else
				{
					//If the dragged port is an output var, then this can only connect to a function with an input.
					paramList = &func->ReadInParams();
				}

				//The first parameter must match the port type. For example don't want StringCompare to show up for a bool port only because it has a IsSensitive parameter.
				if (paramList->at(0) == nullptr || ContainerUtils::IsEmpty(*paramList) || paramList->at(0)->GetVarType() != connectVar->GetVarType())
				{
					continue;
				}

				if (func != nullptr && func->IsVariableRelatedFunction())
				{
					DString displayText = (!func->ReadDisplayName().IsEmpty()) ? func->GetDisplayName() : func->GetFunctionName();
					DString escapedDisplayedText = GetEscapedMenuText(displayText);

					DynamicListMenu::SInitMenuOption& varFunc = initOptions.emplace_back(headerText + escapedDisplayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateFunctionNode, void, BaseGuiDataElement*));
					varFunc.Data = new GuiDataElement<SCmdData>(SCmdData(func), displayText);
				}
			}
		}
	}

	if (GraphFunction* constructedFunc = OwningEditor->GetOwningFunction())
	{
		if (GraphAsset* owningAsset = constructedFunc->GetOwningAsset())
		{
			if (connectVar->GetVarType() == ScriptVariable::VT_Obj || connectVar->GetVarType() == ScriptVariable::VT_Class)
			{
				GraphAsset* draggedAsset = connectVar->GetClassConstraint();
				if (draggedAsset != nullptr && draggedAsset != owningAsset)
				{
					initOptions.emplace_back(DynamicListMenu::DIVIDER);

					//Add options from dragged asset
					SFunctionQuery filter;
					filter.bConstOnly = constructedFunc->IsConst();
					filter.bStaticOnly = (connectVar->GetVarType() == ScriptVariable::VT_Class);
					filter.bPublicOnly = !draggedAsset->IsParentOf(owningAsset);
					AddFunctionOptions(translator, draggedAsset, filter, OUT initOptions);
				}
			}

			//Add options for 'self'
			SFunctionQuery filter;
			filter.bConstOnly = constructedFunc->IsConst(); //Const functions can only call other const functions.
			filter.bStaticOnly = constructedFunc->IsStatic(); //Static functions can only call other static functions from self
			filter.bPublicOnly = false; //Protected functions are available within self.
			AddFunctionOptions(translator, owningAsset, filter, OUT initOptions);
		}
	}

	if (!ContainerUtils::IsEmpty(initOptions))
	{
		//Create a divider between the context-related options & the common options
		initOptions.emplace_back(DynamicListMenu::DIVIDER);
	}

	AddGlobalFunctions(translator, OUT initOptions);
	AddEditorFunctions(translator, OUT initOptions);

	InitializeList(contextComp, mouse, initOptions);
}

void GraphContextMenu::InitGraphContext (ContextMenuComponent* contextComp, MousePointer* mouse, GraphEditor* inOwningEditor)
{
	if (inOwningEditor != nullptr && dynamic_cast<VariablePort*>(inOwningEditor->GetDraggedPort()) != nullptr)
	{
		InitGraphContextFromVar(contextComp, mouse, inOwningEditor);
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	OwningEditor = inOwningEditor;
	PlacementLocation = mouse->ReadPosition();
	std::vector<DynamicListMenu::SInitMenuOption> initOptions;

	//List all local variables
	AddLocalVariables(translator, ScriptVariable::VT_Unknown, PD_Any, OUT initOptions);

	//Add options for self
	if (GraphFunction* constructedFunc = OwningEditor->GetOwningFunction())
	{
		if (GraphAsset* owningAsset = constructedFunc->GetOwningAsset())
		{
			SFunctionQuery filter;
			filter.bConstOnly = constructedFunc->IsConst(); //Const functions can only call other const functions.
			filter.bStaticOnly = constructedFunc->IsStatic(); //Static functions can only call other static functions from self
			filter.bPublicOnly = false; //Protected functions are available within self.
			AddFunctionOptions(translator, owningAsset, filter, OUT initOptions);
		}
	}

	if (!ContainerUtils::IsEmpty(initOptions))
	{
		//Create a divider between the context-related options & the common options
		initOptions.emplace_back(DynamicListMenu::DIVIDER);
	}

	AddGlobalFunctions(translator, OUT initOptions);
	AddEditorFunctions(translator, OUT initOptions);

	InitializeList(contextComp, mouse, initOptions);
}

void GraphContextMenu::PopulateVarRelatedClasses ()
{
	VarRelatedClasses.push_back(HashedString("Core"));
	VarRelatedClasses.push_back(HashedString("Math"));
}

void GraphContextMenu::PopulateVarAccessFunctions ()
{
	if (ContainerUtils::IsEmpty(AllGetFunctions))
	{
		EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
		CHECK(editorEngine != nullptr)

		for (const HashedString& className : VarRelatedClasses)
		{
			if (!editorEngine->ReadLoadedAssets().contains(className))
			{
				continue;
			}

			if (GraphAsset* asset = editorEngine->ReadLoadedAssets().at(className))
			{
				if (!asset->IsGlobalAsset())
				{
					continue;
				}

				for (GraphFunction* func : asset->ReadMemberFunctions())
				{
					if (!func->IsGlobal())
					{
						continue;
					}

					switch(func->GetUniqueFlag())
					{
						case(GraphFunction::UF_Getter):
							AllGetFunctions.push_back(func);
							break;

						case(GraphFunction::UF_Setter):
							AllSetFunctions.push_back(func);
							break;

						case(GraphFunction::UF_Edit):
							AllEditFunctions.push_back(func);
							break;
					}
				}
			}
		}
	}
	else if (ContainerUtils::IsEmpty(VarRelatedClasses))
	{
		//PopulateVarRelatedClasses is empty, the core functions must have been destroyed/unregistered. Should empty the vectors since the static vectors are pointing to garbage.
		ContainerUtils::Empty(OUT AllGetFunctions);
		ContainerUtils::Empty(OUT AllSetFunctions);
		ContainerUtils::Empty(OUT AllEditFunctions);
	}
}

DString GraphContextMenu::GetEscapedMenuText (const DString& originalText) const
{
	DString result = DString::Replace(originalText, TXT(">"), TXT("\\>"), DString::CC_CaseSensitive);
	if (result.EndsWith('>') && result.Length() >= 2)
	{
		//Remove the last escape character since the context menu component would not interpret that last character as a sub menu.
		result.Remove(result.Length() - 2, 1);
	}

	if (result.Compare(TXT(">"), DString::CC_CaseSensitive) == 0)
	{
		//Edge case: any functions with the display name exactly equal to '>' must have a space character in front of it. Otherwise, the context menu would interpret the '>' to be in the parent menu instead of the sub menu.
		result.Insert(0, ' ');
	}

	return result;
}

void GraphContextMenu::AddLocalVariables (TextTranslator* translator, ScriptVariable::EVarType restrictedVarType, EPortDirection portDir, std::vector<DynamicListMenu::SInitMenuOption>& outOptions)
{
	DString contextHeader = translator->TranslateText(TXT("LocalVariables"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);
	CHECK(OwningEditor != nullptr)

	if (GraphFunction* owningFunc = OwningEditor->GetOwningFunction())
	{
		std::vector<GraphFunction::SVarInfo> allLocalVars;
		owningFunc->FindLocalVariables(restrictedVarType, OUT allLocalVars);

		for (const GraphFunction::SVarInfo& varInfo : allLocalVars)
		{
			//Add getter if there is one
			if (GraphFunction* getter = FindFunction(varInfo.VarType, portDir, AllGetFunctions))
			{
				DString displayedText = varInfo.VarDisplay + TXT(">") + getter->GetPresentedName();
				SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + displayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateLocalVar, void, BaseGuiDataElement*));
				newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varInfo.VarDisplay, getter), displayedText);
			}

			//Add setter if there is one
			if (GraphFunction* setter = FindFunction(varInfo.VarType, portDir, AllSetFunctions))
			{
				DString displayedText = varInfo.VarDisplay + TXT(">") + setter->GetPresentedName();
				SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + displayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateLocalVar, void, BaseGuiDataElement*));
				newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varInfo.VarDisplay, setter), displayedText);
			}

			//Add Edit if there is one
			if (GraphFunction* editor = FindFunction(varInfo.VarType, portDir, AllEditFunctions))
			{
				DString displayedText = varInfo.VarDisplay + TXT(">") + editor->GetPresentedName();
				SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + displayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateLocalVar, void, BaseGuiDataElement*));
				newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varInfo.VarDisplay, editor), displayedText);
			}
		}
	}
}

void GraphContextMenu::AddFunctionOptions (TextTranslator* translator, GraphAsset* asset, const SFunctionQuery& filter, std::vector<DynamicListMenu::SInitMenuOption>& outOptions)
{
	CHECK(translator != nullptr && asset != nullptr)

	EPortDirection portDir = PD_Any;
	bool isDraggingExecPort = false;
	ScriptVariable::EVarType restrictedVarType = ScriptVariable::VT_Unknown;
	GraphAsset* owningAsset = nullptr;
	if (GraphFunction* constructedFunc = OwningEditor->GetOwningFunction())
	{
		owningAsset = constructedFunc->GetOwningAsset();
	}

	if (asset == owningAsset) //If the asset is different, then allow any variable types and direction since the user is dragging a variable port of a different asset type.
	{
		if (NodePort* draggedPort = OwningEditor->GetDraggedPort())
		{
			portDir = (draggedPort->GetPortType() == NodePort::PT_Input) ? PD_Output : PD_Input;
			isDraggingExecPort = true;

			if (VariablePort* draggedVar = dynamic_cast<VariablePort*>(draggedPort))
			{
				restrictedVarType = draggedVar->GetVarType();
				isDraggingExecPort = false;
			}
		}
	}

	DString contextHeader = asset->GetAssetName();
	DString varSectionName = translator->TranslateText(TXT("Variables"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);
	DString functionSectionName = translator->TranslateText(TXT("Functions"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);
	for (GraphAsset* curAsset = asset; curAsset != nullptr; curAsset = curAsset->GetSuperAsset())
	{
		if (curAsset->GetLoadState() != GraphAsset::LS_Loaded)
		{
			curAsset->LoadFromFile();
		}

		Int varCounter = 0;

		//Add variable functions
		for (GraphVariable* var : curAsset->EditMemberVariables())
		{
			if (var == nullptr)
			{
				continue;
			}

			if (filter.bPublicOnly && var->IsProtected())
			{
				continue; //variable is not visible
			}

			if (filter.bStaticOnly && !var->IsStatic())
			{
				continue;
			}

			if (restrictedVarType != ScriptVariable::VT_Unknown && restrictedVarType != var->GetVarType())
			{
				continue;
			}

			DString varId = var->GetIdentifyingName();

			if (!isDraggingExecPort)
			{
				if (GraphFunction* getter = FindFunction(var->GetVarType(), portDir, AllGetFunctions))
				{
					DString displayedText = getter->GetPresentedName() + TXT(" ") + var->GetPresentedName();
					SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + varSectionName + TXT(">") + var->GetPresentedName() + TXT(">") + getter->GetPresentedName(), SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateVarAccessNode, void, BaseGuiDataElement*));
					newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varId, getter), displayedText);
				}
			}

			if (filter.bConstOnly)
			{
				continue; //Setters and editor functions are not available inside const functions when accessing member variables.
			}

			if (!isDraggingExecPort)
			{
				if (GraphFunction* setter = FindFunction(var->GetVarType(), portDir, AllSetFunctions))
				{
					DString displayedText = setter->GetPresentedName() + TXT(" ") + var->GetPresentedName();
					SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + varSectionName + TXT(">") + var->GetPresentedName() + TXT(">") + setter->GetPresentedName(), SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateVarAccessNode, void, BaseGuiDataElement*));
					newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varId, setter), displayedText);
				}
			}

			if (GraphFunction* editor = FindFunction(var->GetVarType(), portDir, AllEditFunctions))
			{
				DString displayedText = editor->GetPresentedName() + TXT(" ") + var->GetPresentedName();
				SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + varSectionName + TXT(">") + var->GetPresentedName() + TXT(">") + editor->GetPresentedName(), SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateVarAccessNode, void, BaseGuiDataElement*));
				newOption.Data = new GuiDataElement<SCmdData>(SCmdData(varId, editor), displayedText);
			}
		}

		for (GraphFunction* func : curAsset->ReadMemberFunctions())
		{
			if (filter.bStaticOnly && !func->IsStatic())
			{
				continue; //Static functions can only call other static functions
			}

			if (filter.bPublicOnly && func->IsProtected())
			{
				continue; //This function is invisible
			}

			if (filter.bConstOnly && !func->IsConst())
			{
				continue; //Const functions can only call other const functions
			}

			DString displayedText = func->GetPresentedName();
			DString escapedDisplayedText = GetEscapedMenuText(displayedText);

			DString functionCategory;
			if (!func->ReadContextMenuCategory().IsEmpty())
			{
				functionCategory = func->ReadContextMenuCategory() + TXT(">");
			}

			SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + functionSectionName + TXT(">") + functionCategory + escapedDisplayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateFunctionNode, void, BaseGuiDataElement*));
			newOption.Data = new GuiDataElement<SCmdData>(SCmdData(func), displayedText);
		}
	}
}

void GraphContextMenu::AddGlobalFunctions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions)
{
	DString contextHeader = translator->TranslateText(TXT("GlobalFunctions"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);

	//Iterate through each loaded asset, and if they have a global function, add it to this list.
	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	for (const std::pair<HashedString, GraphAsset*>& loadedAsset : editorEngine->ReadLoadedAssets())
	{
		size_t firstIdx = outOptions.size();
		for (GraphFunction* func : loadedAsset.second->ReadMemberFunctions())
		{
			if (!func->IsGlobal())
			{
				continue;
			}

			//If this function has a unique flag, then don't add it here since it resides somewhere else in the context menu.
			if (func->GetUniqueFlag() != GraphFunction::UF_None)
			{
				continue;
			}

			//Global function is found.
			DString displayedText = func->GetPresentedName();
			DString escapedDisplayedText = GetEscapedMenuText(displayedText);
			DString functionCategory;
			if (!func->ReadContextMenuCategory().IsEmpty())
			{
				functionCategory = func->ReadContextMenuCategory() + TXT(">");
			}

			SInitMenuOption& newOption = outOptions.emplace_back(contextHeader + TXT(">") + loadedAsset.second->GetPresentedName() + TXT(">") + functionCategory + escapedDisplayedText, SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateFunctionNode, void, BaseGuiDataElement*));
			newOption.Data = new GuiDataElement<SCmdData>(SCmdData(func), displayedText);
		}

		if (loadedAsset.second->IsGlobalAsset())
		{
			DisambiguateCmdOptions(translator, OUT outOptions, firstIdx, loadedAsset.second);
		}
	}
}

void GraphContextMenu::AddEditorFunctions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions)
{
	DString contextHeader(translator->TranslateText(TXT("EditorOperations"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME));

	outOptions.emplace_back(contextHeader + TXT(">") + translator->TranslateText(TXT("ReturnNode"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME), SDFUNCTION_1PARAM(this, GraphContextMenu, HandleCreateReturnNode, void, BaseGuiDataElement*));
}

void GraphContextMenu::DisambiguateCmdOptions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions, size_t startSearchFrom, GraphAsset* targetAsset)
{
	CHECK(targetAsset != nullptr)
	for (size_t firstIdx = startSearchFrom; firstIdx < outOptions.size(); ++firstIdx)
	{
		GuiDataElement<SCmdData>* firstData = dynamic_cast<GuiDataElement<SCmdData>*>(outOptions.at(firstIdx).Data);
		if (firstData == nullptr)
		{
			continue;
		}

		GraphFunction* firstFunc = firstData->Data.Func;
		if (firstFunc == nullptr)
		{
			continue;
		}

		GraphAsset* firstAsset = firstFunc->GetOwningAsset();
		if (firstAsset != targetAsset)
		{
			continue;
		}

		bool hasConflict = false;
		for (size_t secondIdx = startSearchFrom; secondIdx < outOptions.size(); ++secondIdx)
		{
			if (firstIdx == secondIdx)
			{
				continue;
			}

			GuiDataElement<SCmdData>* secondData = dynamic_cast<GuiDataElement<SCmdData>*>(outOptions.at(secondIdx).Data);
			if (secondData == nullptr)
			{
				continue;
			}

			GraphFunction* secondFunc = secondData->Data.Func;
			if (secondFunc == nullptr || secondFunc->GetOwningAsset() != targetAsset)
			{
				continue;
			}

			if (firstData->ReadLabelText().Compare(secondData->ReadLabelText(), DString::CC_CaseSensitive) != 0)
			{
				continue;
			}

			hasConflict = true;
			DString cmdSuffix = GetDisambiguationSuffix(translator, secondFunc);
			if (!cmdSuffix.IsEmpty())
			{
				if (secondData->LabelText.Compare(TXT(">"), DString::CC_CaseSensitive) == 0)
				{
					//Special case. Need to restore the escape character. Otherwise the context menu would move the suffix to a submenu.
					outOptions.at(secondIdx).FullCommandPath.PopBack();
					outOptions.at(secondIdx).FullCommandPath += TXT("\\> ") + cmdSuffix;
				}
				else
				{
					outOptions.at(secondIdx).FullCommandPath += TXT(" ") + cmdSuffix;
				}

				secondData->LabelText += TXT(" ") + cmdSuffix;
			}
		}

		if (hasConflict)
		{
			DString cmdSuffix = GetDisambiguationSuffix(translator, firstFunc);
			if (!cmdSuffix.IsEmpty())
			{
				if (firstData->LabelText.Compare(TXT(">"), DString::CC_CaseSensitive) == 0)
				{
					//Special case. Need to restore the escape character. Otherwise the context menu would move the suffix to a submenu.
					outOptions.at(firstIdx).FullCommandPath.PopBack();
					outOptions.at(firstIdx).FullCommandPath += TXT("\\> ") + cmdSuffix;
				}
				else
				{
					outOptions.at(firstIdx).FullCommandPath += TXT(" ") + cmdSuffix;
				}

				firstData->LabelText += TXT(" ") + cmdSuffix;
			}
		}
	}
}

DString GraphContextMenu::GetDisambiguationSuffix (TextTranslator* translator, GraphFunction* func) const
{
	CHECK(translator != nullptr && func != nullptr)

	ScriptVariable::EVarType paramType = ScriptVariable::VT_Unknown;
	if (!ContainerUtils::IsEmpty(func->ReadInParams()) && func->ReadInParams().at(0) != nullptr)
	{
		paramType = func->ReadInParams().at(0)->GetVarType();
	}
	else if (!ContainerUtils::IsEmpty(func->ReadOutParams()) && func->ReadOutParams().at(0) != nullptr)
	{
		paramType = func->ReadOutParams().at(0)->GetVarType();
	}

	if (paramType == ScriptVariable::VT_Unknown)
	{
		return DString::EmptyString;
	}

	DString translationKey = DString::EmptyString;
	switch (paramType)
	{
		case(ScriptVariable::VT_Bool):
			translationKey = TXT("VarTypeSuffixBool");
			break;

		case(ScriptVariable::VT_Int):
			translationKey = TXT("VarTypeSuffixInt");
			break;

		case(ScriptVariable::VT_Float):
			translationKey = TXT("VarTypeSuffixFloat");
			break;

		case(ScriptVariable::VT_String):
			translationKey = TXT("VarTypeSuffixString");
			break;

		case(ScriptVariable::VT_Function):
			translationKey = TXT("VarTypeSuffixFunction");
			break;

		case(ScriptVariable::VT_Obj):
			translationKey = TXT("VarTypeSuffixObj");
			break;

		case(ScriptVariable::VT_Class):
			translationKey = TXT("VarTypeSuffixClass");
			break;
	}

	if (translationKey.IsEmpty())
	{
		return DString::EmptyString;
	}

	return translator->TranslateText(translationKey, EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);
}

GraphFunction* GraphContextMenu::FindFunction (ScriptVariable::EVarType restrictedVarType, EPortDirection portDir, const std::vector<GraphFunction*>& searchIn) const
{
	for (GraphFunction* func : searchIn)
	{
		if (portDir == PD_Input && (ContainerUtils::IsEmpty(func->ReadInParams()) || func->ReadInParams().at(0) == nullptr) )
		{
			continue; //This function does not accept input connections
		}
		else if (portDir == PD_Output && (ContainerUtils::IsEmpty(func->ReadOutParams()) || func->ReadOutParams().at(0) == nullptr) )
		{
			continue; //This function does not accept output connections
		}

		if (restrictedVarType != ScriptVariable::VT_Unknown)
		{
			//The types must match either the in or out param

			if (portDir != PD_Output)
			{
				//Check input
				if (!ContainerUtils::IsEmpty(func->ReadInParams()) && func->ReadInParams().at(0)->GetVarType() != restrictedVarType)
				{
					continue;
				}
			}

			if (portDir != PD_Input)
			{
				//Check output
				if (!ContainerUtils::IsEmpty(func->ReadOutParams()) && func->ReadOutParams().at(0)->GetVarType() != restrictedVarType)
				{
					continue;
				}
			}
		}

		return func;
	}

	return nullptr;
}

bool GraphContextMenu::IsCmdRelevant (GuiDataElement<SCmdData>* cmdData, const DString& searchText) const
{
	CHECK(cmdData != nullptr)

	if (ContainerUtils::IsEmpty(cmdData->Data.SearchableText))
	{
		DString& searchableLabel = cmdData->Data.SearchableText.emplace_back(cmdData->GetLabelText());

		//Remove spaces and underscores.
		searchableLabel.ReplaceInline(TXT(" "), DString::EmptyString, DString::CC_CaseSensitive);
		searchableLabel.ReplaceInline(TXT("_"), DString::EmptyString, DString::CC_CaseSensitive);
		searchableLabel.ToUpper();

		if (cmdData->Data.Func != nullptr)
		{
			for (const DString& alias : cmdData->Data.Func->ReadAliases())
			{
				DString& searchableAlias = cmdData->Data.SearchableText.emplace_back(alias);
				searchableAlias.ReplaceInline(TXT(" "), DString::EmptyString, DString::CC_CaseSensitive);
				searchableAlias.ReplaceInline(TXT("_"), DString::EmptyString, DString::CC_CaseSensitive);
				searchableAlias.ToUpper();
			}
		}
	}

	for (const DString& searchableText : cmdData->Data.SearchableText)
	{
		if (searchableText.Contains(searchText, DString::CC_CaseSensitive))
		{
			return true;
		}
	}

	return false;
}

void GraphContextMenu::CreateLocalVariableNode (GraphFunction* func, const DString& varName)
{
	CHECK(OwningEditor != nullptr && func != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	VariablePort* draggedVar = dynamic_cast<VariablePort*>(OwningEditor->GetDraggedPort());

	VariableNode* varNode = VariableNode::CreateObject();
	if (OwningEditor->AddComponent(varNode))
	{
		varNode->BindFunction(func); //sets the var type, too

		DString newVarName(varName);
		if (newVarName.IsEmpty())
		{
			newVarName = translator->TranslateText(TXT("NewLocalVariableName"), EditorEngineComponent::LOCALIZATION_FILE_NAME, SECTION_NAME);
		}

		if (varName.IsEmpty())
		{
			std::vector<GraphFunction::SVarInfo> allLocalVars;
			if (GraphFunction* constructedFunc = OwningEditor->GetOwningFunction())
			{
				constructedFunc->FindLocalVariables(ScriptVariable::VT_Unknown, OUT allLocalVars);
			}

			Int counter = 0;
			DString countedVarName = newVarName; //"_0" is hidden. The counter should only appear if there's a conflict.
			while (true)
			{
				bool foundConflict = false;

				for (const GraphFunction::SVarInfo& varInfo : allLocalVars)
				{
					if (varInfo.VarDisplay.Compare(countedVarName, DString::CC_CaseSensitive) == 0)
					{
						foundConflict = true;
						break;
					}
				}

				if (foundConflict)
				{
					++counter;
					countedVarName = newVarName + TXT("_") + counter.ToString();
					continue;
				}

				break;
			}

			newVarName = countedVarName;
		}

		varNode->SetLocalVariable(newVarName);
		varNode->SetPosition(PlacementLocation - (varNode->ReadSize() * 0.5f));

		if (draggedVar != nullptr)
		{
			NodePort* port = nullptr;

			//Connect to the last port. Connect to last in case the graph function has its exec ports visible or not.
			if (draggedVar->GetPortType() == NodePort::PT_Output && !ContainerUtils::IsEmpty(func->ReadInParams()) && !ContainerUtils::IsEmpty(varNode->ReadInputPorts()))
			{
				 GraphNode::SPort portData = ContainerUtils::GetLast(varNode->ReadInputPorts());
				 port = portData.Port;
			}
			else if (draggedVar->GetPortType() == NodePort::PT_Input && !ContainerUtils::IsEmpty(func->ReadOutParams()) && !ContainerUtils::IsEmpty(varNode->ReadOutputPorts()))
			{
				GraphNode::SPort portData = ContainerUtils::GetLast(varNode->ReadOutputPorts());
				port = portData.Port;
			}

			if (port != nullptr)
			{
				varNode->SetConstraintClass(draggedVar->GetClassConstraint());
				if (port->CanAcceptConnectionsFrom(draggedVar, true))
				{
					port->EstablishConnectionTo(draggedVar);
				}
			}
		}
	}
}

FunctionNode* GraphContextMenu::CreateFunctionNode (GraphFunction* func)
{
	CHECK(OwningEditor != nullptr && func != nullptr)

	FunctionNode* funcNode = FunctionNode::CreateObject();
	if (OwningEditor->AddComponent(funcNode))
	{
		funcNode->BindFunction(func);
		funcNode->SetPosition(PlacementLocation - (funcNode->ReadSize() * 0.5f));

		//If the user is dragging a port, try to find a connection between that port and the port associated with the new node.
		if (NodePort* draggedPort = OwningEditor->GetDraggedPort())
		{
			//Check if it should connect to the InvokeOn port
			if (VariablePort* varPort = dynamic_cast<VariablePort*>(draggedPort))
			{
				if (VariablePort* invokeOnPort = funcNode->GetInvokeOnPort())
				{
					if (invokeOnPort->IsVisible() && varPort->GetClassConstraint() != nullptr && (varPort->GetVarType() == ScriptVariable::VT_Obj || varPort->GetVarType() == ScriptVariable::VT_Class))
					{
						if (invokeOnPort->CanAcceptConnectionsFrom(varPort, true))
						{
							invokeOnPort->EstablishConnectionTo(varPort);
							return funcNode;
						}
					}
				}
			}

			bool connectToInput = (draggedPort->GetPortType() == NodePort::PT_Output);

			const std::vector<GraphNode::SPort>* portList = nullptr;
			if (connectToInput)
			{
				portList = &funcNode->ReadInputPorts();
			}
			else
			{
				portList = &funcNode->ReadOutputPorts();
			}

			for (const GraphNode::SPort& portData : *portList)
			{
				if (portData.Port != nullptr && portData.Port->CanAcceptConnectionsFrom(draggedPort, true))
				{
					portData.Port->EstablishConnectionTo(draggedPort);
					break;
				}
			}
		}
	}

	return funcNode;
}

void GraphContextMenu::CreateReturnNode ()
{
	CHECK(OwningEditor != nullptr)

	GraphFunction* constructedFunc = OwningEditor->GetOwningFunction();
	if (constructedFunc == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to create a return node since a GraphFunction doesn't own this graph editor instance."));
		return;
	}

	ExecNode* returnNode = ExecNode::CreateObject();
	if (OwningEditor->AddComponent(returnNode))
	{
		returnNode->InitializeExecType(false);
		returnNode->SetPosition(PlacementLocation - (returnNode->ReadSize() * 0.5f));
	}
}

void GraphContextMenu::InitVarAccessNode (FunctionNode* funcNode, const DString& varId)
{
	CHECK(funcNode != nullptr && !varId.IsEmpty())

	funcNode->SetVarId(varId);

	if (OwningEditor != nullptr)
	{
		DString displayName;
		DString varOwner;
		bool foundVar = false;

		if (GraphFunction* constructedFunc = OwningEditor->GetOwningFunction())
		{
			if (GraphAsset* owningAsset = constructedFunc->GetOwningAsset())
			{
				GraphVariable var;

				//If the user is dragging an object or class port of a different type, check their member variables.
				if (VariablePort* draggedVarPort = dynamic_cast<VariablePort*>(OwningEditor->GetDraggedPort()))
				{
					if (draggedVarPort->GetVarType() == ScriptVariable::VT_Obj || draggedVarPort->GetVarType() == ScriptVariable::VT_Class)
					{
						GraphAsset* draggedAsset = draggedVarPort->GetClassConstraint();
						if (draggedAsset != nullptr && draggedAsset != owningAsset)
						{
							if (draggedAsset->FindMemberVar(varId, true, OUT var))
							{
								funcNode->EnableInvokeOnComps(draggedAsset, var.IsStatic());

								//Try to establish a connection
								if (VariablePort* invokeOnPort = funcNode->GetInvokeOnPort())
								{
									if (invokeOnPort->CanAcceptConnectionsFrom(draggedVarPort, true))
									{
										invokeOnPort->EstablishConnectionTo(draggedVarPort);
									}
								}

								varOwner = draggedAsset->GetPresentedName();
								displayName = var.GetPresentedName();
								foundVar = true;
							}
						}
					}
				}

				//Check owning asset's member variables
				if (!foundVar && owningAsset->FindMemberVar(varId, true, OUT var))
				{
					funcNode->EnableInvokeOnComps(owningAsset, var.IsStatic());
					varOwner = owningAsset->GetPresentedName();
					displayName = var.GetPresentedName();
					foundVar = true;
				}

				if (foundVar)
				{
					InitVarAccessNodePorts(funcNode, var);
				}
			}
		}

		if (!foundVar)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize variable access node since the variable %s is not found."), varId);
			return;
		}

		if (GraphFunction* associatedFunc = funcNode->FindGraphFunction())
		{
			//Append the variable name to the header text
			funcNode->SetHeaderText(associatedFunc->GetPresentedName() + TXT(" ") + displayName);

			//It should display the variable owner instead of the function owner "Core"
			funcNode->SetInvokeOnTextOverride(varOwner);
		}
	}
}

void GraphContextMenu::InitVarAccessNodePorts (FunctionNode* funcNode, const GraphVariable& var)
{
	if (var.GetVarType() != ScriptVariable::VT_Obj && var.GetVarType() != ScriptVariable::VT_Class)
	{
		return;
	}

	if (var.GetClassConstraint() == nullptr)
	{
		return;
	}

	for (const GraphNode::SPort& inPort : funcNode->ReadInputPorts())
	{
		if (VariablePort* varPort = dynamic_cast<VariablePort*>(inPort.Port))
		{
			varPort->SetClassConstraint(var.GetClassConstraint());
		}
	}

	for (const GraphNode::SPort& port : funcNode->ReadOutputPorts())
	{
		if (VariablePort* varPort = dynamic_cast<VariablePort*>(port.Port))
		{
			varPort->SetClassConstraint(var.GetClassConstraint());
		}
	}
}

void GraphContextMenu::HandleSearchEdit (TextFieldComponent* uiComponent)
{
	if (SearchResults == nullptr || SearchResultScroll == nullptr)
	{
		return;
	}

	DString fullText = uiComponent->GetContent();
	bool shouldListBeVisible = !fullText.IsEmpty();
	if (shouldListBeVisible != SearchResultScroll->IsVisible())
	{
		SearchResultScroll->SetVisibility(shouldListBeVisible);
		for (SMenuBlock* block : VisibleMenus)
		{
			if (block->Frame != nullptr)
			{
				block->Frame->SetVisibility(!shouldListBeVisible);
			}
		}
	}

	if (!fullText.IsEmpty())
	{
		DString formattedStr = DString::Replace(fullText, TXT(" "), DString::EmptyString, DString::CC_CaseSensitive);
		formattedStr = DString::Replace(formattedStr, TXT("_"), DString::EmptyString, DString::CC_CaseSensitive);
		formattedStr.ToUpper();

		std::vector<GuiDataElement<size_t>> filteredResults;
		if (!LastSearchText.IsEmpty() && fullText.StartsWith(LastSearchText, DString::CC_CaseSensitive))
		{
			//If adding to the text, only search through results instead of searching through all commands.
			for (BaseGuiDataElement* data : SearchResults->ReadList())
			{
				if (GuiDataElement<size_t>* cmdIdx = dynamic_cast<GuiDataElement<size_t>*>(data))
				{
					if (ContainerUtils::IsValidIndex(AllOptions, cmdIdx->Data))
					{
						SMenuOption& menuOption = AllOptions.at(cmdIdx->Data);
						if (GuiDataElement<SCmdData>* cmdData = dynamic_cast<GuiDataElement<SCmdData>*>(menuOption.Data))
						{
							if (IsCmdRelevant(cmdData, formattedStr))
							{
								filteredResults.emplace_back(cmdIdx->Data, AllOptions.at(cmdIdx->Data).Data->ReadLabelText());
							}
						}
					}
				}
			}
		}
		else //Search everything
		{
			//Populate the search results with commands. The results are indices to the AllOptions vector.
			for (size_t i = 0; i < AllOptions.size(); ++i)
			{
				if (GuiDataElement<SCmdData>* cmdData = dynamic_cast<GuiDataElement<SCmdData>*>(AllOptions.at(i).Data))
				{
					if (IsCmdRelevant(cmdData, formattedStr))
					{
						filteredResults.emplace_back(i, cmdData->ReadLabelText());
					}
				}
			}
		}
		
		SearchResults->SetList(filteredResults);
	}

	LastSearchText = fullText;
}

void GraphContextMenu::HandleSearchReturn (TextFieldComponent* uiComponent)
{
	if (SearchResults == nullptr)
	{
		return;
	}

	//If there's only option available, select that option.
	if (SearchResults->ReadList().size() != 1)
	{
		return;
	}

	if (GuiDataElement<size_t>* cmdIdx = dynamic_cast<GuiDataElement<size_t>*>(SearchResults->ReadList().at(0)))
	{
		if (!ContainerUtils::IsValidIndex(AllOptions, cmdIdx->Data))
		{
			return;
		}

		SMenuOption& selectedOption = AllOptions.at(cmdIdx->Data);
		if (selectedOption.OnCommand.IsBounded())
		{
			selectedOption.OnCommand(selectedOption.Data);
		}
	}
}

void GraphContextMenu::HandleItemSelected (Int selectedItemIdx)
{
	if (SearchResults == nullptr)
	{
		return;
	}

	size_t optionIdx;
	if (SearchResults->GetItemByIdx<size_t>(selectedItemIdx, OUT optionIdx))
	{
		if (!ContainerUtils::IsValidIndex(AllOptions, optionIdx))
		{
			return;
		}

		SMenuOption& selectedOption = AllOptions.at(optionIdx);
		if (selectedOption.OnCommand.IsBounded())
		{
			selectedOption.OnCommand(selectedOption.Data);
		}

		Destroy();
	}
}

void GraphContextMenu::HandleCreateLocalVar (BaseGuiDataElement* data)
{
	if (GuiDataElement<SCmdData>* cmdData = dynamic_cast<GuiDataElement<SCmdData>*>(data))
	{
		CreateLocalVariableNode(cmdData->Data.Func, cmdData->Data.VarId);
	}
}

void GraphContextMenu::HandleCreateFunctionNode (BaseGuiDataElement* data)
{
	if (GuiDataElement<SCmdData>* cmdData = dynamic_cast<GuiDataElement<SCmdData>*>(data))
	{
		CreateFunctionNode(cmdData->Data.Func);
	}
}

void GraphContextMenu::HandleCreateVarAccessNode (BaseGuiDataElement* data)
{
	if (GuiDataElement<SCmdData>* cmdData = dynamic_cast<GuiDataElement<SCmdData>*>(data))
	{
		FunctionNode* funcNode = CreateFunctionNode(cmdData->Data.Func);
		if (funcNode != nullptr && !cmdData->Data.VarId.IsEmpty())
		{
			InitVarAccessNode(funcNode, cmdData->Data.VarId);
		}
	}
}

void GraphContextMenu::HandleCreateReturnNode (BaseGuiDataElement* data)
{
	CreateReturnNode();
}
SD_END