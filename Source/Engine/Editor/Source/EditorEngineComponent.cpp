/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorEngineComponent.cpp
=====================================================================
*/

#include "CoreAssetBuilder.h"
#include "EditorEngineComponent.h"
#include "EditorInterface.h"
#include "EditorTheme.h"
#include "EditorWorkerEngineComponent.h"
#include "EditPropertyComponent.h"
#include "GraphAsset.h"
#include "TaskMessenger.h"

IMPLEMENT_ENGINE_COMPONENT(SD::EditorEngineComponent)
SD_BEGIN

#ifdef WITH_MULTI_THREAD
/**
 * Creates and initializes an Engine object responsible for running the worker thread.
 */
SD_THREAD_FUNCTION(RunEditorWorkerThread);
#endif

const DString EditorEngineComponent::LOCALIZATION_FILE_NAME(TXT("Editor"));

EditorEngineComponent::SNameRegistry::SNameRegistry (const DString& inEditorName) :
	EditorName(inEditorName),
	LatestId(-1)
{
	//Noop
}

Int EditorEngineComponent::SNameRegistry::GetNextId ()
{
	if (LatestId >= SD_MAXINT - 2)
	{
		Int objLimit = SD_MAXINT - 2;
		if (EditorObjects.size() >= objLimit)
		{
			EditorLog.Log(LogCategory::LL_Fatal, TXT("Editor Object limit reached. There are over %s %s instances. Try reducing the number of objects or splitting these objects into multiple categories/names."), objLimit, EditorName);
			return -1;
		}

		//Hit the numeric limit. Try resetting the IDs of the objects.
		for (size_t i = 0; i < EditorObjects.size(); ++i)
		{
			EditorObjects.at(i)->SetEditorId(i);
		}

		if (!ContainerUtils::IsEmpty(EditorObjects))
		{
			LatestId = EditorObjects.size() - 1;
		}
		else
		{
			LatestId = -1;
		}
	}

	++LatestId;
	return LatestId;
}

EditorEngineComponent::EditorEngineComponent () : Super(),
	UiTheme(nullptr)
#ifdef WITH_MULTI_THREAD
	, WorkerMessenger(nullptr)
#endif
{
	//Noop
}

void EditorEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_EDITOR, TICK_GROUP_PRIORITY_EDITOR);

	ImportEditorResources();
}

void EditorEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	UiTheme = EditorTheme::CreateObject();
	UiTheme->InitializeTheme();
	UiTheme->InitializeStyles();
	UiTheme->SetDefaultStyle(EditorTheme::EDITOR_STYLE_NAME);

	CreateCoreAssets();
}

void EditorEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

#ifdef WITH_MULTI_THREAD
	InitializeWorkerThread();
#endif
}

void EditorEngineComponent::ShutdownComponent ()
{
	if (UiTheme != nullptr)
	{
		UiTheme->Destroy();
		UiTheme = nullptr;
	}

	for (const std::pair<HashedString, SNameRegistry*>& asset : EditorObjects)
	{
		if (asset.second != nullptr)
		{
			delete asset.second;
		}
	}
	EditorObjects.clear();

	//Destroy all loaded assets
	for (const std::pair<HashedString, GraphAsset*>& asset : LoadedAssets)
	{
		asset.second->LoadState = GraphAsset::LS_NotLoaded; //Clear this flag to prevent this asset from unregistering itself from this engine component.
		asset.second->Destroy();
	}
	LoadedAssets.clear();

#ifdef WITH_MULTI_THREAD
	if (WorkerMessenger != nullptr)
	{
		WorkerMessenger->Destroy();
		WorkerMessenger = nullptr;
	}
#endif

	Super::ShutdownComponent();
}

void EditorEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(GraphicsEngineComponent::SStaticClass()); //For the texture pool
}

void EditorEngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(GuiEngineComponent::SStaticClass());
}

bool EditorEngineComponent::RegisterAsset (GraphAsset* newAsset)
{
	if (newAsset == nullptr || newAsset->GetLoadState() == GraphAsset::LS_NotLoaded)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Refused to load asset to the EditorEngineComponent since it's not yet loaded or partially loaded."));
		return false;
	}

	HashedString assetName(newAsset->ReadAssetName());
	if (LoadedAssets.contains(assetName))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Refused to load asset to the EditorEngineComponent since %s is already registered."), newAsset->ReadAssetName());
		return false;
	}

	LoadedAssets.insert({assetName, newAsset});
	return true;
}

GraphAsset* EditorEngineComponent::ObtainOrLoadAsset (const HashedString& assetName)
{
	if (LoadedAssets.contains(assetName))
	{
		return LoadedAssets.at(assetName);
	}

	DString assetNameStr = assetName.ToString();

	//See if the asset name is found in the catalogue. The catalogue may contain attributes of assets that haven't been loaded.
	for (const FileAttributes& catalogue : AssetCatalogue)
	{
		if (catalogue.ReadFileName().Compare(assetNameStr, DString::CC_CaseSensitive) == 0)
		{
			//Create a new asset and register it.
			GraphAsset* newAsset = GraphAsset::CreateObject();
			newAsset->InitializeAsset(assetNameStr, true);
			newAsset->SetSaveLocation(catalogue.ReadPath());
			if (!newAsset->LoadEssentials()) //Loading the asset will register it to this engine component.
			{
				newAsset->Destroy();
				return nullptr;
			}

			return newAsset;
		}
	}

	EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain asset \"%s\" from the EditorEngineComponent since that asset isn't loaded nor does it exist in the asset catalogue."), assetNameStr);
	return nullptr;
}

bool EditorEngineComponent::RemoveAsset (GraphAsset* target)
{
	if (target == nullptr)
	{
		return false;
	}

	HashedString assetName(target->ReadAssetName());
	if (!LoadedAssets.contains(assetName))
	{
		return false;
	}

	LoadedAssets.erase(assetName);
	return true;
}

void EditorEngineComponent::AddEditorObject (EditorInterface* newObj, HashedString& outRegisteredHash)
{
	if (newObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to add a null editor object to the EditorEngineComponent."));
		return;
	}

	DString objName = newObj->GetEditorObjName();
	outRegisteredHash = HashedString(objName);

	SNameRegistry* registry = nullptr;
	if (!EditorObjects.contains(outRegisteredHash))
	{
		registry = new SNameRegistry(objName);
		EditorObjects.insert({outRegisteredHash, registry});
	}
	else
	{
		registry = EditorObjects.at(outRegisteredHash);
	}

	Int nextId = registry->GetNextId();
	registry->EditorObjects.push_back(newObj);
	newObj->SetEditorId(nextId);
}

void EditorEngineComponent::RemoveEditorObject (EditorInterface* target, const HashedString& registeredHash)
{
	if (target == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot remove a null editor object from the EditorEngineComponent."));
		return;
	}

	if (EditorObjects.contains(registeredHash))
	{
		if (SNameRegistry* registry = EditorObjects.at(registeredHash))
		{
			ContainerUtils::RemoveItem(OUT registry->EditorObjects, target);
			target->SetEditorId(-1);
		}
	}
}

EditorInterface* EditorEngineComponent::FindEditorObject (const DString& baseName, Int expectedId, DString::ECaseComparison caseComparison) const
{
	HashedString hashName(baseName);
	SNameRegistry* registry = nullptr;
	if (EditorObjects.contains(hashName))
	{
		registry = EditorObjects.at(hashName);
	}
	else if (caseComparison == DString::CC_IgnoreCase)
	{
		DString upperName(baseName);
		upperName.ToUpper();

		for (const std::pair<HashedString, SNameRegistry*>& editorObj : EditorObjects)
		{
			DString registryName = editorObj.second->EditorName;
			registryName.ToUpper();
			if (upperName.Compare(registryName, DString::CC_CaseSensitive) == 0)
			{
				registry = editorObj.second;
				break;
			}
		}
	}

	if (registry == nullptr)
	{
		return nullptr;
	}

	for (size_t i = 0; i < registry->EditorObjects.size(); ++i)
	{
		CHECK(registry->EditorObjects.at(i) != nullptr)
		if (registry->EditorObjects.at(i)->GetEditorId() == expectedId)
		{
			return registry->EditorObjects.at(i);
		}
	}

	return nullptr;
}

void EditorEngineComponent::ReplaceAssetCatalogue (const std::vector<FileAttributes>& newAssetCatalogue)
{
	AssetCatalogue = newAssetCatalogue;
}

void EditorEngineComponent::AppendAssetCatalogue (const std::vector<FileAttributes>& additionalAssets)
{
	for (size_t i = 0; i < additionalAssets.size(); ++i)
	{
		if (ContainerUtils::FindInVector(AssetCatalogue, additionalAssets.at(i)) == UINT_INDEX_NONE)
		{
			AssetCatalogue.push_back(additionalAssets.at(i));
		}
	}
}

void EditorEngineComponent::ImportEditorResources ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Button textures
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("AddButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("AddButtonSquared.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("CopyButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("CreateButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("DeleteButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("EditButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("ElipsesButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("ExportButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("MoveDownButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("MoveUpButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("NewButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("OpenButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("PassFail.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("RemoveButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("ResetDefaultsButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("SaveButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("TargetButton.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("ViewButton.txtr")));

	//Graph resources
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("GraphGrid.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("InvokeOnBackground.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("InvokeOnEdge.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("NodeBackground.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("NodePortArrow.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("NodePortCircle.txtr")));

	//Misc textures
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Editor"), TXT("Dot.txtr")));
}

void EditorEngineComponent::CreateCoreAssets ()
{
	ClmEngineComponent* clmEngine = ClmEngineComponent::Find();
	if (clmEngine == nullptr)
	{
		//Perhaps this application is using the editor module only for its inspector classes instead of using the GraphEditor.
		EditorLog.Log(LogCategory::LL_Log, TXT("The ClmEngineComponent is not found. The CoreAssetBuilder will not build the graph editor's core assets."));
		return;
	}

	CoreAssetBuilder::CreateCoreAsset(clmEngine);
	CoreAssetBuilder::CreateMathAsset(clmEngine);
}

#ifdef WITH_MULTI_THREAD
void EditorEngineComponent::InitializeWorkerThread ()
{
	CHECK(OwningEngine != nullptr)

	//Check if the engine has a MultiThreadEngineComponent. If not, then a worker engine component will not run.
	const std::vector<EngineComponent*>& engineComps = OwningEngine->ReadEngineComponents();
	bool bHasMultiThreadEngine = false;
	for (EngineComponent* engineComp : engineComps)
	{
		if (dynamic_cast<MultiThreadEngineComponent*>(engineComp) != nullptr)
		{
			bHasMultiThreadEngine = true;
			break;
		}
	}

	if (!bHasMultiThreadEngine)
	{
		return;
	}

	SDThread thread;
	THREAD_INIT_TYPE threadInit;
	threadInit.ThreadName = TXT("Editor Worker Thread");
	int exitCode = OS_CreateThread(thread, RunEditorWorkerThread, nullptr, &threadInit);
	if (exitCode != 0)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to launch the %s. Exit code:  %s"), threadInit.ThreadName, Int(exitCode));
		return;
	}

	Int timeoutTime = 30000; //30 seconds
	Int totalWaitTime = 0;
	Int sleepInterval = 500; //half second

	//Wait for the sound engine to finish initializing
	while (Engine::GetEngine(MULTI_THREAD_EDITOR_WORKER_IDX) == nullptr || !Engine::GetEngine(MULTI_THREAD_EDITOR_WORKER_IDX)->IsInitialized())
	{
		totalWaitTime += sleepInterval;
		if (totalWaitTime >= timeoutTime)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to launch the %s. Application timed out after waiting for %s ms for the external engine to initialize."), threadInit.ThreadName, timeoutTime);
			return;
		}

		OS_Sleep(sleepInterval);
	}

	EditorLog.Log(LogCategory::LL_Log, TXT("%s initialized and ready! The EditorEngineComponent can now run asynchronous tasks."), threadInit.ThreadName);

	WorkerMessenger = TaskMessenger::CreateObject();
}

SD_THREAD_FUNCTION(RunEditorWorkerThread)
{
	Engine* workerEngine = new Engine();
	CHECK(workerEngine != nullptr)

	//Set to the same tick rate as the main engine to reduce the frozen time whenever the main engine needs to send to this thread.
	Float updateRate = 0.2f; //5 fps
	workerEngine->SetMinDeltaTime(updateRate);

	MultiThreadEngineComponent* multiThreadEngine = new MultiThreadEngineComponent();

	std::vector<EngineComponent*> engineComponents;
	engineComponents.push_back(multiThreadEngine);
	engineComponents.push_back(new EditorWorkerEngineComponent());

	workerEngine->InitializeEngine(MULTI_THREAD_EDITOR_WORKER_IDX, engineComponents);
#ifdef DEBUG_MODE
	workerEngine->SetDebugName(TXT("Editor Worker Engine"));
#endif

	if (ThreadManager* manager = multiThreadEngine->GetThreadManager())
	{
		//Set a grace period on the worker thread to allow the main thread to send data to the worker thread without freezing the main engine.
		manager->SetGracePeriod(updateRate);
	}

	while (true)
	{
		workerEngine->Tick();
		if (workerEngine->IsShuttingDown())
		{
			break;
		}
	}

	delete workerEngine;
	workerEngine = nullptr;

	SD_EXIT_THREAD
}
#endif
SD_END