/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorTheme.cpp
=====================================================================
*/

#include "EditableArray.h"
#include "EditableBool.h"
#include "EditableDString.h"
#include "EditableEnum.h"
#include "EditableFloat.h"
#include "EditableInt.h"
#include "EditableObjectComponent.h"
#include "EditablePropCommand.h"
#include "EditableStruct.h"
#include "EditorTheme.h"
#include "EditPropertyComponent.h"
#include "ErrorNode.h"
#include "ExecNode.h"
#include "FunctionNode.h"
#include "GraphNode.h"
#include "NodePort.h"
#include "VariableNode.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::EditorTheme, SD::GuiTheme)
SD_BEGIN

const DString EditorTheme::EDITOR_STYLE_NAME(TXT("EditorTheme"));

void EditorTheme::InitProps ()
{
	Super::InitProps();

	FieldControlAdd = nullptr;
	FieldControlRemove = nullptr;
	FieldControlReset = nullptr;
	FieldControlMoveUp = nullptr;
	FieldControlMoveDown = nullptr;
	FieldControlCreate = nullptr;
	FieldControlCopy = nullptr;
	FieldControlDelete = nullptr;
	FieldControlTarget = nullptr;

	ArrayReorderRegionTexture = nullptr;

	PortTexture = nullptr;
}

void EditorTheme::InitializeTheme ()
{
	Super::InitializeTheme();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	FieldControlAdd = localTexturePool->GetTexture(HashedString("Engine.Editor.AddButton"));
	FieldControlRemove = localTexturePool->GetTexture(HashedString("Engine.Editor.RemoveButton"));
	FieldControlReset = localTexturePool->GetTexture(HashedString("Engine.Editor.ResetDefaultsButton"));
	FieldControlMoveUp = localTexturePool->GetTexture(HashedString("Engine.Editor.MoveUpButton"));
	FieldControlMoveDown = localTexturePool->GetTexture(HashedString("Engine.Editor.MoveDownButton"));
	FieldControlCreate = localTexturePool->GetTexture(HashedString("Engine.Editor.CreateButton"));
	FieldControlCopy = localTexturePool->GetTexture(HashedString("Engine.Editor.CopyButton"));
	FieldControlDelete = localTexturePool->GetTexture(HashedString("Engine.Editor.DeleteButton"));
	FieldControlTarget = localTexturePool->GetTexture(HashedString("Engine.Editor.TargetButton"));

	ArrayReorderRegionTexture = localTexturePool->GetTexture(HashedString("Engine.Editor.Dot"));

	InvokeOnBackground = localTexturePool->GetTexture(HashedString("Engine.Editor.InvokeOnBackground"));
	InvokeOnEdge = localTexturePool->GetTexture(HashedString("Engine.Editor.InvokeOnEdge"));
	NodeBackground = localTexturePool->GetTexture(HashedString("Engine.Editor.NodeBackground"));
	PortTexture = localTexturePool->GetTexture(HashedString("Engine.Editor.NodePortCircle"));
	ExecPortTexture = localTexturePool->GetTexture(HashedString("Engine.Editor.NodePortArrow"));
}

void EditorTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	InitializeEditorStyle();
}

void EditorTheme::InitializeEditorStyle ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SStyleInfo editorStyle = CreateStyleFrom(Styles.at(DefaultStyleIdx.ToUnsignedInt()));

	InitEditPropTemplate(EditPropertyComponent::SStaticClass(), OUT editorStyle);
	InitEditPropTemplate(EditableArray::SStaticClass(), OUT editorStyle);
	InitEditPropTemplate(EditableBool::SStaticClass(), OUT editorStyle);
	InitEditPropTemplate(EditableDString::SStaticClass(), OUT editorStyle);

	if (EditableEnum* enumTemplate = InitEditPropTemplateCast<EditableEnum>(EditableEnum::SStaticClass(), OUT editorStyle))
	{
		if (DropdownComponent* dropdown = enumTemplate->GetDropdown())
		{
			if (FrameComponent* frame = dropdown->GetSelectedObjBackground())
			{
				frame->SetCenterColor(Color(85, 85, 85));
				frame->SetBorderThickness(1.f);
			}

			if (ListBoxComponent* list = dropdown->GetExpandMenuList())
			{
				list->SetHoverColor(Color(35, 35, 35, 96));
				if (FrameComponent* frame = list->GetBackground())
				{
					frame->SetCenterColor(Color(64, 64, 64));
				}
			}
		}
	}

	InitEditPropTemplate(EditableFloat::SStaticClass(), OUT editorStyle);
	InitEditPropTemplate(EditableInt::SStaticClass(), OUT editorStyle);
	InitEditPropTemplate(EditableObjectComponent::SStaticClass(), OUT editorStyle);
	if (EditablePropCommand* cmdTemplate = InitEditPropTemplateCast<EditablePropCommand>(EditablePropCommand::SStaticClass(), OUT editorStyle))
	{
		if (ButtonComponent* button = cmdTemplate->GetButton())
		{
			if (ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(button->ReplaceStateComponent(ColorButtonState::SStaticClass())))
			{
				colorState->SetDefaultColor(Color(64, 64, 64));
				colorState->SetHoverColor(Color(80, 80, 80));
				colorState->SetDownColor(Color(48, 48, 48));
				colorState->SetDisabledColor(Color(24, 24, 24));
			}
		}
	}

	InitEditPropTemplate(EditableStruct::SStaticClass(), OUT editorStyle);

	//ErrorNode
	{
		ErrorNode* errorNodeTemplate = ErrorNode::CreateObject();
		errorNodeTemplate->SetHighlightThickness(4.f);
		errorNodeTemplate->SetBackgroundColor(Color(128, 32, 32));
		if (FrameComponent* background = errorNodeTemplate->GetBackground())
		{
			background->SetCenterTexture(NodeBackground);
		}

		editorStyle.Templates.push_back(errorNodeTemplate);
	}

	//ExecNode
	{
		ExecNode* execNodeTemplate = ExecNode::CreateObject();
		execNodeTemplate->SetHighlightThickness(4.f);
		execNodeTemplate->SetBackgroundColor(Color(150, 32, 255)); //Light purple because edge of rainbow. Ultra violet light. And exec nodes are typically at the ends of the definition.
		if (FrameComponent* background = execNodeTemplate->GetBackground())
		{
			background->SetCenterTexture(NodeBackground);
		}

		editorStyle.Templates.push_back(execNodeTemplate);
	}

	//FunctionNode
	{
		FunctionNode* functionNodeTemplate = FunctionNode::CreateObject();
		functionNodeTemplate->SetHighlightThickness(4.f);
		if (FrameComponent* background = functionNodeTemplate->GetBackground())
		{
			background->SetCenterTexture(NodeBackground);
		}

		if (FrameComponent* invokeOnBackground = functionNodeTemplate->GetInvokeOnBackground())
		{
			invokeOnBackground->SetCenterTexture(InvokeOnBackground);
		}

		if (SpriteComponent* invokeOnEdgeComp = functionNodeTemplate->GetInvokeOnEdgeSprite())
		{
			invokeOnEdgeComp->SetSpriteTexture(InvokeOnEdge);
		}

		editorStyle.Templates.push_back(functionNodeTemplate);
	}

	//GraphNode
	{
		GraphNode* nodeTemplate = GraphNode::CreateObject();
		nodeTemplate->SetHighlightThickness(4.f);
		if (FrameComponent* background = nodeTemplate->GetBackground())
		{
			background->SetCenterTexture(NodeBackground);
		}
		editorStyle.Templates.push_back(nodeTemplate);
	}

	//NodePort
	{
		NodePort* portTemplate = NodePort::CreateObject();
		if (SpriteComponent* spriteComp = portTemplate->GetSpriteComp())
		{
			spriteComp->SetSpriteTexture(PortTexture);
		}
		editorStyle.Templates.push_back(portTemplate);
	}

	//VariableNode
	{
		VariableNode* nodeTemplate = VariableNode::CreateObject();
		nodeTemplate->SetHighlightThickness(4.f);
		if (FrameComponent* nodeBackground = nodeTemplate->GetBackground())
		{
			nodeBackground->SetCenterTexture(NodeBackground);
		}

		if (ButtonComponent* buttonComp = nodeTemplate->GetEditButton())
		{
			buttonComp->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

			if (FrameComponent* background = buttonComp->GetBackground())
			{
				if (const Texture* editTexture = localTexturePool->EditTexture(HashedString("Engine.Editor.EditButton")))
				{
					background->SetCenterTexture(editTexture);
				}
			}

			if (LabelComponent* caption = buttonComp->GetCaptionComponent())
			{
				caption->Destroy();
			}
		}

		editorStyle.Templates.push_back(nodeTemplate);
	}

	//VariablePort
	{
		VariablePort* varTemplate = VariablePort::CreateObject();
		if (SpriteComponent* spriteComp = varTemplate->GetSpriteComp())
		{
			spriteComp->SetSpriteTexture(PortTexture);
		}
		editorStyle.Templates.push_back(varTemplate);
	}

	DefaultStyleIdx = Int(Styles.size());
	editorStyle.Name = EDITOR_STYLE_NAME;
	Styles.push_back(editorStyle);
}

EditPropertyComponent* EditorTheme::InitEditPropTemplate (const DClass* editPropClass, SStyleInfo& outStyle)
{
	EditPropertyComponent* propTemplate = dynamic_cast<EditPropertyComponent*>(editPropClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	CHECK(propTemplate != nullptr) //Ensure the given DClass is a EditPropertyComponent

	propTemplate->SetNormalFontColor(Color(200, 200, 200));
	propTemplate->SetReadOnlyFontColor(Color(128, 128, 128));

	if (ColorRenderComponent* barrierComp = propTemplate->GetBarrierComponent())
	{
		barrierComp->SolidColor = Color(16, 16, 16);
	}

	outStyle.Templates.push_back(propTemplate);
	return propTemplate;
}
SD_END