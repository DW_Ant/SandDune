/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphFunction.cpp
=====================================================================
*/

#include "EditableArray.h"
#include "EditableBool.h"
#include "EditableDString.h"
#include "EditablePropCommand.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "ExecNode.h"
#include "FunctionNode.h"
#include "GraphAsset.h"
#include "GraphCompiler.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "GraphNode.h"
#include "GraphVariable.h"
#include "NodePort.h"
#include "VariableNode.h"
#include <regex>

IMPLEMENT_CLASS(SD::GraphFunction, SD::Object)
SD_BEGIN

MulticastDelegate<const DString&, const DString&, const DString&> GraphFunction::OnFunctionNameChanged = {};
MulticastDelegate<const DString&, const DString&> GraphFunction::OnCommentChanged = {};
MulticastDelegate<const DString&, const DString&, const DString&> GraphFunction::OnParamNameChanged = {};
MulticastDelegate<const DString&> GraphFunction::OnParamChanged = {};

const DString GraphFunction::PERMITTED_CHARS_FOR_NAME(TXT("[a-zA-Z0-9-_]")); //Alphanumeric characters plus underscores and hyphens

GraphFunction::SVarInfo::SVarInfo (const DString& inVarId, const DString& inVarDisplay, ScriptVariable::EVarType inVarType) :
	VarId(inVarId),
	VarDisplay(inVarDisplay),
	VarType(inVarType)
{
	//Noop
}

void GraphFunction::InitProps ()
{
	Super::InitProps();

	FunctionName = DString::EmptyString;
	OwningAsset = nullptr;
	ContextMenuCategory = DString::EmptyString;
	UniqueFlag = UF_None;
	bVariableRelatedFunction = false;
	ExecPortVisibility = EPV_Default;
	bHeader = false;
	bGlobal = false;
	bStatic = false;
	bConst = false;
	bProtected = true;
	bFinal = false;
	Comment = DString::EmptyString;
	NativeAssociation = nullptr;
	SuperFunction = nullptr;

	bSignatureDirty = false;
}

DString GraphFunction::GetFriendlyName () const
{
	if (!DisplayName.IsEmpty())
	{
		return TXT("GraphFunction: ") + DisplayName;
	}

	if (!FunctionName.IsEmpty())
	{
		return TXT("GraphFunction: ") + FunctionName;
	}

	return Super::GetFriendlyName();
}

CopiableObjectInterface* GraphFunction::CreateCopiableInstanceOfMatchingType () const
{
	if (const DClass* duneClass = StaticClass())
	{
		if (const GraphFunction* cdo = dynamic_cast<const GraphFunction*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void GraphFunction::CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate)
{
	if (const GraphFunction* cpyObj = dynamic_cast<const GraphFunction*>(objectTemplate))
	{
		SetFunctionName(cpyObj->ReadFunctionName());
		SetDisplayName(cpyObj->ReadDisplayName());
		EditAliases() = cpyObj->ReadAliases();
		SetOwningAsset(cpyObj->GetOwningAsset());
		SetContextMenuCategory(cpyObj->ReadContextMenuCategory());

		CopiableObjectInterface::CopyVector<GraphVariable>(cpyObj->InParams, OUT InParams, [](GraphVariable* objToDelete)
		{
			delete objToDelete;
		});

		CopiableObjectInterface::CopyVector<GraphVariable>(cpyObj->OutParams, OUT OutParams, [](GraphVariable* objToDelete)
		{
			delete objToDelete;
		});

		SetUniqueFlag(cpyObj->GetUniqueFlag());
		SetVariableRelatedFunction(cpyObj->IsVariableRelatedFunction());
		SetExecPortVisibility(cpyObj->GetExecPortVisibility());
		SetHeader(cpyObj->IsHeader());
		SetGlobal(cpyObj->IsGlobal());
		SetStatic(cpyObj->IsStatic());
		SetConst(cpyObj->IsConst());
		SetProtected(cpyObj->IsProtected());
		SetFinal(cpyObj->IsFinal());
		SetComment(cpyObj->ReadComment());
		SetNativeAssociation(cpyObj->GetNativeAssociation());
		SetSuperFunction(cpyObj->GetSuperFunction());
	}
}

DString GraphFunction::GetEditorObjName () const
{
	return GetPresentedName();
}

void GraphFunction::PopulateInspectorList (InspectorList& outList)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString fileName = TXT("Editor");
	DString sectionName = TXT("GraphFunction");

	if (EditableDString::SInspectorString* nameProp = outList.AddProp(new EditableDString::SInspectorString(TXT("FunctionName"), DString::EmptyString, nullptr, DString::EmptyString)))
	{
		nameProp->PermittedChars = PERMITTED_CHARS_FOR_NAME;
		nameProp->PropertyId = FA_Name;
		nameProp->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphFunction, HandleFunctionNameEdit, void, EditPropertyComponent*);
	}

	if (EditablePropCommand::SInspectorPropCmd* propCmd = outList.AddProp(new EditablePropCommand::SInspectorPropCmd(TXT("Open Function"), DString::EmptyString, SDFUNCTION_2PARAM(this, GraphFunction, HandleOpenFunction, void, ButtonComponent*, EditablePropCommand*))))
	{
		propCmd->PropertyId = FA_Open;
	}

	//InParams
	{
		InspectorList* varInspector = new InspectorList();
		if (EditableObjectComponent::SInspectorObject* varProp = varInspector->AddProp(new EditableObjectComponent::SInspectorObject(TXT("In Param"), DString::EmptyString, nullptr, true)))
		{
			varProp->OnCreateEditorObject = [&](DropdownComponent*)
			{
				return new GraphVariable(HashedString(), this);
			};

			varProp->OnIsEditorObjectRelevant = [](EditorInterface*)
			{
				return false;
			};

			varProp->OnInitCreatedObject = [&](EditorInterface* obj)
			{
				if (GraphVariable* newVar = dynamic_cast<GraphVariable*>(obj))
				{
					newVar->SetInspectorType(GraphVariable::IT_InParam);
					newVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
				}
			};
		}

		ObjectArrayBinder<GraphVariable>* arrayBinder = new ObjectArrayBinder<GraphVariable>(&InParams);

		if (EditableArray::SInspectorArray* inParamArray = outList.AddProp(new EditableArray::SInspectorArray(TXT("InParams"), translator->TranslateText(TXT("InParams"), fileName, sectionName), varInspector, arrayBinder)))
		{
			inParamArray->PropertyId = FA_InParams;
		}
	}

	//OutParams
	//Must create another copy of the inspector and binder since the SInspectorArray will take ownership over each instance.
	{
		InspectorList* outVarInspector = new InspectorList();
		if (EditableObjectComponent::SInspectorObject* varProp = outVarInspector->AddProp(new EditableObjectComponent::SInspectorObject(TXT("Out Param"), DString::EmptyString, nullptr, true)))
		{
			varProp->OnCreateEditorObject = [&](DropdownComponent*)
			{
				return new GraphVariable(HashedString(), this);
			};

			varProp->OnIsEditorObjectRelevant = [](EditorInterface*)
			{
				return false;
			};

			varProp->OnInitCreatedObject = [&](EditorInterface* obj)
			{
				if (GraphVariable* newVar = dynamic_cast<GraphVariable*>(obj))
				{
					newVar->SetInspectorType(GraphVariable::IT_OutParam);
					newVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
				}
			};
		}

		ObjectArrayBinder<GraphVariable>* arrayBinder = new ObjectArrayBinder<GraphVariable>(&OutParams);

		if (EditableArray::SInspectorArray* outParamArray = outList.AddProp(new EditableArray::SInspectorArray(TXT("OutParams"), translator->TranslateText(TXT("OutParams"), fileName, sectionName), outVarInspector, arrayBinder)))
		{
			outParamArray->PropertyId = FA_OutParams;
		}
	}

	if (EditableBool::SInspectorBool* editGlobal = outList.AddProp(new EditableBool::SInspectorBool(TXT("Global"), translator->TranslateText(TXT("Global"), fileName, sectionName), nullptr, false)))
	{
		editGlobal->PropertyId = GraphFunction::FA_Global;
		editGlobal->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphFunction, HandleFunctionGlobalToggle, void, EditPropertyComponent*);
	}

	if (EditableBool::SInspectorBool* editStatic = outList.AddProp(new EditableBool::SInspectorBool(TXT("Static"), translator->TranslateText(TXT("Static"), fileName, sectionName), &bStatic, false)))
	{
		editStatic->PropertyId = GraphFunction::FA_Static;
	}

	if (EditableBool::SInspectorBool* editConst = outList.AddProp(new EditableBool::SInspectorBool(TXT("Const"), translator->TranslateText(TXT("Const"), fileName, sectionName), &bConst, false)))
	{
		editConst->PropertyId = GraphFunction::FA_Const;
	}

	if (EditableBool::SInspectorBool* editProtected = outList.AddProp(new EditableBool::SInspectorBool(TXT("Protected"), translator->TranslateText(TXT("Protected"), fileName, sectionName), &bProtected, true)))
	{
		editProtected->PropertyId = GraphFunction::FA_Protected;
	}

	if (EditableBool::SInspectorBool* editFinal = outList.AddProp(new EditableBool::SInspectorBool(TXT("Final"), translator->TranslateText(TXT("Final"), fileName, sectionName), &bFinal, false)))
	{
		editFinal->PropertyId = GraphFunction::FA_Final;
	}

	if (EditableDString::SInspectorString* editComment = outList.AddProp(new EditableDString::SInspectorString(TXT("Comment"), DString::EmptyString, &Comment)))
	{
		editComment->PropertyId = GraphFunction::FA_Comment;
		editComment->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphFunction, HandleFunctionCommentEdit, void, EditPropertyComponent*);
	}
}

void GraphFunction::DeleteEditorObject ()
{
	Destroy();
}

void GraphFunction::Destroyed ()
{
	UnregisterEditorInterface();

	for (GraphVariable* var : InParams)
	{
		if (var != nullptr)
		{
			var->DeleteEditorObject();
		}
	}
	ContainerUtils::Empty(OUT InParams);

	for (GraphVariable* var : OutParams)
	{
		if (var != nullptr)
		{
			var->DeleteEditorObject();
		}
	}
	ContainerUtils::Empty(OUT OutParams);

	if (SuperFunction != nullptr)
	{
		ContainerUtils::RemoveItem(SuperFunction->SubFunctions, this);
	}

	for (GraphFunction* subFunction : SubFunctions)
	{
		CHECK(subFunction != nullptr)

		//The root function is being destroyed. Copy its signature to the sub function.
		subFunction->FunctionName = FunctionName;
		subFunction->InParams = InParams;
		subFunction->OutParams = OutParams;
		subFunction->SuperFunction = nullptr;
	}

	if (FunctionDefinition != nullptr)
	{
		FunctionDefinition->Destroy();
	}

	Super::Destroyed();
}

const GraphFunction& GraphFunction::ReadRootFunction () const
{
	if (SuperFunction != nullptr)
	{
		return SuperFunction->ReadRootFunction();
	}

	return *this;
}

GraphFunction& GraphFunction::EditRootFunction ()
{
	if (SuperFunction != nullptr)
	{
		return SuperFunction->EditRootFunction();
	}

	return *this;
}

void GraphFunction::CreateBlankDefinition (const Vector2& functionSize)
{
	if (FunctionDefinition != nullptr)
	{
		FunctionDefinition->Destroy();
	}

	FunctionDefinition = GraphEditor::CreateObject();
	if (FunctionDefinition != nullptr)
	{
		FunctionDefinition->SetOwningFunction(this);
		FunctionDefinition->SetSize(functionSize);
		InitFunctionDefinition();
	}
}

void GraphFunction::ClearDirtySignature ()
{
	bSignatureDirty = false;

	for (GraphFunction* subFunction : SubFunctions)
	{
		CHECK(subFunction != nullptr)
		subFunction->ClearDirtySignature();
	}
}

void GraphFunction::SaveToConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	DString superFunctionName(DString::EmptyString);
	if (SuperFunction != nullptr)
	{
		superFunctionName = SuperFunction->ReadFunctionName();
	}
	config->SavePropertyText(sectionName, TXT("SuperFunction"), superFunctionName);

	if (SuperFunction == nullptr)
	{
		config->SavePropertyText(sectionName, TXT("Comment"), Comment);
		config->SaveProperty<Bool>(sectionName, TXT("bGlobal"), bGlobal);
		config->SaveProperty<Bool>(sectionName, TXT("bStatic"), bStatic);
		config->SaveProperty<Bool>(sectionName, TXT("bConst"), bConst);
		config->SaveProperty<Bool>(sectionName, TXT("bProtected"), bProtected);
		config->SaveProperty<Bool>(sectionName, TXT("bFinal"), bFinal);

		std::vector<DString> inParamNames;
		inParamNames.resize(InParams.size());
		for (size_t i = 0; i < InParams.size(); ++i)
		{
			if (InParams.at(i) != nullptr)
			{
				inParamNames.at(i) = InParams.at(i)->ReadVariableName().ToString();
				InParams.at(i)->SaveToConfig(config, sectionName + TXT(".") + inParamNames.at(i));
			}
		}
		config->SaveArray(sectionName, TXT("InParams"), inParamNames);

		std::vector<DString> outParamNames;
		outParamNames.resize(OutParams.size());
		for (size_t i = 0; i < OutParams.size(); ++i)
		{
			if (OutParams.at(i) != nullptr)
			{
				outParamNames.at(i) = OutParams.at(i)->ReadVariableName().ToString();
				OutParams.at(i)->SaveToConfig(config, sectionName + TXT(".") + outParamNames.at(i));
			}
		}
		config->SaveArray(sectionName, TXT("OutParams"), outParamNames);
	}
}

void GraphFunction::LoadFromConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)

	SuperFunction = nullptr;
	DString superFunctionName = config->GetPropertyText(sectionName, TXT("SuperFunction"));
	if (superFunctionName.IsEmpty())
	{
		Comment = config->GetPropertyText(sectionName, TXT("Comment"));
		bGlobal = config->GetProperty<Bool>(sectionName, TXT("bGlobal"));
		bStatic = config->GetProperty<Bool>(sectionName, TXT("bStatic"));
		bConst = config->GetProperty<Bool>(sectionName, TXT("bConst"));
		bProtected = config->GetProperty<Bool>(sectionName, TXT("bProtected"));
		bFinal = config->GetProperty<Bool>(sectionName, TXT("bFinal"));

		std::vector<DString> inParamNames;
		config->GetArrayValues<DString>(sectionName, TXT("InParams"), OUT inParamNames);
		for (const DString& inParamName : inParamNames)
		{
			GraphVariable* inParam = new GraphVariable(inParamName, this);
			inParam->SetInspectorType(GraphVariable::IT_InParam);
			inParam->LoadFromConfig(config, sectionName + TXT(".") + inParamName);
			inParam->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
			InParams.push_back(inParam);
		}
		
		std::vector<DString> outParamNames;
		config->GetArrayValues<DString>(sectionName, TXT("OutParams"), OUT outParamNames);
		for (const DString& outParamName : outParamNames)
		{
			GraphVariable* outParam = new GraphVariable(outParamName, this);
			outParam->SetInspectorType(GraphVariable::IT_OutParam);
			outParam->LoadFromConfig(config, sectionName + TXT(".") + outParamName);
			outParam->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
			OutParams.push_back(outParam);
		}
	}
	else //If there is a SuperAsset
	{
		//Only need to get a reference to the super function since the super function is responsible for maintaining the function's meta data.
		SuperFunction = FindSuperFunction(superFunctionName);
	}

	//TODO: When function definition is being saved, update this function to load from it. For now default to a blank function.
	CreateBlankDefinition(Vector2(8192.f, 8192.f));
}

void GraphFunction::Serialize (DataBuffer& outBuffer) const
{
	outBuffer << FunctionName;

	if (SuperFunction != nullptr)
	{
		outBuffer << SuperFunction->ReadFunctionName();
	}
	else
	{
		DString superFunctionName(DString::EmptyString);
		outBuffer << superFunctionName;

		Int numNull = 0;
		for (size_t i = 0; i < InParams.size(); ++i)
		{
			if (InParams.at(i) == nullptr)
			{
				++numNull;
			}
		}

		Int numInParams(InParams.size() - numNull);
		outBuffer << numInParams;
		for (GraphVariable* inVar : InParams)
		{
			if (inVar != nullptr)
			{
				inVar->Serialize(OUT outBuffer);
			}
		}

		numNull = 0;
		for (size_t i = 0; i < OutParams.size(); ++i)
		{
			if (OutParams.at(i) == nullptr)
			{
				numNull++;
			}
		}

		Int numOutParams(OutParams.size() - numNull);
		outBuffer << numOutParams;
		for (GraphVariable* outVar : OutParams)
		{
			if (outVar != nullptr)
			{
				outVar->Serialize(OUT outBuffer);
			}
		}

		outBuffer << bHeader;
		outBuffer << bGlobal;
		outBuffer << bStatic;
		outBuffer << bConst;
		outBuffer << bProtected;
		outBuffer << bFinal;
		outBuffer << Comment;
	}
}

bool GraphFunction::Deserialize (const DataBuffer& incomingData)
{
	incomingData >> FunctionName;

	DString superFunctionName;
	incomingData >> superFunctionName;
	if (superFunctionName.IsEmpty())
	{
		SuperFunction = FindSuperFunction(superFunctionName);
	}
	else
	{
		Int numInParams;
		incomingData >> numInParams;

		for (size_t i = 0; i < InParams.size(); ++i)
		{
			if (InParams.at(i) != nullptr)
			{
				delete InParams.at(i);
				InParams.at(i) = nullptr;
			}
		}

		InParams.resize(numInParams.ToUnsignedInt());
		for (size_t i = 0; i < InParams.size(); ++i)
		{
			InParams.at(i) = new GraphVariable(HashedString(), this);
			InParams.at(i)->SetInspectorType(GraphVariable::IT_InParam);
			InParams.at(i)->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
		}

		for (GraphVariable* inVar : InParams)
		{
			if (!inVar->Deserialize(incomingData))
			{
				return false;
			}
		}

		Int numOutParams;
		incomingData >> numOutParams;

		for (size_t i = 0; i < OutParams.size(); ++i)
		{
			if (OutParams.at(i) != nullptr)
			{
				delete OutParams.at(i);
				OutParams.at(i) = nullptr;
			}
		}

		OutParams.resize(numOutParams.ToUnsignedInt());

		for (size_t i = 0; i < OutParams.size(); ++i)
		{
			OutParams.at(i) = new GraphVariable(HashedString(), this);
			OutParams.at(i)->SetInspectorType(GraphVariable::IT_OutParam);
			OutParams.at(i)->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphFunction, HandleParamDeleted, void, EditorInterface*));
		}

		for (GraphVariable* outVar : OutParams)
		{
			if (!outVar->Deserialize(incomingData))
			{
				return false;
			}
		}

		incomingData >> bHeader;
		incomingData >> bGlobal;
		incomingData >> bStatic;
		incomingData >> bConst;
		incomingData >> bProtected;
		incomingData >> bFinal;
		incomingData >> Comment;
	}

	return !incomingData.HasReadError();
}

void GraphFunction::FindLocalVariables (ScriptVariable::EVarType varType, std::vector<SVarInfo>& outLocalVars) const
{
	if (OwningAsset != nullptr && OwningAsset->GetLoadState() != GraphAsset::LS_Loaded)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot obtain local variables from %s, since the asset %s is not completely loaded."), ReadFunctionName(), OwningAsset->ReadAssetName());
		return;
	}

	if (FunctionDefinition == nullptr)
	{
		return;
	}

	//The GraphEditor contains the function definition, and the function definition, itself, is responsible for defining the local variables.
	for (ComponentIterator iter(FunctionDefinition.Get(), false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (VariableNode* node = dynamic_cast<VariableNode*>(iter.GetSelectedComponent()))
		{
			if (!node->IsLocalVariable())
			{
				continue;
			}

			if (varType != ScriptVariable::VT_Unknown && node->GetVarType() != varType)
			{
				continue;
			}

			bool varFound = false;
			for (const SVarInfo& localVar : outLocalVars)
			{
				if (localVar.VarId.Compare(node->ReadVariableId(), DString::CC_CaseSensitive) == 0)
				{
					varFound = true;
					break;
				}
			}

			if (varFound)
			{
				continue;
			}

			outLocalVars.emplace_back(node->ReadVariableId(), node->ReadVarDisplayName(), node->GetVarType());
		}
	}

	//Sort alphabetically
	std::sort(outLocalVars.begin(), outLocalVars.end(), [](const SVarInfo& a, const SVarInfo& b)
	{
		return (a.VarDisplay.Compare(b.VarDisplay, DString::CC_CaseSensitive) < 0);
	});
}

void GraphFunction::GetAllMemberVariables (GraphVariable::EVarPermissions permissions, bool staticOnly, ScriptVariable::EVarType varType, std::vector<GraphVariable*>& outMemberVariables) const
{
	for (GraphAsset* asset = OwningAsset; asset != nullptr; asset = asset->GetSuperAsset())
	{
		for (GraphVariable* var : asset->EditMemberVariables())
		{
			if (var == nullptr)
			{
				continue;
			}

			if (staticOnly && !var->IsStatic())
			{
				continue;
			}

			if (IsConst() && (permissions & GraphVariable::VP_Write) == 0)
			{
				//const functions do not have write access to this variable.
				continue;
			}

			if (varType != ScriptVariable::VT_Unknown && var->GetVarType() != varType)
			{
				continue;
			}

			outMemberVariables.push_back(var);
		}
	}
}

DString GraphFunction::GetPresentedName () const
{
	if (!DisplayName.IsEmpty())
	{
		return DisplayName;
	}

	return FunctionName;
}

DString GraphFunction::GetIdentifyingName () const
{
	if (OwningAsset != nullptr)
	{
		return DString::CreateFormattedString(TXT("Function'%s.%s'"), OwningAsset->ReadAssetName(), FunctionName);
	}

	return DString::CreateFormattedString(TXT("Function'%s'"), FunctionName);
}

bool GraphFunction::ParseFunctionId (const DString& functionId, DString& outClassName, DString& outFuncName)
{
	DString idPrefix = TXT("Function");
	DString regexStr;
	size_t numExpectedMatches;

	//If a period is provided, then the asset name exists
	bool ownerNameGiven = (functionId.Find('.', idPrefix.Length()) != INT_INDEX_NONE);
	if (ownerNameGiven)
	{
		/**
		idPrefix + ' - finds the text "Function'" literally.
		([\\w\\W]*) - Finds any character up to the first period found. The results of this will be stored in the outClassName variable. Extra slashes to escape the escape characters.
		\\. - Finds the period character literally.
		([\\w\\W]*) - Finds any character up to the last closing apostrophe. The results of this will be stored in the outFuncName variable.
		' - Matches the closing apostrophe. This is not saved in outVarName.
		*/
		regexStr = idPrefix + TXT("'([\\w\\W]*)\\.([\\w\\W]*)'");
		numExpectedMatches = 3;
	}
	else
	{
		regexStr = idPrefix + TXT("'([\\w\\W]*)'");
		numExpectedMatches = 2;
	}

	std::smatch matches;
	std::regex searchPattern(regexStr.ToCString(), std::regex_constants::icase);
	std::string stdId = functionId.ToStr();
	if (!std::regex_search(stdId, OUT matches, searchPattern) || matches.size() != numExpectedMatches)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to parse the function ID \"%s\" since that is not in the expected format \"%s'<Scope>.<Name>'\"."), functionId, idPrefix);
		return false;
	}

	if (ownerNameGiven)
	{
		outClassName = matches[1].str();
		outFuncName = matches[2].str();
	}
	else
	{
		outClassName = DString::EmptyString;
		outFuncName = matches[1].str();
	}

	return true;
}

void GraphFunction::SetFunctionName (const DString& newFunctionName)
{
	if (SuperFunction != nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to change the function name from %s to %s in a child function. A function signature can only be updated from the root function."), FunctionName, newFunctionName);
		GraphFunction& rootFunction = EditRootFunction();
		rootFunction.SetFunctionName(newFunctionName);
		return;
	}

	DString prevId = GetIdentifyingName();
	FunctionName = newFunctionName;
	UpdateEditorObjName();
	OnFunctionNameChanged.Broadcast(prevId, GetIdentifyingName(), FunctionName);

	UpdateSignature();
}

void GraphFunction::SetDisplayName (const DString& newDisplayName)
{
	if (SuperFunction != nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to change the function's display name from %s to %s in a child function. A function signature can only be updated from the root function."), DisplayName, newDisplayName);
		GraphFunction& rootFunction = EditRootFunction();
		rootFunction.SetDisplayName(newDisplayName);
		return;
	}

	DString id = GetIdentifyingName(); //The display name doesn't affect ID
	DisplayName = newDisplayName;
	OnFunctionNameChanged.Broadcast(id, id, DisplayName);
}

void GraphFunction::SetOwningAsset (GraphAsset* newOwningAsset)
{
	OwningAsset = newOwningAsset;
}

void GraphFunction::SetContextMenuCategory (const DString& newContextMenuCategory)
{
	EditRootFunction().ContextMenuCategory = newContextMenuCategory;
}

void GraphFunction::SetInParams (const std::vector<GraphVariable*>& newInParams)
{
	if (SuperFunction != nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to change the function signature from a child function. A function signature can only be updated from the root function."));
		GraphFunction& rootFunction = EditRootFunction();
		rootFunction.SetInParams(newInParams);
		return;
	}

	InParams = newInParams;
	UpdateSignature();
}

void GraphFunction::SetOutParams (const std::vector<GraphVariable*>& newOutParams)
{
	if (SuperFunction != nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to change the function signature from a child function. A function signature can only be updated from the root function."));
		GraphFunction& rootFunction = EditRootFunction();
		rootFunction.SetOutParams(newOutParams);
		return;
	}

	OutParams = newOutParams;

	UpdateSignature();
}

void GraphFunction::SetUniqueFlag (EUniqueFlag newUniqueFlag)
{
	EditRootFunction().UniqueFlag = newUniqueFlag;
}

void GraphFunction::SetVariableRelatedFunction (bool newVariableRelatedFunction)
{
	EditRootFunction().bVariableRelatedFunction = newVariableRelatedFunction;
}

void GraphFunction::SetExecPortVisibility (EExecPortVisibility newExecPortVisibility)
{
	EditRootFunction().ExecPortVisibility = newExecPortVisibility;
}

void GraphFunction::SetHeader (bool newHeader)
{
	EditRootFunction().bHeader = newHeader;
}

void GraphFunction::SetGlobal (bool newGlobal)
{
	EditRootFunction().bGlobal = newGlobal;
}

void GraphFunction::SetStatic (bool newStatic)
{
	EditRootFunction().bStatic = newStatic;
}

void GraphFunction::SetConst (bool newConst)
{
	EditRootFunction().bConst = newConst;
}

void GraphFunction::SetProtected (bool newProtected)
{
	EditRootFunction().bProtected = newProtected;
}

void GraphFunction::SetFinal (bool newFinal)
{
	EditRootFunction().bFinal = newFinal;
}

void GraphFunction::SetComment (const DString& newComment)
{
	EditRootFunction().Comment = newComment;
	OnCommentChanged.Broadcast(GetIdentifyingName(), newComment);
}

void GraphFunction::SetNativeAssociation (const Subroutine* newNativeAssociation)
{
	NativeAssociation = newNativeAssociation;
}

void GraphFunction::SetSuperFunction (GraphFunction* newSuperFunction)
{
	if (SuperFunction != nullptr)
	{
		ContainerUtils::RemoveItem(SuperFunction->SubFunctions, this);
	}

	//Ensure there isn't a circular link.
	for (GraphFunction* curFunction = newSuperFunction; curFunction != nullptr; curFunction = curFunction->SuperFunction)
	{
		if (curFunction == this)
		{
			EditorLog.Log(LogCategory::LL_Critical, TXT("Cannot assign (%s) as the super function for (%s). An infinite loop has been detected since (%s) is currently a super function of (%s)."), newSuperFunction->ToString(), ToString(), ToString(), newSuperFunction->ToString());
			return;
		}
	}

	SuperFunction = newSuperFunction;

	if (SuperFunction != nullptr)
	{
		SuperFunction->SubFunctions.push_back(this);
	}
}

void GraphFunction::SetFunctionDefinition (GraphEditor* newFunctionDefinition)
{
	if (FunctionDefinition != nullptr)
	{
		FunctionDefinition->SetOwningFunction(nullptr);
	}

	FunctionDefinition = newFunctionDefinition;
	if (FunctionDefinition != nullptr)
	{
		FunctionDefinition->SetOwningFunction(this);
	}
}

void GraphFunction::UpdateSignature ()
{
	if (FunctionDefinition != nullptr)
	{
		//TODO: Update input and output nodes
	}

	for (GraphFunction* subFunction : SubFunctions)
	{
		CHECK(subFunction != nullptr)
		subFunction->UpdateSignature();
	}

	bSignatureDirty = true;
}

GraphFunction* GraphFunction::FindSuperFunction (const DString& functionName) const
{
	if (OwningAsset == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to find super function %s since the GraphFunction is not associated with an OwningAsset. The function cannot find where %s is defined if there's no class chain."), functionName, functionName);
		return nullptr;
	}

	for (GraphAsset* asset = OwningAsset->GetSuperAsset(); asset != nullptr; asset = asset->GetSuperAsset()) //Keep climbing the class chain until we find the function with the exact name.
	{
		for (GraphFunction* assetFunction : asset->ReadOverrideFunctions())
		{
			if (functionName.Compare(assetFunction->ReadFunctionName(), DString::CC_CaseSensitive) == 0)
			{
				return assetFunction;
			}
		}

		for (GraphFunction* assetFunction : asset->ReadMemberFunctions())
		{
			if (functionName.Compare(assetFunction->ReadFunctionName(), DString::CC_CaseSensitive) == 0)
			{
				return assetFunction;
			}
		}
	}

	return nullptr;
}

void GraphFunction::InitFunctionDefinition ()
{
	CHECK(FunctionDefinition != nullptr)

	Vector2 placementLocation(-256.f, 0.f);
	placementLocation += (FunctionDefinition->ReadSize() * 0.5f);

	ExecNode* startNode = ExecNode::CreateObject();
	if (FunctionDefinition->AddComponent(startNode))
	{
		startNode->InitializeExecType(true);
		startNode->SetPosition(placementLocation - (startNode->ReadSize() * 0.5f));
		placementLocation.X += 512.f;

		ExecNode* endNode = ExecNode::CreateObject();
		if (FunctionDefinition->AddComponent(endNode))
		{
			endNode->InitializeExecType(false);
			endNode->SetPosition(placementLocation - (endNode->ReadSize() * 0.5f));

			if (!ContainerUtils::IsEmpty(startNode->ReadOutputPorts()) && !ContainerUtils::IsEmpty(endNode->ReadInputPorts()))
			{
				NodePort* startPort = startNode->ReadOutputPorts().at(0).Port;
				NodePort* endPort = endNode->ReadInputPorts().at(0).Port;
				if (startPort->CanAcceptConnectionsFrom(endPort, true))
				{
					startPort->EstablishConnectionTo(endPort);
				}
			}
		}
	}
}

void GraphFunction::HandleFunctionNameEdit (EditPropertyComponent* editedComp)
{
	EditableField* editField = dynamic_cast<EditableField*>(editedComp);
	if (editField == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to process function name edit since %s is not an EditableField component."), editedComp->GetPropertyNameStr());
		return;
	}

	SetFunctionName(editField->GetPropValue());

	if (OwningAsset != nullptr)
	{
		DString errorMsg;
		if (GraphCompiler::CheckFunctionName(*this, OUT errorMsg, {&OwningAsset->EditMemberFunctions()}) != GraphCompiler::CR_Success)
		{
			editedComp->ShowCaption(errorMsg, editedComp->ReadCaptionErrorColors());
		}
		else
		{
			editedComp->HideCaption();
		}
	}
}


void GraphFunction::HandleFunctionCommentEdit (EditPropertyComponent* editedComp)
{
	EditableDString* editString = dynamic_cast<EditableDString*>(editedComp);
	if (editString == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to process function comment edit since %s is not an EditableDString component."), editedComp->GetPropertyNameStr());
		return;
	}

	DString funcId = GetIdentifyingName();
	DString newComment = editString->GetPropValue();
	OnCommentChanged.Broadcast(funcId, newComment);
}

void GraphFunction::HandleOpenFunction (ButtonComponent* button, EditablePropCommand* propCmd)
{
	CHECK(propCmd != nullptr)

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	EditorLog.Log(LogCategory::LL_Log, TXT("Opening function graph: %s"), FunctionName);
	editorEngine->OnOpenFunction.Broadcast(this);
}

void GraphFunction::HandleFunctionGlobalToggle (EditPropertyComponent* editedComp)
{
	if (OwningAsset == nullptr)
	{
		return;
	}

	editedComp->HideCaption();

	if (!OwningAsset->IsGlobalAsset())
	{
		return;
	}

	if (EditableBool* editBool = dynamic_cast<EditableBool*>(editedComp))
	{
		if (CheckboxComponent* checkbox = editBool->GetCheckbox())
		{
			if (!checkbox->IsChecked())
			{
				//Cannot uncheck this option for global assets.
				TextTranslator* translator = TextTranslator::GetTranslator();
				CHECK(translator != nullptr)

				editedComp->ShowCaption(translator->TranslateText(TXT("ErrorFunctionMustBeGlobal"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("GraphFunction")), editedComp->ReadCaptionErrorColors());
				editBool->SetBoolValue(true, false);
			}
		}
	}
}

void GraphFunction::HandleParamDeleted (EditorInterface* obj)
{
	if (GraphVariable* deletedVar = dynamic_cast<GraphVariable*>(obj))
	{
		for (size_t i = 0; i < InParams.size(); ++i)
		{
			if (InParams.at(i) == deletedVar)
			{
				InParams.at(i) = nullptr;
				return;
			}
		}

		for (size_t i = 0; i < OutParams.size(); ++i)
		{
			if (OutParams.at(i) == deletedVar)
			{
				OutParams.at(i) = nullptr;
				return;
			}
		}
	}
}
SD_END