/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphViewport.cpp
=====================================================================
*/

#include "GraphEditor.h"
#include "GraphViewport.h"

IMPLEMENT_CLASS(SD::GraphViewport, SD::GuiComponent)
SD_BEGIN

void GraphViewport::InitProps ()
{
	Super::InitProps();

	GraphMouse = nullptr;
	UpdateTextureSizeTick = nullptr;

	CurSize = Vector2::ZERO_VECTOR;
	OverrideMouse = nullptr;
}

void GraphViewport::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	if (!CurSize.IsNearlyEqual(ReadCachedAbsSize()) && UpdateTextureSizeTick != nullptr)
	{
		/*
		We don't actually reconstruct the frame texture here since it's very probable this is called in a middle of a render cycle.
		We shouldn't destroy frame textures in a middle of a render cycle. Instead wait until the next Gui Tick to reconstruct it.
		*/
		UpdateTextureSizeTick->SetTicking(true);
		CurSize = ReadCachedAbsSize();
	}
}

void GraphViewport::BeginObject ()
{
	Super::BeginObject();

	UpdateTextureSizeTick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(UpdateTextureSizeTick))
	{
		UpdateTextureSizeTick->SetTickHandler(SDFUNCTION_1PARAM(this, GraphViewport, HandleUpdateTextureSize, void, Float));
		UpdateTextureSizeTick->SetTicking(false);
	}
}

void GraphViewport::ExecuteInput (const sf::Event& evnt)
{
	Super::ExecuteInput(evnt);

	if (Graph != nullptr)
	{
		Graph->ExecuteScrollableInterfaceInput(evnt);
	}
}

void GraphViewport::ExecuteText (const sf::Event& evnt)
{
	Super::ExecuteText(evnt);

	if (Graph != nullptr)
	{
		Graph->ExecuteScrollableInterfaceText(evnt);
	}
}

void GraphViewport::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	GraphMouse->SetOriginalMouse(mouse);

	const Vector2 mousePosition(Float(sfmlEvent.x), Float(sfmlEvent.y));

	//Relay mouse event to ViewedObject
	if (Graph != nullptr)
	{
		const Vector2 graphCoord = ToGraphCoordinates(mousePosition);
		GraphMouse->SetPosition(graphCoord);

		sf::Event::MouseMoveEvent frameMouseMove;
		frameMouseMove.x = graphCoord.X.ToInt().ToInt32();
		frameMouseMove.y = graphCoord.Y.ToInt().ToInt32();
		Graph->ExecuteScrollableInterfaceMouseMove(GraphMouse, frameMouseMove, deltaMove);
	}

	if (OverrideMouse == nullptr && RenderFrame != nullptr && RenderFrame->IsWithinBounds(mousePosition))
	{
		CHECK(mouse != nullptr)
		
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		if (const Texture* hoverTexture = localTexturePool->GetTexture(HashedString("Engine.Input.CursorPrecision")))
		{
			OverrideMouse = mouse;
			mouse->PushMouseIconOverride(hoverTexture, SDFUNCTION_2PARAM(this, GraphViewport, HandleIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}
	}
}

void GraphViewport::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (Graph != nullptr)
	{
		Vector2 graphCoord = ToGraphCoordinates(mouse->ReadPosition());
		sf::Event::MouseButtonEvent graphEvent;
		graphEvent.button = sfmlEvent.button;
		graphEvent.x = graphCoord.X.Value;
		graphEvent.y = graphCoord.Y.Value;

		Graph->ExecuteScrollableInterfaceMouseClick(GraphMouse, graphEvent, eventType);
	}
}

void GraphViewport::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	Super::ExecuteMouseWheelMove(mouse, sfmlEvent);

	if (Graph != nullptr)
	{
		Vector2 graphCoord = ToGraphCoordinates(mouse->ReadPosition());
		sf::Event::MouseWheelScrollEvent graphEvent;
		graphEvent.delta = sfmlEvent.delta;
		graphEvent.wheel = sfmlEvent.wheel;
		graphEvent.x = graphCoord.X.Value;
		graphEvent.y = graphCoord.Y.Value;

		Graph->ExecuteScrollableInterfaceMouseWheelMove(GraphMouse, graphEvent);
	}
}

bool GraphViewport::ExecuteConsumableInput (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableInput(evnt))
	{
		return true;
	}

	if (Graph != nullptr && Graph->ExecuteScrollableInterfaceConsumableInput(evnt))
	{
		return true;
	}

	return false;
}

bool GraphViewport::ExecuteConsumableText (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableText(evnt))
	{
		return true;
	}

	if (Graph != nullptr && Graph->ExecuteScrollableInterfaceConsumableText(evnt))
	{
		return true;
	}

	return false;
}

bool GraphViewport::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (Graph != nullptr && RenderFrame != nullptr && RenderFrame->IsWithinBounds(mouse->ReadPosition()))
	{
		Graph->SetInputCam(Cam.Get());

		Vector2 graphCoordinates = ToGraphCoordinates(mouse->ReadPosition());

		sf::Event::MouseButtonEvent graphEvent;
		graphEvent.button = sfmlEvent.button;
		graphEvent.x = graphCoordinates.X.Value;
		graphEvent.y = graphCoordinates.Y.Value;

		Graph->ExecuteScrollableInterfaceConsumableMouseClick(GraphMouse, graphEvent, eventType);
		return true; //Always consume input if it's within the RenderFrame.
	}

	return false;
}

bool GraphViewport::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::ExecuteConsumableMouseWheelMove(mouse, sfmlEvent))
	{
		return true;
	}

	if (Graph != nullptr && RenderFrame != nullptr && RenderFrame->IsWithinBounds(mouse->ReadPosition()))
	{
		Graph->SetInputCam(Cam.Get());

		Vector2 graphCoord = ToGraphCoordinates(mouse->ReadPosition());

		sf::Event::MouseWheelScrollEvent graphEvent;
		graphEvent.delta = sfmlEvent.delta;
		graphEvent.wheel = sfmlEvent.wheel;
		graphEvent.x = graphCoord.X.Value;
		graphEvent.y = graphCoord.Y.Value;

		Graph->ExecuteScrollableInterfaceConsumableMouseWheelMove(GraphMouse, graphEvent);
		return true;
	}

	return false;
}

Vector2 GraphViewport::FakeMouseToInnerCoordinates (const Vector2& outerMousePos) const
{
	if (Cam != nullptr && RenderFrame != nullptr)
	{
		Vector2 deltaPos = outerMousePos - ReadCachedAbsPosition();
		return deltaPos + (Cam->ReadPosition() - (Cam->GetZoomedExtents() * 0.5f));
	}

	return Vector2::ZERO_VECTOR;
}

Vector2 GraphViewport::FakeMouseToOuterCoordinates (const Vector2& innerMousePos) const
{
	if (Cam != nullptr && RenderFrame != nullptr)
	{
		Vector2 deltaPos = innerMousePos - (Cam->ReadPosition() - (Cam->GetZoomedExtents() * 0.5f));
		return RenderFrame->ReadCachedAbsPosition() + deltaPos;
	}

	return Vector2::ZERO_VECTOR;
}

void GraphViewport::InitializeComponents ()
{
	Super::InitializeComponents();

	ViewportTexture = RenderTexture::CreateObject();
	Cam = PlanarCamera::CreateObject();

	GuiDrawLayer* guiDrawLayer = GuiDrawLayer::CreateObject();
	guiDrawLayer->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
	ViewportTexture->EditDrawLayers().push_back(RenderTarget::SDrawLayerCamera(guiDrawLayer, Cam.Get()));

	ViewportTexture->SetDestroyCamerasOnDestruction(false); //The GraphViewport will take ownership over the camera.
	ViewportTexture->SetDestroyDrawLayersOnDestruction(true);

	GraphMouse = FakeMouse::CreateObject();
	GraphMouse->SetMouseOwner(this);

	RenderFrame = FrameComponent::CreateObject();
	if (AddComponent(RenderFrame))
	{
		RenderFrame->SetBorderThickness(1.f);
	}
}

void GraphViewport::Destroyed ()
{
	if (Cam != nullptr)
	{
		Cam->Destroy();
	}

	if (ViewportTexture != nullptr)
	{
		ViewportTexture->Destroy();
	}

	if (GraphMouse != nullptr)
	{
		GraphMouse->Destroy();
		GraphMouse = nullptr;
	}

	if (OverrideMouse != nullptr)
	{
		OverrideMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, GraphViewport, HandleIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		OverrideMouse = nullptr;
	}

	Super::Destroyed();
}

void GraphViewport::SetGraph (GraphEditor* newGraph)
{
	CHECK(ViewportTexture != nullptr)
	if (Graph != nullptr)
	{
		for (size_t i = 0; i < ViewportTexture->ReadDrawLayers().size(); ++i)
		{
			if (GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(ViewportTexture->ReadDrawLayers().at(i).Layer))
			{
				guiLayer->UnregisterMenu(Graph.Get());
				break;
			}
		}
	}

	Graph = newGraph;

	if (Graph != nullptr)
	{
		for (size_t i = 0; i < ViewportTexture->ReadDrawLayers().size(); ++i)
		{
			if (GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(ViewportTexture->ReadDrawLayers().at(i).Layer))
			{
				guiLayer->RegisterMenu(Graph.Get());
				break;
			}
		}
	}

	//Center the camera
	if (Cam != nullptr && Graph != nullptr)
	{
		Cam->SetPosition(Graph->ReadPosition() + (Graph->ReadSize() * 0.5f));
	}
}

Vector2 GraphViewport::ToGraphCoordinates (const Vector2& absCoordinates) const
{
	CHECK(Cam != nullptr && RenderFrame != nullptr)

	Vector2 relativePos = absCoordinates - RenderFrame->ReadCachedAbsPosition();

	Rotator dir;
	Vector3 graphLocation;
	Cam->CalculatePixelProjection(RenderFrame->ReadCachedAbsSize(), relativePos, OUT dir, OUT graphLocation);

	return graphLocation.ToVector2();
}

void GraphViewport::ReconstructRenderTexture ()
{
	if (ViewportTexture == nullptr || RenderFrame == nullptr)
	{
		return;
	}

	Vector2 newSize(RenderFrame->GetCachedAbsSize());
	if (newSize.X <= 0.f || newSize.Y <= 0.f)
	{
		return;
	}

	RenderTexture* newViewport = RenderTexture::CreateObject();
	newViewport->CreateResource(newSize.X.ToInt(), newSize.Y.ToInt(), false);
	ViewportTexture->TransferDrawLayersTo(newViewport);

	newViewport->SetDestroyCamerasOnDestruction(ViewportTexture->GetDestroyCamerasOnDestruction());
	newViewport->SetDestroyDrawLayersOnDestruction(ViewportTexture->GetDestroyDrawLayersOnDestruction());
	newViewport->ResetColor = ViewportTexture->ResetColor;
#ifdef DEBUG_MODE
	newViewport->DebugName = ViewportTexture->DebugName;
#endif

	RenderTexture* oldViewport = ViewportTexture.Get();
	ViewportTexture = newViewport;
	if (RenderFrame != nullptr)
	{
		RenderFrame->SetCenterTexture(newViewport);
	}

	if (Cam != nullptr)
	{
		Cam->SetViewExtents(ReadCachedAbsSize());
	}

	oldViewport->Destroy();
}

void GraphViewport::HandleUpdateTextureSize (Float deltaSec)
{
	ReconstructRenderTexture();

	if (UpdateTextureSizeTick != nullptr)
	{
		UpdateTextureSizeTick->SetTicking(false);
	}
}

bool GraphViewport::HandleIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	bool isHovering = (RenderFrame != nullptr && RenderFrame->IsWithinBounds(mouse->ReadPosition()));
	if (!isHovering)
	{
		OverrideMouse = nullptr;
	}

	return isHovering;
}
SD_END