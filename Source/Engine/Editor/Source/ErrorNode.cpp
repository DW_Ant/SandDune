/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ErrorNode.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "ErrorNode.h"

IMPLEMENT_CLASS(SD::ErrorNode, SD::GraphNode)
SD_BEGIN

void ErrorNode::InitProps ()
{
	Super::InitProps();

	ErrorType = ET_None;
	HeaderTooltip = nullptr;
}

void ErrorNode::SetErrorType (EErrorType newErrorType)
{
	ErrorType = newErrorType;
	if (HeaderText == nullptr)
	{
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	DString localizationKey;
	DString localizationKeyTooltip;
	const DString sectionName = TXT("ErrorNode");
	switch (ErrorType)
	{
		case(ET_None):
			localizationKey = DString::EmptyString;
			localizationKeyTooltip = DString::EmptyString;
			break;

		case(ET_StaleConnections):
			localizationKey = TXT("StaleConnections");
			localizationKeyTooltip = TXT("StaleConnectionsTooltip");
	}

	DString headerText = DString::EmptyString;
	DString tooltip = DString::EmptyString;
	if (!localizationKey.IsEmpty())
	{
		headerText = translator->TranslateText(localizationKey, EditorEngineComponent::LOCALIZATION_FILE_NAME, sectionName);
	}

	if (!localizationKeyTooltip.IsEmpty())
	{
		tooltip = translator->TranslateText(localizationKeyTooltip, EditorEngineComponent::LOCALIZATION_FILE_NAME, sectionName);
	}

	HeaderText->SetText(headerText);

	if (tooltip.IsEmpty())
	{
		if (HeaderTooltip != nullptr)
		{
			HeaderTooltip->Destroy();
			HeaderTooltip = nullptr;
		}

		return;
	}

	if (HeaderTooltip == nullptr)
	{
		HeaderTooltip = TooltipComponent::CreateObject();
		if (HeaderText->AddComponent(HeaderTooltip))
		{
			//Noop
		}
	}

	CHECK(HeaderTooltip != nullptr)
	HeaderTooltip->SetTooltipText(tooltip);
}
SD_END