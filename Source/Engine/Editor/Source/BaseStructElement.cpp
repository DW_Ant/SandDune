/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseStructElement.cpp
=====================================================================
*/

#include "BaseStructElement.h"
#include "EditableStruct.h"
#include "EditPropertyComponent.h"

SD_BEGIN
BaseStructElement::BaseStructElement (const DString& inPropName) :
	PropName(inPropName)
{
	//Noop
}

void BaseStructElement::InitializePropComponent (EditableStruct* attachTo, EditPropertyComponent* propComp) const
{
	CHECK(attachTo != nullptr && propComp != nullptr)

	if (attachTo->AddComponent(propComp))
	{
		if (LabelComponent* label = propComp->GetPropertyName())
		{
			label->SetText(PropName);
		}
	}
}
SD_END