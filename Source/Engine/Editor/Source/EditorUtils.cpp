/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorUtils.cpp
=====================================================================
*/

#include "EditorInterface.h"
#include "EditorUtils.h"
#include "EditPropertyComponent.h"
#include "InspectorList.h"

IMPLEMENT_ABSTRACT_CLASS(SD::EditorUtils, SD::BaseUtils)
SD_BEGIN

GuiEntity* EditorUtils::CreateInspector (EditorInterface* editedObj, Float lineSpacing)
{
	if (editedObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot create an inspector from a null object."));
		return nullptr;
	}

	GuiEntity* inspector = GuiEntity::CreateObject();
	inspector->SetAutoSizeVertical(true);
	inspector->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));

	VerticalList* list = VerticalList::CreateObject();
	if (inspector->AddComponent(list))
	{
		list->ComponentSpacing = lineSpacing;
		editedObj->AddInspectorCompsTo(list);
		list->RefreshComponentTransforms();
	}

	return inspector;
}

void EditorUtils::LoadFromConfig (ConfigWriter* config, const DString& sectionName, EditorInterface* loadingObj)
{
	if (config == nullptr || loadingObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load config data from section \"%s\" for a null object."), sectionName);
		return;
	}

	InspectorList propList;
	loadingObj->PopulateInspectorList(OUT propList);

	//Automatically add prefixes to property names to prevent name collisions.
	for (EditPropertyComponent::SInspectorProperty* prop : propList.ReadPropList())
	{
		if (prop->OwningStruct != nullptr)
		{
			//NOTE: Since the structs are listed before its subproperties, this line should automatically handle the recursion for nested structs.
			prop->PropertyName = prop->OwningStruct->PropertyName + TXT(".") + prop->PropertyName;
		}
	}

	for (auto prop : propList.ReadPropList())
	{
		//Note: The associated variable is already pointing to the interface owner's variable. Calling LoadConfig should write to those variables.
		prop->LoadConfig(config, sectionName);
	}
}

void EditorUtils::SaveToConfig (ConfigWriter* config, const DString& sectionName, EditorInterface* savingObj)
{
	if (config == nullptr || savingObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save config data to section \"%s\" from a null object."), sectionName);
		return;
	}

	InspectorList propList;
	savingObj->PopulateInspectorList(OUT propList);

	//Automatically add prefixes to property names to prevent name collisions.
	for (EditPropertyComponent::SInspectorProperty* prop : propList.ReadPropList())
	{
		if (prop->OwningStruct != nullptr)
		{
			//NOTE: Since the structs are listed before its subproperties, this line should automatically handle the recursion for nested structs.
			prop->PropertyName = prop->OwningStruct->PropertyName + TXT(".") + prop->PropertyName;
		}
	}


	for (auto prop : propList.ReadPropList())
	{
		prop->SaveConfig(config, sectionName);
	}
}

bool EditorUtils::LoadFromBuffer (const DataBuffer& incomingData, EditorInterface* loadingObj)
{
	if (loadingObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load binary data to a null object."));
		return false;
	}

	InspectorList propList;
	loadingObj->PopulateInspectorList(OUT propList);

	for (auto prop : propList.ReadPropList())
	{
		if (!prop->LoadBinary(incomingData))
		{
			return false;
		}
	}

	return true;
}

void EditorUtils::SaveToBuffer (DataBuffer& outData, EditorInterface* savingObj)
{
	if (savingObj == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save binary data from a null object."));
		return;
	}

	InspectorList propList;
	savingObj->PopulateInspectorList(OUT propList);

	for (auto prop : propList.ReadPropList())
	{
		prop->SaveBinary(OUT outData);
	}
}
SD_END