/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorWorkerEngineComponent.cpp
=====================================================================
*/

#include "AssetCatalogue.h"
#include "EditorWorkerEngineComponent.h"
#include "ReferenceUpdater.h"

#ifdef WITH_MULTI_THREAD
IMPLEMENT_ENGINE_COMPONENT(SD::EditorWorkerEngineComponent)
SD_BEGIN

EditorWorkerEngineComponent::EditorWorkerEngineComponent () : Super()
{
	//Noop
}

void EditorWorkerEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	WorkerCatalogue = AssetCatalogue::CreateObject();
	WorkerCatalogue->SetMaxUpdateInterval(10); //Send an update every 10 files.

	RefUpdater = ReferenceUpdater::CreateObject();
	RefUpdater->SetMaxUpdateInterval(10);
}

void EditorWorkerEngineComponent::ShutdownComponent ()
{
	if (WorkerCatalogue != nullptr)
	{
		WorkerCatalogue->Destroy();
		WorkerCatalogue = nullptr;
	}

	if (RefUpdater != nullptr)
	{
		RefUpdater->Destroy();
		RefUpdater = nullptr;
	}

	Super::ShutdownComponent();
}

void EditorWorkerEngineComponent::SetLatestCatalogue (const std::vector<FileAttributes>& newLatestCatalogue)
{
	LatestCatalogue = newLatestCatalogue;
}

SD_END
#endif