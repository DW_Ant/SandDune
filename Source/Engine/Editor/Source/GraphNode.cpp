/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphNode.cpp
=====================================================================
*/

#include "GraphEditor.h"
#include "GraphNode.h"
#include "NodePort.h"

IMPLEMENT_CLASS(SD::GraphNode, SD::GuiComponent)
SD_BEGIN

GraphNode::SInitPort::SInitPort (bool inIsInput, const DClass* inPortClass, const DString& inLabel) :
	bIsInput(inIsInput),
	PortClass(inPortClass),
	ScaleMultiplier(1.f),
	Label(inLabel),
	TooltipText(DString::EmptyString),
	InlinedComponentClass(nullptr)
{
	CHECK(PortClass != nullptr && PortClass->IsA(NodePort::SStaticClass()))
	CHECK(InlinedComponentClass == nullptr || InlinedComponentClass->IsA(GuiComponent::SStaticClass()));
}

GraphNode::SPort::SPort () :
	Port(nullptr),
	PortLabel(nullptr),
	PortInlineEdit(nullptr)
{
	//Noop
}

void GraphNode::InitProps ()
{
	Super::InitProps();

	//GraphNodes should not use fraction scaling so that it will not jump in the middle of the graph when the user drags it near the top or the left borders (when position axis ranges between 0 and 1).
	SetEnableFractionScaling(false);

	bAutoUpdateSize = true;
	PortVerticalSpacing = 12.f;
	PortHorizontalSpacing = 4.f;
	PortBaseSize = 12.f;
	MinSpacingBetweenInputAndOutput = 24.f;
	LabelFontSize = 10;
	LabelSpacing = 4.f;
	InlinedFieldWidth = 64.f;
	HighlightThickness = 4.f;
	HighlightColor = Color(200, 200, 32, 200);
	BackgroundColor = Color::WHITE;
	bSelected = false;
	bDragging = false;
	bDeletable = true;

	HeaderText = nullptr;

	PrevMousePress = Vector2::ZERO_VECTOR;
	MouseDragSelectThreshold = 4.f;
	MouseDragOffset = Vector2::ZERO_VECTOR;
	DoubleClickTime = -1.f;
}


void GraphNode::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const GraphNode* nodeTemplate = dynamic_cast<const GraphNode*>(objTemplate))
	{
		SetAutoUpdateSize(false);
		SetPortVerticalSpacing(nodeTemplate->GetPortVerticalSpacing());
		SetPortHorizontalSpacing(nodeTemplate->GetPortHorizontalSpacing());
		SetPortBaseSize(nodeTemplate->GetPortBaseSize());
		SetMinSpacingBetweenInputAndOutput(nodeTemplate->GetMinSpacingBetweenInputAndOutput());
		SetLabelFontSize(nodeTemplate->GetLabelFontSize());
		SetLabelSpacing(nodeTemplate->GetLabelSpacing());
		SetInlinedFieldWidth(nodeTemplate->GetInlinedFieldWidth());
		SetHighlightThickness(nodeTemplate->GetHighlightThickness());
		SetHighlightColor(nodeTemplate->GetHighlightColor());
		SetBackgroundColor(nodeTemplate->GetBackgroundColor());
		
		bool bCreatedObj;
		Background = ReplaceTargetWithObjOfMatchingClass(Background.Get(), nodeTemplate->Background.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Background);
		}

		if (Background != nullptr)
		{
			Background->CopyPropertiesFrom(nodeTemplate->Background.Get());
		}

		HeaderText = ReplaceTargetWithObjOfMatchingClass(HeaderText, nodeTemplate->HeaderText, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(HeaderText);
		}

		if (HeaderText != nullptr)
		{
			HeaderText->CopyPropertiesFrom(nodeTemplate->HeaderText);
		}

		SetAutoUpdateSize(true);
	}
}

void GraphNode::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (bDragging && sf::Mouse::isButtonPressed(sf::Mouse::Left) && !MousePointer::IsEngineMovingMouse())
	{
		if (!bIsMousePressDragAction)
		{
			bIsMousePressDragAction = (mouse->ReadPosition() - PrevMousePress).CalcDistSquared() >= Float::Pow(MouseDragSelectThreshold, 2.f);
		}

		if (InputBroadcaster::GetCtrlHeld())
		{
			SetPosition(mouse->ReadPosition() - MouseDragOffset);
		}
		else
		{
			//Update the drag offset (handles case where the user is dragging while tapping ctrl).
			MouseDragOffset = mouse->ReadPosition() - ReadCachedAbsPosition();
		}
	}
}

void GraphNode::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		return;
	}

	//Selecting out of bounds deselects this node.
	if (IsSelected() && !InputBroadcaster::GetCtrlHeld() && !bIsMousePressDragAction && eventType == sf::Event::MouseButtonReleased && !IsWithinBounds(mouse->ReadPosition()))
	{
		SetSelected(false);
	}

	if (eventType == sf::Event::MouseButtonPressed)
	{
		PrevMousePress = mouse->ReadPosition();
		bIsMousePressDragAction = false;
	}

	//Update drag offset even if it's out of bounds to handle dragging multiple nodes.
	if (bDragging)
	{
		MouseDragOffset = mouse->ReadPosition() - ReadCachedAbsPosition();
	}
}

bool GraphNode::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	bool isWithinBounds = IsWithinBounds(mouse->ReadPosition());
	if (!isWithinBounds)
	{
		DoubleClickTime = -1.f; //Not clicking on this node will reset the double click timer
	}

	bool bConsumeEvent = false;

	//Handle double click
	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonPressed && isWithinBounds)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (DoubleClickTime >= 0.f && localEngine->GetElapsedTime() - DoubleClickTime < mouse->GetDoubleClickThreshold())
		{
			ProcessDoubleClick();
			DoubleClickTime = -1.f;
			bConsumeEvent = true;
		}
		else
		{
			DoubleClickTime = localEngine->GetElapsedTime();
		}
	}

	//Right clicking this node selects it.
	if (sfmlEvent.button == sf::Mouse::Right && eventType == sf::Event::MouseButtonReleased && !bIsMousePressDragAction && isWithinBounds)
	{
		SetSelected(true);
		bConsumeEvent = true;
	}

	//Handle left click selection & drag
	if (sfmlEvent.button == sf::Mouse::Left && isWithinBounds)
	{
		/*
		Selection controls:
		If nothing is held, select node if pressed within bounds on release. Deselect node if released outside of bounds AND if the user didn't drag the mouse since press (cannot move mouse between press and release).
		If ctrl is held, Select node if pressed within bounds. On release, toggle selection if within bounds and the user did not drag the mouse since on press.
		Shift is for box selection, and that's handled in GraphViewport.
		Right click to select node is handled via consumable mouse click since it must be able to be intercepted (for example: right clicking a port will disconnect the port instead of adding node selection).
		*/
		EKeyboardState keyState = InputBroadcaster::GetKeyboardState();
		if ((keyState & (KS_Ctrl | KS_Shift)) == 0)
		{
			if (eventType == sf::Event::MouseButtonReleased && !bIsMousePressDragAction)
			{
				SetSelected(true);
				bConsumeEvent = true;
			}
		}
		else if ((keyState & (KS_Ctrl | KS_Shift)) == KS_Ctrl) //If ctrl is held, shift is not held down. If the user is not box selecting, but holding ctrl to toggle selections
		{
			if (!bIsMousePressDragAction && eventType == sf::Event::MouseButtonReleased)
			{
				SetSelected(!IsSelected());
				bConsumeEvent = true;
			}
		}

		//Update the dragging state
		bDragging = (IsSelected() && sf::Event::MouseButtonPressed);
	}

	return bConsumeEvent;
}

bool GraphNode::AllowCameraMovement (const Vector2& mousePos) const
{
	//Holding shift permits the camera AND the node(s) to be dragged around.
	if (InputBroadcaster::GetShiftHeld())
	{
		return true;
	}

	//If the user is dragging this node around, stop camera movement.
	return !(InputBroadcaster::GetCtrlHeld() && bDragging);
}

void GraphNode::ProcessMouseLock (const Vector2& oldMousePos, const Vector2& newMousePos, Float camZoom)
{
	//If shift is held, the node is moving with the camera, causing the mouse displacement to not move.
	if (IsSelected() && bDragging && !InputBroadcaster::GetShiftHeld())
	{
		//The locked mouse position is at the same pixel, but the node is moving, need to update the mouse drag offset to reflect the new distance between node and mouse.
		Vector2 displacement = (newMousePos - oldMousePos);
		if (!camZoom.IsCloseTo(0.f))
		{
			displacement /= camZoom;
		}

		MouseDragOffset += displacement;
	}
}

bool GraphNode::IsSelected () const
{
	return bSelected;
}

void GraphNode::SetSelected (bool newSelected)
{
	if (bSelected == newSelected)
	{
		return;
	}

	bSelected = newSelected;
	if (!bSelected)
	{
		//Handles case where the nonconsumable mouse click clears the selection. Stop this node from being dragged around.
		bDragging = false;

		if (PlanarTransform* ownerTransform = GetRelativeTo())
		{
			//Clamp the position to be within bounds of the owner to prevent the user from dragging nodes beyond camera range.
			Vector2 clampedPosition = Vector2::Clamp(ReadCachedAbsPosition(), Vector2::ZERO_VECTOR, ownerTransform->ReadCachedAbsSize() - ReadCachedAbsSize());
			if (!clampedPosition.IsNearlyEqual(ReadCachedAbsPosition(), 0.01f))
			{
				SetPosition(clampedPosition);
			}
		}
	}

	if (Background != nullptr)
	{
		if (BorderRenderComponent* borderComp = Background->GetBorderComp())
		{
			Color borderColor(HighlightColor);
			if (!IsSelected())
			{
				borderColor.Source.a = 0;
			}

			borderComp->SetBorderColor(borderColor);
		}
	}
}

bool GraphNode::IsDeletable () const
{
	return bDeletable;
}

void GraphNode::DeleteEditorObject ()
{
	Destroy();
}

void GraphNode::InitializeComponents ()
{
	Super::InitializeComponents();

	Background = FrameComponent::CreateObject();
	if (AddComponent(Background))
	{
		Background->SetPosition(Vector2::ZERO_VECTOR);
		Background->SetSize(Vector2(1.f, 1.f));
		Background->SetBorderThickness(HighlightThickness);
		Background->SetLockedFrame(true);

		if (BackgroundColor != Color::WHITE)
		{
			Background->SetColorMultiplier(BackgroundColor);
		}

		if (BorderRenderComponent* borderComp = Background->GetBorderComp())
		{
			borderComp->SetBorderTexture(nullptr);
			Color borderColor(HighlightColor);
			if (!IsSelected())
			{
				borderColor.Source.a = 0; //Make the borders invisible while still occupying space.
			}

			borderComp->SetBorderColor(borderColor);
		}

		HeaderText = LabelComponent::CreateObject();
		if (Background->AddComponent(HeaderText))
		{
			HeaderText->SetAutoRefresh(false);
			HeaderText->SetPosition(Vector2(0.f, HighlightThickness));
			HeaderText->EditSize().X = 1.f;
			HeaderText->SetAutoSizeVertical(true);
			HeaderText->SetWrapText(false);
			HeaderText->SetClampText(false);
			HeaderText->SetHorizontalAlignment(LabelComponent::HA_Center);
			if (FrameComponent* headerBackground = HeaderText->GetBackgroundComponent())
			{
				headerBackground->SetVisibility(true);
				headerBackground->SetCenterColor(Color(150, 150, 150, 128));
				headerBackground->SetBorderThickness(0.f);
				if (BorderRenderComponent* headerBorder = headerBackground->GetBorderComp())
				{
					headerBorder->Destroy();
				}
			}

			HeaderText->SetAutoRefresh(true);
		}
	}
}

GraphNode::SPort* GraphNode::CreatePort (const SInitPort& initPort)
{
	CHECK(initPort.PortClass != nullptr)

	const NodePort* portClass = dynamic_cast<const NodePort*>(initPort.PortClass->GetDefaultObject());
	if (portClass == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to create port since %s is not a port class."), initPort.PortClass->ToString());
		return nullptr;
	}

	EntityComponent* compOwner = Background.Get();
	if (compOwner == nullptr)
	{
		compOwner = this;
	}

	Vector2 position(0.f, HighlightThickness); //position.x is zero since the components will be relying on anchoring.
	if (HeaderText != nullptr)
	{
		position.Y += HeaderText->ReadSize().Y;
	}

	std::vector<SPort>& portList = (initPort.bIsInput) ? InputPorts : OutputPorts;
	for (const SPort& otherPort : portList)
	{
		if (otherPort.PortLabel != nullptr)
		{
			position.Y += otherPort.PortLabel->ReadSize().Y + LabelSpacing;
		}

		if (otherPort.Port != nullptr)
		{
			position.Y += otherPort.Port->ReadSize().Y + PortVerticalSpacing;
		}
	}

	SPort& newPortData = portList.emplace_back();

	if (!initPort.Label.IsEmpty())
	{
		LabelComponent* newLabel = LabelComponent::CreateObject();
		if (compOwner->AddComponent(newLabel))
		{
			newLabel->SetAutoRefresh(false);
			newLabel->SetPosition(position);
			newLabel->SetSize(Vector2(0.f, LabelFontSize.ToFloat()));
			newLabel->SetAutoSizeHorizontal(true);
			newLabel->SetEnableFractionScaling(false); //Disable fraction scaling since this component may determine the node's size.
			newLabel->SetCharacterSize(LabelFontSize);
			position.Y += LabelFontSize.ToFloat() + LabelSpacing;
			newLabel->SetWrapText(false);
			
			if (initPort.bIsInput)
			{
				newLabel->SetAnchorLeft(PortHorizontalSpacing + HighlightThickness);
				newLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
			}
			else
			{
				newLabel->SetAnchorRight(PortHorizontalSpacing + HighlightThickness);
				newLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
			}

			newLabel->SetVerticalAlignment(LabelComponent::VA_Bottom);
			newLabel->SetText(initPort.Label);
			newLabel->SetAutoRefresh(true);
			newPortData.PortLabel = newLabel;

			if (!initPort.TooltipText.IsEmpty())
			{
				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (newLabel->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(initPort.TooltipText);
				}
			}
		}
	}

	NodePort* newPort = portClass->CreateObjectOfMatchingClass();
	CHECK(newPort != nullptr)
	if (compOwner->AddComponent(newPort))
	{
		newPort->SetPosition(position);
		newPort->SetSize(Vector2(PortBaseSize, PortBaseSize) * initPort.ScaleMultiplier);
		newPort->SetEnableFractionScaling(false); //Disable fraction scaling since this component may determine the node's size.
		newPort->OnConnectionChanged = SDFUNCTION_1PARAM(this, GraphNode, HandlePortConnectionChanged, void, NodePort*);

		if (initPort.bIsInput)
		{
			newPort->SetAnchorLeft(PortHorizontalSpacing + HighlightThickness); //Align left
			newPort->SetPortType(NodePort::PT_Input);
		}
		else
		{
			newPort->SetAnchorRight(PortHorizontalSpacing + HighlightThickness); //Align right
			newPort->SetPortType(NodePort::PT_Output);
		}

		newPortData.Port = newPort;
	}

	if (initPort.InlinedComponentClass != nullptr)
	{
		const GuiComponent* guiCdo = dynamic_cast<const GuiComponent*>(initPort.InlinedComponentClass->GetDefaultObject());
		if (guiCdo == nullptr)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate an inlined component to graph node %s (Port with label = \"%s\"). The given class (%s) is not a GuiComponent that can be instantiated."), ToString(), initPort.Label, initPort.InlinedComponentClass->ToString());
		}
		else
		{
			GuiComponent* inlinedComp = guiCdo->CreateObjectOfMatchingClass();
			if (compOwner->AddComponent(inlinedComp))
			{
				inlinedComp->SetPosition(position);
				inlinedComp->SetSize(Vector2(InlinedFieldWidth, PortBaseSize * initPort.ScaleMultiplier));
				inlinedComp->SetEnableFractionScaling(false); //Disable fraction scaling since this component may determine the node's size.

				Float anchorDist = (PortHorizontalSpacing * 2.f) + (PortBaseSize * initPort.ScaleMultiplier) + HighlightThickness;
				if (initPort.bIsInput)
				{
					inlinedComp->SetAnchorLeft(anchorDist);
				}
				else
				{
					inlinedComp->SetAnchorRight(anchorDist);
				}

				newPortData.PortInlineEdit = inlinedComp;
			}
		}
	}

	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}

	return &newPortData;
}

bool GraphNode::RemovePort (bool isInputPort, size_t portIdx)
{
	std::vector<SPort>& affectedList = (isInputPort) ? InputPorts : OutputPorts;
	bool bSuccess = false;

	if (ContainerUtils::IsValidIndex(affectedList, portIdx))
	{
		Float deltaY = 0.f;

		SPort& portData = affectedList.at(portIdx);
		if (portData.PortLabel != nullptr)
		{
			deltaY = portData.PortLabel->ReadSize().Y + LabelSpacing;
			portData.PortLabel->Destroy();
		}

		if (portData.Port != nullptr)
		{
			deltaY += portData.Port->ReadSize().Y + PortVerticalSpacing;
			portData.Port->Destroy();
		}

		if (portData.PortInlineEdit != nullptr)
		{
			portData.PortInlineEdit->Destroy();
		}

		affectedList.erase(affectedList.begin() + portIdx);
		bSuccess = true;

		for (size_t i = portIdx; i < affectedList.size(); ++i)
		{
			SPort& portData = affectedList.at(i);
			if (portData.PortLabel != nullptr)
			{
				portData.PortLabel->EditPosition().Y -= deltaY;
			}

			if (portData.Port != nullptr)
			{
				portData.Port->EditPosition().Y -= deltaY;
			}

			if (portData.PortInlineEdit != nullptr)
			{
				portData.PortInlineEdit->EditPosition().Y -= deltaY;
			}
		}

		if (bAutoUpdateSize)
		{
			UpdateNodeSize();
		}
	}

	return bSuccess;
}

GraphFunction* GraphNode::GetConstructedFunction () const
{
	if (GraphEditor* owningEditor = dynamic_cast<GraphEditor*>(GetRootEntity()))
	{
		return owningEditor->GetOwningFunction();
	}

	return nullptr;
}

void GraphNode::SetAutoUpdateSize (bool isAutoUpdateSize)
{
	if (bAutoUpdateSize == isAutoUpdateSize)
	{
		return;
	}

	bAutoUpdateSize = isAutoUpdateSize;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetPortVerticalSpacing (Float newPortVerticalSpacing)
{
	if (PortVerticalSpacing.IsCloseTo(newPortVerticalSpacing))
	{
		return;
	}

	PortVerticalSpacing = newPortVerticalSpacing;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetPortHorizontalSpacing (Float newPortHorizontalSpacing)
{
	if (PortHorizontalSpacing.IsCloseTo(newPortHorizontalSpacing))
	{
		return;
	}

	PortHorizontalSpacing = newPortHorizontalSpacing;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetPortBaseSize (Float newPortBaseSize)
{
	PortBaseSize = newPortBaseSize;
	//Don't bother updating existing port sizes primarily because the scalar multipliers used to create those ports are lost.
}

void GraphNode::SetMinSpacingBetweenInputAndOutput (Float newMinSpacingBetweenInputAndOutput)
{
	if (MinSpacingBetweenInputAndOutput.IsCloseTo(newMinSpacingBetweenInputAndOutput))
	{
		return;
	}

	MinSpacingBetweenInputAndOutput = newMinSpacingBetweenInputAndOutput;
	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}
}

void GraphNode::SetLabelFontSize (Int newLabelFontSize)
{
	if (LabelFontSize == newLabelFontSize)
	{
		return;
	}

	LabelFontSize = newLabelFontSize;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetLabelSpacing (Float newLabelSpacing)
{
	if (LabelSpacing.IsCloseTo(newLabelSpacing))
	{
		return;
	}

	LabelSpacing = newLabelSpacing;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetInlinedFieldWidth (Float newInlinedFieldWidth)
{
	if (InlinedFieldWidth.IsCloseTo(newInlinedFieldWidth))
	{
		return;
	}

	InlinedFieldWidth = newInlinedFieldWidth;
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetHighlightThickness (Float newHighlightThickness)
{
	HighlightThickness = newHighlightThickness;
	if (Background != nullptr)
	{
		if (BorderRenderComponent* borderComp = Background->GetBorderComp())
		{
			borderComp->SetBorderThickness(HighlightThickness);
		}
	}

	if (HeaderText != nullptr)
	{
		HeaderText->SetPosition(HighlightThickness, HighlightThickness);
	}

	//Need to update component positions since the highlight thickness influences the node's spacing to the borders.
	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

void GraphNode::SetHighlightColor (Color newHighlightColor)
{
	HighlightColor = newHighlightColor;
	if (Background != nullptr)
	{
		if (BorderRenderComponent* borderComp = Background->GetBorderComp())
		{
			Color borderColor(HighlightColor);
			if (!IsSelected())
			{
				borderColor.Source.a = 0;
			}

			borderComp->SetBorderColor(borderColor);
		}
	}
}

void GraphNode::SetBackgroundColor (Color newBackgroundColor)
{
	if (BackgroundColor == newBackgroundColor)
	{
		return;
	}

	BackgroundColor = newBackgroundColor;

	if (Background != nullptr)
	{
		Background->SetColorMultiplier(BackgroundColor);
	}
}

void GraphNode::SetDeletable (bool newDeletable)
{
	bDeletable = newDeletable;
}

void GraphNode::SetHeaderText (const DString& newHeaderText)
{
	if (HeaderText == nullptr)
	{
		return;
	}

	HeaderText->SetText(newHeaderText);

	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}
}

void GraphNode::RefreshNodeComponents ()
{
	RefreshNodeComponents(InputPorts, true);
	RefreshNodeComponents(OutputPorts, false);

	UpdateNodeSize();
}

void GraphNode::RefreshNodeComponents (const std::vector<SPort>& portList, bool bIsInputList)
{
	Float posY = HighlightThickness;

	if (HeaderText != nullptr)
	{
		posY += HeaderText->ReadSize().Y;
	}

	for (const SPort& port : portList)
	{
		if (port.PortLabel != nullptr)
		{
			port.PortLabel->SetAutoRefresh(false);
			if (bIsInputList)
			{
				port.PortLabel->SetAnchorLeft(PortHorizontalSpacing + HighlightThickness);
			}
			else
			{
				port.PortLabel->SetAnchorRight(PortHorizontalSpacing + HighlightThickness);
			}

			port.PortLabel->EditPosition().Y = posY;
			port.PortLabel->EditSize().Y = LabelFontSize.ToFloat();
			port.PortLabel->SetCharacterSize(LabelFontSize);
			port.PortLabel->SetAutoRefresh(true);
			posY += LabelFontSize.ToFloat() + LabelSpacing;
		}

		if (port.Port != nullptr)
		{
			port.Port->EditPosition().Y = posY;
			if (bIsInputList)
			{
				port.Port->SetAnchorLeft(PortHorizontalSpacing + HighlightThickness);
			}
			else
			{
				port.Port->SetAnchorRight(PortHorizontalSpacing + HighlightThickness);
			}

			if (port.PortInlineEdit != nullptr)
			{
				port.PortInlineEdit->EditPosition().Y = posY;
				port.PortInlineEdit->EditSize().X = InlinedFieldWidth;
				Float anchorDist = (PortHorizontalSpacing * 2.f) + port.Port->ReadSize().X + HighlightThickness;
				if (bIsInputList)
				{
					port.PortInlineEdit->SetAnchorLeft(anchorDist);
				}
				else
				{
					port.PortInlineEdit->SetAnchorRight(anchorDist);
				}
			}

			posY += port.Port->ReadSize().Y;
		}

		posY += PortVerticalSpacing;
	}
}

void GraphNode::RemovePortComponents ()
{
	for (SPort& inputData : InputPorts)
	{
		if (inputData.PortLabel != nullptr)
		{
			inputData.PortLabel->Destroy();
		}

		if (inputData.Port != nullptr)
		{
			inputData.Port->Destroy();
		}

		if (inputData.PortInlineEdit != nullptr)
		{
			inputData.PortInlineEdit->Destroy();
		}
	}

	for (SPort& outputData : OutputPorts)
	{
		if (outputData.PortLabel != nullptr)
		{
			outputData.PortLabel->Destroy();
		}

		if (outputData.Port != nullptr)
		{
			outputData.Port->Destroy();
		}

		if (outputData.PortInlineEdit != nullptr)
		{
			outputData.PortInlineEdit->Destroy();
		}
	}

	ContainerUtils::Empty(OUT InputPorts);
	ContainerUtils::Empty(OUT OutputPorts);

	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}
}

void GraphNode::UpdateNodeSize ()
{
	Vector2 minSize(Vector2::ZERO_VECTOR);
	CalcNodeSize(OUT minSize);
	minSize = Vector2::Max(Vector2(32.f, 32.f), minSize);

	SetSize(minSize);
	if (HeaderText != nullptr)
	{
		HeaderText->EditSize().X = minSize.X - (HighlightThickness * 2.f);
	}
}

void GraphNode::CalcNodeSize (Vector2& outMinSize) const
{
	size_t maxSize = Utils::Max(InputPorts.size(), OutputPorts.size());

	if (HeaderText != nullptr)
	{
		//Calculate the text width
		Float longestLine = 0.f;
		const Font* headerFont = HeaderText->GetFont();
		if (headerFont != nullptr)
		{
			for (const DString& headerLine : HeaderText->ReadContent())
			{
				longestLine = Utils::Max(longestLine, headerFont->CalculateStringWidth(headerLine, HeaderText->GetCharacterSize()));
			}
		}

		outMinSize.X += Utils::Max(outMinSize.X, longestLine + (HighlightThickness * 2.f));
		outMinSize.Y += HeaderText->ReadSize().Y;
	}

	outMinSize.Y += (HighlightThickness * 2.f);

	for (size_t i = 0; i < maxSize; ++i)
	{
		Float rowWidth = 0.f;
		Float rowHeight = 0.f;
		if (i < InputPorts.size())
		{
			const SPort& input = InputPorts.at(i);
			if (input.PortLabel != nullptr)
			{
				rowWidth = input.PortLabel->ReadSize().X + PortHorizontalSpacing + HighlightThickness;
				rowHeight = input.PortLabel->ReadSize().Y + LabelSpacing;
			}

			if (input.Port != nullptr)
			{
				Float inlineWidth = 0.f;
				if (input.PortInlineEdit != nullptr)
				{
					inlineWidth = PortHorizontalSpacing + input.PortInlineEdit->ReadSize().X;
				}

				//The width is equal to whichever line is greater: The label or the (Port + InlineField)
				rowWidth = Utils::Max(input.Port->ReadSize().X + PortHorizontalSpacing + HighlightThickness + inlineWidth, rowWidth);
				rowHeight += input.Port->ReadSize().Y;
			}
		}

		if (i < OutputPorts.size())
		{
			Float outputWidth = 0.f;
			Float outputHeight = 0.f;

			const SPort& output = OutputPorts.at(i);
			if (output.PortLabel != nullptr)
			{
				outputWidth = output.PortLabel->ReadSize().X + PortHorizontalSpacing + HighlightThickness;
				outputHeight = output.PortLabel->ReadSize().Y + LabelSpacing;
			}

			if (output.Port != nullptr)
			{
				Float inlineWidth = 0.f;
				if (output.PortInlineEdit != nullptr)
				{
					inlineWidth = output.PortInlineEdit->ReadSize().X + PortHorizontalSpacing + HighlightThickness;
				}

				//The width is equal to whichever line is greater: The label or the (Port + InlineField)
				outputWidth = Utils::Max(output.Port->ReadSize().X + PortHorizontalSpacing + HighlightThickness + inlineWidth, outputWidth);
				outputHeight += output.Port->ReadSize().Y;
			}

			rowWidth += outputWidth;
			rowHeight = Utils::Max(rowHeight, outputHeight);
		}

		//Add a buffer between input and output components
		rowWidth += MinSpacingBetweenInputAndOutput;
		outMinSize.X = Utils::Max(outMinSize.X, rowWidth);

		if (i != maxSize - 1) //Don't add extra space to the last row.
		{
			rowHeight += GetPortVerticalSpacing();
		}

		outMinSize.Y += rowHeight;
	}
}

void GraphNode::ProcessDoubleClick ()
{
	//Noop
}

void GraphNode::ProcessPortConnectionChanged (SPort* portData)
{
	CHECK(portData != nullptr)
	if (portData->PortInlineEdit != nullptr && portData->Port != nullptr)
	{
		//The inlined field becomes visible if nothing is connected to the port.
		portData->PortInlineEdit->SetVisibility(ContainerUtils::IsEmpty(portData->Port->ReadConnections()));
	}
}

void GraphNode::HandlePortConnectionChanged (NodePort* changedPort)
{
	//Find the collection of components associated with the changed port.
	SPort* relevantPortData = nullptr;
	for (SPort& inputData : InputPorts)
	{
		if (inputData.Port == changedPort)
		{
			relevantPortData = &inputData;
			break;
		}
	}

	if (relevantPortData == nullptr)
	{
		for (SPort& outputData : OutputPorts)
		{
			if (outputData.Port == changedPort)
			{
				relevantPortData = &outputData;
				break;
			}
		}
	}

	if (relevantPortData != nullptr)
	{
		ProcessPortConnectionChanged(relevantPortData);
	}
}
SD_END