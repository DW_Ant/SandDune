/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableDString.cpp
=====================================================================
*/

#include "EditableDString.h"

IMPLEMENT_CLASS(SD::EditableDString, SD::EditableField)
SD_BEGIN

EditableDString::SInspectorString::SInspectorString (const DString& inPropertyName, const DString& inTooltipText, DString* inAssociatedVariable, const DString& inDefaultString) : SInspectorProperty(inPropertyName, inTooltipText),
	AssociatedVariable(inAssociatedVariable),
	PermittedChars(DString::EmptyString),
	bMultiLine(false),
	MaxCharacters(0),
	DefaultString(inDefaultString),
	InitOverride(inDefaultString)
{
	//Noop
}

void EditableDString::SInspectorString::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString configStr = config->GetPropertyText(sectionName, PropertyName);
	if (configStr.IsEmpty())
	{
		*AssociatedVariable = DefaultString;
	}
	else
	{
		*AssociatedVariable = configStr;
	}
}

void EditableDString::SInspectorString::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to config since there isn't a variable associated with it."), PropertyName);
		return;
	}

	config->SavePropertyText(sectionName, PropertyName, *AssociatedVariable);
}

bool EditableDString::SInspectorString::LoadBinary (const DataBuffer& incomingData)
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorString is not associated with a variable."), PropertyName);
		return false;
	}

	if (!incomingData.CanReadBytes(DString::SGetMinBytes()))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary. Not enough bytes in the data buffer."), PropertyName);
		return false;
	}

	if ((incomingData >> *AssociatedVariable).HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load %s from binary. The data is malformed, preventing it from reading a DString."), PropertyName);
		return false;
	}

	return true;
}

void EditableDString::SInspectorString::SaveBinary (DataBuffer& outData) const
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to binary since it's not associated with any variable."), PropertyName);
		return;
	}

	outData << *AssociatedVariable;
}

EditPropertyComponent* EditableDString::SInspectorString::CreateBlankEditableComponent () const
{
	return EditableDString::CreateObject();
}

void EditableDString::SInspectorString::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableDString* stringComp = dynamic_cast<EditableDString*>(AssociatedComp))
	{
		stringComp->BindVariable(AssociatedVariable);
		stringComp->SetPermittedChars(PermittedChars);

		ELineType lineType = (bMultiLine) ? ELineType::LT_MultiLine : ELineType::LT_SingleLine;
		stringComp->SetLineType(lineType);
		if (bMultiLine)
		{
			stringComp->SetLineHeight(stringComp->GetLineHeight() * 3.f);
		}

		stringComp->SetDefaultValue(DefaultString);
		if (stringComp->GetPropValueField() != nullptr)
		{
			stringComp->GetPropValueField()->MaxNumCharacters = MaxCharacters;
		}

		if (TextFieldComponent* textComp = stringComp->GetPropValueField())
		{
			textComp->SetSingleLine(!bMultiLine);

			if (AssociatedVariable == nullptr)
			{
				if (InitType == IIV_UseOverride)
				{
					textComp->SetText(InitOverride);
				}
				else
				{
					textComp->SetText(DefaultString);
				}
			}
		}
	}
}

void EditableDString::InitProps ()
{
	Super::InitProps();

	PermittedChars = DString::EmptyString;
	EditedString = nullptr;
	DefaultString = DString::EmptyString;

	UpdateStringTick = nullptr;
	MultiLineScrollbar = nullptr;
}

void EditableDString::BeginObject ()
{
	Super::BeginObject();

	UpdateStringTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateStringTick))
	{
		UpdateStringTick->SetTickInterval(0.2f); //Not important to refresh continuously.
		UpdateStringTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableDString, HandleUpdateStringTick, void, Float));
	}
}

void EditableDString::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableDString* stringTemplate = dynamic_cast<const EditableDString*>(objTemplate))
	{
		SetPermittedChars(stringTemplate->PermittedChars);
		SetNormalBackgroundColor(stringTemplate->NormalBackgroundColor);
		SetReadOnlyBackgroundColor(stringTemplate->ReadOnlyBackgroundColor);
	}
}

bool EditableDString::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (MultiLineScrollbar != nullptr && PropValueField.IsValid() && PropValueField->GetCursorPosition() < 0)
	{
		//If the user clicked within the MultiLineScrollbar, jump the cursor to the text field component
		if (sfmlEvent.button == sf::Mouse::Button::Left && eventType == sf::Event::EventType::MouseButtonReleased)
		{
			if (IsWithinEditingField(Vector2(sfmlEvent.x, sfmlEvent.y)))
			{
				PropValueField->SetCursorPosition(SD_MAXINT); //Jump to end
				return true;
			}
		}
	}

	return false;
}

void EditableDString::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateStringTick != nullptr)
	{
		UpdateStringTick->Destroy();
		UpdateStringTick = nullptr;
	}
}

void EditableDString::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (EditedString != nullptr)
	{
		*EditedString = DefaultString;
	}
	else if (PropValueField.IsValid())
	{
		PropValueField->SetText(DefaultString);
	}

	MarkPropertyTextDirty();
	ProcessApplyEdit(this);
}

void EditableDString::UnbindVariable ()
{
	Super::UnbindVariable();

	BindVariable(nullptr);
}

ButtonComponent* EditableDString::AddFieldControl ()
{
	ButtonComponent* result = Super::AddFieldControl();

	if (MultiLineScrollbar != nullptr && FieldBackground != nullptr)
	{
		MultiLineScrollbar->SetAnchorRight(FieldBackground->GetAnchorRightDist());
	}

	return result;
}

void EditableDString::CopyToBuffer (DataBuffer& copyTo) const
{
	Super::CopyToBuffer(OUT copyTo);

	if (PropValueField != nullptr)
	{
		DString text = PropValueField->GetContent();
		copyTo << text;
	}
}

bool EditableDString::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	DString text;
	if ((incomingData >> text).HasReadError())
	{
		return false;
	}

	if (PropValueField != nullptr)
	{
		PropValueField->SetText(text);
	}

	if (EditedString != nullptr)
	{
		(*EditedString) = text;
	}

	return true;
}

void EditableDString::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	LocalString = DString::EmptyString;
	if (PropValueField != nullptr)
	{
		LocalString = PropValueField->GetContent();
	}

	EditedString = &LocalString;
}

EditPropertyComponent::SInspectorProperty* EditableDString::CreateDefaultInspector (const DString& propName) const
{
	return new SInspectorString(propName, DString::EmptyString, nullptr);
}

void EditableDString::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (MultiLineScrollbar != nullptr && FieldBackground != nullptr)
	{
		MultiLineScrollbar->SetAnchorLeft(FieldBackground->GetAnchorLeftDist());
	}
}

void EditableDString::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	if (MultiLineScrollbar != nullptr)
	{
		MultiLineScrollbar->EditSize().Y = LineHeight;
	}
}

void EditableDString::InitializeComponents ()
{
	Super::InitializeComponents();

	if (PropValueField != nullptr)
	{
		PropValueField->SetWrapText(true);
	}
}

bool EditableDString::IsWithinEditingField (const Vector2& mousePos) const
{
	if (MultiLineScrollbar != nullptr && MultiLineScrollbar->GetFrameSpriteTransform() != nullptr)
	{
		return MultiLineScrollbar->GetFrameSpriteTransform()->IsWithinBounds(mousePos);
	}

	//run single line case
	return Super::IsWithinEditingField(mousePos);
}

void EditableDString::ApplyEdits ()
{
	if (PropValueField.IsValid() && !IsReadOnly() && EditedString != nullptr)
	{
		*EditedString = PropValueField->GetContent();
		if (ResetDefaultsButton.IsValid())
		{
			ResetDefaultsButton->SetEnabled(DefaultString.Compare(*EditedString, DString::CC_CaseSensitive) != 0);
			if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
			{
				tooltipEntity->HideTooltip();
			}
		}
		else if (ResetDefaultsButton.IsValid())
		{
			DString newEdit;
			newEdit = PropValueField->GetContent();
			ResetDefaultsButton->SetEnabled(DefaultString != newEdit);
			if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
			{
				tooltipEntity->HideTooltip();
			}
		}
	}
}

DString EditableDString::GetPermittedInputChars () const
{
	return PermittedChars;
}

void EditableDString::HandleReturn (TextFieldComponent* uiComponent)
{
	//Hack: This function is overridden to bypass the 'cannot access protected member function' issue when rebinding PropValueField.
	//	For some reason subclasses cannot do this even if the functions are protected rather than private. *rolls eyes*
	Super::HandleReturn(uiComponent);
}

void EditableDString::GrabBarrier ()
{
	Super::GrabBarrier();

	if (MultiLineScrollbar != nullptr)
	{
		MultiLineScrollbar->SetFreezeTextureSize(true);
	}
}

void EditableDString::ReleaseBarrier ()
{
	Super::ReleaseBarrier();

	if (MultiLineScrollbar != nullptr)
	{
		MultiLineScrollbar->SetFreezeTextureSize(false);
	}
}

DString EditableDString::ConstructValueAsText () const
{
	if (EditedString == nullptr)
	{
		return DString::EmptyString;
	}

	//Wrap the string in quotation marks
	return TXT("\"") + *EditedString + TXT("\"");
}

void EditableDString::BindVariable (DString* newEditedString)
{
	EditedString = newEditedString;
	MarkPropertyTextDirty();

	if (PropValueField.IsValid())
	{
		if (EditedString != nullptr)
		{
			PropValueField->SetText(*EditedString);
		}
		else
		{
			PropValueField->SetText(DString::EmptyString);
		}
	}
}

void EditableDString::SetLineType (ELineType newLineType)
{
	if ((newLineType == LT_SingleLine && MultiLineScrollbar == nullptr) ||
		(newLineType == LT_MultiLine && MultiLineScrollbar != nullptr))
	{
		//This component is already using the specified line type
		return;
	}

	if (FieldBackground == nullptr || PropValueField.IsNullptr())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to set the line type for the editable DString since the component does not have a PropValueField component."));
		return;
	}

	if (newLineType == LT_MultiLine)
	{
		MultiLineScrollbar = ScrollbarComponent::CreateObject();
		if (AddComponent(MultiLineScrollbar))
		{
			//Position the scrollbar over the prop value field's old position
			MultiLineScrollbar->SetSize(FieldBackground->ReadSize());
			MultiLineScrollbar->SetPosition(FieldBackground->ReadPosition());
			MultiLineScrollbar->SetAnchorLeft(FieldBackground->GetAnchorLeftDist());
			MultiLineScrollbar->SetAnchorRight(FieldBackground->GetAnchorRightDist());
			MultiLineScrollbar->SetHideControlsWhenFull(true);

			if (MultiLineScrollbar->GetFrameTexture() != nullptr)
			{
				if (ColorRenderComponent* colorComp = FieldBackground->GetCenterCompAs<ColorRenderComponent>())
				{
					MultiLineScrollbar->GetFrameTexture()->ResetColor = colorComp->SolidColor;
				}
			}

			GuiEntity* viewedObj = GuiEntity::CreateObject();
			viewedObj->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			viewedObj->SetAutoSizeVertical(true);
			MultiLineScrollbar->SetViewedObject(viewedObj);

			PropValueField->DetachSelfFromOwner();
			if (viewedObj->AddComponent(PropValueField))
			{
				FieldBackground->SetVisibility(false);
				PropValueField->SetPosition(Vector2::ZERO_VECTOR);
				PropValueField->SetSize(Vector2(1.f, 1.f));
				PropValueField->SetAnchorTop(-1.f);
				PropValueField->SetAnchorRight(-1.f);
				PropValueField->SetAnchorBottom(-1.f);
				PropValueField->SetAnchorLeft(-1.f);
				PropValueField->SetSingleLine(false);
				PropValueField->OnReturn.ClearFunction(); //Return is now an acceptable character in the text field. It should add a new line character rather than applying changes.
				PropValueField->SetAutoSizeVertical(true);
			}
		}
	}
	else if (newLineType == LT_SingleLine)
	{
		if (MultiLineScrollbar->GetFrameTexture() != nullptr)
		{
			FieldBackground->SetCenterColor(MultiLineScrollbar->GetFrameTexture()->ResetColor);
		}

		PropValueField->DetachSelfFromOwner();
		if (FieldBackground->AddComponent(PropValueField))
		{
			FieldBackground->SetVisibility(true);
			Float margin = FieldBackground->GetBorderThickness() * 1.5f;
			PropValueField->SetAnchorTop(margin);
			PropValueField->SetAnchorRight(margin);
			PropValueField->SetAnchorBottom(margin);
			PropValueField->SetAnchorLeft(margin);
			PropValueField->SetSingleLine(true);
			PropValueField->OnReturn = SDFUNCTION_1PARAM(this, EditableDString, HandleReturn, void, TextFieldComponent*);
			PropValueField->SetAutoSizeVertical(false);
		}

		MultiLineScrollbar->Destroy();
		MultiLineScrollbar = nullptr;
	}
}

void EditableDString::SetPermittedChars (const DString& newPermittedChars)
{
	PermittedChars = newPermittedChars;
}

void EditableDString::SetDefaultValue (const DString& newDefaultString)
{
	DefaultString = newDefaultString;

	if (ResetDefaultsButton.IsNullptr())
	{
		AddResetToDefaultsControl();
	}

	if (ResetDefaultsButton.IsValid() && EditedString != nullptr)
	{
		ResetDefaultsButton->SetEnabled(!IsReadOnly() && DefaultString.Compare(*EditedString, DString::CC_CaseSensitive) != 0);
	}
}

void EditableDString::HandleUpdateStringTick (Float deltaSec)
{
	//Don't update the text field if the user is currently editing it.
	if (PropValueField.IsValid() && PropValueField->GetCursorPosition() < 0 && EditedString != nullptr)
	{
		if (EditedString->Compare(PropValueField->GetContent(), DString::CC_CaseSensitive) != 0)
		{
			MarkPropertyTextDirty();
			PropValueField->SetText(*EditedString);
		}
	}
}
SD_END