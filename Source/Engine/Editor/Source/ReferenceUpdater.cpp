/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReferenceUpdater.cpp
=====================================================================
*/

#include "ReferenceUpdater.h"
#include "EditorEngineComponent.h"
#include "EditorWorkerEngineComponent.h"

IMPLEMENT_CLASS(SD::ReferenceUpdater, SD::Entity)
SD_BEGIN

void ReferenceUpdater::InitProps ()
{
	Super::InitProps();

	MaxUpdateInterval = -1;
	NextFileIdx = 0;
	Tick = nullptr;
}

void ReferenceUpdater::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	DString tickGroupName = TICK_GROUP_MISC;

	//The Editor Tick Group may not exist in this thread. If it doesn't, fallback to the misc catagory.
	if (localEngine->FindTickGroup(TICK_GROUP_EDITOR) != nullptr)
	{
		tickGroupName = TICK_GROUP_EDITOR;
	}

	Tick = TickComponent::CreateObject(tickGroupName);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, ReferenceUpdater, HandleTick, void, Float));
		Tick->SetTicking(false);
	}
}

void ReferenceUpdater::ReplaceReferences (const DString& oldName, const DString& newName)
{
	SUpdateRequest& newRequest = UpdateRequests.emplace_back();
	newRequest.OldName = oldName;
	newRequest.NewName = newName;
	newRequest.Filter = FF_InCatalogue;

	if (UpdateRequests.size() == 1)
	{
		if (Tick != nullptr)
		{
			Tick->SetTicking(true);
		}

		LaunchNextProcess();
	}
}

void ReferenceUpdater::ReplaceReferences (const DString& oldName, const DString& newName, const Directory& rootDir, const std::vector<DString>& fileExtensions)
{
	SUpdateRequest& newRequest = UpdateRequests.emplace_back();
	newRequest.OldName = oldName;
	newRequest.NewName = newName;
	newRequest.Filter = FF_Everything;
	newRequest.RootDirectory = rootDir;
	newRequest.Extensions = fileExtensions;

	if (UpdateRequests.size() == 1)
	{
		if (Tick != nullptr)
		{
			Tick->SetTicking(true);
		}

		LaunchNextProcess();
	}
}

void ReferenceUpdater::AbortProcesses ()
{
	if (!ContainerUtils::IsEmpty(UpdateRequests))
	{
		ContainerUtils::Empty(OUT UpdateRequests);
		ContainerUtils::Empty(OUT FileList);

		OnReferenceUpdate.ClearFunction();

		if (Tick != nullptr)
		{
			Tick->SetTicking(false);
		}
	}
}

void ReferenceUpdater::SetMaxUpdateInterval (Int newMaxUpdateInterval)
{
	MaxUpdateInterval = newMaxUpdateInterval;
}

void ReferenceUpdater::LaunchNextProcess ()
{
	CHECK(!ContainerUtils::IsEmpty(UpdateRequests))
	ContainerUtils::Empty(OUT FileList);

	SUpdateRequest& nextRequest = UpdateRequests.at(0);
	if (nextRequest.Filter == FF_InCatalogue)
	{
#ifdef WITH_MULTI_THREAD
		//First check the catalogue in the worker thread
		if (EditorWorkerEngineComponent* localWorker = EditorWorkerEngineComponent::Find())
		{
			FileList = localWorker->ReadLatestCatalogue();
		}
#endif

		if (ContainerUtils::IsEmpty(FileList))
		{
			//Catalogue not found, try the main editor engine next
			if (EditorEngineComponent* localEditor = EditorEngineComponent::Find())
			{
				FileList = localEditor->ReadAssetCatalogue();
			}
		}
	}
	else //FF_Everything
	{
		//Use a file iterator instead
		for (FileIterator iter(nextRequest.RootDirectory, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
		{
			if (!ContainerUtils::IsEmpty(nextRequest.Extensions))
			{
				//Must match one of the extensions
				bool bExtensionMatch = false;

				for (const DString& ext : nextRequest.Extensions)
				{
					if (ext.Compare(iter.ReadSelectedAttributes().ReadFileExtension(), DString::CC_IgnoreCase) == 0)
					{
						bExtensionMatch = true;
						break;
					}
				}

				if (!bExtensionMatch)
				{
					continue;
				}
			}

			FileList.push_back(iter.GetSelectedAttributes());
		}
	}

	if (ContainerUtils::IsEmpty(FileList))
	{
		//There aren't any files to process this request. Complete it immediately.
		UpdateRequests.erase(UpdateRequests.begin());

		if (!ContainerUtils::IsEmpty(UpdateRequests))
		{
			//Process next request.
			LaunchNextProcess();
		}
		else
		{
			//All processes finished.
			ProcessFinishedUpdate();
		}

		return;
	}

	NextFileIdx = 0;
}

void ReferenceUpdater::UpdateReferencesInFile (const FileAttributes& fileAttrib)
{
	CHECK(!ContainerUtils::IsEmpty(UpdateRequests))

	bool bUpdateFile = false;
	std::vector<DString> allLines;

	TextFileWriter* fileAsText = TextFileWriter::CreateObject();
	if (fileAsText->OpenFile(fileAttrib, false))
	{
		DString curLine;
		while (fileAsText->ReadLine(OUT curLine))
		{
			//Check if this line needs to replace any data.
			if (curLine.StartsWith(';'))
			{
				allLines.push_back(curLine);
				curLine.Clear();
				continue;
			}

			Int equalIdx = curLine.Find('=', 1);
			if (equalIdx == INT_INDEX_NONE)
			{
				//Equals is not found. Nothing to replace here since this isn't a key/value pair
				allLines.push_back(curLine);
				curLine.Clear();
				continue;
			}

			//Start searching after the first equals character
			Int startSearch = equalIdx + 1;
			while (startSearch < curLine.Length())
			{
				Int findIdx = curLine.Find(UpdateRequests.at(0).OldName, startSearch, DString::CC_CaseSensitive);
				if (findIdx == INT_INDEX_NONE)
				{
					//Text is not found anywhere. Stop searching in this line
					break;
				}

				//Ensure this text doesn't appear inside quotation marks.
				Int numQuotes = 0;
				StringIterator iter(&curLine);
				iter += equalIdx + 1;
				bool bIsEscaped = false;
				for (Int iterIdx(equalIdx + 1); iterIdx < findIdx; ++iterIdx)
				{
					if (bIsEscaped) //Ignore escaped quotes and escaped escape characters
					{
						DString iterChar = iter.GetString();
						if (iterChar == TXT("\""))
						{
							++numQuotes;
						}

						bIsEscaped = (iterChar == TXT("\\"));
					}
					else
					{
						bIsEscaped = false;
					}

					++iter;
				}

				if (numQuotes.IsEven())
				{
					//If outside quotation marks
					//Safe to replace the text
					curLine.Remove(findIdx, UpdateRequests.at(0).OldName.Length());
					curLine.Insert(findIdx, UpdateRequests.at(0).NewName);
					startSearch = findIdx + UpdateRequests.at(0).NewName.Length();
					bUpdateFile = true;
				}
				else
				{
					//Inside quotation marks. Do not replace this text.
					startSearch = findIdx + UpdateRequests.at(0).OldName.Length();
				}
			}

			allLines.push_back(curLine);
			curLine.Clear();
		}
	}

	if (bUpdateFile)
	{
		if (fileAsText->EmptyFile())
		{
			for (const DString& curLine : allLines)
			{
				fileAsText->AddTextEntry(curLine);
			}

			fileAsText->WriteToFile();
		}
	}

	fileAsText->Destroy();
}

void ReferenceUpdater::ProcessFinishedUpdate ()
{
	ContainerUtils::Empty(OUT FileList);

	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}

	if (OnReferenceUpdate.IsBounded())
	{
		OnReferenceUpdate(true);
	}
}

void ReferenceUpdater::HandleTick (Float deltaSec)
{
	Int numProcessed = 0;
	while (NextFileIdx < FileList.size())
	{
		UpdateReferencesInFile(FileList.at(NextFileIdx));
		++NextFileIdx;
		++numProcessed;

		if (MaxUpdateInterval > 0 && numProcessed >= MaxUpdateInterval)
		{
			if (OnReferenceUpdate.IsBounded())
			{
				OnReferenceUpdate(false);
			}

			//Pause in this frame. Next tick will continue where it left off.
			return;
		}
	}

	UpdateRequests.erase(UpdateRequests.begin());
	if (!ContainerUtils::IsEmpty(UpdateRequests))
	{
		LaunchNextProcess();
	}
	else
	{
		ProcessFinishedUpdate();
	}
}
SD_END