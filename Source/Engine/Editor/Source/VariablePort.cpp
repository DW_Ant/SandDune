/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariablePort.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "FunctionNode.h"
#include "GraphAsset.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "GraphNode.h"
#include "GraphVariable.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::VariablePort, SD::NodePort)
SD_BEGIN

const std::map<ScriptVariable::EVarType, Color> VariablePort::COLOR_MAP =
{
	{ScriptVariable::VT_Unknown, Color(25, 25, 25)}, //black
	{ScriptVariable::VT_Bool, Color(162, 17, 7)}, //red
	{ScriptVariable::VT_Int, Color(209, 142, 6)}, //orange
	{ScriptVariable::VT_Float, Color(240, 235, 97)}, //yellow
	{ScriptVariable::VT_String, Color(42, 152, 21)}, //green
	{ScriptVariable::VT_Function, Color(151, 209, 238)}, //cyan
	{ScriptVariable::VT_Obj, Color(16, 29, 189)}, //blue
	{ScriptVariable::VT_Class, Color(163, 23, 147)}, //purple
	{ScriptVariable::VT_Pointer, Color(191, 191, 191)} //grey (Editor wouldn't expose pointers though)
};

void VariablePort::InitProps ()
{
	Super::InitProps();

	VarType = ScriptVariable::VT_Unknown;
	ClassConstraint = nullptr;
}

void VariablePort::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const VariablePort* varPortTemplate = dynamic_cast<const VariablePort*>(objTemplate))
	{
		bool bCreatedObj;
		ClassTooltip = ReplaceTargetWithObjOfMatchingClass(ClassTooltip.Get(), varPortTemplate->ClassTooltip.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ClassTooltip);
		}

		if (ClassTooltip != nullptr)
		{
			ClassTooltip->CopyPropertiesFrom(varPortTemplate->ClassTooltip.Get());
		}
	}
}

bool VariablePort::CanAcceptConnectionsFrom (const NodePort* otherPort, bool bCheckOtherPort) const
{
	if (PortType != PT_Any)
	{
		//Can only connect to another variable port.
		const VariablePort* varPort = dynamic_cast<const VariablePort*>(otherPort);
		if (varPort == nullptr)
		{
			return false;
		}

		//Check if these variable ports are compatible.
		if (!HasConversionFor(VarType, varPort->GetVarType()))
		{
			return false;
		}

		if (VarType == ScriptVariable::VT_Obj || VarType == ScriptVariable::VT_Class)
		{
			//Only input ports are checked. If there's a difference in class assets, the input determines the parent class constraint, allowing sub classes to connect to this.
			if (PortType == PT_Input && ClassConstraint != nullptr && varPort->ClassConstraint != nullptr)
			{
				if (!ClassConstraint->IsParentOf(varPort->ClassConstraint))
				{
					return false;
				}
			}
		}
	}

	return Super::CanAcceptConnectionsFrom(otherPort, bCheckOtherPort);
}

void VariablePort::EstablishConnectionTo (NodePort* otherPort)
{
	CHECK(otherPort != nullptr)

	if (VariablePort* varPort = dynamic_cast<VariablePort*>(otherPort))
	{
		if (varPort->GetVarType() != VarType)
		{
			//Create a conversion node between the two ports.
			VariablePort* outputPort = nullptr;
			VariablePort* inputPort = nullptr;
			if (PortType == PT_Output)
			{
				outputPort = this;
				inputPort = varPort;
			}
			else
			{
				outputPort = varPort;
				inputPort = this;
			}

			if (CreateConversionNode(outputPort, inputPort))
			{
				return; //Don't call super. Instead of connecting the two ports together, the CreateConversionNode function will connect the two ports to the new node.
			}
		}
	}
	else
	{
		//Set the other port's color to match this port's color (useful for reroute ports)
		otherPort->SetTextureColor(TextureColor);

		//Propagate the color to other reroute ports
		std::vector<NodePort*> allConnections;
		std::vector<NodePort*> checkedPorts;
		otherPort->GetConnectedPortsOfMatchingType(OUT allConnections, PT_Any, OUT checkedPorts);
		for (NodePort* otherReroutePort : allConnections)
		{
			otherReroutePort->SetTextureColor(TextureColor);
		}
	}

	Super::EstablishConnectionTo(otherPort);
}

bool VariablePort::CanBeGroupedWith (NodePort* otherPort) const
{
	if (!Super::CanBeGroupedWith(otherPort))
	{
		return false;
	}

	if (VariablePort* otherVar = dynamic_cast<VariablePort*>(otherPort))
	{
		//Variable types must match exactly. Conversion nodes will automatically be inserted when connecting to nonreroute nodes.
		if (VarType != otherVar->VarType)
		{
			return false;
		}
	}

	return true;
}

void VariablePort::InitializeComponents ()
{
	Super::InitializeComponents();

	ClassTooltip = TooltipComponent::CreateObject();
	if (AddComponent(ClassTooltip))
	{
		ClassTooltip->RevealDelay = 0.f;
		ClassTooltip->HideMoveThreshold = 64.f;
		ClassTooltip->ShowMoveThreshold = 1.f;
	}
}

void VariablePort::ReconnectToVarPort (NodePort* otherPort)
{
	std::vector<NodePort*> checkedPorts;
	if (IsConnectedTo(otherPort, OUT checkedPorts))
	{
		SeverConnectionFrom(otherPort);
	}

	if (CanAcceptConnectionsFrom(otherPort, true))
	{
		EstablishConnectionTo(otherPort);
	}
}

void VariablePort::SetVarType (ScriptVariable::EVarType newVarType)
{
	if (VarType == newVarType)
	{
		return;
	}

	VarType = newVarType;

	if (COLOR_MAP.contains(VarType))
	{
		SetTextureColor(COLOR_MAP.at(VarType));
	}
}

void VariablePort::SetClassConstraint (GraphAsset* newClassConstraint)
{
	ClassConstraint = newClassConstraint;

	if (ClassTooltip != nullptr)
	{
		if (ClassConstraint != nullptr)
		{
			ClassTooltip->SetTooltipText(ClassConstraint->GetPresentedName());
		}
		else
		{
			ClassTooltip->SetTooltipText(DString::EmptyString);
		}
	}
}

bool VariablePort::HasConversionFor (ScriptVariable::EVarType originalVarType, ScriptVariable::EVarType desiredType)
{
	if (originalVarType == desiredType)
	{
		return true;
	}

	switch (originalVarType)
	{
		case(ScriptVariable::VT_Bool):
			return (desiredType == ScriptVariable::VT_Int || desiredType == ScriptVariable::VT_Float || desiredType == ScriptVariable::VT_String);

		case(ScriptVariable::VT_Int):
			return (desiredType == ScriptVariable::VT_Bool || desiredType == ScriptVariable::VT_Float || desiredType == ScriptVariable::VT_String);

		case(ScriptVariable::VT_Float):
			return (desiredType == ScriptVariable::VT_Bool || desiredType == ScriptVariable::VT_Int || desiredType == ScriptVariable::VT_String);

		case(ScriptVariable::VT_String):
			return (desiredType == ScriptVariable::VT_Bool || desiredType == ScriptVariable::VT_Int || desiredType == ScriptVariable::VT_Float);
	}

	return false;
}

bool VariablePort::CreateConversionNode (VariablePort* outputPort, VariablePort* inputPort)
{
	CHECK(OwningEditor != nullptr && outputPort != nullptr && inputPort != nullptr)

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	HashedString coreAssetName("Core");
	if (!editorEngine->ReadLoadedAssets().contains(coreAssetName))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("The editor is unable to create conversion node since the %s asset doesn't exist."), coreAssetName);
		return false;
	}

	GraphAsset* coreAsset = editorEngine->ReadLoadedAssets().at(coreAssetName);
	CHECK(coreAsset != nullptr)

	DString functionName = DString::CreateFormattedString(TXT("%sTo%s"), GetTypeAsString(outputPort->GetVarType()), GetTypeAsString(inputPort->GetVarType()));
	GraphFunction* conversionFunction = nullptr;
	for (GraphFunction* func : coreAsset->ReadMemberFunctions())
	{
		if (func->ReadFunctionName().Compare(functionName, DString::CC_CaseSensitive) == 0)
		{
			conversionFunction = func;
			break;
		}
	}

	if (conversionFunction == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("The editor is unable to create conversion node since the %s asset doesn't have a conversion function named %s."), coreAssetName, functionName);
		return false;
	}

	FunctionNode* newNode = FunctionNode::CreateObject();
	if (OwningEditor->AddComponent(newNode))
	{
		inputPort->ComputeAbsTransform();
		outputPort->ComputeAbsTransform();

		newNode->SetPosition((outputPort->ReadCachedAbsPosition() + inputPort->ReadCachedAbsPosition()) * 0.5f); //Place it between the two ports
		newNode->BindFunction(conversionFunction);

		bool successfulConnection = false;

		if (newNode->ReadInputPorts().size() >= 1 && newNode->ReadOutputPorts().size() >= 1)
		{
			VariablePort* conversionInput = dynamic_cast<VariablePort*>(newNode->ReadInputPorts().at(0).Port);
			VariablePort* conversionOutput = dynamic_cast<VariablePort*>(newNode->ReadOutputPorts().at(0).Port);

			if (conversionInput != nullptr && conversionOutput != nullptr)
			{
				conversionInput->EstablishConnectionTo(outputPort);
				conversionOutput->EstablishConnectionTo(inputPort);
				successfulConnection = true;
			}
		}

		if (!successfulConnection)
		{
			newNode->Destroy();
			newNode = nullptr;
		}
	}

	return (newNode != nullptr);
}

DString VariablePort::GetTypeAsString (ScriptVariable::EVarType varType) const
{
	switch(varType)
	{
		case(ScriptVariable::VT_Bool):
			return TXT("Bool");

		case(ScriptVariable::VT_Int):
			return TXT("Int");

		case(ScriptVariable::VT_Float):
			return TXT("Float");

		case(ScriptVariable::VT_String):
			return TXT("String");

		case(ScriptVariable::VT_Function):
			return TXT("Function");

		case(ScriptVariable::VT_Obj):
			return TXT("Object");

		case(ScriptVariable::VT_Class):
			return TXT("Class");
	}

	return DString::EmptyString;
}
SD_END