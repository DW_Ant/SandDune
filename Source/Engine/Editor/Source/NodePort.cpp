/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NodePort.cpp
=====================================================================
*/

#include "GraphEditor.h"
#include "NodePort.h"

IMPLEMENT_CLASS(SD::NodePort, SD::GuiComponent)
SD_BEGIN

void NodePort::InitProps ()
{
	Super::InitProps();

	PortType = PT_Any;
	bAcceptsMultiConnections = true;
	TextureColor = Color::WHITE;
	RefreshConnectionThreshold = 2.f;
	LastCurveRefreshPos = Vector2::ZERO_VECTOR;

	OwningEditor = nullptr;
	bIsHovered = false;
	bIsPressed = false;
}

void NodePort::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const NodePort* nodeTemplate = dynamic_cast<const NodePort*>(objTemplate))
	{
		SetTextureColor(nodeTemplate->TextureColor);
		SetRefreshConnectionThreshold(nodeTemplate->GetRefreshConnectionThreshold());

		bool bCreatedObj;
		SpriteComp = ReplaceTargetWithObjOfMatchingClass(SpriteComp.Get(), nodeTemplate->SpriteComp.Get(), OUT bCreatedObj);
		if (bCreatedObj && AddComponent(SpriteComp))
		{
			SpriteComp->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, NodePort, HandleTextureChanged, void));
		}

		if (SpriteComp != nullptr)
		{
			SpriteComp->CopyPropertiesFrom(nodeTemplate->SpriteComp.Get());
		}
	}
}

bool NodePort::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	//This component must either directly attach to a GraphEditor or to a component that is attached to a GraphEditor.
	//The GraphEditor is needed to allow node ports to connect to other node ports. The GraphEditor is responsible for holding a reference to the dragged port.
	GraphEditor* editorCandidate = dynamic_cast<GraphEditor*>(ownerCandidate);
	if (editorCandidate == nullptr)
	{
		if (EntityComponent* compCandidate = dynamic_cast<EntityComponent*>(ownerCandidate))
		{
			editorCandidate = dynamic_cast<GraphEditor*>(compCandidate->GetRootEntity());
		}
	}

	return (editorCandidate != nullptr);
}

void NodePort::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (OwningEditor == nullptr)
	{
		return;
	}

	bool curHovered = IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y));
	if (curHovered != bIsHovered)
	{
		bIsHovered = curHovered;
		UpdateSpriteRender();
	}
}

void NodePort::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (bIsPressed && sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonReleased)
	{
		bIsPressed = false;
		UpdateSpriteRender();
	}
}

bool NodePort::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//Right click removes all connections
	if (sfmlEvent.button == sf::Mouse::Right && InputBroadcaster::GetCtrlHeld() && IsWithinBounds(mouse->ReadPosition()))
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			SeverAllConnections();
		}

		return true;
	}

	if (sfmlEvent.button != sf::Mouse::Left || OwningEditor == nullptr)
	{
		return false;	
	}

	Vector2 mousePoint(sfmlEvent.x, sfmlEvent.y);
	if (IsWithinBounds(mousePoint))
	{
		if (eventType == sf::Event::MouseButtonPressed && InputBroadcaster::GetCtrlHeld())
		{
			OwningEditor->SetDraggedPort(this);
			bIsPressed = true;
			UpdateSpriteRender();
			return true;
		}
		else if (NodePort* grabbedPort = OwningEditor->GetDraggedPort())
		{
			OwningEditor->SetDraggedPort(nullptr);
			return true;
		}
	}

	return false;
}

void NodePort::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	//Do nothing if this port isn't connected to anything (since there aren't any curves associated with a disconnected port).
	if (ContainerUtils::IsEmpty(Connections) || !OnRefreshCurveConnections.IsBounded())
	{
		return;
	}

	Vector2 deltaPos = ReadCachedAbsPosition() - LastCurveRefreshPos;
	if (deltaPos.VSize() < GetRefreshConnectionThreshold())
	{
		//Didn't move far enough
		return;
	}

	LastCurveRefreshPos = GetCachedAbsPosition();
	OnRefreshCurveConnections(this);
}

void NodePort::InitializeComponents ()
{
	Super::InitializeComponents();

	SpriteComp = SpriteComponent::CreateObject();
	if (AddComponent(SpriteComp))
	{
		SpriteComp->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, NodePort, HandleTextureChanged, void));

		if (sf::Sprite* sprite = SpriteComp->GetSprite())
		{
			sprite->setColor(TextureColor.Source);
		}

		UpdateSpriteRender();
	}
}

void NodePort::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (EntityComponent* compOwner = dynamic_cast<EntityComponent*>(newOwner))
	{
		OwningEditor = dynamic_cast<GraphEditor*>(compOwner->GetRootEntity());
	}
	else
	{
		OwningEditor = dynamic_cast<GraphEditor*>(newOwner);
	}
}

void NodePort::ComponentDetached ()
{
	OwningEditor = nullptr;

	Super::ComponentDetached();
}

void NodePort::Destroyed ()
{
	SeverAllConnections();

	Super::Destroyed();
}

bool NodePort::CanAcceptConnectionsFrom (const NodePort* otherPort, bool bCheckOtherPort) const
{
	CHECK(otherPort != nullptr)

	if (otherPort == this)
	{
		return false; //Can't connect to self
	}

	if (PortType != PT_Any && otherPort->PortType != PT_Any)
	{
		if (PortType == otherPort->PortType)
		{
			return false; //Only opposites can connect. Can't connect an input port to an input port.
		}
	}

	//Can't make a circular connection (eg: reroute nodes connecting to other reroute nodes that loop back to itself).
	if (PortType == PT_Any && otherPort->PortType == PT_Any)
	{
		std::vector<NodePort*> checkedPorts;
		if (otherPort->IsConnectedTo(this, OUT checkedPorts))
		{
			return false;
		}
	}

	if (bCheckOtherPort && !otherPort->CanAcceptConnectionsFrom(this, false))
	{
		return false;
	}

	return true;
}

void NodePort::EstablishConnectionTo (NodePort* otherPort)
{
	CHECK(otherPort != nullptr)

	//If this port is already connected to the other port, do nothing.
	if (ContainerUtils::FindInVector(Connections, otherPort) != UINT_INDEX_NONE)
	{
		return;
	}

	if (!bAcceptsMultiConnections)
	{
		//Remove existing connection
		SeverAllConnections();
	}

	Connections.push_back(otherPort);
	UpdateSpriteRender();

	if (OnConnectionChanged.IsBounded())
	{
		OnConnectionChanged(this);
	}

	if (ContainerUtils::FindInVector(otherPort->Connections, this) == UINT_INDEX_NONE)
	{
		//Have the other port connect to this if a connection is not yet established.
		otherPort->EstablishConnectionTo(this);

		if (OwningEditor != nullptr)
		{
			OwningEditor->CreateCurveConnecting(this, otherPort);
		}
	}
}

void NodePort::SeverConnectionFrom (NodePort* otherPort)
{
	CHECK(otherPort != nullptr)

	if (ContainerUtils::RemoveItem(Connections, otherPort) != UINT_INDEX_NONE)
	{
		UpdateSpriteRender();
		otherPort->SeverConnectionFrom(this);

		if (OnConnectionChanged.IsBounded())
		{
			OnConnectionChanged(this);
		}

		if (OwningEditor != nullptr)
		{
			OwningEditor->RemoveCurveConnecting(this, otherPort);
		}
	}
}

void NodePort::SeverAllConnections ()
{
	while (!ContainerUtils::IsEmpty(Connections))
	{
		SeverConnectionFrom(Connections.at(0));
	}
}

void NodePort::GetConnectedPortsOfMatchingType (std::vector<NodePort*>& outMatches, EPortType reqType, std::vector<NodePort*>& outCheckedPorts) const
{
	if (PortType == reqType)
	{
		outMatches.push_back(const_cast<NodePort*>(this));
	}

	if (PortType == PT_Any)
	{
		for (NodePort* connection : Connections)
		{
			if (ContainerUtils::FindInVector(outCheckedPorts, connection) == UINT_INDEX_NONE)
			{
				outCheckedPorts.push_back(connection);
				connection->GetConnectedPortsOfMatchingType(OUT outMatches, reqType, OUT outCheckedPorts);
			}
		}
	}
}

bool NodePort::CanBeGroupedWith (NodePort* otherPort) const
{
	if (PortType != PT_Any && otherPort->PortType != PT_Any)
	{
		//Classes must match. Functions can only be bundled with other functions. Variables can only be bundled with other variables.
		if (StaticClass() != otherPort->StaticClass())
		{
			return false;
		}
	}

	return true;
}

void NodePort::SetPortType (EPortType newPortType)
{
	PortType = newPortType;
}

void NodePort::SetAcceptMultiConnections (bool newAcceptMultiConnections)
{
	bAcceptsMultiConnections = newAcceptMultiConnections;
	if (!bAcceptsMultiConnections)
	{
		while (Connections.size() > 1)
		{
			SeverConnectionFrom(Connections.at(0));
		}
	}
}

void NodePort::SetTextureColor (Color newTextureColor)
{
	TextureColor = newTextureColor;

	if (SpriteComp != nullptr)
	{
		if (sf::Sprite* sprite = SpriteComp->GetSprite())
		{
			sprite->setColor(TextureColor.Source);
		}
	}
}

void NodePort::SetRefreshConnectionThreshold (Float newRefreshConnectionThreshold)
{
	RefreshConnectionThreshold = newRefreshConnectionThreshold;
}

bool NodePort::IsConnectedTo (const NodePort* otherPort, std::vector<NodePort*>& outCheckedPorts) const
{
	for (NodePort* connection : Connections)
	{
		if (connection == otherPort)
		{
			return true;
		}

		if (ContainerUtils::FindInVector(outCheckedPorts, const_cast<NodePort*>(this)) == UINT_INDEX_NONE)
		{
			outCheckedPorts.push_back(connection);
			if (connection->IsConnectedTo(otherPort, OUT outCheckedPorts))
			{
				return true;
			}
		}
	}

	return false;
}

void NodePort::UpdateSpriteRender ()
{
	if (SpriteComp == nullptr)
	{
		return;
	}

	Int vertIdx = 0; //default
	if (bIsHovered)
	{
		++vertIdx;

		if (bIsPressed)
		{
			++vertIdx;
		}
	}

	Int horizontalIdx = (ContainerUtils::IsEmpty(Connections)) ? 0 : 1;
	SpriteComp->SetSubDivision(2, 3, horizontalIdx, vertIdx);
}

void NodePort::HandleTextureChanged ()
{
	UpdateSpriteRender();
}
SD_END