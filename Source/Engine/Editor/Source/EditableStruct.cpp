/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableStruct.cpp
=====================================================================
*/

#include "BaseStructElement.h"
#include "EditableFloat.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "EditorTheme.h"
#include "StructElement.h"

IMPLEMENT_CLASS(SD::EditableStruct, SD::EditPropertyComponent)
SD_BEGIN

EditableStruct::SInspectorStruct::SInspectorStruct (const DString& inPropertyName, const DString& inTooltipText) : SInspectorProperty(inPropertyName, inTooltipText),
	HeaderText(DString::EmptyString)
{
	//Noop
}

void EditableStruct::SInspectorStruct::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	//Noop - Nothing to load
}

void EditableStruct::SInspectorStruct::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	//Noop - Nothing to save
}

bool EditableStruct::SInspectorStruct::LoadBinary (const DataBuffer& incomingData)
{
	//Nothing to load
	return true;
}

void EditableStruct::SInspectorStruct::SaveBinary (DataBuffer& outData) const
{
	//Noop - Nothing to save
}

EditPropertyComponent* EditableStruct::SInspectorStruct::CreateBlankEditableComponent () const
{
	return EditableStruct::CreateObject();
}

void EditableStruct::SInspectorStruct::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableStruct* structComp = dynamic_cast<EditableStruct*>(AssociatedComp))
	{
		structComp->bDynamicHeader = HeaderText.IsEmpty();
		if (structComp->GetHeaderComponent() != nullptr)
		{
			structComp->GetHeaderComponent()->SetText(HeaderText);
		}
	}
}

void EditableStruct::InitProps ()
{
	Super::InitProps();

	FirstSubPropPos = LineHeight;

	ResetDefaultsButton = nullptr;
	bDynamicHeader = true;
	bIsCollapsed = true;
	HeaderUpdateTick = nullptr;
}

void EditableStruct::BeginObject ()
{
	Super::BeginObject();

	HeaderUpdateTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(HeaderUpdateTick))
	{
		HeaderUpdateTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableStruct, HandleUpdateHeader, void, Float));
		HeaderUpdateTick->SetTickInterval(0.2f);
		HeaderUpdateTick->SetTicking(bDynamicHeader);
	}
}

void EditableStruct::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableStruct* structTemplate = dynamic_cast<const EditableStruct*>(objTemplate))
	{
		bool bCreatedObj;
		HeaderComponent = ReplaceTargetWithObjOfMatchingClass(HeaderComponent.Get(), structTemplate->GetHeaderComponent(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(HeaderComponent);
		}

		if (HeaderComponent.IsValid())
		{
			HeaderComponent->CopyPropertiesFrom(structTemplate->GetHeaderComponent());
		}

		bDynamicHeader = structTemplate->bDynamicHeader;
	}
}

bool EditableStruct::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//If the user clicked over the label component, toggle expanding and collapsing this field.
	if (sfmlEvent.button == sf::Mouse::Button::Left && eventType == sf::Event::MouseButtonPressed)
	{
		Vector2 mousePos(sfmlEvent.x, sfmlEvent.y);
		if ((PropertyName != nullptr && PropertyName->IsWithinBounds(mousePos)) ||
			(HeaderComponent != nullptr && HeaderComponent->IsWithinBounds(mousePos)))
		{
			//Must click on the first line (not clicking in the spaces between the subproperties)
			Float delta = mousePos.Y - ReadCachedAbsPosition().Y;
			if (delta < FirstSubPropPos)
			{
				ToggleCollapse();
				return true;
			}
		}
	}

	return false;
}

void EditableStruct::PostInitPropertyComponent ()
{
	Super::PostInitPropertyComponent();

	RefreshHeaderText();
}

void EditableStruct::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (HeaderComponent.IsValid() && BarrierTransform.IsValid())
	{
		//No need to set position since the header component is anchored to the right.
		HeaderComponent->SetSize(Vector2(1.f - (BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X), 1.f));
	}
}

bool EditableStruct::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	RefreshHeaderText();
	return true;
}

void EditableStruct::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	RefreshHeaderText();
}

EditPropertyComponent::SInspectorProperty* EditableStruct::CreateDefaultInspector (const DString& propName) const
{
	return new SInspectorStruct(propName, DString::EmptyString);
}

GuiComponent* EditableStruct::GetComponentTooltipsShouldAttachTo ()
{
	if (PropertyName != nullptr)
	{
		return PropertyName.Get();
	}

	return Super::GetComponentTooltipsShouldAttachTo();
}

void EditableStruct::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (HeaderUpdateTick != nullptr)
	{
		HeaderUpdateTick->Destroy();
		HeaderUpdateTick = nullptr;
	}
}

void EditableStruct::SetNormalFontColor (Color newNormalFontColor)
{
	Super::SetNormalFontColor(newNormalFontColor);

	SyncFontColors();
}

void EditableStruct::SetReadOnlyFontColor (Color newReadOnlyFontColor)
{
	Super::SetReadOnlyFontColor(newReadOnlyFontColor);

	SyncFontColors();
}

void EditableStruct::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	SyncFontColors();
}

void EditableStruct::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	FirstSubPropPos = LineHeight;
}

bool EditableStruct::AddComponent_Implementation (EntityComponent* newComponent)
{
	if (!Super::AddComponent_Implementation(newComponent))
	{
		return false;
	}

	if (EditPropertyComponent* subProp = dynamic_cast<EditPropertyComponent*>(newComponent))
	{
		subProp->SetVisibility(!IsCollapsed());
	}

	return true;
}

void EditableStruct::InitializeComponents ()
{
	Super::InitializeComponents();

	HeaderComponent = LabelComponent::CreateObject();
	if (AddComponent(HeaderComponent))
	{
		HeaderComponent->SetAutoRefresh(false);
		HeaderComponent->SetAnchorRight(0.f);
		HeaderComponent->SetSize(Vector2(1.f - (BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X), 1.f));
		HeaderComponent->SetWrapText(false);
		HeaderComponent->SetClampText(true);

		if (HeaderComponent->GetRenderComponent() != nullptr && PropertyName.IsValid() && PropertyName->GetRenderComponent() != nullptr)
		{
			HeaderComponent->GetRenderComponent()->SetFontColor(PropertyName->GetRenderComponent()->GetFontColor());
		}

		HeaderComponent->SetAutoRefresh(true);
	}
}

Float EditableStruct::CalcVerticalSpace (bool includeSubComp) const
{
	//Don't the sub components if the struct is collapsed.
	return Super::CalcVerticalSpace(!IsCollapsed());
}

DString EditableStruct::ConstructValueAsText () const
{
	DString result = TXT("(");

	for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
	{
		if (SubPropertyComponents.at(i)->GetPropertyName() == nullptr)
		{
			continue;
		}

		if (i > 0)
		{
			result += TXT(", ");
		}

		result += SubPropertyComponents.at(i)->GetPropertyName()->GetContent() + TXT("=") + SubPropertyComponents.at(i)->GetValueAsText();
	}

	result += TXT(")");
	return result;
}

void EditableStruct::ProcessApplyEdit (EditPropertyComponent* editedComp)
{
	Super::ProcessApplyEdit(editedComp);

	RefreshHeaderText();
}

void EditableStruct::BindVector2 (Vector2* associatedVar, const Vector2& defaultVect)
{
	if (!ContainerUtils::IsEmpty(SubPropertyComponents))
	{
		RebindVector2(associatedVar);
		SetDefaultVector2(defaultVect);
	}
	else if (associatedVar != nullptr)
	{
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("X"), &associatedVar->X, defaultVect.X));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Y"), &associatedVar->Y, defaultVect.Y));
	}
	else
	{
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("X"), nullptr, defaultVect.X));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Y"), nullptr, defaultVect.Y));
	}
}

void EditableStruct::BindVector3 (Vector3* associatedVar, const Vector3& defaultVect)
{
	if (!ContainerUtils::IsEmpty(SubPropertyComponents))
	{
		RebindVector3(associatedVar);
		SetDefaultVector3(defaultVect);
	}
	else if (associatedVar != nullptr)
	{
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("X"), &associatedVar->X, defaultVect.X));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Y"), &associatedVar->Y, defaultVect.Y));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Z"), &associatedVar->Z, defaultVect.Z));
	}
	else
	{
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("X"), nullptr, defaultVect.X));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Y"), nullptr, defaultVect.Y));
		AddStructElement(new StructElement<Float, EditableFloat>(TXT("Z"), nullptr, defaultVect.Z));
	}
}

void EditableStruct::RebindVector2 (Vector2* associatedVar)
{
	if (associatedVar == nullptr)
	{
		UnbindVariable();
		return;
	}

	CHECK(SubPropertyComponents.size() == 2)

	//Only need to rebind the components
	for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
	{
		EditableFloat* subFloat = dynamic_cast<EditableFloat*>(SubPropertyComponents.at(i));
		CHECK(subFloat != nullptr)
		subFloat->BindVariable(&(*associatedVar)[i]);
	}
}

void EditableStruct::RebindVector3 (Vector3* associatedVar)
{
	if (associatedVar == nullptr)
	{
		UnbindVariable();
		return;
	}

	CHECK(SubPropertyComponents.size() == 3)

	//Only need to rebind the components
	for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
	{
		EditableFloat* subFloat = dynamic_cast<EditableFloat*>(SubPropertyComponents.at(i));
		CHECK(subFloat != nullptr)
		subFloat->BindVariable(&(*associatedVar)[i]);
	}
}

void EditableStruct::SetDefaultVector2 (const Vector2& defaultVect)
{
	if (ContainerUtils::IsEmpty(SubPropertyComponents))
	{
		BindVector2(nullptr, defaultVect);
	}
	else
	{
		CHECK(SubPropertyComponents.size() == 2)
		for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
		{
			EditableFloat* subFloat = dynamic_cast<EditableFloat*>(SubPropertyComponents.at(i));
			CHECK(subFloat != nullptr)
			subFloat->SetDefaultValue(defaultVect[i]);
		}
	}
}

void EditableStruct::SetDefaultVector3 (const Vector3& defaultVect)
{
	if (ContainerUtils::IsEmpty(SubPropertyComponents))
	{
		BindVector3(nullptr, defaultVect);
	}
	else
	{
		CHECK(SubPropertyComponents.size() == 3)
		for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
		{
			EditableFloat* subFloat = dynamic_cast<EditableFloat*>(SubPropertyComponents.at(i));
			CHECK(subFloat != nullptr)
			subFloat->SetDefaultValue(defaultVect[i]);
		}
	}
}

EditPropertyComponent* EditableStruct::AddStructElement (BaseStructElement* newStructElement)
{
	if (newStructElement == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to add a nullptr StructElement to %s."), ToString());
		return nullptr;
	}

	EditPropertyComponent* newComp = newStructElement->SetupPropComp(this);
	CHECK(newComp != nullptr)
	newComp->SetVisibility(!IsCollapsed());

	if (!IsCollapsed())
	{
		MarkVerticalSpaceDirty();
	}

	//The struct element is no longer needed
	delete newStructElement;
	newStructElement = nullptr;

	if (ResetDefaultsButton == nullptr)
	{
		ResetDefaultsButton = AddFieldControl();
		if (ResetDefaultsButton != nullptr)
		{
			ResetDefaultsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableStruct, HandleResetToDefaultsReleased, void, ButtonComponent*));

			EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());
			if (localTheme != nullptr)
			{
				if (FrameComponent* background = ResetDefaultsButton->GetBackground())
				{
					background->SetCenterTexture(localTheme->FieldControlReset);
				}
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (ResetDefaultsButton->AddComponent(tooltip))
			{
				TextTranslator* translator = TextTranslator::GetTranslator();
				CHECK(translator != nullptr)

				tooltip->SetTooltipText(translator->TranslateText(TXT("ResetDefaults"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
			}
		}
	}

	return newComp;
}

void EditableStruct::RefreshHeaderText ()
{
	if (!bDynamicHeader || HeaderComponent == nullptr)
	{
		//Header is manually set. Don't refresh it.
		return;
	}

	DString newHeader = DString::EmptyString;
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		if (subProp->GetPropertyName() == nullptr)
		{
			continue;
		}

		if (!newHeader.IsEmpty())
		{
			newHeader += TXT(", ");
		}

		newHeader += subProp->GetPropertyName()->GetContent() + TXT("=") + subProp->GetValueAsText();
	}

	HeaderComponent->SetText(newHeader);

	//The header of this struct has changed. Mark the text as dirty for the component owning this struct (eg: nested structs).
	MarkPropertyTextDirty();
}

void EditableStruct::DestroyStructElements ()
{
	std::vector<EditPropertyComponent*> propsToDestroy;
	for (EditPropertyComponent* propComp : SubPropertyComponents)
	{
		propsToDestroy.push_back(propComp);
	}

	for (size_t i = 0; i < propsToDestroy.size(); ++i)
	{
		propsToDestroy.at(i)->Destroy();
	}

	if (!IsCollapsed())
	{
		MarkVerticalSpaceDirty();
	}
}

void EditableStruct::SetDynamicHeader (bool newDynamicHeader)
{
	bDynamicHeader = newDynamicHeader;
	if (HeaderUpdateTick != nullptr)
	{
		HeaderUpdateTick->SetTicking(bDynamicHeader);
	}
}

void EditableStruct::SetStaticHeaderText (const DString& newStaticHeaderText)
{
	CHECK_INFO(!bDynamicHeader, "Cannot set the EditableStruct's header if it's a dynamic header since the struct would automatically replace the header text.")
	if (HeaderComponent != nullptr)
	{
		MarkPropertyTextDirty();
		HeaderComponent->SetText(newStaticHeaderText);
	}
}

void EditableStruct::SetCollapsed (bool newCollapsed)
{
	if (newCollapsed != bIsCollapsed)
	{
		ToggleCollapse();
	}
}

void EditableStruct::ToggleCollapse ()
{
	bIsCollapsed = !bIsCollapsed;

	for (EditPropertyComponent* comp : SubPropertyComponents)
	{
		comp->SetVisibility(!bIsCollapsed);
	}

	MarkVerticalSpaceDirty();

	if (OnToggleCollapse.IsBounded())
	{
		OnToggleCollapse(bIsCollapsed);
	}
}

void EditableStruct::SyncFontColors ()
{
	if (HeaderComponent.IsValid() && HeaderComponent->GetRenderComponent() != nullptr &&
		PropertyName.IsValid() && PropertyName->GetRenderComponent() != nullptr)
	{
		HeaderComponent->GetRenderComponent()->SetFontColor(PropertyName->GetRenderComponent()->GetFontColor());
	}
}

void EditableStruct::HandleResetToDefaultsReleased (ButtonComponent* uiComp)
{
	if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
	{
		//Tooltips will not receive a hide call since the button won't pass input events to subcomponents when disabled.
		tooltipEntity->HideTooltip();
	};

	ResetToDefaults();
}

void EditableStruct::HandleUpdateHeader (Float deltaSec)
{
	bool bChangeDetected = false;
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		if (subProp->HasPropertyTextChanged())
		{
			bChangeDetected = true;
			break;
		}
	}

	if (bChangeDetected)
	{
		RefreshHeaderText();
	}
}
SD_END