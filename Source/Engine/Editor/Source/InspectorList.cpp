/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InspectorList.cpp
=====================================================================
*/

#include "EditorInterface.h"
#include "InspectorList.h"

SD_BEGIN
InspectorList::InspectorList ()
{
	//Noop
}

InspectorList::~InspectorList ()
{
	if (!ContainerUtils::IsEmpty(StructStack))
	{
		DString lastStructName = ContainerUtils::GetLast(StructStack)->PropertyName;
		EditorLog.Log(LogCategory::LL_Warning, TXT("Finished processing InspectorList without clearing the StructStack. For each PushStruct call, there should be a PopStruct. The last struct's name is \"%s\""), lastStructName);
	}

	for (size_t i = 0; i < PropList.size(); ++i)
	{
		delete PropList.at(i);
	}
	ContainerUtils::Empty(OUT PropList);
}

EditableStruct::SInspectorStruct* InspectorList::PushStruct (EditableStruct::SInspectorStruct* newStruct)
{
	CHECK(newStruct != nullptr)

	AddPropImplementation(newStruct);
	StructStack.push_back(newStruct);

	return newStruct;
}

void InspectorList::PopStruct ()
{
	if (ContainerUtils::IsEmpty(StructStack))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Popped too many structs from the InspectorList. There aren't any more structs to pop."));
		return;
	}

	StructStack.pop_back();
}

void InspectorList::MoveListTo (InspectorList* otherList)
{
	if (otherList == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot move the InspectorList property list to a null inspector list."));
		return;
	}

	for (size_t i = 0; i < PropList.size(); ++i)
	{
		otherList->PropList.push_back(PropList.at(i));
	}

	for (size_t i = 0; i < StructStack.size(); ++i)
	{
		otherList->StructStack.push_back(StructStack.at(i));
	}
	
	//The otherList now has ownership over the pointers. Empty this list.
	ContainerUtils::Empty(OUT PropList);
	ContainerUtils::Empty(OUT StructStack);
}

void InspectorList::AddPropImplementation (EditPropertyComponent::SInspectorProperty* newProp)
{
	CHECK(newProp != nullptr)

	if (!ContainerUtils::IsEmpty(StructStack))
	{
		newProp->OwningStruct = ContainerUtils::GetLast(StructStack);
	}

	PropList.push_back(newProp);
}
SD_END