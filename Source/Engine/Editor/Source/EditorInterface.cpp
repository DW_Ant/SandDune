/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorInterface.cpp
=====================================================================
*/

#include "EditableFloat.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "EditorInterface.h"
#include "InspectorList.h"

SD_BEGIN
EditorInterface::~EditorInterface ()
{
	CHECK_INFO(EditorId < 0, "EditorObject's EditorId is not negative when it was deleted. Did you forgot to call UnregisterEditorInterface?")
}

DString EditorInterface::GetCompleteEditorName () const
{
	return GetEditorObjName() + TXT("_") + EditorId.ToString();
}

void EditorInterface::UpdateEditorObjName ()
{
	if (EditorEngineComponent* editorEngine = EditorEngineComponent::Find())
	{
		if (EditorId >= 0)
		{
			editorEngine->RemoveEditorObject(this, RegisteredHash);
		}

		EditorId = -1;
		RegisteredHash.ResetToDefaults();

		editorEngine->AddEditorObject(this, OUT RegisteredHash);
		OnEditorNameChanged.Broadcast();
	}
}

DString EditorInterface::GetEditorObjTooltip () const
{
	return DString::EmptyString;
}

void EditorInterface::AddInspectorCompsTo (GuiComponent* owningComp)
{
	CHECK(owningComp != nullptr)

	InspectorList propList;
	PopulateInspectorList(OUT propList);

	for (EditPropertyComponent::SInspectorProperty* inspectorProp : propList.ReadPropList())
	{
		EditPropertyComponent* newItem = inspectorProp->CreateBlankEditableComponent();
		if (newItem == nullptr)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate an Editable Property Component for the variable \"%s\"."), inspectorProp->PropertyName);
			continue;
		}
		inspectorProp->AssociatedComp = newItem;

		GuiComponent* componentOwner = owningComp;
		if (inspectorProp->OwningStruct != nullptr)
		{
			if (inspectorProp->OwningStruct->AssociatedComp == nullptr)
			{
				EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to attach struct sub property %s to %s. The struct is expected to be initialized earlier in the inspector list before its sub properties."), inspectorProp->PropertyName, inspectorProp->OwningStruct->PropertyName);
			}
			else
			{
				componentOwner = inspectorProp->OwningStruct->AssociatedComp;
			}
		}

		if (componentOwner->AddComponent(newItem))
		{
			inspectorProp->EditPropComponent();
			newItem->SetPosition(SD::Vector2::ZERO_VECTOR);
			newItem->PostInitPropertyComponent();
		}
	}
}

void EditorInterface::SetEditorId (Int newEditorId)
{
	bool invokeDelegate = (EditorId >= 0);
	EditorId = newEditorId;

	if (invokeDelegate)
	{
		OnEditorIdChanged.Broadcast();
	}
}

void EditorInterface::AddVector2ToInspectorList (const DString& propName, Vector2& vect, InspectorList& outList, const Vector2& defaultVect) const
{
	outList.PushStruct(new EditableStruct::SInspectorStruct(propName, DString::EmptyString));
	{
		EditableFloat::SInspectorFloat* xAxis = outList.AddProp(new EditableFloat::SInspectorFloat(TXT("X"), DString::EmptyString, &vect.X));
		xAxis->DefaultValue = defaultVect.X;

		EditableFloat::SInspectorFloat* yAxis = outList.AddProp(new EditableFloat::SInspectorFloat(TXT("Y"), DString::EmptyString, &vect.Y));
		yAxis->DefaultValue = defaultVect.Y;
	}
	outList.PopStruct();
}

void EditorInterface::AddVector3ToInspectorList (const DString& propName, Vector3& vect, InspectorList& outList, const Vector3& defaultVect) const
{
	outList.PushStruct(new EditableStruct::SInspectorStruct(propName, DString::EmptyString));
	{
		EditableFloat::SInspectorFloat* xAxis = outList.AddProp(new EditableFloat::SInspectorFloat(TXT("X"), DString::EmptyString, &vect.X));
		xAxis->DefaultValue = defaultVect.X;

		EditableFloat::SInspectorFloat* yAxis = outList.AddProp(new EditableFloat::SInspectorFloat(TXT("Y"), DString::EmptyString, &vect.Y));
		yAxis->DefaultValue = defaultVect.Y;

		EditableFloat::SInspectorFloat* zAxis = outList.AddProp(new EditableFloat::SInspectorFloat(TXT("Z"), DString::EmptyString, &vect.Z));
		zAxis->DefaultValue = defaultVect.Z;
	}
	outList.PopStruct();
}

void EditorInterface::RegisterEditorInterface ()
{
	if (EditorEngineComponent* editorEngine = EditorEngineComponent::Find())
	{
		editorEngine->AddEditorObject(this, OUT RegisteredHash);
		CHECK(EditorId >= 0)
	}
}

void EditorInterface::UnregisterEditorInterface ()
{
	OnClearEditorReference.Broadcast(this);
	OnClearEditorReference.Empty();

	if (EditorId >= 0)
	{
		if (EditorEngineComponent* editorEngine = EditorEngineComponent::Find())
		{
			editorEngine->RemoveEditorObject(this, RegisteredHash);
		}

		EditorId = -1;
		RegisteredHash.ResetToDefaults();
	}
}
SD_END