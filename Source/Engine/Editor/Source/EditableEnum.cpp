/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableEnum.cpp
=====================================================================
*/

#include "EditableEnum.h"
#include "EditorEngineComponent.h"
#include "EditorTheme.h"

IMPLEMENT_CLASS(SD::EditableEnum, SD::EditPropertyComponent)
SD_BEGIN

EditableEnum::SInspectorEnum::SInspectorEnum (const DString& inPropertyName, const DString& inTooltipText, DynamicEnum* inEnumMeta, Int inDefaultIdx) :
	SInspectorProperty(inPropertyName, inTooltipText),
	EnumMeta(inEnumMeta),
	DefaultIdx(inDefaultIdx),
	InitOverride(inDefaultIdx),
	bEnableSearchBar(false)
{
	//Noop
}

EditableEnum::SInspectorEnum::SInspectorEnum (const DString& inPropertyName, const DString& inTooltipText, const SDFunction<void, DropdownComponent*>& inOnPopulateDropdown, Int inDefaultIdx) :
	SInspectorProperty(inPropertyName, inTooltipText),
	EnumMeta(nullptr),
	DefaultIdx(inDefaultIdx),
	bEnableSearchBar(false),
	OnPopulateDropdown(inOnPopulateDropdown)
{
	//Noop
}

EditableEnum::SInspectorEnum::~SInspectorEnum ()
{
	if (EnumMeta != nullptr)
	{
		delete EnumMeta;
		EnumMeta = nullptr;
	}
}

void EditableEnum::SInspectorEnum::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)

	if (EnumMeta == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString configStr = config->GetPropertyText(sectionName, PropertyName);
	if (!configStr.IsEmpty())
	{
		EnumMeta->AssignFromString(configStr);
	}
}

void EditableEnum::SInspectorEnum::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	if (EnumMeta == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString configStr = EnumMeta->ToString();
	config->SavePropertyText(sectionName, PropertyName, configStr);
}

bool EditableEnum::SInspectorEnum::LoadBinary (const DataBuffer& incomingData)
{
	if (EnumMeta == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorEnum is not associated with a variable."), PropertyName);
		return false;
	}

	Int idx;
	incomingData >> idx;
	if (incomingData.HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the data buffer is unable to read the index Int."), PropertyName);
		return false;
	}

	EnumMeta->AssignFromIndex(idx);
	return true;
}

void EditableEnum::SInspectorEnum::SaveBinary (DataBuffer& outData) const
{
	if (EnumMeta == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to binary since the SInspectorEnum is not associated with a variable."), PropertyName);
		return;
	}

	Int idx = EnumMeta->ToIndex();
	outData << idx;
}

EditPropertyComponent* EditableEnum::SInspectorEnum::CreateBlankEditableComponent () const
{
	return EditableEnum::CreateObject();
}

void EditableEnum::SInspectorEnum::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableEnum* editEnum = dynamic_cast<EditableEnum*>(AssociatedComp))
	{
		editEnum->SetDefaultIndex(DefaultIdx);

		if (DropdownComponent* dropdown = editEnum->GetDropdown())
		{
			if (EnumMeta != nullptr)
			{
				EnumMeta->InitDropdown(dropdown);

				//If EditedEnum is associated with an enum, Transfer ownership from the inspector to the editable enum.
				if (EnumMeta->IsBounded())
				{
					editEnum->EditedEnum = EnumMeta;
					SInspectorEnum* writeSelf = const_cast<SInspectorEnum*>(this);
					writeSelf->EnumMeta = nullptr;
				}
			}
			else if (OnPopulateDropdown.IsBounded())
			{
				OnPopulateDropdown(dropdown);
			}

			dropdown->SetEnableSearchBar(bEnableSearchBar);

			Int initValue = (InitType == IIV_UseOverride) ? InitOverride : DefaultIdx;
			if (initValue != INDEX_NONE)
			{
				//Temporarily unbind OnOptionSelected since the owning EditableComponent (eg: structs and arrays) probably haven't finished constructing their elements.
				SDFunction<void, DropdownComponent*, Int> optionSelected = dropdown->OnOptionSelected;
				dropdown->OnOptionSelected.ClearFunction();
				if (!dropdown->IsItemSelected()) //Handles case where OnPopulateDropdown selects an item.
				{
					dropdown->SetSelectedItemByIdx(initValue);
				}

				dropdown->OnOptionSelected = optionSelected;
			}
		}
	}
}

void EditableEnum::InitProps ()
{
	Super::InitProps();

	NumLinesExpandMenu = 7.f;
	ReadOnlyBlackout = nullptr;
	EditedEnum = nullptr;
	DefaultIdx = INDEX_NONE;
	UpdateEnumTick = nullptr;
}

void EditableEnum::BeginObject ()
{
	Super::BeginObject();

	UpdateEnumTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateEnumTick))
	{
		UpdateEnumTick->SetTickInterval(0.2f); //Not important to refresh continuously.
		UpdateEnumTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableEnum, HandleUpdateEnumTick, void, Float));
	}
}

void EditableEnum::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableEnum* enumTemplate = dynamic_cast<const EditableEnum*>(objTemplate))
	{
		bool bCreatedObj;
		Dropdown = ReplaceTargetWithObjOfMatchingClass(Dropdown.Get(), enumTemplate->GetDropdown(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Dropdown);
		}

		if (Dropdown != nullptr)
		{
			Dropdown->CopyPropertiesFrom(enumTemplate->GetDropdown());
		}

		ReadOnlyBlackout = ReplaceTargetWithObjOfMatchingClass(ReadOnlyBlackout, enumTemplate->ReadOnlyBlackout, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ReadOnlyBlackout);
		}

		if (ReadOnlyBlackout != nullptr)
		{
			ReadOnlyBlackout->CopyPropertiesFrom(enumTemplate->ReadOnlyBlackout);
		}

		SetNumLinesExpandMenu(enumTemplate->GetNumLinesExpandMenu());
	}
}

void EditableEnum::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateEnumTick != nullptr)
	{
		UpdateEnumTick->Destroy();
		UpdateEnumTick = nullptr;
	}
}

void EditableEnum::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (EditedEnum != nullptr)
	{
		EditedEnum->AssignFromIndex(DefaultIdx);
	}

	if (Dropdown != nullptr && DefaultIdx != INDEX_NONE)
	{
		Dropdown->SetSelectedItemByIdx(DefaultIdx.ToUnsignedInt());
	}

	MarkPropertyTextDirty();
	ProcessApplyEdit(this);
}

ButtonComponent* EditableEnum::AddFieldControl ()
{
	ButtonComponent* result = Super::AddFieldControl();

	if (result != nullptr && Dropdown != nullptr)
	{
		Dropdown->SetAnchorRight(FieldControls.size() * FieldButtonSize.X);

		if (ReadOnlyBlackout != nullptr)
		{
			ReadOnlyBlackout->SetAnchorRight(Dropdown->GetAnchorRightDist());
		}
	}

	return result;
}

void EditableEnum::CopyToBuffer (DataBuffer& copyTo) const
{
	Super::CopyToBuffer(OUT copyTo);

	Int idx = INDEX_NONE;
	if (Dropdown != nullptr)
	{
		idx = Dropdown->GetSelectedItemByIdx();
	}

	copyTo << idx;
}

bool EditableEnum::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	Int idx;
	if ((incomingData >> idx).HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read Int from data buffer for %s."), GetPropertyNameStr());
		return false;
	}

	if (idx != INDEX_NONE && Dropdown != nullptr)
	{
		Dropdown->SetSelectedItemByIdx(idx.ToUnsignedInt());
		MarkPropertyTextDirty();
	}

	return true;
}

EditPropertyComponent::SInspectorProperty* EditableEnum::CreateDefaultInspector (const DString& propName) const
{
	DynamicEnum* enumMapCpy = nullptr;
	if (EditedEnum != nullptr)
	{
		enumMapCpy = EditedEnum->Duplicate();
	}

	return new SInspectorEnum(propName, DString::EmptyString, enumMapCpy);
}

void EditableEnum::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (Dropdown != nullptr && BarrierTransform != nullptr)
	{
		Dropdown->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);

		if (ReadOnlyBlackout != nullptr)
		{
			ReadOnlyBlackout->SetAnchorLeft(Dropdown->GetAnchorLeftDist());
		}
	}
}

void EditableEnum::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	if (Dropdown != nullptr)
	{
		Dropdown->SetEnabled(!IsReadOnly());
	}

	if (ReadOnlyBlackout != nullptr)
	{
		ReadOnlyBlackout->SetVisibility(IsReadOnly());
	}
}

void EditableEnum::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	if (Dropdown != nullptr)
	{
		//Plus one to include the extra line that shows the selected item.
		Dropdown->EditSize().Y = (NumLinesExpandMenu + 1.f) * LineHeight;
	}

	if (ReadOnlyBlackout != nullptr)
	{
		ReadOnlyBlackout->EditSize().Y = LineHeight;
	}
}

void EditableEnum::InitializeComponents ()
{
	Super::InitializeComponents();

	Dropdown = DropdownComponent::CreateObject();
	if (AddComponent(Dropdown))
	{
		Dropdown->SetAnchorRight(0.f);
		Dropdown->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);
		Dropdown->SetSize(Vector2(1.f, LineHeight * (NumLinesExpandMenu + 1.f)));
		Dropdown->SetItemSelectionHeight(LineHeight);
		Dropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, EditableEnum, HandleOptionSelected, void, DropdownComponent*, Int);

		ReadOnlyBlackout = FrameComponent::CreateObject();
		if (AddComponent(ReadOnlyBlackout))
		{
			ReadOnlyBlackout->SetAnchorRight(Dropdown->GetAnchorRightDist());
			ReadOnlyBlackout->SetAnchorLeft(Dropdown->GetAnchorLeftDist());
			ReadOnlyBlackout->SetPosition(Vector2::ZERO_VECTOR);
			ReadOnlyBlackout->SetSize(Vector2(1.f, LineHeight));
			ReadOnlyBlackout->SetVisibility(false);
			ReadOnlyBlackout->SetCenterColor(Color(16, 16, 16, 180));
		}
	}
}

DString EditableEnum::ConstructValueAsText () const
{
	if (Dropdown != nullptr)
	{
		if (LabelComponent* label = Dropdown->GetSelectedItemLabel())
		{
			return label->GetContent();
		}
	}

	return DString::EmptyString;
}

void EditableEnum::Destroyed ()
{
	if (EditedEnum != nullptr)
	{
		delete EditedEnum;
		EditedEnum = nullptr;
	}

	Super::Destroyed();
}

void EditableEnum::AddResetToDefaultsControl ()
{
	ButtonComponent* newButton = AddFieldControl();
	if (newButton != nullptr)
	{
		newButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableEnum, HandleResetToDefaultsReleased, void, ButtonComponent*));
		newButton->SetEnabled(!IsReadOnly());
		ResetDefaultsButton = newButton;

		EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());
		if (localTheme != nullptr)
		{
			if (FrameComponent* background = ResetDefaultsButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->FieldControlReset);
			}
		}

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (newButton->AddComponent(tooltip))
		{
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)

			tooltip->SetTooltipText(translator->TranslateText(TXT("ResetDefaults"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
		}
	}
}

void EditableEnum::SetDropdownSelectIdx (Int newIdx, bool bInvokeCallback)
{
	if (Dropdown != nullptr && Dropdown->GetSelectedItemByIdx() != newIdx)
	{
		if (!bInvokeCallback)
		{
			SDFunction<void, DropdownComponent*, Int> originalOptionSelected = Dropdown->OnOptionSelected;
			Dropdown->OnOptionSelected.ClearFunction();
			Dropdown->SetSelectedItemByIdx(newIdx);
			Dropdown->OnOptionSelected = originalOptionSelected;
			ProcessDropdownItemChange(newIdx);
		}
		else
		{
			Dropdown->SetSelectedItemByIdx(newIdx); //The callback will invoke ProcessDropdownItemChange.
		}

		MarkPropertyTextDirty();
	}
}

void EditableEnum::SetNumLinesExpandMenu (Float newNumLinesExpandMenu)
{
	if (NumLinesExpandMenu == newNumLinesExpandMenu)
	{
		return;
	}

	NumLinesExpandMenu = Utils::Max<Float>(1.f, newNumLinesExpandMenu);

	if (Dropdown != nullptr)
	{
		//Plus one to include the extra line that shows the selected item.
		Dropdown->EditSize().Y = (NumLinesExpandMenu + 1.f) * LineHeight;
	}
}

void EditableEnum::SetReadOnlyColor (Color newReadOnlyColor)
{
	if (ReadOnlyBlackout != nullptr)
	{
		ReadOnlyBlackout->SetCenterColor(newReadOnlyColor);
	}
}

void EditableEnum::SetDefaultIndex (Int newDefaultIndex)
{
	DefaultIdx = newDefaultIndex;
	bool isEnabled = false;

	if (Dropdown != nullptr)
	{
		Int curIdx = Dropdown->GetSelectedItemByIdx();
		if (curIdx == INDEX_NONE && Dropdown->GetExpandMenuList() != nullptr && DefaultIdx.ToUnsignedInt() < Dropdown->GetExpandMenuList()->ReadList().size())
		{
			curIdx = DefaultIdx;
			SetDropdownSelectIdx(curIdx, false);
		}

		isEnabled = (curIdx != DefaultIdx);
	}

	if (ResetDefaultsButton == nullptr)
	{
		AddResetToDefaultsControl();
	}

	if (ResetDefaultsButton != nullptr)
	{
		ResetDefaultsButton->SetEnabled(isEnabled);
	}
}

Color EditableEnum::GetReadOnlyColor () const
{
	if (ReadOnlyBlackout != nullptr)
	{
		if (ColorRenderComponent* color = ReadOnlyBlackout->GetCenterCompAs<ColorRenderComponent>())
		{
			return color->SolidColor;
		}
	}

	return Color::INVISIBLE;
}

void EditableEnum::ProcessDropdownItemChange (Int newSelectedIdx)
{
	if (ResetDefaultsButton != nullptr)
	{
		ResetDefaultsButton->SetEnabled(newSelectedIdx != DefaultIdx);
	}

	if (EditedEnum != nullptr)
	{
		EditedEnum->AssignFromIndex(newSelectedIdx);
	}

	MarkPropertyTextDirty();
}

void EditableEnum::HandleOptionSelected (DropdownComponent* dropdown, Int selectedIdx)
{
	ProcessDropdownItemChange(selectedIdx);
	ProcessApplyEdit(this);
}

void EditableEnum::HandleResetToDefaultsReleased (ButtonComponent* uiComp)
{
	if (ResetDefaultsButton != nullptr)
	{
		ResetDefaultsButton->SetEnabled(false);
	}

	if (Dropdown != nullptr && DefaultIdx != INDEX_NONE)
	{
		Dropdown->SetSelectedItemByIdx(DefaultIdx);
	}

	if (EditedEnum != nullptr)
	{
		EditedEnum->AssignFromIndex(DefaultIdx);
	}

	ProcessApplyEdit(this);
	MarkPropertyTextDirty();
}

void EditableEnum::HandleUpdateEnumTick (Float deltaSec)
{
	if (EditedEnum != nullptr && Dropdown != nullptr)
	{
		Int curIdx = EditedEnum->ToIndex();
		Int dropdownIdx = Dropdown->GetSelectedItemByIdx();

		if (curIdx != INDEX_NONE && dropdownIdx != curIdx)
		{
			Dropdown->SetSelectedItemByIdx(curIdx.ToUnsignedInt());
			MarkPropertyTextDirty();
		}
	}
}
SD_END