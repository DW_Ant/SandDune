/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetCatalogue.cpp
=====================================================================
*/

#include "AssetCatalogue.h"
#include "EditorWorkerEngineComponent.h"

IMPLEMENT_CLASS(SD::AssetCatalogue, SD::Entity)
SD_BEGIN

void AssetCatalogue::InitProps ()
{
	Super::InitProps();

	MaxUpdateInterval = -1;
	NumFilesProcessed = 0;
	Tick = nullptr;
}

void AssetCatalogue::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	DString tickGroupName = TICK_GROUP_MISC;

	//The EditorEngineComponent may not exist in this thread. If it doesn't, fallback to the misc catagory.
	if (localEngine->FindTickGroup(TICK_GROUP_EDITOR) != nullptr)
	{
		tickGroupName = TICK_GROUP_EDITOR;
	}

	Tick = TickComponent::CreateObject(tickGroupName);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, AssetCatalogue, HandleTick, void, Float));
		Tick->SetTicking(false);
	}
}

void AssetCatalogue::PopulateCatalogue (const Directory& searchIn, const DString& fileExtension, const std::vector<DString>& inAssetTypes)
{
	NumFilesProcessed = 0;
	ContainerUtils::Empty(OUT RemainingFiles);
	AssetTypes = inAssetTypes;
	ContainerUtils::Empty(OUT FoundAssets);
	NextFileIdx = 0;

	for (FileIterator iter(searchIn, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		if (iter.GetSelectedAttributes().ReadFileExtension().Compare(fileExtension, DString::CC_IgnoreCase) == 0)
		{
			RemainingFiles.push_back(iter.GetSelectedAttributes());
		}
	}

	if (Tick != nullptr)
	{
		Tick->SetTicking(true);
	}
}

void AssetCatalogue::AbortProcess ()
{
	ContainerUtils::Empty(OUT RemainingFiles);
	ContainerUtils::Empty(OUT FoundAssets);

	OnCatalogueUpdate.ClearFunction();

	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}
}

void AssetCatalogue::SetMaxUpdateInterval (Int newMaxUpdateInterval)
{
	MaxUpdateInterval = newMaxUpdateInterval;
}

bool AssetCatalogue::ProcessFile (const FileAttributes& fileAttrib)
{
	ConfigWriter* config = ConfigWriter::CreateObject();
	config->SetReadEverything(false);
	if (!config->OpenFile(fileAttrib, false))
	{
		config->Destroy();
		return false;
	}

	DString readAssetType = config->GetPropertyText(DString::EmptyString, ASSET_TYPE);
	config->Destroy();

	if (readAssetType.IsEmpty())
	{
		//Global assets are always loaded (partially at least)
		return true;
	}

	for (const DString& assetType : AssetTypes)
	{
		if (assetType.Compare(readAssetType, DString::CC_CaseSensitive) == 0)
		{
			return true;
		}
	}

	return false;
}

void AssetCatalogue::ProcessFinishedCatalogue ()
{
#ifdef WITH_MULTI_THREAD
	EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
	if (workerEngine != nullptr)
	{
		workerEngine->SetLatestCatalogue(FoundAssets);
	}
#endif

	if (OnCatalogueUpdate.IsBounded())
	{
		OnCatalogueUpdate(true);
	}

	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}
}

void AssetCatalogue::HandleTick (Float deltaSec)
{
	Int numFilesBeforeUpdate = MaxUpdateInterval;
	while (NextFileIdx < RemainingFiles.size())
	{
		if (ProcessFile(RemainingFiles.at(NextFileIdx)))
		{
			--numFilesBeforeUpdate;
			++NumFilesProcessed;
			FoundAssets.push_back(RemainingFiles.at(NextFileIdx));
			RemainingFiles.erase(RemainingFiles.begin() + NextFileIdx);

			if (MaxUpdateInterval > 0 && numFilesBeforeUpdate <= 0 && OnCatalogueUpdate.IsBounded())
			{
				OnCatalogueUpdate(false);
				return; //Pause here. The next tick cycle will pick up where this left off.
			}
			
			continue;
		}

		++NextFileIdx;
	}

	ProcessFinishedCatalogue();
}
SD_END