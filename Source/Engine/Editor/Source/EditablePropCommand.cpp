/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditablePropCommand.cpp
=====================================================================
*/

#include "EditablePropCommand.h"

IMPLEMENT_CLASS(SD::EditablePropCommand, SD::EditPropertyComponent)
SD_BEGIN

EditablePropCommand::SInspectorPropCmd::SInspectorPropCmd (const DString& inPropertyName, const DString& inTooltipText, const SDFunction<void, ButtonComponent*, EditablePropCommand*>& inOnReleased) :
	SInspectorProperty(inPropertyName, inTooltipText),
	OnPressed(nullptr),
	OnRightPressed(nullptr),
	OnReleased(inOnReleased),
	OnRightReleased(nullptr)
{
	//Noop
}

void EditablePropCommand::SInspectorPropCmd::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	//Noop
}

void EditablePropCommand::SInspectorPropCmd::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	//Noop
}

bool EditablePropCommand::SInspectorPropCmd::LoadBinary (const DataBuffer& incomingData)
{
	return true;
}

void EditablePropCommand::SInspectorPropCmd::SaveBinary (DataBuffer& outData) const
{
	//Noop
}

EditPropertyComponent* EditablePropCommand::SInspectorPropCmd::CreateBlankEditableComponent () const
{
	return EditablePropCommand::CreateObject();
}

void EditablePropCommand::SInspectorPropCmd::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditablePropCommand* propCmd = dynamic_cast<EditablePropCommand*>(AssociatedComp))
	{
		propCmd->OnPressed = OnPressed;
		propCmd->OnRightPressed = OnRightPressed;
		propCmd->OnReleased = OnReleased;
		propCmd->OnRightReleased = OnRightReleased;

		if (ButtonComponent* button = propCmd->Button.Get())
		{
			button->SetCaptionText(PropertyName);
		}
	}
}

void EditablePropCommand::InitProps ()
{
	Super::InitProps();

	bInitPropName = false; //The button will display the name, and there's no barrier.
}

void EditablePropCommand::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditablePropCommand* cmdTemplate = dynamic_cast<const EditablePropCommand*>(objTemplate))
	{
		bool bCreatedObj;
		Button = ReplaceTargetWithObjOfMatchingClass(Button.Get(), cmdTemplate->Button.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Button);
		}

		Button->CopyPropertiesFrom(cmdTemplate->Button.Get());
	}
}

ButtonComponent* EditablePropCommand::AddFieldControl ()
{
	ButtonComponent* result = Super::AddFieldControl();
	if (result != nullptr && Button != nullptr)
	{
		Button->SetAnchorRight(Float(FieldControls.size()) * FieldButtonSize.X);
		Button->SetAnchorLeft(0.f);
	}

	return result;
}

EditPropertyComponent::SInspectorProperty* EditablePropCommand::CreateDefaultInspector (const DString& propName) const
{
	SDFunction<void, ButtonComponent*, EditablePropCommand*> onRelease;
	onRelease.ClearFunction();

	return new SInspectorPropCmd(propName, DString::EmptyString, onRelease);
}

GuiComponent* EditablePropCommand::GetComponentTooltipsShouldAttachTo ()
{
	return Button.Get();
}

void EditablePropCommand::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	if (Button != nullptr)
	{
		Button->SetEnabled(!bReadOnly);
	}
}

void EditablePropCommand::InitializeComponents ()
{
	Super::InitializeComponents();

	Button = ButtonComponent::CreateObject();
	if (AddComponent(Button))
	{
		Button->SetPosition(Vector2(0.035f, 0.035f));
		Button->SetSize(Vector2(1.f, 1.f) - (Button->ReadPosition() * 2.f));
		Button->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, EditablePropCommand, HandleButtonPressed, void, ButtonComponent*));
		Button->SetButtonPressedRightClickHandler(SDFUNCTION_1PARAM(this, EditablePropCommand, HandleButtonRPressed, void, ButtonComponent*));
		Button->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditablePropCommand, HandleButtonReleased, void, ButtonComponent*));
		Button->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, EditablePropCommand, HandleButtonRReleased, void, ButtonComponent*));
	}
}

void EditablePropCommand::HandleButtonPressed (ButtonComponent* button)
{
	if (OnPressed.IsBounded())
	{
		OnPressed(button, this);
	}
}

void EditablePropCommand::HandleButtonRPressed (ButtonComponent* button)
{
	if (OnRightPressed.IsBounded())
	{
		OnRightPressed(button, this);
	}
	else if (OnPressed.IsBounded())
	{
		OnPressed(button, this);
	}
}

void EditablePropCommand::HandleButtonReleased (ButtonComponent* button)
{
	if (OnReleased.IsBounded())
	{
		OnReleased(button, this);
	}
}

void EditablePropCommand::HandleButtonRReleased (ButtonComponent* button)
{
	if (OnRightReleased.IsBounded())
	{
		OnRightReleased(button, this);
	}
	else if (OnReleased.IsBounded())
	{
		OnReleased(button, this);
	}
}
SD_END