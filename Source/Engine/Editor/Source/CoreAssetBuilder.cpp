/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreAssetBuilder.cpp
=====================================================================
*/

#include "CoreAssetBuilder.h"
#include "EditorEngineComponent.h"
#include "GraphAsset.h"
#include "GraphFunction.h"
#include "GraphVariable.h"

IMPLEMENT_ABSTRACT_CLASS(SD::CoreAssetBuilder, SD::Object)
SD_BEGIN

const DString CoreAssetBuilder::LOCALIZATION_SECTION(TXT("CoreAssetBuilder"));

GraphAsset* CoreAssetBuilder::CreateCoreAsset (ClmEngineComponent* clmEngine)
{
	CHECK(clmEngine != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	const DString& fileName = EditorEngineComponent::LOCALIZATION_FILE_NAME;

	GraphAsset* coreAsset = GraphAsset::CreateObject();
	InitAsset(coreAsset);
	coreAsset->InitializeAsset(TXT("Core"), true);
	coreAsset->SetDisplayedName(translator->TranslateText(TXT("Core"), fileName, LOCALIZATION_SECTION));
	coreAsset->EditComment() = translator->TranslateText(TXT("CoreDescription"), fileName, LOCALIZATION_SECTION);

	//Create variable accessors
	{
		std::vector<ScriptVariable::EVarType> supportedVarTypes
		{
			ScriptVariable::VT_Bool,
			ScriptVariable::VT_Int,
			ScriptVariable::VT_Float,
			ScriptVariable::VT_String,
			ScriptVariable::VT_Obj,
			ScriptVariable::VT_Class
		};

		DString getStr = translator->TranslateText(TXT("Get"), fileName, LOCALIZATION_SECTION);
		DString setStr = translator->TranslateText(TXT("Set"), fileName, LOCALIZATION_SECTION);
		DString editStr = translator->TranslateText(TXT("Edit"), fileName, LOCALIZATION_SECTION);
		DString categoryStr = translator->TranslateText(TXT("CategoryVarAccess"), fileName, LOCALIZATION_SECTION);

		//These functions are distinct. There's no CLM Subroutine associated with them, nor there are function definitions (via graph nodes). This is because the compiler will directly access the variable instead of creating a function.
		//The reason why GraphFunctions are created for functions without definitions is because the GraphEditor should still interpret them as a process for visualization purposes.
		for (ScriptVariable::EVarType varType : supportedVarTypes)
		{
			//Getter
			{
				GraphFunction* getter = GraphFunction::CreateObject();
				getter->SetFunctionName(TXT("Get"));
				getter->SetDisplayName(getStr);
				getter->SetOwningAsset(coreAsset);
				getter->SetContextMenuCategory(categoryStr);
				getter->SetGlobal(true);
				getter->SetFinal(true);
				getter->SetUniqueFlag(GraphFunction::UF_Getter);
				getter->SetVariableRelatedFunction(true);

				GraphVariable* outParam = new GraphVariable(HashedString("Var"), getter);
				outParam->SetFixedType(varType);
				outParam->SetShowParamName(false);
				getter->EditOutParams().push_back(outParam);
				coreAsset->EditMemberFunctions().push_back(getter);
			}

			//Setter
			{
				GraphFunction* setter = GraphFunction::CreateObject();
				setter->SetFunctionName(TXT("Set"));
				setter->SetDisplayName(setStr);
				setter->SetOwningAsset(coreAsset);
				setter->SetContextMenuCategory(categoryStr);
				setter->SetGlobal(true);
				setter->SetFinal(true);
				setter->SetUniqueFlag(GraphFunction::UF_Setter);
				setter->SetVariableRelatedFunction(true);
				GraphVariable* inParam = new GraphVariable(HashedString("NewValue"), setter);
				inParam->SetFixedType(varType);
				inParam->SetShowParamName(false);
				setter->EditInParams().push_back(inParam);
				coreAsset->EditMemberFunctions().push_back(setter);
			}

			//Edit
			{
				GraphFunction* editor = GraphFunction::CreateObject();
				editor->SetFunctionName(TXT("Edit"));
				editor->SetDisplayName(editStr);
				editor->SetOwningAsset(coreAsset);
				editor->SetContextMenuCategory(categoryStr);
				editor->SetGlobal(true);
				editor->SetFinal(true);
				editor->SetUniqueFlag(GraphFunction::UF_Edit);
				editor->SetExecPortVisibility(GraphFunction::EPV_AlwaysVisible);
				editor->SetVariableRelatedFunction(true);
				GraphVariable* inParam = new GraphVariable(HashedString("NewValue"), editor);
				inParam->SetFixedType(varType);
				inParam->SetShowParamName(false);
				editor->EditInParams().push_back(inParam);

				GraphVariable* outParam = new GraphVariable(HashedString("Var"), editor);
				outParam->SetFixedType(varType);
				outParam->SetShowParamName(false);
				editor->EditOutParams().push_back(outParam);

				coreAsset->EditMemberFunctions().push_back(editor);
			}
		}
	}

	//Create comparison functions
	{
		DString categoryStr = translator->TranslateText(TXT("CategoryComparison"), fileName, LOCALIZATION_SECTION);

		GraphFunction* newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Bool==Bool"), translator, TXT("CompareEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int==Int"), translator, TXT("CompareEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float==Float"), translator, TXT("CompareEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("String==String"), translator, TXT("CompareEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->EditInParams().at(2)->SetDisplayName(translator->TranslateText(TXT("StringCompare.IsSensitive"), fileName, LOCALIZATION_SECTION));

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Object==Object"), translator, TXT("CompareEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Bool!=Bool"), translator, TXT("CompareNotEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int!=Int"), translator, TXT("CompareNotEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float!=Float"), translator, TXT("CompareNotEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("String!=String"), translator, TXT("CompareNotEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->EditInParams().at(2)->SetDisplayName(translator->TranslateText(TXT("StringCompare.IsSensitive"), fileName, LOCALIZATION_SECTION));

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Object!=Object"), translator, TXT("CompareNotEquals"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int>Int"), translator, TXT("CompareGreaterThan"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float>Float"), translator, TXT("CompareGreaterThan"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int>=Int"), translator, TXT("CompareGreaterThanEqual"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float>=Float"), translator, TXT("CompareGreaterThanEqual"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int<Int"), translator, TXT("CompareLessThan"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float<Float"), translator, TXT("CompareLessThan"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Int<=Int"), translator, TXT("CompareLessThanEqual"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitCompareFunction(clmEngine, coreAsset, HashedString("Float<=Float"), translator, TXT("CompareLessThanEqual"), categoryStr);
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("IsObjectValid"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("IsObjectValid"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetDisplayName(translator->TranslateText(TXT("IsObjectValid.Out"), fileName, LOCALIZATION_SECTION));

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("IsObjectNull"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("IsObjectNull"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetDisplayName(translator->TranslateText(TXT("IsObjectNull.Out"), fileName, LOCALIZATION_SECTION));
	}

	//Create bool operations
	{
		DString categoryStr = translator->TranslateText(TXT("CategoryBoolOperation"), fileName, LOCALIZATION_SECTION);

		GraphFunction* newFunc = nullptr;
		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("&&"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolAnd"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("||"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolOr"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("XOR"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolXor"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("NOT"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolNot"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
	}

	//Create conversion functions
	{
		DString categoryStr = translator->TranslateText(TXT("CategoryConversion"), fileName, LOCALIZATION_SECTION);

		GraphFunction* newFunc = nullptr;
		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("BoolToInt"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolToInt"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("BoolToFloat"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolToFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("BoolToString"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("BoolToString"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("IntToBool"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("IntToBool"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("IntToFloat"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("IntToFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("IntToString"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("IntToString"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("FloatToBool"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("FloatToBool"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("FloatToInt"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("FloatToInt"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("FloatToString"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("FloatToString"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("StringToBool"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("StringToBool"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("StringToInt"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("StringToInt"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		newFunc = InitGlobalFunction(clmEngine, coreAsset, HashedString("StringToFloat"));
		if (newFunc == nullptr)
		{
			coreAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("StringToFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(categoryStr);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
	}

	return coreAsset;
}

GraphAsset* CoreAssetBuilder::CreateMathAsset (ClmEngineComponent* clmEngine)
{
	CHECK(clmEngine != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	const DString& fileName = EditorEngineComponent::LOCALIZATION_FILE_NAME;

	GraphAsset* mathAsset = GraphAsset::CreateObject();
	InitAsset(mathAsset);
	mathAsset->InitializeAsset(TXT("Math"), true);
	mathAsset->SetDisplayedName(translator->TranslateText(TXT("Math"), fileName, LOCALIZATION_SECTION));
	mathAsset->EditComment() = translator->TranslateText(TXT("MathDescription"), fileName, LOCALIZATION_SECTION);

	//Create arithmetic functions
	{
		GraphFunction* newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int+Int"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("AddInt"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		DString addAliases = translator->TranslateText(TXT("AddAliases"), fileName, LOCALIZATION_SECTION);
		std::vector<DString> parts;
		if (!addAliases.IsEmpty())
		{
			addAliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Float+Float"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("AddFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditAliases() = parts;

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("String+String"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("CombineString"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetDisplayName(translator->TranslateText(TXT("CombinedString"), fileName, LOCALIZATION_SECTION));
		newFunc->SetVariableRelatedFunction(true);
		DString aliases = translator->TranslateText(TXT("CombineStringAliases"), fileName, LOCALIZATION_SECTION);
		if (!aliases.IsEmpty())
		{
			aliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int++"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("Increment"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
	}

	//Create difference functions
	{
		GraphFunction* newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int-Int"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("SubtractInt"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		DString aliases = translator->TranslateText(TXT("SubtractAliases"), fileName, LOCALIZATION_SECTION);
		std::vector<DString> parts;
		if (!aliases.IsEmpty())
		{
			aliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Float-Float"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("SubtractFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditAliases() = parts;

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int--"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("Decrement"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(0)->SetHasInlineComp(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
	}

	//Create product functions
	{
		GraphFunction* newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int*Int"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("MultiplyInt"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		DString aliases = translator->TranslateText(TXT("MultiplyAliases"), fileName, LOCALIZATION_SECTION);
		std::vector<DString> parts;
		if (!aliases.IsEmpty())
		{
			aliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Float*Float"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("MultiplyFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditAliases() = parts;
	}

	//Create division functions
	{
		GraphFunction* newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int/Int"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("DivideInt"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		DString aliases = translator->TranslateText(TXT("DivideAliases"), fileName, LOCALIZATION_SECTION);
		std::vector<DString> parts;
		if (!aliases.IsEmpty())
		{
			aliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Float/Float"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("DivideFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditAliases() = parts;

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Int%Int"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("ModuloInt"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		aliases = translator->TranslateText(TXT("ModuloAliases"), fileName, LOCALIZATION_SECTION);
		ContainerUtils::Empty(OUT parts);
		if (!aliases.IsEmpty())
		{
			aliases.ParseString(',', OUT parts, true);
			newFunc->EditAliases() = parts;
		}

		newFunc = InitGlobalFunction(clmEngine, mathAsset, HashedString("Float%Float"));
		if (newFunc == nullptr)
		{
			mathAsset->Destroy();
			return nullptr;
		}
		newFunc->SetDisplayName(translator->TranslateText(TXT("ModuloFloat"), fileName, LOCALIZATION_SECTION));
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditAliases() = parts;
	}

	return mathAsset;
}

void CoreAssetBuilder::InitAsset (GraphAsset* asset)
{
	CHECK(asset != nullptr)
	asset->EditAuthors() = TXT("Translucent Games LLC");
	asset->EditLicense() = TXT("Copyright (c) 2016-2024");
	asset->SetLoadState(GraphAsset::LS_Loaded); //Registers asset to EditorEngineComponent
	asset->SetGlobal(true);
}

GraphFunction* CoreAssetBuilder::InitGlobalFunction (ClmEngineComponent* clmEngine, GraphAsset* functionOwner, const HashedString& functionName)
{
	CHECK(clmEngine != nullptr && functionOwner != nullptr)

	if (!clmEngine->ReadGlobalFunctions().contains(functionName.GetHash()))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize %s since the function %s is not found in the ClmEngineComponent."), functionOwner->ReadAssetName(), functionName.ToString());
		return nullptr;
	}

	Subroutine* nativeAssociation = clmEngine->ReadGlobalFunctions().at(functionName.GetHash());
	CHECK(nativeAssociation != nullptr)

	GraphFunction* graphFunc = GraphFunction::CreateObject();
	functionOwner->EditMemberFunctions().push_back(graphFunc);
	graphFunc->SetFunctionName(functionName.ToString());
	graphFunc->SetOwningAsset(functionOwner);
	graphFunc->SetGlobal(true);
	graphFunc->SetFinal(true);
	graphFunc->SetNativeAssociation(nativeAssociation);
	InitFunctionParams(nativeAssociation, graphFunc);

	return graphFunc;
}

void CoreAssetBuilder::InitFunctionParams (Subroutine* nativeAssociation, GraphFunction* funcToInit)
{
	CHECK(nativeAssociation != nullptr && funcToInit != nullptr)

	std::vector<GraphVariable*> graphInParams;
	for (const ScriptVariable& inParam : nativeAssociation->ReadParams())
	{
		GraphVariable* newParam = new GraphVariable(inParam);
		newParam->SetInspectorType(GraphVariable::IT_InParam);
		graphInParams.push_back(newParam);
	}

	std::vector<GraphVariable*> graphOutParams;
	for (const ScriptVariable& outParam : nativeAssociation->ReadOutput())
	{
		GraphVariable* newParam = new GraphVariable(outParam);
		newParam->SetInspectorType(GraphVariable::IT_OutParam);
		graphOutParams.push_back(newParam);
	}

	funcToInit->SetInParams(graphInParams);
	funcToInit->SetOutParams(graphOutParams);
}

GraphFunction* CoreAssetBuilder::InitCompareFunction (ClmEngineComponent* clmEngine, GraphAsset* functionOwner, const HashedString& functionName, TextTranslator* translator, const DString& translationKey, const DString& contextCategory)
{
	GraphFunction* newFunc = InitGlobalFunction(clmEngine, functionOwner, functionName);
	if (newFunc != nullptr)
	{
		newFunc->SetDisplayName(translator->TranslateText(translationKey, EditorEngineComponent::LOCALIZATION_FILE_NAME, LOCALIZATION_SECTION));
		newFunc->SetContextMenuCategory(contextCategory);
		newFunc->SetVariableRelatedFunction(true);
		newFunc->EditInParams().at(0)->SetShowParamName(false);
		newFunc->EditInParams().at(1)->SetShowParamName(false);
		newFunc->EditOutParams().at(0)->SetShowParamName(false);

		//For Alias, spell out the symbol. For example: instead of ">", it should be "Greater Than"
		DString alias = translator->TranslateText(translationKey + TXT("Alias"), EditorEngineComponent::LOCALIZATION_FILE_NAME, LOCALIZATION_SECTION);
		newFunc->EditAliases().push_back(alias);
	}

	return newFunc;
}
SD_END