/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableObjectComponent.cpp
=====================================================================
*/

#include "EditableObjectComponent.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "EditorInterface.h"
#include "EditorTheme.h"
#include "GraphAsset.h"

IMPLEMENT_CLASS(SD::EditableObjectComponent, SD::EditPropertyComponent)
SD_BEGIN

EditableObjectComponent::SInspectorObject::SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject) : SInspectorProperty(inPropertyName, inTooltipText),
	AssociatedPointer(inAssociatedPointer),
	bCreatesObject(inCreatesObject),
	BaseDClass(nullptr),
	BaseAsset(nullptr)
{
	//Noop
}

EditableObjectComponent::SInspectorObject::SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject, const DClass* inBaseDClass) : SInspectorProperty(inPropertyName, inTooltipText),
	AssociatedPointer(inAssociatedPointer),
	bCreatesObject(inCreatesObject),
	BaseDClass(inBaseDClass),
	BaseAsset(nullptr)
{
	if (AssociatedPointer != nullptr && AssociatedPointer->GetEditorInterface() != nullptr)
	{
		CHECK(dynamic_cast<Object*>(AssociatedPointer->GetEditorInterface()) != nullptr)
	}
}

EditableObjectComponent::SInspectorObject::SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject, GraphAsset* inBaseAsset) : SInspectorProperty(inPropertyName, inTooltipText),
	AssociatedPointer(inAssociatedPointer),
	bCreatesObject(inCreatesObject),
	BaseDClass(nullptr),
	BaseAsset(inBaseAsset)
{
	if (AssociatedPointer != nullptr && AssociatedPointer->GetEditorInterface() != nullptr)
	{
		CHECK(dynamic_cast<GraphAsset*>(AssociatedPointer->GetEditorInterface()) != nullptr)
	}
}

EditableObjectComponent::SInspectorObject::~SInspectorObject ()
{
	if (AssociatedPointer != nullptr)
	{
		delete AssociatedPointer;
		AssociatedPointer = nullptr;
	}
}

void EditableObjectComponent::SInspectorObject::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	//Noop - Loading pointers from a config doesn't make sense.
}

void EditableObjectComponent::SInspectorObject::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	//Noop - Saving pointers to a config doesn't make sense.
}

bool EditableObjectComponent::SInspectorObject::LoadBinary (const DataBuffer& incomingData)
{
	return true;
}

void EditableObjectComponent::SInspectorObject::SaveBinary (DataBuffer& outData) const
{
	//Noop
}

EditPropertyComponent* EditableObjectComponent::SInspectorObject::CreateBlankEditableComponent () const
{
	return EditableObjectComponent::CreateObject();
}

void EditableObjectComponent::SInspectorObject::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableObjectComponent* objComp = dynamic_cast<EditableObjectComponent*>(AssociatedComp))
	{
		objComp->SetCreateNewObject(bCreatesObject);

		if (BaseDClass != nullptr)
		{
			objComp->SetBaseClass(BaseDClass);
		}
		else if (BaseAsset != nullptr)
		{
			objComp->SetBaseAsset(BaseAsset);
		}
		else
		{
			objComp->OnCreateEditorObject = OnCreateEditorObject;
		}

		objComp->OnIsEditorObjectRelevant = OnIsEditorObjectRelevant;
		objComp->OnInitCreatedObject = OnInitCreatedObject;

		if (AssociatedPointer != nullptr)
		{
			objComp->BindVariable(AssociatedPointer);
			AssociatedPointer = nullptr; //The objComp took ownership.
		}
	}
}

void EditableObjectComponent::InitProps ()
{
	Super::InitProps();

	bCreatesNewObject = false;
	EditedObject = nullptr;
	SelectModeOverlayColor = Color(128, 128, 128, 175);
	AssociatedVariable = nullptr;
	UpdateReferenceTick = nullptr;
	ObjNameField = nullptr;
	ClearButton = nullptr;
	CreateObjButton = nullptr;
	DuplicateObjButton = nullptr;
	DuplicateObjField = nullptr;
	SelectObjButton = nullptr;
	DetectedMouse = nullptr;
	SelectModeScreenCap = nullptr;
	SelectModeOverlay = nullptr;

	BaseClass = nullptr;
	bCreateObjectInitialized = false;
}

void EditableObjectComponent::BeginObject ()
{
	Super::BeginObject();

	UpdateReferenceTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateReferenceTick))
	{
		UpdateReferenceTick->SetTickInterval(0.2f);
		UpdateReferenceTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleUpdateReferenceTick, void, Float));
	}
}

void EditableObjectComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableObjectComponent* editObjTemplate = dynamic_cast<const EditableObjectComponent*>(objTemplate))
	{
		SetSelectModeOverlayColor(editObjTemplate->GetSelectModeOverlayColor());

		bool bCreatedObj;
		ObjNameField = ReplaceTargetWithObjOfMatchingClass(ObjNameField, editObjTemplate->ObjNameField, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ObjNameField);
		}

		if (ObjNameField != nullptr)
		{
			ObjNameField->CopyPropertiesFrom(editObjTemplate->ObjNameField);
		}

		ObjTypeDropdown = ReplaceTargetWithObjOfMatchingClass(ObjTypeDropdown.Get(), editObjTemplate->ObjTypeDropdown.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ObjTypeDropdown);
		}

		if (ObjTypeDropdown != nullptr)
		{
			ObjTypeDropdown->CopyPropertiesFrom(editObjTemplate->ObjTypeDropdown.Get());
		}

		ExpandableComponent = ReplaceTargetWithObjOfMatchingClass(ExpandableComponent.Get(), editObjTemplate->ExpandableComponent.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(ExpandableComponent))
			{
				ExpandableComponent->OnToggleCollapse = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleToggleExpandComp, void, bool);
			}
		}

		if (ExpandableComponent != nullptr)
		{
			ExpandableComponent->CopyPropertiesFrom(editObjTemplate->ExpandableComponent.Get());
		}
	}
}

ButtonComponent* EditableObjectComponent::AddFieldControl ()
{
	ButtonComponent* result = Super::AddFieldControl();
	if (result != nullptr)
	{
		Float anchorRight = FieldControls.size() * FieldButtonSize.X;
		if (ObjTypeDropdown != nullptr)
		{
			ObjTypeDropdown->SetAnchorRight(anchorRight);
		}

		if (ObjNameField != nullptr)
		{
			ObjNameField->SetAnchorRight(anchorRight);
		}

		if (DuplicateObjField != nullptr)
		{
			DuplicateObjField->SetAnchorRight(anchorRight);
		}
	}

	return result;
}

void EditableObjectComponent::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (BarrierTransform != nullptr)
	{
		Float leftAnchor = BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X;
		if (ObjTypeDropdown != nullptr)
		{
			ObjTypeDropdown->SetAnchorLeft(leftAnchor);
		}

		if (ObjNameField != nullptr)
		{
			ObjNameField->SetAnchorLeft(leftAnchor);
		}

		if (DuplicateObjField != nullptr)
		{
			DuplicateObjField->SetAnchorLeft(leftAnchor);
		}
	}
}

void EditableObjectComponent::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateReferenceTick != nullptr)
	{
		UpdateReferenceTick->Destroy();
		UpdateReferenceTick = nullptr;
	}
}

void EditableObjectComponent::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (AssociatedVariable != nullptr)
	{
		SetEditedObject(nullptr);
	}
}

void EditableObjectComponent::UnbindVariable ()
{
	Super::UnbindVariable();

	BindVariable(nullptr);
}

void EditableObjectComponent::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	if (ClearButton != nullptr)
	{
		ClearButton->SetEnabled(!bNewReadOnly);
	}

	if (CreateObjButton != nullptr)
	{
		CreateObjButton->SetEnabled(!bNewReadOnly);
	}

	if (DuplicateObjButton != nullptr)
	{
		DuplicateObjButton->SetEnabled(!bNewReadOnly);
	}

	if (DuplicateObjField != nullptr)
	{
		DuplicateObjField->SetEditable(!bNewReadOnly);
	}

	if (SelectObjButton != nullptr)
	{
		SelectObjButton->SetEnabled(!bNewReadOnly);
	}

	if (ObjNameField != nullptr)
	{
		ObjNameField->SetEditable(!bCreatesNewObject && !bNewReadOnly);
	}
}

void EditableObjectComponent::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	if (ObjTypeDropdown != nullptr)
	{
		ObjTypeDropdown->SetItemSelectionHeight(LineHeight);
	}

	if (ObjNameField != nullptr)
	{
		ObjNameField->EditSize().Y = LineHeight;
	}

	if (DuplicateObjField != nullptr)
	{
		DuplicateObjField->EditSize().Y = LineHeight;
	}

	if (ExpandableComponent != nullptr)
	{
		ExpandableComponent->SetAnchorTop(LineHeight);
	}
}

void EditableObjectComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (IsWithinBounds(mouse->ReadPosition()))
	{
		DetectedMouse = mouse;
	}

	if (IsInCopyObjectMode() && DuplicateObjField != nullptr && !DuplicateObjField->IsWithinBounds(mouse->ReadPosition()))
	{
		EndCopyObjectMode();
	}
}

bool EditableObjectComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	if (Super::ExecuteConsumableInput(evnt))
	{
		return true;
	}

	if (IsInCopyObjectMode())
	{
		if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
		{
			EndCopyObjectMode();
			return true;
		}
	}

	return false;
}

void EditableObjectComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	Float leftAnchor = BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X;

	ObjTypeDropdown = DropdownComponent::CreateObject();
	if (AddComponent(ObjTypeDropdown))
	{
		ObjTypeDropdown->SetAnchorRight(0.f);
		ObjTypeDropdown->SetAnchorLeft(leftAnchor);
		ObjTypeDropdown->SetItemSelectionHeight(LineHeight);
		ObjTypeDropdown->EditSize().Y = LineHeight * 8.f;
		ObjTypeDropdown->SetVisibility(false);
	}

	ObjNameField = TextFieldComponent::CreateObject();
	if (AddComponent(ObjNameField))
	{
		ObjNameField->SetAutoRefresh(false);
		ObjNameField->EditSize().Y = LineHeight;
		ObjNameField->SetAnchorRight(0.f);
		ObjNameField->SetAnchorLeft(leftAnchor);
		ObjNameField->SetSingleLine(true);
		ObjNameField->SetText(ConstructValueAsText());
		ObjNameField->SetVisibility(false);
		ObjNameField->SetAutoRefresh(true);

		ObjNameField->OnReturn = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleObjNameFieldReturn, void, TextFieldComponent*);
	}

	ExpandableComponent = EditableStruct::CreateObject();
	if (AddComponent(ExpandableComponent))
	{
		ExpandableComponent->SetAnchorTop(LineHeight);
		ExpandableComponent->SetDynamicHeader(false);
		ExpandableComponent->SetVisibility(false);
		ExpandableComponent->OnToggleCollapse = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleToggleExpandComp, void, bool);
	}
}

DString EditableObjectComponent::ConstructValueAsText () const
{
	if (EditedObject != nullptr)
	{
		return EditedObject->GetCompleteEditorName();
	}

	return TXT("None");
}

void EditableObjectComponent::Destroyed ()
{
	if (SelectModeOverlay != nullptr)
	{
		SelectModeOverlay->Destroy();
		SelectModeOverlay = nullptr;
	}

	if (SelectModeScreenCap != nullptr)
	{
		SelectModeScreenCap->Destroy();
		SelectModeScreenCap = nullptr;
	}

	if (EditedObject != nullptr)
	{
		EditedObject->OnEditorIdChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeId, void));
		EditedObject->OnEditorNameChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeName, void));
		EditedObject->OnClearEditorReference.TryUnregisterHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleEditedObjectDestroyed, void, EditorInterface*));

		if (OwnsReferencedObj())
		{
			EditedObject->DeleteEditorObject();
		}

		EditedObject = nullptr;
	}

	if (AssociatedVariable != nullptr)
	{
		delete AssociatedVariable;
		AssociatedVariable = nullptr;
	}

	Super::Destroyed();
}

void EditableObjectComponent::EnterSelectMode ()
{
	if (SelectModeScreenCap != nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("%s is already in select object mode."), ToString());
		return;
	}

	Window* viewModeTarget = dynamic_cast<Window*>(FindSelectModeRenderTarget());
	if (viewModeTarget == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("%s is unable to enter SelectMode since it doesn't have a RenderTarget associated with it."), ToString());
		return;
	}

	//Draw layer must have a PlanarDrawLayer to render the texture sprite on top of it.
	PlanarDrawLayer* topDrawLayer = nullptr;
	if (!ContainerUtils::IsEmpty(viewModeTarget->ReadDrawLayers()))
	{
		size_t i = viewModeTarget->ReadDrawLayers().size() - 1;
		while (true)
		{
			if (topDrawLayer = dynamic_cast<PlanarDrawLayer*>(viewModeTarget->ReadDrawLayers().at(i).Layer))
			{
				break;
			}

			if (i == 0)
			{
				break;
			}

			--i;
		}
	}
	
	if (topDrawLayer == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("%s is unable to enter SelectMode since the specified RenderTarget %s does not have a PlanarDrawLayer to register the sprite over."), ToString(), viewModeTarget->ToString());
		return;
	}

	SelectModeScreenCap = ScreenCapture::CreateObject();
	SelectModeScreenCap->SetPixelMapType(ScreenCapture::PMT_PerPixel);
	SelectModeScreenCap->OnIsRelevant = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleIsScreenCapRelevant, bool, RenderComponent*);
	if (!SelectModeScreenCap->GenerateCaptureTexture(viewModeTarget, SelectModeOverlayColor))
	{
		ExitSelectMode();
		return;
	}

	if (SelectModeScreenCap->GetNumCapturedComponents() <= 0)
	{
		ExitSelectMode();
		BeginCopyObjectMode();
		return;
	}
	HideCaption();

	SelectModeOverlay = Entity::CreateObject();
	PlanarTransformComponent* transform = PlanarTransformComponent::CreateObject();
	if (SelectModeOverlay->AddComponent(transform))
	{
		transform->SetPosition(Vector2::ZERO_VECTOR);
		transform->SetSize(Vector2(1.f, 1.f));
		topDrawLayer->RegisterPlanarObject(transform);

		SpriteComponent* sprite = SpriteComponent::CreateObject();
		if (transform->AddComponent(sprite))
		{
			sprite->SetSpriteTexture(SelectModeScreenCap->GetCapturedTexture());
		}
	}

	InputBroadcaster* broadcaster = InputBroadcaster::FindBroadcaster(viewModeTarget);
	if (broadcaster != nullptr)
	{
		InputComponent* input = InputComponent::CreateObject();
		if (SelectModeOverlay->AddComponent(input))
		{
			broadcaster->AddInputComponent(input, MousePointer::INPUT_PRIORITY - 100); //Slightly less priority than the mouse pointer.
			input->OnInput = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleSelectModeInput, bool, const sf::Event&);
			input->OnText = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleSelectModeText, bool, const sf::Event&);
			input->OnMouseClick = SDFUNCTION_3PARAM(this, EditableObjectComponent, HandleSelectModeMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
			input->OnMouseWheel = SDFUNCTION_2PARAM(this, EditableObjectComponent, HandleSelectModeMouseWheel, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
		}
	}

	if (DetectedMouse != nullptr)
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		if (const Texture* newIcon = localTexturePool->GetTexture(HashedString("Engine.Input.CursorPrecision")))
		{
			DetectedMouse->PushMouseIconOverride(newIcon, SDFUNCTION_2PARAM(this, EditableObjectComponent, HandleSelectModeMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}
	}
}

bool EditableObjectComponent::IsInSelectMode () const
{
	return (SelectModeScreenCap != nullptr);
}

void EditableObjectComponent::ExitSelectMode ()
{
	if (SelectModeOverlay != nullptr)
	{
		SelectModeOverlay->Destroy();
		SelectModeOverlay = nullptr;
	}

	if (SelectModeScreenCap != nullptr)
	{
		SelectModeScreenCap->Destroy();
		SelectModeScreenCap = nullptr;
	}
}

void EditableObjectComponent::BindVariable (BaseObjBinder* newAssociatedVariable)
{
	if (AssociatedVariable != nullptr)
	{
		delete AssociatedVariable;
		AssociatedVariable = nullptr;
	}

	AssociatedVariable = newAssociatedVariable;
	
	if (AssociatedVariable != nullptr)
	{
		SetEditedObject(AssociatedVariable->GetEditorInterface());
	}
	else
	{
		SetEditedObject(nullptr);
	}
}

void EditableObjectComponent::SetBaseClass (const DClass* parentClass)
{
	if (!bCreateObjectInitialized)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("SetBaseClass depends on if bCreateNewObject is set or not. Be sure to call SetCreateNewObject before calling SetBaseClass. Failed to initialize %s"), GetPropertyNameStr());
		return;
	}

	if (parentClass == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot call SetBaseClass without specifying a parent class. Failed to initialize %s."), GetPropertyNameStr());
		return;
	}

	BaseClass = parentClass;
	if (bCreatesNewObject)
	{
		OnCreateEditorObject = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleCreateObject, EditorInterface*, DropdownComponent*);
		if (ObjTypeDropdown != nullptr)
		{
			std::vector<GuiDataElement<const DClass*>> classes;
			for (ClassIterator iter(parentClass); iter.GetSelectedClass() != nullptr; ++iter)
			{
				const DClass* selectedClass = iter.GetSelectedClass();
				if (!selectedClass->IsAbstract())
				{
					if (dynamic_cast<const EditorInterface*>(selectedClass->GetDefaultObject()) != nullptr)
					{
						classes.emplace_back(selectedClass, selectedClass->GetClassNameWithoutNamespace());
					}
				}
			}

			if (!ContainerUtils::IsEmpty(classes))
			{
				//Sort alphabetically
				std::sort(classes.begin(), classes.end(), [](const GuiDataElement<const DClass*>& a, const GuiDataElement<const DClass*>& b)
				{
					return (a.ReadLabelText().Compare(b.ReadLabelText(), DString::CC_CaseSensitive) < 0);
				});

				if (ListBoxComponent* typeList = ObjTypeDropdown->GetExpandMenuList())
				{
					typeList->SetList(classes);
				}

				if (!parentClass->IsAbstract() && dynamic_cast<const EditorInterface*>(parentClass->GetDefaultObject()) != nullptr)
				{
					//Try to default the selection to the parent class
					ObjTypeDropdown->SetSelectedItem(parentClass);
				}

				ObjTypeDropdown->SetVisibility(classes.size() > 1);
			}
		}
	}

	OnIsEditorObjectRelevant = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleIsObjectRelevant, bool, EditorInterface*);
}

void EditableObjectComponent::SetBaseAsset (GraphAsset* parentAsset)
{
	if (!bCreateObjectInitialized)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("SetBaseAsset depends on if bCreateNewObject is set or not. Be sure to call SetCreateNewObject before calling SetBaseAsset. Failed to initialize %s"), GetPropertyNameStr());
		return;
	}

	if (parentAsset == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot call SetBaseAsset without specifying a parent class. Failed to initialize %s."), GetPropertyNameStr());
		return;
	}

	BaseAsset = parentAsset;
	if (bCreatesNewObject)
	{
		OnCreateEditorObject = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleCreateGraphAsset, EditorInterface*, DropdownComponent*);
		if (ObjTypeDropdown != nullptr)
		{
			std::vector<GuiDataElement<GraphAsset*>> assets;

			EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
			if (editorEngine != nullptr)
			{
				for (const std::pair<HashedString, GraphAsset*>& asset : editorEngine->ReadLoadedAssets())
				{
					if (asset.second->IsAbstract())
					{
						continue;
					}

					if (asset.second == parentAsset || asset.second->IsChildOf(parentAsset))
					{
						assets.emplace_back(asset.second, asset.second->GetPresentedName());
					}
				}
			}

			if (!ContainerUtils::IsEmpty(assets))
			{
				//Sort alphabetically
				std::sort(assets.begin(), assets.end(), [](const GuiDataElement<GraphAsset*>& a, const GuiDataElement<GraphAsset*>& b)
				{
					return (a.ReadLabelText().Compare(b.ReadLabelText(), DString::CC_CaseSensitive) < 0);
				});

				if (ListBoxComponent* typeList = ObjTypeDropdown->GetExpandMenuList())
				{
					typeList->SetList(assets);
				}

				//Try to default the selection to the parent class
				ObjTypeDropdown->SetSelectedItem(parentAsset);
				ObjTypeDropdown->SetVisibility(assets.size() > 1);
			}
		}
	}

	OnIsEditorObjectRelevant = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleIsObjectRelevant, bool, EditorInterface*);
}

void EditableObjectComponent::MovePointer (EditorInterface* newObj)
{
	EditedObject = newObj;
}

void EditableObjectComponent::SetCreateNewObject (bool newCreateNewObject)
{
	if (bCreateObjectInitialized)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot call EditableObjectComponent::SetCreateNewObject more than once. %s is already initialized."), GetPropertyNameStr());
		return;
	}
	bCreateObjectInitialized = true;

	bCreatesNewObject = newCreateNewObject;
	UpdateFieldControls();
	RefreshObjTypeVisibility();

	if (ObjNameField != nullptr)
	{
		ObjNameField->SetVisibility(!bCreatesNewObject || EditedObject != nullptr);
	}
}

void EditableObjectComponent::SetEditedObject (EditorInterface* newEditedObject)
{
	if (EditedObject == newEditedObject)
	{
		return;
	}

	if (EditedObject != nullptr)
	{
		EditedObject->OnEditorIdChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeId, void));
		EditedObject->OnEditorNameChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeName, void));
		EditedObject->OnClearEditorReference.TryUnregisterHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleEditedObjectDestroyed, void, EditorInterface*));

		if (bCreatesNewObject)
		{
			EditedObject->DeleteEditorObject();
		}
	}

	EditedObject = newEditedObject;
	if (AssociatedVariable != nullptr)
	{
		//Update the asset's or object's var reference to point at this new EditedObject.
		AssociatedVariable->SetObjReference(EditedObject);
	}

	if (EditedObject != nullptr)
	{
		EditedObject->OnEditorIdChanged.RegisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeId, void));
		EditedObject->OnEditorNameChanged.RegisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeName, void));
		EditedObject->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleEditedObjectDestroyed, void, EditorInterface*));
	}

	if (ExpandableComponent != nullptr)
	{
		ExpandableComponent->DestroyStructElements();
		ExpandableComponent->SetCollapsed(true);
		ExpandableComponent->SetVisibility(EditedObject != nullptr);
		ExpandableComponent->SetStaticHeaderText(ConstructValueAsText());
		MarkVerticalSpaceDirty(); //Since ExpandableComponent may become visible/invisible, which would change the vertical space of this component.
	}

	UpdateFieldControls();

	RefreshObjTypeVisibility();

	if (ObjNameField != nullptr)
	{
		ObjNameField->SetVisibility(!bCreatesNewObject || EditedObject != nullptr);
		ObjNameField->SetEditable(!bCreatesNewObject && !IsReadOnly());
		ObjNameField->SetText(ConstructValueAsText());
	}
}

void EditableObjectComponent::SetSelectModeOverlayColor (Color newSelectModeOverlayColor)
{
	SelectModeOverlayColor = newSelectModeOverlayColor;
}

bool EditableObjectComponent::OwnsReferencedObj () const
{
	/*
	This component will never take ownership over the object if it's referencing an existing object.
	If this component is bound to a variable, then that variable should take ownership over the object instead of this component.
	*/
	return (bCreatesNewObject && AssociatedVariable == nullptr);
}

RenderTarget* EditableObjectComponent::FindSelectModeRenderTarget ()
{
	RenderTarget* result = const_cast<RenderTarget*>(FindRootRenderTarget()); //Start from the root transform's render target.

	RenderTexture* rendTexture = dynamic_cast<RenderTexture*>(result);
	while (rendTexture != nullptr)
	{
		result = rendTexture->GetOuterRenderTarget();
		rendTexture = dynamic_cast<RenderTexture*>(result);
	}

	return result;
}

void EditableObjectComponent::UpdateFieldControls ()
{
	EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());

	if (EditedObject != nullptr)
	{
		if (CreateObjButton != nullptr)
		{
			ContainerUtils::RemoveItem(OUT FieldControls, CreateObjButton);
			CreateObjButton->Destroy();
			CreateObjButton = nullptr;
		}

		if (DuplicateObjButton != nullptr)
		{
			ContainerUtils::RemoveItem(OUT FieldControls, DuplicateObjButton);
			DuplicateObjButton->Destroy();
			DuplicateObjButton = nullptr;
		}

		if (SelectObjButton != nullptr)
		{
			ContainerUtils::RemoveItem(OUT FieldControls, SelectObjButton);
			SelectObjButton->Destroy();
			SelectObjButton = nullptr;
		}

		if (ClearButton == nullptr)
		{
			if (ClearButton = AddFieldControl())
			{
				ClearButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleClearButtonReleased, void, ButtonComponent*));
				ClearButton->SetEnabled(!IsReadOnly());

				if (localTheme != nullptr && ClearButton->GetBackground() != nullptr)
				{
					ClearButton->GetBackground()->SetCenterTexture(localTheme->FieldControlDelete);
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (ClearButton->AddComponent(tooltip))
				{
					TextTranslator* translator = TextTranslator::GetTranslator();
					CHECK(translator != nullptr)
					tooltip->SetTooltipText(translator->TranslateText(TXT("DeleteObject"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
				}
			}
		}
	}
	else //EditedObject is nullptr
	{
		if (ClearButton != nullptr)
		{
			ContainerUtils::RemoveItems(OUT FieldControls, ClearButton);
			ClearButton->Destroy();
			ClearButton = nullptr;
		}

		if (bCreatesNewObject)
		{
			if (DuplicateObjButton == nullptr)
			{
				if (DuplicateObjButton = AddFieldControl())
				{
					DuplicateObjButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleDuplicateButtonReleased, void, ButtonComponent*));
					DuplicateObjButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleDuplicateButtonReleasedRightClick, void, ButtonComponent*));
					DuplicateObjButton->SetEnabled(!IsReadOnly());

					if (localTheme != nullptr && DuplicateObjButton->GetBackground() != nullptr)
					{
						DuplicateObjButton->GetBackground()->SetCenterTexture(localTheme->FieldControlCopy);
					}

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (DuplicateObjButton->AddComponent(tooltip))
					{
						TextTranslator* translator = TextTranslator::GetTranslator();
						CHECK(translator != nullptr)

						tooltip->SetTooltipText(translator->TranslateText(TXT("DuplicateObject"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
					}
				}
			}

			if (CreateObjButton == nullptr)
			{
				if (CreateObjButton = AddFieldControl())
				{
					CreateObjButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleCreateButtonReleased, void, ButtonComponent*));
					CreateObjButton->SetEnabled(!IsReadOnly());

					if (localTheme != nullptr && CreateObjButton->GetBackground() != nullptr)
					{
						CreateObjButton->GetBackground()->SetCenterTexture(localTheme->FieldControlCreate);
					}

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (CreateObjButton->AddComponent(tooltip))
					{
						TextTranslator* translator = TextTranslator::GetTranslator();
						CHECK(translator != nullptr)

						tooltip->SetTooltipText(translator->TranslateText(TXT("CreateObject"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
					}
				}
			}
		}
		else
		{
			if (SelectObjButton == nullptr)
			{
				if (SelectObjButton = AddFieldControl())
				{
					SelectObjButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleSelectObjButtonReleased, void, ButtonComponent*));
					SelectObjButton->SetEnabled(!IsReadOnly());

					if (localTheme != nullptr && SelectObjButton->GetBackground() != nullptr)
					{
						SelectObjButton->GetBackground()->SetCenterTexture(localTheme->FieldControlTarget);
					}

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (SelectObjButton->AddComponent(tooltip))
					{
						TextTranslator* translator = TextTranslator::GetTranslator();
						CHECK(translator != nullptr)

						tooltip->SetTooltipText(translator->TranslateText(TXT("SelectObject"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
					}
				}
			}
		}
	}

	FixFieldControlOrder();
}

void EditableObjectComponent::FixFieldControlOrder ()
{
	std::function<void(ButtonComponent*, size_t)> moveButtonTo([&](ButtonComponent* buttonToMove, size_t newIdx)
	{
		ContainerUtils::RemoveItem(OUT FieldControls, buttonToMove);
		FieldControls.insert(FieldControls.begin() + newIdx, buttonToMove);
	});

	size_t idx = 0;

	//The Duplicate object button should always be to the right of create object
	if (DuplicateObjButton != nullptr)
	{
		moveButtonTo(DuplicateObjButton, idx);
		idx++;
	}

	if (CreateObjButton != nullptr)
	{
		moveButtonTo(CreateObjButton, idx);
		idx++;
	}

	if (SelectObjButton != nullptr)
	{
		moveButtonTo(SelectObjButton, idx);
		idx++;
	}

	if (ClearButton != nullptr)
	{
		moveButtonTo(ClearButton, idx);
	}

	for (size_t i = 0; i < FieldControls.size(); ++i)
	{
		FieldControls.at(i)->SetAnchorRight(Float(i) * FieldButtonSize.X);
	}
}

void EditableObjectComponent::BeginCopyObjectMode ()
{
	if (IsInCopyObjectMode())
	{
		return;
	}

	CHECK(DuplicateObjField == nullptr && ObjNameField != nullptr)

	DuplicateObjField = TextFieldComponent::CreateObject();
	if (AddComponent(DuplicateObjField))
	{
		Float leftAnchor = BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X;
		Float rightAnchor = FieldControls.size() * FieldButtonSize.X;

		DuplicateObjField->SetAutoRefresh(false);
		DuplicateObjField->SetPosition(Vector2::ZERO_VECTOR);
		DuplicateObjField->SetSize(Vector2(1.f, LineHeight));
		DuplicateObjField->SetAnchorLeft(leftAnchor);
		DuplicateObjField->SetAnchorRight(rightAnchor);
		DuplicateObjField->SetSingleLine(true);
		DuplicateObjField->SetEditable(!IsReadOnly());
		DuplicateObjField->SetAutoRefresh(true);
		DuplicateObjField->OnReturn = SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleDuplicateObjFieldReturn, void, TextFieldComponent*);
		DuplicateObjField->SetCursorPosition(0);

		RefreshObjTypeVisibility();

		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		ShowCaption(translator->TranslateText(TXT("EnterCopyObjectMode"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("EditableObjectComponent")), CaptionInfoColors);
	}
}

bool EditableObjectComponent::IsInCopyObjectMode () const
{
	return (DuplicateObjField != nullptr);
}

void EditableObjectComponent::EndCopyObjectMode ()
{
	if (DuplicateObjField != nullptr)
	{
		DuplicateObjField->Destroy();
		DuplicateObjField = nullptr;

		RefreshObjTypeVisibility();
	}

	HideCaption();
}

bool EditableObjectComponent::CopyObjectByName (const DString& objName)
{
	//Object duplication should only work for components that'll take ownership over the copy. Otherwise, there will be a memory leak when referencing an object copy while also not taking ownership over it.
	CHECK(bCreatesNewObject)

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString sectionName = TXT("EditableObjectComponent");

	Int underscoreIdx = objName.Find('_', 0, DString::SD_RightToLeft);
	if (underscoreIdx < 0 || underscoreIdx >= objName.Length() - 1) //Cannot end with trailing underscore
	{
		DString errorMsg = DString::CreateFormattedString(translator->TranslateText(TXT("CopyObjectError-InvalidFormat"), editorEngine->LOCALIZATION_FILE_NAME, sectionName), objName);
		ShowCaption(errorMsg, CaptionErrorColors);
		return false;
	}

	DString firstPart;
	DString secondPart;
	objName.SplitString(underscoreIdx, OUT firstPart, OUT secondPart);
	Int editorId = secondPart.Atoi();

	EditorInterface* associatedObj = editorEngine->FindEditorObject(firstPart, editorId, DString::CC_IgnoreCase);
	if (associatedObj == nullptr)
	{
		DString errorMsg = DString::CreateFormattedString(translator->TranslateText(TXT("CopyObjectError-ObjectNotFound"), editorEngine->LOCALIZATION_FILE_NAME, sectionName), objName);
		ShowCaption(errorMsg, CaptionErrorColors);
		return false;
	}

	if (AssociatedVariable != nullptr && !AssociatedVariable->CanBeAssignedTo(associatedObj))
	{
		DString errorMsg = DString::CreateFormattedString(translator->TranslateText(TXT("CopyObjectError-CannotBeAssigned"), editorEngine->LOCALIZATION_FILE_NAME, sectionName), objName);
		ShowCaption(errorMsg, CaptionErrorColors);
		return false;
	}

	if (OnIsEditorObjectRelevant.IsBounded() && !OnIsEditorObjectRelevant(associatedObj))
	{
		DString errorMsg = DString::CreateFormattedString(translator->TranslateText(TXT("CopyObjectError-IsNotRelevant"), editorEngine->LOCALIZATION_FILE_NAME, sectionName), objName);
		ShowCaption(errorMsg, CaptionErrorColors);
		return false;
	}

	EditorInterface* objCpy = dynamic_cast<EditorInterface*>(associatedObj->Duplicate());
	CHECK(objCpy != nullptr)

	if (OnInitCreatedObject.IsBounded())
	{
		OnInitCreatedObject(objCpy);
	}

	SetEditedObject(objCpy);
	return true;
}

void EditableObjectComponent::PopulateObjectProperties ()
{
	if (EditedObject != nullptr && ExpandableComponent != nullptr)
	{
		EditedObject->AddInspectorCompsTo(ExpandableComponent.Get());
	}
}

void EditableObjectComponent::ProcessObjNameChange ()
{
	CHECK(ObjNameField != nullptr && !bCreatesNewObject) //Shouldn't be able to hit Enter on this field if it creates a new object.
	DString fieldText = ObjNameField->GetContent();

	//Find the last underscore position since that will be used to split the name from the EditorId
	Int underscoreIdx = fieldText.Find('_', 0, DString::SD_RightToLeft);
	if (underscoreIdx == INT_INDEX_NONE || underscoreIdx < 1 || underscoreIdx >= fieldText.Length()) //Must be at least one character between (start, underscore) and (underscore, end).
	{
		SetEditedObject(nullptr);
		ObjNameField->SetText(ConstructValueAsText());
		return;
	}

	DString baseName = fieldText.SubString(0, underscoreIdx - 1);
	CHECK(!baseName.IsEmpty())

	Int editorId = fieldText.SubString(underscoreIdx + 1).Atoi();

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	if (editorEngine == nullptr)
	{
		//This could happen if running an application without an editor engine component, but using this component for its UI (eg: TextureEditor).
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to process object name \"%s\" since the EditorEngineComponent doesn't exist. The engine component is used for the object registry."), fieldText);
		SetEditedObject(nullptr);
		ObjNameField->SetText(ConstructValueAsText());
		return;
	}

	EditorInterface* foundObj = editorEngine->FindEditorObject(baseName, editorId, DString::CC_IgnoreCase);
	SetEditedObject(foundObj);
	ObjNameField->SetText(ConstructValueAsText()); //Corrects any casing errors
}

void EditableObjectComponent::RefreshHeaderText ()
{
	DString newHeader = ConstructValueAsText();
	if (ObjNameField != nullptr)
	{
		ObjNameField->SetText(newHeader);
	}

	if (ExpandableComponent != nullptr)
	{
		ExpandableComponent->SetStaticHeaderText(newHeader);
	}
}

void EditableObjectComponent::RefreshObjTypeVisibility ()
{
	if (ObjTypeDropdown == nullptr)
	{
		return;
	}

	if (!bCreatesNewObject || EditedObject != nullptr || IsInCopyObjectMode())
	{
		ObjTypeDropdown->SetVisibility(false);
		return;
	}

	size_t numItems = 0;
	if (ListBoxComponent* typeList = ObjTypeDropdown->GetExpandMenuList())
	{
		numItems = typeList->ReadList().size();
	}

	ObjTypeDropdown->SetVisibility(numItems > 1);
}

void EditableObjectComponent::HandleClearButtonReleased (ButtonComponent* button)
{
	SetEditedObject(nullptr);
}

void EditableObjectComponent::HandleCreateButtonReleased (ButtonComponent* button)
{
	if (!OnCreateEditorObject.IsBounded())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("EditableObjectComponent is unable to create a new object since nothing is bound to the OnCreateEditorObject delegate."));
		return;
	}

	EditorInterface* newObj = OnCreateEditorObject(ObjTypeDropdown.Get());
	if (newObj != nullptr && OnInitCreatedObject.IsBounded())
	{
		OnInitCreatedObject(newObj);
	}

	HideCaption();
	SetEditedObject(newObj);
}

void EditableObjectComponent::HandleDuplicateButtonReleased (ButtonComponent* button)
{
	EnterSelectMode();
}

void EditableObjectComponent::HandleDuplicateButtonReleasedRightClick (ButtonComponent* button)
{
	BeginCopyObjectMode();
}

void EditableObjectComponent::HandleSelectObjButtonReleased (ButtonComponent* button)
{
	EnterSelectMode();
}

void EditableObjectComponent::HandleDuplicateObjFieldReturn (TextFieldComponent* uiComp)
{
	CHECK(uiComp != nullptr)
	DString objName = uiComp->GetContent();
	if (CopyObjectByName(objName))
	{
		EndCopyObjectMode();
	}
}

void EditableObjectComponent::HandleObjNameFieldReturn (TextFieldComponent* uiComp)
{
	ProcessObjNameChange();
}

void EditableObjectComponent::HandleToggleExpandComp (bool isCollapsed)
{
	if (!isCollapsed && ExpandableComponent != nullptr && ContainerUtils::IsEmpty(ExpandableComponent->ReadSubPropertyComponents()))
	{
		PopulateObjectProperties();
	}
}

void EditableObjectComponent::HandleEditedObjectChangeId ()
{
	RefreshHeaderText();
}

void EditableObjectComponent::HandleEditedObjectChangeName ()
{
	RefreshHeaderText();
}

void EditableObjectComponent::HandleEditedObjectDestroyed (EditorInterface* obj)
{
	//Don't call SetEditedObject since this component may still think it needs to destroy that object. It's possible that something else may have deleted the object (eg: functions owning variables while this component also owns those same variables).
	//Instead just clear the references.
	if (EditedObject != nullptr)
	{
		EditedObject->OnEditorIdChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeId, void));
		EditedObject->OnEditorNameChanged.UnregisterHandler(SDFUNCTION(this, EditableObjectComponent, HandleEditedObjectChangeName, void));
		EditedObject->OnClearEditorReference.TryUnregisterHandler(SDFUNCTION_1PARAM(this, EditableObjectComponent, HandleEditedObjectDestroyed, void, EditorInterface*));
		EditedObject = nullptr;
	}
}

EditorInterface* EditableObjectComponent::HandleCreateObject (DropdownComponent* objTypeDropdown)
{
	const DClass* selectedClass = BaseClass;
	if (objTypeDropdown->IsItemSelected())
	{
		selectedClass = objTypeDropdown->GetSelectedItem<const DClass*>();
	}

	if (selectedClass != nullptr && !selectedClass->IsAbstract())
	{
		if (const Object* cdo = selectedClass->GetDefaultObject())
		{
			CHECK(dynamic_cast<const EditorInterface*>(cdo) != nullptr)
			Object* createdObj = cdo->CreateObjectOfMatchingClass();
			EditorInterface* result = dynamic_cast<EditorInterface*>(createdObj);
			CHECK(result != nullptr)
			return result;
		}
	}

	return nullptr;
}

EditorInterface* EditableObjectComponent::HandleCreateGraphAsset (DropdownComponent* objTypeDropdown)
{
	GraphAsset* selectedAsset = BaseAsset.Get();
	if (objTypeDropdown->IsItemSelected())
	{
		selectedAsset = objTypeDropdown->GetSelectedItem<GraphAsset*>();
	}

	if (selectedAsset != nullptr)
	{
		GraphAsset* assetInstance = GraphAsset::CreateObject();
		assetInstance->InitializeAsset(selectedAsset->ReadAssetName(), false);
		return assetInstance;
	}

	return nullptr;
}

bool EditableObjectComponent::HandleIsObjectRelevant (EditorInterface* obj)
{
	if (BaseClass == nullptr)
	{
		return false;
	}

	if (Object* duneObject = dynamic_cast<Object*>(obj))
	{
		if (const DClass* duneClass = duneObject->StaticClass())
		{
			return duneClass->IsChildOf(BaseClass);
		}
	}

	return false;
}

bool EditableObjectComponent::HandleIsGraphAssetRelevant (EditorInterface* obj)
{
	if (BaseAsset == nullptr)
	{
		return false;
	}

	if (GraphAsset* asset = dynamic_cast<GraphAsset*>(obj))
	{
		if (asset == BaseAsset || asset->IsChildOf(BaseAsset.Get()))
		{
			return true;
		}
	}

	return false;
}

bool EditableObjectComponent::HandleIsScreenCapRelevant (RenderComponent* renderComp)
{
	CHECK(renderComp != nullptr)
	EditorInterface* editorObj = dynamic_cast<EditorInterface*>(renderComp->GetRootEntity());
	if (editorObj == nullptr)
	{
		return false;
	}

	if (AssociatedVariable != nullptr && !AssociatedVariable->CanBeAssignedTo(editorObj))
	{
		return false;
	}

	if (OnIsEditorObjectRelevant.IsBounded() && !OnIsEditorObjectRelevant(editorObj))
	{
		return false;
	}

	return true;
}

bool EditableObjectComponent::HandleSelectModeInput (const sf::Event& sfEvent)
{
	if (sfEvent.type == sf::Event::KeyReleased && sfEvent.key.code == sf::Keyboard::Escape)
	{
		ExitSelectMode();
	}

	return true;
}

bool EditableObjectComponent::HandleSelectModeText (const sf::Event& sfEvent)
{
	return true;
}

bool EditableObjectComponent::HandleSelectModeMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType type)
{
	if (mouseEvent.button == sf::Mouse::Button::Left && type == sf::Event::MouseButtonReleased)
	{
		//Attempt to select the object the user clicked on.
		if (SelectModeScreenCap != nullptr)
		{
			RenderTarget* outerViewport = FindSelectModeRenderTarget();
			Vector2 pixelOffset = outerViewport->GetWindowCoordinates();

			Int pixelX = (mouse->ToWindowCoordinates().X - pixelOffset.X).ToInt();
			Int pixelY = (mouse->ToWindowCoordinates().Y - pixelOffset.Y).ToInt();

			Int width;
			Int height;
			outerViewport->GetSize(OUT width, OUT height);
			if (pixelX >= 0 && pixelX < width && pixelY >= 0 && pixelY < height)
			{
				if (RenderComponent* rendComp = SelectModeScreenCap->GetMappedPixel(pixelX, pixelY))
				{
					if (EditorInterface* editorObj = dynamic_cast<EditorInterface*>(rendComp->GetRootEntity()))
					{
						if (bCreatesNewObject)
						{
							//The duplicate object button was clicked on instead.
							EditorInterface* newObj = dynamic_cast<EditorInterface*>(editorObj->Duplicate());
							CHECK(newObj != nullptr)

							if (OnInitCreatedObject.IsBounded())
							{
								OnInitCreatedObject(newObj);
							}

							SetEditedObject(newObj);
						}
						else
						{
							SetEditedObject(editorObj);
						}
					}
				}
			}
		}

		ExitSelectMode();
	}

	return true;
}

bool EditableObjectComponent::HandleSelectModeMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& scrollEvent)
{
	return true;
}

bool EditableObjectComponent::HandleSelectModeMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt)
{
	return IsInSelectMode();
}

void EditableObjectComponent::HandleUpdateReferenceTick (Float deltaSec)
{
	if (AssociatedVariable != nullptr && AssociatedVariable->GetEditorInterface() != EditedObject)
	{
		SetEditedObject(AssociatedVariable->GetEditorInterface());
	}
}
SD_END