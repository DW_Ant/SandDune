/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReroutePort.cpp
=====================================================================
*/

#include "ReroutePort.h"

IMPLEMENT_CLASS(SD::ReroutePort, SD::NodePort)
SD_BEGIN

void ReroutePort::InitProps ()
{
	Super::InitProps();

	EmptyColor = Color(194, 194, 194);

	TextureColor = EmptyColor;
	PortType = PT_Any;
}

void ReroutePort::EstablishConnectionTo (NodePort* otherPort)
{
	//Check if all nodes are compatible with each other. If not, then sever all connections
	std::vector<NodePort*> allConnections;
	std::vector<NodePort*> checkedPorts;
	GetConnectedPortsOfMatchingType(OUT allConnections, PT_Input, OUT checkedPorts);

	ContainerUtils::Empty(OUT checkedPorts);
	GetConnectedPortsOfMatchingType(OUT allConnections, PT_Output, OUT checkedPorts);

	bool bSeverOldConnections = false;
	for (NodePort* connection : allConnections)
	{
		if (!connection->CanBeGroupedWith(otherPort))
		{
			bSeverOldConnections = true;
			break;
		}
	}

	//There's an incompatible connection. Detach all old connections before connecting the new port.
	if (bSeverOldConnections)
	{
		SeverAllConnections();
	}

	Super::EstablishConnectionTo(otherPort);
}

void ReroutePort::SeverConnectionFrom (NodePort* otherPort)
{
	Super::SeverConnectionFrom(otherPort);

	if (ContainerUtils::IsEmpty(Connections))
	{
		//Restore the colors back to default color
		SetTextureColor(EmptyColor);
	}
}
SD_END