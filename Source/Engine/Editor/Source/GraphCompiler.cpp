/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphCompiler.cpp
=====================================================================
*/

#include "GraphCompiler.h"
#include "GraphFunction.h"
#include "GraphVariable.h"

IMPLEMENT_CLASS(SD::GraphCompiler, SD::Object)
SD_BEGIN

GraphCompiler::ECompileResults GraphCompiler::CheckVariableName (const GraphVariable& var, DString& outErrorMsg, const std::vector< std::vector<GraphVariable*>* >& otherVars)
{
	//Ensure the variable has a valid name
	if (var.ReadVariableName().GetHash() == 0)
	{
		outErrorMsg = TXT("All variables are required to have a name.");
		return CR_Error;
	}

	DString varName = var.ReadVariableName().GetString();
	if (ContainerUtils::FindInVector(ClmEngineComponent::RESERVED_WORDS, varName) != UINT_INDEX_NONE)
	{
		outErrorMsg = DString::CreateFormattedString(TXT("The variable name \"%s\" is a reserved word. Cannot give a variable that name."), varName);
		return CR_Error;
	}

	//Check for invalid symbols since that may interfere with inlined parsing.
	std::vector<DString> invalidChars
	{
		' ',
		'\'',
		'\"'
	};

	for (const DString& invChar : invalidChars)
	{
		if (varName.Contains(invChar, DString::CC_CaseSensitive))
		{
			outErrorMsg = DString::CreateFormattedString(TXT("The variable name \"%s\" contains an invalid character (%s)."), varName, invChar);
			return CR_Error;
		}
	}

	Int counter = 0;
	for (std::vector<GraphVariable*>* curList : otherVars)
	{
		for (GraphVariable* curVar : *curList)
		{
			if (curVar != nullptr && curVar->ReadVariableName() == var.ReadVariableName())
			{
				++counter;
				if (counter >= 2)
				{
					outErrorMsg = DString::CreateFormattedString(TXT("The variable name \"%s\" is already in use."), varName);
					return CR_Error;
				}
			}
		}
	}

	return CR_Success;
}

GraphCompiler::ECompileResults GraphCompiler::CheckVariableType (const GraphVariable& var, DString& outErrorMsg)
{
	if (var.GetVarType() == ScriptVariable::VT_Unknown)
	{
		DString varName = var.ReadVariableName().GetString();
		outErrorMsg = DString::CreateFormattedString(TXT("The variable \"%s\" is an unknown type."), varName);
		return CR_Error;
	}

	return CR_Success;
}

GraphCompiler::ECompileResults GraphCompiler::CheckVariableDefault (const GraphVariable& var, DString& outErrorMsg)
{
	//Ensure the variable's default value is readable.
	if (!var.ReadDefaultValue().IsEmpty())
	{
		DataBuffer defaultCpy;
		var.ReadDefaultValue().CopyBufferTo(OUT defaultCpy);
		defaultCpy.JumpToBeginning();

		//Try to read from it.
		switch(var.GetVarType())
		{
			case(ScriptVariable::VT_Bool):
			{
				Bool defaultBool;
				defaultCpy >> defaultBool;
				break;
			}

			case(ScriptVariable::VT_Int):
			{
				Int defaultInt;
				defaultCpy >> defaultInt;
				break;
			}

			case(ScriptVariable::VT_Float):
			{
				Float defaultFloat;
				defaultCpy >> defaultFloat;
				break;
			}

			case(ScriptVariable::VT_String):
			{
				DString defaultStr;
				defaultCpy >> defaultStr;
				break;
			}
		}

		if (defaultCpy.HasReadError())
		{
			DString varName = var.ReadVariableName().GetString();
			outErrorMsg = DString::CreateFormattedString(TXT("Failed to read the default value from %s."), varName);
			return CR_Error;
		}
	}

	return CR_Success;
}

GraphCompiler::ECompileResults GraphCompiler::CheckFunction (const GraphFunction& func, DString& outErrorMsg, const std::vector< std::vector<GraphFunction*>* >& otherFunctions)
{
	ECompileResults result = CheckFunctionName(func, OUT outErrorMsg, otherFunctions);
	if (result == CR_Error)
	{
		return result;
	}

	//Functions should not override final functions.
	if (func.GetSuperFunction() != nullptr && func.ReadRootFunction().IsFinal())
	{
		outErrorMsg = DString::CreateFormattedString(TXT("%s is overriding a final function. The subclass's implementation will be ignored."), func.ReadFunctionName());
		result = CR_Warning;
		//Fallthrough is intended. It's only a warning message.
	}

	return result;
}

GraphCompiler::ECompileResults GraphCompiler::CheckFunctionName (const GraphFunction& func, DString& outErrorMsg, const std::vector< std::vector<GraphFunction*>* >& otherFunctions)
{
	//Ensure the variable has a valid name
	if (func.ReadFunctionName().IsEmpty())
	{
		outErrorMsg = TXT("All functions are required to have a name.");
		return CR_Error;
	}

	if (ContainerUtils::FindInVector(ClmEngineComponent::RESERVED_WORDS, func.ReadFunctionName()) != UINT_INDEX_NONE)
	{
		outErrorMsg = DString::CreateFormattedString(TXT("The function name \"%s\" is a reserved word. Cannot give a function that name."), func.ReadFunctionName());
		return CR_Error;
	}

	//Check for invalid symbols since that may interfere with inlined parsing.
	std::vector<DString> invalidChars
	{
		' ',
		'\'',
		'\"'
	};

	for (const DString& invChar : invalidChars)
	{
		if (func.ReadFunctionName().Contains(invChar, DString::CC_CaseSensitive))
		{
			outErrorMsg = DString::CreateFormattedString(TXT("The function name \"%s\" contains an invalid character (%s)."), func.ReadFunctionName(), invChar);
			return CR_Error;
		}
	}

	Int counter = 0;
	for (std::vector<GraphFunction*>* curList : otherFunctions)
	{
		for (const GraphFunction* curFunc : *curList)
		{
			if (curFunc != nullptr && curFunc->ReadFunctionName().Compare(func.ReadFunctionName(), DString::CC_CaseSensitive) == 0)
			{
				++counter;
				if (counter >= 2)
				{
					outErrorMsg = DString::CreateFormattedString(TXT("The function name \"%s\" is already in use."), func.ReadFunctionName());
					return CR_Error;
				}
			}
		}
	}

	return CR_Success;
}
SD_END