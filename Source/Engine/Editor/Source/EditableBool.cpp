/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableBool.cpp
=====================================================================
*/

#include "EditableBool.h"
#include "EditorEngineComponent.h"
#include "EditorTheme.h"

IMPLEMENT_CLASS(SD::EditableBool, SD::EditPropertyComponent)
SD_BEGIN

EditableBool::SInspectorBool::SInspectorBool (const DString& inPropertyName, const DString& inTooltipText, bool* inAssociatedVariable, bool inDefaultValue) : SInspectorProperty(inPropertyName, inTooltipText),
	AssociatedVariable(inAssociatedVariable),
	DefaultValue(inDefaultValue),
	InitOverride(inDefaultValue)
{
	//Noop
}

void EditableBool::SInspectorBool::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString propText = config->GetPropertyText(sectionName, PropertyName);
	if (propText.IsEmpty())
	{
		*AssociatedVariable = DefaultValue;
	}
	else
	{
		Bool configBool;
		configBool.ParseString(propText);
		*AssociatedVariable = configBool.Value;
	}
}

void EditableBool::SInspectorBool::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to config since there isn't a variable associated with it."), PropertyName);
		return;
	}

	config->SaveProperty<Bool>(sectionName, PropertyName, Bool(*AssociatedVariable));
}

bool EditableBool::SInspectorBool::LoadBinary (const DataBuffer& incomingData)
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorBool is not associated with a variable."), PropertyName);
		return false;
	}

	if (!incomingData.CanReadBytes(Bool::SGetMinBytes()))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary. Not enough bytes in the data buffer."), PropertyName);
		return false;
	}

	Bool loadedData;
	if ((incomingData >> loadedData).HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load %s from binary. The data is malformed, preventing it from reading a Bool."), PropertyName);
		return false;
	}

	*AssociatedVariable = loadedData;
	return true;
}

void EditableBool::SInspectorBool::SaveBinary (DataBuffer& outData) const
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to binary since it's not associated with any variable."), PropertyName);
		return;
	}

	Bool savedData(*AssociatedVariable);
	outData << savedData;
}

EditPropertyComponent* EditableBool::SInspectorBool::CreateBlankEditableComponent () const
{
	return EditableBool::CreateObject();
}

void EditableBool::SInspectorBool::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableBool* boolProp = dynamic_cast<EditableBool*>(AssociatedComp))
	{
		boolProp->BindVariable(AssociatedVariable);
		boolProp->SetDefaultValue(DefaultValue);
		if (boolProp->GetCheckbox() != nullptr && boolProp->GetCheckbox()->GetCaptionComponent() != nullptr)
		{
			boolProp->GetCheckbox()->GetCaptionComponent()->SetText(PropertyName);
		}

		if (AssociatedVariable == nullptr)
		{
			if (CheckboxComponent* checkbox = boolProp->GetCheckbox())
			{
				if (InitType == IIV_UseOverride)
				{
					checkbox->SetChecked(InitOverride);
				}
				else
				{
					checkbox->SetChecked(DefaultValue);
				}
			}
		}
	}
}

void EditableBool::InitProps ()
{
	Super::InitProps();

	bInitPropName = false; //The checkbox component will handle the name, and there's no barrier.

	EditedBool = nullptr;
	DefaultBool = false;
	UpdateBoolTick = nullptr;
	ResetDefaultsButton = nullptr;
}

void EditableBool::BeginObject ()
{
	Super::BeginObject();

	UpdateBoolTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateBoolTick))
	{
		UpdateBoolTick->SetTickInterval(0.2f); //Not important to refresh continuously.
		UpdateBoolTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableBool, HandleUpdateBoolTick, void, Float));
	}
}

void EditableBool::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableBool* boolTemplate = dynamic_cast<const EditableBool*>(objTemplate))
	{
		bool bCreatedObj;
		Checkbox = ReplaceTargetWithObjOfMatchingClass(Checkbox.Get(), boolTemplate->GetCheckbox(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Checkbox);
		}

		if (Checkbox.IsValid())
		{
			Checkbox->CopyPropertiesFrom(boolTemplate->GetCheckbox());
		}
	}
}

ButtonComponent* EditableBool::AddFieldControl ()
{
	ButtonComponent* result = Super::AddFieldControl();
	if (result != nullptr && Checkbox.IsValid())
	{
		Checkbox->SetAnchorRight(Float(FieldControls.size()) * FieldButtonSize.X);
		Checkbox->SetAnchorLeft(0.f);
	}

	return result;
}

void EditableBool::CopyToBuffer (DataBuffer& copyTo) const
{
	Super::CopyToBuffer(OUT copyTo);

	if (Checkbox != nullptr)
	{
		Bool isChecked = Checkbox->IsChecked();
		copyTo << isChecked;
	}
}

bool EditableBool::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	Bool dBool;
	if ((incomingData >> dBool).HasReadError())
	{
		return false;
	}

	if (Checkbox != nullptr)
	{
		Checkbox->SetChecked(dBool);
	}

	if (EditedBool != nullptr)
	{
		(*EditedBool) = dBool;
	}

	return true;
}

void EditableBool::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	LocalBool = (Checkbox != nullptr && Checkbox->IsChecked());
	EditedBool = &LocalBool;
}

EditPropertyComponent::SInspectorProperty* EditableBool::CreateDefaultInspector (const DString& propName) const
{
	return new SInspectorBool(propName, DString::EmptyString, nullptr);
}

void EditableBool::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateBoolTick != nullptr)
	{
		UpdateBoolTick->Destroy();
		UpdateBoolTick = nullptr;
	}
}

void EditableBool::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (EditedBool != nullptr)
	{
		*EditedBool = DefaultBool;
	}

	if (Checkbox != nullptr && Checkbox->IsChecked() != DefaultBool)
	{
		MarkPropertyTextDirty();
		Checkbox->SetChecked(DefaultBool);
	}

	ProcessApplyEdit(this);
}

void EditableBool::UnbindVariable ()
{
	Super::UnbindVariable();

	BindVariable(nullptr);
}

void EditableBool::SetNormalFontColor (Color newNormalFontColor)
{
	Super::SetNormalFontColor(newNormalFontColor);

	if (!IsReadOnly() && Checkbox.IsValid() && Checkbox->GetCaptionComponent() != nullptr && Checkbox->GetCaptionComponent()->GetRenderComponent() != nullptr)
	{
		Checkbox->GetCaptionComponent()->GetRenderComponent()->SetFontColor(NormalFontColor.Source);
	}
}

void EditableBool::SetReadOnlyFontColor (Color newReadOnlyFontColor)
{
	Super::SetReadOnlyFontColor(newReadOnlyFontColor);

	if (IsReadOnly() && Checkbox.IsValid() && Checkbox->GetCaptionComponent() != nullptr && Checkbox->GetCaptionComponent()->GetRenderComponent() != nullptr)
	{
		Checkbox->GetCaptionComponent()->GetRenderComponent()->SetFontColor(ReadOnlyFontColor.Source);
	}
}

void EditableBool::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	if (Checkbox.IsValid())
	{
		Checkbox->SetEnabled(!IsReadOnly());

		if (Checkbox->GetCaptionComponent() != nullptr && Checkbox->GetCaptionComponent()->GetRenderComponent() != nullptr)
		{
			Checkbox->GetCaptionComponent()->GetRenderComponent()->SetFontColor((IsReadOnly()) ? ReadOnlyFontColor.Source : NormalFontColor.Source);
		}
	}

	if (ResetDefaultsButton.IsValid())
	{
		ResetDefaultsButton->SetEnabled(!IsReadOnly());
	}
}

void EditableBool::InitializeComponents ()
{
	Super::InitializeComponents();

	Checkbox = CheckboxComponent::CreateObject();
	if (AddComponent(Checkbox))
	{
		Checkbox->SetPosition(Vector2::ZERO_VECTOR);
		Checkbox->SetSize(Vector2(1.f, 1.f));
		Checkbox->SetCheckboxRightSide(true);
		Checkbox->OnChecked = SDFUNCTION_1PARAM(this, EditableBool, HandleCheckboxChecked, void, CheckboxComponent*);

		if (Checkbox->GetCaptionComponent() != nullptr && Checkbox->GetCaptionComponent()->GetRenderComponent() != nullptr)
		{
			Checkbox->GetCaptionComponent()->GetRenderComponent()->SetFontColor((IsReadOnly()) ? ReadOnlyFontColor.Source : NormalFontColor.Source);
			PropertyName = Checkbox->GetCaptionComponent(); //These are synonymous since the checkbox component already has a label component for the property name.
		}
	}
}

DString EditableBool::ConstructValueAsText () const
{
	if (Checkbox == nullptr)
	{
		return DString::EmptyString;
	}

	//TODO: localize - The editor already depends on Localization
	return Bool(Checkbox->IsChecked()).ToString();
}

void EditableBool::BindVariable (bool* newEditedBool)
{
	EditedBool = newEditedBool;
	MarkPropertyTextDirty();

	if (Checkbox.IsValid() && EditedBool != nullptr)
	{
		Checkbox->SetChecked(*EditedBool);
	}
}

void EditableBool::SetBoolValue (bool newBool, bool bInvokeCallback)
{
	if (Checkbox != nullptr && Checkbox->IsChecked() != newBool)
	{
		Checkbox->SetChecked(newBool); //Calling SetChecked will not invoke OnChecked callback.
		ProcessCheckboxStateChange();

		if (bInvokeCallback)
		{
			ProcessApplyEdit(this);
		}
	}
}

void EditableBool::SetDefaultValue (bool newDefaultBool)
{
	DefaultBool = newDefaultBool;

	if (ResetDefaultsButton.IsNullptr())
	{
		ResetDefaultsButton = AddFieldControl();
		if (ResetDefaultsButton.IsValid())
		{
			ResetDefaultsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableBool, HandleResetToDefaultsReleased, void, ButtonComponent*));
			ResetDefaultsButton->SetEnabled(!IsReadOnly());

			EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());
			if (localTheme != nullptr && ResetDefaultsButton->GetBackground() != nullptr)
			{
				ResetDefaultsButton->GetBackground()->SetCenterTexture(localTheme->FieldControlReset);
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (ResetDefaultsButton->AddComponent(tooltip))
			{
				TextTranslator* translator = TextTranslator::GetTranslator();
				CHECK(translator != nullptr)

				tooltip->SetTooltipText(translator->TranslateText(TXT("ResetDefaults"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
			}
		}
	}
}

void EditableBool::ProcessCheckboxStateChange ()
{
	if (EditedBool != nullptr && Checkbox != nullptr)
	{
		*EditedBool = Checkbox->IsChecked();
	}

	MarkPropertyTextDirty();
}

void EditableBool::HandleUpdateBoolTick (Float deltaSec)
{
	if (EditedBool != nullptr && Checkbox != nullptr && Checkbox->IsChecked() != *EditedBool)
	{
		MarkPropertyTextDirty();
		Checkbox->SetChecked(*EditedBool);
	}
}

void EditableBool::HandleCheckboxChecked (CheckboxComponent* uiComp)
{
	ProcessCheckboxStateChange();
	ProcessApplyEdit(this);
}

void EditableBool::HandleResetToDefaultsReleased (ButtonComponent* uiComp)
{
	ResetToDefaults();
}
SD_END