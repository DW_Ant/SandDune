/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphEditor.cpp
=====================================================================
*/

#include "EditableArray.h"
#include "EditableDString.h"
#include "EditableEnum.h"
#include "EditableInt.h"
#include "GraphCompiler.h"
#include "GraphContextMenu.h"
#include "GraphEditor.h"
#include "GraphInterface.h"
#include "GraphVariable.h"
#include "GraphViewport.h"
#include "NodePort.h"

IMPLEMENT_CLASS(SD::GraphEditor, SD::GuiEntity)
SD_BEGIN

void GraphEditor::InitProps ()
{
	Super::InitProps();

	CamPanMode = CPM_DragMouse;
	CamSpeedMultiplier = 1.f;
	CamZoomMultiplier = 0.003f;
	CamZoomWheelMultiplier = 0.2f;
	CurveProperties.Thickness = 8.f;
	CurveProperties.CurveSharpness = 0.5f;
	CurveProperties.CurveSharpnessReverseDirection = 1.5f;
	CurveProperties.SegmentLength = 4.f;
	CurveProperties.SegmentLengthPreview = 8.f;
	CurveProperties.StepAmount = 0.01f;
	CurveProperties.StepAmountPreview = 0.1f;

	DragActionThreshold = 16.f;
	RegenerateCurveThreshold = 2.f;
	ConfigSectionName = TXT("GraphEditor");
	Grid = nullptr;
	ContextComp = nullptr;
	bLMouseDown = false;
	bRMouseDown = false;
	bLockMousePos = false;

	bDragAction = false;
	DragDistance = 0.f;
}

void GraphEditor::BeginObject ()
{
	Super::BeginObject();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const Texture* gridTexture = localTexturePool->GetTexture(HashedString("Engine.Editor.GraphGrid"));
	if (gridTexture != nullptr)
	{
		Grid = SpriteComponent::CreateObject();
		if (AddComponent(Grid))
		{
			Grid->SetDrawMode(SpriteComponent::DM_Tiled);
			Grid->SetSpriteTexture(gridTexture);
		}
	}

	InitializeGraphPreferences();
}

void GraphEditor::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	if (InputCam != nullptr)
	{
		ClampCamPos();
	}
}

void GraphEditor::ConstructUI ()
{
	Super::ConstructUI();

	ContextComp = ContextMenuComponent::CreateObject();
	if (AddComponent(ContextComp))
	{
		ContextComp->SetContextClass(GraphContextMenu::SStaticClass());
		ContextComp->SetRevealMenuButton(sf::Mouse::Right);
		ContextComp->OnInitializeMenu = [&](GuiEntity* menu, ContextMenuComponent* contextComp, MousePointer* mouse)
		{
			if (GraphContextMenu* graphContext = dynamic_cast<GraphContextMenu*>(menu))
			{
				graphContext->OnGraphContextClosing = SDFUNCTION(this, GraphEditor, HandleContextClose, bool);
				graphContext->InitGraphContext(contextComp, mouse, this);
				return true;
			}

			return false;
		};
	}
}

void GraphEditor::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent, const Vector2& deltaMove)
{
	Super::HandleMouseMove(mouse, sfEvent, deltaMove);

	if (!MousePointer::IsEngineMovingMouse())
	{
		if (bDragAction && DragDistance < DragActionThreshold && (bLMouseDown || bRMouseDown))
		{
			DragDistance += deltaMove.VSize();
			if (DragDistance >= DragActionThreshold && DraggedPort == nullptr)
			{
				//Moved far enough away to intercept mouse events to graph objects. Disable MouseEvent and MouseWheel events.
				ApplyComponentInputPermissions(GuiComponent::IE_All & (~(GuiComponent::IE_MouseEvent | GuiComponent::IE_MouseWheel)));
			}
		}

		//Update the dragged port preview
		if (DraggedPort != nullptr && DragPortRender != nullptr)
		{
			//Don't regenerate the curve if the context menu is open in order to 'lock' the curve at the reveal position.
			if (ContextComp == nullptr || !ContextComp->IsRevealed())
			{
				GenerateCurveTo(DraggedPort.Get(), mouse->ReadCachedAbsPosition(), DragPortTransform.Get(), DragPortRender.Get(), true);
			}
		}

		//Check if the components allow camera movement.
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (GraphInterface* graphInter = dynamic_cast<GraphInterface*>(iter.GetSelectedComponent()))
			{
				if (!graphInter->AllowCameraMovement(mouse->ReadPosition()))
				{
					if (bLockMousePos)
					{
						ApplyMouseLock(mouse);
					}
					else
					{
						StartDragPosition += deltaMove; //An object is prohibiting the camera from moving. Need to update the drag position offset since the relative mouse position is changing.
					}

					return;
				}
			}
		}

		if (bLMouseDown && bRMouseDown)
		{
			Float zoomAmount(deltaMove.Y);
			if (bLockMousePos)
			{
				//Reference based on mouse position instead of deltaMove since deltaMove would cancel itself out every other movement frame due to the OS offsets.
				zoomAmount = OS_GetMousePosition().Y - StartDragPosition.Y;
			}

			ProcessCameraZoom(mouse, -zoomAmount * CamZoomMultiplier, true);
		}
		else if (bLMouseDown)
		{
			ProcessCameraMovement(mouse, deltaMove);
		}

		if (bLockMousePos)
		{
			ApplyMouseLock(mouse);
		}
	}
}

void GraphEditor::HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType type)
{
	Super::HandlePassiveMouseClick(mouse, sfEvent, type);

	if (type == sf::Event::MouseButtonReleased)
	{
		if (sfEvent.button == sf::Mouse::Left && bLMouseDown)
		{
			bLMouseDown = false;
			if (CamPanMode == CPM_DragMouse)
			{
				mouse->SetMouseVisibility(true);
			}
		}
		else if (sfEvent.button == sf::Mouse::Right)
		{
			bRMouseDown = false;
		}

		bLockMousePos &= (bLMouseDown || bRMouseDown);
	}
}

bool GraphEditor::HandleConsumableInput (const sf::Event& evnt)
{
	if (Super::HandleConsumableInput(evnt))
	{
		return true;
	}

	if (evnt.type == sf::Event::KeyReleased)
	{
		if (evnt.key.code == sf::Keyboard::Delete)
		{
			DeleteAllSelectedObjects();
			return true;
		}
		else if (evnt.key.code == sf::Keyboard::Escape)
		{
			DeselectAllObjects();
			return true;
		}
	}

	return false;
}

bool GraphEditor::HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::HandleConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (eventType == sf::Event::MouseButtonPressed)
	{
		SetDragAction(true);
	}
	else if (!bLMouseDown && !bRMouseDown)
	{
		SetDragAction(false);
	}

	if (sfmlEvent.button == sf::Mouse::Left)
	{
		//Relinquish reference to the DraggedPort here instead of nonconsumable mouse click since the NodePorts may need to reference the DraggedPort BEFORE the editor should lose reference to it.
		if (eventType == sf::Event::MouseButtonReleased && DraggedPort != nullptr && sfmlEvent.button == sf::Mouse::Left)
		{
			bool isOverComp = false;

			//Identify if the mouse is over any other port.
			for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				if (PlanarTransform* transform = dynamic_cast<PlanarTransform*>(iter.GetSelectedComponent()))
				{
					if (!transform->IsWithinBounds(mouse->ReadPosition()))
					{
						iter.SkipSubComponents();
						continue;
					}

					if (!isOverComp && dynamic_cast<GraphInterface*>(iter.GetSelectedComponent()))
					{
						isOverComp = true;
					}
					
					if (NodePort* otherPort = dynamic_cast<NodePort*>(iter.GetSelectedComponent()))
					{
						if (otherPort->CanAcceptConnectionsFrom(DraggedPort.Get(), true))
						{
							otherPort->EstablishConnectionTo(DraggedPort.Get());
						}

						break;
					}
				}
			}

			if (!isOverComp && ContextComp != nullptr)
			{
				//If the context menu becomes visible, don't clear the dragged port yet since that is used to determine which commands are available. Wait until the context closes before clearing the dragged port.
				ContextComp->RevealContextMenu(mouse);
			}
			else
			{
				SetDraggedPort(nullptr);
			}
		}

		if (!bLMouseDown && eventType == sf::Event::MouseButtonPressed)
		{
			//Enter drag camera mode
			bLMouseDown = true;
			StartDragPosition = OS_GetMousePosition();

			if (CamPanMode == CPM_DragMouse)
			{
				bLockMousePos = true;

				//For normal drag mode, the mouse cursor will be invisible while dragging.
				mouse->SetMouseVisibility(false);
			}
			else
			{
				TexturePool* localTexturePool = TexturePool::FindTexturePool();
				CHECK(localTexturePool != nullptr)
				if (const Texture* grabTexture = localTexturePool->GetTexture(HashedString("Engine.Input.CursorGrab")))
				{
					IconOverrideMouse = mouse;
					mouse->PushMouseIconOverride(grabTexture, SDFUNCTION_2PARAM(this, GraphEditor, HandleGrabOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
				}
			}
		}

		return true;
	}
	else if (sfmlEvent.button == sf::Mouse::Right && eventType == sf::Event::MouseButtonPressed)
	{
		bRMouseDown = true;
		bLockMousePos = (bLMouseDown && CamPanMode == CPM_DragMouse); //If both mouse buttons are held down in DragMouse mode, lock the mouse.
	}

	return false;
}

bool GraphEditor::HandleConsumableMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::HandleConsumableMouseWheel(mouse, sfmlEvent))
	{
		return true;
	}

	ProcessCameraZoom(mouse, sfmlEvent.delta * CamZoomWheelMultiplier, false);

	return true;
}

void GraphEditor::Destroyed ()
{
	if (IconOverrideMouse != nullptr)
	{
		IconOverrideMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, GraphEditor, HandleGrabOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		IconOverrideMouse = nullptr;
	}

	Super::Destroyed();
}

void GraphEditor::CreateCurveConnecting (NodePort* portA, NodePort* portB)
{
	if (portA == nullptr || portB == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("The GraphEditor cannot create a curve between nullptr port(s)."));
		return;
	}

	//Check if there is already a connection between the ports.
	for (const SCurveConnection& connection : CurveConnections)
	{
		Int numMatches = 0;
		if (connection.OwningPort == portA || connection.OtherPort == portA)
		{
			++numMatches;
		}

		if (connection.OwningPort == portB || connection.OtherPort == portB)
		{
			++numMatches;
		}

		if (numMatches >= 2)
		{
			if (connection.OwningPort == portA)
			{
				//This curve already exists. Do nothing.
				return;
			}

			//The curve already exists, but the curve owner is different. Remove this connection, and rebuild this connection.
			RemoveCurveConnecting(portA, portB);
			break;
		}
	}

	SCurveConnection& newConnection = CurveConnections.emplace_back();
	newConnection.OwningPort = portA;
	newConnection.OtherPort = portB;
	newConnection.CurveTransform = PlanarTransformComponent::CreateObject();
	if (portA->AddComponent(newConnection.CurveTransform))
	{
		newConnection.CurveTransform->SetPosition(portA->ReadSize() * 0.5f);
		newConnection.CurveTransform->SetEnableFractionScaling(false);
		newConnection.CurveTransform->ComputeAbsTransform(); //AbsTransform is needed for the generated curve

		newConnection.CurveRender = CurveRenderComponent::CreateObject();
		if (newConnection.CurveTransform->AddComponent(newConnection.CurveRender))
		{
			newConnection.CurveRender->SetCurveColor(portA->GetTextureColor().Source);

			Vector2 otherEndPt = portB->ReadCachedAbsPosition() + (portB->ReadCachedAbsSize() * 0.5f);
			GenerateCurveTo(portA, otherEndPt, newConnection.CurveTransform, newConnection.CurveRender, false);
			newConnection.RelativeDistance = portB->ReadCachedAbsPosition() - portA->ReadCachedAbsPosition();

			if (!portA->OnRefreshCurveConnections.IsBounded())
			{
				portA->OnRefreshCurveConnections = SDFUNCTION_1PARAM(this, GraphEditor, HandlePortRefreshCurve, void, NodePort*);
			}

			if (!portB->OnRefreshCurveConnections.IsBounded())
			{
				portB->OnRefreshCurveConnections = SDFUNCTION_1PARAM(this, GraphEditor, HandlePortRefreshCurve, void, NodePort*);
			}
		}
	}
}

void GraphEditor::RemoveCurveConnecting (NodePort* portA, NodePort* portB)
{
	for (size_t i = 0; i < CurveConnections.size(); ++i)
	{
		Int numMatches = 0;
		if (CurveConnections.at(i).OwningPort == portA || CurveConnections.at(i).OtherPort == portA)
		{
			++numMatches;
		}

		if (CurveConnections.at(i).OwningPort == portB || CurveConnections.at(i).OtherPort == portB)
		{
			++numMatches;
		}

		if (numMatches >= 2)
		{
			if (CurveConnections.at(i).CurveTransform != nullptr)
			{
				CurveConnections.at(i).CurveTransform->Destroy(); //Also destroys the CurveRenderComponent
			}

			CurveConnections.erase(CurveConnections.begin() + i);
			break;
		}
	}

	//Check if the ports need to unbind their delegates
	bool removeA = true;
	bool removeB = true;
	for (const SCurveConnection& connection : CurveConnections)
	{
		if (connection.OwningPort == portA)
		{
			removeA = false; //portA is still in use
		}

		if (connection.OwningPort == portB)
		{
			removeB = false; //portB is still in use
		}

		if (!removeA && !removeB)
		{
			break;
		}
	}

	if (removeA)
	{
		portA->OnRefreshCurveConnections.ClearFunction();
	}

	if (removeB)
	{
		portB->OnRefreshCurveConnections.ClearFunction();
	}
}

void GraphEditor::SetOwningFunction (GraphFunction* newOwningFunction)
{
	OwningFunction = newOwningFunction;
}

void GraphEditor::SetDragActionThreshold (Float newDragActionThreshold)
{
	DragActionThreshold = newDragActionThreshold;
}

void GraphEditor::SetRegenerateCurveThreshold (Float newRegenerateCurveThreshold)
{
	RegenerateCurveThreshold = newRegenerateCurveThreshold;
}

void GraphEditor::SetInputCam (PlanarCamera* newInputCam)
{
	InputCam = newInputCam;
}

void GraphEditor::SetDraggedPort (NodePort* newDraggedPort)
{
	if (DraggedPort == newDraggedPort)
	{
		return;
	}

	DraggedPort = newDraggedPort;

	if (DraggedPort != nullptr)
	{
		//When dragging a port around, the editor should intercept all mouse events, too, to prevent things like:
		//Selecting a node when releasing a port.
		//Dragging selected nodes while also dragging a port around.
		//Interacting with inlined components while releasing the dragged port.
		ApplyComponentInputPermissions(GuiComponent::IE_Input | GuiComponent::IE_Text);
	}
	else
	{
		ApplyComponentInputPermissions(GuiComponent::IE_All);
	}

	if (DraggedPort == nullptr)
	{
		//Remove the preview components
		if (DragPortTransform != nullptr)
		{
			DragPortTransform->Destroy(); //Also destroys the DragPortRender
			DragPortRender = nullptr;
		}
	}
	else
	{
		if (DragPortTransform == nullptr)
		{
			CHECK(DragPortRender == nullptr) //DragPortRender should only be null if its transform is also null
			DragPortTransform = PlanarTransformComponent::CreateObject();
			if (DraggedPort->AddComponent(DragPortTransform))
			{
				DragPortTransform->SetPosition(DraggedPort->ReadSize() * 0.5f);
				DragPortTransform->SetEnableFractionScaling(false);
				DragPortTransform->ComputeAbsTransform(); //AbsTransform is needed for the generated curve

				DragPortRender = CurveRenderComponent::CreateObject();
				if (DragPortTransform->AddComponent(DragPortRender))
				{
					Color curveColor = DraggedPort->GetTextureColor();
					curveColor.Source.a = 128;
					DragPortRender->SetCurveColor(curveColor.Source);
				}
			}
		}
		else
		{
			DragPortTransform->DetachSelfFromOwner();
			if (DraggedPort->AddComponent(DragPortTransform))
			{
				DragPortTransform->SetPosition(DraggedPort->ReadSize() * 0.5f);
			}
		}
	}
}

void GraphEditor::SetDragAction (bool isDragging)
{
	if (isDragging == bDragAction)
	{
		return;
	}

	bDragAction = isDragging;
	if (bDragAction)
	{
		DragDistance = 0.f; //reset the counter
	}
	else
	{
		//Reenable input to its components
		ApplyComponentInputPermissions(GuiComponent::IE_All);
	}
}

void GraphEditor::ApplyComponentInputPermissions (GuiComponent::EInputEvent newPermissions)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (GuiComponent* comp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()))
		{
			comp->SetAcceptedInputEvents(newPermissions);
		}
	}
}

void GraphEditor::InitializeGraphPreferences ()
{
	ConfigWriter* config = ConfigWriter::CreateObject();
	CHECK(config != nullptr)
	if (config->OpenFile(FileAttributes(Directory::CONFIG_DIRECTORY, TXT("Editor.ini")), false))
	{
		Int camMoveMode = config->GetProperty<Int>(ConfigSectionName, TXT("CamPanMode"));
		CamPanMode = static_cast<ECamPanMode>(camMoveMode.Value);
		CamSpeedMultiplier = config->GetProperty<Float>(ConfigSectionName, TXT("CamSpeedMultiplier"));
		CamZoomMultiplier = config->GetProperty<Float>(ConfigSectionName, TXT("CamZoomMultiplier"));
		CamZoomWheelMultiplier = config->GetProperty<Float>(ConfigSectionName, TXT("CamZoomWheelMultiplier"));
	}

	config->Destroy();
}

void GraphEditor::ApplyMouseLock (MousePointer* mouse)
{
	Vector2 prevMousePos(OS_GetMousePosition());
	CHECK(mouse != nullptr)
	mouse->SetAbsMousePosition(StartDragPosition);

	Float camZoom = (InputCam != nullptr) ? InputCam->GetZoom() : 1.f;
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (GraphInterface* graphObj = dynamic_cast<GraphInterface*>(iter.GetSelectedComponent()))
		{
			graphObj->ProcessMouseLock(prevMousePos, StartDragPosition, camZoom);
		}
	}
}

void GraphEditor::ProcessCameraMovement (MousePointer* mouse, const Vector2& deltaMove)
{
	Vector2 camMoveAmount(deltaMove);
	Vector2 multipliers(1.f, 1.f);
	multipliers *= CamSpeedMultiplier;
	if (CamPanMode == CPM_DragMouseInvert)
	{
		multipliers *= -1.f;
	}
	
	if (bLockMousePos)
	{
		//Reference based on mouse position instead of deltaMove since deltaMove would cancel itself out every other movement frame due to the OS offsets.
		camMoveAmount = OS_GetMousePosition() - StartDragPosition;
	}

	if (InputCam != nullptr)
	{
		InputCam->SetPosition(InputCam->ReadPosition() + ((camMoveAmount * multipliers) / InputCam->GetZoom()));
		ClampCamPos();
	}
}

void GraphEditor::GenerateCurveTo (NodePort* startPort, const Vector2& curveEndPt, PlanarTransformComponent* curveTransform, CurveRenderComponent* curveComp, bool isPreviewCurve)
{
	CHECK(curveComp != nullptr && curveTransform != nullptr)

	Vector2 endPt(curveEndPt - curveTransform->ReadCachedAbsPosition());
	if (endPt.IsNearlyEqual(Vector2::ZERO_VECTOR, 0.01f))
	{
		return;
	}

	Vector2 ctrlPt1 = Vector2::ZERO_VECTOR;
	Vector2 ctrlPt2 = endPt;
	bool moveRight = (endPt.X > 0.f); //If true, then ctrlPt1 will be to the right of the starting point.
	bool originalMoveRight = moveRight;
	if (startPort != nullptr)
	{
		if (startPort->GetPortType() == NodePort::PT_Input)
		{
			moveRight = false; //If 'pulling' a curve from an input port, the curve should always start left.
		}
		else if (startPort->GetPortType() == NodePort::PT_Output)
		{
			moveRight = true;
		}
	}

	Float curveSharpness = (originalMoveRight != moveRight) ? CurveProperties.CurveSharpnessReverseDirection : CurveProperties.CurveSharpness;
	Float dist = endPt.VSize() * curveSharpness;

	if (moveRight)
	{
		ctrlPt1.X += dist;
		ctrlPt2.X -= dist;
	}
	else //If moving left
	{
		ctrlPt1.X -= dist;
		ctrlPt2.X += dist;
	}

	Float segLength = (isPreviewCurve) ? CurveProperties.SegmentLengthPreview : CurveProperties.SegmentLength;
	Float stepAmount = (isPreviewCurve) ? CurveProperties.StepAmountPreview : CurveProperties.StepAmount;

	curveComp->GenerateBezierCurve(Vector2::ZERO_VECTOR, endPt, ctrlPt1, ctrlPt2, segLength, CurveProperties.Thickness, stepAmount);
}

void GraphEditor::ClampCamPos ()
{
	CHECK(InputCam != nullptr)
	InputCam->EditPosition().X = Utils::Clamp(InputCam->ReadPosition().X, ReadCachedAbsPosition().X, ReadCachedAbsPosition().X + ReadCachedAbsSize().X);
	InputCam->EditPosition().Y = Utils::Clamp(InputCam->ReadPosition().Y, ReadCachedAbsPosition().Y, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y);
}

void GraphEditor::ProcessCameraZoom (MousePointer* mouse, Float zoomAmount, bool bCenteredZoom)
{
	CHECK(mouse != nullptr)
	if (InputCam != nullptr)
	{
		Vector2 oldTopLeft = InputCam->ReadPosition() - (InputCam->GetZoomedExtents() * 0.5f);
		Vector2 displaceMult = (mouse->ReadPosition() - oldTopLeft) / InputCam->GetZoomedExtents();

		//displaceMult ranges from 0 to 1. Instead it should range from -1 to 1 where -1 is top left corner and 1 is bottom right. If the mouse cursor is centered, the camera should not be displaced (eg: multiplier = 0)
		displaceMult.X = Utils::Lerp<Float>(displaceMult.X, -1.f, 1.f);
		displaceMult.Y = Utils::Lerp<Float>(displaceMult.Y, -1.f, 1.f);

		InputCam->SetZoom(InputCam->GetZoom() + (InputCam->GetZoom() * zoomAmount));

		if (!bCenteredZoom)
		{
			//Displace the camera based on the cursor position. When zooming in, it should move towards the cursor.
			Vector2 newTopLeft = InputCam->ReadPosition() - (InputCam->GetZoomedExtents() * 0.5f);
			Vector2 deltaTopLeft = newTopLeft - oldTopLeft;
			InputCam->EditPosition() += (deltaTopLeft * displaceMult);
			ClampCamPos();
		}
	}
}

void GraphEditor::DeleteAllSelectedObjects ()
{
	std::vector<GraphInterface*> objsToDestroy;

	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (GraphInterface* obj = dynamic_cast<GraphInterface*>(iter.GetSelectedComponent()))
		{
			if (obj->IsSelected() && obj->IsDeletable())
			{
				objsToDestroy.push_back(obj);
			}
		}
	}

	for (size_t i = 0; i < objsToDestroy.size(); ++i)
	{
		objsToDestroy.at(i)->DeleteEditorObject();
	}
}

void GraphEditor::DeselectAllObjects ()
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (GraphInterface* node = dynamic_cast<GraphInterface*>(iter.GetSelectedComponent()))
		{
			if (node->IsSelected())
			{
				node->SetSelected(false);
			}
		}
	}
}

void GraphEditor::HandlePortRefreshCurve (NodePort* changedPort)
{
	CHECK(changedPort != nullptr)

	for (SCurveConnection& connection : CurveConnections)
	{
		if (connection.OwningPort != changedPort && connection.OtherPort != changedPort)
		{
			continue;
		}

		Vector2 delta = connection.OtherPort->ReadCachedAbsPosition() - connection.OwningPort->ReadCachedAbsPosition();
		if ((delta - connection.RelativeDistance).VSize() <= RegenerateCurveThreshold)
		{
			continue;
		}

		connection.RelativeDistance = delta;
		Vector2 endPt = connection.OtherPort->ReadCachedAbsPosition() + (connection.OtherPort->ReadCachedAbsSize() * 0.5f);
		GenerateCurveTo(connection.OwningPort, endPt, connection.CurveTransform, connection.CurveRender, false);
	}
}

bool GraphEditor::HandleGrabOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt)
{
	bool result = bLMouseDown;
	if (!result)
	{
		IconOverrideMouse = nullptr;
	}

	return result;
}

bool GraphEditor::HandleContextClose ()
{
	SetDraggedPort(nullptr);
	return true;
}
SD_END