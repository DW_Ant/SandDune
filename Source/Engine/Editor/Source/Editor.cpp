/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Editor.cpp
=====================================================================
*/

#include "Editor.h"

SD_BEGIN
LogCategory EditorLog(TXT("Editor"), LogCategory::VERBOSITY_DEFAULT, LogCategory::FLAG_ALL);
DString ASSET_TYPE(TXT("AssetType"));
SD_END