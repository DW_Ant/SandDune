/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableArray.cpp
=====================================================================
*/

#include "EditableArray.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "EditorTheme.h"

IMPLEMENT_CLASS(SD::EditableArray, SD::EditPropertyComponent)
SD_BEGIN

EditableArray::SInspectorArray::SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, const DClass* inItemTemplateClass, BaseEditableArrayBinder* inBinder) : SInspectorProperty(inPropertyName, inTooltipText),
	ItemTemplateList(nullptr),
	Binder(inBinder),
	bFixedSize(false),
	MaxItems(-1)
{
	CHECK(inItemTemplateClass != nullptr)

	ItemTemplateClass = dynamic_cast<const EditPropertyComponent*>(inItemTemplateClass->GetDefaultObject());
	if (ItemTemplateClass == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Fatal, TXT("Failed to instantiate EditableArray::SInspectorArray. %s is not an EditPropertyComponent."), inItemTemplateClass->ReadDuneClassName());
		return;
	}

	CHECK_INFO(dynamic_cast<const EditableArray*>(ItemTemplateClass) == nullptr, "Using this constructor for an array of arrays is not supported due to infinite recursion from EditPropertyComponent::CreateDefaultInspector. Instead use the other constructor that requires an InspectorList.")
	CHECK_INFO(dynamic_cast<const EditableStruct*>(ItemTemplateClass) == nullptr, "Using this constructor for an array of structs is not supported since a data struct requires at least one sub property. Instead use the other constructor that requires an InspectorList.")
}

EditableArray::SInspectorArray::SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, const EditPropertyComponent* inItemTemplateClass, BaseEditableArrayBinder* inBinder) : SInspectorProperty(inPropertyName, inTooltipText),
	ItemTemplateClass(inItemTemplateClass),
	ItemTemplateList(nullptr),
	Binder(inBinder),
	bFixedSize(false),
	MaxItems(-1)
{
	CHECK_INFO(dynamic_cast<const EditableArray*>(ItemTemplateClass) == nullptr, "Using this constructor for an array of arrays is not supported due to infinite recursion from EditPropertyComponent::CreateDefaultInspector. Instead use the other constructor that requires an InspectorList.")
	CHECK_INFO(dynamic_cast<const EditableStruct*>(ItemTemplateClass) == nullptr, "Using this constructor for an array of structs is not supported since a data struct requires at least one sub property. Instead use the other constructor that requires an InspectorList.")
}

EditableArray::SInspectorArray::SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, InspectorList* inItemTemplateList, BaseEditableArrayBinder* inBinder) : SInspectorProperty(inPropertyName, inTooltipText),
	ItemTemplateClass(nullptr),
	ItemTemplateList(inItemTemplateList),
	Binder(inBinder),
	bFixedSize(false),
	MaxItems(-1)
{
	CHECK_INFO(ItemTemplateList != nullptr && ItemTemplateList->ReadPropList().size() > 0, "The ItemTemplateList must have at least one property in its list since that is used to determine what GuiComponents are instantiated for each element.")
}

EditableArray::SInspectorArray::~SInspectorArray ()
{
	if (Binder != nullptr)
	{
		delete Binder;
		Binder = nullptr;
	}

	if (ItemTemplateList != nullptr)
	{
		delete ItemTemplateList;
		ItemTemplateList = nullptr;
	}
}

void EditableArray::SInspectorArray::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)
	if (Binder == nullptr || !Binder->IsBounded())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	Binder->LoadConfig(config, sectionName, PropertyName);
}

void EditableArray::SInspectorArray::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)
	if (Binder == nullptr || !Binder->IsBounded())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to config since there isn't a variable associated with it."), PropertyName);
		return;
	}

	Binder->SaveConfig(config, sectionName, PropertyName);
}

bool EditableArray::SInspectorArray::LoadBinary (const DataBuffer& incomingData)
{
	if (Binder == nullptr || !Binder->IsBounded())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorArray is not associated with a variable."), PropertyName);
		return false;
	}

	if (!Binder->LoadBinary(incomingData))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s in binary format."), PropertyName);
		return false;
	}

	return true;
}

void EditableArray::SInspectorArray::SaveBinary (DataBuffer& outData) const
{
	if (Binder == nullptr || !Binder->IsBounded())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to binary since it's not associated with any variable."), PropertyName);
		return;
	}

	Binder->SaveBinary(OUT outData);
}

EditPropertyComponent* EditableArray::SInspectorArray::CreateBlankEditableComponent () const
{
	return EditableArray::CreateObject();
}

void EditableArray::SInspectorArray::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableArray* propArray = dynamic_cast<EditableArray*>(AssociatedComp))
	{
		if (ItemTemplateList != nullptr && !ContainerUtils::IsEmpty(ItemTemplateList->ReadPropList()))
		{
			ItemTemplateList->MoveListTo(&propArray->EditItemTemplate());
			CHECK(ContainerUtils::IsEmpty(ItemTemplateList->ReadPropList())) //Ensure the prop list ownership successfully transferred to the array.
		}
		else if (ItemTemplateClass != nullptr)
		{
			SInspectorProperty* newProp = ItemTemplateClass->CreateDefaultInspector(TXT("Template"));
			CHECK(newProp != nullptr)
			propArray->EditItemTemplate().AddProp(newProp);
		}
		//else this EditableArray will attempt to find the properties from the owning array.

		propArray->Binder = Binder;
		Binder = nullptr; //Transfering Binder ownership over to the Array. The Array is now responsible for deleting the binder when it's done with it.
		propArray->SetFixedSize(bFixedSize);
		propArray->SetMaxItems(MaxItems);

		if (propArray->Binder == nullptr && OnPopulateArray.IsBounded())
		{
			OnPopulateArray(propArray);
		}

		if (EditableArray* arrayProp = dynamic_cast<EditableArray*>(AssociatedComp))
		{
			arrayProp->SetElementHeight(AssociatedComp->GetLineHeight());
			arrayProp->ConstructArray();
			arrayProp->RefreshArraySizeState();
		}
	}
}

void EditableArray::InitProps ()
{
	Super::InitProps();

	FirstSubPropPos = LineHeight;

	ArraySizeLabel = nullptr;
	AddItemButton = nullptr;
	EmptyArrayButton = nullptr;
	Binder = nullptr;
	ElementHeight = 16.f;
	bFixedSize = false;
	MaxItems = -1;
	bExtended = false;
	ElementDragIdx = INDEX_NONE;
	ElementDragDestIdx = INDEX_NONE;
	DragGhostTransform = nullptr;
	DragGhostTexture = nullptr;
	DragDestTransform = nullptr;
	DragDestColorComp = nullptr;

	HoveredDragRegionTransform = nullptr;
}

void EditableArray::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableArray* arrayTemplate = dynamic_cast<const EditableArray*>(objTemplate))
	{
		bool bCreatedObj;
		ArraySizeLabel = ReplaceTargetWithObjOfMatchingClass(ArraySizeLabel, arrayTemplate->ArraySizeLabel, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ArraySizeLabel);
		}

		if (ArraySizeLabel != nullptr)
		{
			ArraySizeLabel->CopyPropertiesFrom(arrayTemplate->ArraySizeLabel);
		}

		AddItemButton = ReplaceTargetWithObjOfMatchingClass(AddItemButton, arrayTemplate->AddItemButton, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(AddItemButton);
		}

		if (AddItemButton != nullptr)
		{
			AddItemButton->CopyPropertiesFrom(arrayTemplate->AddItemButton);
		}

		EmptyArrayButton = ReplaceTargetWithObjOfMatchingClass(EmptyArrayButton, arrayTemplate->EmptyArrayButton, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(EmptyArrayButton);
		}

		if (EmptyArrayButton != nullptr)
		{
			EmptyArrayButton->CopyPropertiesFrom(arrayTemplate->EmptyArrayButton);
		}

		DragDestColorComp = ReplaceTargetWithObjOfMatchingClass(DragDestColorComp, arrayTemplate->DragDestColorComp, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(DragDestColorComp);
		}

		if (DragDestColorComp != nullptr)
		{
			DragDestColorComp->CopyPropertiesFrom(arrayTemplate->DragDestColorComp);
		}

		SetElementHeight(arrayTemplate->ElementHeight);
		SetFixedSize(arrayTemplate->bFixedSize);
		SetMaxItems(arrayTemplate->MaxItems);
	}
}

bool EditableArray::RemoveComponent (EntityComponent* target)
{
	if (!Super::RemoveComponent(target))
	{
		return false;
	}

	//Identify if any of the drag regions are attached to target
	if (dynamic_cast<EditPropertyComponent*>(target) != nullptr)
	{
		for (size_t i = 0; i < ElementDragRegions.size(); ++i)
		{
			if (ElementDragRegions.at(i)->GetOwner() == target)
			{
				if (HoveredDragRegionTransform == ElementDragRegions.at(i))
				{
					HoveredDragRegionTransform = nullptr; //Notify mouse to stop its hover icon
				}

				ElementDragRegions.erase(ElementDragRegions.begin() + i);
				break;
			}
		}
	}

	return true;
}

void EditableArray::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (ElementDragIdx != INDEX_NONE)
	{
		if (DragGhostTransform == nullptr)
		{
			Float deltaY = mouse->ReadCachedAbsPosition().Y - StartDragPos.Y;
			deltaY.AbsInline();
			if (deltaY >= ElementHeight) //If the user dragged the mouse far enough, start the reorder procedure.
			{
				InitDragGhostObjs(mouse);
				MoveComponentToBack(DragDestTransform);
				DragMouseYPosThreshold = Range<Float>(MIN_FLOAT, MIN_FLOAT); //Force the array to calculate the destination on next mouse move.
			}
		}
		else if (!IsCollapsed())
		{
			//Only calculate if the mouse passed the thresholds. Can't simply increment or decrement the drag dest idx since the mouse position may jump (eg: user dragged outside the window, then come back from the top).
			if (!DragMouseYPosThreshold.ContainsValue(mouse->ReadCachedAbsPosition().Y))
			{
				//Identify which element the mouse is hovering over.
				if (IsWithinBounds(mouse->ReadCachedAbsPosition()) && DragDestTransform != nullptr)
				{
					//Default to the last element position
					ElementDragDestIdx = SubPropertyComponents.size();
					Vector2 newPos(0.f, ReadSize().Y);

					CHECK(!ContainerUtils::IsEmpty(SubPropertyComponents)) //Shouldn't be able to drag elements around if it's empty
					EditPropertyComponent* lastComp = ContainerUtils::GetLast(SubPropertyComponents);
					DragMouseYPosThreshold.Min = lastComp->ReadCachedAbsPosition().Y + (lastComp->ReadCachedAbsSize().Y * 0.5);
					DragMouseYPosThreshold.Max = MAX_FLOAT;

					//Can't mathmatically calculate which element is being hovered since element's vertical size may vary (eg: captions or structs). Instead iterate through each element.
					for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
					{
						if (SubPropertyComponents.at(i)->ReadCachedAbsPosition().Y + (SubPropertyComponents.at(i)->ReadCachedAbsSize().Y * 0.5f) > mouse->ReadCachedAbsPosition().Y)
						{
							ElementDragDestIdx = i;
							newPos = SubPropertyComponents.at(i)->ReadPosition();

							if (i > 0)
							{
								DragMouseYPosThreshold.Min = newPos.Y - (SubPropertyComponents.at(i-1)->ReadCachedAbsSize().Y * 0.5f);
							}
							else
							{
								DragMouseYPosThreshold.Min = MIN_FLOAT;
							}

							DragMouseYPosThreshold.Max = newPos.Y + (SubPropertyComponents.at(i)->ReadCachedAbsSize().Y * 0.5f);

							break;
						}
					}

					DragDestTransform->SetVisibility(true);
					DragDestTransform->SetPosition(newPos);
				}
			}
		}
	}
	else if (!IsReadOnly() && !IsCollapsed() && HoveredDragRegionTransform == nullptr && IsWithinBounds(mouse->ReadPosition())) //If not dragging an element, write access, expanded, and not hovering over something.
	{
		//Find the drag transform the mouse is hovering over.
		for (EditPropertyComponent* subProp : SubPropertyComponents)
		{
			//Check if the mouse is hovering over this component. Ignore X since that is checked above.
			if (subProp->ReadCachedAbsPosition().Y + subProp->ReadCachedAbsSize().Y < mouse->ReadPosition().Y)
			{
				continue;
			}

			if (subProp->ReadCachedAbsPosition().Y > mouse->ReadPosition().Y)
			{
				//The SubPropertyComponents are sorted in ascending order. If the mouse is above this component, then assume all other components are also out of range.
				break;
			}

			//Find the drag component residing in this component.
			for (PlanarTransformComponent* elementDragRegion : ElementDragRegions)
			{
				if (elementDragRegion->GetOwner() != subProp)
				{
					continue;
				}

				//Found it
				if (elementDragRegion->IsWithinBounds(mouse->ReadPosition()))
				{
					TexturePool* localTexturePool = TexturePool::FindTexturePool();
					CHECK(localTexturePool != nullptr)
					if (const Texture* handTexture = localTexturePool->GetTexture(HashedString("Engine.Input.CursorOpenHand")))
					{
						HoveredDragRegionTransform = elementDragRegion;
						OverrideMouse = mouse;
						mouse->PushMouseIconOverride(handTexture, SDFUNCTION_2PARAM(this, EditableArray, HandleDragRegionHover, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
					}
				}

				break; //Break ElementDragRegions loop since the one with the matching owner was found.
			}

			break; //Break the SubPropertyComponents loop since the one that could possibly be hovered was found.
		}
	}
}

void EditableArray::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (ElementDragIdx != INDEX_NONE && sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonReleased)
	{
		if (ContainerUtils::IsValidIndex(SubPropertyComponents, ElementDragIdx))
		{
			SubPropertyComponents.at(ElementDragIdx)->SetVisibility(true);
		}

		//Perform the reorder
		if (ElementDragDestIdx != INDEX_NONE && ElementDragIdx != ElementDragDestIdx && ElementDragIdx + 1 != ElementDragDestIdx)
		{
			if (ElementDragDestIdx > ElementDragIdx)
			{
				//If moving to an element below the current dragged item, decrement the destination idx by 1 since the marker is between the elements (moving the cur element would cause others to shift).
				ElementDragDestIdx--;
			}

			MoveElement(ElementDragIdx, ElementDragDestIdx);
		}

		if (DragGhostTransform != nullptr)
		{
			DragGhostTransform->Destroy();
			DragGhostTransform = nullptr;
		}

		if (DragGhostTexture != nullptr)
		{
			DragGhostTexture->Destroy();
			DragGhostTexture = nullptr;
		}

		if (DragDestTransform != nullptr)
		{
			DragDestTransform->SetVisibility(false);
		}

		ElementDragIdx = INDEX_NONE;
		ElementDragDestIdx = INDEX_NONE;
	}
}

bool EditableArray::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool consumeEvent = Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType);
	if (consumeEvent)
	{
		return true;
	}

	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonPressed && !bGrabbingBarrier)
	{
		Vector2 mousePos(sfmlEvent.x, sfmlEvent.y);

		//If the user clicked on the header, toggle collapse.
		if ((PropertyName != nullptr && PropertyName->IsWithinBounds(mousePos)) ||
			(ArraySizeLabel != nullptr && ArraySizeLabel->IsWithinBounds(mousePos)))
		{
			ToggleCollapse();
			consumeEvent = true;
		}
		else if (!IsCollapsed() && ElementDragIdx == INDEX_NONE && !IsReadOnly() && IsWithinBounds(mousePos))
		{
			//If user clicked on an element's property name
			for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
			{
				if (SubPropertyComponents.at(i)->ReadCachedAbsPosition().Y > mousePos.Y)
				{
					//Since the SubPropertyComponents are sorted in ascending order, we can abort early if the cur component is beyond the mouse pointer. All following components would have been beyond range, too.
					break;
				}

				if (!SubPropertyComponents.at(i)->IsWithinBounds(mousePos))
				{
					continue;
				}

				//Find the drag region under this component.
				for (PlanarTransformComponent* dragRegion : ElementDragRegions)
				{
					if (dragRegion->GetOwner() != SubPropertyComponents.at(i))
					{
						continue;
					}

					if (dragRegion->IsWithinBounds(mousePos))
					{
						ElementDragIdx = i;
						StartDragPos = mouse->GetPosition();
						consumeEvent = true;

						//Change the mouse icon to a hand grab
						TexturePool* localTexturePool = TexturePool::FindTexturePool();
						CHECK(localTexturePool != nullptr)
						if (const Texture* grabTexture = localTexturePool->GetTexture(HashedString("Engine.Input.CursorGrab")))
						{
							OverrideMouse = mouse;
							mouse->PushMouseIconOverride(grabTexture, SDFUNCTION_2PARAM(this, EditableArray, HandleDragElement, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
						}
					}

					break;
				}

				break;
			}
		}
	}

	return consumeEvent;
}

EditPropertyComponent::SInspectorProperty* EditableArray::CreateDefaultInspector (const DString& propName) const
{
	CHECK_INFO(false, "Cannot create a default inspector from an EditableArray due to infinite recursion. If there's a need for an array of an array, then use the EditableArray::SInspectorArray InspectorList constructor.")
	return nullptr;
}

GuiComponent* EditableArray::GetComponentTooltipsShouldAttachTo ()
{
	if (PropertyName != nullptr)
	{
		return PropertyName.Get();
	}

	return Super::GetComponentTooltipsShouldAttachTo();
}

void EditableArray::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (BarrierTransform.IsValid())
	{
		if (ArraySizeLabel != nullptr)
		{
			ArraySizeLabel->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);
			ArraySizeLabel->SetSize(Vector2(1.f - ArraySizeLabel->GetAnchorLeftDist(), ArraySizeLabel->ReadSize().Y));
		}
	}
}

void EditableArray::UnbindVariable ()
{
	Super::UnbindVariable();

	if (Binder != nullptr)
	{
		delete Binder;
		Binder = nullptr;
	}
}

void EditableArray::SetNormalFontColor (Color newNormalFontColor)
{
	Super::SetNormalFontColor(newNormalFontColor);

	if (!IsReadOnly() && ArraySizeLabel != nullptr && ArraySizeLabel->GetRenderComponent() != nullptr)
	{
		ArraySizeLabel->GetRenderComponent()->SetFontColor(NormalFontColor.Source);
	}

	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (EditPropertyComponent* element = dynamic_cast<EditPropertyComponent*>(iter.GetSelectedComponent()))
		{
			element->SetNormalFontColor(NormalFontColor);
		}
	}
}

void EditableArray::SetReadOnlyFontColor (Color newReadOnlyFontColor)
{
	Super::SetReadOnlyFontColor(newReadOnlyFontColor);

	if (IsReadOnly() && ArraySizeLabel != nullptr && ArraySizeLabel->GetRenderComponent() != nullptr)
	{
		ArraySizeLabel->GetRenderComponent()->SetFontColor(ReadOnlyFontColor.Source);
	}

	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (EditPropertyComponent* element = dynamic_cast<EditPropertyComponent*>(iter.GetSelectedComponent()))
		{
			element->SetReadOnlyFontColor(ReadOnlyFontColor);
		}
	}
}

void EditableArray::SetReadOnly (bool bNewReadOnly)
{
	Super::SetReadOnly(bNewReadOnly);

	if (ArraySizeLabel != nullptr && ArraySizeLabel->GetRenderComponent() != nullptr)
	{
		ArraySizeLabel->GetRenderComponent()->SetFontColor(IsReadOnly() ? ReadOnlyFontColor.Source : NormalFontColor.Source);
	}

	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (EditPropertyComponent* element = dynamic_cast<EditPropertyComponent*>(iter.GetSelectedComponent()))
		{
			element->SetReadOnly(IsReadOnly());
		}
	}
}

void EditableArray::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	FirstSubPropPos = LineHeight;
}

void EditableArray::Destroyed ()
{
	if (DragGhostTransform != nullptr)
	{
		DragGhostTransform->Destroy();
		DragGhostTransform = nullptr;
	}

	if (DragGhostTexture != nullptr)
	{
		DragGhostTexture->Destroy();
		DragGhostTexture = nullptr;
	}

	if (Binder != nullptr)
	{
		delete Binder;
		Binder = nullptr;
	}

	//Empty the drag regions so this component would not have to clean it up one-by-one when each sub property is removed.
	ContainerUtils::Empty(OUT ElementDragRegions);

	//Unregister callbacks
	if (OverrideMouse != nullptr)
	{
		SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> hoverOverride(SDFUNCTION_2PARAM(this, EditableArray, HandleDragRegionHover, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> dragOverride(SDFUNCTION_2PARAM(this, EditableArray, HandleDragElement, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		if (OverrideMouse->IsIconOverrideRegistered(hoverOverride))
		{
			OverrideMouse->RemoveIconOverride(hoverOverride);
		}

		if (OverrideMouse->IsIconOverrideRegistered(dragOverride))
		{
			OverrideMouse->RemoveIconOverride(dragOverride);
		}

		OverrideMouse = nullptr;
	}

	Super::Destroyed();
}

void EditableArray::InitializeComponents ()
{
	Super::InitializeComponents();

	Float lineHeight = ElementHeight;

	CHECK(PropertyName.IsValid() && BarrierTransform.IsValid())
	PropertyName->SetSize(PropertyName->ReadSize().X, lineHeight);
	BarrierTransform->SetSize(BarrierTransform->ReadSize().X, lineHeight);

	ArraySizeLabel = LabelComponent::CreateObject();
	if (AddComponent(ArraySizeLabel))
	{
		ArraySizeLabel->SetAutoRefresh(false);
		ArraySizeLabel->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);
		ArraySizeLabel->SetSize(1.f - ArraySizeLabel->GetAnchorLeftDist(), lineHeight);
		ArraySizeLabel->SetWrapText(false);
		ArraySizeLabel->SetClampText(true);
		ArraySizeLabel->SetAutoRefresh(true);
	}

	DragDestTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(DragDestTransform))
	{
		DragDestTransform->SetPosition(Vector2::ZERO_VECTOR);
		DragDestTransform->SetSize(Vector2(1.f, 8.f));
		DragDestTransform->SetVisibility(false);

		DragDestColorComp = ColorRenderComponent::CreateObject();
		if (DragDestTransform->AddComponent(DragDestColorComp))
		{
			DragDestColorComp->SetPivot(Vector2(0.f, 0.5f));
			DragDestColorComp->SolidColor = Color(200, 200, 64, 175);
		}
	}
}

Float EditableArray::CalcVerticalSpace (bool includeSubComp) const
{
	//Only include the subcomponents if this array is expanded.
	return Super::CalcVerticalSpace(!IsCollapsed());
}

DString EditableArray::ConstructValueAsText () const
{
	if (Binder == nullptr)
	{
		return TXT("nullptr");
	}

	//Return the size of the array
	Int numItems = Int(Binder->GetArraySize());
	DString result = numItems.ToString() + TXT(" item");
	if (numItems != 1)
	{
		result += TXT("s");
	}

	return result;
}

void EditableArray::ProcessApplyEdit (EditPropertyComponent* editedComp)
{
	CHECK(editedComp != nullptr)

	if (editedComp != this) //If one of the components within this array was edited. . .
	{
		if (Binder != nullptr && Binder->IsBounded())
		{
			//Find the component that's directly attached to this array. (For data structs, we want the data struct instead of its SubProperties). This should have no affect if the changedElement is directly attached to the array.
			EditPropertyComponent* relevantComp = editedComp;
			while (true)
			{
				EditPropertyComponent* owningComp = dynamic_cast<EditPropertyComponent*>(relevantComp->GetOwner());
				if (owningComp == nullptr)
				{
					EditorLog.Log(LogCategory::LL_Warning, TXT("Error in EditableArray::HandleElementChanged. Unable to find connection in the component chain between %s and %s."), ToString(), editedComp->ToString());
					return;
				}

				if (owningComp == this)
				{
					break; //Found the relevant comp
				}

				//Keep climbing up until we find the array
				relevantComp = owningComp;
			}

			//Find the index this component belongs to.
			size_t foundIdx = INDEX_NONE;
			for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
			{
				if (SubPropertyComponents.at(i) == relevantComp)
				{
					foundIdx = i;
					break;
				}
			}

			if (foundIdx != INDEX_NONE)
			{
				Binder->CopyGuiToArray(relevantComp, foundIdx);
			}
		}
	}

	Super::ProcessApplyEdit(editedComp);
}

void EditableArray::ConstructArray ()
{
	if (IsFixedSize() || IsReadOnly())
	{
		if (AddItemButton != nullptr)
		{
			ContainerUtils::RemoveItem(OUT FieldControls, AddItemButton);
			AddItemButton->Destroy();
			AddItemButton = nullptr;
		}

		if (EmptyArrayButton != nullptr)
		{
			ContainerUtils::RemoveItem(OUT FieldControls, EmptyArrayButton);
			EmptyArrayButton->Destroy();
			EmptyArrayButton = nullptr;
		}
	}
	else
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());

		if (EmptyArrayButton == nullptr)
		{
			EmptyArrayButton = AddFieldControl();
			if (EmptyArrayButton != nullptr)
			{
				EmptyArrayButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				EmptyArrayButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableArray, HandleEmptyArrayReleased, void, ButtonComponent*));
				if (localTheme != nullptr)
				{
					if (FrameComponent* background = EmptyArrayButton->GetBackground())
					{
						background->SetCenterTexture(localTheme->FieldControlRemove);
					}
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (EmptyArrayButton->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("EmptyArray"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
				}
			}
		}

		if (AddItemButton == nullptr)
		{
			AddItemButton = AddFieldControl();
			if (AddItemButton != nullptr)
			{
				AddItemButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				AddItemButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableArray, HandleAddItemReleased, void, ButtonComponent*));
				if (localTheme != nullptr)
				{
					if (FrameComponent* background = AddItemButton->GetBackground())
					{
						background->SetCenterTexture(localTheme->FieldControlAdd);
					}
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (AddItemButton->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("AddItem"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
				}
			}
		}
	}

	ImportVector();
}

void EditableArray::SetArraySize (size_t newSize)
{
	if (SubPropertyComponents.size() == newSize)
	{
		return;
	}
	else if (SubPropertyComponents.size() > newSize)
	{
		//Need to shrink the array
		std::vector<EditPropertyComponent*> compsToRemove;
		for (size_t i = newSize; i < SubPropertyComponents.size(); ++i)
		{
			compsToRemove.push_back(SubPropertyComponents.at(i));
		}

		for (EditPropertyComponent* oldComp : compsToRemove)
		{
			oldComp->Destroy(); //This should automatically shrink the SubPropertyComponents vector.
		}

		CHECK(SubPropertyComponents.size() == newSize)
	}
	else
	{
		size_t numNewElements = newSize - SubPropertyComponents.size();
		for (size_t i = 0; i < numNewElements; ++i)
		{
			size_t insertIdx = INDEX_NONE;
			if (Binder != nullptr)
			{
				insertIdx = Binder->GetArraySize();
				Binder->InsertArrayElement(this, insertIdx);
			}

			AddEditableComponent(false, SubPropertyComponents.size()); //Don't bother refreshing controls. They will be refreshed later in this function.
		}
	}

	RefreshArraySizeState();
	MarkPropertyTextDirty();
	MarkVerticalSpaceDirty();
}

void EditableArray::SyncVector ()
{
	ImportVector();
}

void EditableArray::ToggleCollapse ()
{
	bExtended = !bExtended;
	for (EditPropertyComponent* subProp : SubPropertyComponents)
	{
		subProp->SetVisibility(bExtended);
	}

	MarkVerticalSpaceDirty();
}

void EditableArray::SetElementHeight (Float newElementHeight)
{
	ElementHeight = newElementHeight;
	FieldButtonSize.Y = ElementHeight;

	if (ArraySizeLabel != nullptr)
	{
		ArraySizeLabel->SetSize(ArraySizeLabel->ReadSize().X, ElementHeight);
	}

	if (PropertyName.IsValid())
	{
		PropertyName->SetSize(PropertyName->ReadSize().X, ElementHeight);
	}

	if (BarrierTransform.IsValid())
	{
		BarrierTransform->SetSize(BarrierTransform->ReadSize().X, ElementHeight);
	}

	for (ButtonComponent* fieldControl : FieldControls)
	{
		fieldControl->SetSize(FieldButtonSize);
	}
}

void EditableArray::SetFixedSize (bool bNewFixedSize)
{
	bFixedSize = bNewFixedSize;
}

void EditableArray::SetMaxItems (Int newMaxItems)
{
	MaxItems = newMaxItems;
}

void EditableArray::ImportVector ()
{
	if (Binder == nullptr || !Binder->IsBounded())
	{
		return;
	}

	SDFunction<void, ButtonComponent*> insertDelegate = SDFUNCTION_1PARAM(this, EditableArray, HandleInsertItemReleased, void, ButtonComponent*);
	SDFunction<void, ButtonComponent*> removeDelegate = SDFUNCTION_1PARAM(this, EditableArray, HandleDeleteItemReleased, void, ButtonComponent*);

	size_t arraySize = Binder->GetArraySize();
	for (size_t i = 0; i < arraySize; ++i)
	{
		if (i >= SubPropertyComponents.size()) //Need to create a new EditPropertyComponent for this element
		{
			AddEditableComponent(false, SubPropertyComponents.size()); //Don't bother refreshing controls. The controls will be refreshed later in this function.
		}
		else //using existing EditPropertyComponent
		{
			//Update existing EditPropertyComponent's field controls enabledness
			ButtonComponent* insertButton = FindElementFieldControl(SubPropertyComponents.at(i), insertDelegate);
			if (insertButton != nullptr)
			{
				insertButton->SetEnabled(!IsFixedSize() && !IsReadOnly() && (MaxItems <= 0 || Int(arraySize) < MaxItems));
			}

			ButtonComponent* removeButton = FindElementFieldControl(SubPropertyComponents.at(i), removeDelegate);
			if (removeButton != nullptr)
			{
				removeButton->SetEnabled(!IsFixedSize() && !IsReadOnly());
			}
		}

		//Update the element to this vector element.
		Binder->CopyArrayToGui(i, SubPropertyComponents.at(i));
	}

	//Trim out the extra elements (eg: if the new EditedArray is smaller than the number of GUI elements.)
	if (arraySize < SubPropertyComponents.size())
	{
		std::vector<EditPropertyComponent*> compsToRemove;
		for (size_t i = arraySize; i < SubPropertyComponents.size(); ++i)
		{
			compsToRemove.push_back(SubPropertyComponents.at(i));
		}

		for (EditPropertyComponent* subProp : compsToRemove)
		{
			subProp->Destroy();
		}
	}

	RefreshArraySizeState();
	MarkPropertyTextDirty();
	MarkVerticalSpaceDirty();
}

void EditableArray::ExportVector ()
{
	if (Binder != nullptr && Binder->IsBounded())
	{
		Binder->SetArraySize(this, SubPropertyComponents.size());

		for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
		{
			Binder->CopyGuiToArray(SubPropertyComponents.at(i), i);
		}
	}

	ProcessApplyEdit(this);
}

void EditableArray::AddEditableComponent (bool bRefreshControls, size_t componentIndex)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());

	//This vector will either point to ItemTemplateList's prop list, or (in the case for inner arrays) the inner item template's prop list.
	const std::vector<EditPropertyComponent::SInspectorProperty*>* arrayPropList = nullptr;
	if (!ContainerUtils::IsEmpty(ItemTemplate.ReadPropList()))
	{
		arrayPropList = &ItemTemplate.ReadPropList();
	}
	else
	{
		//TODO: Implement Arrays within Arrays. The inner array should climb up the component owner chain to find the EditableArray that defines this array's item template.
	}

	CHECK_INFO(arrayPropList != nullptr && arrayPropList->size() > 0, "ItemTemplate doesn't have any properties. Make sure to call EditItemTemplate to define the Gui component(s) each element should be.")

	//Add main element
	EditPropertyComponent::SInspectorProperty* rootProp = arrayPropList->at(0);
	EditPropertyComponent* rootElement = rootProp->CreateBlankEditableComponent();
	CHECK(rootElement != nullptr)
		
	if (AddComponent(rootElement))
	{
		rootProp->AssociatedComp = rootElement;

		rootProp->EditPropComponent();
		rootElement->RemoveVarUpdateTickComponent(); //Destroy ticks since the vectors are not syncing continuously anyways. Might as well save on resources by preventing each component from ticking.
		rootElement->SetSize(Vector2(1.f, ElementHeight));
		rootElement->SetReadOnly(IsReadOnly());
		rootElement->SetVisibility(!IsCollapsed());

		ButtonComponent* removeButton = rootElement->AddFieldControl();
		if (removeButton != nullptr)
		{
			removeButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			removeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableArray, HandleDeleteItemReleased, void, ButtonComponent*));
			removeButton->SetEnabled(!IsFixedSize() && !IsReadOnly());
			if (localTheme != nullptr)
			{
				if (FrameComponent* background = removeButton->GetBackground())
				{
					background->SetCenterTexture(localTheme->FieldControlRemove);
				}
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (removeButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("RemoveItem"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
			}
		}

		ButtonComponent* insertButton = rootElement->AddFieldControl();
		if (insertButton != nullptr)
		{
			size_t arraySize = (Binder != nullptr) ? Binder->GetArraySize() : 0;

			insertButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			insertButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableArray, HandleInsertItemReleased, void, ButtonComponent*));
			insertButton->SetEnabled(!IsFixedSize() && !IsReadOnly() && (MaxItems <= 0 || Int(arraySize) < MaxItems));
			if (localTheme != nullptr)
			{
				if (FrameComponent* background = insertButton->GetBackground())
				{
					background->SetCenterTexture(localTheme->FieldControlAdd);
				}
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (insertButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("InsertItem"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
			}
		}

		if (rootElement->GetPropertyName() != nullptr)
		{
			rootElement->GetPropertyName()->SetText(TXT(" [") + DString::MakeString(SubPropertyComponents.size() - 1) + TXT("]"));

			//The developer can reorder elements when dragging on this region on the property name.
			PlanarTransformComponent* dragTransform = PlanarTransformComponent::CreateObject();
			if (rootElement->AddComponent(dragTransform))
			{
				dragTransform->SetPosition(Vector2::ZERO_VECTOR);
				dragTransform->SetSize(Vector2(16.f, 1.f));
				dragTransform->SetAnchorTop(2.f);
				dragTransform->SetAnchorBottom(2.f);
				ElementDragRegions.push_back(dragTransform);

				//Shift the property name a bit so it doesn't overlap with the drag region (would cause conflicts with mouse click since structs and arrays would toggle collapse on mouse click).
				rootElement->GetPropertyName()->SetAnchorLeft(rootElement->GetPropertyName()->GetAnchorLeftDist() + dragTransform->ReadSize().X);

				SpriteComponent* dragSprite = SpriteComponent::CreateObject();
				if (dragTransform->AddComponent(dragSprite))
				{
					dragSprite->SetSpriteTexture(localTheme->ArrayReorderRegionTexture);
					dragSprite->SetDrawMode(SpriteComponent::DM_Tiled);
					dragSprite->SetDrawCoordinatesMultipliers(4.f, 4.f);
				}
			}
		}

		if (rootElement->GetBarrierTransform() != nullptr)
		{
			//Default the barrier's horizontal position be equal to this array's barrier horizontal position
			rootElement->GetBarrierTransform()->SetPosition(BarrierTransform->ReadPosition().X, rootElement->GetBarrierTransform()->ReadPosition().Y);
			rootElement->UpdatePropertyHorizontalTransforms();
		}
	}
	
	if (arrayPropList->size() > 1)
	{
		//Item template specifies multiple components. It's most likely to be a struct. Add sub components
		for (size_t i = 1; i < arrayPropList->size(); ++i)
		{
			EditPropertyComponent::SInspectorProperty* subPropTemplate = arrayPropList->at(i);
			CHECK(subPropTemplate != nullptr)

			EditPropertyComponent* subProp = subPropTemplate->CreateBlankEditableComponent();
			CHECK(subProp != nullptr)

			EditPropertyComponent* propOwner = rootElement;
			if (subPropTemplate->OwningStruct != nullptr && subPropTemplate->OwningStruct->AssociatedComp != nullptr)
			{
				propOwner = subPropTemplate->OwningStruct->AssociatedComp;
			}
		
			if (propOwner->AddComponent(subProp))
			{
				subPropTemplate->AssociatedComp = subProp;
				subPropTemplate->EditPropComponent();
				subProp->RemoveVarUpdateTickComponent(); //No need to sync this variable to an array element. The vectors will sync instead. Vars may move around; however the prop components do not.

				if (subProp->GetBarrierTransform() != nullptr)
				{
					//Default the barrier's horizontal position be equal to this array's barrier horizontal position
					subProp->GetBarrierTransform()->SetPosition(BarrierTransform->ReadPosition().X, subProp->GetBarrierTransform()->ReadPosition().Y);
					subProp->UpdatePropertyHorizontalTransforms();
				}
			}
		}
	}

	//Instruct the rootElement to bind a local variable (this recursively binds its local variables for structs). Can't have them bind to the vector element since the vector may change memory addresses.
	//Instead they will bind to themselves for functionality purposes.
	rootElement->BindToLocalVariable();

	//Clear template item references since this InspectorList is reused for the next item.
	for (EditPropertyComponent::SInspectorProperty* templateProp : *arrayPropList)
	{
		templateProp->AssociatedComp = nullptr;
	}

	//Instead of adding the new element at the end of the array, move it to be at the specified index.
	if (componentIndex != SubPropertyComponents.size() - 1)
	{
		DString nextIdxStr = rootElement->GetPropertyNameStr();
		rootElement->GetPropertyName()->SetText(SubPropertyComponents.at(componentIndex)->GetPropertyNameStr());

		for (size_t i = SubPropertyComponents.size() - 1; i > componentIndex; --i)
		{
			SubPropertyComponents.at(i) = SubPropertyComponents.at(i-1);

			//Make sure the [x] index is correct based on the component's new position.
			DString nextIdxStrCpy = SubPropertyComponents.at(i)->GetPropertyNameStr();
			SubPropertyComponents.at(i)->GetPropertyName()->SetText(nextIdxStr);
			nextIdxStr = nextIdxStrCpy;
		}

		SubPropertyComponents.at(componentIndex) = rootElement;
	}

	if (bRefreshControls)
	{
		MarkPropertyTextDirty();
		MarkVerticalSpaceDirty();
		RefreshArraySizeState();
	}
}

void EditableArray::MoveElement (size_t oldIdx, size_t newIdx)
{
	CHECK(ContainerUtils::IsValidIndex(SubPropertyComponents, oldIdx) && ContainerUtils::IsValidIndex(SubPropertyComponents, newIdx) && oldIdx != newIdx)

	size_t lesserIdx;
	size_t greaterIdx;
	if (oldIdx < newIdx)
	{
		lesserIdx = oldIdx;
		greaterIdx = newIdx;
	}
	else
	{
		lesserIdx = newIdx;
		greaterIdx = oldIdx;
	}

	if (Binder != nullptr)
	{
		Binder->MoveArrayElement(this, oldIdx, newIdx);
	}

	//Move SubPropertyComponents. The array populates its vertical list in order they appear in this vector.
	EditPropertyComponent* movedComp = SubPropertyComponents.at(oldIdx);

	if (oldIdx > newIdx)
	{
		++oldIdx;
	}
	else
	{
		++newIdx;
	}

	SubPropertyComponents.insert(SubPropertyComponents.begin() + newIdx, movedComp);
	SubPropertyComponents.erase(SubPropertyComponents.begin() + oldIdx);

	//Even though the vertical size hasn't changed, mark it as dirty anyways since the array would need to reposition its elements around since the component order has changed.
	MarkVerticalSpaceDirty();

	//Need to update the element names for all affected components (since the components, themselves, are moving around).
	for (size_t i = lesserIdx; i <= greaterIdx; ++i)
	{
		if (LabelComponent* nameComp = SubPropertyComponents.at(i)->GetPropertyName())
		{
			nameComp->SetText(TXT(" [") + DString::MakeString(i) + TXT("]"));
		}
	}
}

size_t EditableArray::FindEditableCompIdx (ButtonComponent* fieldControl) const
{
	size_t compIdx = SD_MAXINT;

	EditPropertyComponent* editComp = dynamic_cast<EditPropertyComponent*>(fieldControl->GetOwner());
	if (editComp == nullptr)
	{
		return compIdx;
	}

	for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
	{
		if (editComp == SubPropertyComponents.at(i))
		{
			compIdx = i;
			break;
		}
	}

	return compIdx;
}

ButtonComponent* EditableArray::FindElementFieldControl (EditPropertyComponent* editComp, SDFunction<void, ButtonComponent*> ctrlDelegate) const
{
	CHECK(editComp != nullptr)
	for (ButtonComponent* fieldCtrl : editComp->ReadFieldControls())
	{
		if (fieldCtrl->ReadOnButtonReleased() == ctrlDelegate)
		{
			return fieldCtrl;
		}
	}

	return nullptr;
}

void EditableArray::RefreshArraySizeState ()
{
	size_t arraySize = SubPropertyComponents.size();
	if (Binder != nullptr)
	{
		arraySize = Binder->GetArraySize();
	}

	if (ArraySizeLabel != nullptr)
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		ArraySizeLabel->SetText(DString::CreateFormattedString(translator->TranslateText(TXT("ArraySize"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")), Int(arraySize)));
	}

	bool bAllowEdits = (!bReadOnly && !bFixedSize);
	if (AddItemButton != nullptr)
	{
		AddItemButton->SetEnabled(bAllowEdits && (MaxItems <= 0 || Int(arraySize) < MaxItems));
	}

	if (EmptyArrayButton != nullptr)
	{
		EmptyArrayButton->SetEnabled(bAllowEdits && arraySize > 0);
	}

	SDFunction<void, ButtonComponent*> insertDelegate = SDFUNCTION_1PARAM(this, EditableArray, HandleInsertItemReleased, void, ButtonComponent*);
	bool insertEnabled = (MaxItems <= 0 || Int(arraySize) < MaxItems);
	for (EditPropertyComponent* editComp : SubPropertyComponents)
	{
		for (ButtonComponent* fieldCtrl : editComp->ReadFieldControls())
		{
			if (fieldCtrl->ReadOnButtonReleased() == insertDelegate)
			{
				fieldCtrl->SetEnabled(insertEnabled);
				break;
			}
		}
	}
}

void EditableArray::InitDragGhostObjs (MousePointer* attachTo)
{
	CHECK(attachTo != nullptr && ContainerUtils::IsValidIndex(SubPropertyComponents, ElementDragIdx))
	CHECK(DragGhostTransform == nullptr && DragGhostTexture == nullptr)

	MousePointer* rootMouse = attachTo->GetRootMouse();

	DragGhostTexture = RenderTexture::CreateObject();
	const Vector2& textureSize = SubPropertyComponents.at(ElementDragIdx)->ReadCachedAbsSize();
	if (!DragGhostTexture->CreateResource(textureSize.X.ToInt(), textureSize.Y.ToInt(), false))
	{
		return;
	}

	if (TickComponent* tick = DragGhostTexture->GetTick())
	{
		//Prevent the texture from redrawing on tick. It's a static image.
		tick->Destroy();
	}

	//Calc deltaPos before the absolute position of the component changed for rendering the texture.
	EditPropertyComponent* dragComp = SubPropertyComponents.at(ElementDragIdx);
	Vector2 deltaPos(dragComp->ReadCachedAbsPosition() - StartDragPos);

	//Draw the specific SubProperty to the texture one time. Don't use draw layers since it doesn't need to render the entire GuiEntity, recompute projections. This should also ignore cameras, mouse cursors, and tooltips.
	{
		//Hack: Temporarily set SubProp relative to nothing since it should render relative to the top left corner of the texture instead of the GuiEntity's position.
		PlanarTransform* oldRelativeTo = dragComp->GetRelativeTo();
		Vector2 oldPos = dragComp->GetPosition();
		dragComp->SetRelativeTo(nullptr);
		dragComp->SetPosition(Vector2::ZERO_VECTOR);
		dragComp->SetRootRenderTarget(DragGhostTexture);
		dragComp->ComputeAbsTransform();

		DragGhostTexture->Reset();
		for (ComponentIterator iter(dragComp, true); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (!iter.GetSelectedComponent()->IsVisible())
			{
				continue;
			}

			if (RenderComponent* rendComp = dynamic_cast<RenderComponent*>(iter.GetSelectedComponent()))
			{
				if (Transformation* transform = rendComp->GetOwnerTransform())
				{
					transform->MarkProjectionDataDirty();
				}

				rendComp->Render(DragGhostTexture, nullptr);
			}
		}
		DragGhostTexture->Display();

		dragComp->SetPosition(oldPos);
		dragComp->SetRelativeTo(oldRelativeTo);
	}

	DragGhostTransform = PlanarTransformComponent::CreateObject();
	if (rootMouse->AddComponent(DragGhostTransform))
	{
		rootMouse->MoveComponentToFront(DragGhostTransform); //Render behind the mouse
		DragGhostTransform->SetPosition(deltaPos);
		DragGhostTransform->SetSize(textureSize);

		SpriteComponent* sprite = SpriteComponent::CreateObject();
		if (DragGhostTransform->AddComponent(sprite))
		{
			sprite->SetSpriteTexture(DragGhostTexture);
			if (sf::Sprite* innerSprite = sprite->GetSprite())
			{
				innerSprite->setColor(sf::Color(255, 255, 255, 200)); //Make the sprite transparent
			}
		}
	}

	//Hide the sub property component to make it look like the user moved it from its original location.
	SubPropertyComponents.at(ElementDragIdx)->SetVisibility(false);
}

void EditableArray::HandleEmptyArrayReleased (ButtonComponent* uiComp)
{
	CHECK(!IsReadOnly() && !IsFixedSize())

	std::vector<EditPropertyComponent*> compsToRemove;
	compsToRemove.resize(SubPropertyComponents.size());
	for (size_t i = 0; i < SubPropertyComponents.size(); ++i)
	{
		compsToRemove.at(i) = SubPropertyComponents.at(i);
	}

	for (EditPropertyComponent* subProp : compsToRemove)
	{
		subProp->Destroy();
	}

	if (Binder != nullptr)
	{
		Binder->SetArraySize(this, 0);
	}

	RefreshArraySizeState();
	MarkPropertyTextDirty();
	MarkVerticalSpaceDirty();
	ExportVector();
}

void EditableArray::HandleAddItemReleased (ButtonComponent* uiComp)
{
	CHECK(!IsReadOnly() && !IsFixedSize())

	size_t insertIdx = INDEX_NONE;
	if (Binder != nullptr)
	{
		insertIdx = Binder->GetArraySize();
		Binder->InsertArrayElement(this, insertIdx);
	}

	AddEditableComponent(true, SubPropertyComponents.size());
	EditPropertyComponent* newElement = ContainerUtils::GetLast(SubPropertyComponents);

	if (Binder != nullptr)
	{
		Binder->CopyArrayToGui(insertIdx, newElement);
	}

	ExportVector();
}

void EditableArray::HandleInsertItemReleased (ButtonComponent* uiComp)
{
	CHECK(!IsReadOnly() && !IsFixedSize())

	size_t insertIdx = FindEditableCompIdx(uiComp);
	if (insertIdx >= SubPropertyComponents.size())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Attempted to insert an array element from a component that's not found in the EditableComponent vector."));
		return;
	}

	if (Binder != nullptr && Binder->IsBounded())
	{
		Binder->InsertArrayElement(this, insertIdx);
	}

	AddEditableComponent(true, insertIdx);

	if (Binder != nullptr && Binder->IsBounded())
	{
		Binder->CopyArrayToGui(insertIdx, SubPropertyComponents.at(insertIdx));
	}
	else
	{
		SubPropertyComponents.at(insertIdx)->ResetToDefaults();
	}

	ExportVector();
}

void EditableArray::HandleDeleteItemReleased (ButtonComponent* uiComp)
{
	CHECK(!IsReadOnly() && !IsFixedSize())

	size_t removeIdx = FindEditableCompIdx(uiComp);
	if (removeIdx >= SubPropertyComponents.size())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Attempted to remove an array element from a component that's not found in the EditableComponent vector."));
		return;
	}

	size_t arraySize = 0;
	if (Binder != nullptr && Binder->IsBounded())
	{
		Binder->RemoveArrayElement(removeIdx);
		arraySize = Binder->GetArraySize();
	}
	else if (!ContainerUtils::IsEmpty(SubPropertyComponents))
	{
		arraySize = SubPropertyComponents.size() - 1;
	}

	DString prevIdxStr = SubPropertyComponents.at(removeIdx)->GetPropertyNameStr();
	SubPropertyComponents.at(removeIdx)->Destroy();
	MarkPropertyTextDirty();
	MarkVerticalSpaceDirty();

	for (size_t i = removeIdx; i < SubPropertyComponents.size(); ++i)
	{
		DString curIdx = SubPropertyComponents.at(i)->GetPropertyNameStr();

		//Update the [x] text to reflect the property component's new position in the array.
		SubPropertyComponents.at(i)->GetPropertyName()->SetText(prevIdxStr);
		prevIdxStr = curIdx;
	}

	if (ArraySizeLabel != nullptr)
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		ArraySizeLabel->SetText(DString::CreateFormattedString(translator->TranslateText(TXT("ArraySize"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")), Int(arraySize)));
	}

	//Need to enable controls to add elements
	bool bOldEnabledness = true;
	if (AddItemButton != nullptr)
	{
		bOldEnabledness = AddItemButton->GetEnabled();
		AddItemButton->SetEnabled(true);
	}

	if (!bOldEnabledness) //inserting buttons were disabled. Need to enable them.
	{
		SDFunction<void, ButtonComponent*> insertDelegate = SDFUNCTION_1PARAM(this, EditableArray, HandleInsertItemReleased, void, ButtonComponent*);
		for (EditPropertyComponent* editComp : SubPropertyComponents)
		{
			for (ButtonComponent* fieldCtrl : editComp->ReadFieldControls())
			{
				if (fieldCtrl->ReadOnButtonReleased() == insertDelegate)
				{
					fieldCtrl->SetEnabled(true);
					break;
				}
			}
		}
	}

	if (ContainerUtils::IsEmpty(SubPropertyComponents) && EmptyArrayButton != nullptr)
	{
		//Deleted the last element
		EmptyArrayButton->SetEnabled(false);
	}

	ExportVector();
}

bool EditableArray::HandleDragRegionHover (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent)
{
	if (HoveredDragRegionTransform != nullptr && HoveredDragRegionTransform->IsWithinBounds(mouse->ReadPosition()))
	{
		return true;
	}

	HoveredDragRegionTransform = nullptr;
	return false;
}

bool EditableArray::HandleDragElement (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent)
{
	return (ElementDragIdx != INDEX_NONE);
}
SD_END