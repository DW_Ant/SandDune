/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphAsset.cpp
=====================================================================
*/

#include "DynamicEnum.h"
#include "EditableArray.h"
#include "EditableBool.h"
#include "EditableDString.h"
#include "EditableEnum.h"
#include "EditableObjectComponent.h"
#include "EditablePropCommand.h"
#include "EditableInt.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "GraphAsset.h"
#include "GraphCompiler.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "GraphVariable.h"
#include "InspectorList.h"
#include <regex>

IMPLEMENT_CLASS(SD::GraphAsset, SD::Object)
SD_BEGIN

const DString GraphAsset::PERMITTED_CHARS_FOR_NAME(TXT("[a-zA-Z0-9-_]")); //Alphanumeric characters plus underscores and hyphens
const DString GraphAsset::NULL_CLASS_NAME(TXT("<None>"));
const DString GraphAsset::DEV_ASSET_EXTENSION(TXT("dasset.ini"));

std::vector<DString> GraphAsset::PendingLoadAssets;

GraphAsset::SVarOverride::SVarOverride () :
	VarName(DString::EmptyString),
	bIsRelative(false)
{
	//Noop
}

GraphAsset::SVarOverride::SVarOverride (const SVarOverride& cpyObj) :
	VarName(cpyObj.VarName),
	bIsRelative(cpyObj.bIsRelative)
{
	cpyObj.NewValueData.CopyBufferTo(OUT NewValueData);
}

GraphAsset::SDefinitionTransfer::SDefinitionTransfer (const DString& inFunctionId, GraphEditor* inFuncDefinition) :
	FunctionId(inFunctionId),
	FuncDefinition(inFuncDefinition)
{
	//Noop
}

GraphAsset::SDefinitionTransfer::SDefinitionTransfer (const SDefinitionTransfer& cpyObj) :
	FunctionId(cpyObj.FunctionId),
	FuncDefinition(cpyObj.FuncDefinition)
{
	//Noop
}

void GraphAsset::InitProps ()
{
	Super::InitProps();

	AssetDefinition = nullptr;
	AssetName = DString::EmptyString;
	bGlobal = false;
	DisplayedName = DString::EmptyString;
	AssetType = DString::EmptyString;
	LoadState = LS_NotLoaded;
	SaveLocation = Directory::INVALID_DIRECTORY;
	ScriptClass = nullptr;
	Authors = TXT("Anonymous");
	License = DString::EmptyString;
	Comment = DString::EmptyString;
	bAbstract = false;
	DefaultFunctionSize = Vector2(8192.f, 8192.f);

	bAssetInitialized = false;
}

void GraphAsset::BeginObject ()
{
	Super::BeginObject();

	RegisterEditorInterface();

#ifdef WITH_TIME
	DateTime date;
	date.SetToCurrentTime();

	//By default in the US, Copyright is protected the moment the asset is created. Developers are welcomed to change this to a different license through a text editor or the editor tools.
	License = DString::CreateFormattedString(TXT("Copyright (c) %s YourCompany"), date.Year);
#else
	License = TXT("Copyright (c) YourCompany");
#endif
}

DString GraphAsset::GetFriendlyName () const
{
	if (!AssetName.IsEmpty())
	{
		return TXT("GraphAsset: ") + GetPresentedName();
	}

	return Super::GetFriendlyName();
}

CopiableObjectInterface* GraphAsset::CreateCopiableInstanceOfMatchingType () const
{
	if (IsAssetDefinition())
	{
		//Cannot have two assets definitions.
		return nullptr;
	}

	if (const DClass* duneClass = StaticClass())
	{
		if (const GraphAsset* cdo = dynamic_cast<const GraphAsset*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void GraphAsset::CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate)
{
	if (const GraphAsset* assetTemplate = dynamic_cast<const GraphAsset*>(objectTemplate))
	{
		AssetDefinition = assetTemplate->AssetDefinition;
		AssetName = assetTemplate->AssetName;
		UpdateEditorObjName();
		SetDisplayedName(assetTemplate->DisplayedName);
		SetAssetType(assetTemplate->AssetType);
		SetGlobal(assetTemplate->IsGlobalAsset());
		LoadState = assetTemplate->LoadState;
		SetSaveLocation(assetTemplate->ReadSaveLocation());

		//Copy member variables
		for (size_t i = 0; i < MemberVariables.size(); ++i)
		{
			delete MemberVariables.at(i);
			MemberVariables.at(i) = nullptr;
		}

		MemberVariables.resize(assetTemplate->MemberVariables.size());
		for (size_t i = 0; i < MemberVariables.size(); ++i)
		{
			if (assetTemplate->MemberVariables.at(i) != nullptr)
			{
				GraphVariable* newVar = new GraphVariable(*assetTemplate->MemberVariables.at(i));
				newVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemVarDeleted, void, EditorInterface*));
				MemberVariables.at(i) = newVar;
			}
		}

		//Copy functions
		for (size_t i = 0; i < MemberFunctions.size(); ++i)
		{
			MemberFunctions.at(i)->Destroy();
			MemberFunctions.at(i) = nullptr;
		}

		MemberFunctions.resize(assetTemplate->MemberFunctions.size());
		for (size_t i = 0; i < MemberFunctions.size(); ++i)
		{
			if (assetTemplate->MemberFunctions.at(i) != nullptr)
			{
				MemberFunctions.at(i) = dynamic_cast<GraphFunction*>(assetTemplate->MemberFunctions.at(i)->Duplicate());
			}
		}

		DefaultOverrides.resize(assetTemplate->DefaultOverrides.size());
		for (size_t i = 0; i < DefaultOverrides.size(); ++i)
		{
			DefaultOverrides.at(i).bIsRelative = assetTemplate->DefaultOverrides.at(i).bIsRelative;

			DefaultOverrides.at(i).NewValueData.EmptyBuffer();
			assetTemplate->DefaultOverrides.at(i).NewValueData.CopyBufferTo(DefaultOverrides.at(i).NewValueData);

			DefaultOverrides.at(i).VarName = assetTemplate->DefaultOverrides.at(i).VarName;
		}

		ScriptClass = assetTemplate->ScriptClass;
		SuperAsset = assetTemplate->SuperAsset;
		Authors = assetTemplate->Authors;
		License = assetTemplate->License;
		Comment = assetTemplate->Comment;
		SetAbstract(assetTemplate->IsAbstract());
		DefaultFunctionSize = assetTemplate->DefaultFunctionSize;
		
		bAssetInitialized = assetTemplate->bAssetInitialized;
	}
}

DString GraphAsset::GetEditorObjName () const
{
	return GetPresentedName();
}

DString GraphAsset::GetEditorObjTooltip () const
{
	return Comment;
}

void GraphAsset::PopulateInspectorList (InspectorList& outList)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString fileName = TXT("Editor");
	DString sectionName = TXT("GraphAsset");

	if (EditableDString::SInspectorString* editLicense = outList.AddProp(new EditableDString::SInspectorString(TXT("License"), DString::EmptyString, &License, DString::EmptyString)))
	{
		editLicense->PropertyId = AA_License;
		editLicense->bMultiLine = true;
	}

	if (EditableDString::SInspectorString* editAuthor = outList.AddProp(new EditableDString::SInspectorString(TXT("Authors"), DString::EmptyString, &Authors, TXT("Anonymous"))))
	{
		editAuthor->PropertyId = AA_Authors;
		editAuthor->bMultiLine = true;
	}

	if (EditableDString::SInspectorString* editComment = outList.AddProp(new EditableDString::SInspectorString(TXT("Comment"), DString::EmptyString, &Comment)))
	{
		editComment->PropertyId = AA_Comment;
		editComment->bMultiLine = true;
	}

	//Don't bind AssetName to this since we need to reference the old AssetName before assigning it to the new value so that we can update the old references to this class name.
	if (EditableDString::SInspectorString* editStr = outList.AddProp(new EditableDString::SInspectorString(TXT("Class Name"), translator->TranslateText(TXT("ClassNameTooltip"), fileName, sectionName), nullptr, AssetName))) //Set the default equal to whatever the editor's default value is.
	{
		editStr->PropertyId = AA_Class;
		editStr->PermittedChars = PERMITTED_CHARS_FOR_NAME;
		editStr->MaxCharacters = 64;
		editStr->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleAssetNameEdit, void, EditPropertyComponent*);
	}

	if (EditableEnum::SInspectorEnum* editInheritance = outList.AddProp(new EditableEnum::SInspectorEnum(TXT("Parent Class"), translator->TranslateText(TXT("ParentClassTooltip"), fileName, sectionName), SDFUNCTION_1PARAM(this, GraphAsset, HandlePopulateClassList, void, DropdownComponent*), 0)))
	{
		editInheritance->PropertyId = AA_ParentClass;
		editInheritance->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleSelectParentClass, void, EditPropertyComponent*);
	}

	if (EditableBool::SInspectorBool* editGlobal = outList.AddProp(new EditableBool::SInspectorBool(TXT("Is Global"), translator->TranslateText(TXT("IsGlobal"), fileName, sectionName), nullptr, false)))
	{
		editGlobal->PropertyId = AA_Global;
		editGlobal->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleToggleGlobal, void, EditPropertyComponent*);
		editGlobal->InitType = EditPropertyComponent::IIV_UseOverride;
		editGlobal->InitOverride = IsGlobalAsset();
	}

	if (EditableBool::SInspectorBool* editAbstract = outList.AddProp(new EditableBool::SInspectorBool(TXT("Is Abstract"), translator->TranslateText(TXT("IsAbstractTooltip"), fileName, sectionName), nullptr, false)))
	{
		editAbstract->PropertyId = AA_Abstract;
		editAbstract->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleToggleAbstract, void, EditPropertyComponent*);
		editAbstract->InitType = EditPropertyComponent::IIV_UseOverride;

		if (ScriptClass != nullptr)
		{
			editAbstract->InitOverride = ScriptClass->IsAbstract();
		}
		else
		{
			editAbstract->InitOverride = bAbstract;
		}
	}

	InspectorList* memberVarTemplate = new InspectorList();
	if (EditableObjectComponent::SInspectorObject* varProp = memberVarTemplate->AddProp(new EditableObjectComponent::SInspectorObject(TXT("Member Variable"), DString::EmptyString, nullptr, true)))
	{
		varProp->OnCreateEditorObject = [&](DropdownComponent*)
		{
			return new GraphVariable(HashedString(), this);
		};

		varProp->OnIsEditorObjectRelevant = [](EditorInterface*)
		{
			return false;
		};

		varProp->OnInitCreatedObject = [&](EditorInterface* obj)
		{
			if (GraphVariable* newVar = dynamic_cast<GraphVariable*>(obj))
			{
				newVar->SetInspectorType(GraphVariable::IT_MemberVariable);
				newVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemVarDeleted, void, EditorInterface*));
			}
		};
	}

	ObjectArrayBinder<GraphVariable>* arrayBinder = new ObjectArrayBinder<GraphVariable>(&MemberVariables);

	if (EditableArray::SInspectorArray* memberVarArray = outList.AddProp(new EditableArray::SInspectorArray(TXT("Member Variables"), DString::EmptyString, memberVarTemplate, arrayBinder)))
	{
		memberVarArray->PropertyId = AA_MemberVariables;
		memberVarArray->bFixedSize = false;
		memberVarArray->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleVarArrayEdit, void, EditPropertyComponent*);
		memberVarArray->OnPopulateArray = SDFUNCTION_1PARAM(this, GraphAsset, HandleInitializeMemberVars, void, EditableArray*);
	}

	InspectorList* inheritedFunctionTemplate = new InspectorList();
	InitItemTemplateForInheritedFunctions(OUT *inheritedFunctionTemplate);
	if (EditableArray::SInspectorArray* inheritFunctionArray = outList.AddProp(new EditableArray::SInspectorArray(TXT("Inherited Functions"), DString::EmptyString, inheritedFunctionTemplate, nullptr)))
	{
		inheritFunctionArray->PropertyId = AA_InheritFunctions;
		inheritFunctionArray->bFixedSize = false;
		inheritFunctionArray->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleInheritedFunctionArrayEdit, void, EditPropertyComponent*);
		inheritFunctionArray->OnPopulateArray = SDFUNCTION_1PARAM(this, GraphAsset, HandleInitializeInheritedFunctions, void, EditableArray*);
	}

	InspectorList* functionList = new InspectorList();
	if (EditableObjectComponent::SInspectorObject* functionProp = functionList->AddProp(new EditableObjectComponent::SInspectorObject(TXT("Functions"), DString::EmptyString, nullptr, true)))
	{
		functionProp->OnCreateEditorObject = [&](DropdownComponent*)
		{
			return GraphFunction::CreateObject();
		};

		functionProp->OnIsEditorObjectRelevant = [](EditorInterface*)
		{
			return false;
		};

		functionProp->OnInitCreatedObject = [&](EditorInterface* obj)
		{
			if (GraphFunction* newFunction = dynamic_cast<GraphFunction*>(obj))
			{
				newFunction->SetOwningAsset(this);
				newFunction->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemFunctionDeleted, void, EditorInterface*));
			}
		};
	}

	ObjectArrayBinder<GraphFunction>* functionBinder = new ObjectArrayBinder<GraphFunction>(&MemberFunctions);

	if (EditableArray::SInspectorArray* memberFuncArray = outList.AddProp(new EditableArray::SInspectorArray(TXT("Functions"), DString::EmptyString, functionList, functionBinder)))
	{
		memberFuncArray->PropertyId = AA_Functions;
		memberFuncArray->bFixedSize = false;
	}

	InspectorList* overrideTemplate = new InspectorList();
	overrideTemplate->PushStruct(new EditableStruct::SInspectorStruct(TXT("Default Override"), DString::EmptyString));
	{
		if (EditableEnum::SInspectorEnum* selectVar = overrideTemplate->AddProp(new EditableEnum::SInspectorEnum(TXT("Parent Variable"), DString::EmptyString, SDFUNCTION_1PARAM(this, GraphAsset, HandlePopulateOverrideVariables, void, DropdownComponent*))))
		{
			selectVar->PropertyId = GraphVariable::VA_VarName;
			selectVar->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleSelectVariableOverride, void, EditPropertyComponent*);
		}

		if (EditableBool::SInspectorBool* isRelative = overrideTemplate->AddProp(new EditableBool::SInspectorBool(TXT("Is Relative"), translator->TranslateText(TXT("IsRelative"), fileName, sectionName), nullptr, false)))
		{
			isRelative->PropertyId = GraphVariable::VA_Relative;
			isRelative->bReadOnly = true;
		}

		if (EditableDString::SInspectorString* defaultVal = overrideTemplate->AddProp(new EditableDString::SInspectorString(TXT("DefaultValue"), DString::EmptyString, nullptr, TXT("0"))))
		{
			defaultVal->PropertyId = GraphVariable::VA_DefaultValue;
		}
	}
	overrideTemplate->PopStruct();

	if (EditableArray::SInspectorArray* defaultOverrides = outList.AddProp(new EditableArray::SInspectorArray(TXT("Default Overrides"), translator->TranslateText(TXT("DefaultOverrides"), fileName, sectionName), overrideTemplate, nullptr)))
	{
		defaultOverrides->PropertyId = AA_DefaultOverrides;
		defaultOverrides->bFixedSize = false;
		defaultOverrides->OnPopulateArray = SDFUNCTION_1PARAM(this, GraphAsset, HandleInitializeVarOverrides, void, EditableArray*);
		defaultOverrides->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleDefaultOverrideArrayEdit, void, EditPropertyComponent*);
	}
}

void GraphAsset::DeleteEditorObject ()
{
	Destroy();
}

void GraphAsset::Destroyed ()
{
	UnregisterEditorInterface();

	if (!IsAssetDefinition())
	{
		CHECK(AssetDefinition != nullptr)
		ContainerUtils::RemoveItem(OUT AssetDefinition->AssetInstances, this);
	}
	else
	{
		GraphVariable::OnNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, GraphAsset, HandleMemVarNameChanged, void, const DString&, const DString&, const DString&));

		std::vector<GraphAsset*> instanceListCopy(AssetInstances);
		ContainerUtils::Empty(OUT AssetInstances); //Empty the vector to prevent the GraphAsset::Destroyed from iterating through the vector for each destroyed instance.
		for (GraphAsset* asset : instanceListCopy)
		{
			asset->Destroy();
		}
	}

	for (size_t i = 0; i < OverrideFunctions.size(); ++i)
	{
		if (OverrideFunctions.at(i) != nullptr)
		{
			OverrideFunctions.at(i)->Destroy();
		}
	}
	ContainerUtils::Empty(OUT OverrideFunctions);

	for (size_t i = 0; i < MemberFunctions.size(); ++i)
	{
		if (MemberFunctions.at(i) != nullptr)
		{
			MemberFunctions.at(i)->Destroy();
		}
	}
	ContainerUtils::Empty(OUT MemberFunctions);

	for (size_t i = 0; i < MemberVariables.size(); ++i)
	{
		if (MemberVariables.at(i) != nullptr)
		{
			MemberVariables.at(i)->DeleteEditorObject();
			MemberVariables.at(i) = nullptr;
		}
	}
	ContainerUtils::Empty(OUT MemberVariables);

	for (size_t i = 0; i < VarInstanceValues.size(); ++i)
	{
		if (VarInstanceValues.at(i) != nullptr)
		{
			VarInstanceValues.at(i)->DeleteEditorObject();
			VarInstanceValues.at(i) = nullptr;
		}
	}
	ContainerUtils::Empty(OUT VarInstanceValues);

	if (LoadState != LS_NotLoaded && IsAssetDefinition())
	{
		EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
		CHECK(editorEngine != nullptr)
		editorEngine->RemoveAsset(this);
	}

	if (ScriptClass != nullptr && IsAssetDefinition())
	{
		if (ClmEngineComponent* clmEngine = ClmEngineComponent::Find())
		{
			clmEngine->DestroyScriptClass(ScriptClass, false);
		}
	}

	Super::Destroyed();
}

void GraphAsset::InitializeAsset (const DString& newAssetName, bool isAssetDefinition)
{
	if (bAssetInitialized && isAssetDefinition != IsAssetDefinition())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize GraphAsset \"%s\". A GraphAsset cannot change its bAssetDefinition once it is set."), newAssetName);
		return;
	}
	bAssetInitialized = true;

	if (AssetName.Compare(newAssetName, DString::CC_CaseSensitive) == 0)
	{
		return;
	}

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	if (LoadState != LS_NotLoaded)
	{
		if (isAssetDefinition)
		{
			editorEngine->RemoveAsset(this);
		}
		else if (AssetDefinition != nullptr)
		{
			ContainerUtils::RemoveItem(OUT AssetDefinition->AssetInstances, this);
		}
	}

	for (GraphVariable* memVar : MemberVariables)
	{
		if (memVar != nullptr)
		{
			memVar->SetVarOwner(this);
		}
	}

	AssetName = newAssetName;
	UpdateEditorObjName();

	if (LoadState != LS_NotLoaded)
	{
		if (isAssetDefinition)
		{
			editorEngine->RegisterAsset(this);
			AssetDefinition = nullptr;

			SDFunction<void, const DString&, const DString&, const DString&> nameChangeDelegate(SDFUNCTION_3PARAM(this, GraphAsset, HandleMemVarNameChanged, void, const DString&, const DString&, const DString&));
			if (!GraphVariable::OnNameChanged.IsRegistered(nameChangeDelegate, true))
			{
				GraphVariable::OnNameChanged.RegisterHandler(nameChangeDelegate);
			}
		}
		else
		{
			HashedString hashedName(AssetName);
			if (editorEngine->ReadLoadedAssets().contains(hashedName))
			{
				if (AssetDefinition = editorEngine->ReadLoadedAssets().at(hashedName))
				{
					AssetDefinition->AssetInstances.push_back(this);
					CopyVarsFrom(AssetDefinition);
				}
			}
		}
	}
}

bool GraphAsset::LoadEssentials ()
{
	if (AssetName.IsEmpty())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to load a GraphAsset without a name."));
		return false;
	}

	if (ContainerUtils::FindInVector(PendingLoadAssets, AssetName) != UINT_INDEX_NONE)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load %s. Circular dependency detected! One of the parent classes to this asset is deriving from this same asset!"), AssetName);
		return false;
	}

	if (LoadState == LS_NotLoaded)
	{
		ConfigWriter* config = ConfigWriter::CreateObject();
		config->SetReadEverything(false);
		if (!config->OpenFile(FileAttributes(SaveLocation, AssetName + TXT(".") + DEV_ASSET_EXTENSION), false))
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load GraphAsset %s since it was unable to open the config file."), AssetName);
			config->Destroy();
			return false;
		}

		bool result = ProcessLoadEssentials(config);
		config->Destroy();
		return result;
	}

	return true;
}

bool GraphAsset::LoadFromFile ()
{
	if (LoadState != LS_Loaded)
	{
		ConfigWriter* config = ConfigWriter::CreateObject();
		if (!config->OpenFile(FileAttributes(SaveLocation, AssetName + TXT(".") + DEV_ASSET_EXTENSION), false))
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load GraphAsset %s since it was unable to open the config file."), AssetName);
			config->Destroy();
			return false;
		}

		bool bResult = true;
		if (LoadState == LS_NotLoaded)
		{
			bResult = ProcessLoadEssentials(config);
		}

		if (bResult)
		{
			bResult = ProcessLoadEverything(config);
		}

		config->Destroy();
		return bResult;
	}

	return true;
}

bool GraphAsset::SaveToDevAsset () const
{
	if (LoadState != LS_Loaded)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to an ini since the asset has not been completely loaded."), AssetName);
		return false;
	}

	if (AssetName.IsEmpty())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save an asset that doesn't have a class name."));
		return false;
	}

	if (!IsAssetDefinition())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save GraphAsset instances as a DevAsset. Instead only SaveInstanceData can be used for GraphAsset instances. Failed to save asset \"%s\"."), AssetName);
		return false;
	}

	ConfigWriter* config = ConfigWriter::CreateObject();
	config->OpenFile(FileAttributes(SaveLocation, AssetName + TXT(".") + DEV_ASSET_EXTENSION), true);

	config->SavePropertyText(DString::EmptyString, TXT("License"), License); //License should be listed on the top of the ini file.
	config->SavePropertyText(DString::EmptyString, TXT("Authors"), Authors);
	config->SavePropertyText(DString::EmptyString, TXT("Comment"), Comment);

	DString assetTypeValue = DString::EmptyString;
	if (!IsGlobalAsset())
	{
		assetTypeValue = AssetType;
	}

	config->SavePropertyText(DString::EmptyString, ASSET_TYPE, assetTypeValue);
	config->SavePropertyText(DString::EmptyString, TXT("SuperClass"), (SuperAsset != nullptr) ? SuperAsset->GetFullAssetName() : DString::EmptyString);

	Bool isAbstract = (ScriptClass != nullptr) ? ScriptClass->IsAbstract() : bAbstract;
	config->SaveProperty<Bool>(DString::EmptyString, TXT("IsAbstract"), isAbstract);

	std::vector<DString> varNames;
	varNames.reserve(MemberVariables.size());
	for (GraphVariable* var : MemberVariables)
	{
		if (var != nullptr)
		{
			varNames.push_back(var->ReadVariableName().ToString());
		}
	}
	config->SaveArray(DString::EmptyString, TXT("MemberVariables"), varNames);

	std::vector<DString> overrideFunctionNames;
	overrideFunctionNames.reserve(OverrideFunctions.size());
	for (GraphFunction* overrideFunc : OverrideFunctions)
	{
		overrideFunctionNames.push_back(overrideFunc->ReadFunctionName());
	}
	config->SaveArray(DString::EmptyString, TXT("OverrideFunctions"), overrideFunctionNames);

	std::vector<DString> memberFunctionNames;
	memberFunctionNames.reserve(MemberFunctions.size());
	for (GraphFunction* function : MemberFunctions)
	{
		memberFunctionNames.push_back(function->ReadFunctionName());
	}
	config->SaveArray(DString::EmptyString, TXT("Functions"), memberFunctionNames);

	for (GraphVariable* var : MemberVariables)
	{
		if (var != nullptr)
		{
			var->SaveToConfig(config, var->ReadVariableName().ToString());
		}
	}

	for (GraphFunction* overrideFunc : OverrideFunctions)
	{
		overrideFunc->SaveToConfig(config, overrideFunc->ReadFunctionName());
	}

	for (GraphFunction* function : MemberFunctions)
	{
		function->SaveToConfig(config, function->ReadFunctionName());
	}

	std::vector<DString> overrideArray;
	for (const SVarOverride& varOverride : DefaultOverrides)
	{
		HashedString varHashedName(varOverride.VarName);

		//Find the variable this is going to override
		for (GraphAsset* otherAsset = SuperAsset.Get(); otherAsset != nullptr; otherAsset = otherAsset->SuperAsset.Get())
		{
			bool bFoundVar = false;

			for (GraphVariable* parentVar : otherAsset->ReadMemberVariables())
			{
				if (parentVar == nullptr)
				{
					continue;
				}

				if (parentVar->ReadVariableName() == varHashedName)
				{
					bFoundVar = true;

					//Create a graph variable copy since its type would know how to convert its data buffer to a string.
					GraphVariable varCpy(*parentVar);
					varCpy.SetDefaultValue(varOverride.NewValueData);
					DString configData = varCpy.DefaultValueToString();

					if (varOverride.bIsRelative)
					{
						configData = TXT("+") + configData;
					}

					overrideArray.push_back(varOverride.VarName + TXT("=") + configData);
				}
			}

			if (bFoundVar)
			{
				break;
			}
		}
	}
	config->SaveArray(DString::EmptyString, TXT("DefaultOverrides"), overrideArray);
	config->Destroy();

	return true;
}

bool GraphAsset::SaveInstanceData (std::vector<DString>& outVarData) const
{
	if (IsAssetDefinition())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save GraphAsset definition as an instance data. Instead only SaveToDevAsset can be used for GraphAsset definitions. Failed to save asset \"%s\"."), AssetName);
		return false;
	}
	CHECK(AssetDefinition != nullptr)

	for (GraphVariable* instanceValue : VarInstanceValues)
	{
		if (instanceValue == nullptr)
		{
			continue;
		}

		for (const GraphAsset* curAsset = AssetDefinition; curAsset != nullptr; curAsset = curAsset->GetSuperAsset())
		{
			DataBuffer defaultVal;
			bool saveVar = false;
			bool foundValue = false;

			//First check if the default overrides set the variable. Overrides take priority.
			for (const SVarOverride& overrideData : curAsset->DefaultOverrides)
			{
				HashedString overrideName(overrideData.VarName);
				if (overrideName == instanceValue->ReadVariableName())
				{
					foundValue = true;

					//If it's relevant, always save the data for this instance. Cheaper to do it this way than interpreting the data buffer as float/int then accumulate values.
					saveVar = overrideData.bIsRelative;
					overrideData.NewValueData.CopyBufferTo(OUT defaultVal);
					break;
				}
			}

			if (!foundValue)
			{
				for (GraphVariable* memVar : curAsset->MemberVariables)
				{
					if (memVar != nullptr && memVar->ReadVariableName() == instanceValue->ReadVariableName())
					{
						foundValue = true;
						memVar->ReadDefaultValue().CopyBufferTo(OUT defaultVal);
						break;
					}
				}
			}

			if (foundValue)
			{
				if (saveVar || defaultVal != instanceValue->ReadDefaultValue())
				{
					outVarData.emplace_back(DString::CreateFormattedString(TXT("%s=%s"), instanceValue->ReadVariableName().ToString(), instanceValue->DefaultValueToString()));					
				}

				break; //Stop climbing the inherited assets
			}
		}
	}

	return true;
}

EditableArray* GraphAsset::InitializeInheritedFunctionInspector (Entity* arrayOwner)
{
	if (arrayOwner == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot configure %s inherited functions without specifying the Owner instance the editable components would reside in."), ToString());
		return nullptr;
	}

	EditableArray* funcArray = EditableArray::CreateObject();
	if (arrayOwner->AddComponent(funcArray))
	{
		funcArray->SetLineHeight(32.f);
		if (LabelComponent* propName = funcArray->GetPropertyName())
		{
			propName->SetText(TXT("Inherted Functions"));
		}

		InitItemTemplateForInheritedFunctions(OUT funcArray->EditItemTemplate());

		funcArray->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleInheritedFunctionArrayEdit, void, EditPropertyComponent*);
	}

	return funcArray;
}

DString GraphAsset::GetPresentedName () const
{
	if (!DisplayedName.IsEmpty())
	{
		return DisplayedName;
	}

	return AssetName;
}

DString GraphAsset::GetFullAssetName () const
{
	DString path = SaveLocation.ToString();
	DString devAssetPath = Directory::DEV_ASSET_DIRECTORY.GetInnerMostDirectory();

	Int removeIdx = path.Find(devAssetPath, 0, DString::CC_IgnoreCase);
	if (removeIdx >= 0) //If the DevAssetPath is included.
	{
		removeIdx += devAssetPath.Length();
		path.Remove(0, removeIdx + 1); //plus one to include the last character of the dev asset path.

		if (!path.IsEmpty() && path.StartsWith(Directory::DIRECTORY_SEPARATOR, DString::CC_IgnoreCase))
		{
			path.Remove(0, 1); //Remove the directory separator between DevAsset path and the next path.
		}
	}

	path.ReplaceInline(Directory::DIRECTORY_SEPARATOR, TXT("."), DString::CC_CaseSensitive);
	if (!path.IsEmpty() && !path.EndsWith('.'))
	{
		path += '.'; //Ensure there's a trailing period to keep the path and AssetName separated.
	}

	return DString::CreateFormattedString(TXT("class\'%s%s\'"), path, AssetName);
}

bool GraphAsset::ParseFullAssetName (const DString& fullAssetName, DString& outPath, DString& outAssetName)
{
	DString regexStr;

	bool isPathGiven = (fullAssetName.Find(TXT("."), 6, DString::CC_CaseSensitive) > 0);
	size_t expectedMatches;

	if (isPathGiven)
	{
		/*
		class' - Matches the text "class'" literally
		Group 1: ([\w\W]*)
			Matches every character up until the last period. Everything (except for the last period) is stored in group 1.

		\. - Matches the period character literally.

		Group 2: ([\w\W]*)
			Matches the rest of the characters and stores it in group 2 up until the last quotation mark.
		' - Matches the single quotation mark character.
		*/
		regexStr = TXT("class'([\\w\\W]*)\\.([\\w\\W]*)'");
		expectedMatches = 3; //Always plus 1 because the first group is the entire match.
	}
	else //No path is given (eg: class'MyAsset')
	{
		regexStr = TXT("class'([\\w\\W]*)'"); //Similar to the other regex pattern but without Group 1.
		expectedMatches = 2;
	}

	std::regex searchPattern(regexStr.ToCString(), std::regex_constants::icase);
	std::smatch matches;
	std::string stdData = fullAssetName.ToStr();

	if (!std::regex_search(stdData, OUT matches, searchPattern) || matches.size() < expectedMatches)
	{
		if (isPathGiven)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to parse full asset name \"%s\" since that is not in a valid format. A valid format with path information is: class'Path.To.Asset'"), fullAssetName);
		}
		else
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to parse full asset name \"%s\" since that is not in a valid format. A valid format without path information is: class'MyAsset'"), fullAssetName);
		}
		
		return false;
	}

	//[0] is the whole match
	//[1] is either the path or the asset name (if the path is not given).
	//[2] only exists if the path information is given. If it does exist, then it's the asset name.
	if (isPathGiven)
	{
		outPath = matches[1].str();
		outAssetName = matches[2].str();
	}
	else
	{
		outPath = DString::EmptyString;
		outAssetName = matches[1].str();
	}

	return true;
}

bool GraphAsset::IsChildOf (const GraphAsset* otherAsset) const
{
	return otherAsset->IsParentOf(this);
}

bool GraphAsset::IsParentOf (const GraphAsset* otherAsset) const
{
	for (const GraphAsset* curAsset = otherAsset; curAsset != nullptr; curAsset = curAsset->GetSuperAsset())
	{
		if (curAsset == this)
		{
			return true;
		}
	}

	return false;
}

GraphAsset* GraphAsset::FindMemberVar (const DString& varName, bool checkInheritedVars, GraphVariable& outVar)
{
	GraphAsset* curAsset = this;

	while (curAsset != nullptr)
	{
		for (GraphVariable* var : curAsset->ReadMemberVariables())
		{
			if (var != nullptr && var->GetIdentifyingName().Compare(varName, DString::CC_CaseSensitive) == 0)
			{
				outVar = *var;
				return curAsset;
			}
		}

		if (!checkInheritedVars)
		{
			break;
		}

		curAsset = curAsset->GetSuperAsset();
	}

	return nullptr;
}


void GraphAsset::RefreshClassList (DropdownComponent* classDropdown, GraphAsset* forbiddenClass)
{
	CHECK(classDropdown != nullptr)

	ListBoxComponent* expandList = classDropdown->GetExpandMenuList();
	CHECK(expandList != nullptr)

	//Need to populate the dropdown component with a list of classes.
	std::vector<GuiDataElement<GraphAsset*>> classList;

	//Always begin with a null class.
	classList.emplace_back(nullptr, NULL_CLASS_NAME);

	//Need to obtain assets from the catalogue. These assets are probably not yet loaded.
	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	for (const std::pair<HashedString, GraphAsset*>& asset : editorEngine->ReadLoadedAssets())
	{
		bool isForbidden = false;
		for (GraphAsset* curAsset = asset.second; curAsset != nullptr; curAsset = curAsset->GetSuperAsset())
		{
			if (forbiddenClass == curAsset)
			{
				isForbidden = true;
				break;
			}
		}

		if (isForbidden)
		{
			continue;
		}

		classList.emplace_back(asset.second, asset.second->GetPresentedName());
	}

	if (classList.size() > 2)
	{
		//Sort alphabetically
		std::sort(classList.begin() + 1, classList.end(), [](const GuiDataElement<GraphAsset*>& a, const GuiDataElement<GraphAsset*>& b) //Reason for +1 is to skip the None element.
		{
			return (a.ReadLabelText().Compare(b.ReadLabelText(), DString::CC_CaseSensitive) < 0);
		});
	}

	expandList->SetList(classList); //SetList will clear the previous list
}

void GraphAsset::SetDisplayedName (const DString& newDisplayedName)
{
	DisplayedName = newDisplayedName;
	UpdateEditorObjName();
}

void GraphAsset::SetAssetType (const DString& newAssetType)
{
	AssetType = newAssetType;
}

void GraphAsset::SetGlobal (bool newGlobal)
{
	bGlobal = newGlobal;
}

void GraphAsset::SetLoadState (ELoadState newLoadState)
{
	if (LoadState == newLoadState)
	{
		return;
	}

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	if (LoadState != LS_NotLoaded)
	{
		editorEngine->RemoveAsset(this);
	}

	LoadState = newLoadState;

	if (LoadState != LS_NotLoaded)
	{
		editorEngine->RegisterAsset(this);
	}
}

void GraphAsset::SetSaveLocation (const Directory& newSaveLocation)
{
	SaveLocation = newSaveLocation;
}

void GraphAsset::SetScriptClass (ContentClass* newScriptClass)
{
	ScriptClass = newScriptClass;
}

void GraphAsset::SetSuperAsset (GraphAsset* newSuperAsset)
{
	for (GraphAsset* curAsset = newSuperAsset; curAsset != nullptr; curAsset = curAsset->SuperAsset.Get())
	{
		if (curAsset == this)
		{
			EditorLog.Log(LogCategory::LL_Critical, TXT("Infinite loop detected! Cannot set the super asset for (%s) to (%s) since (%s) is currently the super asset of (%s)."), ToString(), newSuperAsset->ToString(), ToString(), newSuperAsset->ToString());
			return;
		}
	}

	SuperAsset = newSuperAsset;
}

void GraphAsset::SetAbstract (bool newAbstract)
{
	bAbstract = newAbstract;
	if (ScriptClass != nullptr)
	{
		ScriptClass->SetIsAbstract(bAbstract);
	}
}

void GraphAsset::SetDefaultFunctionSize (const Vector2& newDefaultFunctionSize)
{
	DefaultFunctionSize = newDefaultFunctionSize;
}

void GraphAsset::InitItemTemplateForInheritedFunctions (InspectorList& outList)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString fileName = TXT("Editor");
	DString sectionName = TXT("GraphFunction");

	outList.PushStruct(new EditableStruct::SInspectorStruct(TXT("Inherited Function"), DString::EmptyString));
	{
		if (EditableEnum::SInspectorEnum* editEnum = outList.AddProp(new EditableEnum::SInspectorEnum(TXT("Parent Function"), translator->TranslateText(TXT("ParentFunctionTooltip"), fileName, sectionName), SDFUNCTION_1PARAM(this, GraphAsset, HandlePopulateInheritedFunctions, void, DropdownComponent*))))
		{
			editEnum->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphAsset, HandleParentFunctionSelected, void, EditPropertyComponent*);
			editEnum->PropertyId = GraphFunction::FA_DeriveFrom;
		}

		if (EditablePropCommand::SInspectorPropCmd* propCmd = outList.AddProp(new EditablePropCommand::SInspectorPropCmd(TXT("Open Function"), DString::EmptyString, SDFUNCTION_2PARAM(this, GraphAsset, HandleOpenInheritedFunction, void, ButtonComponent*, EditablePropCommand*))))
		{
			propCmd->PropertyId = GraphFunction::FA_Open;
		}

		if (EditableBool::SInspectorBool* editedBool = outList.AddProp(new EditableBool::SInspectorBool(TXT("Static"), translator->TranslateText(TXT("Static"), fileName, sectionName), nullptr, false)))
		{
			editedBool->bReadOnly = true;
			editedBool->PropertyId = GraphFunction::FA_Static;
		}

		if (EditableBool::SInspectorBool* editedBool = outList.AddProp(new EditableBool::SInspectorBool(TXT("Const"), translator->TranslateText(TXT("Const"), fileName, sectionName), nullptr, false)))
		{
			editedBool->bReadOnly = true;
			editedBool->PropertyId = GraphFunction::FA_Const;
		}

		if (EditableBool::SInspectorBool* editedBool = outList.AddProp(new EditableBool::SInspectorBool(TXT("Protected"), translator->TranslateText(TXT("Protected"), fileName, sectionName), nullptr, true)))
		{
			editedBool->bReadOnly = true;
			editedBool->PropertyId = GraphFunction::FA_Protected;
		}

		if (EditableDString::SInspectorString* editedStr = outList.AddProp(new EditableDString::SInspectorString(TXT("Comment"), DString::EmptyString, nullptr)))
		{
			editedStr->bReadOnly = true;
			editedStr->PropertyId = GraphFunction::FA_Comment;
		}
	}
	outList.PopStruct();
}

bool GraphAsset::ProcessLoadEssentials (ConfigWriter* config)
{
	CHECK(LoadState == LS_NotLoaded)

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	if (editorEngine->ReadLoadedAssets().contains(HashedString(AssetName)))
	{
		CHECK(false) //This should never happen since this asset's load state is not loaded and yet it's registered to the editor engine component. Only loaded or partial loaded assets should be registered to the engine component.
		EditorLog.Log(LogCategory::LL_Warning, TXT("%s is already loaded."), AssetName);
		return true; //It's fine to continue since this asset is already loaded.
	}

	PendingLoadAssets.push_back(AssetName);

	InitializeAsset(AssetName, true);

	DString fullSuperAsset = config->GetPropertyText(DString::EmptyString, TXT("SuperClass"));
	if (!fullSuperAsset.IsEmpty())
	{
		DString parentName;
		DString assetPath;
		if (!ParseFullAssetName(fullSuperAsset, OUT assetPath, OUT parentName))
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load GraphAsset %s since its super class %s is not in a valid format."), AssetName, fullSuperAsset);
			PendingLoadAssets.pop_back();
			return false;
		}

		HashedString parentAssetHash(parentName);
		if (editorEngine->ReadLoadedAssets().contains(parentAssetHash))
		{
			SuperAsset = editorEngine->ReadLoadedAssets().at(parentAssetHash);
		}
		else
		{
			SuperAsset = GraphAsset::CreateObject();
			SuperAsset->InitializeAsset(parentName, true);

			assetPath.ReplaceInline(TXT("."), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
			SuperAsset->SetSaveLocation(Directory::DEV_ASSET_DIRECTORY / assetPath);

			//Parent asset doesn't exist yet. Recursively load the class chain.
			bool bParentLoaded = false;
			if (config->IsReadingEverything())
			{
				bParentLoaded = SuperAsset->LoadFromFile();
			}
			else
			{
				bParentLoaded = SuperAsset->LoadEssentials();
			}

			if (!bParentLoaded)
			{
				EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load GraphAsset %s since it failed to load its super asset %s."), AssetName, parentName);
				PendingLoadAssets.pop_back();
				SuperAsset->Destroy();
				return false;
			}

			//At this point, it's safe to assume that the super asset is stored to the engine component.
			CHECK(editorEngine->ReadLoadedAssets().contains(parentAssetHash))
		}
	}

	if (!config->ContainsProperty(DString::EmptyString, ASSET_TYPE))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Error when loading GraphAsset %s. An AssetType is not specified. This will assume the asset is a global asset."), AssetName);
	}

	DString assetType = config->GetPropertyText(DString::EmptyString, ASSET_TYPE);
	bGlobal = assetType.IsEmpty();
	SetAssetType(assetType);

	//Skipping meta variables like abstract and license is intended since we're just loading essentials in this function.

	CHECK(ContainerUtils::IsEmpty(MemberVariables));
	std::vector<DString> varNames;
	config->GetArrayValues(DString::EmptyString, TXT("MemberVariables"), OUT varNames);
	MemberVariables.reserve(varNames.size());
	for (const DString& varName : varNames)
	{
		GraphVariable* newMemVar = new GraphVariable(varName, this);
		newMemVar->SetInspectorType(GraphVariable::IT_MemberVariable);
		newMemVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemVarDeleted, void, EditorInterface*));
		MemberVariables.push_back(newMemVar);
	}

	CHECK(ContainerUtils::IsEmpty(OverrideFunctions))
	std::vector<DString> overrideFunctionNames;
	config->GetArrayValues(DString::EmptyString, TXT("OverrideFunctions"), OUT overrideFunctionNames);
	OverrideFunctions.reserve(overrideFunctionNames.size());
	for (const DString& overrideFunctionName : overrideFunctionNames)
	{
		GraphFunction* newFunc = GraphFunction::CreateObject();
		newFunc->SetFunctionName(overrideFunctionName);
		newFunc->SetOwningAsset(this);
		newFunc->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleOverrideFunctionDeleted, void, EditorInterface*));
		OverrideFunctions.push_back(newFunc);
	}

	CHECK(ContainerUtils::IsEmpty(MemberFunctions))
	std::vector<DString> functionNames;
	config->GetArrayValues(DString::EmptyString, TXT("Functions"), OUT functionNames);
	MemberFunctions.reserve(functionNames.size());
	for (const DString& functionName : functionNames)
	{
		GraphFunction* newFunc = GraphFunction::CreateObject();
		newFunc->SetFunctionName(functionName);
		newFunc->SetOwningAsset(this);
		newFunc->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemFunctionDeleted, void, EditorInterface*));
		MemberFunctions.push_back(newFunc);
	}

	LoadState = LS_Partial;
	editorEngine->RegisterAsset(this);
	PendingLoadAssets.pop_back();
	return true;
}

bool GraphAsset::ProcessLoadEverything (ConfigWriter* config)
{
	CHECK_INFO(LoadState == LS_Partial, "Invoke ProcessLoadEssentials before loading the rest of the contents.")
	if (LoadState != LS_Partial)
	{
		return false;
	}

	License = config->GetPropertyText(DString::EmptyString, TXT("License"));
	Authors = config->GetPropertyText(DString::EmptyString, TXT("Authors"));
	Comment = config->GetPropertyText(DString::EmptyString, TXT("Comment"));
	
	SetAbstract(config->GetProperty<Bool>(DString::EmptyString, TXT("IsAbstract")));

	for (GraphVariable* var : MemberVariables)
	{
		if (var != nullptr)
		{
			DString sectionName = var->ReadVariableName().ToString();
			var->LoadFromConfig(config, sectionName);
		}
	}

	for (GraphFunction* overrideFunction : OverrideFunctions)
	{
		CHECK(overrideFunction != nullptr)
		overrideFunction->LoadFromConfig(config, overrideFunction->ReadFunctionName());
	}

	for (GraphFunction* function : MemberFunctions)
	{
		CHECK(function != nullptr)
		function->LoadFromConfig(config, function->ReadFunctionName());
	}

	std::vector<DString> overrideArray;
	config->GetArrayValues(DString::EmptyString, TXT("DefaultOverrides"), OUT overrideArray);
	ContainerUtils::Empty(OUT DefaultOverrides);
	for (const DString& defaultOverride : overrideArray)
	{
		Int equalIdx = defaultOverride.Find(TXT("="), 0, DString::CC_CaseSensitive);
		if (equalIdx <= 0)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to extract DefaultOverride from config file since \"%s\" is not a valid entry. Expected format is \"VarName=VarValue\""), defaultOverride);
			continue;
		}

		SVarOverride& newOverride = DefaultOverrides.emplace_back();

		newOverride.VarName = defaultOverride.SubString(0, equalIdx - 1);
		DString varValue = defaultOverride.SubString(equalIdx + 1);
		newOverride.bIsRelative = varValue.StartsWith('+');
		if (newOverride.bIsRelative)
		{
			varValue.Remove(0); //remove the plus prefix
		}

		HashedString hashedVarName(newOverride.VarName);
		
		//Check if this variable exists
		for (GraphAsset* otherAsset = SuperAsset.Get(); otherAsset != nullptr; otherAsset = otherAsset->GetSuperAsset())
		{
			bool bFoundVar = false;

			for (GraphVariable* otherVar : otherAsset->ReadMemberVariables())
			{
				if (otherVar != nullptr && otherVar->ReadVariableName() == hashedVarName)
				{
					bFoundVar = true;

					//Create a variable copy since its type knows how to translate a string to a data buffer
					GraphVariable varCpy(*otherVar);
					varCpy.SetDefaultValueFromString(varValue);
					varCpy.ReadDefaultValue().CopyBufferTo(OUT newOverride.NewValueData);
					break;
				}
			}

			if (bFoundVar)
			{
				break;
			}
		}
	}

	LoadState = LS_Loaded;
	return true;
}

void GraphAsset::CopyVarsFrom (GraphAsset* cpyFrom)
{
	ContainerUtils::Empty(OUT VarInstanceValues);
	for (GraphAsset* curAsset = cpyFrom; curAsset != nullptr; curAsset = curAsset->GetSuperAsset())
	{
		for (GraphVariable* memVar : curAsset->MemberVariables)
		{
			if (memVar != nullptr)
			{
				GraphVariable* newVarCpy = new GraphVariable(*memVar);
				newVarCpy->SetVarOwner(this);
				newVarCpy->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleVarInstanceDeleted, void, EditorInterface*));
				VarInstanceValues.push_back(newVarCpy);
			}
		}
	}
}

void GraphAsset::SyncVarArray (EditableArray* editArray)
{
	CHECK(editArray != nullptr)
	ContainerUtils::Empty(OUT MemberVariables);

	for (size_t i = 0; i < editArray->ReadSubPropertyComponents().size(); ++i)
	{
		EditableStruct* editStruct = dynamic_cast<EditableStruct*>(editArray->ReadSubPropertyComponents().at(i));
		if (editStruct == nullptr)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to sync %s to the MemberVariables array since the EditableArray element at index %s is not an EditableStruct."), editArray->GetPropertyNameStr(), Int(i));
			continue;
		}

		GraphVariable* newVar = new GraphVariable(DString::EmptyString, this);
		newVar->SetInspectorType(GraphVariable::IT_MemberVariable);
		newVar->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemVarDeleted, void, EditorInterface*));
		MemberVariables.push_back(newVar);
	}
}

void GraphAsset::SyncInheritedFunctionArray (EditableArray* editArray)
{
	CHECK(editArray != nullptr)

	std::vector<SDefinitionTransfer> definitions;

	for (GraphFunction* oldFunc : OverrideFunctions)
	{
		if (oldFunc != nullptr)
		{
			definitions.emplace_back(oldFunc->GetIdentifyingName(), oldFunc->GetFunctionDefinition());
			oldFunc->SetFunctionDefinition(nullptr);
			oldFunc->Destroy();
		}
	}
	ContainerUtils::Empty(OUT OverrideFunctions);

	for (EditPropertyComponent* subProp : editArray->ReadSubPropertyComponents())
	{
		EditableStruct* editStruct = dynamic_cast<EditableStruct*>(subProp);
		GraphFunction* superFunction = nullptr;

		if (EditableEnum* editEnum = dynamic_cast<EditableEnum*>(editStruct->FindSubPropWithMatchingId(GraphFunction::FA_DeriveFrom, false)))
		{
			if (DropdownComponent* dropdown = editEnum->GetDropdown())
			{
				if (dropdown->IsItemSelected())
				{
					superFunction = dropdown->GetSelectedItem<GraphFunction*>();
				}
			}
		}

		if (superFunction != nullptr)
		{
			GraphFunction* newFunc = GraphFunction::CreateObject();
			OverrideFunctions.push_back(newFunc);
			newFunc->SetSuperFunction(superFunction);
			newFunc->SetOwningAsset(this);
			newFunc->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleOverrideFunctionDeleted, void, EditorInterface*));

			DString funcId = newFunc->GetIdentifyingName();
			bool definitionTransferred = false;
			for (size_t i = 0; i < definitions.size(); ++i)
			{
				if (definitions.at(i).FunctionId.Compare(funcId, DString::CC_CaseSensitive) == 0)
				{
					newFunc->SetFunctionDefinition(definitions.at(i).FuncDefinition);
					definitions.erase(definitions.begin() + i);
					definitionTransferred = true;
					break;
				}
			}

			if (!definitionTransferred)
			{
				newFunc->CreateBlankDefinition(DefaultFunctionSize);
			}

			SyncInheritedFunctionArrayElement(subProp, OverrideFunctions.size() - 1, false);
		}
	}
}

void GraphAsset::SyncInheritedFunctionArrayElement (EditPropertyComponent* arrayElement, size_t funcIdx, bool syncDropdown)
{
	CHECK(arrayElement != nullptr && ContainerUtils::IsValidIndex(OverrideFunctions, funcIdx))

	if (syncDropdown)
	{
		if (EditableEnum* editEnum = dynamic_cast<EditableEnum*>(arrayElement->FindSubPropWithMatchingId(GraphFunction::FA_DeriveFrom, true)))
		{
			if (DropdownComponent* dropdown = editEnum->GetDropdown())
			{
				if (ListBoxComponent* options = dropdown->GetExpandMenuList())
				{
					//Little messy. It's comparing all options in the dropdown until it finds the item that matches the function name.
					for (size_t optionIdx = 0; optionIdx < options->ReadList().size(); ++optionIdx)
					{
						if (options->ReadList().at(optionIdx)->ReadLabelText().Compare(OverrideFunctions.at(funcIdx)->ReadFunctionName(), DString::CC_CaseSensitive) == 0)
						{
							editEnum->SetDropdownSelectIdx(optionIdx, false);
							break;
						}
					}
				}
			}
		}
	}

	if (EditableBool* editedBool = dynamic_cast<EditableBool*>(arrayElement->FindSubPropWithMatchingId(GraphFunction::FA_Static, true)))
	{
		editedBool->SetBoolValue(OverrideFunctions.at(funcIdx)->IsStatic(), false);
	}

	if (EditableBool* editedBool = dynamic_cast<EditableBool*>(arrayElement->FindSubPropWithMatchingId(GraphFunction::FA_Const, true)))
	{
		editedBool->SetBoolValue(OverrideFunctions.at(funcIdx)->IsConst(), false);
	}

	if (EditableBool* editedBool = dynamic_cast<EditableBool*>(arrayElement->FindSubPropWithMatchingId(GraphFunction::FA_Protected, true)))
	{
		editedBool->SetBoolValue(OverrideFunctions.at(funcIdx)->IsProtected(), false);
	}

	if (EditableDString* editedStr = dynamic_cast<EditableDString*>(arrayElement->FindSubPropWithMatchingId(GraphFunction::FA_Comment, true)))
	{
		editedStr->SetPropValue(OverrideFunctions.at(funcIdx)->ReadComment(), false);
	}
}

void GraphAsset::SyncFunctionArray (EditableArray* editArray)
{
	CHECK(editArray != nullptr)

	std::vector<SDefinitionTransfer> definitions;
	for (GraphFunction* func : MemberFunctions)
	{
		if (func != nullptr)
		{
			definitions.emplace_back(func->GetIdentifyingName(), func->GetFunctionDefinition());
			func->SetFunctionDefinition(nullptr);
			func->Destroy();
		}
	}
	ContainerUtils::Empty(OUT MemberFunctions);

	for (EditPropertyComponent* subProp : editArray->ReadSubPropertyComponents())
	{
		EditableStruct* editStruct = dynamic_cast<EditableStruct*>(subProp);
		if (editStruct == nullptr)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to sync %s to the MemberFunctions array since the EditableArray element is not an EditableStruct."), editArray->GetPropertyNameStr());
			continue;
		}

		GraphFunction* newFunc = GraphFunction::CreateObject();
		newFunc->SetOwningAsset(this);
		newFunc->OnClearEditorReference.RegisterHandler(SDFUNCTION_1PARAM(this, GraphAsset, HandleMemFunctionDeleted, void, EditorInterface*));
		MemberFunctions.push_back(newFunc);

		DString funcId = newFunc->GetIdentifyingName();
		bool foundDefinition = false;
		for (size_t i = 0; i < definitions.size(); ++i)
		{
			if (definitions.at(i).FunctionId.Compare(funcId, DString::CC_CaseSensitive) == 0)
			{
				newFunc->SetFunctionDefinition(definitions.at(i).FuncDefinition);
				definitions.erase(definitions.begin() + i);
				foundDefinition = true;
				break;
			}
		}

		if (!foundDefinition)
		{
			newFunc->CreateBlankDefinition(DefaultFunctionSize);
		}
		else
		{
			GraphFunction::OnParamChanged.Broadcast(funcId);
		}
	}
}

void GraphAsset::SyncDefaultOverrideArray (EditableArray* editArray)
{
	CHECK(editArray != nullptr)

	ContainerUtils::Empty(OUT DefaultOverrides);

	for (EditPropertyComponent* subProp : editArray->ReadSubPropertyComponents())
	{
		EditableStruct* editStruct = dynamic_cast<EditableStruct*>(subProp);
		if (editStruct == nullptr)
		{
			EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to sync %s to the DefaultOverrides array since the EditableArray element is not an EditableStruct."), editArray->GetPropertyNameStr());
			continue;
		}

		SVarOverride& newOverride = DefaultOverrides.emplace_back();
		if (EditableEnum* editVar = dynamic_cast<EditableEnum*>(subProp->FindSubPropWithMatchingId(GraphVariable::VA_VarName, false)))
		{
			if (DropdownComponent* dropdown = editVar->GetDropdown())
			{
				if (dropdown->IsItemSelected())
				{
					newOverride.VarName = dropdown->GetSelectedGuiDataElement<ScriptVariable::EVarType>()->ReadLabelText();
				}
			}
		}

		if (EditableBool* editRelative = dynamic_cast<EditableBool*>(subProp->FindSubPropWithMatchingId(GraphVariable::VA_Relative, false)))
		{
			if (CheckboxComponent* checkbox = editRelative->GetCheckbox())
			{
				newOverride.bIsRelative = checkbox->IsChecked();
			}
		}

		if (EditPropertyComponent* editDefault = subProp->FindSubPropWithMatchingId(GraphVariable::VA_DefaultValue, false))
		{
			editDefault->CopyToBuffer(OUT newOverride.NewValueData);
		}
	}
}

void GraphAsset::FindAllSubclassAssets (std::vector<GraphAsset*>& outAssets) const
{
	if (EditorEngineComponent* editorEngine = EditorEngineComponent::Find())
	{
		for (const std::pair<HashedString, GraphAsset*>& loadedAsset : editorEngine->ReadLoadedAssets())
		{
			if (loadedAsset.second->IsChildOf(this) || loadedAsset.second == this)
			{
				outAssets.push_back(loadedAsset.second);
			}
		}
	}
}

GraphVariable* GraphAsset::FindEditedVar (EditPropertyComponent* editComp)
{
	EditPropertyComponent* prevComp = nullptr;
	for (EditPropertyComponent* curComp = editComp; curComp != nullptr; curComp = dynamic_cast<EditPropertyComponent*>(curComp->GetOwner()))
	{
		if (curComp->GetPropertyId() == AA_MemberVariables && prevComp != nullptr)
		{
			size_t varIdx = prevComp->FindSelfFromOwnerSubProperties();
			CHECK(ContainerUtils::IsValidIndex(MemberFunctions, varIdx))
			return MemberVariables.at(varIdx);
		}

		prevComp = curComp;
	}

	return nullptr;
}

GraphFunction* GraphAsset::FindEditedFunction (EditPropertyComponent* editComp)
{
	EditPropertyComponent* prevComp = nullptr;
	for (EditPropertyComponent* curComp = editComp; curComp != nullptr; curComp = dynamic_cast<EditPropertyComponent*>(curComp->GetOwner()))
	{
		if (curComp->GetPropertyId() == AA_Functions && prevComp != nullptr)
		{
			size_t funcIdx = prevComp->FindSelfFromOwnerSubProperties();
			CHECK(ContainerUtils::IsValidIndex(MemberFunctions, funcIdx))
			return MemberFunctions.at(funcIdx);
		}

		prevComp = curComp;
	}

	return nullptr;
}

void GraphAsset::CheckForNameConflict (EditableField* editField)
{
	DString varName = editField->GetPropValue();
	GraphVariable tmpVar(varName, this);

	DString errorMsg;
	if (GraphCompiler::CheckVariableName(tmpVar, OUT errorMsg, {&MemberVariables}) != GraphCompiler::CR_Success)
	{
		editField->ShowCaption(errorMsg, editField->ReadCaptionErrorColors());
	}
	else
	{
		editField->HideCaption();
	}
}

void GraphAsset::HandleInitializeMemberVars (EditableArray* editArray)
{
	editArray->SetArraySize(MemberVariables.size());
	CHECK(editArray->ReadSubPropertyComponents().size() == MemberVariables.size())

	for (size_t varIdx = 0; varIdx < MemberVariables.size(); ++varIdx)
	{
		if (EditableObjectComponent* arrayElement = dynamic_cast<EditableObjectComponent*>(editArray->ReadSubPropertyComponents().at(varIdx)))
		{
			ObjBinder<GraphVariable>* varBinder = new ObjBinder<GraphVariable>(&MemberVariables.at(varIdx));
			arrayElement->BindVariable(varBinder);

			arrayElement->PostInitPropertyComponent();
		}
	}
}

void GraphAsset::HandleInitializeInheritedFunctions (EditableArray* editArray)
{
	editArray->SetArraySize(OverrideFunctions.size());
	CHECK(editArray->ReadSubPropertyComponents().size() == OverrideFunctions.size())

	for (size_t funcIdx = 0; funcIdx < OverrideFunctions.size(); ++funcIdx)
	{
		if (EditPropertyComponent* arrayElement = editArray->ReadSubPropertyComponents().at(funcIdx))
		{
			SyncInheritedFunctionArrayElement(arrayElement, funcIdx, true);
			arrayElement->PostInitPropertyComponent();
		}
	}
}

void GraphAsset::HandleInitializeVarOverrides (EditableArray* editArray)
{
	editArray->SetArraySize(DefaultOverrides.size());
	CHECK(editArray->ReadSubPropertyComponents().size() == DefaultOverrides.size())

	for (size_t varIdx = 0; varIdx < DefaultOverrides.size(); ++varIdx)
	{
		SVarOverride& varOverride = DefaultOverrides.at(varIdx);
		if (EditPropertyComponent* arrayElement = editArray->ReadSubPropertyComponents().at(varIdx))
		{
			bool supportsRelative = false;
			bool isVarEditable = false;
			if (EditableEnum* editEnum = dynamic_cast<EditableEnum*>(arrayElement->FindSubPropWithMatchingId(GraphVariable::VA_VarName, true)))
			{
				if (DropdownComponent* dropdown = editEnum->GetDropdown())
				{
					if (ListBoxComponent* expandList = dropdown->GetExpandMenuList())
					{
						for (size_t expandIdx = 0; expandIdx < expandList->ReadList().size(); ++expandIdx)
						{
							if (expandList->ReadList().at(expandIdx)->ReadLabelText().Compare(varOverride.VarName, DString::CC_CaseSensitive) == 0)
							{
								if (GuiDataElement<ScriptVariable::EVarType>* varType = dynamic_cast<GuiDataElement<ScriptVariable::EVarType>*>(expandList->ReadList().at(expandIdx)))
								{
									supportsRelative = GraphVariable::IsTypeSupportingRelative(varType->Data);
									isVarEditable = GraphVariable::CanTypeBeConfiguredInEditor(varType->Data);
								}

								editEnum->SetDropdownSelectIdx(Int(expandIdx), false);
								GraphVariable::UpdateDefaultValueComp(editEnum);
								break;
							}
						}
					}
				}
			}

			if (EditableBool* isRelative = dynamic_cast<EditableBool*>(arrayElement->FindSubPropWithMatchingId(GraphVariable::VA_Relative, true)))
			{
				isRelative->SetReadOnly(!supportsRelative);
				if (CheckboxComponent* checkbox = isRelative->GetCheckbox())
				{
					checkbox->SetChecked(varOverride.bIsRelative);
				}
			}

			if (EditPropertyComponent* defaultVal = arrayElement->FindSubPropWithMatchingId(GraphVariable::VA_DefaultValue, true))
			{
				varOverride.NewValueData.JumpToBeginning();
				defaultVal->CopyFromBuffer(varOverride.NewValueData);
				defaultVal->SetReadOnly(!isVarEditable);
			}

			arrayElement->PostInitPropertyComponent();
		}
	}
}

void GraphAsset::HandleAssetNameEdit (EditPropertyComponent* editedComp)
{
	DString newAssetName;
	if (EditableDString* editedStr = dynamic_cast<EditableDString*>(editedComp))
	{
		editedStr->GetPropValue(newAssetName);
	}

	if (newAssetName.IsEmpty() || newAssetName.Compare(AssetName, DString::CC_CaseSensitive) == 0)
	{
		return;
	}

	//Check for name collisions
	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	for (const std::pair<HashedString, GraphAsset*>& scriptClass : editorEngine->ReadLoadedAssets())
	{
		if (scriptClass.second == this)
		{
			continue;
		}

		if (scriptClass.second->ReadAssetName().Compare(newAssetName, DString::CC_CaseSensitive) == 0)
		{
			editedComp->ShowCaption(TXT("\"%s\" is already in use."), editedComp->GetCaptionErrorColors());
			return;
		}
	}

	editedComp->HideCaption();

	DString oldFullName = GetFullAssetName();
	InitializeAsset(newAssetName, true);

	if (OnAssetNameChanged.IsBounded())
	{
		OnAssetNameChanged(this, oldFullName, GetFullAssetName());
	}
}

void GraphAsset::HandlePopulateClassList (DropdownComponent* dropdown)
{
	RefreshClassList(dropdown, this);

	//Need to select the dropdown element that matches the current class
	if (SuperAsset != nullptr && dropdown != nullptr)
	{
		if (ListBoxComponent* list = dropdown->GetExpandMenuList())
		{
			HashedString superClassName(SuperAsset->ReadAssetName());
			list->SetSelectedItem(superClassName);
		}
	}
}

void GraphAsset::HandleSelectParentClass (EditPropertyComponent* editedComp)
{
	SuperAsset = nullptr;

	if (EditableEnum* editedEnum = dynamic_cast<EditableEnum*>(editedComp))
	{
		if (DropdownComponent* dropdown = editedEnum->GetDropdown())
		{
			if (dropdown->IsItemSelected())
			{
				SuperAsset = dropdown->GetSelectedItem<GraphAsset*>();
				if (SuperAsset != nullptr && SuperAsset->GetLoadState() != LS_Loaded)
				{
					SuperAsset->LoadFromFile();
				}
			}
		}
	}
}

void GraphAsset::HandleToggleGlobal (EditPropertyComponent* editedComp)
{
	if (EditableBool* editBool = dynamic_cast<EditableBool*>(editedComp))
	{
		if (CheckboxComponent* checkbox = editBool->GetCheckbox())
		{
			if (checkbox->IsChecked())
			{
				TextTranslator* translator = TextTranslator::GetTranslator();
				CHECK(translator != nullptr)
				const DString& fileName = EditorEngineComponent::LOCALIZATION_FILE_NAME;
				DString sectionName = TXT("GraphAsset");

				//Ensure the user is allowed to check this component.
				//Global assets cannot have member variables.
				if (!ContainerUtils::IsEmpty(MemberVariables))
				{
					editedComp->ShowCaption(translator->TranslateText(TXT("ErrorGlobalCantHaveVar"), fileName, sectionName), editedComp->ReadCaptionErrorColors());
					bGlobal = false;
					editBool->SetBoolValue(false, false);
					return;
				}

				//Global assets require all of its functions to be also global.
				for (GraphFunction* func : MemberFunctions)
				{
					if (func != nullptr && !func->IsGlobal())
					{
						editedComp->ShowCaption(translator->TranslateText(TXT("ErrorGlobalFoundInvalidFunction"), fileName, sectionName), editedComp->ReadCaptionErrorColors());
						bGlobal = false;
						editBool->SetBoolValue(false, false);
						return;
					}
				}
			}

			editedComp->HideCaption();
			bGlobal = checkbox->IsChecked();
		}
	}
}

void GraphAsset::HandleToggleAbstract (EditPropertyComponent* editedComp)
{
	if (EditableBool* editBool = dynamic_cast<EditableBool*>(editedComp))
	{
		if (CheckboxComponent* checkbox = editBool->GetCheckbox())
		{
			SetAbstract(checkbox->IsChecked());
		}
	}
}

void GraphAsset::HandlePopulateOverrideVariables (DropdownComponent* dropdown)
{
	//Don't point to the variable, itself. The GraphVariables can be purged at anytime. Instead point to them via string. The compiler will match them up.
	//Especially because the member variable array flushes every time it syncs with the EditableArray.
	std::vector<GuiDataElement<ScriptVariable::EVarType>> allVars;

	std::vector<GraphAsset*> hierarchyChain;
	for (GraphAsset* curClass = SuperAsset.Get(); curClass != nullptr; curClass = curClass->SuperAsset.Get())
	{
		hierarchyChain.push_back(curClass);
	}

	if (!ContainerUtils::IsEmpty(hierarchyChain))
	{
		//Traverse backwards to have the root-most variables listed first in the order they are defined in those assets.
		size_t classIdx = hierarchyChain.size() - 1;
		while (true)
		{
			for (GraphVariable* memVar : hierarchyChain.at(classIdx)->MemberVariables)
			{
				if (memVar != nullptr)
				{
					allVars.emplace_back(memVar->GetVarType(), memVar->ReadVariableName().ToString());
				}
			}

			if (classIdx == 0)
			{
				break;
			}
			classIdx--;
		}
	}

	if (ListBoxComponent* expandMenu = dropdown->GetExpandMenuList())
	{
		expandMenu->SetList(allVars);
	}
}

void GraphAsset::HandleSelectVariableOverride (EditPropertyComponent* editedComp)
{
	CHECK(editedComp != nullptr)
	EditableEnum* editedEnum = dynamic_cast<EditableEnum*>(editedComp);
	if (editedEnum == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to apply variable override since the component %s is not an EditableEnum."), editedComp->GetPropertyNameStr());
		return;
	}

	EditableStruct* owningStruct = dynamic_cast<EditableStruct*>(editedEnum->GetOwner());
	if (owningStruct == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to apply variable override since the owning component of the EditableEnum is not an EditableStruct."));
		return;
	}

	//First find the variable with the matching name.
	HashedString editedVarName;
	if (DropdownComponent* dropdown = editedEnum->GetDropdown())
	{
		if (dropdown->IsItemSelected())
		{
			const DString& varNameStr = dropdown->GetSelectedGuiDataElement<ScriptVariable::EVarType>()->ReadLabelText();
			editedVarName = HashedString(varNameStr);
		}
	}

	if (editedVarName.GetHash() == 0)
	{
		return;
	}
	
	GraphVariable* associatedParentVar = nullptr;
	for (GraphAsset* curParent = SuperAsset.Get(); curParent != nullptr; curParent = curParent->SuperAsset.Get())
	{
		for (GraphVariable* memVar : curParent->MemberVariables)
		{
			if (memVar != nullptr && memVar->ReadVariableName() == editedVarName)
			{
				associatedParentVar = memVar;
				break;
			}
		}
	}

	if (associatedParentVar == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to process variable override since %s is not found in the class chain."), editedVarName.ToString());
		return;
	}

	//Check if IsRelative should be read only.
	if (EditableBool* editRelative = dynamic_cast<EditableBool*>(owningStruct->FindSubPropWithMatchingId(GraphVariable::VA_Relative, false)))
	{
		bool bRelativeEnabled = GraphVariable::IsTypeSupportingRelative(associatedParentVar->GetVarType());
		editRelative->SetReadOnly(!bRelativeEnabled);

		if (!bRelativeEnabled && editRelative->GetCheckbox() != nullptr)
		{
			editRelative->GetCheckbox()->SetChecked(false);
		}
	}

	GraphVariable::UpdateDefaultValueComp(editedEnum);
}

void GraphAsset::HandleMemVarNameChanged (const DString& prevId, const DString& varId, const DString& newName)
{
	CHECK(IsAssetDefinition())

	//Need to update the asset instances
	std::vector<GraphAsset*> allClasses;
	FindAllSubclassAssets(OUT allClasses);

	for (GraphAsset* curAsset : allClasses)
	{
		for (GraphAsset* instancedAsset : AssetInstances)
		{
			for (GraphVariable* memVar : instancedAsset->VarInstanceValues)
			{
				if (memVar != nullptr && memVar->GetIdentifyingName().Compare(prevId, DString::CC_CaseSensitive) == 0)
				{
					memVar->SetVariableName_Silent(newName);
					break; //Move onto the next asset instance.
				}
			}
		}
	}
}

void GraphAsset::HandleVarArrayEdit (EditPropertyComponent* editedComp)
{
	CHECK(editedComp != nullptr)

	EditableArray* owningArray = nullptr;
	for (EntityComponent* compOwner = editedComp; compOwner != nullptr; compOwner = dynamic_cast<EntityComponent*>(compOwner->GetOwner()))
	{
		if (EditableArray* castArray = dynamic_cast<EditableArray*>(compOwner))
		{
			owningArray = castArray;
			break;
		}
	}

	if (owningArray == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to apply variable edit since the property component \"%s\" does not reside in an EditableArray."), editedComp->GetPropertyNameStr());
		return;
	}

	size_t varIdx = editedComp->FindSelfFromOwnerSubProperties();
	if (editedComp->GetPropertyId() == GraphVariable::VA_VarName && ContainerUtils::IsValidIndex(MemberVariables, varIdx))
	{
		DString errorMsg;
		GraphCompiler::CheckVariableName(*MemberVariables.at(varIdx), OUT errorMsg, {&MemberVariables});

		if (!errorMsg.IsEmpty())
		{
			editedComp->ShowCaption(errorMsg, editedComp->ReadCaptionErrorColors());
		}
		else
		{
			editedComp->HideCaption();
		}
	}
}

void GraphAsset::HandleMemVarDeleted (EditorInterface* obj)
{
	if (GraphVariable* deletedVar = dynamic_cast<GraphVariable*>(obj))
	{
		for (size_t i = 0; i < MemberVariables.size(); ++i)
		{
			if (MemberVariables.at(i) == deletedVar)
			{
				MemberVariables.at(i) = nullptr;
				return;
			}
		}
	}
}

void GraphAsset::HandleVarInstanceDeleted (EditorInterface* obj)
{
	if (GraphVariable* deletedVar = dynamic_cast<GraphVariable*>(obj))
	{
		for (size_t i = 0; i < VarInstanceValues.size(); ++i)
		{
			if (VarInstanceValues.at(i) == deletedVar)
			{
				VarInstanceValues.at(i) = nullptr;
				return;
			}
		}
	}
}

void GraphAsset::HandleMemFunctionDeleted (EditorInterface* obj)
{
	if (GraphFunction* deletedFunc = dynamic_cast<GraphFunction*>(obj))
	{
		for (size_t i = 0; i < MemberFunctions.size(); ++i)
		{
			if (MemberFunctions.at(i) == deletedFunc)
			{
				MemberFunctions.at(i) = nullptr;
				return;
			}
		}
	}
}

void GraphAsset::HandlePopulateInheritedFunctions (DropdownComponent* dropdown)
{
	CHECK(dropdown != nullptr && dropdown->GetExpandMenuList() != nullptr)

	std::vector<GuiDataElement<GraphFunction*>> allSuperFunctions;
	for (GraphAsset* curAsset = SuperAsset.Get(); curAsset != nullptr; curAsset = curAsset->SuperAsset.Get())
	{
		for (GraphFunction* func : curAsset->ReadMemberFunctions())
		{
			if (func == nullptr || func->GetSuperFunction() != nullptr)
			{
				//Skip this function since this is already inherited from something else (avoid duplicated results).
				continue;
			}

			//Ensure a function is not already overriding this function. Can't have two functions overriding the same function.
			for (GraphFunction* memberFunction : OverrideFunctions)
			{
				if (memberFunction->GetSuperFunction() != func)
				{
					//One of the functions already overrides from this function.
					continue;
				}
			}

			allSuperFunctions.emplace_back(func, func->ReadFunctionName());
		}
	}

	//Sort alphabetically
	std::sort(allSuperFunctions.begin(), allSuperFunctions.end(), [](const GuiDataElement<GraphFunction*>& a, const GuiDataElement<GraphFunction*>& b)
	{
		return (a.Data->ReadFunctionName().Compare(b.Data->ReadFunctionName(), DString::CC_CaseSensitive) < 0);
	});

	dropdown->GetExpandMenuList()->SetList(allSuperFunctions);
}

void GraphAsset::HandleParentFunctionSelected (EditPropertyComponent* editedComp)
{
	CHECK(editedComp != nullptr)
	EditableEnum* editedEnum = dynamic_cast<EditableEnum*>(editedComp);
	if (editedEnum == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to process parent function selection since %s is not an EditableEnum."), editedComp->GetPropertyNameStr());
		return;
	}

	CHECK(editedEnum->GetDropdown() != nullptr)
	DropdownComponent* dropdown = editedEnum->GetDropdown();

	EditableStruct* owningStruct = dynamic_cast<EditableStruct*>(editedEnum->GetOwner());
	if (owningStruct == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to process parent function selection for %s in asset %s since the edited enum does not reside in an EditableStruct."), editedComp->GetPropertyNameStr(), AssetName);
		return;
	}

	GraphFunction* newInheritedFunction = nullptr;
	if (EditableArray* owningArray = dynamic_cast<EditableArray*>(owningStruct->GetOwner()))
	{
		size_t owningIdx = owningStruct->FindSelfFromOwnerSubProperties();
		if (owningIdx != INDEX_NONE && owningIdx < OverrideFunctions.size())
		{
			GraphFunction* oldInheritedFunction = OverrideFunctions.at(owningIdx)->GetSuperFunction();
			if (dropdown->IsItemSelected())
			{
				newInheritedFunction = dropdown->GetSelectedItem<GraphFunction*>();
			}
			OverrideFunctions.at(owningIdx)->SetSuperFunction(newInheritedFunction);

			//Add the old inherited function to all other array items.
			//Remove the new inherited function from all other array items.
			for (size_t i = 0; i < owningArray->ReadSubPropertyComponents().size(); ++i)
			{
				if (i == owningIdx)
				{
					//Skip this index since it's the index that's being edited, itself.
					continue;
				}

				if (EditableStruct* otherStruct = dynamic_cast<EditableStruct*>(owningArray->ReadSubPropertyComponents().at(i)))
				{
					EditableEnum* otherEnum = dynamic_cast<EditableEnum*>(otherStruct->FindSubPropWithMatchingId(GraphFunction::FA_DeriveFrom, false));
					if (otherEnum == nullptr)
					{
						EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to update inheritance function array for %s in asset %s since the editable struct doesn't contain an EditableEnum component with ID %s."), owningArray->GetPropertyNameStr(), AssetName, Int(GraphFunction::FA_DeriveFrom));
						continue;
					}

					if (DropdownComponent* otherDropdown = otherEnum->GetDropdown())
					{
						if (ListBoxComponent* otherList = otherDropdown->GetExpandMenuList())
						{
							if (oldInheritedFunction != nullptr)
							{
								otherList->AddOption(GuiDataElement<GraphFunction*>(oldInheritedFunction, oldInheritedFunction->ReadFunctionName()));
							}

							if (newInheritedFunction != nullptr)
							{
								otherList->RemoveItems<GraphFunction*>([&newInheritedFunction](GraphFunction* a)
								{
									return (a == newInheritedFunction);
								});
							}
						}
					}
				}
			}
		}
	}

	//Update the read-only components to reflect the new inherited function changes.
	if (newInheritedFunction != nullptr)
	{
		if (EditableBool* editStatic = dynamic_cast<EditableBool*>(owningStruct->FindSubPropWithMatchingId(GraphFunction::FA_Static, false)))
		{
			editStatic->GetCheckbox()->SetChecked(newInheritedFunction->IsStatic());
		}

		if (EditableBool* editProtect = dynamic_cast<EditableBool*>(owningStruct->FindSubPropWithMatchingId(GraphFunction::FA_Protected, false)))
		{
			editProtect->GetCheckbox()->SetChecked(newInheritedFunction->IsProtected());
		}

		if (EditableDString* editComment = dynamic_cast<EditableDString*>(owningStruct->FindSubPropWithMatchingId(GraphFunction::FA_Comment, false)))
		{
			editComment->GetPropValueField()->SetText(newInheritedFunction->ReadComment());
		}
	}
}

void GraphAsset::HandleOpenInheritedFunction (ButtonComponent* button, EditablePropCommand* propCmd)
{
	CHECK(propCmd != nullptr)

	if (EditPropertyComponent* owningStruct = dynamic_cast<EditPropertyComponent*>(propCmd->GetOwner()))
	{
		if (EditableEnum* inheritanceEnum = dynamic_cast<EditableEnum*>(owningStruct->FindSubPropWithMatchingId(GraphFunction::FA_DeriveFrom, false)))
		{
			if (DropdownComponent* dropdown = inheritanceEnum->GetDropdown())
			{
				if (dropdown->IsItemSelected())
				{
					if (GraphFunction* selectedFunction = dropdown->GetSelectedItem<GraphFunction*>())
					{
						EditorLog.Log(LogCategory::LL_Log, TXT("Opening function graph: %s"), selectedFunction->ReadFunctionName());
					}
				}
			}
		}
	}
}

void GraphAsset::HandleInheritedFunctionArrayEdit (EditPropertyComponent* editedComp)
{
	EditableArray* editedArray = nullptr;
	for (EntityComponent* owningComp = dynamic_cast<EntityComponent*>(editedComp->GetOwner()); owningComp != nullptr; owningComp = dynamic_cast<EntityComponent*>(owningComp->GetOwner()))
	{
		if (editedArray = dynamic_cast<EditableArray*>(owningComp))
		{
			break;
		}
	}

	if (editedArray != nullptr)
	{
		SyncInheritedFunctionArray(editedArray);
	}
}

void GraphAsset::HandleDefaultOverrideArrayEdit (EditPropertyComponent* editedComp)
{
	EditableArray* editedArray = nullptr;
	for (EntityComponent* owningComp = dynamic_cast<EntityComponent*>(editedComp->GetOwner()); owningComp != nullptr; owningComp = dynamic_cast<EntityComponent*>(owningComp->GetOwner()))
	{
		if (editedArray = dynamic_cast<EditableArray*>(owningComp))
		{
			break;
		}
	}

	if (editedArray != nullptr)
	{
		SyncDefaultOverrideArray(editedArray);
	}
}

void GraphAsset::HandleOverrideFunctionDeleted (EditorInterface* obj)
{
	if (GraphFunction* deletedFunc = dynamic_cast<GraphFunction*>(obj))
	{
		for (size_t i = 0; i < OverrideFunctions.size(); ++i)
		{
			if (OverrideFunctions.at(i) == deletedFunc)
			{
				OverrideFunctions.at(i) = nullptr;
				return;
			}
		}
	}
}
SD_END