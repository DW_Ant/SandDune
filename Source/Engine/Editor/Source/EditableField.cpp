/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableField.cpp
=====================================================================
*/

#include "EditableField.h"
#include "EditorEngineComponent.h"
#include "EditorTheme.h"

IMPLEMENT_ABSTRACT_CLASS(SD::EditableField, SD::EditPropertyComponent)
SD_BEGIN

void EditableField::InitProps ()
{
	Super::InitProps();

	NormalBackgroundColor = Color(64, 64, 64);
	ReadOnlyBackgroundColor = Color(16, 16, 16);

	FieldBackground = nullptr;
	ResetDefaultsButton = nullptr;
}

void EditableField::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableField* fieldTemplate = dynamic_cast<const EditableField*>(objTemplate))
	{
		bool bCreatedObj;
		PropValueField = ReplaceTargetWithObjOfMatchingClass(PropValueField.Get(), fieldTemplate->GetPropValueField(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(PropValueField);
		}

		if (PropValueField.IsValid())
		{
			PropValueField->CopyPropertiesFrom(fieldTemplate->GetPropValueField());
		}

		FieldBackground = ReplaceTargetWithObjOfMatchingClass(FieldBackground, fieldTemplate->FieldBackground, OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(FieldBackground);
		}

		if (FieldBackground != nullptr)
		{
			FieldBackground->CopyPropertiesFrom(fieldTemplate->FieldBackground);
		}

		SetNormalBackgroundColor(fieldTemplate->NormalBackgroundColor);
		SetReadOnlyBackgroundColor(fieldTemplate->ReadOnlyBackgroundColor);
	}
}

void EditableField::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//Note: ReadOnly text fields may still have a cursor position within them to allow copying data.
	if (PropValueField.IsValid() && PropValueField->GetCursorPosition() >= 0 && sfmlEvent.button == sf::Mouse::Button::Left && eventType == sf::Event::MouseButtonPressed && !bReadOnly)
	{
		//The text field is being edited, and the user pressed the left mouse button. If the user clicked outside the field then apply their edits.
		if (!IsWithinEditingField(Vector2(sfmlEvent.x, sfmlEvent.y)))
		{
			ApplyEdits();
			PropValueField->SetCursorPosition(-1);
			MarkPropertyTextDirty();
			ProcessApplyEdit(this);
		}
	}
}

ButtonComponent* EditableField::AddFieldControl ()
{
	ButtonComponent* newButton = Super::AddFieldControl();
	if (newButton != nullptr && FieldBackground != nullptr)
	{
		FieldBackground->SetAnchorRight(FieldControls.size() * FieldButtonSize.X);
	}

	return newButton;
}

bool EditableField::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	MarkPropertyTextDirty();
	return true;
}

void EditableField::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	MarkPropertyTextDirty();
}

void EditableField::UpdatePropertyHorizontalTransforms ()
{
	Super::UpdatePropertyHorizontalTransforms();

	if (FieldBackground != nullptr && BarrierTransform.IsValid())
	{
		//Update the left anchor
		FieldBackground->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);
	}
}

void EditableField::SetNormalFontColor (Color newNormalFontColor)
{
	Super::SetNormalFontColor(newNormalFontColor);

	if (PropValueField.IsValid() && PropValueField->GetRenderComponent() != nullptr && !IsReadOnly())
	{
		PropValueField->GetRenderComponent()->SetFontColor(NormalFontColor.Source);
	}
}

void EditableField::SetReadOnlyFontColor (Color newReadOnlyFontColor)
{
	Super::SetReadOnlyFontColor(newReadOnlyFontColor);

	if (PropValueField.IsValid() && PropValueField->GetRenderComponent() != nullptr && IsReadOnly())
	{
		PropValueField->GetRenderComponent()->SetFontColor(ReadOnlyFontColor.Source);
	}
}

void EditableField::SetReadOnly (bool bNewReadOnly)
{
	if (IsReadOnly() == bNewReadOnly)
	{
		return;
	}

	Super::SetReadOnly(bNewReadOnly);

	if (FieldBackground != nullptr)
	{
		FieldBackground->SetCenterColor(IsReadOnly() ? ReadOnlyBackgroundColor : NormalBackgroundColor);
	}

	if (PropValueField.IsValid())
	{
		PropValueField->SetEditable(!IsReadOnly());

		if (PropValueField->GetRenderComponent() != nullptr)
		{
			PropValueField->GetRenderComponent()->SetFontColor(IsReadOnly() ? ReadOnlyFontColor.Source : NormalFontColor.Source);
		}
	}

	if (ResetDefaultsButton.IsValid())
	{
		ResetDefaultsButton->SetEnabled(!IsReadOnly());
	}
}

void EditableField::SetLineHeight (Float newLineHeight)
{
	Super::SetLineHeight(newLineHeight);

	if (FieldBackground != nullptr)
	{
		FieldBackground->EditSize().Y = LineHeight;
	}
}

void EditableField::InitializeComponents ()
{
	Super::InitializeComponents();

	CHECK(BarrierTransform.IsValid())

	FieldBackground = FrameComponent::CreateObject();
	if (AddComponent(FieldBackground))
	{
		FieldBackground->SetAnchorRight(0.f);
		FieldBackground->SetAnchorLeft(BarrierTransform->ReadPosition().X + BarrierTransform->ReadSize().X);
		FieldBackground->SetSize(Vector2(1.f, LineHeight));
		FieldBackground->SetLockedFrame(true);
		FieldBackground->SetBorderThickness(1.f);
		FieldBackground->SetCenterColor(IsReadOnly() ? ReadOnlyBackgroundColor : NormalBackgroundColor);

		PropValueField = TextFieldComponent::CreateObject();
		if (FieldBackground->AddComponent(PropValueField))
		{
			PropValueField->SetAutoRefresh(false);

			Float margin = FieldBackground->GetBorderThickness() * 1.5f;
			PropValueField->SetAnchorTop(margin);
			PropValueField->SetAnchorRight(margin);
			PropValueField->SetAnchorBottom(margin);
			PropValueField->SetAnchorLeft(margin);

			PropValueField->SetWrapText(false);
			PropValueField->SetClampText(false);
			PropValueField->SetVerticalAlignment(LabelComponent::VA_Center);
			PropValueField->SetEditable(!IsReadOnly());
			PropValueField->SetText(DString::EmptyString);
			if (PropValueField->GetRenderComponent() != nullptr)
			{
				PropValueField->GetRenderComponent()->SetFontColor(IsReadOnly() ? ReadOnlyFontColor.Source : NormalFontColor.Source);
			}

			PropValueField->OnAllowTextInput = SDFUNCTION_1PARAM(this, EditableField, HandleAllowTextInput, bool, const DString&);
			PropValueField->OnReturn = SDFUNCTION_1PARAM(this, EditableField, HandleReturn, void, TextFieldComponent*);
			PropValueField->SetAutoRefresh(true);
		}
	}
}

DString EditableField::ConstructValueAsText () const
{
	//Default to displaying the contents of the field
	if (PropValueField == nullptr)
	{
		return DString::EmptyString;
	}

	const std::vector<DString>& fieldContents = PropValueField->ReadContent();
	if (!ContainerUtils::IsEmpty(fieldContents))
	{
		return fieldContents.at(0); //Only return the first line
	}

	return DString::EmptyString;
}

void EditableField::SetPropValue (const DString& newValue, bool bInvokeCallback)
{
	if (PropValueField != nullptr)
	{
		PropValueField->SetText(newValue);

		ApplyEdits();
		MarkPropertyTextDirty();

		if (bInvokeCallback)
		{
			ProcessApplyEdit(this);
		}
	}
}

void EditableField::AddResetToDefaultsControl ()
{
	ButtonComponent* newButton = AddFieldControl();
	if (newButton != nullptr)
	{
		newButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditableField, HandleResetToDefaultsReleased, void, ButtonComponent*));
		newButton->SetEnabled(!IsReadOnly());
		ResetDefaultsButton = newButton;

		EditorTheme* localTheme = dynamic_cast<EditorTheme*>(GuiTheme::GetGuiTheme());
		if (localTheme != nullptr)
		{
			if (FrameComponent* background = ResetDefaultsButton->GetBackground())
			{
				background->SetCenterTexture(localTheme->FieldControlReset);
			}
		}

		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (newButton->AddComponent(tooltip))
		{
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)

			tooltip->SetTooltipText(translator->TranslateText(TXT("ResetDefaults"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FieldControls")));
		}
	}
}

void EditableField::SetNormalBackgroundColor (Color newNormalColor)
{
	NormalBackgroundColor = newNormalColor;
	if (FieldBackground != nullptr && !IsReadOnly())
	{
		FieldBackground->SetCenterColor(NormalBackgroundColor);
	}
}

void EditableField::SetReadOnlyBackgroundColor (Color newReadOnlyColor)
{
	ReadOnlyBackgroundColor = newReadOnlyColor;
	if (FieldBackground != nullptr && IsReadOnly())
	{
		FieldBackground->SetCenterColor(ReadOnlyBackgroundColor);
	}
}

bool EditableField::IsWithinEditingField (const Vector2& mousePos) const
{
	return (PropValueField.IsValid() && PropValueField->IsWithinBounds(mousePos));
}

DString EditableField::GetPermittedInputChars () const
{
	//Enable any character
	return DString::EmptyString;
}

bool EditableField::HandleAllowTextInput (const DString& txt)
{
	DString permittedChars = GetPermittedInputChars();
	if (permittedChars.IsEmpty())
	{
		return true;
	}

	return txt.HasRegexMatch(permittedChars);
}

void EditableField::HandleReturn (TextFieldComponent* uiComponent)
{
	if (ResetDefaultsButton.IsValid())
	{
		ResetDefaultsButton->SetEnabled(true);
	}

	ApplyEdits();
	MarkPropertyTextDirty();
	ProcessApplyEdit(this);
}

void EditableField::HandleResetToDefaultsReleased (ButtonComponent* uiComp)
{
	uiComp->SetEnabled(false);
	if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
	{
		//Tooltips will not receive a hide call since the button won't pass input events to subcomponents when disabled.
		tooltipEntity->HideTooltip();
	};

	ResetToDefaults();
}
SD_END