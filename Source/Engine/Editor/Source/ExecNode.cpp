/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ExecNode.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "EditorTheme.h"
#include "ErrorNode.h"
#include "ExecNode.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::ExecNode, SD::GraphNode)
SD_BEGIN

void ExecNode::InitProps ()
{
	Super::InitProps();

	bStartPoint = true;
	OwningFunctionId = DString::EmptyString;
	PortName = nullptr;

	bExecNodeInitialized = false;
}

bool ExecNode::IsDeletable () const
{
	if (bStartPoint)
	{
		//Cannot delete the starting node
		return false;
	}

	return Super::IsDeletable();
}

void ExecNode::BeginObject ()
{
	Super::BeginObject();

	//Must rely on delegates instead of having the GraphFunction locating node instances directly in its own definition since there may be other ExecNodes referencing the same function due to inherited functions.
	GraphFunction::OnFunctionNameChanged.RegisterHandler(SDFUNCTION_3PARAM(this, ExecNode, HandleFunctionNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamNameChanged.RegisterHandler(SDFUNCTION_3PARAM(this, ExecNode, HandleFunctionParamNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamChanged.RegisterHandler(SDFUNCTION_1PARAM(this, ExecNode, HandleFunctionParamsChanged, void, const DString&));
}

void ExecNode::ProcessPortConnectionChanged (SPort* portData)
{
	//If the changed port is one of the exec ports, don't change the inlined component visibility.
	bool isExecPort = false;
	if (bStartPoint && !ContainerUtils::IsEmpty(OutputPorts))
	{
		isExecPort = (&OutputPorts.at(0) == portData);
	}
	else if (!bStartPoint && !ContainerUtils::IsEmpty(InputPorts))
	{
		isExecPort = (&InputPorts.at(0) == portData);
	}

	if (!isExecPort)
	{
		Super::ProcessPortConnectionChanged(portData);
	}
}

void ExecNode::Destroyed ()
{
	GraphFunction::OnFunctionNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, ExecNode, HandleFunctionNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, ExecNode, HandleFunctionParamNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamChanged.TryUnregisterHandler(SDFUNCTION_1PARAM(this, ExecNode, HandleFunctionParamsChanged, void, const DString&));

	Super::Destroyed();
}

void ExecNode::InitializeExecType (bool isInputNode)
{
	if (bExecNodeInitialized)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize ExecNode (%s) more than once."), ToString());
		return;
	}

	bStartPoint = isInputNode;
	bExecNodeInitialized = true;
	SetAutoUpdateSize(false);

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString sectionName = TXT("ExecNode");
	if (bStartPoint)
	{
		SetHeaderText(translator->TranslateText(TXT("StartHeader"), EditorEngineComponent::LOCALIZATION_FILE_NAME, sectionName));
	}
	else
	{
		SetHeaderText(translator->TranslateText(TXT("ExitHeader"), EditorEngineComponent::LOCALIZATION_FILE_NAME, sectionName));
	}

	SInitPort initPort(!bStartPoint, NodePort::SStaticClass(), DString::EmptyString);
	initPort.ScaleMultiplier = 1.5f;
	if (!bStartPoint)
	{
		initPort.InlinedComponentClass = TextFieldComponent::SStaticClass();
	}

	if (SPort* newExecPort = CreatePort(initPort))
	{
		PortName = dynamic_cast<TextFieldComponent*>(newExecPort->PortInlineEdit);
		if (newExecPort->Port != nullptr)
		{
			newExecPort->Port->SetAcceptMultiConnections(!bStartPoint);
			
			GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
			CHECK(guiEngine != nullptr)
			if (EditorTheme* editorTheme = dynamic_cast<EditorTheme*>(editorTheme->GetGuiTheme()))
			{
				if (SpriteComponent* spriteComp = newExecPort->Port->GetSpriteComp())
				{
					spriteComp->SetSpriteTexture(editorTheme->ExecPortTexture);
				}
			}
		}

		if (PortName != nullptr)
		{
			PortName->SetCharacterSize(LabelFontSize);
			PortName->SetSingleLine(true);
		}
	}

	GraphFunction* constructedFunc = GetConstructedFunction();
	if (constructedFunc != nullptr)
	{
		OwningFunctionId = constructedFunc->GetIdentifyingName();

		const std::vector<GraphVariable*>* paramList = nullptr;
		if (bStartPoint)
		{
			paramList = &constructedFunc->ReadInParams();
		}
		else
		{
			paramList = &constructedFunc->ReadOutParams();
		}
		
		CHECK(paramList != nullptr)

		for (GraphVariable* param : *paramList)
		{
			if (param == nullptr)
			{
				continue;
			}

			SInitPort initParamPort(!bStartPoint, VariablePort::SStaticClass(), param->GetPresentedName());
			if (!bStartPoint)
			{
				initParamPort.InlinedComponentClass = param->GetInlinedComponentClass();
			}

			if (SPort* newPortData = CreatePort(initParamPort))
			{
				if (VariablePort* varPort = dynamic_cast<VariablePort*>(newPortData->Port))
				{
					varPort->SetAcceptMultiConnections(bStartPoint);
					varPort->SetVarType(param->GetVarType());
					varPort->SetClassConstraint(param->GetClassConstraint());
				}

				if (newPortData->PortInlineEdit != nullptr)
				{
					param->InitializeInlinedComponent(newPortData->PortInlineEdit, !bStartPoint, LabelFontSize);
				}
			}
		}
	}

	SetAutoUpdateSize(true);
}

void ExecNode::RepairPorts (bool checkInputParams, std::vector<SPort>& outPortList)
{
	GraphFunction* owningFunc = GetConstructedFunction();
	CHECK(owningFunc != nullptr)

	const std::vector<GraphVariable*>& paramList = checkInputParams ? owningFunc->ReadInParams() : owningFunc->ReadOutParams();
	GraphNode* staleNode = nullptr;

	SetAutoUpdateSize(false);

	//Check if any port needs to be removed
	if (!ContainerUtils::IsEmpty(outPortList))
	{
		size_t portIdx = outPortList.size() - 1;
		while (portIdx >= 1) //Ignore the first port since it's an exec port
		{
			bool removePort = (outPortList.at(portIdx).PortLabel == nullptr);
			DString oldLabel = DString::EmptyString;
			if (!removePort)
			{
				removePort = true; //assume not found
				oldLabel = outPortList.at(portIdx).PortLabel->GetContent();

				for (size_t i = 0; i < paramList.size(); ++i)
				{
					if (paramList.at(i) != nullptr && paramList.at(i)->GetPresentedName().Compare(oldLabel, DString::CC_CaseSensitive) == 0)
					{
						removePort = false;
						break; //param found
					}
				}
			}

			if (removePort)
			{
				if (VariablePort* oldVarPort = dynamic_cast<VariablePort*>(outPortList.at(portIdx).Port))
				{
					if (!ContainerUtils::IsEmpty(oldVarPort->ReadConnections()))
					{
						if (staleNode == nullptr)
						{
							staleNode = CreateStaleNode();
						}

						MovePortConnections(oldVarPort, oldLabel, staleNode, oldVarPort->GetVarType());
					}
				}

				//Destroy the port components
				{
					SPort& portComps = outPortList.at(portIdx);
					if (portComps.Port != nullptr)
					{
						portComps.Port->Destroy();
					}

					if (portComps.PortLabel != nullptr)
					{
						portComps.PortLabel->Destroy();
					}

					if (portComps.PortInlineEdit != nullptr)
					{
						portComps.PortInlineEdit->Destroy();
					}
				}

				outPortList.erase(outPortList.begin() + portIdx);
			}

			--portIdx;
		}
	}

	CHECK(outPortList.size() >= 1)
	size_t oldPortSize = outPortList.size() - 1; //Minus 1 to exclude the exec port

	//Check if ports need to be added
	for (size_t i = oldPortSize; i < paramList.size(); ++i)
	{
		if (paramList.at(i) == nullptr)
		{
			continue;
		}

		SInitPort newPort(!bStartPoint, VariablePort::SStaticClass(), paramList.at(i)->GetPresentedName());

		if (!bStartPoint)
		{
			newPort.InlinedComponentClass = paramList.at(i)->GetInlinedComponentClass();
		}
		
		if (SPort* newPortData = CreatePort(newPort))
		{
			if (VariablePort* newVarPort = dynamic_cast<VariablePort*>(newPortData->Port))
			{
				newVarPort->SetAcceptMultiConnections(bStartPoint);
				newVarPort->SetVarType(paramList.at(i)->GetVarType());
				newVarPort->SetClassConstraint(paramList.at(i)->GetClassConstraint());
			}

			if (newPortData->PortInlineEdit != nullptr)
			{
				paramList.at(i)->InitializeInlinedComponent(newPortData->PortInlineEdit, !bStartPoint, LabelFontSize);
			}
		}
	}
	
	//Update existing ports if needed
	for (size_t i = 0; i < paramList.size(); ++i)
	{
		if (paramList.at(i) == nullptr)
		{
			continue;
		}

		size_t portIdx = i+1;

		CHECK(outPortList.at(portIdx).PortLabel != nullptr)
		DString paramName = paramList.at(i)->GetPresentedName();

		if (outPortList.at(portIdx).PortLabel->GetContent().Compare(paramName, DString::CC_CaseSensitive) == 0)
		{
			//Names still match. Only need to update the var type.
			if (VariablePort* varPort = dynamic_cast<VariablePort*>(outPortList.at(portIdx).Port))
			{
				if (varPort->GetVarType() == paramList.at(i)->GetVarType())
				{
					//This port is up to date.
					continue;
				}

				const std::vector<NodePort*>& connections = varPort->ReadConnections();
				ScriptVariable::EVarType oldVarType = varPort->GetVarType();
				varPort->SetVarType(paramList.at(i)->GetVarType());
				varPort->SetClassConstraint(paramList.at(i)->GetClassConstraint());

				if (!ContainerUtils::IsEmpty(connections))
				{
					bool isValidConnection = connections.at(0)->CanAcceptConnectionsFrom(varPort, true);
					if (isValidConnection)
					{
						if (VariablePort* connectionsVarPort = dynamic_cast<VariablePort*>(connections.at(0)))
						{
							if (connectionsVarPort->GetVarType() != varPort->GetVarType())
							{
								std::vector<NodePort*> connectionCpy = varPort->ReadConnections();
								for (NodePort* otherPort : connectionCpy)
								{
									//Port can accept connections, but the types do not match, create a conversion node between them.
									varPort->ReconnectToVarPort(otherPort);
								}
							}
						}
					}

					if (!isValidConnection)
					{
						//Connections are no longer valid with this new type. Move all connections to the stale connection node.
						if (staleNode == nullptr)
						{
							staleNode = CreateStaleNode();
						}

						MovePortConnections(varPort, paramName, staleNode, oldVarType);
					}
				}
			}

			continue;
		}

		//Names do not match.
		if (VariablePort* oldVarPort = dynamic_cast<VariablePort*>(outPortList.at(portIdx).Port))
		{
			if (!ContainerUtils::IsEmpty(oldVarPort->ReadConnections()))
			{
				//Break existing connections
				if (staleNode == nullptr)
				{
					staleNode = CreateStaleNode();
				}

				DString oldLabel;
				if (outPortList.at(portIdx).PortLabel != nullptr)
				{
					oldLabel = outPortList.at(portIdx).PortLabel->GetContent();
				}

				MovePortConnections(oldVarPort, oldLabel, staleNode, oldVarPort->GetVarType());
			}

			oldVarPort->SetVarType(paramList.at(i)->GetVarType());
			oldVarPort->SetClassConstraint(paramList.at(i)->GetClassConstraint());
			if (LabelComponent* portLabel = outPortList.at(portIdx).PortLabel)
			{
				portLabel->SetText(paramName);
			}

			if (!bStartPoint)
			{
				const DClass* correctClass = paramList.at(i)->GetInlinedComponentClass();
				GuiComponent* oldInline = outPortList.at(portIdx).PortInlineEdit;
				if (oldInline != nullptr && correctClass != oldInline->StaticClass())
				{
					oldInline->Destroy();
					outPortList.at(portIdx).PortInlineEdit = nullptr;
				}

				if (correctClass != nullptr && outPortList.at(portIdx).PortInlineEdit == nullptr)
				{
					if (const Object* cdo = correctClass->GetDefaultObject())
					{
						CHECK(correctClass->IsA(GuiComponent::SStaticClass()))
						outPortList.at(portIdx).PortInlineEdit = dynamic_cast<GuiComponent*>(cdo->CreateObjectOfMatchingClass());
						paramList.at(i)->InitializeInlinedComponent(outPortList.at(portIdx).PortInlineEdit, !bStartPoint, LabelFontSize);
					}
				}
			}
		}
	}

	SetAutoUpdateSize(true);
}

GraphNode* ExecNode::CreateStaleNode ()
{
	CHECK(GetOwner() != nullptr)

	ErrorNode* result = ErrorNode::CreateObject();
	if (GetOwner()->AddComponent(result))
	{
		Float multiplier = (bStartPoint) ? 1.f : -1.f; //Move to the left if this stale node is for a return node.
		result->SetPosition(ReadPosition() + Vector2(64.f * multiplier, 0.f));
		result->SetErrorType(ErrorNode::ET_StaleConnections);
	}

	return result;
}

void ExecNode::MovePortConnections (VariablePort* moveFrom, const DString& moveToLabel, GraphNode* moveTo, ScriptVariable::EVarType moveToVarType)
{
	CHECK(moveFrom != nullptr && moveTo != nullptr)

	SInitPort newPort(!bStartPoint, VariablePort::SStaticClass(), moveToLabel);
	if (SPort* stalePort = moveTo->CreatePort(newPort))
	{
		if (VariablePort* oldVarPort = dynamic_cast<VariablePort*>(stalePort->Port))
		{
			oldVarPort->SetVarType(moveToVarType);
			std::vector<NodePort*> connectionCpy = moveFrom->ReadConnections();
			moveFrom->SeverAllConnections();

			for (NodePort* oldPort : connectionCpy)
			{
				if (oldVarPort->CanAcceptConnectionsFrom(oldPort, true))
				{
					oldVarPort->EstablishConnectionTo(oldPort);
				}
			}
		}
	}
}

void ExecNode::HandleFunctionNameChanged (const DString& prevFuncId, const DString& newFuncId, const DString& newName)
{
	if (!bExecNodeInitialized || prevFuncId.Compare(OwningFunctionId, DString::CC_CaseSensitive) != 0)
	{
		return;
	}

	OwningFunctionId = newFuncId;
}

void ExecNode::HandleFunctionParamNameChanged (const DString& functionId, const DString& prevName, const DString& newName)
{
	if (!bExecNodeInitialized ||  OwningFunctionId.Compare(functionId, DString::CC_CaseSensitive) != 0)
	{
		return;
	}

	const std::vector<SPort>& portList = (bStartPoint) ? OutputPorts : InputPorts;
	for (const SPort& portData : portList)
	{
		if (portData.PortLabel != nullptr)
		{
			if (portData.PortLabel->GetContent().Compare(prevName, DString::CC_CaseSensitive) == 0)
			{
				portData.PortLabel->SetText(newName);

				if (bAutoUpdateSize)
				{
					UpdateNodeSize();
				}

				break;
			}
		}
	}
}

void ExecNode::HandleFunctionParamsChanged (const DString& functionId)
{
	if (!bExecNodeInitialized || OwningFunctionId.Compare(functionId, DString::CC_CaseSensitive) != 0)
	{
		return;
	}

	//Opposite is intended. If the function's input parameters are changed, then only the starting ExecNode (which only has out parameters) may have been adjusted.
	std::vector<SPort>& portList = bStartPoint ? OutputPorts : InputPorts;
	RepairPorts(bStartPoint, OUT portList);
}
SD_END