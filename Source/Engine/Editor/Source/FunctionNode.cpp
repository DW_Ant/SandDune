/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FunctionNode.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "EditorTheme.h"
#include "ErrorNode.h"
#include "FunctionNode.h"
#include "GraphAsset.h"
#include "GraphFunction.h"
#include "GraphVariable.h"
#include "NodePort.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::FunctionNode, SD::GraphNode)
SD_BEGIN

void FunctionNode::InitProps ()
{
	Super::InitProps();

	FunctionId = DString::EmptyString;
	VarId = DString::EmptyString;
	EntrancePortScaleMultiplier = 2.f;
	InvokeOnTextOverride = DString::EmptyString;
	InvokeOnEdgeTransform = nullptr;
}

void FunctionNode::BeginObject ()
{
	Super::BeginObject();

	GraphFunction::OnFunctionNameChanged.RegisterHandler(SDFUNCTION_3PARAM(this, FunctionNode, HandleFunctionNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnCommentChanged.RegisterHandler(SDFUNCTION_2PARAM(this, FunctionNode, HandleFunctionCommentChanged, void, const DString&, const DString&));
	GraphFunction::OnParamNameChanged.RegisterHandler(SDFUNCTION_3PARAM(this, FunctionNode, HandleFunctionParamNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamChanged.RegisterHandler(SDFUNCTION_1PARAM(this, FunctionNode, HandleFunctionParamsChanged, void, const DString&));
}

void FunctionNode::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const FunctionNode* nodeTemplate = dynamic_cast<const FunctionNode*>(objTemplate))
	{
		SetEntrancePortScaleMultiplier(nodeTemplate->GetEntrancePortScaleMultiplier());

		bool bCreatedObj;
		InvokeOnBackground = ReplaceTargetWithObjOfMatchingClass(InvokeOnBackground.Get(), nodeTemplate->InvokeOnBackground.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(InvokeOnBackground);
		}

		if (InvokeOnBackground != nullptr)
		{
			InvokeOnBackground->CopyPropertiesFrom(nodeTemplate->InvokeOnBackground.Get());
		}

		InvokeOnEdgeSprite = ReplaceTargetWithObjOfMatchingClass(InvokeOnEdgeSprite.Get(), nodeTemplate->InvokeOnEdgeSprite.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			CHECK(InvokeOnEdgeTransform != nullptr)
			InvokeOnEdgeTransform->AddComponent(InvokeOnEdgeSprite);
		}

		if (InvokeOnEdgeSprite != nullptr)
		{
			InvokeOnEdgeSprite->CopyPropertiesFrom(nodeTemplate->InvokeOnEdgeSprite.Get());
		}

		InvokeOnLabel = ReplaceTargetWithObjOfMatchingClass(InvokeOnLabel.Get(), nodeTemplate->InvokeOnLabel.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(InvokeOnLabel);
		}

		if (InvokeOnLabel != nullptr)
		{
			InvokeOnLabel->CopyPropertiesFrom(nodeTemplate->InvokeOnLabel.Get());
		}
	}
}

bool FunctionNode::RemovePort (bool isInputPort, size_t portIdx)
{
	for (size_t i = 0; i < PortVarData.size(); ++i)
	{
		if (PortVarData.at(i).IsInputPort == isInputPort && PortVarData.at(i).PortIdx == portIdx)
		{
			PortVarData.erase(PortVarData.begin() + i);
			break;
		}
	}

	//All port var data that have indices beyond the portIdx needs to be decremented since the index to the SPort vectors (in GraphNode) are about to be displaced.
	for (SPortVarData& varData : PortVarData)
	{
		if (varData.IsInputPort == isInputPort && varData.PortIdx > portIdx)
		{
			varData.PortIdx--;
		}
	}

	return Super::RemovePort(isInputPort, portIdx);
}

void FunctionNode::SetBackgroundColor (Color newBackgroundColor)
{
	Super::SetBackgroundColor(newBackgroundColor);

	if (InvokeOnBackground != nullptr)
	{
		InvokeOnBackground->SetColorMultiplier(BackgroundColor);
	}

	if (InvokeOnEdgeSprite != nullptr)
	{
		if (sf::Sprite* sprite = InvokeOnEdgeSprite->GetSprite())
		{
			sprite->setColor(BackgroundColor.Source);
		}
	}
}

void FunctionNode::InitializeComponents ()
{
	Super::InitializeComponents();

	InvokeOnBackground = FrameComponent::CreateObject();
	if (AddComponent(InvokeOnBackground))
	{
		InvokeOnBackground->SetPosition(Vector2(HighlightThickness, 0.f));
		InvokeOnBackground->SetVisibility(false); //Becomes visible when function is bound
		InvokeOnBackground->SetSize(Vector2(0.5f, Utils::Max(LabelFontSize.ToFloat(), PortBaseSize) + PortVerticalSpacing));

		if (BackgroundColor != Color::WHITE)
		{
			InvokeOnBackground->SetColorMultiplier(BackgroundColor);
		}

		if (BorderRenderComponent* borderComp = InvokeOnBackground->GetBorderComp())
		{
			borderComp->Destroy();
		}
		InvokeOnBackground->SetBorderThickness(0.f);

		//Shift the background to be below the InvokeOn frame
		if (Background != nullptr)
		{
			Float posY = InvokeOnBackground->ReadSize().Y - HighlightThickness;
			Background->SetAnchorTop(posY);
			Background->SetAnchorBottom(0.f);
		}

		InvokeOnEdgeTransform = PlanarTransformComponent::CreateObject();
		if (InvokeOnBackground->AddComponent(InvokeOnEdgeTransform))
		{
			InvokeOnEdgeTransform->EditPosition().X = InvokeOnBackground->ReadSize().X;
			Float size = InvokeOnBackground->ReadSize().Y;
			InvokeOnEdgeTransform->SetSize(size, size);

			InvokeOnEdgeSprite = SpriteComponent::CreateObject();
			if (InvokeOnEdgeTransform->AddComponent(InvokeOnEdgeSprite))
			{
				if (sf::Sprite* sprite = InvokeOnEdgeSprite->GetSprite())
				{
					sprite->setColor(BackgroundColor.Source);
				}
			}
		}

		InvokeOnLabel = LabelComponent::CreateObject();
		if (InvokeOnBackground->AddComponent(InvokeOnLabel))
		{
			InvokeOnLabel->SetAutoRefresh(false);
			InvokeOnLabel->SetPosition(Vector2(PortBaseSize + (PortHorizontalSpacing * 2.f), 0.f));
			InvokeOnLabel->SetSize(Vector2(1.f, 1.f));
			InvokeOnLabel->SetCharacterSize(LabelFontSize);
			InvokeOnLabel->SetVerticalAlignment(LabelComponent::VA_Center);
			InvokeOnLabel->SetAutoSizeHorizontal(true);
			InvokeOnLabel->SetAutoRefresh(true);
		}

		//Don't create the InvokeOnPort component yet. The NodePorts require this node to be associated with a GraphEditor before they can be attached.
	}
}

void FunctionNode::RefreshNodeComponents ()
{
	Super::RefreshNodeComponents();

	if (InvokeOnBackground != nullptr && Background != nullptr)
	{
		Float posY = InvokeOnBackground->IsVisible() ? InvokeOnBackground->ReadSize().Y - HighlightThickness : 0.f;
		Background->SetAnchorTop(posY);
		Background->SetAnchorBottom(0.f);
	}
}

void FunctionNode::CalcNodeSize (Vector2& outMinSize) const
{
	Super::CalcNodeSize(OUT outMinSize);

	if (InvokeOnBackground != nullptr && InvokeOnBackground->IsVisible())
	{
		Float minWidth = InvokeOnBackground->ReadSize().X + (HighlightThickness * 2.f);

		if (InvokeOnEdgeTransform != nullptr)
		{
			minWidth += InvokeOnEdgeTransform->ReadSize().X;
		}

		if (outMinSize.X < minWidth)
		{
			outMinSize.X = minWidth;
		}

		//Add a bit of height size since invoke on background is going to displace the entire node based on its size.
		outMinSize.Y += InvokeOnBackground->ReadSize().Y - HighlightThickness;
	}
}

void FunctionNode::Destroyed ()
{
	if (!FunctionId.IsEmpty())
	{
		UnbindFunction();
	}

	GraphFunction::OnFunctionNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, FunctionNode, HandleFunctionNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnCommentChanged.TryUnregisterHandler(SDFUNCTION_2PARAM(this, FunctionNode, HandleFunctionCommentChanged, void, const DString&, const DString&));
	GraphFunction::OnParamNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, FunctionNode, HandleFunctionParamNameChanged, void, const DString&, const DString&, const DString&));
	GraphFunction::OnParamChanged.TryUnregisterHandler(SDFUNCTION_1PARAM(this, FunctionNode, HandleFunctionParamsChanged, void, const DString&));

	GraphVariable::OnNameChanged.TryUnregisterHandler(SDFUNCTION_3PARAM(this, FunctionNode, HandleVarNameChanged, void, const DString&, const DString&, const DString&));

	Super::Destroyed();
}

void FunctionNode::BindFunction (GraphFunction* newAssociatedFunction)
{
	if (newAssociatedFunction == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot bind a FunctionNode to a nullptr GraphFunction."));
		return;
	}

	EditorTheme* theme = nullptr;
	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	theme = dynamic_cast<EditorTheme*>(guiEngine->GetGuiTheme());

	if (!FunctionId.IsEmpty())
	{
		UnbindFunction();
	}

	SetAutoUpdateSize(false);
	RemovePortComponents();

	FunctionId = newAssociatedFunction->GetIdentifyingName();

	if (InvokeOnBackground != nullptr)
	{
		InvokeOnBackground->SetVisibility(!newAssociatedFunction->IsGlobal());
	}

	InitInvokeOnPort(newAssociatedFunction);

	if (!newAssociatedFunction->ReadDisplayName().IsEmpty())
	{
		SetHeaderText(newAssociatedFunction->ReadDisplayName());
	}
	else
	{
		SetHeaderText(newAssociatedFunction->ReadFunctionName());
	}

	if (!newAssociatedFunction->ReadComment().IsEmpty() && HeaderText != nullptr)
	{
		TooltipComponent* tooltip = TooltipComponent::CreateObject();
		if (HeaderText->AddComponent(tooltip))
		{
			tooltip->SetTooltipText(newAssociatedFunction->ReadComment());
		}
	}

	//Create enter and exit ports
	bool execPortVisible = !newAssociatedFunction->IsConst() && !newAssociatedFunction->IsGlobal();
	if (newAssociatedFunction->GetExecPortVisibility() != GraphFunction::EPV_Default)
	{
		execPortVisible = (newAssociatedFunction->GetExecPortVisibility() == GraphFunction::EPV_AlwaysVisible);
	}

	if (execPortVisible)
	{
		SInitPort inPortData(true, NodePort::SStaticClass(), DString::EmptyString);
		inPortData.ScaleMultiplier = EntrancePortScaleMultiplier;
		if (SPort* portData = CreatePort(inPortData))
		{
			if (portData->Port != nullptr)
			{
				portData->Port->SetAcceptMultiConnections(true);

				if (theme != nullptr)
				{
					if (SpriteComponent* sprite = portData->Port->GetSpriteComp())
					{
						sprite->SetSpriteTexture(theme->ExecPortTexture);
					}
				}
			}
		}

		SInitPort outPortData(false, NodePort::SStaticClass(), DString::EmptyString);
		outPortData.ScaleMultiplier = EntrancePortScaleMultiplier;
		if (SPort* portData = CreatePort(outPortData))
		{
			if (portData->Port != nullptr)
			{
				portData->Port->SetAcceptMultiConnections(false);

				if (theme != nullptr)
				{
					if (SpriteComponent* sprite = portData->Port->GetSpriteComp())
					{
						sprite->SetSpriteTexture(theme->ExecPortTexture);
					}
				}
			}
		}
	}

	for (GraphVariable* inputParam : newAssociatedFunction->EditInParams())
	{
		if (inputParam != nullptr)
		{
			InitPortForParam(inputParam, true);
		}
	}

	for (GraphVariable* outputParam : newAssociatedFunction->EditOutParams())
	{
		if (outputParam != nullptr)
		{
			InitPortForParam(outputParam, false);
		}
	}

	SetAutoUpdateSize(true);
}

void FunctionNode::EnableInvokeOnComps (GraphAsset* acceptedType, bool isStatic)
{
	if (acceptedType == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot enable InvokeOn components in a FunctionNode without specifying an asset type."));
		return;
	}

	if (InvokeOnBackground != nullptr)
	{
		InvokeOnBackground->SetVisibility(true);

		if (InvokeOnPort != nullptr)
		{
			InvokeOnPort->SetClassConstraint(acceptedType);
			InvokeOnPort->SetVarType(isStatic ? ScriptVariable::VT_Class : ScriptVariable::VT_Obj);
		}

		UpdateInvokeOnLabel();
	}

	if (bAutoUpdateSize)
	{
		RefreshNodeComponents();
	}
}

GraphFunction* FunctionNode::FindGraphFunction () const
{
	if (FunctionId.IsEmpty())
	{
		return nullptr;
	}

	DString assetName;
	DString functionName;
	if (!GraphFunction::ParseFunctionId(FunctionId, OUT assetName, OUT functionName))
	{
		return nullptr;
	}

	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)

	HashedString hashedName(assetName);
	GraphAsset* asset = nullptr;
	if (editorEngine->ReadLoadedAssets().contains(hashedName))
	{
		asset = editorEngine->ReadLoadedAssets().at(hashedName);
	}

	if (asset == nullptr)
	{
		return nullptr;
	}

	for (GraphFunction* func : asset->ReadMemberFunctions())
	{
		if (func->ReadFunctionName().Compare(functionName, DString::CC_CaseSensitive) == 0)
		{
			return func;
		}
	}

	return nullptr;
}

void FunctionNode::SetVarId (const DString& newVarId)
{
	VarId = newVarId;

	if (!VarId.IsEmpty())
	{
		SDFunction<void, const DString&, const DString&, const DString&> nameChangeHandler = SDFUNCTION_3PARAM(this, FunctionNode, HandleVarNameChanged, void, const DString&, const DString&, const DString&);
		if (!GraphVariable::OnNameChanged.IsRegistered(nameChangeHandler, true))
		{
			GraphVariable::OnNameChanged.RegisterHandler(nameChangeHandler);
		}
	}
}

void FunctionNode::SetEntrancePortScaleMultiplier (Float newEntrancePortScaleMultiplier)
{
	EntrancePortScaleMultiplier = Utils::Max<Float>(newEntrancePortScaleMultiplier, 0.05f);
}

void FunctionNode::SetInvokeOnTextOverride (const DString& newInvokeOnTextOverride)
{
	InvokeOnTextOverride = newInvokeOnTextOverride;
	UpdateInvokeOnLabel();

	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}
}

void FunctionNode::InitInvokeOnPort (GraphFunction* func)
{
	if (InvokeOnBackground != nullptr && InvokeOnPort == nullptr)
	{
		InvokeOnPort = VariablePort::CreateObject();
		if (InvokeOnBackground->AddComponent(InvokeOnPort))
		{
			InvokeOnPort->SetPosition(Vector2(PortHorizontalSpacing, PortVerticalSpacing * 0.5f));
			InvokeOnPort->SetSize(Vector2(PortBaseSize, PortBaseSize));
			InvokeOnPort->SetAcceptMultiConnections(false);
			InvokeOnPort->SetPortType(NodePort::PT_Input);
			InvokeOnPort->OnConnectionChanged = SDFUNCTION_1PARAM(this, FunctionNode, HandleInvokeOnPortConnectionChanged, void, NodePort*);
		}
	}

	if (InvokeOnPort != nullptr && func != nullptr)
	{
		InvokeOnPort->SetClassConstraint(func->GetOwningAsset());

		if (func->IsStatic())
		{
			InvokeOnPort->SetVarType(ScriptVariable::VT_Class);
		}
		else
		{
			InvokeOnPort->SetVarType(ScriptVariable::VT_Obj);
		}
	}

	UpdateInvokeOnLabel();
}

void FunctionNode::InitPortForParam (GraphVariable* var, bool isInParam)
{
	CHECK(var != nullptr)

	DString labelName;
	if (var->IsParamNameVisible())
	{
		if (!var->ReadDisplayName().IsEmpty())
		{
			labelName = var->GetDisplayName();
		}
		else
		{
			labelName = var->ReadVariableName().ToString();
		}
	}

	SInitPort initData(isInParam, VariablePort::SStaticClass(), labelName);
	initData.TooltipText = var->GetComment();

	if (isInParam && var->HasInlineComp())
	{
		initData.InlinedComponentClass = var->GetInlinedComponentClass();
	}

	if (SPort* portData = CreatePort(initData))
	{
		if (VariablePort* varPort = dynamic_cast<VariablePort*>(portData->Port))
		{
			varPort->SetAcceptMultiConnections(!isInParam);
			varPort->SetVarType(var->GetVarType());
			varPort->SetClassConstraint(var->GetClassConstraint());
		}

		if (portData->PortInlineEdit != nullptr)
		{
			var->InitializeInlinedComponent(portData->PortInlineEdit, isInParam, LabelFontSize);
		}
	}

	SPortVarData& newVarData = PortVarData.emplace_back();
	newVarData.IsInputPort = isInParam;
	newVarData.PortIdx = (isInParam) ? InputPorts.size() - 1 : OutputPorts.size() - 1;
	newVarData.VarType = var->GetVarType();
}

GraphNode::SPort* FunctionNode::GetPortData (const SPortVarData& varData)
{
	std::vector<SPort>& portList = (varData.IsInputPort) ? InputPorts : OutputPorts;
	if (ContainerUtils::IsValidIndex(portList, varData.PortIdx))
	{
		return &portList.at(varData.PortIdx);
	}

	return nullptr;
}

bool FunctionNode::IsMatchingFunctionId (const DString& functionId) const
{
	return (!FunctionId.IsEmpty() && FunctionId.Compare(functionId, DString::CC_CaseSensitive) == 0);
}

void FunctionNode::UpdateInvokeOnLabel ()
{
	if (InvokeOnLabel == nullptr)
	{
		return;
	}

	GraphFunction* func = FindGraphFunction();

	DString invokeText(DString::EmptyString);
	if (func != nullptr && InvokeOnPort != nullptr)
	{
		if (GraphAsset* asset = func->GetOwningAsset())
		{
			if (GraphFunction* constructedFunc = GetConstructedFunction())
			{
				if (ContainerUtils::IsEmpty(InvokeOnPort->ReadConnections()) && asset == constructedFunc->GetOwningAsset())
				{
					TextTranslator* translator = TextTranslator::GetTranslator();
					CHECK(translator != nullptr)

					invokeText = translator->TranslateText(TXT("Self"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("FunctionNode")) + TXT(" ");
				}
			}

			if (InvokeOnTextOverride.IsEmpty())
			{
				invokeText += asset->GetPresentedName();
			}
			else
			{
				invokeText += InvokeOnTextOverride;
			}
		}
	}

	InvokeOnLabel->SetText(invokeText);

	if (InvokeOnBackground != nullptr)
	{
		Float totalWidth = PortBaseSize + (PortHorizontalSpacing*2.f) + InvokeOnLabel->ReadSize().X;
		InvokeOnBackground->EditSize().X = totalWidth;

		if (InvokeOnEdgeTransform != nullptr)
		{
			InvokeOnEdgeTransform->EditPosition().X = totalWidth;
		}
	}
}

void FunctionNode::RepairPorts (bool repairInput)
{
	//Cache the existing port connections.
	std::vector<SCachedPortConnection> prevConnections;

	for (const SPortVarData& portData : PortVarData)
	{
		if (portData.IsInputPort != repairInput)
		{
			continue;
		}

		if (SPort* oldPort = GetPortData(portData))
		{
			if (oldPort->Port == nullptr || oldPort->PortLabel == nullptr)
			{
				continue;
			}

			//If nothing is connecting to this port, there's nothing to cache
			if (ContainerUtils::IsEmpty(oldPort->Port->ReadConnections()))
			{
				continue;
			}

			SCachedPortConnection& newPrevConnection = prevConnections.emplace_back();
			newPrevConnection.bIsInParam = portData.IsInputPort;
			newPrevConnection.PortLabel = oldPort->PortLabel->GetContent();
			newPrevConnection.PortType = portData.VarType;

			for (NodePort* otherConnection : oldPort->Port->ReadConnections())
			{
				newPrevConnection.OtherConnections.push_back(otherConnection);
			}
		}
	}

	//Remove all param ports
	SetAutoUpdateSize(false);
	if (!ContainerUtils::IsEmpty(PortVarData))
	{
		size_t i = PortVarData.size() - 1;

		while (true)
		{
			SPortVarData& varPort = PortVarData.at(i);
			if (varPort.IsInputPort == repairInput)
			{
				if (!RemovePort(varPort.IsInputPort, varPort.PortIdx))
				{
					EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to remove port idx %s from FunctionNode. Somehow the PortVarData array is out of sync from the GraphNode's ports."), Int(varPort.PortIdx));
					break;
				}
			}

			if (i == 0)
			{
				break;
			}

			--i;
		}
	}

	GraphFunction* graphFunc = FindGraphFunction();
	if (graphFunc != nullptr)
	{
		if (repairInput)
		{
			for (GraphVariable* inputParam : graphFunc->EditInParams())
			{
				if (inputParam != nullptr)
				{
					InitPortForParam(inputParam, true);
				}
			}
		}
		else
		{
			for (GraphVariable* outputParam : graphFunc->EditOutParams())
			{
				if (outputParam != nullptr)
				{
					InitPortForParam(outputParam, false);
				}
			}
		}
	}

	SetAutoUpdateSize(true);

	//Try to restablish connections. Ports of matching name and type are reconnected.
	for (size_t i = 0; i < prevConnections.size(); ++i)
	{
		bool portRestored = false;
			
		std::vector<SPort>& portList = prevConnections.at(i).bIsInParam ? InputPorts : OutputPorts;
		for (SPort& port : portList)
		{
			VariablePort* varPort = dynamic_cast<VariablePort*>(port.Port);
			if (varPort == nullptr || port.PortLabel == nullptr)
			{
				continue;
			}

			if (varPort->GetVarType() != prevConnections.at(i).PortType || port.PortLabel->GetContent().Compare(prevConnections.at(i).PortLabel, DString::CC_CaseSensitive) != 0)
			{
				continue;
			}

			portRestored = true;
			for (NodePort* portToConnect : prevConnections.at(i).OtherConnections)
			{
				varPort->EstablishConnectionTo(portToConnect);
			}

			break;
		}

		if (portRestored)
		{
			prevConnections.erase(prevConnections.begin() + i);
		}
	}

	if (!ContainerUtils::IsEmpty(prevConnections) && GetOwner() != nullptr)
	{
		//The remaining ports will move to a temporary node that indicates that these connections need attention.
		GraphNode* inputNode = nullptr;
		GraphNode* outputNode = nullptr;

		for (size_t i = 0; i < prevConnections.size(); ++i)
		{
			if (prevConnections.at(i).bIsInParam && inputNode == nullptr)
			{
				inputNode = CreateStaleConnectionNode(ReadSize() * -0.5f);
			}
			else if (!prevConnections.at(i).bIsInParam && outputNode == nullptr)
			{
				outputNode = CreateStaleConnectionNode(ReadSize() * Vector2(0.5f, -0.5f));
			}

			GraphNode* staleNode = (prevConnections.at(i).bIsInParam) ? inputNode : outputNode;
			CHECK(staleNode != nullptr)

			SInitPort stalePort(prevConnections.at(i).bIsInParam, VariablePort::SStaticClass(), prevConnections.at(i).PortLabel);
			if (SPort* newPortData = staleNode->CreatePort(stalePort))
			{
				if (VariablePort* varPort = dynamic_cast<VariablePort*>(newPortData->Port))
				{
					varPort->SetVarType(prevConnections.at(i).PortType);
					for (NodePort* otherConnection : prevConnections.at(i).OtherConnections)
					{
						varPort->EstablishConnectionTo(otherConnection);
					}
				}
			}
		}
	}
}

GraphNode* FunctionNode::CreateStaleConnectionNode (const Vector2& locationOffset)
{
	CHECK(GetOwner() != nullptr)

	ErrorNode* result = ErrorNode::CreateObject();
	if (GetOwner()->AddComponent(result))
	{
		result->SetPosition(ReadPosition() + locationOffset);
		result->SetErrorType(ErrorNode::ET_StaleConnections);
	}

	return result;
}

void FunctionNode::UnbindFunction ()
{
	FunctionId = DString::EmptyString;
}

void FunctionNode::HandleInvokeOnPortConnectionChanged (NodePort* changedPort)
{
	UpdateInvokeOnLabel();

	if (bAutoUpdateSize)
	{
		UpdateNodeSize();
	}
}

void FunctionNode::HandleFunctionNameChanged (const DString& prevFuncId, const DString& newFuncId, const DString& newName)
{
	if (IsMatchingFunctionId(prevFuncId))
	{
		FunctionId = newFuncId;
		SetHeaderText(newName);
	}
}

void FunctionNode::HandleFunctionCommentChanged (const DString& funcId, const DString& newComment)
{
	if (HeaderText != nullptr && IsMatchingFunctionId(funcId))
	{
		TooltipComponent* tooltip = dynamic_cast<TooltipComponent*>(HeaderText->FindComponent(TooltipComponent::SStaticClass()));
		if (tooltip == nullptr && !newComment.IsEmpty())
		{
			tooltip = TooltipComponent::CreateObject();
			HeaderText->AddComponent(tooltip);
		}

		if (tooltip != nullptr)
		{
			tooltip->SetTooltipText(newComment);
		}
	}
}

void FunctionNode::HandleFunctionParamNameChanged (const DString& functionId, const DString& prevName, const DString& newName)
{
	if (!IsMatchingFunctionId(functionId))
	{
		return;
	}

	//Find the port associated with the previous name.
	for (const SPortVarData& varData : PortVarData)
	{
		SPort* portData = GetPortData(varData);
		if (portData == nullptr || portData->PortLabel == nullptr)
		{
			continue;
		}

		if (portData->PortLabel->GetContent().Compare(prevName, DString::CC_CaseSensitive) == 0)
		{
			//Found the associated port
			portData->PortLabel->SetText(newName);

			if (bAutoUpdateSize)
			{
				UpdateNodeSize();
			}

			break;
		}
	}
}

void FunctionNode::HandleFunctionParamsChanged (const DString& funcId)
{
	if (IsMatchingFunctionId(funcId))
	{
		RepairPorts(true);
		RepairPorts(false);
	}
}

void FunctionNode::HandleVarNameChanged (const DString& prevId, const DString& varId, const DString& newName)
{
	if (VarId.IsEmpty())
	{
		return;
	}

	if (VarId.Compare(prevId, DString::CC_CaseSensitive) == 0)
	{
		VarId = varId;
		
		if (HeaderText != nullptr)
		{
			if (GraphFunction* func = FindGraphFunction())
			{
				DString newHeader = func->GetPresentedName() + TXT(" ") + newName;
				HeaderText->SetText(newHeader);

				if (bAutoUpdateSize)
				{
					UpdateNodeSize();
				}
			}
		}
	}
}
SD_END