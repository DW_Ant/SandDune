/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableFloat.cpp
=====================================================================
*/

#include "EditableFloat.h"

IMPLEMENT_CLASS(SD::EditableFloat, SD::EditableField)
SD_BEGIN

EditableFloat::SInspectorFloat::SInspectorFloat (const DString& inPropertyName, const DString& inTooltipText, Float* inAssociatedVariable, Float inDefaultValue) : SInspectorProperty(inPropertyName, inTooltipText),
	bHasMinValue(false),
	bHasMaxValue(false),
	bEnableDecimal(true),
	ValueRange(0.f, 1.f),
	AssociatedVariable(inAssociatedVariable),
	DefaultValue(inDefaultValue),
	InitOverride(inDefaultValue)
{
	//Noop
}


void EditableFloat::SInspectorFloat::LoadConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot load %s from config since the Inspector struct does not have a variable associated with it."), PropertyName);
		return;
	}

	DString configStr = config->GetPropertyText(sectionName, PropertyName);
	if (configStr.IsEmpty())
	{
		*AssociatedVariable = DefaultValue;
	}
	else
	{
		AssociatedVariable->ParseString(configStr);
	}
}

void EditableFloat::SInspectorFloat::SaveConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to save %s to config since there isn't a variable associated with it."), PropertyName);
		return;
	}

	config->SaveProperty(sectionName, PropertyName, *AssociatedVariable);
}

bool EditableFloat::SInspectorFloat::LoadBinary (const DataBuffer& incomingData)
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary since the SInspectorFloat is not associated with a variable."), PropertyName);
		return false;
	}

	if (!incomingData.CanReadBytes(Float::SGetMinBytes()))
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to read %s from binary. Not enough bytes in the data buffer."), PropertyName);
		return false;
	}

	if ((incomingData >> *AssociatedVariable).HasReadError())
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to load %s from binary. The data is malformed, preventing the buffer from reading a Float."), PropertyName);
		return false;
	}

	return true;
}

void EditableFloat::SInspectorFloat::SaveBinary (DataBuffer& outData) const
{
	if (AssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot save %s to binary since it's not associated with any variable."), PropertyName);
		return;
	}

	outData << *AssociatedVariable;
}

EditPropertyComponent* EditableFloat::SInspectorFloat::CreateBlankEditableComponent () const
{
	return EditableFloat::CreateObject();
}

void EditableFloat::SInspectorFloat::EditPropComponent () const
{
	SInspectorProperty::EditPropComponent();

	if (EditableFloat* floatComp = dynamic_cast<EditableFloat*>(AssociatedComp))
	{
		floatComp->SetHasMinValue(bHasMinValue);
		floatComp->SetHasMaxValue(bHasMaxValue);
		floatComp->SetEnableDecimal(bEnableDecimal);
		floatComp->SetMinValue(ValueRange.Min);
		floatComp->SetMaxValue(ValueRange.Max);
		floatComp->BindVariable(AssociatedVariable);
		floatComp->SetDefaultValue(DefaultValue);

		if (AssociatedVariable == nullptr)
		{
			if (TextFieldComponent* textField = floatComp->GetPropValueField())
			{
				if (InitType == IIV_UseOverride)
				{
					textField->SetText(InitOverride.ToString());
				}
				else
				{
					textField->SetText(DefaultValue.ToString());
				}
			}
		}
	}
}

void EditableFloat::InitProps ()
{
	Super::InitProps();

	bHasMinValue = false;
	bHasMaxValue = false;
	bEnableDecimal = true;
	MinValue = -100.f;
	MaxValue = 100.f;
	EditedFloat = nullptr;
	DefaultFloat = 0.f;

	UpdateFloatTick = nullptr;
}

void EditableFloat::BeginObject ()
{
	Super::BeginObject();

	UpdateFloatTick = TickComponent::CreateObject(TICK_GROUP_EDITOR);
	if (AddComponent(UpdateFloatTick))
	{
		UpdateFloatTick->SetTickInterval(0.2f); //Not important to refresh continuously.
		UpdateFloatTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditableFloat, HandleUpdateFloatTick, void, Float));
	}
}

void EditableFloat::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const EditableFloat* floatTemplate = dynamic_cast<const EditableFloat*>(objTemplate))
	{
		SetHasMinValue(floatTemplate->HasMinValue());
		SetHasMaxValue(floatTemplate->HasMaxValue());
		SetEnableDecimal(floatTemplate->IsDecimalEnabled());
		SetMinValue(floatTemplate->MinValue);
		SetMaxValue(floatTemplate->MaxValue);
	}
}

void EditableFloat::CopyToBuffer (DataBuffer& copyTo) const
{
	Super::CopyToBuffer(OUT copyTo);

	if (PropValueField != nullptr)
	{
		DString text = PropValueField->GetContent();
		Float floatData = text.Stof();
		copyTo << floatData;
	}
}

bool EditableFloat::CopyFromBuffer (const DataBuffer& incomingData)
{
	if (!Super::CopyFromBuffer(incomingData))
	{
		return false;
	}

	Float incomingFloat;
	if ((incomingData >> incomingFloat).HasReadError())
	{
		return false;
	}

	if (PropValueField != nullptr)
	{
		PropValueField->SetText(incomingFloat.ToString());
	}

	if (EditedFloat != nullptr)
	{
		(*EditedFloat) = incomingFloat;
	}

	return true;
}

void EditableFloat::BindToLocalVariable ()
{
	Super::BindToLocalVariable();

	LocalFloat = 0.f;
	if (PropValueField != nullptr)
	{
		LocalFloat = PropValueField->GetContent().Stof();
	}

	EditedFloat = &LocalFloat;
}

EditPropertyComponent::SInspectorProperty* EditableFloat::CreateDefaultInspector (const DString& propName) const
{
	return new SInspectorFloat(propName, DString::EmptyString, nullptr);
}

void EditableFloat::RemoveVarUpdateTickComponent ()
{
	Super::RemoveVarUpdateTickComponent();

	if (UpdateFloatTick != nullptr)
	{
		UpdateFloatTick->Destroy();
		UpdateFloatTick = nullptr;
	}
}

void EditableFloat::ResetToDefaults ()
{
	Super::ResetToDefaults();

	if (EditedFloat != nullptr)
	{
		*EditedFloat = DefaultFloat;
	}
	else if (PropValueField.IsValid())
	{
		PropValueField->SetText(DefaultFloat.ToString());
	}

	MarkPropertyTextDirty();
	ProcessApplyEdit(this);
}

void EditableFloat::UnbindVariable ()
{
	Super::UnbindVariable();

	BindVariable(nullptr);
}

DString EditableFloat::ConstructValueAsText () const
{
	//Display the float with only 2 decimal places since the struct header would be sharing space with other properties.
	if (EditedFloat == nullptr)
	{
		return DString::EmptyString;
	}

	return EditedFloat->ToFormattedString(1, 2, Float::TM_Round);
}

void EditableFloat::ApplyEdits ()
{
	if (PropValueField.IsValid() && !IsReadOnly())
	{
		ApplyClamps();
		Float newValue(PropValueField->GetContent());

		if (EditedFloat != nullptr)
		{
			*EditedFloat = newValue;
			if (ResetDefaultsButton.IsValid())
			{
				ResetDefaultsButton->SetEnabled(DefaultFloat != *EditedFloat);
				if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
				{
					tooltipEntity->HideTooltip();
				}
			}
		}
		else if (ResetDefaultsButton.IsValid())
		{
			Float newEdit;
			newEdit.ParseString(PropValueField->GetContent());
			ResetDefaultsButton->SetEnabled(DefaultFloat != newEdit);
			if (TooltipGuiEntity* tooltipEntity = GuiUtils::FindEntityInSameWindowAs<TooltipGuiEntity>(this))
			{
				tooltipEntity->HideTooltip();
			}
		}
	}
}

DString EditableFloat::GetPermittedInputChars () const
{
	DString permittedChars = TXT("[0-9");
	if (PropValueField != nullptr)
	{
		bool editingFirstPos = (PropValueField->GetCursorPosition() == 0 || PropValueField->GetHighlightEndPosition() == 0);
		bool isEmpty = ContainerUtils::IsEmpty(PropValueField->ReadContent()) || PropValueField->ReadContent().at(0).IsEmpty();
		if (editingFirstPos &&
			(isEmpty || PropValueField->GetHighlightEndPosition() >= 0 || PropValueField->ReadContent().at(0).At(0) != '-'))
		{
			//If inserting at the beginning of field and if the first character is not already a negative sign, permit the negative sign
			permittedChars += TXT("-");
		}

		if (IsDecimalEnabled() && (isEmpty || PropValueField->ReadContent().at(0).Find(TXT("."), 0, DString::CC_CaseSensitive) == INT_INDEX_NONE))
		{
			//If decimals are enabled and there isn't a decimal in the field already, then permit it.
			permittedChars += TXT(".");
		}
	}

	permittedChars += TXT("]");
	return permittedChars;
}

void EditableFloat::BindVariable (Float* newEditedFloat)
{
	EditedFloat = newEditedFloat;
	MarkPropertyTextDirty();

	if (PropValueField.IsValid())
	{
		if (EditedFloat != nullptr)
		{
			//Clamps don't apply when reading from the float.
			PropValueField->SetText(EditedFloat->ToString());
		}
		else
		{
			PropValueField->SetText(DString::EmptyString);
		}
	}
}

void EditableFloat::SetHasMinValue (bool bNewHasMinValue)
{
	bHasMinValue = bNewHasMinValue;
	ApplyClamps();
}

void EditableFloat::SetHasMaxValue (bool bNewHasMaxValue)
{
	bHasMaxValue = bNewHasMaxValue;
	ApplyClamps();
}

void EditableFloat::SetEnableDecimal (bool bNewEnableDecimal)
{
	bEnableDecimal = bNewEnableDecimal;
}

void EditableFloat::SetMinValue (Float newMinValue)
{
	MinValue = newMinValue;
	ApplyClamps();
}

void EditableFloat::SetMaxValue (Float newMaxValue)
{
	MaxValue = newMaxValue;
	ApplyClamps();
}

void EditableFloat::SetDefaultValue (Float newDefaultFloat)
{
	DefaultFloat = newDefaultFloat;

	if (ResetDefaultsButton.IsNullptr())
	{
		AddResetToDefaultsControl();
	}

	if (ResetDefaultsButton.IsValid() && EditedFloat != nullptr)
	{
		ResetDefaultsButton->SetEnabled(!IsReadOnly() && DefaultFloat != *EditedFloat);
	}
}

void EditableFloat::ApplyClamps ()
{
	Float newValue(PropValueField->GetContent());
	if (!bEnableDecimal)
	{
		newValue.RoundInline();
	}

	if (HasMinValue())
	{
		newValue = Utils::Max(MinValue, newValue);
	}

	if (HasMaxValue())
	{
		newValue = Utils::Min(newValue, MaxValue);
	}

	MarkPropertyTextDirty();
	PropValueField->SetText(newValue.ToString());
}

void EditableFloat::HandleUpdateFloatTick (Float deltaSec)
{
	//Don't update the text field if the user is currently editing it.
	if (PropValueField.IsValid() && PropValueField->GetCursorPosition() < 0 && EditedFloat != nullptr)
	{
		if (!PropValueField->GetContent().Stof().IsCloseTo(*EditedFloat))
		{
			MarkPropertyTextDirty();
			PropValueField->SetText(EditedFloat->ToString());
		}
	}
}
SD_END