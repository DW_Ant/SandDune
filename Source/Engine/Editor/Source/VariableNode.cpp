/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariableNode.cpp
=====================================================================
*/

#include "EditorEngineComponent.h"
#include "GraphAsset.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "GraphVariable.h"
#include "NodePort.h"
#include "VariableNode.h"
#include "VariablePort.h"

IMPLEMENT_CLASS(SD::VariableNode, SD::FunctionNode)
SD_BEGIN

//Add a @ before local to prevent possible name conflict with an asset named 'local'. @ symbol is not permitted in asset names.
const DString VariableNode::LOCAL_SCOPE(TXT("@local"));

void VariableNode::InitProps ()
{
	Super::InitProps();

	VariableId = DString::EmptyString;
	VarDisplayName = DString::EmptyString;
	bLocalVariable = false;
	VarType = ScriptVariable::VT_Unknown;
	ConstraintClass = nullptr;
	EditNodeSize = Vector2(256.f, 64.f);
	ErrorBackgroundColor = Color(129, 32, 28, 200);
	PromptBackgroundColor = Color(128, 175, 225, 200);

	CommonVarHighlightTransform = nullptr;
	VarCommentTooltip = nullptr;
	PopupBackground = nullptr;
}

void VariableNode::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	if (const VariableNode* varTemplate = dynamic_cast<const VariableNode*>(objTemplate))
	{
		SetEditNodeSize(varTemplate->ReadEditNodeSize());
		SetErrorBackgroundColor(varTemplate->GetErrorBackgroundColor());
		SetPromptBackgroundColor(varTemplate->GetPromptBackgroundColor());

		GuiComponent* compOwner = this;
		if (Background != nullptr)
		{
			compOwner = Background.Get();
		}

		bool bCreatedObj;
		EditButton = ReplaceTargetWithObjOfMatchingClass(EditButton.Get(), varTemplate->EditButton.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			compOwner->AddComponent(EditButton);
		}

		if (EditButton != nullptr)
		{
			EditButton->CopyPropertiesFrom(varTemplate->EditButton.Get());
		}

		RenameVarField = ReplaceTargetWithObjOfMatchingClass(RenameVarField.Get(), varTemplate->RenameVarField.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			compOwner->AddComponent(RenameVarField);
		}

		if (RenameVarField != nullptr)
		{
			RenameVarField->CopyPropertiesFrom(varTemplate->RenameVarField.Get());
		}

		SelectVarDropdown = ReplaceTargetWithObjOfMatchingClass(SelectVarDropdown.Get(), varTemplate->SelectVarDropdown.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			compOwner->AddComponent(SelectVarDropdown);
		}

		if (SelectVarDropdown != nullptr)
		{
			SelectVarDropdown->CopyPropertiesFrom(varTemplate->SelectVarDropdown.Get());
		}

		VarCommentTooltip = ReplaceTargetWithObjOfMatchingClass(VarCommentTooltip, varTemplate->VarCommentTooltip, OUT bCreatedObj);
		if (bCreatedObj)
		{
			compOwner = this;
			if (HeaderText != nullptr)
			{
				compOwner = HeaderText;
			}

			compOwner->AddComponent(VarCommentTooltip);
		}

		if (VarCommentTooltip != nullptr)
		{
			VarCommentTooltip->CopyPropertiesFrom(varTemplate->VarCommentTooltip);
		}

		CommonVarHighlight = ReplaceTargetWithObjOfMatchingClass(CommonVarHighlight.Get(), varTemplate->CommonVarHighlight.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(CommonVarHighlight);
		}

		if (CommonVarHighlight != nullptr)
		{
			CommonVarHighlight->CopyPropertiesFrom(varTemplate->CommonVarHighlight.Get());
		}
	}
}

void VariableNode::ExecuteInput (const sf::Event& evnt)
{
	Super::ExecuteInput(evnt);

	if (EditButton == nullptr || EditButton->IsVisible())
	{
		return;
	}

	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		ExitEditMode();
	}
}

void VariableNode::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (EditButton == nullptr || EditButton->IsVisible())
	{
		return;
	}

	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonReleased && !IsWithinBounds(mouse->ReadPosition()))
	{
		if (SelectVarDropdown != nullptr && SelectVarDropdown->IsExpanded() && SelectVarDropdown->GetExpandMenuScrollbar() != nullptr && SelectVarDropdown->GetExpandMenuScrollbar()->IsWithinBounds(mouse->ReadPosition()))
		{
			//User is clicking inside the select var dropdown options (which is outside the node's boundaries). Do not exit out of edit mode.
			return;
		}

		//Apply changes if possible. Otherwise cancel.
		if (RenameVarField != nullptr)
		{
			SetLocalVariable(RenameVarField->GetContent());
		}
		else
		{
			ExitEditMode();
		}
	}
}

void VariableNode::SetSelected (bool newSelected)
{
	Super::SetSelected(newSelected);

	if (VariableId.IsEmpty())
	{
		return;
	}

	//Find all other VariableNodes of matching var, and update their highlight visibility.
	std::vector<VariableNode*> associatedNodes;
	FindAllAssociatedNodes(true, VariableId, OUT associatedNodes);

	Int numSelected = 0;
	for (VariableNode* node : associatedNodes)
	{
		if (node->IsSelected())
		{
			++numSelected;
		}
	}

	for (VariableNode* node : associatedNodes)
	{
		if (PlanarTransformComponent* comp = node->CommonVarHighlightTransform)
		{
			comp->SetVisibility(numSelected > 0);
		}
	}
}

void VariableNode::SetHighlightThickness (Float newHighlightThickness)
{
	Super::SetHighlightThickness(newHighlightThickness);

	if (EditButton != nullptr)
	{
		EditButton->SetAnchorTop(HighlightThickness);
		EditButton->SetAnchorRight(HighlightThickness);
	}

	if (RenameVarField != nullptr)
	{
		RenameVarField->SetAnchorTop(HighlightThickness);
		RenameVarField->SetAnchorRight(HighlightThickness * 2.f);
		RenameVarField->SetAnchorLeft(HighlightThickness * 2.f);

		if (SelectVarDropdown != nullptr)
		{
			Float dropdownMargin = (HighlightThickness + PortHorizontalSpacing + PortBaseSize) * 2.f;
			SelectVarDropdown->SetPosition(0.f, RenameVarField->ReadPosition().Y + RenameVarField->ReadSize().Y);
			SelectVarDropdown->SetAnchorRight(dropdownMargin);
			SelectVarDropdown->SetAnchorLeft(dropdownMargin);
		}
	}

	if (CommonVarHighlightTransform != nullptr)
	{
		CommonVarHighlightTransform->SetAnchorTop(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorRight(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorBottom(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorLeft(HighlightThickness);
	}
}

void VariableNode::BindFunction (GraphFunction* newAssociatedFunction)
{
	Super::BindFunction(newAssociatedFunction);

	VarType = ScriptVariable::VT_Unknown;

	if (newAssociatedFunction != nullptr)
	{
		//Set the type based on the associated in or out parameter
		if (!ContainerUtils::IsEmpty(newAssociatedFunction->ReadInParams()) && newAssociatedFunction->ReadInParams().at(0) != nullptr)
		{
			VarType = newAssociatedFunction->ReadInParams().at(0)->GetVarType();
		}
		else if (!ContainerUtils::IsEmpty(newAssociatedFunction->ReadOutParams()) && newAssociatedFunction->ReadOutParams().at(0) != nullptr)
		{
			VarType = newAssociatedFunction->ReadOutParams().at(0)->GetVarType();
		}
	}

	UpdateHeaderText();
}

void VariableNode::InitializeComponents ()
{
	Super::InitializeComponents();

	Float headerHeight = 32.f;
	if (HeaderText != nullptr)
	{
		HeaderText->SetHorizontalAlignment(LabelComponent::HA_Left); //Shift to the left since the edit button will occupy its right-side.
		headerHeight = HeaderText->ReadSize().Y;

		VarCommentTooltip = TooltipComponent::CreateObject();
		if (HeaderText->AddComponent(VarCommentTooltip))
		{
			//Noop
		}
	}

	GuiComponent* compOwner = this;
	if (Background != nullptr)
	{
		compOwner = Background.Get();
	}

	EditButton = ButtonComponent::CreateObject();
	if (compOwner->AddComponent(EditButton))
	{
		Float buttonSize = headerHeight * 0.67f;
		EditButton->SetAnchorTop(HighlightThickness);
		EditButton->SetAnchorRight(HighlightThickness);
		EditButton->SetSize(buttonSize, buttonSize);
		EditButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, VariableNode, HandleEditReleased, void, ButtonComponent*));
	}

	RenameVarField = TextFieldComponent::CreateObject();
	if (compOwner->AddComponent(RenameVarField))
	{
		RenameVarField->SetAutoRefresh(false);
		RenameVarField->SetAnchorTop(HighlightThickness);
		RenameVarField->SetAnchorLeft(HighlightThickness * 2.f);
		RenameVarField->SetAnchorRight(HighlightThickness * 2.f);
		RenameVarField->SetSize(headerHeight, headerHeight);
		RenameVarField->SetHorizontalAlignment(LabelComponent::HA_Center);
		RenameVarField->SetVerticalAlignment(LabelComponent::VA_Center);
		RenameVarField->SetSingleLine(true);
		RenameVarField->OnAllowTextInput = SDFUNCTION_1PARAM(this, VariableNode, HandleAllowVarRenameInput, bool, const DString&);
		RenameVarField->OnReturn = SDFUNCTION_1PARAM(this, VariableNode, HandleApplyVarRename, void, TextFieldComponent*);
		RenameVarField->SetVisibility(false);
		RenameVarField->SetAutoRefresh(true);

		SelectVarDropdown = DropdownComponent::CreateObject();
		if (compOwner->AddComponent(SelectVarDropdown))
		{
			Float dropdownMargin = (HighlightThickness + PortHorizontalSpacing + PortBaseSize) * 2.f;
			SelectVarDropdown->SetAnchorLeft(dropdownMargin);
			SelectVarDropdown->SetAnchorRight(dropdownMargin);
			SelectVarDropdown->SetPosition(0.f, RenameVarField->ReadPosition().Y + RenameVarField->ReadSize().Y);
			SelectVarDropdown->SetSize(headerHeight, headerHeight * 8.f);
			SelectVarDropdown->SetItemSelectionHeight(headerHeight);
			SelectVarDropdown->SetEnableSearchBar(false);
			SelectVarDropdown->SetVisibility(false);
			SelectVarDropdown->SetShowExpandMenuOnTop(false); //Show in the editor since the transform is not calculated correctly in the DropdownGuiEntity since this dropdown component doesn't reside in a scrollbar. In addition to that, there could be multiple viewports rendering this component.
			SelectVarDropdown->OnExpanded = SDFUNCTION(this, VariableNode, HandleSelectVarExpanded, void);
			SelectVarDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, VariableNode, HandleSelectVar, void, DropdownComponent*, Int);
		}
	}

	CommonVarHighlightTransform = PlanarTransformComponent::CreateObject();
	if (compOwner->AddComponent(CommonVarHighlightTransform))
	{
		CommonVarHighlightTransform->SetAnchorTop(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorRight(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorBottom(HighlightThickness);
		CommonVarHighlightTransform->SetAnchorLeft(HighlightThickness);
		CommonVarHighlightTransform->SetVisibility(false);

		ColorRenderComponent* colorRender = ColorRenderComponent::CreateObject();
		if (CommonVarHighlightTransform->AddComponent(colorRender))
		{
			colorRender->SolidColor = Color(138, 161, 238, 64);
			CommonVarHighlight = colorRender;
		}
	}
}

void VariableNode::CalcNodeSize (Vector2& outNodeSize) const
{
	Super::CalcNodeSize(OUT outNodeSize);

	if (EditButton != nullptr)
	{
		//Add a bit extra to the width to accomodate the button size.
		outNodeSize.X += EditButton->ReadSize().X;
	}
}

void VariableNode::Destroyed ()
{
	if (!IsLocalVariable() && !VariableId.IsEmpty())
	{
		if (GraphVariable* memVar = FindGraphVariable(VariableId))
		{
			SDFunction<void, const DString&, const DString&, const DString&> nameChangedDelegate = SDFUNCTION_3PARAM(this, VariableNode, HandleNameChanged, void, const DString&, const DString&, const DString&);
			if (memVar->OnNameChanged.IsRegistered(nameChangedDelegate, true))
			{
				memVar->OnNameChanged.UnregisterHandler(nameChangedDelegate);
			}
		}
	}

	Super::Destroyed();
}

GraphVariable* VariableNode::FindGraphVariable (const DString& varId) const
{
	if (varId.IsEmpty())
	{
		return nullptr;
	}

	GraphFunction* owningFunction = GetConstructedFunction();
	if (owningFunction == nullptr)
	{
		return nullptr;
	}

	DString scope;
	DString varName;
	if (!GraphVariable::ParseVariableId(varId, OUT scope, OUT varName))
	{
		return nullptr;
	}

	if (scope.Compare(LOCAL_SCOPE, DString::CC_CaseSensitive) == 0)
	{
		//This is a local variable. Not a member variable.
		return nullptr;
	}

	for (GraphAsset* asset = owningFunction->GetOwningAsset(); asset != nullptr; asset = asset->GetSuperAsset())
	{
		if (asset->ReadAssetName().Compare(scope, DString::CC_CaseSensitive) != 0)
		{
			continue; //Scope doesn't match. It must reside higher up in the inheritance chain.
		}

		HashedString hashedName(varName);
		for (GraphVariable* var : asset->ReadMemberVariables())
		{
			if (var != nullptr && var->ReadVariableName() == hashedName)
			{
				return var;
			}
		}

		break;
	}

	return nullptr;
}

void VariableNode::SetAssociatedVariable (GraphVariable* newAssociatedVariable)
{
	if (newAssociatedVariable == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot associate a VariableNode to a nullptr member variable. If there's a need to use a local variable, invoke SetLocalVariable instead."));
		return;
	}

	DString newVarName = newAssociatedVariable->GetIdentifyingName();
	if (newVarName.IsEmpty() || VariableId.Compare(newVarName, DString::CC_CaseSensitive) == 0)
	{
		return;
	}

	if (newAssociatedVariable != nullptr && !IsSameTypeAs(newAssociatedVariable))
	{
		DisplayErrorMessage(TXT("ErrorTypeConflict"));
		return;
	}

	if (VarCommentTooltip != nullptr)
	{
		VarCommentTooltip->SetTooltipText(newAssociatedVariable->ReadComment());
	}

	SDFunction<void, const DString&, const DString&, const DString&> onNameChanged = SDFUNCTION_3PARAM(this, VariableNode, HandleNameChanged, void, const DString&, const DString&, const DString&);
	if (GraphVariable* prevVar = FindGraphVariable(VariableId))
	{
		if (prevVar->OnNameChanged.IsRegistered(onNameChanged, true))
		{
			prevVar->OnNameChanged.UnregisterHandler(onNameChanged);
		}
	}

	newAssociatedVariable->OnNameChanged.RegisterHandler(onNameChanged);

	bLocalVariable = false;
	VariableId = newVarName;
	VarDisplayName = newAssociatedVariable->GetPresentedName();

	UpdateHeaderText();
}

bool VariableNode::SetLocalVariable (const DString& newName)
{
	DString newIdName = GraphVariable::GetIdentifyingName(LOCAL_SCOPE, newName);
	if (VariableId.Compare(newIdName, DString::CC_CaseSensitive) == 0)
	{
		ExitEditMode();
		return true;
	}

	if (!newName.IsEmpty())
	{
		//Check if there's another local variable but the types do not match up.
		for (ComponentIterator iter(GetOwner(), false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (VariableNode* varNode = dynamic_cast<VariableNode*>(iter.GetSelectedComponent()))
			{
				if (!varNode->IsLocalVariable() || varNode == this)
				{
					continue;
				}

				if (varNode->VariableId.Compare(newIdName, DString::CC_CaseSensitive) == 0)
				{
					if (!IsSameTypeAs(varNode))
					{
						//Types do not match. Return error.
						DisplayErrorMessage(TXT("ErrorTypeConflict"));
						return false;
					}
				}
			}
		}
	}

	std::vector<VariableNode*> associatedNodes;
	FindAllAssociatedNodes(false, VariableId, OUT associatedNodes);

	bLocalVariable = true;
	LastVarName = VariableId;
	VariableId = newIdName;
	VarDisplayName = newName;

	if (VarCommentTooltip != nullptr)
	{
		VarCommentTooltip->SetTooltipText(DString::EmptyString);
	}

	if (!LastVarName.IsEmpty() && !ContainerUtils::IsEmpty(associatedNodes))
	{
		DisplayUpdateReferencesPrompt(Int(associatedNodes.size()));
	}
	else
	{
		ExitEditMode();
		UpdateHeaderText();
	}

	return true;
}

void VariableNode::SetConstraintClass (GraphAsset* newConstraintClass)
{
	ConstraintClass = newConstraintClass;

	for (SPort& port : InputPorts)
	{
		if (VariablePort* varPort = dynamic_cast<VariablePort*>(port.Port))
		{
			varPort->SetClassConstraint(ConstraintClass);
		}
	}

	for (SPort& port : OutputPorts)
	{
		if (VariablePort* varPort = dynamic_cast<VariablePort*>(port.Port))
		{
			varPort->SetClassConstraint(ConstraintClass);
		}
	}
}

void VariableNode::SetEditNodeSize (const Vector2& newEditNodeSize)
{
	EditNodeSize = newEditNodeSize;
}

void VariableNode::SetErrorBackgroundColor (Color newErrorBackgroundColor)
{
	ErrorBackgroundColor = newErrorBackgroundColor;
}

void VariableNode::SetPromptBackgroundColor (Color newPromptBackgroundColor)
{
	PromptBackgroundColor = newPromptBackgroundColor;
}

bool VariableNode::IsSameTypeAs (VariableNode* otherNode) const
{
	return IsSameTypeAs(otherNode->GetVarType(), otherNode->GetConstraintClass());
}

bool VariableNode::IsSameTypeAs (GraphVariable* var) const
{
	return IsSameTypeAs(var->GetVarType(), var->GetClassConstraint());
}

bool VariableNode::IsSameTypeAs (ScriptVariable::EVarType otherVarType, GraphAsset* otherClassConstraint) const
{
	if (VarType != otherVarType)
	{
		return false;
	}

	if (VarType == ScriptVariable::VT_Obj || VarType == ScriptVariable::VT_Class)
	{
		//Classes must match exactly (can't merge two local variables of different object types).
		return (ConstraintClass == otherClassConstraint);
	}

	return true;
}

void VariableNode::UpdateHeaderText ()
{
	DString headerText = DString::EmptyString;

	GraphFunction* func = FindGraphFunction();
	if (func != nullptr)
	{
		headerText = func->GetPresentedName() + TXT(" ");
	}

	headerText += VarDisplayName;

	SetHeaderText(headerText);
}

void VariableNode::PopulateVariableList ()
{
	CHECK(SelectVarDropdown != nullptr && SelectVarDropdown->GetExpandMenuList() != nullptr)

	std::vector<GraphFunction::SVarInfo> allVars;

	//owningFunction = Function that's using this node. AssociatedFunction = setter/getter
	GraphFunction* owningFunction = GetConstructedFunction();
	if (owningFunction == nullptr)
	{
		SelectVarDropdown->SetVisibility(false);
		return;
	}

	//Add in parameters
	for (GraphVariable* inParam : owningFunction->ReadInParams())
	{
		if (inParam != nullptr && inParam->GetVarType() == VarType)
		{
			allVars.emplace_back(GraphVariable::GetIdentifyingName(LOCAL_SCOPE, inParam->ReadVariableName().ToString()), inParam->GetPresentedName(), inParam->GetVarType());
		}
	}

	owningFunction->FindLocalVariables(VarType, OUT allVars);

	//If the owning function is not a const function, then all member variables are available (since the function has write access)
	//If the function this node is associated with is a readonly (const) function, then display all variables since that function is not going to write to it.
	GraphVariable::EVarPermissions nodePermissions = GraphVariable::VP_Read;
	if (!owningFunction->IsConst())
	{
		nodePermissions |= GraphVariable::VP_Write;
	}
	else
	{
		GraphFunction* associatedFunc = FindGraphFunction();
		if (associatedFunc != nullptr && associatedFunc->IsConst())
		{
			//The associated function is a const, it's safe to access any variable since it's not going to write to it (eg: Getter function).
			nodePermissions |= GraphVariable::VP_Write;
		}
	}

	std::vector<GraphVariable*> memberVars;
	owningFunction->GetAllMemberVariables(nodePermissions, owningFunction->IsStatic(), VarType, OUT memberVars);
	
	std::vector<GuiDataElement<DString>> dropdownSelections;

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString defaultText = translator->TranslateText(TXT("DefaultVariableSelection"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode"));
	dropdownSelections.emplace_back(DString::EmptyString, defaultText);

	//Add local variables to the top of the dropdown (since local vars are most likely accessed in a function)
	for (const GraphFunction::SVarInfo& localVar : allVars)
	{
		dropdownSelections.emplace_back(localVar.VarId, localVar.VarDisplay);
	}

	for (GraphVariable* var : memberVars)
	{
		DString varId = var->GetIdentifyingName();
		dropdownSelections.emplace_back(varId, var->GetPresentedName());
	}

	SelectVarDropdown->GetExpandMenuList()->SetList(dropdownSelections);
	SelectVarDropdown->SetSelectedItemByIdx(0);
}

void VariableNode::DisplayErrorMessage (const DString& localizationKey)
{
	if (PopupBackground != nullptr)
	{
		PopupBackground->Destroy();
		PopupBackground = nullptr;
	}

	GuiComponent* compOwner = this;
	if (Background != nullptr)
	{
		compOwner = Background.Get();
	}

	PopupBackground = FrameComponent::CreateObject();
	if (compOwner->AddComponent(PopupBackground))
	{
		PopupBackground->SetPosition(Vector2::ZERO_VECTOR);
		PopupBackground->SetSize(Vector2(1.f, 1.f));
		if (BorderRenderComponent* borderComp = PopupBackground->GetBorderComp())
		{
			borderComp->Destroy();
		}

		PopupBackground->SetBorderThickness(0.f);
		PopupBackground->SetCenterColor(ErrorBackgroundColor);

		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		LabelComponent* labelComp = LabelComponent::CreateObject();
		if (PopupBackground->AddComponent(labelComp))
		{
			labelComp->SetAutoRefresh(false);
			labelComp->SetPosition(Vector2::ZERO_VECTOR);
			labelComp->SetSize(Vector2(1.f, 0.67f));
			labelComp->SetHorizontalAlignment(LabelComponent::HA_Center);
			labelComp->SetWrapText(true);
			labelComp->SetClampText(false);
			labelComp->SetCharacterSize(10);
			labelComp->SetText(translator->TranslateText(localizationKey, EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode")));
			labelComp->SetAutoRefresh(true);
		}

		ButtonComponent* closeButton = ButtonComponent::CreateObject();
		if (PopupBackground->AddComponent(closeButton))
		{
			Float anchorDist = 0.125f;
			if (SelectVarDropdown != nullptr)
			{
				anchorDist = SelectVarDropdown->GetAnchorLeftDist();
			}

			closeButton->SetPosition(Vector2(0.f, 0.67f));
			closeButton->SetAnchorRight(anchorDist);
			closeButton->SetAnchorLeft(anchorDist);
			closeButton->SetAnchorBottom(0.f);
			closeButton->SetCaptionText(translator->TranslateText(TXT("ConfirmError"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode")));
			closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, VariableNode, HandleCloseError, void, ButtonComponent*));
		}
	}
}

void VariableNode::DisplayUpdateReferencesPrompt (Int numAssociatedNodes)
{
	if (PopupBackground != nullptr)
	{
		PopupBackground->Destroy();
		PopupBackground = nullptr;
	}

	GuiComponent* compOwner = this;
	if (Background != nullptr)
	{
		compOwner = Background.Get();
	}

	PopupBackground = FrameComponent::CreateObject();
	if (compOwner->AddComponent(PopupBackground))
	{
		PopupBackground->SetPosition(Vector2::ZERO_VECTOR);
		PopupBackground->SetSize(Vector2(1.f, 1.f));
		if (BorderRenderComponent* borderComp = PopupBackground->GetBorderComp())
		{
			borderComp->Destroy();
		}

		PopupBackground->SetBorderThickness(0.f);
		PopupBackground->SetCenterColor(PromptBackgroundColor);

		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		Float yPos = 0.f;

		LabelComponent* labelComp = LabelComponent::CreateObject();
		if (PopupBackground->AddComponent(labelComp))
		{
			labelComp->SetAutoRefresh(false);
			labelComp->SetPosition(Vector2::ZERO_VECTOR);
			labelComp->SetSize(Vector2(1.f, 0.67f));
			yPos = labelComp->ReadSize().Y;
			labelComp->SetHorizontalAlignment(LabelComponent::HA_Center);
			labelComp->SetWrapText(true);
			labelComp->SetClampText(false);
			labelComp->SetCharacterSize(10);
			labelComp->SetText(DString::CreateFormattedString(translator->TranslateText(TXT("UpdateReferencesPrompt"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode")), numAssociatedNodes));
			labelComp->SetAutoRefresh(true);
		}

		Float leftMargin = 0.f;
		if (SelectVarDropdown != nullptr)
		{
			leftMargin = SelectVarDropdown->GetAnchorLeftDist();
		}

		Float buttonSpacing = 2.f;
		Float buttonWidth = ReadSize().X - (leftMargin * 2.f) - buttonSpacing;
		buttonWidth *= 0.5f; //because two buttons
		ButtonComponent* acceptButton = ButtonComponent::CreateObject();
		if (PopupBackground->AddComponent(acceptButton))
		{
			acceptButton->SetPosition(Vector2(leftMargin, yPos));
			acceptButton->SetSize(Vector2(buttonWidth, 1.f - (yPos + 0.01f)));
			acceptButton->SetAnchorLeft(leftMargin);
			acceptButton->SetCaptionText(translator->TranslateText(TXT("Yes"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode")));
			acceptButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, VariableNode, HandleUpdateReferences, void, ButtonComponent*));
		}

		ButtonComponent* noButton = ButtonComponent::CreateObject();
		if (PopupBackground->AddComponent(noButton))
		{
			noButton->SetPosition(Vector2(0.f, yPos));
			noButton->SetSize(Vector2(buttonWidth, 1.f - (yPos + 0.01f)));
			noButton->SetAnchorRight(leftMargin);
			noButton->SetCaptionText(translator->TranslateText(TXT("No"), EditorEngineComponent::LOCALIZATION_FILE_NAME, TXT("VariableNode")));
			noButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, VariableNode, HandleDeclineUpdateReferences, void, ButtonComponent*));
		}

		if (RenameVarField != nullptr)
		{
			RenameVarField->SetVisibility(false);
		}

		if (SelectVarDropdown != nullptr)
		{
			SelectVarDropdown->SetVisibility(false);
		}
	}
}

void VariableNode::ExitEditMode ()
{
	if (HeaderText != nullptr)
	{
		HeaderText->SetVisibility(true);
	}
	SetAllPortVisibility(true);

	if (EditButton != nullptr)
	{
		EditButton->SetVisibility(true);
	}

	if (RenameVarField != nullptr)
	{
		RenameVarField->SetVisibility(false);
	}

	if (SelectVarDropdown != nullptr)
	{
		if (ListBoxComponent* list = SelectVarDropdown->GetExpandMenuList())
		{
			list->ClearList(); //free up some memory
		}

		SelectVarDropdown->SetVisibility(false);
	}

	if (bAutoUpdateSize)
	{
		UpdateNodeSize(); //Restore the size while also considering the new variable name's length.
	}
}

void VariableNode::SetAllPortVisibility (bool isVisible)
{
	for (SPort& port : InputPorts)
	{
		size_t numConnections = 0;
		if (port.Port != nullptr)
		{
			//Don't set the port visibility since that may also conceal the connection, too.
			numConnections = port.Port->ReadConnections().size();
		}

		if (port.PortInlineEdit != nullptr)
		{
			port.PortInlineEdit->SetVisibility((numConnections == 0) && isVisible);
		}

		if (port.PortLabel != nullptr)
		{
			port.PortLabel->SetVisibility(isVisible);
		}
	}

	for (SPort& port : OutputPorts)
	{
		size_t numConnections = 0;
		if (port.Port != nullptr)
		{
			//Don't set the port visibility since that may also conceal the connection, too.
			numConnections = port.Port->ReadConnections().size();
		}

		if (port.PortInlineEdit != nullptr)
		{
			port.PortInlineEdit->SetVisibility(isVisible);
		}

		if (port.PortLabel != nullptr)
		{
			port.PortLabel->SetVisibility(isVisible);
		}
	}
}

void VariableNode::FindAllAssociatedNodes (bool includeSelf, const DString& nameMatch, std::vector<VariableNode*>& outOtherNodes) const
{
	if (GetOwner() == nullptr || nameMatch.IsEmpty())
	{
		return;
	}

	for (ComponentIterator iter(GetOwner(), false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (VariableNode* varNode = dynamic_cast<VariableNode*>(iter.GetSelectedComponent()))
		{
			if (varNode->ReadVariableId().IsEmpty())
			{
				continue;
			}

			if (!includeSelf && varNode == this)
			{
				continue;
			}

			if (varNode->ReadVariableId().Compare(nameMatch, DString::CC_CaseSensitive) == 0)
			{
				outOtherNodes.push_back(varNode);
			}
		}
	}
}

void VariableNode::HandleEditReleased (ButtonComponent* button)
{
	EditSize().MaxInline(EditNodeSize);
	ComputeAbsTransform(); //needed for selecting all text

	if (HeaderText != nullptr)
	{
		HeaderText->SetVisibility(false);

		if (TooltipComponent* tooltip = dynamic_cast<TooltipComponent*>(HeaderText->FindComponent(TooltipComponent::SStaticClass(), false)))
		{
			tooltip->HideTooltip();
		}
	}
	SetAllPortVisibility(false);

	if (EditButton != nullptr)
	{
		EditButton->SetVisibility(false);
	}

	if (RenameVarField != nullptr)
	{
		RenameVarField->SetVisibility(true);
		RenameVarField->SetText(VarDisplayName);
		RenameVarField->SelectAll();
	}

	if (SelectVarDropdown != nullptr)
	{
		SelectVarDropdown->SetVisibility(true);
		PopulateVariableList();
	}
}

void VariableNode::HandleCloseError (ButtonComponent* button)
{
	if (PopupBackground != nullptr)
	{
		PopupBackground->Destroy();
		PopupBackground = nullptr;
	}
}

void VariableNode::HandleUpdateReferences (ButtonComponent* button)
{
	if (LastVarName.IsEmpty())
	{
		return;
	}

	std::vector<VariableNode*> varNodes;
	FindAllAssociatedNodes(false, LastVarName, OUT varNodes);

	for (VariableNode* var : varNodes)
	{
		var->VariableId = VariableId;
		var->VarDisplayName = VarDisplayName;
		var->UpdateHeaderText();
	}

	if (PopupBackground != nullptr)
	{
		PopupBackground->Destroy();
		PopupBackground = nullptr;
	}

	ExitEditMode();
	UpdateHeaderText();
}

void VariableNode::HandleDeclineUpdateReferences (ButtonComponent* button)
{
	if (PopupBackground != nullptr)
	{
		PopupBackground->Destroy();
		PopupBackground = nullptr;
	}

	ExitEditMode();
	UpdateHeaderText();
}

bool VariableNode::HandleAllowVarRenameInput (const DString& txt)
{
	return txt.HasRegexMatch(GraphAsset::PERMITTED_CHARS_FOR_NAME);
}

void VariableNode::HandleApplyVarRename (TextFieldComponent* textField)
{
	if (textField != nullptr)
	{
		SetLocalVariable(textField->GetContent());
	}
}

void VariableNode::HandleSelectVarExpanded ()
{
	if (ListBoxComponent* listComp = SelectVarDropdown->GetExpandMenuList())
	{
		//Update the text since the VariableNode changed its dimensions when entering edit mode. Update the text since the dropdown could clamp text when it was narrow.
		listComp->UpdateTextContent();
	}
}

void VariableNode::HandleSelectVar (DropdownComponent* dropdown, Int itemIdx)
{
	if (!dropdown->IsItemSelected())
	{
		return;
	}

	GuiDataElement<DString>* selectedItem = dropdown->GetSelectedGuiDataElement<DString>();
	if (selectedItem == nullptr || selectedItem->Data.IsEmpty())
	{
		//User selected an invalid option (eg: the default text).
		return;
	}

	if (GraphVariable* newVar = FindGraphVariable(selectedItem->Data))
	{
		SetAssociatedVariable(newVar);
	}
	else
	{
		SetLocalVariable(selectedItem->ReadLabelText());
	}
}

void VariableNode::HandleNameChanged (const DString& prevId, const DString& varId, const DString& newName)
{
	VariableId = varId;
	VarDisplayName = newName;
	UpdateHeaderText();
}
SD_END