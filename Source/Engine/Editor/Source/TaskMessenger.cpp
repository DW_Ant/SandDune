/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TaskMessenger.cpp
=====================================================================
*/

#include "AssetCatalogue.h"
#include "EditorEngineComponent.h"
#include "EditorWorkerEngineComponent.h"
#include "ReferenceUpdater.h"
#include "TaskMessenger.h"

#ifdef WITH_MULTI_THREAD
IMPLEMENT_CLASS(SD::TaskMessenger, SD::Entity)
SD_BEGIN

void TaskMessenger::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	bRunInObjIterTest = false;
#endif

	MaxWaitTime = 0.5f;
	ThreadComp = nullptr;
}

void TaskMessenger::BeginObject ()
{
	Super::BeginObject();

	//Identify if this resides in the same thread as the EditorEngineComponent.
	EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
	bool isInEditorThread = (editorEngine != nullptr);

	if (isInEditorThread)
	{
		ThreadComp = ThreadedComponent::CreateObject();
		if (AddComponent(ThreadComp))
		{
			ThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(this, TaskMessenger, HandleCopyDataToOtherThread, void, DataBuffer&));
			ThreadComp->SetOnNewData(SDFUNCTION_1PARAM(this, TaskMessenger, HandleReceiveData, void, const DataBuffer&));
			ThreadComp->InstantiateExternalComp(MULTI_THREAD_EDITOR_WORKER_IDX, [&]() -> ThreadedComponent*
			{
				//Check for circular recursion. The worker thread should not reside in the same thread as the main editor thread.
				EditorEngineComponent* editorEngine = EditorEngineComponent::Find();
				CHECK_INFO(editorEngine == nullptr, "Circular recursion detected. The worker thread should not reside in the same thread as the editor thread.")

				TaskMessenger* workerEntity = TaskMessenger::CreateObject();
				if (workerEntity == nullptr)
				{
					return nullptr;
				}

				ThreadedComponent* workerThreadComp = ThreadedComponent::CreateObject();
				if (workerEntity->AddComponent(workerThreadComp))
				{
					workerThreadComp->SetCopyMethod(SDFUNCTION_1PARAM(workerEntity, TaskMessenger, HandleCopyDataToOtherThread, void, DataBuffer&));
					workerThreadComp->SetOnNewData(SDFUNCTION_1PARAM(workerEntity, TaskMessenger, HandleReceiveData, void, const DataBuffer&));
					workerThreadComp->SetOnDisconnect(SDFUNCTION_1PARAM(workerEntity, TaskMessenger, HandleWorkerDisconnect, void, ThreadedComponent*));
					workerEntity->ThreadComp = workerThreadComp;
				}

				return workerThreadComp;
			});
		}
	}
}

void TaskMessenger::LaunchAssetCatalogue (const Directory& rootPath, const DString& extension, const std::vector<DString>& assetTypes, const SDFunction<void, Float>& onUpdate, const SDFunction<void, const std::vector<FileAttributes>&>& onComplete)
{
	DataBuffer& newData = PendingSendData.emplace_back();
	newData << Int(TM_LaunchAssetCatalogue) << rootPath << extension << assetTypes;

	OnAssetCatalogueUpdate = onUpdate;
	OnAssetCatalogueComplete = onComplete;

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void TaskMessenger::CancelAssetCatalogue ()
{
	DataBuffer& newData = PendingSendData.emplace_back();
	newData << Int(TM_CancelAssetCatalogue);

	OnAssetCatalogueUpdate.ClearFunction();
	OnAssetCatalogueComplete.ClearFunction();

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void TaskMessenger::LaunchReferenceUpdater (const DString& oldName, const DString& newName, const SDFunction<void, Float>& onUpdate, const SDFunction<void>& onComplete)
{
	OnRefUpdateProgress = onUpdate;
	OnRefUpdateComplete = onComplete;

	DataBuffer& newData = PendingSendData.emplace_back();
	newData << Int(TM_LaunchReferenceUpdater) << oldName << newName << Bool(false);

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void TaskMessenger::LaunchReferenceUpdater (const DString& oldName, const DString& newName, const Directory& rootDir, const std::vector<DString>& extensions, const SDFunction<void, Float>& onUpdate, const SDFunction<void>& onComplete)
{
	OnRefUpdateProgress = onUpdate;
	OnRefUpdateComplete = onComplete;

	DataBuffer& newData = PendingSendData.emplace_back();
	newData << Int(TM_LaunchReferenceUpdater) << oldName << newName << Bool(true) << rootDir << extensions;

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void TaskMessenger::CancelReferenceUpdater ()
{
	OnRefUpdateProgress.ClearFunction();
	OnRefUpdateComplete.ClearFunction();

	DataBuffer& newData = PendingSendData.emplace_back();
	newData << Int(TM_CancelReferenceUpdater);

	if (ThreadComp != nullptr)
	{
		ThreadComp->RequestDataTransfer(MaxWaitTime);
	}
}

void TaskMessenger::SetMaxWaitTime (Float newMaxWaitTime)
{
	MaxWaitTime = newMaxWaitTime;
}

void TaskMessenger::ReceiveLaunchAssetCatalogue (const DataBuffer& incomingData)
{
	Directory rootPath;
	incomingData >> rootPath;

	DString extension;
	incomingData >> extension;

	std::vector<DString> assetTypes;
	incomingData >> assetTypes;

	if (!incomingData.HasReadError())
	{
		EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
		CHECK(workerEngine != nullptr)

		if (AssetCatalogue* workerCatalogue = workerEngine->GetWorkerCatalogue())
		{
			workerCatalogue->OnCatalogueUpdate = SDFUNCTION_1PARAM(this, TaskMessenger, HandleAssetCatalogueUpdate, void, bool);
			workerCatalogue->PopulateCatalogue(rootPath, extension, assetTypes);
		}
	}
}

void TaskMessenger::ReceiveCancelAssetCatalogue ()
{
	EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
	if (workerEngine == nullptr)
	{
		return;
	}

	if (AssetCatalogue* workerCatalogue = workerEngine->GetWorkerCatalogue())
	{
		workerCatalogue->AbortProcess();

		OnAssetCatalogueUpdate.ClearFunction();
		OnAssetCatalogueComplete.ClearFunction();
	}
}

void TaskMessenger::ReceiveAssetCatalogueProgress (const DataBuffer& incomingData)
{
	Float percentComplete;
	incomingData >> percentComplete;

	if (!incomingData.HasReadError() && OnAssetCatalogueUpdate.IsBounded())
	{
		OnAssetCatalogueUpdate(percentComplete);
	}
}

void TaskMessenger::ReceiveAssetCatalogueComplete (const DataBuffer& incomingData)
{
	std::vector<FileAttributes> completeCatalogue;
	incomingData >> completeCatalogue;

	OnAssetCatalogueUpdate.ClearFunction();

	if (OnAssetCatalogueComplete.IsBounded())
	{
		OnAssetCatalogueComplete(completeCatalogue);
		OnAssetCatalogueComplete.ClearFunction();
	}
}

void TaskMessenger::ReceiveLaunchReferenceUpdater (const DataBuffer& incomingData)
{
	EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
	if (workerEngine == nullptr)
	{
		return;
	}

	DString oldName;
	incomingData >> oldName;

	DString newName;
	incomingData >> newName;

	Bool iterateEverything;
	incomingData >> iterateEverything;

	if (incomingData.HasReadError())
	{
		return;
	}

	if (iterateEverything)
	{
		Directory rootDir;
		incomingData >> rootDir;

		std::vector<DString> extensions;
		incomingData >> extensions;

		if (incomingData.HasReadError())
		{
			return;
		}

		if (ReferenceUpdater* updater = workerEngine->GetRefUpdater())
		{
			updater->OnReferenceUpdate = SDFUNCTION_1PARAM(this, TaskMessenger, HandleReferenceUpdateProgress, void, bool);
			updater->ReplaceReferences(oldName, newName, rootDir, extensions);
		}
	}
	else if (ReferenceUpdater* updater = workerEngine->GetRefUpdater())
	{
		updater->OnReferenceUpdate = SDFUNCTION_1PARAM(this, TaskMessenger, HandleReferenceUpdateProgress, void, bool);
		updater->ReplaceReferences(oldName, newName);
	}
}

void TaskMessenger::ReceiveCancelReferenceUpdater ()
{
	if (EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find())
	{
		if (ReferenceUpdater* updater = workerEngine->GetRefUpdater())
		{
			updater->AbortProcesses();
		}
	}
}

void TaskMessenger::ReceiveReferenceUpdaterProgress (const DataBuffer& incomingData)
{
	Float percentComplete;
	if ((incomingData >> percentComplete).HasReadError())
	{
		return;
	}

	if (OnRefUpdateProgress.IsBounded())
	{
		OnRefUpdateProgress(percentComplete);
	}
}

void TaskMessenger::ReceiveReferenceUpdaterComplete ()
{
	if (OnRefUpdateComplete.IsBounded())
	{
		OnRefUpdateComplete();
	}
}

void TaskMessenger::HandleCopyDataToOtherThread (DataBuffer& outExternalBuffer)
{
	outExternalBuffer << Int(PendingSendData.size());

	for (const DataBuffer& newData : PendingSendData)
	{
		outExternalBuffer.AppendBytes(newData);
	}

	ContainerUtils::Empty(OUT PendingSendData);
}

void TaskMessenger::HandleReceiveData (const DataBuffer& incomingData)
{
	Int numMessages;
	incomingData >> numMessages;
	for (Int i = 0; i < numMessages; ++i)
	{
		if (incomingData.HasReadError())
		{
			return;
		}

		Int messageTypeInt;
		incomingData >> messageTypeInt;
		EThreadMessage messageType = static_cast<EThreadMessage>(messageTypeInt.Value);

		switch (messageType)
		{
			case(TM_LaunchAssetCatalogue):
				ReceiveLaunchAssetCatalogue(incomingData);
				break;

			case(TM_CancelAssetCatalogue):
				ReceiveCancelAssetCatalogue();
				break;

			case(TM_AssetCatalogueProgress):
				ReceiveAssetCatalogueProgress(incomingData);
				break;

			case(TM_AssetCatalogueComplete):
				ReceiveAssetCatalogueComplete(incomingData);
				break;

			case(TM_LaunchReferenceUpdater):
				ReceiveLaunchReferenceUpdater(incomingData);
				break;

			case(TM_CancelReferenceUpdater):
				ReceiveCancelReferenceUpdater();
				break;

			case(TM_ReferenceUpdaterProgress):
				ReceiveReferenceUpdaterProgress(incomingData);
				break;

			case(TM_ReferenceUpdaterComplete):
				ReceiveReferenceUpdaterComplete();
				break;
		}
	}
}

void TaskMessenger::HandleWorkerDisconnect (ThreadedComponent* delegateOwner)
{
	CHECK(delegateOwner != nullptr)

	//Must destroy the loose Entity that owns this ThreadedComponent.
	if (Entity* rootEntity = delegateOwner->GetRootEntity())
	{
		rootEntity->Destroy();
	}
}

void TaskMessenger::HandleAssetCatalogueUpdate (bool isCompleted)
{
	EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
	if (workerEngine == nullptr)
	{
		return;
	}

	if (AssetCatalogue* assetWorker = workerEngine->GetWorkerCatalogue())
	{
		Int totalFiles = assetWorker->ReadRemainingFiles().size() + assetWorker->GetNumFilesProcessed();
		Float percentComplete = 1.f;
		if (totalFiles > 0)
		{
			percentComplete = assetWorker->GetNumFilesProcessed().ToFloat() / totalFiles.ToFloat();
		}

		DataBuffer& updateMsg = PendingSendData.emplace_back();
		updateMsg << Int(TM_AssetCatalogueProgress) << percentComplete;

		if (isCompleted)
		{
			DataBuffer& completeMsg = PendingSendData.emplace_back();
			completeMsg << Int(TM_AssetCatalogueComplete) << assetWorker->ReadFoundAssets();
		}

		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(MaxWaitTime);
		}
	}
}

void TaskMessenger::HandleReferenceUpdateProgress (bool isCompleted)
{
	EditorWorkerEngineComponent* workerEngine = EditorWorkerEngineComponent::Find();
	if (workerEngine == nullptr)
	{
		return;
	}

	if (ReferenceUpdater* refUpdater = workerEngine->GetRefUpdater())
	{
		Float progress = 1.f;
		if (refUpdater->ReadFileList().size() > 0)
		{
			progress = Int(refUpdater->GetNextFileIdx()).ToFloat() / Int(refUpdater->ReadFileList().size()).ToFloat();
		}

		DataBuffer& updateMsg = PendingSendData.emplace_back();
		updateMsg << Int(TM_ReferenceUpdaterProgress) << progress;

		if (isCompleted)
		{
			DataBuffer& completeMsg = PendingSendData.emplace_back();
			completeMsg << Int(TM_ReferenceUpdaterComplete);
		}

		if (ThreadComp != nullptr)
		{
			ThreadComp->RequestDataTransfer(MaxWaitTime);
		}
	}
}
SD_END
#endif