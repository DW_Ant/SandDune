/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphVariable.cpp
=====================================================================
*/

#include "EditableBool.h"
#include "EditableDString.h"
#include "EditableEnum.h"
#include "EditableFloat.h"
#include "EditableInt.h"
#include "EditableStruct.h"
#include "EditorEngineComponent.h"
#include "EditPropertyComponent.h"
#include "GraphAsset.h"
#include "GraphCompiler.h"
#include "GraphVariable.h"
#include "InspectorList.h"
#include <regex>

SD_BEGIN
MulticastDelegate<const DString&, const DString&, const DString&> GraphVariable::OnNameChanged = {};
MulticastDelegate<const DString&, const DString&> GraphVariable::OnCommentChanged = {};
MulticastDelegate<const DString&> GraphVariable::OnAttributeChanged = {};

const DString GraphVariable::PERMITTED_CHARS_FOR_NAME(TXT("[a-zA-Z0-9-_]")); //Alphanumeric characters plus underscores and hyphens

GraphVariable::GraphVariable () : ScriptVariable(),
	DisplayName(DString::EmptyString),
	InspectorType(IT_None),
	VarOwner(nullptr),
	ClassConstraint(nullptr),
	Comment(DString::EmptyString),
	bProtected(false),
	bStatic(false),
	bShowParamName(true),
	bHasInlineComp(true),
	bPassByReference(false)
{
	RegisterEditorInterface();
}

GraphVariable::GraphVariable (const HashedString& inVariableName, EditorInterface* inVarOwner) : ScriptVariable(inVariableName),
	DisplayName(DString::EmptyString),
	InspectorType(IT_None),
	VarOwner(inVarOwner),
	ClassConstraint(nullptr),
	Comment(DString::EmptyString),
	bProtected(false),
	bStatic(false),
	bShowParamName(true),
	bHasInlineComp(true),
	bPassByReference(false)
{
	RegisterEditorInterface();
}

GraphVariable::GraphVariable (const ScriptVariable& cpyObj) : ScriptVariable(cpyObj),
	DisplayName(DString::EmptyString),
	InspectorType(IT_None),
	VarOwner(nullptr),
	ClassConstraint(nullptr),
	Comment(DString::EmptyString),
	bProtected(false),
	bStatic(false),
	bShowParamName(true),
	bHasInlineComp(true),
	bPassByReference(false)
{
	RegisterEditorInterface();
}

GraphVariable::GraphVariable (const GraphVariable& cpyObj) : ScriptVariable(cpyObj),
	DisplayName(cpyObj.DisplayName),
	InspectorType(cpyObj.InspectorType),
	VarOwner(cpyObj.VarOwner),
	ClassConstraint(cpyObj.ClassConstraint),
	Comment(cpyObj.Comment),
	bProtected(cpyObj.bProtected),
	bStatic(cpyObj.bStatic),
	bShowParamName(cpyObj.bShowParamName),
	bHasInlineComp(cpyObj.bHasInlineComp),
	bPassByReference(cpyObj.bPassByReference)
{
	cpyObj.DefaultValue.CopyBufferTo(OUT DefaultValue);
	RegisterEditorInterface();
}

GraphVariable::~GraphVariable ()
{
	UnregisterEditorInterface();
}

void GraphVariable::operator= (const GraphVariable& cpyFrom)
{
	ScriptVariable::operator=(cpyFrom);

	DisplayName = cpyFrom.DisplayName;
	InspectorType = cpyFrom.InspectorType;
	VarOwner = cpyFrom.VarOwner;
	ClassConstraint = cpyFrom.ClassConstraint;
	cpyFrom.DefaultValue.CopyBufferTo(OUT DefaultValue);
	Comment = cpyFrom.Comment;
	bProtected = cpyFrom.bProtected;
	bStatic = cpyFrom.bStatic;
	bShowParamName = cpyFrom.bShowParamName;
	bHasInlineComp = cpyFrom.bHasInlineComp;
	bPassByReference = cpyFrom.bPassByReference;
}

void GraphVariable::Reset ()
{
	ScriptVariable::Reset();

	DisplayName = DString::EmptyString;
	InspectorType = IT_None;
	VarOwner = nullptr;
	ClassConstraint = nullptr;
	DefaultValue.EmptyBuffer();
	bProtected = false;
	bStatic = false;
	bShowParamName = true;
	bHasInlineComp = true;
	bPassByReference = false;
}

void GraphVariable::SetVariableName (const HashedString& newVariableName)
{
	//Reference the old identifier instead of variable name to ensure the file replacement is adjusting the correct instances (no false positives).
	DString oldIdName = GetIdentifyingName();

	ScriptVariable::SetVariableName(newVariableName);
	UpdateEditorObjName();
	DString newIdName = GetIdentifyingName();

	if (!oldIdName.IsEmpty() && oldIdName.Compare(newIdName, DString::CC_CaseSensitive) != 0)
	{
		if (EditorEngineComponent* localEditor = EditorEngineComponent::Find())
		{
			localEditor->OnInvokeRefUpdater.Broadcast(oldIdName, newIdName);
		}

		OnNameChanged.Broadcast(oldIdName, newIdName, VariableName.ToString());
	}
}

CopiableObjectInterface* GraphVariable::CreateCopiableInstanceOfMatchingType () const
{
	return new GraphVariable();
}

void GraphVariable::CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate)
{
	if (const GraphVariable* varTemplate = dynamic_cast<const GraphVariable*>(objectTemplate))
	{
		(*this) = *varTemplate;
	}
}

DString GraphVariable::GetEditorObjName () const
{
	return GetPresentedName();
}

void GraphVariable::PopulateInspectorList (InspectorList& outList)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString fileName = TXT("Editor");
	DString sectionName = TXT("GraphVariable");

	if (EditableDString::SInspectorString* editedName = outList.AddProp(new EditableDString::SInspectorString(TXT("VariableName"), DString::EmptyString, nullptr, DString::EmptyString)))
	{
		editedName->PermittedChars = PERMITTED_CHARS_FOR_NAME;
		editedName->PropertyId = VA_VarName;
		editedName->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphVariable, HandleNameEdit, void, EditPropertyComponent*);
	}

	if (EditableEnum::SInspectorEnum* editEnum = InitItemTemplateForVarType(OUT outList))
	{
		editEnum->OnApplyEdit = SDFUNCTION_1PARAM(this, GraphVariable, HandleVarTypeEdit, void, EditPropertyComponent*);
	}

	if (InspectorType == IT_MemberVariable)
	{
		if (EditableBool::SInspectorBool* editProtected = outList.AddProp(new EditableBool::SInspectorBool(TXT("Protected"), translator->TranslateText(TXT("ProtectedTooltip"), fileName, sectionName), &bProtected, true)))
		{
			editProtected->PropertyId = VA_Protected;
		}

		if (EditableBool::SInspectorBool* editStatic = outList.AddProp(new EditableBool::SInspectorBool(TXT("Static"), translator->TranslateText(TXT("StaticTooltip"), fileName, sectionName), &bStatic, false)))
		{
			editStatic->PropertyId = VA_Static;
		}
	}
	else if (InspectorType == IT_InParam)
	{
		if (EditableBool::SInspectorBool* editParamNameVis = outList.AddProp(new EditableBool::SInspectorBool(TXT("ParamNameVisible"), translator->TranslateText(TXT("ParamNameVisible"), fileName, sectionName), &bShowParamName, true)))
		{
			editParamNameVis->PropertyId = GraphVariable::VA_ShowParamName;
		}

		if (EditableBool::SInspectorBool* editReference = outList.AddProp(new EditableBool::SInspectorBool(TXT("PassByReference"), translator->TranslateText(TXT("PassByReference"), fileName, sectionName), &bPassByReference, false)))
		{
			editReference->PropertyId = GraphVariable::VA_PassByReference;
		}
	}
	else if (InspectorType == IT_OutParam)
	{
		if (EditableBool::SInspectorBool* editParamNameVis = outList.AddProp(new EditableBool::SInspectorBool(TXT("ParamNameVisible"), translator->TranslateText(TXT("ParamNameVisible"), fileName, sectionName), &bShowParamName, true)))
		{
			editParamNameVis->PropertyId = GraphVariable::VA_ShowParamName;
		}
	}

	if (EditableDString::SInspectorString* editComment = outList.AddProp(new EditableDString::SInspectorString(TXT("Comment"), DString::EmptyString, nullptr, DString::EmptyString)))
	{
		editComment->PropertyId = GraphVariable::VA_Comment;
	}

	if (EditableDString::SInspectorString* editDefaultValue = outList.AddProp(new EditableDString::SInspectorString(TXT("DefaultValue"), DString::EmptyString, nullptr, TXT("0"))))
	{
		editDefaultValue->PropertyId = GraphVariable::VA_DefaultValue;
	}
}

void GraphVariable::DeleteEditorObject ()
{
	delete this;
}

const DClass* GraphVariable::GetEditClass (EVarType varType)
{
	switch (varType)
	{
		case(VT_Bool):
			return EditableBool::SStaticClass();

		case(VT_Int):
			return EditableInt::SStaticClass();

		case(VT_Float):
			return EditableFloat::SStaticClass();

		case(VT_String):
			return EditableDString::SStaticClass();

		case(VT_Class):
			return EditableEnum::SStaticClass();
	}

	return nullptr;
}

bool GraphVariable::CanTypeBeConfiguredInEditor (EVarType varType)
{
	return (varType != ScriptVariable::VT_Unknown && varType != ScriptVariable::VT_Function && varType != ScriptVariable::VT_Obj && varType != ScriptVariable::VT_Pointer);
}

bool GraphVariable::IsTypeSupportingRelative (EVarType varType)
{
	return (varType == ScriptVariable::VT_Int || varType == ScriptVariable::VT_Float);
}

void GraphVariable::SetVariableName_Silent (const HashedString& newVariableName)
{
	ScriptVariable::SetVariableName(newVariableName);
}

const DClass* GraphVariable::GetInlinedComponentClass () const
{
	switch (VarType)
	{
		case(ScriptVariable::VT_Bool):
			return CheckboxComponent::SStaticClass();

		case(ScriptVariable::VT_Int):
		case(ScriptVariable::VT_Float):
		case(ScriptVariable::VT_String):
			return TextFieldComponent::SStaticClass();
	}

	return nullptr;
}

void GraphVariable::InitializeInlinedComponent (GuiComponent* comp, bool isInput, Int fontSize) const
{
	switch(GetVarType())
	{
		case(ScriptVariable::VT_Bool):
		{
			Bool defaultBool = false;
			ReadDefaultValue().JumpToBeginning();
			if (ReadDefaultValue().CanReadBytes(Bool::SGetMinBytes()))
			{
				ReadDefaultValue() >> defaultBool;
			}

			InitializeInlinedComponent(comp, isInput, defaultBool, fontSize);
			break;
		}

		case(ScriptVariable::VT_Int):
		case(ScriptVariable::VT_Float):
		case(ScriptVariable::VT_String):
		{
			InitializeInlinedComponent(comp, isInput, DefaultValueToString(), fontSize);
			break;
		}
	}
}

void GraphVariable::InitializeInlinedComponent (GuiComponent* comp, bool isInput, bool defaultValue, Int fontSize) const
{
	if (CheckboxComponent* checkbox = dynamic_cast<CheckboxComponent*>(comp))
	{
		checkbox->SetCheckboxRightSide(!isInput);
		checkbox->SetChecked(defaultValue);
	}
}

void GraphVariable::InitializeInlinedComponent (GuiComponent* comp, bool isInput, const DString& defaultValue, Int fontSize) const
{
	if (TextFieldComponent* textField = dynamic_cast<TextFieldComponent*>(comp))
	{
		textField->SetAutoRefresh(false);
		textField->SetSingleLine(true);
		textField->SetCharacterSize(fontSize);
		textField->SetText(defaultValue);

		if (FrameComponent* textBackground = textField->GetBackgroundComponent())
		{
			if (BorderRenderComponent* borderComp = textBackground->GetBorderComp())
			{
				borderComp->Destroy();
			}

			textBackground->SetBorderThickness(0.f);
		}

		if (isInput)
		{
			textField->SetHorizontalAlignment(LabelComponent::HA_Left);
		}
		else
		{
			textField->SetHorizontalAlignment(LabelComponent::HA_Right);
		}

		textField->SetAutoRefresh(true);
	}
}

void GraphVariable::SaveToConfig (ConfigWriter* config, const DString& sectionName) const
{
	CHECK(config != nullptr)

	config->SavePropertyText(sectionName, TXT("Comment"), Comment);

	Int varTypeValue(VarType);
	config->SaveProperty(sectionName, TXT("VarType"), varTypeValue);

	DString constraintName = (ClassConstraint != nullptr) ? ClassConstraint->GetFullAssetName() : DString::EmptyString;
	config->SaveProperty(sectionName, TXT("ClassConstraint"), constraintName);

	DString defaultValueStr = DefaultValueToString();
	config->SavePropertyText(sectionName, TXT("DefaultValue"), defaultValueStr);
	config->SaveProperty<Bool>(sectionName, TXT("bProtected"), bProtected);
	config->SaveProperty<Bool>(sectionName, TXT("bStatic"), bStatic);
	config->SaveProperty<Bool>(sectionName, TXT("bShowParamName"), bShowParamName);
	config->SaveProperty<Bool>(sectionName, TXT("bPassByReference"), bPassByReference);
}

void GraphVariable::LoadFromConfig (ConfigWriter* config, const DString& sectionName)
{
	CHECK(config != nullptr)
	
	ResetType();
	Comment = config->GetPropertyText(sectionName, TXT("Comment"));

	Int varTypeValue = config->GetProperty<Int>(sectionName, TXT("VarType"));
	SetFixedType(static_cast<EVarType>(varTypeValue.Value));

	DString fullAssetName = config->GetPropertyText(sectionName, TXT("ClassConstraint"));
	if (!fullAssetName.IsEmpty())
	{
		DString path;
		DString assetName;
		if (GraphAsset::ParseFullAssetName(fullAssetName, OUT path, OUT assetName))
		{
			HashedString hashedName(assetName);
			if (EditorEngineComponent* editorEngine = EditorEngineComponent::Find())
			{
				if (editorEngine->ReadLoadedAssets().contains(hashedName))
				{
					ClassConstraint = editorEngine->ReadLoadedAssets().at(hashedName);
				}
				else
				{
					ClassConstraint = GraphAsset::CreateObject();
					ClassConstraint->InitializeAsset(assetName, true);
					
					path.ReplaceInline(TXT("."), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
					ClassConstraint->SetSaveLocation(Directory::DEV_ASSET_DIRECTORY / path);
					ClassConstraint->LoadEssentials();
				}
			}
		}
	}

	DString defaultValueStr = config->GetPropertyText(sectionName, TXT("DefaultValue"));
	SetDefaultValueFromString(defaultValueStr);
	
	bProtected = config->GetProperty<Bool>(sectionName, TXT("bProtected"));
	bStatic = config->GetProperty<Bool>(sectionName, TXT("bStatic"));
	bShowParamName = config->GetProperty<Bool>(sectionName, TXT("bShowParamName"));
	bPassByReference = config->GetProperty<Bool>(sectionName, TXT("bPassByReference"));
}

void GraphVariable::Serialize (DataBuffer& outBuffer) const
{
	outBuffer << ReadVariableName().ToString();

	Int varTypeInt(VarType);
	outBuffer << varTypeInt;

	outBuffer << DefaultValue;

	outBuffer << Comment;
	outBuffer << bProtected;
	outBuffer << bStatic;
	outBuffer << bShowParamName;
	outBuffer << bPassByReference;
}

bool GraphVariable::Deserialize (const DataBuffer& incomingData)
{
	ResetType(); 

	DString varName;
	incomingData >> varName;
	EditVariableName() = HashedString(varName);

	Int varTypeInt;
	incomingData >> varTypeInt;
	SetFixedType(static_cast<EVarType>(varTypeInt.Value));

	incomingData >> DefaultValue;
	incomingData >> Comment;
	incomingData >> bProtected;
	incomingData >> bStatic;
	incomingData >> bShowParamName;
	incomingData >> bPassByReference;

	return !incomingData.HasReadError();
}

DString GraphVariable::GetIdentifyingName () const
{
	if (VarOwner == nullptr)
	{
		return DString::CreateFormattedString(TXT("Variable'%s'"), ReadVariableName().ToString());
	}

	return DString::CreateFormattedString(TXT("Variable'%s.%s'"), VarOwner->GetCompleteEditorName(), ReadVariableName().ToString());
}

DString GraphVariable::GetIdentifyingName (const DString& scope, const DString& varName)
{
	if (scope.IsEmpty())
	{
		return DString::CreateFormattedString(TXT("Variable'%s'"), varName);
	}

	return DString::CreateFormattedString(TXT("Variable'%s.%s'"), scope, varName);
}

bool GraphVariable::ParseVariableId (const DString& varId, DString& outScope, DString& outVarName)
{
	DString idPrefix = TXT("Variable");
	DString regexStr;
	size_t numExpectedMatches;

	//If a period is provided, then scope information exists
	bool scopeGiven = (varId.Find('.', idPrefix.Length()) != INT_INDEX_NONE);
	if (scopeGiven)
	{
		/**
		idPrefix + ' - finds the text "Variable'" literally.
		([\\w\\W]*) - Finds any character up to the first period found. The results of this will be stored in the outScope variable. Extra slashes to escape the escape characters.
		\\. - Finds the period character literally.
		([\\w\\W]*) - Finds any character up to the last closing apostrophe. The results of this will be stored in the outVarName variable.
		' - Matches the closing apostrophe. This is not saved in outVarName.
		*/
		regexStr = idPrefix + TXT("'([\\w\\W]*)\\.([\\w\\W]*)'");
		numExpectedMatches = 3;
	}
	else
	{
		regexStr = idPrefix + TXT("'([\\w\\W]*)'");
		numExpectedMatches = 2;
	}

	std::smatch matches;
	std::regex searchPattern(regexStr.ToCString(), std::regex_constants::icase);
	std::string stdId = varId.ToStr();
	if (!std::regex_search(stdId, OUT matches, searchPattern) || matches.size() != numExpectedMatches)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Unable to parse the variable ID \"%s\" since that is not in the expected format \"%s'<Scope>.<Name>'\"."), varId, idPrefix);
		return false;
	}

	if (scopeGiven)
	{
		outScope = matches[1].str();
		outVarName = matches[2].str();
	}
	else
	{
		outScope = DString::EmptyString;
		outVarName = matches[1].str();
	}

	return true;
}

DString GraphVariable::GetPresentedName () const
{
	if (!DisplayName.IsEmpty())
	{
		return DisplayName;
	}

	return VariableName.ToString();
}

DString GraphVariable::DefaultValueToString () const
{
	DefaultValue.JumpToBeginning();
	switch(VarType)
	{
		case(VT_Bool):
		{
			Bool defaultBool(false);
			if (DefaultValue.CanReadBytes(Bool::SGetMinBytes()))
			{
				DefaultValue >> defaultBool;
			}

			return defaultBool.ToString();
		}

		case(VT_Int):
		{
			Int defaultInt(0);
			if (DefaultValue.CanReadBytes(Int::SGetMinBytes()))
			{
				DefaultValue >> defaultInt;
			
			}

			return defaultInt.ToString();
		}

		case(VT_Float):
		{
			Float defaultFloat(0.f);
			if (DefaultValue.CanReadBytes(Float::SGetMinBytes()))
			{
				DefaultValue >> defaultFloat;
			}

			return defaultFloat.ToString();
		}

		case(VT_String):
		case(VT_Class): //Classes are treated like strings
		{
			DString result;

			if (DefaultValue.CanReadBytes(DString::SGetMinBytes()))
			{
				DefaultValue >> result;
			}

			return result;
		}
	}

	return DString::EmptyString;
}

void GraphVariable::SetDefaultValueFromString (const DString& newValue)
{
	DefaultValue.EmptyBuffer();

	switch(VarType)
	{
		case(VT_Bool):
		{
			Bool defaultBool;
			defaultBool.ParseString(newValue);
			DefaultValue << defaultBool;
			break;
		}

		case(VT_Int):
		{
			Int defaultInt;
			defaultInt.ParseString(newValue);
			DefaultValue << defaultInt;
			break;
		}

		case(VT_Float):
		{
			Float defaultFloat;
			defaultFloat.ParseString(newValue);
			DefaultValue << defaultFloat;
			break;
		}

		case(VT_String):
		case(VT_Class):
		{
			DefaultValue << newValue;
			break;
		}
	}
}

EditPropertyComponent* GraphVariable::UpdateDefaultValueComp (EditableEnum* editedComp)
{
	EditPropertyComponent* foundProp = nullptr;
	Entity* owner = editedComp->GetOwner();
	EditableStruct* owningStruct = nullptr;

	/*
	The default value component may reside within various layers of structs.
	For example: Member variables would have
	struct memberVars
		struct varType
			VarType <--- This is the editable Enum
	DefaultValue <--- this is the comp to edit.

	While the VariableOverrides would have
	struct DefaultOverrides
		VarName <--- this is the editable enum
		DefaultOverride <--- this is the comp to edit.
	*/
	while (owner != nullptr)
	{
		owningStruct = dynamic_cast<EditableStruct*>(owner);
		if (owningStruct == nullptr)
		{
			break;
		}

		foundProp = owningStruct->FindSubPropWithMatchingId(GraphVariable::VA_DefaultValue, false);
		if (foundProp != nullptr)
		{
			break;
		}

		owner = owningStruct->GetOwner();
	}
	
	if (foundProp == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Cannot update value component since none of the owning struct's SubPropertyComponents does not have a property with ID (%s)."), Int(GraphVariable::VA_DefaultValue));
		return foundProp;
	}

	if (DropdownComponent* dropdown = editedComp->GetDropdown())
	{
		ScriptVariable::EVarType varType = dropdown->GetSelectedItem<ScriptVariable::EVarType>();

		bool hasDefaultComp = CanTypeBeConfiguredInEditor(varType);
		foundProp->SetReadOnly(!hasDefaultComp);

		const DClass* desiredClass = GetEditClass(varType);
		if (desiredClass == nullptr)
		{
			//This type doesn't have a default value
			return foundProp;
		}

		const EditPropertyComponent* correctCdo = dynamic_cast<const EditPropertyComponent*>(desiredClass->GetDefaultObject());
		CHECK(correctCdo != nullptr)

		if (foundProp->StaticClass() != desiredClass)
		{
			Int defaultPropId = foundProp->GetPropertyId();

			//There's a class mismatch, need to replace the sub property instance with the new class.
			foundProp->Destroy();
			foundProp = nullptr;
				
			foundProp = correctCdo->CreateObjectOfMatchingClass();
			if (owningStruct->AddComponent(foundProp))
			{
				foundProp->SetPropertyId(defaultPropId);

				if (LabelComponent* labelComp = foundProp->GetPropertyName())
				{
					labelComp->SetText(TXT("DefaultValue"));
				}

				if (EditableEnum* editEnum = dynamic_cast<EditableEnum*>(foundProp))
				{
					if (varType == ScriptVariable::VT_Class)
					{
						GraphAsset::RefreshClassList(editEnum->GetDropdown(), nullptr);
					}

					editEnum->SetDefaultIndex(0);
				}
			}
		}
	}

	return foundProp;
}

void GraphVariable::SetDisplayName (const DString& newDisplayName)
{
	DString id = GetIdentifyingName(); //DisplayName doesn't affect the ID
	DisplayName = newDisplayName;
	OnNameChanged.Broadcast(id, id, DisplayName);
}

void GraphVariable::SetInspectorType (EInspectorType newInspectorType)
{
	InspectorType = newInspectorType;
}

void GraphVariable::SetVarOwner (EditorInterface* newVarOwner)
{
	VarOwner = newVarOwner;
}

void GraphVariable::SetClassConstraint (GraphAsset* newClassConstraint)
{
	ClassConstraint = newClassConstraint;
}

void GraphVariable::SetDefaultValue (const DataBuffer& newDefaultValue)
{
	newDefaultValue.CopyBufferTo(OUT DefaultValue);
}

void GraphVariable::SetComment (const DString& newComment)
{
	Comment = newComment;
	OnCommentChanged.Broadcast(GetIdentifyingName(), Comment);
}

void GraphVariable::SetProtected (bool newProtected)
{
	bProtected = newProtected;
	OnAttributeChanged.Broadcast(GetIdentifyingName());
}

void GraphVariable::SetStatic (bool newStatic)
{
	bStatic = newStatic;
	OnAttributeChanged.Broadcast(GetIdentifyingName());
}

void GraphVariable::SetShowParamName (bool newShowParamName)
{
	bShowParamName = newShowParamName;
	OnAttributeChanged.Broadcast(GetIdentifyingName());
}

void GraphVariable::SetHasInlineComp (bool newHasInlineComp)
{
	bHasInlineComp = newHasInlineComp;
	OnAttributeChanged.Broadcast(GetIdentifyingName());
}

void GraphVariable::SetPassByReference (bool newPassByReference)
{
	bPassByReference = newPassByReference;
	OnAttributeChanged.Broadcast(GetIdentifyingName());
}

EditableEnum::SInspectorEnum* GraphVariable::InitItemTemplateForVarType (InspectorList& outList)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	DString fileName = TXT("Editor");
	DString sectionName = TXT("GraphVariable");

	EditableEnum::SInspectorEnum* result = nullptr;

	outList.PushStruct(new EditableStruct::SInspectorStruct(TXT("VariableType"), DString::EmptyString));
	{
		if (EditableEnum::SInspectorEnum* editEnum = outList.AddProp(new EditableEnum::SInspectorEnum(TXT("Type"), translator->TranslateText(TXT("VariableTypeTooltip"), fileName, sectionName), new GraphVariable::Dynamic_Enum_EVarType(nullptr), 0)))
		{
			editEnum->DefaultIdx = 0; //Default to Unknown type
			editEnum->PropertyId = VA_VarType;
			result = editEnum;
		}

		//When arrays are implemented, add that checkbox here.

		//Don't bother adding class type dropdown at this time. That will be dynamically added whenever the user selects obj or class type.
	}
	outList.PopStruct();

	return result;
}

void GraphVariable::UpdateClassTypeProperty (EditableEnum* varTypeEnum, GraphAsset* initValue)
{
	EditableStruct* owningStruct = dynamic_cast<EditableStruct*>(varTypeEnum->GetOwner());
	CHECK(owningStruct != nullptr)

	if (DropdownComponent* dropdown = varTypeEnum->GetDropdown())
	{
		ScriptVariable::EVarType varType = dropdown->GetSelectedItem<ScriptVariable::EVarType>();
		bool hasClassType = (varType == ScriptVariable::VT_Obj || varType == ScriptVariable::VT_Class);
		EditableEnum* classDropdown = dynamic_cast<EditableEnum*>(owningStruct->FindSubPropWithMatchingId(GraphVariable::VA_ClassType, false));
		if ((classDropdown != nullptr) == hasClassType)
		{
			//ClassDropdown is null while hasClassType is false OR ClassDropdown is not null while hasClassType is true. Nothing needs to be done here since the class dropdown is already initialized.
			return;
		}

		if (classDropdown != nullptr)
		{
			//Need to remove this component since the new type doesn't support class types.
			classDropdown->Destroy();
		}
		else
		{
			classDropdown = EditableEnum::CreateObject();
			if (owningStruct->AddComponent(classDropdown))
			{
				classDropdown->SetPropertyId(GraphVariable::VA_ClassType);

				if (LabelComponent* labelComp = classDropdown->GetPropertyName())
				{
					labelComp->SetText(TXT("ClassType"));
				}

				GraphAsset::RefreshClassList(classDropdown->GetDropdown(), nullptr);
				classDropdown->SetDefaultIndex(0);

				if (initValue != nullptr)
				{
					Int itemIdx = INT_INDEX_NONE;
					if (DropdownComponent* dropdown = classDropdown->GetDropdown())
					{
						if (ListBoxComponent* expandComp = dropdown->GetExpandMenuList())
						{
							itemIdx = expandComp->FindItemIdx(initValue);
						}
					}

					if (itemIdx != INT_INDEX_NONE)
					{
						classDropdown->SetDropdownSelectIdx(itemIdx, false);
					}
				}
			}
		}
	}
}

void GraphVariable::ProcessParamTypeChecking (EditableEnum* editedComp)
{
	CHECK(editedComp != nullptr)

	EditableStruct* typeStruct = dynamic_cast<EditableStruct*>(editedComp->GetOwner());
	if (typeStruct == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to process ParamTypeChecking since the owner of %s is not an EditableStruct."), editedComp->ToString());
		return;
	}

	EditableStruct* varStruct = dynamic_cast<EditableStruct*>(typeStruct->GetOwner());
	if (varStruct == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to process ParamTypeChecking since the owner of %s doesn't reside within a struct within a struct."), editedComp->ToString());
		return;
	}

	DString errorMsg;
	if (GraphCompiler::CheckVariableType(*this, OUT errorMsg) != GraphCompiler::CR_Success)
	{
		editedComp->ShowCaption(errorMsg, editedComp->ReadCaptionErrorColors());
	}
	else
	{
		editedComp->HideCaption();
	}
}

void GraphVariable::HandleNameEdit (EditPropertyComponent* editedComp)
{
	if (EditableDString* editStr = dynamic_cast<EditableDString*>(editedComp))
	{
		SetVariableName(HashedString(editStr->GetPropValue()));
	}
}

void GraphVariable::HandleVarTypeEdit (EditPropertyComponent* editedComp)
{
	EditableEnum* editedEnum = dynamic_cast<EditableEnum*>(editedComp);
	if (editedEnum == nullptr)
	{
		EditorLog.Log(LogCategory::LL_Warning, TXT("Failed to handle variable type edit since %s is not an EditableEnum."), editedComp->GetPropertyNameStr());
		return;
	}

	UpdateClassTypeProperty(editedEnum, nullptr);

	if (DropdownComponent* dropdown = editedEnum->GetDropdown())
	{
		if (dropdown->IsItemSelected())
		{
			bFixedType = false;
			SetFixedType(dropdown->GetSelectedItem<ScriptVariable::EVarType>());
		}
	}

	ProcessParamTypeChecking(editedEnum);
	UpdateDefaultValueComp(editedEnum);
}
SD_END