/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableStruct.h
  A GuiComponent responsible for defining a collapsable EditPropertyComponent where
  it may have multiple editable components.
=====================================================================
*/

#pragma once

#include "EditPropertyComponent.h"

SD_BEGIN
class BaseStructElement;

class EDITOR_API EditableStruct : public EditPropertyComponent
{
	DECLARE_CLASS(EditableStruct)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked whenver the user expanded or collapsed this struct. */
	SDFunction<void, bool /*isCollapsed*/> OnToggleCollapse;


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorStruct : public SInspectorProperty
	{
	public:
		/* Text to display in the struct's header component */
		DString HeaderText;

		SInspectorStruct (const DString& inPropertyName, const DString& inTooltipText);
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Label Component that describes a summary of the vector. */
	DPointer<LabelComponent> HeaderComponent;

	/* Button component responsible for resetting the struct back to its defaults. */
	ButtonComponent* ResetDefaultsButton;

	/* If true, then the Header text will dynamically reflect the contents of its inner variables.
	If false, then it's expected that the header field is manually set. */
	bool bDynamicHeader;

	bool bIsCollapsed;

	TickComponent* HeaderUpdateTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void PostInitPropertyComponent () override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual GuiComponent* GetComponentTooltipsShouldAttachTo () override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void SetNormalFontColor (Color newNormalFontColor) override;
	virtual void SetReadOnlyFontColor (Color newReadOnlyFontColor) override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual void SetLineHeight (Float newLineHeight) override;

protected:
	virtual bool AddComponent_Implementation (EntityComponent* newComponent) override;
	virtual void InitializeComponents () override;
	virtual Float CalcVerticalSpace (bool includeSubComp) const override;
	virtual DString ConstructValueAsText () const override;
	virtual void ProcessApplyEdit (EditPropertyComponent* editedComp) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Quick utility functions that bind a common struct to this EditableStruct.
	 */
	virtual void BindVector2 (Vector2* associatedVar, const Vector2& defaultVect);
	virtual void BindVector3 (Vector3* associatedVar, const Vector3& defaultVect);
	virtual void RebindVector2 (Vector2* associatedVar);
	virtual void RebindVector3 (Vector3* associatedVar);
	virtual void SetDefaultVector2 (const Vector2& defaultVect);
	virtual void SetDefaultVector3 (const Vector3& defaultVect);

	/**
	 * Adds the given struct element to this EditableStruct. The BaseStructElement is abstract. Use the templated StructElement class to pass into this parameter.
	 * The given struct element will be deleted in this function since it's no longer needed. It is not safe to access this variable afterwards.
	 * Returns a reference to the instantiated EditPropertyComponent in case there's a need to configure specific properties for this element (eg: arrays, nested structs, multi line strings, etc...)
	 */
	virtual EditPropertyComponent* AddStructElement (BaseStructElement* newStructElement);

	/**
	 * Updates the struct's header text to contain a summary of what its inner components are.
	 *
	 * This is called automatically whenever the user changed any of the struct's properties.
	 * This is also automatically called whenever the variable the sub props are bound to are updated.
	 * If the UpdateTickComponents are running, then this function is also called at their set intervals.
	 * This function should be called externally if there's a need to update the header text while none of the conditions listed above are met.
	 */
	virtual void RefreshHeaderText ();

	/**
	 * Removes all struct SubProperties that make up the struct. Call this if there's a need to rebind/repurpose this UI component.
	 */
	virtual void DestroyStructElements ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Set to true if the struct's header should automatically be constructed based on the contents of its sub properties.
	 * Set to false to manually set the struct's header via SetStaticHeaderText.
	 */
	virtual void SetDynamicHeader (bool newDynamicHeader);
	virtual void SetStaticHeaderText (const DString& newStaticHeaderText);
	virtual void SetCollapsed (bool newCollapsed);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsCollapsed () const
	{
		return bIsCollapsed;
	}

	inline LabelComponent* GetHeaderComponent () const
	{
		return HeaderComponent.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Called whenever the user clicked on the header label in order to toggle between collapse and expanded.
	 */
	virtual void ToggleCollapse ();

	/**
	 * Syncs the header component's color font to be equal to the PropertyName's font color.
	 */
	virtual void SyncFontColors ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleResetToDefaultsReleased (ButtonComponent* uiComp);
	virtual void HandleUpdateHeader (Float deltaSec);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * A templated function the StructElement may call into when a EditableStruct is its template argument.
	 * The struct defaults are not set here. Instead the individual struct components are assigned. This is defined to prevent compiler errors.
	 */
	template <class V>
	void BindVariable (V* notUsed)
	{
		//Noop
	}

	template <class V>
	void SetDefaultValue (const V& notUsed)
	{
		//Noop
	}
};
SD_END