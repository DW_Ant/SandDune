/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  StructElement.h
  A templated object that represents a singular editable property for a data struct.

  This class defines the methods that require access to the actual data.
=====================================================================
*/

#pragma once

#include "BaseStructElement.h"
#include "EditableStruct.h"
#include "EditPropertyComponent.h"

SD_BEGIN
template <class V /*variable*/, class GuiType>
class StructElement : public BaseStructElement
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The variable this element is directly editing. */
	V* EditedVariable;

	V DefaultValue;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	StructElement () = delete;

	StructElement (const DString& inPropName, V* inEditedVariable, V inDefaultValue) : BaseStructElement(inPropName),
		EditedVariable(inEditedVariable),
		DefaultValue(inDefaultValue)
	{
		//Noop
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual EditPropertyComponent* SetupPropComp (EditableStruct* attachTo) const override
	{
		GuiType* propComp = GuiType::CreateObject();
		InitializePropComponent(attachTo, propComp);
		return propComp;
	}

	virtual void ResetToDefault () override
	{
		*EditedVariable = DefaultValue;
	}

protected:
	virtual void InitializePropComponent (EditableStruct* attachTo, EditPropertyComponent* propComp) const override
	{
		BaseStructElement::InitializePropComponent(attachTo, propComp);

		if (GuiType* specializedComp = dynamic_cast<GuiType*>(propComp))
		{
			specializedComp->BindVariable(EditedVariable);
			specializedComp->SetDefaultValue(DefaultValue);
		}
	}


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetDefaultValue (V newDefaultValue)
	{
		DefaultValue = newDefaultValue;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline V* GetEditedVariable () const
	{
		return EditedVariable;
	}

	inline V GetDefaultValue () const
	{
		return DefaultValue;
	}
};
SD_END