/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FunctionNode.h

  A GraphNode GuiComponent that corresponds to a GraphFunction. The GraphFunction
  contains meta data about a particular function exposed to the editor.
  The FunctionNode is responsible for presenting it on the graph. There may be multiple
  FunctionNodes to one GraphFunction.

  FunctionNodes never bind to override functions. They are always associated with the super
  function.

  One of the most important functions behind this class is to try to preserve data whenever its
  associated function changes.
=====================================================================
*/

#pragma once

#include "GraphNode.h"
#include "GraphVariable.h"

SD_BEGIN
class GraphAsset;
class GraphFunction;
class NodePort;
class VariablePort;

class EDITOR_API FunctionNode : public GraphNode
{
	DECLARE_CLASS(FunctionNode)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPortVarData
	{
		bool IsInputPort;
		size_t PortIdx;
		GraphVariable::EVarType VarType;
	};

	struct SCachedPortConnection
	{
		/* The other port that is connected to this. */
		std::vector<NodePort*> OtherConnections;

		bool bIsInParam;

		/* The label text of the cached port. */
		DString PortLabel;

		ScriptVariable::EVarType PortType;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* ID of the GraphFunction this node is associated with. It uses an ID instead of a function reference since the GraphFunction instance may be replaced when 
	* the array of edit components syncs the graph asset's function array. */
	DString FunctionId;

	/* If this is a function node for a variable operation, then this ID is equal to the relevant variable. Otherwise this is empty. */
	DString VarId;

	std::vector<SPortVarData> PortVarData;

	/* Scale multiplier for the ports used to enter and exit the function. */
	Float EntrancePortScaleMultiplier;

	/* If not empty, this text will be displayed on the InvokeOn label component instead of its associated function's name. */
	DString InvokeOnTextOverride;

	DPointer<FrameComponent> InvokeOnBackground;

	/* Components used to render a sprite at the end of the InvokeOnBackground. */
	PlanarTransformComponent* InvokeOnEdgeTransform;
	DPointer<SpriteComponent> InvokeOnEdgeSprite;

	/* Label component above the node describing the object instance that owns the associated function. */
	DPointer<LabelComponent> InvokeOnLabel;

	/* Port used to connect the function owner to determine the object/class instance to invoke on. */
	DPointer<VariablePort> InvokeOnPort;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool RemovePort (bool isInputPort, size_t portIdx) override;
	virtual void SetBackgroundColor (Color newBackgroundColor) override;

protected:
	virtual void InitializeComponents () override;
	virtual void RefreshNodeComponents () override;
	virtual void CalcNodeSize (Vector2& outMinSize) const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes this node to reflect the attributes of the given function.
	 * This function will generate the GuiComponents (such as ports and labels).
	 */
	virtual void BindFunction (GraphFunction* newAssociatedFunction);

	/**
	 * Forcibly enables the 'InvokeOn' components to be visible.
	 * It'll initialize the port to accept types of the given param.
	 * @param isStatic If true, then the port will only accept classes instead of objects.
	 */
	virtual void EnableInvokeOnComps (GraphAsset* acceptedType, bool isStatic);

	/**
	 * Parses this node's associated FunctionId, then later searches for the function instance that matches the ID.
	 */
	virtual GraphFunction* FindGraphFunction () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetVarId (const DString& newVarId);
	virtual void SetEntrancePortScaleMultiplier (Float newEntrancePortScaleMultiplier);
	virtual void SetInvokeOnTextOverride (const DString& newInvokeOnTextOverride);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetFunctionId () const
	{
		return FunctionId;
	}

	inline const DString& ReadFunctionId () const
	{
		return FunctionId;
	}

	inline Float GetEntrancePortScaleMultiplier () const
	{
		return EntrancePortScaleMultiplier;
	}

	inline FrameComponent* GetInvokeOnBackground () const
	{
		return InvokeOnBackground.Get();
	}

	inline SpriteComponent* GetInvokeOnEdgeSprite () const
	{
		return InvokeOnEdgeSprite.Get();
	}

	inline LabelComponent* GetInvokeOnLabel () const
	{
		return InvokeOnLabel.Get();
	}

	inline VariablePort* GetInvokeOnPort () const
	{
		return InvokeOnPort.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitInvokeOnPort (GraphFunction* func);

	/**
	 * Creates a port on this node where this port is intended to configure the given in or out parameter.
	 */
	virtual void InitPortForParam (GraphVariable* var, bool isInParam);

	/**
	 * Iterates through the ports to find the components associated with the given port data.
	 */
	virtual SPort* GetPortData (const SPortVarData& varData);

	/**
	 * Returns true if the given function ID matches this node's associated function ID.
	 */
	virtual bool IsMatchingFunctionId (const DString& functionId) const;

	/**
	 * Update the invoke label to display the asset name that owns the function associated with this node.
	 */
	virtual void UpdateInvokeOnLabel ();

	/**
	 * The associated function parameters have been updated entirely due to an array sync. Variables could have been added, moved, and removed.
	 *
	 * This function is responsible for the following:
	 * Reordering the ports so that they match the function's new parameters.
	 * Pair up existing connections based on matching type and name. This may require ports to connect to a different index.
	 * Existing connections that were not found will be redirected to a temporary GraphNode so that the user can manually hook them back up.
	 */
	virtual void RepairPorts (bool repairInput);

	/**
	 * Creates a GraphNode instance for stale connections.
	 * The location offset parameter will place the node at that location relative to where this node is located.
	 */
	virtual GraphNode* CreateStaleConnectionNode (const Vector2& locationOffset);

	/**
	 * Removes all delegates bound to the AssociatedFunction.
	 */
	virtual void UnbindFunction ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleInvokeOnPortConnectionChanged (NodePort* changedPort);
	virtual void HandleFunctionNameChanged (const DString& prevFuncId, const DString& newFuncId, const DString& newName);
	virtual void HandleFunctionCommentChanged (const DString& functionId, const DString& newComment);
	virtual void HandleFunctionParamNameChanged (const DString& functionId, const DString& prevName, const DString& newName);
	virtual void HandleFunctionParamsChanged (const DString& functionId);

	virtual void HandleVarNameChanged (const DString& prevId, const DString& varId, const DString& newName);
};
SD_END