/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableField.h
  The other half of the EditPropertyComponent where this creates a text field component
  the user can leverage to set the property value.
=====================================================================
*/

#pragma once

#include "EditPropertyComponent.h"

SD_BEGIN
class EDITOR_API EditableField : public EditPropertyComponent
{
	DECLARE_CLASS(EditableField)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Background color to use when in normal editing mode and read only mode. */
	Color NormalBackgroundColor;
	Color ReadOnlyBackgroundColor;

	DPointer<TextFieldComponent> PropValueField;
	FrameComponent* FieldBackground;

	/* Button component responsible for resetting to defaults. */
	DPointer<ButtonComponent> ResetDefaultsButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual void SetNormalFontColor (Color newNormalFontColor) override;
	virtual void SetReadOnlyFontColor (Color newReadOnlyFontColor) override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual void SetLineHeight (Float newLineHeight) override;

protected:
	virtual void InitializeComponents () override;
	virtual DString ConstructValueAsText () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Writes to the PropValueField with the given content.
	 */
	virtual void SetPropValue (const DString& newValue, bool bInvokeCallback);

	/**
	 * Adds a field control button that will reset this field back to defaults.
	 */
	virtual void AddResetToDefaultsControl ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetNormalBackgroundColor (Color newNormalColor);
	virtual void SetReadOnlyBackgroundColor (Color newReadOnlyColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline TextFieldComponent* GetPropValueField () const
	{
		return PropValueField.Get();
	}

	inline DString GetPropValue () const
	{
		if (PropValueField != nullptr)
		{
			return PropValueField->GetContent();
		}

		return DString::EmptyString;
	}

	inline ButtonComponent* GetResetDefaultsButton () const
	{
		return ResetDefaultsButton.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the specified mouse coordinates resides within the text field component pertaining the user edits.
	 */
	virtual bool IsWithinEditingField (const Vector2& mousePos) const;

	/**
	 * The user pressed enter or clicked out of the field without escaping.
	 * This function should validate the text field's input before copying it to the associated property.
	 */
	virtual void ApplyEdits () = 0;

	/**
	 * Returns the Regex pattern of acceptable characters for the property field.
	 * If this returns an empty string, then any character is permitted.
	 */
	virtual DString GetPermittedInputChars () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleAllowTextInput (const DString& txt);
	virtual void HandleReturn (TextFieldComponent* uiComponent);
	virtual void HandleResetToDefaultsReleased (ButtonComponent* uiComp);


	/*
	=====================
	  Template
	=====================
	*/

public:
	/**
	 * Quick utility to read the user specified text and interpret that string as the template type.
	 * This is primarily used to quickly read the PropValueField's text as a specific variable when it's not bound to any variable.
	 * Returns true on success. outPropValue is not assigned if it returns false.
	 */
	template <class T>
	bool GetPropValue (T& outPropValue) const
	{
		if (PropValueField.IsNullptr())
		{
			return false;
		}

		outPropValue.ParseString(PropValueField->GetContent());
		return true;
	}
};
SD_END