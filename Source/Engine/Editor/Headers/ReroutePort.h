/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReroutePort.h
  A port that allows other ports of the same kind to bundle up to a single connection.

  Reroute ports are allowed to be chained to other reroute ports as long as it doesn't
  form a loop.
=====================================================================
*/

#pragma once

#include "NodePort.h"

SD_BEGIN
class EDITOR_API ReroutePort : public NodePort
{
	DECLARE_CLASS(ReroutePort)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Default color to use when nothing is connected to this reroute port. */
	Color EmptyColor;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void EstablishConnectionTo (NodePort* otherPort) override;
	virtual void SeverConnectionFrom (NodePort* otherPort) override;
};
SD_END