/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReferenceUpdater.h
  An object responsible for iterating through relevant files to update
  references. This is useful where if the developer changed a name of an asset
  (such as a texture or class), all other assets that reference that old name
  must also be updated.

  This can be an expensive task to perform, and it's recommended to run this in the editor's
  worker thread. Otherwise, running this in the same thread is also supported.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EDITOR_API ReferenceUpdater : public Entity
{
	DECLARE_CLASS(ReferenceUpdater)


	/*
	=====================
	  Enum
	=====================
	*/

protected:
	enum EFileFilter
	{
		FF_InCatalogue, //Only files found in the asset catalogue are considered. Typically use this for renaming asset classes.
		FF_Everything //Iterate through all files found in the specified directory. Typically use this for renaming common resources like Textures.
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SUpdateRequest
	{
		DString OldName;
		DString NewName;

		EFileFilter Filter;

		/* If the file filter is everything, this is the directory to iterate from. */
		Directory RootDirectory;

		/* If the file filter is everything, this is these are the file extensions considered. If empty, then all extensions are considered. */
		std::vector<DString> Extensions;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked whenever this Entity paused for a frame to send an update. IsCompleted is set to true if it finished processing through all updates. */
	SDFunction<void, bool /*isCompleted*/> OnReferenceUpdate;

protected:
	/* The maximum number of files that this catalogue should run before it sends a progress update. If not positive, then this will not send any
	progress updates until the very end (for cases where the AssetCatalogue is used synchronously). */
	Int MaxUpdateInterval;

	/* Index to the AssetCatalogue that needs to be processed next. */
	size_t NextFileIdx;

	/* List of names to replace. This is defined in order this Entity receives update requests. This is to handle cases where the user changed an asset name multiple times.
	In order to prevent skipping references, the ReferenceUpdater must process this list in order. */
	std::vector<SUpdateRequest> UpdateRequests;

	/* List of files to check for reference updates */
	std::vector<FileAttributes> FileList;

	TickComponent* Tick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through the asset catalogue to find all class references that matches the old name and replace it with the new name.
	 * This is typically used for cases where only assets of matching types should be affected (eg: class names).
	 * If the updater is already processing another request, then this request will be pushed to the end of the queue.
	 */
	virtual void ReplaceReferences (const DString& oldName, const DString& newName);

	/**
	 * Iterates through all files in the specified directory to update references that matches the old name and replace it with the new name.
	 * If the file extensions vector is empty, then it'll process any file type. Otherwise, it'll only iterate through files that matches any of the given extensions.
	 * This is typically used for cases where a common used resource is renamed (eg: Textures).
	 * If the updater is already processing another request, then this request will be pushed to the end of the queue.
	 */
	virtual void ReplaceReferences (const DString& oldName, const DString& newName, const Directory& rootDir, const std::vector<DString>& fileExtensions);

	/**
	 * Cancels all requests and stops updating the files.
	 */
	virtual void AbortProcesses ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMaxUpdateInterval (Int newMaxUpdateInterval);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetMaxUpdateInterval () const
	{
		return MaxUpdateInterval;
	}

	inline size_t GetNextFileIdx () const
	{
		return NextFileIdx;
	}

	inline const std::vector<FileAttributes>& ReadFileList () const
	{
		return FileList;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes the process to begin iterating through the next process in the list.
	 * This function assumes that this updater is currently not in the middle of processing a request.
	 */
	virtual void LaunchNextProcess ();

	/**
	 * Attempts to open the specified file. If successful, it'll iterate through each property to see if it needs to update any references.
	 */
	virtual void UpdateReferencesInFile (const FileAttributes& fileAttrib);

	/**
	 * Function invoked as soon as the updater finishes processing through all of its requests.
	 */
	virtual void ProcessFinishedUpdate ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END