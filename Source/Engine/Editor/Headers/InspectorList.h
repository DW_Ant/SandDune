/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InspectorList.h
  This class interfaces with the EditorUtils to allow the inspected objects to easily
  populate the vector of editable properties.

  Although it's possible to manually populate that vector, this class will make it easier
  for the following reasons:
  * Automatically tracks which struct owner instances sub properties belong to.
  * Returns the templated result from what was recently added to the list in case
  there's a need to access/edit specific properties for the new entry.
  * Automatically prepend the struct owner's config name before its sub properties.
  * Recursively populate the inspector list whenever one of the properties is a reference to
  another EditorInterface.
=====================================================================
*/

#pragma once

#include "EditableStruct.h"
#include "EditPropertyComponent.h"
#include "Editor.h"

SD_BEGIN
class EditorInterface;

class EDITOR_API InspectorList
{
	

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of items that contains each added inspector item. This object will take ownership over these SInspectorProperties.
	They will be deleted on destruction. */
	std::vector<EditPropertyComponent::SInspectorProperty*> PropList;

	/* Stack of structs where the last item in this vector is the inner most struct (eg: nested structs).
	This is used to track when new properties are added, they are automatically a sub property of the inner most struct.
	If empty, then it assumes the property is a root property that's directly added to the inspector (doesn't belong to a struct).
	NOTE: If another EditorInterface reference is passed in, those objects are treated like structs in the Inspector. */
	std::vector<EditableStruct::SInspectorStruct*> StructStack;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	InspectorList ();
	InspectorList (const InspectorList& cpyObj) = delete;
	virtual ~InspectorList ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pushes a new struct to the end of the stack. Any following properties added will belong to this struct until it is popped off the stack.
	 */
	virtual EditableStruct::SInspectorStruct* PushStruct (EditableStruct::SInspectorStruct* newStruct);

	/**
	 * Removes the struct at the end of the stack.
	 */
	virtual void PopStruct ();

	/**
	 * Moves the pointer list from self to the other InspectorList.
	 * This InspectorList will no longer have ownership over the pointers.
	 */
	virtual void MoveListTo (InspectorList* otherList);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<EditPropertyComponent::SInspectorProperty*>& ReadPropList () const
	{
		return PropList;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Adds the specified property to the PropList. This will automatically associate it to a struct if the
	 * StructStack is not empty.
	 */
	virtual void AddPropImplementation (EditPropertyComponent::SInspectorProperty* newProp);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template<class T>
	T* AddProp (T* newProp)
	{
		AddPropImplementation(newProp);

		return newProp;
	}

	template<>
	EditableStruct::SInspectorStruct* AddProp <EditableStruct::SInspectorStruct> (EditableStruct::SInspectorStruct* newProp)
	{
		/*
		Add an assertion to warn the developer. It's suppose to be populated like this:
		{
			AddStruct(new EditableStruct::SInspectorProperty(...));
			{
				AddProp(new prop);
				...
			}
			PopStruct();
		}

		Instead of:
		{
			AddProp(new EditableStruct::SInspectorProperty(...));
			AddStruct(new EditableStruct::SInspectorProperty(...)); //<-- This would add an extra struct
			{
				AddProp(new prop);
				...
			}
			PopStruct();
		}
		*/
		CHECK_INFO(false, "Don't add structs to the inspector list. Call PushStruct instead.")
		return PushStruct(newProp);
	}
};
SD_END