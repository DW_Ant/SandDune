/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ExecNode.h

  A GraphNode responsible for configuring a function's exec ports.
  This is essentially the function's start and end points.

  If this node is not inheriting from an existing function, then this node has the option to
  add identifiers to an exit port. These identifiers are used to add multiple exec ports on the calling function.

  When inheriting from a function, the identifiers are read only.
=====================================================================
*/

#pragma once

#include "GraphNode.h"

SD_BEGIN
class VariablePort;

class EDITOR_API ExecNode : public GraphNode
{
	DECLARE_CLASS(ExecNode)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then this node is intended be the starting point for a function. */
	bool bStartPoint;

	/* ID of the function this node resides in. */
	DString OwningFunctionId;

	/* Component that allows the user to identify the exec ports. If left empty, it'll would not provide an ID to an exec port.
	Only applicable for exit points. */
	TextFieldComponent* PortName;

private:
	bool bExecNodeInitialized;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool IsDeletable () const override;

protected:
	virtual void ProcessPortConnectionChanged (SPort* portData) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Determines which ports becomes available.
	 * If this node is an input node, it'll only expose the output exec port.
	 * Otherwise, it'll only expose the input port and the PortName component.
	 * Finally, it'll generate the parameters based on the function signature. For example, if this is an input node, it'll create ports for each in parameter.
	 */
	virtual void InitializeExecType (bool isInputNode);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsStartPoint () const
	{
		return bStartPoint;
	}

	inline DString GetPortName () const
	{
		if (PortName != nullptr)
		{
			return PortName->GetContent();
		}

		return DString::EmptyString;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through the given port list to ensure it's still in sync with the owning function signature.
	 */
	virtual void RepairPorts (bool checkInputParams, std::vector<SPort>& outPortList);

	/**
	 * Creates and places a GraphNode beside this node to indicate that connections are stale and need attention.
	 */
	virtual GraphNode* CreateStaleNode ();

	/**
	 * Moves the connections from one var port to the given node.
	 * Typically the GraphNode represents the stale connections node.
	 */
	virtual void MovePortConnections (VariablePort* moveFrom, const DString& moveToLabel, GraphNode* moveTo, ScriptVariable::EVarType moveToVarType);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleFunctionNameChanged (const DString& prevFuncId, const DString& newFuncId, const DString& newName);
	virtual void HandleFunctionParamNameChanged (const DString& functionId, const DString& prevName, const DString& newName);
	virtual void HandleFunctionParamsChanged (const DString& functionId);
};
SD_END