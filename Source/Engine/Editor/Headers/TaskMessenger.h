/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TaskMessenger.h
  An Entity that processes cross thread messages between editor and its worker thread.

  This messenger will kick off events on the external thread, and it may receive progress updates
  or a notification whenever the task is completed.

  This object is only applicable for multi threaded applications since synchronous tasks most likely wont
  have progress bars, and it wont need notifications when the task is finished.
=====================================================================
*/

#pragma once

#include "Editor.h"

#ifdef WITH_MULTI_THREAD
SD_BEGIN
class EDITOR_API TaskMessenger : public Entity
{
	DECLARE_CLASS(TaskMessenger)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EThreadMessage : unsigned char
	{
		TM_Unknown,
		TM_LaunchAssetCatalogue,
		TM_CancelAssetCatalogue,
		TM_AssetCatalogueProgress,
		TM_AssetCatalogueComplete,

		TM_LaunchReferenceUpdater,
		TM_CancelReferenceUpdater,
		TM_ReferenceUpdaterProgress,
		TM_ReferenceUpdaterComplete
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Maximum number of seconds to wait before sending a batch of data across threads. */
	Float MaxWaitTime;

	ThreadedComponent* ThreadComp;

	/* Delegate to invoke whenever the other thread component sends a progress update for the AssetCatalogue.
	The progress percent is a number ranging from 0-1 where 1 implies it's completed. */
	SDFunction<void, Float /*progressPercent*/> OnAssetCatalogueUpdate;
	SDFunction<void, const std::vector<FileAttributes>& /*completeCatalogue*/> OnAssetCatalogueComplete;

	/* Delegate to invoke whenever the other thread component sends a progress update for the ReferenceUpdater. */
	SDFunction<void, Float /*percentComplete*/> OnRefUpdateProgress;
	SDFunction<void> OnRefUpdateComplete;

	/* List of messages to send over to the other thread. */
	std::vector<DataBuffer> PendingSendData;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Notifies the other thread to begin the asset catalogue process.
	 * @param rootPath The directory where to begin searching through. All subdirectories will also be searched.
	 * @param extension Only files matching this extension will be checked for their asset type.
	 * @param assetTypes If the file's type matches any of these types, then this file will be listed in the asset catalogue.
	 * @param onUpdate Delegate that will be invoked whenever the worker thread sends an update.
	 */
	virtual void LaunchAssetCatalogue (const Directory& rootPath, const DString& extension, const std::vector<DString>& assetTypes, const SDFunction<void, Float>& onUpdate, const SDFunction<void, const std::vector<FileAttributes>&>& onComplete);

	/**
	 * Aborts the asset catalogue processes if this one running.
	 */
	virtual void CancelAssetCatalogue ();

	/**
	 * Notifies the other thread to launch the reference updater. Multiple requests will be pushed to a queue to be executed after the current process finishes.
	 * This overload will iterate through all files found in the engine component's asset catalogue.
	 * @param oldName The text to search through in the assets.
	 * @param newName Replaces the oldName instances in the files with this new name.
	 */
	virtual void LaunchReferenceUpdater (const DString& oldName, const DString& newName, const SDFunction<void, Float>& onUpdate, const SDFunction<void>& onComplete);

	/**
	 * This overload iterates through all files that are found in the given directory. If extensions are specified, it'll only iterate through files that matches any of the extensions.
	 */
	virtual void LaunchReferenceUpdater (const DString& oldName, const DString& newName, const Directory& rootDir, const std::vector<DString>& extensions, const SDFunction<void, Float>& onUpdate, const SDFunction<void>& onComplete);

	/**
	 * Aborts all reference updater processes.
	 */
	virtual void CancelReferenceUpdater ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMaxWaitTime (Float newMaxWaitTime);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetMaxWaitTime () const
	{
		return MaxWaitTime;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ReceiveLaunchAssetCatalogue (const DataBuffer& incomingData);
	virtual void ReceiveCancelAssetCatalogue ();
	virtual void ReceiveAssetCatalogueProgress (const DataBuffer& incomingData);
	virtual void ReceiveAssetCatalogueComplete (const DataBuffer& incomingData);

	virtual void ReceiveLaunchReferenceUpdater (const DataBuffer& incomingData);
	virtual void ReceiveCancelReferenceUpdater ();
	virtual void ReceiveReferenceUpdaterProgress (const DataBuffer& incomingData);
	virtual void ReceiveReferenceUpdaterComplete ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCopyDataToOtherThread (DataBuffer& outExternalBuffer);
	virtual void HandleReceiveData (const DataBuffer& incomingData);

	virtual void HandleWorkerDisconnect (ThreadedComponent* delegateOwner); //Invoked on the ThreadedComponent in worker thread.

	virtual void HandleAssetCatalogueUpdate (bool isCompleted); //This handler resides on the same thread where the AssetCatalogue object resides.
	virtual void HandleReferenceUpdateProgress (bool isCompleted);
};
SD_END
#endif