/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableBool.h
  A GuiComponent responsible for enabling the user to edit a bool.
=====================================================================
*/

#pragma once

#include "EditPropertyComponent.h"

SD_BEGIN
class EDITOR_API EditableBool : public EditPropertyComponent
{
	DECLARE_CLASS(EditableBool)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorBool : public SInspectorProperty
	{
	public:
		bool* AssociatedVariable;

		bool DefaultValue;

		/* Determines the initial bool. Only applicable if AssociatedVariable is nullptr and InitType is IIV_UseOverride */
		bool InitOverride;

		SInspectorBool (const DString& inPropertyName, const DString& inTooltipText, bool* inAssociatedVariable, bool inDefaultValue = false);
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<CheckboxComponent> Checkbox;

	/* The actual bool instance this component is editing. */
	bool* EditedBool;
	bool DefaultBool;

	/* Component responsible for updating the field whenever EditedBool has changed. */
	TickComponent* UpdateBoolTick;
	DPointer<ButtonComponent> ResetDefaultsButton;

private:
	/* Variable the EditedBool will bind to if this component is bound to a local variable (from calling BindToLocalVariable). */
	bool LocalBool;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual void CopyToBuffer (DataBuffer& copyTo) const override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual void UnbindVariable () override;
	virtual void SetNormalFontColor (Color newNormalFontColor) override;
	virtual void SetReadOnlyFontColor (Color newReadOnlyFontColor) override;
	virtual void SetReadOnly (bool bNewReadOnly) override;

protected:
	virtual void InitializeComponents () override;
	virtual DString ConstructValueAsText () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pairs this component to the variable so that any changes to this component is reflected in its associated variable.
	 * It's important to keep this name the same as others to remain compatible with the templated EditableArray.
	 */
	virtual void BindVariable (bool* newEditedBool);

	/**
	 * Sets the checkbox to reflect the new bool value.
	 */
	virtual void SetBoolValue (bool newBool, bool bInvokeCallback);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * The name of this function must match others to allow the templated StructElement to interface with this function.
	 */
	virtual void SetDefaultValue (bool newDefaultBool);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline CheckboxComponent* GetCheckbox () const
	{
		return Checkbox.Get();
	}

	inline bool* GetEditedBool () const
	{
		return EditedBool;
	}

	inline ButtonComponent* GetResetDefaultsButton () const
	{
		return ResetDefaultsButton.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the state of this component and its bound variable based on the checkbox's state.
	 */
	virtual void ProcessCheckboxStateChange ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleUpdateBoolTick (Float deltaSec);
	virtual void HandleCheckboxChecked (CheckboxComponent* uiComp);
	virtual void HandleResetToDefaultsReleased (ButtonComponent* uiComp);
};
SD_END