/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphCompiler.h
  A utility responsible for assembling ContentLogic objects from a Graph.

  The compiler runs the following sequence:
  * Evaluation - During this stage, the compiler will look at the system as a whole.
  It will check if all systems have correct references, variables contain valid data,
  and no required parameters are missing.

  * Generation - During this stage, the compiler will iterate through each graph
  object (nodes, variables, inner graphs), and will generate ContentLogic objects
  internally. The compiler, itself, is responsible for these ContentLogic objects.

  * Serialization - The compiler will iterate through all ContentLogic objects and
  save them out to a binary file. Alternatively, the objects can be returned if the
  entity that invoked the compiler is interested in the generated ContentLogic objects
  rather than saving them out to a binary file.

  * Cleanup - This stage, the compiler will destroy all generated content logic objects.
  This stage will execute even if there's an error.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class GraphFunction;
class GraphVariable;

class EDITOR_API GraphCompiler : public Object
{
	DECLARE_CLASS(GraphCompiler)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ECompileResults
	{
		CR_Success,
		CR_Warning, //There may be issues, but they aren't errors that'll prohibit the compiler from generating the asset.
		CR_Error //The compiler is unable to generate the asset.
	};


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Evaluation step
	 * This function will look at the given variable.
	 * If it returns a result that's not a success, it'll populate the outErrorMsg with a human readable description of the error.
	 * @param otherVars contain a list of vector references where each of them would have a list of variables. This function will check if there's a name conflict with the given var param.
	 */
	static ECompileResults CheckVariableName (const GraphVariable& var, DString& outErrorMsg, const std::vector< std::vector<GraphVariable*>* >& otherVars);
	static ECompileResults CheckVariableType (const GraphVariable& var, DString& outErrorMsg);
	static ECompileResults CheckVariableDefault (const GraphVariable& var, DString& outErrorMsg);

	/**
	 * Evaluation step
	 * This function will look at the given function.
	 * If it returns a result that's not a succeess, it'll populate the outErrorMsg with a human readable description of the error.
	 */
	static ECompileResults CheckFunction (const GraphFunction& func, DString& outErrorMsg, const std::vector< std::vector<GraphFunction*>* >& otherFunctions);
	static ECompileResults CheckFunctionName (const GraphFunction& func, DString& outErrorMsg, const std::vector< std::vector<GraphFunction*>* >& otherFunctions);
};
SD_END