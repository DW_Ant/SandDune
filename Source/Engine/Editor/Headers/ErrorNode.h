/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ErrorNode.h

  A simple GraphNode that generates a compiler error.
=====================================================================
*/

#pragma once

#include "GraphNode.h"

SD_BEGIN
class EDITOR_API ErrorNode: public GraphNode
{
	DECLARE_CLASS(ErrorNode)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EErrorType
	{
		ET_None, //Don't generate a compiler error
		ET_StaleConnections //Generate a compiler error suggesting that the connections are old and needs to be rewired.
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	EErrorType ErrorType;

	TooltipComponent* HeaderTooltip;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetErrorType (EErrorType newErrorType);
};
SD_END