/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphEditor.h
  An Entity that allows the user to place and edit nodes in a 2D graphical
  interface. This is essentially the stage for the node editors.

  A single graph instance is typically used to modify a single GraphFunction instance.

  Implements the following:
  - Mouse controls to manipulate the camera
  - Renders a 2D grid
  - Displays the nodes and their connections for a single GraphFunction.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EditableArray;
class EditPropertyComponent;
class GraphViewport;
class GraphFunction;
class NodePort;

class EDITOR_API GraphEditor : public GuiEntity
{
	DECLARE_CLASS(GraphEditor)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	//Control modes that determine how the user can pan the camera.
	enum ECamPanMode : unsigned char
	{
		CPM_DragMouse = 0, //Drag mouse left to move the camera left
		CPM_DragMouseInvert = 1, //Drag mouse left to move the graph left
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SCurveProperties
	{
		Float Thickness;

		/* The generated bazier curve relies on control points to determine the 'sharpness' of the curvature. This value is related to the distance between the end points.
		The higher the value, the more of a S bend it'll create. If 0, it'll generate a straight line. */
		Float CurveSharpness;

		/* Curve sharpness to use when the port is heading the wrong direction. For example an output port node is heading towards an input port that resides to the out port's left instead of right. */
		Float CurveSharpnessReverseDirection;

		Float SegmentLength;

		/* The segment length to use when generating a curve preview. */
		Float SegmentLengthPreview;

		Float StepAmount;

		/* The step amount to use when generating a curve preview. */
		Float StepAmountPreview;
	};

protected:
	struct SCurveConnection
	{
		/* The port where the CurveComponent is connected to. */
		NodePort* OwningPort;

		NodePort* OtherPort;

		PlanarTransformComponent* CurveTransform;

		CurveRenderComponent* CurveRender;

		/* The relative distance between the two ports when the curve was generated (used to detect when it's time to regenerate the curve whenever the ports moved). */
		Vector2 RelativeDistance;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Controls to manipulate the graph. */
	ECamPanMode CamPanMode;

	/* The user configured value that multiplies the camera movement speed.
	The multiplier is based on the cam move amount per pixel. */
	Float CamSpeedMultiplier;

	/* The user configured value that multiplies the camera zoom rate.
	The multiplier is based on the cam zoom amount per pixel.
	Negative multipliers will invert zoom controls. Setting this to zero will disable camera zoom from mouse drag. */
	Float CamZoomMultiplier;

	/* The user configured value that multiplies the camera zoom rate when using the mouse wheel.
	The multiplier is based on the cam zoom amount per mouse wheel. */
	Float CamZoomWheelMultiplier;

	/* A collection of properties used to configure how the connections between ports appear. */
	SCurveProperties CurveProperties;

protected:
	/* Function that this graph editor is responsible to define. */
	DPointer<GraphFunction> OwningFunction;

	/* Determines how much the mouse should move from the MousePress location before it considers a mouse drag action. When a mouse move event becomes a drag action,
	the editor will prevent input events from being relayed to its components while dragging around. For example, cannot select a new node on mouse release after dragging
	the camera. This flag is reset every time the drag action terminated (when both left and right mouse buttons are released). */
	Float DragActionThreshold;

	/* Determines how much a port must move away from its original curve generation point in order for the curve to regenerate. The smaller the value, the more frequently the curve
	will regenerate when moving the port. Higher values will save on calculations, but it would cause the curve to jump whenever the threshold has been met. */
	Float RegenerateCurveThreshold;

	/* The section name in the Editor configuration file to load in for this Graph instance. This must be assigned before BeginObject is invoked. */
	DString ConfigSectionName;

	SpriteComponent* Grid;
	ContextMenuComponent* ContextComp;

	/* Becomes true if the user is currently holding the left mouse button. */
	bool bLMouseDown;

	/* Becomes true if the user is currently holding the right mouse button. */
	bool bRMouseDown;

	/* If true, the graph will hold the mouse position so it remains at the StartDragPosition. */
	bool bLockMousePos;

	/* When processing input events, this is the most relevant camera to the input event. For multiple viewports rendering this graph, this is the camera
	instance that most recently received an input event. */
	DPointer<PlanarCamera> InputCam;

	/* Reference to the NodePort that is currently being dragged from in order to establish a connection to another port. */
	DPointer<NodePort> DraggedPort;

	/* Curve used to represent the dragged port. */
	DPointer<PlanarTransformComponent> DragPortTransform;
	DPointer<CurveRenderComponent> DragPortRender;

	/* List of all CurveComponents that are connecting ports */
	std::vector<SCurveConnection> CurveConnections;

private:
	/* Reference to the mouse pointer this graph is overriding. */
	DPointer<MousePointer> IconOverrideMouse;

	/* Becomes true if either mouse button is held down. This flag is used to reset the drag distance threshold and possibly reset input permissions to components. */
	bool bDragAction;

	/* The amount the mouse moved when the user started holding the left or right mouse button. */
	Float DragDistance;

	/* The mouse position when the left mouse button was pressed. This is used to snap the mouse position back to this point on mouse move
	This is only applicable for DragMouse mode. The DragMouseInvert doesn't reset the mouse since it shows the user 'dragging' the graph like a pdf file.
	The vector is in OS mouse coordinates. */
	Vector2 StartDragPosition;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void PostAbsTransformUpdate () override;

protected:
	virtual void ConstructUI () override;
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void HandlePassiveMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool HandleConsumableInput (const sf::Event& evnt) override;
	virtual bool HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool HandleConsumableMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a CurveRenderComponent that connects the two given ports.
	 */
	virtual void CreateCurveConnecting (NodePort* portA, NodePort* portB);

	/**
	 * Finds the CurveRenderComponent that's connecting between the two ports. Once found, it'll remove that curve.
	 */
	virtual void RemoveCurveConnecting (NodePort* portA, NodePort* portB);
	

	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningFunction (GraphFunction* newOwningFunction);
	virtual void SetDragActionThreshold (Float newDragActionThreshold);
	virtual void SetRegenerateCurveThreshold (Float newRegenerateCurveThreshold);
	virtual void SetInputCam (PlanarCamera* newInputCam);
	virtual void SetDraggedPort (NodePort* newDraggedPort);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline GraphFunction* GetOwningFunction () const
	{
		return OwningFunction.Get();
	}

	inline Float GetRegenerateCurveThreshold () const
	{
		return RegenerateCurveThreshold;
	}

	inline NodePort* GetDraggedPort () const
	{
		return DraggedPort.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void SetDragAction (bool isDragging);

	/**
	 * Iterates through each immediate component to set their input permissions.
	 */
	virtual void ApplyComponentInputPermissions (GuiComponent::EInputEvent newPermissions);

	/**
	 * Initializes the user controls/preferences from an configuration file.
	 */
	virtual void InitializeGraphPreferences ();

	/**
	 * Snaps the mouse cursor to return to the StartDragPosition.
	 */
	virtual void ApplyMouseLock (MousePointer* mouse);

	/**
	 * Implements the process of panning the camera based on the mouse movement and user preferences.
	 */
	virtual void ProcessCameraMovement (MousePointer* mouse, const Vector2& deltaMove);

	/**
	 * Initializes the given CurveComponent to generate a bazier curve from its starting point to the given location.
	 * The curveEndPt is in absolute coordinates. The curveTransform assumes that it's positioned at the other end point.
	 * If isPreviewCurve is true, it'll use the 'preview' properties found in the CurveProperties struct.
	 */
	virtual void GenerateCurveTo (NodePort* startPort, const Vector2& curveEndPt, PlanarTransformComponent* curveTransform, CurveRenderComponent* curveComp, bool isPreviewCurve);

	/**
	 * Clamps the camera position so that it doesn't move beyond this GuiEntity's size.
	 */
	virtual void ClampCamPos ();

	/**
	 * Implements the process of zooming the camera based on the zoom amount.
	 * @param bCenteredZoom - If false then the camera will translate based on the mouse position. Otherwise it will not translate at all, causing the zoom in/out relative to the center of viewport.
	 */
	virtual void ProcessCameraZoom (MousePointer* mouse, Float zoomAmount, bool bCenteredZoom);

	/**
	 * Iterates through each selected object to destroy them.
	 */
	virtual void DeleteAllSelectedObjects ();

	virtual void DeselectAllObjects ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePortRefreshCurve (NodePort* changedPort);
	virtual bool HandleGrabOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt);
	virtual bool HandleContextClose ();
};
SD_END