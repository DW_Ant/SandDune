/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphNode.h

  A GraphNode is a GuiComponent within a GraphEditor that is typically responsible for one
  action for an asset. An action could range from: start of a function, call a function, branching, etc...

  Typically a GraphNode would contain a header text describing what it's doing, and NodePorts that are
  responsible for linking GraphNodes together.

  The base class also implements the move node and selection functionalities.
=====================================================================
*/

#pragma once

#include "Editor.h"
#include "GraphInterface.h"

SD_BEGIN
class NodePort;
class GraphFunction;

class EDITOR_API GraphNode : public GuiComponent, public GraphInterface
{
	DECLARE_CLASS(GraphNode)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	//Data struct used to create a port
	struct EDITOR_API SInitPort
	{
		/* If true, then this port is an input port, and it'll reside on the left side of this node. Otherwise it'll reside on the right side as an output port. */
		bool bIsInput;

		/* GraphNode class to use when creating this port. */
		const DClass* PortClass;

		/* Multiplies the scale of the NodePort by this value. */
		Float ScaleMultiplier;

		/* Text to describe the port. If empty then it's not going to create a LabelComponent associated with this port. */
		DString Label;

		/* If not empty, this text will appear as a tooltip when hovering over this port. */
		DString TooltipText;

		/* If not nullptr, then this port will have an GuiComponent beside it, allowing the developer to make inlined edits without establishing a connection. */
		const DClass* InlinedComponentClass;

		SInitPort (bool inIsInput, const DClass* inPortClass, const DString& inLabel);
	};

	struct EDITOR_API SPort
	{
		NodePort* Port;

		/* Label describing what this port is for. */
		LabelComponent* PortLabel;

		/* If enabled, this allows the user to set the port value through this component instead of connecting the port to something else. */
		GuiComponent* PortInlineEdit;

		SPort ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then this node will automatically adjust its size based on its sub components and its attributes every time one of its properties changes. */
	bool bAutoUpdateSize;

	/* Number of pixels between each port. */
	Float PortVerticalSpacing;

	/* The number of pixels between the port and the edge of this node. */
	Float PortHorizontalSpacing;

	/* The default size each port is without any scalar multipliers. */
	Float PortBaseSize;

	/* The number of pixels between the longest input and output ports.
	Not applicable if the header is long enough. */
	Float MinSpacingBetweenInputAndOutput;

	/* Font size to use for port labels and inlined text. */
	Int LabelFontSize;

	/* Number of extra vertical pixels to insert between the label and its associated port. */
	Float LabelSpacing;

	/* Determines how wide each inlined text field can be within this node. */
	Float InlinedFieldWidth;

	/* Determines the thickness of the highlight selection. */
	Float HighlightThickness;

	/* Color to use when highlighting the perimeter of this node. */
	Color HighlightColor;

	/* Color to use for the background components. */
	Color BackgroundColor;

	/* Becomes true if this node is selected. */
	bool bSelected;

	/* Becomes true if the user is currently dragging this node around. */
	bool bDragging;

	/* If true, then the user is able to delete this node. */
	bool bDeletable;
	
	DPointer<FrameComponent> Background;
	LabelComponent* HeaderText;

	/* List of all node ports intended for the left side of the node. */
	std::vector<SPort> InputPorts;

	/* List of all node ports intended for the right side of this node. */
	std::vector<SPort> OutputPorts;

private:
	/* Location where the previous mouse press was detected. This is set even if it's outside this component's bounds. */
	Vector2 PrevMousePress;

	/* The minimum amount of distance the mouse must drag between mouse press and mouse release events in order to prevent the node from interpreting the release event
	as a toggle select event. Units are in pixels. */
	Float MouseDragSelectThreshold;

	/* Becomes true if the mouse moved enough (from the initial mouse press) to determine that the mouse release action is merely a mouse drag instead of selecting something.
	This handles cases such as pressing down on something, moving away then moving back to original position before mouse release. 
	This also handles the case where if the user is dragging the mouse (to move the camera), it would not deselect nodes. */
	bool bIsMousePressDragAction;

	/* Location offset when the mouse clicked on this node. Used to determining this node's new location relative to the mouse when it's dragged around. */
	Vector2 MouseDragOffset;

	/* Timestamp when this node was last clicked (in seconds). This is used for detecting double click. If negative, then the next mouse click cannot be a double click event. */
	Float DoubleClickTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	//GraphInterface
	virtual bool AllowCameraMovement (const Vector2& mousePos) const override;
	virtual void ProcessMouseLock (const Vector2& oldMousePos, const Vector2& newMousePos, Float camZoom) override;
	virtual bool IsSelected () const override;
	virtual void SetSelected (bool newSelected) override;
	virtual bool IsDeletable () const override;
	virtual void DeleteEditorObject () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets up a new port associated with this node. The node will automatically scale based on the contents of its ports.
	 * If successful, it'll return a valid pointer to the SPort instance.
	 */
	virtual SPort* CreatePort (const SInitPort& initPort);

	/**
	 * Removes an input or output port of the specified index.
	 * Returns true if a port is removed.
	 */
	virtual bool RemovePort (bool isInputPort, size_t portIdx);

	/**
	 * Returns the GraphFunction instance this node is helping to define.
	 */
	virtual GraphFunction* GetConstructedFunction () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAutoUpdateSize (bool isAutoUpdateSize);
	virtual void SetPortVerticalSpacing (Float newPortVerticalSpacing);
	virtual void SetPortHorizontalSpacing (Float newPortHorizontalSpacing);
	virtual void SetPortBaseSize (Float newPortBaseSize); //Must be set before ports are added due to lost scalar multipliers.
	virtual void SetMinSpacingBetweenInputAndOutput (Float newMinSpacingBetweenInputAndOutput);
	virtual void SetLabelFontSize (Int newLabelFontSize);
	virtual void SetLabelSpacing (Float newLabelSpacing);
	virtual void SetInlinedFieldWidth (Float newInlinedFieldWidth);
	virtual void SetHighlightThickness (Float newHighlightThickness);
	virtual void SetHighlightColor (Color newHighlightColor);
	virtual void SetBackgroundColor (Color newBackgroundColor);
	virtual void SetDeletable (bool newDeletable);
	virtual void SetHeaderText (const DString& newHeaderText);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetPortVerticalSpacing () const
	{
		return PortVerticalSpacing;
	}

	inline Float GetPortHorizontalSpacing () const
	{
		return PortHorizontalSpacing;
	}

	inline Float GetPortBaseSize () const
	{
		return PortBaseSize;
	}

	inline Float GetMinSpacingBetweenInputAndOutput () const
	{
		return MinSpacingBetweenInputAndOutput;
	}

	inline Int GetLabelFontSize () const
	{
		return LabelFontSize;
	}

	inline Float GetLabelSpacing () const
	{
		return LabelSpacing;
	}

	inline Float GetInlinedFieldWidth () const
	{
		return InlinedFieldWidth;
	}

	inline Float GetHighlightThickness () const
	{
		return HighlightThickness;
	}

	inline Color GetHighlightColor () const
	{
		return HighlightColor;
	}

	inline Color GetBackgroundColor () const
	{
		return BackgroundColor;
	}

	inline FrameComponent* GetBackground () const
	{
		return Background.Get();
	}

	inline const std::vector<SPort>& ReadInputPorts () const
	{
		return InputPorts;
	}

	inline const std::vector<SPort>& ReadOutputPorts () const
	{
		return OutputPorts;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each component (port labels, ports, and inlined fields) to update their positions and size based on this node's attributes.
	 */
	virtual void RefreshNodeComponents ();
	virtual void RefreshNodeComponents (const std::vector<SPort>& portList, bool bIsInputList);

	/**
	 * Removes all labels, ports, and inlined components that are associated with a port.
	 */
	virtual void RemovePortComponents ();

	/**
	 * Checks the components attached to this Entity to evaluate how large this node should be.
	 */
	virtual void UpdateNodeSize ();

	/**
	 * Iterates through this node's components to figure out how large the node should be.
	 */
	virtual void CalcNodeSize (Vector2& outNodeSize) const;

	/**
	 * The user clicked on this node twice in rapid succession. This method is executed whenever the user double clicks this node.
	 */
	virtual void ProcessDoubleClick ();

	virtual void ProcessPortConnectionChanged (SPort* portData);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePortConnectionChanged (NodePort* changedPort);
};
SD_END