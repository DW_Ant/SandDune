/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableArrayBinder.h
  A class wrapper that associates a vector to an EditableArray. The main purpose of this Binder
  is to redirect all template-related functions to this wrapper. That way, EditableArray will
  not be a templated class, allowing for utilities such as DClasses, CDOs, and styling.

  This also allows the EditableArray to have its own cpp file. Although it's possible to have templated
  classes be defined in cpp files, imported DLLs cannot instantiate new template instances without it.
  This would prevent user-defined instances from being defined.

  BaseEditableArrayBinder
  The nontemplated abstract class that the EditableArray will call into.
  
  EditableArrayBinder
  This is a templated class that has access to the actual data. Use this type for simple types and structs.
  This assumes the array type has a default value, and can be loaded/saved to config via single string.
=====================================================================
*/

#pragma once

#include "EditableObjectComponent.h"
#include "Editor.h"
#include "EditorInterface.h"
#include "EditPropertyComponent.h"

SD_BEGIN
class BaseEditableArrayBinder
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this is bound to a vector.
	 */
	virtual bool IsBounded () const = 0;

	/**
	 * Methods that allow the item to transfer data between the array and GUI.
	 * Returns true on success.
	 */
	virtual bool CopyGuiToArray (EditPropertyComponent* srcGui, size_t destIdx) const = 0;
	virtual bool CopyArrayToGui (size_t srcIdx, EditPropertyComponent* destGui) const = 0;

	/**
	 * Returns the number of elements of the vector this is bound to.
	 */
	virtual size_t GetArraySize () const = 0;

	/**
	 * Sets the bound vector size to the specified size.
	 */
	virtual void SetArraySize (EditPropertyComponent* editComp, size_t newSize) = 0;

	/**
	 * Inserts an element to the Array at the specified index.
	 * This function assumes that the associated vector will insert the element before the EdtiableArray adds the EditableComponent associated with the insertIdx.
	 */
	virtual void InsertArrayElement (EditPropertyComponent* editComp, size_t insertIdx) = 0;

	/**
	 * Moves the specified element from the old index to move towards the desired new index.
	 * This would displace the elements between the two indices.
	 */
	virtual void MoveArrayElement (EditPropertyComponent* editComp, size_t oldIdx, size_t newIdx) = 0;

	/**
	 * Removes the specified element from the array.
	 */
	virtual void RemoveArrayElement (size_t removeIdx) = 0;

	/**
	 * Saves the contents of the associated vector to the given config file.
	 */
	virtual void SaveConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) = 0;

	/**
	 * Populates the associated vector with the contents of the given config file.
	 */
	virtual void LoadConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) = 0;

	/**
	 * Appends the contents of the associated vector to the DataBuffer in binary format.
	 */
	virtual void SaveBinary (DataBuffer& outData) const = 0;

	/**
	 * Populates the associated vector with the contents of the DataBuffer.
	 * Returns false on read error.
	 */
	virtual bool LoadBinary (const DataBuffer& incomingData) = 0; 
};

/**
 * Binder used to bind object pointers to an EditableArray.
 * The object pointer must be an EditorInterface.
 */
template <class V>
class ObjectArrayBinder : public BaseEditableArrayBinder
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	std::vector<V*>* EditedArray;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ObjectArrayBinder (std::vector<V*>* inEditedArray) :
		EditedArray(inEditedArray)
	{
		//Noop
	}

	ObjectArrayBinder (const ObjectArrayBinder& cpyObj) :
		EditedArray(cpyObj.EditedArray)
	{
		//Noop
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsBounded () const override
	{
		return (EditedArray != nullptr);
	}

	virtual bool CopyGuiToArray (EditPropertyComponent* srcGui, size_t destIdx) const override
	{
		if (!IsBounded() || srcGui == nullptr || !ContainerUtils::IsValidIndex(*EditedArray, destIdx))
		{
			return false;
		}

		if (EditableObjectComponent* objComp = dynamic_cast<EditableObjectComponent*>(srcGui))
		{
			EditedArray->at(destIdx) = dynamic_cast<V*>(objComp->GetEditedObject());
			return true;
		}

		return false;
	}

	virtual bool CopyArrayToGui (size_t srcIdx, EditPropertyComponent* destGui) const override
	{
		if (!IsBounded() || destGui == nullptr || !ContainerUtils::IsValidIndex(*EditedArray, srcIdx))
		{
			return false;
		}

		if (EditableObjectComponent* objComp = dynamic_cast<EditableObjectComponent*>(destGui))
		{
			objComp->SetEditedObject(EditedArray->at(srcIdx));
			return true;
		}

		return false;
	}

	virtual size_t GetArraySize () const override
	{
		if (EditedArray != nullptr)
		{
			return EditedArray->size();
		}

		return 0;
	}

	virtual void SetArraySize (EditPropertyComponent* editComp, size_t newSize) override
	{
		CHECK(editComp != nullptr)

		if (EditedArray != nullptr)
		{
			size_t prevCapacity = EditedArray->capacity();
			EditedArray->resize(newSize);

			if (prevCapacity != EditedArray->capacity())
			{
				//The vector resized, it's possible that the editable object pointers are pointing to garbage memory now. Need to move them to the new pointer addresses.
				for (size_t i = 0; i < EditedArray->size(); ++i)
				{
					if (EditableObjectComponent* subProp = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(i)))
					{
						subProp->MovePointer(EditedArray->at(i));
					}
				}
			}
		}
	}

	virtual void InsertArrayElement (EditPropertyComponent* editComp, size_t insertIdx) override
	{
		if (EditedArray == nullptr)
		{
			return;
		}

		size_t prevCapacity = EditedArray->capacity();
		EditedArray->insert(EditedArray->begin() + insertIdx, nullptr);

		if (prevCapacity != EditedArray->capacity())
		{
			for (size_t i = 0; i < insertIdx; ++i)
			{
				if (EditableObjectComponent* editObj = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(i)))
				{
					editObj->MovePointer(EditedArray->at(i));
				}
			}

			for (size_t i = insertIdx + 1; i < EditedArray->size(); ++i)
			{
				//This function assumes that the vector is inserted before the EditPropertyComponent associated with insertIdx is inserted.
				if (EditableObjectComponent* editObj = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(i - 1)))
				{
					editObj->MovePointer(EditedArray->at(i));
				}
			}
		}
	}

	virtual void MoveArrayElement (EditPropertyComponent* editComp, size_t oldIdx, size_t newIdx) override
	{
		if (EditedArray == nullptr)
		{
			return;
		}

		CHECK(oldIdx != newIdx && oldIdx < EditedArray->size() && newIdx < EditedArray->size())

		V* movedElement = EditedArray->at(oldIdx);

		//It'll be easier to move pointers around rather than moving the vector elements and dealing with capacity changes.
		if (oldIdx < newIdx)
		{
			for (size_t i = oldIdx; i < newIdx; ++i)
			{
				EditedArray->at(i) = EditedArray->at(i+1);

				if (EditableObjectComponent* editObjComp = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(i+1)))
				{
					editObjComp->MovePointer(EditedArray->at(i));
				}
			}
		}
		else
		{
			for (size_t i = oldIdx; i > newIdx; --i)
			{
				EditedArray->at(i) = EditedArray->at(i-1);

				if (EditableObjectComponent* editObjComp = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(i-1)))
				{
					editObjComp->MovePointer(EditedArray->at(i));
				}
			}
		}

		EditedArray->at(newIdx) = movedElement;
		if (EditableObjectComponent* editObjComp = dynamic_cast<EditableObjectComponent*>(editComp->ReadSubPropertyComponents().at(oldIdx)))
		{
			editObjComp->MovePointer(EditedArray->at(newIdx));
		}
	}

	virtual void RemoveArrayElement (size_t removeIdx) override
	{
		if (EditedArray != nullptr)
		{
			EditedArray->erase(EditedArray->begin() + removeIdx);

			for (size_t i = removeIdx + 1; i < EditedArray->size(); ++i)
			{
				if (EditableObjectComponent* editObj = dynamic_cast<EditableObjectComponent*>(EditedArray->at(i)))
				{
					editObj->MovePointer(EditedArray->at(i - 1));
				}
			}
		}
	}

	virtual void SaveConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) override
	{
		//Implement when object serialization is implemented
	}

	virtual void LoadConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) override
	{
		//Implement when object serialization is implemented
	}

	virtual void SaveBinary (DataBuffer& outData) const override
	{
		//Implement when object serialization is implemented
	}

	virtual bool LoadBinary (const DataBuffer& incomingData) override
	{
		//Implement when object serialization is implemented
		return true;
	}
};

template <class V>
class EditableArrayBinder : public BaseEditableArrayBinder
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to the vector this binder is modifying. */
	std::vector<V>* EditedArray;

	/* The default item to use when inserting new elements to the EditedArray. */
	V DefaultItem;

	/* Lambda used to produce a vector element from the DataBuffer. Returns true if it successfully extracted data from the DataBuffer.
	It is important that the data saved to the DataBuffer is in the exact order the variables appear in the EditableProperties! */
	std::function<bool(const DataBuffer& /*src*/, V& /*outDest*/)> OnBufferToItem;
	
	/* Lambda used to generate a DataBuffer from a vector element.
	It is important that the data saved to the DataBuffer is in the exact order the variables appear in the EditableProperties! */
	std::function<void(const V& /*src*/, DataBuffer& /*outDest*/)> OnItemToBuffer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditableArrayBinder (std::vector<V>* inEditedArray, V inDefaultItem, const std::function<bool(const DataBuffer&, V&)> inBufferToItem, const std::function<void(const V&, DataBuffer&)> inItemToBuffer) :
		EditedArray(inEditedArray),
		DefaultItem(inDefaultItem),
		OnBufferToItem(inBufferToItem),
		OnItemToBuffer(inItemToBuffer)
	{
		CHECK(OnBufferToItem != nullptr && OnItemToBuffer != nullptr)
	}

	EditableArrayBinder (EditableArrayBinder& cpyFrom) :
		EditedArray(cpyFrom.EditedArray),
		DefaultItem(cpyFrom.DefaultItem),
		OnBufferToItem(cpyFrom.OnBufferToItem),
		OnItemToBuffer(cpyFrom.OnItemToBuffer)
	{
		//Noop
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsBounded () const override
	{
		return (EditedArray != nullptr);
	}

	virtual bool CopyGuiToArray (EditPropertyComponent* srcGui, size_t destIdx) const override
	{
		if (srcGui == nullptr || !ContainerUtils::IsValidIndex(*EditedArray, destIdx))
		{
			return false;
		}

		DataBuffer buffer;
		srcGui->CopyToBuffer(OUT buffer);
		return OnBufferToItem(buffer, OUT EditedArray->at(destIdx));
	}

	virtual bool CopyArrayToGui (size_t srcIdx, EditPropertyComponent* destGui) const override
	{
		if (destGui == nullptr || !ContainerUtils::IsValidIndex(*EditedArray, srcIdx))
		{
			return false;
		}

		DataBuffer buffer;
		OnItemToBuffer(EditedArray->at(srcIdx), OUT buffer);
		return destGui->CopyFromBuffer(buffer);
	}

	virtual size_t GetArraySize () const override
	{
		if (EditedArray != nullptr)
		{
			return EditedArray->size();
		}

		return 0;
	}

	virtual void SetArraySize (EditPropertyComponent* editComp, size_t newSize) override
	{
		if (EditedArray != nullptr)
		{
			EditedArray->resize(newSize);
		}
	}

	virtual void InsertArrayElement (EditPropertyComponent* editComp, size_t insertIdx) override
	{
		if (EditedArray != nullptr)
		{
			EditedArray->insert(EditedArray->begin() + insertIdx, DefaultItem);
		}
	}

	virtual void MoveArrayElement (EditPropertyComponent* editComp, size_t oldIdx, size_t newIdx) override
	{
		if (EditedArray != nullptr)
		{
			if (newIdx > oldIdx)
			{
				//Need to increment newIdx by one since the oldIdx is about to be removed, which would displace the newIdx.
				newIdx++;
			}

			EditedArray->insert(EditedArray->begin() + newIdx, EditedArray->at(oldIdx));

			if (newIdx < oldIdx)
			{
				//When inserting earlier in the vector, the indices following it would be displaced by one.
				oldIdx++;
			}

			EditedArray->erase(EditedArray->begin() + oldIdx);
		}
	}

	virtual void RemoveArrayElement (size_t removeIdx) override
	{
		if (EditedArray != nullptr)
		{
			EditedArray->erase(EditedArray->begin() + removeIdx);
		}
	}

	virtual void SaveConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) override
	{
		config->SaveArray(sectionName, propertyName, *EditedArray);
	}

	virtual void LoadConfig (ConfigWriter* config, const DString& sectionName, const DString& propertyName) override
	{
		config->GetArrayValues(sectionName, propertyName, OUT *EditedArray);
	}

	virtual void SaveBinary (DataBuffer& outData) const override
	{
		Int numElements(EditedArray->size());
		outData << numElements;

		for (size_t i = 0; i < EditedArray->size(); ++i)
		{
			OnItemToBuffer(EditedArray->at(i), OUT outData);
		}
	}

	virtual bool LoadBinary (const DataBuffer& incomingData) override
	{
		CHECK(EditedArray != nullptr)

		Int numElements;
		if ((incomingData >> numElements).HasReadError())
		{
			return false;
		}

		EditedArray->resize(numElements.ToUnsignedInt());
		for (Int i = 0; i < numElements; ++i)
		{
			if (!OnBufferToItem(incomingData, EditedArray->at(i.ToUnsignedInt())))
			{
				return false;
			}
		}

		return true;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline std::vector<V>* GetEditedArray () const
	{
		return EditedArray;
	}
};
SD_END