/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableDString.h
  A property component that allows the end user to edit a DString variable within an editor.
=====================================================================
*/

#pragma once

#include "EditableField.h"

SD_BEGIN
class EDITOR_API EditableDString : public EditableField
{
	DECLARE_CLASS(EditableDString)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ELineType
	{
		/* The text field remains a direct sub component to this Entity.
		Prohibits user from adding new line characters.
		Recommended that text fields are tall enough only for one line. */
		LT_SingleLine,

		/* The text field resides inside a scrollable Entity. This Entity contains a scrollbar component that views said scrollable Entity.
		Allows user to add multiple lines.
		Recommend that this component is tall to render multiple lines. */
		LT_MultiLine
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorString : public SInspectorProperty
	{
	public:
		DString* AssociatedVariable;
		DString PermittedChars;
		bool bMultiLine;
		Int MaxCharacters;
		DString DefaultString;

		/* Determines the initial string. Only applicable if AssociatedVariable is nullptr and InitType is IIV_UseOverride */
		DString InitOverride;

		SInspectorString (const DString& inPropertyName, const DString& inTooltipText, DString* inAssociatedVariable, const DString& inDefaultString = DString::EmptyString);
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Regex pattern of permitted characters. If empty, then any character is permitted. */
	DString PermittedChars;

	/* The actual DString this component is editing. */
	DString* EditedString;

	/* The string to revert to when clicking on reset to defaults. */
	DString DefaultString;

	/* Component responsible for updating the field whenever EditedString has changed. */
	TickComponent* UpdateStringTick;

	/* Only applicable for multi line EditableDStrings. If this is not null, it's safe to assume this is a multi line component. */
	ScrollbarComponent* MultiLineScrollbar;

private:
	/* Variable the EditedString will bind to if this component is bound to a local variable (from calling BindToLocalVariable). */
	DString LocalString;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual void UnbindVariable () override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual void CopyToBuffer (DataBuffer& copyTo) const override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual void SetLineHeight (Float newLineHeight) override;

protected:
	virtual void InitializeComponents () override;
	virtual bool IsWithinEditingField (const Vector2& mousePos) const override;
	virtual void ApplyEdits () override;
	virtual DString GetPermittedInputChars () const override;
	virtual void HandleReturn (TextFieldComponent* uiComponent) override;
	virtual void GrabBarrier () override;
	virtual void ReleaseBarrier () override;
	virtual DString ConstructValueAsText () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pairs this component to the variable so that any changes to this component is reflected in its associated variable.
	 * It's important to keep this name the same as others to remain compatible with the templated EditableArray.
	 */
	virtual void BindVariable (DString* newEditedString);

	/**
	 * Configures the property value field to either remain single line or reside within a scrollbar for multi line.
	 */
	virtual void SetLineType (ELineType newLineType);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPermittedChars (const DString& newPermittedChars);

	/**
	 * The name of this function must match others to allow the templated StructElement to interface with this function.
	 */
	virtual void SetDefaultValue (const DString& newDefaultString);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetPermittedChars () const
	{
		return PermittedChars;
	}

	inline DString* GetEditedString () const
	{
		return EditedString;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleUpdateStringTick (Float deltaSec);
};
SD_END