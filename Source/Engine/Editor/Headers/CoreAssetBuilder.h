/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CoreAssetBuilder.h
  An Entity responsible for building various GraphAsset instances to bind
  to native associated functions that the ClmEngine already comes with.

  Although it would be sufficient enough to reference those Subroutines directly,
  these assets exist to add editor meta data associated with them such as
  comments and searchable keywords.

  The compiler will check these assets for verification (to ensure parameters match up).
  However the assets may choose to not be compiled. For example, if the ClmEngineComponent already
  defines every function the asset needed, there's no need for the compiler to generate that asset instance.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class GraphAsset;
class GraphFunction;

class EDITOR_API CoreAssetBuilder : public Object
{
	DECLARE_CLASS(CoreAssetBuilder)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	static const DString LOCALIZATION_SECTION;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Various functions to build essential graph asset instances. These functions will return the created graph asset instance if successful.
	 * These instances are automatically registered to the local EditorEngineComponent.
	 */
	static GraphAsset* CreateCoreAsset (ClmEngineComponent* clmEngine);
	static GraphAsset* CreateMathAsset (ClmEngineComponent* clmEngine);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes common properties that will not change between the various GraphAssets.
	 */
	static void InitAsset (GraphAsset* asset);

	/**
	 * Creates and returns a GraphFunction reference that is bound to a global function that matches the given name.
	 */
	static GraphFunction* InitGlobalFunction (ClmEngineComponent* clmEngine, GraphAsset* functionOwner, const HashedString& functionName);

	/**
	 * Iterates through the Subroutine's in and out parameters and generates GraphVariables instances from those.
	 * Those GraphVariables are then assigned to the GraphFunction's parameters.
	 */
	static void InitFunctionParams (Subroutine* nativeAssociation, GraphFunction* funcToInit);

	/**
	 * Creates and initializes a function intended to be a comparison function. It expects 2 in parameters, 1 out parameter.
	 */
	static GraphFunction* InitCompareFunction (ClmEngineComponent* clmEngine, GraphAsset* functionOwner, const HashedString& functionName, TextTranslator* translator, const DString& translationKey, const DString& contextCategory);

	virtual void MakeAbstract () = 0;
};
SD_END