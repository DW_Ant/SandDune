/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableFloat.h
  A property component that allows the end user to edit an Float variable within an editor.
=====================================================================
*/

#pragma once

#include "EditableField.h"

SD_BEGIN
class EDITOR_API EditableFloat : public EditableField
{
	DECLARE_CLASS(EditableFloat)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorFloat : public SInspectorProperty
	{
		bool bHasMinValue;
		bool bHasMaxValue;
		bool bEnableDecimal;
		Range<Float> ValueRange;
		Float* AssociatedVariable;
		Float DefaultValue;

		/* Determines the initial float. Only applicable if AssociatedVariable is nullptr and InitType is IIV_UseOverride */
		Float InitOverride;

		SInspectorFloat (const DString& inPropertyName, const DString& inTooltipText, Float* inAssociatedVariable, Float inDefaultValue = 0.f);
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true then the min/max bounds are enabled. */
	bool bHasMinValue;
	bool bHasMaxValue;

	/* If true, then the user can place a decimal point. */
	bool bEnableDecimal;

	/* The min and max values to clamp the float to. These are disabled if bHasMinValue and bHasMaxValue respectively. */
	Float MinValue;
	Float MaxValue;

	/* The actual Float this component is editing. */
	Float* EditedFloat;

	Float DefaultFloat;

	/* Component responsible for updating the field whenever EditedFloat has changed. */
	TickComponent* UpdateFloatTick;

private:
	/* Variable the EditedFloat will bind to if this component is bound to a local variable (from calling BindToLocalVariable). */
	Float LocalFloat;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void CopyToBuffer (DataBuffer& copyTo) const override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual void UnbindVariable () override;

protected:
	virtual DString ConstructValueAsText () const override;
	virtual void ApplyEdits () override;
	virtual DString GetPermittedInputChars () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pairs this component to the variable so that any changes to this component is reflected in its associated variable.
	 * It's important to keep this name the same as others to remain compatible with the templated EditableArray.
	 */
	virtual void BindVariable (Float* newEditedFloat);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetHasMinValue (bool bNewHasMinValue);
	virtual void SetHasMaxValue (bool bNewHasMaxValue);
	virtual void SetEnableDecimal (bool bNewEnableDecimal);
	virtual void SetMinValue (Float newMinValue);
	virtual void SetMaxValue (Float newMaxValue);

	/**
	 * The name of this function must match others to allow the templated StructElement to interface with this function.
	 */
	virtual void SetDefaultValue (Float newDefaultFloat);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool HasMinValue () const
	{
		return bHasMinValue;
	}

	inline bool HasMaxValue () const
	{
		return bHasMaxValue;
	}

	inline bool IsDecimalEnabled () const
	{
		return bEnableDecimal;
	}

	inline Float GetMinValue () const
	{
		return MinValue;
	}

	inline Float GetMaxValue () const
	{
		return MaxValue;
	}

	inline Float* GetEditedInt () const
	{
		return EditedFloat;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Applies the clamp constraints in the text field. This does not write to the editable float
	 */
	virtual void ApplyClamps ();



	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleUpdateFloatTick (Float deltaSec);
};
SD_END