/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphInterface.h

  An interface for Entities that reside in a graph editor.

  The graph owner may invoke various functions that implement this interface to influence its behaviors.
  The graph owner will iterate through its immediate components, and if any of those components implement this interface,
  then those components can influence the owner's behavior.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EDITOR_API GraphInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * The GraphEditor received a mouse move event to adjust its camera. The interface owner should return true to permit it.
	 * It should return false if it wants to prohibit the GraphEditor from adjusting its camera.
	 */
	virtual bool AllowCameraMovement (const Vector2& mousePos) const = 0;

	/**
	 * Notifies the graph object that the system is snapping the mouse from the old position to the new position without user input.
	 */
	virtual void ProcessMouseLock (const Vector2& oldMousePos, const Vector2& newMousePos, Float camZoom)
	{
		//Noop
	}

	virtual bool IsSelected () const = 0;
	virtual void SetSelected (bool newSelected) = 0;

	/**
	 * Returns true if the editor is permitted to destroy this object.
	 */
	virtual bool IsDeletable () const = 0;

	/**
	 * Removes this object from the editor. Most classes would simply call Destroy().
	 */
	virtual void DeleteEditorObject () = 0;
};
SD_END