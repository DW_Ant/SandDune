/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Editor.h
  Contains important file includes and definitions for the Editor module.

  The Editor module defines essential utilities most editors (such as map, Gui, and particle
  editors) will be using.  This module will define the following:
  
	-Gui components that allows editing properties.
	-Utilities for adding meta data to each variable (such as clamping and tooltips).
=====================================================================
*/

#pragma once

#include "Engine\ContentLogic\Headers\ContentLogicClasses.h"
#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"

#ifdef WITH_MULTI_THREAD
#include "Engine\MultiThread\Headers\MultiThreadClasses.h"
#endif

#ifdef WITH_TIME
#include "Engine\Time\Headers\TimeClasses.h"
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef EDITOR_EXPORT
		#define EDITOR_API __declspec(dllexport)
	#else
		#define EDITOR_API __declspec(dllimport)
	#endif
#else
	#define EDITOR_API
#endif

#define TICK_GROUP_EDITOR "Editor"
#define TICK_GROUP_PRIORITY_EDITOR 5050 //Tick just before the Gui TickGroup since the GuiComponents (like the VerticalList and scrollbars) may adjust based on the size of the EditPropertyComponents.

#define MULTI_THREAD_EDITOR_WORKER_IDX 3 //3 Because it looks like a backwards E. Get it? E because Editor? Eh?

SD_BEGIN
extern LogCategory EDITOR_API EditorLog;

//Global variable name used to define the asset types. Editors can reference this variable in a dev asset file to get an early warning to see if it matches the type it expects.
extern DString EDITOR_API ASSET_TYPE;
SD_END