/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphVariable.h
  An editor object that corresponds to a CLM ScriptVariable. This class
  will encompass all variable types. A GraphVariable can be used for
  ContentObject's member variables, local variables, and function parameters.

  The intention of this variable is to allow developers to configure the
  variables for a Graph. This is different from any editable variable.
  An editable variable is simply an existing native variable exposed
  to an editor (such as a map editor or texture editor). The GraphVariable
  are dynamic variables a developer has created to help structure the
  logic for their assets.

  Differences between the various variable types:
  Core types: DString, Int, Float, Bool
  These are wrappers to primitives. These must have small profiles since
  they are used almost everywhere. These classes derive from DProperties,
  allowing them to interface with DataBuffers as well as converting
  to and from strings.

  ScriptVariable
  A generic variable that interfaces with the ContentLogic module. This
  variable is evaluated during runtime. This is slower compared to the core
  types, but allows for these variables to be dynamically stored as
  Subroutine local variables and ContentObject member variables.

  EditPropertyComponents: EditableDString, EditableInt, EditableFloat
  These are GuiComponents that allows the developer to edit a variable
  the GUI is bound to.

  GraphVariable
  This variable is an object that extends a ScriptVariable. This variable
  contain editor functionality such as a comment, default value, maps to a
  EditPropertyComponent class for an inspector. If the GraphVariable contains
  data that doesn't exist in the ScriptVariable, then they are most likely
  compiled out when exporting these variables to the ContentLogic module.
=====================================================================
*/

#pragma once

#include "DynamicEnum.h"
#include "EditableEnum.h"
#include "Editor.h"
#include "EditorInterface.h"

SD_BEGIN
class EditableStruct;
class EditPropertyComponent;
class GraphAsset;
class InspectorList;

class EDITOR_API GraphVariable : public ScriptVariable, public EditorInterface
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	DEFINE_DYNAMIC_ENUM_9VAL(EVarType, VT_Unknown, VT_Bool, VT_Int, VT_Float, VT_String, VT_Function, VT_Obj, VT_Class, VT_Pointer);

	/**
	 * Type of enum used to identify what kind of properties should appear when displaying this property in the inspector.
	 */
	enum EInspectorType
	{
		IT_None,
		IT_MemberVariable,
		IT_InParam,
		IT_OutParam
	};

	/**
	 * Enum used to map an EditPropertyComponent to a variable property.
	 */
	enum EVarAttribute
	{
		VA_Unknown, //Use this to skip a SubPropertyComponent
		VA_VarName,
		VA_VarType,
		VA_ClassType,
		VA_DefaultValue,
		VA_Comment,
		VA_Protected,
		VA_Static,
		VA_ShowParamName,
		VA_PassByReference,
		VA_Relative //When overriding a variable value, is it relative to the parent's default?
	};

	enum EVarPermissions : unsigned char
	{
		VP_None = 0x00,
		VP_Read = 0x01,
		VP_Write = 0x02,
		VP_All = 0xFF
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various delegates broadcasted whenever this variable changes. */
	static MulticastDelegate<const DString& /*prevId*/, const DString& /*varId*/, const DString& /*newName*/> OnNameChanged;
	static MulticastDelegate<const DString& /*varId*/, const DString& /*newComment*/> OnCommentChanged;
	static MulticastDelegate<const DString& /*varId*/> OnAttributeChanged;

	/* Regex used to identify which characters are permitted when naming GraphVariables. */
	static const DString PERMITTED_CHARS_FOR_NAME;

protected:
	/* If not empty, then this name is displayed when this variable is used in a GraphNode. This is typically set for native functions for localization purposes.
	If it's empty, the nodes will display the variable name, itself. */
	DString DisplayName;

	/* Enum used to identify how this variable should appear when generating an inspector of its properties. */
	EInspectorType InspectorType;

	/* Reference to the Editor object that owns this variable. Typically this would be the GraphFunction (for local or parameters) or GraphAsset (for member variables). */
	EditorInterface* VarOwner;

	/* If this variable is an object or class type, the ClassConstraint is used to restrict the type to be either this class or any of its subclasses.
	If nullptr, then this GraphVariable is compatible of any other variable regardless of its ClassConstraint. Useful for generic functions like checking if a pointer is valid.
	This is not used if the variable type is neither obj or class type. */
	GraphAsset* ClassConstraint;

	/* The data buffer containing the data of this variable's default value.
	If empty, it'll use the ScriptVariable's default value (eg: 0 or empty string). */
	DataBuffer DefaultValue;

	/* Optional human readable text displayed in the editor that describes this variable. */
	DString Comment;

	//Member Variables

	/* If true, then this variable is protected. Cannot be referenced from external classes.
	Sublcasses can still reference this variable.
	This is only applicable for member variables. */
	bool bProtected;

	/* If true, then this variable does not require an object instance. Instead it can be referenced within scope.
	There are only one instance of this variable even if there are multiple ContentObjects.
	This is only applicable for member variables. */
	bool bStatic;

	//Parameters

	/* If true, then the name of this variable will be displayed above the port in the graph nodes. If false, then only the port is visible. */
	bool bShowParamName;

	/* If true, then the inline component associated with the port representing this parameter will be visible.
	Otherwise the inline component would be hidden.
	The variable automatically hides its inline component if its type doesn't support inlining (eg: object pointers). */
	bool bHasInlineComp;

	/* If true, then variables linked to this parameter will be references instead of copies of the variable.
	This will allow the function to write to the calling function's variable.
	NOTE: Some variables (such as strings) will be automatically compiled by reference instead of copies if the function doesn't write to the variable for performance reasons.
	This is only applicable for parameter variables. */
	bool bPassByReference;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GraphVariable ();
	GraphVariable (const HashedString& inVariableName, EditorInterface* inVarOwner);
	explicit GraphVariable (const ScriptVariable& cpyObj);
	explicit GraphVariable (const GraphVariable& cpyObj);

	virtual ~GraphVariable ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const GraphVariable& cpyFrom);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void Reset () override;
	virtual void SetVariableName (const HashedString& newVariableName) override;

	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;

	virtual DString GetEditorObjName () const override;
	virtual void PopulateInspectorList (InspectorList& outList) override;
	virtual void DeleteEditorObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the DClass reference of an EditPropertyComponent that can be used to edit a variable of the given type.
	 * For example if the type is VT_String, this function will return EditableDString::SStaticClass.
	 * Returns nullptr if this type doesn't support a GUI class (eg: delegates and object pointers).
	 */
	static const DClass* GetEditClass (EVarType varType);

	/**
	 * Returns true if the given variable type can be assigned during editor time.
	 */
	static bool CanTypeBeConfiguredInEditor (EVarType varType);

	/**
	 * Returns true if the given type can be relative to another variable (eg: ints and floats can be relative, class pointers cannot).
	 */
	static bool IsTypeSupportingRelative (EVarType varType);

	/**
	 * Same as SetVariableName, but it will not invoke the reference updater nor would it invoke the OnNameChanged delegate.
	 */
	virtual void SetVariableName_Silent (const HashedString& newVariableName);

	/**
	 * Returns the GuiComponent class to use for inlined parameter components based on the variable type.
	 */
	virtual const DClass* GetInlinedComponentClass () const;
	virtual void InitializeInlinedComponent (GuiComponent* comp, bool isInput, Int fontSize) const;
	virtual void InitializeInlinedComponent (GuiComponent* comp, bool isInput, bool defaultValue, Int fontSize) const;
	virtual void InitializeInlinedComponent (GuiComponent* comp, bool isInput, const DString& defaultValue, Int fontSize) const;

	/**
	 * Saves the contents of this variable to the given config file.
	 */
	virtual void SaveToConfig (ConfigWriter* config, const DString& sectionName) const;

	/**
	 * Loads the contents of this variable from the given config file.
	 */
	virtual void LoadFromConfig (ConfigWriter* config, const DString& sectionName);

	/**
	 * Writes the contents of this variable to the given data buffer.
	 */
	virtual void Serialize (DataBuffer& outBuffer) const;

	/**
	 * Reads from the given buffer and stores its data to this variable. Returns true on success.
	 */
	virtual bool Deserialize (const DataBuffer& incomingData);

	/**
	 * Returns a formatted string that will be used to identify this particular instance.
	 * The scope is typically the name of the Entity that owns this function (such as the GraphAsset name).
	 * Example: "Variable'Scope.VariableName'"
	 */
	virtual DString GetIdentifyingName () const;
	static DString GetIdentifyingName (const DString& scope, const DString& varName);

	/**
	 * Parses the given variable ID to split it up into scope and its name.
	 * The variable ID's format is expected to be in the same format produced from GetIdentifyingName.
	 * Returns true if the given string is in the correct format.
	 */
	static bool ParseVariableId (const DString& varId, DString& outScope, DString& outVarName);

	/**
	 * Returns DisplayName if not empty. Otherwise it'll return the variable name.
	 */
	virtual DString GetPresentedName () const;

	/**
	 * Returns a string representing the contents of this variable's default value based on its type.
	 */
	virtual DString DefaultValueToString () const;

	/**
	 * Sets the default value data buffer based on the given string.
	 */
	virtual void SetDefaultValueFromString (const DString& newValue);

	/**
	 * Searches through the component chain, and if an EditPropertyComponent with a default value property ID is found, it'll
	 * update that component to reflect based on the contents of the given editedComp.
	 * Returns an instance of the default value component.
	 */
	static EditPropertyComponent* UpdateDefaultValueComp (EditableEnum* editedComp);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDisplayName (const DString& newDisplayName);
	virtual void SetInspectorType (EInspectorType newInspectorType);
	virtual void SetVarOwner (EditorInterface* newVarOwner);
	virtual void SetClassConstraint (GraphAsset* newClassConstraint);
	virtual void SetDefaultValue (const DataBuffer& newDefaultValue);
	virtual void SetComment (const DString& newComment);
	virtual void SetProtected (bool newProtected);
	virtual void SetStatic (bool newStatic);
	virtual void SetShowParamName (bool newShowParamName);
	virtual void SetHasInlineComp (bool newHasInlineComp);
	virtual void SetPassByReference (bool newPassByReference);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetDisplayName () const
	{
		return DisplayName;
	}

	inline EInspectorType GetInspectorType () const
	{
		return InspectorType;
	}

	inline const DString& ReadDisplayName () const
	{
		return DisplayName;
	}

	inline EditorInterface* GetVarOwner () const
	{
		return VarOwner;
	}

	inline GraphAsset* GetClassConstraint () const
	{
		return ClassConstraint;
	}

	inline const DataBuffer& ReadDefaultValue () const
	{
		return DefaultValue;
	}

	inline DString GetComment () const
	{
		return Comment;
	}

	inline const DString& ReadComment () const
	{
		return Comment;
	}

	inline bool IsProtected () const
	{
		return bProtected;
	}

	inline bool IsStatic () const
	{
		return bStatic;
	}

	inline bool IsParamNameVisible () const
	{
		return bShowParamName;
	}

	inline bool HasInlineComp () const
	{
		return bHasInlineComp;
	}

	inline bool IsPassByReference () const
	{
		return bPassByReference;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void RegisterVarToEditor ();

	/**
	 * Initializes the given ItemTemplate to initialize components that can configure a variable type.
	 * If successful, it'll return the EditableEnum instance that can configure the variable type.
	 */
	virtual EditableEnum::SInspectorEnum* InitItemTemplateForVarType (InspectorList& outList);

	/**
	 * Updates the owning struct to add or remove the class type property based on the data found in the given varTypeEnum.
	 * This function will also update that dropdown with all objects currently loaded in the EditorEngineComponent.
	 * If the variable type is an object or class, the struct should have a class type dropdown. Otherwise, it should be removed.
	 */
	virtual void UpdateClassTypeProperty (EditableEnum* varTypeEnum, GraphAsset* initValue);

	/**
	 * Checks the given editedComp to identify if there are any errors with its type.
	 * Displays the error as a caption under the given component.
	 */
	virtual void ProcessParamTypeChecking (EditableEnum* editedComp);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNameEdit (EditPropertyComponent* editedComp);
	virtual void HandleVarTypeEdit (EditPropertyComponent* editedComp);
};

DEFINE_ENUM_FUNCTIONS(GraphVariable::EVarPermissions);
SD_END