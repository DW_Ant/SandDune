/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorInterface.h
  An interface classes may derive from in order to interact with various Editor systems.
  
  This interface makes it easier to expose various properties to a properties inspector.
  This can also be registered to the local EditorEngineComponent so that EditableObjectComponents
  may interact with these objects.
=====================================================================
*/

#pragma once

#include "EditPropertyComponent.h"
#include "Editor.h"

SD_BEGIN
class InspectorList;

class EDITOR_API EditorInterface : public CopiableObjectInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate used to notify the any property components referencing this instance to clear its reference. This is invoked whenever this interface is unregistered. */
	MulticastDelegate<EditorInterface*> OnClearEditorReference;

	/* Invoked if the Editor assigns this object a new ID. This will not be invoked if the previous ID is negative. */
	MulticastDelegate<> OnEditorIdChanged;

	/* Invoked whenever this object changes its name. */
	MulticastDelegate<> OnEditorNameChanged;

private:
	/* The unique number the EditorEngineComponent assigned to this object. */
	Int EditorId = -1;

	/* The string hash used to register this object to the local editor engine component. */
	HashedString RegisteredHash;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	virtual ~EditorInterface ();
	

	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Retrieves the name of this editor object. This is used to display to the end user when modifying this object instance.
	 */
	virtual DString GetEditorObjName () const = 0;

	/**
	 * Unregisters and reregisters this object to the editor engine component.
	 * This function should be called if the GetEditorObjName changes its return value.
	 * This function will also invoke the OnEditorNameChanged. Due to its name change, it'll likely obtain a different EditorID, too.
	 */
	void UpdateEditorObjName ();

	/**
	 * Returns a string containing the object name followed by the editor ID.
	 * EG: "ObjName_EditorId"
	 */
	DString GetCompleteEditorName () const;

	/**
	 * Retrieves the description of this object. This text is intended to be human readable and will be displayed when the developer
	 * hovers the mouse over this object as a tooltip.
	 * If this returns an empty string, then no tooltip will be displayed.
	 */
	virtual DString GetEditorObjTooltip () const;

	/**
	 * Adds subcomponents to the given owning GuiComponent. The components added are based on the data found in PopulateInspectorList.
	 */
	void AddInspectorCompsTo (GuiComponent* owningComp);

	/**
	 * Populates the vector with meta data that instructs the EditorUtils how to edit, save, and load this instance.
	 * The order the instances appear in the vector determines the order the properties appear in the Inspector where element 0 is the property on top.
	 */
	virtual void PopulateInspectorList (InspectorList& outList) = 0;

	/**
	 * This method is called whenever the user pressed the delete button over a EditObjectComponent that created this object.
	 * This function is responsible for destroying this object.
	 */
	virtual void DeleteEditorObject () = 0;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetEditorId (Int newEditorId);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetEditorId () const
	{
		return EditorId;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick utilities that adds common data structs to the given inspector property list.
	 */
	void AddVector2ToInspectorList (const DString& propName, Vector2& vect, InspectorList& outList, const Vector2& defaultVect = Vector2::ZERO_VECTOR) const;
	void AddVector3ToInspectorList (const DString& propName, Vector3& vect, InspectorList& outList, const Vector3& defaultVect = Vector3::ZERO_VECTOR) const;

	/**
	 * Registers this interface to the local editor engine component so this may be assigned a unique ID.
	 * Should be only called on instances that should interface with an editor (eg: BeginObject instead of constructor to prevent CDOs from receiving modifications).
	 */
	void RegisterEditorInterface ();

	/**
	 * Removes this interface from the local editor engine component.
	 * Call this function on destructor or when this object becomes invalidated (eg: Destroyed for Objects).
	 */
	void UnregisterEditorInterface ();
};
SD_END