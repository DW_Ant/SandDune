/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditPropertyComponent.h
  A GuiComponent responsible for enabling the user to edit a DProperty.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EDITOR_API EditPropertyComponent : public GuiComponent
{
	DECLARE_CLASS(EditPropertyComponent)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EInspectorInitValue
	{
		IIV_UseAssociatedVar, //If there is a variable associated with this component, then the EditPropertyComponent will be assigned to it. Otherwise it'll fallback to defaults.
		IIV_UseDefault, //If there isn't an associated variable, then the EditPropertyComponent will be assigned to the default value.
		IIV_UseOverride //If there isn't an associated variable, then the EditPropertyComponent will use the override variable for its initial value.
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/**
	 * List of variables that interfaces with the EditorInterface.
	 * This contains meta data information for the convenience EditorUtils in order to construct the inspector and handle serialization.
	 */
	struct EDITOR_API SInspectorProperty
	{
	public:
		/* The displayed name on the UI. This PropertyName is also what appears in ini files. */
		DString PropertyName;

		/* If not empty, then a tooltip component will be attached to the AssociatedComp. The tooltip will contain this text. */
		DString TooltipText;

		bool bReadOnly;
		SDFunction<void, EditPropertyComponent* /*comp*/> OnApplyEdit;

		/* If not nullptr, then this struct reference will own this inspector property. */
		SInspectorProperty* OwningStruct;

		/* The GuiComponent instance associated with this inspector. No need to set this. This is automatically assigned after the GuiComponent is constructed. */
		EditPropertyComponent* AssociatedComp;

		/* Optional variable that'll provide the associated component the property identifier. This is primarily useful for components that aren't associated with a variable
		(eg: sub properties within an array), and yet there's a need to find a sub property component of a specific ID. */
		Int PropertyId;

		/* Enum that defines how the EditPropertyComponent will be initialized. */
		EInspectorInitValue InitType;

		SInspectorProperty (const DString& inPropertyName, const DString& inTooltipText);

		/**
		 * Utility functions that loads/saves this variable to the specified config and section.
		 * If the variable is not found when loading, it'll assign to the default value.
		 */
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) = 0;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const = 0;

		/**
		 * Utility functions that loads/saves this variable to the specified data buffer.
		 * When loading, this will return false if there's unable to read the variable from it.
		 */
		virtual bool LoadBinary (const DataBuffer& incomingData) = 0;
		virtual void SaveBinary (DataBuffer& outData) const = 0;

		/**
		 * Creates the EditPropertyComponent instance, but does not initialize it.
		 * It's handled this way because templated classes like the EditableArray doesn't have an associated DClass.
		 * This function returns the instance of the initialized component.
		 * The caller should assume ownership of the returned object. Otherwise it would be a memory leak.
		 */
		virtual EditPropertyComponent* CreateBlankEditableComponent () const = 0;

		/**
		 * Initializes the AssociatedComp GuiComponent based on the associated variable.
		 */
		virtual void EditPropComponent () const;
	};

	struct SCaptionColors
	{
		Color TextColor;
		Color Background;

		SCaptionColors ();
		SCaptionColors (const Color& inTextColor, const Color& inBackground);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever this component or any of its subproperties made an edit. */
	SDFunction<void, EditPropertyComponent* /*comp*/> OnApplyEdit;

protected:
	/* If true, then this component will create and initialize the PropertyName and Barrier components. This must be assigned in InitProps. */
	bool bInitPropName;

	/* List of all sub property components that are attached to this component. For example if this component is for a Vector3,
	this list will contain 3 more Editable Property Components for X, Y, and Z floats.
	The order of this list determines the order they appear in the list (sorted from top to down). */
	std::vector<EditPropertyComponent*> SubPropertyComponents;

	/* LabelComponent that displays the property name. */
	DPointer<LabelComponent> PropertyName;

	/* Optional property that allows callers to find sub components of matching IDs. This is primarily useful when searching for a component that is not bound
	to a variable (eg: sub properties of arrays). */
	Int PropertyId;

	/* Font color to use normally. */
	Color NormalFontColor;

	/* Font color to use when this component is read only. */
	Color ReadOnlyFontColor;

	/* Colors to use when displaying an error message. */
	SCaptionColors CaptionErrorColors;

	/* Colors to use when displaying an informative message. */
	SCaptionColors CaptionInfoColors;

	/* TransformComponent that is used for the color component that separates the property name from its field. */
	DPointer<PlanarTransformComponent> BarrierTransform;

	/* Color component that separates the property name from its field. */
	DPointer<ColorRenderComponent> BarrierComponent;

	/* List of buttons that'll appear beside the PropValueField. */
	std::vector<ButtonComponent*> FieldControls;

	/* The component responsible for rendering the highlight whenever the caption text is displayed. */
	ColorRenderComponent* CaptionBackground;

	/* The component responsible for displaying the caption text. */
	LabelComponent* CaptionComponent;

	/* If true, then this property cannot be edited. This flag propagates to all sub properties. */
	bool bReadOnly;

	/* Determines the number of space a typical single line should take. Set from the inspector property. */
	Float LineHeight;

	/* The size multiplier that determines the base size of this property component relative to the line height. For example, typical property components would leave this at 1. However,
	a property component with a caption may double the line height. Property components with a custom component (like a color wheel) may take up more space compared to a single line. */
	Float LineHeightMultiplier;

	/* Determines the amount of vertical space this GuiComponent occupies. If it's negative, then it implies that the LineHeight is dirty and needs to be recomputed.
	Units are in pixels. */
	mutable Float VerticalSpace;

	/* The starting vertical position of the first SubPropertyComponent. Elements may be displaced to add header information (eg: arrays contain a header line that describes its size). */
	Float FirstSubPropPos;

	/* Determines the amount the sub properties attached to this component will be indented (in pixels). */
	Float SubPropIndent;

	/* Becomes true if the contents of this property component has changed. This isn't processed recursively. This is also cleared when GetValueAsText is invoked. */
	mutable bool bPropertyTextChanged;

	/* The cached string constructed from GetValueAsText. Becomes invalidated whenever bPropertyTextChanged is true. */
	mutable DString CachedValueAsText;

	/* Becomes true if the user is currently dragging the barrier. */
	bool bGrabbingBarrier;

	/* Minimum and maximum allowable range for the barrier component's local translation. For example: if set between 0.1 and 0.9,
	then the barrier cannot move within 10% of the left and right border. */
	Range<Float> BarrierRange;

	/* The size of the field control buttons. */
	Vector2 FieldButtonSize;

	/* Becomes true if this object is currently overriding the mouse pointer icon. */
	bool bIsReplacingMouseIcon;

	TickComponent* ResizeTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual bool AcceptsInputEvent (EInputEvent inputEvent, const sf::Event& evnt) const override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

protected:
	virtual void Destroyed () override;
	virtual bool AddComponent_Implementation (EntityComponent* newComponent) override;
	virtual bool RemoveComponent (EntityComponent* target) override;
	virtual void AttachTo (Entity* newOwner) override;
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * An optional method to invoke after all other components are instantiated. This is primarily used for nonessential initializing EditPropertyComponents after variables
	 * are bound and components are attached.
	 *
	 * This function recursively intializes sub properties, too.
	 */
	virtual void PostInitPropertyComponent ();

	/**
	 * Pushes a button to the controls list. This function essentially creates/attaches the button and establishes its size and position.
	 * The base class does not bind its callback, nor does it set its caption or icon.
	 * Returns the button instance that was added to the FieldControl list.
	 */
	virtual ButtonComponent* AddFieldControl ();

	/**
	 * Copies the contents of the associated variable to the given data buffer.
	 * This function will recursively iterate through its sub properties and save them out in the DataBuffer in the same order as the sub properties.
	 * This function will copy from the GuiComponent's value instead of its bound variable since that is the data presented to the user, and EditPropertyComponents are not guaranteed to be bound to a variable.
	 */
	virtual void CopyToBuffer (DataBuffer& copyTo) const;

	/**
	 * Copies the contents of the data buffer to store them in the associated variable.
	 * This function will recursively iterate through its sub properties, and it assumes the contents of the DataBuffer is in the same order as its sub properties.
	 * This function will assign the GuiComponent and sync the variable it's bound to.
	 */
	virtual bool CopyFromBuffer (const DataBuffer& incomingData);

	/**
	 * Binds this GuiComponent to a local internal variable. Use this if there's a need to bind to a nonexisting variable while preserving some functionality (eg: struct headers require
	 * the component to be bound to something).
	 */
	virtual void BindToLocalVariable ();

	/**
	 * Instantiates a default InspectorProperty struct associated with this component instance.
	 * This function will allocate a new InspectorProperty instance. The caller is expected to take ownership over this object.
	 */
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const;

	/**
	 * Returns the component instance the tooltip component should attach to. Most cases would be this component.
	 * Structs and arrays would override this to only have the tooltips attach to the property name components.
	 */
	virtual GuiComponent* GetComponentTooltipsShouldAttachTo ();

	/**
	 * Called whenever the barrier translated, the number of lines, or property height has changed.
	 * This function is responsible for updating the sub component transforms to fit accordingly.
	 */
	virtual void UpdatePropertyHorizontalTransforms ();

	/**
	 * Recursively updates the vertical transforms for this component and its sub EditPropertyComponents.
	 * Returns the vertical size of this component.
	 */
	virtual Float UpdatePropertyVerticalTransforms ();

	/**
	 * Recursively marks this property and its parent properties that its vertical space member variable is obsolete. It needs to be calculated next time it's retrieved.
	 */
	virtual void MarkVerticalSpaceDirty ();

	/**
	 * Sets the bPropertyTextChanged flag to true, forcing this component to reconstruct the string from GetValueAsText. Otherwise it'll return the chached string value.
	 */
	virtual void MarkPropertyTextDirty ();

	/**
	 * Quick utility that adjusts the position of the barrier that separates the property name with its value.
	 * Only normalized values are supported. The parameter value will be bounded by this component's BarrierRange.
	 * Setting it to 0.25 causes the property name to occupy 25% of the line while the value occupies 75%.
	 */
	virtual void SetBarrierPosition (Float barrierPercent);

	/**
	 * Retrieves a DString that summaries the value of this property component. This is generally used as readable text for struct headers.
	 */
	DString GetValueAsText () const;

	/**
	 * If this component resides in another EditPropertyComponent, this function returns the index position where this component is found in the owner's SubPropertyComponents vector.
	 * Returns INDEX_NONE if not found or doesn't reside in an EditPropertyComponent.
	 */
	virtual size_t FindSelfFromOwnerSubProperties () const;

	/**
	 * Iterates through each sub property component until it finds the first component that matches the given property id.
	 */
	virtual EditPropertyComponent* FindSubPropWithMatchingId (Int propId, bool bRecursiveSearch) const;

	/**
	 * Removes any Tick Component that is responsible for automatically updating this component to be equal to the variable it's bound to.
	 */
	virtual void RemoveVarUpdateTickComponent ();

	/**
	 * The user pressed the reset to defaults button. This function should restore the variable to its defaults.
	 */
	virtual void ResetToDefaults ();

	/**
	 * Removes any variable associated with this component.
	 */
	virtual void UnbindVariable ();

	/**
	 * Displays the specified text at the bottom of this component.
	 */
	virtual void ShowCaption (const DString& text, const SCaptionColors& captionColors);
	virtual void ShowCaption (const DString& text, const SD::Color& textColor, const SD::Color& backgroundColor);

	virtual void HideCaption ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPropertyId (Int newPropertyId);
	virtual void SetNormalFontColor (Color newNormalFontColor);
	virtual void SetReadOnlyFontColor (Color newReadOnlyFontColor);
	virtual void SetCaptionErrorColors (const SCaptionColors& newCaptionErrorColors);
	virtual void SetCaptionInfoColors (const SCaptionColors& newCaptionInfoColors);
	virtual void SetReadOnly (bool bNewReadOnly);
	virtual void SetLineHeight (Float newLineHeight);
	virtual void SetSubPropIndent (Float newSubPropIndent);
	virtual void SetBarrierRange (const Range<Float>& newBarrierRange);
	virtual void SetFieldButtonSize (const Vector2& newFieldButtonSize);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<EditPropertyComponent*>& ReadSubPropertyComponents () const
	{
		return SubPropertyComponents;
	}

	inline LabelComponent* GetPropertyName () const
	{
		return PropertyName.Get();
	}

	inline DString GetPropertyNameStr () const
	{
		if (PropertyName != nullptr)
		{
			return PropertyName->GetContent();
		}

		return DString::EmptyString;
	}

	inline Int GetPropertyId () const
	{
		return PropertyId;
	}

	inline SCaptionColors GetCaptionErrorColors () const
	{
		return CaptionErrorColors;
	}

	inline const SCaptionColors& ReadCaptionErrorColors () const
	{
		return CaptionErrorColors;
	}

	inline SCaptionColors GetCaptionInfoColors () const
	{
		return CaptionInfoColors;
	}

	inline const SCaptionColors& ReadCaptionInfoColors () const
	{
		return CaptionInfoColors;
	}

	inline PlanarTransformComponent* GetBarrierTransform () const
	{
		return BarrierTransform.Get();
	}

	inline ColorRenderComponent* GetBarrierComponent () const
	{
		return BarrierComponent.Get();
	}

	inline const std::vector<ButtonComponent*>& ReadFieldControls () const
	{
		return FieldControls;
	}

	inline bool IsReadOnly () const
	{
		return bReadOnly;
	}

	inline Float GetLineHeight () const
	{
		return LineHeight;
	}

	virtual Float GetVerticalSpace () const;

	inline bool HasPropertyTextChanged () const
	{
		return bPropertyTextChanged;
	}

	inline Range<Float> GetBarrierRange () const
	{
		return BarrierRange;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Climbs up the component chain until it finds a component that isn't an EditPropertyComponent. This will return the closest EditPropertyComponent that is not an EditPropertyComponent.
	 */
	virtual EditPropertyComponent* GetRootEditableProperty ();

	/**
	 * Invoked whenever the user grabs the barrier.
	 */
	virtual void GrabBarrier ();

	/**
	 * Invoked whenever the user releases the barrier.
	 */
	virtual void ReleaseBarrier ();

	/**
	 * Calculates the total amount of space needed for this component. If includeSubComp is true, then this also includes the space for each EditPropertyComponent within this component.
	 */
	virtual Float CalcVerticalSpace (bool includeSubComp) const;

	/**
	 * Assumes the value as text is dirty. This function reconstructs and returns the string for GetValueAsText.
	 */
	virtual DString ConstructValueAsText () const;

	/**
	 * Invoked whenever the user has made an edit to this property or any properties attached to this component.
	 */
	virtual void ProcessApplyEdit (EditPropertyComponent* editedComp);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBarrierTransformChanged ();
	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvent);
	virtual void HandleResizeTick (Float deltaSec);
};
SD_END