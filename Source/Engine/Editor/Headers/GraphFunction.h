/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphFunction.h
  An editor object that corresponds to a CLM Subroutine. This class
  will encompass all function types: member functions, static functions,
  and global functions.

  This class defines various meta data the editor uses for context. In
  addition to that, the compiler will use the meta data to identify how
  the CLM Subroutine should be defined.

  This class is the owner of all nodes and local variables
  defined WITHIN the scope of the function.

  If a GraphFunction instance is inheriting from a super function, the
  super function will define the function's signature (such as parameter names).
=====================================================================
*/

#pragma once

#include "Editor.h"
#include "EditorInterface.h"
#include "GraphVariable.h"

SD_BEGIN
class EditablePropCommand;
class EditableStruct;
class GraphAsset;
class GraphEditor;

class EDITOR_API GraphFunction : public Object, public EditorInterface
{
	DECLARE_CLASS(GraphFunction)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/**
	 * Enum that describes a particular attribute in the function. Used for mapping EditableStruct components to a GraphFunction property.
	 */
	enum EFunctionAttribute
	{
		FA_Name = 100000, //Start from a high value to avoid ID conflicts with EVariableAttribute
		FA_Open,
		FA_DeriveFrom,
		FA_InParams,
		FA_OutParams,
		FA_Header,
		FA_Global,
		FA_Static,
		FA_Const,
		FA_Protected,
		FA_Final,
		FA_Comment,
		FA_Unknown //Use this to skip over this component in the EditableStruct.
	};

	/**
	 * Unique flags used to determine if this function should be considered in certain places within the context menu.
	 */
	enum EUniqueFlag
	{
		UF_None,
		UF_Getter, //This function will appear for each variable option to retrieve it.
		UF_Setter, //This function will appear for each variable option to initialize it.
		UF_Edit //This function will appear for each variable to initialize it and retrieve it.
	};

	/**
	 * Enum used to determine if this function should use defaults or override the exec port visibility.
	 * Exec ports are the ports in the graph nodes that determine when this function should execute.
	 */
	enum EExecPortVisibility
	{
		EPV_Default, //Only becomes visible if this function is not a global or const function.
		EPV_AlwaysHidden,
		EPV_AlwaysVisible
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SVarInfo
	{
		/* Identifier used to distinguish a variable. */
		DString VarId;

		/* The name of the variable displayed to the user. */
		DString VarDisplay;

		ScriptVariable::EVarType VarType;

		SVarInfo (const DString& inVarId, const DString& inVarDisplay, ScriptVariable::EVarType inVarType);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various multicast delegates broadcasted whenever a function changes attributes.
	These are static instead of instanced member variables since GraphFunctions may be destroyed and recreated whenever something changes.
	Being that these are static delegates, the FunctionNodes only have to register to these delegates instead of having to worrying about reregistering delegates for each change. */
	static MulticastDelegate<const DString& /*prevFunctionId*/, const DString& /*newFunctionId*/, const DString& /*newName*/> OnFunctionNameChanged;
	static MulticastDelegate<const DString& /*functionId*/, const DString& /*newComment*/> OnCommentChanged;
	static MulticastDelegate<const DString& /*functionId*/, const DString& /*prevParamName*/, const DString& /*newParamName*/> OnParamNameChanged;
	static MulticastDelegate<const DString& /*functionId*/> OnParamChanged;

	/* String regex used to determine what characters are valid for function names. */
	static const DString PERMITTED_CHARS_FOR_NAME;

protected:
	/* A string that uniquely identifies this function. */
	DString FunctionName;

	/* If not empty, then this name is displayed on graph nodes instead of FunctionName. Editors are unable to configure this variable. This is intended for native functions
	for localization purposes. */
	DString DisplayName;

	/* Alternative names of this function. This is used as searchable text in the context menu. For example a function may be named ">", but an alias of that function would be "Greater Than". */
	std::vector<DString> Aliases;

	/* Reference to the GraphAsset that owns this function. */
	GraphAsset* OwningAsset;

	/* If not empty, then this function will be placed in this category in the context menu. Most cases wouldn't need this. This should only be used for GraphAssets with dozens of functions. */
	DString ContextMenuCategory;

	/* List of parameters developers are allowed to pass into this function. */
	std::vector<GraphVariable*> InParams;

	/* List of out parameters this function will return. */
	std::vector<GraphVariable*> OutParams;

	/* Enum used to determine if this function has a distinct place in the context menu compared to other functions. */
	EUniqueFlag UniqueFlag;

	/* If true, then this function will appear under the variable context menu. Use this for comparisons, getters, setters, math operations, etc...
	Slightly different from UniqueFlag. UniqueFlag handles edge cases for the graph context menu. This flag encompasses all variable-related options. */
	bool bVariableRelatedFunction;

	/* Enum that determines when this function's exec ports are visible. */
	EExecPortVisibility ExecPortVisibility;

	/* If true, then this function will not be compiled. This is primarily used for functions to represent as Graph Headers (eg: comments only). */
	bool bHeader;

	/* If true, then this function should be globally available regardless of the ScriptScope. This function will not have access to any static or member variables.
	This cannot reside in a ContentObject context if true. */
	bool bGlobal;

	/* If true, then this function can be invoked without having a reference to a ContentObject. Static functions only have access to static member variables and other static functions. */
	bool bStatic;

	/* If true, then this function can only have read only access to the object reference handling this function. This cannot write to static or member variables. However it can read from variables,
	call global functions, and call other const functions. A const function can also be static, too.
	In short, const functions cannot change the state of this object or other objects.
	If a function is a const function, its associated GraphNodes will not present the in and out ports. This is because the function's out parameters are expected to be directly linked. */
	bool bConst;

	/* If true, then only matching or subclasses of the ContentObject may invoke this function. */
	bool bProtected;

	/* If true, then subclasses cannot override this function. */
	bool bFinal;

	/* Human readable text that describes this function. */
	DString Comment;

	/* If not nullptr, then this function is associated with a natively defined Subroutine (eg: Tick and BeginObject). */
	const Subroutine* NativeAssociation;

	/* Reference to the GraphFunction this function is overriding. If nullptr, then assume it's not inheriting from anything. */
	GraphFunction* SuperFunction;

	/* List of functions directly inheriting from this function. */
	std::vector<GraphFunction*> SubFunctions;

	/* The editor instance used to configure what this function is suppose to do when executed. */
	DPointer<GraphEditor> FunctionDefinition;

private:
	/* If true, then this function's signature has changed. Whenever this asset is saved, it needs to update all assets referencing this function.
	NOTE: attributes evaluated at compile time such as bProtected does not need to save out each asset. Only attributes that affects the nodes' sockets. */
	bool bSignatureDirty;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;

	virtual DString GetEditorObjName () const override;
	virtual void PopulateInspectorList (InspectorList& outList) override;
	virtual void DeleteEditorObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Climbs up the Super function hiararchy until it finds the root function.
	 * The root function is responsible for defining the function's meta data.
	 * Returns self if this function doesn't inherit from anything.
	 */
	virtual const GraphFunction& ReadRootFunction () const;
	virtual GraphFunction& EditRootFunction ();

	/**
	 * Replaces the FunctionDefinition with a new one without any nodes placed in it.
	 * This function will destroy the previous definition if it's still assigned.
	 */
	virtual void CreateBlankDefinition (const Vector2& functionSize);

	inline bool IsNative () const
	{
		return (NativeAssociation != nullptr);
	}

	virtual void ClearDirtySignature ();

	/**
	 * Saves the contents of this function and its parameters to the given config file.
	 */
	virtual void SaveToConfig (ConfigWriter* config, const DString& sectionName) const;

	/**
	 * Loads the contents of this function and its parameters from the given config file.
	 */
	virtual void LoadFromConfig (ConfigWriter* config, const DString& sectionName);

	/**
	 * Writes the contents of this function to the given data buffer.
	 */
	virtual void Serialize (DataBuffer& outBuffer) const;

	/**
	 * Reads from the given buffer and stores its data to this function. Returns true on success.
	 */
	virtual bool Deserialize (const DataBuffer& incomingData);

	/**
	 * Iterates through each node to populate the list of local variables. The results of the variables are sorted alphabetically (sorted by display name).
	 * This function does not include in/out parameters.
	 * If varType is not unknown, then it'll only populate the vector with variables that matches this type.
	 */
	virtual void FindLocalVariables (ScriptVariable::EVarType varType, std::vector<SVarInfo>& outLocalVars) const;

	/**
	 * Gathers a list of all member and static variables this function has access to. The names are sorted in the same order as the variables defined in the owning asset.
	 * This function also climbs up the inherited assets to obtain those variables, too. The results of those variables are appended at the end.
	 * @param permissions Determines what kind of variables this function should return. If only read access is passed in, it'll will skip over variables this function does not have write access to.
	 * @param varType If not unknown, it'll only populate variables matching this type.
	 */
	virtual void GetAllMemberVariables (GraphVariable::EVarPermissions permissions, bool staticOnly, ScriptVariable::EVarType varType, std::vector<GraphVariable*>& outMemberVariables) const;

	/**
	 * Returns the DisplayName if it's not empty. Otherwise, it'll return the function's actual name.
	 */
	virtual DString GetPresentedName () const;

	/**
	 * Returns a string representation that uniquely identifies this function compared to others.
	 * eg: returns "Function'ClassName.FunctionName'
	 */
	virtual DString GetIdentifyingName () const;

	/**
	 * Parses the given function ID to extract the class name and function name from it.
	 * Returns true if the ID is in the correct format.
	 */
	static bool ParseFunctionId (const DString& functionId, DString& outClassName, DString& outFuncName);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetFunctionName (const DString& newFunctionName);
	virtual void SetDisplayName (const DString& newDisplayName);
	virtual void SetOwningAsset (GraphAsset* newOwningAsset);
	virtual void SetContextMenuCategory (const DString& newContextMenuCategory);
	virtual void SetInParams (const std::vector<GraphVariable*>& newInParams);
	virtual void SetOutParams (const std::vector<GraphVariable*>& newOutParams);
	virtual void SetUniqueFlag (EUniqueFlag newUniqueFlag);
	virtual void SetVariableRelatedFunction (bool newVariableRelatedFunction);
	virtual void SetExecPortVisibility (EExecPortVisibility newExecPortVisibility);
	virtual void SetHeader (bool newHeader);
	virtual void SetGlobal (bool newGlobal);
	virtual void SetStatic (bool newStatic);
	virtual void SetConst (bool newConst);
	virtual void SetProtected (bool newProtected);
	virtual void SetFinal (bool newFinal);
	virtual void SetComment (const DString& newComment);
	virtual void SetNativeAssociation (const Subroutine* newNativeAssociation);
	virtual void SetSuperFunction (GraphFunction* newSuperFunction);

	//Relinquishes ownership from previous FunctionDefinition. Takes ownership over new definition.
	virtual void SetFunctionDefinition (GraphEditor* newFunctionDefinition);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetFunctionName () const
	{
		return ReadRootFunction().FunctionName;
	}

	inline const DString& ReadFunctionName () const
	{
		return ReadRootFunction().FunctionName;
	}

	inline DString GetDisplayName () const
	{
		return ReadRootFunction().DisplayName;
	}

	inline const DString& ReadDisplayName () const
	{
		return ReadRootFunction().DisplayName;
	}

	inline const std::vector<DString>& ReadAliases () const
	{
		return ReadRootFunction().Aliases;
	}

	inline std::vector<DString>& EditAliases ()
	{
		return EditRootFunction().Aliases;
	}

	inline GraphAsset* GetOwningAsset () const
	{
		return OwningAsset;
	}

	inline DString GetContextMenuCategory () const
	{
		return ReadRootFunction().ContextMenuCategory;
	}

	inline const DString& ReadContextMenuCategory () const
	{
		return ReadRootFunction().ContextMenuCategory;
	}

	inline std::vector<GraphVariable*> GetInParams () const
	{
		return ReadRootFunction().InParams;
	}

	inline const std::vector<GraphVariable*>& ReadInParams () const
	{
		return ReadRootFunction().InParams;
	}

	inline std::vector<GraphVariable*>& EditInParams ()
	{
		return EditRootFunction().InParams;
	}

	inline std::vector<GraphVariable*> GetOutParams () const
	{
		return ReadRootFunction().OutParams;
	}

	inline const std::vector<GraphVariable*>& ReadOutParams () const
	{
		return ReadRootFunction().OutParams;
	}

	inline std::vector<GraphVariable*>& EditOutParams ()
	{
		return EditRootFunction().OutParams;
	}

	inline EUniqueFlag GetUniqueFlag () const
	{
		return ReadRootFunction().UniqueFlag;
	}

	inline bool IsVariableRelatedFunction () const
	{
		return ReadRootFunction().bVariableRelatedFunction;
	}

	inline EExecPortVisibility GetExecPortVisibility () const
	{
		return ReadRootFunction().ExecPortVisibility;
	}
	
	inline bool IsHeader () const
	{
		return ReadRootFunction().bHeader;
	}

	inline bool IsGlobal () const
	{
		return ReadRootFunction().bGlobal;
	}

	inline bool IsStatic () const
	{
		return ReadRootFunction().bStatic;
	}

	inline bool IsConst () const
	{
		return ReadRootFunction().bConst;
	}

	inline bool IsProtected () const
	{
		return ReadRootFunction().bProtected;
	}

	inline bool IsFinal () const
	{
		return ReadRootFunction().bFinal;
	}

	inline DString GetComment () const
	{
		return ReadRootFunction().Comment;
	}

	inline const DString& ReadComment () const
	{
		return ReadRootFunction().Comment;
	}

	inline const Subroutine* GetNativeAssociation () const
	{
		return NativeAssociation;
	}

	inline GraphFunction* GetSuperFunction () const
	{
		return SuperFunction;
	}

	inline GraphEditor* GetFunctionDefinition () const
	{
		return FunctionDefinition.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Recursively iterates through sub functions to update their signatures.
	 * This will not save it out to their files, but it would mark the signature dirty so when it does save,
	 * it will update the files.
	 */
	virtual void UpdateSignature ();

	/**
	 * Climbs up the class chain to find the function matching the given name. If one is found, it'll return a reference to that function.
	 */
	virtual GraphFunction* FindSuperFunction (const DString& functionName) const;

	/**
	 * Creates the essential nodes that make up minimum function definition. For example, it'll create the starter exec node.
	 */
	virtual void InitFunctionDefinition ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleFunctionNameEdit (EditPropertyComponent* editedComp);
	virtual void HandleFunctionCommentEdit (EditPropertyComponent* editedComp);
	virtual void HandleOpenFunction (ButtonComponent* button, EditablePropCommand* propCmd);
	virtual void HandleFunctionGlobalToggle (EditPropertyComponent* editedComp);
	virtual void HandleParamDeleted (EditorInterface* obj);
};
SD_END