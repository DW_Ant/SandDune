/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorWorkerEngineComponent.h
  EditorEngineComponent that resides in the worker thread.

  This component doesn't do much other than maintain a collection of data gathered
  from other Entities that may be repurposed for other Entities within the same thread.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
#ifdef WITH_MULTI_THREAD
class AssetCatalogue;
class ReferenceUpdater;

class EDITOR_API EditorWorkerEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(EditorWorkerEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The latest result from the AssetCatalogue's work. */
	std::vector<FileAttributes> LatestCatalogue;

	/* Object responsible for actually parsing through files for the catalogue. */
	DPointer<AssetCatalogue> WorkerCatalogue;

	/* Object responsible for replacing references in files. */
	DPointer<ReferenceUpdater> RefUpdater;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditorWorkerEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetLatestCatalogue (const std::vector<FileAttributes>& newLatestCatalogue);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<FileAttributes>& ReadLatestCatalogue () const
	{
		return LatestCatalogue;
	}

	inline AssetCatalogue* GetWorkerCatalogue () const
	{
		return WorkerCatalogue.Get();
	}

	inline ReferenceUpdater* GetRefUpdater () const
	{
		return RefUpdater.Get();
	}
};
#endif
SD_END