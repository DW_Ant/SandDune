/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphContextMenu.h

  A DynamicListMenu that comes with a search bar. This would populate a
  ListBoxComponent containing the search results. Clicking on these elements
  would also execute the context commands.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class FunctionNode;
class GraphAsset;
class GraphEditor;
class GraphFunction;

class EDITOR_API GraphContextMenu : public DynamicListMenu
{
	DECLARE_CLASS(GraphContextMenu)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EPortDirection
	{
		PD_Input,
		PD_Output,
		PD_Any
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SCmdData
	{
		/* If it's a variable access node, this is the ID of the variable the function will be accessing. */
		DString VarId;

		/* If not nullptr, then the cmd data is associated with this function. */
		GraphFunction* Func;

		/* The searchable text. This contains the displayed name without spaces/underscores. And it's all in upper case. Populate when first time used. */
		std::vector<DString> SearchableText;

		SCmdData ();
		SCmdData (GraphFunction* inFunc);
		SCmdData (const DString& inVarId, GraphFunction* inFunc);
	};

	struct SFunctionQuery
	{
		bool bStaticOnly;
		bool bPublicOnly;
		bool bConstOnly;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked as soon as it's destroyed and when a command is executed (at the beginning of the fade out). If the callback returns true, it'll clear the delegate assignent. */
	SDFunction<bool> OnGraphContextClosing;

protected:
	static const DString SECTION_NAME;

	/* List of AssetNames to check when looking for functions related to variables. The order of this list also determines the order of the function that appear in the context menu. */
	std::vector<HashedString> VarRelatedClasses;

	/* List of unique variable names that appear for each variable listing for assets. */
	static std::vector<GraphFunction*> AllGetFunctions;
	static std::vector<GraphFunction*> AllSetFunctions;
	static std::vector<GraphFunction*> AllEditFunctions;

	/* The location where to place the node (where the user clicked in the editor). */
	Vector2 PlacementLocation;

	GraphEditor* OwningEditor;
	TextFieldComponent* SearchField;

	/* List box that appears whenever the user enters something in the search field. When search field is empty, this is concealed, and the parent DynamicListMenu becomes visible. */
	ScrollbarComponent* SearchResultScroll;
	ListBoxComponent* SearchResults;

private:
	DString LastSearchText;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual void InitializeList (ContextMenuComponent* contextComp, MousePointer* mouse, const std::vector<SInitMenuOption>& inOptions) override;

protected:
	virtual void ConstructUI () override;
	virtual void BeginFadeOut () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes the context menu to add options related to the variable port the editor is currently dragging (if it is dragging a variable).
	 */
	virtual void InitGraphContextFromVar (ContextMenuComponent* contextComp, MousePointer* mouse, GraphEditor* inOwningEditor);

	/**
	 * Initializes the context menu to add default options based on the function being edited in the given editor instance.
	 */
	virtual void InitGraphContext (ContextMenuComponent* contextComp, MousePointer* mouse, GraphEditor* inOwningEditor);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void PopulateVarRelatedClasses ();
	virtual void PopulateVarAccessFunctions ();

	/**
	 * Adds escape characters in front of every submenu character '>' unless it's the last character in the string.
	 */
	virtual DString GetEscapedMenuText (const DString& originalText) const;

	/**
	 * Adds all local variable options to the options vector.
	 * This will provide setters and getters for each local variable.
	 * @param restrictedVarType If not unknown, then this function will it'll only populate local variables matching this type.
	 * @param portDir Determines what kind of functions are available for each local variable. For example, setter functions will not appear if the port direction is output (setters expect input).
	 */
	virtual void AddLocalVariables (TextTranslator* translator, ScriptVariable::EVarType restrictedVarType, EPortDirection portDir, std::vector<DynamicListMenu::SInitMenuOption>& outOptions);
	
	/**
	 * Iterates through the given asset to add context options for each of its visible functions.
	 */
	virtual void AddFunctionOptions (TextTranslator* translator, GraphAsset* asset, const SFunctionQuery& filter, std::vector<DynamicListMenu::SInitMenuOption>& outOptions);

	/**
	 * Iterates through all global functions and adds them to the outOptions list.
	 */
	virtual void AddGlobalFunctions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions);

	virtual void AddEditorFunctions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions);

	/**
	 * Iterates through all options. Any option referencing the targetAsset that also have duplicate function names will slightly change the presented option to disambiguate it from the other duplicate names.
	 */
	virtual void DisambiguateCmdOptions (TextTranslator* translator, std::vector<DynamicListMenu::SInitMenuOption>& outOptions, size_t startSearchFrom, GraphAsset* targetAsset);

	/**
	 * Looks at the given function to figure out what text to place at the end of it to disambiguate it from other functions with identical name.
	 * This function simply looks at the function parameters to distinguish it from its duplicate.
	 */
	virtual DString GetDisambiguationSuffix (TextTranslator* translator, GraphFunction* func) const;

	/**
	 * Searches through the given list of functions and returns the first reference that meets all constraints.
	 * @param portDir determines what port types are required for the function to have. If portDir is input, then the resulting function is expected to have input parameters.
	 */
	virtual GraphFunction* FindFunction (ScriptVariable::EVarType restrictedVarType, EPortDirection portDir, const std::vector<GraphFunction*>& searchIn) const;

	/**
	 * Returns true if the given search text should locate the given command data.
	 */
	virtual bool IsCmdRelevant (GuiDataElement<SCmdData>* cmdData, const DString& searchText) const;

	/**
	 * Creates a function node that initializes a blank local variable.
	 * If a variable name is not empty, it'll initialize the node to reference that variable.
	 */
	virtual void CreateLocalVariableNode (GraphFunction* func, const DString& varName);

	/**
	 * Creates a function node. If the user is draggined a port, it'll attempt to find and establish a connection between that port and a port on this new node.
	 */
	virtual FunctionNode* CreateFunctionNode (GraphFunction* func);

	virtual void CreateReturnNode ();

	/**
	 * Updates the function node's 'InvokeOn' components to accept the variable owner.
	 * If the editor is currently dragging a port, this function will evaluate that connection to identify if it should connect to that port.
	 */
	virtual void InitVarAccessNode (FunctionNode* funcNode, const DString& varId);

	virtual void InitVarAccessNodePorts (FunctionNode* funcNode, const GraphVariable& var);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleSearchEdit (TextFieldComponent* uiComp);
	virtual void HandleSearchReturn (TextFieldComponent* uiComponent);
	virtual void HandleItemSelected (Int selectedItemIdx);
	virtual void HandleCreateLocalVar (BaseGuiDataElement* data);
	virtual void HandleCreateFunctionNode (BaseGuiDataElement* data);
	virtual void HandleCreateVarAccessNode (BaseGuiDataElement* data);
	virtual void HandleCreateReturnNode (BaseGuiDataElement* data);
};
SD_END