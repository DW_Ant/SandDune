/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableEnum.h
  An EditPropertyComponent that allows the developer to edit an enumerator.
=====================================================================
*/

#pragma once

#include "DynamicEnum.h"
#include "EditPropertyComponent.h"

SD_BEGIN
class EDITOR_API EditableEnum : public EditPropertyComponent
{
	DECLARE_CLASS(EditableEnum)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorEnum : public SInspectorProperty
	{
	public:
		/* The DynamicEnum used to automatically populate the DropdownComponent and optionally associate this component with an enum value.
		This Inspector takes ownership over this DynamicEnum instance, and will be deleted automatically when no longer in use. */
		DynamicEnum* EnumMeta;

		Int DefaultIdx;

		/* Determines the initial idx. Only applicable if InitType is IIV_UseOverride */
		Int InitOverride;

		/* If true, then this enumator will enable the DropdownComponent's search bar. */
		bool bEnableSearchBar;

		/* If EnumMeta is not used, this callback used to populate the DropdownComponent. */
		SDFunction<void, DropdownComponent*> OnPopulateDropdown;

		SInspectorEnum (const DString& inPropertyName, const DString& inTooltipText, DynamicEnum* inEnumMeta, Int inDefaultIdx = INDEX_NONE);
		SInspectorEnum (const DString& inPropertyName, const DString& inTooltipText, const SDFunction<void, DropdownComponent*>& inOnPopulateDropdown, Int inDefaultIdx = INDEX_NONE);
		virtual ~SInspectorEnum ();
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines how many lines the DropdownComponent will cover when it's expanded. This does not include the extra line showing the selected line. */
	Float NumLinesExpandMenu;

	DPointer<DropdownComponent> Dropdown;

	/* Gray out box that indicates that this Dropdown component is read only. */
	FrameComponent* ReadOnlyBlackout;

	/* Button component responsible for resetting to defaults. */
	DPointer<ButtonComponent> ResetDefaultsButton;

	/* Enum that binds to an actual enum value. This is a reference to a base class to avoid templatizing this class.
	This is an optional variable. If it's not assigned, then this EditableEnum will not be bound to a variable. */
	DynamicEnum* EditedEnum;

	/* Index value of the dropdown selection that indicates the default value. If INDEX_NONE, then there are no defaults. */
	Int DefaultIdx;

	/* Component responsible for updating the dropdown whenever EditedEnum's value has changed. */
	TickComponent* UpdateEnumTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual void CopyToBuffer (DataBuffer& copyTo) const override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual void SetLineHeight (Float newLineHeight) override;

protected:
	virtual void InitializeComponents () override;
	virtual DString ConstructValueAsText () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds a field control button that will reset this field back to defaults.
	 */
	virtual void AddResetToDefaultsControl ();

	/**
	 * Sets the dropdown component to select the item at the given index.
	 */
	virtual void SetDropdownSelectIdx (Int newIdx, bool bInvokeCallback);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetNumLinesExpandMenu (Float newNumLinesExpandMenu);
	virtual void SetReadOnlyColor (Color newReadOnlyColor);
	virtual void SetDefaultIndex (Int newDefaultIndex);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetNumLinesExpandMenu () const
	{
		return NumLinesExpandMenu;
	}

	inline DropdownComponent* GetDropdown () const
	{
		return Dropdown.Get();
	}

	virtual Color GetReadOnlyColor () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the state of this component and its bound variable based on the dropdown's selection.
	 * This function assumes that the dropdown has already selected the new item.
	 */
	virtual void ProcessDropdownItemChange (Int newSelectedIdx);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleOptionSelected (DropdownComponent* dropdown, Int selectedIdx);
	virtual void HandleResetToDefaultsReleased (ButtonComponent* uiComp);
	virtual void HandleUpdateEnumTick (Float deltaSec);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Replaces the EditableEnum with the new enum. The template type DynEnum expects a DynamicEnum type.
	 * If the enum type is EMyEnum, the DynamicEnum class name is Dynamic_Enum_EMyEnum.
	 */
	template <class DynEnum, class Enum>
	void BindVariable (Enum* dynamicEnum)
	{
		if (EditedEnum != nullptr)
		{
			delete EditedEnum;
			EditedEnum = nullptr;
		}

		if (Dropdown != nullptr)
		{
			Dropdown->ClearList();
		}

		if (dynamicEnum != nullptr)
		{
			EditedEnum = new DynEnum(dynamicEnum);
			if (Dropdown != nullptr)
			{
				EditedEnum->InitDropdown(Dropdown.Get());
			}
		}
	}
};
SD_END