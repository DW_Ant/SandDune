/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariablePort.h
  A NodePort that is intended to link to a variable.

  A common use case for this would be an in and out parameter for
  a function node.
=====================================================================
*/

#pragma once

#include "NodePort.h"

SD_BEGIN
class GraphAsset;

class EDITOR_API VariablePort : public NodePort
{
	DECLARE_CLASS(VariablePort)
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Map that associates a color to a variable type. */
	static const std::map<ScriptVariable::EVarType, Color> COLOR_MAP;

protected:
	/* The variable type this port is used for. */
	ScriptVariable::EVarType VarType;

	/* If the variable type is a class or object, this class restricts what kind of connections are permitted to connect to this port (if not nullptr).
	This only applies to input ports. Output ports can connect to anything (handles case where the subclass is connecting to an input port that expects parent). */
	GraphAsset* ClassConstraint;

	/* Tooltip used to display the ClassConstraint. */
	DPointer<TooltipComponent> ClassTooltip;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanAcceptConnectionsFrom (const NodePort* otherPort, bool bCheckOtherPort) const override;
	virtual void EstablishConnectionTo (NodePort* otherPort) override;
	virtual bool CanBeGroupedWith (NodePort* otherPort) const override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Reconnects this port to the other port. If necessary, it'll create a conversion node between the two.
	 * Typically call this function if one of the connected ports have changed variable types.
	 * If the ports are no longer compatible, it'll simply sever the connection instead.
	 */
	virtual void ReconnectToVarPort (NodePort* otherPort);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetVarType (ScriptVariable::EVarType newVarType);
	virtual void SetClassConstraint (GraphAsset* newClassConstraint);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ScriptVariable::EVarType GetVarType () const
	{
		return VarType;
	}

	inline GraphAsset* GetClassConstraint () const
	{
		return ClassConstraint;
	}

	inline TooltipComponent* GetClassTooltip () const
	{
		return ClassTooltip.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the given variable type can be converted to the other variable type.
	 */
	static bool HasConversionFor (ScriptVariable::EVarType originalVarType, ScriptVariable::EVarType desiredType);

	/**
	 * Creates a GraphNode between the two variable ports where the node is responsible for converting the variable type.
	 * The outputPort is the conversion node's input variable. The conversion node's out variable connects to the given inputPort.
	 */
	virtual bool CreateConversionNode (VariablePort* outputPort, VariablePort* inputPort);

	/**
	 * Returns a string associated with the variable type. This string is used to find the conversion function name.
	 */
	virtual DString GetTypeAsString (ScriptVariable::EVarType varType) const;
};
SD_END