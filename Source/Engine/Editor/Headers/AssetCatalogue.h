/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetCatalogue.h
  An Object responsible for iterating through files in the DevAsset directory in
  order to populate the editor's asset catalogue.

  This process is time consuming. It's recommended to run this on a separate thread in order
  to prevent the locking the main thread as this runs.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EDITOR_API AssetCatalogue : public Entity
{
	DECLARE_CLASS(AssetCatalogue)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked whenever this Entity completed a batch of files or when it finished populating the catalogue. */
	SDFunction<void, bool /*isCompleted*/> OnCatalogueUpdate;

protected:
	/* The maximum number of files that this catalogue should run before it sends a progress update. If not positive, then this will not send any
	progress updates until the very end (for cases where the AssetCatalogue is used synchronously). */
	Int MaxUpdateInterval;

	/* The total number of files that was processed. */
	Int NumFilesProcessed;

	/* List of all files the iterator found that still need to be processed. */
	std::vector<FileAttributes> RemainingFiles;

	/* Only files of these types will be populated in the catalogue. */
	std::vector<DString> AssetTypes;

	/* List of relevant assets that was found. */
	std::vector<FileAttributes> FoundAssets;

	TickComponent* Tick;

private:
	/* Index to the RemainingFiles vector that points to the next attribute to be processed. */
	size_t NextFileIdx;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Clears the FoundAssets vector and launches a new process to iterate through the given directory to repopulate it.
	 */
	virtual void PopulateCatalogue (const Directory& searchIn, const DString& fileExtension, const std::vector<DString>& inAssetTypes);

	/**
	 * Cancels the process of populating the catalogue. This will not invoke the OnCatalogueUpdate delegate.
	 */
	virtual void AbortProcess ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMaxUpdateInterval (Int newMaxUpdateInterval);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetMaxUpdateInterval () const
	{
		return MaxUpdateInterval;
	}

	inline Int GetNumFilesProcessed () const
	{
		return NumFilesProcessed;
	}

	inline const std::vector<FileAttributes>& ReadRemainingFiles () const
	{
		return RemainingFiles;
	}

	inline const std::vector<DString>& ReadAssetTypes () const
	{
		return AssetTypes;
	}

	inline const std::vector<FileAttributes>& ReadFoundAssets () const
	{
		return FoundAssets;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Attempts to open the specified file. If successful, it'll read through its contents to figure out if the file matches the asset type.
	 * Returns true on success even if the file does not match the asset type.
	 */
	virtual bool ProcessFile (const FileAttributes& fileAttrib);

	/**
	 * Function invoked as soon as the AssetCatalogue finished running through the file list.
	 * This function will conclude the procedure.
	 */
	virtual void ProcessFinishedCatalogue ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
SD_END