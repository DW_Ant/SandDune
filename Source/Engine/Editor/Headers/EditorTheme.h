/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorTheme.h
  A theme that defines the templates the Editor GuiComponents could use to for styling.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EditPropertyComponent;

class EDITOR_API EditorTheme : public GuiTheme
{
	DECLARE_CLASS(EditorTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString EDITOR_STYLE_NAME;

	/* Various textures to use for various field control buttons. */
	const Texture* FieldControlAdd;
	const Texture* FieldControlRemove;
	const Texture* FieldControlReset;
	const Texture* FieldControlMoveUp;
	const Texture* FieldControlMoveDown;
	const Texture* FieldControlCreate;
	const Texture* FieldControlCopy;
	const Texture* FieldControlDelete;
	const Texture* FieldControlTarget;

	/* Texture to use to indicate where the developer can drag to reorder editable array elements. */
	const Texture* ArrayReorderRegionTexture;

	/* Texture to use for Graph objects. */
	const Texture* InvokeOnBackground;
	const Texture* InvokeOnEdge; //Sprite used at the end of the InvokeOn component, typically 'slopes' down towards the node's header component.
	const Texture* NodeBackground;
	const Texture* PortTexture;
	const Texture* ExecPortTexture;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void InitializeTheme () override;
	virtual void InitializeStyles () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeEditorStyle ();

	/**
	 * Creates an edit property component template, and initializes its common properties to the editor theme.
	 * The created template is registered to the given style.
	 */
	virtual EditPropertyComponent* InitEditPropTemplate (const DClass* editPropClass, SStyleInfo& outStyle);


	/*
	=====================
	  Template
	=====================
	*/

protected:
	template <class T>
	T* InitEditPropTemplateCast (const DClass* editPropClass, SStyleInfo& outStyle)
	{
		if (T* result = dynamic_cast<T*>(InitEditPropTemplate(editPropClass, OUT outStyle)))
		{
			return result;
		}

		return nullptr;
	}
};
SD_END