/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DynamicEnum.h
  Adds runtime information to an enumerator.

  The DynamicEnum comes with two classes. The base DynamicEnum class is an abstract
  class that declares various functions the EditableEnum component interface with.

  The second classes are defined via DEFINE_DYNAMIC_ENUM macros. These generated
  classes will define each function from the DynamicEnum. These classes also
  map enumerator values to strings.
=====================================================================
*/

#pragma once

#include "Editor.h"

/**
 * Defines a class wrapper to a variable that maps an enum type to string instances.
 * To use this, call one of the DEFINE_DYNAMIC_ENUM_XVAL macros. The order of the enums determine the order how they appear in the dropdown component.
 */
#define DEFINE_DYNAMIC_ENUM(enumType, defaultEnumValue, mapInit) \
class Dynamic_Enum_##enumType : public SD::DynamicEnum \
{ \
public: \
	/* Optional reference that associates this DynamicEnum instance with the actual Enum variable. */ \
	enumType##* AssociatedEnum; \
	\
	Dynamic_Enum_##enumType (##enumType##* inAssociatedEnum) : SD::DynamicEnum(), \
		AssociatedEnum(inAssociatedEnum) \
	{ \
		/* Noop */ \
	} \
	\
	virtual void InitDropdown (SD::DropdownComponent* dropdown) override \
	{ \
		if (dropdown == nullptr) \
		{ \
			return; \
		} \
		\
		SD::ListBoxComponent* expandMenu = dropdown->GetExpandMenuList(); \
		if (expandMenu == nullptr) \
		{ \
			return; \
		} \
		\
		std::vector<SD::GuiDataElement<##enumType##>> items; \
		items.reserve(ReadMap().size()); \
		for (const std::pair<##enumType##, SD::DString>& item : ReadMap()) \
		{ \
			items.emplace_back(item.first, item.second); \
		} \
		\
		expandMenu->SetList(items); \
	} \
	\
	virtual bool IsBounded () const override \
	{ \
		return (AssociatedEnum != nullptr); \
	} \
	\
	virtual [[nodiscard]] DynamicEnum* Duplicate () const override \
	{ \
		return new Dynamic_Enum_##enumType##(AssociatedEnum); \
	} \
	\
	virtual void AssignFromString (const SD::DString& enumText) override \
	{ \
		if (AssociatedEnum == nullptr) \
		{ \
			return; \
		} \
		\
		for (const std::pair<##enumType##, SD::DString>& item : ReadMap()) \
		{ \
			if (item.second.Compare(enumText, SD::DString::CC_CaseSensitive) == 0) \
			{ \
				*AssociatedEnum = item.first; \
				break; \
			} \
		} \
	} \
	\
	virtual SD::DString ToString () const override \
	{ \
		if (AssociatedEnum == nullptr) \
		{ \
			return SD::DString::EmptyString; \
		} \
		\
		if (ReadMap().contains(*AssociatedEnum)) \
		{ \
			return ReadMap().at(*AssociatedEnum); \
		} \
		return SD::DString::EmptyString; \
	} \
	\
	virtual void AssignFromIndex (SD::Int enumIdx) override \
	{ \
		if (AssociatedEnum == nullptr || enumIdx == INDEX_NONE) \
		{ \
			return; \
		} \
		\
		SD::Int i = 0; \
		for (const std::pair<##enumType##, SD::DString>& item : ReadMap()) \
		{ \
			if (i == enumIdx) \
			{ \
				*AssociatedEnum = item.first; \
				return; \
			} \
			\
			++i; \
		} \
	} \
	\
	virtual SD::Int ToIndex () const override \
	{ \
		if (AssociatedEnum == nullptr) \
		{ \
			return INDEX_NONE; \
		} \
		\
		SD::Int idx = 0; \
		for (const std::pair<##enumType##, SD::DString>& item : ReadMap()) \
		{ \
			if (*AssociatedEnum == item.first) \
			{ \
				return idx; \
			} \
			\
			++idx; \
		} \
		\
		return INDEX_NONE; \
	} \
	\
	/* The map is defined within a function since a static map member variable cannot be initialized in a header file. Workaround this limitation using a static local variable. */ \
	static const std::map<##enumType##, SD::DString>& ReadMap () \
	{ \
		static std::map<##enumType##, SD::DString> map(##mapInit##); \
		return map; \
	} \
}; \
\
/* Quick utility that converts the given enum variable to string. */ \
static SD::DString EnumToString (##enumType myEnum) \
{ \
	const std::map<##enumType##, SD::DString>& map = Dynamic_Enum_##enumType##::ReadMap(); \
	if (map.contains(myEnum)) \
	{ \
		return map.at(myEnum); \
	} \
	\
	return SD::DString::EmptyString; \
} \
\
/* Quick utility that converts attempts to find a enum that matches the string (case sensitive). If none is found, it'll assign to the default enum value. Returns true if the string is found in the enum map. */ \
static bool StringToEnum (const SD::DString& str, enumType##& outEnum) \
{ \
	if (str.IsEmpty()) \
	{ \
		outEnum = defaultEnumValue; \
		return false; \
	} \
	\
	const std::map<##enumType##, SD::DString>& map = Dynamic_Enum_##enumType##::ReadMap(); \
	for (const std::pair<##enumType##, SD::DString>& item : map) \
	{ \
		if (item.second.Compare(str, SD::DString::CC_CaseSensitive) == 0) \
		{ \
			outEnum = item.first; \
			return true; \
		} \
	} \
	\
	SD::EditorLog.Log(SD::LogCategory::LL_Warning, TXT("The string \"%s\" is not associated with any " #enumType " value."), str); \
	outEnum = defaultEnumValue; \
	return false; \
}

/* Placing commas within a macro since commas would be treated as a separate macro parameter. Define this macro to workaround that. */
#define EDITOR_COMMA_MACRO ,

#define DEFINE_DYNAMIC_ENUM_1VAL(EnumType, val1) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )}} )

#define DEFINE_DYNAMIC_ENUM_2VAL(EnumType, val1, val2) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )}} )

#define DEFINE_DYNAMIC_ENUM_3VAL(EnumType, val1, val2, val3) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )}} )

#define DEFINE_DYNAMIC_ENUM_4VAL(EnumType, val1, val2, val3, val4) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )}} )

#define DEFINE_DYNAMIC_ENUM_5VAL(EnumType, val1, val2, val3, val4, val5) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )}} )

#define DEFINE_DYNAMIC_ENUM_6VAL(EnumType, val1, val2, val3, val4, val5, val6) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )} EDITOR_COMMA_MACRO {val6 EDITOR_COMMA_MACRO TXT( #val6 )}} )

#define DEFINE_DYNAMIC_ENUM_7VAL(EnumType, val1, val2, val3, val4, val5, val6, val7) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )} EDITOR_COMMA_MACRO {val6 EDITOR_COMMA_MACRO TXT( #val6 )} EDITOR_COMMA_MACRO {val7 EDITOR_COMMA_MACRO TXT( #val7 )}} )

#define DEFINE_DYNAMIC_ENUM_8VAL(EnumType, val1, val2, val3, val4, val5, val6, val7, val8) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )} EDITOR_COMMA_MACRO {val6 EDITOR_COMMA_MACRO TXT( #val6 )} EDITOR_COMMA_MACRO {val7 EDITOR_COMMA_MACRO TXT( #val7 )} EDITOR_COMMA_MACRO {val8 EDITOR_COMMA_MACRO TXT( #val8 )}} )

#define DEFINE_DYNAMIC_ENUM_9VAL(EnumType, val1, val2, val3, val4, val5, val6, val7, val8, val9) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )} EDITOR_COMMA_MACRO {val6 EDITOR_COMMA_MACRO TXT( #val6 )} EDITOR_COMMA_MACRO {val7 EDITOR_COMMA_MACRO TXT( #val7 )} EDITOR_COMMA_MACRO {val8 EDITOR_COMMA_MACRO TXT( #val8 )} EDITOR_COMMA_MACRO {val9 EDITOR_COMMA_MACRO TXT( #val9 )}} )

#define DEFINE_DYNAMIC_ENUM_10VAL(EnumType, val1, val2, val3, val4, val5, val6, val7, val8, val9, val10) \
DEFINE_DYNAMIC_ENUM(EnumType, val1, {{val1 EDITOR_COMMA_MACRO TXT( #val1 )} EDITOR_COMMA_MACRO {val2 EDITOR_COMMA_MACRO TXT( #val2 )} EDITOR_COMMA_MACRO {val3 EDITOR_COMMA_MACRO TXT( #val3 )} EDITOR_COMMA_MACRO {val4 EDITOR_COMMA_MACRO TXT( #val4 )} EDITOR_COMMA_MACRO {val5 EDITOR_COMMA_MACRO TXT( #val5 )} EDITOR_COMMA_MACRO {val6 EDITOR_COMMA_MACRO TXT( #val6 )} EDITOR_COMMA_MACRO {val7 EDITOR_COMMA_MACRO TXT( #val7 )} EDITOR_COMMA_MACRO {val8 EDITOR_COMMA_MACRO TXT( #val8 )} EDITOR_COMMA_MACRO {val9 EDITOR_COMMA_MACRO TXT( #val9 )} EDITOR_COMMA_MACRO {val10 EDITOR_COMMA_MACRO TXT( #val10 )}} )

SD_BEGIN
class EDITOR_API DynamicEnum
{
public:
	virtual void InitDropdown (DropdownComponent* dropdown) = 0;

	/**
	 * Returns true if this DynamicEnum instance is associated with an enum.
	 */
	virtual bool IsBounded () const = 0;

	/**
	 * Creates a copy of this DynamicEnum instance and returns its reference.
	 * The caller of this function is expected to handle the pointer to memory leaks.
	 */
	virtual [[nodiscard]] DynamicEnum* Duplicate () const = 0;

	/**
	 * Assigns the associated enum to a value based on the given string.
	 */
	virtual void AssignFromString (const DString& enumText) = 0;

	/**
	 * Returns a string associated with this enum value.
	 * Returns nullptr if this isn't associated with an enum.
	 */
	virtual DString ToString () const = 0;

	/**
	 * Assigns the associated enum based on the given index value. This assumes the index corresponds to the enum order defined in the map.
	 */
	virtual void AssignFromIndex (Int enumIdx) = 0;

	/**
	 * Returns the index value of the associated enum based on the order of the map.
	 */
	virtual Int ToIndex () const = 0;
};
SD_END