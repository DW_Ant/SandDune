/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorUtils.h
  Common convenience functions for Editors.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EditorInterface;

class EDITOR_API EditorUtils : public BaseUtils
{
	DECLARE_CLASS(EditorUtils)
	

	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes a GuiEntity that contains a VerticalList composed of EditableComponents.
	 * It'll create a new component for each editable property specified in the given interface.
	 * The returned GuiEntity should reside within a Scrollbar since the vertical size is absolute and unknown at design time since
	 * EditableComponents may adjust their size runtime (eg: collapsing arrays).
	 * This function will not maintain a pointer to the returned object, the caller should take ownership over the return value.
	 */
	static GuiEntity* CreateInspector (EditorInterface* editedObj, Float lineSpacing);

	/**
	 * Iterates through each property in the given interface, and attempts to them from the specified config file and section.
	 * If it fails to load a property, it'll assign it to the default value.
	 */
	static void LoadFromConfig (ConfigWriter* config, const DString& sectionName, EditorInterface* loadingObj);

	/**
	 * Iterates through each property in the given interface, and attempts to save it to the specified config file and section.
	 * This function should be symmetrical to LoadFromConfig.
	 */
	static void SaveToConfig (ConfigWriter* config, const DString& sectionName, EditorInterface* savingObj);

	/**
	 * Attempts to load each variable for the given object from the DataBuffer.
	 * This function will not run any backwards compatibility checks since DataBuffers could be obtained outside of binary files.
	 * However, this function will return false if it failed to load any variable. A log warning will be issued for load errors.
	 */
	static bool LoadFromBuffer (const DataBuffer& incomingData, EditorInterface* loadingObj);

	/**
	 * Saves every variable from the specified object to the data buffer.
	 */
	static void SaveToBuffer (DataBuffer& outData, EditorInterface* savingObj);
};
SD_END