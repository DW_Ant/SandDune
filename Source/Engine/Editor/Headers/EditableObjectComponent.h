/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableObjectComponent.h
  This component is responsible for configuring an object reference. This is similar
  to an EditableStruct but it performs the following differences:

  * The sub properties are not loaded until the user expands the component. This is not only
  to save on resources, but also to handle infinite recursion (in case there's a circular reference).
  * Can become 'None' where the component doesn't reference any object.
  * The component can either own the object or reference an existing object. If it owns an object,
  the user must create the object in the component. This also allows the component to destroy the object.
  However if the component references an existing object, the component cannot destroy it. It'll only clear references
  when the object is destroyed externally.
  * EditableObjectComponent can only reference objects implementing the EditorInterface.
=====================================================================
*/

#pragma once

#include "Editor.h"
#include "EditPropertyComponent.h"

SD_BEGIN
class EditableStruct;
class GraphAsset;
class EditorInterface;

class EDITOR_API BaseObjBinder
{
public:
	//Returns true if the given EditorInterface can be assigned to the object reference the binder is associated with.
	virtual bool CanBeAssignedTo (EditorInterface* objRef) const = 0;

	//Assigns the object reference to the given object.
	virtual void SetObjReference (EditorInterface* newObjRef) = 0;
	virtual EditorInterface* GetEditorInterface () const = 0;
};

/**
 * Allows the associated variable to point to the new object reference. If the EditableObjectComponent changes
 * the object reference, it'll also update the edited object's object pointer to also update. If the edited object
 * changes the object it's pointing, it'll update all EditableObjectComponent's bound to that variable to also update.
 *
 * Reason for using separated template class:
 * 1. Can't directly convert EditorInterface** to ChildClass** even if ChildClass is an EditorInterface.
 * This is because the pointers would point to a different memory address based on the pointer type even if both
 * pointers are pointing at the same object instance.
 * 2. Extracted to its own templated class so that the EditableObjectComponent can remain as a nontemplated class.
 */
template <class T>
class ObjBinder : public BaseObjBinder
{
private:
	/* Pointer to the object reference. The object is expected to implement the EditorInterface. */
	T** AssociatedVariable;

public:
	ObjBinder (T** inAssociatedVariable) :
		AssociatedVariable(inAssociatedVariable)
	{
		//Noop
	}

	virtual bool CanBeAssignedTo (EditorInterface* objRef) const override
	{
		return (dynamic_cast<T*>(objRef) != nullptr);
	}

	virtual void SetObjReference (EditorInterface* newObjRef) override
	{
		(*AssociatedVariable) = dynamic_cast<T*>(newObjRef);
	}

	virtual EditorInterface* GetEditorInterface () const override
	{
		return (*AssociatedVariable);
	}
};

class EDITOR_API EditableObjectComponent : public EditPropertyComponent
{
	DECLARE_CLASS(EditableObjectComponent)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorObject : SInspectorProperty
	{
	public:
		/* If not nullptr, then this points to the variable pointer this inspector will be modifying.
		This object will take ownership over this pointer. */
		mutable BaseObjBinder* AssociatedPointer;

		/* If true, then this component will instantiate its own object. Otherwise, it'll reference an existing object instead. */
		bool bCreatesObject;

		/* If not nullptr, this property configures the object component to only reference objects of this class and its subclasses. */
		const DClass* BaseDClass;

		/* If not nullptr, this properties configures the object component to only reference GraphAssets of this class and its subclasses. */
		GraphAsset* BaseAsset;

		/* Set these delegates if the both BaseDClass and BaseAsset are nullptrs. */
		SDFunction<bool, EditorInterface*> OnIsEditorObjectRelevant; //Optional delegate: If it's not bound then any EditorInterface object is relevant.
		SDFunction<EditorInterface*, DropdownComponent*> OnCreateEditorObject;

		/* This is an optional delegate used to initialize created objects. */
		SDFunction<void, EditorInterface*> OnInitCreatedObject;

		SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject);
		SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject, const DClass* inBaseDClass);
		SInspectorObject (const DString& inPropertyName, const DString& inTooltipText, BaseObjBinder* inAssociatedPointer, bool inCreatesObject, GraphAsset* inBaseAsset);
		virtual ~SInspectorObject ();

		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate invoked whenever the user clicked on an object implementing an EditorInterface while trying to select an object (via: object picker).
	If this delegate returns true, then that object will become the EditedObject. If this is not bound, then all editor objects are relevant. */
	SDFunction<bool, EditorInterface*> OnIsEditorObjectRelevant;

	/* Delegate invoked whenever the user clicked on the create object button. This delegate is responsible for returning a created instance of a EditorInterface.
	The EditableObjectComponent will take ownership over the created object. */
	SDFunction<EditorInterface*, DropdownComponent* /*objTypeDropdown*/> OnCreateEditorObject;

	/* Delegate invoked immediately after an object is created. This is used to initialize anything for this object. This delegate is an optional delegate. This is not
	called for selecting existing objects. */
	SDFunction<void, EditorInterface*> OnInitCreatedObject;

protected:
	/* If true, then this component will own the object it references. Users can create and destroy objects using this component. */
	bool bCreatesNewObject;

	/* Reference to the object that this component is configuring. */
	EditorInterface* EditedObject;

	/* The color to use to wash out the screen when selectig an object. */
	Color SelectModeOverlayColor;

	/* Reference to the variable this component is associated with. If the system changes the variable reference, it'll also update the EditedObject.
	This is an optional variable. This is only used to bind this component to an existing member variable.
	The EditableObjectComponent takes ownership over the binder instance. */
	BaseObjBinder* AssociatedVariable;

	/* Component responsible for updating this component whenever the AssociatedVariable has changed. */
	TickComponent* UpdateReferenceTick;

	TextFieldComponent* ObjNameField;

	/* Button responsible for either setting the EditedObject to null or destroy the EditedObject (if bCreatesNewObject is true). */
	ButtonComponent* ClearButton;

	/* If bCreatesNewObject is true, then this button changes the state of this component to create an object. */
	ButtonComponent* CreateObjButton;

	/* If bCreatesNewObject is true, then this button allows developers to pick an existing object to duplicate its properties to the newly created object. */
	ButtonComponent* DuplicateObjButton;

	/* TextField that allows users to enter the name of an existing object to copy from. */
	TextFieldComponent* DuplicateObjField;

	/* If bCreatesNewObject is false, then this button changes the editor state to an 'object picker', allowing the user to click on an existing object to become
	this object's EditedObject. */
	ButtonComponent* SelectObjButton;

	MousePointer* DetectedMouse;

	/* If this component can create a new object, this DropdownComponent is used to select what kind of object to create. */
	DPointer<DropdownComponent> ObjTypeDropdown;

	/* The EditPropertyComponent the user clicks on the expand or collapse properties. */
	DPointer<EditableStruct> ExpandableComponent;

	/* When this component is in select object mode, this is the screen capture object used to generate the pixel map. This component will take ownership over this screen capture. */
	ScreenCapture* SelectModeScreenCap;

	/* Entity responsible for drawing the SelectMode sprite on top of the viewport. */
	Entity* SelectModeOverlay;

private:
	/* Object references specified when SetBaseClass or SetBaseAsset is called. */
	const DClass* BaseClass;
	DPointer<GraphAsset> BaseAsset;

	bool bCreateObjectInitialized;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual void UnbindVariable () override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual void SetLineHeight (Float newLineHeight) override;

	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableInput (const sf::Event& evnt) override;

protected:
	virtual void InitializeComponents () override;
	virtual DString ConstructValueAsText () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pairs this component to the variable so that any changes to the variable is reflected in this component.
	 * It's important to keep this name the same as others to remain compatible with the templated EditableArray.
	 */
	virtual void BindVariable (BaseObjBinder* newAssociatedVariable);

	/**
	 * Only applicable if this component can only select existing objects. This function is responsible for generating a grayed out sprite that overlays the entire window.
	 * While in select mode, the user is required to click on a relevant object drawn on screen or escape to cancel.
	 * This mode will override the mouse cursor and take input priority.
	 * If no objects are visible, it'll display a caption stating that nothing is selectable from current view.
	 */
	virtual void EnterSelectMode ();

	virtual bool IsInSelectMode () const;

	/**
	 * Only applicable if this component is in select mode, where it permits the user to select an EditorInterface to be placed as this component's EditedObject.
	 * This function exits that mode.
	 */
	virtual void ExitSelectMode ();

	/**
	 * If bCreatesNewObject is true:
	 * 1. Populates the ObjTypeDropdown with all non abstract subclasses.
	 * 2. This also binds the create object delegate to create an Object based on the user selected DClass.
	 * If bCreatesNewObject is false:
	 * 1. Filters the object selection to only select objects that are either equal or a subclass of the specified DClass.
	 */
	virtual void SetBaseClass (const DClass* parentClass);
	virtual void SetBaseAsset (GraphAsset* parentAsset);

	/**
	 * Adjusts the pointer to look at the new pointer address.
	 * Calling this function will not cause this editable object component to destroy the previous instance it was pointing at nor unbind any delegates.
	 * If there's a need to point to a different EditorInterface instance, call SetEditedObject instead.
	 * Use this in case the pointer, itself, was moved (eg: when vectors move).
	 */
	virtual void MovePointer (EditorInterface* newObj);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCreateNewObject (bool newCreateNewObject);
	virtual void SetEditedObject (EditorInterface* newEditedObject);
	virtual void SetSelectModeOverlayColor (Color newSelectModeOverlayColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EditorInterface* GetEditedObject () const
	{
		return EditedObject;
	}

	inline Color GetSelectModeOverlayColor () const
	{
		return SelectModeOverlayColor;
	}

	inline DropdownComponent* GetObjTypeDropdown () const
	{
		return ObjTypeDropdown.Get();
	}

	inline EditableStruct* GetExpandableComponent () const
	{
		return ExpandableComponent.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if this component is responsible for deleting the object it's referencing.
	 */
	virtual bool OwnsReferencedObj () const;

	/**
	 * Recursively climbs up the RenderTarget chain that this component resides in until it finds the outer-most render target.
	 */
	virtual RenderTarget* FindSelectModeRenderTarget ();

	virtual void UpdateFieldControls ();
	virtual void FixFieldControlOrder ();

	/**
	 * Changes the state of this component to be in a 'enter object by name' mode. While in this mode, a text field appears with a caption.
	 * The user can enter the name of the object. If it exists, the component will duplicate the object, and this component will take ownership over the new copy.
	 */
	virtual void BeginCopyObjectMode ();
	virtual bool IsInCopyObjectMode () const;
	virtual void EndCopyObjectMode ();

	/**
	 * Searches for an Editor object with the matching name, and if one is found, it'll duplicate that object and reference the object copy.
	 * Returns true if an object is duplicated.
	 */
	virtual bool CopyObjectByName (const DString& objName);

	/**
	 * Iterates through the EditableObject's editable properties to populate the ExpandComponent's property list.
	 */
	virtual void PopulateObjectProperties ();

	/**
	 * Invoked whenever the user manually entered something in the ObjNameField. This is used to find the object associated with the matching name.
	 * If it's in invalid format or an object is not found, it'll reset the name field to "None".
	 */
	virtual void ProcessObjNameChange ();
	virtual void RefreshHeaderText ();

	/**
	 * Updates the ObjType's visibility based on the state of this component.
	 */
	virtual void RefreshObjTypeVisibility ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleClearButtonReleased (ButtonComponent* button);
	virtual void HandleCreateButtonReleased (ButtonComponent* button);
	virtual void HandleDuplicateButtonReleased (ButtonComponent* button);
	virtual void HandleDuplicateButtonReleasedRightClick (ButtonComponent* button);
	virtual void HandleSelectObjButtonReleased (ButtonComponent* button);
	virtual void HandleDuplicateObjFieldReturn (TextFieldComponent* uiComp);
	virtual void HandleObjNameFieldReturn (TextFieldComponent* uiComp);
	virtual void HandleToggleExpandComp (bool isCollapsed);
	virtual void HandleEditedObjectChangeId ();
	virtual void HandleEditedObjectChangeName ();
	virtual void HandleEditedObjectDestroyed (EditorInterface* obj);

	virtual EditorInterface* HandleCreateObject (DropdownComponent* objTypeDropdown);
	virtual EditorInterface* HandleCreateGraphAsset (DropdownComponent* objTypeDropdown);
	virtual bool HandleIsObjectRelevant (EditorInterface* obj);
	virtual bool HandleIsGraphAssetRelevant (EditorInterface* obj);

	virtual bool HandleIsScreenCapRelevant (RenderComponent* renderComp);

	virtual bool HandleSelectModeInput (const sf::Event& sfEvent);
	virtual bool HandleSelectModeText (const sf::Event& sfEvent);
	virtual bool HandleSelectModeMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType type);
	virtual bool HandleSelectModeMouseWheel (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& scrollEvent);

	virtual bool HandleSelectModeMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt);

	virtual void HandleUpdateReferenceTick (Float deltaSec);
};
SD_END