/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableArray.h
  A templated EditPropertyComponent that permits the end user to modify a vector of EditableProperties.
=====================================================================
*/

#pragma once

#include "EditableArrayBinder.h"
#include "EditPropertyComponent.h"
#include "InspectorList.h"

SD_BEGIN
class EditorInterface;

class EDITOR_API EditableArray : public EditPropertyComponent 
{
	DECLARE_CLASS(EditableArray)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorArray : public SInspectorProperty
	{
	public:
		/* Reference to the EditPropertyComponent CDO instance that will be used to initialize the Array's ItemTemplate. Use this for simple cases (single components using default meta data).
		This is disabled if ItemTemplateList's PropList is not empty. */
		const EditPropertyComponent* ItemTemplateClass;

		/* ItemTemplate used to initialize the Array's ItemTemplate. Use this for custom meta (such as min/max values), and use this for data structs (for sub property names and multiple components). */
		InspectorList* ItemTemplateList;

		/* Binder used to associate a vector to the constructed EditableArray. The SInspectorArray struct will take ownership over this pointer. It will be
		automatically deleted when the struct is destroyed. Mutable to transfer ownership to EditableArray. */
		mutable BaseEditableArrayBinder* Binder;

		/* Callback to use to populate the EditableArray. This is only applicable if the Binder is nullptr. */
		SDFunction<void, EditableArray*> OnPopulateArray;

		bool bFixedSize;
		Int MaxItems;

		/**
		 * The SInspectorArray will take ownership over the ItemTemplate and binder. The objects will automatically be deleted on destruction.
		 */
		SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, const DClass* inItemTemplateClass, BaseEditableArrayBinder* inBinder);
		SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, const EditPropertyComponent* inItemTemplateClasses, BaseEditableArrayBinder* inBinder);
		SInspectorArray (const DString& inPropertyName, const DString& inTooltipText, InspectorList* inItemTemplateList, BaseEditableArrayBinder* inBinder);
		virtual ~SInspectorArray ();
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;

		/**
		 * Quick utility that creates a Binder that assumes its type implements the DataBuffer >> and << operators.
		 * Call this for DProperties. If the operators are not defined, then manually create an EditableArrayBinder instance that defines the lambdas.
		 * NOTE: For types with multiple variables (structs), it is important that the data order must match exactly how the GuiComponents are ordered.
		 */
		template<class V>
		static BaseEditableArrayBinder* CreateBasicBinder (std::vector<V>* associatedVector, V inDefaultValue)
		{
			return new EditableArrayBinder<V>(associatedVector, inDefaultValue,
				[](const DataBuffer& incomingData, V& outVar)
				{
					return !(incomingData >> outVar).HasReadError();
				},
				[](const V& incomingVar, DataBuffer& outData)
				{
					outData << incomingVar;
				});
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The label that simply displays the array size. */
	LabelComponent* ArraySizeLabel;

	/* Various array controls. These are also in the FieldControl vector.*/
	ButtonComponent* AddItemButton;
	ButtonComponent* EmptyArrayButton;

	/* List of planar transform components (one for each element in the array) where these components determine where the developer can drag the element to reorder it.
	This vector doesn't correspond is not guaranteed to be in the same order of the array. */
	std::vector<PlanarTransformComponent*> ElementDragRegions;

	/* Object responsible for binding this GuiComponent to a vector. */
	BaseEditableArrayBinder* Binder;

	/* The vertical size in absolute space each line occupies. */
	Float ElementHeight;

	/* Template used to figure out what elements should this array contain. */
	InspectorList ItemTemplate;

	/* If true, then this component will not permit the user to modify the array's size. */
	bool bFixedSize;

	/* Maximum number of elements that are permitted. If not positive, then there is no limit. */
	Int MaxItems;

	/* Becomes true if the individual elements are visible. Otherwise it's collapsed. */
	bool bExtended;

	/* The element the user is currently dragging for reordering purposes. */
	size_t ElementDragIdx;

	/* When dragging elements around, this is the index position where the element will be positioned if the user released the mouse button. */
	size_t ElementDragDestIdx;

	/* The mouse position when the user first started dragging an element (in mouse coordinates). */
	Vector2 StartDragPos;

	/* Object used to display the ghost image attached to the cursor when dragging an element around to reposition it in the array. */
	PlanarTransformComponent* DragGhostTransform;

	/* The texture used to display as the ghost image when dragging an element around. */
	RenderTexture* DragGhostTexture;

	/* When dragging elements around, this is the transform of the sprite component that displays where the element will be positioned. */
	PlanarTransformComponent* DragDestTransform;

	/* Color component used to display where the dragged element will be positioned. */
	ColorRenderComponent* DragDestColorComp;

private:
	/* The absolute Y coordinates the mouse cursor must cross before the element drag destination idx is recomputed. Without it, the destination will be computed
	on every mouse move event. */
	Range<Float> DragMouseYPosThreshold;

	/* Reference to the mouse pointer instance this array is overriding its icon. */
	DPointer<MousePointer> OverrideMouse;

	/* Only applicable when hovering over an element. This is the PlanarTransformComponent the developer is currently dragging around. */
	PlanarTransformComponent* HoveredDragRegionTransform;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool RemoveComponent (EntityComponent* target) override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual GuiComponent* GetComponentTooltipsShouldAttachTo () override;
	virtual void UpdatePropertyHorizontalTransforms () override;
	virtual void UnbindVariable () override;
	virtual void SetNormalFontColor (Color newNormalFontColor) override;
	virtual void SetReadOnlyFontColor (Color newReadOnlyFontColor) override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual void SetLineHeight (Float newLineHeight) override;

protected:
	virtual void Destroyed () override;
	virtual void InitializeComponents () override;
	virtual Float CalcVerticalSpace (bool includeSubComp) const override;
	virtual DString ConstructValueAsText () const override;
	virtual void ProcessApplyEdit (EditPropertyComponent* editedComp) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Once all mutators are set, call this function to construct the GuiComponents.
	 * It's not handled automatically for performance reasons. This function must be explicitly called.
	 */
	virtual void ConstructArray ();

	/**
	 * Either creates or destroys sub components until the EditableArray matches the specified size.
	 */
	virtual void SetArraySize (size_t newSize);

	/**
	 * Syncs this component to the EditedVector. All values from the EditedVector are set in each individual element regardless
	 * if the user is currently editing an element or not.
	 */
	virtual void SyncVector ();

	/**
	 * Toggles the collapse and expansion of the array list.
	 */
	virtual void ToggleCollapse ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetElementHeight (Float newElementHeight);
	virtual void SetFixedSize (bool bNewFixedSize);
	virtual void SetMaxItems (Int newMaxItems);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline InspectorList& EditItemTemplate ()
	{
		return ItemTemplate;
	}

	inline bool IsFixedSize () const
	{
		return bFixedSize;
	}

	inline bool IsCollapsed () const
	{
		return !bExtended;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports the contents of the edited vector into the array components. EditedArray imported to EditedArrayCopy.
	 */
	virtual void ImportVector ();

	/**
	 * Exports the contents of the edited elements into the edited vector. This will replace the contents of that vector with these items.
	 * EditedArrayCopy imported to EditedArray.
	 */
	virtual void ExportVector ();

	/**
	 * Creates and initializes a new EditPropertyComponent that will attach at the end of the ArrayList.
	 * This will not bind the new component to a variable.
	 * @param bRefreshControls - If true, then this function will update the array size label and field controls enabledness based on the new array size. Set this to false if calling this inside a loop.
	 */
	virtual void AddEditableComponent (bool bRefreshControls, size_t componentIndex);

	/**
	 * Changes the order of the vector so that the element can move from old index to new index.
	 */
	virtual void MoveElement (size_t oldIdx, size_t newIdx);

	/**
	 * Returns the index of the SubPropertyComponents that contains the given FieldControl.
	 * Complexity is linear. Returns SD_MAXINT if not found.
	 */
	virtual size_t FindEditableCompIdx (ButtonComponent* fieldControl) const;

	/**
	 * Returns the FieldControl button from the given EditPropertyComponent that also handles the given delegate.
	 */
	virtual ButtonComponent* FindElementFieldControl (EditPropertyComponent* editComp, SDFunction<void, ButtonComponent*> ctrlDelegate) const;

	/**
	 * Refreshes the AddElement, EmptyArray, and the ArraySize label based on the state of the current state of the array.
	 */
	virtual void RefreshArraySizeState ();

	/**
	 * Creates the Entity instances to display and attach a ghostly image to the given mouse cursor.
	 */
	virtual void InitDragGhostObjs (MousePointer* attachTo);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEmptyArrayReleased (ButtonComponent* uiComp);
	virtual void HandleAddItemReleased (ButtonComponent* uiComp);
	virtual void HandleInsertItemReleased (ButtonComponent* uiComp);
	virtual void HandleDeleteItemReleased (ButtonComponent* uiComp);

	virtual bool HandleDragRegionHover (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent);
	virtual bool HandleDragElement (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Associates this component to modify the specified vector.
	 * This component doesn't sync the vector until either ConstructArray or SyncVector is called.
	 */
	template <class V>
	void BindVariable (std::vector<V>* editedVector, V defaultItem)
	{
		if (Binder != nullptr)
		{
			delete Binder;
			Binder = nullptr;
		}

		//The lambdas assume V is a DProperty that interfaces with DataBuffers.
		if (editedVector != nullptr)
		{
			Binder = new EditableArrayBinder<V>(editedVector, defaultItem,
				[](const DataBuffer& incomingData, V& outVar)
				{
					return !(incomingData >> outVar).HasReadError();
				},
				[](const V& incomingVar, DataBuffer& outData)
				{
					outData << incomingVar;
				});
		}
	}

	/**
	 * Same as BindVariable but this defines custom lambda functions that converts its array elements to a data buffer.
	 * Call this function if it's a vector of nonDProperty types (eg: structs, objects, or primitives).
	 */
	template <class V>
	void BindVariable (std::vector<V>* editedVector, V defaultItem, const std::function<bool(const DataBuffer&, V&)> onBufferToItem, const std::function<void(const V&, DataBuffer&)> onItemToBuffer)
	{
		if (Binder != nullptr)
		{
			delete Binder;
			Binder = nullptr;
		}

		if (editedVector != nullptr)
		{
			Binder = new EditableArrayBinder<V>(editedVector, defaultItem, onBufferToItem, onItemToBuffer);
		}
	}
};
SD_END