/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditableInt.h
  A property component that allows the end user to edit an Int variable within an editor.
=====================================================================
*/

#pragma once

#include "EditableField.h"

SD_BEGIN
class EDITOR_API EditableInt : public EditableField
{
	DECLARE_CLASS(EditableInt)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorInt : public SInspectorProperty
	{
		bool bHasMinValue;
		bool bHasMaxValue;
		Range<Int> ValueRange;
		Int* AssociatedVariable;
		Int DefaultValue;
		
		/* Determines the initial int. Only applicable if AssociatedVariable is nullptr and InitType is IIV_UseOverride */
		Int InitOverride;

		SInspectorInt (const DString& inPropertyName, const DString& inTooltipText, Int* inAssociatedVariable, Int inDefaultValue = 0);
		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The min and max values to clamp the int to. These are disabled if set to SD_MININT and SD_MAXINT respectively. */
	Int MinValue;
	Int MaxValue;

	/* The actual Int this component is editing. */
	Int* EditedInt;

	Int DefaultInt;

	/* Component responsible for updating the field whenever EditedInt has changed. */
	TickComponent* UpdateIntTick;

private:
	/* Variable the EditedInt will bind to if this component is bound to a local variable (from calling BindToLocalVariable). */
	Int LocalInt;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void CopyToBuffer (DataBuffer& copyTo) const override;
	virtual bool CopyFromBuffer (const DataBuffer& incomingData) override;
	virtual void BindToLocalVariable () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual void RemoveVarUpdateTickComponent () override;
	virtual void ResetToDefaults () override;
	virtual void UnbindVariable () override;

protected:
	virtual void InitializeComponents () override;
	virtual void ApplyEdits () override;
	virtual DString GetPermittedInputChars () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Pairs this component to the variable so that any changes to this component is reflected in its associated variable.
	 * It's important to keep this name the same as others to remain compatible with the templated EditableArray.
	 */
	virtual void BindVariable (Int* newEditedInt);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMinValue (Int newMinValue);
	virtual void SetMaxValue (Int newMaxValue);

	/**
	 * The name of this function must match others to allow the templated StructElement to interface with this function.
	 */
	virtual void SetDefaultValue (Int newDefaultInt);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsUsingMinBounds () const
	{
		return (MinValue == SD_MININT);
	}

	inline bool IsUsingMaxBounds () const
	{
		return (MaxValue == SD_MAXINT);
	}

	inline Int GetMinValue () const
	{
		return MinValue;
	}

	inline Int GetMaxValue () const
	{
		return MaxValue;
	}

	inline Int* GetEditedInt () const
	{
		return EditedInt;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleUpdateIntTick (Float deltaSec);
};
SD_END