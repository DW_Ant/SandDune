/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditablePropCommand.h
  A property component that simply displays a button. The purpose of this
  component is to interface a simple button to the Editor property structures
  so it can be used in inspectors, structs, and arrays.
=====================================================================
*/

#pragma once

#include "EditPropertyComponent.h"

SD_BEGIN
class EDITOR_API EditablePropCommand : public EditPropertyComponent
{
	DECLARE_CLASS(EditablePropCommand)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct EDITOR_API SInspectorPropCmd : public SInspectorProperty
	{
	public:
		/* The command to execute when the button is pushed down. */
		SDFunction<void, ButtonComponent*, EditablePropCommand*> OnPressed;

		/* The alternate command to execute when the button is pressed down via right click.
		If this is nullptr, right click will execute the default command (left click). */
		SDFunction<void, ButtonComponent*, EditablePropCommand*> OnRightPressed;

		/* The command to execute when the button is released. */
		SDFunction<void, ButtonComponent*, EditablePropCommand*> OnReleased;

		/* This is the alternate command to execute when the button is pressed via right click.
		If this is a nullptr, right click will execute the default command (left click). */
		SDFunction<void, ButtonComponent*, EditablePropCommand*> OnRightReleased;

		SInspectorPropCmd (const DString& inPropertyName, const DString& inTooltipText, const SDFunction<void, ButtonComponent*, EditablePropCommand*>& inOnReleased);

		virtual void LoadConfig (ConfigWriter* config, const DString& sectionName) override;
		virtual void SaveConfig (ConfigWriter* config, const DString& sectionName) const override;
		virtual bool LoadBinary (const DataBuffer& incomingData) override;
		virtual void SaveBinary (DataBuffer& outData) const override;
		virtual EditPropertyComponent* CreateBlankEditableComponent () const override;
		virtual void EditPropComponent () const override;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The command to execute when the button is pushed down. */
	SDFunction<void, ButtonComponent*, EditablePropCommand*> OnPressed;

	/* The alternate command to execute when the button is pressed down via right click.
	If this is nullptr, right click will execute the default command (left click). */
	SDFunction<void, ButtonComponent*, EditablePropCommand*> OnRightPressed;

	/* The command to execute when the button is released. */
	SDFunction<void, ButtonComponent*, EditablePropCommand*> OnReleased;

	/* This is the alternate command to execute when the button is pressed via right click.
	If this is a nullptr, right click will execute the default command (left click). */
	SDFunction<void, ButtonComponent*, EditablePropCommand*> OnRightReleased;

protected:
	DPointer<ButtonComponent> Button;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual ButtonComponent* AddFieldControl () override;
	virtual SInspectorProperty* CreateDefaultInspector (const DString& propName) const override;
	virtual GuiComponent* GetComponentTooltipsShouldAttachTo () override;
	virtual void SetReadOnly (bool bNewReadOnly) override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ButtonComponent* GetButton () const
	{
		return Button.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleButtonPressed (ButtonComponent* button);
	virtual void HandleButtonRPressed (ButtonComponent* button);
	virtual void HandleButtonReleased (ButtonComponent* button);
	virtual void HandleButtonRReleased (ButtonComponent* button);
};
SD_END