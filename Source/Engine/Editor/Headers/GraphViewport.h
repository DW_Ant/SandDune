/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphViewport.h
  A GuiComponent that manages objects to display a viewport to an GraphEditor.

  This Entity manages the RenderTexture used to display the Graph.
  It also manages the camera used to determine the perspective.

  This Entity does NOT manage the Graph, itself, since there may be
  multiple viewports viewing the same Graph entity.

  This Viewport acts very similar to the ScrollbarComponent regarding displaying
  a frame and relaying input events via FakeMouse. The main differences between
  this Entity and the ScrollbarComponent is that the GraphViewport does NOT
  own the Graph entity. There can be a many-to-one relationship between Viewports
  and the Graph. The Scrollbar contains a one-to-one relationship.

  In addition to that, this Viewport does not have the same input controls as the
  scrollbar. The Graph will determine how the camera moves instead of the scrollbar
  determining how the camera moves. This also doesn't include the Scrollbar's
  slider bars on the right and bottom of the component.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class GraphEditor;

class EDITOR_API GraphViewport : public GuiComponent, public FakeMouseOwnerInterface
{
	DECLARE_CLASS(GraphViewport)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The RenderTexture used to display the graph. */
	DPointer<RenderTexture> ViewportTexture;

	/* Reference to the GraphEditor this viewport is displaying. This Entity does NOT take ownership of this graph to support split screen. */
	DPointer<GraphEditor> Graph;

	/* Camera responsible for projecting all rendered objects in this graph. */
	DPointer<PlanarCamera> Cam;

	/* The SpriteComponent used to rendering the ViewportTexture. */
	DPointer<FrameComponent> RenderFrame;

	/* The fake mouse instance the GraphEditor will reference when processing mouse events. This instance always resides within graph coordinates. */
	FakeMouse* GraphMouse;

	/* TickComponent responsible for updating the texture size. This is only active whenever this viewport changes size. */
	TickComponent* UpdateTextureSizeTick;

private:
	/* The previous size of the RenderFrame. This is used if the ViewportTexture size needs to be updated. */
	Vector2 CurSize;

	/* If not nullptr, then this is the mouse pointer instance this Entity is overriding its icon. */
	DPointer<MousePointer> OverrideMouse;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual void PostAbsTransformUpdate () override;

	virtual void ExecuteInput (const sf::Event& evnt) override;
	virtual void ExecuteText (const sf::Event& evnt) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual bool ExecuteConsumableInput (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableText (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;

	virtual Vector2 FakeMouseToInnerCoordinates (const Vector2& outerMousePos) const override;
	virtual Vector2 FakeMouseToOuterCoordinates (const Vector2& innerMousePos) const override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetGraph (GraphEditor* newGraph);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline RenderTexture* GetViewportTexture () const
	{
		return ViewportTexture.Get();
	}

	inline GraphEditor* GetGraph () const
	{
		return Graph.Get();
	}

	inline PlanarCamera* GetCam () const
	{
		return Cam.Get();
	}

	inline FrameComponent* GetRenderFrame () const
	{
		return RenderFrame.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Converts the given absolute coordinates (relative to top left corner of the game window) to graph coordinates.
	 */
	virtual Vector2 ToGraphCoordinates (const Vector2& absCoordinates) const;

	/**
	 * Replaces the RenderTexture with a new instance that reflects the current size of this component's size.
	 */
	virtual void ReconstructRenderTexture ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleUpdateTextureSize (Float deltaSec);
	virtual bool HandleIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent);
};
SD_END