/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariableNode.h

  A VariableNode is a graph component responsible for accessing a ScriptVariable.
  A VariableNode is still a FunctionNode since a getter and setter methods are still
  technically a function.

  The VariableNode can only access local variables and member variables from the owning asset, allowing
  this to omit the 'InvokeOn' components. Because of this constraint, the user is able to change the variable
  without having to reconstruct the node. If there's a need to use variables from an external asset, a standard
  FunctionNode must be used to expose the 'InvokeOn' components while also disabling variable change components found here.

  This class adds the following functionalities:
  * Additional GuiComponents that enables the user to specify which variable.
  * Relevance checks to see which variables are available based on the function context.
  * Highlighting other nodes of the same variable.
  * Updating node references whenever the variable, itself, changes or conflicts with
  other variables.
=====================================================================
*/

#pragma once

#include "FunctionNode.h"
#include "GraphVariable.h"

SD_BEGIN
class GraphAsset;

class EDITOR_API VariableNode : public FunctionNode
{
	DECLARE_CLASS(VariableNode)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* String to use as the variable scope when saving this node as a local variable. */
	static const DString LOCAL_SCOPE;

	/* Name of the variable this node is accessing. The reason why it's using a string instead of a GraphVariable reference because of two reasons.
	* Local variables should be managed here instead of the owning asset. And multiple local nodes of the same name are associated with each other. The compiler will pair them up.
	* Member variables can change. This could lead to dangling pointers (especially since the variables are inside vectors). This is extra problematic for assets that haven't loaded (due to inherited assets),
	* or partially loaded assets (function definitions may not exist, but the member variable array can be still modified).
	* Developers can modify the ini file directly. The saved variable names may or may not be found. If the node cannot find the variable (and it expects to be a member variable),
	* then it'll throw an error message.
	* If empty, then this node is not associated with any variable (not yet initialized).
	* If the associated variable will always match the member variable's variable name regardless if the variable has a display name or not. */
	DString VariableId;

	/* The name of the variable being displayed on the node. This is without the variable's identifying string format, and it may even be the localized version of the variable (eg: when the var's display name has data in it). */
	DString VarDisplayName;

	/* If true, then the associated variable is a local variable. Local variables behaved differently from member variables. If it's a local variable, this node will do the following:
	- Allows this variable and other variables of matching names to be renamed.
	- Obtains variable type from the asset instead of the function it's linked to. 
	- Member variables can produce error messages if the variable is not found, or does not match the expected type. */
	bool bLocalVariable;

	/* When binding to a function, this type is updated to inform this node what variables are permitted to bind to this node.
	The AssociatedVariable type should always be this value. */
	ScriptVariable::EVarType VarType;

	/* If the VarType is an Object or Class type, this is the GraphAsset class associated with this variable. */
	GraphAsset* ConstraintClass;

	/* When configuring the variable, the node's size will stretch to this value if it's smaller than this threshold. If not positive, then this node will not
	adjust its size when the user is editing the associated variable. */
	Vector2 EditNodeSize;

	/* Color to use when displaying an error message over this node. */
	Color ErrorBackgroundColor;

	/* Color to use for the prompt when asking the user if they want to update all variable references. */
	Color PromptBackgroundColor;

	DPointer<ButtonComponent> EditButton;
	DPointer<TextFieldComponent> RenameVarField;
	DPointer<DropdownComponent> SelectVarDropdown;

	/* Tooltip responsible for displaying the variable comment. */
	TooltipComponent* VarCommentTooltip;

	/* Color modifier that becomes visible whenever the user selected a different variable node that modifies the same variable instance. */
	PlanarTransformComponent* CommonVarHighlightTransform;
	DPointer<RenderComponent> CommonVarHighlight;

	/* Frame component to use when displaying error messages or a prompt. */
	FrameComponent* PopupBackground;

private:
	/* Last name this variable use to be before the renaming. */
	DString LastVarName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual void ExecuteInput (const sf::Event& evnt) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	virtual void SetSelected (bool newSelected) override;
	virtual void SetHighlightThickness (Float newHighlightThickness) override;

	virtual void BindFunction (GraphFunction* newAssociatedFunction) override;

protected:
	virtual void InitializeComponents () override;
	virtual void CalcNodeSize (Vector2& outNodeSize) const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to find the variable reference the given varId corresponds to. Expected format of varId is based on the results produced from GraphVariable::GetIdentifyingName.
	 * This will not search through local variables.
	 * This will not perform any variable checks (eg: it may still return normal member variables even if the owning function is a static function).
	 */
	virtual GraphVariable* FindGraphVariable (const DString& varId) const;

	/**
	 * Binds this node to the given member variable.
	 */
	virtual void SetAssociatedVariable (GraphVariable* newAssociatedVariable);

	/**
	 * Converts the variable to a local variable if not already. It'll rename the variable to the new name if possible.
	 * This function also performs error checking to ensure it will not conflict with other nodes. Returns true if there are no errors.
	 * If renaming is successful and there are other node references that use to have the same name, this function will prompt the user if they also want to update those references.
	 */
	virtual bool SetLocalVariable (const DString& newName);
	

	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetConstraintClass (GraphAsset* newConstraintClass);
	virtual void SetEditNodeSize (const Vector2& newEditNodeSize);
	virtual void SetErrorBackgroundColor (Color newErrorBackgroundColor);
	virtual void SetPromptBackgroundColor (Color newPromptBackgroundColor);

	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetVariableId () const
	{
		return VariableId;
	}

	inline const DString& ReadVariableId () const
	{
		return VariableId;
	}

	inline DString GetVarDisplayName () const
	{
		return VarDisplayName;
	}

	inline const DString& ReadVarDisplayName () const
	{
		return VarDisplayName;
	}

	inline bool IsLocalVariable () const
	{
		return bLocalVariable;
	}

	inline ScriptVariable::EVarType GetVarType () const
	{
		return VarType;
	}

	inline GraphAsset* GetConstraintClass () const
	{
		return ConstraintClass;
	}

	inline Vector2 GetEditNodeSize () const
	{
		return EditNodeSize;
	}

	inline const Vector2& ReadEditNodeSize () const
	{
		return EditNodeSize;
	}

	inline Color GetErrorBackgroundColor () const
	{
		return ErrorBackgroundColor;
	}

	inline Color GetPromptBackgroundColor () const
	{
		return PromptBackgroundColor;
	}

	inline ButtonComponent* GetEditButton () const
	{
		return EditButton.Get();
	}

	inline TextFieldComponent* GetRenameVarField () const
	{
		return RenameVarField.Get();
	}

	inline DropdownComponent* GetSelectVarDropdown () const
	{
		return SelectVarDropdown.Get();
	}

	inline RenderComponent* GetCommonVarHighlight () const
	{
		return CommonVarHighlight.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if this VariableNode is the same type as the given node.
	 */
	virtual bool IsSameTypeAs (VariableNode* otherNode) const;
	virtual bool IsSameTypeAs (GraphVariable* var) const;
	virtual bool IsSameTypeAs (ScriptVariable::EVarType otherVarType, GraphAsset* otherClassConstraint) const;

	/**
	 * Assembles a string for the header text based on the associated function and variable this node is bound to.
	 */
	virtual void UpdateHeaderText ();

	/**
	 * Queries the owning function to retrieve all variables this node has access to. If the function is const and the associated function is not a const function,
	 * it'll only return local variables. The results of the list is then populated to the SelectVarDropdown.
	 */
	virtual void PopulateVariableList ();

	/**
	 * Temporarily displays an error message above this node. The label component displaying the error message will only display for a short time period.
	 */
	virtual void DisplayErrorMessage (const DString& localizationKey);

	/**
	 * Displays a message if the user wants to update all associated variable nodes to the new names.
	 */
	virtual void DisplayUpdateReferencesPrompt (Int numAssociatedNodes);

	/**
	 * Restores this node component to exit edit mode. It'll conceal the components that can edit the variable, and possibly restore it back to its normal dimensions.
	 */
	virtual void ExitEditMode ();

	virtual void SetAllPortVisibility (bool isVisible);

	/**
	 * Iterates through each component attached to the owner of this node, and populates all variable nodes that are associated with the same variable as this node.
	 * This function does not include the function parameters.
	 * @param nameMatch If searching for local variables, then the local variable must match this name in order for it to be in the list.
	 */
	virtual void FindAllAssociatedNodes (bool includeSelf, const DString& nameMatch, std::vector<VariableNode*>& outOtherNodes) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEditReleased (ButtonComponent* button);
	virtual void HandleCloseError (ButtonComponent* button);
	virtual void HandleUpdateReferences (ButtonComponent* button);
	virtual void HandleDeclineUpdateReferences (ButtonComponent* button);
	virtual bool HandleAllowVarRenameInput (const DString& txt);
	virtual void HandleApplyVarRename (TextFieldComponent* textField);
	virtual void HandleSelectVarExpanded ();
	virtual void HandleSelectVar (DropdownComponent* dropdown, Int itemIdx);

	virtual void HandleNameChanged (const DString& prevId, const DString& varId, const DString& newName);
};
SD_END