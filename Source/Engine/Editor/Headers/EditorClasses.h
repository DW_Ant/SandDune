/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorClasses.h
  Contains all header includes for the Editor module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the EditorClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_EDITOR
#include "AssetCatalogue.h"
#include "BaseStructElement.h"
#include "CoreAssetBuilder.h"
#include "DynamicEnum.h"
#include "EditableArray.h"
#include "EditableArrayBinder.h"
#include "EditableBool.h"
#include "EditableDString.h"
#include "EditableEnum.h"
#include "EditableField.h"
#include "EditableFloat.h"
#include "EditableInt.h"
#include "EditableObjectComponent.h"
#include "EditablePropCommand.h"
#include "EditableStruct.h"
#include "Editor.h"
#include "EditorEngineComponent.h"
#include "EditorInterface.h"
#include "EditorTheme.h"
#include "EditorUtils.h"
#include "EditorWorkerEngineComponent.h"
#include "EditPropertyComponent.h"
#include "ErrorNode.h"
#include "ExecNode.h"
#include "FunctionNode.h"
#include "GraphAsset.h"
#include "GraphCompiler.h"
#include "GraphContextMenu.h"
#include "GraphEditor.h"
#include "GraphFunction.h"
#include "GraphInterface.h"
#include "GraphNode.h"
#include "GraphVariable.h"
#include "GraphViewport.h"
#include "InspectorList.h"
#include "NodePort.h"
#include "ReferenceUpdater.h"
#include "ReroutePort.h"
#include "StructElement.h"
#include "TaskMessenger.h"
#include "VariableNode.h"
#include "VariablePort.h"

#endif
