/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphAsset.h
  This class enables the developer to craft their CLM objects.

  The GraphAsset is essentially the central class that manages a collection
  of member variables, GraphFunctions, and a GraphEntity for each function.

  This class doesn't always have to correspond to a CLM ContentObject.
  An asset may also be used to define a collection of global functions.

  There are two variations of GraphAssets. GraphAssets that define the asset, and
  those that instantiate a copy of an asset.
  Those that define an asset will have variables such as meta data, function definitions,
  and inheritance defined.
  Instances will only hold a reference to the definition and a copy of all member variables.
=====================================================================
*/

#pragma once

#include "EditableEnum.h"
#include "Editor.h"
#include "EditorInterface.h"
#include "GraphVariable.h"

SD_BEGIN
class EditableArray;
class EditableField;
class EditablePropCommand;
class EditPropertyComponent;
class GraphEditor;
class GraphFunction;
class InspectorList;

class EDITOR_API GraphAsset : public Object, public EditorInterface
{
	DECLARE_CLASS(GraphAsset)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ELoadState
	{
		LS_NotLoaded, //The object exists, but its variables are not yet initialized from the file such as the asset name, functions, variables, and parent class reference.
		LS_Partial, //Asset name and parent classes are linked. Member variables are declared but not defined. For example a texture references are only a string reference. Functions are declared but not defined.
		LS_Loaded //Partial loaded plus variables are initialized, assets are referenced, and functions are defined.
	};

	enum EAssetAttribute
	{
		AA_License,
		AA_Authors,
		AA_Comment,
		AA_Class,
		AA_ParentClass,
		AA_Global,
		AA_Abstract,
		AA_MemberVariables,
		AA_InheritFunctions,
		AA_Functions,
		AA_DefaultOverrides
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SVarOverride
	{
		/* Name of the variable this data struct is overriding. */
		DString VarName;

		/* If true, then the data buffer's value is relative to the parent's value. */
		bool bIsRelative;

		/* Data buffer containing the contents of the new value. */
		DataBuffer NewValueData;

		SVarOverride ();
		SVarOverride (const SVarOverride& cpyObj);
	};

	//Used to transfer FunctionDefinitions whenever a GraphFunctions are being replaced.
	struct SDefinitionTransfer
	{
		DString FunctionId;
		GraphEditor* FuncDefinition;

		SDefinitionTransfer (const DString& inFunctionId, GraphEditor* inFuncDefinition);
		SDefinitionTransfer (const SDefinitionTransfer& cpyObj);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* String regex used to determine what characters are valid for class, function, and variable names. */
	static const DString PERMITTED_CHARS_FOR_NAME;

	/* The string used to identify if a class is nullptr. */
	static const DString NULL_CLASS_NAME;

	/* Extension to use when saving this asset to the DevAssets directory. */
	static const DString DEV_ASSET_EXTENSION;

	/* Delegate broadcasted whenever this asset name has changed. This is to allow systems to adapt to the new name.
	Most editors will invoke the ReferenceUpdater at a time of their choosing (such as when saving or immediately).
	The old and new asset names are the full asset names (includes the "Class" prefix and the path). */
	SDFunction<void, GraphAsset* /*asset*/, const DString& /*oldName*/, const DString& /*newName*/> OnAssetNameChanged;

protected:
	/* If nullptr, then this asset is the class definition of that asset. Changing its variables and functions will affect all instances of matching name. This instance is registered as a LoadedAsset in the engine component.
	If valid pointer, then this asset is a mere instance of this pointer. Setting variables in this instance will not affect the others. Saving this asset will only save deltas compared to the
	definition's default property values. */
	GraphAsset* AssetDefinition;

	DString AssetName;

	/* If not empty, then this string is presented in the UI instead of AssetName. This is typically used for native assets, and may vary based on the localization settings. */
	DString DisplayedName;

	/* The type of asset this is. Editors use this to figure out if they should attempt to open this asset or not. For example, the map editor should only open map files.
	Although it's easy for the developer to simply edit the ini to a different type, this variable is merely protecting the developer from making accidental mistakes.
	Should they take the risk of changing the asset type in the ini, the worse that can happen is that its data is either not used or replaced when saving this asset in the wrong editor.
	For example, if attempting to load a texture asset as an audio asset may cause the audio editor to ignore texture data, and replace it with default audio properties.
	Becomes empty if this asset is a global asset. */
	DString AssetType;

	/* If true, then this asset is considered a global asset. A global asset only contains generic global functions that do not depend on other editor assets.
	A global asset allows any editor to open this asset. If true, then the AssetType will be empty, allowing any editor to load it. */
	bool bGlobal;

	ELoadState LoadState;

	/* The directory location where this asset is saved to. This is typically an absolute path. Otherwise it should be relative to the DevAsset directory. */
	Directory SaveLocation;

	/* List of dynamic variables the developer specified for this asset. This is only applicable for asset definitions for 'one source of truth' purposes. */
	std::vector<GraphVariable*> MemberVariables;

	/* List of dynamic variables copied from the asset definition. These variables can be changed at a per instance basis without affecting the asset's default values.
	This list also contain copies of the asset's inherited variables. This list does not include copies of DefaultOverrides.
	This vector is only applicable if the GraphAsset is not the asset definition. */
	std::vector<GraphVariable*> VarInstanceValues;

	/* List of member functions that override functions from the parent class. */
	std::vector<GraphFunction*> OverrideFunctions;

	/* List of new member functions this asset defines. */
	std::vector<GraphFunction*> MemberFunctions;

	/* List of variables this asset is overriding. */
	std::vector<SVarOverride> DefaultOverrides;

	/* Reference to the CLM's ContentClass this asset is associated with.
	This is only set when AssetName is not an empty string. */
	ContentClass* ScriptClass;

	/* Reference to the Asset this instance will inherit its variables and functions from.
	This instance will also have access to the SuperAsset's protected data. */
	DPointer<GraphAsset> SuperAsset;

	/* List of all GraphAsset instances that are referencing this asset as its definition. This is only used if bAssetDefinition is true. */
	std::vector<GraphAsset*> AssetInstances;

	/* Name of each collaborator that created or modified this asset. */
	DString Authors;

	/* License associated with this asset. */
	DString License;

	/* Human readable text that describes this asset. */
	DString Comment;

	/* If true, then instances of this asset cannot be created. */
	bool bAbstract;

	/* Determines the default size of the functions this asset creates where the size is essentially the stage size where to place nodes in the editor. */
	Vector2 DefaultFunctionSize;

private:
	/* List of all assets currently loading (via LoadEssentials or LoadFromFile). This is used to protect against infinite recursion when loading super assets. */
	static std::vector<DString> PendingLoadAssets;

	bool bAssetInitialized;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual DString GetFriendlyName () const override;

	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;

	virtual DString GetEditorObjName () const override;
	virtual DString GetEditorObjTooltip () const override;
	virtual void PopulateInspectorList (InspectorList& outList) override;
	virtual void DeleteEditorObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Changes the AssetName to the given name.
	 * If isAssetDefinition is true, then it'll register this asset to the EditorEngineComponent. If it's already registered, it'll reregister it to the engine component under the new name.
	 * Otherwise, it'll act like an instance of the definition.
	 */
	virtual void InitializeAsset (const DString& newAssetName, bool isAssetDefinition);

	/**
	 * Loads the essential variables for this asset (shift its load state from NotLoaded to PartialLoaded).
	 */
	virtual bool LoadEssentials ();

	/**
	 * Reads everything from the ini file for this asset (completely load the asset).
	 */
	virtual bool LoadFromFile ();

	/**
	 * Saves the contents of this asset to an ini.
	 * The ini file is named the same name as the class name.
	 * This will only save if the contents have fully loaded (does nothing if the load state is partial or not loaded).
	 * This is only applicable for GraphAsset definitions.
	 */
	virtual bool SaveToDevAsset () const;

	/**
	 * Records the contents of this GraphAsset instance to the give vector. Each element in the vector is a variable name and value pair (eg: "PropName=PropValue").
	 * Only variables that are different from their default values are recorded in the resulting vector.
	 * This is only applicable for GraphAsset instances.
	 */
	virtual bool SaveInstanceData (std::vector<DString>& outVarData) const;

	/**
	 * Creates and attaches an EditableArray to the given owner that allows the developer to override inherited functions for this asset.
	 * Returns a reference to the instantiated array if successful.
	 */
	virtual EditableArray* InitializeInheritedFunctionInspector (Entity* arrayOwner);

	/**
	 * If DisplayName is not empty, it'll return DisplayName.
	 * Otherwise it'll return the AssetName.
	 */
	virtual DString GetPresentedName () const;

	/**
	 * Returns this asset's name in class format (eg: class'Path.To.Asset')
	 * The path always begin with the path relative to the Dev_Asset directory.
	 */
	virtual DString GetFullAssetName () const;

	/**
	 * Converts the full asset name and figures out the path and asset name from that data.
	 * For example, parses class'Path.To.Asset' to outPath="Path.To" and outAssetName="Asset"
	 * Returns true if the fullAssetName is in a valid format.
	 */
	static bool ParseFullAssetName (const DString& fullAssetName, DString& outPath, DString& outAssetName);

	/**
	 * Returns true if this asset derives from the specified asset.
	 */
	virtual bool IsChildOf (const GraphAsset* otherAsset) const;

	/**
	 * Returns true if the given asset derives from this asset.
	 */
	virtual bool IsParentOf (const GraphAsset* otherAsset) const;

	/**
	 * Iterates through all member variables and try to find the variable that matches the variable matching the ID.
	 * This will not iterate through local variables since there could be multiple local variables of the same name in different functions.
	 * If a variable is found, it'll return a reference to the asset that owns the member variable. For example if a super asset defines the member variable, it'll return that asset.
	 */
	virtual GraphAsset* FindMemberVar (const DString& varName, bool checkInheritedVars, GraphVariable& outVar);

	/**
	 * Replaces the contents of the given class enumerator with the current state of the local engine's class list.
	 * Classes are sorted alphabetically.
	 * If a forbidden class is given, then it'll skip that class and all of its children when populating the list.
	 */
	static void RefreshClassList (DropdownComponent* classDropdown, GraphAsset* forbiddenClass);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDisplayedName (const DString& newDisplayedName);
	virtual void SetAssetType (const DString& newAssetType);
	virtual void SetGlobal (bool newGlobal);
	virtual void SetLoadState (ELoadState newLoadState);
	virtual void SetSaveLocation (const Directory& newSaveLocation);
	virtual void SetScriptClass (ContentClass* newScriptClass);
	virtual void SetSuperAsset (GraphAsset* newSuperAsset);
	virtual void SetAbstract (bool newAbstract);
	virtual void SetDefaultFunctionSize (const Vector2& newDefaultFunctionSize);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsAssetDefinition () const
	{
		return (AssetDefinition == nullptr);
	}

	inline GraphAsset* GetAssetDefinition () const
	{
		return AssetDefinition;
	}

	inline DString GetAssetName () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetAssetName();
		}

		return AssetName;
	}

	inline const DString& ReadAssetName () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadAssetName();
		}

		return AssetName;
	}

	inline DString GetAssetType () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetAssetType();
		}

		return AssetType;
	}

	inline const DString& ReadAssetType () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadAssetType();
		}

		return AssetType;
	}

	inline bool IsGlobalAsset () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->IsGlobalAsset();
		}

		return bGlobal;
	}

	inline ELoadState GetLoadState () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetLoadState();
		}

		return LoadState;
	}

	inline Directory GetSaveLocation () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetSaveLocation();
		}

		return SaveLocation;
	}

	inline const Directory& ReadSaveLocation () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadSaveLocation();
		}

		return SaveLocation;
	}

	inline const std::vector<GraphVariable*>& ReadMemberVariables () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadMemberVariables();
		}

		return MemberVariables;
	}

	inline std::vector<GraphVariable*>& EditMemberVariables ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditMemberVariables();
		}

		return MemberVariables;
	}

	inline const std::vector<GraphFunction*>& ReadOverrideFunctions () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadOverrideFunctions();
		}

		return OverrideFunctions;
	}

	inline std::vector<GraphFunction*>& EditOverrideFunctions ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditOverrideFunctions();
		}

		return OverrideFunctions;
	}

	inline const std::vector<GraphFunction*>&  ReadMemberFunctions () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadMemberFunctions();
		}

		return MemberFunctions;
	}

	inline std::vector<GraphFunction*>& EditMemberFunctions ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditMemberFunctions();
		}

		return MemberFunctions;
	}

	inline const std::vector<SVarOverride>& ReadDefaultOverrides () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadDefaultOverrides();
		}

		return DefaultOverrides;
	}

	inline std::vector<SVarOverride>& EditDefaultOverrides ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditDefaultOverrides();
		}

		return DefaultOverrides;
	}

	inline ContentClass* GetScriptClass () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetScriptClass();
		}

		return ScriptClass;
	}

	inline GraphAsset* GetSuperAsset () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetSuperAsset();
		}

		return SuperAsset.Get();
	}

	inline DString GetAuthors () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetAuthors();
		}

		return Authors;
	}

	inline const DString& ReadAuthors () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadAuthors();
		}

		return Authors;
	}

	inline DString& EditAuthors ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditAuthors();
		}

		return Authors;
	}

	inline DString GetLicense () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetLicense();
		}

		return License;
	}

	inline const DString& ReadLicense () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadLicense();
		}

		return License;
	}

	inline DString& EditLicense ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditLicense();
		}

		return License;
	}

	inline DString GetComment () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetComment();
		}

		return Comment;
	}

	inline const DString& ReadComment () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadComment();
		}

		return Comment;
	}

	inline DString& EditComment ()
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->EditComment();
		}

		return Comment;
	}

	inline bool IsAbstract () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->IsAbstract();
		}

		return bAbstract;
	}

	inline Vector2 GetDefaultFunctionSize () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->GetDefaultFunctionSize();
		}

		return DefaultFunctionSize;
	}

	inline const Vector2& ReadDefaultFunctionSize () const
	{
		if (AssetDefinition != nullptr)
		{
			return AssetDefinition->ReadDefaultFunctionSize();
		}

		return DefaultFunctionSize;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes the given ItemTemplate in a way it contains components that can configure this asset's inherited functions.
	 */
	virtual void InitItemTemplateForInheritedFunctions (InspectorList& outList);

	/**
	 * Reads from the given config file to load essential data.
	 * Returns true if successful.
	 */
	virtual bool ProcessLoadEssentials (ConfigWriter* config);

	/**
	 * Reads from the given config file to load everything about this asset.
	 * Returns true if successful.
	 */
	virtual bool ProcessLoadEverything (ConfigWriter* config);

	/**
	 * Copies the member and inherited variables from the given asset and replaces this asset's variables with the new list.
	 */
	virtual void CopyVarsFrom (GraphAsset* cpyFrom);

	/**
	 * Replaces the contents of this asset's variables with the data found in the given EditableArray.
	 */
	virtual void SyncVarArray (EditableArray* editArray);

	/**
	 * Replaces the contents of the asset's inherited functions with the data found in the given EditableArray.
	 */
	virtual void SyncInheritedFunctionArray (EditableArray* editArray);

	/**
	 * Updates the contents of the given EditPropertyComponent to reflect the OverrideFunction's data.
	 * This function assumes the given EditPropertyComponent is an element array of a struct.
	 * @param funcIdx The index to the OverrideFunctions vector.
	 * @param syncDropdown If true, then it'll iterate through each inherited function to find the one with the matching name. Once found, it'll update the dropdown component. False will skip that process.
	 */
	virtual void SyncInheritedFunctionArrayElement (EditPropertyComponent* arrayElement, size_t funcIdx, bool syncDropdown);

	/**
	 * Replaces the contents of this assets's functions with the data found in the EditableArray.
	 */
	virtual void SyncFunctionArray (EditableArray* editArray);

	/**
	 * Replaces the contents of this asset's DefaultOverrides with the data found in the given EditableArray.
	 */
	virtual void SyncDefaultOverrideArray (EditableArray* editArray);

	/**
	 * Iterates through all assets loaded in the local editor engine, and any asset equal or subclass of this asset will be added to the resulting vector.
	 */
	virtual void FindAllSubclassAssets (std::vector<GraphAsset*>& outAssets) const;

	/**
	 * Assuming that the given field is a component that configures a member variable, this function will climb up the component chain
	 * to obtain the associated GraphVariable that's being configured.
	 * Since this is accessing a variable reference inside a vector it's important to not modify the MemberVariable list before accessing the pointer.
	 */
	virtual GraphVariable* FindEditedVar (EditPropertyComponent* editComp);

	/**
	 * Assuming that the given field is a component that configures a function, this function will climb up the component chain
	 * to obtain the associated MemberFunction that's being configured.
	 */
	virtual GraphFunction* FindEditedFunction (EditPropertyComponent* editComp);

	/**
	 * Checks the given edit field to ensure there are no name duplication between this asset's member variables and this field's contents.
	 * If there is an error, it'll display an error message around the given field.
	 */
	virtual void CheckForNameConflict (EditableField* editField);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleInitializeMemberVars (EditableArray* editArray);
	virtual void HandleInitializeInheritedFunctions (EditableArray* editArray);
	virtual void HandleInitializeVarOverrides (EditableArray* editArray);

	virtual void HandleAssetNameEdit (EditPropertyComponent* editedComp);
	virtual void HandlePopulateClassList (DropdownComponent* dropdown);
	virtual void HandleSelectParentClass (EditPropertyComponent* editedComp);
	virtual void HandleToggleGlobal (EditPropertyComponent* editedComp);
	virtual void HandleToggleAbstract (EditPropertyComponent* editedComp);
	virtual void HandlePopulateOverrideVariables (DropdownComponent* dropdown);
	virtual void HandleSelectVariableOverride (EditPropertyComponent* editedEnum);

	virtual void HandleMemVarNameChanged (const DString& prevId, const DString& varId, const DString& newName);
	virtual void HandleVarArrayEdit (EditPropertyComponent* editedComp);
	virtual void HandleMemVarDeleted (EditorInterface* obj);
	virtual void HandleVarInstanceDeleted (EditorInterface* obj);

	virtual void HandleMemFunctionDeleted (EditorInterface* obj);

	virtual void HandlePopulateInheritedFunctions (DropdownComponent* dropdown);
	virtual void HandleParentFunctionSelected (EditPropertyComponent* editedComp);
	virtual void HandleOpenInheritedFunction (ButtonComponent* button, EditablePropCommand* propCmd);
	virtual void HandleInheritedFunctionArrayEdit (EditPropertyComponent* editedComp);

	virtual void HandleDefaultOverrideArrayEdit (EditPropertyComponent* editedComp);
	virtual void HandleOverrideFunctionDeleted (EditorInterface* obj);

	friend class EditorEngineComponent;
};
SD_END