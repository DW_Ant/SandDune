/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorEngineComponent.h
  Loads essential resources needed to render editable properties.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EditorTheme;
class EditPropertyComponent;
class GraphAsset;
class GraphFunction;
class EditorInterface;

#ifdef WITH_MULTI_THREAD
class TaskMessenger;
#endif

class EDITOR_API EditorEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(EditorEngineComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SNameRegistry
	{
	public:
		DString EditorName;
		std::vector<EditorInterface*> EditorObjects;

		SNameRegistry (const DString& inEditorName);
		Int GetNextId ();

	private:
		/* Latest ID that was given to an object. */
		Int LatestId;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The name of the localization file to use when displaying human readable text. */
	static const DString LOCALIZATION_FILE_NAME;

	/* Delegate that broadcasts whenever the application should update the ReferenceUpdater to replace the strings in the content files.
	The ReferenceUpdater is handled in the application instead of the Editor module since it's up to application to determine how it
	wants to present progress bars, and provide feedback to the user in its status.
	In addition to that, this saves the trouble of requiring the editor to register delegates to every variable, function, and asset. */
	MulticastDelegate<const DString& /*oldName*/, const DString& /*newName*/> OnInvokeRefUpdater;

	/* Delegate that broadcasts whenever the user wants to open a function graph. It's up to the application instead of the editor to react how it should open
	since the application should determine how it should open it (eg: replace existing viewport, open another window, add a tab, prompt the user to save previous function, etc...) */
	MulticastDelegate<GraphFunction* /*func*/> OnOpenFunction;

protected:
	EditorTheme* UiTheme;

	/* List of all asset names. This include assets that are loaded and not loaded. To populate this, use the AssetCatalogue to generate a list of strings
	then notify the EditorEngineComponent to append or replace the catalogue with its results.
	Results may vary based on the editor's asset type and directory preferences. */
	std::vector<FileAttributes> AssetCatalogue;

	/* List of all GraphAssets that have been loaded from an ini files. Only partial or fully loaded assets are referenced here.
	The key of this map is equal to the asset's name. */
	std::unordered_map<HashedString, GraphAsset*> LoadedAssets;

	/* List of all EditorInstances that were instantiated. The key of this map is equal to the hash of the editor object's name.
	It's a pointer to the SNameRegistry to avoid copying large chunks of memory when resizing the map.*/
	std::unordered_map<HashedString, SNameRegistry*> EditorObjects;

#ifdef WITH_MULTI_THREAD
	/* The object responsible for communicating tasks between threads. */
	TaskMessenger* WorkerMessenger;
#endif


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditorEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * If the given asset is partial or fully loaded, then this asset can be registered to this engine component assuming it doesn't collide with a different asset.
	 * Returns true if it was added to the map.
	 */
	virtual bool RegisterAsset (GraphAsset* newAsset);

	/**
	 * Attempts to obtain an asset reference that matches the given name. If the asset doesn't exist, then it'll search for that asset
	 * name in the catalogue. If it does exist in the catalogue, it'll partially load that asset and register it to this engine component
	 * before it finally returns it. Returns nullptr if it's unable to find the asset name or if it errored when partially loading it.
	 */
	virtual GraphAsset* ObtainOrLoadAsset (const HashedString& assetName);

	/**
	 * Removes the specified asset from the map. Returns true if the asset was found and removed.
	 * This will not destroy the asset instance.
	 */
	virtual bool RemoveAsset (GraphAsset* target);

	virtual void AddEditorObject (EditorInterface* newObj, HashedString& outRegisteredHash);
	virtual void RemoveEditorObject (EditorInterface* target, const HashedString& registeredHash);

	/**
	 * Searches through the object registry in the hopes to find an EditorObject with the matching name and EditorId.
	 * If caseComparison is case sensitive, it'll only search through the name registry with the exact spelling.
	 * If caseComparison is insensitive, it'll prioritize the search on the exact match. If it's not found, then it'll linearly search through all name registries.
	 *
	 * If a registry is found, it'll iterate through the object list to find the instance with the matching editorId.
	 */
	virtual EditorInterface* FindEditorObject (const DString& baseName, Int expectedId, DString::ECaseComparison caseComparison) const;

	/**
	 * Replaces the catalogue with the given one.
	 */
	virtual void ReplaceAssetCatalogue (const std::vector<FileAttributes>& newAssetCatalogue);

	/**
	 * Adds to the catalogue with the given new items.
	 * This function will not add duplicates.
	 */
	virtual void AppendAssetCatalogue (const std::vector<FileAttributes>& additionalAssets);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EditorTheme* GetUiTheme () const
	{
		return UiTheme;
	}

	inline const std::vector<FileAttributes> ReadAssetCatalogue () const
	{
		return AssetCatalogue;
	}

	inline const std::unordered_map<HashedString, GraphAsset*>& ReadLoadedAssets () const
	{
		return LoadedAssets;
	}

#ifdef WITH_MULTI_THREAD
	inline TaskMessenger* GetWorkerMessenger () const
	{
		return WorkerMessenger;
	}
#endif


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportEditorResources ();
	virtual void CreateCoreAssets ();

#ifdef WITH_MULTI_THREAD
	/**
	 * Launches a separate thread that may assist the Editor with time consuming tasks without having to freeze the main thread.
	 * If running the editor without the Multi-Thread module, then the developer can directly reference the classes that typically resides in the worker thread.
	 * For example: the developer can instantiate the AssetCatalogue on the editor thread to handle the discovery process synchronously.
	 *
	 * This function should execute on the thread where the EditorEngineComponent resides.
	 */
	virtual void InitializeWorkerThread ();
#endif
};
SD_END