/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BaseStructElement.h
  The base abstract class that represents an editable property for a data struct.

  This class defines the functions the EditableStruct may call into for comparisons
  and assignments. This isn't a template class to prevent the EdtiableStruct from becoming
  a template class.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class EditableStruct;
class EditPropertyComponent;

class EDITOR_API BaseStructElement
{


	/*
	=====================
	  Properties
	=====================
	*/
	
protected:
	/* Name of the property to be edited. */
	DString PropName;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	BaseStructElement (const DString& inPropName);


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initialize an EditPropertyComponent instance that is bound to this struct property.
	 */
	virtual EditPropertyComponent* SetupPropComp (EditableStruct* attachTo) const = 0;

	/**
	 * Assigns the value of this property equal to its default value.
	 */
	virtual void ResetToDefault () = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetPropName () const
	{
		return PropName;
	}

	inline const DString& ReadPropName () const
	{
		return PropName;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Modifies the given EditPropertyComponent to assign common variables to it.
	 */
	virtual void InitializePropComponent (EditableStruct* attachTo, EditPropertyComponent* propComp) const;
};
SD_END