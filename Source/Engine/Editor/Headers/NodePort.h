/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NodePort.h
  A GuiComponent that enables developers to connect nodes together.

  A NodePort could be treated as an input paramater or an output connection
  of a node.

  This component assumes that this resides inside a GraphEditor in order to maintain
  drag port states between other NodePorts.
=====================================================================
*/

#pragma once

#include "Editor.h"

SD_BEGIN
class GraphEditor;

class EDITOR_API NodePort : public GuiComponent
{
	DECLARE_CLASS(NodePort)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EPortType
	{
		PT_Input, //This port is an input port. It can only accept connections from output lines (can't connect input-to-input).
		PT_Output, //This port is an output port. It can only accept connections from input lines (can't connect output-to-output).
		PT_Any //Lines connected to this port can be interpretted as input or output (used for ports such as reroute nodes).
	};
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate broadcasted whenever this port's absolute transform has changed, and any CurveComponents attached to this port may need to recompute its end points.
	This is only broadcasted if it has at least one connection. */
	SDFunction<void, NodePort* /*changedPort*/> OnRefreshCurveConnections;

	/* Delegate invoked whenever this port add or removed a connection. */
	SDFunction<void, NodePort* /*changedPort*/> OnConnectionChanged;

protected:
	/* Enum that determines what kind of connections can enter. */
	EPortType PortType;

	/* If true, then multiple connections can be joined with this port. Otherwise, the other connection will be severed as soon as a new one is made. */
	bool bAcceptsMultiConnections;

	/* Color multiplier to apply to the sprite component. */
	Color TextureColor;

	/* Sprite responsible for rendering the port. The texture for this sprite may want to be in gray scale since a color multiplier will be applied to this port.
	In addition to that, the texture is expected to support the following subdivisions for the various render states:
	Default Disconnected | Default Connected
	Hovered Disconnected | Hovered Connected
	Pressed Disconnected | Pressed Connected
	
	Disconnected implies that there's nothing connected to this port.*/
	DPointer<SpriteComponent> SpriteComp;

	/* List of ports this is connected to. */
	std::vector<NodePort*> Connections;

	/* Determines how much this port's absolute transform must change in order for it to invoke its CurveConnections. */
	Float RefreshConnectionThreshold;

	/* The last abs location when the OnRefreshCurveConnections delegate was invoked. */
	Vector2 LastCurveRefreshPos;

	/* Reference to the GraphEditor this component resides in. The GraphEditor maintains the dragged port reference to establish new connections. */
	GraphEditor* OwningEditor;

private:
	/* Becomes true if the developer is currently hovering over this port. */
	bool bIsHovered;

	/* Becomes true if the developer pressed down over this port. */
	bool bIsPressed;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	virtual void PostAbsTransformUpdate () override;

protected:
	virtual void InitializeComponents () override;

	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;

	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this port can accept a connection to the other port.
	 * If bCheckOtherPort is true, it'll ensure the other port also accept connections from this port.
	 */
	virtual bool CanAcceptConnectionsFrom (const NodePort* otherPort, bool bCheckOtherPort) const;

	/**
	 * Performs a connection between this port and the other port. Most cases would simply just connect the two.
	 * There may be some cases where ports may have to insert nodes between them (eg: converting variables from one type to another).
	 */
	virtual void EstablishConnectionTo (NodePort* otherPort);

	/**
	 * Detaches this port from the given connection.
	 */
	virtual void SeverConnectionFrom (NodePort* otherPort);
	virtual void SeverAllConnections ();

	/**
	 * Recursively iterates through all connections to populate the outMatches vector. Each match must also equal to specified connection type.
	 * If it's connected to a reroute node, it'll follow the route until it finds the input ports.
	 * The checked ports vector is populated based on the ports that were found. This is also used to prevent infinite recursions.
	 */
	virtual void GetConnectedPortsOfMatchingType (std::vector<NodePort*>& outMatches, EPortType reqType, std::vector<NodePort*>& outCheckedPorts) const;

	/**
	 * Returns true if this port can connect to the same reroute the other port is also connected to.
	 */
	virtual bool CanBeGroupedWith (NodePort* otherPort) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPortType (EPortType newPortType);
	virtual void SetAcceptMultiConnections (bool newAcceptMultiConnections);
	virtual void SetTextureColor (Color newTextureColor);
	virtual void SetRefreshConnectionThreshold (Float newRefreshConnectionThreshold);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline EPortType GetPortType () const
	{
		return PortType;
	}

	inline bool AcceptsMultiConnections () const
	{
		return bAcceptsMultiConnections;
	}

	inline Color GetTextureColor () const
	{
		return TextureColor;
	}

	inline SpriteComponent* GetSpriteComp () const
	{
		return SpriteComp.Get();
	}

	inline const std::vector<NodePort*>& ReadConnections () const
	{
		return Connections;
	}

	inline Float GetRefreshConnectionThreshold () const
	{
		return RefreshConnectionThreshold;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if there's a direct or indirect connection between this port and the other port.
	 * An indirect connection would be a chained reroute nodes together. This function will not consider connections going through nodes (eg: through an function node).
	 * The checked ports vector is used to list out nodes that were already checked for connections (used to avoid infinite recursions).
	 */
	virtual bool IsConnectedTo (const NodePort* otherPort, std::vector<NodePort*>& outCheckedPorts) const;

	/**
	 * Updates the sprite's render state based on the condition of this node.
	 */
	virtual void UpdateSpriteRender ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTextureChanged ();
};
SD_END