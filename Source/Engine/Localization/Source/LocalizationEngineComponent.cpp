/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LocalizationEngineComponent.cpp
=====================================================================
*/

#include "LocalizationEngineComponent.h"
#include "TextTranslator.h"

IMPLEMENT_ENGINE_COMPONENT(SD::LocalizationEngineComponent)
SD_BEGIN

const Directory LocalizationEngineComponent::LOCALIZATION_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Localization"));

void LocalizationEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Translator = TextTranslator::CreateObject();
	if (Translator.IsNullptr())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate a text translator object.  Text will not be translated."));
	}
}

void LocalizationEngineComponent::ShutdownComponent ()
{
	if (Translator.IsValid())
	{
		Translator->Destroy();
		Translator = nullptr;
	}

	Super::ShutdownComponent();
}

#ifdef DEBUG_MODE
void LocalizationEngineComponent::ProcessEngineIntegrityTestStep (EEngineIntegrityStep testStep)
{
	Super::ProcessEngineIntegrityTestStep(testStep);

	if (testStep == EIS_PreCleanup || testStep == EIS_PreInstantiation)
	{
		/*
		Need to clear the translated files from the TextTranslator. The problem is that whenever the test instantiate various UI objects, those objects
		are very likely to reference translation files for its displayed text. Those files are cached in order to be repurposed for other objects that may need translations.
		However those cached files would be misinterpreted as a memory leak from the unit test since it did not get destroyed when the instigating object was destroyed.

		The component also clears the cache on pre instantiation to handle cases where the translation file was created before the test even began.
		The test is going to clear the translation files right before clean up, causing that extra translation file to be removed. This extra removal would cause the test to misinterpret the num objects.
		To have the two vectors sync up at the end of the test, the cached translation files are cleared before and after the instantiation test.
		*/
		if (Translator != nullptr)
		{
			Translator->ClearCachedFiles();
		}
	}
}
#endif

TextTranslator* LocalizationEngineComponent::GetTranslator () const
{
	return Translator.Get();
}
SD_END