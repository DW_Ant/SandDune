/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LocalizationTester.cpp
=====================================================================
*/

#include "LocalizationClasses.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD::LocalizationTester, SD::UnitTester)
SD_BEGIN

bool LocalizationTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult = (TestTextTranslator(testFlags));
	}

	if (bResult && (testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_NeverFails) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult = TestUnicode(testFlags);
	}

	return bResult;
}

bool LocalizationTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	if (!Super::MetRequirements(completedTests))
	{
		return false;
	}

	//File module testing is required
	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (FileUnitTester::SStaticClass()->GetDefaultObject() == completedTests.at(i))
		{
			return Super::MetRequirements(completedTests);
		}
	}

	return false;
}

bool LocalizationTester::TestTextTranslator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Text Translator"));

	TestLog(testFlags, TXT("The engine's localization directory is:  %s"), LocalizationEngineComponent::LOCALIZATION_DIRECTORY.ReadDirectoryPath());
	TestLog(testFlags, TXT("The localization files' file extension is:  ") + DString(LOCALIZATION_EXT));

	TextTranslator* translator = LocalizationEngineComponent::Find()->GetTranslator();
	if (translator == nullptr)
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Unable to get a handle to the translator from LocalizationEngineComponent."));
		return false;
	}

	std::vector<DString> engAnswerKey;
	engAnswerKey.push_back(TXT("Translated to English."));
	engAnswerKey.push_back(TXT("Testing various \"Characters\" such as ; semicolon, and commas"));
	engAnswerKey.push_back(TXT("Testing multiple sections within .loc"));

	std::vector<DString> engArrayAnswerKey;
	engArrayAnswerKey.push_back(TXT("Value1"));
	engArrayAnswerKey.push_back(TXT("Value2"));
	engArrayAnswerKey.push_back(TXT("Value3"));

	std::vector<DString> xxxAnswerKey;
	xxxAnswerKey.push_back(TXT("Translated to Debugging language."));
	xxxAnswerKey.push_back(TXT("XXXXXXXX"));
	xxxAnswerKey.push_back(TXT("XXXXXXXX"));

	TextTranslator::ELanguages oldLanguage = translator->GetSelectedLanguage();
	TestLog(testFlags, TXT("Setting current language to English"));
	translator->SelectLanguage(TextTranslator::English);

	DString key1 = translator->TranslateText(TXT("Key1"), TXT("UnitTests"), TXT("SectionA"));
	TestLog(testFlags, TXT("Translated Key1 to \"%s\""), key1);
	if (key1 != engAnswerKey.at(0))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key1 within SectionA does not match expected value \"%s\".  Instead it translated to \"%s\"."), engAnswerKey.at(0), key1);
		return false;
	}

	DString key2 = translator->TranslateText(TXT("Key2"), TXT("UnitTests"), TXT("SectionB"));
	TestLog(testFlags, TXT("Translated Key2 to \"%s\""), key2);
	if (key2 != engAnswerKey.at(1))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key2 within SectionB does not match expected value \"%s\".  Instead it translated to \"%s\"."), engAnswerKey.at(1), key2);
		return false;
	}

	DString key3 = translator->TranslateText(TXT("Key3"), TXT("UnitTests"), TXT("SectionC"));
	TestLog(testFlags, TXT("Translated Key3 to \"%s\""), key3);
	if (key3 != engAnswerKey.at(2))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key3 within SectionC does not match expected value \"%s\".  Instead it translated to \"%s\"."), engAnswerKey.at(2), key3);
		return false;
	}

	TestLog(testFlags, TXT("Retrieving array from english localization."));
	std::vector<DString> engArray;
	translator->GetTranslations(TXT("ArrayKey"), engArray, TXT("UnitTests"), TXT("SectionD"));
	if (engArray.size() != engArrayAnswerKey.size())
	{
		UnitTestError(testFlags, TXT("Text translator test failed.  Translated text for ArrayKey should have returned %s results.  Instead it found %s results."), Int(engArrayAnswerKey.size()), Int(engArray.size()));
		return false;
	}

	for (UINT_TYPE i = 0; i < engArray.size(); i++)
	{
		TestLog(testFlags, TXT("    [%s] = \"%s\""), Int(i), engArray.at(i));
		if (engArray.at(i) != engArrayAnswerKey.at(i))
		{
			UnitTestError(testFlags, TXT("Text translator test failed.  Translated ArrayKey element %s within SectionD does not match the expected value \"%s\".  Instead it translated to \"%s\"."), Int(i), engArrayAnswerKey.at(i), engArray.at(i));
			return false;
		}
	}

	TestLog(testFlags, TXT("Setting current language to XXX"));
	translator->SelectLanguage(TextTranslator::Debugging);

	key1 = translator->TranslateText(TXT("Key1"), TXT("UnitTests"));
	TestLog(testFlags, TXT("Translated Key1 to \"%s\""), key1);
	if (key1 != xxxAnswerKey.at(0))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key1 does not match expected value \"%s\".  Instead it translated to \"%s\"."), xxxAnswerKey.at(0), key1);
		return false;
	}

	key2 = translator->TranslateText(TXT("Key2"), TXT("UnitTests"));
	TestLog(testFlags, TXT("Translated Key2 to \"%s\""), key2);
	if (key2 != xxxAnswerKey.at(1))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key2 does not match expected value \"%s\".  Instead it translated to \"%s\"."), xxxAnswerKey.at(1), key2);
		return false;
	}

	key3 = translator->TranslateText(TXT("Key3"), TXT("UnitTests"));
	TestLog(testFlags, TXT("Translated Key3 to \"%s\""), key3);
	if (key3 != xxxAnswerKey.at(2))
	{
		UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key3 does not match expected value \"%s\".  Instead it translated to \"%s\"."), xxxAnswerKey.at(2), key3);
		return false;
	}

	std::vector<DString> debugArray;
	translator->GetTranslations(TXT("ArrayKey"), debugArray, TXT("UnitTests"));
	if (debugArray.size() != engArray.size())
	{
		UnitTestError(testFlags, TXT("Text translator test failed.  The expected size for ArrayKey within debugging localization is %s.  Instead it found %s translations."), Int(engArray.size()), Int(debugArray.size()));
		return false;
	}

	TestLog(testFlags, TXT("Translated the following text from debugging localization file. . ."));
	for (UINT_TYPE i = 0; i < debugArray.size(); i++)
	{
		TestLog(testFlags, TXT("    [%s] = \"%s\""), Int(i), debugArray.at(i));
		if (debugArray.at(i).IsEmpty())
		{
			UnitTestError(testFlags, TXT("Text translator test failed.  The translator is expected to find some text within the debugging localization array.  Instead it found an empty entry."));
			return false;
		}

		if (debugArray.at(i) == engArray.at(i))
		{
			UnitTestError(testFlags, TXT("Text translator test failed.  The debugging array and english array are expected to contain different results.  Instead they both returned %s at index %s."), debugArray.at(i), Int(i));
			return false;
		}
	}

	TestLog(testFlags, TXT("Restoring current language to defaults."));
	translator->SelectLanguage(oldLanguage);

	ExecuteSuccessSequence(testFlags, TXT("Text Translator"));
	return true;
}

bool LocalizationTester::TestUnicode (EUnitTestFlags testFlags) const
{
#if USE_UTF32 | USE_UTF16 | USE_UTF8
	BeginTestSequence(testFlags, TXT("Unicode Test"));

	TestLog(testFlags, TXT("The Unicode test will display various strings.  The text translator should be able to pull text from various languages, and should be displayed correctly in Engine."));
	std::vector<DString> testsToRun;
	testsToRun.push_back(TXT("DanishTest"));
	testsToRun.push_back(TXT("GermanTest1"));
	testsToRun.push_back(TXT("GermanTest2"));
	testsToRun.push_back(TXT("GermanTest3"));
	testsToRun.push_back(TXT("GreekTest1"));
	testsToRun.push_back(TXT("GreekTest2"));
	testsToRun.push_back(TXT("EnglishTest"));
	testsToRun.push_back(TXT("SpanishTest"));
	testsToRun.push_back(TXT("FrenchTest1"));
	testsToRun.push_back(TXT("FrenchTest2"));
	testsToRun.push_back(TXT("FrenchTest3"));
	testsToRun.push_back(TXT("IrishTest"));
	testsToRun.push_back(TXT("HungarianTest"));
	testsToRun.push_back(TXT("IcelandicTest1"));
	testsToRun.push_back(TXT("IcelandicTest2"));
	testsToRun.push_back(TXT("JapaneseTest1"));
	testsToRun.push_back(TXT("JapaneseTest2"));
	testsToRun.push_back(TXT("HebrewTest"));
	testsToRun.push_back(TXT("PolishTest"));
	testsToRun.push_back(TXT("RussianTest1"));
	testsToRun.push_back(TXT("RussianTest2"));
	testsToRun.push_back(TXT("Turkishtest"));

	TextTranslator* translator = LocalizationEngineComponent::Find()->GetTranslator();
	CHECK(translator != nullptr)

	for (UINT_TYPE i = 0; i < testsToRun.size(); i++)
	{
		DString translatedText = translator->TranslateText(testsToRun.at(i), TXT("UnitTests"), TXT("UnicodeTest"));
		TestLog(testFlags, TXT("[%s]%s=%s"), Int(i), testsToRun.at(i), translatedText);

		if (translatedText.IsEmpty() || !translatedText.IsValidEncoding())
		{
			UnitTestError(testFlags, TXT("Failed to extract text from localization file."));
			return false;
		}
	}

	ExecuteSuccessSequence(testFlags, TXT("Unicode Test"));
	return true;
#else
	TestLog(testFlags, TXT("Current configuration doesn't encode the strings in Unicode.  Skipping LocalizationTester::TestUnicode test."));
	return true;
#endif
}
SD_END

#endif