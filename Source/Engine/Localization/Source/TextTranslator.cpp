/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextTranslator.cpp
=====================================================================
*/

#include "LocalizationEngineComponent.h"
#include "TextTranslator.h"

IMPLEMENT_CLASS(SD::TextTranslator, SD::Object)
SD_BEGIN

void TextTranslator::InitProps ()
{
	Super::InitProps();

	SelectedLanguage = English;

	SLanguageMapping newLanguageMapping;
	newLanguageMapping.Language = English;
	newLanguageMapping.TextSuffix = TXT("-ENG");
	newLanguageMapping.FriendlyName = TXT("English");
	LanguageMappings.push_back(newLanguageMapping);

	newLanguageMapping.Language = Debugging;
	newLanguageMapping.TextSuffix = TXT("-XXX");
	newLanguageMapping.FriendlyName = TXT("Debugging");
	LanguageMappings.push_back(newLanguageMapping);
}

void TextTranslator::BeginObject ()
{
	Super::BeginObject();

	SelectDefaultLanguage();
}

void TextTranslator::Destroyed ()
{
	ClearCachedFiles();

	Super::Destroyed();
}

TextTranslator* TextTranslator::GetTranslator ()
{
	return LocalizationEngineComponent::Find()->GetTranslator();
}

DString TextTranslator::TranslateText (const DString& localizationKey, const DString& baseFileName, const DString& sectionName) const
{
	DString fileName = GetFullFileFormat(baseFileName);
	ConfigWriter* curFile = GetFileReader(fileName);
	DString result = DString::EmptyString;

	if (curFile == nullptr || !curFile->IsFileOpened())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
	}
	else
	{
		result = curFile->GetPropertyText(sectionName, localizationKey);
	}

	if (result.IsEmpty())
	{
		//Key is not translated for current language.
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
#ifdef DEBUG_MODE
		//Manipulate the resulting string to display the error message since there's a good chance this string will appear on screen in some way.
		result = DString::CreateFormattedString(TXT("Missing Translation in file (%s), section (%s), key (%s)"), baseFileName, sectionName, localizationKey);
#endif
	}

	return result;
}

void TextTranslator::GetTranslations (const DString& localizationKey, std::vector<DString>& outTranslations, const DString& baseFileName, const DString& sectionName) const
{
	DString fileName = GetFullFileFormat(baseFileName);
	ConfigWriter* curFile = GetFileReader(fileName);

	if (curFile == nullptr || !curFile->IsFileOpened())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
	}
	else
	{
		curFile->GetArrayValues<DString>(sectionName, localizationKey, outTranslations);
	}

	if (outTranslations.size() <= 0)
	{
		//Key is not translated for current language.
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
	}
}

void TextTranslator::SelectLanguage (ELanguages newLanguage)
{
	if (newLanguage == SelectedLanguage)
	{
		return;
	}

	ClearCachedFiles();

	for (UINT_TYPE i = 0; i < LanguageMappings.size(); i++)
	{
		if (LanguageMappings.at(i).Language == newLanguage)
		{
			SelectedLanguageMappingIdx = i;
			SelectedLanguage = newLanguage;
			return;
		}
	}

	LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to set language since there isn't a language mapping associated with specified language."));
}

void TextTranslator::ClearCachedFiles ()
{
	for (ConfigWriter* writer : TranslationFiles)
	{
		writer->Destroy();
	}
	ContainerUtils::Empty(OUT TranslationFiles);
}

TextTranslator::ELanguages TextTranslator::GetSelectedLanguage () const
{
	return SelectedLanguage;
}

DString TextTranslator::GetSelectedLanguageName () const
{
	return LanguageMappings[SelectedLanguageMappingIdx].FriendlyName;
}

void TextTranslator::GetLanguageMappings (std::vector<SLanguageMapping>& outLanguageMappings) const
{
	outLanguageMappings = LanguageMappings;
}

TextTranslator::SLanguageMapping TextTranslator::GetSelectedLanguageData () const
{
	if (SelectedLanguageMappingIdx != UINT_INDEX_NONE)
	{
		return LanguageMappings[SelectedLanguageMappingIdx];
	}

	LocalizationLog.Log(LogCategory::LL_Warning, TXT("Failed to get the selected language data since SelectedLanguageMappingIdx variable is not yet set."));
	return SLanguageMapping();
}

DString TextTranslator::GetFullFileFormat (const DString& baseFileName) const
{
	if (baseFileName.IsEmpty())
	{
		return LocalizationEngineComponent::LOCALIZATION_DIRECTORY.ReadDirectoryPath() + ProjectName + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
	}

	return LocalizationEngineComponent::LOCALIZATION_DIRECTORY.ReadDirectoryPath() + baseFileName + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
}

void TextTranslator::SelectDefaultLanguage ()
{
	SelectLanguage(DEFAULT_LANGUAGE);
}

ConfigWriter* TextTranslator::GetFileReader (const DString& fullFileName) const
{
	for (ConfigWriter* writer : TranslationFiles)
	{
		if (writer->GetCurrentFileName().Compare(fullFileName, DString::CC_CaseSensitive) == 0)
		{
			return writer;
		}
	}

	ConfigWriter* newReader = ConfigWriter::CreateObject();
	newReader->SetReadOnly(true);
	newReader->OpenFile(fullFileName, false);
	TranslationFiles.push_back(newReader);

	return newReader;
}
SD_END