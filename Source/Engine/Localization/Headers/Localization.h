/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Localization.h
  Contains important file includes and definitions for the Localization module.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef LOCALIZATION_EXPORT
		#define LOCALIZATION_API __declspec(dllexport)
	#else
		#define LOCALIZATION_API __declspec(dllimport)
	#endif
#else
	#define LOCALIZATION_API
#endif

/* Directory location relative to base directory where Localization files may be located. */
#define LOCALIZATION_LOCATION "Localization"

/* LocalizationExt is the file extension all localization files use.  Although they're the same as config files,
they use a different extension to notify the user that this is not meant for edits unless they plan to mod the system. */
#define LOCALIZATION_EXT ".loc"

SD_BEGIN
extern LogCategory LOCALIZATION_API LocalizationLog;
SD_END