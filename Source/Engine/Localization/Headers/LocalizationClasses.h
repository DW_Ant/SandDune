/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LocalizationClasses.h
  Contains all header includes for the Localization module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the LocalizationClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#ifdef WITH_LOCALIZATION
#include "Localization.h"
#include "LocalizationEngineComponent.h"
#include "LocalizationTester.h"
#include "TextTranslator.h"

#endif
