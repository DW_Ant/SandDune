/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextTranslator.h
  Object responsible for tracking current selected language, and retrieve
  localized text from identifiers.
=====================================================================
*/

#pragma once

#include "Localization.h"

/**
 * Default language to select whenever this application launches (and if there's no default language to load from an ini).
 * The Language should map to one of the enumerators in ELanguages.
 */
#ifndef DEFAULT_LANGUAGE
#define DEFAULT_LANGUAGE English
#endif

SD_BEGIN
class LOCALIZATION_API TextTranslator : public Object
{
	DECLARE_CLASS(TextTranslator)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ELanguages
	{
		None,
		English,
		Debugging
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/**
	 * Maps a language to an associated text (to associate config files to a specific language.
	 */
	struct SLanguageMapping
	{
	public:
		ELanguages Language;

		/* Text appearance at the end of file name to find files specific to this language.  This does not include extension. */
		DString TextSuffix;

		/* Human-readable text to language. */
		DString FriendlyName;

		SLanguageMapping ()
		{
			Language = None;
			TextSuffix = TXT("");
			FriendlyName = TXT("None");
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	ELanguages SelectedLanguage;

	/* Language mappings to associate config file suffixes with a specific language.  The AssociatedText should be unique to avoid shared configs. */
	std::vector<SLanguageMapping> LanguageMappings;

	/* List of cached files that were opened for this language. This list is purge as soon as the language changes. */
	mutable std::vector<ConfigWriter*> TranslationFiles;

private:
	/* Cached index to LanguageMapping affiliated with the SelectedLanguage. */
	UINT_TYPE SelectedLanguageMappingIdx;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static TextTranslator* GetTranslator ();

	/**
	 * Finds an associated text within the specified section of specified file based on selected language.
	 * localizationKey is the text to search for in the localization file to find associated translated text.
	 * baseFileName is the name of the file without the language suffix and file extension.
	 * sectionName is an optional variable to help narrow down where localizationKey is found.  If not specified, then it'll search the entire file.
	 *
	 * Returns an error message if the translated text is not found.
	 */
	virtual DString TranslateText (const DString& localizationKey, const DString& baseFileName, const DString& sectionName = TXT("")) const;

	/**
	 * Returns all translations of the given localizationKey.  This is needed to translate words with multiple meanings.
	 * For example:  String to bool could return true for text like "True", "Yes", or "On".
	 */
	virtual void GetTranslations (const DString& localizationKey, std::vector<DString>& outTranslations, const DString& baseFileName = TXT(""), const DString& sectionName = TXT("")) const;

	virtual void SelectLanguage (ELanguages newLanguage);

	/**
	 * Destroys all translated files. If there's a need to reference a particular file, the Translator would have to reopen the file to read from its contents.
	 */
	virtual void ClearCachedFiles ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	ELanguages GetSelectedLanguage () const;
	DString GetSelectedLanguageName () const;
	void GetLanguageMappings (std::vector<SLanguageMapping>& outLanguageMappings) const;
	SLanguageMapping GetSelectedLanguageData () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Adds file location before the file name, and appends the language suffix plus file extension.
	 */
	virtual DString GetFullFileFormat (const DString& baseFileName) const;

	/**
	 * Initializes selected language variable.  Defaults to DEFAULT_LANGUAGE.  This is the function
	 * to override when loading a selected language from an ini saved from an earlier session.
	 */
	virtual void SelectDefaultLanguage ();

	/**
	 * Obtains a file handle based on the specified name.
	 * Note:  This function will create a ConfigWriter instance.
	 */
	virtual ConfigWriter* GetFileReader (const DString& fullFileName) const;
};
SD_END