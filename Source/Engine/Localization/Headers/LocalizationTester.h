/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LocalizationTester.h
  Localization module's unit tester.
=====================================================================
*/

#pragma once

#include "Localization.h"

#ifdef DEBUG_MODE

SD_BEGIN
class LOCALIZATION_API LocalizationTester : public UnitTester
{
	DECLARE_CLASS(LocalizationTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual bool TestTextTranslator (EUnitTestFlags testFlags) const;

	/**
	 * Test that'll return true if the tester was able to extract non-English characters from the unit test localization file.
	 */
	virtual bool TestUnicode (EUnitTestFlags testFlags) const;
};
SD_END

#endif