/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  LocalizationEngineComponent.h
  This component is responsible for launching the translator object.
=====================================================================
*/

#pragma once

#include "Localization.h"

SD_BEGIN
class TextTranslator;

class LOCALIZATION_API LocalizationEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(LocalizationEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Directory LOCALIZATION_DIRECTORY;

protected:
	/* The translator responsible for tracking the selected language and translating any keys to localized text. */
	DPointer<TextTranslator> Translator;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void ShutdownComponent () override;

#ifdef DEBUG_MODE
	virtual void ProcessEngineIntegrityTestStep (EEngineIntegrityStep testStep) override;
#endif


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	TextTranslator* GetTranslator () const;
};
SD_END