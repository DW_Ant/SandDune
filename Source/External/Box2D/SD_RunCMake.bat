@echo off
pushd %~dp0

rem Store first parameter to variable architecture
set architecture=%1

IF /I "%architecture%" == "Win64" GOTO :MAKE64BIT
IF /I "%architecture%" == "Win32" GOTO :MAKE32BIT
IF "%architecture%" == "" GOTO :PROMPT

echo Unknown first parameter.  Only acceptable parameters are Win64 or Win32.
GOTO :ENDBATCH

:PROMPT
SET /P Is64Bit=Generate 64 Bit Box2D libraries ([Y]/N)?  "N" generates 32 bit libraries. 
IF /I "%Is64Bit%" == "N" GOTO :MAKE32BIT

:MAKE64BIT

IF NOT EXIST "ProjectFiles\Win64" (
	mkdir "ProjectFiles\Win64"
)

pushd ProjectFiles\Win64
cmake ..\.. -G "Visual Studio 16 2019" -A "x64" -DBOX2D_BUILD_UNIT_TESTS=OFF -DBOX2D_BUILD_TESTBED=OFF -DBOX2D_BUILD_DOCS=off
popd


GOTO :ENDBATCH
:MAKE32BIT

IF NOT EXIST "ProjectFiles\Win32" (
	mkdir "ProjectFiles\Win32"
)

pushd ProjectFiles\Win32
cmake ..\.. -G "Visual Studio 16 2019" -A "Win32" -DBOX2D_BUILD_UNIT_TESTS=OFF -DBOX2D_BUILD_TESTBED=OFF -DBOX2D_BUILD_DOCS=off
popd

:ENDBATCH

pause

popd
@echo on