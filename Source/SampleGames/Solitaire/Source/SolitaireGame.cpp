/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireGame.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::SolitaireGame, SD::Object)
SOLITAIRE_BEGIN

void SolitaireGame::InitProps ()
{
	Super::InitProps();

	DrawStation = nullptr;
	StageStation = nullptr;
	StartTime = -1.f;
}

void SolitaireGame::BeginObject ()
{
	Super::BeginObject();

	std::vector<Card::ESuit> suits;
	suits.push_back(Card::S_Hearts);
	suits.push_back(Card::S_Diamonds);
	suits.push_back(Card::S_Clubs);
	suits.push_back(Card::S_Spades);

	for (Card::ESuit suit : suits)
	{
		for (int i = 1; i < 14; ++i)
		{
			Card* newCard = Card::CreateObject();
			newCard->InitCard(static_cast<Card::ECardValue>(i), suit);
			AllCards.push_back(newCard);
		}
	}

	DrawStation = CardStation::CreateObject();
	DrawStation->SetAnchorLeft(8.f);
	DrawStation->SetAnchorTop(8.f);
	DrawStation->OnEmptyClicked = SDFUNCTION(this, SolitaireGame, HandleEmptyDrawStationClicked, void);
	DrawStation->OnCardReveal = SDFUNCTION_2PARAM(this, SolitaireGame, HandleDrawReveal, void, CardStation*, Card*);
	AllStations.push_back(DrawStation);

	Vector2 cardSize = Card::GetCardSize();

	StageStation = CardStation::CreateObject();
	StageStation->SetAnchorLeft(DrawStation->GetAnchorLeftDist() + 8.f + Card::GetCardSize().X);
	StageStation->SetAnchorTop(DrawStation->GetAnchorTopDist());
	AllStations.push_back(StageStation);

	//Place the victory stations in top right corner
	for (Int i = 0; i < 4; ++i)
	{
		VictoryStation* victoryStation = VictoryStation::CreateObject();
		victoryStation->SetAnchorRight(8.f + (i.ToFloat() * (cardSize.X + 8.f)));
		victoryStation->SetAnchorTop(8.f);
		victoryStation->OnCardAdded = SDFUNCTION_1PARAM(this, SolitaireGame, HandleVictoryCardPlaced, void, Card*);
		AllStations.push_back(victoryStation);
		VictoryStations.push_back(victoryStation);
	}

	//Place central stations in the upper middle
	Float centralVertPos = DrawStation->GetAnchorTopDist() + cardSize.Y + 8.f;
	for (Int i = 0; i < 7; ++i)
	{
		CardStation* centralStation = CardStation::CreateObject();
		centralStation->SetAnchorLeft(8.f + (i.ToFloat() * 24.f) + (i.ToFloat() * cardSize.X));
		centralStation->SetAnchorTop(centralVertPos);
		centralStation->OnCardReveal = SDFUNCTION_2PARAM(this, SolitaireGame, HandleCentralCardReveal, void, CardStation*, Card*);
		AllStations.push_back(centralStation);
		CentralStations.push_back(centralStation);

		CardColumn* centralColumn = CardColumn::CreateObject();
		centralColumn->SetRelativeTo(centralStation);
		CentralColumns.push_back(centralColumn);
	}
}

void SolitaireGame::Destroyed ()
{
	ClearGame();
	for (UINT_TYPE i = 0; i < AllStations.size(); ++i)
	{
		AllStations.at(i)->Destroy();
	}
	ContainerUtils::Empty(AllStations);

	for (UINT_TYPE i = 0; i < CentralColumns.size(); ++i)
	{
		CentralColumns.at(i)->Destroy();
	}
	ContainerUtils::Empty(CentralColumns);

	for (UINT_TYPE i = 0; i < AllCards.size(); ++i)
	{
		AllCards.at(i)->Destroy();
	}

	Super::Destroyed();
}

void SolitaireGame::SetupNewGame ()
{
	CHECK(AllCards.size() >= 29) //Must be enough cards to fill the board
	RandomUtils::ShuffleVector(AllCards);

	for (UINT_TYPE i = 0; i < AllCards.size(); ++i)
	{
		AllCards.at(i)->SetFacingUp(false);
		AllCards.at(i)->SetPositionNear(Vector2(0.f, 0.f));

		VictoryComponent* comp = dynamic_cast<VictoryComponent*>(AllCards.at(i)->FindComponent(VictoryComponent::SStaticClass(), false));
		if (comp != nullptr)
		{
			comp->Destroy();
		}
	}

	//Place cards at central stations from right to left.
	UINT_TYPE drawCardIdx = AllCards.size() - 1;
	for (UINT_TYPE faceUpIdx = 0; faceUpIdx < CentralStations.size(); ++faceUpIdx)
	{
		for (Int i = Int(CentralStations.size()) - 1; i >= faceUpIdx; --i)
		{
			if (i == faceUpIdx)
			{
				CentralColumns.at(i.ToUnsignedInt())->AppendCard(AllCards.at(drawCardIdx));
				AllCards.at(drawCardIdx)->SetFacingUp(true);
			}
			else
			{
				CentralStations.at(i.ToUnsignedInt())->AddCard(AllCards.at(drawCardIdx));
			}

			--drawCardIdx;
		}
	}

	//Place the rest of the cards in drawing station
	for (UINT_TYPE i = 0; i <= drawCardIdx; ++i)
	{
		DrawStation->AddCard(AllCards.at(i));
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	StartTime = localEngine->GetElapsedTime();
}

void SolitaireGame::ClearGame ()
{
	//Empty all stations
	for (CardStation* station : AllStations)
	{
		station->RemoveAllCards();
	}

	//Destroy all columns
	for (ObjectIterator iter(Engine::FindEngine()->GetEntityHashNumber()); iter.SelectedObject; ++iter)
	{
		if (CardColumn* col = dynamic_cast<CardColumn*>(iter.SelectedObject.Get()))
		{
			if (ContainerUtils::FindInVector(CentralColumns, col) == UINT_INDEX_NONE)
			{
				col->Destroy();
			}
			else
			{
				col->RemoveAllCards();
			}
		}
	}
}

bool SolitaireGame::HasWonGame () const
{
	for (UINT_TYPE i = 0; i < VictoryStations.size(); ++i)
	{
		if (!VictoryStations.at(i)->HasCompleteSet())
		{
			return false;
		}
	}

	return true;
}

void SolitaireGame::ExecuteVictorySequence ()
{
	Engine* engine = Engine::FindEngine();
	CHECK(engine != nullptr)

	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	CHECK(solitaireEngine != nullptr)
	solitaireEngine->BeatenGame(engine->GetElapsedTime() - StartTime);

	if (solitaireEngine->GetMainMenu() != nullptr)
	{
		solitaireEngine->GetMainMenu()->PauseScoreboard();
	}

	const Float CardAnimInterval = 0.2f;
	for (UINT_TYPE i = 0; i < AllCards.size(); ++i)
	{
		VictoryComponent* animComp = VictoryComponent::CreateObject();
		if (AllCards.at(i)->AddComponent(animComp))
		{
			animComp->Delay = (Float::MakeFloat(i) * CardAnimInterval);
		}
	}

	SolitaireLog.Log(LogCategory::LL_Log, TXT("The player has won the game!"));
}

void SolitaireGame::HandleEmptyDrawStationClicked ()
{
	//Return all cards from staging to drawing
	CHECK(DrawStation != nullptr && StageStation != nullptr)

	while (true)
	{
		Card* transferCard = StageStation->GetTopCard();
		if (transferCard == nullptr)
		{
			break;
		}

		transferCard->SetFacingUp(false);
		DrawStation->AddCard(transferCard);
		StageStation->RemoveTopCard();
	}
}

void SolitaireGame::HandleDrawReveal (CardStation* station, Card* revealedCard)
{
	CHECK(revealedCard != nullptr)

	//Transfer the card from draw to stage
	StageStation->AddCard(revealedCard);
	DrawStation->RemoveTopCard();
}

void SolitaireGame::HandleCentralCardReveal (CardStation* station, Card* revealedCard)
{
	CHECK(CentralStations.size() == CentralColumns.size())
	for (UINT_TYPE i = 0; i < CentralStations.size(); ++i)
	{
		if (CentralStations.at(i) == station)
		{
			CentralColumns.at(i)->AppendCard(revealedCard);
			station->RemoveTopCard();
			break;
		}
	}
}

void SolitaireGame::HandleVictoryCardPlaced (Card* addedCard)
{
	CHECK(addedCard != nullptr)

	if (addedCard->GetValue() == Card::CV_King && HasWonGame())
	{
		ExecuteVictorySequence();
	}
}
SOLITAIRE_END