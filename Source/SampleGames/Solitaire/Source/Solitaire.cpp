/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Solitaire.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Solitaire.h"
#include "SolitaireEngineComponent.h"

SOLITAIRE_BEGIN
LogCategory SolitaireLog(TXT("Solitaire"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);
SOLITAIRE_END

//Forward function declarations
int BeginSandDune (int numArgs, TCHAR* args[]);
SD::DString ConvertArgsToStr (int numArgs, TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int numArgs;
	//Only using this to get numArgs because of reasons
	LPWSTR *notUsed = CommandLineToArgvW(GetCommandLineW(), &numArgs);
	LocalFree(notUsed);

	TCHAR* args = GetCommandLine();
	return BeginSandDune(numArgs, &args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(numArgs, args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (int numArg, TCHAR* args[])
{
	SD::ProjectName = TXT("Solitaire");

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(ConvertArgsToStr(numArg, args));

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
	std::vector<SD::EngineComponent*> engineComponents;
	engineComponents.push_back(new SD::GraphicsEngineComponent());
	engineComponents.push_back(new SD::LocalizationEngineComponent());
	engineComponents.push_back(new SD::InputEngineComponent());
	engineComponents.push_back(new SD::GuiEngineComponent());
	engineComponents.push_back(new SD::RandomEngineComponent());
	engineComponents.push_back(new SD::Solitaire::SolitaireEngineComponent());

	engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

	return RunMainLoop();
}

/**
 * Converts the given args into a single string variable.
 */
SD::DString ConvertArgsToStr (int numArgs, TCHAR* args[])
{
	SD::DString allArgs = SD::DString::EmptyString;

	for (int i = 0; i < numArgs; ++i)
	{
		allArgs += args[i];
	}

	return allArgs;
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}