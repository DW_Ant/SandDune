/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Card.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::Card, SD::Entity)
SOLITAIRE_BEGIN

const Vector2 Card::SIZE_RATIO(1.f, 1.4f);
Float Card::CardSize = 90.f;
Float Card::MaxAngle = 3.f;
Float Card::MaxShift = 3.f;

void Card::InitProps ()
{
	Super::InitProps();

	SetEnableFractionScaling(false);

	Value = CV_Ace;
	Suit = S_Hearts;
	bFacingUp = false;
	Sprite = nullptr;
}

void Card::BeginObject ()
{
	Super::BeginObject();

	Sprite = SpriteComponent::CreateObject();
	if (AddComponent(Sprite))
	{
		Sprite->SetPivot(Vector2(0.5f, 0.5f));
		Sprite->EditBaseSize() = GetCardSize();
	}
}

DString Card::CardValueToString (int cardValue)
{
	switch (cardValue)
	{
		case (1): return TXT("Ace");
		case (11): return TXT("Jack");
		case (12): return TXT("Queen");
		case (13): return TXT("King");
	}

	return DString::MakeString(cardValue);
}

void Card::SetPositionNear (const Vector2& position)
{
	BasePosition = position;

	Float randX = RandomUtils::RandRange(-MaxShift, MaxShift);
	Float randY = RandomUtils::RandRange(-MaxShift, MaxShift);

	Vector2 newPosition(position);
	newPosition.X += randX;
	newPosition.Y += randY;

	//The positions specified in this function is relative to top left corner.  Offset the position so that
	//The origin is at the center of desired placement.
	newPosition += (GetCardSize() * 0.5f);

	SetPosition(newPosition);

	//Add a random spin
	SetRotation(RandomUtils::RandRange(-MaxAngle, MaxAngle));
}

void Card::ReapplyRandomShifting ()
{
	SetPositionNear(BasePosition);
}

void Card::InitCard (ECardValue inValue, ESuit inSuit)
{
	Value = inValue;
	Suit = inSuit;

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	Sprite->SetSpriteTexture(localTexturePool->GetTexture(HashedString("SampleGames.Solitaire.CardBack")));
}

bool Card::IsRedCard () const
{
	return (Suit == S_Hearts || Suit == S_Diamonds);
}

void Card::SetCardSize (Float newCardSize)
{
	CardSize = newCardSize;
}

void Card::SetMaxAngle (Float newMaxAngle)
{
	MaxAngle = newMaxAngle;
}

void Card::SetMaxShift (Float newMaxShift)
{
	MaxShift = newMaxShift;
}

void Card::SetFacingUp (bool bNewFacingUp)
{
	if (bFacingUp == bNewFacingUp)
	{
		return;
	}

	bFacingUp = bNewFacingUp;

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (Sprite.IsValid() && localTexturePool != nullptr)
	{
		if (!bFacingUp)
		{
			Sprite->SetSpriteTexture(localTexturePool->GetTexture(HashedString("SampleGames.Solitaire.CardBack")));
		}
		else
		{
			DString suitName;
			switch (Suit)
			{
				case (S_Hearts):
					suitName = TXT("Hearts");
					break;

				case (S_Clubs):
					suitName = TXT("Clubs");
					break;

				case (S_Diamonds):
					suitName = TXT("Diamonds");
					break;

				case (S_Spades):
					suitName = TXT("Spades");
					break;
			}

			Sprite->SetSpriteTexture(localTexturePool->GetTexture(HashedString("SampleGames.Solitaire." + suitName + "." + CardValueToString(static_cast<int>(Value)))));
		}
	}
}
SOLITAIRE_END