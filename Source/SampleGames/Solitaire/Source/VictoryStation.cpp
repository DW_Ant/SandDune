/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VictoryStation.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::VictoryStation, SD::Solitaire::CardStation)
SOLITAIRE_BEGIN

void VictoryStation::BeginObject ()
{
	Super::BeginObject();

	//Player can never drag a card away from victory station.
	if (Input.IsValid())
	{
		Input->Destroy();
	}
}

bool VictoryStation::CanAddCard (Card* newCard) const
{
	Card* topCard = GetTopCard();
	if (topCard != nullptr)
	{
		//Must be in ascending order and match suit
		return (newCard->GetSuit() == topCard->GetSuit() &&
			static_cast<int>(newCard->GetValue()) - static_cast<int>(topCard->GetValue()) == 1);
	}
	else
	{
		return (newCard->GetValue() == Card::CV_Ace);
	}
}

bool VictoryStation::CanAddColumn (CardColumn* newColumn) const
{
	//Only a single card can be added one at a time
	if (newColumn->ReadCardList().size() != 1)
	{
		return false;
	}

	return CanAddCard(newColumn->ReadCardList().at(0));
}

bool VictoryStation::HasCompleteSet () const
{
	const int numCardsInSuit = 13;
	if (CardStack.size() != numCardsInSuit)
	{
		return false;
	}

	for (UINT_TYPE i = 0; i < numCardsInSuit; ++i)
	{
		if (static_cast<int>(CardStack.at(i)->GetValue()) != (i+1))
		{
			return false;
		}
	}

	return true;
}
SOLITAIRE_END