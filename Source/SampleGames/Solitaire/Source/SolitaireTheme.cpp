/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireTheme.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::SolitaireTheme, SD::GuiTheme)
SOLITAIRE_BEGIN

const DString SolitaireTheme::SOLITAIRE_STYLE_NAME = TXT("SolitaireTheme");

void SolitaireTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	InitializeSolitaireTheme();
}

void SolitaireTheme::InitializeSolitaireTheme ()
{
	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo solitaireStyle = CreateStyleFrom(*defaultStyle);
		solitaireStyle.Name = SOLITAIRE_STYLE_NAME;
		Styles.push_back(solitaireStyle);

		ButtonComponent* buttonTemplate = dynamic_cast<ButtonComponent*>(FindTemplate(&solitaireStyle, ButtonComponent::SStaticClass()));
		CHECK(buttonTemplate != nullptr)

		LabelComponent* buttonLabel = buttonTemplate->GetCaptionComponent();
		if (buttonLabel != nullptr)
		{
			buttonLabel->SetCharacterSize(24);
		}

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(buttonTemplate->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		CHECK(colorState != nullptr)
		colorState->SetDefaultColor(Color(128, 128, 128, 200));
		colorState->SetHoverColor(Color(145, 145, 145, 200));
		colorState->SetDownColor(Color(96, 96, 96, 200));
		colorState->SetDisabledColor(Color(32, 32, 32, 200));
	}
}
SOLITAIRE_END