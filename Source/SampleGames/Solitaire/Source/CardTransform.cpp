/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CardTransform.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
CardTransform::CardTransform () : PlanarTransform(),
	Rotation(0.f)
{

}

CardTransform::CardTransform (const Vector2& inPosition, const Vector2& inSize, Float inDepth, PlanarTransform* inRelativeTo) : PlanarTransform(inPosition, inSize, inDepth, inRelativeTo),
	Rotation(0.f)
{

}

CardTransform::CardTransform (const CardTransform& copyObj) : PlanarTransform(copyObj),
	Rotation(copyObj.Rotation)
{

}

CardTransform::~CardTransform ()
{

}

void CardTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	PlanarTransform::CalculateScreenSpaceTransform(target, cam, OUT outProjectionData);

	outProjectionData.Rotation = Rotation.Value;
}

void CardTransform::SetRotation (Float newRotation)
{
	Rotation = newRotation;
}
SOLITAIRE_END