/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CardStation.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::CardStation, SD::Entity)
SOLITAIRE_BEGIN

const Int CardStation::STATION_INPUT_PRIORITY = 10;

void CardStation::InitProps ()
{
	Super::InitProps();

	SetSize(Card::GetCardSize());

	StationOutline = nullptr;
	Input = nullptr;
}

void CardStation::BeginObject ()
{
	Super::BeginObject();

	StationOutline = SpriteComponent::CreateObject();
	if (AddComponent(StationOutline))
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		StationOutline->SetSpriteTexture(localTexturePool->GetTexture(HashedString("SampleGames.Solitaire.CardOutline")));
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		InputBroadcaster* mainBroadcaster = InputEngineComponent::Find()->GetMainBroadcaster();
		CHECK(mainBroadcaster != nullptr)

		mainBroadcaster->AddInputComponent(Input.Get(), STATION_INPUT_PRIORITY);
		Input->OnMouseClick = SDFUNCTION_3PARAM(this, CardStation, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	if (solitaireEngine == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardStation to a SolitaireDrawLayer since it could not find a SolitaireEngineComponent in curren thread."));
		return;
	}

	SolitaireDrawLayer* drawLayer = solitaireEngine->GetCardLayer();
	if (drawLayer == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardStation to a SolitaireDrawLayer since the local Solitaire Engine does not have a SolitaireDrawLayer."));
		return;
	}

	drawLayer->RegisterCardStation(this);
}

void CardStation::ReturnCards (CardColumn* columnToReturn)
{
	AddColumn(columnToReturn);
}

void CardStation::AddCard (Card* newCard)
{
	CardStack.push_back(newCard);
	newCard->SetRelativeTo(this);
	newCard->SetPositionNear(Vector2::ZERO_VECTOR); //shift card a bit

	if (OnCardAdded.IsBounded())
	{
		OnCardAdded(newCard);
	}
}

void CardStation::AddColumn (CardColumn* columnToAdd)
{
	for (Int i = Int(columnToAdd->ReadCardList().size() - 1); i >= 0; --i)
	{
		AddCard(columnToAdd->ReadCardList().at(i.ToUnsignedInt()));
	}

	columnToAdd->Destroy();
}

void CardStation::RemoveTopCard ()
{
	if (ContainerUtils::IsEmpty(CardStack))
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Attempting to remove a card from an empty stack."));
		return;
	}

	CardStack.erase(CardStack.begin() + CardStack.size() - 1);
}

void CardStation::RemoveAllCards ()
{
	ContainerUtils::Empty(CardStack);
}

bool CardStation::CanAddCard (Card* newCard) const
{
	return false;
}

bool CardStation::CanAddColumn (CardColumn* newColumn) const
{
	return false;
}

void CardStation::RenderCardsInStation (RenderTarget* renderTarget, Camera* camera)
{
	if (ContainerUtils::IsEmpty(CardStack))
	{
		//Render the outline
		if (StationOutline.IsValid() && StationOutline->IsVisible())
		{
			StationOutline->Render(renderTarget, camera);
		}
	}
	else
	{
		//Although we can get away with just rendering the top card, still render all cards underneathe due to rotation differences
		for (size_t i = 0; i < CardStack.size(); ++i)
		{
			CardStack.at(i)->MarkProjectionDataDirty();
			SpriteComponent* cardSprite = CardStack.at(i)->GetSprite();
			if (cardSprite != nullptr && cardSprite->IsVisible())
			{
				cardSprite->Render(renderTarget, camera);
			}
		}
	}
}

Card* CardStation::GetTopCard () const
{
	if (ContainerUtils::IsEmpty(CardStack))
	{
		return nullptr;
	}

	return CardStack.at(CardStack.size() - 1);
}

bool CardStation::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType)
{
	if (buttonEvent.button != sf::Mouse::Left)
	{
		return false;
	}

	if (!IsWithinBounds(Vector2(Float::MakeFloat(buttonEvent.x), Float::MakeFloat(buttonEvent.y))))
	{
		return false;
	}

	if (ContainerUtils::IsEmpty(CardStack))
	{
		if (OnEmptyClicked.IsBounded())
		{
			OnEmptyClicked();
		}
		return true;
	}

	Card* topCard = GetTopCard();
	CHECK(topCard != nullptr)

	if (topCard->IsFacingUp())
	{
		if (eventType == sf::Event::MouseButtonPressed)
		{
			Vector2 clickOffset(ReadCachedAbsPosition() - mouse->GetPosition());
			CardColumn* floatingColumn = CardColumn::CreateObject();
			floatingColumn->SetDragging(true, this);
			floatingColumn->SetPosition(clickOffset);
			floatingColumn->SetRelativeTo(mouse);
			floatingColumn->SetCardList({GetTopCard()});
			floatingColumn->GetInput()->SetInputPriority(CardColumn::COLUMN_INPUT_PRIORITY + 5);
			RemoveTopCard();
		}

		return true;
	}
	else
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			//Reveal top card
			topCard->SetFacingUp(true);
			if (OnCardReveal.IsBounded())
			{
				OnCardReveal(this, topCard);
			}
		}

		return true;
	}
}
SOLITAIRE_END