/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireDrawLayer.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::SolitaireDrawLayer, SD::PlanarDrawLayer)
SOLITAIRE_BEGIN
const Int SolitaireDrawLayer::SOLITAIRE_DRAW_LAYER_PRIORITY = 100;

void SolitaireDrawLayer::InitProps ()
{
	Super::InitProps();

	Background = nullptr;
}

void SolitaireDrawLayer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, SolitaireDrawLayer, HandleGarbageCollection, void));
	}
}

void SolitaireDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	//Render background first to be drawn behind everything else
	if (Background.IsValid())
	{
		if (PlanarTransform* transform = dynamic_cast<PlanarTransform*>(Background->GetOwner()))
		{
			transform->MarkProjectionDataDirty();
			Background->Render(renderTarget, cam);
		}
		else
		{
			SolitaireLog.Log(LogCategory::LL_Warning, TXT("The Background (%s) component's owner does not have a PlanarTransformation."), Background.Get()->ToString());
		}
	}

	Super::RenderDrawLayer(renderTarget, cam);

	SortRenderedObjects();

	//Render stations first since they should be drawn under columns
	RenderCardStations(renderTarget, cam);
	RenderCardColumns(renderTarget, cam);
}

void SolitaireDrawLayer::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, SolitaireDrawLayer, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

void SolitaireDrawLayer::RegisterCardColumn (CardColumn* newColumn)
{
	Columns.push_back(newColumn);
}

void SolitaireDrawLayer::RegisterCardStation (CardStation* newStation)
{
	Stations.push_back(newStation);
}

void SolitaireDrawLayer::SortRenderedObjects ()
{
	PurgeExpiredObjects();

	std::sort(Columns.begin(), Columns.end(), [](CardColumn* a, CardColumn* b)
	{
		return a->GetDepth() > b->GetDepth();
	});

	//Don't bother sorting stations since no station overlaps another station.
}

void SolitaireDrawLayer::RenderCardStations (RenderTarget* renderTarget, Camera* cam)
{
	for (UINT_TYPE i = 0; i < Stations.size(); ++i)
	{
		if (Stations.at(i)->IsVisible())
		{
			Stations.at(i)->MarkProjectionDataDirty();
			Stations.at(i)->RenderCardsInStation(renderTarget, cam);
		}
	}
}

void SolitaireDrawLayer::RenderCardColumns (RenderTarget* renderTarget, Camera* cam)
{
	for (UINT_TYPE i = 0; i < Columns.size(); ++i)
	{
		if (Columns.at(i)->IsVisible())
		{
			Columns.at(i)->MarkProjectionDataDirty();
			Columns.at(i)->RenderCards(renderTarget, cam);
		}
	}
}

void SolitaireDrawLayer::PurgeExpiredObjects ()
{
	UINT_TYPE i = 0;
	while (i < Columns.size())
	{
		if (!VALID_OBJECT(Columns.at(i)))
		{
			Columns.erase(Columns.begin() + i);
			continue;
		}

		++i;
	}

	i = 0;
	while (i < Stations.size())
	{
		if (!VALID_OBJECT(Stations.at(i)))
		{
			Stations.erase(Stations.begin() + i);
		}

		++i;
	}
}

void SolitaireDrawLayer::HandleGarbageCollection ()
{
	PurgeExpiredObjects();
}
SOLITAIRE_END