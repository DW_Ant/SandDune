/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireEngineComponent.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_ENGINE_COMPONENT(SD::Solitaire::SolitaireEngineComponent)
SOLITAIRE_BEGIN

SolitaireEngineComponent::SolitaireEngineComponent () : EngineComponent(),
	NumPlays(0),
	NumVictories(0),
	FastestTime(-1.f)
{

}

void SolitaireEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_SOLITAIRE, TICK_GROUP_PRIORITY_SOLITAIRE);

	CardDrawLayer = SolitaireDrawLayer::CreateObject();
	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	if (mainWindow != nullptr)
	{
		mainWindow->RegisterDrawLayer(CardDrawLayer.Get());
	}

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(SolitaireTheme::SStaticClass());
}

void SolitaireEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	ImportSolitaireResources();

	GuiTheme* theme = GuiTheme::GetGuiTheme();
	CHECK(theme != nullptr)
	theme->SetDefaultStyle(SolitaireTheme::SOLITAIRE_STYLE_NAME);
}

void SolitaireEngineComponent::PostInitializeComponent ()
{
	Background = Table::CreateObject();

	MainMenu = SolitaireMenu::CreateObject();
	MainMenu->RegisterToMainWindow();

	Bot = SolitaireBot::CreateObject();
}

void SolitaireEngineComponent::ShutdownComponent ()
{
	if (Bot.IsValid())
	{
		Bot->Destroy();
	}

	if (CardDrawLayer.IsValid())
	{
		CardDrawLayer->Destroy();
	}

	if (MainMenu.IsValid())
	{
		MainMenu->Destroy();
	}

	if (ActiveGame.IsValid())
	{
		ActiveGame->Destroy();
	}

	Super::ShutdownComponent();
}

void SolitaireEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(GraphicsEngineComponent::SStaticClass());
	outDependencies.push_back(GuiEngineComponent::SStaticClass());
}

void SolitaireEngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(GuiEngineComponent::SStaticClass());
}

void SolitaireEngineComponent::StartNewGame (const DClass* gameClass)
{
	NumPlays++;

	if (ActiveGame.IsValid())
	{
		ActiveGame->ClearGame();
	}

	if (const SolitaireGame* gameCdo = dynamic_cast<const SolitaireGame*>(gameClass->GetDefaultObject()))
	{
		ActiveGame = Object::ReplaceTargetWithObjOfMatchingClass<SolitaireGame>(ActiveGame.Get(), gameCdo);
		if (ActiveGame.IsValid())
		{
			ActiveGame->SetupNewGame();
		}
	}
	else
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot start a new game since the specified class (%s) is not a SolitaireGame."), gameClass->ToString());
	}
}

void SolitaireEngineComponent::BeatenGame (Float elapsedTime)
{
	NumVictories++;
	if (FastestTime <= 0.f || elapsedTime < FastestTime)
	{
		FastestTime = elapsedTime;
	}

	if (MainMenu.IsValid())
	{
		MainMenu->RefreshScoreboard();
	}
}

void SolitaireEngineComponent::ImportSolitaireResources ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("Background.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("CardOutline.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("CardBack.txtr")));

	std::vector<DString> suits;
	suits.push_back(TXT("Clubs"));
	suits.push_back(TXT("Diamonds"));
	suits.push_back(TXT("Hearts"));
	suits.push_back(TXT("Spades"));

	for (UINT_TYPE suitIdx = 0; suitIdx < suits.size(); ++suitIdx)
	{
		for (int i = 1; i <= 13; ++i)
		{
			localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Solitaire") / suits.at(suitIdx), Card::CardValueToString(i) + TXT(".txtr")));
		}
	}
}
SOLITAIRE_END