/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VictoryComponent.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::VictoryComponent, SD::TickComponent)
SOLITAIRE_BEGIN

void VictoryComponent::InitProps ()
{
	Super::InitProps();

	Velocity = Vector2::ZERO_VECTOR;
	Acceleration = Vector2::ZERO_VECTOR;
	RotationRate = 0.f;
	Delay = 0.f;
}

void VictoryComponent::BeginObject ()
{
	Super::BeginObject();

	Velocity.X = RandomUtils::RandRange(-384.f, 64.f);
	Velocity.Y = RandomUtils::RandRange(-64.f, 32.f);
	Acceleration.X = 0.f;
	Acceleration.Y = RandomUtils::RandRange(256.f, 512.f);
	RotationRate = RandomUtils::RandRange(-180.f, 180.f);
}

bool VictoryComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	return (Super::CanBeAttachedTo(ownerCandidate) && dynamic_cast<CardTransform*>(ownerCandidate) != nullptr);
}

void VictoryComponent::Tick (Float deltaSec)
{
	Super::Tick(deltaSec);

	if (Delay > 0.f)
	{
		Delay -= deltaSec;
		return;
	}

	CardTransform* ownerCard = dynamic_cast<CardTransform*>(GetOwner());
	if (ownerCard != nullptr)
	{
		Velocity += (Acceleration * deltaSec);
		ownerCard->SetPosition(ownerCard->GetPosition() + (Velocity * deltaSec));
		ownerCard->SetRotation(ownerCard->GetRotation() + (RotationRate * deltaSec));
	}
}
SOLITAIRE_END