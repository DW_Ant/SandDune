/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireMenu.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::SolitaireMenu, SD::GuiEntity)
SOLITAIRE_BEGIN

void SolitaireMenu::InitProps ()
{
	Super::InitProps();

	StartGameButton = nullptr;
	ExitGameButton = nullptr;
	OptionsButton = nullptr;
	Title = nullptr;

	ScoreboardBackground = nullptr;
	NumPlaysLabel = nullptr;
	NumVictoriesLabel = nullptr;
	SpeedRecordLabel = nullptr;
	CurTimeLabel = nullptr;
	Tick = nullptr;

	IsGameActive = false;
}

void SolitaireMenu::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, SolitaireMenu, HandleTick, void, Float));
		Tick->SetTickInterval(0.25f);
	}
}

void SolitaireMenu::ConstructUI ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	StartGameButton = ButtonComponent::CreateObject();
	if (AddComponent(StartGameButton))
	{
		StartGameButton->SetPosition(Vector2(0.2f, 0.5f));
		StartGameButton->SetSize(Vector2(0.6f, 0.15f));
		StartGameButton->SetCaptionText(translator->TranslateText(TXT("StartGame"), TXT("Solitaire"), TXT("SolitaireMenu")));
		StartGameButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SolitaireMenu, HandleStartGameClicked, void, ButtonComponent*));
	}

	ExitGameButton = ButtonComponent::CreateObject();
	if (AddComponent(ExitGameButton))
	{
		ExitGameButton->SetPosition(Vector2(0.2f, 0.75f));
		ExitGameButton->SetSize(StartGameButton->ReadSize());
		ExitGameButton->SetCaptionText(translator->TranslateText(TXT("ExitGame"), TXT("Solitaire"), TXT("SolitaireMenu")));
		ExitGameButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SolitaireMenu, HandleExitClicked, void, ButtonComponent*));
	}

	OptionsButton = ButtonComponent::CreateObject();
	if (AddComponent(OptionsButton))
	{
		OptionsButton->SetPosition(Vector2(0.4f, 0.875f));
		OptionsButton->SetSize(Vector2(0.2f, 0.075f));
		OptionsButton->SetCaptionText(translator->TranslateText(TXT("Options"), TXT("Solitaire"), TXT("SolitaireMenu")));
		OptionsButton->SetVisibility(false);
		OptionsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SolitaireMenu, HandleOptionsClicked, void, ButtonComponent*));
	}

	Title = LabelComponent::CreateObject();
	if (AddComponent(Title))
	{
		Title->SetAutoRefresh(false);
		Title->SetPosition(Vector2(0.f, 0.15f));
		Title->SetSize(Vector2(1.f, 20.f));
		Title->SetCharacterSize(76);
		Title->SetHorizontalAlignment(LabelComponent::HA_Center);
		Title->SetVerticalAlignment(LabelComponent::VA_Center);
		Title->SetText(TXT("SOLITAIRE"));
		Title->SetAutoRefresh(true);
	}

	ScoreboardBackground = FrameComponent::CreateObject();
	if (AddComponent(ScoreboardBackground))
	{
		ScoreboardBackground->SetAnchorBottom(0.f);
		ScoreboardBackground->SetAnchorRight(0.f);
		ScoreboardBackground->SetAnchorLeft(0.f);
		ScoreboardBackground->SetSize(Vector2(1.f, 24.f));
		ScoreboardBackground->SetLockedFrame(true);
		ScoreboardBackground->SetVisibility(false); //Reveals on start game
		ScoreboardBackground->SetCenterColor(Color(200, 200, 200));
		Float borderThickness = ScoreboardBackground->GetBorderThickness();
		ScoreboardBackground->SetBorderThickness(0.f); //Allow the frame component to fill the space.
		if (BorderRenderComponent* borderComp = ScoreboardBackground->GetBorderComp())
		{
			borderComp->Destroy();
		}

		//Add a sprite that smooths out the top frame
		const Texture* topFrame = localTexturePool->GetTexture(HashedString("Engine.Gui.FrameBorder"));
		if (topFrame != nullptr)
		{
			PlanarTransformComponent* topTrimTransform = PlanarTransformComponent::CreateObject();
			if (ScoreboardBackground->AddComponent(topTrimTransform))
			{
				topTrimTransform->SetPosition(Vector2(0.f, borderThickness));
				topTrimTransform->SetSize(Vector2(1.f, -borderThickness)); //Negative to invert the texture vertically.

				SpriteComponent* spriteComp = SpriteComponent::CreateObject();
				if (topTrimTransform->AddComponent(spriteComp))
				{
					spriteComp->SetSpriteTexture(topFrame);
				}
			}
		}

		NumPlaysLabel = LabelComponent::CreateObject();
		if (ScoreboardBackground->AddComponent(NumPlaysLabel))
		{
			NumPlaysLabel->SetAutoRefresh(false);
			NumPlaysLabel->SetPosition(Vector2(0.f, 4.f));
			NumPlaysLabel->SetSize(Vector2(0.2f, 1.f));
			NumPlaysLabel->SetClampText(true);
			NumPlaysLabel->SetWrapText(false);
			NumPlaysLabel->SetAutoRefresh(true);
			if (TextRenderComponent* textRender = NumPlaysLabel->GetRenderComponent())
			{
				textRender->SetFontColor(sf::Color(0, 0, 0));
			}
		}

		NumVictoriesLabel = LabelComponent::CreateObject();
		if (ScoreboardBackground->AddComponent(NumVictoriesLabel))
		{
			NumVictoriesLabel->SetAutoRefresh(false);
			NumVictoriesLabel->SetPosition(Vector2(0.25f, 4.f));
			NumVictoriesLabel->SetSize(Vector2(0.2f, 1.f));
			NumVictoriesLabel->SetClampText(true);
			NumVictoriesLabel->SetWrapText(false);
			NumVictoriesLabel->SetAutoRefresh(true);
			if (TextRenderComponent* textRender = NumVictoriesLabel->GetRenderComponent())
			{
				textRender->SetFontColor(sf::Color(0, 0, 0));
			}
		}

		SpeedRecordLabel = LabelComponent::CreateObject();
		if (ScoreboardBackground->AddComponent(SpeedRecordLabel))
		{
			SpeedRecordLabel->SetAutoRefresh(false);
			SpeedRecordLabel->SetPosition(Vector2(0.55f, 4.f));
			SpeedRecordLabel->SetSize(Vector2(0.2f, 1.f));
			SpeedRecordLabel->SetClampText(true);
			SpeedRecordLabel->SetWrapText(false);
			SpeedRecordLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
			SpeedRecordLabel->SetAutoRefresh(true);
			if (TextRenderComponent* textRender = SpeedRecordLabel->GetRenderComponent())
			{
				textRender->SetFontColor(sf::Color(0, 0, 0));
			}
		}

		CurTimeLabel = LabelComponent::CreateObject();
		if (ScoreboardBackground->AddComponent(CurTimeLabel))
		{
			CurTimeLabel->SetAutoRefresh(false);
			CurTimeLabel->SetPosition(Vector2(0.8f, 4.f));
			CurTimeLabel->SetSize(Vector2(0.2f, 1.f));
			CurTimeLabel->SetClampText(true);
			CurTimeLabel->SetWrapText(false);
			CurTimeLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
			CurTimeLabel->SetAutoRefresh(true);
			if (TextRenderComponent* textRender = CurTimeLabel->GetRenderComponent())
			{
				textRender->SetFontColor(sf::Color(0, 0, 0));
			}
		}
	}

	RefreshScoreboard();
}

void SolitaireMenu::RefreshScoreboard ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SolitaireEngineComponent* engineComp = SolitaireEngineComponent::Find();
	CHECK(engineComp != nullptr)

	if (NumPlaysLabel.IsValid())
	{
		DString text = translator->TranslateText(TXT("NumPlays"), TXT("Solitaire"), TXT("SolitaireMenu"));
		DString::FormatString(text, engineComp->GetNumPlays());
		NumPlaysLabel->SetText(text);
	}

	if (NumVictoriesLabel.IsValid() && engineComp->GetNumVictories() > 0)
	{
		DString text = translator->TranslateText(TXT("NumVictories"), TXT("Solitaire"), TXT("SolitaireMenu"));
		DString::FormatString(text, engineComp->GetNumVictories());
		NumVictoriesLabel->SetText(text);
	}

	if (SpeedRecordLabel.IsValid() && engineComp->GetFastestTime() > 0.f)
	{
		DString text = translator->TranslateText(TXT("SpeedRecord"), TXT("Solitaire"), TXT("SolitaireMenu"));
		DString::FormatString(text, engineComp->GetFastestTime());
		SpeedRecordLabel->SetText(text);
	}

	//No need to update current time here since it's updated in Tick function.
}

void SolitaireMenu::PauseScoreboard ()
{
	if (Tick.IsValid())
	{
		Tick->SetTicking(false);
	}
}

void SolitaireMenu::ResumeScoreboard ()
{
	if (Tick.IsValid())
	{
		Tick->SetTicking(true);
	}
}

void SolitaireMenu::SetMenuToGameMode ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	if (StartGameButton.IsValid())
	{
		StartGameButton->SetPosition(Vector2(0.1f, 0.875f));
		StartGameButton->SetSize(Vector2(0.2f, 0.075f));
		StartGameButton->SetCaptionText(translator->TranslateText(TXT("ReplaceGame"), TXT("Solitaire"), TXT("SolitaireMenu")));

		if (!IsGameActive && StartGameButton->GetStateComponent() != nullptr)
		{
			StartGameButton->GetStateComponent()->SetDefaultAppearance();
			IsGameActive = true;
		}
	}

	if (ExitGameButton.IsValid())
	{
		ExitGameButton->SetPosition(Vector2(0.7f, 0.875f));
		ExitGameButton->SetSize(Vector2(0.2f, 0.075f));
	}

	if (OptionsButton.IsValid())
	{
		OptionsButton->SetVisibility(true);
	}

	if (Title.IsValid())
	{
		Title->SetVisibility(false);
	}

	if (ScoreboardBackground.IsValid())
	{
		ScoreboardBackground->SetVisibility(true);
	}
}

void SolitaireMenu::HandleStartGameClicked (ButtonComponent* uiComponent)
{
	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	CHECK(solitaireEngine != nullptr)

	solitaireEngine->StartNewGame(SolitaireGame::SStaticClass());
	SetMenuToGameMode();
	RefreshScoreboard();
	ResumeScoreboard();
}

void SolitaireMenu::HandleExitClicked (ButtonComponent* uiComponent)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->Shutdown();
}

void SolitaireMenu::HandleOptionsClicked (ButtonComponent* uiComponent)
{
	OptionsMenu* optionsMenu = OptionsMenu::CreateObject();
	optionsMenu->RegisterToMainWindow();
}

void SolitaireMenu::HandleTick (Float deltaSec)
{
	if (CurTimeLabel.IsNullptr())
	{
		return;
	}

	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	CHECK(solitaireEngine != nullptr)
	SolitaireGame* game = solitaireEngine->GetActiveGame();
	if (game != nullptr)
	{
		Engine* engine = Engine::FindEngine();
		CHECK(engine != nullptr)

		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		Float deltaTime = engine->GetElapsedTime() - game->GetStartTime();
		DString text = translator->TranslateText(TXT("CurTime"), TXT("Solitaire"), TXT("SolitaireMenu"));
		DString::FormatString(text, deltaTime);
		CurTimeLabel->SetText(text);
	}
}
SOLITAIRE_END