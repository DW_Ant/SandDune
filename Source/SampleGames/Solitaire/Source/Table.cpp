/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Table.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

IMPLEMENT_CLASS(SD::Solitaire::Table, SD::Entity)
SOLITAIRE_BEGIN

void Table::InitProps ()
{
	Super::InitProps();

	Sprite = nullptr;

	//Set transform to fill entire screen
	Position = Vector2::ZERO_VECTOR;
	Size = Vector2(1.f, 1.f);
}

void Table::BeginObject ()
{
	Super::BeginObject();

	Sprite = SpriteComponent::CreateObject();
	if (AddComponent(Sprite))
	{
		Sprite->SetSpriteTexture(TexturePool::FindTexturePool()->GetTexture(HashedString("SampleGames.Solitaire.Background")));
		RegisterToDrawLayer();
	}
}

void Table::RegisterToDrawLayer ()
{
	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	if (solitaireEngine != nullptr)
	{
		SolitaireDrawLayer* drawLayer = solitaireEngine->GetCardLayer();
		CHECK(drawLayer != nullptr)

		drawLayer->Background = Sprite;
	}
}
SOLITAIRE_END