/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireEngineComponent.h
  The EngineComponent that spawns the UI that launches Solitaire game instances.
  This EngineComponent is also responsible for setting up the custom DrawLayer.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "SolitaireMenu.h"
#include "SolitaireDrawLayer.h"
#include "SolitaireGame.h"
#include "SolitaireBot.h"

SOLITAIRE_BEGIN

class SolitaireEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(SolitaireEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The UI menu that's responsible for choosing a Solitaire game mode. */
	DPointer<SolitaireMenu> MainMenu;

	/* Draw Layer that handles rendering card objects on the table. */
	DPointer<SolitaireDrawLayer> CardDrawLayer;

	/* Game Instance that's defines the game rules in how to play Solitaire. */
	DPointer<SolitaireGame> ActiveGame;

	/* Entity that renders the table in the background. */
	DPointer<Entity> Background;

	/* The bot that could take control to play the Solitaire games. */
	DPointer<SolitaireBot> Bot;

	/* Number of times the player started a new game. */
	Int NumPlays;

	/* Number of times the player beaten the game. */
	Int NumVictories;

	/* Fastest game completion time (in seconds).  Becomes negative if the player has not completed a game yet. */
	Float FastestTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SolitaireEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Instantiates/replaces a new game instance if the class is different from what's sepcified.
	 * Otherwise, the current game simply restarts.
	 */
	virtual void StartNewGame (const DClass* gameClass);

	virtual void BeatenGame (Float elapsedTime);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SolitaireDrawLayer* GetCardLayer () const
	{
		return CardDrawLayer.Get();
	}

	inline SolitaireGame* GetActiveGame () const
	{
		return ActiveGame.Get();
	}

	inline SolitaireMenu* GetMainMenu ()
	{
		return MainMenu.Get();
	}

	inline Int GetNumPlays () const
	{
		return NumPlays;
	}

	inline Int GetNumVictories () const
	{
		return NumVictories;
	}

	inline Float GetFastestTime () const
	{
		return FastestTime;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportSolitaireResources ();
};
SOLITAIRE_END