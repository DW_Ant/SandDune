/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireClasses.h
  Contains all header includes for the Solitaire module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the SolitaireClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#include "Card.h"
#include "CardColumn.h"
#include "CardStation.h"
#include "CardTransform.h"
#include "OptionsMenu.h"
#include "ReturnCardInterface.h"
#include "Solitaire.h"
#include "SolitaireBot.h"
#include "SolitaireDrawLayer.h"
#include "SolitaireEngineComponent.h"
#include "SolitaireGame.h"
#include "SolitaireMenu.h"
#include "SolitaireTheme.h"
#include "stdafx.h"
#include "Table.h"
#include "VictoryComponent.h"
#include "VictoryStation.h"

