/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Table.h
  Simple Entity that renders the table the cards rest on.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class Table : public Entity, public PlanarTransform
{
	DECLARE_CLASS(Table)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<SpriteComponent> Sprite;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Registers this Entity to the SolitaireDraw layer as this game's background.
	 */
	virtual void RegisterToDrawLayer ();
};
SOLITAIRE_END