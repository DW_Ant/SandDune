/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VictoryStation.h
  A station where the player must place all cards in the same suit in this station in ascending
  order.  When all victory stations are full, the player has won the game.
=====================================================================
*/

#pragma once

#include "CardStation.h"

SOLITAIRE_BEGIN
class VictoryStation : public CardStation
{
	DECLARE_CLASS(VictoryStation)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual bool CanAddCard (Card* newCard) const override;
	virtual bool CanAddColumn (CardColumn* newColumn) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this stack contains all cards in the suit.
	 */
	virtual bool HasCompleteSet () const;
};
SOLITAIRE_END