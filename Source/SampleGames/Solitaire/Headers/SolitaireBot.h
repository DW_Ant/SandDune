/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireBot.h
  An Entity that can play the Solitaire Game by analyzing the game board, and sending
  mouse events to play the game.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class SolitaireBot : public Entity
{
	DECLARE_CLASS(SolitaireBot)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The location the bot is sending the mouse pointer to. */
	Vector2 MouseDestination;

	/* Location the mouse release button is suppose to go. */
	Vector2 MouseReleaseDestination;

	/* The distance the mouse must be from destination before the mouse event is sent. */
	Float MouseDistTolerance;

	/* How many pixels per second this bot is traversing through the board. */
	Float MouseSpeed;

	/* Delay between each action (in seconds). */
	Range<Float> ActionDelay;

	/* If true, then this bot is dragging a card around. */
	bool IsDraggingCard;

	DPointer<TickComponent> Tick;
	DPointer<InputComponent> Input;

	/* Becomes true if the bot is currently suspended due to user override. */
	bool IsPaused;

private:
	/* Amount of time to wait before processing Tick.  If negative, then this bot is not sleeping. */
	Float SleepTime;

	/* Becomes true if an action was taken place within this drawing cycle.
	This is primarily used to detect a game over, and the bot needs to decide to start a new game. */
	bool ActionDoneThisCycle;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetPaused (bool newIsPaused);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Evaluates the board to figure out where the bot should take the mouse pointer.
	 */
	virtual void CalculateNextAction ();

	/**
	 * Moves the mouse closer to destination.
	 */
	virtual void MoveMouse (Float deltaSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& evnt);
	virtual bool HandleWheelInput (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType type);
	virtual void HandleTick (Float deltaSec);
};
SOLITAIRE_END