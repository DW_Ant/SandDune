/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ReturnCardInterface.h
  An interface that allows dragged cards to return to this object whenever the player
  released the mouse button over an invalid location.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class CardColumn;

class ReturnCardInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void ReturnCards (CardColumn* columnToReturn) = 0;
};
SOLITAIRE_END