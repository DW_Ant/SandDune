/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CardStation.h
  A place where multiple cards may stack.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "ReturnCardInterface.h"

SOLITAIRE_BEGIN
class Card;

class CardStation : public Entity, public PlanarTransform, public ReturnCardInterface
{
	DECLARE_CLASS(CardStation)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Int STATION_INPUT_PRIORITY;

	/* Callback invoked whenever a face down top card is revealed. */
	SDFunction<void, CardStation*, Card*> OnCardReveal;

	/* Callback whenever the player clicked on this station while there aren't any cards on it. */
	SDFunction<void> OnEmptyClicked;

	/* Callback whenever a card is added to this station. */
	SDFunction<void, Card*> OnCardAdded;

protected:
	/* List of cards resting on this station.  The last card is the one on top of the pile. */
	std::vector<Card*> CardStack;

	DPointer<SpriteComponent> StationOutline;
	DPointer<InputComponent> Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void ReturnCards (CardColumn* columnToReturn) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void AddCard (Card* newCard);
	virtual void AddColumn (CardColumn* columnToAdd);
	virtual void RemoveTopCard ();
	virtual void RemoveAllCards ();

	/**
	 * Returns true if the given card can be added to this stack.
	 */
	virtual bool CanAddCard (Card* newCard) const;
	virtual bool CanAddColumn (CardColumn* newColumn) const;

	/**
	 * Iterates through the cards in this station to render them in the specified render target.
	 */
	virtual void RenderCardsInStation (RenderTarget* renderTarget, Camera* camera);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the top card the user may interact with.
	 */
	virtual Card* GetTopCard () const;

	inline SpriteComponent* GetStationOutline () const
	{
		return StationOutline.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType);
};
SOLITAIRE_END