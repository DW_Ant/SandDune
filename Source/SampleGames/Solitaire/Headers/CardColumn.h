/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CardColumn.h
  A list of cards where the user may interact with any of the cards.  Columns may detach
  into two separate columns.  Cards can be added to a column only if they're alternating colors
  from neighboring cards and are also in descending order.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "ReturnCardInterface.h"

SOLITAIRE_BEGIN
class Card;
class CardStation;

class CardColumn : public Entity, public PlanarTransform, public ReturnCardInterface
{
	DECLARE_CLASS(CardColumn)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Int COLUMN_INPUT_PRIORITY;

protected:
	/* Vertical spacing between each card within a single column. */
	static const Float VERTICAL_SPACING;

	/* If true, then the player is dragging this column around with a mouse. */
	bool bIsDragging;

	/* The place to return to if the user released the dragging column at an invalid location. */
	ReturnCardInterface* CardReturn;

	/* List of cards that make up this column.  The first entry is the top card, and last entry is the card with least value. */
	std::vector<Card*> CardList;

	DPointer<InputComponent> Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool IsWithinBounds (const Vector2& point) const override;
	virtual void ReturnCards (CardColumn* columnToReturn) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the cards from the bottomColumn to the end of this column.
	 * Removes old column
	 */
	virtual void AppendColumn (CardColumn* bottomColumn);

	virtual void AppendCard (Card* newBottomCard);

	virtual void RemoveAllCards ();

	/**
	 * Detaches the card at the specified index.  Once detached, the card and all
	 * lesser cards will form its own column.
	 * Returns the newly created detached card column.
	 */
	virtual CardColumn* DetachAt (UINT_TYPE cardIdx);

	/**
	 * Returns true if the given column can be appended to this column.
	 */
	virtual bool CanCombine (CardColumn* bottomColumn) const;

	/**
	 * Returns true if the card can be added at the end of this column.
	 */
	virtual bool CanCombine (Card* newCard) const;

	virtual void RenderCards (RenderTarget* renderTarget, Camera* camera);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCardList (const std::vector<Card*> newCardList);

	/**
	 * Sets the dragging flag and the return place.  If the player releases the mouse at an invalid
	 * location, it'll return the cards of this column back to the returnPlace.
	 */
	virtual void SetDragging (bool bNewDragging, ReturnCardInterface* returnPlace);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	InputComponent* GetInput () const
	{
		return Input.Get();
	}

	const std::vector<Card*>& ReadCardList () const
	{
		return CardList;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual Int FindCardFromCoordinates (const Vector2& mousePos) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType);
};
SOLITAIRE_END