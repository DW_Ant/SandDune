/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  OptionsMenu.h
  Tiny menu that enables card customization.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class OptionsMenu : public GuiEntity
{
	DECLARE_CLASS(OptionsMenu)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Range<Float> MaxAngleRange;
	Range<Float> MaxShiftRange;

	/* Determines the strength behind each increment click. */
	Float IncrementStep;

	/* The original settings when this menu was first created (for restoration if cancelled). */
	Float OriginalMaxAngle;
	Float OriginalMaxShift;

	/* Background behind the menu that darkens everything else, and consumes mouse input. */
	DPointer<FrameComponent> FadeOut;
	DPointer<FrameComponent> Background;
	DPointer<LabelComponent> Title;
	DPointer<ButtonComponent> MaxAngleIncrement;
	DPointer<ButtonComponent> MaxAngleDecrement;
	DPointer<ButtonComponent> MaxShiftIncrement;
	DPointer<ButtonComponent> MaxShiftDecrement;
	DPointer<LabelComponent> MaxAngleText;
	DPointer<LabelComponent> MaxShiftText;
	DPointer<ButtonComponent> AcceptButton;
	DPointer<ButtonComponent> CancelButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	static DString FormatFloat (Float value);
	virtual void RefreshMaxAngleText ();
	virtual void RefreshMaxShiftText ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMaxAngleIncrement (ButtonComponent* uiComponent);
	virtual void HandleMaxAngleDecrement (ButtonComponent* uiComponent);
	virtual void HandleMaxShiftIncrement (ButtonComponent* uiComponent);
	virtual void HandleMaxShiftDecrement (ButtonComponent* uiComponent);
	virtual void HandleAcceptClicked (ButtonComponent* uiComponent);
	virtual void HandleCancelClicked (ButtonComponent* uiComponent);
};
SOLITAIRE_END