/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireTheme.h
  Theme that imports various UI resources for the solitaire game, and defines
  UI templates Solitaire menus may use.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class SolitaireTheme : public GuiTheme
{
	DECLARE_CLASS(SolitaireTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString SOLITAIRE_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeSolitaireTheme ();
};
SOLITAIRE_END