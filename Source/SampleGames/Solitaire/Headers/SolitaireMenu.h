/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireMenu.h
  Menu that provides the player the option to start a new game or quit.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class SolitaireMenu : public GuiEntity
{
	DECLARE_CLASS(SolitaireMenu)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<ButtonComponent> StartGameButton;
	DPointer<ButtonComponent> ExitGameButton;
	DPointer<ButtonComponent> OptionsButton;
	DPointer<LabelComponent> Title;

	//Scoreboard bar
	DPointer<FrameComponent> ScoreboardBackground;
	DPointer<LabelComponent> NumPlaysLabel;
	DPointer<LabelComponent> NumVictoriesLabel;
	DPointer<LabelComponent> SpeedRecordLabel;
	DPointer<LabelComponent> CurTimeLabel;
	DPointer<TickComponent> Tick;

private:
	/* Becomes true if the buttons are placed out of the way since there's an active game running. */
	bool IsGameActive;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Refreshes the values on the scoreboard to match with the game's score.
	 */
	virtual void RefreshScoreboard ();

	/**
	 * Pauses/resumes the timer portion of the scoreboard.
	 */
	virtual void PauseScoreboard ();
	virtual void ResumeScoreboard ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ButtonComponent* GetStartButton () const
	{
		return StartGameButton.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Repositions UI elements so it's out of the way from the game board.
	 */
	virtual void SetMenuToGameMode ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleStartGameClicked (ButtonComponent* uiComponent);
	virtual void HandleExitClicked (ButtonComponent* uiComponent);
	virtual void HandleOptionsClicked (ButtonComponent* uiComponent);
	virtual void HandleTick (Float deltaSec);
};
SOLITAIRE_END