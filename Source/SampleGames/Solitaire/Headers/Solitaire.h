/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Solitaire.h
  This application allows the user to play Solitaire.

  This game's primary purpose is to test the Graphics Engine rendering pipeline in
  particular on draw layers and draw order.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"

//Solitaire namespace is within the SD namespace
#define SOLITAIRE_BEGIN SD_BEGIN namespace Solitaire {
#define SOLITAIRE_END } SD_END

#define TICK_GROUP_SOLITAIRE "Solitaire"
#define TICK_GROUP_PRIORITY_SOLITAIRE 500000

SOLITAIRE_BEGIN
extern LogCategory SolitaireLog;
SOLITAIRE_END