/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireDrawLayer.h
  Specialized DrawLayer that draws cards in order based on their stack positions.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class CardColumn;
class CardStation;

class SolitaireDrawLayer : public PlanarDrawLayer
{
	DECLARE_CLASS(SolitaireDrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Int SOLITAIRE_DRAW_LAYER_PRIORITY;

	/* Background that renders behind the cards. */
	DPointer<SpriteComponent> Background;

protected:
	std::vector<CardColumn*> Columns;
	std::vector<CardStation*> Stations;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void RegisterCardColumn (CardColumn* newColumn);
	virtual void RegisterCardStation (CardStation* newStation);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sorts the Rendered cards based on their depth values.
	 */
	virtual void SortRenderedObjects ();

	virtual void RenderCardStations (RenderTarget* renderTarget, Camera* cam);
	virtual void RenderCardColumns (RenderTarget* renderTarget, Camera* cam);

	virtual void PurgeExpiredObjects ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SOLITAIRE_END