/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SolitaireGame.h
  Object that defines the game rules (win conditions, initial placements, and monitors
  player activity.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class Card;
class CardStation;
class VictoryStation;
class CardColumn;

class SolitaireGame : public Object
{
	DECLARE_CLASS(SolitaireGame)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all cards on the table (no joker cards). */
	std::vector<Card*> AllCards;

	std::vector<CardStation*> AllStations;

	/* The station the player may draw cards from. */
	CardStation* DrawStation;

	/* The station the drawn cards are placed. */
	CardStation* StageStation;

	/* The seven locations that make up the center row. */
	std::vector<CardStation*> CentralStations;

	/* The seven columns over the central stations. */
	std::vector<CardColumn*> CentralColumns;

	/* The 4 foundations in the top right corner that the user must place the cards in ascending order to win the game. */
	std::vector<VictoryStation*> VictoryStations;

	/* Timestamp when this game has begun (in seconds). */
	Float StartTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetupNewGame ();
	virtual void ClearGame ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline CardStation* GetDrawStation () const
	{
		return DrawStation;
	}

	inline CardStation* GetStageStation () const
	{
		return StageStation;
	}

	inline const std::vector<CardStation*>& ReadAllStations () const
	{
		return AllStations;
	}

	inline const std::vector<Card*>& ReadAllCards () const
	{
		return AllCards;
	}

	inline const std::vector<CardStation*>& ReadCentralStations () const
	{
		return CentralStations;
	}

	inline const std::vector<CardColumn*>& ReadCentralColumns () const
	{
		return CentralColumns;
	}

	inline const std::vector<VictoryStation*>& ReadVictoryStations () const
	{
		return VictoryStations;
	}

	inline Float GetStartTime () const
	{
		return StartTime;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the cards are in the correct state to win the game.
	 */
	virtual bool HasWonGame () const;

	virtual void ExecuteVictorySequence ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEmptyDrawStationClicked ();
	virtual void HandleDrawReveal (CardStation* station, Card* revealedCard);
	virtual void HandleCentralCardReveal (CardStation* station, Card* revealedCard);
	virtual void HandleVictoryCardPlaced (Card* addedCard);
};
SOLITAIRE_END