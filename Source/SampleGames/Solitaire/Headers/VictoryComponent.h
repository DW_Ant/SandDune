/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VictoryComponent.h
  A very simple component that manipulates the owner's transforms every frame.
=====================================================================
*/

#pragma once

#include "CardStation.h"

SOLITAIRE_BEGIN
class VictoryComponent : public TickComponent
{
	DECLARE_CLASS(VictoryComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Vector2 Velocity;
	Vector2 Acceleration;
	Float RotationRate;
	Float Delay;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual void Tick (Float deltaSec) override;
};
SOLITAIRE_END