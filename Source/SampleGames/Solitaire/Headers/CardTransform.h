/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CardTransform.h
  Nearly identical to the parent class PlanarTransform, this transform allows cards
  to rotate.  The reason the rotation functionality is not added to Engine's PlanarTransform
  is because certain functions (such as IsWithinBounds) do not support Rotations.

  This class is internallized to Solitaire game since the rotations used are subtle, and to
  hint that rotations in planar transforms are not supported.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class CardTransform : public PlanarTransform
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The amount of spin about the origin/pivot point in degrees. */
	Float Rotation;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CardTransform ();
	CardTransform (const Vector2& inPosition, const Vector2& inSize, Float inDepth = 0.f, PlanarTransform* inRelativeTo = nullptr);
	CardTransform (const CardTransform& copyObj);
	virtual ~CardTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetRotation (Float newRotation);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetRotation () const
	{
		return Rotation;
	}
};
SOLITAIRE_END