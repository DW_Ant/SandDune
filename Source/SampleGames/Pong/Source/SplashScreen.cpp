/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SplashScreen.cpp
=====================================================================
*/

#include "PongEngineComponent.h"
#include "PongGame.h"
#include "SplashScreen.h"

IMPLEMENT_CLASS(SD::P::SplashScreen, SD::GuiEntity)
P_BEGIN

void SplashScreen::SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority)
{
	Super::SetupInputComponent(broadcaster, inputPriority);

	if (Input != nullptr)
	{
		Input->AddKeybind(sf::Keyboard::Space, false, SDFUNCTION(this, SplashScreen, HandleStartGame, bool));
	}
}

void SplashScreen::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	LabelComponent* title = LabelComponent::CreateObject();
	if (AddComponent(title))
	{
		title->SetAutoRefresh(false);
		title->SetPosition(Vector2(0.2f, 0.1f));
		title->SetSize(Vector2(0.6f, 0.1f));
		title->SetWrapText(false);
		title->SetClampText(false);
		title->SetCharacterSize(64);
		title->SetHorizontalAlignment(LabelComponent::HA_Center);
		title->SetVerticalAlignment(LabelComponent::VA_Center);
		title->SetText(TXT("P O N G"));
		title->SetAutoRefresh(true);
	}

	LabelComponent* intro = LabelComponent::CreateObject();
	if (AddComponent(intro))
	{
		intro->SetAutoRefresh(false);
		intro->SetPosition(Vector2(0.15f, 0.35f));
		intro->SetSize(Vector2(0.7f, 0.3f));
		intro->SetWrapText(true);
		intro->SetClampText(false);
		intro->SetHorizontalAlignment(LabelComponent::HA_Center);
		intro->SetVerticalAlignment(LabelComponent::VA_Top);
		intro->SetText(translator->TranslateText(TXT("Intro"), TXT("Pong"), TXT("SplashScreen")));
		intro->SetAutoRefresh(true);
	}
}

bool SplashScreen::HandleStartGame ()
{
	PongEngineComponent* pongEngine = PongEngineComponent::Find();
	CHECK(pongEngine != nullptr && pongEngine->GetGame() != nullptr)
	pongEngine->GetGame()->StartGame();

	Destroy();
	return true;
}
P_END