/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Paddle.cpp
=====================================================================
*/

#include "AiPaddle.h"
#include "BouncyBall.h"
#include "Paddle.h"
#include "PongEngineComponent.h"
#include "PongGame.h"
#include "PongHud.h"
#include "PongPerspective.h"

IMPLEMENT_CLASS(SD::P::Paddle, SD::SceneEntity)
P_BEGIN

void Paddle::InitProps ()
{
	Super::InitProps();

	PaddleSize = Vector2(200.f, 150.f);

	ImpactDuration = 0.67f;
	MaxImpactSize = 100.f;
	ImpactTransform = nullptr;
	ImpactColor = nullptr;
	ImpactTick = nullptr;

	ImpactTime = -1.f;
}

void Paddle::BeginObject ()
{
	Super::BeginObject();

	PongEngineComponent* localPongEngine = PongEngineComponent::Find();
	CHECK(localPongEngine != nullptr)

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const Texture* paddleTexture = localTexturePool->GetTexture(HashedString("SampleGames.Pong.Paddle"));

	if (paddleTexture != nullptr)
	{
		Sprite = SpriteComponent::CreateObject();
		if (AddComponent(Sprite))
		{
			Sprite->SetSpriteTexture(paddleTexture);
			Sprite->SetPivot(Vector2(0.5f, 0.5f));
			Sprite->EditBaseSize() = PaddleSize;

			localPongEngine->AddToScene(Sprite.Get());
		}
	}

	ImpactTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(ImpactTransform))
	{
		ImpactColor = ColorRenderComponent::CreateObject();
		if (ImpactTransform->AddComponent(ImpactColor))
		{
			ImpactColor->SetShape(ColorRenderComponent::ST_Circle);
			ImpactColor->SetPivot(Vector2(0.5f, 0.5f));
			ImpactColor->SolidColor = Color(200, 200, 64);
			ImpactColor->SetVisibility(false);
			localPongEngine->AddToScene(ImpactColor);

			ImpactTick = TickComponent::CreateObject(TICK_GROUP_PONG);
			if (ImpactColor->AddComponent(ImpactTick))
			{
				ImpactTick->SetTickHandler(SDFUNCTION_1PARAM(this, Paddle, HandleImpactTick, void, Float));
				ImpactTick->SetTicking(false);
			}
		}
	}
}

void Paddle::SetupInputComp ()
{
	if (HasComponent(InputComponent::SStaticClass(), false))
	{
		PongLog.Log(LogCategory::LL_Warning, TXT("Cannot setup an InputComponent to a paddle that already has an InputComponent."));
		return;
	}

	InputComponent* input = InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->OnMouseMove = SDFUNCTION_3PARAM(this, Paddle, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		input->AddKeybind(sf::Keyboard::Space, false, SDFUNCTION(this, Paddle, HandleServe, bool));
	}
}

void Paddle::SetupBotComp ()
{
	if (HasComponent(AiPaddle::SStaticClass(), false))
	{
		PongLog.Log(LogCategory::LL_Warning, TXT("Cannot setup an AiPaddle component to a paddle that already contains an AiPaddle component."));
		return;
	}

	AiPaddle* comp = AiPaddle::CreateObject();
	if (AddComponent(comp))
	{
		//Noop
	}
}

void Paddle::DisplayImpactPoint (const Vector3& impactPoint)
{
	if (ImpactTransform == nullptr || ImpactColor == nullptr || ImpactTick == nullptr)
	{
		PongLog.Log(LogCategory::LL_Warning, TXT("Unable to display impact point on a paddle since it's missing the essential components used for displaying the impact point."));
		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	ImpactTime = localEngine->GetElapsedTime();

	ImpactColor->SolidColor.Source.a = 255;
	ImpactColor->SetVisibility(true);
	ImpactColor->SetBaseSize(Vector2::ZERO_VECTOR);

	ImpactTransform->SetTranslation(impactPoint);
	ImpactTick->SetTicking(true);
}

void Paddle::SetPaddleSize (const Vector2& newPaddleSize)
{
	PaddleSize = newPaddleSize;
	if (Sprite != nullptr)
	{
		Sprite->EditBaseSize() = PaddleSize;
	}
}

void Paddle::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent, const Vector2& deltaMove)
{
	Vector3 newPos = GetTranslation();

	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	if (localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr)
	{
		PongEngineComponent* localPongEngine = PongEngineComponent::Find();
		CHECK(localPongEngine != nullptr)
		if (PongPerspective* cam = localPongEngine->GetCamera())
		{
			Rotator dir;
			Vector3 projectedMouse;
			cam->CalculatePixelProjection(localGraphics->GetPrimaryWindow()->GetWindowSize(), Vector2(moveEvent.x, moveEvent.y), OUT dir, OUT projectedMouse);

			Float multiplier = ReadTranslation().Y / cam->GetFocalLength();
			projectedMouse *= multiplier;

			newPos.X = projectedMouse.X;
			newPos.Z = projectedMouse.Z;
		}
	}
	
	SetTranslation(newPos);
}

bool Paddle::HandleServe ()
{
	//Find the ball
	PongEngineComponent* localPongEngine = PongEngineComponent::Find();
	CHECK(localPongEngine != nullptr)

	if (localPongEngine->GetGame())
	{
		if (BouncyBall* ball = localPongEngine->GetGame()->GetBall())
		{
			if (ball->ReadVelocity().IsEmpty() && Float::Abs(ball->ReadTranslation().Y - ReadTranslation().Y) <= ball->GetBallRadius()) //The ball must be in serve mode, and it must be near this paddle.
			{
				Vector2 ballCollision = ball->CalcPaddleCollision(this);
				if (Float::Abs(ballCollision.X) < 1.f && Float::Abs(ballCollision.Y) < 1.f)
				{
					ball->ServeBall(ballCollision);
					DisplayImpactPoint(Vector3(ballCollision.X * PaddleSize.X, 0.f, ballCollision.Y * PaddleSize.Y) * 0.5f);
					if (PongHud* hud = localPongEngine->GetHud())
					{
						hud->HideServePrompt();
					}

					return true;
				}
			}
		}
	}

	return false;
}

void Paddle::HandleImpactTick (Float deltaSec)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	Float ratio = (localEngine->GetElapsedTime() - ImpactTime) / ImpactDuration;
	if (ratio >= 1.f)
	{
		if (ImpactColor != nullptr)
		{
			ImpactColor->SetVisibility(false);
		}

		if (ImpactTick != nullptr)
		{
			ImpactTick->SetTicking(false);
		}
	}
	else if (ImpactColor != nullptr)
	{
		Float curSize = Utils::Lerp<Float>(ratio, 0.f, MaxImpactSize);
		Float curAlpha = Utils::Lerp<Float>(ratio, 255, 0);

		ImpactColor->SetBaseSize(Vector2(curSize, curSize));
		ImpactColor->SolidColor.Source.a = curAlpha.ToInt().ToUnsignedInt();
	}
}
P_END