/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongPerspective.cpp
=====================================================================
*/

#include "PongPerspective.h"

IMPLEMENT_CLASS(SD::P::PongPerspective, SD::SceneCamera)
P_BEGIN

void PongPerspective::InitProps ()
{
	Super::InitProps();

	FocalLength = 100.f; //1 meter
	ListenComp = nullptr;
}

void PongPerspective::BeginObject ()
{
	Super::BeginObject();

	ListenComp = PlayerListenerComponent::CreateObject();
	if (AddComponent(ListenComp))
	{
		//Noop
	}
}

void PongPerspective::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	//Pong perspective always looks along the positive Y-axis.
	static const Rotator rotation(Vector3(0.f, 1.f, 0.f));
	outDirection = rotation;

	outLocation = GetAbsTranslation();
	outLocation.X = ReadAbsTranslation().X - (viewportSize.X * 0.5f) + pixelPos.X;
	outLocation.Z = ReadAbsTranslation().Z + (viewportSize.Y * 0.5f) - pixelPos.Y; //Invert Z since positive Y points towards negative Z.
}

void PongPerspective::ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const
{
	Float depth = transform.ReadAbsTranslation().Y - ReadAbsTranslation().Y;
	if (depth == 0.f)
	{
		depth = 0.00001f; //Avoid dividing by zero. Project the object so it's really close to the camera instead.
	}

	Float projectedX = (transform.ReadAbsTranslation().X - ReadAbsTranslation().X) * FocalLength / depth;
	Float projectedY = (transform.ReadAbsTranslation().Z - ReadAbsTranslation().Z) * FocalLength / depth;
	projectedY *= -1.f; //Positive Z moves towards the negative Y direction.

	projectedX += (viewportSize.X * 0.5f);
	projectedY += (viewportSize.Y * 0.5f);

	outProjectionData.Position = sf::Vector2f(projectedX.Value, projectedY.Value);
	outProjectionData.Rotation = 0.f;
	outProjectionData.Scale = sf::Vector2f(1.f, 1.f) * (FocalLength / depth).Value;
}

Float PongPerspective::GetDistSquaredToPoint (const Vector3& worldLocation) const
{
	return Float::Pow(Float::Abs(worldLocation.Y - ReadAbsTranslation().Y), 2.f);
}
P_END