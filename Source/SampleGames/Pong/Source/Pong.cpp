/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Pong.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Pong.h"
#include "PongEngineComponent.h"

SD::LogCategory SD::P::PongLog(TXT("Pong"), SD::LogCategory::VERBOSITY_DEFAULT,
	SD::LogCategory::FLAG_STANDARD_OUTPUT |
	SD::LogCategory::FLAG_OS_OUTPUT |
	SD::LogCategory::FLAG_LOG_FILE |
	SD::LogCategory::FLAG_OUTPUT_WINDOW |
	SD::LogCategory::FLAG_CONSOLE_MSG);

//Forward function declarations
int BeginSandDune (TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	TCHAR* args = GetCommandLine();
	return BeginSandDune(&args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (TCHAR* args[])
{
	SD::ProjectName = TXT("Pong");

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(args[0]);
	engine->SetMinDeltaTime(0.f); //No limit

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	bool bMultiThread = engine->HasCmdLineSwitch(TXT("-MultiThread"), SD::DString::CC_IgnoreCase);

	//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
	std::vector<SD::EngineComponent*> engineComponents;
	engineComponents.push_back(new SD::AudioEngineComponent(bMultiThread));
	engineComponents.push_back(new SD::GraphicsEngineComponent());
	engineComponents.push_back(new SD::GuiEngineComponent());
	engineComponents.push_back(new SD::InputEngineComponent());
	engineComponents.push_back(new SD::LocalizationEngineComponent());
	engineComponents.push_back(new SD::LoggerEngineComponent());
	engineComponents.push_back(new SD::P::PongEngineComponent());
	engineComponents.push_back(new SD::RandomEngineComponent());

#ifdef WITH_MULTI_THREAD
	if (bMultiThread)
	{
		//Switching the AudioEngineComponent to MultiThreaded mode is overkill for this simple game, but it's good to have a simple example to test on.
		engineComponents.push_back(new SD::MultiThreadEngineComponent());
	}
#endif

	engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

	return RunMainLoop();
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}