/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongHud.cpp
=====================================================================
*/

#include "PongHud.h"

IMPLEMENT_CLASS(SD::P::PongHud, SD::GuiEntity)
P_BEGIN

void PongHud::InitProps ()
{
	Super::InitProps();

	RedScore = nullptr;
	BlueScore = nullptr;
	VolumeLabel = nullptr;
	PauseFrame = nullptr;
	ServePrompt = nullptr;
	ServeTick = nullptr;
}

void PongHud::SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority)
{
	Super::SetupInputComponent(broadcaster, inputPriority);

	if (Input != nullptr)
	{
		Input->AddKeybind(sf::Keyboard::Hyphen, true, SDFUNCTION(this, PongHud, HandleDecreaseVolume, bool));
		Input->AddKeybind(sf::Keyboard::Equal, true, SDFUNCTION(this, PongHud, HandleIncreaseVolume, bool));
		Input->AddKeybind(sf::Keyboard::Pause, false, SDFUNCTION(this, PongHud, HandleTogglePause, bool));
	}
}

void PongHud::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	const Texture* leftBorder = localTexturePool->GetTexture(HashedString("SampleGames.Pong.HudBorder"));
	const Texture* rightBorder = localTexturePool->GetTexture(HashedString("SampleGames.Pong.HudBorderRight"));

	FrameComponent* redBorders = FrameComponent::CreateObject();
	if (AddComponent(redBorders))
	{
		redBorders->SetPosition(Vector2(0.35f, 0.f));
		redBorders->SetSize(Vector2(0.15f, 0.04f));

		if (leftBorder != nullptr)
		{
			redBorders->SetCenterTexture(leftBorder);
		}

		FrameComponent* redScoreBackground = FrameComponent::CreateObject();
		if (redBorders->AddComponent(redScoreBackground))
		{
			redScoreBackground->SetPosition(Vector2(0.25f, 0.08f));
			redScoreBackground->SetSize(Vector2(0.5f, 0.8f));

			RedScore = LabelComponent::CreateObject();
			if (redScoreBackground->AddComponent(RedScore))
			{
				RedScore->SetAutoRefresh(false);
				RedScore->SetWrapText(false);
				RedScore->SetClampText(false);
				RedScore->SetHorizontalAlignment(LabelComponent::HA_Center);
				RedScore->SetVerticalAlignment(LabelComponent::VA_Center);
				RedScore->GetRenderComponent()->SetFontColor(sf::Color(150, 0, 0));
				RedScore->SetText(TXT("0"));
				RedScore->SetAutoRefresh(true);
			}
		}
	}

	FrameComponent* blueBorders = FrameComponent::CreateObject();
	if (AddComponent(blueBorders))
	{
		blueBorders->SetPosition(Vector2(0.5f, 0.f));
		blueBorders->SetSize(Vector2(0.15f, 0.04f));

		if (rightBorder != nullptr)
		{
			blueBorders->SetCenterTexture(rightBorder);
		}

		FrameComponent* blueScoreBackground = FrameComponent::CreateObject();
		if (blueBorders->AddComponent(blueScoreBackground))
		{
			blueScoreBackground->SetPosition(Vector2(0.25f, 0.08f));
			blueScoreBackground->SetSize(Vector2(0.5f, 0.8f));

			BlueScore = LabelComponent::CreateObject();
			if (blueScoreBackground->AddComponent(BlueScore))
			{
				BlueScore->SetAutoRefresh(false);
				BlueScore->SetWrapText(false);
				BlueScore->SetClampText(false);
				BlueScore->SetHorizontalAlignment(LabelComponent::HA_Center);
				BlueScore->SetVerticalAlignment(LabelComponent::VA_Center);
				BlueScore->GetRenderComponent()->SetFontColor(sf::Color(0, 0, 225));
				BlueScore->SetText(TXT("0"));
				BlueScore->SetAutoRefresh(true);
			}
		}
	}

	ServePrompt = LabelComponent::CreateObject();
	if (AddComponent(ServePrompt))
	{
		ServePrompt->SetAutoRefresh(false);
		ServePrompt->SetPosition(Vector2(0.2f, 0.75f));
		ServePrompt->SetSize(Vector2(0.6f, 0.1f));
		ServePrompt->SetWrapText(true);
		ServePrompt->SetClampText(false);
		ServePrompt->SetHorizontalAlignment(LabelComponent::HA_Center);
		ServePrompt->SetText(translator->TranslateText(TXT("ServePrompt"), TXT("Pong"), TXT("PongHud")));
		ServePrompt->SetAutoRefresh(true);

		ServeTick = TickComponent::CreateObject(TICK_GROUP_PONG);
		if (ServePrompt->AddComponent(ServeTick))
		{
			ServeTick->SetTickHandler(SDFUNCTION_1PARAM(this, PongHud, HandleTogglePrompt, void, Float));
			ServeTick->SetTickInterval(0.4f);
		}
	}

	PauseFrame = FrameComponent::CreateObject();
	if (AddComponent(PauseFrame))
	{
		PauseFrame->SetPosition(Vector2::ZERO_VECTOR);
		PauseFrame->SetSize(Vector2(1.f, 1.f));
		PauseFrame->SetVisibility(false);
		PauseFrame->SetCenterColor(Color(0, 0, 0, 160));

		LabelComponent* pauseText = LabelComponent::CreateObject();
		if (PauseFrame->AddComponent(pauseText))
		{
			pauseText->SetAutoRefresh(false);
			pauseText->SetPosition(Vector2(0.f, 0.35f));
			pauseText->SetSize(Vector2(1.f, 0.5f));
			pauseText->SetCharacterSize(80);
			pauseText->SetWrapText(false);
			pauseText->SetClampText(false);
			pauseText->SetHorizontalAlignment(LabelComponent::HA_Center);
			pauseText->SetText(translator->TranslateText(TXT("PauseText"), TXT("Pong"), TXT("PongHud")));
			pauseText->SetAutoRefresh(true);
		}
	}

	FrameComponent* volumeTexture = FrameComponent::CreateObject();
	if (AddComponent(volumeTexture))
	{
		volumeTexture->SetSize(Vector2(0.125f, 0.06f));
		volumeTexture->SetAnchorTop(0.f);
		volumeTexture->SetAnchorRight(0.f);
		if (rightBorder != nullptr)
		{
			volumeTexture->SetCenterTexture(leftBorder);
		}

		FrameComponent* volumeFrame = FrameComponent::CreateObject();
		if (volumeTexture->AddComponent(volumeFrame))
		{
			volumeFrame->SetPosition(Vector2(0.2f, 0.1f));
			volumeFrame->SetSize(Vector2(0.6f, 0.8f));

			LabelComponent* volumeHeader = LabelComponent::CreateObject();
			if (volumeFrame->AddComponent(volumeHeader))
			{
				volumeHeader->SetAutoRefresh(false);
				volumeHeader->SetPosition(Vector2::ZERO_VECTOR);
				volumeHeader->SetSize(Vector2(1.f, 0.5f));
				volumeHeader->SetWrapText(false);
				volumeHeader->SetClampText(false);
				volumeHeader->SetCharacterSize(13);
				volumeHeader->SetHorizontalAlignment(LabelComponent::HA_Center);
				volumeHeader->SetVerticalAlignment(LabelComponent::VA_Center);
				volumeHeader->SetText(translator->TranslateText(TXT("VolumeHeader"), TXT("Pong"), TXT("PongHud")));
				volumeHeader->SetAutoRefresh(true);
			}

			VolumeLabel = LabelComponent::CreateObject();
			if (volumeFrame->AddComponent(VolumeLabel))
			{
				VolumeLabel->SetAutoRefresh(false);
				VolumeLabel->SetPosition(Vector2(0.f, 0.5f));
				VolumeLabel->SetSize(Vector2(1.f, 0.5f));
				VolumeLabel->SetWrapText(false);
				VolumeLabel->SetClampText(false);
				VolumeLabel->SetCharacterSize(15);
				VolumeLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				VolumeLabel->SetVerticalAlignment(LabelComponent::VA_Center);
				UpdateVolumeLabel();
				VolumeLabel->SetAutoRefresh(true);
			}
		}
	}
}

void PongHud::DisplayServePrompt ()
{
	if (ServeTick != nullptr)
	{
		ServeTick->SetTicking(true);
	}
}

void PongHud::HideServePrompt ()
{
	if (ServePrompt != nullptr)
	{
		ServePrompt->SetVisibility(false);
	}

	if (ServeTick != nullptr)
	{
		ServeTick->SetTicking(false);
	}
}

void PongHud::SetRedScore (Int newScore)
{
	if (RedScore != nullptr)
	{
		RedScore->SetText(newScore.ToString());
	}
}

void PongHud::SetBlueScore (Int newScore)
{
	if (BlueScore != nullptr)
	{
		BlueScore->SetText(newScore.ToString());
	}
}

void PongHud::UpdateVolumeLabel ()
{
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	if (VolumeLabel != nullptr)
	{
		Float newVolume = localAudioEngine->GetMasterVolume() * 100.f;
		newVolume.RoundInline();
		VolumeLabel->SetText(DString::CreateFormattedString(TXT("%s%"), newVolume.ToInt()));
	}
}

void PongHud::PlayBeep (AudioEngineComponent* audioEngine)
{
	CHECK(audioEngine != nullptr)

	AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
	CHECK(localAudioPool != nullptr)
	const AudioAsset* asset = localAudioPool->GetAudio(HashedString("SampleGames.Pong.VolSound"));
	if (asset != nullptr)
	{
		audioEngine->PlayStaticSound(asset, 1.f, 1.f);
	}
}

bool PongHud::HandleDecreaseVolume ()
{
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	localAudioEngine->SetMasterVolume(Utils::Max<Float>(localAudioEngine->GetMasterVolume() - 0.05f, 0.f));
	UpdateVolumeLabel();

	PlayBeep(localAudioEngine);

	return true;
}

bool PongHud::HandleIncreaseVolume ()
{
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)

	localAudioEngine->SetMasterVolume(Utils::Min<Float>(localAudioEngine->GetMasterVolume() + 0.05f, 1.f));
	UpdateVolumeLabel();

	PlayBeep(localAudioEngine);

	return true;
}

bool PongHud::HandleTogglePause ()
{
	if (PauseFrame == nullptr)
	{
		return false;
	}

	bool shouldPause = !PauseFrame->IsVisible();
	PauseFrame->SetVisibility(shouldPause);

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	TickGroup* pongTick = localEngine->FindTickGroup(TICK_GROUP_PONG);
	if (pongTick != nullptr)
	{
		pongTick->SetTicking(!shouldPause);
	}

	//Pause/Unpause the general sounds channel
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	if (localAudio != nullptr)
	{
		Float dilation = shouldPause ? 0.f : 1.f;
		localAudio->SetChannelDilation(AudioEngineComponent::AUDIO_CHANNEL_GENERAL, dilation);
	}

	return true;
}

void PongHud::HandleTogglePrompt (Float deltaSec)
{
	if (ServePrompt != nullptr)
	{
		ServePrompt->SetVisibility(!ServePrompt->IsVisible());
	}
}
P_END