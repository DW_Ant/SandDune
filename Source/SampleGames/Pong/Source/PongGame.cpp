/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongGame.cpp
=====================================================================
*/

#include "AiPaddle.h"
#include "BouncyBall.h"
#include "Paddle.h"
#include "PongEngineComponent.h"
#include "PongGame.h"
#include "PongHud.h"

IMPLEMENT_CLASS(SD::P::PongGame, SD::Entity)
P_BEGIN

void PongGame::InitProps ()
{
	Super::InitProps();

	PlayerScore = 0;
	BotScore = 0;
	Borders = Aabb(1000.f, 1000.f, 1000.f, Vector3(0.f, 650.f, 0.f));

	bHasStarted = false;
}

void PongGame::Destroyed ()
{
	if (Ball != nullptr)
	{
		Ball->Destroy();
		Ball = nullptr;
	}

	if (PlayerPaddle != nullptr)
	{
		PlayerPaddle->Destroy();
		PlayerPaddle = nullptr;
	}

	if (BotPaddle != nullptr)
	{
		BotPaddle->Destroy();
		BotPaddle = nullptr;
	}

	Super::Destroyed();
}

void PongGame::StartGame ()
{
	if (bHasStarted)
	{
		PongLog.Log(LogCategory::LL_Warning, TXT("The PongGame has already started."));
		return;
	}
	bHasStarted = true;

	PlayerPaddle = Paddle::CreateObject();
	PlayerPaddle->SetTranslation(Vector3(0.f, Borders.GetLeft() - 5.f, 0.f)); //-5 to ensure this paddle is rendered in front of the outline walls.
	PlayerPaddle->SetupInputComp();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr && localEngine->HasCmdLineSwitch(TXT("-BigPaddle"), DString::CC_IgnoreCase))
	{
		PlayerPaddle->SetPaddleSize(PlayerPaddle->GetPaddleSize() * 3.f);
	}

	if (SpriteComponent* sprite = PlayerPaddle->GetSprite())
	{
		sprite->GetSprite()->setColor(sf::Color(200, 0, 0));
	}

	BotPaddle = Paddle::CreateObject();
	BotPaddle->SetTranslation(Vector3(0.f, Borders.GetRight(), 0.f));
	BotPaddle->SetupBotComp();
	if (SpriteComponent* sprite = BotPaddle->GetSprite())
	{
		sprite->GetSprite()->setColor(sf::Color(96, 96, 255));
	}

	CreateBall();

	//Create the walls
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	const Texture* wallTexture = localTexturePool->GetTexture(HashedString("SampleGames.Pong.Outline"));

	if (wallTexture != nullptr)
	{
		PongEngineComponent* localPongEngine = PongEngineComponent::Find();
		CHECK(localPongEngine != nullptr)

		Float outlineInterval = 100.f; //Outline every meter
		for (Float y = Borders.GetRight(); y >= Borders.GetLeft(); y -= outlineInterval)
		{
			SceneTransformComponent* transformComp = SceneTransformComponent::CreateObject();
			if (AddComponent(transformComp))
			{
				transformComp->SetTranslation(Vector3(0.f, y, 0.f));

				SpriteComponent* sprite = SpriteComponent::CreateObject();
				if (transformComp->AddComponent(sprite))
				{
					sprite->SetSpriteTexture(wallTexture);
					sprite->EditBaseSize() = Vector2(Borders.Depth, Borders.Height) * 1.25f; //Increase the size of the sprites to ensure they're rendered behind the ball
					sprite->SetPivot(Vector2(0.5f, 0.5f));

					Float ratio = (y - Borders.GetLeft()) / (Borders.GetRight() - Borders.GetLeft());
					Color lerpColor = Color::Lerp(ratio, Color(200, 128, 128), Color(128, 128, 255));
					sprite->GetSprite()->setColor(lerpColor.Source);

					localPongEngine->AddToScene(sprite);
				}
			}
		}
	}

	//Play a thump sound
	AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
	CHECK(localAudioPool != nullptr)
	if (const AudioAsset* thumpAsset = localAudioPool->GetAudio(HashedString("SampleGames.Pong.ThumpSound")))
	{
		AudioEngineComponent* audioEngine = AudioEngineComponent::Find();
		CHECK(audioEngine != nullptr)
		audioEngine->PlayStaticSound(thumpAsset, 0.5f, 0.75f);
	}
}

void PongGame::LeftScore ()
{
	PlayerScore++;

	if (Ball != nullptr && BotPaddle != nullptr)
	{
		Ball->SetTranslation(Vector3::ZERO_VECTOR);
		Ball->EditTranslation().Y = BotPaddle->ReadTranslation().Y;
		Ball->SetVelocity(Vector3::ZERO_VECTOR);
		
		//Notify the bot to serve
		if (AiPaddle* bot = dynamic_cast<AiPaddle*>(BotPaddle->FindComponent(AiPaddle::SStaticClass())))
		{
			bot->EnterServeMode();
			bot->EvaluateDifficulty(BotScore, PlayerScore);
		}
	}

	PongEngineComponent* localPongEngine = PongEngineComponent::Find();
	CHECK(localPongEngine != nullptr)
	if (PongHud* hud = localPongEngine->GetHud())
	{
		hud->SetRedScore(PlayerScore);
	}
}

void PongGame::RightScore ()
{
	BotScore++;
	if (BotPaddle != nullptr)
	{
		if (AiPaddle* paddle = dynamic_cast<AiPaddle*>(BotPaddle->FindComponent(AiPaddle::SStaticClass())))
		{
			paddle->EvaluateDifficulty(BotScore, PlayerScore);
		}
	}

	if (Ball != nullptr && PlayerPaddle != nullptr)
	{
		Ball->SetTranslation(Vector3::ZERO_VECTOR);
		Ball->EditTranslation().Y = PlayerPaddle->ReadTranslation().Y + 5.f;
		Ball->SetVelocity(Vector3::ZERO_VECTOR);
	}

	PongEngineComponent* localPongEngine = PongEngineComponent::Find();
	CHECK(localPongEngine != nullptr)
	if (PongHud* hud = localPongEngine->GetHud())
	{
		hud->SetBlueScore(BotScore);
		hud->DisplayServePrompt();
	}
}

void PongGame::CreateBall ()
{
	if (Ball != nullptr)
	{
		return;
	}

	CHECK(PlayerPaddle != nullptr)

	Ball = BouncyBall::CreateObject();
	Ball->SetTranslation(Vector3(0.f, PlayerPaddle->ReadTranslation().Y + 5.f, 0.f));
	Ball->SetGame(this);
}
P_END