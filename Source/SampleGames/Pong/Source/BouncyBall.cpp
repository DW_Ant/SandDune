/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BouncyBall.cpp
=====================================================================
*/

#include "BouncyBall.h"
#include "Paddle.h"
#include "PongEngineComponent.h"
#include "PongGame.h"

IMPLEMENT_CLASS(SD::P::BouncyBall, SD::SceneEntity)
P_BEGIN

void BouncyBall::InitProps ()
{
	Super::InitProps();

	BallRadius = 25.f;
	ServeSpeed = 335.f;
	Velocity = Vector3::ZERO_VECTOR;
	AccelerationRate = 26.f;
	Game = nullptr;
	Sprite = nullptr;
	DepthHighlighter = nullptr;
	Audio = nullptr;

	ImpactSound = nullptr;
}

void BouncyBall::BeginObject ()
{
	Super::BeginObject();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const Texture* ballTexture = localTexturePool->GetTexture(HashedString("SampleGames.Pong.Sphere"));

	PongEngineComponent* pongEngine = PongEngineComponent::Find();
	CHECK(pongEngine != nullptr)

	if (ballTexture != nullptr)
	{
		Sprite = SpriteComponent::CreateObject();
		if (AddComponent(Sprite))
		{
			if (ballTexture != nullptr)
			{
				Sprite->SetSpriteTexture(ballTexture);
				Sprite->EditBaseSize() = Vector2(BallRadius, BallRadius) * 2.f;
				Sprite->SetPivot(Vector2(0.5f, 0.5f));
				pongEngine->AddToScene(Sprite);
			}
		}
	}

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_PONG);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, BouncyBall, HandleTick, void, Float));
	}

	Audio = AudioComponent::CreateObject();
	if (AddComponent(Audio))
	{
		Audio->SetVolumeMultiplier(0.5f);
		Audio->SetInnerRadius(150.f);
		Audio->SetOuterRadius(1500.f);

#ifdef WITH_MULTI_THREAD
		Audio->SetMaxWaitTime(0.05f); //Set to a high frequency since the fast moving ball is very time sensitive and the main focus of the game.
#endif
	}

	const Texture* outlineTexture = localTexturePool->GetTexture(HashedString("SampleGames.Pong.Outline"));
	if (outlineTexture != nullptr)
	{
		DepthHighlighter = SceneEntity::CreateObject();
		DepthHighlighter->SetTranslation(GetTranslation() * Vector3(0.f, 1.f, 0.f));
		SpriteComponent* sprite = SpriteComponent::CreateObject();
		if (DepthHighlighter->AddComponent(sprite))
		{
			sprite->SetSpriteTexture(outlineTexture);
			
			sprite->SetPivot(Vector2(0.5f, 0.5f));
			sprite->GetSprite()->setColor(sf::Color(255, 255, 96, 196));
			pongEngine->AddToScene(sprite);
		}
	}

}

void BouncyBall::Destroyed ()
{
	if (DepthHighlighter != nullptr)
	{
		DepthHighlighter->Destroy();
		DepthHighlighter = nullptr;
	}

	Super::Destroyed();
}

Vector2 BouncyBall::CalcPaddleCollision (Paddle* paddle) const
{
	CHECK(paddle != nullptr)

	Vector3 deltaPos = ReadTranslation() - paddle->ReadTranslation();
	Vector2 results;
	results.X = deltaPos.X / ((paddle->ReadPaddleSize().X * 0.5f) + BallRadius);
	results.Y = deltaPos.Z / ((paddle->ReadPaddleSize().Y * 0.5f) + BallRadius);

	return results;
}

void BouncyBall::ServeBall (const Vector2& direction)
{
	Vector3 newVelocity(0.f, ServeSpeed, 0.f);
	newVelocity.X += (ServeSpeed * direction.X);
	newVelocity.Z += (ServeSpeed * direction.Y);
	SetVelocity(newVelocity);

	AudioAssetPool* audioPool = AudioAssetPool::FindAudioAssetPool();
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr && audioPool != nullptr)

	const AudioAsset* serveSound = audioPool->GetAudio(HashedString("SampleGames.Pong.ServeSound"));
	const AudioAsset* thumpSound = audioPool->GetAudio(HashedString("SampleGames.Pong.ThumpSound"));
	if (serveSound != nullptr)
	{
		localAudioEngine->PlayStaticSound(serveSound, 1.f, 1.f);
	}

	if (thumpSound != nullptr)
	{
		localAudioEngine->PlayStaticSound(thumpSound, 1.f, 1.f);
	}

	const AudioAsset* moveSound = audioPool->GetAudio(HashedString("SampleGames.Pong.MoveBallSound"));
	if (moveSound != nullptr && Audio != nullptr)
	{
		Audio->PlaySound(moveSound, 0.5f);
	}
}

void BouncyBall::SetVelocity (const Vector3& newVelocity)
{
	Velocity = newVelocity;
}

void BouncyBall::SetGame (PongGame* newGame)
{
	Game = newGame;

	if (DepthHighlighter != nullptr)
	{
		SpriteComponent* sprite = dynamic_cast<SpriteComponent*>(DepthHighlighter->FindComponent(SpriteComponent::SStaticClass(), false));
		if (sprite != nullptr)
		{
			sprite->EditBaseSize() = Vector2(Game->ReadBorders().Depth, Game->ReadBorders().Height) * 1.25f;
		}
	}
}

bool BouncyBall::FindImpactSound ()
{
	AudioAssetPool* localAudioAsset = AudioAssetPool::FindAudioAssetPool();
	if (localAudioAsset != nullptr)
	{
		ImpactSound = localAudioAsset->GetAudio(HashedString("SampleGames.Pong.ImpactSound"));
	}

	return (ImpactSound != nullptr);
}

void BouncyBall::PlayScoreSound ()
{
	AudioEngineComponent* localAudio = AudioEngineComponent::Find();
	CHECK(localAudio != nullptr && Audio != nullptr)

	AudioAssetPool* localAudioAsset = AudioAssetPool::FindAudioAssetPool();
	if (localAudioAsset != nullptr)
	{
		if (const AudioAsset* asset = localAudioAsset->GetAudio(HashedString("SampleGames.Pong.ScoreSound")))
		{
			Audio->PlayDetachedSound(asset, 1.f);
		}
	}
}

void BouncyBall::ProcessCollision ()
{
	if (ImpactSound == nullptr && !FindImpactSound())
	{
		return;
	}

	CHECK(Game != nullptr && Audio != nullptr)
	const Vector3& curPosition = ReadAbsTranslation();

	//A primitive solution to handle collision
	if (Velocity.X > 0.f && curPosition.X > Game->ReadBorders().GetForward())
	{
		Velocity.X *= -1.f;
		Audio->PlayDetachedSound(ImpactSound, 1.f);
	}
	else if (Velocity.X < 0.f && curPosition.X < Game->ReadBorders().GetBackward())
	{
		Velocity.X *= -1.f;
		Audio->PlayDetachedSound(ImpactSound, 1.f);
	}

	if (Velocity.Y > 0.f && curPosition.Y > Game->ReadBorders().GetRight())
	{
		if (Paddle* paddle = Game->GetRightPaddle())
		{
			Vector2 paddleCollision = CalcPaddleCollision(paddle);
			if (Float::Abs(paddleCollision.X) <= 1.f && Float::Abs(paddleCollision.Y) <= 1.f)
			{
				//Collided against the paddle. Adjust the ball velocity based on how far it is from the paddle center. The further out it is, the more the ball shifts in that direction.
				Float vSize = Velocity.VSize();
				Velocity.X += (vSize * paddleCollision.X);
				Velocity.Z += (vSize * paddleCollision.Y);
				Velocity.Y *= -1.f;
				Audio->PlayDetachedSound(ImpactSound, 1.f);

				paddle->DisplayImpactPoint(Vector3(paddleCollision.X * paddle->ReadPaddleSize().X, 0.f, paddleCollision.Y * paddle->ReadPaddleSize().Y) * 0.5f);
			}
			else
			{
				Game->LeftScore();
				Velocity = Vector3::ZERO_VECTOR;
				if (Audio != nullptr)
				{
					PlayScoreSound();
					Audio->InterruptSounds();
				}
			}
		}
	}
	else if (Velocity.Y < 0.f && curPosition.Y < Game->ReadBorders().GetLeft())
	{
		if (Paddle* paddle = Game->GetLeftPaddle())
		{
			Vector2 paddleCollision = CalcPaddleCollision(paddle);
			if (Float::Abs(paddleCollision.X) <= 1.f && Float::Abs(paddleCollision.Y) <= 1.f)
			{
				//Collided against the paddle. Adjust the ball velocity based on how far it is from the paddle center. The further out it is, the more the ball shifts in that direction.
				Float vSize = Velocity.VSize();
				Velocity.X += (vSize * paddleCollision.X);
				Velocity.Z += (vSize * paddleCollision.Y);
				Velocity.Y *= -1.f;
				Audio->PlayDetachedSound(ImpactSound, 1.f);

				paddle->DisplayImpactPoint(Vector3(paddleCollision.X * paddle->ReadPaddleSize().X, 0.f, paddleCollision.Y * paddle->ReadPaddleSize().Y) * 0.5f);
			}
			else
			{
				Game->RightScore();
				Velocity = Vector3::ZERO_VECTOR;
				if (Audio != nullptr)
				{
					PlayScoreSound();
					Audio->InterruptSounds();
				}
			}
		}
	}

	if (Velocity.Z > 0.f && curPosition.Z > Game->ReadBorders().GetUp())
	{
		Velocity.Z *= -1.f;
		Audio->PlayDetachedSound(ImpactSound, 1.f);
	}
	else if (Velocity.Z < 0.f && curPosition.Z < Game->ReadBorders().GetDown())
	{
		Velocity.Z *= -1.f;
		Audio->PlayDetachedSound(ImpactSound, 1.f);
	}
}

void BouncyBall::HandleTick (Float deltaSec)
{
	if (Game == nullptr)
	{
		return;
	}

	Vector3 newPosition = GetTranslation();
	if (!Velocity.IsEmpty())
	{
		ProcessCollision();

		if (!Velocity.IsEmpty())
		{
			//Gradually accelerate the ball
			Float dirMultiplier = (Velocity.Y > 0.f) ? 1.f : -1.f;
			Velocity.Y += (dirMultiplier * AccelerationRate * deltaSec);

			newPosition += (Velocity * deltaSec);

			SetTranslation(newPosition);
		}
	}

	//Update the highlighter's position
	if (DepthHighlighter != nullptr)
	{
		DepthHighlighter->EditTranslation().Y = newPosition.Y;
	}
}
P_END