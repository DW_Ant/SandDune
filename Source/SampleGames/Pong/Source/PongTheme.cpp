/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongTheme.cpp
=====================================================================
*/

#include "PongTheme.h"

IMPLEMENT_CLASS(SD::P::PongTheme, SD::GuiTheme)
P_BEGIN

const DString PongTheme::PONG_STYLE_NAME(TXT("PongStyle"));

void PongTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	InitializePongStyle();
}

void PongTheme::InitializePongStyle ()
{
	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	CHECK(defaultStyle != nullptr)

	SStyleInfo pongStyle = CreateStyleFrom(*defaultStyle);
	pongStyle.Name = PONG_STYLE_NAME;

	sf::Color fontColor = sf::Color(200, 200, 255);

	//Frames are used as black foregrounds over other sprites.
	FrameComponent* frameTemplate = dynamic_cast<FrameComponent*>(FindTemplate(&pongStyle, FrameComponent::SStaticClass()));
	if (frameTemplate != nullptr)
	{
		frameTemplate->SetLockedFrame(true);
		frameTemplate->SetCenterColor(Color::BLACK);
		frameTemplate->SetBorderThickness(0.f);

		if (BorderRenderComponent* borderComp = frameTemplate->GetBorderComp())
		{
			borderComp->Destroy();
		}
	}

	LabelComponent* labelTemplate = dynamic_cast<LabelComponent*>(FindTemplate(&pongStyle, LabelComponent::SStaticClass()));
	if (labelTemplate != nullptr)
	{
		labelTemplate->SetCharacterSize(26);

		if (labelTemplate->GetRenderComponent() != nullptr)
		{
			labelTemplate->GetRenderComponent()->SetFontColor(fontColor);
		}
	}

	Styles.emplace_back(pongStyle);
}
P_END