/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AiPaddle.cpp
=====================================================================
*/

#include "AiPaddle.h"
#include "BouncyBall.h"
#include "Paddle.h"
#include "PongEngineComponent.h"
#include "PongGame.h"

IMPLEMENT_CLASS(SD::P::AiPaddle, SD::EntityComponent)
P_BEGIN

void AiPaddle::InitProps ()
{
	Super::InitProps();

	Destination = Vector3::ZERO_VECTOR;
	Tolerance = 1.f;
	DestRadius = 50.f;
	Speed = 536.f;
	MinSpeed = 335.f;
	MaxSpeed = 2010.f;
	MaxSpread = 10; //10 point difference would cause the bot to be either max or min speed.
	OwningPaddle = nullptr;
	ServeTick = nullptr;
}

void AiPaddle::BeginObject ()
{
	Super::BeginObject();

	TickComponent* moveTick = TickComponent::CreateObject(TICK_GROUP_PONG);
	if (AddComponent(moveTick))
	{
		moveTick->SetTickHandler(SDFUNCTION_1PARAM(this, AiPaddle, HandleMoveTick, void, Float));
	}

	TickComponent* destTick = TickComponent::CreateObject(TICK_GROUP_PONG);
	if (AddComponent(destTick))
	{
		destTick->SetTickHandler(SDFUNCTION_1PARAM(this, AiPaddle, HandleDestinationTick, void, Float));
		destTick->SetTickInterval(0.75f);
	}

	ServeTick = TickComponent::CreateObject(TICK_GROUP_PONG);
	if (AddComponent(ServeTick))
	{
		ServeTick->SetTickHandler(SDFUNCTION_1PARAM(this, AiPaddle, HandleServeTick, void, Float));
		ServeTick->SetTickInterval(3.f);
		ServeTick->SetTicking(false);
	}
}

bool AiPaddle::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<Paddle*>(ownerCandidate) != nullptr);
}

void AiPaddle::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (OwningPaddle = dynamic_cast<Paddle*>(newOwner))
	{
		DestRadius = Utils::Min(OwningPaddle->ReadPaddleSize().X, OwningPaddle->ReadPaddleSize().Y) * 0.5f;
	}
}

void AiPaddle::ComponentDetached ()
{
	OwningPaddle = nullptr;

	Super::ComponentDetached();
}

void AiPaddle::EnterServeMode ()
{
	if (ServeTick != nullptr)
	{
		ServeTick->ResetAccumulatedTickTime();
		ServeTick->SetTicking(true);
	}
}

void AiPaddle::EvaluateDifficulty (Int botScore, Int playerScore)
{
	Float defaultSpeed = Speed;
	if (const AiPaddle* cdo = dynamic_cast<const AiPaddle*>(GetDefaultObject()))
	{
		defaultSpeed = cdo->Speed;
	}

	Float ratio = (botScore - playerScore).ToFloat() / MaxSpread.ToFloat();
	if (ratio < 0.f) //Player is winning
	{
		Speed = Utils::Lerp(-ratio, defaultSpeed, MaxSpeed);
	}
	else //Bot is winning
	{
		Speed = Utils::Lerp(ratio, defaultSpeed, MinSpeed);
	}
}

bool AiPaddle::FindBall ()
{
	PongEngineComponent* localPongEngine = PongEngineComponent::Find();
	CHECK(localPongEngine != nullptr)

	if (PongGame* game = localPongEngine->GetGame())
	{
		Ball = game->GetBall();
	}

	return (Ball != nullptr);
}

void AiPaddle::ProcessMovement (Float deltaSec)
{
	CHECK(OwningPaddle != nullptr)

	if (Ball == nullptr && !FindBall())
	{
		return;
	}

	const Vector3& oldPos = OwningPaddle->ReadTranslation();
	
	Vector3 newPos = Ball->GetTranslation();
	newPos += Destination;
	newPos.Y = oldPos.Y;

	Vector3 deltaPos = newPos - oldPos;
	if (deltaPos.CalcDistSquared() <= Tolerance * Tolerance)
	{
		return;
	}

	deltaPos.SetLengthTo(Speed * deltaSec);
	OwningPaddle->SetTranslation(oldPos + deltaPos);
}

void AiPaddle::HandleMoveTick (Float deltaSec)
{
	if (OwningPaddle != nullptr)
	{
		ProcessMovement(deltaSec);
	}
}

void AiPaddle::HandleDestinationTick (Float deltaSec)
{
	Vector2 randPt = RandomUtils::RandPointWithinCircle(DestRadius);
	Destination.X = randPt.X;
	Destination.Z = randPt.Y;
}

void AiPaddle::HandleServeTick (Float deltaSec)
{
	if (OwningPaddle == nullptr)
	{
		return;
	}

	if (Ball == nullptr && !FindBall())
	{
		return;
	}

	Vector2 ballCollision = Ball->CalcPaddleCollision(OwningPaddle);
	if (Float::Abs(ballCollision.X) < 1.f && Float::Abs(ballCollision.Y) < 1.f)
	{
		Ball->ServeBall(ballCollision);
		OwningPaddle->DisplayImpactPoint(Vector3(ballCollision.X * OwningPaddle->ReadPaddleSize().X, 0.f, ballCollision.Y * OwningPaddle->ReadPaddleSize().Y) * 0.5f);

		//The bot always serves down the negative Y-axis
		Ball->EditVelocity().Y *= -1.f;

		if (ServeTick != nullptr)
		{
			ServeTick->SetTicking(false);
		}
	}
}
P_END