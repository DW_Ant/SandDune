/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongEngineComponent.cpp
=====================================================================
*/

#include "PongEngineComponent.h"
#include "PongGame.h"
#include "PongHud.h"
#include "PongPerspective.h"
#include "PongTheme.h"
#include "SplashScreen.h"

IMPLEMENT_ENGINE_COMPONENT(SD::P::PongEngineComponent)
P_BEGIN

PongEngineComponent::PongEngineComponent () : Super(),
	Scene(nullptr)
{
	//Noop
}

void PongEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	CHECK(OwningEngine != nullptr)
	OwningEngine->CreateTickGroup(TICK_GROUP_PONG, TICK_GROUP_PRIORITY_PONG);

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	if (guiEngine != nullptr)
	{
		guiEngine->SetDefaultThemeClass(PongTheme::SStaticClass());
	}

	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	CHECK(localGraphics != nullptr)
	if (Window* window = localGraphics->GetPrimaryWindow())
	{
		Scene = SceneDrawLayer::CreateObject();
		Scene->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY - 100); //Renders behind the hud
		Camera = PongPerspective::CreateObject();
		window->RegisterDrawLayer(Scene, Camera.Get());

		//Set the window size so that it renders the entire wall outline.
		window->SetSize(Vector2(800.f, 800.f));
	}
}

void PongEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	//Hide the mouse for this game
	if (MousePointer* mouse = MousePointer::GetMousePointer())
	{
		mouse->SetVisibility(false);
	}

	ImportAssets();

	//Reduce the speed of sound to exaggerate the Doppler Effect
	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)
	localAudioEngine->SetSpeedOfSound(12500.f); //125 meters per second
}

void PongEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	if (guiEngine != nullptr && guiEngine->GetGuiTheme() != nullptr)
	{
		guiEngine->GetGuiTheme()->SetDefaultStyle(PongTheme::PONG_STYLE_NAME);
	}

	Game = PongGame::CreateObject();

	Hud = PongHud::CreateObject();
	Hud->SetDepth(50.f);
	Hud->RegisterToMainWindow(true, true, 150);

	InitSplashScreen = SplashScreen::CreateObject();
	InitSplashScreen->SetDepth(100.f);
	InitSplashScreen->RegisterToMainWindow(true, true, 100);
}

void PongEngineComponent::ShutdownComponent ()
{
	//Restore the mouse visibility if it's still around
	if (MousePointer* mouse = MousePointer::GetMousePointer())
	{
		mouse->SetVisibility(true);
	}

	if (InitSplashScreen != nullptr)
	{
		InitSplashScreen->Destroy();
		InitSplashScreen = nullptr;
	}

	if (Game != nullptr)
	{
		Game->Destroy();
		Game = nullptr;
	}

	if (Camera != nullptr)
	{
		Camera->Destroy();
		Camera = nullptr;
	}

	if (Scene != nullptr)
	{
		Scene->Destroy();
		Scene = nullptr;
	}

	if (Hud != nullptr)
	{
		Hud->Destroy();
		Hud = nullptr;
	}

	Super::ShutdownComponent();
}

void PongEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(AudioEngineComponent::SStaticClass());
	outDependencies.push_back(GraphicsEngineComponent::SStaticClass());
	outDependencies.push_back(GuiEngineComponent::SStaticClass());
}

void PongEngineComponent::AddToScene (RenderComponent* comp)
{
	if (Scene != nullptr && comp != nullptr)
	{
		Scene->RegisterSingleComponent(comp);
	}
}

void PongEngineComponent::ImportAssets ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Pong"), TXT("HudBorder.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Pong"), TXT("HudBorderRight.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Pong"), TXT("Outline.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Pong"), TXT("Paddle.txtr")));
	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("SampleGames") / TXT("Pong"), TXT("Sphere.txtr")));

	AudioEngineComponent* localAudioEngine = AudioEngineComponent::Find();
	CHECK(localAudioEngine != nullptr)
	bool bInSoundThread = localAudioEngine->IsMainAudioEngine();

	AudioAssetPool* localAudioPool = AudioAssetPool::FindAudioAssetPool();
	CHECK(localAudioPool != nullptr)

	Directory pongDir(Directory::DEV_ASSET_DIRECTORY / TXT("SampleGames") / TXT("Pong"));

	//TODO: When the AudioEditor is implemented, replace this with ImportAudio method.
	AudioAsset* serveSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("Jingle_Win_Synth_00.wav")), HashedString("SampleGames.Pong.ServeSound"));
	if (serveSound != nullptr)
	{
		serveSound->SetAudioName(TXT("ServeSound"));
		serveSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_GENERAL);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			serveSound->CopyPropsToOtherThread();
		}
#endif
	}

	AudioAsset* thumpSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("UI_Electric_01.wav")), HashedString("SampleGames.Pong.ThumpSound"));
	if (thumpSound != nullptr)
	{
		thumpSound->SetAudioName(TXT("ThumpSound"));
		thumpSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_GENERAL);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			thumpSound->CopyPropsToOtherThread();
		}
#endif
	}

	AudioAsset* impactSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("UI_Electric_08.wav")), HashedString("SampleGames.Pong.ImpactSound"));
	if (impactSound != nullptr)
	{
		impactSound->SetAudioName(TXT("ImpactSound"));
		impactSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_GENERAL);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			impactSound->CopyPropsToOtherThread();
		}
#endif
	}

	AudioAsset* scoreSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("UI_Electric_06.wav")), HashedString("SampleGames.Pong.ScoreSound"));
	if (scoreSound != nullptr)
	{
		scoreSound->SetAudioName(TXT("ScoreSound"));
		scoreSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_GENERAL);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			scoreSound->CopyPropsToOtherThread();
		}
#endif
	}

	AudioAsset* moveBallSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("MovingBall.wav")), HashedString("SampleGames.Pong.MoveBallSound"));
	if (moveBallSound != nullptr)
	{
		moveBallSound->SetAudioName(TXT("MoveBallSound"));
		moveBallSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_GENERAL);
		moveBallSound->SetLooping(true);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			moveBallSound->CopyPropsToOtherThread();
		}
#endif
	}

	AudioAsset* volSound = localAudioPool->CreateAndImportAudio(FileAttributes(pongDir, TXT("UI_Synth_02.wav")), HashedString("SampleGames.Pong.VolSound"));
	if (volSound != nullptr)
	{
		volSound->SetAudioName(TXT("VolSound"));
		volSound->SetChannelName(AudioEngineComponent::AUDIO_CHANNEL_UI);

#ifdef WITH_MULTI_THREAD
		if (!bInSoundThread)
		{
			volSound->CopyPropsToOtherThread();
		}
#endif
	}
}
P_END