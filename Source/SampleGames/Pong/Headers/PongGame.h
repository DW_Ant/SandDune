/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongGame.h
  This Entity is responsible for running the game flow and manage
  its objects.

  For managing the game flow, it contains a few states such as when it's
  time to serve the ball, and playing the game, itself.

  It also keeps track of the player scores and notifies the Hud whenever
  the state changes.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class BouncyBall;
class Paddle;

class PongGame : public Entity
{
	DECLARE_CLASS(PongGame)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<BouncyBall> Ball;
	DPointer<Paddle> PlayerPaddle;
	DPointer<Paddle> BotPaddle;

	Int PlayerScore;
	Int BotScore;

	/* The region that determines when the game's borders. This where defines where the ball would bounce and the size of the outlines. */
	Aabb Borders;

private:
	/* Becomes true when the game has started. */
	bool bHasStarted;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void StartGame ();

	/**
	 * Adds a point to the corresponding team, and changes the game state to serving state.
	 */
	virtual void LeftScore ();
	virtual void RightScore ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline BouncyBall* GetBall () const
	{
		return Ball.Get();
	}

	inline Paddle* GetRightPaddle () const
	{
		return BotPaddle.Get();
	}

	inline Paddle* GetLeftPaddle () const
	{
		return PlayerPaddle.Get();
	}

	inline Aabb GetBorders () const
	{
		return Borders;
	}

	inline const Aabb& ReadBorders () const
	{
		return Borders;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void CreateBall ();
};
P_END