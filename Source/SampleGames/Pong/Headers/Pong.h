/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Pong.h
  Contains important file includes and definitions for the Pong sample game.

  This game provides a simple example of playing sounds directly through
  the AudioEngineComponent and through an AudioCompnent.
  
  It demonstrates the following:
  * Playing static sounds
  * Playing dynamic sounds
  * Playing/pausing looping sounds
  * Severing sounds from source
  * Global master volume
  * Audio channels
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#ifdef WITH_MULTI_THREAD
#include "Engine\MultiThread\Headers\MultiThreadClasses.h"
#endif
#include "Engine\Random\Headers\RandomClasses.h"
#include "Engine\Sound\Headers\SoundClasses.h"

#define P_BEGIN namespace SD { namespace P {
#define P_END } }

#define TICK_GROUP_PONG "Pong"
#define TICK_GROUP_PRIORITY_PONG 1200

P_BEGIN
extern LogCategory PongLog;
P_END