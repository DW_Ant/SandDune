/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongTheme.h
  The object responsible for determining how the GuiComponents appear for the
  Pong game.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class PongTheme : public GuiTheme
{
	DECLARE_CLASS(PongTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString PONG_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializePongStyle ();
};
P_END