/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongHud.h
  The Gui overlay that appears over the game, displaying the game state.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class PongHud : public GuiEntity
{
	DECLARE_CLASS(PongHud)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	LabelComponent* RedScore;
	LabelComponent* BlueScore;
	LabelComponent* VolumeLabel;

	FrameComponent* PauseFrame;

	LabelComponent* ServePrompt;
	TickComponent* ServeTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority) override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void DisplayServePrompt ();
	virtual void HideServePrompt ();
	virtual void SetRedScore (Int newScore);
	virtual void SetBlueScore (Int newScore);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void UpdateVolumeLabel ();
	virtual void PlayBeep (AudioEngineComponent* audioEngine);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleDecreaseVolume ();
	virtual bool HandleIncreaseVolume ();
	virtual bool HandleTogglePause ();
	virtual void HandleTogglePrompt (Float deltaSec);
};
P_END