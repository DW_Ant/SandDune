/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongEngineComponent.h
  An engine component responsible for managing Entities that runs the Pong game.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class PongGame;
class PongHud;
class PongPerspective;
class SplashScreen;

class PongEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(PongEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<PongGame> Game;
	DPointer<PongHud> Hud;
	SceneDrawLayer* Scene;
	DPointer<PongPerspective> Camera;
	DPointer<SplashScreen> InitSplashScreen;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PongEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the given object to draw to the SceneDrawLayer.
	 */
	virtual void AddToScene (RenderComponent* comp);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline PongGame* GetGame () const
	{
		return Game.Get();
	}

	inline PongHud* GetHud () const
	{
		return Hud.Get();
	}

	inline PongPerspective* GetCamera () const
	{
		return Camera.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports all assets used in the Pong game.
	 */
	virtual void ImportAssets ();
};
P_END