/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SplashScreen.h
  A simple Gui that displays the purpose of this game, and prompts
  the user to start the game.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class SplashScreen : public GuiEntity
{
	DECLARE_CLASS(SplashScreen)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void SetupInputComponent (InputBroadcaster* broadcaster, Int inputPriority) override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleStartGame ();
};
P_END