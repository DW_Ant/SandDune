/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PongPerspective.h
  A hacky camera that simulates a 3D perspective. Not until the Graphics Module
  is overhauled to support 3D, this is a work around where object further from the camera
  are drawn smaller and closer towards the vanishing point.

  For simplicity purposes, the camera looks down the positive Y axis.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class PongPerspective : public SceneCamera
{
	DECLARE_CLASS(PongPerspective)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Distance between the camera position and the 2D plane used for calculating the projection. */
	Float FocalLength;

	PlayerListenerComponent* ListenComp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const override;
	virtual void ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const override;
	virtual Float GetDistSquaredToPoint (const Vector3& worldLocation) const override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetFocalLength () const
	{
		return FocalLength;
	}
};
P_END