/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BouncyBall.h
  The main focus of the game. This object can bounce off walls and paddles.
  It can also score points for colliding against goals.

  Silly name but it's distinct enough from the other sphere classes.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class Paddle;
class PongGame;

class BouncyBall : public SceneEntity
{
	DECLARE_CLASS(BouncyBall)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines the size of this ball for rendering and collision detection purposes (in cms). */
	Float BallRadius;

	/* Determines the Y-axis movement speed of the ball immediately after a serve. */
	Float ServeSpeed;

	/* Determines how quickly this ball travels in cms per second. */
	Vector3 Velocity;

	/* Determines how quickly the ball accelerates in the Y-axis (in cms per second per second). */
	Float AccelerationRate;

	/* Reference to the game instance that instantiated this ball. The game holds references to the borders and the paddles this ball can collide against. */
	PongGame* Game;

	SpriteComponent* Sprite;

	/* The Entity that moves along with the ball to highlight its depth position. */
	SceneEntity* DepthHighlighter;

	AudioComponent* Audio;

private:
	/* The sound to play whenever the ball collides against the wall or paddle. */
	const AudioAsset* ImpactSound;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a vector that indicates how far this ball is from the given paddle. A vector of (0,0) implies that the ball is centered over the paddle.
	 * A value of (1, 0) implies that the ball collided against the very right-most edge of the paddle.
	 * A value of (0, 1.1) implies that the ball barely went over the top of the paddle.
	 */
	virtual Vector2 CalcPaddleCollision (Paddle* paddle) const;

	/**
	 * Launches the ball from its current position.
	 * The X and Z velocity direction is determined by the given parameter.
	 * The Y-axis is determined based on the ball's ServeSpeed.
	 */
	virtual void ServeBall (const Vector2& direction);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetVelocity (const Vector3& newVelocity);
	virtual void SetGame (PongGame* newGame);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetBallRadius () const
	{
		return BallRadius;
	}

	inline Vector3 GetVelocity () const
	{
		return Velocity;
	}

	inline const Vector3& ReadVelocity () const
	{
		return Velocity;
	}

	inline Vector3& EditVelocity ()
	{
		return Velocity;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the audio asset pool to find the impact sound. If it's found, it'll cache it to the ImpactSound member variable.
	 * Returns true on success.
	 */
	virtual bool FindImpactSound ();

	virtual void PlayScoreSound ();

	/**
	 * Looks at the velocity and position of this ball to determine if the ball should change its velocity.
	 * In addition to that, it'll check if it reached any of the goals.
	 */
	virtual void ProcessCollision ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
P_END