/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AiPaddle.h
  A component that controls the owning Paddle's position to try to block
  the ball.

  The component will pick an approximate location around the ball, then move
  the paddle towards that position at a set rate.

  This component can only be attached to a Paddle Entity.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class BouncyBall;
class Paddle;

class AiPaddle : public EntityComponent
{
	DECLARE_CLASS(AiPaddle)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The position this component is moving the paddle towards. The Y axis is ignored. This position is relative to the ball position. */
	Vector3 Destination;

	/* The threshold that determines if the AI reached their destination. */
	Float Tolerance;

	/* The maximum radius allowed when picking a random destination value. 0 radius implies that the bot will always try to hit the ball at the center. */
	Float DestRadius;

	/* Determines how quickly this component can move the paddle towards its destination (in cms per second). */
	Float Speed;

	/* Minimum allowable speed this bot will move when the player is not doing too well. */
	Float MinSpeed;

	/* Maximum allowable speed this bot will move when the player is performing well. */
	Float MaxSpeed;

	/* The maximum score differences is when this bot reaches either the min/max speed. */
	Int MaxSpread;

	DPointer<BouncyBall> Ball;
	Paddle* OwningPaddle;

	TickComponent* ServeTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Enables the tick component that will eventually cause the bot to serve the ball.
	 */
	virtual void EnterServeMode ();

	/**
	 * Adjusts the AI difficulty based on how well the player is performing relative to the bot.
	 */
	virtual void EvaluateDifficulty (Int botScore, Int playerScore);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Float GetSpeed () const
	{
		return Speed;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick utility to find the ball. Returns true if the ball is found.
	 */
	virtual bool FindBall ();

	/**
	 * Checks the ball location relative to the owning paddle, and moves the paddle towards the destination.
	 */
	virtual void ProcessMovement (Float deltaSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMoveTick (Float deltaSec);
	virtual void HandleDestinationTick (Float deltaSec);
	virtual void HandleServeTick (Float deltaSec);
};
P_END