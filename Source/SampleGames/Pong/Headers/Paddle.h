/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Paddle.h
  The object the player or bot controls to deflect the ball.
=====================================================================
*/

#pragma once

#include "Pong.h"

P_BEGIN
class Paddle : public SceneEntity
{
	DECLARE_CLASS(Paddle)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Vector2 PaddleSize;
	DPointer<SpriteComponent> Sprite;

	/* Time it takes for the circle to completely expand and fade out. */
	Float ImpactDuration;

	/* The maximum size the impact circle will expand to right when it hits the ImpactDuration. */
	Float MaxImpactSize;

	SceneTransformComponent* ImpactTransform;
	ColorRenderComponent* ImpactColor;
	TickComponent* ImpactTick;

private:
	/* Timestamp when the impact was called on this paddle. */
	Float ImpactTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds an InputComponent that allows the player to take control over this paddle.
	 */
	virtual void SetupInputComp ();

	/**
	 * Adds a AiPaddle component that allows the system to move this paddle.
	 */
	virtual void SetupBotComp ();

	/**
	 * Temporarily displays an expanding circle at the given position.
	 * The position is relative to the paddle's center.
	 */
	virtual void DisplayImpactPoint (const Vector3& impactPoint);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPaddleSize (const Vector2& newPaddleSize);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const Vector2& ReadPaddleSize () const
	{
		return PaddleSize;
	}

	inline Vector2 GetPaddleSize () const
	{
		return PaddleSize;
	}

	inline SpriteComponent* GetSprite () const
	{
		return Sprite.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent, const Vector2& deltaMove);
	virtual bool HandleServe ();
	virtual void HandleImpactTick (Float deltaSec);
};
P_END