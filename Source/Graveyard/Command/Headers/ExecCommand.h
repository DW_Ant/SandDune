/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ExecCommand.h
  Contains a mapping between strings and its function.
  This class will also handle text conversion from strings to a list of parameters.
=====================================================================
*/

#pragma once

#include "Command.h"
#include "CommandEngineComponent.h"

SD_BEGIN

class COMMAND_API ExecCommand : public Object
{
	DECLARE_CLASS(ExecCommand)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	/**
	 * Various function flags that determines an exec function's availability.
	 */
	enum EExecFunctionFlags
	{
		EFF_Offline = 0x00000001, //This exec command is available when offline (Could support multiplayer offline such as split screen)
		EFF_Online = 0x00000002, //This exec command is available when online
		EFF_OnlineAndOffline = EFF_Offline | EFF_Online, //This exec command contains all inclusive flags.

		EFF_IsCheatCommand = 0x00000010 //This exec command is considered a cheat.  The environment may disable cheats (such as cheats are disabled for campaign mode).
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Character delimiter that'll divide the full text into parameters.  Characters inside double quotes are excluded. */
	static const DString ParamDelimiter;

	/* If true, then only offline commands are available. */
	static bool bIsOnline;
	static bool bCheatsEnabled;

protected:
	/* Identifier that will trigger the ExecFunction.  Spaces are not permitted. */
	DString FunctionName;

	/* Flags that'll determine this ExecFunction's availability. */
	EExecFunctionFlags ExecFunctionFlags;

	/* Determines the number of parameters this ExecCommand is expecting. */
	INT NumParams;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to execute the function using the specified parameter list.
	 *
	 * @Param:  outError explains what kind of error (if any) occured whenever the paramList is invalid.
	 */
	virtual void ExecuteFunction (DString paramList, CommandEngineComponent::ECmdError& outError) const = 0;

	/**
	 * Returns true if this command is currently available based on the application's current state (eg:  cheats enabled?)
	 */
	virtual bool IsCommandAvailable () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	EExecFunctionFlags GetExecFunctionFlags () const;
	DString GetFunctionName () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Divides up the string into a vector of strings where each element of the vector represents a parameter value.
	 * Returns false if the string param is invalid.
	 */
	virtual bool ExtractParams (const DString& param, std::vector<DString>& outParamList, CommandEngineComponent::ECmdError& outError) const;

	/**
	 * Iterates through the param list, and any delimiter character that's not wrapped in quotes assumes is signalling that it's a different parameter.
	 * This function populates the param list based on the parameter delimiter appearances.
	 */
	virtual void PopulateParamList (const DString& param, std::vector<DString>& outParamList) const;
};

DEFINE_ENUM_FUNCTIONS(ExecCommand::EExecFunctionFlags);

SD_END