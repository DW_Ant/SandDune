/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Command.h
  Contains important file includes and definitions for the Command module.

  The Command module implements the foundation for any command-oriented functionality.
  Commands are generally functions that can be invoked using a series of strings, and they
  are typically user facing.

	- Command list framework where associated functions may be invoked by only using strings.
	- Generic utilities for parsing the command line arguments.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#if !(MODULE_CORE)
#error The Command module requires the Core module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef COMMAND_EXPORT
		#define COMMAND_API __declspec(dllexport)
	#else
		#define COMMAND_API __declspec(dllimport)
	#endif
#else
	#define COMMAND_API
#endif

#include "CommandMacros.h"

SD_BEGIN
extern LogCategory COMMAND_API CommandLog;
SD_END