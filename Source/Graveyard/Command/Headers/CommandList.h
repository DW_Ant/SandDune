/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandList.h
  Manages a list of ExecCommands to handle converting a string to an execfunction.
=====================================================================
*/

#pragma once

#include "Command.h"
#include "CommandEngineComponent.h"

SD_BEGIN

class ExecCommand;
class COMMAND_API CommandList : public Object
{
	DECLARE_CLASS(CommandList)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	std::vector<DPointer<const ExecCommand>> RegisteredExecCommands;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Divides the given string into two segments:  the function name and its parameter list (if any).
	 */
	static void ExtractFunctionName (const DString& fullText, DString& outFunctionName, DString& outParameters);

	/**
	 * Uses the class iterator to populate its list of RegisteredExecCommands.
	 */
	virtual void RegisterExecCommands ();

	/**
	 * Extracts the function from its parameters, and attempts to run that function with those parameters.
	 */
	virtual void ProcessCommand (const DString& fullCommand) const;
	virtual void ProcessCommand (const DString& fullCommand, OUT CommandEngineComponent::ECmdError& outCmdError) const;

	/**
	 * Finds and executes the function (if any).
	 */
	virtual void ExecuteFunction (const DString& functionName, const DString& parameters, CommandEngineComponent::ECmdError& outCmdError) const;

	/**
	 * Returns the ExecCommand where its function name matches the given string.
	 */
	virtual const ExecCommand* FindExecCommand (const DString& functionName) const;
};

SD_END