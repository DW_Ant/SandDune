/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandMacros.h
  Defines complex macros that are related to the Command module.
=====================================================================
*/

#pragma once

#include "Command.h"

/**
 * Macros that'll define an ExecFunction.  These functions are defined in their own namespaces to avoid the IMPLEMENT_CLASS's Super typedef conflict.
 * These macros must be placed in implementation files to avoid defining static variables more than once.
 */
#define DEFINE_EXEC_FUNCTION(className, functionName, execFlags) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 0; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				className##::##functionName##(); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_1PARAM(className, functionName, execFlags, param1Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 1; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				\
				className##::##functionName##(a); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_2PARAM(className, functionName, execFlags, param1Type, param2Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 2; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				\
				className##::##functionName##(a, b); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}

#define DEFINE_EXEC_FUNCTION_3PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 3; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				\
				className##::##functionName##(a, b, c); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_4PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type, param4Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 4; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				param4Type d = param4Type(parsedParams.at(3)); \
				\
				className##::##functionName##(a, b, c, d); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_5PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type, param4Type, param5Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 5; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				param4Type d = param4Type(parsedParams.at(3)); \
				param5Type e = param5Type(parsedParams.at(4)); \
				\
				className##::##functionName##(a, b, c, d, e); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_6PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type, param4Type, param5Type, param6Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 6; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				param4Type d = param4Type(parsedParams.at(3)); \
				param5Type e = param5Type(parsedParams.at(4)); \
				param6Type f = param6Type(parsedParams.at(5)); \
				\
				className##::##functionName##(a, b, c, d, e, f); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_7PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type, param4Type, param5Type, param6Type, param7Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 7; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				param4Type d = param4Type(parsedParams.at(3)); \
				param5Type e = param5Type(parsedParams.at(4)); \
				param6Type f = param6Type(parsedParams.at(5)); \
				param7Type g = param7Type(parsedParams.at(6)); \
				\
				className##::##functionName##(a, b, c, d, e, f, g); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}


#define DEFINE_EXEC_FUNCTION_8PARAM(className, functionName, execFlags, param1Type, param2Type, param3Type, param4Type, param5Type, param6Type, param7Type, param8Type) \
	namespace SD_Exec \
	{ \
		class exec##functionName : public SD::ExecCommand \
		{ \
			DECLARE_CLASS(exec##functionName##) \
			\
		public: \
			virtual void InitProps () override \
			{ \
				ExecCommand::InitProps(); \
				\
				FunctionName = STRINGIFY(functionName); \
				ExecFunctionFlags = (##execFlags##); \
				NumParams = 8; \
			} \
			\
			virtual inline void ExecuteFunction (SD::DString paramList, SD::CommandEngineComponent::ECmdError& outError) const override \
			{ \
				if (!IsCommandAvailable()) \
				{ \
					outError = SD::CommandEngineComponent::ECmdError::CE_CmdNotAvailable; \
					return; \
				} \
				\
				std::vector<SD::DString> parsedParams; \
				if (!ExtractParams(paramList, parsedParams, outError)) \
				{ \
					return; \
				} \
				\
				param1Type a = param1Type(parsedParams.at(0)); \
				param2Type b = param2Type(parsedParams.at(1)); \
				param3Type c = param3Type(parsedParams.at(2)); \
				param4Type d = param4Type(parsedParams.at(3)); \
				param5Type e = param5Type(parsedParams.at(4)); \
				param6Type f = param6Type(parsedParams.at(5)); \
				param7Type g = param7Type(parsedParams.at(6)); \
				param8Type h = param8Type(parsedParams.at(7)); \
				\
				className##::##functionName##(a, b, c, d, e, f, g, h); \
			} \
		}; \
		\
		IMPLEMENT_CLASS(SD_Exec::exec##functionName##, SD::ExecCommand) \
	}