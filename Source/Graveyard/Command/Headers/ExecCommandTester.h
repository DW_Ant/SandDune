/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandUnitTester.h
  This class assists the CommandUnitTester.  This class will be executing various Exec commands
  it implements, and it'll notify the CommandUnitTester if the test met the requirements.
=====================================================================
*/

#pragma once

#include "Command.h"

#ifdef DEBUG_MODE

SD_BEGIN

class CommandUnitTester;
class COMMAND_API ExecCommandTester : public Object
{
	DECLARE_CLASS(ExecCommandTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	static DPointer<ExecCommandTester> LatestInstancedTester;

	DPointer<const CommandUnitTester> CommandTester;
	UnitTester::EUnitTestFlags TestFlags;

	/* Becomes true if one of the exec function tests failed. */
	bool bExecTestFailed;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual bool RunTest (const CommandUnitTester* tester, UnitTester::EUnitTestFlags testFlags);

	static void CmdTester_NoParamFunction ();
	static void CmdTester_SingleParamFunction (INT a);
	static void CmdTester_StringParamFunction (DString a);
	static void CmdTester_MultiParamFunction (INT i, FLOAT f, DString str, INT otherInt);

	//Testing ExecFunctionMacros
	static void CmdTester_2Param (INT a, FLOAT b);
	static void CmdTester_3Param (INT a, FLOAT b, DString c);
	static void CmdTester_4Param (INT a, FLOAT b, DString c, INT d);
	static void CmdTester_5Param (INT a, FLOAT b, DString c, INT d, FLOAT e);
	static void CmdTester_6Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f);
	static void CmdTester_7Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f, INT g);
	static void CmdTester_8Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f, INT g, FLOAT h);

	//Testing user errors
	static void CmdTester_ThisCmdShouldNotRun (INT a, INT b);

	//Testing ExecFunctionFlags
	static void CmdTester_AlwaysAvail ();
	static void CmdTester_OfflineOnly ();
	static void CmdTester_OnlineOnly ();
	static void CmdTester_CheatCommand ();
	static void CmdTester_CheatOfflineCommand ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Attempts to execute the given command.
	 * Sets the failed test to true, and assumes the command that implements the exec function will switch that flag to false.
	 */
	virtual void ExecuteTestCommand (const DString& testCommand) const;

	/**
	 * Executes all available exec functions that'll be testing the exec flags.  This function sets the flag accordingly.
	 * This function will also execute the UnitTest's error sequence as soon as it fails.
	 * Returns true if this test passed.
	 */
	virtual bool TestExecFunctionAvailability ();
};

SD_END

#endif