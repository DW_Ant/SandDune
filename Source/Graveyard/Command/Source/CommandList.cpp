/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandList.cpp
=====================================================================
*/

#include "CommandClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD::CommandList, SD::Object)

void CommandList::ExtractFunctionName (const DString& fullText, DString& outFunctionName, DString& outParameters)
{
	INT parameterIdx = fullText.Find(TXT(" "), 0, DString::CC_IgnoreCase, DString::SD_LeftToRight);
	if (parameterIdx == INT_INDEX_NONE)
	{
		outFunctionName = fullText;
		outParameters = DString::EmptyString;
		return;
	}

	outFunctionName = fullText.SubString(0, parameterIdx - 1); //Include text before space idx
	outParameters = fullText.SubString(parameterIdx + 1); //Include text after space idx
}

void CommandList::RegisterExecCommands ()
{
	RegisteredExecCommands.clear();

	for (ClassIterator iter(ExecCommand::SStaticClass()); iter.GetSelectedClass() != nullptr; iter++)
	{
		const ExecCommand* execCDO = dynamic_cast<const ExecCommand*>(iter.GetSelectedClass()->GetDefaultObject());
		if (execCDO != nullptr)
		{
			RegisteredExecCommands.push_back(execCDO);
		}
	}
}

void CommandList::ProcessCommand (const DString& fullCommand) const
{
	CommandEngineComponent::ECmdError cmdError;
	ProcessCommand(fullCommand, cmdError);
}

void CommandList::ProcessCommand (const DString& fullCommand, OUT CommandEngineComponent::ECmdError& outCmdError) const
{
	DString functionName;
	DString params;
	ExtractFunctionName(fullCommand, functionName, params);

	ExecuteFunction(functionName, params, outCmdError);
}

void CommandList::ExecuteFunction (const DString& functionName, const DString& parameters, CommandEngineComponent::ECmdError& outError) const
{
	const ExecCommand* cmd = FindExecCommand(functionName);
	if (cmd == nullptr)
	{
		outError = CommandEngineComponent::CE_UnknownCmd;
		return;
	}

	cmd->ExecuteFunction(parameters, outError);
}

const ExecCommand* CommandList::FindExecCommand (const DString& functionName) const
{
	for (UINT_TYPE i = 0; i < RegisteredExecCommands.size(); i++)
	{
		if (RegisteredExecCommands.at(i)->GetFunctionName().Compare(functionName, DString::CC_IgnoreCase) == 0)
		{
			return RegisteredExecCommands.at(i).Get();
		}
	}

	return nullptr;
}
SD_END