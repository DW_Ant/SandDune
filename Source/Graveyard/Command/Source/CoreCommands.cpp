/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CoreCommands.cpp
=====================================================================
*/

#include "CommandClasses.h"

SD_BEGIN

IMPLEMENT_CLASS(SD::ECoreCommands, SD::Object)

#pragma region "Exec Functions"
DEFINE_EXEC_FUNCTION(ECoreCommands, Exit, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline)
DEFINE_EXEC_FUNCTION(ECoreCommands, Quit, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline)
#pragma endregion

void ECoreCommands::Exit ()
{
	Engine* engine = Engine::FindEngine();
	CHECK(engine != nullptr)

	CommandLog.Log(LogCategory::LL_Log, TXT("Terminate engine requested."));
	if (!engine->IsShuttingDown())
	{
		engine->Shutdown();
	}
}

void ECoreCommands::Quit ()
{
	Exit();
}

SD_END