/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ExecCommandTester.cpp
=====================================================================
*/

#include "CommandClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD::ExecCommandTester, SD::Object)

#pragma region "Exec Functions"
DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_NoParamFunction, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline)
DEFINE_EXEC_FUNCTION_1PARAM(ExecCommandTester, CmdTester_SingleParamFunction, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT)
DEFINE_EXEC_FUNCTION_1PARAM(ExecCommandTester, CmdTester_StringParamFunction, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, DString)
DEFINE_EXEC_FUNCTION_4PARAM(ExecCommandTester, CmdTester_MultiParamFunction, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT)

DEFINE_EXEC_FUNCTION_2PARAM(ExecCommandTester, CmdTester_2Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT)
DEFINE_EXEC_FUNCTION_3PARAM(ExecCommandTester, CmdTester_3Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString)
DEFINE_EXEC_FUNCTION_4PARAM(ExecCommandTester, CmdTester_4Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT)
DEFINE_EXEC_FUNCTION_5PARAM(ExecCommandTester, CmdTester_5Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT, FLOAT)
DEFINE_EXEC_FUNCTION_6PARAM(ExecCommandTester, CmdTester_6Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT, FLOAT, DString)
DEFINE_EXEC_FUNCTION_7PARAM(ExecCommandTester, CmdTester_7Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT, FLOAT, DString, INT)
DEFINE_EXEC_FUNCTION_8PARAM(ExecCommandTester, CmdTester_8Param, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, FLOAT, DString, INT, FLOAT, DString, INT, FLOAT)

DEFINE_EXEC_FUNCTION_2PARAM(ExecCommandTester, CmdTester_ThisCmdShouldNotRun, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline, INT, INT)

DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_AlwaysAvail, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline)
DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_OfflineOnly, ExecCommand::EExecFunctionFlags::EFF_Offline)
DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_OnlineOnly, ExecCommand::EExecFunctionFlags::EFF_Online)
DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_CheatCommand, ExecCommand::EExecFunctionFlags::EFF_OnlineAndOffline | ExecCommand::EExecFunctionFlags::EFF_IsCheatCommand)
DEFINE_EXEC_FUNCTION(ExecCommandTester, CmdTester_CheatOfflineCommand, ExecCommand::EExecFunctionFlags::EFF_Offline | ExecCommand::EExecFunctionFlags::EFF_IsCheatCommand)
#pragma endregion

DPointer<ExecCommandTester> ExecCommandTester::LatestInstancedTester = nullptr;

void ExecCommandTester::InitProps ()
{
	Super::InitProps();

	LatestInstancedTester = this;

	CommandTester = nullptr;
	bExecTestFailed = false;
}

void ExecCommandTester::Destroyed ()
{
	LatestInstancedTester = nullptr;

	Super::Destroyed();
}

bool ExecCommandTester::RunTest (const CommandUnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	CommandTester = tester;
	TestFlags = testFlags;
	bExecTestFailed = false;

	CommandTester->BeginTestSequence(TestFlags, TXT("Exec Commands"));
	CommandTester->SetTestCategory(TestFlags, TXT("Param Types"));

	CommandEngineComponent* localCmdEngine = CommandEngineComponent::Find();
	CHECK(localCmdEngine != nullptr && localCmdEngine->GetCmdList() != nullptr)

	ExecuteTestCommand(TXT("CmdTester_NoParamFunction"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute No Param function."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_SingleParamFunction 5"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute Single Param function with param = 5."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_StringParamFunction \"String with Quotes\""));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute string Param function with param = \"String with Quotes\"."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_MultiParamFunction 4 2.5 stringWithoutQuotes 8"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute Multi Param function with params = 4 2.5 stringWithoutQuotes 8"));
		return false;
	}

	CommandTester->CompleteTestCategory(TestFlags);
	CommandTester->SetTestCategory(TestFlags, TXT("Num Param Exec Test"));

	ExecuteTestCommand(TXT("CmdTester_2Param 1 2"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 2 Param function with params = 1 2."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_3Param 1 2 3"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 3 Param function with params = 1 2 3."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_4Param 1 2 3 -4"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 4 Param function with params = 1 2 3 -4."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_5Param 1 2 3 -4 -5.725"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 5 Param function with params = 1 2 3 -4 -5.725."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_6Param 1 2 3 -4 -5.725 \"-6 and a half\""));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 6 Param function with params = 1 2 3 -4 -5.725 \"-6 and a half\"."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_7Param 1 2 3 -4 -5.725 \"-6 and a half\" 7"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 7 Param function with params = 1 2 3 -4 -5.725 \"-6 and a half\" 7."));
		return false;
	}

	ExecuteTestCommand(TXT("CmdTester_8Param 1 2 3 -4 -5.725 \"-6 and a half\" 7 8.125"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute 8 Param function with params = 1 2 3 -4 -5.725 \"-6 and a half\" 7 8.125."));
		return false;
	}

	CommandTester->CompleteTestCategory(TestFlags);
	CommandTester->SetTestCategory(TestFlags, TXT("Param Mismatches"));

	bExecTestFailed = false;
	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_ThisCmdShouldNotRun 4"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Was able to execute a command despite missing a parameter."));
		return false;
	}

	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_ThisCmdShouldNotRun 4 4 4"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Was able to execute a command despite having too many parameters."));
		return false;
	}

	CommandTester->CompleteTestCategory(TestFlags);
	CommandTester->SetTestCategory(TestFlags, TXT("Exec Availability"));

	bool bWasOnline = ExecCommand::bIsOnline;
	bool bWasCheatsEnabled = ExecCommand::bCheatsEnabled;

	UnitTester::TestLog(TestFlags, TXT("Simulating the environment to be offline with cheats enabled..."));
	ExecCommand::bIsOnline = false;
	ExecCommand::bCheatsEnabled = true;
	if (!TestExecFunctionAvailability())
	{
		ExecCommand::bIsOnline = bWasOnline;
		ExecCommand::bCheatsEnabled = bWasCheatsEnabled;

		//Note:  Skipping the UnitTestError call here is intended since the TestExecFunctionAvailability invokes that.
		return false;
	}

	UnitTester::TestLog(TestFlags, TXT("Simulating the environment to be online with cheats enabled..."));
	ExecCommand::bIsOnline = true;
	ExecCommand::bCheatsEnabled = true;
	if (!TestExecFunctionAvailability())
	{
		ExecCommand::bIsOnline = bWasOnline;
		ExecCommand::bCheatsEnabled = bWasCheatsEnabled;

		//Note:  Skipping the UnitTestError call here is intended since the TestExecFunctionAvailability invokes that.
		return false;
	}

	UnitTester::TestLog(TestFlags, TXT("Simulating the environment to be offline with cheats disabled..."));
	ExecCommand::bIsOnline = false;
	ExecCommand::bCheatsEnabled = false;
	if (!TestExecFunctionAvailability())
	{
		ExecCommand::bIsOnline = bWasOnline;
		ExecCommand::bCheatsEnabled = bWasCheatsEnabled;

		//Note:  Skipping the UnitTestError call here is intended since the TestExecFunctionAvailability invokes that.
		return false;
	}

	UnitTester::TestLog(TestFlags, TXT("Simulating the environment to be online with cheats disabled..."));
	ExecCommand::bIsOnline = true;
	ExecCommand::bCheatsEnabled = false;
	if (!TestExecFunctionAvailability())
	{
		ExecCommand::bIsOnline = bWasOnline;
		ExecCommand::bCheatsEnabled = bWasCheatsEnabled;

		//Note:  Skipping the UnitTestError call here is intended since the TestExecFunctionAvailability invokes that.
		return false;
	}

	//Restore ExecCommand state to whatever it was before
	ExecCommand::bIsOnline = bWasOnline;
	ExecCommand::bCheatsEnabled = bWasCheatsEnabled;

	CommandTester->CompleteTestCategory(TestFlags);
	CommandTester->ExecuteSuccessSequence(TestFlags, TXT("Exec Commands"));
	return true;
}

void ExecCommandTester::CmdTester_NoParamFunction ()
{
	LatestInstancedTester->bExecTestFailed = false;
}

void ExecCommandTester::CmdTester_SingleParamFunction (INT a)
{
	LatestInstancedTester->bExecTestFailed = (a != 5);
}

void ExecCommandTester::CmdTester_StringParamFunction (DString a)
{
	LatestInstancedTester->bExecTestFailed = (a != TXT("String with Quotes"));
}

void ExecCommandTester::CmdTester_MultiParamFunction (INT i, FLOAT f, DString str, INT otherInt)
{
	bool bPassed = (i == 4 && f == 2.5f && str == TXT("stringWithoutQuotes") && otherInt == 8);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_2Param (INT a, FLOAT b)
{
	bool bPassed = (a == 1 && b == 2.f);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_3Param (INT a, FLOAT b, DString c)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3"));
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_4Param (INT a, FLOAT b, DString c, INT d)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3") && d == -4);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_5Param (INT a, FLOAT b, DString c, INT d, FLOAT e)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3") && d == -4 && e == -5.725f);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_6Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3") && d == -4 && e == -5.725f && f == TXT("-6 and a half"));
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_7Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f, INT g)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3") && d == -4 && e == -5.725f && f == TXT("-6 and a half") && g == 7);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_8Param (INT a, FLOAT b, DString c, INT d, FLOAT e, DString f, INT g, FLOAT h)
{
	bool bPassed = (a == 1 && b == 2.f && c == TXT("3") && d == -4 && e == -5.725f && f == TXT("-6 and a half") && g == 7 && h == 8.125f);
	LatestInstancedTester->bExecTestFailed = !bPassed;
}

void ExecCommandTester::CmdTester_ThisCmdShouldNotRun (INT a, INT b)
{
	LatestInstancedTester->bExecTestFailed = true;
}

void ExecCommandTester::CmdTester_AlwaysAvail ()
{
	UnitTester::TestLog(LatestInstancedTester->TestFlags, TXT("Always available command executed."));
	LatestInstancedTester->bExecTestFailed = false;
}

void ExecCommandTester::CmdTester_OfflineOnly ()
{
	UnitTester::TestLog(LatestInstancedTester->TestFlags, TXT("Offline only command executed."));
	if (ExecCommand::bIsOnline)
	{
		LatestInstancedTester->bExecTestFailed = true;
	}
}
void ExecCommandTester::CmdTester_OnlineOnly ()
{
	UnitTester::TestLog(LatestInstancedTester->TestFlags, TXT("Online only command executed."));
	if (!ExecCommand::bIsOnline)
	{
		LatestInstancedTester->bExecTestFailed = true;
	}
}

void ExecCommandTester::CmdTester_CheatCommand ()
{
	UnitTester::TestLog(LatestInstancedTester->TestFlags, TXT("Cheat command executed."));
	if (!ExecCommand::bCheatsEnabled)
	{
		LatestInstancedTester->bExecTestFailed = true;
	}
}

void ExecCommandTester::CmdTester_CheatOfflineCommand ()
{
	UnitTester::TestLog(LatestInstancedTester->TestFlags, TXT("Cheating offline only command executed."));
	if (!ExecCommand::bCheatsEnabled || ExecCommand::bIsOnline)
	{
		LatestInstancedTester->bExecTestFailed = true;
	}
}

void ExecCommandTester::ExecuteTestCommand (const DString& testCommand) const
{
	UnitTester::TestLog(TestFlags, TXT("Executing command:  \"%s\""), testCommand);
	LatestInstancedTester->bExecTestFailed = true; //Assume that the test failed in case the function was never called.  The function (if executed) should flip this switch back.

	CommandEngineComponent* localCmdEngine = CommandEngineComponent::Find();
	CHECK(localCmdEngine != nullptr && localCmdEngine->GetCmdList() != nullptr)
	localCmdEngine->GetCmdList()->ProcessCommand(testCommand);
}

bool ExecCommandTester::TestExecFunctionAvailability ()
{
	bExecTestFailed = true; //The next command should set this to false.

	CommandEngineComponent* localCmdEngine = CommandEngineComponent::Find();
	CHECK(localCmdEngine != nullptr && localCmdEngine->GetCmdList() != nullptr)
	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_AlwaysAvail"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  Failed to execute a command that should always be available."));
		return false;
	}

	bExecTestFailed = false; //Other commands would set this to true if they are invoked when they're not suppose to.
	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_OfflineOnly"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  The Offline only command should not be available."));
		return false;
	}

	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_OnlineOnly"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  The Online only command should not be available."));
		return false;
	}

	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_CheatCommand"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  The Cheating command should not be available."));
		return false;
	}

	localCmdEngine->GetCmdList()->ProcessCommand(TXT("CmdTester_CheatOfflineCommand"));
	if (bExecTestFailed)
	{
		CommandTester->UnitTestError(TestFlags, TXT("Exec Command test failed.  The cheating offline only command should not be available."));
		return false;
	}

	return true;
}
SD_END

#endif