/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandUnitTester.cpp
=====================================================================
*/

#include "CommandClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN

IMPLEMENT_CLASS(SD::CommandUnitTester, SD::UnitTester)

bool CommandUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestCommandList(testFlags) && TestExecCommand(testFlags));
	}

	return true;
}

bool CommandUnitTester::TestCommandList (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Command List"));
	SetTestCategory(testFlags, TXT("Parameter Extraction"));

	DString testCmd = TXT("MyCommand Param1 Param2 \"Param3 with Spaces\"");
	DString functionName;
	DString params;
	DString expectedFunctionName = TXT("MyCommand");
	DString expectedParams = TXT("Param1 Param2 \"Param3 with Spaces\"");
	CommandList::ExtractFunctionName(testCmd, functionName, params);
	TestLog(testFlags, TXT("Extracting function name from parameters from command \"%s\".  Function=\"%s\" Params=\"%s\""), testCmd, functionName, params);
	if (functionName != expectedFunctionName || params != expectedParams)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Extracting function name from its parameters from command \"%s\" should have returned function=\"%s\" and params=\"%s\".  Instead it returned function=\"%s\" and params=\"%s\""), {testCmd, expectedFunctionName, expectedParams, functionName, params});
		return false;
	}

	testCmd = TXT("MySimpleCommand");
	expectedFunctionName = TXT("MySimpleCommand");
	expectedParams = TXT("");
	CommandList::ExtractFunctionName(testCmd, functionName, params);
	TestLog(testFlags, TXT("Extracting function name from parameters from command \"%s\".  Function=\"%s\" Params=\"%s\""), testCmd, functionName, params);
	if (functionName != expectedFunctionName || params != expectedParams)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Extracting function name from its parameters from command \"%s\" should have returned function=\"%s\" and params=\"%s\".  Instead it returned function=\"%s\" and params=\"%s\""), {testCmd, expectedFunctionName, expectedParams, functionName, params});
		return false;
	}

	CompleteTestCategory(testFlags);
	SetTestCategory(testFlags, TXT("Find Registered Commands"));

	CommandEngineComponent* localCmdEngine = CommandEngineComponent::Find();
	CHECK(localCmdEngine != nullptr && localCmdEngine->GetCmdList() != nullptr);
	CommandList* cmdList = localCmdEngine->GetCmdList();
	DString targetExecFunction = TXT("CmdTester_NoParamFunction");
	const ExecCommand* execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_MultiParamFunction");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_2Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_3Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_4Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_5Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_6Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_7Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_8Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_OnlineOnly");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_CheatOfflineCommand");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Command List"));
	return true;
}

bool CommandUnitTester::TestExecCommand (EUnitTestFlags testFlags) const
{
	//Redirect the test to an object that implements various exec commands
	ExecCommandTester* tester = ExecCommandTester::CreateObject();
	CHECK(tester != nullptr)

	bool bResult = tester->RunTest(this, testFlags);
	tester->Destroy();

	return bResult;
}
SD_END

#endif