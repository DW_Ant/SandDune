/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ExecCommand.cpp
=====================================================================
*/

#include "CommandClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD::ExecCommand, SD::Object)

const DString ExecCommand::ParamDelimiter = TXT(" ");
bool ExecCommand::bIsOnline = false;
bool ExecCommand::bCheatsEnabled = true;

bool ExecCommand::IsCommandAvailable () const
{
	if (bIsOnline)
	{
		if ((ExecFunctionFlags & EFF_Online) == 0)
		{
			return false;
		}
	}
	else
	{
		if ((ExecFunctionFlags & EFF_Offline) == 0)
		{
			return false;
		}
	}

	if (!bCheatsEnabled && (ExecFunctionFlags & EFF_IsCheatCommand) > 0)
	{
		return false;
	}

	return true;
}

ExecCommand::EExecFunctionFlags ExecCommand::GetExecFunctionFlags () const
{
	return ExecFunctionFlags;
}

DString ExecCommand::GetFunctionName () const
{
	return FunctionName;
}

bool ExecCommand::ExtractParams (const DString& param, std::vector<DString>& outParamList, CommandEngineComponent::ECmdError& outError) const
{
	if (NumParams == 0)
	{
		outError = CommandEngineComponent::CE_NoError;
		return true;
	}

	if (param.IsEmpty())
	{
		outError = CommandEngineComponent::CE_InsufficientParams;
		return false;
	}

	PopulateParamList(param, outParamList);
	if (outParamList.size() < NumParams)
	{
		outError = CommandEngineComponent::CE_InsufficientParams;
		return false;
	}
	else if (outParamList.size() > NumParams)
	{
		outError = CommandEngineComponent::CE_TooManyParams;
		return false;
	}

	outError = CommandEngineComponent::CE_NoError;
	return true;
}

void ExecCommand::PopulateParamList (const DString& param, std::vector<DString>& outParamList) const
{
	INT curIdx = 0;
	INT prevSpaceIdx = -1;
	do
	{
		INT spaceIdx = param.Find(ParamDelimiter, curIdx, DString::CC_IgnoreCase, DString::SD_LeftToRight);
		if (spaceIdx == INT_INDEX_NONE)
		{
			//Push left over text to last param
			outParamList.push_back(param.SubString(prevSpaceIdx + 1));
			break;
		}

		if ((spaceIdx - prevSpaceIdx) > 1 && !param.IsWrappedByChar('\"', spaceIdx))
		{
			outParamList.push_back(param.SubString(prevSpaceIdx + 1, spaceIdx - 1));
			prevSpaceIdx = spaceIdx;
			curIdx = spaceIdx + 1;
			continue;
		}

		//This space character is either wrapped inside quotes, or there is a double space, ignore this character.
		curIdx = spaceIdx + 1;
	}
	while (curIdx < param.Length());

	//Trim quotes from any parameters surrounded by quotes
	for (UINT_TYPE i = 0; i < outParamList.size(); i++)
	{
		//If the first and last character is equal to delimiter char
		if (outParamList.at(i).At(0) == '\"' && outParamList.at(i).At(outParamList.at(i).Length() - 1) == '\"')
		{
			//Trim quotes
			outParamList.at(i) = outParamList.at(i).SubStringCount(1, outParamList.at(i).Length() - 2);
		}
	}
}
SD_END