/*
=====================================================================
  MIT License

  Copyright (c) 2016-2018 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.h
  A datatype that represents a direction.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef ROTATOR_H
#define ROTATOR_H

#include "Configuration.h"

#if INCLUDE_GEOMETRY2D
#include "Geometry2D.h"

/* Set this to false if your program prefers for Rotators to use Radians instead of degrees. */
#ifndef ROTATOR_UNIT_DEGREES
#define ROTATOR_UNIT_DEGREES 1
#endif

#if ROTATOR_UNIT_DEGREES
#define ROTATOR_RIGHT_ANGLE 90.f
#else
#define ROTATOR_RIGHT_ANGLE 1.570796327f
#endif

// pi/180
#define DEGREE_TO_RADIAN 0.017453293f

// 180/pi
#define RADIAN_TO_DEGREE 57.29577951f

SD_BEGIN
class Rotator : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Represents direction.  0 degrees = up, 90 = right, 180 = down, 270 left. */
	FLOAT Value;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rotator ();

	Rotator (FLOAT initialAngle);

	Rotator (float initialAngle);

	Rotator (const Rotator& copyRotator);


		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Rotator& copyRotator);
		

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the rotation and ensures the value is between [0, 359].
	 */
	virtual void SetRotation (FLOAT newRotation);

	/**
	 * Sets the rotation value based on the given axis.
	 * <1,0> => 0 degrees, <0,-1> => 90 degrees, <-1,0> => 180 degrees, <0,1> => 270 degrees.
	 */
	virtual void SetRotation (Vector2 axis);

	/**
	 * Returns true if the given rotator is perpendicular to this rotator.
	 */
	virtual bool IsPerpendicular (const Rotator& otherRotator) const;

	/**
	 * Returns true if the given rotator is parallel to this rotator.
	 */
	virtual bool IsParallel (const Rotator& otherRotator) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the x and y coordinates if you were to place the direction on a wheel.
	 * 0 degrees =>  (1, 0), 90 degrees => (0, -1), 180 degrees => (-1, 0), 270 degrees => (0, 1).
	 */
	virtual Vector2 GetAxis () const;

	virtual FLOAT GetRotation () const;

	/**
	 * Returns the rotator's value in degrees.
	 */
	virtual FLOAT GetDegrees () const;

	/**
	 * Returns the rotator's value in radians.
	 */
	virtual FLOAT GetRadians () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Ensures the rotation value ranges from [0-359] (or [0 - 2pi] for radians)
	 */
	virtual void ClampRotation ();
};

#pragma region "External Operators"
bool operator== (const Rotator& left, const Rotator& right);
bool operator!= (const Rotator& left, const Rotator& right);
Rotator operator+ (const Rotator& left, const Rotator& right);
Rotator operator+ (const Rotator& left, const FLOAT& right);
Rotator operator+ (const FLOAT &left, const Rotator& right);
Rotator& operator+= (Rotator& left, const Rotator& right);
Rotator& operator+= (Rotator& left, const FLOAT& right);
Rotator operator- (const Rotator& left, const Rotator& right);
Rotator operator- (const Rotator& left, const FLOAT& right);
Rotator operator- (const FLOAT& left, const Rotator& right);
Rotator& operator-= (Rotator& left, const Rotator& right);
Rotator& operator-= (Rotator& left, const FLOAT& right);
#pragma endregion
SD_END

#endif
#endif