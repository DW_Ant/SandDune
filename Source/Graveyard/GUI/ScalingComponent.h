/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScalingComponent.h
  A component responsible for correctly scaling and positioning absolute
  transformation components relative to a recent change in dimensions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SCALINGCOMPONENT_H
#define SCALINGCOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class ScalingComponent : public EntityComponent
	{
		DECLARE_CLASS(ScalingComponent)


		/*
		=====================
		  Datatypes
		=====================
		*/

	public:
		enum eHorizontalAction
		{
			HA_None,
			HA_Left,
			HA_Center,
			HA_Right
		};

		enum eVerticalAction
		{
			VA_None,
			VA_Top,
			VA_Center,
			VA_Bottom
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* The behavior how this entity aligns horizontally to borders. */
		eHorizontalAction HorizontalAlignment;

		/* The position offset from the border when positioning horizontally.  Not applicable for no or center alignment. */
		FLOAT HorizontalPositionOffset;

		/* Callback to use to figure out HorizontalPositionOffset.  Useful if you want to bind a particular variable to the position offset.  If not null, then this will override HorizontalPositionOffset variable. */
		SDFunction<FLOAT> OnCalcHorizontalPositionOffset;

		/* The behavior how this entity aligns vertically to borders. */
		eVerticalAction VerticalAlignment;

		/* The position offset from the border when positioning vertically.  Not applicable for no or center alignment. */
		FLOAT VerticalPositionOffset;

		/* Callback to use to figure out VerticalPositionOffset.  Useful if you want to bind a particular variable to the position offset.  If not null, then this will override VerticalPositionOffset variable. */
		SDFunction<FLOAT> OnCalcVerticalPositionOffset;

		/* The behavior how this entity scales horizontally when borders scale.  Note:  HorizontalAlignment may override where the owning entity is positioned. */
		eHorizontalAction HorizontalScaling;

		/* The behavior how this entity scales vertically when borders scale.  Note:  VerticalAlignment may override where the owning entity is positioned.*/
		eVerticalAction VerticalScaling;

		/* Attributes of other entity's dimensions of previous ScaleAttributes call (so it may scale relatively properly). */
		Vector2 OtherEntitySize;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void InitProps (const Object* copyObject) override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Clears cached reference to previous change in attributes.
		 */
		virtual void ClearHistory ();

		/**
		  Adjusts the given position and scaling attributes based on the given relative change
		  in scale and position of another entity.
		  @Param owningEntityBounds: bounds the owning component should align to when positioning itself (in abs coordinates).
		 */
		virtual void ScaleAttributes (const Vector2& currentPosition, const Vector2& currentSize, const Vector2& owningEntitySize, const Rectangle<FLOAT>& owningEntityBounds, Vector2& outNewPosition, Vector2& outNewSize);


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Calculates the relative change in size and position.
		 */
		virtual void CalculateRelativeSize (const Vector2& owningEntitySize, Vector2& outRelativeSize) const;
		virtual void SetSizeRelativeTo (const Vector2& relativeSize, const Vector2& currentSize, const Vector2& currentPosition, Vector2& outNewPosition, Vector2& outNewSize);

		/**
		  Adjusts outPosition to be aligned to the given bounds based on the alignment properties.
		  outPosition is expected to be initialized to the currentPosition.
		 */
		virtual void SetPositionRelativeTo (const Rectangle<FLOAT>& bounds, const Vector2& newSize, Vector2& outPosition);
	};
}

#endif
#endif