/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIComponent.h
  Contains base functionality for all UI Components.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

//TODO:  Have GUIComponent's listen to mouse events and pass events to sub UIs (but not register MouseInterface if the parent is another GUIComponent)

#ifndef GUICOMPONENT_H
#define GUICOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class GUITransformationComponent;
	class ScalingComponent;

	//TODO:  Handle case where the draw order does not align with UI components (ie:  button behind sprite takes mouse input even if it was rendered behind sprite)
	class GUIComponent : public EntityComponent, public WindowHandleInterface, public MouseInterface
	{
		DECLARE_CLASS(GUIComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Determines the priority value when the MouseInterface registers to the InputManager. */
		int InterfacePriority;

		/* Object responsible for handling this component's size and position. */
		GUIAttributes Attributes;

	protected:
		/* List of child GUIComponents that are expecting mouse events regardless of the conditions.
		The components handling the events should be able to support repeated mouse events since the normal 
		event may be triggered normally, and the second event will be broadcasted to these components.
		Note:  Events may not be passed to the components if this entity's MouseInterface is not registered. */
		std::vector<GUIComponent*> ForcedMouseEventsComponents;

		/* Component that determines how this GUI entity scales and adjusts position whenever the parent GUI Component changes attributes. */
		ScalingComponent* ScalingProperties;

		/* Component determining this GUI's scale and position. */
		GUITransformationComponent* TransformationComponent;

	private:
		/* Used to determine the difference before and after any size/position changes */
		INT OldAbsSizeX;
		INT OldAbsSizeY;
		INT OldAbsPositionX;
		INT OldAbsPositionY;

		/* Becomes true when this component is registered to the mouse interface. */
		bool bRegisteredToInterface;

		/* Becomes true when this component is registered to the OwningComponent's ForcedMouseEventsComponents vector. */
		bool bForcedMouseEvents;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		//Mouse Interface
		virtual void HandleMouseMove (const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool HandleMouseClick (const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual bool HandleMouseWheelMove (const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;

		//Window Handle interface
		virtual void HandleResolutionChange (INT oldResX, INT oldResY) override;

		//Super class
		virtual void InitProps () override;
		virtual void InitProps (const Object* copyObject) override;
		virtual void BeginObject () override;
		virtual bool SetOwner (Entity* newOwner) override;
		virtual void CalculateRenderPosition (sf::Vector2f& outResults, const Camera* targetCamera) override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Updates draw order for this component's render component and all children components' render components.
		  All children component's draw order will be one value higher than its parent.
		 */
		virtual void SetDrawOrder (unsigned int newDrawOrder);

		/**
		  Returns true if the given absolute coordinates are within the component's borders.
		 */
		virtual bool WithinBounds (unsigned int x, unsigned int y) const;
		virtual bool WithinBounds (INT x, INT y) const;

		virtual void SetAbsolutePosition (INT newPositionX, INT newPositionY);
		virtual void SetRelativePosition (FLOAT newPositionX, FLOAT newPositionY);

		virtual void SetAbsoluteSize (INT newSizeX, INT newSizeY);
		virtual void SetRelativeSize (FLOAT newSizeX, FLOAT newSizeY);

		/**
		  Notifies this component's attributes to update its clamp values since its owner's changes
		  may affect the attribute's limits.
		 */
		virtual void RefreshAttributeClamps ();

		/**
		  Checks the ShouldRegisterInterface function and (un)registers this object from the InputManager accordingly.
		 */
		virtual void ReevaluateMouseInterface ();

		/**
		  Unconditionally adds GUIComponent to the list of ForcedMouseEventsComponents vector.
		 */
		virtual void AddForcedMouseEventsComponent (GUIComponent* newTarget);

		/**
		  Removes targeted GUIComponent from the list of ForcedMouseEventsComponents vector.
		  Returns true if target was found and removed from the vector.
		 */
		virtual bool RemoveForcedMouseEventsComponent (GUIComponent* target);

		/**
		  Sets the bForcedMouseEvents bool to the new value, and registers this component to
		  the parent component's ForcedMouseEventsComponents vector appropriately.
		 */
		virtual void SetForcedMouseEvents (bool bNewForcedMouseEvents);


		/*
		=====================
		  Accessors
		=====================
		*/

		/**
		  Returns the GUIComponent that owns this component.  Returns self, if no GUIComponent owns this.
		 */
		virtual GUIComponent* GetParentGUIComponent ();

		virtual bool GetRegisteredToInterface () const;

		virtual bool GetForcedMouseEvents () const;

		virtual ScalingComponent* GetScalingProperties () const;

		virtual GUITransformationComponent* GetTransformationComponent () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Invoked from the MouseInterface class so that not all GUIComponents have to register themselves to the MouseInterface.
		  Instead the parent GUIComponents will only redirect the mouse events to its components if the mouse event is relevant.
		 */
		virtual void ExecuteMouseMove (const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool ExecuteMouseClick (const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
		virtual bool ExecuteMouseWheelMove (const sf::Event::MouseWheelScrollEvent& sfmlEvent);

		/**
		  Returns true, if mouse events are relevant for this component and its subcomponents.
		 */
		virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const;

		/**
		  Returns true if the conditions are right for this component to register its MouseInterface to the InputManager.
		 */
		virtual bool ShouldRegisterInterface ();

		/**
		  Invoked whenever position changed.
		 */
		virtual void HandlePositionChanged ();

		/**
		  Invoked whenever size changed.
		 */
		virtual void HandleSizeChanged ();


		/*
		=====================
		  Event Handler
		=====================
		*/

	protected:
		/**
		  Invoked whenever the TransformationComponent changed in scale or position.
		 */
		virtual void HandleTransformationChange ();
	};
}

#endif
#endif