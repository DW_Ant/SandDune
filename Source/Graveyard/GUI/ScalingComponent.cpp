/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScalingComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/


#include "GUIClasses.h"

#if INCLUDE_GUI

namespace SD
{
	IMPLEMENT_CLASS(ScalingComponent, EntityComponent)

	void ScalingComponent::InitProps ()
	{
		Super::InitProps();

		HorizontalAlignment = HA_Center;
		HorizontalPositionOffset = 0.f;
		VerticalAlignment = VA_Center;
		VerticalPositionOffset = 0.f;
		HorizontalScaling = HA_Center;
		VerticalScaling = VA_Center;

		OtherEntitySize = Vector2();
	}

	void ScalingComponent::InitProps (const Object* copyObject)
	{
		Super::InitProps(copyObject);

		const ScalingComponent* castCopyObject = dynamic_cast<const ScalingComponent*>(copyObject);
		if (VALID_OBJECT(castCopyObject))
		{
			HorizontalAlignment = castCopyObject->HorizontalAlignment;
			HorizontalPositionOffset = castCopyObject->HorizontalPositionOffset;
			OnCalcHorizontalPositionOffset = castCopyObject->OnCalcHorizontalPositionOffset;
			VerticalAlignment = castCopyObject->VerticalAlignment;
			VerticalPositionOffset = castCopyObject->VerticalPositionOffset;
			OnCalcVerticalPositionOffset = castCopyObject->OnCalcHorizontalPositionOffset;
			HorizontalScaling = castCopyObject->HorizontalScaling;
			VerticalScaling = castCopyObject->VerticalScaling;

			OtherEntitySize = castCopyObject->OtherEntitySize;
		}
	}

	void ScalingComponent::ClearHistory ()
	{
		OtherEntitySize = Vector2();
	}

	void ScalingComponent::ScaleAttributes (const Vector2& currentPosition, const Vector2& currentSize, const Vector2& owningEntitySize, const Rectangle<FLOAT>& owningEntityBounds, Vector2& outNewPosition, Vector2& outNewSize)
	{
		Vector2 relativeSizeChange;
		CalculateRelativeSize(owningEntitySize, relativeSizeChange);

		//Cache other entity's attributes for next time this function is called
		OtherEntitySize = owningEntitySize;

		if (relativeSizeChange.IsEmpty())
		{
			return; //No history found, can't scale attributes without a history
		}

		//Figure out the size first (position depends on size)
		SetSizeRelativeTo(relativeSizeChange, currentSize, currentPosition, outNewPosition, outNewSize);
		SetPositionRelativeTo(owningEntityBounds, outNewSize, outNewPosition);
	}

	void ScalingComponent::CalculateRelativeSize (const Vector2& owningEntitySize, Vector2& outRelativeSize) const
	{
		if (!OtherEntitySize.IsEmpty())
		{
			outRelativeSize = owningEntitySize / OtherEntitySize;
		}
	}

	void ScalingComponent::SetSizeRelativeTo (const Vector2& relativeSize, const Vector2& currentSize, const Vector2& currentPosition, Vector2& outNewPosition, Vector2& outNewSize)
	{
		Vector2 positionOffsets; //Amount to move to the left and up relative to the total size

		switch(HorizontalScaling)
		{
			case(HA_Left):
				positionOffsets.X = 1.f;
				break;

			case(HA_Center):
				positionOffsets.X = 0.5f;
				break;

			case(HA_Right):
				positionOffsets.X = 0.f;
				break;
		}

		switch(VerticalScaling)
		{
			case(VA_Top):
				positionOffsets.Y = 1.f;
				break;

			case(VA_Center):
				positionOffsets.Y = 0.5f;
				break;

			case(VA_Bottom):
				positionOffsets.Y = 0.f;
				break;
		}

		outNewSize = currentSize * relativeSize;

		//Disable scaling
		if (HorizontalScaling == HA_None)
		{
			outNewSize.X = currentSize.X;
		}

		if (VerticalScaling == VA_None)
		{
			outNewSize.Y = currentSize.Y;
		}

		const Vector2 deltaSize = outNewSize - currentSize;
		outNewPosition = currentPosition - (deltaSize * positionOffsets);
	}

	void ScalingComponent::SetPositionRelativeTo (const Rectangle<FLOAT>& bounds, const Vector2& newSize, Vector2& outPosition)
	{
		FLOAT curHorizontalOffset = (OnCalcHorizontalPositionOffset.IsBounded()) ? OnCalcHorizontalPositionOffset() : HorizontalPositionOffset;
		FLOAT curVerticalOffset = (OnCalcVerticalPositionOffset.IsBounded()) ? OnCalcVerticalPositionOffset() : VerticalPositionOffset;

		switch (HorizontalAlignment)
		{
			case(HA_None):
				break;

			case(HA_Left):
				outPosition.X = (bounds.Left + curHorizontalOffset);
				break;

			case(HA_Center):
				outPosition.X = ((bounds.GetWidth()/2.f) + bounds.Left) - (newSize.X * 0.5f);
				break;

			case(HA_Right):
				outPosition.X = (bounds.Right - newSize.X - curHorizontalOffset);
				break;
		}

		switch (VerticalAlignment)
		{
			case(VA_None):
				break;

			case(VA_Top):
				outPosition.Y = bounds.Top - curVerticalOffset;
				break;

			case(HA_Center):
				outPosition.Y = ((bounds.GetHeight()/2.f) + bounds.Top) - (newSize.Y * 0.5f);
				break;

			case(HA_Right):
				outPosition.Y = (bounds.Bottom - newSize.Y + curVerticalOffset);
				break;
		}
	}
}

#endif