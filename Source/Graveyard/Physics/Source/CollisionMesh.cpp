/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollisionMesh.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionMesh.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"

SD_BEGIN
CollisionMesh::CollisionMesh () :
	CollisionShape()
{
	Shape = ST_Mesh;
}

CollisionMesh::CollisionMesh (FLOAT inScale, const Vector3& inPosition) :
	CollisionShape(inScale, inPosition)
{
	Shape = ST_Mesh;
}

CollisionMesh::CollisionMesh (FLOAT inScale, const Vector3& inPosition, const std::vector<Vector3>& inVertices) :
	CollisionShape(inScale, inPosition),
	Vertices(inVertices)
{
	Shape = ST_Mesh;
}

void CollisionMesh::RefreshBoundingBox ()
{
	BoundingBox = Aabb::GenerateEncompassingAabb(Vertices);
	BoundingBox.Depth *= Scale;
	BoundingBox.Width *= Scale;
	BoundingBox.Height *= Scale;
	BoundingBox.Center *= Scale;
}

bool CollisionMesh::OverlapsWith (const CollisionShape* otherShape) const
{
	if (!CollisionShape::OverlapsWith(otherShape))
	{
		return false;
	}

	//Attempt to use the faster algorithms when checking against specific shapes
	switch (otherShape->GetShape())
	{
		case(ST_Sphere):
		{
			const CollisionSphere* otherSphere = dynamic_cast<const CollisionSphere*>(otherShape);
			CHECK(otherSphere != nullptr)
			return CollisionUtils::Overlaps(this, otherSphere);
		}
		case(ST_Capsule):
		{
			const CollisionCapsule* otherCapsule = dynamic_cast<const CollisionCapsule*>(otherShape);
			CHECK(otherCapsule != nullptr)
			return CollisionUtils::Overlaps(this, otherCapsule);
		}
		case(ST_Mesh):
		{
			const CollisionMesh* otherMesh = dynamic_cast<const CollisionMesh*>(otherShape);
			CHECK(otherMesh != nullptr)
			return CollisionUtils::Overlaps(this, otherMesh);
		}
	}

	//Unknown shapes are not supported
	return false;
}

bool CollisionMesh::EncompassesPoint (const Vector3& point) const
{
	//This shape does not have a volume. It can't encompass a point.
	return false;
}

Vector3 CollisionMesh::CalcClosestPoint (const Vector3& desiredPoint) const
{
	for (size_t i = 0; (i+2) < Vertices.size(); i += 3)
	{
		//TODO: Draw a perpendicular line from desiredPoint towards the plane of the triangle.
		//If the distance of that segment is shorter than the best dist, then find the point from the intersecting point towards the point on the triangle.
	}
}

void CollisionMesh::SetTriangles (const std::vector<Vector3>& newTriangles);
SD_END