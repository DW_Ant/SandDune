/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollisionUtils.cpp
=====================================================================
*/

#include "CollisionCapsule.h"
#include "CollisionMesh.h"
#include "CollisionSphere.h"
#include "CollisionUtils.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, CollisionUtils, SD, BaseUtils)

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionSphere* b)
{
	//Simple distance check
	FLOAT distSquared = (a->ReadPosition() - b->ReadPosition()).CalcDistSquared();

	return (distSquared <= std::pow((a->GetRadius() * a->GetScale()).Value + (b->GetRadius() * b->GetScale()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionCapsule* b)
{
	//If the sphere is vertically between the capsule's end points...
	if (a->ReadPosition().Z - b->ReadPosition().Z <= (b->GetScale() * b->GetHeight() * 0.5f))
	{
		//...Then only check the distance between the two centers on the XY plane.
		Vector3 delta = (a->ReadPosition() - b->ReadPosition());
		return (delta.CalcDistSquared(false) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
	}

	//Otherwise, compare the sphere's distance to one of the two end points.
	Vector3 endPoint(b->GetPosition());
	FLOAT halfHeight = (b->GetScale() * b->GetHeight() * 0.5f);
	endPoint.Z += (a->ReadPosition().Z < endPoint.Z) ? -halfHeight : halfHeight;

	return ((a->ReadPosition() - endPoint).CalcDistSquared(true) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionSphere* b)
{
	return Overlaps(b, a);
}

bool CollisionUtils::Overlaps (const CollisionSphere* a, const CollisionMesh* b)
{
	//Return true if the sphere encompasses this meshes' closest point towards the sphere's center
	return a->EncompassesPoint(b->CalcClosestPoint(a->ReadPosition()));
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionCapsule* b)
{
	//Identify if the two cylinders intersect along the Z-axis
	FLOAT deltaHeight = (a->ReadPosition().Z - b->ReadPosition().Z);
	if (deltaHeight <= (a->GetScale() * a->GetHeight() * 0.5f) + (b->GetScale() * b->GetHeight() * 0.5f))
	{
		Vector3 delta = (a->ReadPosition() - b->ReadPosition());

		//These overlap if the dist between their centers along the XY plane is within the sum of their radii.
		return (delta.CalcDistSquared(false) <= std::pow((a->GetScale() * a->GetRadius()).Value + (b->GetScale() * b->GetRadius()).Value, 2.f));
	}

	//Otherwise compare the two spherical end points
	Vector3 lowerSphere;
	Vector3 upperSphere;

	//Figure out which two of the four spheres are closest to each other.
	if (a->ReadPosition().Z < b->ReadPosition.Z)
	{
		lowerSphere = a->GetPosition();
		lowerSphere.Z -= (a->GetScale() * a->GetHeight() * 0.5f);
		upperSphere = b->GetPosition();
		upperSphere.Z += (b->GetScale() * b->GetHeight() * 0.5f);
	}
	else
	{
		lowerSphere = b->GetPosition();
		lowerSphere.Z -= (b->GetScale() * b->GetHeight() * 0.5f);
		upperSphere = a->GetPosition();
		upperSphere.Z += (a->GetScale() * a->GetHeight() * 0.5f);
	}

	return ((upperSphere - lowerSphere).CalcDistSquared() < std::pow((a->GetRadius() * a->GetScale()).Value + (b->GetRadius() * b->GetScale()).Value, 2.f));
}

bool CollisionUtils::Overlaps (const CollisionCapsule* a, const CollisionMesh* b)
{
	for (size_t i = 0; (i+2) < b->ReadVertices().size(); i += 3)
	{
		//Get the line perpendicular to two vectors that intersect all points in the triangle
		Vector3 vecA = b->ReadVertices().at(i+1) - b->ReadVertices().at(i);
		Vector3 vecB = b->ReadVertices().at(i+2) - b->ReadVertices().at(i);

		Vector3 crossVec = vecA.CrossProduct(vecB);
		if (crossVec.IsEmpty())
		{
			PhysicsLog.Log(LogCategory::LL_Warning, TXT("Invalid mesh detected. Cannot calculate a plane from three collinear points [%s, %s, %s]. Make sure every three vertices inside a mesh generates a triangle with an area greater than 0."), b->ReadVertices()[i], b->ReadVertices()[i+1], b->ReadVertices()[i+2]);
			continue; //Ignore this triangle and try the next one.
		}

		if (crossVec.Z == 0.f)
		{
			//TODO: Handle special case where the plane is vertical
		}

		//Since capsules are upright, figure out what is the point on the plane where a vertical line intersects both the plan and the capsule's Position.
		//Formula used: a(x-x0) + b(y-y0) + c(z-z0) = 0 where
		//	a,b,c is the crossVec
		//	x,y,z is the position that intersects the plane and the vertical line that intersects the capsule center
		//	x0, y0, z0 is the point on the plane that is also one of the vertices on the triangle.
		//Solve for z
		//c(z-z0) = -a(x-x0) - b(y-y0)
		//(z-z0) = [-a(x-x0) - b(y-y0)] / c
		FLOAT zPos = (((-crossVec.X * a->ReadPosition().X) + (crossVec.X * b->ReadVertices()[i].X)) + ((-crossVec.Y * a->ReadPosition().Y) + (crossVec.Y * a->ReadPosition().Y))) / crossVec.Z;
		zPos += b->ReadVertices()[i].Z;

		//TODO: This is wrong. Think of a wide capsule approaching a steap triangle
		//Point b->ReadPosition().X, b->ReadPosition().Y, zPos resides on the plane that is parallel to the triangle.
		//First check if it's even within the capsule height (is the plane too far above or below it?)
		if (FLOAT::Abs(zPos - a->ReadPosition().Z) > a->GetHeight() * 0.5f)
		{
			//Capsule is too high above or below the plane to intersect. Test next triangle.
			continue;
		}

		//The point is within the height range, but now we need to see if the intersecting point resides within the triangle.
	}
}

bool CollisionUtils::Overlaps (const CollisionMesh* a, const CollisionSphere* b);
bool CollisionUtils::Overlaps (const CollisionMesh* a, const CollisionCapsule* b);
bool CollisionUtils::Overlaps (const CollisionMesh* a, const CollisionMesh* b);
SD_END