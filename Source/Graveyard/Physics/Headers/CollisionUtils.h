/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollisionUtils.h

  Defines a series of functions that handles special cases that can quickly calculate
  collision detection.
=====================================================================
*/

#pragma once

#include "Physics.h"

SD_BEGIN
class CollisionSphere;
class CollisionCapsule;
class CollisionMesh;

class PHYSICS_API CollisionUtils : public BaseUtils
{
	DECLARE_CLASS(CollisionUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the two shapes overlap each other.
	 * These functions assume the shapes' Aabb are already overlapping.
	 */
	static bool Overlaps (const CollisionSphere* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionSphere* a, const CollisionCapsule* b);
	static bool Overlaps (const CollisionSphere* a, const CollisionMesh* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionCapsule* b);
	static bool Overlaps (const CollisionCapsule* a, const CollisionMesh* b);
	static bool Overlaps (const CollisionMesh* a, const CollisionSphere* b);
	static bool Overlaps (const CollisionMesh* a, const CollisionCapsule* b);

	/**
	 * Very expensive function. Use sparingly with meshes with small polygon count. O(n^2)
	 */
	static bool Overlaps (const CollisionMesh* a, const CollisionMesh* b);

	/**
	 * Identifies a plane (defined by a point and a perpendicular direction) based on three non-collinear points.
	 * Returns false if the three points are collinear and no plane is defined.
	 */
	//static bool CalcPlaneFromPoints (const Vector3& ptA, const Vector3& ptB, const Vector3& ptC,
};
SD_END