/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollisionMesh.h

  A 3D geometric collision shape that is defined by a series of arbitrary triangles. Each triangle is
  defined by 3 vertices that are relative to the shape's position.

  CollisionMeshes are not required to assemble meshes to form a complete 3D polygon. Gaps are permitted, and
  2D planes are permitted. Because of this, this mesh does not have any fill at all. Objects inside the mesh
  are not overlapping. Collision is only checked at triangle intersections.

  It's very expensive to check for collision against meshes since it checks each triangle intersection.
  Make sure to use very simple meshes. If complicated meshes are required, use multiple CollisionMeshes so
  that their Aabbs may filter sections of the meshes.
=====================================================================
*/

#pragma once

#include "CollisionShape.h"

SD_BEGIN
class PHYSICS_API CollisionMesh : public CollisionShape
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* A series of vertices that compose this mesh.
	Every third vertex constructs a triangle connecting the previous two vertices.
	These coordinates are relative to the shape's Position. */
	std::vector<Vector3> Vertices;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollisionMesh ();
	CollisionMesh (FLOAT inScale, const Vector3& inPosition);
	CollisionMesh (FLOAT inScale, const Vector3& inPosition, const std::vector<Vector3>& inVertices);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RefreshBoundingBox () override;
	virtual bool OverlapsWith (const CollisionShape* otherShape) const override;
	virtual bool EncompassesPoint (const Vector3& point) const override;
	virtual Vector3 CalcClosestPoint (const Vector3& desiredPoint) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetVertices (const std::vector<Vector3>& newVertices);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<Vector3>& ReadVertices () const
	{
		return Vertices;
	}
};
SD_END