/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ConsoleOverlay.h
  Entity that contains an interface that converts user input into commands.
=====================================================================
*/

#pragma once

#include "Console.h"

SD_BEGIN
class CONSOLE_API ConsoleOverlay : public GuiEntity
{
	DECLARE_CLASS(ConsoleOverlay)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Time it takes for the animation to slide the console in and out (in seconds). */
	FLOAT SlideDuration;

protected:
	/* Tick Component responsible for animating the Console sliding animation. */
	DPointer<TickComponent> SliderTicker;

	/* Start time at which the SliderTicker began the Console slide animation. */
	FLOAT StartSlideTime;

	/* UI component that lists the console logs. */
	DPointer<TextFieldComponent> ConsoleOutput;

	DPointer<FrameComponent> OutputBackground;

	/* UI component where the user can enter commands. */
	DPointer<TextFieldComponent> InputField;

	DPointer<FrameComponent> InputBackground;

	/* UI component that displays the most recent commands. */
	DPointer<ListBoxComponent> CommandHistory;

	/* Checkbox that determines if the console overlay should automatically collapse after a command. */
	DPointer<CheckboxComponent> AutoCollapseComponent;

	/* Becomes true if the console is expanded and capturing input. */
	bool bExpanded;

	/* Absolute coordinates where the console UI is positioned when the console is collapsed. */
	Vector2 CollapsePos;

	/* Absolute coordinates where the console UI is positioned when the console is expanded. */
	Vector2 ExpandPos;

	/* Lists of console output messages displayed in ConsoleOutput.  Its length is limited based on value of MaxNumOutputMessages.
	The messages are ordered from oldest [0] to youngest [size-1]. */
	std::vector<DString> OutputMessages;

	/* Determines the maximum number of output messages that can be cached. */
	INT MaxNumOutputMessages;

private:
	Engine* LocalEngine;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual void LoseFocus () override;
	virtual void GainFocus () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Slides the console over the screen, sets focus, and consumes input.
	 */
	virtual void ShowConsole ();

	/**
	 * Slides the console beyond the screen, loses focus, and no longer consumes input.
	 */
	virtual void HideConsole ();

	/**
	 * Pushes a new console output log to the text queue.
	 */
	virtual void AddOutputLog (const DString& formattedMsg);

	virtual void SetMaxNumOutputMessages (INT newMaxNumOutputMessages);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(INT, MaxNumOutputMessages)

	inline bool IsExpanded () const
	{
		return bExpanded;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeConsoleOutput ();
	virtual void InitializeInputField ();
	virtual void InitializeCommandHistory ();
	virtual void InitializeAutoCollapseComponent ();

	/**
	 * Executes the string as a command, and notifies various UI components of the new command.
	 */
	virtual void ExecuteConsoleCommand (const DString& cmd);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& input);
	virtual bool HandleInputFieldEdit (const sf::Event& input);
	virtual void HandleCommandHistorySelected (INT selectedIdx);
	virtual void HandleAutoCollapseToggle (CheckboxComponent* uiComponent);
	virtual void HandleSliderTick (FLOAT deltaSec);
};
SD_END