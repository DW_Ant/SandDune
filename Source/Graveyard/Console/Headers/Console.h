/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Console.h
  Contains important file includes and definitions for the Console module.

  The console module instantiates a menu item on the main window that converts
  user input into Commands.  The Console also displays the most recent console
  log messages.

  This module depends on the following modules:
  - Core
  - Graphics
  - Gui
  - File
  - Input
  - Command
  - Localization
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Command\Headers\CommandClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"

#if !(MODULE_CORE)
#error The Console module requires the Core module.
#endif

#if !(MODULE_FILE)
#error The Console module requires the File module.
#endif

#if !(MODULE_GRAPHICS)
#error The Console module requires the Graphics module.
#endif

#if !(MODULE_INPUT)
#error The Console module requires the Input module.
#endif

#if !(MODULE_GUI)
#error The Console module requires the Gui module.
#endif

#if !(MODULE_COMMAND)
#error The Console module requires the Command module.
#endif

#if !(MODULE_LOCALIZATION)
#error The Console module requires the Localization module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef CONSOLE_EXPORT
		#define CONSOLE_API __declspec(dllexport)
	#else
		#define CONSOLE_API __declspec(dllimport)
	#endif
#else
	#define CONSOLE_API
#endif

SD_BEGIN
extern LogCategory CONSOLE_API ConsoleLog;
SD_END