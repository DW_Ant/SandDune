/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ConsoleEngineComponent.cpp
=====================================================================
*/

#include "ConsoleClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD::ConsoleEngineComponent)

void ConsoleEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	Console = ConsoleOverlay::CreateObject();
}

void ConsoleEngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{
	Super::ProcessLog(category, logLevel, formattedMsg);

	if ((category->GetUsageFlags() & LogCategory::FLAG_CONSOLE_MSG) > 0 && Console != nullptr)
	{
		Console->AddOutputLog(formattedMsg);
	}
}

ConsoleOverlay* ConsoleEngineComponent::GetConsole () const
{
	return Console.Get();
}
SD_END