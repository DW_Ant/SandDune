/*
=====================================================================
  MIT License

  Copyright (c) 2016-2021 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ConsoleOverlay.cpp
=====================================================================
*/

#include "ConsoleClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD::ConsoleOverlay, SD::GuiEntity)


void ConsoleOverlay::InitProps ()
{
	Super::InitProps();

	SlideDuration = 0.25f;
	SliderTicker = nullptr;
	StartSlideTime = -1.f;
	ConsoleOutput = nullptr;
	OutputBackground = nullptr;
	InputField = nullptr;
	InputBackground = nullptr;
	bExpanded = false;
	CommandHistory = nullptr;
	AutoCollapseComponent = nullptr;

	CollapsePos = Vector2::ZERO_VECTOR;
	ExpandPos = Vector2::ZERO_VECTOR;

	LocalEngine = nullptr;
}

void ConsoleOverlay::BeginObject ()
{
	Super::BeginObject();

	LocalEngine = Engine::FindEngine();
	CHECK(LocalEngine != nullptr)

	SliderTicker = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(SliderTicker))
	{
		SliderTicker->SetTickHandler(SDFUNCTION_1PARAM(this, ConsoleOverlay, HandleSliderTick, void, FLOAT));
		SliderTicker->SetTicking(false);
	}

	InputComponent* input = InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->SetInputPriority(1000000);
		input->OnInput = SDFUNCTION_1PARAM(this, ConsoleOverlay, HandleInput, bool, const sf::Event&);
	}

	ConfigWriter* config = ConfigWriter::CreateObject();
	if (config != nullptr)
	{
		if (config->OpenFile(Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + DString(TXT("Console.ini")), false))
		{
			SlideDuration = config->GetProperty<FLOAT>(TXT("ConsoleOverlay"), TXT("SlideDuration"));
			SetMaxNumOutputMessages(config->GetProperty<INT>(TXT("ConsoleOverlay"), TXT("MaxNumOutputMessages")));

			if (AutoCollapseComponent != nullptr)
			{
				AutoCollapseComponent->SetChecked(config->GetProperty<BOOL>(TXT("ConsoleOverlay"), TXT("AutoCollapse")));
			}
		}

		config->Destroy();
	}

	SetPosition(IsExpanded() ? ExpandPos : CollapsePos);
}

void ConsoleOverlay::LoseFocus ()
{
	Super::LoseFocus();

	HideConsole();
}

void ConsoleOverlay::GainFocus ()
{
	Super::GainFocus();

	ShowConsole();
}

void ConsoleOverlay::ConstructUI ()
{
	InitializeConsoleOutput();
	InitializeInputField();
	InitializeAutoCollapseComponent();
	InitializeCommandHistory();

	SetSize(Vector2(1.f, 1.f));
}

void ConsoleOverlay::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus != nullptr)
	Focus->TabOrder.push_back(InputField.Get());
	Focus->TabOrder.push_back(CommandHistory.Get());
}

void ConsoleOverlay::Destroyed ()
{
	Super::Destroyed();
}

void ConsoleOverlay::ShowConsole ()
{
	if (InputField != nullptr)
	{
		InputField->SetText(DString::EmptyString);
		InputField->SetCursorPosition(0);
		InputField->SetHighlightEndPosition(-1);
	}

	StartSlideTime = LocalEngine->GetElapsedTime();
	SliderTicker->SetTicking(true);
	bExpanded = true;
}

void ConsoleOverlay::HideConsole ()
{
	if (InputField != nullptr)
	{
		InputField->SetCursorPosition(-1);
	}

	StartSlideTime = LocalEngine->GetElapsedTime();
	SliderTicker->SetTicking(true);
	bExpanded = false;
}

void ConsoleOverlay::AddOutputLog (const DString& formattedMsg)
{
	OutputMessages.push_back(formattedMsg);
	if (OutputMessages.size() > MaxNumOutputMessages)
	{
		OutputMessages.erase(OutputMessages.begin());
	}

	if (ConsoleOutput != nullptr)
	{
		DString fullContent = DString::EmptyString;
		for (UINT_TYPE i = 0; i < OutputMessages.size(); i++)
		{
			fullContent += OutputMessages.at(i) + TXT("\n");
		}

		ConsoleOutput->SetText(fullContent);
	}
}

void ConsoleOverlay::SetMaxNumOutputMessages (INT newMaxNumOutputMessages)
{
	MaxNumOutputMessages = Utils::Max<INT>(0, newMaxNumOutputMessages);

	INT numToRemove = OutputMessages.size() - MaxNumOutputMessages;
	if (numToRemove > 0)
	{
		OutputMessages.erase(OutputMessages.begin() + (OutputMessages.size() - numToRemove.ToUnsignedInt()), OutputMessages.end());
	}
}

void ConsoleOverlay::InitializeConsoleOutput ()
{
	CHECK(ConsoleOutput == nullptr)

	OutputBackground = FrameComponent::CreateObject();
	if (AddComponent(OutputBackground))
	{
		OutputBackground->SetLockedFrame(true);

		ConsoleOutput = TextFieldComponent::CreateObject();
		if (OutputBackground->AddComponent(ConsoleOutput))
		{
			ConsoleOutput->SetAutoRefresh(false);
			ConsoleOutput->SetWrapText(true);
			ConsoleOutput->SetClampText(true);
			ConsoleOutput->SetEditable(false);
			ConsoleOutput->SetSelectable(true);
			ConsoleOutput->SetVerticalAlignment(LabelComponent::VA_Bottom);
			ConsoleOutput->SetAutoRefresh(true);
		}
	}
}

void ConsoleOverlay::InitializeInputField ()
{
	CHECK(InputField == nullptr && ConsoleOutput != nullptr)

	InputBackground = FrameComponent::CreateObject();
	if (OutputBackground->AddComponent(InputBackground))
	{
		InputBackground->SetLockedFrame(true);

		InputField = TextFieldComponent::CreateObject();
		if (InputBackground->AddComponent(InputField))
		{
			InputField->SetAutoRefresh(false);
			InputField->SetWrapText(false);
			InputField->SetEditable(true);
			InputField->SetAutoRefresh(true);
			InputField->OnEdit = SDFUNCTION_1PARAM(this, ConsoleOverlay, HandleInputFieldEdit, bool, const sf::Event&);
		}
	}
}

void ConsoleOverlay::InitializeCommandHistory ()
{
	CHECK(CommandHistory == nullptr && InputField != nullptr)

	CommandHistory = ListBoxComponent::CreateObject();
	if (InputField->AddComponent(CommandHistory))
	{
		CommandHistory->SetAutoDeselect(true);
		CommandHistory->SetReadOnly(false);
		CommandHistory->SetVisibility(false);
		CommandHistory->OnItemSelected = SDFUNCTION_1PARAM(this, ConsoleOverlay, HandleCommandHistorySelected, void, INT);
	}
}

void ConsoleOverlay::InitializeAutoCollapseComponent ()
{
	CHECK(AutoCollapseComponent == nullptr && InputBackground != nullptr)

	AutoCollapseComponent = CheckboxComponent::CreateObject();
	if (InputBackground->AddComponent(AutoCollapseComponent))
	{
		AutoCollapseComponent->CaptionComponent->SetText(TXT("Auto Close"));
		AutoCollapseComponent->OnChecked = SDFUNCTION_1PARAM(this, ConsoleOverlay, HandleAutoCollapseToggle, void, CheckboxComponent*);
		AutoCollapseComponent->SetCheckboxRightSide(true);
	}
}

void ConsoleOverlay::ExecuteConsoleCommand (const DString& cmd)
{
	if (InputField != nullptr)
	{
		InputField->SetText(DString::EmptyString);
		InputField->SetCursorPosition(0);
		InputField->SetHighlightEndPosition(-1);
	}

	if (CommandHistory != nullptr)
	{
		const std::vector<BaseGuiDataElement*>& prevCmds = CommandHistory->ReadList();

		//If previous command is different from what was executed
		if (prevCmds.size() <= 0 || cmd.Compare(prevCmds.at(prevCmds.size() - 1)->GetLabelText(), DString::CC_IgnoreCase) != 0)
		{
			CommandHistory->AddOption(GuiDataElement<DString>(cmd, cmd));
		}
	}

	//Execute the command, itself
	CommandEngineComponent* cmdEngine = CommandEngineComponent::Find();
	CHECK(cmdEngine != nullptr && cmdEngine->GetCmdList())

	CommandEngineComponent::ECmdError cmdError;
	cmdEngine->GetCmdList()->ProcessCommand(cmd, cmdError);

	DString output = DString::EmptyString;
	DString errorKey;
	switch (cmdError)
	{
		case (CommandEngineComponent::ECmdError::CE_NoError):
			errorKey = DString::EmptyString;
			break;

		case (CommandEngineComponent::ECmdError::CE_UnknownCmd):
			errorKey = TXT("UnknownCommand");
			break;

		case (CommandEngineComponent::ECmdError::CE_CmdNotAvailable):
			errorKey = TXT("CommandNotAvailable");
			break;

		case (CommandEngineComponent::ECmdError::CE_InsufficientParams):
			errorKey = TXT("NotEnoughParams");
			break;

		case (CommandEngineComponent::ECmdError::CE_TooManyParams):
			errorKey = TXT("TooManyParams");
			break;
	}

	if (!errorKey.IsEmpty())
	{
#if INCLUDE_LOCALIZATION
		TextTranslator* translator = TextTranslator::GetTranslator();
		if (translator != nullptr)
		{
			output = translator->TranslateText(errorKey, TXT("Console"), TXT("ConsoleOverlay"));
		}
#else
		output = TXT("Unrecognized command");
#endif

		if (!output.IsEmpty())
		{
			ConsoleLog.Log(LogCategory::LL_Log, output);
		}
	}

	if (AutoCollapseComponent != nullptr && AutoCollapseComponent->GetChecked())
	{
		HideConsole();
	}
}

bool ConsoleOverlay::HandleInput (const sf::Event& input)
{
	//Check for activating and deactivating console
	if (input.key.code != sf::Keyboard::Tilde)
	{
		return false;
	}

	if (input.type == sf::Event::KeyReleased)
	{
		IsExpanded() ? HideConsole() : ShowConsole();
	}

	return true;
}

bool ConsoleOverlay::HandleInputFieldEdit (const sf::Event& input)
{
	CHECK(InputField != nullptr)

	if (input.key.code == sf::Keyboard::Return)
	{
		if (input.type == sf::Event::KeyPressed)
		{
			DString inputText = InputField->GetContent();
			inputText.TrimSpaces();
			if (!inputText.IsEmpty())
			{
				ExecuteConsoleCommand(inputText);
			}
		}
	}
	else if (IsExpanded())
	{
		if (input.key.code == sf::Keyboard::Escape)
		{
			if (input.type == sf::Event::KeyPressed)
			{
				HideConsole();
			}

			return true;
		}
		/*
		TODO:  Implement command history after refactoring GuiEntities
		else if (input.key.code == sf::Keyboard::Up || input.key.code == sf::Keyboard::Down)
		{
			CommandHistory->SetVisibility(true);
		}
		*/
	}

	return false;
}

void ConsoleOverlay::HandleCommandHistorySelected (INT selectedIdx)
{
	CHECK(InputField != nullptr && CommandHistory != nullptr)

	DString selectedText = CommandHistory->ReadList().at(selectedIdx.ToUnsignedInt())->GetLabelText();
	InputField->SetText(selectedText);
	CommandHistory->SetVisibility(false);
}

void ConsoleOverlay::HandleAutoCollapseToggle (CheckboxComponent* uiComponent)
{
	CHECK(AutoCollapseComponent != nullptr)

	if (!ConfigWriter::SavePropertyText(Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + DString(TXT("Console.ini")), TXT("ConsoleOverlay"), TXT("AutoCollapse"), BOOL(AutoCollapseComponent->GetChecked()).ToString()))
	{
		ConsoleLog.Log(LogCategory::ELogLevel::LL_Warning, TXT("Unable to save the AutoCollapse flag to config file."));
	}
}

void ConsoleOverlay::HandleSliderTick (FLOAT deltaSec)
{
	if (StartSlideTime > 0.f)
	{
		Vector2 destination = (bExpanded) ? ExpandPos : CollapsePos;

		//Handle edge cases
		if (SlideDuration <= 0.f)
		{
			//Disable animation and make the UI snap to position instantly
			StartSlideTime = -1.f;
			SliderTicker->SetTicking(false);

			SetPosition(destination);
			return;
		}

		FLOAT finishedTime = SlideDuration + StartSlideTime;
		FLOAT ratio = (LocalEngine->GetElapsedTime() - StartSlideTime) / (finishedTime - StartSlideTime);
		if (ratio >= 1.f)
		{
			//Disable animation and make the UI snap to destination
			StartSlideTime = -1.f;
			SliderTicker->SetTicking(false);

			SetPosition(destination);
			return;
		}

		Vector2 sourcePos = (bExpanded) ? CollapsePos : ExpandPos;
		Vector2 lerpedDestination = Vector2::Lerp(ratio, sourcePos, destination);
		SetPosition(lerpedDestination);
	}
}
SD_END