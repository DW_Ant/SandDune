/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MultiThreadEngineComponent.h
  Other ForkedEngines will be able to communicate with the main Engine through this component.
  If any ForkedEngines are pending to read data from the main Engine, this component will lock the
  main engine to create an opportunity for ThreadedComponents to read data.

  ForkedEngines are created through this EngineComponent, and this EngineComponent may join/end the forked
  engines.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef MULTITHREADENGINECOMPONENT_H
#define MULTITHREADENGINECOMPONENT_H

#include "MultiThread.h"

#if INCLUDE_MULTITHREAD

namespace SD
{
	class MultiThreadEngineComponent : public EngineComponent
	{
		DECLARE_ENGINE_COMPONENT(MultiThreadEngineComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* If true, then this EngineComponent will lock the main engine at the end of its Tick cycle. */
		bool bPendingLock;


		/*
		=====================
		  Constructors
		=====================
		*/

	protected:
		MultiThreadEngineComponent ();


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void PostTick (FLOAT deltaSec) override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Creates an instance of a threaded engine to run in parallel with the main thread.
		  The parameter is the callback to invoke on the threaded engine.
		  This function is thread-safe.
		 */
		void ForkEngine (SDFunction<void,> initializationSequence);



		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void LockOwningEngine ();
	};
}

#endif
#endif