/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsCore.h
  This singleton class contains base utility functions and defines about
  rendering content to the screen.

  The GraphicsCore also contains functions about the window handler, resource
  pools, and render targets.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GRAPHICSCORE_H
#define GRAPHICSCORE_H

#include "Core.h"
#include "ResourcePool.h"
#include "RenderTarget.h"

#if INCLUDE_CORE

namespace SD
{
	class GraphicsCore
	{

		/*
		=====================
		  Data types
		=====================
		*/

	public:
		struct SRenderTargetInfo
		{
			RenderTarget* AssociatedRenderTarget;
			FLOAT TargetFrameRate; //Converted from frames per second to elapsed time
			FLOAT TimeRemaining; //Time remaining before the render target must generate a scene
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the current window */
		static sf::RenderWindow* WindowHandle;

		//Global draw order defines entities are drawn in ascending order.
		static const unsigned int DrawOrderBackground = 100000;
		static const unsigned int DrawOrderMainScene = 200000;
		static const unsigned int DrawOrderForeground = 300000;
		static const unsigned int DrawOrderUI = 400000;

	protected:
		/* Reference to the instantiated Graphics Engine */
		static GraphicsCore* GraphicsInstance;

		/* Reference to the current dimensions of the window size. */
		static INT WindowHeight;
		static INT WindowWidth;

		bool bFullScreen = false;

		std::vector<ResourcePool*> RegisteredResourcePools;
		std::vector<SRenderTargetInfo> RenderTargetList;

		/* Reference to the main render target. */
		RenderTarget* PrimaryRenderTarget = nullptr;

	private:
		static bool bInitialized;

		/* Color to turn to when clearing the back buffer. */
		static const float BackBufferResetColor[4];


		/*
		=====================
		  Constructors
		=====================
		*/

	protected:
		GraphicsCore ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Setups up SFML
		 */
		virtual void InitializeGraphicsEngine ();

		/**
		  Informs all instanced objects that implement the WindowHandleInterface that the resolution changed.
		 */
		virtual void BroadcastResolutionChange ();

		/**
		  Adds the resource pool to the resource pools vector (for resource cleanup)
		 */
		virtual void RegisterResourcePool (ResourcePool* newResourcePool);

		/**
		  Adds the RenderTarget to the RenderTargetList vector
		 */
		virtual void RegisterRenderTarget (RenderTarget* newRenderTarget);

		/**
		  Removes the target from the RenderTargetList vector.
		 */
		virtual void UnregisterRenderTarget (RenderTarget* target);

		/**
		  Invokes the GraphicsEngine to update the RenderTargetList's TargetFrameRate to the renderTarget's current FrameRate.
		 */
		virtual void UpdateRenderTargetFrameRate (RenderTarget* renderTarget, bool bResetElapsedTime = true);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		 The method to obtain an instance of the GraphicsCore
		 */
		static GraphicsCore* GetGraphicsCore ();

		virtual void GetWindowSize (INT& outWidth, INT& outHeight) const;

		/**
		  Retrieves the OS's coordinates of the current window.  If bIncludeTitlebar is false, then it'll retrieve
		  the coordinates within the view.
		 */
		virtual void GetWindowPosition (INT& outPosX, INT& outPosY, bool bIncludeTitlebar = false) const;

		virtual const RenderTarget* GetPrimaryRenderTarget () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Called on every update where the deltaTime is the time elapsed since last tick
		 */
		virtual void Tick (FLOAT deltaTime);

		/**
		  Setups the scene to draw for the next frame
		 */
		virtual void PreRenderFrame ();

		/**
		  Presents the scene.
		 */
		virtual void PostRenderFrame ();

		/**
		  Shutsdown graphics core and cleans resources
		 */
		virtual void Shutdown ();

	friend class Engine;
	};
}

#endif
#endif