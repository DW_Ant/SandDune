/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HelloPluginsEngineComponent.cpp
=====================================================================
*/

#include "HelloPluginsEngineComponent.h"

IMPLEMENT_ENGINE_COMPONENT(SD::HelloPluginsEngineComponent)
SD_BEGIN

void HelloPluginsEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	HelloPluginsLog.Log(LogCategory::LL_Log, TXT("Hello Plugins Engine Component is initialized!"));
}

void HelloPluginsEngineComponent::ShutdownComponent ()
{
	HelloPluginsLog.Log(LogCategory::LL_Log, TXT("Hello Plugins Engine Component is shutting down!"));

	Super::ShutdownComponent();
}
SD_END