/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HelloPlugins.cpp
=====================================================================
*/

#include "HelloPlugins.h"
#include "HelloPluginsEngineComponent.h"

SD_BEGIN
LogCategory HelloPluginsLog(TXT("HelloPlugins"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);

HelloPluginsEngineComponent* ComponentInstance = nullptr;

int PLUGIN_FUNCTION_DECORATION StartPlugin (size_t engineIdx)
{
	/* This function could check for any dependencies from the engine. If there's a missing dependency, then
	the DelayedStartPlugin function will be invoked every frame until it returns true.
	Since this plugin doesn't have any dependencies, it'll load immediately.*/
	ComponentInstance = new HelloPluginsEngineComponent();
	Engine* owningEngine = Engine::GetEngine(engineIdx);
	CHECK(owningEngine != nullptr)
	bool success = owningEngine->AddEngineComponent(ComponentInstance);
	if (!success)
	{
		HelloPluginsLog.Log(LogCategory::LL_Warning, TXT("Hello Plugins could not register its engine component since the engine rejected it."));
		delete ComponentInstance;
		ComponentInstance = nullptr;

		//Notify the plugin component that this plugin has failed to load
		return -1;
	}

	return 1;
}

bool PLUGIN_FUNCTION_DECORATION DelayedStartPlugin (size_t engineIdx, float deltaSec)
{
	/* If this plugin required a particular plugin to be registered first, it can use this function to check frequently if the engine is ready. */
#if 0
	//Example
	Engine* owningEngine = Engine::GetEngine(engineIdx);
	const std::vector<EngineComponent*>& readyEngineComps = owningEngine->ReadEngineComponents();
	for (EngineComponent* comp : readyEngineComps)
	{
		//GraphicsEngineComponent is just an example
		if (dynamic_cast<MyRequiredEngineComponent*>(comp) != nullptr)
		{
			//Register HelloPluginsEngineComponent to owningEngine just like StartPlugin
			//Return true in order to stop the plugin manager from calling this function.
		}
	}
	return false; //Notify plugin manager to call this function later.
#endif

	return true; //Return true immediately since this plugin should have been initialized at StartPlugin instead.
}

void PLUGIN_FUNCTION_DECORATION UpdatePlugin (float deltaSec)
{
	//This function is for plugins without EngineComponents or Entities not using SD's framework.
	//EngineComponents from plugins can still tick on their own if their bTickingComponent is true.
}

void PLUGIN_FUNCTION_DECORATION ShutdownPlugin ()
{
	if (ComponentInstance != nullptr)
	{
		Engine* localEngine = ComponentInstance->GetOwningEngine();
		CHECK(localEngine != nullptr)

		//Engine::RemoveEngineComponent deletes the component
		localEngine->RemoveEngineComponent(ComponentInstance->StaticClass());
		ComponentInstance = nullptr;
	}
}
SD_END