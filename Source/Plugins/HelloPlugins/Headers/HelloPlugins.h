/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HelloPlugins.h

  A simple plugin that presents a minimal case in adding a plugin to Sand Dune.
  This plugin simply logs when it's created and removed.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Plugin\Headers\PluginClasses.h"

#ifdef PLATFORM_WINDOWS
	#ifdef HELLO_PLUGINS_EXPORT
		#define HELLO_PLUGINS_API __declspec(dllexport)
	#else
		#define HELLO_PLUGINS_API __declspec(dllimport)
	#endif
#else
	#define HELLO_PLUGINS_API
#endif

SD_BEGIN
extern HELLO_PLUGINS_API LogCategory HelloPluginsLog;
extern class HelloPluginsEngineComponent* ComponentInstance;

extern "C"
{
	/**
	 * Plugin interface functions. See OS_DynamicallyLoadLibrary for more information.
	 */
	int HELLO_PLUGINS_API PLUGIN_FUNCTION_DECORATION StartPlugin (size_t engineIdx);
	bool HELLO_PLUGINS_API PLUGIN_FUNCTION_DECORATION DelayedStartPlugin (size_t engineIdx, float deltaSec);
	void HELLO_PLUGINS_API PLUGIN_FUNCTION_DECORATION UpdatePlugin (float deltaSec);
	void HELLO_PLUGINS_API PLUGIN_FUNCTION_DECORATION ShutdownPlugin ();
}
SD_END