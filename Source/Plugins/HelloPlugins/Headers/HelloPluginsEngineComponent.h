/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HelloPluginsEngineComponent.h
  A simple component that's used to show how a plugin could register their
  own engine components.
=====================================================================
*/

#pragma once

#include "HelloPlugins.h"

SD_BEGIN
class HELLO_PLUGINS_API HelloPluginsEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(HelloPluginsEngineComponent)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;
};
SD_END