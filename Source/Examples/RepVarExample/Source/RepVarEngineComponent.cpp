/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarEngineComponent.cpp
=====================================================================
*/

#include "RepVarEngineComponent.h"
#include "RepVarEntity.h"
#include "RepVarInterface.h"

IMPLEMENT_ENGINE_COMPONENT(SD::RVE::RepVarEngineComponent)
RVE_BEGIN

RepVarEngineComponent::RepVarEngineComponent () : Super(),
	MainInterface(nullptr),
	bHosting(false),
	ListeningPortNum(0),
	LocalEntity(nullptr),
	HostCmdIdx(UINT_INDEX_NONE),
	JoinCmdIdx(UINT_INDEX_NONE),
	DisconnectCmdIdx(UINT_INDEX_NONE),
	SpawnCmdIdx(UINT_INDEX_NONE),
	RepObjCmdIdx(UINT_INDEX_NONE),
	ListVarCmdIdx(UINT_INDEX_NONE),
	SetVarCmdIdx(UINT_INDEX_NONE),
	DestroyCmdIdx(UINT_INDEX_NONE),
	RepVarSpacing(0.1f),
	RepVarRadius(64.f)
{
	//Noop
}

void RepVarEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	MainInterface = RepVarInterface::CreateObject();
	MainInterface->SetPosition(Vector2::ZERO_VECTOR);
	MainInterface->SetSize(Vector2(1.f, 1.f));

	HostCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("HostSession"),
		TXT("HostSession <OPTIONAL: Port Number> - Opens up a port to listen for incoming connections. If a port is specified, it'll attempt to open that port. Ports 0-1023 are reserved for system ports. Defaults to port 2011."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleLaunchHost, void, const DString&), true));

	JoinCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("JoinSession"),
		TXT("JoinSession <IpAddress> <OPTIONAL: Port Number> - Attempts to connect to the process of matching IP address. The other process must be listening for a connection. If a port number is specified, it'll attempt to connect through that port."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleJoinClient, void, const DString&), true));

	DisconnectCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("Disconnect"),
		TXT("Disconnect - Terminates the connection all processes."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleDisconnect, void, const DString&), false));

	SpawnCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("SpawnObject"),
		TXT("SpawnObject - Creates a new instance of RepVarEntity. This Entity will automatically replicate to every connected client."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleSpawnEntity, void, const DString&), true));

	RepObjCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("ReplicateObject"),
		TXT("ReplicateObject - Sets the Entity's Net Role to Authority. This will cause this object to notify remote clients of its existance."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleReplicateEntity, void, const DString&), false));

	ListVarCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("ListVariables"),
		TXT("ListVariables - Lists every variable that can be modified."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleListVariables, void, const DString&), false));

	SetVarCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("SetVariable"),
		TXT("SetVariable <InstanceId> <VariableName> <VariableValue> - Assigns a particular replicated variable to a value. Its value may or may not be replicated to others based on its properties. The instance ID is number that uniquely identifies an instance."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleSetVariable, void, const DString&), false));

	DestroyCmdIdx = MainInterface->Commands.size();
	MainInterface->Commands.emplace_back(RepVarInterface::SCommandMap(TXT("DestroyObject"),
		TXT("DestroyObject - Destroys the object that was created earlier. Every client that is aware of this object will also destroy their local copies."),
		SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleDestroyEntity, void, const DString&), false));

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	localNetEngine->GetConnections()->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleConnectionAccepted, void, NetworkSignature*));
	localNetEngine->GetConnections()->OnConnectionRejected.RegisterHandler(SDFUNCTION_2PARAM(this, RepVarEngineComponent, HandleConnectionRejected, void, const sf::IpAddress&, Int));
}

void RepVarEngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{
	Super::ProcessLog(category, logLevel, formattedMsg);

	if (category == &NetworkLog || category == &RepVarExampleLog)
	{
		if (((logLevel & (LogCategory::LL_Log | LogCategory::LL_Warning | LogCategory::LL_Critical)) > 0) && MainInterface != nullptr)
		{
			MainInterface->AddLog(formattedMsg);
		}
	}
}

void RepVarEngineComponent::ShutdownComponent ()
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	if (localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	{
		localNetEngine->GetConnections()->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleConnectionAccepted, void, NetworkSignature*));
		localNetEngine->GetConnections()->OnConnectionRejected.UnregisterHandler(SDFUNCTION_2PARAM(this, RepVarEngineComponent, HandleConnectionRejected, void, const sf::IpAddress&, Int));
	}

	if (MainInterface != nullptr)
	{
		MainInterface->Destroy();
		MainInterface = nullptr;
	}

	Super::ShutdownComponent();
}

void RepVarEngineComponent::AddRemoteEntity (RepVarEntity* newEntity)
{
	if (ContainerUtils::FindInVector(RemoteEntities, newEntity) == UINT_INDEX_NONE)
	{
		RemoteEntities.push_back(newEntity);

		Float multiplier = Int(RemoteEntities.size() - 1).ToFloat();
		newEntity->SetPosition(Vector2((RepVarSpacing * multiplier) + 0.33f, 0.05f));

		if (MainInterface != nullptr)
		{
			MainInterface->Commands.at(ListVarCmdIdx).bActive = true;
			MainInterface->Commands.at(SetVarCmdIdx).bActive = true;
		}
	}
}

void RepVarEngineComponent::RemoveRemoteEntity (RepVarEntity* targetEntity)
{
	size_t removedIdx = ContainerUtils::RemoveItem(OUT RemoteEntities, targetEntity);

	//Reposition each of the subsequent Entities to close any gaps
	for (size_t i = removedIdx; i < RemoteEntities.size(); ++i)
	{
		Float multiplier = Int(i).ToFloat();
		RemoteEntities.at(i)->SetPosition(Vector2((RepVarSpacing * multiplier) + 0.33f, 0.05f));
	}

	if (MainInterface != nullptr)
	{
		MainInterface->Commands.at(ListVarCmdIdx).bActive = (LocalEntity != nullptr || !ContainerUtils::IsEmpty(RemoteEntities));
		MainInterface->Commands.at(SetVarCmdIdx).bActive = MainInterface->Commands.at(ListVarCmdIdx).bActive;
	}
}

void RepVarEngineComponent::HandleLaunchHost (const DString& params)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)

	unsigned short port = ConnectionManager::DP_Game;
	if (!params.IsEmpty())
	{
		Int portNum;
		portNum.ParseString(params);
		port = static_cast<unsigned short>(portNum.Value);
		if (port < 1024)
		{
			port = ConnectionManager::DP_Game;
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid port. It must be a number between 1024-65535. Falling back to default %s."), params, Int(port));
		}
	}

	if (localNetEngine->GetConnections()->OpenPort(port))
	{
		ListeningPortNum = port;

		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("This process is now listening on port %s"), Int(port));
		if (MainInterface != nullptr)
		{
			MainInterface->SetStatusText(DString::CreateFormattedString(TXT("Listening on port %s . . ."), ListeningPortNum));
			bHosting = true;
			MainInterface->Commands.at(HostCmdIdx).bActive = false;
			MainInterface->Commands.at(JoinCmdIdx).bActive = false;
			MainInterface->Commands.at(DisconnectCmdIdx).bActive = true;
		}
	}
	else
	{
		RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("This process failed to open the port %s."), Int(port));
	}
}

void RepVarEngineComponent::HandleJoinClient (const DString& params)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)

	DString addressStr = params;
	DString portStr = DString::EmptyString;
	Int spaceIdx = params.Find(TXT(" "), 0, DString::CC_CaseSensitive);
	if (spaceIdx != INT_INDEX_NONE)
	{
		//A second parameter is specified
		params.SplitString(spaceIdx, OUT addressStr, OUT portStr);
	}

	unsigned short port = ConnectionManager::DP_Game;
	if (!portStr.IsEmpty())
	{
		Int portNum;
		portNum.ParseString(portStr);
		port = static_cast<unsigned short>(portNum.Value);
		if (port < 1024)
		{
			port = ConnectionManager::DP_Game;
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("%s is not a valid port. A port must range from 1024-65535. Falling back to default %s."), portStr, Int(port));
		}
	}

	if (MainInterface != nullptr)
	{
		MainInterface->SetStatusText(TXT("Connecting. . ."));
	}

	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Attempting to connect this process to %s on port %s."), addressStr, Int(port));
	localNetEngine->GetConnections()->OpenConnection(sf::IpAddress(addressStr.ToSfmlString()), port); 
}

void RepVarEngineComponent::HandleDisconnect (const DString& params)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	localNetEngine->GetConnections()->DisconnectFromAll(false);

	if (bHosting)
	{
		localNetEngine->GetConnections()->CloseAllPorts();
		ListeningPortNum = 0;
		bHosting = false;
	}

	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Disconnecting from everything."));
	if (MainInterface != nullptr)
	{
		MainInterface->SetStatusText(TXT("Disconnected"));
		MainInterface->Commands.at(HostCmdIdx).bActive = true;
		MainInterface->Commands.at(JoinCmdIdx).bActive = true;
		MainInterface->Commands.at(DisconnectCmdIdx).bActive = false;
	}
}

void RepVarEngineComponent::HandleConnectionAccepted (NetworkSignature* remoteSignature)
{
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("The connection to %s is ACCEPTED!"), DString(remoteSignature->ReadAddress().toString()));
	if (MainInterface != nullptr)
	{
		remoteSignature->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleSignatureDisconnected, void, NetworkSignature*));
		if (bHosting)
		{
			MainInterface->SetStatusText(DString::CreateFormattedString(TXT("CONNECTED - Hosting on port %s"), ListeningPortNum));
		}
		else
		{
			MainInterface->SetStatusText(TXT("CONNECTED"));
			MainInterface->Commands.at(HostCmdIdx).bActive = false;
			MainInterface->Commands.at(JoinCmdIdx).bActive = false;
			MainInterface->Commands.at(DisconnectCmdIdx).bActive = true;
		}
	}
}

void RepVarEngineComponent::HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason)
{
	RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Failed to establish a connection to %s. Reason Number: %s."), DString(remoteAddr.toString()), reason);
	if (MainInterface != nullptr)
	{
		MainInterface->SetStatusText(TXT("NOT CONNECTED"));
	}
}

void RepVarEngineComponent::HandleSignatureDisconnected (NetworkSignature* signature)
{
	signature->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, RepVarEngineComponent, HandleSignatureDisconnected, void, NetworkSignature*));
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("%s disconnected from this process."), DString(signature->GetAddress().toString()));

	//Check if that was the last connection
	bool bHasConnection = false;
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		bHasConnection = true;
		break;
	}

	if (!bHasConnection && MainInterface != nullptr)
	{
		if (bHosting)
		{
			MainInterface->SetStatusText(TXT("Listening. . ."));
		}
		else
		{
			MainInterface->Commands.at(HostCmdIdx).bActive = true;
			MainInterface->Commands.at(JoinCmdIdx).bActive = true;
			MainInterface->Commands.at(DisconnectCmdIdx).bActive = false;
			MainInterface->SetStatusText(TXT("Disconnected"));
		}
	}
}

void RepVarEngineComponent::HandleSpawnEntity (const DString& params)
{
	CHECK(LocalEntity == nullptr)

	LocalEntity = RepVarEntity::CreateObject();
	LocalEntity->SetPosition(Vector2(0.9f, 0.05f));
	LocalEntity->SetSize(RepVarRadius, RepVarRadius);
	TextRenderComponent* specialText = TextRenderComponent::CreateObject();
	if (LocalEntity->AddComponent(specialText))
	{
		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		specialText->SetFontColor(Color::WHITE.Source);
		DString msg = TXT("Y O U R S");
		Int fontSize = 18;
		TextRenderComponent::STextData& textData = specialText->CreateTextInstance(msg, localFontPool->GetDefaultFont(), fontSize.ToUnsignedInt32());
		Float lineWidth = localFontPool->GetDefaultFont()->CalculateStringWidth(msg, fontSize);
		textData.DrawOffset.x = lineWidth.Value * -0.5f; //center horizontally
		textData.DrawOffset.y = fontSize.ToFloat().Value * -1.5f; //Displace it to be above the Net ID text
	}

	if (MainInterface != nullptr)
	{
		MainInterface->Commands.at(SpawnCmdIdx).bActive = false;
		MainInterface->Commands.at(RepObjCmdIdx).bActive = true;
		MainInterface->Commands.at(ListVarCmdIdx).bActive = true;
		MainInterface->Commands.at(SetVarCmdIdx).bActive = true;
		MainInterface->Commands.at(DestroyCmdIdx).bActive = true;
	}

	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Spawned local Entity. This Entity will automatically replicate to every connected client."));
}

void RepVarEngineComponent::HandleReplicateEntity (const DString& params)
{
	CHECK(LocalEntity != nullptr)

	if (NetworkRoleComponent* roleComp = LocalEntity->GetRoleComp())
	{
		roleComp->SetNetRole(NetworkRoleComponent::NR_Authority);
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("The Entity's NetRole is now NR_Authority. Remote instances may now be aware of this object's existance. Only the first client is the network owner."));
	}

	if (MainInterface != nullptr)
	{
		MainInterface->Commands.at(RepObjCmdIdx).bActive = false;
	}
}

void RepVarEngineComponent::HandleDestroyEntity (const DString& params)
{
	CHECK(LocalEntity != nullptr)
	LocalEntity->Destroy();
	LocalEntity = nullptr;

	if (MainInterface != nullptr)
	{
		MainInterface->Commands.at(SpawnCmdIdx).bActive = true;
		MainInterface->Commands.at(RepObjCmdIdx).bActive = false;
		MainInterface->Commands.at(ListVarCmdIdx).bActive = !ContainerUtils::IsEmpty(RemoteEntities);
		MainInterface->Commands.at(SetVarCmdIdx).bActive = !ContainerUtils::IsEmpty(RemoteEntities);
		MainInterface->Commands.at(DestroyCmdIdx).bActive = false;
	}

	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Destroying local Entity. Every client that is aware of this object will also destroy their copies."));
}

void RepVarEngineComponent::HandleListVariables (const DString& params)
{
	RepVarEntity* targetEntity = LocalEntity;
	if (targetEntity == nullptr && !ContainerUtils::IsEmpty(RemoteEntities))
	{
		targetEntity = RemoteEntities.at(0);
	}

	if (targetEntity != nullptr)
	{
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("====================="));
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("List of variables are:"));
		for (size_t i = 0; i < targetEntity->VarNameMappings.size(); ++i)
		{
			RepVarExampleLog.Log(LogCategory::LL_Log, TXT("    %s"), targetEntity->VarNameMappings.at(i).VarName);
		}
	}
}

void RepVarEngineComponent::HandleSetVariable (const DString& params)
{
	/*
	Expected params:
	<InstanceId> - int. Must match the RoleComponent's NetId to specify which instance.
	<VarName> - DString. Must match a specific name to specify which variable is being edited.
	<VarValue> - Assigns the value of the variable to this value.
	*/
	const DString exampleUsage = TXT("Example usage: SetVariable 1 SyncOnChange 40");
	DString parsedParams = params;
	Int spaceIdx = parsedParams.Find(TXT(" "), 1, DString::CC_CaseSensitive);
	if (spaceIdx == INT_INDEX_NONE)
	{
		RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Invalid parameters \"%s\". The instance ID must be specified followed by the space character. %s"), params, exampleUsage);
		return;
	}

	RepVarEntity* targetEntity = nullptr;
	//First Param
	{
		DString firstParam = parsedParams.SubString(0, spaceIdx - 1);
		DString::SubString(OUT parsedParams, spaceIdx + 1);
		parsedParams.TrimSpaces();

		Int netId;
		netId.ParseString(firstParam);

		if (LocalEntity != nullptr && LocalEntity->GetRoleComp() != nullptr && LocalEntity->GetRoleComp()->GetNetId() == netId)
		{
			targetEntity = LocalEntity;
		}
		else
		{
			for (size_t i = 0; i < RemoteEntities.size(); ++i)
			{
				if (RemoteEntities.at(i) != nullptr && RemoteEntities.at(i)->GetRoleComp() != nullptr && RemoteEntities.at(i)->GetRoleComp()->GetNetId() == netId)
				{
					targetEntity = RemoteEntities.at(i);
					break;
				}
			}
		}

		if (targetEntity == nullptr)
		{
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Unable to find Entity with ID %s. NOTE: Entities without a number displayed has an ID of 0. %s"), netId, exampleUsage);
			return;
		}
	}
	
	DProperty* repVar = nullptr;
	DString secondParam;
	//Second param
	{
		spaceIdx = parsedParams.Find(TXT(" "), 1, DString::CC_IgnoreCase);
		if (spaceIdx == INT_INDEX_NONE)
		{
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Invalid parameters \"%s\". The parameters must specify the following in this order: InstanceId, VariableName, VariableValue. %s"), params, exampleUsage);
			return;
		}

		secondParam = parsedParams.SubString(0, spaceIdx - 1);
		DString::SubString(OUT parsedParams, spaceIdx + 1);

		for (size_t i = 0; i < targetEntity->VarNameMappings.size(); ++i)
		{
			if (targetEntity->VarNameMappings.at(i).VarName.Compare(secondParam, DString::CC_IgnoreCase) == 0)
			{
				repVar = targetEntity->VarNameMappings.at(i).Var;
				break;
			}
		}

		if (repVar == nullptr)
		{
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Invalid parameters \"%s\". The \"%s\" is not recognized as a variable name. The available variables are:"), params, secondParam);
			for (size_t i = 0; i < targetEntity->VarNameMappings.size(); ++i)
			{
				RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("    %s"), targetEntity->VarNameMappings.at(i).VarName);
			}

			return;
		}
	}

	//Third param
	{
		//Special case for color handling
		if (targetEntity->GetRenderComp() != nullptr && repVar == &targetEntity->GetRenderComp()->SolidColor)
		{
			if (Color* repColor = dynamic_cast<Color*>(repVar))
			{
				static const std::vector<std::pair<DString, Color>> colorMap({
					{TXT("Red"), Color::RED},
					{TXT("Orange"), Color(255, 128, 0)},
					{TXT("Yellow"), Color::YELLOW},
					{TXT("Green"), Color::GREEN},
					{TXT("Blue"), Color::BLUE},
					{TXT("Indigo"), Color(75, 0, 130)},
					{TXT("Violet"), Color(128, 0, 255)},
					{TXT("Magenta"), Color::MAGENTA},
					{TXT("Cyan"), Color::CYAN},
					{TXT("Black"), Color::BLACK},
					{TXT("Gray"), Color(128, 128, 128)},
					{TXT("White"), Color::WHITE}});

				parsedParams.TrimSpaces(); //Ensure there aren't any trailing spaces that might mess up the string comparisons.
				bool bFoundColor = false;
				for (size_t i = 0; i < colorMap.size(); ++i)
				{
					if (colorMap.at(i).first.Compare(parsedParams, DString::CC_IgnoreCase) == 0)
					{
						*repColor = colorMap.at(i).second;
						bFoundColor = true;
						break;
					}
				}

				if (!bFoundColor)
				{
					RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("The color %s is not recognized. The following colors are available:"), parsedParams);
					for (size_t i = 0; i < colorMap.size(); ++i)
					{
						RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("    %s"), colorMap.at(i).first);
					}
				}

				return;
			}
		}
		else if (repVar == &targetEntity->DesiredBaseSize)
		{
			Float newDesiredSize;
			newDesiredSize.ParseString(parsedParams);

			if (newDesiredSize <= 0.f)
			{
				//Special case. Ensure the value is positive to ensure it'll animate. Nonpositive values indicate that the Entity should not animate.
				parsedParams = TXT("0.01f");
			}
		}

		//fallback to generic string parsing
		repVar->ParseString(parsedParams);
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("%s is now: %s"), secondParam, repVar->ToString());
	}
}
RVE_END