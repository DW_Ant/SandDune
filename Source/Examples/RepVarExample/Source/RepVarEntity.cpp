/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarEntity.cpp
=====================================================================
*/

#include "RepVarEngineComponent.h"
#include "RepVarEntity.h"
#include "VariableMonitor.h"

IMPLEMENT_CLASS(SD::RVE::RepVarEntity, SD::Entity)
RVE_BEGIN

RepVarEntity::SVarNameMapping::SVarNameMapping (const DString& inVarName, DProperty* inVar) :
	VarName(inVarName),
	Var(inVar)
{
	//Noop
}

void RepVarEntity::InitProps ()
{
	Super::InitProps();

	SyncOnInitOnly = 0;
	SyncOnChange = 0;
	NetOwnerOnly = 0;
	PositiveOnly = 1.f;
	RepEvenOnly = 0;
	RepNotify = TXT("Not Assigned");
	DesiredBaseSize = -1.f;
	ReplicatedBaseSize = 0.f;

	Monitor = nullptr;
	RenderCompMonitor = nullptr;
}

void RepVarEntity::BeginObject ()
{
	Super::BeginObject();

	RenderComp = ColorRenderComponent::CreateObject();
	if (AddComponent(RenderComp))
	{
		RenderComp->SetShape(ColorRenderComponent::ST_Circle);
		RenderComp->SolidColor = Color(96, 96, 96, 200);
		RenderComp->SetBaseSize(1.f, 1.f);
		RenderComp->SetPivot(0.5f, 0.5f);
		ReplicatedBaseSize = RenderComp->ReadBaseSize().X;

		RenderNetComp = NetworkComponent::CreateObject();
		if (RenderComp->AddComponent(RenderNetComp))
		{
			//Using default replication properties
			RenderNetComp->AddReplicatedVariable(&RenderComp->SolidColor);
		}
	}

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		//By default, RoleComponents are authoritative. To prevent role components from replicating data immediately, set it to local.
		RoleComp->SetNetRole(NetworkRoleComponent::NR_Local);

		RoleComp->OnGetComponentList = SDFUNCTION_1PARAM(this, RepVarEntity, HandleGetNetList, void, std::vector<NetworkComponent*>&);
		RoleComp->OnNetInitialize = SDFUNCTION_1PARAM(this, RepVarEntity, HandleNetInitialize, void, NetworkSignature*);
		RoleComp->OnIsNetOwner = SDFUNCTION_1PARAM(this, RepVarEntity, HandleIsNetOwner, bool, NetworkSignature*);
		//By leaving OnIsNetRelevant unbound, the NetworkRoleComponent will assume that this is relevant to everyone connected to this process.
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		NetComp->AddReplicatedVariable<Int>(&SyncOnInitOnly, [](ReplicatedVariable<Int>& outRep)
		{
			//This instructs the component that this variable will only replicate once. The client is expected to simulate this variable on its own after init.
			outRep.SetSyncOnChange(false);
			outRep.SetSyncOnInit(true);
		});

		//NOTE: This is the same as calling:
		//NetComp->AddReplicatedVariable(&SyncOnChange);
		NetComp->AddReplicatedVariable<Int>(&SyncOnChange, [](ReplicatedVariable<Int>& outRep)
		{
			//By default, variables will sync on change and on init
			//outRep.SetSyncOnChange(true);
		});

		NetComp->AddReplicatedVariable<Int>(&NetOwnerOnly, [](ReplicatedVariable<Int>& outRep)
		{
			outRep.SetBroadcastToAll(false);
		});

		NetComp->AddReplicatedVariable<Float>(&PositiveOnly, [&](ReplicatedVariable<Float>& outRep)
		{
			//Note: Using the & in the capture list to allow for 'this' in the delegate.
			outRep.OnValidateData = SDFUNCTION_1PARAM(this, RepVarEntity, HandleValidatePositiveOnly, bool, const Float&);
		});

		NetComp->AddReplicatedVariable<Int>(&RepEvenOnly, [&](ReplicatedVariable<Int>& outRep)
		{
			outRep.OnShouldReplicate = [&](NetworkSignature* destination)
			{
				//This check runs on the sender side. This is not checked on the recipient side.
				return (RepEvenOnly.IsEven());
			};

			outRep.OnValidateData = [&](const Int& pendingData)
			{
				//This check runs on the recipient side. If false, it'll send a rejection packet.
				return (pendingData.IsEven());
			};
		});

		NetComp->AddReplicatedVariable<DString>(&RepNotify, [&](ReplicatedVariable<DString>& outRep)
		{
			//Note: Using the & in the capture list to allow for 'this' in the delegate.
			outRep.OnReplicated = SDFUNCTION(this, RepVarEntity, HandleRepNotifyReplicated, void);
		});

		//DesiredBaseSize will replicate immediately after change. The RenderComponent's BaseSize will replicate periodically as this Entity transitions to its desired size.
		//NOTE: When uncommenting this, this will cause the client to also simulate growth/shrink, causing a smooth transition for both processes.
		//NetComp->AddReplicatedVariable(&DesiredBaseSize);

		NetComp->AddReplicatedVariable<Float>(&ReplicatedBaseSize, [](ReplicatedVariable<Float>& outRep)
		{
			//Intentionally set to a high interval to demonstrate SyncInterval when updating replicated variables frequently.
			outRep.SetSyncInterval(1.f);
		});

		SetSize(Vector2::ZERO_VECTOR); //Set to an invisible value. The process spawning this Entity will initialize this variable. Nonauthoritative Entities will initialize this variable via replication.
		NetComp->AddReplicatedVariable<Vector2>(&Size, [&](ReplicatedVariable<Vector2>& outRep)
		{
			outRep.SetSyncOnChange(false);
			outRep.SetSyncOnInit(true); //Only interested in replicating initial data. Everything else is simulated locally.
			outRep.OnReplicated = SDFUNCTION(this, RepVarEntity, HandleSizeReplicated, void);
		});
	}

	TickComponent* sizeTick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(sizeTick))
	{
		sizeTick->SetTickHandler(SDFUNCTION_1PARAM(this, RepVarEntity, HandleReplicatedSizeTick, void, Float));
	}

	TickComponent* growthTick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(growthTick))
	{
		growthTick->SetTickHandler(SDFUNCTION_1PARAM(this, RepVarEntity, HandleGrowthTick, void, Float));
	}

	//Automatically register to the Canvas layer
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr && localGraphicsEngine->GetCanvasDrawLayer() != nullptr)
	localGraphicsEngine->GetCanvasDrawLayer()->RegisterPlanarObject(this);

	InitRepVarComponents();
}

void RepVarEntity::Destroyed ()
{
	if (RoleComp.IsValid())
	{
		if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_Impotent || RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
		{
			RepVarEngineComponent* localRepEngine = RepVarEngineComponent::Find();
			if (localRepEngine != nullptr)
			{
				localRepEngine->RemoveRemoteEntity(this);
			}
		}
	}

	Super::Destroyed();
}

void RepVarEntity::InitRepVarComponents ()
{
	//Initialize commands to set the variables.
	if (RenderComp.IsValid())
	{
		VarNameMappings.emplace_back(SVarNameMapping(TXT("Color"), &RenderComp->SolidColor));
	}

	VarNameMappings.emplace_back(SVarNameMapping(TXT("NetOwnerOnly"), &NetOwnerOnly));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("PositiveOnly"), &PositiveOnly));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("RepEvenOnly"), &RepEvenOnly));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("RepNotify"), &RepNotify));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("Size"), &DesiredBaseSize));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("SyncOnChange"), &SyncOnChange));
	VarNameMappings.emplace_back(SVarNameMapping(TXT("SyncOnInitOnly"), &SyncOnInitOnly));

	//Initialize monitors to display changes
	Monitor = VariableMonitor::CreateObject();
	if (AddComponent(Monitor))
	{
		Monitor->AddWatchedVariable(TXT("SyncOnInitOnly"), &SyncOnInitOnly);
		Monitor->AddWatchedVariable(TXT("SyncOnChange"), &SyncOnChange);
		Monitor->AddWatchedVariable(TXT("NetOwnerOnly"), &NetOwnerOnly);
		Monitor->AddWatchedVariable(TXT("PositiveOnly"), &PositiveOnly);
		Monitor->AddWatchedVariable(TXT("RepEvenOnly"), &RepEvenOnly);
		Monitor->AddWatchedVariable(TXT("RepNotify"), &RepNotify);
	}

	if (RenderComp.IsValid())
	{
		RenderCompMonitor = VariableMonitor::CreateObject();
		if (RenderComp->AddComponent(RenderCompMonitor))
		{
			RenderCompMonitor->AddWatchedVariable(TXT("RenderComponent->Color"), &RenderComp->SolidColor);
		}
	}
}

void RepVarEntity::HandleGetNetList (std::vector<NetworkComponent*>& outNetComps) const
{
	outNetComps.push_back(NetComp.Get());
	outNetComps.push_back(RenderNetComp.Get());
}

void RepVarEntity::HandleNetInitialize (NetworkSignature* connectedTo)
{
	CHECK(RoleComp.IsValid())
	if (RoleComp->IsClient())
	{
		//Notify the local RepVar engine about this new remote entity
		RepVarEngineComponent* localRepEngine = RepVarEngineComponent::Find();
		CHECK(localRepEngine != nullptr)
		localRepEngine->AddRemoteEntity(this);
	}

	//Add an text component that displays this component's ID.
	TextRenderComponent* idText = TextRenderComponent::CreateObject();
	if (AddComponent(idText))
	{
		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)
			
		idText->SetFontColor(sf::Color::White);
		DString msg = RoleComp->GetNetId().ToString();
		Int fontSize = 24;
		TextRenderComponent::STextData& textData = idText->CreateTextInstance(msg, localFontPool->GetDefaultFont(), fontSize.ToUnsignedInt32());
		textData.DrawOffset.x = (localFontPool->GetDefaultFont()->CalculateStringWidth(msg, fontSize)).Value * -0.5f;
		textData.DrawOffset.y = fontSize.Value * -0.5f;
	}

	if (Monitor != nullptr)
	{
		Monitor->LogPrefix = DString::CreateFormattedString(TXT("For Entity %s:  "), RoleComp->GetNetId());
	}

	if (RenderCompMonitor != nullptr)
	{
		RenderCompMonitor->LogPrefix = DString::CreateFormattedString(TXT("For Entity %s:  "), RoleComp->GetNetId());
	}
}

bool RepVarEntity::HandleIsNetOwner (NetworkSignature* remoteClient) const
{
	//For this test, the first connection is the remote owner.
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		return (remoteClient == iter.GetSelectedSignature());
	}

	return false;
}

void RepVarEntity::HandleReplicatedSizeTick (Float deltaSec)
{
	if (RenderComp.IsValid())
	{
		RenderComp->SetBaseSize(ReplicatedBaseSize, 1.f);
	}
}

void RepVarEntity::HandleGrowthTick (Float deltaSec)
{
	if (DesiredBaseSize > 0.f && Float::Abs(ReplicatedBaseSize - DesiredBaseSize) > 0.001f)
	{
		Float multiplier = (ReplicatedBaseSize > DesiredBaseSize) ? -1.f : 1.f;
		Float growthPerSec = 0.25f;
		ReplicatedBaseSize += (growthPerSec * deltaSec * multiplier);

		if (Float::Abs(ReplicatedBaseSize - DesiredBaseSize) <= 0.05f)
		{
			DesiredBaseSize = -1.f; //Disable animation
		}
	}
}

bool RepVarEntity::HandleValidatePositiveOnly (const Float& val)
{
	//If this function returns false, it will NOT accept the incoming replicated variable value.
	return (val > 0.f);
}

void RepVarEntity::HandleRepNotifyReplicated ()
{
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("The RepNotify variable invoked its RepNotify callback. It is now: \"%s\""), RepNotify);
}

void RepVarEntity::HandleSizeReplicated ()
{
	MarkTransformDirty();
}
RVE_END