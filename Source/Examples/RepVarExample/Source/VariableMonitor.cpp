/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariableMonitor.cpp
=====================================================================
*/

#include "VariableMonitor.h"

IMPLEMENT_CLASS(SD::RVE::VariableMonitor, SD::EntityComponent)
RVE_BEGIN

VariableMonitor::SVarData::SVarData (const DString& inVarName, DProperty* inVar) :
	VarName(inVarName),
	Var(inVar),
	CurValue(DString::EmptyString)
{
	if (Var != nullptr)
	{
		CurValue = Var->ToString();
	}
}

void VariableMonitor::InitProps ()
{
	Super::InitProps();

	LogPrefix = DString::EmptyString;
	WatchTick = nullptr;
}

void VariableMonitor::BeginObject ()
{
	Super::BeginObject();

	WatchTick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(WatchTick))
	{
		WatchTick->SetTickInterval(0.5f);
		WatchTick->SetTickHandler(SDFUNCTION_1PARAM(this, VariableMonitor, HandleWatchTick, void, Float));
	}
}

void VariableMonitor::ComponentDetached ()
{
	ContainerUtils::Empty(OUT WatchedVariables);

	Super::ComponentDetached();
}

void VariableMonitor::AddWatchedVariable (const DString& varName, DProperty* var)
{
	if (var == nullptr)
	{
		RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Unable to add watch variable %s since it's pointing to a null address."), varName);
		return;
	}

	//Ensure this variable is not already registered
	for (size_t i = 0; i < WatchedVariables.size(); ++i)
	{
		if (WatchedVariables.at(i).Var == var)
		{
			RepVarExampleLog.Log(LogCategory::LL_Warning, TXT("Unable to add watched variable %s since that variable is already registered to the VariableMonitor at index %s."), varName, Int(i));
			return;
		}
	}

	WatchedVariables.emplace_back(SVarData(varName, var));
}

void VariableMonitor::SetWatchFrequency (Float interval)
{
	if (WatchTick != nullptr)
	{
		WatchTick->SetTickInterval(interval);
	}
}

void VariableMonitor::EvaluateWatchVariables ()
{
	for (size_t i = 0; i < WatchedVariables.size(); ++i)
	{
		CHECK(WatchedVariables.at(i).Var != nullptr)

		DString newValue = WatchedVariables.at(i).Var->ToString();
		if (newValue.Compare(WatchedVariables.at(i).CurValue, DString::CC_CaseSensitive) != 0)
		{
			RepVarExampleLog.Log(LogCategory::LL_Log, TXT("%sVariable %s changed to %s."), LogPrefix, WatchedVariables.at(i).VarName, newValue);
			WatchedVariables.at(i).CurValue = newValue;
		}
	}
}

void VariableMonitor::HandleWatchTick (Float deltaSec)
{
	EvaluateWatchVariables();
}
RVE_END