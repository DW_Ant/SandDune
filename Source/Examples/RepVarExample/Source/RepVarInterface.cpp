/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarInterface.cpp
=====================================================================
*/

#include "RepVarInterface.h"

IMPLEMENT_CLASS(SD::RVE::RepVarInterface, SD::Entity)
RVE_BEGIN

RepVarInterface::SCommandMap::SCommandMap (const DString& inCommandString, const DString& inHelpDesc, const SDFunction<void, const DString&>& inExec, bool inActive) :
	CommandString(inCommandString),
	HelpDesc(inHelpDesc),
	Exec(inExec),
	bActive(inActive)
{
	//Noop
}

void RepVarInterface::InitProps ()
{
	Super::InitProps();

	Commands.emplace_back(SCommandMap(TXT("Help"),
		TXT("Help - Lists available commands, their usage, and the controls."),
		SDFUNCTION_1PARAM(this, RepVarInterface, HandleHelp, void, const DString&),
		true));

	Commands.emplace_back(SCommandMap(TXT("List"),
		TXT("List - Logs out every connection to this process."),
		SDFUNCTION_1PARAM(this, RepVarInterface, HandleListConnections, void, const DString&),
		true));

	Commands.emplace_back(SCommandMap(TXT("IP"),
		TXT("IP - Logs out your local, LAN, and public IP addresses."),
		SDFUNCTION_1PARAM(this, RepVarInterface, HandleLogSelfIp, void, const DString&),
		true));

	Commands.emplace_back(SCommandMap(TXT("Exit"),
		TXT("Exit - Disconnects and terminates this program."),
		SDFUNCTION_1PARAM(this, RepVarInterface, HandleExit, void, const DString&),
		true));

	FontSize = 16;
	VertBottomScrollIdx = 0;
	Input = DString::EmptyString;

	StatusTextTransform = nullptr;
	StatusText = nullptr;
	StatusBarTransform = nullptr;
	StatusBar = nullptr;
	OutputTransform = nullptr;
	OutputText = nullptr;
	ScrollStatusTransform = nullptr;
	ScrollStatus = nullptr;
	InputBarTransform = nullptr;
	InputBar = nullptr;
	HelpTransform = nullptr;
	HelpText = nullptr;
	InputTextTransform = nullptr;
	InputText = nullptr;
	CursorTransform = nullptr;
	Cursor = nullptr;
	InputComp = nullptr;
	CursorTick = nullptr;
}

void RepVarInterface::BeginObject ()
{
	Super::BeginObject();

	Float lineHeight = 0.05f;

	StatusTextTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(StatusTextTransform))
	{
		StatusTextTransform->SetPosition(Vector2::ZERO_VECTOR);
		StatusTextTransform->SetSize(Vector2(0.25f, lineHeight));

		StatusText = TextRenderComponent::CreateObject();
		if (StatusTextTransform->AddComponent(StatusText))
		{
			StatusText->SetFontColor(sf::Color::White);
		}
	}

	StatusBarTransform  = PlanarTransformComponent::CreateObject();
	if (AddComponent(StatusBarTransform))
	{
		StatusBarTransform->SetPosition(Vector2(0.f, lineHeight * 1.5f));
		StatusBarTransform->SetSize(Vector2(1.f, lineHeight * 0.125f));

		StatusBar = ColorRenderComponent::CreateObject();
		if (StatusBarTransform->AddComponent(StatusBar))
		{
			StatusBar->SetPivot(0.f, 0.5f);
			StatusBar->SolidColor = Color(128, 128, 128);
		}
	}

	OutputTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(OutputTransform))
	{
		OutputTransform->SetPosition(Vector2(0.f, lineHeight * 2.f));
		OutputTransform->SetSize(Vector2(4096.f, 1.f - (lineHeight * 5.f)));

		OutputText = TextRenderComponent::CreateObject();
		if (OutputTransform->AddComponent(OutputText))
		{
			OutputText->SetFontColor(sf::Color::White);
		}
	}

	ScrollStatusTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(ScrollStatusTransform))
	{
		ScrollStatusTransform->SetPosition(Vector2(0.f, 1.f - (lineHeight * 3.f)));
		ScrollStatusTransform->SetSize(Vector2(1.f, lineHeight));

		ScrollStatus = TextRenderComponent::CreateObject();
		if (ScrollStatusTransform->AddComponent(ScrollStatus))
		{
			ScrollStatus->SetFontColor(sf::Color::White);
		}
	}

	InputBarTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(InputBarTransform))
	{
		InputBarTransform->SetPosition(Vector2(0.f, 1.f - (lineHeight * 2.f)));
		InputBarTransform->SetSize(Vector2(1.f, lineHeight * 0.125f));

		InputBar = ColorRenderComponent::CreateObject();
		if (InputBarTransform->AddComponent(InputBar))
		{
			InputBar->SetPivot(0.f, 0.5f);
			InputBar->SolidColor = Color(128, 128, 128);
		}
	}

	HelpTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(HelpTransform))
	{
		HelpTransform->SetPosition(Vector2(0.f, 1.f - (lineHeight * 2.f)));
		HelpTransform->SetSize(Vector2(1.f, lineHeight));

		HelpText = TextRenderComponent::CreateObject();
		if (HelpTransform->AddComponent(HelpText))
		{
			HelpText->SetFontColor(sf::Color::White);
		}
	}

	InputTextTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(InputTextTransform))
	{
		InputTextTransform->SetPosition(Vector2(0.f, 1.f - lineHeight));
		InputTextTransform->SetSize(Vector2(0.f, 1.f));

		InputText = TextRenderComponent::CreateObject();
		if (InputTextTransform->AddComponent(InputText))
		{
			InputText->SetFontColor(sf::Color::White);
		}
	}
	
	CursorTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(CursorTransform))
	{
		CursorTransform->SetPosition(Vector2(0.f, 1.f - lineHeight));
		CursorTransform->SetSize(Vector2(4.f, lineHeight * 0.75f));

		Cursor = ColorRenderComponent::CreateObject();
		if (CursorTransform->AddComponent(Cursor))
		{
			Cursor->SolidColor = Color::WHITE;
		}
	}
	
	InputComp = InputComponent::CreateObject();
	if (AddComponent(InputComp))
	{
		InputComp->OnInput = SDFUNCTION_1PARAM(this, RepVarInterface, HandleInput, bool, const sf::Event&);
		InputComp->OnText = SDFUNCTION_1PARAM(this, RepVarInterface, HandleText, bool, const sf::Event&);
	}
	
	CursorTick = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (AddComponent(CursorTick))
	{
		CursorTick->SetTickHandler(SDFUNCTION_1PARAM(this, RepVarInterface, HandleCursorTick, void, Float));
		CursorTick->SetTickInterval(0.25f);
	}

	//Register render components to the canvas layer. This simple application does not need a dedicated draw layer.
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr && localGraphicsEngine->GetCanvasDrawLayer() != nullptr)
	localGraphicsEngine->GetCanvasDrawLayer()->RegisterPlanarObject(this);

	SetStatusText(TXT("NOT CONNECTED"));
	RefreshScrollStatus();
	SetHelpText(TXT("Enter \'Help\' to list commands and controls."));
	SetInputText(TXT("> "));
}

void RepVarInterface::AddLog (const DString& log)
{
	bool bSnapBottom = VertBottomScrollIdx >= OutputMessages.size() - 1;
	OutputMessages.push_back(log);

	if (bSnapBottom)
	{
		VertBottomScrollIdx = OutputMessages.size() - 1;
	}

	RefreshScrollStatus();
	RefreshOutput();
}

void RepVarInterface::RefreshOutput ()
{
	if (OutputTransform == nullptr || OutputText == nullptr || !ContainerUtils::IsValidIndex(OutputMessages, VertBottomScrollIdx))
	{
		return;
	}

	FontPool* localFontPool = FontPool::FindFontPool();
	if (localFontPool == nullptr || localFontPool->GetDefaultFont() == nullptr)
	{
		return;
	}

	OutputText->DeleteAllText();

	size_t maxNumLines = (OutputTransform->ReadCachedAbsSize().Y / FontSize.ToFloat()).ToInt().ToUnsignedInt();
	if (maxNumLines == 0)
	{
		return;
	}

	size_t i = VertBottomScrollIdx;
	Float offset = (FontSize.ToFloat() * Float(maxNumLines - 1));
	while (true)
	{
		TextRenderComponent::STextData& textData = OutputText->CreateTextInstance(OutputMessages.at(i), localFontPool->GetDefaultFont(), FontSize.ToUnsignedInt32());
		textData.DrawOffset.y = offset.Value;
		offset -= FontSize.ToFloat();

		if (i == 0 || (VertBottomScrollIdx - i) >= maxNumLines)
		{
			break;
		}

		--i;
	}
}

void RepVarInterface::RefreshScrollStatus ()
{
	if (ScrollStatus != nullptr && !ContainerUtils::IsEmpty(OutputMessages))
	{
		DString newStatusText = DString::CreateFormattedString(TXT("%s / %s"), Int(VertBottomScrollIdx+1), Int(OutputMessages.size()));

		FontPool* localFontPool = FontPool::FindFontPool();
		if (localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)
		{
			ScrollStatus->DeleteAllText();
			ScrollStatus->CreateTextInstance(newStatusText, localFontPool->GetDefaultFont(), FontSize.ToUnsignedInt32());
		}
	}
}

void RepVarInterface::SetStatusText (const DString& newStatusText)
{
	if (StatusText != nullptr)
	{
		FontPool* localFontPool = FontPool::FindFontPool();
		if (localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)
		{
			StatusText->DeleteAllText();
			StatusText->CreateTextInstance(newStatusText, localFontPool->GetDefaultFont(), FontSize.ToUnsignedInt32());
		}
	}
}

void RepVarInterface::SetHelpText (const DString& newHelpText)
{
	if (HelpText != nullptr)
	{
		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		HelpText->DeleteAllText();
		HelpText->CreateTextInstance(newHelpText, localFontPool->GetDefaultFont(), FontSize.ToUnsignedInt32());
	}
}

void RepVarInterface::SetInputText (const DString& newInputText)
{
	if (InputText != nullptr)
	{
		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		InputText->DeleteAllText();
		InputText->CreateTextInstance(newInputText, localFontPool->GetDefaultFont(), FontSize.ToUnsignedInt32());

		if (CursorTransform != nullptr)
		{
			CursorTransform->EditPosition().X = localFontPool->GetDefaultFont()->CalculateStringWidth(newInputText, FontSize);
		}
	}
}

void RepVarInterface::HandleHelp (const DString& params)
{
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("List of available commands. . ."));
	for (const SCommandMap& cmd : Commands)
	{
		if (cmd.bActive)
		{
			RepVarExampleLog.Log(LogCategory::LL_Log, TXT("    %s"), cmd.HelpDesc);
		}
	}

	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("====================="));
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Home scrolls to the top. End scrolls to the bottom."));
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Left Arrow/Right Arrow scrolls left and right. Holding ctrl increases scroll speed."));
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("Page Up/Page Down scrolls up and down."));
}

void RepVarInterface::HandleListConnections (const DString& params)
{
	std::vector<NetworkSignature*> signatures;
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		signatures.push_back(iter.GetSelectedSignature());
	}

	if (ContainerUtils::IsEmpty(signatures))
	{
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("This process is not connected to anything."));
	}
	else
	{
		RepVarExampleLog.Log(LogCategory::LL_Log, TXT("This process is connected to the following..."));
		for (size_t i = 0; i < signatures.size(); ++i)
		{
			RepVarExampleLog.Log(LogCategory::LL_Log, TXT("    %s"), DString(signatures.at(i)->GetAddress().toString()));
		}
	}
}

void RepVarInterface::HandleLogSelfIp (const DString& params)
{
	RepVarExampleLog.Log(LogCategory::LL_Log, TXT("This computer's local host is %s.    LAN is %s.    Public IP is %s."), DString(sf::IpAddress::LocalHost.toString()), DString(NetworkEngineComponent::GetLanAddress().toString()), DString(NetworkEngineComponent::GetPublicAddress().toString()));
}

void RepVarInterface::HandleExit (const DString& params)
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->Shutdown();
	}
}

bool RepVarInterface::HandleInput (const sf::Event& input)
{
	switch (input.key.code)
	{
		case(sf::Keyboard::Key::Left):
			if (input.type != sf::Event::KeyReleased && OutputTransform != nullptr)
			{
				Float multiplier = InputBroadcaster::GetCtrlHeld() ? 4.f : 1.f;
				OutputTransform->EditPosition().X = Utils::Min<Float>(0.f, OutputTransform->ReadPosition().X + (10.f * multiplier));
				RefreshOutput();
			}

			return true;

		case(sf::Keyboard::Key::Right):
			if (input.type != sf::Event::KeyReleased && OutputTransform != nullptr)
			{
				Float multiplier = InputBroadcaster::GetCtrlHeld() ? 4.f : 1.f;
				OutputTransform->EditPosition().X = Utils::Max<Float>(-1500.f, OutputTransform->ReadPosition().X - (10.f * multiplier));
				RefreshOutput();
			}

			return true;

		case(sf::Keyboard::Key::PageUp):
			if (input.type != sf::Event::KeyReleased)
			{
				size_t multiplier = (InputBroadcaster::GetCtrlHeld()) ? 4 : 1;
				if (multiplier > VertBottomScrollIdx)
				{
					VertBottomScrollIdx = 0;
				}
				else
				{
					VertBottomScrollIdx -= multiplier;
				}
				
				RefreshScrollStatus();
				RefreshOutput();
			}

			return true;

		case(sf::Keyboard::Key::PageDown):
			if (input.type != sf::Event::KeyReleased)
			{
				size_t multiplier = (InputBroadcaster::GetCtrlHeld()) ? 4 : 1;
				VertBottomScrollIdx = Utils::Min(VertBottomScrollIdx + multiplier, OutputMessages.size() - 1);
				RefreshScrollStatus();
				RefreshOutput();
			}

			return true;

		case(sf::Keyboard::Key::Home):
			if (input.type == sf::Event::KeyReleased)
			{
				VertBottomScrollIdx = 0;
				RefreshScrollStatus();
				RefreshOutput();
			}
			return true;

		case(sf::Keyboard::Key::End):
			if (input.type == sf::Event::KeyReleased)
			{
				VertBottomScrollIdx = OutputMessages.size() - 1;
				RefreshScrollStatus();
				RefreshOutput();
			}
			return true;

		case(sf::Keyboard::Key::Enter):
			if (input.type == sf::Event::KeyReleased)
			{
				DString cmd = Input;
				DString params = DString::EmptyString;
				cmd.TrimSpaces();

				if (cmd.Length() > 1)
				{
					Int spaceIdx = cmd.Find(TXT(" "), 1, DString::CC_CaseSensitive);
					if (spaceIdx > 0)
					{
						params = cmd.SubString(spaceIdx + 1);
						params.TrimSpaces();

						DString::SubString(OUT cmd, 0, spaceIdx - 1);
					}

					bool bFoundCmd = false;
					for (const SCommandMap& cmdMap : Commands)
					{
						if (cmdMap.bActive && cmdMap.Exec.IsBounded() && cmdMap.CommandString.Compare(cmd, DString::CC_IgnoreCase) == 0)
						{
							cmdMap.Exec.Execute(params);
							bFoundCmd = true;
							break;
						}
					}

					if (!bFoundCmd)
					{
						RepVarExampleLog.Log(LogCategory::LL_Log, TXT("%s is not a recognized command."), cmd);
					}
				}

				Input = DString::EmptyString;
				SetInputText(TXT("> ") + Input);
			}
			return true;

		case(sf::Keyboard::Key::Escape):
			if (input.type == sf::Event::KeyReleased)
			{
				Input = DString::EmptyString;
				SetInputText(TXT("> "));
			}
			return true;

		case(sf::Keyboard::Key::V):
			if (InputBroadcaster::GetCtrlHeld())
			{
				//User is pasting
				if (input.type == sf::Event::KeyReleased)
				{
					Input += OS_PasteFromClipboard();
					SetInputText(TXT("> ") + Input);
				}

				return true;
			}
	}

	return false;
}

bool RepVarInterface::HandleText (const sf::Event& text)
{
	if (text.text.unicode == 8) //backspace
	{
		if (!Input.IsEmpty())
		{
			Input.PopBack();
		}
	}
	else if (text.text.unicode == '\r' || text.text.unicode == '\n')
	{
		//Noop don't add return characters
	}
	else
	{
		Input += DString(sf::String(text.text.unicode));
	}

	SetInputText(TXT("> ") + Input);
	return true;
}

void RepVarInterface::HandleCursorTick (Float deltaSec)
{
	if (Cursor != nullptr)
	{
		Cursor->SetVisibility(!Cursor->IsVisible());
	}
}
RVE_END