/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarEngineComponent.h
  An engine component responsible for managing Entities that conducts the flow of this example.
=====================================================================
*/

#pragma once

#include "RepVarExample.h"

RVE_BEGIN
class RepVarEntity;
class RepVarInterface;

class RepVarEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(RepVarEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The object responsible for drawing text and interfacing with user input. */
	RepVarInterface* MainInterface;

	/* Becomes true if this process is hosting (port opened). */
	bool bHosting;

	/* The port number this process is listening to for incoming connections. */
	Int ListeningPortNum;

	/* The Entity this client has instantiated. */
	RepVarEntity* LocalEntity;

	/* List of Entities that remote processes instantiated. */
	std::vector<RepVarEntity*> RemoteEntities;

private:
	size_t HostCmdIdx;
	size_t JoinCmdIdx;
	size_t DisconnectCmdIdx;
	size_t SpawnCmdIdx;
	size_t RepObjCmdIdx;
	size_t ListVarCmdIdx;
	size_t SetVarCmdIdx;
	size_t DestroyCmdIdx;

	/* Determines the horizontal spacing between each RepVarEntity (in normalized coordinates). */
	Float RepVarSpacing;

	/* Determins the radius of each RepVarEntity (in pixels). */
	Float RepVarRadius;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	RepVarEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PostInitializeComponent () override;
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg) override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the new Entity to the remote entity vector.
	 * This function also figures out the position this Entity should be displayed on the window.
	 */
	virtual void AddRemoteEntity (RepVarEntity* newEntity);

	/**
	 * Removes the specified Entity from the remote entity vector.
	 * This function also repositions any remaining entities to fill in any missing gaps.
	 */
	virtual void RemoveRemoteEntity (RepVarEntity* targetEntity);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLaunchHost (const DString& params);
	virtual void HandleJoinClient (const DString& params);
	virtual void HandleDisconnect (const DString& params);

	virtual void HandleConnectionAccepted (NetworkSignature* remoteSignature);
	virtual void HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason);

	virtual void HandleSignatureDisconnected (NetworkSignature* signature);

	virtual void HandleSpawnEntity (const DString& params);
	virtual void HandleReplicateEntity (const DString& params);
	virtual void HandleDestroyEntity (const DString& params);

	virtual void HandleListVariables (const DString& params);
	virtual void HandleSetVariable (const DString& params);
};
RVE_END