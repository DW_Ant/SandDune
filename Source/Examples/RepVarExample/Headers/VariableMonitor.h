/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  VariableMonitor.h
  A simple component that watches every registered variable.
  As soon as the variable changes in value, this monitor will log its change.
=====================================================================
*/

#pragma once

#include "RepVarExample.h"

RVE_BEGIN
class VariableMonitor : public EntityComponent
{
	DECLARE_CLASS(VariableMonitor)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SVarData
	{
		DString VarName;
		DProperty* Var;

		/* The most recent Var->ToString() value. Used for detecting changes. */
		DString CurValue;

		SVarData (const DString& inVarName, DProperty* inVar);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Prefix string to log whenever any of the watched variables changed. */
	DString LogPrefix;

	/* List of variables that this component will be watching. */
	std::vector<SVarData> WatchedVariables;

protected:
	TickComponent* WatchTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the specified variable to the watch list. These variables will automatically
	 * unregister as soon as this component detaches from its owner.
	 */
	virtual void AddWatchedVariable (const DString& varName, DProperty* var);

	/**
	 * Determines the frequency how often this component will check each watched variable for changes.
	 * The interval is specified in seconds.
	 */
	virtual void SetWatchFrequency (Float interval);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each variable to check their values. If any values have changed since last check,
	 * this component will log it out.
	 */
	virtual void EvaluateWatchVariables ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWatchTick (Float deltaSec);
};
RVE_END