/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarInterface.h
  An object responsible for displaying the status and logs over the window.
  This also handles the human interfacing aspect that allows the end user to connect
  and send packets to the other process.
=====================================================================
*/

#pragma once

#include "RepVarExample.h"

RVE_BEGIN
class RepVarInterface : public Entity, public PlanarTransform
{
	DECLARE_CLASS(RepVarInterface)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/* Maps a string to a delegate. The string is user facing. It should not contain any spaces since spaces are delimited for parameters. Commands are case insensitive. */
	struct SCommandMap
	{
		DString CommandString;

		/* Help text that describes this command. This is displayed when this command is active and when the user entered 'Help'. */
		DString HelpDesc;

		/* The actual function to execute. Event handlers are expected the parse the parameters. */
		SDFunction<void, const DString& /*params*/> Exec;

		/* If true, then this user can potentially be executed. Becomes false when it's not applicable (eg: can't disconnect when they're already disconnected). */
		bool bActive;

		SCommandMap (const DString& inCommandString, const DString& inHelpDesc, const SDFunction<void, const DString&>& inExec, bool inActive);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	std::vector<SCommandMap> Commands;

protected:
	Int FontSize;

	/* The scroll position where this is the position of the OutputMessages that is at the bottom of the visible text. */
	size_t VertBottomScrollIdx;

	/* List of all recorded logs Network and RepVar logs. */
	std::vector<DString> OutputMessages;

	/* Current input command the user is entering. */
	DString Input;

	/* Components responsible for rendering the display. */
	PlanarTransformComponent* StatusTextTransform;
	TextRenderComponent* StatusText;
	PlanarTransformComponent* StatusBarTransform;
	ColorRenderComponent* StatusBar;
	PlanarTransformComponent* OutputTransform;
	TextRenderComponent* OutputText;
	PlanarTransformComponent* ScrollStatusTransform;
	TextRenderComponent* ScrollStatus;
	PlanarTransformComponent* InputBarTransform;
	ColorRenderComponent* InputBar;
	PlanarTransformComponent* HelpTransform;
	TextRenderComponent* HelpText;

	PlanarTransformComponent* InputTextTransform;
	TextRenderComponent* InputText;
	PlanarTransformComponent* CursorTransform;
	ColorRenderComponent* Cursor;

	/* Other components that controls the flow of this application. */
	InputComponent* InputComp;
	TickComponent* CursorTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Records and displays the log output.
	 */
	virtual void AddLog (const DString& log);

	/**
	 * Redraws the output text based on the scroll positions.
	 */
	virtual void RefreshOutput ();

	/**
	 * Updates the scroll text to reflect the user's current scroll position over the total number of lines.
	 */
	virtual void RefreshScrollStatus ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetStatusText (const DString& newStatusText);
	virtual void SetHelpText (const DString& newHelpText);
	virtual void SetInputText (const DString& newInputText);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleHelp (const DString& params);
	virtual void HandleListConnections (const DString& params);
	virtual void HandleLogSelfIp (const DString& params);
	virtual void HandleExit (const DString& params);

	virtual bool HandleInput (const sf::Event& input);
	virtual bool HandleText (const sf::Event& text);
	virtual void HandleCursorTick (Float deltaSec);
};
RVE_END