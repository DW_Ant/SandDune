/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarExample.h
  Contains important file includes and definitions for the RepVarExample.

  This example provides a simple case of a network application that replicates
  various variables between processes.

  This application uses a peer-to-peer network architecture.

  This application also doesn't consider net relevance, permissions, and ownership.
  The RepVarEntity is always relevant for every connection.
  The host and clients can process any packet regardless if they're net owners or not.
  With the acception of basic bans by ip, there are no access controls. Listeners will accept any connection.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#include "Engine\Network\Headers\NetworkClasses.h"

#define RVE_BEGIN namespace SD { namespace RVE {
#define RVE_END } }

RVE_BEGIN
extern LogCategory RepVarExampleLog;
RVE_END