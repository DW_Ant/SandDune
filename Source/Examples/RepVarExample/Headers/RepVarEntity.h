/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RepVarEntity.h
  A test entity that demonstrates how to replicate variables.
=====================================================================
*/

#pragma once

#include "RepVarExample.h"

RVE_BEGIN
class VariableMonitor;

class RepVarEntity : public Entity, public PlanarTransform
{
	DECLARE_CLASS(RepVarEntity)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SVarNameMapping
	{
		DString VarName;
		DProperty* Var;

		SVarNameMapping (const DString& inVarName, DProperty* inVar);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of variables that maps to a variable name. The Variable Name shall not have spaces. */
	std::vector<SVarNameMapping> VarNameMappings;

	/* This variable will only sync whenever a new client learns of this replicated entity instance. */
	Int SyncOnInitOnly;

	/* This variable will sync every time this value changes. */
	Int SyncOnChange;

	/* This variable will only replicate value changes to net owners. */
	Int NetOwnerOnly;

	/* This variable can only replicate positive numbers. */
	Float PositiveOnly;

	/* This variable only replicates when it's an even number. */
	Int RepEvenOnly;

	/* This variable will invoke the HandleRepNotifyReplicated delegate every time the remote client replicates this variable. */
	DString RepNotify;

	/* The desired size this Entity will gradually grow/shrink to. Does not animate if this value is not positive. */
	Float DesiredBaseSize;

	/* Mirrors the RenderComponent's BaseSize. Since the variable is protected, this variably simply writes to the RenderComponent's BaseSize on tick.
	The reason BaseSize was chosen to demonstrate replication because this is something that's easily visible for continuous changes. */
	Float ReplicatedBaseSize;

protected:
	DPointer<ColorRenderComponent> RenderComp;

	/* Network Component attached to the render component. */
	DPointer<NetworkComponent> RenderNetComp;

	DPointer<NetworkRoleComponent> RoleComp;

	/* Network Component attached to this Entity. */
	DPointer<NetworkComponent> NetComp;

	/* Components related to logging changes whenever certain variaables change in value. */
	VariableMonitor* Monitor;
	VariableMonitor* RenderCompMonitor;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ColorRenderComponent* GetRenderComp () const
	{
		return RenderComp.Get();
	}

	inline NetworkRoleComponent* GetRoleComp () const
	{
		return RoleComp.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initialize variables and components that are specific to the RepVarExample, and are not essential for variable replication.
	 */
	 virtual void InitRepVarComponents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGetNetList (std::vector<NetworkComponent*>& outNetComps) const;
	virtual void HandleNetInitialize (NetworkSignature* connectedTo);
	virtual bool HandleIsNetOwner (NetworkSignature* remoteClient) const;

	virtual void HandleReplicatedSizeTick (Float deltaSec);
	virtual void HandleGrowthTick (Float deltaSec);

	virtual bool HandleValidatePositiveOnly (const Float& val);
	virtual void HandleRepNotifyReplicated ();
	virtual void HandleSizeReplicated ();
};
RVE_END