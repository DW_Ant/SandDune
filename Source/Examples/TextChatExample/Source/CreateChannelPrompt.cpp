/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CreateChannelPrompt.cpp
=====================================================================
*/

#include "CreateChannelPrompt.h"
#include "Messenger.h"
#include "TextChatEngineComponent.h"

IMPLEMENT_CLASS(SD::TCE::CreateChannelPrompt, SD::GuiEntity)
TCE_BEGIN

void CreateChannelPrompt::InitProps ()
{
	Super::InitProps();

	Blackout = nullptr;

	PromptFrame = nullptr;
	PromptLabel = nullptr;
	ChannelNameField = nullptr;
	AcceptButton = nullptr;
	CancelButton = nullptr;
}

void CreateChannelPrompt::ConstructUI ()
{
	Super::ConstructUI();

	Blackout = FrameComponent::CreateObject();
	if (AddComponent(Blackout))
	{
		Blackout->SetCenterColor(Color(0, 0, 0, 96));
		Blackout->SetBorderThickness(0.f);
		Blackout->SetAlwaysConsumeMouseEvents(true);

		PromptFrame = FrameComponent::CreateObject();
		if (Blackout->AddComponent(PromptFrame))
		{
			PromptFrame->SetPosition(Vector2(0.25f, 0.33f));
			PromptFrame->SetSize(Vector2(0.5f, 0.33f));

			PromptLabel = LabelComponent::CreateObject();
			if (PromptFrame->AddComponent(PromptLabel))
			{
				PromptLabel->SetAutoRefresh(false);
				PromptLabel->SetAnchorLeft(0.15f);
				PromptLabel->SetAnchorTop(0.15f);
				PromptLabel->SetAnchorRight(0.15f);
				PromptLabel->SetAnchorBottom(0.5f);
				PromptLabel->SetWrapText(true);
				PromptLabel->SetClampText(true);
				PromptLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				PromptLabel->SetText(TXT("Specify the new channel's name."));
				PromptLabel->SetAutoRefresh(true);
			}

			ChannelNameField = TextFieldComponent::CreateObject();
			if (PromptFrame->AddComponent(ChannelNameField))
			{
				ChannelNameField->SetAutoRefresh(false);
				ChannelNameField->SetAnchorLeft(0.15f);
				ChannelNameField->SetAnchorTop(0.45f);
				ChannelNameField->SetAnchorRight(0.15f);
				ChannelNameField->SetAnchorBottom(0.45f);
				ChannelNameField->SetWrapText(false);
				ChannelNameField->SetClampText(true);
				ChannelNameField->SetText(TXT("New Channel"));
				ChannelNameField->SetHorizontalAlignment(LabelComponent::HA_Center);
				ChannelNameField->MaxNumCharacters = 32;
				ChannelNameField->OnReturn = SDFUNCTION_1PARAM(this, CreateChannelPrompt, HandleChannelNameReturn, void, TextFieldComponent*);
				ChannelNameField->SetAutoRefresh(true);				
			}

			AcceptButton = ButtonComponent::CreateObject();
			if (PromptFrame->AddComponent(AcceptButton))
			{
				AcceptButton->SetPosition(Vector2(0.f, 0.8f));
				AcceptButton->SetAnchorLeft(0.1f);
				AcceptButton->SetSize(Vector2(0.2f, 0.15f));
				AcceptButton->SetCaptionText(TXT("Create"));
				AcceptButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, CreateChannelPrompt, HandleAcceptReleased, void, ButtonComponent*));
			}

			CancelButton = ButtonComponent::CreateObject();
			if (PromptFrame->AddComponent(CancelButton))
			{
				CancelButton->SetPosition(Vector2(0.f, 0.8f));
				CancelButton->SetAnchorRight(0.1f);
				CancelButton->SetSize(Vector2(0.2f, 0.15f));
				CancelButton->SetCaptionText(TXT("Cancel"));
				CancelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, CreateChannelPrompt, HandleCancelReleased, void, ButtonComponent*));
			}
		}
	}
}

void CreateChannelPrompt::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	Focus->TabOrder.push_back(ChannelNameField);
	Focus->TabOrder.push_back(AcceptButton);
	Focus->TabOrder.push_back(CancelButton);
}

void CreateChannelPrompt::CreateChannel ()
{
	if (ChannelNameField != nullptr)
	{
		TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
		CHECK(localChatEngine != nullptr)

		if (Messenger* chatHistory = localChatEngine->GetChatHistory())
		{
			DString channelName = ChannelNameField->GetContent();
			channelName.TrimSpaces();
			if (!channelName.IsEmpty())
			{
				chatHistory->ServerCreateChannel(BaseRpc::RT_Servers, channelName);
			}
		}
	}

	Destroy();
}

void CreateChannelPrompt::HandleChannelNameReturn (TextFieldComponent* uiComp)
{
	CreateChannel();
}

void CreateChannelPrompt::HandleAcceptReleased (ButtonComponent* uiComp)
{
	CreateChannel();
}

void CreateChannelPrompt::HandleCancelReleased (ButtonComponent* uiComp)
{
	Destroy();
}
TCE_END