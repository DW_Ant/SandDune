/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatExample.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "TextChatEngineComponent.h"
#include "TextChatExample.h"

SD::LogCategory SD::TCE::TextChatExampleLog(TXT("Text Chat Example"), SD::LogCategory::VERBOSITY_DEFAULT,
	SD::LogCategory::FLAG_STANDARD_OUTPUT |
	SD::LogCategory::FLAG_OS_OUTPUT |
	SD::LogCategory::FLAG_LOG_FILE |
	SD::LogCategory::FLAG_OUTPUT_WINDOW |
	SD::LogCategory::FLAG_CONSOLE_MSG);

//Forward function declarations
int BeginSandDune (TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	TCHAR* args = GetCommandLine();
	return BeginSandDune(&args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (TCHAR* args[])
{
	SD::ProjectName = TXT("Text Chat Example");

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(args[0]);
	engine->SetMinDeltaTime(0.033f); //~30 fps. This simple application doesn't need a high refresh rate.

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	bool bIsServer = engine->HasCmdLineSwitch(TXT("-server"), SD::DString::CC_IgnoreCase);

	//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
	std::vector<SD::EngineComponent*> engineComponents;
	engineComponents.push_back(new SD::LoggerEngineComponent());
	engineComponents.push_back(new SD::NetworkEngineComponent());
	engineComponents.push_back(new SD::TCE::TextChatEngineComponent(bIsServer));

	if (!bIsServer)
	{
		engineComponents.push_back(new SD::GraphicsEngineComponent());
		engineComponents.push_back(new SD::GuiEngineComponent());
		engineComponents.push_back(new SD::InputEngineComponent());
	}

	engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

	SD::NetworkEngineComponent* localNetEngine = SD::NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)

	SD::NetworkSignature::ENetworkRole role = (bIsServer) ? SD::NetworkSignature::NR_Server : SD::NetworkSignature::NR_Client;
	if (!localNetEngine->InitializeLocalNetworkSignature(role, {}))
	{
		SD::TCE::TextChatExampleLog.Log(SD::LogCategory::LL_Critical, TXT("Unable to to initialize local network signature."));
		engine->Shutdown();
		delete engine;
		engine = nullptr;
		return -1;
	}

	if (bIsServer)
	{
		SD::LoggerEngineComponent* localLogEngine = SD::LoggerEngineComponent::Find();
		CHECK(localLogEngine != nullptr)
		localLogEngine->CreateConsole();
	}

	return RunMainLoop();
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}