/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatGui.cpp
=====================================================================
*/

#include "ChatParticipant.h"
#include "CreateChannelPrompt.h"
#include "Messenger.h"
#include "TextChatEngineComponent.h"
#include "TextChatGui.h"

IMPLEMENT_CLASS(SD::TCE::TextChatGui, SD::GuiEntity)
TCE_BEGIN

void TextChatGui::InitProps ()
{
	Super::InitProps();

	TopFrame = nullptr;
	StatusText = nullptr;
	DestinationPrompt = nullptr;
	ServerDestinationField = nullptr;
	ConnectButton = nullptr;
	PlayerNamePrompt = nullptr;
	PlayerNameField = nullptr;

	ChannelFrame = nullptr;
	ChannelScrollbar = nullptr;
	ChannelEntity = nullptr;
	ChannelList = nullptr;
	CreateChannelButton = nullptr;

	ChatFrame = nullptr;
	ChatScrollbar = nullptr;
	ChatEntity = nullptr;
	ChatHistory = nullptr;
	ChatField = nullptr;

	ParticipantFrame = nullptr;
	ParticipantScrollbar = nullptr;
	ParticipantEntity = nullptr;
	ParticipantList = nullptr;

	OriginalDestinationColor = sf::Color::Black;
}

void TextChatGui::BeginObject ()
{
	Super::BeginObject();

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleConnectionAccepted, void, NetworkSignature*));
		connections->OnConnectionRejected.RegisterHandler(SDFUNCTION_2PARAM(this, TextChatGui, HandleConnectionRejected, void, const sf::IpAddress&, Int));
	}
}

void TextChatGui::ConstructUI ()
{
	Super::ConstructUI();

	Color scrollbarBackgroundColor = Color(0, 0, 0, 32); //Faint dark highlight

	TopFrame = FrameComponent::CreateObject();
	if (AddComponent(TopFrame))
	{
		TopFrame->SetPosition(Vector2(0.f, 0.f));
		TopFrame->SetSize(Vector2(1.f, 0.15f));
		TopFrame->SetLockedFrame(true);

		StatusText = LabelComponent::CreateObject();
		if (TopFrame->AddComponent(StatusText))
		{
			StatusText->SetAutoRefresh(false);
			StatusText->SetPosition(Vector2(0.05f, 0.05f));
			StatusText->SetSize(Vector2(0.4f, 0.5f));
			StatusText->SetWrapText(false);
			StatusText->SetClampText(true);
			StatusText->SetText(TXT("Offline"));
			StatusText->SetAutoRefresh(true);
		}

		DestinationPrompt = LabelComponent::CreateObject();
		if (TopFrame->AddComponent(DestinationPrompt))
		{
			DestinationPrompt->SetAutoRefresh(false);
			DestinationPrompt->SetPosition(Vector2(0.02f, 0.5f));
			DestinationPrompt->SetSize(Vector2(0.1f, 0.5f));
			DestinationPrompt->SetWrapText(false);
			DestinationPrompt->SetClampText(true);
			DestinationPrompt->SetText(TXT("URL"));
			DestinationPrompt->SetAutoRefresh(true);
		}

		ServerDestinationField = TextFieldComponent::CreateObject();
		if (TopFrame->AddComponent(ServerDestinationField))
		{
			ServerDestinationField->SetAutoRefresh(false);
			ServerDestinationField->SetPosition(Vector2(0.1f, 0.5f));
			ServerDestinationField->SetSize(Vector2(0.2f, 0.25f));
			ServerDestinationField->SetWrapText(false);
			ServerDestinationField->SetClampText(true);
			ServerDestinationField->SetText(TXT("127.0.0.1"));
			ServerDestinationField->OnReturn = SDFUNCTION_1PARAM(this, TextChatGui, HandleDestinationReturn, void, TextFieldComponent*);
			ServerDestinationField->OnEdit = SDFUNCTION_1PARAM(this, TextChatGui, HandleDestinationEdit, bool, const sf::Event&);
			ServerDestinationField->SetAutoRefresh(true);

			if (ServerDestinationField->GetRenderComponent() != nullptr)
			{
				OriginalDestinationColor = ServerDestinationField->GetRenderComponent()->GetFontColor();
			}
		}

		ConnectButton = ButtonComponent::CreateObject();
		if (TopFrame->AddComponent(ConnectButton))
		{
			ConnectButton->SetPosition(Vector2(0.35f, 0.45f));
			ConnectButton->SetSize(Vector2(0.2f, 0.35f));
			ConnectButton->SetCaptionText(TXT("Connect"));
			ConnectButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleConnectReleased, void, ButtonComponent*));
		}

		PlayerNamePrompt = LabelComponent::CreateObject();
		if (TopFrame->AddComponent(PlayerNamePrompt))
		{
			PlayerNamePrompt->SetAutoRefresh(false);
			PlayerNamePrompt->SetPosition(Vector2(0.6f, 0.05f));
			PlayerNamePrompt->SetSize(Vector2(0.33f, 0.5f));
			PlayerNamePrompt->SetWrapText(false);
			PlayerNamePrompt->SetClampText(true);
			PlayerNamePrompt->SetHorizontalAlignment(LabelComponent::HA_Center);
			PlayerNamePrompt->SetText(TXT("Your Name:"));
			PlayerNamePrompt->SetAutoRefresh(true);
		}

		PlayerNameField = TextFieldComponent::CreateObject();
		if (TopFrame->AddComponent(PlayerNameField))
		{
			PlayerNameField->SetAutoRefresh(false);
			PlayerNameField->SetPosition(Vector2(0.6f, 0.5f));
			PlayerNameField->SetSize(Vector2(0.33f, 0.25f));
			PlayerNameField->SetWrapText(false);
			PlayerNameField->SetClampText(true);
			PlayerNameField->MaxNumCharacters = 26;
			PlayerNameField->SetHorizontalAlignment(LabelComponent::HA_Center);
			PlayerNameField->SetText(TXT("Anonymous"));
			PlayerNameField->OnReturn = SDFUNCTION_1PARAM(this, TextChatGui, HandlePlayerNameReturn, void, TextFieldComponent*);
			PlayerNameField->SetAutoRefresh(true);
		}
	}

	ChannelFrame = FrameComponent::CreateObject();
	if (AddComponent(ChannelFrame))
	{
		ChannelFrame->SetPosition(Vector2(0.f, 0.15f));
		ChannelFrame->SetSize(Vector2(0.2f, 1.f - ChannelFrame->ReadPosition().Y));

		ChannelScrollbar = ScrollbarComponent::CreateObject();
		if (ChannelFrame->AddComponent(ChannelScrollbar))
		{
			ChannelScrollbar->SetPosition(Vector2(0.01f, 0.01f));
			ChannelScrollbar->SetSize(Vector2(0.98f, 0.85f));
			ChannelScrollbar->SetHideControlsWhenFull(true);
			if (ChannelScrollbar->GetFrameTexture() != nullptr)
			{
				ChannelScrollbar->GetFrameTexture()->ResetColor = scrollbarBackgroundColor;
			}

			ChannelEntity = GuiEntity::CreateObject();
			ChannelEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			ChannelEntity->SetAutoSizeVertical(true);
			ChannelScrollbar->SetViewedObject(ChannelEntity);

			ChannelList = ListBoxComponent::CreateObject();
			if (ChannelEntity->AddComponent(ChannelList))
			{
				ChannelList->SetPosition(Vector2::ZERO_VECTOR);
				ChannelList->SetSize(Vector2(1.f, 16.f));
				ChannelList->SetAutoDeselect(true);
				ChannelList->SetMinNumSelected(1); //Make it impossible to "toggle off" a channel
				ChannelList->OnItemSelected = SDFUNCTION_1PARAM(this, TextChatGui, HandleChannelSelected, void, Int);
			}
		}

		CreateChannelButton = ButtonComponent::CreateObject();
		if (ChannelFrame->AddComponent(CreateChannelButton))
		{
			CreateChannelButton->SetAnchorLeft(0.125f);
			CreateChannelButton->SetAnchorRight(0.125f);
			CreateChannelButton->SetAnchorBottom(0.025f);
			CreateChannelButton->SetSize(Vector2(1.f, 0.075f));
			CreateChannelButton->SetCaptionText(TXT("Create Channel"));
			CreateChannelButton->SetEnabled(false);
			CreateChannelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleCreateChannelReleased, void, ButtonComponent*));
		}
	}

	ChatFrame = FrameComponent::CreateObject();
	if (AddComponent(ChatFrame))
	{
		ChatFrame->SetPosition(Vector2(ChannelFrame->ReadSize().X, TopFrame->ReadSize().Y));
		ChatFrame->SetSize(Vector2(1.f - (ChatFrame->ReadPosition().X * 2.f), 1.f - ChatFrame->ReadPosition().Y));
		
		ChatScrollbar = ScrollbarComponent::CreateObject();
		if (ChatFrame->AddComponent(ChatScrollbar))
		{
			ChatScrollbar->SetPosition(Vector2::ZERO_VECTOR);
			ChatScrollbar->SetSize(Vector2(1.f, 0.85f));
			ChatScrollbar->SetHideControlsWhenFull(true);
			if (ChatScrollbar->GetFrameTexture() != nullptr)
			{
				ChatScrollbar->GetFrameTexture()->ResetColor = scrollbarBackgroundColor;
			}

			ChatEntity = GuiEntity::CreateObject();
			ChatEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			ChatEntity->SetAutoSizeVertical(true);
			ChatScrollbar->SetViewedObject(ChatEntity);

			ChatHistory = LabelComponent::CreateObject();
			if (ChatEntity->AddComponent(ChatHistory))
			{
				ChatHistory->SetAutoRefresh(false);
				ChatHistory->SetPosition(Vector2(0.01f, 0.01f));
				ChatHistory->SetSize(1.f, 1.f);
				ChatHistory->SetAutoSizeVertical(true);
				ChatHistory->SetWrapText(true);
				ChatHistory->SetClampText(false);
				ChatHistory->SetAutoRefresh(true);
			}

			ChatField = TextFieldComponent::CreateObject();
			if (ChatFrame->AddComponent(ChatField))
			{
				ChatField->SetAutoRefresh(false);
				ChatField->SetPosition(Vector2(0.f, ChatScrollbar->ReadSize().Y));
				ChatField->SetSize(Vector2(1.f, 1.f - ChatScrollbar->ReadSize().Y));
				ChatField->SetWrapText(true);
				ChatField->SetClampText(true);
				ChatField->SetEditable(false);
				ChatField->MaxNumCharacters = 1024;
				ChatField->OnReturn = SDFUNCTION_1PARAM(this, TextChatGui, HandleChatReturn, void, TextFieldComponent*);
				ChatField->SetAutoRefresh(true);
			}
		}
	}

	ParticipantFrame = FrameComponent::CreateObject();
	if (AddComponent(ParticipantFrame))
	{
		ParticipantFrame->SetPosition(Vector2(ChatFrame->ReadPosition().X + ChatFrame->ReadSize().X, TopFrame->ReadSize().Y));
		ParticipantFrame->SetSize(Vector2(1.f - ParticipantFrame->ReadPosition().X, 1.f - TopFrame->ReadSize().Y));
		
		ParticipantScrollbar = ScrollbarComponent::CreateObject();
		if (ParticipantFrame->AddComponent(ParticipantScrollbar))
		{
			ParticipantScrollbar->SetPosition(Vector2::ZERO_VECTOR);
			ParticipantScrollbar->SetSize(Vector2(1.f, 1.f));
			ParticipantScrollbar->SetHideControlsWhenFull(true);
			if (ParticipantScrollbar->GetFrameTexture() != nullptr)
			{
				ParticipantScrollbar->GetFrameTexture()->ResetColor = scrollbarBackgroundColor;
			}

			ParticipantEntity = GuiEntity::CreateObject();
			ParticipantEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			ParticipantEntity->SetAutoSizeVertical(true);
			ParticipantScrollbar->SetViewedObject(ParticipantEntity);

			ParticipantList = LabelComponent::CreateObject();
			if (ParticipantEntity->AddComponent(ParticipantList))
			{
				ParticipantList->SetAutoRefresh(false);
				ParticipantList->SetPosition(Vector2::ZERO_VECTOR);
				ParticipantList->SetSize(Vector2(1.f, 1.f));
				ParticipantList->SetAutoSizeVertical(true);
				ParticipantList->SetWrapText(false);
				ParticipantList->SetClampText(false);
				ParticipantList->SetHorizontalAlignment(LabelComponent::HA_Center);
				ParticipantList->SetAutoRefresh(true);
			}
		}
	}
}

void TextChatGui::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	Focus->TabOrder.push_back(ServerDestinationField);
	Focus->TabOrder.push_back(ConnectButton);
	Focus->TabOrder.push_back(PlayerNameField);

	Focus->TabOrder.push_back(CreateChannelButton);

	Focus->TabOrder.push_back(ChatField);
}

void TextChatGui::Destroyed ()
{
	if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
	{
		connection->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleConnectionAccepted, void, NetworkSignature*));
		connection->OnConnectionRejected.UnregisterHandler(SDFUNCTION_2PARAM(this, TextChatGui, HandleConnectionRejected, void, const sf::IpAddress&, Int));
	}

	Super::Destroyed();
}

void TextChatGui::AddChatHistory (const DString& channelName, const Messenger::SMessageEntry& newMsg)
{
	//Check if the channel name is the selected channel
	if (ChannelList->IsItemSelected(channelName) && ChatHistory != nullptr)
	{
		DString content = ChatHistory->GetContent();
		content += MessageEntryToString(newMsg);
		content += TXT("\n\n");
		ChatHistory->SetText(content);
	}
}

void TextChatGui::RefreshChannelList (Messenger* history)
{
	if (ChannelList != nullptr && history != nullptr)
	{
		std::vector<GuiDataElement<DString>> newChannels;

		for (const Messenger::SChannelMessages& channel : history->ReadChatHistory())
		{
			newChannels.emplace_back(GuiDataElement<DString>(channel.ChannelName, channel.ChannelName));
		}

		ChannelList->SetList(newChannels);

		if (!ContainerUtils::IsEmpty(ChannelList->ReadList()) && ContainerUtils::IsEmpty(ChannelList->GetSelectedItems()))
		{
			//Select the first channel
			ChannelList->SetItemSelectionByIdx(0, true);
		}
	}
}

void TextChatGui::RefreshUserList ()
{
	if (ParticipantList == nullptr)
	{
		return;
	}

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr)

	DString newList = DString::EmptyString;
	if (localChatEngine->GetLocalParticipant() != nullptr) //LocalParticipant could be nullptr if the server sent other participant data before the local one is sent.
	{
		newList = localChatEngine->GetLocalParticipant()->GetUserName(); //The local user is always listed on top.
	}

	std::vector<DString> users;
	for (ChatParticipant* remoteUser : localChatEngine->ReadRemoteParticipants())
	{
		if (remoteUser != nullptr)
		{
			users.push_back(remoteUser->GetUserName());
		}
	}

	//Sort alphabetically
	std::sort(users.begin(), users.end(), [](const DString& a, const DString& b)
	{
		return (a.Compare(b, DString::CC_CaseSensitive) < 0);
	});

	for (size_t i = 0; i < users.size(); ++i)
	{
		newList += TXT("\n") + users.at(i);
	}

	ParticipantList->SetText(newList);
}

DString TextChatGui::GetLocalUserName () const
{
	if (PlayerNameField != nullptr)
	{
		return PlayerNameField->GetContent();
	}

	return DString::EmptyString;
}

void TextChatGui::SetToOfflineMode ()
{
	if (ServerDestinationField != nullptr)
	{
		ServerDestinationField->SetEditable(true);
	}

	if (ConnectButton != nullptr)
	{
		ConnectButton->SetCaptionText(TXT("Connect"));
		ConnectButton->SetEnabled(true);
	}

	if (StatusText != nullptr)
	{
		StatusText->SetText(TXT("Offline"));
	}

	if (CreateChannelButton != nullptr)
	{
		CreateChannelButton->SetEnabled(false);
	}

	if (ChatField != nullptr)
	{
		ChatField->SetEditable(false);
	}
}

void TextChatGui::SetToOnlineMode ()
{
	if (ConnectButton != nullptr)
	{
		ConnectButton->SetCaptionText(TXT("Disconnect"));
		ConnectButton->SetEnabled(true);
	}

	if (StatusText != nullptr)
	{
		StatusText->SetText(TXT("Online"));
	}

	if (CreateChannelButton != nullptr)
	{
		CreateChannelButton->SetEnabled(true);
	}

	if (ChatField != nullptr)
	{
		ChatField->SetEditable(true);
	}
}

DString TextChatGui::MessageEntryToString (const Messenger::SMessageEntry& msg) const
{
	DString author = (msg.Author != nullptr) ? msg.Author->ReadUserName() : TXT("Departed User");
	return DString::CreateFormattedString(TXT("%s - %s\n%s"), author, msg.Timestamp.FormatString(TXT("%hour:%minute:%second")), msg.Message);
}

void TextChatGui::HandleConnectReleased (ButtonComponent* uiComp)
{
	CHECK(ServerDestinationField != nullptr && ConnectButton != nullptr)

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	if (localNetEngine->GetConnections()->GetNumConnections() == 0) //is offline
	{
		ServerDestinationField->SetEditable(false);
		ConnectButton->SetEnabled(false);

		//check if it's a valid ip
		sf::IpAddress dest(ServerDestinationField->GetContent().ToStr());
		if (dest == sf::IpAddress::None)
		{
			TextChatExampleLog.Log(LogCategory::LL_Warning, TXT("\"%s\" is not a valid destination."), ServerDestinationField->GetContent());
			if (TextRenderComponent* rendComp = ServerDestinationField->GetRenderComponent())
			{
				rendComp->SetFontColor(sf::Color::Red);
			}

			ServerDestinationField->SetEditable(true);
			ConnectButton->SetEnabled(true);
			return;
		}

		if (StatusText != nullptr)
		{
			StatusText->SetText(TXT("Connecting. . ."));
		}

	
		localNetEngine->GetConnections()->OpenConnection(dest);
	}
	else //is online
	{
		SetToOfflineMode();
		localNetEngine->GetConnections()->DisconnectFromAll(false);
	}
}

void TextChatGui::HandleDestinationReturn (TextFieldComponent* uiComp)
{
	if (ConnectButton != nullptr)
	{
		ConnectButton->SetButtonUp(true, true);
	}
}

bool TextChatGui::HandleDestinationEdit (const sf::Event& evnt)
{
	if (ServerDestinationField != nullptr)
	{
		if (TextRenderComponent* renderComp = ServerDestinationField->GetRenderComponent())
		{
			renderComp->SetFontColor(OriginalDestinationColor);
		}
	}

	return false; //Don't consume input
}

void TextChatGui::HandlePlayerNameReturn (TextFieldComponent* uiComp)
{
	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr)

	if (localChatEngine->GetLocalParticipant() != nullptr && PlayerNameField != nullptr)
	{
		DString newName = PlayerNameField->GetContent();
		newName.TrimSpaces();
		if (!newName.IsEmpty())
		{
			localChatEngine->GetLocalParticipant()->SetUserName(newName);
		}
	}
}

void TextChatGui::HandleChannelSelected (Int selectedItemIdx)
{
	CHECK(ChannelList != nullptr)
	DString channelName;
	if (!ChannelList->GetItemByIdx(selectedItemIdx, OUT channelName))
	{
		TextChatExampleLog.Log(LogCategory::LL_Warning, TXT("TextChatGui is unable to respond to a channel selection. Unable to obtain a channel name from index %s."), selectedItemIdx);
		return;
	}

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr)
	if (localChatEngine->GetChatHistory() == nullptr)
	{
		//Must be disconnected
		return;
	}

	DString chatHistoryText = DString::EmptyString;
	const std::vector<Messenger::SChannelMessages>& messages = localChatEngine->GetChatHistory()->ReadChatHistory();
	for (const Messenger::SChannelMessages& channel : messages)
	{
		if (channel.ChannelName.Compare(channelName, DString::CC_CaseSensitive) == 0)
		{
			for (const Messenger::SMessageEntry& msg : channel.Messages)
			{
				chatHistoryText += MessageEntryToString(msg) + TXT("\n\n");
			}

			break;
		}
	}

	if (ChatHistory != nullptr)
	{
		ChatHistory->SetText(chatHistoryText);
	}
}

void TextChatGui::HandleCreateChannelReleased (ButtonComponent* uiComp)
{
	CreateChannelPrompt* prompt = CreateChannelPrompt::CreateObject();
	prompt->RegisterToMainWindow(true, true, 200);
}

void TextChatGui::HandleChatReturn (TextFieldComponent* uiComp)
{
	CHECK(uiComp != nullptr)
	DString newMsg = uiComp->GetContent();

	DString channelName;
	if (ChannelList == nullptr)
	{
		return;
	}

	std::vector<DString> selectedChannels;
	ChannelList->GetSelectedItems(OUT selectedChannels);
	if (ContainerUtils::IsEmpty(selectedChannels))
	{
		return;
	}
	channelName = selectedChannels.at(0);

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr && localChatEngine->GetLocalParticipant())
	if (Messenger* chatHistory = localChatEngine->GetChatHistory())
	{
		chatHistory->ServerAddMessage(BaseRpc::RT_Servers, localChatEngine->GetLocalParticipant(), channelName, newMsg);
	}

	if (ChatField != nullptr)
	{
		ChatField->SetText(DString::EmptyString);
		ChatField->SetCursorPosition(0);
	}
}

void TextChatGui::HandleConnectionAccepted (NetworkSignature* signature)
{
	if (signature != nullptr)
	{
		signature->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleServerDisconnect, void, NetworkSignature*));
	}

	SetToOnlineMode();
}

void TextChatGui::HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason)
{
	SetToOfflineMode();
}

void TextChatGui::HandleServerDisconnect (NetworkSignature* serverSignature)
{
	if (serverSignature != nullptr)
	{
		serverSignature->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, TextChatGui, HandleServerDisconnect, void, NetworkSignature*));
	}

	SetToOfflineMode();
}
TCE_END