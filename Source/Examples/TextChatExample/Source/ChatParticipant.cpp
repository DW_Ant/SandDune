/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ChatParticipant.cpp
=====================================================================
*/

#include "ChatParticipant.h"
#include "Messenger.h"
#include "TextChatEngineComponent.h"
#include "TextChatGui.h"

IMPLEMENT_CLASS(SD::TCE::ChatParticipant, SD::Entity)
TCE_BEGIN

void ChatParticipant::InitProps ()
{
	Super::InitProps();

	UserName = TXT("Anonymous");

	RoleComp = nullptr;
	NetComp = nullptr;

	bReceivedInitUserName = false;
}

void ChatParticipant::BeginObject ()
{
	Super::BeginObject();

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = SDFUNCTION_1PARAM(this, ChatParticipant, HandleGetComponentList, void, std::vector<NetworkComponent*>&);
		RoleComp->OnIsNetOwner = SDFUNCTION_1PARAM(this, ChatParticipant, HandleIsNetOwner, bool, NetworkSignature*);
		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
			CHECK(localChatEngine != nullptr)

			if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
			{
				//Update the user name based on the Gui Text Field
				if (localChatEngine->GetChatGui() != nullptr)
				{
					DString newName = localChatEngine->GetChatGui()->GetLocalUserName();
					if (!newName.IsEmpty())
					{
						//Forcibly notify the server of this client's user name since the client can set their name before connecting to the server.
						ServerSetUserName(connectedTo, newName);
						//If the server approves the request to set the name, then the UserName will be set.
					}
				}
			}
			else if (RoleComp->HasAuthority() && RoleComp->GetRemoteNetRole(connectedTo) != NetworkRoleComponent::NR_NetOwner)
			{
				//The server needs to notify the client about this participant's name with the exception of the NetOwner.
				//The NetOwner will notify the server what their name is.
				ClientSetUserName(connectedTo, UserName);
			}

			//Register the chat participant
			if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
			{
				localChatEngine->SetLocalParticipant(this);
			}
			else
			{
				//Note: This can be called multiple times on the server. Called once for each client. This is why the AddRemoteParticipant checks for duplicates.
				localChatEngine->AddRemoteParticipant(this);
			}
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		BIND_RPC_1PARAM(NetComp, ServerSetUserName, this, ChatParticipant, DString);
		ServerSetUserName.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1PARAM(NetComp, ClientSetUserName, this, ChatParticipant, DString);
		ClientSetUserName.CanBeExecutedOn = NetworkSignature::NR_Client;
	}
}

void ChatParticipant::Destroyed ()
{
	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	if (localChatEngine != nullptr)
	{
		if (RoleComp != nullptr)
		{
			if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
			{
				localChatEngine->SetLocalParticipant(nullptr);
			}
			else
			{
				localChatEngine->RemoveRemoteParticipant(this);
			}
		}

		if (Messenger* chatHistory = localChatEngine->GetChatHistory())
		{
			chatHistory->RemoveParticipant(this);
		}
	}

	Super::Destroyed();
}

void ChatParticipant::SetUserName (DString newUserName)
{
	newUserName.TrimSpaces();
	if (!newUserName.IsEmpty())
	{
		//Alternatively, having UserName as a replicated variable would be easier to handle than relying on RPC's.
		UserName = newUserName;
		if (RoleComp->IsClient())
		{
			if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
			{
				//Client is notifying the server about the name change.
				ServerSetUserName(BaseRpc::RT_Servers, UserName);
			}

			TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
			CHECK(localChatEngine != nullptr && localChatEngine->GetChatGui() != nullptr)
			localChatEngine->GetChatGui()->RefreshUserList();
		}
		else
		{
			//Server needs to update every client about this client's name change.
			ClientSetUserName(BaseRpc::RT_Clients, UserName);
		}
	}
}

void ChatParticipant::SetOwningClient (NetworkSignature* newOwningClient)
{
	OwningClient = newOwningClient;
}

bool ChatParticipant::ServerSetUserName_Implementation (DString newUserName)
{
	newUserName.TrimSpaces();
	if (newUserName.IsEmpty())
	{
		//Client's application should be checking for empty names before sending to the server. Returning false to send a rejection packet.
		//A rejection packet would cause the the NetworkMonitor to keep an eye on that client for sending a malformed packet.
		return false;
	}

	//Check if the name is already taken.
	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr)
	if (bReceivedInitUserName)
	{
		for (ChatParticipant* remoteParticipant : localChatEngine->ReadRemoteParticipants())
		{
			if (remoteParticipant->ReadUserName().Compare(newUserName, DString::CC_CaseSensitive) == 0)
			{
				if (remoteParticipant == this)
				{
					//User is not changing their name. Do nothing. Still return true to notify the system that the RPC was accepted.
					return true;
				}
				else
				{
					//User chose a name conflict. Reset to defaults.
					if (const ChatParticipant* cdo = dynamic_cast<const ChatParticipant*>(GetDefaultObject()))
					{
						newUserName = cdo->UserName;
						break;
					}
				}
			}
		}
	}

	bReceivedInitUserName = true;
	SetUserName(newUserName);
	return true;
}

bool ChatParticipant::ClientSetUserName_Implementation (DString newUserName)
{
	newUserName.TrimSpaces();
	if (newUserName.IsEmpty())
	{
		//Malformed packet. The server should never send an empty name.
		return false;
	}

	//Clients will receive a callback even if they sent the server call. The server could have responded with a different name.
	//Check if there's a name change. If so, then rerun SetUserName. Otherwise return early to avoid resending the change back to the server.
	//Alternatively, using a replicated variable would be easier in this case as there's no need to have a circular check.
	if (bReceivedInitUserName && UserName.Compare(newUserName, DString::CC_IgnoreCase) == 0)
	{
		return true;
	}

	bReceivedInitUserName = true;
	SetUserName(newUserName);
	return true;
}

void ChatParticipant::HandleGetComponentList (std::vector<NetworkComponent*>& outNetComponents) const
{
	outNetComponents.push_back(NetComp);
}

bool ChatParticipant::HandleIsNetOwner (NetworkSignature* remoteClient) const
{
	/*
	Clients instantiate their own ChatParticipant instances for each user (since the RoleComponent states that each participant is relevant).
	However only the server will notify which one of the instances are the net owner for each client. This function determines the net ownership.
	Clients can only invoke RPC's when they are a NetOwner of that instance. This prevents clients from setting user names for others.
	*/
	return (remoteClient == OwningClient);
}

void ChatParticipant::HandleDisconnected (NetworkSignature* client)
{
	Destroy();
}
TCE_END