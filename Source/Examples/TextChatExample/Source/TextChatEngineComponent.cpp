/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatEngineComponent.cpp
=====================================================================
*/

#include "ChatParticipant.h"
#include "Messenger.h"
#include "TextChatEngineComponent.h"
#include "TextChatGui.h"
#include "TextChatTheme.h"

IMPLEMENT_ENGINE_COMPONENT(SD::TCE::TextChatEngineComponent)
TCE_BEGIN

TextChatEngineComponent::TextChatEngineComponent () : Super(),
	bIsServer(false)
{
	//Noop
}

TextChatEngineComponent::TextChatEngineComponent (bool bInServer) : Super(),
	bIsServer(bInServer)
{
	//Noop
}

void TextChatEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	if (guiEngine != nullptr)
	{
		guiEngine->SetDefaultThemeClass(TextChatTheme::SStaticClass());
	}
}

void TextChatEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	if (!bIsServer)
	{
		GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
		if (guiEngine != nullptr && guiEngine->GetGuiTheme() != nullptr)
		{
			guiEngine->GetGuiTheme()->SetDefaultStyle(TextChatTheme::CHAT_STYLE_NAME);
		}

		ChatGui = TextChatGui::CreateObject();
		ChatGui->RegisterToMainWindow(true, true);
	}
	else
	{
		//LocalParticipant = ChatParticipant::CreateObject();
		ChatHistory = Messenger::CreateObject();

		//Have the server create its own default channel. This is executed locally.
		ChatHistory->ServerCreateChannel.LocalExecute(TXT("General"));
	}

	if (ConnectionManager* localConnections = ConnectionManager::GetConnectionManager())
	{
		localConnections->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, TextChatEngineComponent, HandleNewConnection, void, NetworkSignature*));
	}
}

void TextChatEngineComponent::ShutdownComponent ()
{
	if (ConnectionManager* localConnections = ConnectionManager::GetConnectionManager())
	{
		localConnections->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, TextChatEngineComponent, HandleNewConnection, void, NetworkSignature*));
	}

	if (ChatHistory.IsValid())
	{
		ChatHistory->Destroy();
		ChatHistory = nullptr;
	}

	if (ChatGui.IsValid())
	{
		ChatGui->Destroy();
		ChatGui = nullptr;
	}

	if (LocalParticipant.IsValid())
	{
		LocalParticipant->Destroy();
		LocalParticipant = nullptr;
	}

	Super::ShutdownComponent();
}

void TextChatEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	if (!bIsServer)
	{
		outDependencies.push_back(GuiEngineComponent::SStaticClass());
	}
}

void TextChatEngineComponent::SetLocalParticipant (ChatParticipant* newLocalParticipant)
{
	if (LocalParticipant.IsValid() && newLocalParticipant != nullptr)
	{
		TextChatExampleLog.Log(LogCategory::LL_Critical, TXT("Multiple local participants detected. A client is only permitted to be a network owner of one Participant instance."));
	}

	LocalParticipant = newLocalParticipant;
}

void TextChatEngineComponent::AddRemoteParticipant (ChatParticipant* newRemoteParticipant)
{
	ContainerUtils::AddUnique(OUT RemoteParticipants, newRemoteParticipant);
}

void TextChatEngineComponent::RemoveRemoteParticipant (ChatParticipant* oldRemoteParticipant)
{
	if (oldRemoteParticipant != nullptr)
	{
		size_t findIdx = ContainerUtils::RemoveItem(OUT RemoteParticipants, oldRemoteParticipant);
		if (findIdx == UINT_INDEX_NONE)
		{
			TextChatExampleLog.Log(LogCategory::LL_Warning, TXT("Unable to find chat participant %s from the RemoteParticipant list."), oldRemoteParticipant->ReadUserName());
		}
	}

	if (!bIsServer && ChatGui.IsValid())
	{
		ChatGui->RefreshUserList();
	}
}

void TextChatEngineComponent::SetChatHistory (Messenger* newChatHistory)
{
	//This should only be executed on the client. The server automatically instantiates the authoritative Chat History in PostInitializeComponent.
	CHECK(!bIsServer)
	if (ChatHistory.IsValid())
	{
		ChatHistory->Destroy();
	}

	ChatHistory = newChatHistory;
}

void TextChatEngineComponent::HandleNewConnection (NetworkSignature* newClient)
{
	if (bIsServer)
	{
		ChatParticipant* newParticipant = ChatParticipant::CreateObject();
		newParticipant->SetOwningClient(newClient);
		newClient->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, TextChatEngineComponent, HandleClientDisconnect, void, NetworkSignature*));
	}
}

void TextChatEngineComponent::HandleClientDisconnect (NetworkSignature* oldClient)
{
	CHECK(bIsServer)

	oldClient->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, TextChatEngineComponent, HandleClientDisconnect, void, NetworkSignature*));
	for (size_t i = 0; i < RemoteParticipants.size(); ++i)
	{
		if (RemoteParticipants.at(i)->GetOwningClient() == oldClient)
		{
			RemoteParticipants.at(i)->Destroy(); //Destroying participant removes from the participant list
			return;
		}
	}
}
TCE_END