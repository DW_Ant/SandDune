/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Messenger.cpp
=====================================================================
*/

#include "ChatParticipant.h"
#include "Messenger.h"
#include "TextChatEngineComponent.h"
#include "TextChatGui.h"

IMPLEMENT_CLASS(SD::TCE::Messenger, SD::Entity)
TCE_BEGIN

Messenger::SMessageEntry::SMessageEntry (ChatParticipant* inAuthor, const DString& inMessage) :
	Author(inAuthor),
	Message(inMessage)
{
	Timestamp.SetToCurrentTime();
}

Messenger::SChannelMessages::SChannelMessages (const DString& inChannelName) :
	ChannelName(inChannelName)
{
	//Noop
}

void Messenger::InitProps ()
{
	Super::InitProps();

	RoleComp = nullptr;
}

void Messenger::BeginObject ()
{
	Super::BeginObject();

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = SDFUNCTION_1PARAM(this, Messenger, HandleGetComponentList, void, std::vector<NetworkComponent*>&);
		RoleComp->OnIsNetOwner = SDFUNCTION_1PARAM(this, Messenger, HandleIsNetOwner, bool, NetworkSignature*);
		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			if (RoleComp->HasAuthority())
			{
				//Alternatively, a developer can rely on ReplicatedVariables to sync on init. This approach how to invoke a function for new clients.
				TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
				CHECK(localChatEngine != nullptr && localChatEngine->GetChatHistory() != nullptr)

				//Gather a list of channels
				std::vector<DString> channels;
				for (const Messenger::SChannelMessages& chatHistory : localChatEngine->GetChatHistory()->ReadChatHistory())
				{
					channels.push_back(chatHistory.ChannelName);
				}

				ClientInitializeChannels(connectedTo, channels);
			}
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		BIND_RPC_1REF_PARAM(NetComp, ClientInitializeChannels, this, Messenger, std::vector<DString>);
		ClientInitializeChannels.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC_1PARAM(NetComp, ServerCreateChannel, this, Messenger, DString);
		ServerCreateChannel.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1PARAM(NetComp, ClientCreateChannel, this, Messenger, DString);
		ClientCreateChannel.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC_3PARAM(NetComp, ServerAddMessage, this, Messenger, RpcPtr<ChatParticipant>, DString, DString);
		ServerAddMessage.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_3PARAM(NetComp, ClientAddMessage, this, Messenger, RpcPtr<ChatParticipant>, DString, DString);
		ClientAddMessage.CanBeExecutedOn = NetworkSignature::NR_Client;
	}

	TextChatEngineComponent* localTextEngine = TextChatEngineComponent::Find();
	if (localTextEngine != nullptr && !localTextEngine->IsServer())
	{
		//The server instantiated this object. Register this to the engine component for external objects to find it.
		localTextEngine->SetChatHistory(this);
	}
}

void Messenger::AddMessage (const DString& channelName, ChatParticipant* author, const DString& msg)
{
	SMessageEntry newMsg(author, msg);
	for (SChannelMessages& channel : ChatHistory)
	{
		if (channel.ChannelName.Compare(channelName, DString::CC_CaseSensitive) == 0)
		{
			channel.Messages.push_back(newMsg);
			return;
		}
	}

	SChannelMessages newChannel(channelName);
	newChannel.Messages.push_back(newMsg);

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr)
	if (TextChatGui* textGui = localChatEngine->GetChatGui())
	{
		textGui->AddChatHistory(channelName, newMsg);
	}
}

void Messenger::RemoveParticipant (ChatParticipant* leavingParticipant)
{
	for (SChannelMessages& channel : ChatHistory)
	{
		for (SMessageEntry& msg : channel.Messages)
		{
			if (msg.Author == leavingParticipant)
			{
				msg.Author = nullptr;
			}
		}
	}
}

bool Messenger::ClientInitializeChannels_Implementation (const std::vector<DString>& newChannels)
{
	ContainerUtils::Empty(OUT ChatHistory);

	for (size_t i = 0; i < newChannels.size(); ++i)
	{
		DString channelName = newChannels.at(i);
		channelName.TrimSpaces();
		if (!channelName.IsEmpty()) //Always validate data coming from the network
		{
			ChatHistory.emplace_back(SChannelMessages(channelName));
		}
	}

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr && !localChatEngine->IsServer())

	if (TextChatGui* chatGui = localChatEngine->GetChatGui())
	{
		chatGui->RefreshChannelList(this);
	}

	return true; //always accept any channel
}

bool Messenger::ServerCreateChannel_Implementation (DString channelName)
{
	channelName.TrimSpaces();
	if (channelName.IsEmpty())
	{
		return false;
	}

	//Ensure this channel does not exist
	for (const SChannelMessages& channel : ChatHistory)
	{
		if (channel.ChannelName.Compare(channelName, DString::CC_IgnoreCase) == 0)
		{
			TextChatExampleLog.Log(LogCategory::LL_Log, TXT("Refusing to create channel \"%s\" since that channel already exists."), channelName);
			return false;
		}
	}

	ChatHistory.emplace_back(SChannelMessages(channelName));

	//Sort alphabetically
	std::sort(ChatHistory.begin(), ChatHistory.end(), [](const SChannelMessages& a, const SChannelMessages& b)
	{
		return (a.ChannelName.Compare(b.ChannelName, DString::CC_CaseSensitive) < 0);
	});

	ClientCreateChannel(BaseRpc::RT_Clients, channelName);
	return true;
}

bool Messenger::ClientCreateChannel_Implementation (DString channelName)
{
	channelName.TrimSpaces();
	if (channelName.IsEmpty())
	{
		return false;
	}

	ChatHistory.emplace_back(SChannelMessages(channelName));

	//Sort alphabetically
	std::sort(ChatHistory.begin(), ChatHistory.end(), [](const SChannelMessages& a, const SChannelMessages& b)
	{
		return (a.ChannelName.Compare(b.ChannelName, DString::CC_CaseSensitive) < 0);
	});

	TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
	CHECK(localChatEngine != nullptr && localChatEngine->GetChatGui() != nullptr)

	localChatEngine->GetChatGui()->RefreshChannelList(this);
	return true;
}

bool Messenger::ServerAddMessage_Implementation (RpcPtr<ChatParticipant> author, DString channelName, DString msg)
{
	if (author.IsNullptr())
	{
		return false; //Each message must have an author
	}

	for (SChannelMessages& channel : ChatHistory)
	{
		if (channel.ChannelName.Compare(channelName, DString::CC_CaseSensitive) == 0)
		{
			channel.Messages.emplace_back(SMessageEntry(author.Get(), msg));
			ClientAddMessage(BaseRpc::RT_Clients, author, channelName, msg);
			return true;
		}
	}

	//Channel not found.
	return false;
}

bool Messenger::ClientAddMessage_Implementation (RpcPtr<ChatParticipant> author, DString channelName, DString msg)
{
	if (author.IsNullptr())
	{
		return false;
	}

	for (SChannelMessages& channel : ChatHistory)
	{
		if (channel.ChannelName.Compare(channelName, DString::CC_CaseSensitive) == 0)
		{
			channel.Messages.emplace_back(SMessageEntry(author.Get(), msg));
			
			TextChatEngineComponent* localChatEngine = TextChatEngineComponent::Find();
			CHECK(localChatEngine != nullptr && localChatEngine->GetChatGui() != nullptr)
			localChatEngine->GetChatGui()->AddChatHistory(channelName, ContainerUtils::GetLast(channel.Messages));
			return true;
		}
	}

	return false;
}

bool Messenger::HandleIsNetOwner (NetworkSignature* remoteClient)
{
	//Everyone is a network owner. This will allow anyone to send RPCs to this Messenger.
	return true;
}

void Messenger::HandleGetComponentList (std::vector<NetworkComponent*>& outNetComps) const
{
	outNetComps.push_back(NetComp.Get());
}
TCE_END