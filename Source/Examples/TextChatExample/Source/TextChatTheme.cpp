/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatTheme.cpp
=====================================================================
*/

#include "TextChatTheme.h"

IMPLEMENT_CLASS(SD::TCE::TextChatTheme, SD::GuiTheme)
TCE_BEGIN

const DString TextChatTheme::CHAT_STYLE_NAME(TXT("CHAT_STYLE"));

void TextChatTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	InitializeTextChatStyle();
}

void TextChatTheme::InitializeTextChatStyle ()
{
	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	CHECK(defaultStyle != nullptr)

	SStyleInfo chatStyle = CreateStyleFrom(*defaultStyle);
	chatStyle.Name = CHAT_STYLE_NAME;

	sf::Color fontColor = sf::Color(59, 53, 44);

	ButtonComponent* buttonTemplate = dynamic_cast<ButtonComponent*>(FindTemplate(&chatStyle, ButtonComponent::SStaticClass()));
	if (buttonTemplate != nullptr)
	{
		if (buttonTemplate->GetCaptionComponent() != nullptr && buttonTemplate->GetCaptionComponent()->GetRenderComponent() != nullptr)
		{
			buttonTemplate->GetCaptionComponent()->GetRenderComponent()->SetFontColor(fontColor);
		}

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(buttonTemplate->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(253, 203, 177));
			colorState->SetDownColor(Color(233, 100, 75));
			colorState->SetDisabledColor(Color(203, 75, 50));
			colorState->SetHoverColor(Color(255, 225, 200));
		}
	}

	FrameComponent* frameTemplate = dynamic_cast<FrameComponent*>(FindTemplate(&chatStyle, FrameComponent::SStaticClass()));
	if (frameTemplate != nullptr)
	{
		frameTemplate->SetBorderThickness(0.f);
		frameTemplate->SetCenterColor(Color(249, 225, 187));

		if (BorderRenderComponent* borderComp = frameTemplate->GetBorderComp())
		{
			borderComp->Destroy();
		}
	}

	LabelComponent* labelTemplate = dynamic_cast<LabelComponent*>(FindTemplate(&chatStyle, LabelComponent::SStaticClass()));
	if (labelTemplate != nullptr && labelTemplate->GetRenderComponent() != nullptr)
	{
		labelTemplate->GetRenderComponent()->SetFontColor(fontColor);
	}

	ListBoxComponent* listTemplate = dynamic_cast<ListBoxComponent*>(FindTemplate(&chatStyle, ListBoxComponent::SStaticClass()));
	if (listTemplate != nullptr)
	{
		listTemplate->SetSelectionColor(Color(227, 208, 159, 64));
		listTemplate->SetHoverColor(Color(253, 239, 177, 48));

		if (FrameComponent* background = listTemplate->GetBackground())
		{
			background->SetCenterColor(Color::INVISIBLE);
			background->SetBorderThickness(0.f);
			if (BorderRenderComponent* borderComp = background->GetBorderComp())
			{
				borderComp->Destroy();
			}
		}

		if (listTemplate->GetItemListText() != nullptr && listTemplate->GetItemListText()->GetRenderComponent() != nullptr)
		{
			listTemplate->GetItemListText()->GetRenderComponent()->SetFontColor(fontColor);
		}
	}

	ScrollbarComponent* scrollbarTemplate = dynamic_cast<ScrollbarComponent*>(FindTemplate(&chatStyle, ScrollbarComponent::SStaticClass()));
	if (scrollbarTemplate != nullptr)
	{
		scrollbarTemplate->SetZoomEnabled(false);
		scrollbarTemplate->SetEnabledThumbColor(Color(247, 158, 106));
		scrollbarTemplate->SetThumbDragColor(Color(227, 138, 96));
		scrollbarTemplate->SetEnabledTrackColor(Color(107, 68, 42));
	}

	TextFieldComponent* textFieldTemplate = dynamic_cast<TextFieldComponent*>(FindTemplate(&chatStyle, TextFieldComponent::SStaticClass()));
	if (textFieldTemplate != nullptr)
	{
		if (FrameComponent* background = textFieldTemplate->GetBackgroundComponent())
		{
			background->SetCenterColor(Color(227, 192, 159));
			if (BorderRenderComponent* borderComp = background->GetBorderComp())
			{
				borderComp->SetBorderTexture(nullptr);
				borderComp->SetBorderColor(Color(235, 200, 165));
			}
		}

		if (TextFieldRenderComponent* renderComp = dynamic_cast<TextFieldRenderComponent*>(textFieldTemplate->GetRenderComponent()))
		{
			renderComp->SetFontColor(fontColor);
			renderComp->CursorColor = fontColor;
			renderComp->SetHighlightColor(Color(230, 134, 50, 150));
		}
	}

	Styles.emplace_back(chatStyle);
}
TCE_END