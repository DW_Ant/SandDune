/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatTheme.h
  The object responsible for determining how the GuiComponents appear for the
  TextChatExample program.
=====================================================================
*/

#pragma once

#include "TextChatExample.h"

TCE_BEGIN
class TextChatTheme : public GuiTheme
{
	DECLARE_CLASS(TextChatTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString CHAT_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeTextChatStyle ();
};
TCE_END