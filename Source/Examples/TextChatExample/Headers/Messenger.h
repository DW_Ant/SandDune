/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Messenger.h
  An Entity that keeps track of each message.
  Only the client instantiates this object to remember which messages belong to which
  channels.

  The server solely instantiates this object to add new messages to clients.
=====================================================================
*/

#pragma once

#include "TextChatExample.h"

TCE_BEGIN
class ChatParticipant;

class Messenger : public Entity
{
	DECLARE_CLASS(Messenger)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SMessageEntry
	{
		ChatParticipant* Author;
		DateTime Timestamp;
		DString Message;

		SMessageEntry (ChatParticipant* inAuthor, const DString& inMessage);
	};

	struct SChannelMessages
	{
		DString ChannelName;
		std::vector<SMessageEntry> Messages;

		SChannelMessages (const DString& inChannelName);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Rpc<const std::vector<DString>&> ClientInitializeChannels;
	Rpc<DString> ServerCreateChannel;
	Rpc<DString> ClientCreateChannel;

	Rpc<RpcPtr<ChatParticipant>, DString, DString> ServerAddMessage;
	Rpc<RpcPtr<ChatParticipant>, DString, DString> ClientAddMessage;

protected:
	/* List of messages that was broadcasted in each channel. */
	std::vector<SChannelMessages> ChatHistory;

	NetworkRoleComponent* RoleComp;
	DPointer<NetworkComponent> NetComp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the specified chat message in the chat history.
	 * If the channel is not found, it'll create a new one.
	 * After adding the message, it'll update the TextChatGui if the current channel is displayed.
	 */
	virtual void AddMessage (const DString& channelName, ChatParticipant* author, const DString& msg);

	/**
	 * Clears out any references to this participant.
	 */
	virtual void RemoveParticipant (ChatParticipant* leavingParticipant);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<SChannelMessages>& ReadChatHistory () const
	{
		return ChatHistory;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Receives the channel list from the server. The client will replace their channel list with these new channels.
	 */
	virtual bool ClientInitializeChannels_Implementation (const std::vector<DString>& newChannels);

	/**
	 * Sends a request to the server to create a channel.
	 */
	virtual bool ServerCreateChannel_Implementation (DString channelName);

	/**
	 * Adds the given channel name to the TextChatGui channel list.
	 */
	virtual bool ClientCreateChannel_Implementation (DString channelName);

	virtual bool ServerAddMessage_Implementation (RpcPtr<ChatParticipant> author, DString channelName, DString msg);
	virtual bool ClientAddMessage_Implementation (RpcPtr<ChatParticipant> author, DString channelName, DString msg);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleIsNetOwner (NetworkSignature* remoteClient);
	virtual void HandleGetComponentList (std::vector<NetworkComponent*>& outNetComps) const;
};
TCE_END