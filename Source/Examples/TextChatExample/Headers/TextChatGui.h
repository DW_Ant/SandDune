/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatGui.h
  An Entity that handles the Gui Components that allows the user to do the following:
  * Connect to and disconnect from a server
  * Create a chat room
  * Send text messages to others within the chat room

  Only clients may instantiate this Entity primarily because the server does not
  have a graphics or gui engine component.
=====================================================================
*/

#pragma once

#include "Messenger.h"
#include "TextChatExample.h"

TCE_BEGIN
class ChatParticipant;

class TextChatGui : public GuiEntity
{
	DECLARE_CLASS(TextChatGui)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Components related to the top frame. */
	FrameComponent* TopFrame;
	LabelComponent* StatusText;
	LabelComponent* DestinationPrompt;
	TextFieldComponent* ServerDestinationField;
	ButtonComponent* ConnectButton;
	LabelComponent* PlayerNamePrompt;
	TextFieldComponent* PlayerNameField;

	/* Components related to the left frame (channels). */
	FrameComponent* ChannelFrame;
	ScrollbarComponent* ChannelScrollbar;
	GuiEntity* ChannelEntity;
	ListBoxComponent* ChannelList;
	ButtonComponent* CreateChannelButton;

	/* Components related to the center frame (chat). */
	FrameComponent* ChatFrame;
	ScrollbarComponent* ChatScrollbar;
	GuiEntity* ChatEntity;
	LabelComponent* ChatHistory;
	TextFieldComponent* ChatField;

	/* Components related to the right field (participant list). */
	FrameComponent* ParticipantFrame;
	ScrollbarComponent* ParticipantScrollbar;
	GuiEntity* ParticipantEntity;
	LabelComponent* ParticipantList;

private:
	sf::Color OriginalDestinationColor;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * If the selected channel is the given channel name, then this function will add this message to
	 * the chat history.
	 */
	virtual void AddChatHistory (const DString& channelName, const Messenger::SMessageEntry& newMsg);

	/**
	 * Refreshes the channel list to reflect the given channel history's channel list.
	 */
	virtual void RefreshChannelList (Messenger* history);

	/**
	 * Looks at each client, and updates the participant list component to reflect the user names.
	 */
	virtual void RefreshUserList ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual DString GetLocalUserName () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Changes the state of the UI to enable/disable components to be in 'offline mode'
	 */
	virtual void SetToOfflineMode ();

	/**
	 * Changes the state of the UI to enable/disable components to be in 'online mode'.
	 */
	virtual void SetToOnlineMode ();

	/**
	 * Generates a string based on the message entry.
	 */
	virtual DString MessageEntryToString (const Messenger::SMessageEntry& msg) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleConnectReleased (ButtonComponent* uiComp);
	virtual void HandleDestinationReturn (TextFieldComponent* uiComp);
	virtual bool HandleDestinationEdit (const sf::Event& evnt);
	virtual void HandlePlayerNameReturn (TextFieldComponent* uiComp);

	virtual void HandleChannelSelected (Int selectedItemIdx);
	virtual void HandleCreateChannelReleased (ButtonComponent* uiComp);

	virtual void HandleChatReturn (TextFieldComponent* uiComp);

	virtual void HandleConnectionAccepted (NetworkSignature* signature);
	virtual void HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason);

	virtual void HandleServerDisconnect (NetworkSignature* serverSignature);
};
TCE_END