/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatExample.h
  Contains important file includes and definitions for the TextChatExample.

  This program is a feature incomplete text chat application that allows
  two or more users to communicate in a chat room.
  The purpose of this example is to provide a simple case how a
  network application may invoke RPC's between processes.

  This application uses a centralized network architecture where the server is
  responsible for relaying messages within the chat room while the clients can only
  communicate with each other through the server.

  This application also considers the network permissions.
  Clients cannot create Entities. They can only send RPCs that may instantiate Entities
  on the server.

  Clients can call RPC's regardless if they're a net owner or not.

  There are no access controls. The server will accept any connection.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#include "Engine\Network\Headers\NetworkClasses.h"

#define TCE_BEGIN namespace SD { namespace TCE {
#define TCE_END } }

TCE_BEGIN
extern LogCategory TextChatExampleLog;
TCE_END