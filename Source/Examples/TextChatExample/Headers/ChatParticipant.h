/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ChatParticipant.h
  An Entity that represents a user that can add and receive text messages.
=====================================================================
*/

#pragma once

#include "TextChatExample.h"

#ifdef GetUserName
//Remove intrusive MS preprocessor definition
#undef GetUserName
#endif

TCE_BEGIN
class ChatParticipant : public Entity
{
	DECLARE_CLASS(ChatParticipant)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The name that is displayed in the chat room. */
	DString UserName;

	/* The signature that is responsible for this participant. This is only assigned on the server side since clients
	are not aware of other client's signatures. */
	DPointer<NetworkSignature> OwningClient;

	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;

	Rpc<DString> ServerSetUserName;
	Rpc<DString> ClientSetUserName;

private:
	/* Becomes true after this process receives the UserName for the first time (bypasses name conflict comparisons). */
	bool bReceivedInitUserName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetUserName (DString newUserName);
	virtual void SetOwningClient (NetworkSignature* newOwningClient);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetUserName () const
	{
		return UserName;
	}

	inline const DString& ReadUserName () const
	{
		return UserName;
	}

	inline NetworkSignature* GetOwningClient () const
	{
		return OwningClient.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Attempts to change this user's name to the specified name.
	 * The server may reject the name if it's already taken.
	 */
	virtual bool ServerSetUserName_Implementation (DString newUserName);

	/**
	 * The server is notifying the client that their name is the given value.
	 */
	virtual bool ClientSetUserName_Implementation (DString newUserName);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGetComponentList (std::vector<NetworkComponent*>& outNetComponents) const;
	virtual bool HandleIsNetOwner (NetworkSignature* remoteClient) const;
	virtual void HandleDisconnected (NetworkSignature* client);
};
TCE_END