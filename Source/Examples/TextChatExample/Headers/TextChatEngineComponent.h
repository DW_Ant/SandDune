/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextChatEngineComponent.h
  An engine component responsible for managing Entities that conducts the flow of this example.
=====================================================================
*/

#pragma once

#include "TextChatExample.h"

TCE_BEGIN
class ChatParticipant;
class Messenger;
class TextChatGui;

class TextChatEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(TextChatEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then this engine component is running in server mode.
	When in server mode, the application will not have graphics and gui engine components.
	It will also have authority to transfer messages between clients. */
	bool bIsServer;
	
	/* The participant that represents the local user. */
	DPointer<ChatParticipant> LocalParticipant;

	/* List of participants that are connected to the server. */
	std::vector<ChatParticipant*> RemoteParticipants;

	DPointer<TextChatGui> ChatGui;

	DPointer<Messenger> ChatHistory;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TextChatEngineComponent ();
	TextChatEngineComponent (bool bInServer);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetLocalParticipant (ChatParticipant* newLocalParticipant);
	virtual void AddRemoteParticipant (ChatParticipant* newRemoteParticipant);
	virtual void RemoveRemoteParticipant (ChatParticipant* oldRemoteParticipant);
	virtual void SetChatHistory (Messenger* newChatHistory);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsServer () const
	{
		return bIsServer;
	}

	inline ChatParticipant* GetLocalParticipant () const
	{
		return LocalParticipant.Get();
	}

	inline const std::vector<ChatParticipant*>& ReadRemoteParticipants () const
	{
		return RemoteParticipants;
	}

	inline TextChatGui* GetChatGui () const
	{
		return ChatGui.Get();
	}

	inline Messenger* GetChatHistory () const
	{
		return ChatHistory.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNewConnection (NetworkSignature* newClient);
	virtual void HandleClientDisconnect (NetworkSignature* oldClient);
};
TCE_END