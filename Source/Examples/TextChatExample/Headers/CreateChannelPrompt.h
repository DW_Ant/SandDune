/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CreateChannelPrompt.h
  This Entity asks the user for the channel name before creating one.
  A GuiEntity that takes priority over other GuiEntities.
=====================================================================
*/

#pragma once

#include "TextChatExample.h"

TCE_BEGIN
class CreateChannelPrompt : public GuiEntity
{
	DECLARE_CLASS(CreateChannelPrompt)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	FrameComponent* Blackout;
	FrameComponent* PromptFrame;
	LabelComponent* PromptLabel;
	TextFieldComponent* ChannelNameField;
	ButtonComponent* AcceptButton;
	ButtonComponent* CancelButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void CreateChannel ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleChannelNameReturn (TextFieldComponent* uiComp);
	virtual void HandleAcceptReleased (ButtonComponent* uiComp);
	virtual void HandleCancelReleased (ButtonComponent* uiComp);
};
TCE_END