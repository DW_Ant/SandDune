/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClientHackerTestObject.cpp
=====================================================================
*/

#include "ClientHackerPanel.h"
#include "ClientHackerTestObject.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubHud.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::ClientHackerTestObject, SD::Entity)
NC_BEGIN

void ClientHackerTestObject::InitProps ()
{
	Super::InitProps();

	Input = nullptr;
	RoleComp = nullptr;
	NetComp = nullptr;
	FloodCounter = 1;
}

void ClientHackerTestObject::BeginObject ()
{
	Super::BeginObject();

	NetworkSignature* localSig = NetworkSignature::GetLocalSignature();
	if (localSig != nullptr && localSig->HasRole(NetworkSignature::NR_Client))
	{
		Input = InputComponent::CreateObject();
		if (AddComponent(Input))
		{
			Input->SetInputPriority(30); //Low priority to ensure UI components processes the input before this keybind (eg: user entering 3 in a text field takes priority)
			Input->AddKeybind(sf::Keyboard::Num3, false, SDFUNCTION(this, ClientHackerTestObject, HandleToggleHackerPanel, bool));
		}

		NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
		CHECK(localNightclub != nullptr)
		if (NightclubHud* hud = localNightclub->GetHud())
		{
			hud->RevealHackerControls();
		}
	}

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = [&](std::vector<NetworkComponent*>& outNetComponents)
		{
			outNetComponents.push_back(NetComp);
		};

		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			NetworkSignature* lambdaLocalSig = NetworkSignature::GetLocalSignature();
			if (lambdaLocalSig->HasRole(NetworkSignature::NR_Client))
			{
				NightclubEngineComponent* lambdaNightclub = NightclubEngineComponent::Find();
				CHECK(lambdaNightclub != nullptr)
				
				if (lambdaNightclub->GetHackerTestObject() != nullptr && lambdaNightclub->GetHackerTestObject() != this)
				{
					lambdaNightclub->GetHackerTestObject()->Destroy();
				}

				lambdaNightclub->SetHackerTestObject(this);
			}
		};

		//Relevant to all clients (not master servers)
		RoleComp->OnIsNetRelevant = [&](NetworkSignature* remoteClient)
		{
			return remoteClient->HasRole(NetworkSignature::NR_Client);
		};

		//Give all clients net ownership to permit them to call this object's RPC's
		RoleComp->OnIsNetOwner = [&](NetworkSignature* remoteClient)
		{
			return true;
		};

		RoleComp->OnLoseRelevance = [&]()
		{
			NightclubEngineComponent* lambdaNightclub = NightclubEngineComponent::Find();
			if (lambdaNightclub != nullptr && lambdaNightclub->GetHackerTestObject() == this)
			{
				lambdaNightclub->SetHackerTestObject(nullptr);
			}
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		BIND_RPC(NetComp, ServerIncrementFloodCounter, this, ClientHackerTestObject);
		ServerIncrementFloodCounter.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1PARAM(NetComp, ClientReceiveFloodCounter, this, ClientHackerTestObject, Int);
		ClientReceiveFloodCounter.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC_1PARAM(NetComp, ServerRunReferenceCheck, this, ClientHackerTestObject, RpcPtr<Player>);
		ServerRunReferenceCheck.CanBeExecutedOn = NetworkSignature::NR_Server;
	}
}

void ClientHackerTestObject::Destroyed ()
{
	if (HackerPanel.IsValid())
	{
		HackerPanel->Destroy();
	}

	Super::Destroyed();
}

bool ClientHackerTestObject::ServerIncrementFloodCounter_Implementation ()
{
	if ((FloodCounter % 10000) == 0)
	{
		NightclubLog.Log(LogCategory::LL_Log, TXT("Server is sending flood counter back to client: %s"), FloodCounter);
	}

	ClientReceiveFloodCounter(ServerIncrementFloodCounter.GetInstigatedFrom(), FloodCounter);
	FloodCounter++;
	return true;
}

bool ClientHackerTestObject::ClientReceiveFloodCounter_Implementation (Int counter)
{
	if ((counter % 10000) == 0)
	{
		NightclubLog.Log(LogCategory::LL_Log, TXT("Client received flood counter: %s"), counter);
	}

	return true;
}

bool ClientHackerTestObject::ServerRunReferenceCheck_Implementation (RpcPtr<Player> player)
{
	if (player.IsNullptr())
	{
		return false;
	}

	if (ServerRunReferenceCheck.GetInstigatedFrom() != player->GetNetOwner())
	{
		DString instigatorName = TXT("Unknown Client");
		if (NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(ServerRunReferenceCheck.GetInstigatedFrom()->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
		{
			instigatorName = clientId->GetPlayerName();
		}

		NightclubLog.Log(LogCategory::LL_Critical, TXT("Some how %s was able to reference player %s. This application's permissions are configured so that clients are not allowed to reference Entities they're not net owners over."), instigatorName, player->ReadPlayerName());
		return false;
	}
	else
	{
		NightclubLog.Log(LogCategory::LL_Log, TXT("%s is permitted to reference their own player object."), player->ReadPlayerName());
	}

	return true;
}

bool ClientHackerTestObject::HandleToggleHackerPanel ()
{
	if (HackerPanel.IsNullptr())
	{
		HackerPanel = ClientHackerPanel::CreateObject();
		HackerPanel->RegisterToMainWindow(true, true, 75);
		HackerPanel->SetPosition(Vector2::ZERO_VECTOR);
		HackerPanel->SetSize(Vector2(0.5f, 1.f));
		HackerPanel->SetDepth(5.f); //Drawn behind other popups, but drawn above the hud.

		GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
		NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
		CHECK(localGraphics != nullptr && localNightclub != nullptr)

		if (localGraphics->GetPrimaryWindow() != nullptr)
		{
			if (TopDownCamera* cam = localNightclub->GetCamera())
			{
				//Shift the camera to center the player since the client hacker panel will cover one half of the screen. Destroying the hacker panel will automatically restore the camera.
				cam->EditTranslation().X = (localGraphics->GetPrimaryWindow()->GetWindowSize().X * -0.25f) / cam->GetScalarProjection();
			}
		}
	}
	else
	{
		HackerPanel->Destroy();
	}

	return true;
}
NC_END