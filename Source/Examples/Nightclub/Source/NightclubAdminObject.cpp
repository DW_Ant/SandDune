/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubAdminObject.cpp
=====================================================================
*/

#include "BanListAccessControl.h"
#include "NightclubAdminObject.h"
#include "NightclubAdminPanel.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubHud.h"

IMPLEMENT_CLASS(SD::NC::NightclubAdminObject, SD::Entity)
NC_BEGIN

void NightclubAdminObject::InitProps ()
{
	Super::InitProps();

	RoleComp = nullptr;
	NetComp = nullptr;

	RequestTimeoutTime = 10.f;
	PlayerListRequestTime = -RequestTimeoutTime;
	BanListRequestTime = -RequestTimeoutTime;
	ConnectedClientRequestTime = -RequestTimeoutTime;
}

void NightclubAdminObject::BeginObject ()
{
	Super::BeginObject();

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = [&](std::vector<NetworkComponent*>& outNetComponents)
		{
			outNetComponents.push_back(NetComp);
		};

		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
			if (localSignature != nullptr && localSignature->HasRole(NetworkSignature::NR_Client))
			{
				//Clients need to spawn a InputComponent. Servers and Master Servers don't need to.
				SetupInput();
			}
		};

		RoleComp->OnIsNetRelevant = [&](NetworkSignature* remoteClient)
		{
			//It's always relevant to servers and master servers
			if (remoteClient->HasRole(NetworkSignature::NR_Server | NetworkSignature::NR_MasterServer))
			{
				return true;
			}

			//If it's a client, then this is only relevant if their name is equal to "Admin"
			if (remoteClient->HasRole(NetworkSignature::NR_Client))
			{
				if (NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(remoteClient->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
				{
					if (identifier->ReadPlayerName().Compare(TXT("Admin"), DString::CC_IgnoreCase) == 0)
					{
						return true;
					}
				}
			}

			return false;
		};

		RoleComp->OnIsNetOwner = [&](NetworkSignature* remoteClient)
		{
			return true;
		};

		RoleComp->OnLoseRelevance = [&]()
		{
			//If this object is one of the pending request list, remove it from the vector to avoid dangling pointer.
			//This will prevent crashes when a server suddenly closes while the master server is processing its request.
			NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
			if (localNightclub != nullptr)
			{
				if (NightclubAdminObject* authorityObj = localNightclub->GetAdminObject())
				{
					ContainerUtils::RemoveItems(OUT authorityObj->PlayerListObjRequesters, this);
				}
			}
		};

		TickComponent* relevanceTick = RoleComp->GetRelevanceTick();
		if (relevanceTick != nullptr)
		{
			//It's not imperative this object propagates as quickly as possible. Set to a high interval
			relevanceTick->SetTickInterval(5.f);
		}
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		BIND_RPC(NetComp, ServerRequestPlayerList, this, NightclubAdminObject);
		ServerRequestPlayerList.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC(NetComp, MasterServerRequestPlayerList, this, NightclubAdminObject);
		MasterServerRequestPlayerList.CanBeExecutedOn = NetworkSignature::NR_MasterServer;

		BIND_RPC(NetComp, ServerGetConnectedClients, this, NightclubAdminObject);
		ServerGetConnectedClients.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, MasterServerPopulatePlayerList, this, NightclubAdminObject, std::vector<DString>);
		MasterServerPopulatePlayerList.CanBeExecutedOn = NetworkSignature::NR_MasterServer;

		BIND_RPC_1REF_PARAM(NetComp, ServerPopulatePlayerList, this, NightclubAdminObject, std::vector<DString>);
		ServerPopulatePlayerList.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, ClientPopulatePlayerList, this, NightclubAdminObject, std::vector<DString>);
		ClientPopulatePlayerList.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC_1REF_PARAM(NetComp, ServerBanPlayer, this, NightclubAdminObject, DString);
		ServerBanPlayer.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, MasterServerBanPlayer, this, NightclubAdminObject, DString);
		MasterServerBanPlayer.CanBeExecutedOn = NetworkSignature::NR_MasterServer;

		BIND_RPC_1REF_PARAM(NetComp, ServerDisconnectPlayer, this, NightclubAdminObject, DString);
		ServerDisconnectPlayer.CanBeExecutedFrom = NetworkSignature::NR_MasterServer;
		ServerDisconnectPlayer.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, ServerUnbanPlayer, this, NightclubAdminObject, DString);
		ServerUnbanPlayer.CanBeExecutedOn = NetworkSignature::NR_Server | NetworkSignature::NR_MasterServer;

		BIND_RPC(NetComp, ServerRequestBanList, this, NightclubAdminObject);
		ServerRequestBanList.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC(NetComp, MasterServerRequestBanList, this, NightclubAdminObject);
		MasterServerRequestBanList.CanBeExecutedOn = NetworkSignature::NR_MasterServer;

		BIND_RPC_1REF_PARAM(NetComp, ServerReceiveBanList, this, NightclubAdminObject, std::vector<DString>);
		ServerReceiveBanList.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, ClientReceiveBanList, this, NightclubAdminObject, std::vector<DString>);
		ClientReceiveBanList.CanBeExecutedOn = NetworkSignature::NR_Client;
	}
}

void NightclubAdminObject::Destroyed ()
{
	if (Panel.IsValid())
	{
		Panel->Destroy();
	}

	Super::Destroyed();
}

void NightclubAdminObject::SetupInput ()
{
	if (FindComponent(InputComponent::SStaticClass(), false))
	{
		return;
	}

	InputComponent* input = InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->AddKeybind(sf::Keyboard::Tab, false, SDFUNCTION(this, NightclubAdminObject, HandleActivatePanel, bool));
	}

	//Notify the hud to display controls for this object.
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetHud() != nullptr)
	{
		localNightclub->GetHud()->RevealAdminControls();
	}
}

bool NightclubAdminObject::ServerRequestPlayerList_Implementation ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	PlayerListRequesters.push_back(ServerRequestPlayerList.GetInstigatedFrom());
	if (localEngine->GetElapsedTime() - PlayerListRequestTime < RequestTimeoutTime)
	{
		//A request is already on its way. Simply add the requester to the list.
		return true;
	}

	MasterServerRequestPlayerList(BaseRpc::RT_MasterServers);
	PlayerListRequestTime = localEngine->GetElapsedTime();
	return true;
}

bool NightclubAdminObject::MasterServerRequestPlayerList_Implementation ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	/*
	This is a tricky topic.
	Since there can only be on Network Signature that is authoritative over an Entity, network Entities can only be propagated to immediate connections.
	This means a master server object cannot be propagated to clients, and servers cannot propagate objects to other servers.
	Therefore, both the master server and servers instantiate their own AdminObjects.
	Because of this, the master server cannot directly call ServerGetconnectedClients since 'this' doesn't exist on other servers.
	Instead the master server will refer to its own AdminObject to send the request.
	*/
	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr && localNightEngine->GetAdminObject() != nullptr)
	NightclubAdminObject* authAdminObj = localNightEngine->GetAdminObject();

	ContainerUtils::AddUnique(authAdminObj->PlayerListObjRequesters, this);
	if (localEngine->GetElapsedTime() - authAdminObj->PlayerListRequestTime < authAdminObj->RequestTimeoutTime)
	{
		return true;
	}

	if (localEngine->GetElapsedTime() - authAdminObj->ConnectedClientRequestTime < authAdminObj->RequestTimeoutTime)
	{
		return true; //A request is already processing
	}

	ContainerUtils::Empty(OUT authAdminObj->PendingConnectedClientResponses);
	authAdminObj->ConnectedClientRequestTime = localEngine->GetElapsedTime();
	authAdminObj->PlayerListRequestTime = localEngine->GetElapsedTime();

	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		//If it's a server, and if it's not the one that originally sent the request...
		if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Server) && iter.GetSelectedSignature() != MasterServerRequestPlayerList.GetInstigatedFrom())
		{
			if (authAdminObj->ServerGetConnectedClients(iter.GetSelectedSignature()))
			{
				authAdminObj->PendingConnectedClientResponses.push_back(iter.GetSelectedSignature());
			}
		}
	}

	if (ContainerUtils::IsEmpty(authAdminObj->PendingConnectedClientResponses))
	{
		//There aren't any other servers other than the one that sent the request. Immediately send a response of an empty vector.
		ServerPopulatePlayerList(MasterServerRequestPlayerList.GetInstigatedFrom(), {});
		ContainerUtils::Empty(OUT authAdminObj->PlayerListObjRequesters);
		authAdminObj->ConnectedClientRequestTime = -authAdminObj->RequestTimeoutTime;
		authAdminObj->PlayerListRequestTime = -authAdminObj->RequestTimeoutTime;
	}
	
	return true;
}

bool NightclubAdminObject::ServerGetConnectedClients_Implementation ()
{
	std::vector<DString> playerNames;
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Client))
		{
			if (NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(iter.GetSelectedSignature()->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
			{
				playerNames.push_back(clientId->ReadPlayerName());
			}
		}
	}

	MasterServerPopulatePlayerList(ServerGetConnectedClients.GetInstigatedFrom(), playerNames);
	return true;
}

bool NightclubAdminObject::MasterServerPopulatePlayerList_Implementation (const std::vector<DString>& playerNames)
{
	//Ensure the server is one of the requests the master server is waiting for.
	bool bFound = false;
	for (size_t i = 0; i < PendingConnectedClientResponses.size(); ++i)
	{
		if (PendingConnectedClientResponses.at(i) == MasterServerPopulatePlayerList.GetInstigatedFrom())
		{
			bFound = true;
			PendingConnectedClientResponses.erase(PendingConnectedClientResponses.begin() + i);
			break;
		}
	}

	if (!bFound)
	{
		//The server is not authorized to send a response since the master server didn't send a request.
		return false;
	}

	for (const DString& playerName : playerNames)
	{
		PlayersInNetwork.push_back(playerName);
	}

	//The last response is sent. Send the collection of names back to the requesters.
	if (ContainerUtils::IsEmpty(PendingConnectedClientResponses))
	{
		for (NightclubAdminObject* adminObj : PlayerListObjRequesters)
		{
			//Even though it's broadcasting to all servers, the adminObj instance should only hold a reference to one server.
			adminObj->ServerPopulatePlayerList(BaseRpc::RT_Servers, PlayersInNetwork);
		}

		ContainerUtils::Empty(OUT PlayersInNetwork);
		ContainerUtils::Empty(OUT PlayerListObjRequesters);
		ConnectedClientRequestTime = -RequestTimeoutTime;
		PlayerListRequestTime = -RequestTimeoutTime;
	}

	return true;
}

bool NightclubAdminObject::ServerPopulatePlayerList_Implementation (const std::vector<DString>& playerNames)
{
	if (ContainerUtils::IsEmpty(PlayerListRequesters))
	{
		//No one to send the data to.
		PlayerListRequestTime = -RequestTimeoutTime;
		return false;
	}

	std::vector<DString> responses;

	//Add all clients connected to this server to the response list (since the master server wouldn't have queried the instigator for its player list to save bandwidth).
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Client))
		{
			if (NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(iter.GetSelectedSignature()->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
			{
				responses.push_back(clientId->ReadPlayerName());
			}
		}
	}

	for (const DString& playerName : playerNames)
	{
		responses.push_back(playerName);
	}

	//Send the response back to the requesters
	for (NetworkSignature* requester : PlayerListRequesters)
	{
		ClientPopulatePlayerList(requester, responses);
	}

	ContainerUtils::Empty(OUT PlayerListRequesters);
	PlayerListRequestTime = -RequestTimeoutTime;
	return true;
}

bool NightclubAdminObject::ClientPopulatePlayerList_Implementation (const std::vector<DString>& playerNames)
{
	if (Panel.IsValid())
	{
		Panel->PopulatePlayerListing(playerNames);
	}

	return true;
}

bool NightclubAdminObject::ServerBanPlayer_Implementation (const DString& bannedPlayer)
{
	if (bannedPlayer.IsEmpty())
	{
		return false;
	}

	//Simply relay to the master server
	MasterServerBanPlayer(BaseRpc::RT_MasterServers, bannedPlayer);
	return true;
}

bool NightclubAdminObject::MasterServerBanPlayer_Implementation (const DString& bannedPlayer)
{
	if (bannedPlayer.IsEmpty())
	{
		return false;
	}

	//Add banned player to the list
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	for (AccessControl* ctrl : localNetEngine->EditAccessControls())
	{
		if (BanListAccessControl* banCtrl = dynamic_cast<BanListAccessControl*>(ctrl))
		{
			banCtrl->AddBan(bannedPlayer);
			break;
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr && localNightclub->GetAdminObject() != nullptr)

	//Notify all servers to kick any players matching the name
	localNightclub->GetAdminObject()->ServerDisconnectPlayer(BaseRpc::RT_Servers, bannedPlayer);
	return true;
}

bool NightclubAdminObject::ServerDisconnectPlayer_Implementation (const DString& playerName)
{
	if (playerName.IsEmpty())
	{
		return false;
	}

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
		{
			if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Client))
			{
				if (NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(iter.GetSelectedSignature()->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
				{
					if (clientId->ReadPlayerName().Compare(playerName, DString::CC_CaseSensitive) == 0)
					{
						connections->Disconnect(iter.GetSelectedSignature(), true);
						break;
					}
				}
			}
		}
	}

	return true;
}

bool NightclubAdminObject::ServerUnbanPlayer_Implementation (const DString& playerName)
{
	if (playerName.IsEmpty())
	{
		return false;
	}

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (!localSignature->HasRole(NetworkSignature::NR_MasterServer))
	{
		//Simply redirect to the master server since that instance manages the ban list.
		ServerUnbanPlayer(BaseRpc::RT_MasterServers, playerName);
		return true;
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	for (AccessControl* ctrl : localNetEngine->EditAccessControls())
	{
		if (BanListAccessControl* banCtrl = dynamic_cast<BanListAccessControl*>(ctrl))
		{
			banCtrl->RemoveBan(playerName);
			break;
		}
	}

	return true;
}

bool NightclubAdminObject::ServerRequestBanList_Implementation ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	BannedListRequesters.push_back(ServerRequestBanList.GetInstigatedFrom());
	if (localEngine->GetElapsedTime() - BanListRequestTime < RequestTimeoutTime)
	{
		return true;
	}

	MasterServerRequestBanList(BaseRpc::RT_MasterServers);
	BanListRequestTime = localEngine->GetElapsedTime();
	return true;
}

bool NightclubAdminObject::MasterServerRequestBanList_Implementation ()
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)

	std::vector<DString> bannedPlayers;
	for (AccessControl* ctrl : localNetEngine->EditAccessControls())
	{
		if (BanListAccessControl* banCtrl = dynamic_cast<BanListAccessControl*>(ctrl))
		{
			bannedPlayers = banCtrl->ReadBannedPlayers();
			break;
		}
	}

	ServerReceiveBanList(MasterServerRequestBanList.GetInstigatedFrom(), bannedPlayers);
	return true;
}

bool NightclubAdminObject::ServerReceiveBanList_Implementation (const std::vector<DString>& bannedPlayers)
{
	for (NetworkSignature* requester : BannedListRequesters)
	{
		ClientReceiveBanList(requester, bannedPlayers);
	}

	ContainerUtils::Empty(OUT BannedListRequesters);
	BanListRequestTime = -RequestTimeoutTime;
	return true;
}

bool NightclubAdminObject::ClientReceiveBanList_Implementation (const std::vector<DString>& bannedPlayers)
{
	if (Panel.IsValid())
	{
		Panel->PopulateBanList(bannedPlayers);
	}

	return true;
}

bool NightclubAdminObject::HandleActivatePanel ()
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (!localSignature->HasRole(NetworkSignature::NR_Client))
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("nightclubAdminObject::HandleActivatePanel is only supported on clients. The server and master server will not open the admin panel."));
		return false;
	}
	
	if (Panel.IsNullptr())
	{
		Panel = NightclubAdminPanel::CreateObject();
		Panel->SetAdminObj(this);
		Panel->SetPosition(Vector2(0.25f, 0.125f));
		Panel->SetSize(Vector2(0.5f, 0.75f));
		Panel->RegisterToMainWindow();

		//Request data to populate player list and ban list in the panel.
		ServerRequestPlayerList(BaseRpc::RT_Servers);
		ServerRequestBanList(BaseRpc::RT_Servers);

		return true;
	}

	return false;
}
NC_END