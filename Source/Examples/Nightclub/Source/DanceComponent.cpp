/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DanceComponent.cpp
=====================================================================
*/

#include "DanceComponent.h"

IMPLEMENT_CLASS(SD::NC::DanceComponent, SD::EntityComponent)
NC_BEGIN

void DanceComponent::InitProps ()
{
	Super::InitProps();

	bEnabled = true;
	JumpInterval = 0.15f;
	JumpHeight = 96.f;
	Tick = nullptr;
	OwnerTransform = nullptr;

	LocalEngine = nullptr;
}

void DanceComponent::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(Tick))
	{
		Tick->SetTicking(false);
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, DanceComponent, HandleTick, void, Float));

		LocalEngine = Engine::FindEngine();
		CHECK(LocalEngine != nullptr)
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		NetComp->AddReplicatedVariable<Bool>(&bEnabled, [&](ReplicatedVariable<Bool>& outInitRepVar)
		{
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = [&]()
			{
				SetEnabled(bEnabled);
			};
		});

		BIND_RPC_1PARAM(NetComp, ServerSetDancing, this, DanceComponent, Bool);
		ServerSetDancing.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1PARAM(NetComp, ClientSetDancing, this, DanceComponent, Bool);
		ClientSetDancing.CanBeExecutedOn = NetworkSignature::NR_Client;
	}
}

bool DanceComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	return (Super::CanBeAttachedTo(ownerCandidate) && (dynamic_cast<SceneTransform*>(ownerCandidate) != nullptr));
}

void DanceComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwnerTransform = dynamic_cast<SceneTransform*>(newOwner);
	CHECK(OwnerTransform != nullptr)
}

void DanceComponent::ComponentDetached ()
{
	OwnerTransform = nullptr;

	Super::ComponentDetached();
}

void DanceComponent::ClientHackGrantRepVarWritePermissions ()
{
	if (NetComp.IsValid())
	{
		for (BaseReplicatedVariable* var : NetComp->ReadReplicatedVariables())
		{
			var->WritePermissions |= NetworkSignature::NR_Client;
		}
	}
}

void DanceComponent::SetJumpInterval (Float newJumpInterval)
{
	JumpInterval = Utils::Max<Float>(newJumpInterval, 0.01f);
}

void DanceComponent::SetJumpHeight (Float newJumpHeight)
{
	JumpHeight = newJumpHeight;
}

void DanceComponent::SetEnabled (bool bNewEnabled)
{
	if (bEnabled != bNewEnabled)
	{
		bEnabled = bNewEnabled;
		if (!bEnabled && IsDancing())
		{
			SetDancing(false);
		}

		if (OnEnableChanged.IsBounded())
		{
			OnEnableChanged(bEnabled);
		}
	}
}

void DanceComponent::SetDancing (bool bNewDancing)
{
	if (!bEnabled)
	{
		bNewDancing = false;
	}

	if (Tick != nullptr)
	{
		Tick->SetTicking(bNewDancing);
		if (!bNewDancing && OwnerTransform != nullptr)
		{
			OwnerTransform->SetTranslation(Vector3::ZERO_VECTOR);
		}
	}
}

bool DanceComponent::ServerSetDancing_Implementation (Bool bNewDancing)
{
	if (!bEnabled)
	{
		bNewDancing = false;
	}

	ClientSetDancing(BaseRpc::RT_Clients, bNewDancing);
	return true;
}

bool DanceComponent::ClientSetDancing_Implementation (Bool bNewDancing)
{
	SetDancing(bNewDancing);
	return true;
}

void DanceComponent::HandleTick (Float deltaSec)
{
	if (LocalEngine != nullptr && OwnerTransform != nullptr)
	{
		Float danceResult = std::sin(((1 / JumpInterval) * LocalEngine->GetElapsedTime()).Value) * JumpHeight;

		//Stop the entity from dancing "low". Instead they would pause for a bit.
		if (danceResult < 0.f)
		{
			danceResult = 0.f;
		}

		OwnerTransform->EditTranslation().Y = -danceResult;
	}
}
NC_END