/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MasterServer.cpp
=====================================================================
*/

#include "DanceFloorMap.h"
#include "MasterConnection.h"
#include "MasterServer.h"

IMPLEMENT_CLASS(SD::NC::MasterServer, SD::Object)
NC_BEGIN

MasterServer::SServerInstance::SServerInstance (ProcessInfo* inServerProcess, const DClass* inMap, unsigned short inPort) :
	ServerProcess(inServerProcess),
	Signature(nullptr),
	Map(inMap),
	Port(inPort)
{
	//Noop
}

void MasterServer::InitProps ()
{
	Super::InitProps();

	Communicator = nullptr;
}

void MasterServer::BeginObject ()
{
	Super::BeginObject();

	Communicator = MasterConnection::CreateObject();

	DString exeLocation;
	DString exeName;
	OS_GetProcessLocation(OUT exeLocation, OUT exeName);
	ExecFile = FileAttributes(exeLocation, exeName);

	//Launch the dance floor map (since that's the first zone new players will connect to).
	LaunchServer(DanceFloorMap::SStaticClass(), ConnectionManager::DP_MasterServer + 1);
}

void MasterServer::Destroyed ()
{
	if (Communicator.IsValid())
	{
		Communicator->Destroy();
		Communicator = nullptr;
	}

	for (size_t i = 0; i < ServerInstances.size(); ++i)
	{
		if (ServerInstances.at(i).Signature != nullptr)
		{
			ServerInstances.at(i).Signature->OnDisconnected.UnregisterHandler(SDFUNCTION_1PARAM(this, MasterServer, HandleDisconnect, void, NetworkSignature*));
		}

		OS_KillProcess(*ServerInstances.at(i).ServerProcess);
		delete ServerInstances.at(i).ServerProcess;
	}
	ContainerUtils::Empty(OUT ServerInstances);

	Super::Destroyed();
}

bool MasterServer::LaunchServer (const DClass* map, unsigned short serverPort)
{
	LaunchProcessOptions launchOptions(ExecFile);
	launchOptions.bChildProcess = true;
	DString mapName = map->GetClassNameWithoutNamespace();
	launchOptions.CmdLineArgs = DString::CreateFormattedString(TXT("map=%s port=%s -log -server"), mapName, Int(serverPort));

	ProcessInfo* serverProcess = new ProcessInfo();
	bool bResult = OS_LaunchProcess(launchOptions, OUT *serverProcess);
	if (!bResult)
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("Master Server failed to launch a server process. It attempted to run %s with the cmd line args: \"%s\""), ExecFile.ToString(), launchOptions.CmdLineArgs);
		return false;
	}

	NightclubLog.Log(LogCategory::LL_Log, TXT("Launched server for map %s on port %s."), mapName, Int(serverPort));
	ServerInstances.emplace_back(SServerInstance(serverProcess, map, serverPort));
	return true;
}

bool MasterServer::LaunchServer (const DClass* map)
{
	unsigned short serverPort = ConnectionManager::DP_MasterServer + 1;

	while (true)
	{
		bool bPortInUse = false;
		for (const SServerInstance& instance : ServerInstances)
		{
			if (instance.Port == serverPort)
			{
				serverPort++;
				bPortInUse = true;
				break;
			}
		}

		if (!bPortInUse)
		{
			break;
		}
	}

	return LaunchServer(map, serverPort);
}

bool MasterServer::PairSignature (NetworkSignature* signature, unsigned short serverPort)
{
	if (signature == nullptr || serverPort == 0)
	{
		return false;
	}

	for (SServerInstance& instance : ServerInstances)
	{
		if (instance.Signature == nullptr && instance.Port == serverPort)
		{
			instance.Signature = signature;
			signature->OnDisconnected.RegisterHandler(SDFUNCTION_1PARAM(this, MasterServer, HandleDisconnect, void, NetworkSignature*));
			return true;
		}
	}

	return false;
}

NetworkSignature* MasterServer::FindSignature (const DClass* map) const
{
	for (const SServerInstance& instance : ServerInstances)
	{
		if (instance.Signature != nullptr && instance.Map == map)
		{
			return instance.Signature;
		}
	}

	return nullptr;
}

const DClass* MasterServer::FindMap (NetworkSignature* serverInstance) const
{
	for (const SServerInstance& instance : ServerInstances)
	{
		if (instance.Signature == serverInstance)
		{
			return instance.Map;
		}
	}

	return nullptr;
}

bool MasterServer::FindServerPort (NetworkSignature* serverSignature, unsigned short& outPort)
{
	for (const SServerInstance& instance : ServerInstances)
	{
		if (instance.Signature == serverSignature)
		{
			outPort = instance.Port;
			return true;
		}
	}

	return false;
}

void MasterServer::HandleDisconnect (NetworkSignature* signature)
{
	for (size_t i = 0; i < ServerInstances.size(); ++i)
	{
		if (ServerInstances.at(i).Signature == signature)
		{
			ServerInstances.erase(ServerInstances.begin() + i);
			return;
		}
	}
}
NC_END