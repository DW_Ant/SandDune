/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlayerNameAccessControl.cpp
=====================================================================
*/

#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "PlayerNameAccessControl.h"

IMPLEMENT_CLASS(SD::NC::PlayerNameAccessControl, SD::AccessControl)
NC_BEGIN

void PlayerNameAccessControl::InitProps ()
{
	Super::InitProps();

	bMustAppearOnList = true;
}

bool PlayerNameAccessControl::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	//Clients must tell inform the host their name to identify them.
	NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(signature->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
	if (identifier == nullptr)
	{
		outRejectReason = RR_InvalidData;
		return false;
	}

	if (identifier->ReadPlayerName().IsEmpty())
	{
		outRejectReason = RR_InvalidData;
		return false;
	}

	if (bMustAppearOnList)
	{
		NightclubEngineComponent* nightclubEngine = NightclubEngineComponent::Find();
		CHECK(nightclubEngine != nullptr)

		bool bFoundName = false;
		for (size_t i = 0; i < nightclubEngine->PendingPlayerData.size(); ++i)
		{
			if (nightclubEngine->PendingPlayerData.at(i).PlayerName.Compare(identifier->ReadPlayerName(), DString::CC_CaseSensitive) == 0)
			{
				bFoundName = true;
				break;
			}
		}
		
		if (!bFoundName)
		{
			//The player name is not on the list.
			outRejectReason = RR_InvalidData;
			return false;
		}
	}

	return true;
}
NC_END