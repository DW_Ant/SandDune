/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubEngineComponent.cpp
=====================================================================
*/

#include "BanListAccessControl.h"
#include "DuplicateNameAccessControl.h"
#include "ClientHackerTestObject.h"
#include "EnterClubPrompt.h"
#include "HackedConnectionManager.h"
#include "ListeningPortAccessControl.h"
#include "MapPrompt.h"
#include "MasterServer.h"
#include "NightclubAdminObject.h"
#include "NightclubBot.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubServerIdentifier.h"
#include "NightclubHud.h"
#include "NightclubTheme.h"
#include "OutdoorMap.h"
#include "Player.h"
#include "PlayerNameAccessControl.h"

IMPLEMENT_ENGINE_COMPONENT(SD::NC::NightclubEngineComponent)
NC_BEGIN

const Int NightclubEngineComponent::COLLISION_CHANNEL_OBJECT = 2;
const Int NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER = 4;
const DString NightclubEngineComponent::TRANSLATION_FILE = TXT("Nightclub");

NightclubEngineComponent::SPendingPlayerData::SPendingPlayerData (const DString& inPlayerName, Bool inHasBeverage) :
	PlayerName(inPlayerName),
	bHasBeverage(inHasBeverage)
{
	//Noop
}

NightclubEngineComponent::NightclubEngineComponent () : Super(),
	bReturnToOutdoorsOnDisconnect(true),
	LocalRole(NetworkSignature::NR_None),
	bInitialized(false)
{
	//Noop
}

void NightclubEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_NIGHTCLUB, TICK_GROUP_PRIORITY_NIGHTCLUB);

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	localNetEngine->SetConnectionManagerClass(HackedConnectionManager::SStaticClass());

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	if (guiEngine != nullptr)
	{
		guiEngine->SetDefaultThemeClass(NightclubTheme::SStaticClass());
	}
}

void NightclubEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	if (LocalRole == NetworkSignature::NR_Server)
	{
		NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
		CHECK(localNetEngine != nullptr)

		//Need to stop the NetworkEngineComponent from automatically opening the Game port. Instead it'll open the ports based on the command line arguments.
		localNetEngine->SetCmdLineOpenPorts(false);
		if (OwningEngine != nullptr && OwningEngine->HasCmdLineSwitch(TXT("-server"), DString::CC_IgnoreCase))
		{
			DString portStr = OwningEngine->GetCmdLineValue(TXT("port"), DString::CC_IgnoreCase);
			if (portStr.IsEmpty())
			{
				NightclubLog.Log(LogCategory::LL_Warning, TXT("Failed to launch Nightclub server instance. This application requires server instances to explicitly pass the port number through the command line arguments."));
				return;
			}

			Int port = portStr.Atoi();
			if (port <= 0)
			{
				NightclubLog.Log(LogCategory::LL_Warning, TXT("Failed to launch Nightclub server instance. A positive port number must be specified in the command line arguments."));
				return;
			}

			if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
			{
				//Must open port in InitializeComponent since PostInit sets up the LocalSignature with a NightclubServerIdentifier that reads in the listening port.
				connection->OpenPort(static_cast<unsigned short>(port.Value));
			}
		}
	}
	else if (LocalRole == NetworkSignature::NR_MasterServer)
	{
		if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
		{
			connection->OpenPort(ConnectionManager::DP_MasterServer);
		}
	}

	if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
	{
		connection->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, NightclubEngineComponent, HandleConnectionAccepted, void, NetworkSignature*));
		connection->OnDisconnect.RegisterHandler(SDFUNCTION_1PARAM(this, NightclubEngineComponent, HandleDisconnect, void, NetworkSignature*));
	}
}

void NightclubEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	std::vector<const DClass*> identifierClasses;
	if (LocalRole == NetworkSignature::NR_Server)
	{
		identifierClasses.push_back(NightclubServerIdentifier::SStaticClass());
	}
	else if (LocalRole == NetworkSignature::NR_Client)
	{
		identifierClasses.push_back(NightclubClientIdentifier::SStaticClass());
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr && localNetEngine->GetConnections() != nullptr)
	if (!localNetEngine->InitializeLocalNetworkSignature(LocalRole, identifierClasses))
	{
		NC::NightclubLog.Log(LogCategory::LL_Critical, TXT("Unable to to initialize local network signature."));
		return;
	}

	ConnectionManager* connections = localNetEngine->GetConnections();
	connections->SetRolePermissions(NetworkSignature::NR_Client, NetworkGlobals::P_Client);
	connections->SetRolePermissions(NetworkSignature::NR_Server, NetworkGlobals::P_Everything); //Servers are authoritative
	connections->SetRolePermissions(NetworkSignature::NR_MasterServer, NetworkGlobals::P_Everything); //Servers are authoritative

	switch (LocalRole)
	{
		case (NetworkSignature::NR_Client):
		{
			SceneLayer = SceneDrawLayer::CreateObject();
			SceneLayer->SetDrawPriority(250.f);
			GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
			CHECK(graphicsEngine != nullptr && graphicsEngine->GetPrimaryWindow() != nullptr)
			Camera = TopDownCamera::CreateObject();
			Camera->SetTranslation(Vector3(0.f, 0.f, 1000.f));
			Camera->Zoom = 0.005f;

			graphicsEngine->GetPrimaryWindow()->RegisterDrawLayer(SceneLayer.Get(), Camera.Get());

			Map = OutdoorMap::CreateObject();
			Player* newPlayer = Player::CreateObject();
			newPlayer->Teleport(Map->ReadPlayerSpawn());
			SetLocalPlayer(newPlayer);
			Camera->SetRelativeTo(newPlayer);

			GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
			if (guiEngine != nullptr && guiEngine->GetGuiTheme() != nullptr)
			{
				guiEngine->GetGuiTheme()->SetDefaultStyle(NightclubTheme::NIGHTCLUB_STYLE_NAME);
			}

			Hud = NightclubHud::CreateObject();
			Hud->RegisterToMainWindow();
			Hud->UpdatePlayerStatus(newPlayer);
			Hud->SetDepth(10.f); //Drawn behind everything else in the GuiDrawLayer.

			if (OwningEngine != nullptr && OwningEngine->HasCmdLineSwitch(TXT("-Bot"), DString::CC_IgnoreCase))
			{
				Bot = NightclubBot::CreateObject();
				Hud->FlashBotWarning();
			}

			break;
		}

		case (NetworkSignature::NR_Server):
		{
			//Create admin object that'll propagate to immediate clients and master server
			AdminObject = NightclubAdminObject::CreateObject();
			HackerTestObject = ClientHackerTestObject::CreateObject();

			//Create map instance that corresponds to the given command line arguments.
			if (OwningEngine != nullptr)
			{
				DString mapName = OwningEngine->GetCmdLineValue(TXT("map"), DString::CC_IgnoreCase);
				if (!mapName.IsEmpty())
				{
					for (ClassIterator iter(NightclubMap::SStaticClass()); iter.GetSelectedClass() != nullptr; ++iter)
					{
						if (iter.GetSelectedClass()->IsAbstract() || iter.GetSelectedClass()->GetDefaultObject() == nullptr)
						{
							continue;
						}

						DString className = iter.GetSelectedClass()->GetClassNameWithoutNamespace();
						if (className.Compare(mapName, DString::CC_IgnoreCase) == 0)
						{
							Map = dynamic_cast<NightclubMap*>(iter.GetSelectedClass()->GetDefaultObject()->CreateObjectOfMatchingClass());
							break;
						}
					}
				}
				else
				{
					NightclubLog.Log(LogCategory::LL_Warning, TXT("No map specified for the server. A map must be specified in the command line arguments."));
				}
			}

			AccessControlMiscSettings* miscAccessCtrl = AccessControlMiscSettings::CreateObject();
			miscAccessCtrl->AllowedRoles = (NetworkSignature::NR_Client | NetworkSignature::NR_MasterServer); //Servers will refuse connections from other servers

			PlayerNameAccessControl* playerAccess = PlayerNameAccessControl::CreateObject();
			playerAccess->bMustAppearOnList = true;
			playerAccess->RelevantRoles = NetworkSignature::NR_Client;

			localNetEngine->EditAccessControls().push_back(miscAccessCtrl);
			localNetEngine->EditAccessControls().push_back(playerAccess);

			//Attempt to connect to the master server immediately
			if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
			{
				connections->OpenConnection(sf::IpAddress::LocalHost, ConnectionManager::DP_MasterServer);
			}

			break;
		}

		case (NetworkSignature::NR_MasterServer):
		{
			ServerCoordinator = MasterServer::CreateObject();

			//Create admin object that'll propagate to all servers
			AdminObject = NightclubAdminObject::CreateObject();

			AccessControlMiscSettings* miscAccessCtrl = AccessControlMiscSettings::CreateObject();
			miscAccessCtrl->AllowedRoles = (NetworkSignature::NR_Client | NetworkSignature::NR_Server); //Master server will refuse connections from other master servers

			BanListAccessControl* banCtrl = BanListAccessControl::CreateObject();
			banCtrl->RelevantRoles = NetworkSignature::NR_Client;

			DuplicateNameAccessControl* dupeNameCtrl = DuplicateNameAccessControl::CreateObject();

			PlayerNameAccessControl* playerAccess = PlayerNameAccessControl::CreateObject();
			playerAccess->bMustAppearOnList = false; //Master server will not manage the PendingPlayerList. If the player connects without appearing on the list, the master server will connect them to the main room.
			playerAccess->RelevantRoles = NetworkSignature::NR_Client; //Master server will not check player names for servers connecting to it.

			ListeningPortAccessControl* portCtrl = ListeningPortAccessControl::CreateObject();
			portCtrl->RelevantRoles = NetworkSignature::NR_Server;

			localNetEngine->EditAccessControls().push_back(miscAccessCtrl);
			localNetEngine->EditAccessControls().push_back(banCtrl);
			localNetEngine->EditAccessControls().push_back(dupeNameCtrl);
			localNetEngine->EditAccessControls().push_back(playerAccess);
			localNetEngine->EditAccessControls().push_back(portCtrl);
			break;
		}
	}

	//Add logs to help developers when launching the Nightclub demo.
	NightclubLog.Log(LogCategory::LL_Log, TXT("To start the Nightclub master server, add \"-masterserver\" in the command line. The master server will automatically start servers."));
	NightclubLog.Log(LogCategory::LL_Log, TXT("To start a Nightclub client that runs a bot, add \"-bot\" in the command line."));
	NightclubLog.Log(LogCategory::LL_Log, TXT("To access the admin panel, connect to the master server under the user name \"Admin\" (case insensitive)."));

	UpdateConsoleTitle();

	bInitialized = true;
}

void NightclubEngineComponent::ShutdownComponent ()
{
	if (Map.IsValid())
	{
		Map->Destroy();
	}

	if (Camera.IsValid())
	{
		Camera->Destroy();
	}

	if (SceneLayer.IsValid())
	{
		SceneLayer->Destroy();
	}

	if (LocalPlayer.IsValid())
	{
		LocalPlayer->Destroy();
	}

	for (size_t i = 0; i < Players.size(); ++i)
	{
		Players.at(i)->Destroy();
	}
	ContainerUtils::Empty(OUT Players);

	if (AdminObject.IsValid())
	{
		AdminObject->Destroy();
	}

	if (HackerTestObject.IsValid())
	{
		HackerTestObject->Destroy();
	}

	if (Bot.IsValid())
	{
		Bot->Destroy();
	}

	if (Hud.IsValid())
	{
		Hud->Destroy();
	}

	if (ServerCoordinator.IsValid())
	{
		ServerCoordinator->Destroy();
	}

	if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
	{
		connection->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, NightclubEngineComponent, HandleConnectionAccepted, void, NetworkSignature*));
		connection->OnDisconnect.UnregisterHandler(SDFUNCTION_1PARAM(this, NightclubEngineComponent, HandleDisconnect, void, NetworkSignature*));
	}

	Super::ShutdownComponent();
}

void NightclubEngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(NetworkEngineComponent::SStaticClass());
}

void NightclubEngineComponent::GetPostInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPostInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(NetworkEngineComponent::SStaticClass());
}

void NightclubEngineComponent::DisplayEnterClubPrompt ()
{
	if (EnterClubUi.IsNullptr())
	{
		EnterClubUi = EnterClubPrompt::CreateObject();
		EnterClubUi->RegisterToMainWindow();
	}
}

void NightclubEngineComponent::CloseEnterClubPrompt ()
{
	if (EnterClubUi.IsValid())
	{
		EnterClubUi->Destroy();
	}
}

void NightclubEngineComponent::DisplayMapPrompt (const DString& promptText, SDFunction<void> onAccept)
{
	if (Prompt.IsNullptr())
	{
		Prompt = MapPrompt::CreateObject();
		Prompt->SetPosition(Vector2(0.25f, 0.25f));
		Prompt->SetSize(Vector2(0.5f, 0.33f));
		Prompt->SetDepth(1.f); //Drawn over everything else.
		Prompt->OnAccept = onAccept;
		Prompt->SetPromptText(promptText);
		Prompt->RegisterToMainWindow();
	}

	if (LocalPlayer.IsValid())
	{
		LocalPlayer->ReleaseMouseDrag();
	}
}

void NightclubEngineComponent::CloseMapPrompt ()
{
	if (Prompt.IsValid())
	{
		Prompt->Destroy();
	}
}

void NightclubEngineComponent::SetLocalRole (NetworkSignature::ENetworkRole newLocalRole)
{
	if (bInitialized)
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("Unable to set Nightclub's EngineComponent's LocalRole since the component has already launched. This role is used for determining how the process runs, which is must be set before Init."));
		return;
	}

	LocalRole = newLocalRole;
}

void NightclubEngineComponent::SetMap (NightclubMap* newMap)
{
	Map = newMap;
	UpdateConsoleTitle();
}

void NightclubEngineComponent::SetLocalPlayer (Player* newLocalPlayer)
{
	if (LocalRole != NetworkSignature::NR_Client)
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("The Nightclub Engine Component's Local Player can only be assigned by client processes."));
		return;
	}

	if (LocalPlayer.IsValid())
	{
		LocalPlayer->RemoveInputComponent();
	}

	LocalPlayer = newLocalPlayer;
	if (LocalPlayer.IsValid())
	{
		LocalPlayer->SetupInputComponent();
	}
}

void NightclubEngineComponent::AddPlayer (Player* newRemotePlayer)
{
	Players.push_back(newRemotePlayer);
}

void NightclubEngineComponent::SetAdminObject (NightclubAdminObject* newAdminObject)
{
	AdminObject = newAdminObject;
}

void NightclubEngineComponent::SetHackerTestObject (ClientHackerTestObject* newHackerTestObject)
{
	HackerTestObject = newHackerTestObject;
}

void NightclubEngineComponent::UpdateConsoleTitle ()
{
	DString completeTitle = ProjectName;

	if (Map.IsValid())
	{
		DString mapName = Map->StaticClass()->GetClassNameWithoutNamespace();
		if (!mapName.IsEmpty())
		{
			completeTitle += TXT(" ") + mapName;
		}
	}

	DString roleStr;
	switch (LocalRole)
	{
		case (NetworkSignature::NR_Client):
			roleStr = TXT("Client");
			break;

		case (NetworkSignature::NR_Server):
			roleStr = TXT("Server");
			break;

		case (NetworkSignature::NR_MasterServer):
			roleStr = TXT("Master-Server");
			break;
	}

	if (!roleStr.IsEmpty())
	{
		completeTitle += TXT(" ") + roleStr;
	}

	if (LocalRole != NetworkSignature::NR_Client)
	{
		Int numPlayers = 0;
		Int numServers = 0;
		bool isConnectedToMaster = false;
		for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
		{
			if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Client))
			{
				numPlayers++;
			}
			else if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Server))
			{
				numServers++;
			}
			else if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_MasterServer))
			{
				isConnectedToMaster = true;
			}
		}

		if (numPlayers > 0)
		{
			completeTitle += TXT(" Clients:") + numPlayers.ToString();
		}

		if (numServers > 0)
		{
			completeTitle += TXT(" Servers:") + numServers.ToString();
		}

		if (isConnectedToMaster)
		{
			completeTitle += TXT(" Connected-To-Master");
		}
	}

	OS_SetConsoleTitle(completeTitle);
}

void NightclubEngineComponent::HandleConnectionAccepted (NetworkSignature* signature)
{
	UpdateConsoleTitle();

	//Servers will instantiate Player objects for each client
	if (LocalRole == NetworkSignature::NR_Server && signature->HasRole(NetworkSignature::NR_Client))
	{
		Player* newPlayer = Player::CreateObject();

		if (Map.IsValid())
		{
			newPlayer->Teleport(Map->ReadPlayerSpawn());
		}

		newPlayer->SetNetOwner(signature);
		Players.push_back(newPlayer);

		if (NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(signature->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
		{
			newPlayer->SetPlayerName(identifier->ReadPlayerName());

			for (size_t i = 0; i < PendingPlayerData.size(); ++i)
			{
				if (PendingPlayerData.at(i).PlayerName.Compare(identifier->ReadPlayerName(), DString::CC_CaseSensitive) == 0)
				{
					newPlayer->SetHasDrink(PendingPlayerData.at(i).bHasBeverage);
					PendingPlayerData.erase(PendingPlayerData.begin() + i);
					break;
				}
			}
		}
	}
}

void NightclubEngineComponent::HandleDisconnect (NetworkSignature* signature)
{
	UpdateConsoleTitle();

	if (!bReturnToOutdoorsOnDisconnect)
	{
		bReturnToOutdoorsOnDisconnect = true;
		return; //Ignore this disconnect. Next disconnect will move the client back to the outdoor map.
	}

	//Destroy Player object associated with the signature
	if (LocalRole == NetworkSignature::NR_Server && signature->HasRole(NetworkSignature::NR_Client))
	{
		for (size_t i = 0; i < Players.size(); ++i)
		{
			if (Players.at(i)->GetNetOwner() == signature)
			{
				Players.at(i)->Destroy();
				Players.erase(Players.begin() + i);
				break;
			}
		}
	}
	else if (LocalRole == NetworkSignature::NR_Client && signature->HasRole(NetworkSignature::NR_MasterServer | NetworkSignature::NR_Server))
	{
		//Client disconnected from the server. Send them back to the Outdoor map
		if (Map.IsValid())
		{
			Map->Destroy();
		}

		if (LocalPlayer.IsValid())
		{
			LocalPlayer->Destroy();
		}

		Engine* localEngine = Engine::FindEngine();
		if (!localEngine->IsShuttingDown())
		{
			SetMap(OutdoorMap::CreateObject());
			Player* newPlayer = Player::CreateObject();
			newPlayer->Teleport(Map->ReadPlayerSpawn());
			SetLocalPlayer(newPlayer);
			Camera->SetRelativeTo(newPlayer);
		}
	}
}
NC_END