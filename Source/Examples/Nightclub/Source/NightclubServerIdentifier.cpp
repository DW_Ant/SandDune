/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubServerIdentifier.cpp
=====================================================================
*/

#include "NightclubServerIdentifier.h"

IMPLEMENT_CLASS(SD::NC::NightclubServerIdentifier, SD::NetworkIdentifier)
NC_BEGIN

void NightclubServerIdentifier::InitProps ()
{
	Super::InitProps();

	ListeningPort = 0;
}

bool NightclubServerIdentifier::InitializeIdToCurSystem (NetworkSignature::ENetworkRole networkRoles)
{
	//The server instance must have a listening port.
	std::vector<unsigned short> openedPorts;
	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->GetListeningPorts(OUT openedPorts);
		if (!ContainerUtils::IsEmpty(openedPorts))
		{
			ListeningPort = openedPorts.at(0);
		}
	}

	return (ListeningPort != 0);
}

bool NightclubServerIdentifier::ReadDataFromBuffer (const DataBuffer& data)
{
	return !((data >> ListeningPort).HasReadError());
}

void NightclubServerIdentifier::WriteDataToBuffer (DataBuffer& outData)
{
	outData << ListeningPort;
}
NC_END