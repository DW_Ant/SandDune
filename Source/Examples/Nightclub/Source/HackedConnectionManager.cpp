/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HackedConnectionManager.cpp
=====================================================================
*/

#include "HackedConnectionManager.h"

IMPLEMENT_CLASS(SD::NC::HackedConnectionManager, SD::ConnectionManager)
NC_BEGIN

void HackedConnectionManager::ElevatePermissions (NetworkGlobals::EPermissions permissionsToGrant)
{
	for (auto& connection : Connections)
	{
		connection.second.LocalPermissions |= permissionsToGrant;
	}
}
NC_END