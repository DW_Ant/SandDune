/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DanceFloorMap.cpp
=====================================================================
*/

#include "BarMap.h"
#include "BoothMap.h"
#include "DanceComponent.h"
#include "DanceFloorMap.h"
#include "MapGate.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::DanceFloorMap, SD::NC::NightclubMap)
NC_BEGIN

void DanceFloorMap::InitProps ()
{
	Super::InitProps();

	bCreatedExitPoints = false;
	DancerNetworkComp = nullptr;
	PlayerSpawn = Vector3(0.f, 1024.f, 0.f);
	MapSize = Vector2(5000.f, 4000.f); //50x40 meters
}

void DanceFloorMap::BeginObject ()
{
	Super::BeginObject();

	DancerNetworkComp = NetworkComponent::CreateObject();
	if (AddComponent(DancerNetworkComp))
	{
		BIND_RPC_1REF_PARAM(DancerNetworkComp, ClientSetDancerLocations, this, DanceFloorMap, std::vector<Vector3>);
		ClientSetDancerLocations.CanBeExecutedOn = NetworkSignature::NR_Client;
	}

	CreateStage();
	CreateDanceFloor();
}

void DanceFloorMap::InitializePlayer (Player* newPlayer)
{
	Super::InitializePlayer(newPlayer);

	//Players cannot drink on the dance floor.
	newPlayer->SetCanDrink(false);
}

void DanceFloorMap::HandleNetInitialize (NetworkSignature* connectedTo)
{
	Super::HandleNetInitialize(connectedTo);

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	if (localSignature->HasRole(NetworkSignature::NR_Server))
	{
		//Server needs to tell the client about its dancer locations
		std::vector<Vector3> dancerLocations;
		for (size_t i = 0; i < DancerTransforms.size(); ++i)
		{
			dancerLocations.push_back(DancerTransforms.at(i)->GetTranslation());
		}

		ClientSetDancerLocations(connectedTo, dancerLocations);
	}

	CreateExitPoints();
}

void DanceFloorMap::HandleGetComponentList (std::vector<NetworkComponent*>& outComponentList)
{
	Super::HandleGetComponentList(OUT outComponentList);

	outComponentList.push_back(DancerNetworkComp);
}

void DanceFloorMap::CreateStage ()
{
	//Create the four steps
	for (Float i = 0.f; i < 4.f; ++i)
	{
		SceneTransformComponent* stageTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(stageTransform))
		{
			stageTransform->SetTranslation(Vector3(0.f, -2000.f, -20.f + i));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (stageTransform->AddComponent(color))
			{
				color->SetPivot(Vector2(0.5f, 0.f));
				color->SetBaseSize(Vector2(2048.f - (i * 32.f), 1024.f - (i * 32.f)));
				color->SolidColor = Color(45, 45, 45);
				if (i%2 == 0.f)
				{
					color->SolidColor.Source.r += 32;
					color->SolidColor.Source.g += 32;
					color->SolidColor.Source.b += 32;
				}

				RegisterRenderComp(color);
			}

			if (i == 0.f)
			{
				//Add a physics component that prevents players from dancing on stage.
				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (stageTransform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetOverlappingChannels(NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
					phys->SetBeginOverlap(SDFUNCTION_2PARAM(this, DanceFloorMap, HandleEnterStage, void, PhysicsComponent*, PhysicsComponent*));
					phys->SetEndOverlap(SDFUNCTION_2PARAM(this, DanceFloorMap, HandleExitStage, void, PhysicsComponent*, PhysicsComponent*));
					phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 256.f,
					{
						Vector2(-1024.f, 0.f),
						Vector2(1024.f, 0.f),
						Vector2(1024.f, 1024.f),
						Vector2(-1024.f, 1024.f)
					}));
				}
			}
		}
	}

	//Create the table
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, -1250.f, -10.f));
			Vector2 tableSize(512.f, 128.f);

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(45, 45, 130);
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SetBaseSize(tableSize);
				RegisterRenderComp(color);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 64.f,
				{
					Vector2(tableSize.X * -0.5f, tableSize.Y * -0.5f),
					Vector2(tableSize.X * 0.5f, tableSize.Y * -0.5f),
					Vector2(tableSize.X * 0.5f, tableSize.Y * 0.5f),
					Vector2(tableSize.X * -0.5f, tableSize.Y * 0.5f)
				}));
			}
		}
	}

	//Create the front pillars
	std::vector<Vector3> frontPillarLocations(
	{
		Vector3(512.f, -1250.f, 5.f),
		Vector3(-512.f, -1250.f, 5.f)
	});

	for (size_t i = 0; i < frontPillarLocations.size(); ++i)
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(frontPillarLocations.at(i));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(196, 139, 81);
				color->SetBaseSize(Vector2(96.f, 1024.f));
				color->SetPivot(Vector2(0.5f, 1.f));
				RegisterRenderComp(color);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 48.f));
			}
		}
	}

	//Create the back pillars
	std::vector<Vector3> backPillarLocations(
	{
		Vector3(768.f, -1750.f, 2.f),
		Vector3(-768.f, -1750.f, 2.f)
	});

	for (size_t i = 0; i < backPillarLocations.size(); ++i)
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(backPillarLocations.at(i));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(150, 100, 50);
				color->SetBaseSize(Vector2(96.f, 1024.f));
				color->SetPivot(Vector2(0.5f, 1.f));
				RegisterRenderComp(color);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 48.f));
			}
		}
	}
}

void DanceFloorMap::CreateDanceFloor ()
{
	//Create base floor
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, 0.f, -100.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(8, 40, 52);
				color->SetBaseSize(MapSize);
				color->SetPivot(0.5f, 0.5f);
				RegisterRenderComp(color);
			}
		}
	}

	//Create back wall
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, MapSize.Y * -0.5f, -95.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(67, 101, 132);
				color->SetPivot(Vector2(0.5f, 1.f));
				color->SetBaseSize(MapSize);
				RegisterRenderComp(color);
			}
		}
	}

	//Create checkered floor
	Int numHorizontalTiles = 24;
	Int numVerticalTiles = 16;
	Vector2 tileSize(96.f, 96.f);
	std::vector<Color> colorPattern(
	{
		Color(25, 245, 135),
		Color(176, 91, 242),
		Color(84, 184, 242),
		Color(235, 223, 116),
		Color(235, 138, 70)
	});
	size_t colorIdx = 0;

	for (Int x = 0; x < numHorizontalTiles; ++x)
	{
		for (Int y = 0; y < numVerticalTiles; ++y)
		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				transform->SetTranslation(Vector3((tileSize.X * -numHorizontalTiles.ToFloat() * 0.5f) + (tileSize.X * x.ToFloat()), (tileSize.Y * -numVerticalTiles.ToFloat() * 0.5f) + (tileSize.Y * y.ToFloat()), -90.f));

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (transform->AddComponent(color))
				{
					color->SetPivot(0.5f, 0.5f);
					color->SetBaseSize(tileSize);
					color->SolidColor = colorPattern.at(colorIdx);
					colorIdx++;
					colorIdx %= colorPattern.size();
					RegisterRenderComp(color);
				}
			}
		}
	}

	//Spawn three flashing lights
	for (size_t i = 0; i < 6; ++i)
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, 0.f, 512.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SetShape(ColorRenderComponent::ST_Circle);
				color->SetBaseSize(768.f, 768.f);
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SolidColor.Source.a = 0; //Force tick frame to randomize color and location
				RegisterRenderComp(color);

				FlashingLights.push_back(color);
			}
		}
	}

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, DanceFloorMap, HandleLightTick, void, Float));
	}

	//Create a few dancers
	for (size_t i = 0; i < 4; ++i)
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(RandomUtils::RandRange(-1152.f, 1152.f), RandomUtils::RandRange(-768.f, 768.f), 0.f));
			DancerTransforms.push_back(transform);

			SceneTransformComponent* danceTransform = SceneTransformComponent::CreateObject();
			if (transform->AddComponent(danceTransform))
			{
				danceTransform->SetTranslation(Vector3::ZERO_VECTOR);

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (danceTransform->AddComponent(color))
				{
					color->SetBaseSize(Vector2(192.f, 192.f));
					color->SetShape(ColorRenderComponent::ST_Circle);
					color->SetPivot(Vector2(0.5f, 0.5f));
					color->SolidColor.Source.r = static_cast<sf::Uint8>(RandomUtils::RandRange(128.f, 255.f).ToInt().Value);
					color->SolidColor.Source.g = static_cast<sf::Uint8>(RandomUtils::RandRange(128.f, 255.f).ToInt().Value);
					color->SolidColor.Source.b = static_cast<sf::Uint8>(RandomUtils::RandRange(128.f, 255.f).ToInt().Value);
					RegisterRenderComp(color);
				}

				DanceComponent* dance = DanceComponent::CreateObject();
				if (danceTransform->AddComponent(dance))
				{
					dance->SetDancing(true);
				}
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT | NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 96.f));
			}
		}
	}
}

void DanceFloorMap::CreateExitPoints ()
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (localSignature->HasRole(NetworkSignature::NR_Client))
	{
		//Clients should spawn the exit point since they don't need a RPC call to disconnect from the server.
		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		Float zoneRadius = 256.f;

		//Create exit point
		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				transform->SetTranslation(Vector3(0.f, 1500.f, -5.f));

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (transform->AddComponent(color))
				{
					color->SetBaseSize(Vector2(zoneRadius * 2.f, zoneRadius * 2.f));
					color->SetShape(ColorRenderComponent::ST_Circle);
					color->SolidColor = Color(200, 0, 0);
					color->SetPivot(Vector2(0.5f, 0.5f));
					RegisterRenderComp(color);
				}

				TextRenderComponent* text = TextRenderComponent::CreateObject();
				if (transform->AddComponent(text))
				{
					TextRenderComponent::STextData& textData = text->CreateTextInstance(translator->TranslateText(TXT("ExitClub"), NightclubEngineComponent::TRANSLATION_FILE, TXT("DanceFloorMap")), localFontPool->GetDefaultFont(), 128);
					textData.DrawOffset.x = -zoneRadius.Value;
					textData.DrawOffset.y = -96;
					RegisterRenderComp(text);
				}

				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (transform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetOverlappingChannels(NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
					phys->SetBeginOverlap(SDFUNCTION_2PARAM(this, DanceFloorMap, HandleLeaveClubPrompt, void, PhysicsComponent*, PhysicsComponent*));
					phys->SetEndOverlap(SDFUNCTION_2PARAM(this, DanceFloorMap, HandleLeavePrompt, void, PhysicsComponent*, PhysicsComponent*));
					phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, zoneRadius));
				}
			}
		}
	}

	if (!bCreatedExitPoints && RoleComp != nullptr && RoleComp->HasAuthority())
	{
		MapGate* toBar = MapGate::CreateObject();
		toBar->SetTranslation(Vector3(-2048.f, 0.f, -5.f));
		toBar->MapTextKey = TXT("DanceFloorToBar");
		toBar->DrawOffset = Vector2(-256.f, -96.f);
		toBar->PromptTextKey = TXT("DanceFloorToBarPrompt");
		toBar->SetDestination(BarMap::SStaticClass());
		MapObjects.push_back(toBar);

		MapGate* toBooth = MapGate::CreateObject();
		toBooth->SetTranslation(Vector3(2048.f, 0.f, -5.f));
		toBooth->MapTextKey = TXT("DanceFloorToBooth");
		toBooth->DrawOffset = Vector2(-128.f, -192.f);
		toBooth->PromptTextKey = TXT("DanceFloorToBoothPrompt");
		toBooth->SetDestination(BoothMap::SStaticClass());
		MapObjects.push_back(toBooth);

		bCreatedExitPoints = true;
	}
}

bool DanceFloorMap::ClientSetDancerLocations_Implementation (const std::vector<Vector3>& dancerLocations)
{
	for (size_t i = 0; i < dancerLocations.size() && i < DancerTransforms.size(); ++i)
	{
		DancerTransforms.at(i)->SetTranslation(dancerLocations.at(i));
	}

	return true;
}

void DanceFloorMap::HandleLightTick (Float deltaSec)
{
	for (ColorRenderComponent* color : FlashingLights)
	{
		if (color->SolidColor.Source.a == 0)
		{
			//Pick a random RGB color
			color->SolidColor.Source.r = RandomUtils::Rand(255);
			color->SolidColor.Source.g = RandomUtils::Rand(255);
			color->SolidColor.Source.b = RandomUtils::Rand(255);
			color->SolidColor.Source.a = static_cast<sf::Uint8>(RandomUtils::RandRange(100, 150).ToInt().Value); //Add some randomization to make the lights flash async to each other.

			//Pick a random location
			if (SceneTransformComponent* transform = dynamic_cast<SceneTransformComponent*>(color->GetOwner()))
			{
				transform->EditTranslation().X = RandomUtils::RandRange(-1024.f, 1024.f);
				transform->EditTranslation().Y = RandomUtils::RandRange(-384.f, 384.f);
			}
		}
		else
		{
			const Float alphaPerSec = 128.f;
			sf::Uint8 delta = static_cast<sf::Uint8>((alphaPerSec * deltaSec).ToInt().Value);
			if (delta < color->SolidColor.Source.a)
			{
				color->SolidColor.Source.a -= delta;
			}
			else
			{
				color->SolidColor.Source.a = 0;
			}
		}
	}
}

void DanceFloorMap::HandleEnterStage (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	if (Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner()))
	{
		if (instigator->GetDanceComp() != nullptr)
		{
			instigator->GetDanceComp()->SetEnabled(false);
		}
	}
}

void DanceFloorMap::HandleExitStage (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	if (Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner()))
	{
		if (instigator->GetDanceComp() != nullptr)
		{
			instigator->GetDanceComp()->SetEnabled(true);
		}
	}
}

void DanceFloorMap::HandleLeaveClubPrompt (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightEngine->GetLocalPlayer())
	{
		localNightEngine->DisplayMapPrompt(translator->TranslateText(TXT("LeaveClubPrompt"), NightclubEngineComponent::TRANSLATION_FILE, TXT("DanceFloorMap")), SDFUNCTION(this, DanceFloorMap, HandleLeaveClub, void));
	}
}

void DanceFloorMap::HandleLeaveClub ()
{
	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->DisconnectFromAll(false);
	}
}

void DanceFloorMap::HandleLeavePrompt (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightEngine->GetLocalPlayer())
	{
		localNightEngine->CloseMapPrompt();
	}
}
NC_END