/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MapPrompt.cpp
=====================================================================
*/

#include "MapPrompt.h"
#include "NightclubEngineComponent.h"

IMPLEMENT_CLASS(SD::NC::MapPrompt, SD::GuiEntity)
NC_BEGIN

void MapPrompt::InitProps ()
{
	Super::InitProps();

	PromptLabel = nullptr;
	AcceptButton = nullptr;
	CancelButton = nullptr;
}

void MapPrompt::ConstructUI ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	FrameComponent* background = FrameComponent::CreateObject();
	if (AddComponent(background))
	{
		background->SetSize(Vector2(1.f, 1.f));
		background->SetBorderThickness(3.f);
		background->SetAlwaysConsumeMouseEvents(true);
		Float margin = 0.075f;
		
		PromptLabel = LabelComponent::CreateObject();
		if (background->AddComponent(PromptLabel))
		{
			PromptLabel->SetAutoRefresh(false);
			PromptLabel->SetAnchorRight(margin);
			PromptLabel->SetAnchorLeft(margin);
			PromptLabel->SetAnchorTop(margin);
			PromptLabel->SetSize(Vector2(1.f, 0.75f));
			PromptLabel->SetWrapText(true);
			PromptLabel->SetClampText(true);
			PromptLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
			PromptLabel->SetVerticalAlignment(LabelComponent::VA_Center);
			PromptLabel->SetAutoRefresh(true);
		}

		AcceptButton = ButtonComponent::CreateObject();
		if (background->AddComponent(AcceptButton))
		{
			AcceptButton->SetAnchorLeft(margin);
			AcceptButton->SetAnchorBottom(margin);
			AcceptButton->SetSize(Vector2(0.4f, 0.2f));
			AcceptButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MapPrompt, HandleAcceptReleased, void, ButtonComponent*));
			AcceptButton->SetCaptionText(translator->TranslateText(TXT("AcceptButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("MapPrompt")));
		}

		CancelButton = ButtonComponent::CreateObject();
		if (background->AddComponent(CancelButton))
		{
			CancelButton->SetAnchorRight(margin);
			CancelButton->SetAnchorBottom(margin);
			CancelButton->SetSize(Vector2(0.4f, 0.2f));
			CancelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MapPrompt, HandleCloseReleased, void, ButtonComponent*));
			CancelButton->SetCaptionText(translator->TranslateText(TXT("CancelButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("MapPrompt")));
		}
	}
}

void MapPrompt::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())
	Focus->TabOrder.push_back(AcceptButton);
	Focus->TabOrder.push_back(CancelButton);
}

void MapPrompt::SetPromptText (const DString& newPromptText)
{
	if (PromptLabel != nullptr)
	{
		PromptLabel->SetText(newPromptText);
	}
}

void MapPrompt::HandleAcceptReleased (ButtonComponent* button)
{
	if (OnAccept.IsBounded())
	{
		OnAccept.Execute();
	}

	Destroy();
}

void MapPrompt::HandleCloseReleased (ButtonComponent* button)
{
	Destroy();
}
NC_END