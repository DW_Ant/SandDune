/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubBot.cpp
=====================================================================
*/

#include "MapPrompt.h"
#include "NightclubBot.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubHud.h"

IMPLEMENT_CLASS(SD::NC::NightclubBot, SD::Entity)
NC_BEGIN

const std::vector<DString> NightclubBot::BOT_NAMES
{
	TXT("Another Nightclub Bot"),
	TXT("Ant"),
	TXT("Artorias"),
	TXT("Assimulated"),
	TXT("Bacon Bits"),
	TXT("Big Thunder"),
	TXT("Bingus"),
	TXT("Blackheart"),
	TXT("Cheapshot"),
	TXT("Dancer"),
	TXT("Drinker"),
	TXT("Dyllus"),
	TXT("Edge Lord"),
	TXT("Fluffy"),
	TXT("Gaggle of Midgets"),
	TXT("Heavy Circle"),
	TXT("Icky"),
	TXT("Jump Jump Jump"),
	TXT("KarmaKat"),
	TXT("Lil Eezay"),
	TXT("Mobius"),
	TXT("November"),
	TXT("Old Potato"),
	TXT("Pancakes"),
	TXT("Patron"),
	TXT("Plitt"),
	TXT("Queen"),
	TXT("R. Roll"),
	TXT("Sal"),
	TXT("Sawrey"),
	TXT("Scratchy"),
	TXT("Totally not a bot"),
	TXT("Triple Trouble"),
	TXT("Under Cube"),
	TXT("Visor"),
	TXT("Winterspell"),
	TXT("XTREME dot"),
	TXT("Yohkuj"),
	TXT("Zoink")
};

void NightclubBot::InitProps ()
{
	Super::InitProps();

	Input = nullptr;
	ConnectTick = nullptr;
	MovementTick = nullptr;
	ActionTick = nullptr;
	PromptTick = nullptr;

	MovementInterval = Range<Float>(0.5f, 1.25f);
	ActionInterval = Range<Float>(3.f, 6.f);
}

void NightclubBot::BeginObject ()
{
	Super::BeginObject();

	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->AddKeybind(sf::Keyboard::Escape, false, SDFUNCTION(this, NightclubBot, HandleToggleBot, bool));
	}

	ConnectTick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(ConnectTick))
	{
		ConnectTick->SetTickInterval(6.f);
		ConnectTick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubBot, HandleConnectTick, void, Float));
	}

	MovementTick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(MovementTick))
	{
		MovementTick->SetTickInterval(RandomUtils::RandRange(MovementInterval.Min, MovementInterval.Max));
		MovementTick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubBot, HandleMovementTick, void, Float));
	}

	ActionTick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(ActionTick))
	{
		ActionTick->SetTickInterval(RandomUtils::RandRange(ActionInterval.Min, ActionInterval.Max));
		ActionTick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubBot, HandleActionTick, void, Float));
	}

	PromptTick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (AddComponent(PromptTick))
	{
		PromptTick->SetTickInterval(0.5f);
		PromptTick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubBot, HandlePromptTick, void, Float));
	}
}

void NightclubBot::CheckConnections ()
{
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		//Bot is already connected to a server. Do nothing.
		return;
	}

	//Pick a random name followed by a number.
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	if (NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(localSignature->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
	{
		identifier->SetPlayerName(DString::CreateFormattedString(TXT("%s_%s"), BOT_NAMES.at(RandomUtils::Rand(BOT_NAMES.size())), RandomUtils::Rand(Int(100))));
	}

	//Connect to the master server. If it fails, the TickComponent will attempt to connect at a later time with a new name.
	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OpenConnection(sf::IpAddress::LocalHost, ConnectionManager::DP_MasterServer);
	}
}

void NightclubBot::PickMovementSpot ()
{
	//Move the mouse pointer in a random spot in the window.
	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	MousePointer* mouse = MousePointer::GetMousePointer();
	InputEngineComponent* localInput = InputEngineComponent::Find();
	if (localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr && mouse != nullptr && localInput != nullptr)
	{
		Vector2 windowSize = localGraphics->GetPrimaryWindow()->GetWindowSize();
		Vector2 mousePos;
		mousePos.X = RandomUtils::RandRange(1.f, windowSize.X - 1);
		mousePos.Y = RandomUtils::RandRange(1.f, windowSize.Y - 1);
		mouse->SetPosition(mousePos);

		//Send a mouse click event
		if (InputBroadcaster* broadcaster = localInput->GetMainBroadcaster())
		{
			sf::Event pressedEvent;
			pressedEvent.type = sf::Event::MouseButtonPressed;
			pressedEvent.mouseButton.button = sf::Mouse::Left;
			pressedEvent.mouseButton.x = mousePos.X.ToInt().Value;
			pressedEvent.mouseButton.y = mousePos.Y.ToInt().Value;

			sf::Event releasedEvent = pressedEvent;
			releasedEvent.type = sf::Event::MouseButtonReleased;
			broadcaster->BroadcastNewEvent(pressedEvent);
			broadcaster->BroadcastNewEvent(releasedEvent);
		}

		//Pick a new interval for the movement tick
		if (MovementTick != nullptr)
		{
			MovementTick->SetTickInterval(RandomUtils::RandRange(MovementInterval.Min, MovementInterval.Max));
		}
	}
}

void NightclubBot::DoNightclubAction ()
{
	InputEngineComponent* localInput = InputEngineComponent::Find();
	if (localInput != nullptr && localInput->GetMainBroadcaster() != nullptr)
	{
		sf::Event pressedEvent;
		pressedEvent.type = sf::Event::KeyPressed;
		pressedEvent.key.alt = false;
		pressedEvent.key.code = (RandomUtils::fRand() > 0.5f) ? sf::Keyboard::Num1 : sf::Keyboard::Num2; //Randomize between dancing and drinking actions.
		pressedEvent.key.control = false;
		pressedEvent.key.shift = false;
		pressedEvent.key.system = false;

		sf::Event releasedEvent = pressedEvent;
		releasedEvent.type = sf::Event::KeyReleased;

		localInput->GetMainBroadcaster()->BroadcastNewEvent(pressedEvent);
		localInput->GetMainBroadcaster()->BroadcastNewEvent(releasedEvent);
	}

	if (ActionTick != nullptr)
	{
		ActionTick->SetTickInterval(RandomUtils::RandRange(ActionInterval.Min, ActionInterval.Max));
	}
}

void NightclubBot::AcceptPrompts ()
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (MapPrompt* prompt = localNightclub->GetPrompt())
	{
		if (prompt->OnAccept.IsBounded())
		{
			prompt->OnAccept();
			prompt->Destroy();

			//Prevent the bot from trying to reconnect to the master server during transit.
			if (ConnectTick != nullptr)
			{
				ConnectTick->ResetAccumulatedTickTime();
			}
		}
	}
}

bool NightclubBot::HandleToggleBot ()
{
	bool bBotActive = false;
	if (ConnectTick != nullptr)
	{
		bBotActive = !ConnectTick->IsTicking();
		ConnectTick->SetTicking(bBotActive);
	}

	if (MovementTick != nullptr)
	{
		MovementTick->SetTicking(bBotActive);
	}

	if (ActionTick != nullptr)
	{
		ActionTick->SetTicking(bBotActive);
	}

	if (PromptTick != nullptr)
	{
		PromptTick->SetTicking(bBotActive);
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetHud() != nullptr)
	{
		localNightclub->GetHud()->SetBotModeActive(bBotActive);
	}

	return true;
}

void NightclubBot::HandleConnectTick (Float deltaSec)
{
	CheckConnections();
}

void NightclubBot::HandleMovementTick (Float deltaSec)
{
	PickMovementSpot();
}

void NightclubBot::HandleActionTick (Float deltaSec)
{
	DoNightclubAction();
}

void NightclubBot::HandlePromptTick (Float deltaSec)
{
	AcceptPrompts();
}
NC_END