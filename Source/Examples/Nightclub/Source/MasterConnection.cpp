/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MasterConnection.cpp
=====================================================================
*/

#include "DanceFloorMap.h"
#include "DuplicateNameAccessControl.h"
#include "MapGate.h"
#include "MasterConnection.h"
#include "MasterServer.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubMap.h"

IMPLEMENT_CLASS(SD::NC::MasterConnection, SD::Entity)
NC_BEGIN

MasterConnection::SPendingTravelData::SPendingTravelData (NetworkSignature* inServerSource, const DString& inPlayerName, Bool inHasBeverage, const DClass* inMap) :
	ServerSource(inServerSource),
	PlayerName(inPlayerName),
	bHasBeverage(inHasBeverage),
	Map(inMap)
{
	//Noop
}

void MasterConnection::InitProps ()
{
	Super::InitProps();

	RoleComp = nullptr;
	NetComp = nullptr;
}

void MasterConnection::BeginObject ()
{
	Super::BeginObject();

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, MasterConnection, HandleConnectionAccepted, void, NetworkSignature*));
		connections->OnDisconnect.RegisterHandler(SDFUNCTION_1PARAM(this, MasterConnection, HandleDisconnect, void, NetworkSignature*));
	}

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = [&](std::vector<NetworkComponent*>& outNetComponents)
		{
			outNetComponents.push_back(NetComp);
		};

		RoleComp->OnIsNetRelevant = [&](NetworkSignature* remoteClient)
		{
			//This Entity is relevant to all
			return true;
		};

		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			/*
			Make sure this is executed OnNetInitialize instead of relying on the callbacks from ConnectionManager::OnConnectionAccepted.
			This way is safer since this callback is executed after the client received a copy of this object (MasterConnection) and it established a connection.
			That way, it's independent of process' frame rates and order of operations. RPC's are safe to call.
			Otherwise there would be a chance that the master server would invoke a RPC while there aren't any clients to broadcast the RPC to.
			*/

			//If a client is connecting to a master server, have the master server start the process of redirecting the client to their desired servers.
			if (RoleComp->HasAuthority() && connectedTo->HasRole(NetworkSignature::NR_Client))
			{
				NightclubClientIdentifier* clientIdentifier = dynamic_cast<NightclubClientIdentifier*>(connectedTo->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
				CHECK(clientIdentifier != nullptr) //Should always pass since the master server should check for the identifier before accepting its connection.
				PendingEnteringClients.push_back(clientIdentifier->ReadPlayerName());

				NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
				CHECK(localNightEngine != nullptr)
				if (MasterServer* coordinator = localNightEngine->GetServerCoordinator())
				{
					NetworkSignature* server = coordinator->FindSignature(DanceFloorMap::SStaticClass());
					if (server != nullptr)
					{
						ServerNotifyPlayerArrival(server, clientIdentifier->ReadPlayerName(), false);
					}
					else //No server instance running, launch a new one
					{
						coordinator->LaunchServer(DanceFloorMap::SStaticClass());
					}
				}
			}

			//If a server is connecting to a master server, check if the master server has any pending travel requests for the server.
			if (RoleComp->HasAuthority() && connectedTo->HasRole(NetworkSignature::NR_Server))
			{
				NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
				CHECK(localNightEngine != nullptr && localNightEngine->GetServerCoordinator())
				MasterServer* coordinator = localNightEngine->GetServerCoordinator();
				const DClass* serverMap = coordinator->FindMap(connectedTo);

				if (serverMap != nullptr)
				{
					for (size_t i = 0; i < PendingTravelRequests.size(); ++i)
					{
						if (PendingTravelRequests.at(i).Map == serverMap)
						{
							ServerNotifyPlayerArrival(connectedTo, PendingTravelRequests.at(i).PlayerName, PendingTravelRequests.at(i).bHasBeverage);
						}
					}

					if (serverMap == DanceFloorMap::SStaticClass())
					{
						for (size_t i = 0; i < PendingEnteringClients.size(); ++i)
						{
							ServerNotifyPlayerArrival(connectedTo, PendingEnteringClients.at(i), false);
						}
						ContainerUtils::Empty(OUT PendingEnteringClients); //Don't need a reference to this player anymore
					}
				}
			}
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		BIND_RPC_2PARAM(NetComp, ServerNotifyPlayerArrival, this, MasterConnection, DString, Bool);
		ServerNotifyPlayerArrival.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_1REF_PARAM(NetComp, MasterServerSendPlayer, this, MasterConnection, DString);
		MasterServerSendPlayer.CanBeExecutedOn = NetworkSignature::NR_MasterServer;
		MasterServerSendPlayer.CanBeExecutedFrom = NetworkSignature::NR_Server;

		BIND_RPC_1PARAM(NetComp, ClientConnectTo, this, MasterConnection, Int);
		ClientConnectTo.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC_2PARAM(NetComp, ServerSendClient, this, MasterConnection, DString, Int);
		ServerSendClient.CanBeExecutedOn = NetworkSignature::NR_Server | NetworkSignature::NR_MasterServer;
		ServerSendClient.CanBeExecutedFrom = NetworkSignature::NR_MasterServer;

		BIND_RPC_1REF_PARAM(NetComp, MasterServerRemovePlayer, this, MasterConnection, DString);
		MasterServerRemovePlayer.CanBeExecutedOn = NetworkSignature::NR_MasterServer;
		MasterServerRemovePlayer.CanBeExecutedFrom = NetworkSignature::NR_Server;
	}
}

void MasterConnection::Destroyed ()
{
	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, MasterConnection, HandleConnectionAccepted, void, NetworkSignature*));
		connections->OnDisconnect.UnregisterHandler(SDFUNCTION_1PARAM(this, MasterConnection, HandleDisconnect, void, NetworkSignature*));
	}

	Super::Destroyed();
}

void MasterConnection::MovePlayer (NetworkSignature* moveFrom, const DString& playerName, Bool bHasBeverage, const DClass* map)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr && localNightclub->GetServerCoordinator() != nullptr)
	if (MasterServer* coordinator = localNightclub->GetServerCoordinator())
	{
		PendingTravelRequests.emplace_back(SPendingTravelData(moveFrom, playerName, bHasBeverage, map));

		NetworkSignature* destination = coordinator->FindSignature(map);
		if (destination != nullptr)
		{
			ServerNotifyPlayerArrival(destination, playerName, bHasBeverage);
			return;
		}

		//Need to launch the server process
		coordinator->LaunchServer(map);
	}
}

bool MasterConnection::ServerNotifyPlayerArrival_Implementation (DString playerName, Bool bHasBeverage)
{
	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	if (localNightEngine->GetLocalRole() == NetworkSignature::NR_Server)
	{
		localNightEngine->PendingPlayerData.emplace_back(NightclubEngineComponent::SPendingPlayerData(playerName, bHasBeverage));

		//Send a notification to the master server that this server is ready for the client.
		MasterServerSendPlayer(ServerNotifyPlayerArrival.GetInstigatedFrom(), playerName);

		return true;
	}
	
	return false;
}

bool MasterConnection::MasterServerSendPlayer_Implementation (const DString& playerName)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)
	MasterServer* coordinator = localNightclub->GetServerCoordinator();

	unsigned short serverPort;
	if (!coordinator->FindServerPort(MasterServerSendPlayer.GetInstigatedFrom(), OUT serverPort))
	{
		//The caller is not authorized to invoke this method
		return false;
	}

	//First check if it's from a PendingTravel list
	bool bFoundPlayer = false;
	if (!ContainerUtils::IsEmpty(PendingTravelRequests))
	{
		size_t i = PendingTravelRequests.size() - 1;
		while (true)
		{
			for (const SPendingTravelData& request : PendingTravelRequests)
			{
				if (request.PlayerName.Compare(playerName, DString::CC_CaseSensitive) == 0)
				{
					ServerSendClient(request.ServerSource, playerName, Int(serverPort));
					bFoundPlayer = true;
					PendingTravelRequests.erase(PendingTravelRequests.begin() + i);
					break;
				}
			}

			if (i == 0)
			{
				break;
			}
			--i;
		}
	}

	if (bFoundPlayer)
	{
		return true;
	}

	//It's possible that the player is connected to the master server (from the initial connection). Try executing locally
	ServerSendClient.LocalExecute(playerName, Int(serverPort));
	return true;
}

bool MasterConnection::ClientConnectTo_Implementation (Int serverPort)
{
	if (serverPort <= 0)
	{
		return false; //Invalid port given
	}

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OpenConnection(sf::IpAddress::LocalHost, static_cast<unsigned short>(serverPort.ToUnsignedInt32()));
		if (NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find())
		{
			localNightclub->bReturnToOutdoorsOnDisconnect = false; //Don't boot the client back to outdoors during this transition.
		}

		//Disconnect from the signature that called this method
		connections->Disconnect(ClientConnectTo.GetInstigatedFrom());
	}

	return true;
}

bool MasterConnection::ServerSendClient_Implementation (DString playerName, Int serverPort)
{
	NetworkSignature* client = nullptr;
	for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
	{
		if (NetworkSignature* signature = iter.GetSelectedSignature())
		{
			if (!signature->HasRole(NetworkSignature::NR_Client))
			{
				continue;
			}

			if (NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(signature->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
			{
				if (identifier->ReadPlayerName().Compare(playerName, DString::CC_CaseSensitive) == 0)
				{
					client = signature;
					break;
				}
			}
		}
	}

	if (client == nullptr)
	{
		//The player name is still a valid parameter, which is why this function is still returning true. The client may have disconnected between transit.
		return true;
	}

	ContainerUtils::AddUnique(OUT PlayersInTransit, playerName);

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	if (localSignature->HasRole(NetworkSignature::NR_MasterServer))
	{
		//Player is now on the server's list. The server needs to notify the client it's safe to connect to that address now.
		ClientConnectTo(client, serverPort);
	}
	else
	{
		/*
		Must be a server. Can't call ClientConnectTo from here since the clients do not have a reference to MasterConnection.
		MasterConnection is an Entity instantiated from a MasterServer. Clients connect to a server do not have a connection to the MasterServer.
		Without a connection to a MasterServer, they don't have a MasterConnection Entity.
		Instead, the servers will reference the MapGate object to tell clients to connect to the address.
		*/
		NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
		CHECK(localNightclub != nullptr && localNightclub->GetMap() != nullptr)
		NightclubMap* map = localNightclub->GetMap();
		MapGate* anyGate = map->FindMapObjectByClass<MapGate>();
		if (anyGate == nullptr)
		{
			NightclubLog.Log(LogCategory::LL_Warning, TXT("Unable to transfer client to a different server because there isn't a MapGate found within %s."), map->ToString());
			return true;
		}

		anyGate->ClientConnectToServer(client, serverPort);
	}

	//Don't disconnect the client since it still needs to process the RPC. Instead the client will disconnect when finished.
	//However there's the possibility that the client wouldn't disconnect on its own. So add a tick component that will cause a delayed disconnect to handle the case where they're still connected.
	TickComponent* delayedDisconnect = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
	if (client->AddComponent(delayedDisconnect))
	{
		delayedDisconnect->SetTickExHandler(SDFUNCTION_2PARAM(this, MasterConnection, HandleDelayedDisconnect, void, Float, TickComponent*));
		delayedDisconnect->SetTickInterval(10.f); //Clients have 10 seconds to disconnect
	}

	return true;
}

bool MasterConnection::MasterServerRemovePlayer_Implementation (const DString& playerName)
{
	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	for (size_t i = 0; i < localNetEngine->ReadAccessControls().size(); ++i)
	{
		if (DuplicateNameAccessControl* dupeNameCtrl = dynamic_cast<DuplicateNameAccessControl*>(localNetEngine->ReadAccessControls().at(i)))
		{
			dupeNameCtrl->RemoveNameInUse(playerName);
			break;
		}
	}

	return true;
}

void MasterConnection::HandleDelayedDisconnect (Float deltaSec, TickComponent* tick)
{
	if (NetworkSignature* tickOwner = dynamic_cast<NetworkSignature*>(tick->GetOwner()))
	{
		if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
		{
			connections->Disconnect(tickOwner);
		}
	}
}

void MasterConnection::HandleConnectionAccepted (NetworkSignature* remoteSignature)
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	
	if (!localSignature->HasRole(NetworkSignature::NR_MasterServer))
	{
		//Only the master server is maintaining the list of player names within the network
		return;
	}

	NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(remoteSignature->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
	if (clientId == nullptr)
	{
		return;
	}

	NetworkEngineComponent* localNetEngine = NetworkEngineComponent::Find();
	CHECK(localNetEngine != nullptr)
	for (AccessControl* accessCtrl : localNetEngine->ReadAccessControls())
	{
		if (DuplicateNameAccessControl* dupeNameCtrl = dynamic_cast<DuplicateNameAccessControl*>(accessCtrl))
		{
			dupeNameCtrl->AddNameInUse(clientId->ReadPlayerName());
			break;
		}
	}
}

//This event handler will remove the player name from the master server's Player Names In Use vector.
void MasterConnection::HandleDisconnect (NetworkSignature* remoteSignature)
{
	NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(remoteSignature->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
	if (clientId == nullptr)
	{
		//Not disconnecting from a client
		return;
	}

	size_t removeIdx = ContainerUtils::RemoveItem(OUT PlayersInTransit, clientId->ReadPlayerName());
	if (removeIdx != UINT_INDEX_NONE)
	{
		//The name was found in the transit vector. Don't notify the master server to remove from the player name list primarily because the client is still connected to the network.
		return;
	}

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)

	if (localSignature->HasRole(NetworkSignature::NR_MasterServer))
	{
		//This process has direct reference to the player name list
		MasterServerRemovePlayer.LocalExecute(clientId->ReadPlayerName());
	}
	else if (localSignature->HasRole(NetworkSignature::NR_Server))
	{
		//This server must notify the master server to remove the player from the name list.
		MasterServerRemovePlayer(BaseRpc::RT_MasterServers, clientId->ReadPlayerName());
	}
}
NC_END