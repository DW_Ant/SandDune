/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  OutdoorMap.cpp
=====================================================================
*/

#include "DanceComponent.h"
#include "NightclubEngineComponent.h"
#include "OutdoorMap.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::OutdoorMap, SD::NC::NightclubMap)
NC_BEGIN

void OutdoorMap::InitProps ()
{
	Super::InitProps();

	PlayerSpawn = Vector3(3500.f, 150.f, 0.f);
	MapSize = Vector2(7500.f, 5000.f); //75x50 meters
}

void OutdoorMap::BeginObject ()
{
	Super::BeginObject();

	CreateLandscape();
	CreateNightclub();
	CreateParkinglot();
	CreateRoad();
}

void OutdoorMap::InitializePlayer (Player* newPlayer)
{
	Super::InitializePlayer(newPlayer);

	//Disable drinking and dancing outdoors.
	newPlayer->SetCanDrink(false);
	if (DanceComponent* dance = newPlayer->GetDanceComp())
	{
		dance->SetEnabled(false);
	}
}

void OutdoorMap::CreateLandscape ()
{
	//Plain grass field that covers the entire field
	SceneTransformComponent* grassTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(grassTransform))
	{
		grassTransform->SetTranslation(Vector3(0.f, 0.5f, -100.f)); //behind everything

		ColorRenderComponent* grass = ColorRenderComponent::CreateObject();
		if (grassTransform->AddComponent(grass))
		{
			grass->SetPivot(0.5f, 0.5f);
			grass->SolidColor = Color(60, 165, 60);
			grass->SetBaseSize(MapSize * 3.f);
			RegisterRenderComp(grass);
		}
	}

	std::function<void(const Vector3&)> spawnTree([&](const Vector3& trunkLocation)
	{
		Float trunkHeight = 500.f;
		Float trunkDiameter = 75.f;

		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				Vector3 translation(0.f, 0.f, 10.f);
				translation += trunkLocation;
				transform->SetTranslation(translation);

				ColorRenderComponent* trunk = ColorRenderComponent::CreateObject();
				if (transform->AddComponent(trunk))
				{
					trunk->SetBaseSize(trunkDiameter, trunkHeight);
					trunk->SetPivot(0.5f, 1.f);
					trunk->SolidColor = Color(112, 72, 25);
					RegisterRenderComp(trunk);
				}

				PhysicsComponent* physComp = PhysicsComponent::CreateObject();
				if (transform->AddComponent(physComp))
				{
					physComp->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					physComp->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					physComp->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, trunkDiameter * 0.5f));
				}
			}
		}

		std::vector<Float> xPos({trunkDiameter * -2.f, -trunkDiameter, 0.f, trunkDiameter, trunkDiameter * 2.f});
		for (size_t i = 0; i < xPos.size(); ++i)
		{
			SceneTransformComponent* leafTransform = SceneTransformComponent::CreateObject();
			if (AddComponent(leafTransform))
			{
				Vector3 translation(xPos.at(i) + (trunkDiameter * 0.5f), -trunkHeight + RandomUtils::RandRange(-48.f, 48.f), 15.f);
				translation += trunkLocation;
				leafTransform->SetTranslation(translation);

				ColorRenderComponent* leaf = ColorRenderComponent::CreateObject();
				if (leafTransform->AddComponent(leaf))
				{
					leaf->SetBaseSize(RandomUtils::RandRange(196.f, 256.f), RandomUtils::RandRange(196.f, 256.f));
					leaf->SolidColor = Color(32, 128, 8);
					leaf->SetShape(ColorRenderComponent::ST_Circle);
					leaf->SetPivot(0.5f, 0.5f);
					RegisterRenderComp(leaf);
				}
			}
		}
	});

	std::function<void(const Vector3&)> spawnBoulder([&](const Vector3& boulderLocation)
	{
		for (int i = 0; i < 3; ++i)
		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				transform->SetTranslation(Vector3(RandomUtils::RandRange(-100.f, 100.f), RandomUtils::RandRange(-100.f, 100.f), 5.f));
				transform->EditTranslation() += boulderLocation;
				Float boulderRadius = RandomUtils::RandRange(100.f, 150.f);

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (transform->AddComponent(color))
				{
					color->SolidColor = Color(80, 80, 80);
					color->SetShape(ColorRenderComponent::ST_Circle);
					color->SetBaseSize(boulderRadius * 2.f, 1.f);
					color->SetPivot(0.5f, 0.5f);
					RegisterRenderComp(color);
				}

				PhysicsComponent* physComp = PhysicsComponent::CreateObject();
				if (transform->AddComponent(physComp))
				{
					physComp->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					physComp->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					physComp->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, boulderRadius));
				}
			}
		}
	});

	spawnTree(Vector3(-1024.f, -1024.f, 0.f));
	spawnTree(Vector3(-600.f, -1100.f, 0.f));
	spawnTree(Vector3(1024.f, -1500.f, 0.f));
	for (Float xPos = -4000.f; xPos < 4500.f; xPos += 850.f)
	{
		spawnTree(Vector3(xPos, 1850.f, 0.f));
	}

	spawnBoulder(Vector3(-1500.f, -256.f, 0.f));
	spawnBoulder(Vector3(-1200.f, -1200.f, 0.f));
}

void OutdoorMap::CreateNightclub ()
{
	Float buildingWidth = 3000.f;
	Float buildingHeight = 1500.f;
	Float trimThickness = 32.f;

	SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
	if (AddComponent(transform))
	{
		transform->SetTranslation(Vector3(500.f, -1500.f, 0.f));
			
		{
			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(35, 90, 180);
				color->SetBaseSize(Vector2(buildingWidth, buildingHeight));
				RegisterRenderComp(color);
			}
		}

		PhysicsComponent* collision = PhysicsComponent::CreateObject();
		if (transform->AddComponent(collision))
		{
			collision->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
			collision->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
			collision->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 1000.f,
			{
				Vector2(0.f, 0.f),
				Vector2(buildingWidth, 0.f),
				Vector2(buildingWidth, buildingHeight),
				Vector2(0.f, buildingHeight)
			}));
		}

		SceneTransformComponent* upperTrim = SceneTransformComponent::CreateObject();
		if (transform->AddComponent(upperTrim))
		{
			upperTrim->SetTranslation(Vector3(0.f, (buildingHeight * 0.33f), 5.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (upperTrim->AddComponent(color))
			{
				color->SolidColor = Color(75, 179, 211);
				color->SetBaseSize(Vector2(buildingWidth, trimThickness));
				color->SetPivot(Vector2(0.f, 1.f));
				RegisterRenderComp(color);
			}
		}

		SceneTransformComponent* leftTrim = SceneTransformComponent::CreateObject();
		if (transform->AddComponent(leftTrim))
		{
			leftTrim->SetTranslation(Vector3(buildingWidth * 0.25f, buildingHeight * 0.33f, 5.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (leftTrim->AddComponent(color))
			{
				color->SolidColor = Color(75, 179, 211);
				color->SetBaseSize(Vector2(trimThickness, buildingHeight * 0.67f));
				color->SetPivot(Vector2(0.5f, 0.f));
				RegisterRenderComp(color);
			}
		}

		SceneTransformComponent* rightTrim = SceneTransformComponent::CreateObject();
		if (transform->AddComponent(rightTrim))
		{
			rightTrim->SetTranslation(Vector3(buildingWidth * 0.75f, buildingHeight * 0.33f, 5.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (rightTrim->AddComponent(color))
			{
				color->SolidColor = Color(75, 179, 211);
				color->SetBaseSize(Vector2(trimThickness, buildingHeight * 0.67f));
				color->SetPivot(Vector2(0.5f, 0.f));
				RegisterRenderComp(color);
			}
		}

		SceneTransformComponent* doorway = SceneTransformComponent::CreateObject();
		if (transform->AddComponent(doorway))
		{
			doorway->SetTranslation(Vector3(buildingWidth * 0.5f, buildingHeight, 5.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (doorway->AddComponent(color))
			{
				color->SolidColor = Color::BLACK;
				color->SetBaseSize(Vector2(250.f, 350.f));
				color->SetPivot(Vector2(0.5f, 1.f));
				RegisterRenderComp(color);
			}
		}

		SceneTransformComponent* titleTransform = SceneTransformComponent::CreateObject();
		if (transform->AddComponent(titleTransform))
		{
			titleTransform->SetTranslation(Vector3(buildingWidth * 0.4f, buildingHeight * 0.55f, 5.f));

			TextRenderComponent* title = TextRenderComponent::CreateObject();
			if (titleTransform->AddComponent(title))
			{
				FontPool* localFontPool = FontPool::FindFontPool();
				CHECK(localFontPool != nullptr)

				title->CreateTextInstance(TXT("Syke Club"), localFontPool->GetDefaultFont(), 135);
				RegisterRenderComp(title);
			}
		}
	}

	//Create dumpster object
	SceneTransformComponent* dumpsterTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(dumpsterTransform))
	{
		Vector2 dumpsterSize(200.f, 350.f);
		dumpsterTransform->SetTranslation(Vector3(425.f, -400.f, 0.f));

		ColorRenderComponent* greenFrame = ColorRenderComponent::CreateObject();
		if (dumpsterTransform->AddComponent(greenFrame))
		{
			greenFrame->SetPivot(Vector2(1.f, 0.f));
			greenFrame->SolidColor = Color(32, 96, 40);
			greenFrame->SetBaseSize(dumpsterSize);
			RegisterRenderComp(greenFrame);
		}

		PhysicsComponent* phys = PhysicsComponent::CreateObject();
		if (dumpsterTransform->AddComponent(phys))
		{
			phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
			phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
			phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 1000.f,
			{
				Vector2::ZERO_VECTOR,
				Vector2(0.f, dumpsterSize.Y),
				Vector2(-dumpsterSize.X, dumpsterSize.Y),
				Vector2(-dumpsterSize.X, 0.f)
			}));
		}

		SceneTransformComponent* openingTransform = SceneTransformComponent::CreateObject();
		if (dumpsterTransform->AddComponent(openingTransform))
		{
			openingTransform->SetTranslation(Vector3(-dumpsterSize.X, 0.f, 3.f));

			ColorRenderComponent* opening = ColorRenderComponent::CreateObject();
			if (openingTransform->AddComponent(opening))
			{
				opening->SetBaseSize(Vector2(dumpsterSize.X * 0.5f, 32.f));
				opening->SolidColor = Color::BLACK;
				RegisterRenderComp(opening);
			}
		}
	}

	//Create Bob the Bouncer
	SceneTransformComponent* bobTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(bobTransform))
	{
		bobTransform->SetTranslation(Vector3(2000.f, 150.f, 5.f));

		ColorRenderComponent* bobColor = ColorRenderComponent::CreateObject();
		if (bobTransform->AddComponent(bobColor))
		{
			bobColor->SetShape(ColorRenderComponent::ST_Circle);
			bobColor->SetPivot(Vector2(0.5f, 0.5f));
			bobColor->SolidColor = Color(150, 150, 48);
			bobColor->SetBaseSize(Vector2(200.f, 200.f));
			RegisterRenderComp(bobColor);
		}

		SceneTransformComponent* textTransform = SceneTransformComponent::CreateObject();
		if (bobTransform->AddComponent(textTransform))
		{
			textTransform->SetTranslation(Vector3(0.f, 0.f, 1.f));

			TextRenderComponent* bobTitle = TextRenderComponent::CreateObject();
			if (textTransform->AddComponent(bobTitle))
			{
				FontPool* localFontPool = FontPool::FindFontPool();
				CHECK(localFontPool != nullptr)

				TextRenderComponent::STextData& textData = bobTitle->CreateTextInstance(TXT("Bob the\nBouncer"), localFontPool->GetDefaultFont(), 96);
				textData.DrawOffset = sf::Vector2f(-128.f, -128.f);
				RegisterRenderComp(bobTitle);
			}
		}

		PhysicsComponent* bobInteraction = PhysicsComponent::CreateObject();
		if (bobTransform->AddComponent(bobInteraction))
		{
			bobInteraction->SetOverlappingChannels(NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
			bobInteraction->SetBeginOverlap(SDFUNCTION_2PARAM(this, OutdoorMap, HandleInteractBob, void, PhysicsComponent*, PhysicsComponent*));
			bobInteraction->SetEndOverlap(SDFUNCTION_2PARAM(this, OutdoorMap, HandleLeaveBob, void, PhysicsComponent*, PhysicsComponent*));
			bobInteraction->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 250.f));
		}
	}
}

void OutdoorMap::CreateParkinglot ()
{
	//Create the parkinglot
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			Vector2 lotSize = Vector2(2048.f, 3000.f);
			transform->SetTranslation(Vector3(4000.f, -2500.f, -5.f));

			ColorRenderComponent* baseLot = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(baseLot))
			{
				baseLot->SetBaseSize(lotSize);
				baseLot->SolidColor = Color(16, 16, 16);
				RegisterRenderComp(baseLot);
			}

			Float parkingWidth = 250.f;
			for (Float yPos = parkingWidth; yPos < lotSize.Y; yPos += parkingWidth)
			{
				SceneTransformComponent* dividerTransform = SceneTransformComponent::CreateObject();
				if (transform->AddComponent(dividerTransform))
				{
					dividerTransform->SetTranslation(Vector3(0.f, yPos, 2.f));

					ColorRenderComponent* dividerColor = ColorRenderComponent::CreateObject();
					if (dividerTransform->AddComponent(dividerColor))
					{
						dividerColor->SolidColor = Color(225, 225, 225);
						dividerColor->SetBaseSize(Vector2(450.f, 16.f));
						RegisterRenderComp(dividerColor);
					}
				}
			}
		}
	}

	//Create path to building
	{
		SceneTransformComponent* pathTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(pathTransform))
		{
			Vector2 pathSize(2500.f, 250.f);
			pathTransform->SetTranslation(Vector3(1900.f, 0.f, -10.f));

			ColorRenderComponent* pathColor = ColorRenderComponent::CreateObject();
			if (pathTransform->AddComponent(pathColor))
			{
				pathColor->SolidColor = Color(96, 96, 96);
				pathColor->SetBaseSize(pathSize);
				RegisterRenderComp(pathColor);
			}

			Float spacing = 250.f;
			for (Float xPos = spacing; xPos < pathSize.X; xPos += spacing)
			{
				SceneTransformComponent* dividerTransform = SceneTransformComponent::CreateObject();
				if (pathTransform->AddComponent(dividerTransform))
				{
					dividerTransform->SetTranslation(Vector3(xPos, 0.f, 1.f));

					ColorRenderComponent* dividerColor = ColorRenderComponent::CreateObject();
					if (dividerTransform->AddComponent(dividerColor))
					{
						dividerColor->SolidColor = Color(48, 48, 48);
						dividerColor->SetBaseSize(Vector2(6.f, pathSize.Y));
						RegisterRenderComp(dividerColor);
					}
				}
			}
		}
	}
}

void OutdoorMap::CreateRoad ()
{
	Vector2 roadSize(15000.f, 700.f);

	//Create the road with lane dividers
	{
		SceneTransformComponent* roadTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(roadTransform))
		{
			roadTransform->SetTranslation(Vector3(-5000.f, 850.f, -10.f));

			ColorRenderComponent* roadColor = ColorRenderComponent::CreateObject();
			if (roadTransform->AddComponent(roadColor))
			{
				roadColor->SetBaseSize(roadSize);
				roadColor->SolidColor = Color(16, 16, 16);
				RegisterRenderComp(roadColor);
			}
		}

		Float lineLength = 300.f;
		for (Float xPos = lineLength * 0.5f; xPos < roadSize.X; xPos += (lineLength * 1.5f))
		{
			SceneTransformComponent* lineTransform = SceneTransformComponent::CreateObject();
			if (roadTransform->AddComponent(lineTransform))
			{
				lineTransform->SetTranslation(Vector3(xPos, roadSize.Y * 0.5f, 1.f));

				ColorRenderComponent* lineColor = ColorRenderComponent::CreateObject();
				if (lineTransform->AddComponent(lineColor))
				{
					lineColor->SetPivot(Vector2(0.f, 0.5f));
					lineColor->SolidColor = Color(190, 190, 48);
					lineColor->SetBaseSize(lineLength, 12.f);
					RegisterRenderComp(lineColor);
				}
			}
		}
	}

	//Create street lights
	{
		Float xPos = -5000.f;
		Color color(105, 105, 125);
		Float poleHeight = 1000.f;
		Float poleRadius = 10.f;

		//Create upper poles
		for (size_t i = 0; i < 6; ++i)
		{
			SceneTransformComponent* lightTransform = SceneTransformComponent::CreateObject();
			if (AddComponent(lightTransform))
			{
				lightTransform->SetTranslation(Vector3(xPos, 650.f, 10.f));

				ColorRenderComponent* poleColor = ColorRenderComponent::CreateObject();
				if (lightTransform->AddComponent(poleColor))
				{
					poleColor->SolidColor = color;
					poleColor->SetBaseSize(Vector2(poleRadius * 2.f, poleHeight));
					poleColor->SetPivot(Vector2(0.5f, 1.f));
					RegisterRenderComp(poleColor);
				}

				SceneTransformComponent* neckTransform = SceneTransformComponent::CreateObject();
				if (lightTransform->AddComponent(neckTransform))
				{
					neckTransform->SetTranslation(Vector3(0.f, -poleHeight, 0.f));
					neckTransform->SetRotation(Rotator(-35.f, 0.f, 0.f, Rotator::RU_Degrees));

					ColorRenderComponent* neckColor = ColorRenderComponent::CreateObject();
					if (neckTransform->AddComponent(neckColor))
					{
						neckColor->SolidColor = color;
						neckColor->SetBaseSize(Vector2(poleRadius * 1.5f, poleHeight * 0.5f));
						RegisterRenderComp(neckColor);
					}
				}

				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (lightTransform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, poleRadius));
				}
			}

			xPos += 2048.f;
		}

		//Create lower poles
		xPos = -4000.f;
		for (size_t i = 0; i < 6; ++i)
		{
			SceneTransformComponent* lightTransform = SceneTransformComponent::CreateObject();
			if (AddComponent(lightTransform))
			{
				lightTransform->SetTranslation(Vector3(xPos, 1800.f, 10.f));

				ColorRenderComponent* poleColor = ColorRenderComponent::CreateObject();
				if (lightTransform->AddComponent(poleColor))
				{
					poleColor->SolidColor = color;
					poleColor->SetBaseSize(Vector2(poleRadius * 2.f, poleHeight));
					poleColor->SetPivot(Vector2(0.5f, 1.f));
					RegisterRenderComp(poleColor);
				}

				SceneTransformComponent* neckTransform = SceneTransformComponent::CreateObject();
				if (lightTransform->AddComponent(neckTransform))
				{
					neckTransform->SetTranslation(Vector3(0.f, -poleHeight, 0.f));
					neckTransform->SetRotation(Rotator(150.f, 0.f, 0.f, Rotator::RU_Degrees));

					ColorRenderComponent* neckColor = ColorRenderComponent::CreateObject();
					if (neckTransform->AddComponent(neckColor))
					{
						neckColor->SolidColor = color;
						neckColor->SetBaseSize(Vector2(poleRadius * 1.5f, poleHeight * 0.5f));
						RegisterRenderComp(neckColor);
					}
				}

				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (lightTransform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, poleRadius));
				}
			}

			xPos += 2048.f;
		}
	}
}

void OutdoorMap::HandleInteractBob (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)
	localNightclub->DisplayEnterClubPrompt();
}

void OutdoorMap::HandleLeaveBob (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)
	localNightclub->CloseEnterClubPrompt();
}
NC_END