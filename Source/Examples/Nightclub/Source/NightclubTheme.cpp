/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubTheme.cpp
=====================================================================
*/

#include "NightclubTheme.h"

IMPLEMENT_CLASS(SD::NC::NightclubTheme, SD::GuiTheme)
NC_BEGIN

const DString NightclubTheme::NIGHTCLUB_STYLE_NAME(TXT("NIGHTCLUB_STYLE"));

void NightclubTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	InitializeNightclubStyle();
}

void NightclubTheme::InitializeNightclubStyle ()
{
	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	CHECK(defaultStyle != nullptr)

	SStyleInfo chatStyle = CreateStyleFrom(*defaultStyle);
	chatStyle.Name = NIGHTCLUB_STYLE_NAME;

	sf::Color fontColor = sf::Color(227, 209, 233);

	ButtonComponent* buttonTemplate = dynamic_cast<ButtonComponent*>(FindTemplate(&chatStyle, ButtonComponent::SStaticClass()));
	if (buttonTemplate != nullptr)
	{
		if (buttonTemplate->GetCaptionComponent() != nullptr && buttonTemplate->GetCaptionComponent()->GetRenderComponent() != nullptr)
		{
			buttonTemplate->GetCaptionComponent()->GetRenderComponent()->SetFontColor(fontColor);
		}

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(buttonTemplate->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(146, 28, 113));
			colorState->SetDownColor(Color(102, 19, 79));
			colorState->SetDisabledColor(Color(50, 9, 39));
			colorState->SetHoverColor(Color(60, 28, 146));
		}
	}

	FrameComponent* frameTemplate = dynamic_cast<FrameComponent*>(FindTemplate(&chatStyle, FrameComponent::SStaticClass()));
	if (frameTemplate != nullptr)
	{
		if (BorderRenderComponent* borderComp = frameTemplate->GetBorderComp())
		{
			borderComp->Destroy();
		}

		frameTemplate->SetBorderThickness(1.5f);
		frameTemplate->SetCenterColor(Color(85, 20, 104));
	}

	LabelComponent* labelTemplate = dynamic_cast<LabelComponent*>(FindTemplate(&chatStyle, LabelComponent::SStaticClass()));
	if (labelTemplate != nullptr && labelTemplate->GetRenderComponent() != nullptr)
	{
		labelTemplate->GetRenderComponent()->SetFontColor(fontColor);
	}

	ListBoxComponent* listTemplate = dynamic_cast<ListBoxComponent*>(FindTemplate(&chatStyle, ListBoxComponent::SStaticClass()));
	if (listTemplate != nullptr)
	{
		listTemplate->SetSelectionColor(Color(60, 28, 146, 150));
		listTemplate->SetHoverColor(Color(118, 96, 178, 80));

		if (FrameComponent* frame = listTemplate->GetBackground())
		{
			frame->SetCenterColor(Color(85, 20, 104));
		}

		if (listTemplate->GetItemListText() != nullptr && listTemplate->GetItemListText()->GetRenderComponent() != nullptr)
		{
			listTemplate->GetItemListText()->GetRenderComponent()->SetFontColor(fontColor);
		}
	}

	ScrollbarComponent* scrollbarTemplate = dynamic_cast<ScrollbarComponent*>(FindTemplate(&chatStyle, ScrollbarComponent::SStaticClass()));
	if (scrollbarTemplate != nullptr)
	{
		scrollbarTemplate->SetZoomEnabled(false);
		scrollbarTemplate->SetEnabledThumbColor(Color(216, 123, 190));
		scrollbarTemplate->SetThumbDragColor(Color(172, 98, 152));
		scrollbarTemplate->SetEnabledTrackColor(Color(68, 39, 60));
	}

	TextFieldComponent* textFieldTemplate = dynamic_cast<TextFieldComponent*>(FindTemplate(&chatStyle, TextFieldComponent::SStaticClass()));
	if (textFieldTemplate != nullptr)
	{
		if (FrameComponent* background = textFieldTemplate->GetBackgroundComponent())
		{
			background->SetCenterColor(Color(54, 25, 113, 64));
			if (BorderRenderComponent* borderComp = background->GetBorderComp())
			{
				borderComp->SetBorderTexture(nullptr);
				borderComp->SetBorderColor(Color(75, 32, 135, 64));
			}
		}

		if (TextFieldRenderComponent* renderComp = dynamic_cast<TextFieldRenderComponent*>(textFieldTemplate->GetRenderComponent()))
		{
			renderComp->SetFontColor(fontColor);
			renderComp->CursorColor = fontColor;
			renderComp->SetHighlightColor(Color(0, 20, 120, 200));
		}
	}

	Styles.emplace_back(chatStyle);
}
NC_END