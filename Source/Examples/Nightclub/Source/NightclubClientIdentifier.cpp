/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubClientIdentifier.cpp
=====================================================================
*/

#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"

IMPLEMENT_CLASS(SD::NC::NightclubClientIdentifier, SD::NetworkIdentifier)
NC_BEGIN

void NightclubClientIdentifier::InitProps ()
{
	Super::InitProps();

	PlayerName = DString::EmptyString;
}

bool NightclubClientIdentifier::InitializeIdToCurSystem (NetworkSignature::ENetworkRole networkRoles)
{
	return true; //PlayerName will be assigned whenever the client sets it in the Gui.
}

bool NightclubClientIdentifier::ReadDataFromBuffer (const DataBuffer& data)
{
	return !(data >> PlayerName).HasReadError();
}

void NightclubClientIdentifier::WriteDataToBuffer (DataBuffer& outData)
{
	outData << PlayerName;
}

void NightclubClientIdentifier::SetPlayerName (const DString& newPlayerName)
{
	PlayerName = newPlayerName;
}
NC_END