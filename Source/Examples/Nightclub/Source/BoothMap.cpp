/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BoothMap.cpp
=====================================================================
*/

#include "BoothMap.h"
#include "DanceComponent.h"
#include "DanceFloorMap.h"
#include "MapGate.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::BoothMap, SD::NC::NightclubMap)
NC_BEGIN

void BoothMap::InitProps ()
{
	Super::InitProps();

	PlayerSpawn = Vector3(-512.f, 0.f, 0.f);
	MapSize = Vector2(4000.f, 4000.f); //40x40 meters

	bCreatedExitPoint = false;
}

void BoothMap::BeginObject ()
{
	Super::BeginObject();

	CreateRoom();
	CreateBooths();
	CreateDartBoards();
}

void BoothMap::InitializePlayer (Player* newPlayer)
{
	Super::InitializePlayer(newPlayer);

	//Disable dancing
	if (newPlayer->GetDanceComp() != nullptr)
	{
		newPlayer->GetDanceComp()->SetEnabled(false);
	}
}

void BoothMap::HandleNetInitialize (NetworkSignature* connectedTo)
{
	Super::HandleNetInitialize(connectedTo);

	CreateExitPoint();
}

void BoothMap::CreateRoom ()
{
	//Create floor
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, 0.f, -100.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(64, 64, 64);
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SetBaseSize(MapSize);
				RegisterRenderComp(color);
			}
		}
	}

	//Create back wall
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, MapSize.Y * -0.5f, -50.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(61, 90, 122);
				color->SetPivot(Vector2(0.5f, 1.f));
				color->SetBaseSize(Vector2(MapSize.X, 1500.f));
				RegisterRenderComp(color);
			}
		}
	}

	//Create pillars
	{
		std::vector<Vector3> pillarLocations
		{
			Vector3(450.f, -1850.f, 100.f),
			Vector3(550.f, -600.f, 110.f),
			Vector3(650.f, 600.f, 120.f),
			Vector3(750.f, 2000.f, 130.f),
			Vector3(1600.f, -1850.f, 140.f),
			Vector3(1700.f, -600.f, 150.f),
			Vector3(1800.f, 600.f, 160.f),
			Vector3(1900.f, 2000.f, 170)
		};

		for (size_t i = 0; i < pillarLocations.size(); ++i)
		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				transform->SetTranslation(pillarLocations.at(i));

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (transform->AddComponent(color))
				{
					color->SolidColor = (Int(i).IsEven()) ? Color(137, 34, 191) : Color(117, 24, 175);
					color->SetPivot(Vector2(0.5f, 1.f));
					color->SetBaseSize(Vector2(150.f, 1500.f));
					RegisterRenderComp(color);
				}

				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (transform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 75.f));
				}
			}
		}
	}
}

void BoothMap::CreateBooths ()
{
	//Create the three booths on the right
	{
		std::vector<Vector3> boothLocations
		{
			Vector3(1000.f, MapSize.Y * -0.25f, -10.f),
			Vector3(1000.f, 0.f, -10.f),
			Vector3(1000.f, MapSize.Y * 0.25f, -10.f)
		};

		Color floorColor(64, 64, 64);

		//Create the booth "background"
		SceneTransformComponent* backTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(backTransform))
		{
			CHECK(!ContainerUtils::IsEmpty(boothLocations));
			backTransform->SetTranslation(Vector3(boothLocations.at(0).X, 0.f, boothLocations.at(0).Z - 5.f));

			ColorRenderComponent* backColor = ColorRenderComponent::CreateObject();
			if (backTransform->AddComponent(backColor))
			{
				backColor->SolidColor = Color::GREEN;
				backColor->SetPivot(Vector2(0.f, 0.5f));
				backColor->SetBaseSize((MapSize.X * 0.5f) - backTransform->ReadTranslation().X, MapSize.Y);
				RegisterRenderComp(backColor);
			}
		}

		//Add collision at the top and bottom parts
		{
			SceneTransformComponent* backgroundTransform = SceneTransformComponent::CreateObject();
			if (AddComponent(backgroundTransform))
			{
				CHECK(!ContainerUtils::IsEmpty(boothLocations))
				backgroundTransform->SetTranslation(Vector3(boothLocations.at(0).X, 0.f, 0.f));

				std::vector<Vector3> topLeftCorners
				{
					Vector3(0.f, MapSize.Y * -0.5f, 0.f),
					Vector3(0.f, (MapSize.Y * 0.5f) - 512.f, 0.f)
				};

				for (const Vector3& collisionLocation : topLeftCorners)
				{
					PhysicsComponent* phys = PhysicsComponent::CreateObject();
					if (backgroundTransform->AddComponent(phys))
					{
						phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
						phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
						phys->AddShape(new CollisionExtrudePolygon(1.f, collisionLocation, 256.f,
						{
							Vector2::ZERO_VECTOR,
							Vector2(512.f, 0.f),
							Vector2(512.f, 512.f),
							Vector2(0.f, 512.f)
						}));
					}
				}
			}
		}

		for (const Vector3& location : boothLocations)
		{
			SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
			if (AddComponent(transform))
			{
				transform->SetTranslation(location);

				//Create the seating
				{
					Float seatRadius = 450.f;
					ColorRenderComponent* color = ColorRenderComponent::CreateObject();
					if (transform->AddComponent(color))
					{
						color->SolidColor = Color(237, 123, 145);
						color->SetShape(ColorRenderComponent::ST_Circle);
						color->SetBaseSize(Vector2(seatRadius * 2.f, seatRadius * 2.f));
						color->SetPivot(Vector2(0.5f, 0.5f));
						RegisterRenderComp(color);
					}

					PhysicsComponent* phys = PhysicsComponent::CreateObject();
					if (transform->AddComponent(phys))
					{
						phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
						phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
						phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 256.f,
						{
							//Create a reversed C shape
							Vector2(0.f, -seatRadius * 1.25f),
							Vector2(seatRadius * 1.25f, -seatRadius * 1.25f),
							Vector2(seatRadius * 1.25f, seatRadius * 1.25f),
							Vector2(0.f, seatRadius * 1.25f),
							Vector2(0.f, seatRadius),
							Vector2(seatRadius, seatRadius),
							Vector2(seatRadius, -seatRadius),
							Vector2(0.f, -seatRadius)
						}));
					}

					//Hollow out the seating
					SceneTransformComponent* hollowTransform = SceneTransformComponent::CreateObject();
					if (transform->AddComponent(hollowTransform))
					{
						hollowTransform->SetTranslation(Vector3(0.f, 0.f, 1.f));

						ColorRenderComponent* hollowColor = ColorRenderComponent::CreateObject();
						if (hollowTransform->AddComponent(hollowColor))
						{
							hollowColor->SolidColor = floorColor;
							hollowColor->SetPivot(Vector2(0.5f, 0.5f));
							hollowColor->SetShape(ColorRenderComponent::ST_Circle);
							hollowColor->SetBaseSize(Vector2(seatRadius, seatRadius));
							RegisterRenderComp(hollowColor);
						}

						//Cut out the edge of the seats to make it a reversed C shape.
						ColorRenderComponent* cutoutColor = ColorRenderComponent::CreateObject();
						if (hollowTransform->AddComponent(cutoutColor))
						{
							cutoutColor->SolidColor = floorColor;
							cutoutColor->SetBaseSize(Vector2(seatRadius, seatRadius * 2.f));
							cutoutColor->SetPivot(Vector2(1.f, 0.5f));
							RegisterRenderComp(cutoutColor);
						}

						//Create the table
						SceneTransformComponent* tableTransform = SceneTransformComponent::CreateObject();
						if (hollowTransform->AddComponent(tableTransform))
						{
							tableTransform->SetTranslation(Vector3(0.f, 0.f, 1.f));

							ColorRenderComponent* tableColor = ColorRenderComponent::CreateObject();
							if (tableTransform->AddComponent(tableColor))
							{
								tableColor->SolidColor = Color(237, 134, 9);
								tableColor->SetShape(ColorRenderComponent::ST_Circle);
								tableColor->SetBaseSize(Vector2(seatRadius * 0.5f, seatRadius * 0.5f));
								tableColor->SetPivot(Vector2(0.5f, 0.5f));
								RegisterRenderComp(tableColor);
							}

							PhysicsComponent* tableCollision = PhysicsComponent::CreateObject();
							if (tableTransform->AddComponent(tableCollision))
							{
								tableCollision->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
								tableCollision->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
								tableCollision->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, seatRadius * 0.25f));
							}

							//Create bottles on the table
							{
								Int numBottles = RandomUtils::Rand(4);
								for (Int i = 0; i < numBottles; ++i)
								{
									SceneTransformComponent* bottleTransform = SceneTransformComponent::CreateObject();
									if (tableTransform->AddComponent(bottleTransform))
									{
										Vector2 randLocation = RandomUtils::RandPointWithinCircle(seatRadius * 0.225f);

										//displace it a bit since the pivot point is the point between the neck and the base
										randLocation.Y -= 48.f;
										bottleTransform->SetTranslation(Vector3(randLocation.X, randLocation.Y, 1.f));

										ColorRenderComponent* baseBottle = ColorRenderComponent::CreateObject();
										if (bottleTransform->AddComponent(baseBottle))
										{
											baseBottle->SolidColor = Color(37, 186, 21, 175);
											baseBottle->SetBaseSize(Vector2(48.f, 64.f));
											baseBottle->SetPivot(Vector2(0.5f, 0.f));
											RegisterRenderComp(baseBottle);
										}

										ColorRenderComponent* bottleNeck = ColorRenderComponent::CreateObject();
										if (bottleTransform->AddComponent(bottleNeck))
										{
											bottleNeck->SolidColor = Color(37, 186, 21, 175);
											bottleNeck->SetBaseSize(Vector2(24.f, 48.f));
											bottleNeck->SetPivot(Vector2(0.5f, 1.f));
											RegisterRenderComp(bottleNeck);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	//Create the three booths at the bottom.
	{
		std::vector<Vector3> tableLocations
		{
			Vector3(-1500.f, MapSize.Y * 0.5f, -5.f),
			Vector3(-750.f, MapSize.Y * 0.5f, -5.f),
			Vector3(0.f, MapSize.Y * 0.5f, -5.f)
		};

		Float tableRadius = 187.f;

		for (const Vector3& tableLocation : tableLocations)
		{
			SceneTransformComponent* tableTransform = SceneTransformComponent::CreateObject();
			if (AddComponent(tableTransform))
			{
				tableTransform->SetTranslation(tableLocation);

				ColorRenderComponent* tableColor = ColorRenderComponent::CreateObject();
				if (tableTransform->AddComponent(tableColor))
				{
					tableColor->SolidColor = Color(237, 134, 9);
					tableColor->SetPivot(Vector2(0.5f, 0.5f));
					tableColor->SetShape(ColorRenderComponent::ST_Circle);
					tableColor->SetBaseSize(Vector2(tableRadius * 2.f, tableRadius * 2.f));
					RegisterRenderComp(tableColor);
				}

				PhysicsComponent* phys = PhysicsComponent::CreateObject();
				if (tableTransform->AddComponent(phys))
				{
					phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
					phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, tableRadius));
				}
			}
		}

		//Create a black box to cut off the tables out of view
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, MapSize.Y * 0.5f, 0.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color::BLACK;
				color->SetPivot(Vector2(0.5f, 0.f));
				color->SetBaseSize(MapSize);
				RegisterRenderComp(color);
			}
		}
	}
}

void BoothMap::CreateDartBoards ()
{
	std::vector<Vector3> dartLocations
	{
		Vector3(-1500.f, MapSize.Y * -0.6f, -10.f),
		Vector3(-1000.f, MapSize.Y * -0.6f, -10.f),
		Vector3(-500.f, MapSize.Y * -0.6f, -10.f)
	};

	Color blueRingColor(81, 81, 237);
	Color whiteRingColor(230, 230, 230);
	for (const Vector3& location : dartLocations)
	{
		SceneTransformComponent* rootTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(rootTransform))
		{
			rootTransform->SetTranslation(location);
			SceneTransformComponent* prevTransform = rootTransform;
			Float diameter = 150.f;

			//Create four rings of alternating colors
			for (Int i = 0; i < 4; ++i)
			{
				SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
				if (prevTransform->AddComponent(transform))
				{
					transform->SetTranslation(Vector3(0.f, 0.f, 1.f));
					prevTransform = transform;

					ColorRenderComponent* ring = ColorRenderComponent::CreateObject();
					if (transform->AddComponent(ring))
					{
						ring->SolidColor = (i.IsEven()) ? blueRingColor : whiteRingColor;
						ring->SetShape(ColorRenderComponent::ST_Circle);
						ring->SetPivot(Vector2(0.5f, 0.5f));
						ring->SetBaseSize(diameter, diameter);
						RegisterRenderComp(ring);
					}

					diameter -= 30.f;
				}
			}

			SceneTransformComponent* bullsEyeTransform = SceneTransformComponent::CreateObject();
			if (prevTransform->AddComponent(bullsEyeTransform))
			{
				bullsEyeTransform->SetTranslation(Vector3(0.f, 0.f, 1.f));

				ColorRenderComponent* bullsEye = ColorRenderComponent::CreateObject();
				if (bullsEyeTransform->AddComponent(bullsEye))
				{
					bullsEye->SolidColor = Color(200, 0, 0);
					bullsEye->SetShape(ColorRenderComponent::ST_Circle);
					bullsEye->SetBaseSize(diameter, diameter);
					bullsEye->SetPivot(Vector2(0.5f, 0.5f));
					RegisterRenderComp(bullsEye);
				}
			}
		}
	}
}

void BoothMap::CreateExitPoint ()
{
	if (!bCreatedExitPoint && RoleComp != nullptr && RoleComp->HasAuthority())
	{
		MapGate* toDanceFloor = MapGate::CreateObject();
		toDanceFloor->SetTranslation(Vector3(-1660.f, 0.f, -5.f));
		toDanceFloor->MapTextKey = TXT("BoothToDanceFloor");
		toDanceFloor->DrawOffset = Vector2(-320.f, -256.f);
		toDanceFloor->PromptTextKey = TXT("BoothToDanceFloorPrompt");
		toDanceFloor->SetDestination(DanceFloorMap::SStaticClass());
		MapObjects.push_back(toDanceFloor);

		bCreatedExitPoint = true;
	}
}
NC_END