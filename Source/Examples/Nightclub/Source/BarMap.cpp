/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BarMap.cpp
=====================================================================
*/

#include "BarMap.h"
#include "DanceFloorMap.h"
#include "MapGate.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::BarMap, SD::NC::NightclubMap)
NC_BEGIN

void BarMap::InitProps ()
{
	Super::InitProps();

	PlayerSpawn = Vector3(512.f, 0.f, 0.f);
	MapSize = Vector2(3500.f, 5000.f); //35x50 meters

	bCreatedExitPoint = false;
}

void BarMap::BeginObject ()
{
	Super::BeginObject();

	CreateBar();
	CreateGameObjects();
}

void BarMap::HandleNetInitialize (NetworkSignature* connectedTo)
{
	Super::HandleNetInitialize(connectedTo);

	CreateExitPoint();
}

void BarMap::CreateBar ()
{
	//Create base floor
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(0.f, 0.f, -100.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = Color(8, 69, 71);
				color->SetBaseSize(MapSize);
				color->SetPivot(0.5f, 0.5f);
				RegisterRenderComp(color);
			}
		}
	}

	//Create backwall
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(-1750.f, 0.f, 5.f));
			Vector2 wallSize(256.f, MapSize.Y * 0.9f);

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SetBaseSize(wallSize);
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SolidColor = Color(188, 136, 45);
				RegisterRenderComp(color);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 256.f,
				{
					Vector2(wallSize.X * -0.5f, wallSize.Y * 0.5f),
					Vector2(wallSize.X * 0.5f, wallSize.Y * 0.5f),
					Vector2(wallSize.X * 0.5f, wallSize.Y * -0.5f),
					Vector2(wallSize.X * -0.5f, wallSize.Y * -0.5f)
				}));
			}
		}
	}

	//Create bar
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(-800.f, 0.f, 5.f));
			Vector2 barSize(512.f, MapSize.Y * 0.67f);

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SetBaseSize(barSize);
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SolidColor = Color(164, 52, 186);
				RegisterRenderComp(color);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 256.f,
				{
					Vector2(barSize.X * -0.5f, barSize.Y * 0.5f),
					Vector2(barSize.X * 0.5f, barSize.Y * 0.5f),
					Vector2(barSize.X * 0.5f, barSize.Y * -0.5f),
					Vector2(barSize.X * -0.5f, barSize.Y * -0.5f)
				}));
			}

			std::vector<Vector3> coasterLocations(
			{
				Vector3(barSize.X * 0.35f, barSize.Y * -0.45f, 1.f),
				Vector3(barSize.X * 0.33f, barSize.Y * -0.2f, 1.f),
				Vector3(barSize.X * 0.335f, barSize.Y * 0.15f, 1.f),
				Vector3(barSize.X * 0.32f, barSize.Y * 0.4f, 1.f)
			});

			for (size_t i = 0; i < coasterLocations.size(); ++i)
			{
				SceneTransformComponent* coasterTransform = SceneTransformComponent::CreateObject();
				if (transform->AddComponent(coasterTransform))
				{
					coasterTransform->SetTranslation(coasterLocations.at(i));

					ColorRenderComponent* coasterColor = ColorRenderComponent::CreateObject();
					if (coasterTransform->AddComponent(coasterColor))
					{
						coasterColor->SetShape(ColorRenderComponent::ST_Circle);
						coasterColor->SolidColor = Color(212, 159, 64);
						coasterColor->SetPivot(Vector2(0.5f, 0.5f));
						coasterColor->SetBaseSize(Vector2(80.f, 80.f));
						RegisterRenderComp(coasterColor);
					}
				}
			}

			std::vector<Vector3> bottleLocations(
			{
				Vector3(barSize.X * -0.15f, barSize.Y * -0.35f, 1.f),
				Vector3(barSize.X * 0.1f, barSize.Y * -0.05f, 1.f),
				Vector3(barSize.X * -0.35f, barSize.Y * 0.3f, 1.f)
			});

			for (size_t i = 0; i < bottleLocations.size(); ++i)
			{
				SceneTransformComponent* baseBottleTransform = SceneTransformComponent::CreateObject();
				if (transform->AddComponent(baseBottleTransform))
				{
					baseBottleTransform->SetTranslation(bottleLocations.at(i));
					Vector2 bottleBaseSize(96.f, 220.f);

					ColorRenderComponent* bottleColor = ColorRenderComponent::CreateObject();
					if (baseBottleTransform->AddComponent(bottleColor))
					{
						bottleColor->SetBaseSize(bottleBaseSize);
						bottleColor->SetPivot(Vector2(0.5f, 1.f));
						bottleColor->SolidColor = Color(37, 186, 21);
						RegisterRenderComp(bottleColor);
					}

					SceneTransformComponent* neckBottleTransform = SceneTransformComponent::CreateObject();
					if (baseBottleTransform->AddComponent(neckBottleTransform))
					{
						neckBottleTransform->SetTranslation(Vector3(0.f, -bottleBaseSize.Y, 0.f));

						ColorRenderComponent* neckColor = ColorRenderComponent::CreateObject();
						if (neckBottleTransform->AddComponent(neckColor))
						{
							neckColor->SetBaseSize(bottleBaseSize * Vector2(0.5f, 0.33f));
							neckColor->SetPivot(Vector2(0.5f, 1.f));
							neckColor->SolidColor = Color(37, 186, 21);
							RegisterRenderComp(neckColor);
						}
					}
				}
			}
			
		}
	}

	//Create barstools
	std::vector<Vector3> barStoolLocations(
	{
		Vector3(-200.f, -1250.f, 10.f),
		Vector3(-250.f, -800.f, 10.f),
		Vector3(-130.f, -250.f, 10.f),
		Vector3(-100.f, 350.f, 10.f),
		Vector3(-200.f, 900.f, 10.f),
		Vector3(-300.f, 1500.f, 10.f)
	});

	Vector2 stoolSize(125.f, 200.f);

	for (size_t i = 0; i < barStoolLocations.size(); ++i)
	{
		SceneTransformComponent* legTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(legTransform))
		{
			legTransform->SetTranslation(barStoolLocations.at(i));

			ColorRenderComponent* legColor = ColorRenderComponent::CreateObject();
			if (legTransform->AddComponent(legColor))
			{
				legColor->SetBaseSize(stoolSize);
				legColor->SolidColor = Color(97, 113, 126);
				legColor->SetPivot(Vector2(0.5f, 1.f));
				RegisterRenderComp(legColor);
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (legTransform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, stoolSize.X * 0.5f));
			}

			SceneTransformComponent* seatTransform = SceneTransformComponent::CreateObject();
			if (legTransform->AddComponent(seatTransform))
			{
				seatTransform->SetTranslation(Vector3(0.f, -stoolSize.Y, 1.f));

				ColorRenderComponent* seatColor = ColorRenderComponent::CreateObject();
				if (seatTransform->AddComponent(seatColor))
				{
					seatColor->SetBaseSize(Vector2(stoolSize.X * 1.2f, 1.f));
					seatColor->SetPivot(Vector2(0.5f, 0.5f));
					seatColor->SetShape(ColorRenderComponent::ST_Circle);
					seatColor->SolidColor = Color(191, 45, 53);
					RegisterRenderComp(seatColor);
				}
			}
		}
	}

	//Create Barkeep Plikter
	{
		SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->SetTranslation(Vector3(-1150.f, 0.f, 10.f));

			ColorRenderComponent* color = ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SetShape(ColorRenderComponent::ST_Circle);
				color->SetBaseSize(Vector2(192.f, 192.f));
				color->SetPivot(Vector2(0.5f, 0.5f));
				color->SolidColor = Color(180, 170, 184);
				RegisterRenderComp(color);
			}

			NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
			if (localSignature != nullptr && localSignature->HasRole(NetworkSignature::NR_Client))
			{
				FontPool* localFontPool = FontPool::FindFontPool();
				CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)
				Font* defaultFont = localFontPool->GetDefaultFont();

				TextRenderComponent* textComp = TextRenderComponent::CreateObject();
				if (transform->AddComponent(textComp))
				{
					DString barkeepName = TXT("Barkeep\nPlikter");
					Int fontSize = 120;
					Float nameWidth = defaultFont->CalculateStringWidth(barkeepName, fontSize);

					TextRenderComponent::STextData& textData = textComp->CreateTextInstance(barkeepName, defaultFont, fontSize.ToUnsignedInt32());
					textData.DrawOffset.x = (nameWidth * -0.25f).Value;
					textData.DrawOffset.y = (fontSize.ToFloat() * -3.f).Value;
					textComp->SetFontColor(Color::BLACK.Source);
					RegisterRenderComp(textComp);
				}

				PhysicsComponent* physPrompt = PhysicsComponent::CreateObject();
				if (transform->AddComponent(physPrompt))
				{
					physPrompt->SetOverlappingChannels(NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
					physPrompt->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, 1000.f));
					physPrompt->SetBeginOverlap(SDFUNCTION_2PARAM(this, BarMap, HandleApproachPlikter, void, PhysicsComponent*, PhysicsComponent*));
					physPrompt->SetEndOverlap(SDFUNCTION_2PARAM(this, BarMap, HandleLeavePlikter, void, PhysicsComponent*, PhysicsComponent*));
				}
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (transform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionCapsule(1.f, Vector3::ZERO_VECTOR, 256.f, 96.f));
			}
		}
	}
}

void BarMap::CreateGameObjects ()
{
	//Create pool table. I'm surprised how great this turned out considering it's only rectangles and circles!
	{
		SceneTransformComponent* tableTransform = SceneTransformComponent::CreateObject();
		if (AddComponent(tableTransform))
		{
			tableTransform->SetTranslation(Vector3(900.f, -1900.f, -15.f));

			Vector2 tableSize(1000.f, 300.f);
			Color underColor(180, 98, 49);
			SceneTransformComponent* leftLegTransform = SceneTransformComponent::CreateObject();
			if (tableTransform->AddComponent(leftLegTransform))
			{
				leftLegTransform->SetTranslation(Vector3(tableSize.X * -0.475f, 0.f, -2.f));
			
				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (leftLegTransform->AddComponent(color))
				{
					color->SetBaseSize(200.f, 200.f);
					color->SolidColor = underColor;
					RegisterRenderComp(color);
				}
			}

			SceneTransformComponent* rightLegTransform = SceneTransformComponent::CreateObject();
			if (tableTransform->AddComponent(rightLegTransform))
			{
				rightLegTransform->SetTranslation(Vector3(tableSize.X * 0.475f, 0.f, -2.f));

				ColorRenderComponent* color = ColorRenderComponent::CreateObject();
				if (rightLegTransform->AddComponent(color))
				{
					color->SetBaseSize(200.f, 200.f);
					color->SolidColor = underColor;
					color->SetPivot(Vector2(1.f, 0.f));
					RegisterRenderComp(color);
				}
			}

			ColorRenderComponent* colorComp = ColorRenderComponent::CreateObject();
			if (tableTransform->AddComponent(colorComp))
			{
				colorComp->SetBaseSize(tableSize);
				colorComp->SolidColor = underColor;
				colorComp->SetPivot(Vector2(0.5f, 1.f));
				RegisterRenderComp(colorComp);
			}

			SceneTransformComponent* underTransform = SceneTransformComponent::CreateObject();
			if (tableTransform->AddComponent(underTransform))
			{
				underTransform->SetTranslation(Vector3(0.f, 0.f, -1.f));

				ColorRenderComponent* underColor = ColorRenderComponent::CreateObject();
				if (underTransform->AddComponent(underColor))
				{
					underColor->SetBaseSize(Vector2(tableSize.X, tableSize.Y * 0.5f));
					underColor->SolidColor = Color(140, 75, 35);
					underColor->SetPivot(Vector2(0.5f, 0.f));
					RegisterRenderComp(underColor);
				}
			}

			SceneTransformComponent* surfaceTransform = SceneTransformComponent::CreateObject();
			if (tableTransform->AddComponent(surfaceTransform))
			{
				surfaceTransform->SetTranslation(Vector3(0.f, tableSize.Y * -0.95f, 1.f));

				ColorRenderComponent* tableColor = ColorRenderComponent::CreateObject();
				if (surfaceTransform->AddComponent(tableColor))
				{
					tableColor->SolidColor = Color(92, 194, 97);
					tableColor->SetPivot(Vector2(0.5f, 0.f));
					tableColor->SetBaseSize(tableSize * 0.9f);
					RegisterRenderComp(tableColor);
				}

				std::vector<Vector3> pocketLocations(
				{
					//Front pockets (left to right)
					Vector3(tableSize.X * -0.475f, tableSize.Y * 0.95f, 10.f),
					Vector3(0.f, tableSize.Y * 0.95f, 10.f),
					Vector3(tableSize.X * 0.475f, tableSize.Y * 0.95f, 10.f),

					//Back pockets (left to right)
					Vector3(tableSize.X * -0.475f, 0.f, 1.f),
					Vector3(0.f, 0.f, 1.f),
					Vector3(tableSize.X * 0.475f, 0.f, 1.f)
				});

				for (size_t i = 0; i < pocketLocations.size(); ++i)
				{
					SceneTransformComponent* holeTransform = SceneTransformComponent::CreateObject();
					if (surfaceTransform->AddComponent(holeTransform))
					{
						holeTransform->SetTranslation(pocketLocations.at(i));

						ColorRenderComponent* holeColor = ColorRenderComponent::CreateObject();
						if (holeTransform->AddComponent(holeColor))
						{
							holeColor->SetShape(ColorRenderComponent::ST_Circle);
							holeColor->SetPivot(Vector2(0.5f, 0.5f));
							holeColor->SolidColor = Color::BLACK;
							holeColor->SetBaseSize(Vector2(75.f, 75.f));
							RegisterRenderComp(holeColor);
						}
					}

					//If it's a front pocket, add the pouch
					if (i < 3)
					{
						SceneTransformComponent* pouchTransform = SceneTransformComponent::CreateObject();
						if (surfaceTransform->AddComponent(pouchTransform))
						{
							pouchTransform->SetTranslation(pocketLocations.at(i));
							pouchTransform->EditTranslation().Z -= 1.f;

							ColorRenderComponent* pouchColor = ColorRenderComponent::CreateObject();
							if (pouchTransform->AddComponent(pouchColor))
							{
								pouchColor->SolidColor = Color(161, 140, 69);
								pouchColor->SetPivot(Vector2(0.5f, 0.f));
								pouchColor->SetBaseSize(Vector2(75.f, 100.f));
								RegisterRenderComp(pouchColor);
							}
						}
					}
				}

				//Create the first eight balls and white ball. Don't bother with striped balls. Too lazy for that.
				std::vector<Color> ballColors(
				{
					Color(255, 255, 55), //1 = Yellow
					Color(26, 32, 235), //2 = Blue
					Color(255, 56, 36), //3 = Red
					Color(194, 40, 176), //4 = Purple
					Color(255, 125, 0), //5 = Orange
					Color(13, 206, 32), //6 = Green
					Color(223, 110, 120), //7 = Burgundy
					Color(8, 8, 8), //8 = Black
					Color(240, 240, 240) //9 = White (We aint scratchin' like plebs)
				});

				for (size_t i = 0; i < ballColors.size(); ++i)
				{
					SceneTransformComponent* ballTransform = SceneTransformComponent::CreateObject();
					if (surfaceTransform->AddComponent(ballTransform))
					{
						Float minY = 8.f; //Closer to the edge compared to add 'height' to the balls.
						Float maxY = tableSize.Y * 0.75f; //Further away from the edge since the ball shouldn't overlap from this perspective.
						Vector3 ballPosition(RandomUtils::RandRange(tableSize.X * -0.42f, tableSize.X * 0.42f), RandomUtils::RandRange(minY, maxY), 1.f);

						//The closer the ball is to the front panel, the higher the ball to ensure it renders in front of the others in the back.
						ballPosition.Z = Utils::Lerp<Float>((ballPosition.Y - minY) / (maxY - minY), 15.f, 20.f);
						ballTransform->SetTranslation(ballPosition);

						ColorRenderComponent* ballColorComp = ColorRenderComponent::CreateObject();
						if (ballTransform->AddComponent(ballColorComp))
						{
							ballColorComp->SetBaseSize(Vector2(64.f, 64.f));
							ballColorComp->SetShape(ColorRenderComponent::ST_Circle);
							ballColorComp->SetPivot(Vector2(0.5f, 0.5f));
							ballColorComp->SolidColor = ballColors.at(i);
							RegisterRenderComp(ballColorComp);
						}
					}
				}
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (tableTransform->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 512.f,
				{
					Vector2(tableSize.X * -0.5f, tableSize.Y * -1.f),
					Vector2(tableSize.X * 0.5f, tableSize.Y * -1.f),
					Vector2(tableSize.X * 0.5f, tableSize.Y * 0.25f), //For the front side, undercut a bit to allow players to overlap the front of the pool table.
					Vector2(tableSize.X * -0.5f, tableSize.Y * 0.25f)
				}));
			}
		}
	}

	//Add arcade machine
	{
		SceneTransformComponent* arcadeRoot = SceneTransformComponent::CreateObject();
		if (AddComponent(arcadeRoot))
		{
			arcadeRoot->SetTranslation(Vector3(1650.f, 2100.f, 5.f));
			Vector2 baseSize(250.f, 300.f);

			ColorRenderComponent* baseColor = ColorRenderComponent::CreateObject();
			if (arcadeRoot->AddComponent(baseColor))
			{
				baseColor->SolidColor = Color(120, 168, 244);
				baseColor->SetPivot(Vector2(1.f, 1.f));
				baseColor->SetBaseSize(baseSize);
				RegisterRenderComp(baseColor);
			}

			SceneTransformComponent* topTransform = SceneTransformComponent::CreateObject();
			if (arcadeRoot->AddComponent(topTransform))
			{
				topTransform->SetTranslation(Vector3(0.f, -baseSize.Y, 0.f));

				ColorRenderComponent* topColor = ColorRenderComponent::CreateObject();
				if (topTransform->AddComponent(topColor))
				{
					topColor->SolidColor = Color(120, 168, 244);
					topColor->SetPivot(Vector2(1.f, 1.f));
					topColor->SetBaseSize(baseSize * Vector2(0.5f, 1.f));
					RegisterRenderComp(topColor);
				}
			}

			SceneTransformComponent* joystickTransform = SceneTransformComponent::CreateObject();
			if (arcadeRoot->AddComponent(joystickTransform))
			{
				joystickTransform->SetTranslation(Vector3(baseSize.X * -0.9f, -baseSize.Y, 0.f));

				ColorRenderComponent* joystickColor = ColorRenderComponent::CreateObject();
				if (joystickTransform->AddComponent(joystickColor))
				{
					joystickColor->SolidColor = Color(64, 50, 50);
					joystickColor->SetPivot(Vector2(0.5f, 1.f));
					joystickColor->SetBaseSize(Vector2(16.f, 48.f));
					RegisterRenderComp(joystickColor);
				}
			}

			//Add object decorations on the side panel
			std::vector<Vector3> decoLocations(
			{
				Vector3(baseSize.X * -0.8f, baseSize.Y * -0.2f, 1.f),
				Vector3(baseSize.X * -0.4f, baseSize.Y * -0.75f, 1.f),
				Vector3(baseSize.X * -0.2f, baseSize.Y * -1.1f, 1.f)
			});

			for (size_t i = 0; i < decoLocations.size(); ++i)
			{
				SceneTransformComponent* decoTransform = SceneTransformComponent::CreateObject();
				if (arcadeRoot->AddComponent(decoTransform))
				{
					decoTransform->SetTranslation(decoLocations.at(i));

					ColorRenderComponent* decoColor = ColorRenderComponent::CreateObject();
					if (decoTransform->AddComponent(decoColor))
					{
						decoColor->SetShape(ColorRenderComponent::ST_Circle);
						decoColor->SetPivot(Vector2(0.5f, 0.5f));
						decoColor->SolidColor = Color(RandomUtils::RandRange(50.f, 200.f).ToInt().ToUnsignedInt32(), RandomUtils::RandRange(50.f, 200.f).ToInt().ToUnsignedInt32(), RandomUtils::RandRange(50.f, 200.f).ToInt().ToUnsignedInt32());
						decoColor->SetBaseSize(Vector2(75.f, 75.f));
						RegisterRenderComp(decoColor);
					}
				}
			}

			PhysicsComponent* phys = PhysicsComponent::CreateObject();
			if (arcadeRoot->AddComponent(phys))
			{
				phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
				phys->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 256.f,
				{
					Vector2::ZERO_VECTOR,
					Vector2(-baseSize.X, 0.f),
					Vector2(-baseSize.X, baseSize.Y * -0.5f),
					Vector2(0.f, baseSize.Y * -0.5f)
				}));
			}
		}
	}
}

void BarMap::CreateExitPoint ()
{
	if (!bCreatedExitPoint && RoleComp != nullptr && RoleComp->HasAuthority())
	{
		//Have the server instantiate the MapGates, and those objects will propagate to clients.
		MapGate* gate = MapGate::CreateObject();
		gate->SetTranslation(Vector3(1500.f, 0.f, -5.f));
		gate->MapTextKey = TXT("BarToDanceFloor");
		gate->DrawOffset = Vector2(-320.f, -256.f);
		gate->PromptTextKey = TXT("BarToDanceFloorPrompt");
		gate->SetDestination(DanceFloorMap::SStaticClass());
		MapObjects.push_back(gate);

		bCreatedExitPoint = true;
	}
}

void BarMap::HandleApproachPlikter (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightEngine->GetLocalPlayer())
	{
		localNightEngine->DisplayMapPrompt(translator->TranslateText(TXT("BuyDrinkPrompt"), NightclubEngineComponent::TRANSLATION_FILE, TXT("BarMap")), SDFUNCTION(this, BarMap, HandleBuyDrink, void));
	}
}

void BarMap::HandleLeavePlikter (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightEngine->GetLocalPlayer())
	{
		localNightEngine->CloseMapPrompt();
	}
}

void BarMap::HandleBuyDrink ()
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	//Although it's simpler and easier to work with directly assigning this variable, this isn't secure as clients can directly set this variable.
	//This approach is ideal for nonessential variables that are safe to simulate locally or wont be devastating if cheaters alter their game states.
	//If there's a need to secure this, first only the server should have write access to Player::bHasDrink Bool. Second, the server needs to be aware about this interaction.
	//After it's aware of this interaction, then the server will set its HasDrink variable rather than the clients setting it.
	if (Player* localPlayer = localNightclub->GetLocalPlayer())
	{
		localPlayer->SetHasDrink(true);
	}
}
NC_END