/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubMap.cpp
=====================================================================
*/

#include "NightclubEngineComponent.h"
#include "NightclubMap.h"

IMPLEMENT_CLASS(SD::NC::NightclubMap, SD::Entity)
NC_BEGIN

void NightclubMap::InitProps ()
{
	Super::InitProps();

	PlayerSpawn = Vector3::ZERO_VECTOR;
	MapSize = Vector2(10000.f, 10000.f); //100 meters
	RoleComp = nullptr;
}

void NightclubMap::BeginObject ()
{
	Super::BeginObject();

	CreatePerimeter();

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = SDFUNCTION_1PARAM(this, NightclubMap, HandleGetComponentList, void, std::vector<NetworkComponent*>&);
		RoleComp->OnNetInitialize = SDFUNCTION_1PARAM(this, NightclubMap, HandleNetInitialize, void, NetworkSignature*);

		RoleComp->OnIsNetRelevant = [&](NetworkSignature* remoteClient)
		{
			//Don't replicate this object to servers or master server
			return (remoteClient != nullptr && remoteClient->HasRole(NetworkSignature::NR_Client));
		};

		RoleComp->OnLoseRelevance = [&]()
		{
			NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
			CHECK(localNightEngine != nullptr)
			localNightEngine->SetMap(nullptr);
		};
	}
}

void NightclubMap::Destroyed ()
{
	for (Object* mapObj : MapObjects)
	{
		mapObj->Destroy();
	}
	ContainerUtils::Empty(OUT MapObjects);

	Super::Destroyed();
}

void NightclubMap::InitializePlayer (Player* newPlayer)
{
	//Noop
}

void NightclubMap::RegisterRenderComp (RenderComponent* comp)
{
	NightclubEngineComponent* nightclubEngine = NightclubEngineComponent::Find();
	CHECK(nightclubEngine != nullptr)

	if (nightclubEngine->GetDrawLayer() != nullptr)
	{
		nightclubEngine->GetDrawLayer()->RegisterSingleComponent(comp);
	}
}

void NightclubMap::CreatePerimeter ()
{
	SceneTransformComponent* transform = SceneTransformComponent::CreateObject();
	if (AddComponent(transform))
	{
		PhysicsComponent* boundaries = PhysicsComponent::CreateObject();
		if (transform->AddComponent(boundaries))
		{
			boundaries->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
			boundaries->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);

			//North
			boundaries->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 100.f,
			{
				Vector2(MapSize.X * -0.5f, MapSize.Y * -0.5f),
				Vector2(MapSize.X * -0.5f, MapSize.Y * -0.6f),
				Vector2(MapSize.X * 0.5f, MapSize.Y * -0.6f),
				Vector2(MapSize.X * 0.5f, MapSize.Y * -0.5f)
			}));

			//East
			boundaries->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 100.f,
			{
				Vector2(MapSize.X * 0.5f, MapSize.Y * -0.5f),
				Vector2(MapSize.X * 0.6f, MapSize.Y * -0.5f),
				Vector2(MapSize.X * 0.6f, MapSize.Y * 0.5f),
				Vector2(MapSize.X * 0.5f, MapSize.Y * 0.5f)
			}));

			//South
			boundaries->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 100.f,
			{
				Vector2(MapSize.X * -0.5f, MapSize.Y * 0.5f),
				Vector2(MapSize.X * 0.5f, MapSize.Y * 0.5f),
				Vector2(MapSize.X * 0.5f, MapSize.Y * 0.6f),
				Vector2(MapSize.X * -0.5f, MapSize.Y * 0.6f)
			}));

			//West
			boundaries->AddShape(new CollisionExtrudePolygon(1.f, Vector3::ZERO_VECTOR, 100.f,
			{
				Vector2(MapSize.X * -0.5f, MapSize.Y * -0.5f),
				Vector2(MapSize.X * -0.5f, MapSize.Y * 0.5f),
				Vector2(MapSize.X * -0.6f, MapSize.Y * 0.5f),
				Vector2(MapSize.X * -0.6f, MapSize.Y * -0.5f)
			}));
		}
	}
}

void NightclubMap::HandleNetInitialize (NetworkSignature* connectedTo)
{
	if (!RoleComp->HasAuthority())
	{
		//Replace the map object with this new map.
		NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
		CHECK(localNightclub != nullptr)
		if (NightclubMap* prevMap = localNightclub->GetMap())
		{
			prevMap->Destroy();
		}

		localNightclub->SetMap(this);
	}
}

void NightclubMap::HandleGetComponentList (std::vector<NetworkComponent*>& outComponentList)
{
	//Noop - Base class does not have any NetworkComponents
}
NC_END