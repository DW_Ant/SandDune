/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MapGate.cpp
=====================================================================
*/

#include "MapGate.h"
#include "MasterConnection.h"
#include "MasterServer.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "NightclubMap.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::MapGate, SD::Entity)
NC_BEGIN

void MapGate::InitProps ()
{
	Super::InitProps();

	MapTextKey = DString::EmptyString;
	DrawOffset = Vector2::ZERO_VECTOR;
	PromptTextKey = DString::EmptyString;

	RoleComp = nullptr;
	NetComp = nullptr;
	TextComp = nullptr;
	Destination = nullptr;
}

void MapGate::BeginObject ()
{
	Super::BeginObject();

	InitializeSceneComponents();

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = [&](std::vector<NetworkComponent*>& outNetComponents)
		{
			outNetComponents.push_back(NetComp);
		};

		RoleComp->OnIsNetOwner = [&](NetworkSignature* remoteClient)
		{
			//All clients are network owners, allowing all of them to call RPC methods.
			return true;
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		NetComp->AddReplicatedVariable<DString>(&MapTextKey, [&](ReplicatedVariable<DString>& outInitRepVar)
		{
			outInitRepVar.ClearReplicationFlags();
			outInitRepVar.SetSyncOnInit(true); //only init once at beginning
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = SDFUNCTION(this, MapGate, HandleTextReplicated, void);
		});

		NetComp->AddReplicatedVariable<Vector2>(&DrawOffset, [&](ReplicatedVariable<Vector2>& outInitRepVar)
		{
			outInitRepVar.ClearReplicationFlags();
			outInitRepVar.SetSyncOnInit(true);
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = SDFUNCTION(this, MapGate, HandleTextReplicated, void);
		});

		NetComp->AddReplicatedVariable<DString>(&PromptTextKey, [&](ReplicatedVariable<DString>& outInitRepVar)
		{
			outInitRepVar.ClearReplicationFlags();
			outInitRepVar.SetSyncOnInit(true);
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
		});

		NetComp->AddReplicatedVariable<Vector3>(&Translation, [&](ReplicatedVariable<Vector3>& outInitRepVar)
		{
			outInitRepVar.ClearReplicationFlags();
			outInitRepVar.SetSyncOnInit(true);
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = [&]()
			{
				MarkAbsTransformDirty();
			};
		});

		BIND_RPC_1PARAM(NetComp, ClientConnectToServer, this, MapGate, Int);
		ClientConnectToServer.CanBeExecutedOn = NetworkSignature::NR_Client;

		BIND_RPC(NetComp, ServerTravel, this, MapGate);
		ServerTravel.CanBeExecutedOn = NetworkSignature::NR_Server;

		BIND_RPC_3PARAM(NetComp, MasterServerTravel, this, MapGate, DString, Bool, DString);
		MasterServerTravel.CanBeExecutedOn = NetworkSignature::NR_MasterServer;
		MasterServerTravel.CanBeExecutedFrom = NetworkSignature::NR_Server;
	}
}

bool MapGate::ClientConnectToServer_Implementation (Int serverPort)
{
	if (serverPort <= 0)
	{
		return false;
	}

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OpenConnection(sf::IpAddress::LocalHost, static_cast<unsigned short>(serverPort.ToUnsignedInt32()));
		if (NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find())
		{
			localNightclub->bReturnToOutdoorsOnDisconnect = false; //Don't boot the client back to outdoors during this transition.
		}

		//Disconnect from the signature that called this method
		connections->Disconnect(ClientConnectToServer.GetInstigatedFrom());
	}

	return true;
}

void MapGate::SetDestination (const DClass* newDestination)
{
	if (!newDestination->IsA(NightclubMap::SStaticClass()))
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("Cannot set set MapGate::Destination to %s. The DClass must be a class of a NightclubMap."), newDestination->ToString());
		return;
	}

	Destination = newDestination;
}

void MapGate::InitializeSceneComponents ()
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (localSignature != nullptr && !localSignature->HasRole(NetworkSignature::NR_Client))
	{
		//Don't create anything for servers
		return;
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr && localNightclub->GetDrawLayer() != nullptr)

	Float zoneRadius = 256.f;

	ColorRenderComponent* color = ColorRenderComponent::CreateObject();
	if (AddComponent(color))
	{
		color->SetBaseSize(Vector2(zoneRadius * 2.f, zoneRadius * 2.f));
		color->SetShape(ColorRenderComponent::ST_Circle);
		color->SolidColor = Color(200, 0, 0);
		color->SetPivot(Vector2(0.5f, 0.5f));
		localNightclub->GetDrawLayer()->RegisterSingleComponent(color);
	}

	SceneTransformComponent* textTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(textTransform))
	{
		textTransform->SetTranslation(Vector3(0.f, 0.f, 1.f));

		TextComp = TextRenderComponent::CreateObject();
		if (textTransform->AddComponent(TextComp))
		{
			localNightclub->GetDrawLayer()->RegisterSingleComponent(TextComp);
		}
	}

	PhysicsComponent* phys = PhysicsComponent::CreateObject();
	if (AddComponent(phys))
	{
		phys->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
		phys->SetOverlappingChannels(NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
		phys->SetBeginOverlap(SDFUNCTION_2PARAM(this, MapGate, HandleBeginOverlap, void, PhysicsComponent*, PhysicsComponent*));
		phys->SetEndOverlap(SDFUNCTION_2PARAM(this, MapGate, HandleEndOverlap, void, PhysicsComponent*, PhysicsComponent*));
		phys->AddShape(new CollisionSphere(1.f, Vector3::ZERO_VECTOR, zoneRadius));
	}
}

bool MapGate::ServerTravel_Implementation ()
{
	if (Destination != nullptr)
	{
		if (NetworkSignature* instigator = ServerTravel.GetInstigatedFrom())
		{
			//Find the Player associated with the instigator. If found, pass the beverage flag to the master server so that state is maintained during server travel.
			Bool bHasBeverage = false;
			if (NightclubEngineComponent* nightclubEngine = NightclubEngineComponent::Find())
			{
				for (size_t i = 0; i < nightclubEngine->ReadPlayers().size(); ++i)
				{
					if (nightclubEngine->ReadPlayers().at(i)->GetNetOwner() == instigator)
					{
						bHasBeverage = nightclubEngine->ReadPlayers().at(i)->HasDrink();
						break;
					}
				}
			}

			if (NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(instigator->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
			{
				DString playerName = clientId->GetPlayerName();
				MasterServerTravel(BaseRpc::RT_MasterServers, playerName, bHasBeverage, Destination->GetClassNameWithoutNamespace());
			}
		}
	}

	return true;
}

bool MapGate::MasterServerTravel_Implementation (DString playerName, Bool bHasBeverage, DString mapName)
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	if (!localSignature->HasRole(NetworkSignature::NR_MasterServer))
	{
		NightclubLog.Log(LogCategory::LL_Critical, TXT("The MapGate::MasterServerTravel_Implementation should only be executed on master servers!"));
		return false;
	}

	if (playerName.IsEmpty())
	{
		return false;
	}

	//Ensure mapName is a valid class
	const DClass* mapClass = nullptr;
	for (ClassIterator iter(NightclubMap::SStaticClass()); iter.GetSelectedClass() != nullptr; ++iter)
	{
		if (mapName.Compare(iter.GetSelectedClass()->GetClassNameWithoutNamespace(), DString::CC_CaseSensitive) == 0)
		{
			mapClass = iter.GetSelectedClass();
			break;
		}
	}

	if (mapClass == nullptr)
	{
		//Not a valid map name
		return false;
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)
	if (MasterServer* coordinator = localNightclub->GetServerCoordinator())
	{
		if (MasterConnection* masterConnection = coordinator->GetCommunicator())
		{
			masterConnection->MovePlayer(MasterServerTravel.GetInstigatedFrom(), playerName, bHasBeverage, mapClass);
		}
	}

	return true;
}

void MapGate::HandleBeginOverlap (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightEngine = NightclubEngineComponent::Find();
	CHECK(localNightEngine != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightEngine->GetLocalPlayer())
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		localNightEngine->DisplayMapPrompt(translator->TranslateText(PromptTextKey, NightclubEngineComponent::TRANSLATION_FILE, TXT("MapGate")), SDFUNCTION(this, MapGate, HandleAcceptPrompt, void));
	}
}

void MapGate::HandleEndOverlap (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	Player* instigator = dynamic_cast<Player*>(otherComp->GetOwner());
	if (instigator != nullptr && instigator == localNightclub->GetLocalPlayer())
	{
		localNightclub->CloseMapPrompt();
	}
}

void MapGate::HandleAcceptPrompt ()
{
	ServerTravel(BaseRpc::RT_Servers);
}

void MapGate::HandleTextReplicated ()
{
	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	if (!localSignature->HasRole(NetworkSignature::NR_Client))
	{
		return;
	}

	if (TextComp != nullptr && !MapTextKey.IsEmpty())
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		TextComp->DeleteAllText();
		TextRenderComponent::STextData& textData = TextComp->CreateTextInstance(translator->TranslateText(MapTextKey, NightclubEngineComponent::TRANSLATION_FILE, TXT("MapGate")), localFontPool->GetDefaultFont(), 128);
		textData.DrawOffset.x = DrawOffset.X.Value;
		textData.DrawOffset.y = DrawOffset.Y.Value;
	}
}
NC_END