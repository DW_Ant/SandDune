/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClientHackerPanel.cpp
=====================================================================
*/

#include "ClientHackerPanel.h"
#include "ClientHackerTestObject.h"
#include "DanceComponent.h"
#include "HackedConnectionManager.h"
#include "NightclubAdminObject.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::ClientHackerPanel, SD::GuiEntity)
NC_BEGIN

void ClientHackerPanel::InitProps ()
{
	Super::InitProps();

	PermissionsLabel = nullptr;
	PermissionVarRepOutline = nullptr;
	PermissionVarRepButton = nullptr;
	PermissionRpcOutline = nullptr;
	PermissionRpcButton = nullptr;
	PermissionRefAnythingOutline = nullptr;
	PermissionRefAnythingButton = nullptr;
	PermissionInstantiationOutline = nullptr;
	PermissionInstantiationButton = nullptr;
	ActionLabel = nullptr;
	DanceOutline = nullptr;
	DanceButton = nullptr;
	GainBeverageOutline = nullptr;
	GainBeverageButton = nullptr;
	DrinkOutline = nullptr;
	DrinkButton = nullptr;
	ObjectLabel = nullptr;
	SpawnPlayerOutline = nullptr;
	SpawnPlayerButton = nullptr;
	SpawnAdminOutline = nullptr;
	SpawnAdminButton = nullptr;
	TeleportLabel = nullptr;
	TeleportOutline = nullptr;
	TeleportButton = nullptr;
	ServerLabel = nullptr;
	ServerOutline = nullptr;
	ServerField = nullptr;
	InputLabel = nullptr;
	InputList = nullptr;
	FloodOutline = nullptr;
	FloodButton = nullptr;
	CloseButton = nullptr;

	AlphaFadePerSecond = 128.f;
	bIsTeleportingPlayer = false;
}

void ClientHackerPanel::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::CreateObject();
	CHECK(translator != nullptr)

	const DString& fileName = NightclubEngineComponent::TRANSLATION_FILE;
	DString sectionName = TXT("ClientHackerPanel");
	Float lineHeight = 0.067f;
	Float spacing = 0.001f;
	Float posX = spacing;
	Float posY = spacing;
	Color outlineColor(0, 150, 0, 0);

	//Considered wrapping these in macros or elaborate functions, but decided against it for readability and debugging purposes even though it comes at the cost of boilerplate code.
	FrameComponent* background = FrameComponent::CreateObject();
	if (AddComponent(background))
	{
		background->SetBorderThickness(2.f);
		background->SetAlwaysConsumeMouseEvents(true);

		//============ Permissions ============
		FrameComponent* permissionFrame = FrameComponent::CreateObject();
		if (background->AddComponent(permissionFrame))
		{
			permissionFrame->SetAnchorTop(spacing);
			permissionFrame->SetAnchorLeft(spacing);
			permissionFrame->SetAnchorRight(spacing);
			permissionFrame->SetSize(1.f, lineHeight * 2.f);
			permissionFrame->SetBorderThickness(2.f);
			Float subSpacing = 0.1f;

			PermissionsLabel = LabelComponent::CreateObject();
			if (permissionFrame->AddComponent(PermissionsLabel))
			{
				PermissionsLabel->SetAutoRefresh(false);
				PermissionsLabel->SetPosition(subSpacing, subSpacing);
				PermissionsLabel->SetAnchorLeft(subSpacing);
				PermissionsLabel->SetAnchorRight(subSpacing);
				PermissionsLabel->SetSize(Vector2(1.f, 0.45f));
				PermissionsLabel->SetWrapText(true);
				PermissionsLabel->SetClampText(true);
				PermissionsLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				PermissionsLabel->SetVerticalAlignment(LabelComponent::VA_Center);
				PermissionsLabel->SetText(translator->TranslateText(TXT("PermissionsLabel"), fileName, sectionName));
				PermissionsLabel->SetAutoRefresh(true);
			}

			Float buttonWidth = 0.2375f;
			Float buttonSpacing = 0.01f;
			Float outlineThickness = 0.05f;
			posX = buttonSpacing;

			GuiComponent* varRepTransform = GuiComponent::CreateObject();
			if (permissionFrame->AddComponent(varRepTransform))
			{
				varRepTransform->SetPosition(posX, 0.5f);
				varRepTransform->SetSize(buttonWidth, 0.45f);

				PermissionVarRepOutline = ColorRenderComponent::CreateObject();
				if (varRepTransform->AddComponent(PermissionVarRepOutline))
				{
					PermissionVarRepOutline->SolidColor = outlineColor;

					TickComponent* outlineTick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (PermissionVarRepOutline->AddComponent(outlineTick))
					{
						outlineTick->SetTicking(false);
						outlineTick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				PermissionVarRepButton = ButtonComponent::CreateObject();
				if (varRepTransform->AddComponent(PermissionVarRepButton))
				{
					PermissionVarRepButton->SetAnchorLeft(outlineThickness);
					PermissionVarRepButton->SetAnchorTop(outlineThickness);
					PermissionVarRepButton->SetAnchorRight(outlineThickness);
					PermissionVarRepButton->SetAnchorBottom(outlineThickness);
					PermissionVarRepButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandlePermissionVarRepReleased, void, ButtonComponent*));
					PermissionVarRepButton->SetCaptionText(translator->TranslateText(TXT("PermissionVarRepButton"), fileName, sectionName));
				}
			}

			posX += buttonSpacing + buttonWidth;

			GuiComponent* rpcTransform = GuiComponent::CreateObject();
			if (permissionFrame->AddComponent(rpcTransform))
			{
				rpcTransform->SetPosition(posX, 0.5f);
				rpcTransform->SetSize(buttonWidth, 0.45f);

				PermissionRpcOutline = ColorRenderComponent::CreateObject();
				if (rpcTransform->AddComponent(PermissionRpcOutline))
				{
					PermissionRpcOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (PermissionRpcOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				PermissionRpcButton = ButtonComponent::CreateObject();
				if (rpcTransform->AddComponent(PermissionRpcButton))
				{
					PermissionRpcButton->SetAnchorLeft(outlineThickness);
					PermissionRpcButton->SetAnchorTop(outlineThickness);
					PermissionRpcButton->SetAnchorRight(outlineThickness);
					PermissionRpcButton->SetAnchorBottom(outlineThickness);
					PermissionRpcButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandlePermissionRpcReleased, void, ButtonComponent*));
					PermissionRpcButton->SetCaptionText(translator->TranslateText(TXT("PermissionRpcButton"), fileName, sectionName));
				}
			}

			posX += buttonSpacing + buttonWidth;

			GuiComponent* refTransform = GuiComponent::CreateObject();
			if (permissionFrame->AddComponent(refTransform))
			{
				refTransform->SetPosition(posX, 0.5f);
				refTransform->SetSize(buttonWidth, 0.45f);

				PermissionRefAnythingOutline = ColorRenderComponent::CreateObject();
				if (refTransform->AddComponent(PermissionRefAnythingOutline))
				{
					PermissionRefAnythingOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (PermissionRefAnythingOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				PermissionRefAnythingButton = ButtonComponent::CreateObject();
				if (refTransform->AddComponent(PermissionRefAnythingButton))
				{
					PermissionRefAnythingButton->SetAnchorLeft(outlineThickness);
					PermissionRefAnythingButton->SetAnchorTop(outlineThickness);
					PermissionRefAnythingButton->SetAnchorRight(outlineThickness);
					PermissionRefAnythingButton->SetAnchorBottom(outlineThickness);
					PermissionRefAnythingButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandlePermissionRefAnythingReleased, void, ButtonComponent*));
					PermissionRefAnythingButton->SetCaptionText(translator->TranslateText(TXT("PermissionRefAnythingButton"), fileName, sectionName));
				}
			}

			posX += buttonSpacing + buttonWidth;

			GuiComponent* instantiateTransform = GuiComponent::CreateObject();
			if (permissionFrame->AddComponent(instantiateTransform))
			{
				instantiateTransform->SetPosition(posX, 0.5f);
				instantiateTransform->SetSize(buttonWidth, 0.45f);

				PermissionInstantiationOutline = ColorRenderComponent::CreateObject();
				if (instantiateTransform->AddComponent(PermissionInstantiationOutline))
				{
					PermissionInstantiationOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (PermissionInstantiationOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				PermissionInstantiationButton = ButtonComponent::CreateObject();
				if (instantiateTransform->AddComponent(PermissionInstantiationButton))
				{
					PermissionInstantiationButton->SetAnchorLeft(outlineThickness);
					PermissionInstantiationButton->SetAnchorTop(outlineThickness);
					PermissionInstantiationButton->SetAnchorRight(outlineThickness);
					PermissionInstantiationButton->SetAnchorBottom(outlineThickness);
					PermissionInstantiationButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandlePermissionInstantiationReleased, void, ButtonComponent*));
					PermissionInstantiationButton->SetCaptionText(translator->TranslateText(TXT("PermissionInstantiationButton"), fileName, sectionName));
				}
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (permissionFrame->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("PermissionFrameTooltip"), fileName, sectionName));
			}
		}

		posX = spacing;
		posY += (lineHeight * 2.f);

		//============ Action Controls ============
		FrameComponent* actionFrame = FrameComponent::CreateObject();
		if (background->AddComponent(actionFrame))
		{
			actionFrame->SetPosition(posX, posY);
			actionFrame->SetSize(1.f, lineHeight * 3.f);
			actionFrame->SetAnchorLeft(spacing);
			actionFrame->SetAnchorRight(spacing);
			actionFrame->SetBorderThickness(2.f);

			ActionLabel = LabelComponent::CreateObject();
			if (actionFrame->AddComponent(ActionLabel))
			{
				ActionLabel->SetAutoRefresh(false);
				ActionLabel->SetPosition(Vector2::ZERO_VECTOR);
				ActionLabel->SetSize(Vector2(1.f, 0.25f));
				ActionLabel->SetWrapText(true);
				ActionLabel->SetClampText(true);
				ActionLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				ActionLabel->SetVerticalAlignment(LabelComponent::VA_Center);
				ActionLabel->SetText(translator->TranslateText(TXT("ActionLabel"), fileName, sectionName));
				ActionLabel->SetAutoRefresh(true);
			}

			Float buttonWidth = 0.25f;
			Float spacing = (1 - (buttonWidth * 3.f)) / 4.f;
			Float outlineThickness = 0.1f;
			posX = spacing;

			GuiComponent* danceTransform = GuiComponent::CreateObject();
			if (actionFrame->AddComponent(danceTransform))
			{
				danceTransform->SetPosition(posX, 0.33f);
				danceTransform->SetSize(buttonWidth, 0.65f);
				posX += spacing + buttonWidth;

				DanceOutline = ColorRenderComponent::CreateObject();
				if (danceTransform->AddComponent(DanceOutline))
				{
					DanceOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (DanceOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				DanceButton = ButtonComponent::CreateObject();
				if (danceTransform->AddComponent(DanceButton))
				{
					DanceButton->SetAnchorTop(outlineThickness);
					DanceButton->SetAnchorRight(outlineThickness);
					DanceButton->SetAnchorBottom(outlineThickness);
					DanceButton->SetAnchorLeft(outlineThickness);
					DanceButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleDanceReleased, void, ButtonComponent*));
					DanceButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleAllDanceReleased, void, ButtonComponent*));
					DanceButton->SetCaptionText(translator->TranslateText(TXT("DanceButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (DanceButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("DanceButtonTooltip"), fileName, sectionName));
					}
				}
			}

			GuiComponent* giveBeverageTransform = GuiComponent::CreateObject();
			if (actionFrame->AddComponent(giveBeverageTransform))
			{
				giveBeverageTransform->SetPosition(posX, 0.33f);
				giveBeverageTransform->SetSize(buttonWidth, 0.65f);
				posX += spacing + buttonWidth;

				GainBeverageOutline = ColorRenderComponent::CreateObject();
				if (giveBeverageTransform->AddComponent(GainBeverageOutline))
				{
					GainBeverageOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (GainBeverageOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				GainBeverageButton = ButtonComponent::CreateObject();
				if (giveBeverageTransform->AddComponent(GainBeverageButton))
				{
					GainBeverageButton->SetAnchorTop(outlineThickness);
					GainBeverageButton->SetAnchorRight(outlineThickness);
					GainBeverageButton->SetAnchorBottom(outlineThickness);
					GainBeverageButton->SetAnchorLeft(outlineThickness);
					GainBeverageButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleGainBeverageReleased, void, ButtonComponent*));
					GainBeverageButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleAllGainBeverageReleased, void, ButtonComponent*));
					GainBeverageButton->SetCaptionText(translator->TranslateText(TXT("GainBeverageButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (GainBeverageButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("GainBeverageButtonTooltip"), fileName, sectionName));
					}
				}
			}

			GuiComponent* drinkTransform = GuiComponent::CreateObject();
			if (actionFrame->AddComponent(drinkTransform))
			{
				drinkTransform->SetPosition(posX, 0.33f);
				drinkTransform->SetSize(buttonWidth, 0.65f);
				posX += spacing + buttonWidth;

				DrinkOutline = ColorRenderComponent::CreateObject();
				if (drinkTransform->AddComponent(DrinkOutline))
				{
					DrinkOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (DrinkOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				DrinkButton = ButtonComponent::CreateObject();
				if (drinkTransform->AddComponent(DrinkButton))
				{
					DrinkButton->SetAnchorTop(outlineThickness);
					DrinkButton->SetAnchorRight(outlineThickness);
					DrinkButton->SetAnchorBottom(outlineThickness);
					DrinkButton->SetAnchorLeft(outlineThickness);
					DrinkButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleDrinkReleased, void, ButtonComponent*));
					DrinkButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleAllDrinkReleased, void, ButtonComponent*));
					DrinkButton->SetCaptionText(translator->TranslateText(TXT("DrinkButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (DrinkButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("DrinkButtonTooltip"), fileName, sectionName));
					}
				}
			}
		}

		posX = spacing;
		posY += (lineHeight * 3.f);

		//============ Spawn Controls ============
		FrameComponent* spawnFrame = FrameComponent::CreateObject();
		if (background->AddComponent(spawnFrame))
		{
			spawnFrame->SetPosition(posX, posY);
			spawnFrame->SetSize(1.f, lineHeight);
			spawnFrame->SetAnchorLeft(spacing);
			spawnFrame->SetAnchorRight(spacing);
			spawnFrame->SetBorderThickness(2.f);

			Float buttonWidth = 0.4f;
			Float buttonSpacing = (1 - (buttonWidth * 2.f)) / 3.f;
			Float outlineThickness = 0.1f;
			posX = buttonSpacing;

			GuiComponent* playerTransform = GuiComponent::CreateObject();
			if (spawnFrame->AddComponent(playerTransform))
			{
				playerTransform->SetPosition(posX, spacing);
				playerTransform->SetSize(buttonWidth, (1.f - spacing));
				posX += buttonWidth + spacing;

				SpawnPlayerOutline = ColorRenderComponent::CreateObject();
				if (playerTransform->AddComponent(SpawnPlayerOutline))
				{
					SpawnPlayerOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (SpawnPlayerOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				SpawnPlayerButton = ButtonComponent::CreateObject();
				if (playerTransform->AddComponent(SpawnPlayerButton))
				{
					SpawnPlayerButton->SetAnchorTop(outlineThickness);
					SpawnPlayerButton->SetAnchorRight(outlineThickness);
					SpawnPlayerButton->SetAnchorBottom(outlineThickness);
					SpawnPlayerButton->SetAnchorLeft(outlineThickness);
					SpawnPlayerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleSpawnPlayerReleased, void, ButtonComponent*));
					SpawnPlayerButton->SetCaptionText(translator->TranslateText(TXT("SpawnPlayerButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (SpawnPlayerButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("SpawnPlayerButtonTooltip"), fileName, sectionName));
					}
				}
			}

			GuiComponent* adminTransform = GuiComponent::CreateObject();
			if (spawnFrame->AddComponent(adminTransform))
			{
				adminTransform->SetPosition(posX, spacing);
				adminTransform->SetSize(buttonWidth, (1.f - spacing));
				posX += buttonWidth + spacing;

				SpawnAdminOutline = ColorRenderComponent::CreateObject();
				if (adminTransform->AddComponent(SpawnAdminOutline))
				{
					SpawnAdminOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (SpawnAdminOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				SpawnAdminButton = ButtonComponent::CreateObject();
				if (adminTransform->AddComponent(SpawnAdminButton))
				{
					SpawnAdminButton->SetAnchorTop(outlineThickness);
					SpawnAdminButton->SetAnchorRight(outlineThickness);
					SpawnAdminButton->SetAnchorBottom(outlineThickness);
					SpawnAdminButton->SetAnchorLeft(outlineThickness);
					SpawnAdminButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleSpawnAdminReleased, void, ButtonComponent*));
					SpawnAdminButton->SetCaptionText(translator->TranslateText(TXT("SpawnAdminButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (SpawnAdminButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("SpawnAdminButtonTooltip"), fileName, sectionName));
					}
				}
			}
		}

		posX = spacing;
		posY += lineHeight;

		//============ Movement Controls ============
		FrameComponent* movementFrame = FrameComponent::CreateObject();
		if (background->AddComponent(movementFrame))
		{
			movementFrame->SetPosition(posX, posY);
			movementFrame->SetSize(1.f, lineHeight * 6.f);
			movementFrame->SetAnchorLeft(spacing);
			movementFrame->SetAnchorRight(spacing);
			movementFrame->SetBorderThickness(2.f);

			Float subLineHeight = 1.f / 6.f;

			GuiComponent* teleportTransform = GuiComponent::CreateObject();
			if (movementFrame->AddComponent(teleportTransform))
			{
				teleportTransform->SetPosition(0.f, 0.f);
				teleportTransform->SetSize(0.5f, subLineHeight);
				teleportTransform->SetAnchorLeft(0.25f);
				teleportTransform->SetAnchorRight(0.25f);

				TeleportOutline = ColorRenderComponent::CreateObject();
				if (teleportTransform->AddComponent(TeleportOutline))
				{
					TeleportOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (TeleportOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				TeleportButton = ButtonComponent::CreateObject();
				if (teleportTransform->AddComponent(TeleportButton))
				{
					TeleportButton->SetAnchorTop(0.1f);
					TeleportButton->SetAnchorRight(0.1f);
					TeleportButton->SetAnchorBottom(0.1f);
					TeleportButton->SetAnchorLeft(0.1f);
					TeleportButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleTeleportReleased, void, ButtonComponent*));
					TeleportButton->SetCaptionText(translator->TranslateText(TXT("TeleportButton"), fileName, sectionName));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (TeleportButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("TeleportButtonTooltip"), fileName, sectionName));
					}
				}
			}

			ServerLabel = LabelComponent::CreateObject();
			if (movementFrame->AddComponent(ServerLabel))
			{
				ServerLabel->SetAutoRefresh(false);
				ServerLabel->SetPosition(0.f, subLineHeight);
				ServerLabel->SetSize(0.5f, subLineHeight);
				ServerLabel->SetWrapText(false);
				ServerLabel->SetClampText(true);
				ServerLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				ServerLabel->SetText(translator->TranslateText(TXT("ServerLabel"), fileName, sectionName));
				ServerLabel->SetAutoRefresh(true);
			}

			GuiComponent* serverTransform = GuiComponent::CreateObject();
			if (movementFrame->AddComponent(serverTransform))
			{
				serverTransform->SetPosition(0.7f, subLineHeight);
				serverTransform->SetSize(0.25f, subLineHeight);

				ServerOutline = ColorRenderComponent::CreateObject();
				if (serverTransform->AddComponent(ServerOutline))
				{
					ServerOutline->SolidColor = outlineColor;

					TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
					if (ServerOutline->AddComponent(tick))
					{
						tick->SetTicking(false);
						tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
					}
				}

				ServerField = TextFieldComponent::CreateObject();
				if (serverTransform->AddComponent(ServerField))
				{
					Float outlineThickness(0.1f);

					ServerField->SetAutoRefresh(false);
					ServerField->SetAnchorLeft(outlineThickness);
					ServerField->SetAnchorTop(outlineThickness);
					ServerField->SetAnchorRight(outlineThickness);
					ServerField->SetAnchorBottom(outlineThickness);
					ServerField->MaxNumCharacters = 4;
					ServerField->bAutoSelect = true;
					ServerField->OnAllowTextInput = [](const DString& txt)
					{
						//Ports only accept numbers
						return txt.HasRegexMatch(TXT("[0-9]"));
					};
					ServerField->OnReturn = SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleServerReturn, void, TextFieldComponent*);

					ServerField->SetWrapText(false);
					ServerField->SetClampText(false);
					ServerField->SetText(Int(ConnectionManager::DP_Game).ToString());
					ServerField->SetAutoRefresh(true);

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (ServerField->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("ServerFieldTooltip"), fileName, sectionName));
					}
				}
			}

			InputLabel = LabelComponent::CreateObject();
			if (movementFrame->AddComponent(InputLabel))
			{
				InputLabel->SetAutoRefresh(false);
				InputLabel->SetPosition(0.f, subLineHeight * 2.f);
				InputLabel->SetSize(1.f, subLineHeight);
				InputLabel->SetWrapText(true);
				InputLabel->SetClampText(true);
				InputLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				InputLabel->SetVerticalAlignment(LabelComponent::VA_Center);
				InputLabel->SetText(translator->TranslateText(TXT("InputLabel"), fileName, sectionName));
				InputLabel->SetAutoRefresh(true);
			}

			ScrollbarComponent* inputScrollbar = ScrollbarComponent::CreateObject();
			if (movementFrame->AddComponent(inputScrollbar))
			{
				inputScrollbar->SetPosition(0.f, subLineHeight * 3.f);
				inputScrollbar->SetSize(1.f, subLineHeight * 3.f);
				inputScrollbar->SetAnchorLeft(0.05f);
				inputScrollbar->SetAnchorRight(0.05f);
				inputScrollbar->SetHideControlsWhenFull(true);

				GuiEntity* viewedObject = GuiEntity::CreateObject();
				viewedObject->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
				viewedObject->SetAutoSizeVertical(true);
				inputScrollbar->SetViewedObject(viewedObject);

				InputList = VerticalList::CreateObject();
				if (viewedObject->AddComponent(InputList))
				{
					InputList->ComponentSpacing = 3.f;
					if (InputList->GetRefreshingTick())
					{
						//Disable refreshing tick since the refresh function will be explicitly called.
						InputList->GetRefreshingTick()->SetTicking(false);
					}

					PopulatePlayerList();
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (inputScrollbar->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("InputListTooltip"), fileName, sectionName));
				}
			}
		}

		posY += (lineHeight * 6.f);

		GuiComponent* floodTransform = GuiComponent::CreateObject();
		if (background->AddComponent(floodTransform))
		{
			floodTransform->SetPosition(0.25f, posY);
			floodTransform->SetSize(0.5f, lineHeight);

			FloodOutline = ColorRenderComponent::CreateObject();
			if (floodTransform->AddComponent(FloodOutline))
			{
				FloodOutline->SolidColor = outlineColor;

				TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
				if (FloodOutline->AddComponent(tick))
				{
					tick->SetTicking(false);
					tick->SetTickExHandler(SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleOutlineTick, void, Float, TickComponent*));
				}
			}

			FloodButton = ButtonComponent::CreateObject();
			if (floodTransform->AddComponent(FloodButton))
			{
				Float outlineThickness(0.1f);
				FloodButton->SetAnchorTop(outlineThickness);
				FloodButton->SetAnchorRight(outlineThickness);
				FloodButton->SetAnchorBottom(outlineThickness);
				FloodButton->SetAnchorLeft(outlineThickness);
				FloodButton->SetCaptionText(translator->TranslateText(TXT("FloodButton"), fileName, sectionName));
				FloodButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleFloodReleased, void, ButtonComponent*));
				FloodButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleRefFloodReleased, void, ButtonComponent*));

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (FloodButton->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("FloodButtonTooltip"), fileName, sectionName));
				}
			}
		}

		posY += lineHeight;

		CloseButton = ButtonComponent::CreateObject();
		if (background->AddComponent(CloseButton))
		{
			CloseButton->SetPosition(0.25f, posY);
			CloseButton->SetSize(0.5f, lineHeight * 0.75f);
			CloseButton->SetAnchorBottom(spacing * 6.f);
			CloseButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleCloseReleased, void, ButtonComponent*));
			CloseButton->SetCaptionText(translator->TranslateText(TXT("CloseButton"), fileName, sectionName));
		}
	}
}

void ClientHackerPanel::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	Focus->TabOrder.push_back(PermissionVarRepButton);
	Focus->TabOrder.push_back(PermissionRpcButton);
	Focus->TabOrder.push_back(PermissionRpcButton);
	Focus->TabOrder.push_back(PermissionRefAnythingButton);
	Focus->TabOrder.push_back(PermissionInstantiationButton);
	Focus->TabOrder.push_back(DanceButton);
	Focus->TabOrder.push_back(GainBeverageButton);
	Focus->TabOrder.push_back(DrinkButton);
	Focus->TabOrder.push_back(SpawnPlayerButton);
	Focus->TabOrder.push_back(SpawnAdminButton);
	Focus->TabOrder.push_back(TeleportButton);
	Focus->TabOrder.push_back(ServerField);
	Focus->TabOrder.push_back(FloodButton);
	Focus->TabOrder.push_back(CloseButton);
}

bool ClientHackerPanel::HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (bIsTeleportingPlayer && sfmlEvent.button == sf::Mouse::Left)
	{
		Vector2 mousePos(sfmlEvent.x, sfmlEvent.y);
		if (!IsWithinBounds(Vector2(sfmlEvent.x, sfmlEvent.y)))
		{
			if (eventType == sf::Event::MouseButtonReleased)
			{
				GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
				CHECK(localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr)

				NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
				CHECK(localNightclub != nullptr)

				if (TopDownCamera* cam = localNightclub->GetCamera())
				{
					Rotator rotation;
					Vector3 teleportDest;
					cam->CalculatePixelProjection(localGraphics->GetPrimaryWindow()->GetWindowSize(), mousePos, OUT rotation, OUT teleportDest);

					if (Player* localPlayer = localNightclub->GetLocalPlayer())
					{
						teleportDest.Z = localPlayer->ReadTranslation().Z;
						localPlayer->Teleport(teleportDest);
						bIsTeleportingPlayer = false;
					}
				}
			}

			return true;
		}
	}

	return Super::HandleConsumableMouseClick(mouse, sfmlEvent, eventType);
}

void ClientHackerPanel::Destroyed ()
{
	//Just in case this panel is overriding the mouse icon.
	bIsTeleportingPlayer = false;

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetCamera() != nullptr)
	{
		//Recenter the camera
		localNightclub->GetCamera()->EditTranslation().X = 0.f;
	}

	Super::Destroyed();
}

void ClientHackerPanel::PopulatePlayerList ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr && InputList != nullptr)

	for (ObjectIterator iter(localEngine->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
	{
		if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
		{
			CheckboxComponent* checkbox = CheckboxComponent::CreateObject();
			if (InputList->AddComponent(checkbox))
			{
				checkbox->SetSize(Vector2(0.8f, 24.f));
				checkbox->SetAnchorLeft(0.1f);
				checkbox->SetAnchorRight(0.1f);
				if (checkbox->GetCaptionComponent() != nullptr)
				{
					checkbox->GetCaptionComponent()->SetText(player->ReadPlayerName());
				}

				InputComponent* input = dynamic_cast<InputComponent*>(player->FindComponent(InputComponent::SStaticClass(), false));
				checkbox->SetChecked(input != nullptr);
				checkbox->OnChecked = SDFUNCTION_1PARAM(this, ClientHackerPanel, HandleInputChecked, void, CheckboxComponent*);
				InputCheckboxes.push_back(checkbox);
			}
		}
	}

	//Since the refreshing tick is disabled, must manually call refresh.
	InputList->RefreshComponentTransforms();
}

void ClientHackerPanel::HandleOutlineTick (Float deltaSec, TickComponent* tick)
{
	if (ColorRenderComponent* color = dynamic_cast<ColorRenderComponent*>(tick->GetOwner()))
	{
		Float alpha = color->SolidColor.Source.a;
		alpha -= Utils::Max<Float>(AlphaFadePerSecond * deltaSec, 1.f);
		if (alpha <= 0.f)
		{
			color->SolidColor.Source.a = 0;
			tick->SetTicking(false);
		}
		else
		{
			color->SolidColor.Source.a = static_cast<sf::Uint8>(alpha.ToInt().ToUnsignedInt());
		}
	}
}

bool ClientHackerPanel::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt)
{
	return bIsTeleportingPlayer;
}

void ClientHackerPanel::HandlePermissionVarRepReleased (ButtonComponent* button)
{
	if (PermissionVarRepOutline != nullptr)
	{
		PermissionVarRepOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(PermissionVarRepOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	if (HackedConnectionManager* connections = dynamic_cast<HackedConnectionManager*>(ConnectionManager::GetConnectionManager()))
	{
		//Simulate client attempting to elevate permissions to replicate any variable.
		connections->ElevatePermissions(NetworkGlobals::P_SendData);
	}
}

void ClientHackerPanel::HandlePermissionRpcReleased (ButtonComponent* button)
{
	if (PermissionRpcOutline != nullptr)
	{
		PermissionRpcOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(PermissionRpcOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	if (HackedConnectionManager* connections = dynamic_cast<HackedConnectionManager*>(ConnectionManager::GetConnectionManager()))
	{
		//Simulate client attempting to elevate permissions to invoke any RPC
		connections->ElevatePermissions(NetworkGlobals::P_Rpc);
	}
}

void ClientHackerPanel::HandlePermissionRefAnythingReleased (ButtonComponent* button)
{
	if (PermissionRefAnythingOutline != nullptr)
	{
		PermissionRefAnythingOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(PermissionRefAnythingOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	if (HackedConnectionManager* connections = dynamic_cast<HackedConnectionManager*>(ConnectionManager::GetConnectionManager()))
	{
		//Simulate client attempting to elevate permissions to reference any network Entity.
		connections->ElevatePermissions(NetworkGlobals::P_RefAnything);
	}
}

void ClientHackerPanel::HandlePermissionInstantiationReleased (ButtonComponent* button)
{
	if (PermissionInstantiationOutline != nullptr)
	{
		PermissionInstantiationOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(PermissionInstantiationOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	if (HackedConnectionManager* connections = dynamic_cast<HackedConnectionManager*>(ConnectionManager::GetConnectionManager()))
	{
		//Simulate client attempting to elevate permissions to instantiate objects on the server.
		connections->ElevatePermissions(NetworkGlobals::P_InstantiateComponent);
	}
}

void ClientHackerPanel::HandleDanceReleased (ButtonComponent* button)
{
	if (DanceOutline != nullptr)
	{
		DanceOutline->SolidColor = Color(0, 150, 0, 255);
		if (TickComponent* tick = dynamic_cast<TickComponent*>(DanceOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	if (localNightclub->GetLocalPlayer() != nullptr)
	{
		if (DanceComponent* danceComp = localNightclub->GetLocalPlayer()->GetDanceComp())
		{
			danceComp->ServerSetDancing(BaseRpc::RT_Servers, true);

			//Simulate the client hacking local checks (in this case, ignoring if dancing is permitted or not).
			danceComp->ClientHackGrantRepVarWritePermissions();
			danceComp->SetEnabled(true);
			danceComp->SetDancing(true);
		}
	}
}

void ClientHackerPanel::HandleAllDanceReleased (ButtonComponent* button)
{
	if (DanceOutline != nullptr)
	{
		DanceOutline->SolidColor = Color(150, 150, 0, 255); //Yellow color
		if (TickComponent* tick = dynamic_cast<TickComponent*>(DanceOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for (ObjectIterator iter(localEngine->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
	{
		if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
		{
			if (DanceComponent* danceComp = player->GetDanceComp())
			{
				danceComp->ClientHackGrantRepVarWritePermissions();
				danceComp->SetEnabled(true);
				danceComp->SetDancing(true);
			}
		}
	}
}

void ClientHackerPanel::HandleGainBeverageReleased (ButtonComponent* button)
{
	if (GainBeverageOutline != nullptr)
	{
		GainBeverageOutline->SolidColor = Color(0, 150, 0, 255);
		if (TickComponent* tick = dynamic_cast<TickComponent*>(GainBeverageOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	if (Player* player = localNightclub->GetLocalPlayer())
	{
		player->ClientHackElevatePermissions();
		player->SetHasDrink(true);
	}
}

void ClientHackerPanel::HandleAllGainBeverageReleased (ButtonComponent* button)
{
	if (GainBeverageOutline != nullptr)
	{
		GainBeverageOutline->SolidColor = Color(150, 150, 0, 255); //Yellow color
		if (TickComponent* tick = dynamic_cast<TickComponent*>(GainBeverageOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for (ObjectIterator iter(localEngine->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
	{
		if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
		{
			player->ClientHackElevatePermissions();
			player->SetHasDrink(true);
		}
	}
}

void ClientHackerPanel::HandleDrinkReleased (ButtonComponent* button)
{
	if (DrinkOutline != nullptr)
	{
		DrinkOutline->SolidColor = Color(0, 150, 0, 255);
		if (TickComponent* tick = dynamic_cast<TickComponent*>(DrinkOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	if (Player* player = localNightclub->GetLocalPlayer())
	{
		player->ClientHackElevatePermissions();
		player->SetHasDrink(true);
		player->SetCanDrink(true);
		player->SetIsDrinking(true);
	}
}

void ClientHackerPanel::HandleAllDrinkReleased (ButtonComponent* button)
{
	if (DrinkOutline != nullptr)
	{
		DrinkOutline->SolidColor = Color(150, 150, 0, 255); //Yellow color
		if (TickComponent* tick = dynamic_cast<TickComponent*>(DrinkOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for (ObjectIterator iter(localEngine->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
	{
		if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
		{
			player->ClientHackElevatePermissions();
			player->SetHasDrink(true);
			player->SetCanDrink(true);
			player->SetIsDrinking(true);
		}
	}
}

void ClientHackerPanel::HandleSpawnPlayerReleased (ButtonComponent* button)
{
	if (SpawnPlayerOutline != nullptr)
	{
		SpawnPlayerOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(SpawnPlayerOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	static Int spawnId = 1;
	spawnId++;
	
	Player* newPlayer = Player::CreateObject();
	newPlayer->SetPlayerName(TXT("SpawnedPlayer_") + spawnId.ToString());
	newPlayer->Teleport(localNightclub->GetLocalPlayer()->ReadAbsTranslation());

	localNightclub->AddPlayer(newPlayer);
}

void ClientHackerPanel::HandleSpawnAdminReleased (ButtonComponent* button)
{
	if (SpawnAdminOutline != nullptr)
	{
		SpawnAdminOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(SpawnAdminOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	if (localNightclub->GetAdminObject() == nullptr)
	{
		NightclubAdminObject* adminObj = NightclubAdminObject::CreateObject();
		localNightclub->SetAdminObject(adminObj);

		//Since the server will probably reject the client's attempt to spawn an admin object on the server, the admin object's NetInitialize won't be called. Manually call setup InputComponent to allow the client to at least open the admin panel.
		adminObj->SetupInput();
	}
}

void ClientHackerPanel::HandleTeleportReleased (ButtonComponent* button)
{
	if (TeleportOutline != nullptr)
	{
		TeleportOutline->SolidColor.Source.a = 255;
		if (TickComponent* tick = dynamic_cast<TickComponent*>(TeleportOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	bIsTeleportingPlayer = true;

	if (MousePointer* mouse = MousePointer::GetMousePointer())
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		Texture* mouseIcon = localTexturePool->EditTexture(HashedString("Engine.Input.CursorPrecision"));
		if (mouseIcon)
		{
			mouse->PushMouseIconOverride(mouseIcon, SDFUNCTION_2PARAM(this, ClientHackerPanel, HandleMouseIconOverride, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
		}
	}
}

void ClientHackerPanel::HandleServerReturn (TextFieldComponent* uiComponent)
{
	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		DString strPort = uiComponent->GetContent();
		Int port = strPort.Atoi();
		if (port >= 0)
		{
			//Attempt to open a connection regardless if the client is already connected or the server simply doesn't exist. Client and server applications should be able to handle these cases.
			connections->OpenConnection(sf::IpAddress::LocalHost, static_cast<unsigned short>(port.ToUnsignedInt()));

			if (ServerOutline != nullptr)
			{
				ServerOutline->SolidColor.Source.a = 255;
				if (TickComponent* tick = dynamic_cast<TickComponent*>(ServerOutline->FindComponent(TickComponent::SStaticClass(), false)))
				{
					tick->SetTicking(true);
				}
			}
		}
	}
}

void ClientHackerPanel::HandleInputChecked (CheckboxComponent* uiComponent)
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr && uiComponent != nullptr && uiComponent->GetCaptionComponent() != nullptr)

	DString playerName = uiComponent->GetCaptionComponent()->GetContent();

	for (ObjectIterator iter(localEngine->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
	{
		if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
		{
			if (player->ReadPlayerName().Compare(playerName, DString::CC_CaseSensitive) == 0)
			{
				if (uiComponent->IsChecked())
				{
					player->SetupInputComponent();
				}
				else
				{
					player->RemoveInputComponent();
				}

				break;
			}
		}
	}
}

void ClientHackerPanel::HandleFloodReleased (ButtonComponent* button)
{
	if (FloodOutline != nullptr)
	{
		FloodOutline->SolidColor = Color(0, 150, 0, 255);
		if (TickComponent* tick = dynamic_cast<TickComponent*>(FloodOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	if (ClientHackerTestObject* testObj = localNightclub->GetHackerTestObject())
	{
		//Call flood counter a bunch of times to attempt to lag the server.
		for (size_t i = 0; i < 1000000; ++i)
		{
			testObj->ServerIncrementFloodCounter(BaseRpc::RT_Servers);
		}
	}
}

void ClientHackerPanel::HandleRefFloodReleased (ButtonComponent* button)
{
	if (FloodOutline != nullptr)
	{
		FloodOutline->SolidColor = Color(150, 0, 0, 255);
		if (TickComponent* tick = dynamic_cast<TickComponent*>(FloodOutline->FindComponent(TickComponent::SStaticClass(), false)))
		{
			tick->SetTicking(true);
		}
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr && localNightclub->GetOwningEngine() != nullptr)

	if (ClientHackerTestObject* testObj = localNightclub->GetHackerTestObject())
	{
		for (ObjectIterator iter(localNightclub->GetOwningEngine()->GetEntityHashNumber()); iter.SelectedObject.IsValid(); ++iter)
		{
			if (Player* player = dynamic_cast<Player*>(iter.SelectedObject.Get()))
			{
				testObj->ServerRunReferenceCheck(BaseRpc::RT_Servers, player);
			}
		}
	}
}

void ClientHackerPanel::HandleCloseReleased (ButtonComponent* button)
{
	Destroy();
}
NC_END