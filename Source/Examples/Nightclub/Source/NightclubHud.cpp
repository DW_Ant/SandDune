/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubHud.cpp
=====================================================================
*/

#include "DanceComponent.h"
#include "NightclubEngineComponent.h"
#include "NightclubHud.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::NightclubHud, SD::GuiEntity)
NC_BEGIN

void NightclubHud::InitProps ()
{
	Super::InitProps();

	DanceLabel = nullptr;
	DrinkLabel = nullptr;
	HackerLabel = nullptr;
	AdminLabel = nullptr;
	PingLabel = nullptr;
	BotWarningLabel = nullptr;
	BotWarningTick = nullptr;
}

void NightclubHud::ConstructUI ()
{
	Float lineHeight = 0.05f;
	Float spacing = 0.01f;
	Float posY = spacing;

	DanceLabel = LabelComponent::CreateObject();
	DrinkLabel = LabelComponent::CreateObject();
	HackerLabel = LabelComponent::CreateObject();
	AdminLabel = LabelComponent::CreateObject();

	std::vector<LabelComponent*> compsToInit
	{
		DanceLabel,
		DrinkLabel,
		HackerLabel,
		AdminLabel
	};

	for (LabelComponent* comp : compsToInit)
	{
		if (AddComponent(comp))
		{
			comp->SetAutoRefresh(false);
			comp->SetPosition(Vector2(spacing, posY));
			comp->SetSize(Vector2(0.5f, lineHeight));
			comp->SetWrapText(false);
			comp->SetClampText(true);
			comp->SetAutoRefresh(true);
			posY += spacing + lineHeight;
		}
	}

	//This will be revealed when the corresponding object is replicated
	HackerLabel->SetVisibility(false);
	AdminLabel->SetVisibility(false);

	PingLabel = LabelComponent::CreateObject();
	if (AddComponent(PingLabel))
	{
		PingLabel->SetAutoRefresh(false);
		PingLabel->SetAnchorTop(0.f);
		PingLabel->SetAnchorRight(0.f);
		PingLabel->SetSize(Vector2(0.5f, lineHeight));
		PingLabel->SetWrapText(false);
		PingLabel->SetClampText(true);
		PingLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
		PingLabel->SetAutoRefresh(true);

		TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
		if (PingLabel->AddComponent(tick))
		{
			tick->SetTickInterval(4.f); //High tick interval since the heartbeats don't update frequently anyways.
			tick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubHud, HandlePingTick, void, Float));
		}
	}
}

void NightclubHud::UpdatePlayerStatus (Player* player)
{
	if (player == nullptr)
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("Cannot update Nightclub's hud status without specifying a player instance."));
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	if (DanceLabel != nullptr && player->GetDanceComp() != nullptr)
	{
		DString localizationKey = (player->GetDanceComp()->IsEnabled()) ? TXT("DanceCtrls") : TXT("DanceForbidden");
		DanceLabel->SetText(translator->TranslateText(localizationKey, NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubHud")));
	}

	if (DrinkLabel != nullptr)
	{
		DString localizationKey;
		if (!player->HasDrink())
		{
			localizationKey = TXT("DrinkEmptyHanded");
		}
		else if (!player->IsDrinkingEnabled())
		{
			localizationKey = TXT("DrinkForbidden");
		}
		else
		{
			localizationKey = TXT("DrinkCtrls");
		}

		DrinkLabel->SetText(translator->TranslateText(localizationKey, NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubHud")));
	}
}

void NightclubHud::RevealHackerControls ()
{
	if (HackerLabel == nullptr || HackerLabel->IsVisible())
	{
		//Already visible. Do nothing
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	HackerLabel->SetVisibility(true);
	HackerLabel->SetText(translator->TranslateText(TXT("HackerCtrls"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubHud")));
}

void NightclubHud::RevealAdminControls ()
{
	if (AdminLabel == nullptr || AdminLabel->IsVisible())
	{
		//Already visible. Do nothing
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	AdminLabel->SetVisibility(true);
	AdminLabel->SetText(translator->TranslateText(TXT("AdminCtrls"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubHud")));
}

void NightclubHud::FlashBotWarning ()
{
	if (BotWarningLabel != nullptr)
	{
		//Already flashing the warning
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	BotWarningLabel = LabelComponent::CreateObject();
	if (AddComponent(BotWarningLabel))
	{
		BotWarningLabel->SetAutoRefresh(false);
		BotWarningLabel->SetPosition(Vector2(0.1f, 0.2f));
		BotWarningLabel->SetSize(Vector2(0.8f, 0.5f));
		BotWarningLabel->SetWrapText(true);
		BotWarningLabel->SetClampText(false);
		BotWarningLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
		BotWarningLabel->SetText(translator->TranslateText(TXT("BotWarningLabel"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubHud")));
		BotWarningLabel->SetCharacterSize(32);
		BotWarningLabel->SetAutoRefresh(true);

		BotWarningTick = TickComponent::CreateObject(TICK_GROUP_GUI);
		if (BotWarningLabel->AddComponent(BotWarningTick))
		{
			BotWarningTick->SetTickInterval(0.75f);
			BotWarningTick->SetTickHandler(SDFUNCTION_1PARAM(this, NightclubHud, HandleBotWarningTick, void, Float));
		}
	}
}

void NightclubHud::SetBotModeActive (bool bActive)
{
	if (BotWarningLabel != nullptr && BotWarningTick != nullptr)
	{
		BotWarningLabel->SetVisibility(bActive);
		BotWarningTick->SetTicking(bActive);
	}
}

void NightclubHud::HandlePingTick (Float deltaSec)
{
	if (PingLabel == nullptr)
	{
		return;
	}

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		for (SignatureIterator iter(false); iter.GetSelectedSignature() != nullptr; ++iter)
		{
			if (iter.GetSelectedSignature()->HasRole(NetworkSignature::NR_Server))
			{
				Float ping = connections->GetPing(iter.GetSelectedSignature());
				if (ping > 0.f)
				{
					ping *= 1000.f; //Convert seconds to milliseconds
					PingLabel->SetText(DString::CreateFormattedString(TXT("Ping: %s ms"), ping.ToInt()));
					return;
				}

				break;
			}
		}
	}

	PingLabel->SetText(DString::EmptyString);
}

void NightclubHud::HandleBotWarningTick (Float deltaSec)
{
	if (BotWarningLabel != nullptr)
	{
		BotWarningLabel->SetVisibility(!BotWarningLabel->IsVisible());
	}
}
NC_END