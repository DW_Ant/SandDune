/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DuplicateNameAccessControl.cpp
=====================================================================
*/

#include "DuplicateNameAccessControl.h"
#include "NightclubClientIdentifier.h"

IMPLEMENT_CLASS(SD::NC::DuplicateNameAccessControl, SD::AccessControl)
NC_BEGIN

const unsigned char DuplicateNameAccessControl::REJECT_NUM = AccessControl::RR_Max + 1;

DuplicateNameAccessControl::SRecentAttempt::SRecentAttempt (const DString& inPlayerName, Float inConnectionTime) :
	PlayerName(inPlayerName),
	ConnectionTime(inConnectionTime)
{
	//Noop
}

void DuplicateNameAccessControl::InitProps ()
{
	Super::InitProps();

	RelevantRoles = NetworkSignature::NR_Client; //Only check this against clients

	RecentAttemptsPurgeTime = 10.f; //ten seconds
}

bool DuplicateNameAccessControl::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	NightclubClientIdentifier* clientIdentifier = dynamic_cast<NightclubClientIdentifier*>(signature->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
	if (clientIdentifier == nullptr)
	{
		outRejectReason = AccessControl::RR_InvalidData;
		return false;
	}

	for (const DString& nameInUse : NamesInUse)
	{
		if (nameInUse.Compare(clientIdentifier->ReadPlayerName(), DString::CC_IgnoreCase) == 0)
		{
			outRejectReason = REJECT_NUM;
			return false;
		}
	}

	PurgeStaleConnectionAttempts();
	for (const SRecentAttempt& recentAttempt : RecentAttempts)
	{
		if (recentAttempt.PlayerName.Compare(clientIdentifier->ReadPlayerName(), DString::CC_IgnoreCase) == 0)
		{
			outRejectReason = REJECT_NUM;
			return false;
		}
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	RecentAttempts.emplace_back(SRecentAttempt(clientIdentifier->ReadPlayerName(), localEngine->GetElapsedTime()));

	return true;
}

void DuplicateNameAccessControl::AddNameInUse (const DString& newName)
{
	ContainerUtils::AddUnique(NamesInUse, newName);
}

void DuplicateNameAccessControl::RemoveNameInUse (const DString& nameToRemove)
{
	ContainerUtils::RemoveItem(NamesInUse, nameToRemove);
}

void DuplicateNameAccessControl::PurgeStaleConnectionAttempts ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	size_t i = 0;
	while (i < RecentAttempts.size())
	{
		if ((localEngine->GetElapsedTime() - RecentAttempts.at(i).ConnectionTime) >= RecentAttemptsPurgeTime)
		{
			RecentAttempts.erase(RecentAttempts.begin() + i);
			continue;
		}

		++i;
	}
}
NC_END