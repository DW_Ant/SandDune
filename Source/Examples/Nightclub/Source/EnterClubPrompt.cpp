/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EnterClubPrompt.cpp
=====================================================================
*/

#include "EnterClubPrompt.h"
#include "NightclubClientIdentifier.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::EnterClubPrompt, SD::GuiEntity)
NC_BEGIN

void EnterClubPrompt::InitProps ()
{
	Super::InitProps();

	NamePrompt = nullptr;
	NameField = nullptr;
	ServerPrompt = nullptr;
	ServerDestination = nullptr;
	ResponseText = nullptr;
	ConnectButton = nullptr;
	CloseButton = nullptr;
}

void EnterClubPrompt::BeginObject ()
{
	Super::BeginObject();

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OnConnectionAccepted.RegisterHandler(SDFUNCTION_1PARAM(this, EnterClubPrompt, HandleConnectionAccepted, void, NetworkSignature*));
		connections->OnConnectionRejected.RegisterHandler(SDFUNCTION_2PARAM(this, EnterClubPrompt, HandleConnectionRejected, void, const sf::IpAddress&, Int));
	}
}

void EnterClubPrompt::ConstructUI ()
{
	Super::ConstructUI();

	Float lineHeight = 0.1f;
	Float spacing = 0.05f;

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	FrameComponent* background = FrameComponent::CreateObject();
	if (AddComponent(background))
	{
		background->SetPosition(Vector2(0.25f, 0.f));
		background->SetSize(Vector2(0.5f, 0.4f));
		background->SetAlwaysConsumeMouseEvents(true);
		
		NamePrompt = LabelComponent::CreateObject();
		if (background->AddComponent(NamePrompt))
		{
			NamePrompt->SetAutoRefresh(false);
			NamePrompt->SetPosition(Vector2(spacing, spacing));
			NamePrompt->SetSize(Vector2(0.5f - (spacing * 2.f), lineHeight));
			NamePrompt->SetHorizontalAlignment(LabelComponent::HA_Right);
			NamePrompt->SetWrapText(false);
			NamePrompt->SetClampText(true);
			NamePrompt->SetText(translator->TranslateText(TXT("NamePrompt"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			NamePrompt->SetAutoRefresh(true);
		}

		NameField = TextFieldComponent::CreateObject();
		if (background->AddComponent(NameField))
		{
			NameField->SetAutoRefresh(false);
			NameField->SetPosition(Vector2(0.5f + spacing, spacing));
			NameField->SetSize(Vector2(0.5f - (spacing * 2.f), lineHeight));
			NameField->SetWrapText(false);
			NameField->SetClampText(true);
			NameField->MaxNumCharacters = 32;
			NameField->SetText(TXT("Dancer"));
			NameField->SetAutoRefresh(true);

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (NameField->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("NameTooltip"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			}
		}

		ServerPrompt = LabelComponent::CreateObject();
		if (background->AddComponent(ServerPrompt))
		{
			ServerPrompt->SetAutoRefresh(false);
			ServerPrompt->SetPosition(Vector2(spacing, lineHeight + (spacing * 2.f)));
			ServerPrompt->SetSize(Vector2(0.5f - (spacing * 2.f), lineHeight));
			ServerPrompt->SetHorizontalAlignment(LabelComponent::HA_Right);
			ServerPrompt->SetWrapText(false);
			ServerPrompt->SetClampText(true);
			ServerPrompt->SetText(translator->TranslateText(TXT("ServerPrompt"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			ServerPrompt->SetAutoRefresh(true);
		}

		ServerDestination = LabelComponent::CreateObject();
		if (background->AddComponent(ServerDestination))
		{
			ServerDestination->SetAutoRefresh(false);
			ServerDestination->SetPosition(Vector2(0.5f + spacing, lineHeight + (spacing * 2.f)));
			ServerDestination->SetSize(Vector2(0.5f - (spacing * 2.f), lineHeight));
			ServerDestination->SetWrapText(false);
			ServerDestination->SetClampText(true);
			ServerDestination->SetText(TXT("127.0.0.1"));
			ServerDestination->SetAutoRefresh(true);
		}

		LabelComponent* bobSays = LabelComponent::CreateObject();
		if (background->AddComponent(bobSays))
		{
			bobSays->SetAutoRefresh(false);
			bobSays->SetPosition(Vector2(spacing, (lineHeight * 2.f) + (spacing * 3.f)));
			bobSays->SetSize(Vector2((1.f - (spacing * 2.f)), lineHeight));
			bobSays->SetWrapText(false);
			bobSays->SetClampText(true);
			bobSays->SetText(translator->TranslateText(TXT("BobSays"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			bobSays->SetAutoRefresh(true);
		}

		ResponseText = LabelComponent::CreateObject();
		if (background->AddComponent(ResponseText))
		{
			ResponseText->SetAutoRefresh(false);
			ResponseText->SetPosition(Vector2(spacing, (lineHeight * 3.f) + (spacing * 3.f)));
			ResponseText->SetSize(Vector2((1.f - (spacing * 2.f)), lineHeight * 3.f));
			ResponseText->SetWrapText(true);
			ResponseText->SetClampText(true);
			ResponseText->SetText(translator->TranslateText(TXT("ResponseTextDefault"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			ResponseText->SetAutoRefresh(true);
		}

		ConnectButton = ButtonComponent::CreateObject();
		if (background->AddComponent(ConnectButton))
		{
			ConnectButton->SetAnchorLeft(spacing);
			ConnectButton->SetAnchorBottom(spacing);
			ConnectButton->SetSize(Vector2(0.4f, lineHeight));
			ConnectButton->SetCaptionText(translator->TranslateText(TXT("ConnectButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			ConnectButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EnterClubPrompt, HandleConnectReleased, void, ButtonComponent*));
		}

		CloseButton = ButtonComponent::CreateObject();
		if (background->AddComponent(CloseButton))
		{
			CloseButton->SetAnchorRight(spacing);
			CloseButton->SetAnchorBottom(spacing);
			CloseButton->SetSize(Vector2(0.4f, lineHeight));
			CloseButton->SetCaptionText(translator->TranslateText(TXT("CloseButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
			CloseButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EnterClubPrompt, HandleCloseReleased, void, ButtonComponent*));
		}
	}
}

void EnterClubPrompt::Destroyed ()
{
	if (ConnectionManager* connection = ConnectionManager::GetConnectionManager())
	{
		connection->OnConnectionAccepted.UnregisterHandler(SDFUNCTION_1PARAM(this, EnterClubPrompt, HandleConnectionAccepted, void, NetworkSignature*));
		connection->OnConnectionRejected.UnregisterHandler(SDFUNCTION_2PARAM(this, EnterClubPrompt, HandleConnectionRejected, void, const sf::IpAddress&, Int));
	}

	Super::Destroyed();
}

void EnterClubPrompt::HandleCloseReleased (ButtonComponent* button)
{
	Destroy();
}

void EnterClubPrompt::HandleConnectReleased (ButtonComponent* button)
{
	CHECK(ServerDestination != nullptr && NameField != nullptr)

	NetworkSignature* localSignature = NetworkSignature::GetLocalSignature();
	CHECK(localSignature != nullptr)
	if (NightclubClientIdentifier* identifier = dynamic_cast<NightclubClientIdentifier*>(localSignature->FindComponent(NightclubClientIdentifier::SStaticClass(), false)))
	{
		identifier->SetPlayerName(NameField->GetContent());
	}

	DString ipStr = ServerDestination->GetContent();
	sf::IpAddress ipAddress(ipStr.ToSfmlString());

	if (ConnectionManager* connections = ConnectionManager::GetConnectionManager())
	{
		connections->OpenConnection(ipAddress, ConnectionManager::DP_MasterServer);
	}
}

void EnterClubPrompt::HandleConnectionAccepted (NetworkSignature* remoteSignature)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)
	if (localNightclub->GetLocalPlayer() != nullptr)
	{
		//A server is going to spawn a player for this client. Destroy the local controlled one
		localNightclub->GetLocalPlayer()->Destroy();
	}

	Destroy();
}

void EnterClubPrompt::HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason)
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr && ResponseText != nullptr)
	ResponseText->SetText(translator->TranslateText(DString::CreateFormattedString(TXT("ResponseText_%s"), reason), NightclubEngineComponent::TRANSLATION_FILE, TXT("EnterClubPrompt")));
}
NC_END