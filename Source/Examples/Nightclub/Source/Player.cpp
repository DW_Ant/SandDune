/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Player.cpp
=====================================================================
*/

#include "DanceComponent.h"
#include "NightclubEngineComponent.h"
#include "NightclubHud.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::Player, SD::Entity)
NC_BEGIN

void Player::InitProps ()
{
	Super::InitProps();

	Input = nullptr;
	Circle = nullptr;
	RoleComp = nullptr;
	NetComp = nullptr;
	MoveTick = nullptr;
	PhysComp = nullptr;
	NameplateTransform = nullptr;
	Nameplate = nullptr;
	DrinkTransform = nullptr;

	bCanDrink = true;
	bHasDrink = false;
	bIsDrinking = false;
	Radius = 96.f;
	PlayerName = DString::EmptyString;
	NameplateSize = 128.f;
	Destination = Vector3::ZERO_VECTOR;
	RunSpeed = 2000.f;

	bDraggingMouse = false;
}

void Player::BeginObject ()
{
	Super::BeginObject();

	SetUpdateBehavior(SceneTransform::UB_Continuous);

	NightclubEngineComponent* nightclubEngine = NightclubEngineComponent::Find();
	CHECK(nightclubEngine != nullptr)

	//Create a separate transform so that the dance transform doesn't affect physics.
	SceneTransformComponent* danceTransform = SceneTransformComponent::CreateObject();
	if (AddComponent(danceTransform))
	{
		danceTransform->SetTranslation(Vector3::ZERO_VECTOR);

		DanceComp = DanceComponent::CreateObject();
		if (danceTransform->AddComponent(DanceComp))
		{
			DanceComp->SetDancing(false);
		}
	}

	if (nightclubEngine->GetLocalRole() == NetworkSignature::NR_Client)
	{
		CHECK(danceTransform != nullptr)
		Circle = ColorRenderComponent::CreateObject();
		if (danceTransform->AddComponent(Circle))
		{
			Circle->SolidColor = Color(255, 131, 61);
			Circle->SetShape(ColorRenderComponent::ST_Circle);
			Circle->SetBaseSize(Radius * 2.f, Radius * 2.f);
			Circle->SetPivot(0.5f, 0.5f);
			nightclubEngine->GetDrawLayer()->RegisterSingleComponent(Circle);
		}

		NameplateTransform = SceneTransformComponent::CreateObject();
		if (danceTransform->AddComponent(NameplateTransform))
		{
			NameplateTransform->SetTranslation(Vector3(0.f, -Radius - NameplateSize, 1000.f));

			Nameplate = TextRenderComponent::CreateObject();
			if (NameplateTransform->AddComponent(Nameplate))
			{
				nightclubEngine->GetDrawLayer()->RegisterSingleComponent(Nameplate);
			}
		}

		DrinkTransform = SceneTransformComponent::CreateObject();
		if (danceTransform->AddComponent(DrinkTransform))
		{
			DrinkTransform->SetTranslation(Vector3(Radius * 1.5f, 0.f, 1.f));
			DrinkTransform->SetVisibility(false);

			ColorRenderComponent* baseDrink = ColorRenderComponent::CreateObject();
			if (DrinkTransform->AddComponent(baseDrink))
			{
				baseDrink->SetPivot(Vector2(0.5f, 0.f));
				baseDrink->SetBaseSize(Vector2(48.f, 64.f));
				baseDrink->SolidColor = Color(37, 186, 21, 175);
				nightclubEngine->GetDrawLayer()->RegisterSingleComponent(baseDrink);
			}

			ColorRenderComponent* neck = ColorRenderComponent::CreateObject();
			if (DrinkTransform->AddComponent(neck))
			{
				neck->SetPivot(Vector2(0.5f, 1.f));
				neck->SetBaseSize(Vector2(24.f, 48.f));
				neck->SolidColor = Color(37, 186, 21, 175);
				nightclubEngine->GetDrawLayer()->RegisterSingleComponent(neck);
			}

			TickComponent* drinkTick = TickComponent::CreateObject(TICK_GROUP_NIGHTCLUB);
			if (DrinkTransform->AddComponent(drinkTick))
			{
				drinkTick->SetTickHandler(SDFUNCTION_1PARAM(this, Player, HandleDrinkTick, void, Float));
			}
		}
	}

	PhysComp = PhysicsComponent::CreateObject();
	if (AddComponent(PhysComp))
	{
		PhysComp->SetBlockingChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT);
		PhysComp->SetCollisionChannels(NightclubEngineComponent::COLLISION_CHANNEL_OBJECT | NightclubEngineComponent::COLLISION_CHANNEL_CHARACTER);
		PhysComp->AddShape(new CollisionCapsule(1.f, Vector3::ZERO_VECTOR, 1000.f, Radius));
	}

	MoveTick = TickComponent::CreateObject(TICK_GROUP_PRE_PHYSICS);
	if (AddComponent(MoveTick))
	{
		MoveTick->SetTickHandler(SDFUNCTION_1PARAM(this, Player, HandleMoveTick, void, Float));
		MoveTick->SetTicking(false);
	}

	RoleComp = NetworkRoleComponent::CreateObject();
	if (AddComponent(RoleComp))
	{
		RoleComp->OnGetComponentList = [&](std::vector<NetworkComponent*>& outNetComponents)
		{
			outNetComponents.push_back(NetComp);
			outNetComponents.push_back(DanceComp->GetNetComp());
		};

		RoleComp->OnNetInitialize = [&](NetworkSignature* connectedTo)
		{
			if (RoleComp->GetNetRole() == NetworkRoleComponent::NR_NetOwner)
			{
				//Clients need to replace their local Player with this instance.
				NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
				CHECK(localNightclub != nullptr)

				Player* oldPlayer = localNightclub->GetLocalPlayer();
				if (oldPlayer != nullptr)
				{
					oldPlayer->Destroy();
				}

				localNightclub->SetLocalPlayer(this);
				if (TopDownCamera* cam = localNightclub->GetCamera())
				{
					cam->SetRelativeTo(this);
				}

				if (localNightclub->GetHud() != nullptr)
				{
					localNightclub->GetHud()->UpdatePlayerStatus(this);
				}

				if (DanceComp.IsValid())
				{
					DanceComp->OnEnableChanged = SDFUNCTION_1PARAM(this, Player, HandleDancingEnabled, void, bool);
				}
			}
			else if (RoleComp->HasAuthority())
			{
				//Update the client of this Player's current location
				ClientSetPosition(connectedTo, ReadTranslation());
			}
		};

		RoleComp->OnIsNetRelevant = [&](NetworkSignature* remoteClient)
		{
			//Player objects should not replicate to the master server
			return (remoteClient->HasRole(NetworkSignature::NR_Client));
		};

		RoleComp->OnIsNetOwner = [&](NetworkSignature* remoteClient)
		{
			//This prevents other players from controlling other player objects they don't own.
			//It works this way in this application because NetworkSignatures only have permission to send RPCs and variable updates only if they're the NetOwners.
			//See: NetworkGlobals::EPermissions
			return (NetOwner == remoteClient);
		};
	}

	NetComp = NetworkComponent::CreateObject();
	if (AddComponent(NetComp))
	{
		NetComp->AddReplicatedVariable<Bool>(&bCanDrink, [&](ReplicatedVariable<Bool>& outInitRepVar)
		{
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = [&]()
			{
				SetCanDrink(bCanDrink);
			};
		});

		NetComp->AddReplicatedVariable<Bool>(&bHasDrink, [&](ReplicatedVariable<Bool>& outInitRepVar)
		{
			outInitRepVar.SetSyncInterval(1.f);
			outInitRepVar.OnReplicated = [&]()
			{
				SetHasDrink(bHasDrink);
			};
		});

		NetComp->AddReplicatedVariable<Bool>(&bIsDrinking, [&](ReplicatedVariable<Bool>& outInitRepVar)
		{
			outInitRepVar.OnShouldReplicate = [&](NetworkSignature* destination)
			{
				//Prevent clients from replicating bDrinking if they are not permitted to drink in the first place.
				return (IsDrinkingEnabled() || !bIsDrinking);
			};

			outInitRepVar.OnValidateData = [&](Bool bNewDrinking)
			{
				return (IsDrinkingEnabled() || !bNewDrinking);
			};

			outInitRepVar.OnReplicated = [&]()
			{
				SetIsDrinking(bIsDrinking);
			};
		});

		NetComp->AddReplicatedVariable<DString>(&PlayerName, [&](ReplicatedVariable<DString>& outInitRepVar)
		{
			outInitRepVar.WritePermissions = NetworkSignature::NR_Server;
			outInitRepVar.OnReplicated = [&]()
			{
				SetPlayerName(PlayerName);
			};
		});

		NetComp->AddReplicatedVariable<Vector3>(&Destination, [&](ReplicatedVariable<Vector3>& outInitRepVar)
		{
			outInitRepVar.SetSyncInterval(0.1f); //Update frequently since this variable determines movement. The greater the value, the more likelihood of running into desync issues.

			//When there's an update to destination, need to activate the MoveTick
			outInitRepVar.OnReplicated = [&]()
			{
				if (MoveTick != nullptr)
				{
					MoveTick->SetTicking(true);
				}
			};

			//Don't replicate if the Destination is empty. This is because other clients will clear Destination whenever their instances reached their destination, too.
			outInitRepVar.OnShouldReplicate = [&](NetworkSignature* destination)
			{
				return (!Destination.IsEmpty());
			};
		});

		BIND_RPC_1REF_PARAM(NetComp, ClientSetPosition, this, Player, Vector3);
		ClientSetPosition.CanBeExecutedFrom = NetworkSignature::NR_Server; //Only the server can send this message. This prevent malicious clients from calling this function from client to server even if they are net owners.
		ClientSetPosition.CanBeExecutedOn = NetworkSignature::NR_Client; //Only clients can receive this message (MasterServers will not receive this call, saves bandwidth). NOTE: This isn't needed for this example since the Player object doesn't exist on Master Server.
	}

	if (NightclubMap* map = nightclubEngine->GetMap())
	{
		map->InitializePlayer(this);
	}
}

void Player::Destroyed ()
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	CHECK(localNightclub != nullptr)

	TopDownCamera* cam = localNightclub->GetCamera();
	if (cam != nullptr && cam->GetRelativeTo() == this)
	{
		cam->SetRelativeTo(nullptr);
	}

	Super::Destroyed();
}

void Player::SetupInputComponent ()
{
	if (Input != nullptr)
	{
		RemoveInputComponent();
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->AddKeybind(sf::Keyboard::Num1, false, SDFUNCTION(this, Player, HandleToggleDance, bool));
		Input->AddKeybind(sf::Keyboard::Num2, false, SDFUNCTION(this, Player, HandleToggleDrink, bool));
		Input->OnMouseClick = SDFUNCTION_3PARAM(this, Player, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		Input->OnMouseMove = SDFUNCTION_3PARAM(this, Player, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
	}
}

void Player::RemoveInputComponent ()
{
	if (Input != nullptr)
	{
		Input->Destroy();
		Input = nullptr;
	}
}

void Player::Teleport (const Vector3& newLocation)
{
	SetTranslation(newLocation);

	if (RoleComp != nullptr && RoleComp->HasAuthority())
	{
		ClientSetPosition(BaseRpc::RT_Clients, newLocation);
	}
}

void Player::ReleaseMouseDrag ()
{
	bDraggingMouse = false;
}

void Player::ClientHackElevatePermissions ()
{
	if (NetComp != nullptr)
	{
		for (BaseReplicatedVariable* var : NetComp->ReadReplicatedVariables())
		{
			var->WritePermissions |= NetworkSignature::NR_Client;
		}

		for (const std::pair<Int, BaseRpc*>& rpc : NetComp->ReadRpcs())
		{
			CHECK(rpc.second != nullptr)
			rpc.second->CanBeExecutedFrom |= NetworkSignature::NR_Client;
			rpc.second->CanBeExecutedOn |= NetworkSignature::NR_Everything;
		}
	}
}

void Player::SetNetOwner (NetworkSignature* newNetOwner)
{
	if (RoleComp == nullptr || !RoleComp->HasAuthority())
	{
		NightclubLog.Log(LogCategory::LL_Warning, TXT("Player::SetNetOwner can only be assigned if it has a Network Role Component with authority."));
		return;
	}

	NetOwner = newNetOwner;
}

void Player::SetCanDrink (Bool bNewCanDrink)
{
	bCanDrink = bNewCanDrink;
	if (!IsDrinkingEnabled() && bIsDrinking)
	{
		SetIsDrinking(false);
	}

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetLocalPlayer() == this && localNightclub->GetHud() != nullptr)
	{
		localNightclub->GetHud()->UpdatePlayerStatus(this);
	}
}

void Player::SetHasDrink (Bool bNewHasDrink)
{
	bHasDrink = bNewHasDrink;

	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetLocalPlayer() == this && localNightclub->GetHud() != nullptr)
	{
		localNightclub->GetHud()->UpdatePlayerStatus(this);
	}
}

void Player::SetIsDrinking (Bool bNewIsDrinking)
{
	bIsDrinking = bNewIsDrinking;
	if (DrinkTransform != nullptr)
	{
		DrinkTransform->SetVisibility(bIsDrinking);
	}
}

void Player::SetPlayerName (const DString& newPlayerName)
{
	PlayerName = newPlayerName;
	if (Nameplate != nullptr)
	{
		Nameplate->DeleteAllText();

		FontPool* localFontPool = FontPool::FindFontPool();
		CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

		Int fontSize = NameplateSize.ToInt();
		Float textWidth = localFontPool->GetDefaultFont()->CalculateStringWidth(PlayerName, fontSize);

		TextRenderComponent::STextData& textData = Nameplate->CreateTextInstance(PlayerName, localFontPool->GetDefaultFont(), fontSize.ToUnsignedInt32());
		textData.DrawOffset.x = textWidth.Value * -0.5f;
	}
}

void Player::ChangeDestination (const Vector2& mousePos)
{
	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr && graphicsEngine->GetPrimaryWindow() != nullptr)

	Vector2 windowSize = graphicsEngine->GetPrimaryWindow()->GetWindowSize();

	NightclubEngineComponent* nightEngine = NightclubEngineComponent::Find();
	CHECK(nightEngine != nullptr && nightEngine->GetCamera() != nullptr)

	Rotator rot;
	nightEngine->GetCamera()->CalculatePixelProjection(graphicsEngine->GetPrimaryWindow()->GetWindowSize(), mousePos, OUT rot, OUT Destination);
	Destination.Z = ReadAbsTranslation().Z;
}

bool Player::ClientSetPosition_Implementation (const Vector3& newPosition)
{
	SetTranslation(newPosition);
	return true;
}

bool Player::HandleToggleDance ()
{
	if (DanceComp.IsValid() && DanceComp->IsEnabled())
	{
		Bool newDancing = !DanceComp->IsDancing();

		//Broadcast to server
		DanceComp->ServerSetDancing(BaseRpc::RT_Servers, newDancing);

		//Call it locally anyway for client prediction
		DanceComp->SetDancing(newDancing);
		return true;
	}

	return false;
}

bool Player::HandleToggleDrink ()
{
	if (DrinkTransform != nullptr && IsDrinkingEnabled())
	{
		SetIsDrinking(!DrinkTransform->IsVisible());
		return true;
	}

	return false;
}

void Player::HandleDancingEnabled (bool bDancingEnabled)
{
	NightclubEngineComponent* localNightclub = NightclubEngineComponent::Find();
	if (localNightclub != nullptr && localNightclub->GetLocalPlayer() == this && localNightclub->GetHud() != nullptr)
	{
		localNightclub->GetHud()->UpdatePlayerStatus(this);
	}
}

void Player::HandleDrinkTick (Float deltaSec)
{
	if (DrinkTransform != nullptr && DrinkTransform->IsVisible())
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		Float sine = std::sinf(localEngine->GetElapsedTime().Value);
		if (sine > 0.f)
		{
			DrinkTransform->EditRotation().SetYaw(125.f * sine, Rotator::RU_Degrees);
		}
		else
		{
			DrinkTransform->EditRotation().Yaw = 0;
		}

		if (PhysComp != nullptr)
		{
			bool bReverse = (PhysComp->ReadVelocity().X < 0.f);
			DrinkTransform->EditScale().X = (bReverse) ? -1.f : 1.f;
			DrinkTransform->EditTranslation().X = (bReverse) ? Radius * -1.25f : Radius * 1.25f;
		}
	}
}

bool Player::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& button, sf::Event::EventType type)
{
	if (button.button == sf::Mouse::Button::Left)
	{
		ChangeDestination(Vector2(button.x, button.y));
		CHECK(MoveTick != nullptr)
		MoveTick->SetTicking(true);
		bDraggingMouse = (type == sf::Event::MouseButtonPressed);

		return true;
	}

	return false;
}

void Player::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt, const Vector2& deltaMove)
{
	if (bDraggingMouse)
	{
		ChangeDestination(Vector2(evnt.x, evnt.y));
		CHECK(MoveTick != nullptr)
		MoveTick->SetTicking(true);
	}
}

void Player::HandleMoveTick (Float deltaSec)
{
	if (!Destination.IsEmpty())
	{
		Vector3 deltaVector = Destination - ReadAbsTranslation();
		deltaVector.Z = 0.f;
		if (deltaVector.VSize(false) < Radius * 0.5f)
		{
			if (bDraggingMouse)
			{
				//Player reached destination, but they're still holding the mouse.
				MousePointer* mouse = MousePointer::GetMousePointer();
				CHECK(mouse != nullptr)
				ChangeDestination(mouse->ReadPosition());
			}
			else
			{
				Destination = Vector3::ZERO_VECTOR;
				deltaVector = Destination; //stop movement
				if (MoveTick != nullptr)
				{
					MoveTick->SetTicking(false);
				}

				if (RoleComp->HasAuthority())
				{
					//Notify clients to snap to destination
					ClientSetPosition(BaseRpc::RT_Clients, ReadTranslation());
				}
			}
		}
		
		if (PhysComp != nullptr)
		{
			if (!deltaVector.IsEmpty())
			{
				deltaVector.NormalizeInline();
			}

			PhysComp->SetVelocity(deltaVector * RunSpeed);
		}
	}
}
NC_END