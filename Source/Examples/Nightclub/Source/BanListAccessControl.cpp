/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BanListAccessControl.cpp
=====================================================================
*/

#include "BanListAccessControl.h"
#include "NightclubClientIdentifier.h"

IMPLEMENT_CLASS(SD::NC::BanListAccessControl, SD::AccessControl)
NC_BEGIN

bool BanListAccessControl::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	NightclubClientIdentifier* clientId = dynamic_cast<NightclubClientIdentifier*>(signature->FindComponent(NightclubClientIdentifier::SStaticClass(), false));
	if (clientId == nullptr)
	{
		outRejectReason = RR_InvalidData;
		return false;
	}

	for (const DString& bannedPlayer : BannedPlayers)
	{
		if (bannedPlayer.Compare(clientId->ReadPlayerName(), DString::CC_CaseSensitive) == 0)
		{
			outRejectReason = RR_Banned;
			return false;
		}
	}

	return true;
}

void BanListAccessControl::AddBan (const DString& newBannedPlayer)
{
	ContainerUtils::AddUnique(OUT BannedPlayers, newBannedPlayer);
}

void BanListAccessControl::RemoveBan (const DString& unbannedPlayerName)
{
	ContainerUtils::RemoveItem(OUT BannedPlayers, unbannedPlayerName);
}
NC_END