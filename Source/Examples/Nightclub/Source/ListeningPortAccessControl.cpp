/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ListeningPortAccessControl.cpp
=====================================================================
*/

#include "ListeningPortAccessControl.h"
#include "NightclubEngineComponent.h"
#include "NightclubServerIdentifier.h"
#include "MasterServer.h"

IMPLEMENT_CLASS(SD::NC::ListeningPortAccessControl, SD::AccessControl)
NC_BEGIN

bool ListeningPortAccessControl::AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason)
{
	NightclubServerIdentifier* identifier = dynamic_cast<NightclubServerIdentifier*>(signature->FindComponent(NightclubServerIdentifier::SStaticClass()));
	if (identifier == nullptr)
	{
		outRejectReason = RR_InvalidData;
		return false;
	}

	NightclubEngineComponent* nightclub = NightclubEngineComponent::Find();
	if (nightclub == nullptr)
	{
		outRejectReason = RR_InternalError;
		return false;
	}

	MasterServer* coordinator = nightclub->GetServerCoordinator();
	if (coordinator == nullptr)
	{
		outRejectReason = RR_InternalError;
		return false;
	}

	if (!coordinator->PairSignature(signature, static_cast<unsigned short>(identifier->GetListeningPort().Value)))
	{
		outRejectReason = RR_InvalidData;
		return false;
	}

	return true;
}
NC_END