/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubAdminPanel.cpp
=====================================================================
*/

#include "NightclubAdminPanel.h"
#include "NightclubAdminObject.h"
#include "NightclubEngineComponent.h"
#include "Player.h"

IMPLEMENT_CLASS(SD::NC::NightclubAdminPanel, SD::GuiEntity)
NC_BEGIN

void NightclubAdminPanel::InitProps ()
{
	Super::InitProps();

	BanText = nullptr;
	BanScrollbar = nullptr;
	BanList = nullptr;
	UnbanButton = nullptr;
	PlayerListText = nullptr;
	PlayerScrollbar = nullptr;
	PlayerList = nullptr;
	BanButton = nullptr;
	CloseButton = nullptr;
}

void NightclubAdminPanel::ConstructUI ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	FrameComponent* frame = FrameComponent::CreateObject();
	if (AddComponent(frame))
	{
		frame->SetSize(Vector2(1.f, 1.f));
		frame->SetAlwaysConsumeMouseEvents(true);
		Float margin = 0.02f;
		Float spacing = 0.005f;
		Float posY = 0.f;
		Float lineHeight = 0.05f;

		BanText = LabelComponent::CreateObject();
		if (frame->AddComponent(BanText))
		{
			BanText->SetAutoRefresh(false);
			BanText->SetAnchorTop(margin);
			BanText->SetAnchorRight(margin);
			BanText->SetAnchorLeft(margin);
			BanText->SetSize(Vector2(1.f, lineHeight));
			BanText->SetWrapText(true);
			BanText->SetClampText(true);
			BanText->SetHorizontalAlignment(LabelComponent::HA_Center);
			BanText->SetText(translator->TranslateText(TXT("BanText"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubAdminPanel")));
			BanText->SetAutoRefresh(true);

			posY += margin + BanText->ReadSize().Y + spacing;
		}

		BanScrollbar = ScrollbarComponent::CreateObject();
		if (frame->AddComponent(BanScrollbar))
		{
			BanScrollbar->SetPosition(Vector2(0.f, posY));
			BanScrollbar->SetSize(Vector2(1.f, 0.3f));
			BanScrollbar->SetAnchorLeft(margin);
			BanScrollbar->SetAnchorRight(margin);
			BanScrollbar->SetHideControlsWhenFull(true);
			if (BanScrollbar->GetFrameTexture() != nullptr)
			{
				BanScrollbar->GetFrameTexture()->ResetColor.Source.a = 150;
			}

			GuiEntity* viewedObj = GuiEntity::CreateObject();
			BanScrollbar->SetViewedObject(viewedObj);
			viewedObj->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			viewedObj->SetAutoSizeVertical(true);

			BanList = ListBoxComponent::CreateObject();
			if (viewedObj->AddComponent(BanList))
			{
				BanList->SetPosition(Vector2(0.f, 0.f));
				BanList->SetSize(Vector2(1.f, 8.f));
				BanList->SetAutoDeselect(true);
				BanList->OnItemSelected = SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandleBanListSelected, void, Int);
				BanList->OnItemDeselected = SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandleBanListDeselected, void, Int);
			}

			posY = BanScrollbar->ReadPosition().Y + BanScrollbar->ReadSize().Y + spacing;
		}

		UnbanButton = ButtonComponent::CreateObject();
		if (frame->AddComponent(UnbanButton))
		{
			UnbanButton->SetPosition(Vector2(0.f, posY));
			UnbanButton->SetSize(Vector2(1.f, lineHeight));
			UnbanButton->SetAnchorLeft(margin * 2.f);
			UnbanButton->SetAnchorRight(margin * 2.f);
			UnbanButton->SetCaptionText(translator->TranslateText(TXT("UnbanButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubAdminPanel")));
			UnbanButton->SetEnabled(false);
			UnbanButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandleUnbanReleased, void, ButtonComponent*));

			posY = UnbanButton->ReadPosition().Y + UnbanButton->ReadSize().Y + (spacing * 3.f);
		}

		PlayerListText = LabelComponent::CreateObject();
		if (frame->AddComponent(PlayerListText))
		{
			PlayerListText->SetAutoRefresh(false);
			PlayerListText->SetPosition(Vector2(0.f, posY));
			PlayerListText->SetSize(Vector2(1.f, lineHeight));
			PlayerListText->SetAnchorLeft(margin);
			PlayerListText->SetAnchorRight(margin);
			PlayerListText->SetWrapText(true);
			PlayerListText->SetClampText(true);
			PlayerListText->SetHorizontalAlignment(LabelComponent::HA_Center);
			PlayerListText->SetText(translator->TranslateText(TXT("PlayerListText"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubAdminPanel")));
			PlayerListText->SetAutoRefresh(true);

			posY = PlayerListText->ReadPosition().Y + PlayerListText->ReadSize().Y + spacing;
		}

		PlayerScrollbar = ScrollbarComponent::CreateObject();
		if (frame->AddComponent(PlayerScrollbar))
		{
			PlayerScrollbar->SetPosition(Vector2(0.f, posY));
			PlayerScrollbar->SetSize(Vector2(1.f, 0.35f));
			PlayerScrollbar->SetAnchorLeft(margin);
			PlayerScrollbar->SetAnchorRight(margin);
			PlayerScrollbar->SetHideControlsWhenFull(true);
			if (PlayerScrollbar->GetFrameTexture() != nullptr)
			{
				PlayerScrollbar->GetFrameTexture()->ResetColor.Source.a = 150;
			}

			GuiEntity* viewedObj = GuiEntity::CreateObject();
			PlayerScrollbar->SetViewedObject(viewedObj);
			viewedObj->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
			viewedObj->SetAutoSizeVertical(true);

			PlayerList = ListBoxComponent::CreateObject();
			if (viewedObj->AddComponent(PlayerList))
			{
				PlayerList->SetPosition(Vector2(0.f, 0.f));
				PlayerList->SetSize(Vector2(1.f, 8.f));
				PlayerList->SetAutoDeselect(true);
				PlayerList->OnItemSelected = SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandlePlayerListSelected, void, Int);
				PlayerList->OnItemDeselected = SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandlePlayerListDeselected, void, Int);
			}

			posY = PlayerScrollbar->ReadPosition().Y + PlayerScrollbar->ReadSize().Y + spacing;
		}

		BanButton = ButtonComponent::CreateObject();
		if (frame->AddComponent(BanButton))
		{
			BanButton->SetPosition(Vector2(0.f, posY));
			BanButton->SetSize(Vector2(1.f, lineHeight));
			BanButton->SetAnchorLeft(margin);
			BanButton->SetAnchorRight(margin);
			BanButton->SetCaptionText(translator->TranslateText(TXT("BanButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubAdminPanel")));
			BanButton->SetEnabled(false);
			BanButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandleBanReleased, void, ButtonComponent*));

			posY = BanButton->ReadPosition().Y + BanButton->ReadSize().Y + spacing;
		}

		CloseButton = ButtonComponent::CreateObject();
		if (frame->AddComponent(CloseButton))
		{
			CloseButton->SetPosition(Vector2(0.f, posY));
			CloseButton->SetSize(Vector2(1.f, lineHeight));
			CloseButton->SetAnchorLeft(margin);
			CloseButton->SetAnchorRight(margin);
			CloseButton->SetCaptionText(translator->TranslateText(TXT("CloseButton"), NightclubEngineComponent::TRANSLATION_FILE, TXT("NightclubAdminPanel")));
			CloseButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, NightclubAdminPanel, HandleCloseReleased, void, ButtonComponent*));
		}
	}
}

void NightclubAdminPanel::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	Focus->TabOrder.push_back(BanList);
	Focus->TabOrder.push_back(UnbanButton);
	Focus->TabOrder.push_back(PlayerList);
	Focus->TabOrder.push_back(BanButton);
	Focus->TabOrder.push_back(CloseButton);
}

void NightclubAdminPanel::PopulatePlayerListing (const std::vector<DString>& playerNames)
{
	if (PlayerList != nullptr)
	{
		std::vector<GuiDataElement<DString>> playerList;
		for (const DString& playerName : playerNames)
		{
			playerList.emplace_back(GuiDataElement<DString>(playerName, playerName));
		}

		PlayerList->SetList(playerList);
	}
}

void NightclubAdminPanel::PopulateBanList (const std::vector<DString>& bannedPlayers)
{
	if (BanList != nullptr)
	{
		std::vector<GuiDataElement<DString>> banList;
		for (const DString& bannedPlayer : bannedPlayers)
		{
			banList.emplace_back(GuiDataElement<DString>(bannedPlayer, bannedPlayer));
		}

		BanList->SetList(banList);
	}
}

void NightclubAdminPanel::SetAdminObj (NightclubAdminObject* newAdminObj)
{
	AdminObj = newAdminObj;
}

void NightclubAdminPanel::HandleBanListSelected (Int idx)
{
	if (PlayerList != nullptr)
	{
		PlayerList->ClearSelection();
	}

	if (UnbanButton != nullptr)
	{
		UnbanButton->SetEnabled(true);
	}
}

void NightclubAdminPanel::HandleBanListDeselected (Int idx)
{
	if (BanList != nullptr && ContainerUtils::IsEmpty(BanList->ReadSelectedItems()) && UnbanButton != nullptr)
	{
		UnbanButton->SetEnabled(false);
	}
}

void NightclubAdminPanel::HandlePlayerListSelected (Int idx)
{
	if (BanList != nullptr)
	{
		BanList->ClearSelection();
	}

	if (BanButton != nullptr)
	{
		BanButton->SetEnabled(true);
	}
}

void NightclubAdminPanel::HandlePlayerListDeselected (Int idx)
{
	if (PlayerList != nullptr && ContainerUtils::IsEmpty(PlayerList->ReadSelectedItems()) && BanButton != nullptr)
	{
		BanButton->SetEnabled(false);
	}
}

void NightclubAdminPanel::HandleUnbanReleased (ButtonComponent* button)
{
	if (BanList != nullptr)
	{
		const std::vector<Int>& selectedItems = BanList->ReadSelectedItems();
		if (!ContainerUtils::IsEmpty(selectedItems))
		{
			DString playerName;
			if (BanList->GetItemByIdx(selectedItems.at(0), OUT playerName))
			{
				if (AdminObj.IsValid())
				{
					AdminObj->ServerUnbanPlayer(BaseRpc::RT_Servers, playerName);
				}

				BanList->RemoveItemByIdx(selectedItems.at(0));

				//Don't add the player to the PlayerList since it requires for the player to connect to the server to appear in that list.
			}
		}
	}
}

void NightclubAdminPanel::HandleBanReleased (ButtonComponent* button)
{
	if (PlayerList != nullptr)
	{
		const std::vector<Int>& selectedItems = PlayerList->ReadSelectedItems();
		if (!ContainerUtils::IsEmpty(selectedItems))
		{
			DString playerName;
			if (PlayerList->GetItemByIdx(selectedItems.at(0), OUT playerName))
			{
				if (AdminObj.IsValid())
				{
					AdminObj->ServerBanPlayer(BaseRpc::RT_Servers, playerName);
				}

				PlayerList->RemoveItemByIdx(selectedItems.at(0));

				//Immediately add the player to the ban list
				if (BanList != nullptr)
				{
					GuiDataElement<DString> newItem(playerName, playerName);
					BanList->AddOption(newItem);
				}
			}
		}
	}
}

void NightclubAdminPanel::HandleCloseReleased (ButtonComponent* button)
{
	Destroy();
}
NC_END