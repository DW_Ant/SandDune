/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Nightclub.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "NightclubEngineComponent.h"
#include "Nightclub.h"

SD::LogCategory SD::NC::NightclubLog(TXT("Nightclub"), SD::LogCategory::VERBOSITY_DEFAULT,
	SD::LogCategory::FLAG_STANDARD_OUTPUT |
	SD::LogCategory::FLAG_OS_OUTPUT |
	SD::LogCategory::FLAG_LOG_FILE |
	SD::LogCategory::FLAG_OUTPUT_WINDOW |
	SD::LogCategory::FLAG_CONSOLE_MSG);

//Forward function declarations
int BeginSandDune (TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	TCHAR* args = GetCommandLine();
	return BeginSandDune(&args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (TCHAR* args[])
{
	SD::ProjectName = TXT("Nightclub Demo");

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(args[0]);

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
	std::vector<SD::EngineComponent*> engineComponents;
	SD::NC::NightclubEngineComponent* localNightclub = new SD::NC::NightclubEngineComponent();

	if (engine->HasCmdLineSwitch(TXT("-MasterServer"), SD::DString::CC_IgnoreCase))
	{
		engine->SetMinDeltaTime(0.1f); //10 fps
		localNightclub->SetLocalRole(SD::NetworkSignature::NR_MasterServer);
		engineComponents.push_back(new SD::LoggerEngineComponent());
		engineComponents.push_back(new SD::NetworkEngineComponent());
		engineComponents.push_back(localNightclub);
	}
	else if (engine->HasCmdLineSwitch(TXT("-Server"), SD::DString::CC_IgnoreCase))
	{
		engine->SetMinDeltaTime(0.022222f); //~45 fps
		localNightclub->SetLocalRole(SD::NetworkSignature::NR_Server);

		SD::GraphicsEngineComponent* graphicsEngine = new SD::GraphicsEngineComponent(); //Needed for transform calculations
		graphicsEngine->SetInitializeDefaultViewport(false); //Disable the viewport for servers
		engineComponents.push_back(graphicsEngine);

		engineComponents.push_back(new SD::LoggerEngineComponent());
		engineComponents.push_back(new SD::NetworkEngineComponent());
		engineComponents.push_back(new SD::PhysicsEngineComponent());
		engineComponents.push_back(localNightclub);
	}
	else //Client
	{
		engine->SetMinDeltaTime(0.0166667f); //~60 fps

		localNightclub->SetLocalRole(SD::NetworkSignature::NR_Client);
		engineComponents.push_back(new SD::GraphicsEngineComponent());
		engineComponents.push_back(new SD::GuiEngineComponent());
		engineComponents.push_back(new SD::InputEngineComponent());
		engineComponents.push_back(new SD::LocalizationEngineComponent());
		engineComponents.push_back(new SD::LoggerEngineComponent());
		engineComponents.push_back(new SD::NetworkEngineComponent());
		engineComponents.push_back(new SD::PhysicsEngineComponent());
		engineComponents.push_back(localNightclub);
	}

	engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

	return RunMainLoop();
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}