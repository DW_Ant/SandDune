/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubHud.h
  Simple overlay that displays the controls and possessions.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class Player;

class NightclubHud : public GuiEntity
{
	DECLARE_CLASS(NightclubHud)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	LabelComponent* DanceLabel;
	LabelComponent* DrinkLabel;
	LabelComponent* HackerLabel;
	LabelComponent* AdminLabel;
	LabelComponent* PingLabel;
	LabelComponent* BotWarningLabel;
	TickComponent* BotWarningTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the text content based on the status of the given player.
	 */
	virtual void UpdatePlayerStatus (Player* player);

	/**
	 * Reveals the hacker panel controls on the hud.
	 */
	virtual void RevealHackerControls ();

	/**
	 * Reveals the admin controls on the hud.
	 */
	virtual void RevealAdminControls ();

	/**
	 * Adds components that responsible for displaying the bot mode text.
	 */
	virtual void FlashBotWarning ();

	/**
	 * Determines if the bot mode is currently active or not.
	 */
	virtual void SetBotModeActive (bool bActive);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePingTick (Float deltaSec);
	virtual void HandleBotWarningTick (Float deltaSec);
};
NC_END