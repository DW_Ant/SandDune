/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MapGate.h
  An Entity that prompts the player if they want to transition to the next map.
  If the player accepts it, this Entity will communicate with the server to handle
  the transition.

  The reason for a dedicated Entity for this rather than relying on the Map's network
  components is because all clients are network owners over this object. With this
  application's permissions, clients are only allowed to invoke server RPCs if they
  are the network owners. They are not network owners over maps.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class MapGate : public Entity, public SceneTransform
{
	DECLARE_CLASS(MapGate)


	/*
	=====================
	  Properties
	=====================
	*/
public:
	/* Keyword to translate into human readable text for the display text (in TextComp). Replicated from server to client. */
	DString MapTextKey;

	/* The draw offsets used for displacing the map text relative to the center. */
	Vector2 DrawOffset;

	/* Keyword to translate into human readable text for the prompt menu. Replicated from server to client. */
	DString PromptTextKey;

	Rpc<Int> ClientConnectToServer;

protected:
	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;

	/* Comp responsible for rendering the text in map. */
	TextRenderComponent* TextComp;

	/* The Map object this gate is heading towards. This class must be a NightclubMap class. */
	const DClass* Destination;

	Rpc<> ServerTravel;
	Rpc<DString /*playerName*/, Bool /*hasBeverage*/, DString /*mapName*/> MasterServerTravel;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Informs the client to disconnect from this server, and to connect to a different server with the given open port.
	 */
	virtual bool ClientConnectToServer_Implementation (Int serverPort);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDestination (const DClass* newDestination);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates the necessary components to place it on the map.
	 */
	virtual void InitializeSceneComponents ();

	/**
	 * Informs the server that a client accepted its prompt, and it should transition the client
	 * to the next server.
	 */
	virtual bool ServerTravel_Implementation ();

	/**
	 * Tells the master server to move the given client to this a the given destination.
	 * The mapName is the Dune class name without the namespaces.
	 */
	virtual bool MasterServerTravel_Implementation (DString playerName, Bool bHasBeverage, DString mapName);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBeginOverlap (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleEndOverlap (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleAcceptPrompt ();
	virtual void HandleTextReplicated ();
};
NC_END