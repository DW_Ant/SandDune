/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EnterClubPrompt.h
  A UI Entity that allows the player to connect to a master server.
  Here they can configure the following:
  - Their display name
  - The server to connect to

  This UI Entity will display the connection responses.
  "Bob the Bouncer" will explain why the player is not allowed to connect.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class EnterClubPrompt : public GuiEntity
{
	DECLARE_CLASS(EnterClubPrompt)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	LabelComponent* NamePrompt;
	TextFieldComponent* NameField;
	LabelComponent* ServerPrompt;
	LabelComponent* ServerDestination;
	LabelComponent* ResponseText;
	ButtonComponent* ConnectButton;
	ButtonComponent* CloseButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCloseReleased (ButtonComponent* button);
	virtual void HandleConnectReleased (ButtonComponent* button);
	virtual void HandleConnectionAccepted (NetworkSignature* remoteSignature);
	virtual void HandleConnectionRejected (const sf::IpAddress& remoteAddr, Int reason);
};
NC_END