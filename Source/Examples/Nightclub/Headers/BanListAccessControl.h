/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BanListAccessControl.h

  A simple AccessControl that denies all client connections that are on
  the ban list.

  This is only banning players by name. In a real environment, this isn't secure.
  In a real situation, you would ban players by their account identifiers.
  
  For example/simplicity purposes, this demo will only ban clients by their names.

  Also this access control does not save data. The ban list is cleared every
  time the master server instance starts up.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class BanListAccessControl : public AccessControl
{
	DECLARE_CLASS(BanListAccessControl)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of players that are banned. Name comparisons are case sensitive. */
	std::vector<DString> BannedPlayers;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Uniquely adds the given name to the ban list.
	 */
	virtual void AddBan (const DString& newBannedPlayer);

	/**
	 * Searches and removes the given name from the ban list.
	 */
	virtual void RemoveBan (const DString& unbannedPlayerName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<DString>& ReadBannedPlayers () const
	{
		return BannedPlayers;
	}
};
NC_END