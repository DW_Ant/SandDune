/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DanceFloorMap.h
  A map that contains the dance floor.
  Here the players can dance, but they are not permitted to drink.
  From here, they can enter the booth map, bar map, and disconnect by leaving the nightclub.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class DanceFloorMap : public NightclubMap
{
	DECLARE_CLASS(DanceFloorMap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then this object already instantiated its exit points. This is to prevent the server from instantiating exit points for each client
	that connects to the server. */
	bool bCreatedExitPoints;

	/* Component responsible for replicating the NPC dancer locations (since those are randomized).
	The server and client need to sync up due to collision purposes. */
	NetworkComponent* DancerNetworkComp;

	/* List of Color Components that represent the flashing lights. */
	std::vector<ColorRenderComponent*> FlashingLights;

	/* The transform components that make up the dancer objects. */
	std::vector<SceneTransformComponent*> DancerTransforms;

	/* RPC to inform the clients of the server's dancer locations. */
	Rpc<const std::vector<Vector3>& /*dancerLocations*/> ClientSetDancerLocations;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void InitializePlayer (Player* newPlayer) override;

protected:
	virtual void HandleNetInitialize (NetworkSignature* connectedTo) override;
	virtual void HandleGetComponentList (std::vector<NetworkComponent*>& outComponentList) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates the necessary components that represent the stage and its pillars
	 */
	virtual void CreateStage ();

	/**
	 * Creates the necessary components that represnet the dance floor.
	 */
	virtual void CreateDanceFloor ();

	/**
	 * Creates the necessary components that represent the exit points
	 */
	virtual void CreateExitPoints ();

	virtual bool ClientSetDancerLocations_Implementation (const std::vector<Vector3>& dancerLocations);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLightTick (Float deltaSec);
	virtual void HandleEnterStage (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleExitStage (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleLeaveClubPrompt (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleLeaveClub ();
	virtual void HandleLeavePrompt (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
};
NC_END