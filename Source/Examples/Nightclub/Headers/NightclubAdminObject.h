/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubAdminObject.h
  An Entity that is propagated from servers to clients. This object allows
  for communication between server, client.

  Both the master server and servers will instantiate their own AdminObject instances
  to handle communication between server to master server to master server to another server to
  server to client.

  This also handles client controls to toggle the panel as well as managing that panel.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubAdminPanel;

class NightclubAdminObject : public Entity
{
	DECLARE_CLASS(NightclubAdminObject)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Rpc<> ServerRequestPlayerList;
	Rpc<> MasterServerRequestPlayerList;
	Rpc<> ServerGetConnectedClients;
	Rpc<const std::vector<DString>&> MasterServerPopulatePlayerList;
	Rpc<const std::vector<DString>&> ServerPopulatePlayerList;
	Rpc<const std::vector<DString>&> ClientPopulatePlayerList;
	Rpc<const DString&> ServerBanPlayer;
	Rpc<const DString&> MasterServerBanPlayer;
	Rpc<const DString&> ServerDisconnectPlayer;
	Rpc<const DString&> ServerUnbanPlayer;
	Rpc<> ServerRequestBanList;
	Rpc<> MasterServerRequestBanList;
	Rpc<const std::vector<DString>&> ServerReceiveBanList;
	Rpc<const std::vector<DString>&> ClientReceiveBanList;

protected:
	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;
	DPointer<NightclubAdminPanel> Panel;

	/* Maximum number of seconds before the requests time out. If enough time elapses, then this object may send another request. */
	Float RequestTimeoutTime;

	/* Servers only, this is a list of NetworkSignatures that sent a request message to retrieve the PlayerList. */
	std::vector<NetworkSignature*> PlayerListRequesters;

	/* Master servers only, this the list of Admin Objects that requested for Player Lists.
	NOTE: AdminObjects are referenced instead of NetworkSignatures primarily because the master server must send a response to the correct object instances. */
	std::vector<NightclubAdminObject*> PlayerListObjRequesters;

	/* Engine timestamp (in seconds) when the first player list request was sent out. Used to detect timeout. */
	Float PlayerListRequestTime;

	/* Servers only, this is a list of NetworkSignatures that sent a request message to retrieve the banned player list. */
	std::vector<NetworkSignature*> BannedListRequesters;

	/* Engine timestamp (in seconds) when the first ban list request was sent out. Used to detect timeout. */
	Float BanListRequestTime;

	/* Master servers only, this is a list of Network Signatures this process is waiting responses from for its GetConnectedClient list. */
	std::vector<NetworkSignature*> PendingConnectedClientResponses;

	/* Master servers only, this is the list of all players found (so far) during a ConnectedClient listing. */
	std::vector<DString> PlayersInNetwork;

	/* Engine timestamp (in seconds) when the master server first sent out the ConnectedClient request. */
	Float ConnectedClientRequestTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds an Input Component that allows the client to open the admin panel.
	 */
	virtual void SetupInput ();

	/**
	 * Sends a server request to retrieve all player names connected to the network.
	 */
	virtual bool ServerRequestPlayerList_Implementation ();

	/**
	 * Notifies the master server to contact all servers to obtain their player lists.
	 */
	virtual bool MasterServerRequestPlayerList_Implementation ();

	/**
	 * Simply notifies the master server of all players connected to this server.
	 */
	virtual bool ServerGetConnectedClients_Implementation ();

	/**
	 * Returns the master server's response about all connected clients connected to the server instance.
	 */
	virtual bool MasterServerPopulatePlayerList_Implementation (const std::vector<DString>& playerNames);

	/**
	 * Notifies the server of the list of all players connected to this network (except for the one receiving this message).
	 * The server should then notify the admin about this player listing.
	 */
	virtual bool ServerPopulatePlayerList_Implementation (const std::vector<DString>& playerNames);

	/**
	 * Populates the admin panel with the names of players connected to this network.
	 */
	virtual bool ClientPopulatePlayerList_Implementation (const std::vector<DString>& playerNames);

	/**
	 * Forcibly removes the player from the network, and adds their name to the ban list.
	 */
	virtual bool ServerBanPlayer_Implementation (const DString& bannedPlayer);

	/**
	 * Notifies the master server to add the player to the ban list as well as disconnect any
	 * players matching that name that are connected to this network.
	 */
	virtual bool MasterServerBanPlayer_Implementation (const DString& bannedPlayer);

	/**
	 * Searches through all connected clients. If anyone matches that name will be disconnected.
	 */
	virtual bool ServerDisconnectPlayer_Implementation (const DString& playerName);

	/**
	 * Removes the player name from the ban list.
	 * Only the master server manages the ban list. If a server is invoked, it'll simply redirect to the master server.
	 */
	virtual bool ServerUnbanPlayer_Implementation (const DString& playerName);

	/**
	 * Sends a request to retrieve the ban list from the master server.
	 */
	virtual bool ServerRequestBanList_Implementation ();

	/**
	 * Notifies the instigator the list of banned players.
	 */
	virtual bool MasterServerRequestBanList_Implementation ();

	/**
	 * Handles the master server's response event for the player list.
	 */
	virtual bool ServerReceiveBanList_Implementation (const std::vector<DString>& bannedPlayers);

	/**
	 * A response to the ban list. This notifies the client about the ban list to populate their local admin panel.
	 */
	virtual bool ClientReceiveBanList_Implementation (const std::vector<DString>& bannedPlayers);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleActivatePanel ();
};
NC_END