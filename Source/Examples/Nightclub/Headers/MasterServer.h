/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MasterServer.h
  An object that is responsible for coordinating individual server instances.
  The clients will connect to the master server, and the server will determine which
  of these instances the client will end up connecting to.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class MasterConnection;

class MasterServer : public Object
{
	DECLARE_CLASS(MasterServer)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SServerInstance
	{
		ProcessInfo* ServerProcess;

		/* The connection associated with this server instance. If it's null, then it's assumed that the master server launched the process, but that
		process hasn't connected to this master server. */
		NetworkSignature* Signature;

		/* The map class this server instance is running. Parent class must be a NightclubMap. */
		const DClass* Map;

		/* The port clients can use to connect to this server particular instance. */
		unsigned short Port;

		SServerInstance (ProcessInfo* inServerProcess, const DClass* inMap, unsigned short inPort);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The file this MasterServer will use to launch server instances. */
	FileAttributes ExecFile;

	/* List of individual server instances this object is responsible for launching. The first element in this vector is the main instance containing the Dance Floor. */
	std::vector<SServerInstance> ServerInstances;

	/* Entity instantiated in the master server, but it contains NetworkComponents that propagates to remote instances for communication purposes. */
	DPointer<MasterConnection> Communicator;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Launch a server instance that runs the specified map.
	 * Returns true on success.
	 */
	virtual bool LaunchServer (const DClass* map, unsigned short serverPort);
	virtual bool LaunchServer (const DClass* map);

	/**
	 * A process is attempting to connect to the master server as one of its instances. 
	 * This function will pair the given NetworkSignature with a server instance.
	 * Returns true if the signature successfully paired with a pending instance.
	 */
	virtual bool PairSignature (NetworkSignature* signature, unsigned short serverPort);

	/**
	 * Returns the Network Signature that is running the specified map.
	 */
	virtual NetworkSignature* FindSignature (const DClass* map) const;

	/**
	 * Returns the map instance the given signature is running.
	 * If it returns nullptr, then it's not a recognized server.
	 */
	virtual const DClass* FindMap (NetworkSignature* serverInstance) const;

	/**
	 * Returns the server's listening port from the given Network Signature.
	 */
	virtual bool FindServerPort (NetworkSignature* serverSignature, unsigned short& outPort);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline MasterConnection* GetCommunicator () const
	{
		return Communicator.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleDisconnect (NetworkSignature* signature);
};
NC_END