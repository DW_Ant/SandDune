/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Player.h
  An object that a client can control. This Entity has a network component that allows
  the client to move around, dance, obtain a drink, and consume drink.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class DanceComponent;

class Player : public Entity, public SceneTransform
{
	DECLARE_CLASS(Player)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<DanceComponent> DanceComp;
	InputComponent* Input;
	ColorRenderComponent* Circle;
	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;
	TickComponent* MoveTick;
	PhysicsComponent* PhysComp;
	SceneTransformComponent* NameplateTransform;
	TextRenderComponent* Nameplate;
	SceneTransformComponent* DrinkTransform;

	/* Only applies for Authoritative instances. This pointer points to the network signature that's a network owner over this instance. */
	DPointer<NetworkSignature> NetOwner;

	Rpc<const Vector3&> ClientSetPosition;

	/* If true, then this player is permitted to drink if they have a beverage. */
	Bool bCanDrink;

	/* Becomes true when this player possesses a drink. */
	Bool bHasDrink;

	/* If true, then this player's drinking animation is currently active. Although clients can check if the transform is visible, a dedicated Bool is used
	for two reasons: 1. The server doesn't have a DrinkTransform. 2. For replication, allowing late joiners see the drinking animation. */
	Bool bIsDrinking;

	/* Determines the radius how large the player model is (in cms). */
	Float Radius;

	/* Font size for nameplates. */
	Float NameplateSize;

	/* The name of this player object that is replicated and displayed to others. */
	DString PlayerName;

	/* The location the player is moving towards. If zero'd then the player is not moving anywhere. */
	Vector3 Destination;

	/* Determines how fast the player can run (in cm per second). */
	Float RunSpeed;

	/* Becomes true if the user pressed down on the map. Allows the player to change directions by moving the mouse with the button held down. */
	bool bDraggingMouse;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes the Input component that binds certain keys to player actions.
	 * This should be called on Player objects that are locally controlled or when the client
	 * is the NetOwner of this object.
	 */
	virtual void SetupInputComponent ();

	/**
	 * Destroys the Input component associated with this player object.
	 */
	virtual void RemoveInputComponent ();

	/**
	 * Snaps the player position to the specified value, and if this is an authoritative Entity,
	 * it'll broadcast the new location to all clients.
	 */
	virtual void Teleport (const Vector3& newLocation);

	/**
	 * Forcibly releases the drag mouse state. Although the player will continue moving to their destination,
	 * they cannot change directions until they release and press the mouse button again.
	 */
	virtual void ReleaseMouseDrag ();

	/**
	 * This method only exists to simulate a hacked client that's bypassing checks.
	 * This function accesses all replicated variables and RPC's, and elevate its permissions to allow the client to make invalid calls.
	 */
	virtual void ClientHackElevatePermissions ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetNetOwner (NetworkSignature* newNetOwner);
	virtual void SetCanDrink (Bool bNewCanDrink);
	virtual void SetHasDrink (Bool bNewHasDrink);
	virtual void SetIsDrinking (Bool bNewIsDrinking);
	virtual void SetPlayerName (const DString& newPlayerName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DanceComponent* GetDanceComp () const
	{
		return DanceComp.Get();
	}

	inline NetworkSignature* GetNetOwner () const
	{
		return NetOwner.Get();
	}

	inline Bool IsDrinkingEnabled () const
	{
		return (bCanDrink && bHasDrink);
	}

	inline Bool HasDrink () const
	{
		return bHasDrink;
	}

	inline Bool IsDrinking () const
	{
		return bIsDrinking;
	}

	inline const DString& ReadPlayerName () const
	{
		return PlayerName;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the Player's destination based on the given mouse coordinates.
	 */
	virtual void ChangeDestination (const Vector2& mousePos);

	/**
	 * Forcibly snaps this player to the given location.
	 * This is called from the server whenever it needs to snap player locations for 2 reasons:
	 * 1. The server repositions the Player location on startup.
	 * 2. There's a possibility for desync issues. Desync could happen from collision mismatches between server and client. Calling this once in awhile corrects that.
	 */
	virtual bool ClientSetPosition_Implementation (const Vector3& newPosition);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleToggleDance ();
	virtual bool HandleToggleDrink ();
	virtual void HandleDancingEnabled (bool bDancingEnabled);
	virtual void HandleDrinkTick (Float deltaSec);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& button, sf::Event::EventType type);
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt, const Vector2& deltaMove);
	virtual void HandleMoveTick (Float deltaSec);
};
NC_END