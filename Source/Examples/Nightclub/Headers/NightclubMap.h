/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubMap.h
  An Entity responsible for instantiating several components for rendering
  and collision.
  This is the parent class for all Nightclub maps.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class Player;

class NightclubMap : public Entity
{
	DECLARE_CLASS(NightclubMap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Location where the players spawn on the map. */
	Vector3 PlayerSpawn;
	Vector2 MapSize;

	/* Although this component does not have any RPCs or replicated variables, this component is used to replicate the object itself to clients. */
	NetworkRoleComponent* RoleComp;

	/* List of Entities to destroy when this map is destroyed. */
	std::vector<Object*> MapObjects;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Allows the map to set environmental variables upon this Player object.
	 * For example, the dance floor map would prevent players from drinking.
	 */
	virtual void InitializePlayer (Player* newPlayer);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector3 GetPlayerSpawn () const
	{
		return PlayerSpawn;
	}

	inline const Vector3& ReadPlayerSpawn () const
	{
		return PlayerSpawn;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick shortcut that registers the given render component to the scene draw layer.
	 */
	virtual void RegisterRenderComp (RenderComponent* comp);

	/**
	 * Adds a physics component that blocks off the perimeter.
	 */
	virtual void CreatePerimeter ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * The reason why this class is using a dedicated event handlers for the role component's callbacks primarily to allow
	 * subclasses to extend their own behaviors (using virtual overrides).
	 *
	 * NOTE: There is a constraint where each object is allowed up to one NetworkRoleComponent.
	 *
	 * There are other ways going about this such as having NetworkComponents reside on the parent class, or replacing the RoleComponent in subclass.
	 * Or registering RPC's/RepVars to NetworkComponents defined in parent class. This virtual callback is just another way to extend
	 * network functionality from subclasses.
	 */
	virtual void HandleNetInitialize (NetworkSignature* connectedTo);
	virtual void HandleGetComponentList (std::vector<NetworkComponent*>& outComponentList);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Quick utility to find a MapObject by its class.
	 * NOTE: MapObjects are not contained within a DPointer. It is not safe to destroy these objects or else you'll run into dangling pointer crashes.
	 */
	template<class T>
	T* FindMapObjectByClass () const
	{
		for (size_t i = 0; i < MapObjects.size(); ++i)
		{
			if (T* result = dynamic_cast<T*>(MapObjects.at(i)))
			{
				return result;
			}
		}

		return nullptr;
	}
};
NC_END