/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PlayerNameAccessControl.h
  A simple Access Control that checks a Clientidentifier. This object will
  only allow connection requests that contains a player name that is on
  the PendingPlayerName list.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class PlayerNameAccessControl : public AccessControl
{
	DECLARE_CLASS(PlayerNameAccessControl)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then the client name must appear in the Nightclub's PendingPlayerList vector. Otherwise that check is skipped. */
	bool bMustAppearOnList;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;
};
NC_END