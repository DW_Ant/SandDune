/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubEngineComponent.h
  An engine component responsible for managing global objects for the Nightclub example.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class EnterClubPrompt;
class ClientHackerTestObject;
class MapPrompt;
class MasterServer;
class NightclubAdminObject;
class NightclubBot;
class NightclubMap;
class NightclubHud;
class Player;

class NightclubEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(NightclubEngineComponent)


	/*
	=====================
	  Struct
	=====================
	*/

public:
	struct SPendingPlayerData
	{
		DString PlayerName;
		Bool bHasBeverage;
		SPendingPlayerData (const DString& inPlayerName, Bool inHasBeverage);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Collision channel used for all physical entities in this example. */
	static const Int COLLISION_CHANNEL_OBJECT;

	/* Collision channel used exclusively for characters. */
	static const Int COLLISION_CHANNEL_CHARACTER;

	/* Name of the localization file that contains the translations for this application. */
	static const DString TRANSLATION_FILE;

	/* Server instances only. This is a list of player names that are about to connect to this server. */
	std::vector<SPendingPlayerData> PendingPlayerData;

	/* Only applicable for clients. If true, then this component will kick the client back to the Outdoor map when they disconnect from a server.
	A client could be in a middle of transitioning from one server to another. There's no need to launch the outdoor map between map transitions. */
	bool bReturnToOutdoorsOnDisconnect;

protected:
	/* The role for this process. Determines the procedure how this application should run.
	Client is the end user with the display. The client controls a single character Entity.
	Server contains the authoritative code for a single room instance.
	MasterServer manages server instances since it can spin up and shut down servers. This represents the nightclub as a whole.
	Service in this case simply runs an individual Gui panel that represents the admin panel. */
	NetworkSignature::ENetworkRole LocalRole;

	/* Draw layer responsible for rendering the scene (only applicable for clients). */
	DPointer<SceneDrawLayer> SceneLayer;

	DPointer<TopDownCamera> Camera;

	/* The map instance that is currently running. */
	DPointer<NightclubMap> Map;

	/* For local client only. This is the player instance they are controlling. */
	DPointer<Player> LocalPlayer;

	/* For server only. This is the list of players that are controlled by somebody. */
	std::vector<Player*> Players;

	/* Server and Master Server only, this is the Admin Object that gives admin controls to ban/unban players.
	This variable is only set if it's authoritative. */
	DPointer<NightclubAdminObject> AdminObject;

	/* Object used to test against the client hacker panel. This resides on servers and clients. */
	DPointer<ClientHackerTestObject> HackerTestObject;

	/* Only applicable for clients, this object is responsible for simulating input without human intervention. */
	DPointer<NightclubBot> Bot;

	/* The GUI Entity that allows the client to connect to a server. */
	DPointer<EnterClubPrompt> EnterClubUi;

	/* The GUI Entity that allows the client to switch servers. */
	DPointer<MapPrompt> Prompt;

	/* The GUI Entity responsible for displaying the status and controls. */
	DPointer<NightclubHud> Hud;

	/* The object responsible for maintaining server instances. */
	DPointer<MasterServer> ServerCoordinator;

private:
	/* Becomes true when this component is initialized (from PostInitializeComponent). */
	bool bInitialized;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	NightclubEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;
	virtual void GetPostInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Displays the Gui that allows the client to connect to a server.
	 * If there is one up already, then this function will do nothing.
	 */
	virtual void DisplayEnterClubPrompt ();
	virtual void CloseEnterClubPrompt ();

	virtual void DisplayMapPrompt (const DString& promptText, SDFunction<void> onAccept);
	virtual void CloseMapPrompt ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetLocalRole (NetworkSignature::ENetworkRole newLocalRole);
	virtual void SetMap (NightclubMap* newMap);
	virtual void SetLocalPlayer (Player* newLocalPlayer);
	virtual void AddPlayer (Player* newRemotePlayer);
	virtual void SetAdminObject (NightclubAdminObject* newAdminObject);
	virtual void SetHackerTestObject (ClientHackerTestObject* newHackerTestObject);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline NetworkSignature::ENetworkRole GetLocalRole () const
	{
		return LocalRole;
	}

	inline SceneDrawLayer* GetDrawLayer () const
	{
		return SceneLayer.Get();
	}

	inline TopDownCamera* GetCamera () const
	{
		return Camera.Get();
	}

	inline NightclubMap* GetMap () const
	{
		return Map.Get();
	}

	inline Player* GetLocalPlayer () const
	{
		return LocalPlayer.Get();
	}

	inline const std::vector<Player*>& ReadPlayers () const
	{
		return Players;
	}

	inline NightclubAdminObject* GetAdminObject () const
	{
		return AdminObject.Get();
	}

	inline ClientHackerTestObject* GetHackerTestObject () const
	{
		return HackerTestObject.Get();
	}

	inline MapPrompt* GetPrompt () const
	{
		return Prompt.Get();
	}

	inline NightclubHud* GetHud () const
	{
		return Hud.Get();
	}

	inline MasterServer* GetServerCoordinator () const
	{
		return ServerCoordinator.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the console title window to reflect the status of this instance.
	 */
	virtual void UpdateConsoleTitle ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

	virtual void HandleConnectionAccepted (NetworkSignature* signature);
	virtual void HandleDisconnect (NetworkSignature* signature);
};
NC_END