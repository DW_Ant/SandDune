/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DuplicateNameAccessControl.h

  This simple Access Control refuses connections if a player is attempting to connect
  to this server while their name is already taken.

  Typically only the Master Server would have this access control since the Master Server
  will maintain a list of all players that are not only connected to itself, but also
  connected to any servers it's coordinating.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class DuplicateNameAccessControl : public AccessControl
{
	DECLARE_CLASS(DuplicateNameAccessControl)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SRecentAttempt
	{
		/* Name of the player that attempted to connect to this process. */
		DString PlayerName;

		/* Engine timestamp when the player attempted to connect to this server (in seconds). */
		Float ConnectionTime;

		SRecentAttempt (const DString& inPlayerName, Float inConnectionTime);
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const unsigned char REJECT_NUM;

protected:
	/* List of player names that are connected to one of the sub servers (players not directly connected to this process). */
	std::vector<DString> NamesInUse;

	/* List of players that recently attempted to connect to the server. The purpose behind this vector is to maintain a list to handle cases where
	two or more players sent a connection request with the same name, but no one could establish a connection in time.
	This ensures to reject multiple connection attempts with the same name in a short time period. */
	std::vector<SRecentAttempt> RecentAttempts;

	/* Number of seconds that must elapse before removing a RecentAttempt entry from the list. */
	Float RecentAttemptsPurgeTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Uniquely adds the given name to the NamesInUse vector.
	 */
	virtual void AddNameInUse (const DString& newName);

	/**
	 * Searches and removes the given name from the NamesInUse vector.
	 */
	virtual void RemoveNameInUse (const DString& nameToRemove);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<DString>& ReadNamesInUse () const
	{
		return NamesInUse;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each recent connection attempt, and remove those that expired from the list.
	 */
	virtual void PurgeStaleConnectionAttempts ();
};
NC_END