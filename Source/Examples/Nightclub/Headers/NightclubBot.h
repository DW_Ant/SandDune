/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubBot.h
  An Entity that simulates a client without interfacing with a human.
  
  The purpose behind this bot is to test against server load when there are multiple
  clients moving around at the same time.

  The bot can do the following:
  1. Connect to a server.
  2. Move in random directions.
  3. Periodically attempt to drink or dance.
  4. Change servers.

  Pressing Escape in the client window pauses/resumes the bot.

  To run a client window in bot mode, enter "-Bot" in the command line arguments.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubBot : public Entity
{
	DECLARE_CLASS(NightclubBot)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of base names a bot can choose from when attempting to connect to a server. */
	static const std::vector<DString> BOT_NAMES;

protected:
	InputComponent* Input;
	TickComponent* ConnectTick;
	TickComponent* MovementTick;
	TickComponent* ActionTick;
	TickComponent* PromptTick;

	/* Determines the range when the bot can move to a new spot (in seconds). */
	Range<Float> MovementInterval;

	/* Determines the range when the bot can toggle dancing or drinking (in seconds). */
	Range<Float> ActionInterval;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the network status of this bot. If this process is not connected to a server, it'll attempt to connect to the master server.
	 */
	virtual void CheckConnections ();

	/**
	 * Picks a random spot near the player and instructs the player to move to that destination.
	 * Afterwards it'll reset the movement interval.
	 */
	virtual void PickMovementSpot ();

	/**
	 * Toggles either dancing or drinking regardless if the player is allowed to dance or drink.
	 * If they are forbidden to do so, then nothing will happen.
	 * Afterwards this function will reset the action interval.
	 */
	virtual void DoNightclubAction ();

	/**
	 * If there is a prompt from the map gate or from Plikter, then this function will confirm its prompt.
	 */
	virtual void AcceptPrompts ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleToggleBot ();
	virtual void HandleConnectTick (Float deltaSec);
	virtual void HandleMovementTick (Float deltaSec);
	virtual void HandleActionTick (Float deltaSec);
	virtual void HandlePromptTick (Float deltaSec);
};
NC_END