/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubTheme.h
  The object responsible for determining how the GuiComponents appear for the
  Nightclub program.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubTheme : public GuiTheme
{
	DECLARE_CLASS(NightclubTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString NIGHTCLUB_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeNightclubStyle ();
};
NC_END