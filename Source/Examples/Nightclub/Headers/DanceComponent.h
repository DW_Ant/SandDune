/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  DanceComponent.h
  A simple component that adjusts the owner's transformation in sync with the game clock.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class DanceComponent : public EntityComponent
{
	DECLARE_CLASS(DanceComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Rpc<Bool> ServerSetDancing;
	Rpc<Bool> ClientSetDancing;

	/* Invoked whenever this DanceComponent's enabledness has changed. */
	SDFunction<void, bool /*bIsEnabled*/> OnEnableChanged;

protected:
	/* If true, then this component can potentially dance. */
	Bool bEnabled;

	/* Determines how many seconds it takes to complete a single bounce cycle. */
	Float JumpInterval;

	/* Determines how high the jumps are (in cms). */
	Float JumpHeight;

	TickComponent* Tick;

	/* The SceneTransform object this component is attached to. */
	SceneTransform* OwnerTransform;

	/* Net component used to replicate dancing and availability state.
	NOTE: The DanceComponent doesn't have a RoleComponent since it's intended to be used as a subcomponent of a greater object.
	For example a Player object can have a DanceComponent, and that Player object will be responsible for the RoleComponent.
	Remember there can only be one RoleComponent at the root level of the component tree. */
	DPointer<NetworkComponent> NetComp;

private:
	/* The Engine instance this component will sync its animation to. */
	Engine* LocalEngine;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * This method only exists to simulate a hacked client that's bypassing checks.
	 * This function accesses all replicated variables, and sets its writing permissions to both server and client.
	 */
	virtual void ClientHackGrantRepVarWritePermissions ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetJumpInterval (Float newJumpInterval);
	virtual void SetJumpHeight (Float newJumpHeight);
	virtual void SetEnabled (bool bNewEnabled);

	/**
	 * If passing in true, then this component will start animating.
	 * Otherwise this component will stop animating/dancing.
	 */
	virtual void SetDancing (bool bNewDancing);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsEnabled () const
	{
		return bEnabled;
	}

	inline bool IsDancing () const
	{
		return (Tick != nullptr && Tick->IsTicking());
	}

	inline NetworkComponent* GetNetComp () const
	{
		return NetComp.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks to see if this component is permitted to dance. If so, then this will notify all clients.
	 */
	virtual bool ServerSetDancing_Implementation (Bool bNewDancing);

	/**
	 * Activates the tick component to run the animation.
	 */
	virtual bool ClientSetDancing_Implementation (Bool bNewDancing);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
NC_END