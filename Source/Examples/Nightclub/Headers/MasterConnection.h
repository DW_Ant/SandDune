/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MasterConnection.h
  An Entity that resides on the client, server, and master server. This enables the processes
  to send RPC messages between each other.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class MasterConnection : public Entity
{
	DECLARE_CLASS(MasterConnection)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPendingTravelData
	{
		/* Network signature that is connected to the client (source). */
		NetworkSignature* ServerSource;

		/* The name of the player that wants to move. */
		DString PlayerName;

		/* If true, then this player has a beverage in possession. */
		Bool bHasBeverage;

		/* The map instance the player wants to move towards. */
		const DClass* Map;

		SPendingTravelData (NetworkSignature* inServerSource, const DString& inPlayerName, Bool inHasBeverage, const DClass* inMap);
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;

	Rpc<DString, Bool> ServerNotifyPlayerArrival;
	Rpc<const DString&> MasterServerSendPlayer;
	Rpc<Int> ClientConnectTo;
	Rpc<DString, Int> ServerSendClient;
	Rpc<const DString&> MasterServerRemovePlayer;


	/* MasterServer only: List of players that need to connect to the main room (for clients connecting to the master server for the first time). */
	std::vector<DString> PendingEnteringClients;

	/* MasterServer only: List of server instances that are waiting to send their client to their destinations. */
	std::vector<SPendingTravelData> PendingTravelRequests;

	/* MasterServer and Servers only: List of player names that are currently transferring to another server. Their names are removed from this list upon disconnect.
	This is primarily used to prevent servers from falsely removing player names from the MasterServer's NameInUse vector. */
	std::vector<DString> PlayersInTransit;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Moves the given player to a server instance that's running the given map.
	 * If there isn't a server instance, the master server will launch a new process.
	 * The master server will then inform the server instance of the player request.
	 * Finally the master server may send a response that the player may now connect to that instance.
	 * This method should only be called on the master server.
	 */
	virtual void MovePlayer (NetworkSignature* moveFrom, const DString& playerName, Bool bHasBeverage, const DClass* map);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Invoked from the master server, this function notifies
	 * the server instance that the given player is coming.
	 * If the player attempted to connect to a server directly, the server would refuse the player until
	 * the master server informs the server they're coming first.
	 */
	virtual bool ServerNotifyPlayerArrival_Implementation (DString playerName, Bool bHasBeverage);

	/**
	 * The server notified the master server that it added the player to the list.
	 * The master server can go ahead and send the player to that server.
	 */
	virtual bool MasterServerSendPlayer_Implementation (const DString& playerName);

	/**
	 * Tells the client to connect to a server with the given ip and port.
	 */
	virtual bool ClientConnectTo_Implementation (Int serverPort);

	/**
	 * The master server is informing this server to send the client.
	 * It is assumed that the server instance is accepting the player at this point.
	 * Note: The master server may locally execute this function to handle cases when clients are connected to the master server.
	 */
	virtual bool ServerSendClient_Implementation (DString playerName, Int serverPort);

	/**
	 * Tells the master server that the a client with the given name has disconnected from their network.
	 * This should not be called if the client is simply transferring servers.
	 */
	virtual bool MasterServerRemovePlayer_Implementation (const DString& playerName);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleDelayedDisconnect (Float deltaSec, TickComponent* tick);
	virtual void HandleConnectionAccepted (NetworkSignature* remoteSignature);
	virtual void HandleDisconnect (NetworkSignature* remoteSignature);
};
NC_END