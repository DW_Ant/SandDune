/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubServerIdentifier.h
  When connecting to the Master Server as a Server instance, the Master Server requires
  the Server instance to include this identifier information so it may pair up a Network Signature
  with its process.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubServerIdentifier : public NetworkIdentifier
{
	DECLARE_CLASS(NightclubServerIdentifier)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The unique port number this server instance is listening for connections.
	NOTE: If planning on running server instances on separate machines, then using ports would not be guaranteed to be unique.
	If that's the case, recommend using a different identifier. */
	Int ListeningPort;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool InitializeIdToCurSystem (NetworkSignature::ENetworkRole networkRoles) override;
	virtual bool ReadDataFromBuffer (const DataBuffer& data) override;
	virtual void WriteDataToBuffer (DataBuffer& outData) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetListeningPort () const
	{
		return ListeningPort;
	}
};
NC_END