/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BoothMap.h
  A map that contains the booth.

  In this map, they are permitted to drink but they cannot dance. There were too many
  incidents where people were tripping over the tables when dancing at the booths.
  From here, they can enter the dance floor map.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class BoothMap : public NightclubMap
{
	DECLARE_CLASS(BoothMap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	bool bCreatedExitPoint;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void InitializePlayer (Player* newPlayer) override;

protected:
	virtual void HandleNetInitialize (NetworkSignature* connectedTo) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates the necessary components that represent the floor, back wall, and pillars.
	 */
	virtual void CreateRoom ();

	/**
	 * Creates the components that represent the booths.
	 */
	virtual void CreateBooths ();

	/**
	 * Creates the components that represent the dart boards.
	 */
	virtual void CreateDartBoards ();

	/**
	 * Creates the components that allows players to enter the dance floor.
	 */
	virtual void CreateExitPoint ();
};
NC_END