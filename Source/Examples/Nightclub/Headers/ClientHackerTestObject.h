/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClientHackerTestObject.h
  This object exists on the server that is replicated to all clients.

  The purpose behind this object is to expose a few functions that the client
  may call. These functions don't affect the game, but rather this is
  a test place for the client hacker panel.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class ClientHackerPanel;
class Player;

class ClientHackerTestObject : public Entity
{
	DECLARE_CLASS(ClientHackerTestObject)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Rpc<> ServerIncrementFloodCounter;
	Rpc<Int> ClientReceiveFloodCounter;
	Rpc<RpcPtr<Player>> ServerRunReferenceCheck;

protected:
	InputComponent* Input;
	NetworkRoleComponent* RoleComp;
	NetworkComponent* NetComp;

	DPointer<ClientHackerPanel> HackerPanel;

	/* Increments every time the flood function is invoked. */
	Int FloodCounter;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * A test function that will be called on the server repeatedly for the flood test.
	 * Each time this function is called, the server should eventually respond with a counter.
	 */
	virtual bool ServerIncrementFloodCounter_Implementation ();

	/**
	 * A response function from calling the flood counter.
	 */
	virtual bool ClientReceiveFloodCounter_Implementation (Int counter);

	/**
	 * Runs a check on the server to see if the calling client is permitted to reference the given player.
	 * The server is suppose to log an error if the client is some how able to reference a player they are not permitted to reference in a RPC.
	 * This application's permissions are configured to only reference Entities the client are network owners over.
	 * In this case, the client should only be permitted to reference the player object they are controlling.
	 */
	virtual bool ServerRunReferenceCheck_Implementation (RpcPtr<Player> player);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleToggleHackerPanel ();
};
NC_END