/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ClientHackerPanel.h
  This UI simulates a client with a hacked program that allows them to break
  the rules.

  The purpose behind this panel is to simulate and test against the network
  system when invalid packets are sent. In the ideal situation, the client
  is able to alter and change as much as they want only on their program.
  However the server should be resilient enough to reject invalid attempts.
  The cheater should not be able to affect the game nor other clients.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class ClientHackerPanel : public GuiEntity
{
	DECLARE_CLASS(ClientHackerPanel)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	//Components related to permission controls
	LabelComponent* PermissionsLabel;
	ColorRenderComponent* PermissionVarRepOutline;
	ButtonComponent* PermissionVarRepButton;
	ColorRenderComponent* PermissionRpcOutline;
	ButtonComponent* PermissionRpcButton;
	ColorRenderComponent* PermissionRefAnythingOutline;
	ButtonComponent* PermissionRefAnythingButton;
	ColorRenderComponent* PermissionInstantiationOutline;
	ButtonComponent* PermissionInstantiationButton;

	//Components related to forcing actions
	LabelComponent* ActionLabel;
	ColorRenderComponent* DanceOutline;
	ButtonComponent* DanceButton;
	ColorRenderComponent* GainBeverageOutline;
	ButtonComponent* GainBeverageButton;
	ColorRenderComponent* DrinkOutline;
	ButtonComponent* DrinkButton;

	//Instantiation test controls
	LabelComponent* ObjectLabel;
	ColorRenderComponent* SpawnPlayerOutline;
	ButtonComponent* SpawnPlayerButton;
	ColorRenderComponent* SpawnAdminOutline;
	ButtonComponent* SpawnAdminButton;

	//Navigation related
	LabelComponent* TeleportLabel;
	ColorRenderComponent* TeleportOutline;
	ButtonComponent* TeleportButton;
	LabelComponent* ServerLabel;
	ColorRenderComponent* ServerOutline;
	TextFieldComponent* ServerField;

	//Input test
	LabelComponent* InputLabel;
	VerticalList* InputList;
	std::vector<CheckboxComponent*> InputCheckboxes;

	//Flooding test
	ColorRenderComponent* FloodOutline;
	ButtonComponent* FloodButton;

	//Misc
	ButtonComponent* CloseButton;

	/* Determines how quickly the outline components will fade out. */
	Float AlphaFadePerSecond;

	/* Becomes true if this panel is currently listening for a mouse click. If the user click outside the GuiEntity boundaries, it'll attempt to teleport the
	locally controlled player to that spot. */
	bool bIsTeleportingPlayer;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual bool HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through all Player objects. For each Player instance found, it'll create a checkbox in the InputList component.
	 */
	virtual void PopulatePlayerList ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleOutlineTick (Float deltaSec, TickComponent* tick);
	virtual bool HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvnt);

	virtual void HandlePermissionVarRepReleased (ButtonComponent* button);
	virtual void HandlePermissionRpcReleased (ButtonComponent* button);
	virtual void HandlePermissionRefAnythingReleased (ButtonComponent* button);
	virtual void HandlePermissionInstantiationReleased (ButtonComponent* button);

	virtual void HandleDanceReleased (ButtonComponent* button);
	virtual void HandleAllDanceReleased (ButtonComponent* button);
	virtual void HandleGainBeverageReleased (ButtonComponent* button);
	virtual void HandleAllGainBeverageReleased (ButtonComponent* button);
	virtual void HandleDrinkReleased (ButtonComponent* button);
	virtual void HandleAllDrinkReleased (ButtonComponent* button);

	virtual void HandleSpawnPlayerReleased (ButtonComponent* button);
	virtual void HandleSpawnAdminReleased (ButtonComponent* button);

	virtual void HandleTeleportReleased (ButtonComponent* button);
	virtual void HandleServerReturn (TextFieldComponent* uiComponent);

	virtual void HandleInputChecked (CheckboxComponent* uiComponent);

	virtual void HandleFloodReleased (ButtonComponent* button);
	virtual void HandleRefFloodReleased (ButtonComponent* button);

	virtual void HandleCloseReleased (ButtonComponent* button);
};
NC_END