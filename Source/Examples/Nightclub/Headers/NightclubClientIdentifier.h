/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubClientIdentifier.h
  When connecting to a server as a client, the server requires the client to provide
  their name.

  NOTE: This isn't secure as other clients can spoof names and this application
  doesn't guarantee unique names. The correct way of implementing something like
  this is to use an authentication system that provides clients a temporary
  token.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubClientIdentifier : public NetworkIdentifier
{
	DECLARE_CLASS(NightclubClientIdentifier)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DString PlayerName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool InitializeIdToCurSystem (NetworkSignature::ENetworkRole networkRoles) override;
	virtual bool ReadDataFromBuffer (const DataBuffer& data) override;
	virtual void WriteDataToBuffer (DataBuffer& outData) override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPlayerName (const DString& newPlayerName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline DString GetPlayerName () const
	{
		return PlayerName;
	}

	inline const DString& ReadPlayerName () const
	{
		return PlayerName;
	}
};
NC_END