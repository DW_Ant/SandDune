/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Nightclub.h
  Contains important file includes and definitions for the Nightclub demo.

  This example provides a demonstration how one could setup a simple MMO network architecture.
  This example contains a master server that can kick off server instances as well as
  transfer players between servers.

  This example also tests against various aspects in the Network module such as:
  * Access Control where player names can be registered to a ban list.
  * Servers communicating with each other through the master server.
  * Controls that allows clients to send malformed packets to simulate hackers. This allows
  for testing against the Network Monitor and failsafe checks in the ConnectionManager.


  What the client experiences:
  1. When the client first launches the application, they are a character outside of the nightclub.
  In this state, they are not connected to anything.
  Here, they can configure their display name. Their display name is treated as their ID.
  The servers will use this to identify the player. Typically this ID is given
  via authentication system (logging in), but for demo purposes, it's simulated.

  2. While in offline mode (outside of the nightclub), they can attempt to connect to the server.
  The bouncer will act like the AccessControl. He will determine if the player can connect/enter
  the nightclub or not. The server will have a list of banned IDs. The IDs are locally controlled
  since this is only a demo.

  3. When the server and client accepts the connection, the master server will spin up a server instance
  for the dance floor. While in the dance floor, the player can do the following network actions:
  Move - replicated destination variable. The player can teleport locally, but the server will snap them
  back to position.
  Dancing and drinking are replicated functions. Drinking is only permitted if they have a drink.
  Players can also move to two other rooms (server instances) in the nightclub: the bar and booth.

  4. When a player enters a different room, the server will transfer the player to different servers.

  At the bar, the player can obtain a drink.
  At the booth, the player cannot dance.
  At the dance floor, the player cannot drink.
  The player also cannot drink if they did not obtain a beverage from the bar.

  To access the admin controls, simply connect to the master server under the name "Admin" (case insensitive).
  After connecting, users with the "Admin" name can access the admin controls via tab button.
  This allows admins to ban or unban other clients even if they're connected to a different server.

  To further test against the network security, client are armed with a tool that allows them to
  send malformed packets. Here, they can spoof permissions, alter game states (locally only), send
  invalid packet types, send RPCs when they shouldn't normally, send invalid data buffers, and flooding
  the network.

  The server should be able to handle malformed packets, prevent the player from executing invalid
  RPC's (such as dancing at the booth, or drinking without a drink), and suspend the player for
  flooding the network.

  Disclaimer: Only local host is supported. The master server only launches servers on the same machine.
  This application doesn't test connectivity across multiple computers. Everything is handled local
  host primarily to make things simpler to work with, and because it's not the aim of this program.

  Another thing to consider is that this application is not secure. Player names are used for
  identifiers. There is no authentication system. Nightclub 'bans' can be bypassed simply by changing
  your name.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#include "Engine\Network\Headers\NetworkClasses.h"
#include "Engine\Physics\Headers\PhysicsClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"

#define TICK_GROUP_NIGHTCLUB "Nightclub"
#define TICK_GROUP_PRIORITY_NIGHTCLUB 3333

#define NC_BEGIN namespace SD { namespace NC {
#define NC_END } }

NC_BEGIN
extern LogCategory NightclubLog;
NC_END