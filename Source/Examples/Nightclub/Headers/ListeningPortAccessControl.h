/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ListeningPortAccessControl.h
  If the Network Engine Component contains this access control, then this process will
  not accept connections unless the connection request states that they have a port
  opened that matches what this process is expecting.

  In this example, the master server will have this access control. It will only check
  against signatures that are attempting to connect to the master server as one of its
  server instances. This application is configured to give server instances all permissions.
  If attempting to connect to a master server as a server, the connection request must
  inform the master server that it has a port opened, and if that port matches one of
  its expected server instances that hasn't been initialized, it'll accept the connection,
  and pair the network signature to one of its server instances.

  WARNING: There is a vulnerability with this approach as anyone could brute force known ports,
  and if they time their requests just right, they could potentially be connected to the master
  server as a server. If this happens, they will have elevated permissions compared to ordinary clients.
  Even worse, the master server may direct clients to connect to the imposter.

  A more secure way is to implement an authentication system where the master
  server can verify. For simplicity purposes, the Nightclub application does not implement
  an authentication system.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class ListeningPortAccessControl : public AccessControl
{
	DECLARE_CLASS(ListeningPortAccessControl)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool AllowConnection (NetworkSignature* signature, unsigned char& outRejectReason) override;
};
NC_END