/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MapPrompt.h
  A simple UI that confirms if the player wants to confirm an action or not.
  This is typically instigated when interacting with something in the map.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class MapPrompt : public GuiEntity
{
	DECLARE_CLASS(MapPrompt)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to execute once the player accepted this prompt. */
	SDFunction<void> OnAccept;

protected:
	LabelComponent* PromptLabel;
	ButtonComponent* AcceptButton;
	ButtonComponent* CancelButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the prompt text equal to the given string.
	 */
	virtual void SetPromptText (const DString& newPromptText);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleAcceptReleased (ButtonComponent* button);
	virtual void HandleCloseReleased (ButtonComponent* button);
};
NC_END