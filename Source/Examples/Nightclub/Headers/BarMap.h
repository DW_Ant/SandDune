/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BarMap.h
  A map that contains the bar.
  Here the players can obtain a drink component.
  In this map, they are permitted to drink and dance.
  From here, they can enter the dance floor map.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class BarMap : public NightclubMap
{
	DECLARE_CLASS(BarMap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	bool bCreatedExitPoint;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void HandleNetInitialize (NetworkSignature* connectedTo) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates the necessary components that represent the bar
	 */
	virtual void CreateBar ();

	/**
	 * Creates the components that represent the pool table and arcade machine.
	 */
	virtual void CreateGameObjects ();

	/**
	 * Creates the components that allows players to enter the dance floor.
	 */
	virtual void CreateExitPoint ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleApproachPlikter (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleLeavePlikter (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleBuyDrink ();
};
NC_END