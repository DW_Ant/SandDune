/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  OutdoorMap.h
  An object that maintains the components for rendering the exterior of the nightclub.
=====================================================================
*/

#pragma once

#include "NightclubMap.h"

NC_BEGIN
class OutdoorMap : public NightclubMap
{
	DECLARE_CLASS(OutdoorMap)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void InitializePlayer (Player* newPlayer) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates the necessary components that represent the landscape (grass, trees, and bushes).
	 */
	virtual void CreateLandscape ();

	/**
	 * Creates the necessary components that represent the building, itself.
	 * And the pathway to the entrance.
	 */
	virtual void CreateNightclub ();

	/**
	 * Creates the necessary components that represent the parkinglot.
	 */
	virtual void CreateParkinglot ();
	
	/**
	 * Creates the necessary components that represent the road including the street lights.
	 */
	virtual void CreateRoad ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleInteractBob (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
	virtual void HandleLeaveBob (PhysicsComponent* delegateOwner, PhysicsComponent* otherComp);
};
NC_END