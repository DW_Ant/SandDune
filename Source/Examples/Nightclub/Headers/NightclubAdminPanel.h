/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  NightclubAdminPanel.h
  A simple GUI panel that can kick players from the network.
  This is not intended to be used in a final product since players are added to
  the list by their names rather than their IDs. These bans can easily be bypassed
  simply by changing their names.

  To access this panel in the demo, simply enter the Nightclub under the name as
  "Admin" then press tab to access this menu.

  Bans are not stored in a file. All bans are lifted after rebooting the server.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class NightclubAdminObject;

class NightclubAdminPanel : public GuiEntity
{
	DECLARE_CLASS(NightclubAdminPanel)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	LabelComponent* BanText;
	ScrollbarComponent* BanScrollbar;
	ListBoxComponent* BanList;
	ButtonComponent* UnbanButton;
	LabelComponent* PlayerListText;
	ScrollbarComponent* PlayerScrollbar;
	ListBoxComponent* PlayerList;
	ButtonComponent* BanButton;
	ButtonComponent* CloseButton;

	/* The object responsible for allowing this panel. The panel will use this object to leverage server communication. */
	DPointer<NightclubAdminObject> AdminObj;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the player listing with the contents of the given vector of player names.
	 */
	virtual void PopulatePlayerListing (const std::vector<DString>& playerNames);

	/**
	 * Updates the ban list with the contents of the given vector of player names.
	 */
	virtual void PopulateBanList (const std::vector<DString>& bannedPlayers);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAdminObj (NightclubAdminObject* newAdminObj);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBanListSelected (Int idx);
	virtual void HandleBanListDeselected (Int idx);
	virtual void HandlePlayerListSelected (Int idx);
	virtual void HandlePlayerListDeselected (Int idx);

	virtual void HandleUnbanReleased (ButtonComponent* button);
	virtual void HandleBanReleased (ButtonComponent* button);
	virtual void HandleCloseReleased (ButtonComponent* button);
};
NC_END