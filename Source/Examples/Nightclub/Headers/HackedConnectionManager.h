/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  HackedConnectionManager.h
  A modified version of the ConnectionManager that exposes some of its functions that
  allows the ClientHackerPanel to locally modify its environment where it's not
  normally allowed.

  Doing this simulates a hacked client that can bypass local checks (such as connection
  permissions). One of the purposes of this application to check if the server also
  checks and protects itself from hacked clients.
=====================================================================
*/

#pragma once

#include "Nightclub.h"

NC_BEGIN
class HackedConnectionManager : public ConnectionManager
{
	DECLARE_CLASS(HackedConnectionManager)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Elevates the local permissions for each connection.
	 * This function only modifies the local permission, and does not affect the server.
	 * What's suppose to happen is that there would be a disagreement between a client and server regarding permissions.
	 * However the server would assume their permissions are correct, and would reject packets that are not permitted by this client.
	 */
	virtual void ElevatePermissions (NetworkGlobals::EPermissions permissionsToGrant);
};
NC_END