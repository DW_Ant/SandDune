/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetExporter.h
  A simple tool that iterates through all asset exporters.

  This tool should be the first program to run after compiling Sand Dune for the first time
  since this will populate the Content directory.

  The asset list is dynamically populated based on what projects are enabled.

  This tool runs under the assumption that there aren't any assets in the Content directory.
  Because of that, it cannot run many engine components, and it must implement its own render window.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"

#define AE_BEGIN namespace SD { namespace AE {
#define AE_END }}

#define TICK_GROUP_ASSET_EXPORTER "AssetExporter"
#define TICK_GROUP_PRIORITY_ASSET_EXPORTER 490

AE_BEGIN
extern LogCategory AssetExporterLog;
AE_END