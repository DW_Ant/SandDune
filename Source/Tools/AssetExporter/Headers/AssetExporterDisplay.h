/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetExporterDisplay.h
  An object that is responsible for a SFML window handle and rendering text on it.

  Since this program is not guaranteed to have assets in the content directory, it cannot
  leverage the GraphicsEngineComponent, DrawLayers, and RenderComponents.
=====================================================================
*/

#pragma once

#include "AssetExporter.h"
#include "SFML/Graphics.hpp"

AE_BEGIN
class ExportQueue;

class AssetExporterDisplay : public Entity
{
	DECLARE_CLASS(AssetExporterDisplay)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines how frequently the window will refresh its display (in seconds). */
	static const Float WINDOW_REFRESH_RATE;

	/* Determines how frequently this application will poll for window events (in seconds). */
	static const Float WINDOW_POLL_RATE;

protected:
	sf::RenderWindow WindowHandle;

	sf::Vector2f WindowSize;

	sf::Font Font;

	/* Font size used for all characters in the display. */
	unsigned int FontSize;

	/* This program assumes the font is fixed width. This is the width of the 'W' character of the Font. */
	float FontWidth;

	/* Total number of characters that can fit within one line (assuming each character is fixed width). */
	float NumCharsPerLine;

	/* Only applicable when scrolling through exporters are enabled. This is the export number that is at the bottom of the display. */
	Int ScrollPosition;

	/* Redraws everything on the window every tick. */
	TickComponent* RefreshDisplayTick;

	/* Polls events from the Window on this tick's update. */
	TickComponent* PollingTick;

	DPointer<ExportQueue> Exporter;

private:
	/* Characters to cycle through for the spinning wheel animation. */
	std::vector<DString> SpinningWheelChars;
	size_t SpinningWheelIdx;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Renders everything on the window based on the status of the exporters.
	 */
	virtual void RenderEverything ();

	/**
	 * Checks various windows polling events to listen for input events, and notifications (such as closing the window).
	 */
	virtual void PollWindowEvents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNextExporter (size_t exporterIdx);
	virtual void HandleRefreshDisplayTick (Float deltaSec);
	virtual void HandlePollingTick (Float deltaSec);
};
AE_END