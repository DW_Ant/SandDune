/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetExporterEngineComponent.h
  A simple engine component that manages a few objects that monitors and displays
  the progression of exporting assets.
=====================================================================
*/

#pragma once

#include "AssetExporter.h"

AE_BEGIN
class AssetExporterDisplay;
class ExportQueue;

class AssetExporterEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(AssetExporterEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The object responsible for displaying a window handle and drawing text over it. */
	AssetExporterDisplay* Display;

	/* The object responsible for processing through the exporters. */
	DPointer<ExportQueue> Exporter;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	AssetExporterEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ExportQueue* GetExporter () const
	{
		return Exporter.Get();
	}
};
AE_END