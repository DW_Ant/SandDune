/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ExportQueue.h
  The Object that's responsible for kicking off exporting processes and monitor
  their status.

  It'll finish its task when it finishes through the list.
=====================================================================
*/

#pragma once

#include "AssetExporter.h"

AE_BEGIN
class ExportQueue : public Entity
{
	DECLARE_CLASS(ExportQueue)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EExportState
	{
		ES_Pending, //Waiting to launch
		ES_Exporting, //Currently processing
		ES_Passed,
		ES_Failed
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SExportStatus
	{
		DString ExportName;
		EExportState ExportState;

		/* Only applicable if ExportState is ES_Failed. This is the number of assets that failed to export. */
		Int NumFailed;

		SExportStatus (const DString& inExportName) :
			ExportName(inExportName),
			ExportState(ES_Pending),
			NumFailed(0)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever the queue progresses to the next exporter. */
	SDFunction<void, size_t /* newExportIdx */> OnNextExporter;

protected:
	/* List of exporters this queue is going through. */
	std::vector<SExportStatus> ExportList;

	/* Index of the current export this queue is going through. Becomes negative if not launched. Becomes greater than the list, itself, when completed. */
	Int CurrentExportIdx;

	/* Becomes true if the user passed in the "-Clean" command line argument. This will propagate to the exporters. */
	bool bClean;

	/* Becomes true if the user passed in the "-CleanOnly" command line argument. This will propagate to the expoters. */
	bool bCleanOnly;

#ifdef DEBUG_MODE
	/* The number of simulated exporters the user specified from the command line arguments. A simulated exporter pretends to run a process, but all it really does is wait x many seconds before automatically failing/passing.
	Even numbered expoters passes, odd numbered fails. */
	Int NumSimulatedExporters;

	/* Number of seconds to elapse before it automatically completes a simulated exporter. */
	Float SimExporterDuration;

	/* Time remaining before the current simulated exporter completes (in seconds). */
	Float SimExporterTimeRemaining;
#endif

	/* Command line arguments that are passed to each exporter. */
	DString ExportCmdLineArgs;

	TickComponent* ProcCheckTick;

	/* ProcessInfo of the current exporter. */
	ProcessInfo ExportInfo;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Notifies the queue to start the export process.
	 */
	virtual void LaunchExporters ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<SExportStatus>& ReadExportList () const
	{
		return ExportList;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Kicks off the next exporter in the queue.
	 */
	virtual void LaunchNextExporter ();

	/**
	 * Called whenever the queue finished running each exporter regardless if they passed or failed.
	 */
	virtual void FinishAssetExporter ();

	/**
	 * Checks the status of the current process. If it's finished, it'll move on to the next
	 * exporter in the queue.
	 */
	virtual void CheckProcessStatus ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (Float deltaSec);
};
AE_END