/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetExporterDisplay.cpp
=====================================================================
*/

#include "AssetExporterDisplay.h"
#include "AssetExporterEngineComponent.h"
#include "ExportQueue.h"

IMPLEMENT_CLASS(SD::AE::AssetExporterDisplay, SD::Entity)
AE_BEGIN

const Float AssetExporterDisplay::WINDOW_REFRESH_RATE(0.2f); //Five times a second
const Float AssetExporterDisplay::WINDOW_POLL_RATE(0.1f); //Ten times a second

void AssetExporterDisplay::InitProps ()
{
	Super::InitProps();

	WindowSize = sf::Vector2f(400.f, 256.f);
	FontSize = 32;
	FontWidth = 1.f;
	NumCharsPerLine = 32.f;
	ScrollPosition = -1;

	RefreshDisplayTick = nullptr;
	PollingTick = nullptr;

	SpinningWheelChars = 
	{
		TXT("\\"),
		TXT("|"),
		TXT("/"),
		TXT("-")
	};
	SpinningWheelIdx = 0;
}

void AssetExporterDisplay::BeginObject ()
{
	Super::BeginObject();

	RefreshDisplayTick = TickComponent::CreateObject(TICK_GROUP_ASSET_EXPORTER);
	if (AddComponent(RefreshDisplayTick))
	{
		RefreshDisplayTick->SetTickHandler(SDFUNCTION_1PARAM(this, AssetExporterDisplay, HandleRefreshDisplayTick, void, Float));
		RefreshDisplayTick->SetTickInterval(WINDOW_REFRESH_RATE);
	}

	PollingTick = TickComponent::CreateObject(TICK_GROUP_ASSET_EXPORTER);
	if (AddComponent(PollingTick))
	{
		PollingTick->SetTickHandler(SDFUNCTION_1PARAM(this, AssetExporterDisplay, HandlePollingTick, void, Float));
		PollingTick->SetTickInterval(WINDOW_POLL_RATE);
	}

	AssetExporterEngineComponent* assetEngine = AssetExporterEngineComponent::Find();
	CHECK(assetEngine != nullptr && assetEngine->GetExporter() != nullptr)
	Exporter = assetEngine->GetExporter();
	Exporter->OnNextExporter = SDFUNCTION_1PARAM(this, AssetExporterDisplay, HandleNextExporter, void, size_t);

	DString fontName = TXT("Nouveau_IBM.ttf");
	Directory contentDir = Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("AssetExporter");
	if (!Font.loadFromFile(FileAttributes(contentDir, fontName).GetName(true, true).ToStr()))
	{
		AssetExporterLog.Log(LogCategory::LL_Fatal, TXT("Failed to import %s from %s."), fontName, contentDir);
	}
	else
	{
		CHECK(Font.hasGlyph('W'))
		const sf::Glyph& glyph = Font.getGlyph('W', FontSize, false);
		FontWidth = glyph.advance;
		WindowSize.x = (FontWidth * NumCharsPerLine);
	}

	WindowHandle.create(sf::VideoMode(static_cast<unsigned int>(WindowSize.x), static_cast<unsigned int>(WindowSize.y)), ProjectName.ToSfmlString(), sf::Style::Close | sf::Style::Titlebar);
}

void AssetExporterDisplay::Destroyed ()
{
	WindowHandle.close();
	
	Super::Destroyed();
}

void AssetExporterDisplay::RenderEverything ()
{
	if (Exporter.IsNullptr())
	{
		//Don't update if the exporter is destroyed.
		return;
	}

	const std::vector<ExportQueue::SExportStatus>& exportList = Exporter->ReadExportList();
	if (ContainerUtils::IsEmpty(exportList))
	{
		return;
	}

	//If scroll position is not yet assigned, and if there are enough exporters for scrolling, then snap the scrollbar to view the top exporters.
	if (ScrollPosition < 0 && exportList.size() > 5)
	{
		ScrollPosition = 3;
	}

	WindowHandle.clear(sf::Color::Black);

	/*
	This function will render the 8 lines, 32 characters per line.
	Choose to render in cmd prompt style to suggest that Sand Dune does not have assets yet.

	An example:
	1. =========Asset Exporter=========
	2. PdgUp / PdgDown to scroll  (7/9)
	3. 4   Texture Editor |    [PASSED]
	4. 5   Sound Editor   |[405 FAILED]
	5. 6 > Particle Editor| [EXPORTING]
	6. 7   Animation Edit-|   [PENDING]
	7. [01] FAILED! Check logs for info
	8. [===========-          ] 56%   \
	*/

	float lineHeight = WindowSize.y / 8;
	float numCharsPerLine = WindowSize.x / FontWidth;
	float fontDisplacement = FontSize * -0.15f;

	//Draw blue bar across the top
	{
		sf::RectangleShape blueTitle(sf::Vector2f(WindowSize.x, lineHeight));
		blueTitle.setPosition(sf::Vector2f(0.f, 0.f));
		blueTitle.setFillColor(sf::Color(96, 96, 255));
		blueTitle.setOutlineColor(sf::Color(96, 96, 255));
		WindowHandle.draw(blueTitle);
	}

	//Draw title across the blue bar
	{
		sf::Text title(ProjectName.ToSfmlString(), Font, FontSize);

		//Need to align horizontally.
		Float horizontalDisplacement = (NumCharsPerLine - Float(ProjectName.Length())) * 0.5f;
		horizontalDisplacement *= FontWidth;
		title.setPosition(sf::Vector2f(horizontalDisplacement.Value, fontDisplacement));
		title.setColor(sf::Color::White);
		WindowHandle.draw(title);
	}

	//Line 2 - If there are more than 5 exporters, then provide instructions regarding scrolling up and down.
	Int lineIdx = 1;
	size_t exportIdx = 0;
	if (exportList.size() > 5)
	{
		Int numSpaces = 2;
		Int displayedScrollPos = ScrollPosition + 1; //Convert from index to num
		if (displayedScrollPos.NumDigits() > 1)
		{
			numSpaces--;
		}

		if (exportList.size() > 9)
		{
			numSpaces--;
		}

		std::string spacing(numSpaces.Value, ' ');
		DString instructions = DString::CreateFormattedString(TXT("PdgUp / PdgDown to scroll%s(%s/%s)"), DString(spacing), Utils::Min<Int>(displayedScrollPos, 99), Utils::Min<Int>(Int(exportList.size()), 99));
		sf::Text sfText(instructions.ToSfmlString(), Font, FontSize);
		sfText.setPosition(sf::Vector2f(0.f, lineHeight + fontDisplacement));
		sfText.setColor(sf::Color::White);
		WindowHandle.draw(sfText);

		++lineIdx;
		exportIdx = ScrollPosition.ToUnsignedInt() - 3;
	}

	//Lines (2-3) to 6 - Display exporter status
	for (/*Noop*/; lineIdx < 6; ++lineIdx, ++exportIdx)
	{
		if (exportIdx >= exportList.size())
		{
			//No more exporters to display
			break;
		}

		Int exportNameColStartPos = 5; //5th character marks the export name column
		Int resultColStartPos = 20; //20th character marks the result column

		//Draw highlighting boxes if any
		switch(exportList.at(exportIdx).ExportState)
		{
			case(ExportQueue::ES_Exporting):
			{
				//Highlight the entire line
				sf::RectangleShape highlight(sf::Vector2f(WindowSize.x, lineHeight));
				highlight.setPosition(sf::Vector2f(0.f, (lineIdx.ToFloat() * lineHeight).Value));
				highlight.setFillColor(sf::Color(64, 64, 8));
				highlight.setOutlineColor(sf::Color(64, 64, 8));
				WindowHandle.draw(highlight);
				break;
			}
			case(ExportQueue::ES_Failed):
			case(ExportQueue::ES_Passed):
			{
				//Highlight the PASSED/FAILED text
				sf::RectangleShape highlight(sf::Vector2f(((NumCharsPerLine - resultColStartPos).ToFloat() * FontWidth).Value, lineHeight));
				highlight.setPosition(sf::Vector2f((resultColStartPos.ToFloat() * FontWidth).Value, (lineIdx.ToFloat() * lineHeight).Value));

				sf::Color highlightCol = (exportList.at(exportIdx).ExportState == ExportQueue::ES_Passed) ? sf::Color(0, 96, 0) : sf::Color(96, 0, 0);
				highlight.setFillColor(highlightCol);
				highlight.setOutlineColor(highlightCol);
				WindowHandle.draw(highlight);
				break;
			}
		}

		//Populate the text
		DString firstCol; //[4 characters] Contains the line number and the '>' if it's the current active exporter.
		{
			Int numSpaces = 3;
			if (exportIdx >= 9)
			{
				numSpaces--;
			}

			Int displayNum = Utils::Min<Int>(exportIdx+1, 99);

			std::string spaces(numSpaces.Value, ' ');
			firstCol = displayNum.ToString() + spaces;

			if (exportList.at(exportIdx).ExportState == ExportQueue::ES_Exporting)
			{
				firstCol[2] = '>';
			}
		}

		DString secCol; //[15 characters] Contains the exporter name.
		{
			secCol = exportList.at(exportIdx).ExportName;
			if (secCol.Length() > 15)
			{
				secCol = secCol.SubString(0, 13); //Cut off one extra character.
				secCol += '-'; //Add a dash instead.
			}
			else
			{
				//Add spaces until we reach the end.
				while (secCol.Length() < 15)
				{
					secCol += ' ';
				}
			}

			//Add the divider to show where this column ends.
			secCol += '|';
		}

		DString thirdCol; //[12 characters] Displays exporter's status
		{
			switch(exportList.at(exportIdx).ExportState)
			{
				case(ExportQueue::ES_Pending):
					thirdCol = TXT("   [PENDING]");
					break;

				case(ExportQueue::ES_Exporting):
					thirdCol = TXT(" [EXPORTING]");
					break;

				case(ExportQueue::ES_Passed):
					thirdCol = TXT("    [PASSED]");
					break;

				case(ExportQueue::ES_Failed):
				{
					Int numErrors = Utils::Min<Int>(exportList.at(exportIdx).NumFailed, 999);
					Int numSpaces = 4 - numErrors.NumDigits();
					std::string spaces(numSpaces.Value, ' ');
					thirdCol = DString::CreateFormattedString(TXT("[%s%sFAILED]"), numErrors, DString(spaces));
					break;
				}
			}
		}

		sf::Text drawText((firstCol + secCol + thirdCol).ToSfmlString(), Font, FontSize);
		drawText.setPosition(sf::Vector2f(0.f, ((lineIdx.ToFloat() * lineHeight) + fontDisplacement).Value));
		drawText.setColor(sf::Color::White);
		WindowHandle.draw(drawText);
	}

	lineIdx = 6; //Line 7 - Display summary. If nothing finished, simply display 'Exporting...'.  If nothing failed, displayed number of successes. If there's at least one error, display errors.
	{
		DString summaryText(DString::EmptyString);
		if (exportList.at(0).ExportState == ExportQueue::ES_Exporting || exportList.at(0).ExportState == ExportQueue::ES_Pending)
		{
			summaryText = TXT("Exporting . . .");
		}
		else
		{
			Int numFailed = 0;
			Int numPassed = 0;
			for (const ExportQueue::SExportStatus& status : exportList)
			{
				if (status.ExportState == ExportQueue::ES_Failed)
				{
					++numFailed;
				}
				else if (status.ExportState == ExportQueue::ES_Passed)
				{
					++numPassed;
				}
			}

			if (numFailed == 0)
			{
				summaryText = DString::CreateFormattedString(TXT("[%s] EXPORTED!"), numPassed.ToFormattedString(2));
			}
			else
			{
				numFailed = Utils::Min<Int>(numFailed, 99);
				summaryText = DString::CreateFormattedString(TXT("[%s] FAILED! Check logs for info"), numFailed.ToFormattedString(2));
			}
		}

		sf::Text drawText(summaryText.ToSfmlString(), Font, FontSize);
		drawText.setPosition(sf::Vector2f(0.f, ((lineIdx.ToFloat() * lineHeight) + fontDisplacement).Value));
		drawText.setColor(sf::Color::White);
		WindowHandle.draw(drawText);
	}

	lineIdx = 7; //Line 8 - Display progress bar
	{
		DString displayStr;
		
		//Find which exporter is currently exporting.
		size_t activeIdx = UINT_INDEX_NONE;
		for (size_t i = 0; i < exportList.size(); ++i)
		{
			if (exportList.at(i).ExportState == ExportQueue::ES_Exporting)
			{
				activeIdx = i;
				break;
			}
		}

		Float progress = (Float(activeIdx) / Float(exportList.size()));
		if (progress >= 1.f)
		{
			//Draw highlighter
			sf::RectangleShape highlight(sf::Vector2f(WindowSize.x, lineHeight));
			highlight.setPosition(sf::Vector2f(0.f, (lineIdx * lineHeight).Value));
			highlight.setFillColor(sf::Color(24, 32, 96));
			highlight.setOutlineColor(sf::Color(24, 32, 96));
			WindowHandle.draw(highlight);

			displayStr = TXT("DONE! Press <ENTER> to terminate");
		}
		else
		{
			displayStr = TXT("[");

			Int progressBarLength = 22; //Number of characters that makeup the progress bar.
			Float percentPerChar = (100.f / progressBarLength.ToFloat()); //How much of a % one character covers.

			Int numEquals = ((progress * 100.f) / percentPerChar).ToInt();
			Int numSpaces = progressBarLength - numEquals;

			if (numEquals > 0)
			{
				numEquals--; //Last character is a hyphen
				if (numEquals > 0)
				{
					displayStr += std::string(numEquals.Value, '=');
				}

				displayStr += TXT("-");
			}

			if (numSpaces > 0)
			{
				displayStr += std::string(numSpaces.Value, ' ');
			}

			displayStr += TXT("] ") + (progress * 100.f).ToInt().ToFormattedString(2) + TXT("%   ");

			//Display spinning wheel
			displayStr += SpinningWheelChars.at(SpinningWheelIdx);
			++SpinningWheelIdx;
			if (SpinningWheelIdx >= SpinningWheelChars.size())
			{
				SpinningWheelIdx = 0;
			}
		}

		sf::Text drawText(displayStr.ToSfmlString(), Font, FontSize);
		drawText.setPosition(sf::Vector2f(0.f, ((lineIdx * lineHeight) + fontDisplacement).Value));
		drawText.setColor(sf::Color::White);
		WindowHandle.draw(drawText);
	}

	WindowHandle.display();
}

void AssetExporterDisplay::PollWindowEvents ()
{
	bool bPendingDestruction = false;
	static const size_t MIN_SCROLL_SIZE = 5; //Minimum number of exporters to enable scrolling

	sf::Event pollEvent;
	while (WindowHandle.pollEvent(OUT pollEvent))
	{
		switch (pollEvent.type)
		{
			case (sf::Event::Closed):
				bPendingDestruction = true;
				break;

			case (sf::Event::KeyReleased):
			{
				if (Exporter.IsValid())
				{
					switch (pollEvent.key.code)
					{
						case (sf::Keyboard::PageUp):
						{
							if (Exporter->ReadExportList().size() > MIN_SCROLL_SIZE)
							{
								ScrollPosition = Utils::Max<Int>(ScrollPosition - 1, MIN_SCROLL_SIZE - 2);
							}

							break;
						}

						case (sf::Keyboard::PageDown):
						{
							if (Exporter->ReadExportList().size() > MIN_SCROLL_SIZE)
							{
								ScrollPosition = Utils::Min<Int>(ScrollPosition + 1, Exporter->ReadExportList().size() - 1);
							}

							break;
						}

						case (sf::Keyboard::Enter):
						{
							ExportQueue::EExportState exportState = ContainerUtils::GetLast(Exporter->ReadExportList()).ExportState;
							if (exportState != ExportQueue::ES_Pending && exportState != ExportQueue::ES_Exporting)
							{
								bPendingDestruction = true;
							}

							break;
						}
					}
				}

				break;
			} //KeyReleased
		} //PollType
	} //While polling events

	if (bPendingDestruction)
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
	}
}

void AssetExporterDisplay::HandleNextExporter (size_t exporterIdx)
{
	//Automatically scroll down if the next exporter is beyond the scroll position and if the user didn't scroll up.

	//If the next scroll position is equal to the next exporter.
	if ((ScrollPosition + 1).ToUnsignedInt() == exporterIdx && Exporter.IsValid())
	{
		ScrollPosition = Utils::Min<Int>(ScrollPosition + 3, Exporter->ReadExportList().size() - 1);
	}
}

void AssetExporterDisplay::HandleRefreshDisplayTick (Float deltaSec)
{
	RenderEverything();
}

void AssetExporterDisplay::HandlePollingTick (Float deltaSec)
{
	PollWindowEvents();
}
AE_END