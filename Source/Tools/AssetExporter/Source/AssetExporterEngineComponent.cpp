/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AssetExporterEngineComponent.cpp
=====================================================================
*/

#include "AssetExporterDisplay.h"
#include "AssetExporterEngineComponent.h"
#include "ExportQueue.h"

IMPLEMENT_ENGINE_COMPONENT(SD::AE::AssetExporterEngineComponent)
AE_BEGIN

AssetExporterEngineComponent::AssetExporterEngineComponent () :
	Super(),
	Display(nullptr)
{

}

void AssetExporterEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_ASSET_EXPORTER, TICK_GROUP_PRIORITY_ASSET_EXPORTER);
}

void AssetExporterEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	Exporter = ExportQueue::CreateObject();
	Exporter->LaunchExporters();

	//Ensure the display is initialized after Exporter since the display depends on it.
	Display = AssetExporterDisplay::CreateObject();
}

void AssetExporterEngineComponent::ShutdownComponent ()
{
	if (Display != nullptr)
	{
		Display->Destroy();
		Display = nullptr;
	}

	if (Exporter.IsValid())
	{
		Exporter->Destroy();
		Exporter = nullptr;
	}

	Super::ShutdownComponent();
}

void AssetExporterEngineComponent::GetInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetInitializeDependencies(OUT outDependencies);

	//Ensure the Log file is up and running before executing anything in the AssetExporter
	outDependencies.push_back(LoggerEngineComponent::SStaticClass());
}
AE_END