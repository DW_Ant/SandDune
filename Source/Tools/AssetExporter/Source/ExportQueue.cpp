/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ExportQueue.cpp
=====================================================================
*/

#include "ExportQueue.h"
#include "ListOfExporters.CMakeGenerated.h"

IMPLEMENT_CLASS(SD::AE::ExportQueue, SD::Entity)
AE_BEGIN

void ExportQueue::InitProps ()
{
	Super::InitProps();

	CurrentExportIdx = -1;
	bClean = false;
	bCleanOnly = false;
#ifdef DEBUG_MODE
	NumSimulatedExporters = 0;
	SimExporterDuration = 2.f;
	SimExporterTimeRemaining = SimExporterDuration;
#endif
	ExportCmdLineArgs = TXT("-AutoExport");

	ProcCheckTick = nullptr;
}

void ExportQueue::BeginObject ()
{
	Super::BeginObject();

	ProcCheckTick = TickComponent::CreateObject(TICK_GROUP_ASSET_EXPORTER);
	if (AddComponent(ProcCheckTick))
	{
		ProcCheckTick->SetTickHandler(SDFUNCTION_1PARAM(this, ExportQueue, HandleTick, void, Float));
		ProcCheckTick->SetTickInterval(0.5f);
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	bCleanOnly = localEngine->HasCmdLineSwitch(TXT("-CleanOnly"), DString::CC_IgnoreCase);
	bClean = (bCleanOnly || localEngine->HasCmdLineSwitch(TXT("-Clean"), DString::CC_IgnoreCase));
	if (bCleanOnly)
	{
		ExportCmdLineArgs += TXT(" -CleanOnly");
		AssetExporterLog.Log(LogCategory::LL_Log, TXT("Running Asset Exporters in CleanOnly mode."));
	}
	else if (bClean)
	{
		ExportCmdLineArgs += TXT(" -Clean");
		AssetExporterLog.Log(LogCategory::LL_Log, TXT("Running Asset Exporters in Clean mode."));
	}
	else
	{
		AssetExporterLog.Log(LogCategory::LL_Log, TXT("Running Asset Exporters in normal mode."));
	}

	if (localEngine->HasCmdLineSwitch(TXT("-log"), DString::CC_IgnoreCase))
	{
		ExportCmdLineArgs += TXT(" -log");
	}

#ifdef DEBUG_MODE
	NumSimulatedExporters = Int(localEngine->GetCmdLineValue(TXT("NumSimulatedExporters"), DString::CC_IgnoreCase));
	if (NumSimulatedExporters > 8)
	{
		SimExporterDuration = 1.f; //Reduce the duration to one exporter per sec
	}
#endif

	std::vector<DString> exportNames;
#ifdef DEBUG_MODE
	if (NumSimulatedExporters <= 0)
	{
#endif
		GetExportList(OUT exportNames);

		//Sort alphabetically
		std::sort(exportNames.begin(), exportNames.end(), [](const DString& a, const DString& b)
		{
			return (a.Compare(b, DString::CC_CaseSensitive) < 0);
		});
#ifdef DEBUG_MODE
	}
	else
	{
		for (Int i = 1; i <= NumSimulatedExporters; ++i)
		{
			exportNames.push_back(TXT("Test Exprter ") + i.ToString());
		}

		AssetExporterLog.Log(LogCategory::LL_Log, TXT("Running Asset Exporters in simulated mode with %s fake exporter(s)."), NumSimulatedExporters);
	}
#endif

	for (const DString& exportName : exportNames)
	{
		ExportList.emplace_back(exportName);
	}
}

void ExportQueue::LaunchExporters ()
{
	if (CurrentExportIdx >= 0)
	{
		AssetExporterLog.Log(LogCategory::LL_Warning, TXT("The ExportQueue is already processing through its list. The extra LaunchExporters call is ignored."));
		return;
	}

	if (ContainerUtils::IsEmpty(ExportList))
	{
		AssetExporterLog.Log(LogCategory::LL_Warning, TXT("There aren't any exporters to run. Terminating AssetExporter."));
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
		return;
	}

	LaunchNextExporter();
}

void ExportQueue::LaunchNextExporter ()
{
	++CurrentExportIdx;
	size_t exportIdx = CurrentExportIdx.ToUnsignedInt();
	if (OnNextExporter.IsBounded())
	{
		OnNextExporter(exportIdx);
	}

	if (exportIdx >= ExportList.size())
	{
		//That's the last of the exporter
		FinishAssetExporter();
		return;
	}

#ifdef DEBUG_MODE
	if (NumSimulatedExporters > 0)
	{
		ExportList.at(exportIdx).ExportState = ES_Exporting;
		SimExporterTimeRemaining = SimExporterDuration;
		return;
	}
#endif

	LaunchProcessOptions options(FileAttributes(Directory::BINARY_DIRECTORY, ExportList.at(exportIdx).ExportName + Utils::GetBinaryConfigSuffix() + OUTPUT_EXTENSION));
	options.CmdLineArgs = ExportCmdLineArgs;
	options.bChildProcess = true;

	if (OS_LaunchProcess(options, OUT ExportInfo))
	{
		ExportList.at(exportIdx).ExportState = ES_Exporting;
	}
	else
	{
		AssetExporterLog.Log(LogCategory::LL_Warning, TXT("Failed to launch exporter: %s"), options.ExecFile.GetName(true, true));
		ExportList.at(exportIdx).ExportState = ES_Failed;
		ExportList.at(exportIdx).NumFailed = 1;
		LaunchNextExporter();
	}
}

void ExportQueue::FinishAssetExporter ()
{
	CHECK(ProcCheckTick != nullptr)
	ProcCheckTick->SetTicking(false);

	Int numPassed = 0;
	for (const SExportStatus& exportItem : ExportList)
	{
		if (exportItem.ExportState == ES_Passed)
		{
			++numPassed;
		}
	}

	Int numFailed = Int(ExportList.size()) - numPassed;
	if (numFailed > 0)
	{
		AssetExporterLog.Log(LogCategory::LL_Warning, TXT("Asset Exporter task completed. %s exporter(s) passed. %s exporter(s) failed."), numPassed, numFailed);
	}
	else
	{
		AssetExporterLog.Log(LogCategory::LL_Log, TXT("Asset Exporter task completed. All %s exporter(s) passed!"), numPassed);
	}
}

void ExportQueue::CheckProcessStatus ()
{
#ifdef DEBUG_MODE
	if (NumSimulatedExporters > 0)
	{
		if (SimExporterTimeRemaining <= 0.f)
		{
			size_t exportIdx = CurrentExportIdx.ToUnsignedInt();
			ExportList.at(exportIdx).ExportState = (CurrentExportIdx.IsEven()) ? ES_Passed : ES_Failed;
			if (ExportList.at(exportIdx).ExportState == ES_Failed)
			{
				ExportList.at(exportIdx).NumFailed = CurrentExportIdx;
			}

			LaunchNextExporter();
		}

		return;
	}
#endif

	if (OS_IsProcessAlive(ExportInfo))
	{
		return;
	}

	int exitCode;
	if (OS_GetExitCode(ExportInfo, OUT exitCode))
	{
		size_t exportIdx = CurrentExportIdx.ToUnsignedInt();
		LogCategory::ELogLevel logLevel = (exitCode == 0) ? LogCategory::LL_Log : LogCategory::LL_Warning;
		AssetExporterLog.Log(logLevel, TXT("%s finished with exit code: %s."), ExportList.at(exportIdx).ExportName, Int(exitCode));
		
		ExportList.at(exportIdx).ExportState = (exitCode == 0) ? ES_Passed : ES_Failed;
		if (exitCode < 0)
		{
			//Notify the display to show 1 error instead of negative errors.
			exitCode = 1;
		}

		ExportList.at(exportIdx).NumFailed = exitCode;
		LaunchNextExporter();
	}
}

void ExportQueue::HandleTick (Float deltaSec)
{
#ifdef DEBUG_MODE
	if (NumSimulatedExporters > 0)
	{
		SimExporterTimeRemaining -= deltaSec;
	}
#endif

	CheckProcessStatus();
}
AE_END