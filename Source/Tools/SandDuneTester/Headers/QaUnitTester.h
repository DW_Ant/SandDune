/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QaUnitTester.h
  Runs various unit tests that requires a composite of 2 or more modules.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class QaUnitTester : public SD::UnitTester
{
	DECLARE_CLASS(QaUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
};
SD_TESTER_END

#endif