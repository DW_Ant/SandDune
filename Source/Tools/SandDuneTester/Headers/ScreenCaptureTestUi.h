/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScreenCaptureTestUi.h

  The GuiEntity that runs a visualization of a ScreenCaptureTest with various configurable settings.
  This UI is responsible for triggering the ScreenCapture entity to generate a texture, and
  with the generated texture, the user can hover over pixels to find the associated RenderComponent that
  drawn on that pixel.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ScreenCaptureTest;

class ScreenCaptureTestUi : public SD::GuiEntity
{
	DECLARE_CLASS(ScreenCaptureTestUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::ScreenCapture* Capture;

	/* Component responsible for rendering the original scene. */
	SD::ScrollbarComponent* SceneScrollbar;

	/* Component responsible for rendering the generated sprite. */
	SD::LabelComponent* NumCompsLabel;
	SD::FrameComponent* GeneratedTextureFrame;
	SD::LabelComponent* GeneratedTextureLabel;

	SD::LabelComponent* HoveredPixelLabel;

	/* Components related to configuring the screen capture settings. */
	SD::ScrollbarComponent* ConfigurationScrollbar;
	SD::LabelComponent* PixelTypeLabel;
	SD::DropdownComponent* PixelTypeDropdown;
	SD::LabelComponent* AlphaThresholdLabel;
	SD::TextFieldComponent* AlphaThresholdField;
	SD::CheckboxComponent* IncludeColorComps;
	SD::CheckboxComponent* IncludeSpriteComps;
	SD::LabelComponent* RecursiveCaptureDescription;
	SD::CheckboxComponent* RecursiveCaptureComps;

private:
	/* Becomes true if this UI is overriding the mouse icon. */
	bool bOverridingMouseIcon;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void Destroyed () override;
	virtual void HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates a series of components attached to the given Entity. The Entity will be used as the test case for the ScreenCapture.
	 */
	virtual void CreateCompScene (SD::GuiEntity* sceneOwner);

	virtual void RunScreenCapture ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseIconOverride (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent);
	virtual bool HandleAlphaThresholdAllowText (const SD::DString& txt);
	virtual void HandleAlphaThresholdReturn (SD::TextFieldComponent* uiComponent);
	virtual void HandleRunCaptureButtonReleased (SD::ButtonComponent* button);
	virtual bool HandleRenderCompRelevant (SD::RenderComponent* renderComp);
};
SD_TESTER_END
#endif