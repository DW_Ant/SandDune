/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Player.h
  An Entity in 3D space that represents something that the player may manipulate.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class Player : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Player)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various vectors that adjusts the player transform continuously. Values should range between -1 to 1 since they act like multipliers to the base speeds. */
	SD::Vector3 VelocityInput;
	SD::Vector3 GrowthInput;


protected:
	SD::DPointer<SD::ColorRenderComponent> RenderComp;
	SD::DPointer<SD::InputComponent> InputComp;
	SD::TickComponent* Tick;

	/* Determines how quickly this player may traverse through an axis. */
	SD::Float MoveSpeed;

	/* Determines how quickly this player may grow/shrink in size. */
	SD::Float GrowSpeed;

private:
	/* Various input flags for player movement commands. */
	bool InputLeft : 1;
	bool InputUp : 1;
	bool InputRight : 1;
	bool InputDown : 1;
	bool InputGrow : 1;
	bool InputShrink : 1;
	unsigned int InputUnused : 2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void BeginObject () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::ColorRenderComponent* GetRenderComp () const
	{
		return RenderComp.Get();
	}

	inline SD::InputComponent* GetInputComp () const
	{
		return InputComp.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleKeyboardInput (const sf::Event& evnt);
	virtual void HandleTick (SD::Float deltaSec);
};
SD_TESTER_END

#endif