/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTesterTeleporter.h
  A simple Entity associated with the Box2dTester that simply teleports all dynamic
  Box2dComponent Entities to a destination.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN

class Box2dTesterTeleporter : public SD::SceneEntity
{
	DECLARE_CLASS(Box2dTesterTeleporter)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then the teleporting Entities will preserve their X/Y coordinate. */
	bool PreserveXCoordinates;
	bool PreserveYCoordinates;

	/* Point in 3D space entities will teleport to once they overlap with this Entity. */
	SD::Vector3 TeleportDestination;

protected:
	SD::Box2dComponent* PhysComp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::Box2dComponent* GetPhysComp () const
	{
		return PhysComp;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBeginContact (SD::Box2dComponent* otherComp, b2Contact* contactData);
};
SD_TESTER_END

#endif