/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteTester.h
  Object that acts like a manager for all Entities related to the Sprite test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SpriteTesterUi;

class SpriteTester : public WindowedEntityManager
{
	DECLARE_CLASS(SpriteTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* The GuiEntity responsible for handling the components that comprise the Sprite test. */
	SpriteTesterUi* MainTestUi;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);
};

SD_TESTER_END
#endif