/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteTester.h

  The GuiEntity that runs a visualization of a single sprite component with various configurable settings.
  The user can specify if the sprite should be stretched or tiled.
  The user can specify the draw multipliers, sub divisions, or draw coordinates.
  Lastly, the user can drag the sprites by its corners to scale it.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SpriteTester;

class SpriteTesterUi : public SD::GuiEntity
{
	DECLARE_CLASS(SpriteTesterUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The tester that launched this entity. */
	SpriteTester* OwningTester;

	SD::FrameComponent* PreviewFrame;
	SD::FrameComponent* SpriteFrame; //Draggable frame that scales the border and sprite
	SD::BorderRenderComponent* TestBorderComp;
	SD::PlanarTransformComponent* TestSpriteTransform; //Transform that fits the TestSprite within the test border's borders.
	SD::SpriteComponent* TestSprite;
	SD::DropdownComponent* DrawModeDropdown;

	//SubDivision section
	SD::EditableInt* NumHorizontalSegField;
	SD::EditableInt* NumVerticalSegField;
	SD::EditableInt* HorizontalIdxField;
	SD::EditableInt* VerticalIdxField;
	SD::ButtonComponent* ApplySubDivisionButton;

	//SetDrawCoordinatesMultipliers section
	SD::EditableFloat* XScaleMultField;
	SD::EditableFloat* YScaleMultField;
	SD::EditableFloat* XPosMultField;
	SD::EditableFloat* YPosMultField;
	SD::ButtonComponent* ApplyDrawCoordMultButton;

	//SetDrawCoordinates section
	SD::EditableInt* UField;
	SD::EditableInt* VField;
	SD::EditableInt* UlField;
	SD::EditableInt* VlField;
	SD::ButtonComponent* ApplyDrawCoordButton;

	//Components related to testing the BorderRenderComponent
	SD::EditableBool* BorderSetTextureCheckbox;
	SD::EditableFloat* BorderThicknessField;
	SD::EditableInt* BorderColorR;
	SD::EditableInt* BorderColorG;
	SD::EditableInt* BorderColorB;
	SD::EditableFloat* BorderTileTextureField;
	SD::EditableFloat* BorderStretchTextureField;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningTester (SpriteTester* newOwningTester);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleDrawModeOptionSelected (SD::DropdownComponent* comp, SD::Int optionIdx);
	virtual void HandleApplySubDivisionReleased (SD::ButtonComponent* uiComp);
	virtual void HandleApplyDrawCoordMultReleased (SD::ButtonComponent* uiComp);
	virtual void HandleApplyDrawCoordReleased (SD::ButtonComponent* uiComp);

	virtual void HandleSetBorderTexture (SD::EditPropertyComponent* editProp);
	virtual void HandleSetBorderThickness (SD::EditPropertyComponent* editProp);
	virtual void HandleSetBorderColor (SD::EditPropertyComponent* editProp);
	virtual void HandleSetBorderTiledTexture (SD::EditPropertyComponent* uiComp);
	virtual void HandleSetBorderStretchedTexture (SD::EditPropertyComponent* uiComp);
};
SD_TESTER_END
#endif