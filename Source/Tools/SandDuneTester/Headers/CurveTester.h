/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveTester.h
  Object that manages Entities related to the Curve test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.

  This test launches a window containing a GUI that allows the user to draw curves.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class CurveTestPlotter;

class CurveTester : public WindowedEntityManager
{
	DECLARE_CLASS(CurveTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;
	CurveTestPlotter* Plotter;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/
	
public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);
};
SD_TESTER_END
#endif