/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTesterUi.h

  A simple GuiEntity that allows the end user to launch various Box 2D tests.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class Box2dTester;

class Box2dTesterUi : public SD::GuiEntity
{
	DECLARE_CLASS(Box2dTesterUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::ScrollbarComponent* MenuList;
	SD::GuiEntity* LaunchButtonsUi;
	std::vector<SD::ButtonComponent*> LaunchButtons;

	/* Entity responsible for managing this UI. */
	Box2dTester* OwningTester;

protected:
	SD::Color StaticColor;
	SD::Color DynamicColor;
	SD::Color OverlapColor;
	SD::Color TeleporterColor;

private:
	/* All PhysicsComponents instantiated from this test will be using this number
	to ensure these entities will not collide against other objects instantiated from other systems. */
	int16 CollisionGroup;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningTester (Box2dTester* newOwningTester)
	{
		OwningTester = newOwningTester;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes the Render Entity for the given Entity.
	 * The Render component will attach itself to the Entity. It'll initialize its color, and it'll register to the tester's draw layer.
	 */
	virtual void InitializeRenderComponent (SD::SceneEntity* entity, SD::Color renderColor) const;

	/**
	 * Creates and initializes an Entity with a static Physics component that has a rectangular collision box.
	 * The Position specifies the center point of the rectangle.
	 */
	virtual SD::SceneEntity* CreateStaticRectangle (std::vector<Entity*>& outEntityList, const SD::Vector3& position, const SD::Rotator& rotation, const SD::Rectangle& dimensions) const;

	/**
	 * Creates four static rectangles that form a perimeter matching the specified dimensions. Units are in SD units.
	 */
	virtual void CreateStaticPerimeter (std::vector<Entity*>& outEntityList, SD::Float width, SD::Float height) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLaunchBouncyBallTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchMultiBallTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchOverlapTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchTumbleTest (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif