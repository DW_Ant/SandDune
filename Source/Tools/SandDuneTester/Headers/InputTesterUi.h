/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputTesterUi.h
  
  For the Input unit test, this UI entity is responsible for allowing the user
  to configure their controls to change the circle's colors.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class InputTester;

class InputTesterUi : public SD::GuiEntity
{
	DECLARE_CLASS(InputTesterUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	InputTester* TestOwner;

	SD::InputComponent* Input;
	SD::ColorRenderComponent* Circle;

	SD::TextFieldComponent* RedBind;
	SD::TextFieldComponent* GreenBind;
	SD::TextFieldComponent* BlueBind;
	SD::ButtonComponent* ExitButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Assigns various variables to register input components and notify which owner to destroy whenever this test concludes.
	 */
	virtual void InitializeInputTestUi (InputTester* newTestOwner, SD::InputBroadcaster* broadcaster);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if one of the key binding text fields have focus.
	 */
	virtual bool IsConfiguringKeybind () const;

	/**
	 * Updates the content of the given text field to display the keybind for the given event.
	 */
	virtual void UpdateTextField (SD::TextFieldComponent* textField, sf::Keyboard::Key newBind);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleRedEdit (const sf::Event& evnt);
	virtual bool HandleGreenEdit (const sf::Event& evnt);
	virtual bool HandleBlueEdit (const sf::Event& evnt);
	virtual bool HandleRedBind ();
	virtual bool HandleGreenBind ();
	virtual bool HandleBlueBind ();
	virtual bool HandleAddRedBind ();
	virtual bool HandleAddGreenBind ();
	virtual bool HandleAddBlueBind ();
	virtual bool HandleRemoveRedBind ();
	virtual bool HandleRemoveGreenBind ();
	virtual bool HandleRemoveBlueBind ();
	virtual void HandleExitButtonReleased (SD::ButtonComponent* button);
};
SD_TESTER_END
#endif