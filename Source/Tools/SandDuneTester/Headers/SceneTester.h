/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTester.h
  Object that acts like a manager for all Entities related to the SceneTester.

  Generally a manager Entity like this would be an Engine Component, or leveraging the other managers such as the main
  InputBroadcaster. This functionality was moved to this Entity in order to keep the numerous objects somewhat self contained,
  and more importantly, to minimize the impact of other objects influencing this test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SceneTesterHud;
class Player;

class SceneTester : public WindowedEntityManager
{
	DECLARE_CLASS(SceneTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	static const std::pair<SD::Vector3, SD::Vector3> PLAYER_POS_RANGE;
	static const std::pair<SD::Vector3, SD::Vector3> PLAYER_SIZE_RANGE;

	static const std::pair<SD::Vector3, SD::Vector3> CAMERA_POS_RANGE;
	static const std::pair<SD::Float, SD::Float> CAMERA_ZOOM_RANGE;

	/* Various vectors that adjusts the entity transforms continuously. Values should range between -1 to 1 since they act like multipliers to the base speeds. */
	SD::Vector3 CameraVelocityInput;
	SD::Float CameraZoomInput;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* The DrawLayer responsible for rendering the SceneTransform Entities. */
	SD::SceneDrawLayer* SceneLayer;

	/* Camera used to render the Entities in the scene. */
	SD::TopDownCamera* SceneCamera;

	/* Entity used to represent the player object that can be moved around through input. */
	Player* PlayerTester;

	/* GuiEntity responsible for drawing the Hud elements and the controls to manipulate scene objects. */
	SceneTesterHud* Hud;

	SD::TickComponent* Tick;

	/* Determines how fast the camera may traverse along an axis (in units per second). */
	SD::Float CameraMoveSpeed;
	SD::Float CameraZoomSpeed;

private:
	/* Various input flags for camera controls. */
	bool InputCameraUp : 1;
	bool InputCameraRight : 1;
	bool InputCameraDown : 1;
	bool InputCameraLeft : 1;
	bool InputCameraZoomIn : 1;
	bool InputCameraZoomOut : 1;
	bool InputReserved : 2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleKeyboardInput (const sf::Event& evnt);
	virtual void HandleTick (SD::Float deltaSec);
};
SD_TESTER_END

#endif