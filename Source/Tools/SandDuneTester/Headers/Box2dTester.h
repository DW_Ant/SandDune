/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTester.h
  Object that acts like a manager for all Entities related to the Box 2D test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class Box2dTesterUi;

class Box2dTester : public WindowedEntityManager
{
	DECLARE_CLASS(Box2dTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* Draw layer that'll render the Physics entities. */
	SD::SceneDrawLayer* SceneLayer;
	SD::TopDownCamera* SceneCamera;

	/* UI that'll present the user various physics configurations/tests. */
	Box2dTesterUi* Ui;

	/* Active Physics Entities that are currently running. */
	std::vector<Entity*> ActivePhysicsEntities;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);

	/**
	 * Notifies this test to replace all active test Entities with the given ones.
	 * It'll abort the previous test if it's still running.
	 */
	virtual void LaunchNewPhysicsTest (const std::vector<Entity*>& newEntities);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::SceneDrawLayer* GetSceneDrawLayer () const
	{
		return SceneLayer;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Finds the associated RenderComponent and brightens/darkens its red color channel.
	 */
	virtual void RefreshRenderCompColors (SD::Box2dComponent* physComp, bool incrementBrightness) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBeginContact (SD::Box2dComponent* otherComp, b2Contact* contactData);
	virtual void HandleEndContact (SD::Box2dComponent* otherComp, b2Contact* contactData);
	virtual void HandleCollision (SD::Box2dComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse);
};
SD_TESTER_END

#endif