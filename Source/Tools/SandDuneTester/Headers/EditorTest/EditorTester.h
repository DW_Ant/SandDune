/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorTester.h
  Object that acts like a manager for all Entities related to the Editor test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.

  This test simply launches and manages two UI instances that acts like a simple editor.
  One editor is the MockEditor where each component and serialization aspect is handled manually.
	-This showcases more flexibility at the expense of code bloat.
  The other editor leverages the EditorUtils to handle generating inspectors and serialization.
	-This showcases a far simpler solution.

  Only one of the two editors are visible at any given time. There's a button to swap editors.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class GeneratedInspector;
class MockEditor;

class EditorTester : public WindowedEntityManager
{
	DECLARE_CLASS(EditorTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;
	MockEditor* Editor;
	GeneratedInspector* GeneratedEditor;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/
	
public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleRevealMockEditor ();
	virtual void HandleRevealGeneratedInspector ();
};
SD_TESTER_END
#endif