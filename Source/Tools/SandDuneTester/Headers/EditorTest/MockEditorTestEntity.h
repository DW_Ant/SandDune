/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditorTestEntity.h
  The test subject the Mock Editor uses to edit this Entity's variables.

  This class demonstrates how a developer can build an editor for their Entities, save and load it as a developer asset, and
  save and load it in a binary file for release.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ObjTest_ChildClass;
class ObjTest_ParentClass;

class MockEditorTestEntity : public SD::Entity
{
	DECLARE_CLASS(MockEditorTestEntity)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ETestEnum : unsigned char
	{
		TE_FirstValue,
		TE_SecondValue,
		TE_ThirdValue
	};
	DEFINE_DYNAMIC_ENUM_3VAL(ETestEnum, TE_FirstValue, TE_SecondValue, TE_ThirdValue);


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SCaptionStruct
	{
		SD::DString TestStr;
		SD::Int TestInt;
	};

	struct SInnerStruct
	{
		SD::Int InnerInt;
		SD::Float InnerFloat;
		SD::DString InnerString;
	};

	struct SNestedStruct
	{
		bool OuterBool;
		SInnerStruct InnerStruct;
		SD::DString OuterString;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of variables the mock editor can modify. */
	SD::Int SimpleInt;
	SD::Int ReadOnlyInt;
	SD::Int PositiveInt;
	SD::Int NegativeInt;
	SD::Int IntBetweenZeroAndOneHundred;

	/* This Entity will continuously increment this number. The editor should be able to write to it while this Entity is being simulated.
	When the editor writes to this variable, it should simply continue counting starting from where it was set to. */
	SD::Int CountingInt;

	SD::Float SimpleFloat;
	SD::Float NoDecimal;
	SD::Float MinNegHundred;
	SD::Float MaxHundred;
	SD::Float ClampedFloat;

	SD::Vector2 SimpleVect2;
	SD::Vector2 LockedYAxisVect2;
	SD::Vector2 ClampedXAxisVect2;

	SD::Vector3 SimpleVect3;
	SD::Vector3 LockedZAxisVect3;

	SD::DString SimpleStr;
	SD::DString VowelsOnly;
	SD::DString ShortStr;
	SD::DString MultiLineStr;
	SD::DString CaptionTestStr;

	bool SimpleBool;
	bool ReadOnlyBool;

	ObjTest_ParentClass* ParentRef;
	ObjTest_ChildClass* ChildRef;

	ETestEnum TestEnum;

	SCaptionStruct CaptionStruct;
	SNestedStruct NestedStruct;

	std::vector<SD::Int> ArrayInt;
	std::vector<SD::Float> ArrayFloat;
	std::vector<SD::DString> ArrayString;
	std::vector<SD::Vector2> ArrayVector2;
	std::vector<SD::Vector3> ArrayVector3;
	std::vector<SD::Int> ArrayReadOnly;
	std::vector<SD::Int> ArrayFixedSize;
	std::vector<SD::Int> ArrayMaxSize;

protected:
	SD::TickComponent* CountingIntTick;

	
	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Saves the contents of this Entity to an ini file in the DevAssets diretory.
	 */
	virtual void SaveAsDevAsset (const SD::FileAttributes& configFile) const;

	/**
	 * Initializes this Entity's variables from a DevAsset ini file.
	 */
	virtual void LoadFromDevAsset (const SD::FileAttributes& configFile);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCountingIntTick (SD::Float deltaSec);
};
SD_TESTER_END
#endif