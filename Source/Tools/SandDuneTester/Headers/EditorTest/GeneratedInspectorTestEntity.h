/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspectorTestEntity.h
  The test subject the Generated Inspector uses to edit this Entity's variables.

  This class demonstrates how a developer can use the EditorInterface to generate the inspector and
  handle serialization without the code bloat (unlike the MockEditorTestEntity). 
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ObjTest_ParentClass;

class GeneratedInspectorTestEntity : public SD::Entity, public SD::EditorInterface
{
	DECLARE_CLASS(GeneratedInspectorTestEntity)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EInspectorTestEnum
	{
		ITE_FirstValue,
		ITE_SecondValue,
		ITE_DefaultValue,
		ITE_NoStoneUnturned,
		ITE_Inspector,
		ITE_TestEnumValue,
		ITE_DropdownStuff,
		ITE_LastValue
	};
	DEFINE_DYNAMIC_ENUM_8VAL(EInspectorTestEnum, ITE_FirstValue, ITE_SecondValue, ITE_DefaultValue, ITE_NoStoneUnturned, ITE_Inspector, ITE_TestEnumValue, ITE_DropdownStuff, ITE_LastValue)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of variables the mock editor can modify. */
	SD::Int SimpleInt;
	SD::Int ReadOnlyInt;
	SD::Int PositiveInt;
	SD::Int NegativeInt;
	SD::Int IntBetweenZeroAndOneHundred;

	/* This Entity will continuously increment this number. The editor should be able to write to it while this Entity is being simulated.
	When the editor writes to this variable, it should simply continue counting starting from where it was set to. */
	SD::Int CountingInt;

	SD::Float SimpleFloat;
	SD::Float NoDecimal;
	SD::Float MinNegHundred;
	SD::Float MaxHundred;
	SD::Float ClampedFloat;

	SD::Vector2 SimpleVect2;

	SD::Vector3 SimpleVect3;

	SD::DString SimpleStr;
	SD::DString VowelsOnly;
	SD::DString ShortStr;
	SD::DString MultiLineStr;

	bool SimpleBool;
	bool ReadOnlyBool;

	ObjTest_ParentClass* CreateObjInstance;
	ObjTest_ParentClass* SelectObjInstance;

	EInspectorTestEnum InspectorEnum;

	std::vector<SD::Int> ArrayInt;
	std::vector<SD::Float> ArrayFloat;
	std::vector<SD::DString> ArrayString;
	std::vector<SD::Vector2> ArrayVector2;
	std::vector<SD::Vector3> ArrayVector3;
	std::vector<SD::Int> ArrayReadOnly;
	std::vector<SD::Int> ArrayFixedSize;
	std::vector<SD::Int> ArrayMaxSize;

protected:
	SD::TickComponent* CountingIntTick;

	
	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;

	virtual SD::DString GetEditorObjName () const override;
	virtual void PopulateInspectorList (SD::InspectorList& outList) override;
	virtual void DeleteEditorObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCountingIntTick (SD::Float deltaSec);
};
SD_TESTER_END
#endif