/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditor.h
  A GuiEntity that is a very simple editor that can ony modify an instance of a MockEditorTestEntity.

  This is used for testing the EditPropertyComponents, and showcase a basic example of an editor exporting
  data to a developer asset compared to exporting to a binary file.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class MockEditorTestEntity;

class MockEditor : public SD::GuiEntity
{
	DECLARE_CLASS(MockEditor)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SD::SDFunction<void> OnSwapEditors;

protected:
	/* The instance this editor is modifying. */
	MockEditorTestEntity* TestEntity;

	/* The file attributes that point to the file in the dev asset directory. */
	SD::FileAttributes DevAssetFileAttr;

	/* The file attributes that point to the file in the content directory. */
	SD::FileAttributes ContentFileAttr;

	SD::TickComponent* HideStructCaptionTick;

	SD::FrameComponent* EditorOperationsFrame;
	SD::LabelComponent* EditorDescription;
	SD::ButtonComponent* CreateEntityButton;
	SD::ButtonComponent* LoadFromDevButton;
	SD::ButtonComponent* SaveToDevButton;
	SD::ButtonComponent* LoadFromContentButton;
	SD::ButtonComponent* ExportToContentButton;
	SD::ButtonComponent* SwitchEditorButton;

	SD::ScrollbarComponent* InspectorScrollbar;
	SD::GuiEntity* InspectorEntity;
	SD::VerticalList* InspectorList;

	SD::EditableInt* SimpleIntField;
	SD::EditableInt* ReadOnlyIntField;
	SD::EditableInt* PositiveIntField;
	SD::EditableInt* NegativeIntField;
	SD::EditableInt* IntBetweenZeroAndOneHundredField;
	SD::EditableInt* CountingIntField;

	SD::EditableFloat* SimpleFloatField;
	SD::EditableFloat* NoDecimalField;
	SD::EditableFloat* MinNegHundredField;
	SD::EditableFloat* MaxHundredField;
	SD::EditableFloat* ClampedFloatField;

	SD::EditableStruct* SimpleVectorField;
	SD::EditableStruct* LockedYAxisVect2Field;
	SD::EditableStruct* ClampedXAxisVect2Field;

	SD::EditableStruct* SimpleVector3Field;
	SD::EditableStruct* LockedZAxisVect3Field;

	SD::EditableDString* SimpleStrField;
	SD::EditableDString* VowelsOnlyField;
	SD::EditableDString* ShortStrField;
	SD::EditableDString* MultiLineStrField;
	SD::EditableDString* CaptionTestField;

	SD::EditableBool* SimpleBoolField;
	SD::EditableBool* ReadOnlyBoolField;

	SD::EditableObjectComponent* CreateObjComp;
	SD::EditableObjectComponent* BoundParentComp;
	SD::EditableObjectComponent* BoundChildComp;

	SD::EditableEnum* TestEnumDropdown;

	SD::EditableStruct* CaptionStructUi;
	SD::EditableStruct* NestedStructUi;

	SD::EditableArray* ArrayInt;
	SD::EditableArray* ArrayFloat;
	SD::EditableArray* ArrayString;
	SD::EditableArray* ArrayVector2;
	SD::EditableArray* ArrayVector3;
	SD::EditableArray* ArrayReadOnly;
	SD::EditableArray* ArrayFixedSize;
	SD::EditableArray* ArrayMaxSize;
	SD::EditableArray* ArrayOfObjs;

	/* Scrollbar that visually represents instantiated object tests. This also tests against selecting objects. */
	SD::ScrollbarComponent* SelectObjScrollbar;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Displays the inspector and pairs their the values equal to the loaded mock entity.
	 */
	virtual void PairInspectorToTestEntity ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCreateEntityReleased (SD::ButtonComponent* button);
	virtual void HandleLoadFromDevReleased (SD::ButtonComponent* button);
	virtual void HandleSaveToDevReleased (SD::ButtonComponent* button);
	virtual void HandleLoadFromContentReleased (SD::ButtonComponent* button);
	virtual void HandleExportToContentReleased (SD::ButtonComponent* button);
	virtual void HandleSwitchEditors (SD::ButtonComponent* button);

	virtual void HandleCaptionFieldEdit (SD::EditPropertyComponent* editedComp);
	virtual void HandleHideStructCaption (SD::Float deltaSec);
};
SD_TESTER_END
#endif