/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjTest_ChildClass.h
  A simple editor object used to be a test subject for the EditableObjectComponent.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "ObjTest_ParentClass.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ObjTest_ChildClass : public ObjTest_ParentClass
{
	DECLARE_CLASS(ObjTest_ChildClass)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	bool TestBool;
	ObjTest_ParentClass* OtherParentObj;
	ObjTest_ChildClass* OtherChildObj;

	
	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;
	virtual void PopulateInspectorList (SD::InspectorList& outList) override;

	virtual void SaveToDevAsset (SD::ConfigWriter* config) override;
	virtual void LoadFromDevAsset (SD::ConfigWriter* config) override;
	virtual void SaveToByteBuffer (SD::DataBuffer& outBuffer) override;
	virtual void LoadFromBuffer (const SD::DataBuffer& incomingData) override;
};
SD_TESTER_END
#endif