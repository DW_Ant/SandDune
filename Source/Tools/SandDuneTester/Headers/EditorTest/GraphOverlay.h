/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphOverlay.h
  A GuiEntity that displays all panels that make up the test editor.

  The top panel:
  Contains a row of buttons that allows the user to create a new graph,
  load an existing graph, load a sample (hard coded) graph, save the
  current graph, compile the graph to ContentLogic objects, and run the graph.

  The center panel:
  Contains the SpriteComponent that renders the GraphViewport.

  The bottom left panel:
  Contains UI components that allow the user to create, edit,
  and delete member variables.

  The bottom right panel:
  Contains UI components that allow the user to create, rename,
  and delete member functions. Also clicking on the function names
  will switch the graph to view that function.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class GraphEditorTester;

class GraphOverlay : public SD::GuiEntity
{
	DECLARE_CLASS(GraphOverlay)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The type of assets this tester will open and edit. */
	static const SD::DString ASSET_TYPE;

	SD::FrameComponent* TopPanel;

	SD::ScrollbarComponent* InspectorPanel;
	SD::VerticalList* InspectorList;

	/* List of components responsible for displaying the status of various time-expensive tasks. */
	std::vector<SD::FrameComponent*> StatusComponents;

	/* The label component inside the status bar that is responsible for displaying the asset catalogue progress. */
	SD::LabelComponent* AssetCatalogueStatusLabel;

	/* The label component inside the status bar that is responsible for displaying the reference updater progress. */
	SD::LabelComponent* RefUpdaterStatusLabel;

	SD::GraphViewport* GraphPanel;

	/* Instance to the GraphEditorTester that contain references to the Viewport and GraphEditor. */
	GraphEditorTester* GraphTester;

	/* If not empty, then the GraphOverlay will notify the ReferenceUpdater to replace this string instances to the asset's name. This runs as soon as the user saves the asset. */
	SD::DString OldAssetName;

private:
	/* Vertical spacing between each status bar frame component. */
	SD::Float StatusBarSpacing;

	/* Vertical size for each status bar frame component. */
	SD::Float StatusBarSize;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetGraphTester (GraphEditorTester* newGraphTester);


	/*
	=====================
	  Implementation
	=====================
	*/
	
protected:
	virtual void ConstructTopPanel ();
	virtual void ConstructInspectorPanel ();
	virtual void ConstructGraphPanel ();
	virtual void ConstructStatusPanels ();

	virtual void InitializeRefUpdaterStatusBar ();

	/**
	 * Replaces the class inspector Gui components with a new set of components that show the current EditedAsset's attributes.
	 */
	virtual void RefreshInspector ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleAssetNameChanged (SD::GraphAsset* asset, const SD::DString& oldName, const SD::DString& newName);
	virtual void HandleInvokeRefUpdater (const SD::DString& oldName, const SD::DString& newName);
	virtual void HandleOpenFunction (SD::GraphFunction* func);

	virtual void HandleNewReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleLoadReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleLoadSampleReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleSaveReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleCompileReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleRunReleased (SD::ButtonComponent* uiComponent);

	virtual void HandleAssetCatalogueUpdate (SD::Float percentComplete);
	virtual void HandleAssetCatalogueComplete (const std::vector<SD::FileAttributes>& completeCatalogue);

	virtual void HandleReferenceUpdateProgress (SD::Float percentComplete);
	virtual void HandleReferenceUpdateComplete ();
};
SD_TESTER_END
#endif