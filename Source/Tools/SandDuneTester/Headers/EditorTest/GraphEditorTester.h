/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphEditorTester.h
  Object that acts like a manager for all Entities related to the Graph Editor test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.

  This test launches a UI window that displays a mock graphical editor where the tester
  can place and connect nodes. This editor can save out to ContentLogic objects, and
  simulate running those functions.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class GraphOverlay;

class GraphEditorTester : public WindowedEntityManager
{
	DECLARE_CLASS(GraphEditorTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* The asset instance this editor will be modifying. */
	SD::DPointer<SD::GraphAsset> EditedAsset;

	/* List of all assets that were loaded. It's better to cache loaded assets than to destroy them for the following reasons:
	1. It's faster to reuse loaded assets rather than loading them from file (if loading them multiple times).
	2. Simplifies the issue of assets cross-referencing each other. For example, loading an asset that derives from the previously loaded asset.
	These assets will be destroyed when the tester is destroyed. */
	std::vector<SD::GraphAsset*> LoadedAssets;

	SD::DPointer<GraphOverlay> Overlay;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/
	
public:
	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/* Relinquishes ownership over the old edited asset (it will not destroy the old asset).
	And reassigns the EditedAsset to the given asset. */
	virtual void SetEditedAsset (SD::GraphAsset* newEditedAsset);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::GraphAsset* GetEditedAsset () const
	{
		return EditedAsset.Get();
	}

	inline const std::vector<SD::GraphAsset*>& ReadLoadedAssets () const
	{
		return LoadedAssets;
	}

	inline GraphOverlay* GetOverlay () const
	{
		return Overlay.Get();
	}
};
SD_TESTER_END
#endif