/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjTest_ParentClass.h
  A simple editor object used to be a test subject for the EditableObjectComponent.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SelectObjectRegion;

class ObjTest_ParentClass : public SD::Entity, public SD::PlanarTransform, public SD::EditorInterface
{
	DECLARE_CLASS(ObjTest_ParentClass)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SD::Int TestInt;
	SD::Float TestFloat;
	SD::DString TestString;

protected:
	/* Sprite used for the object selection tests. */
	SD::SpriteComponent* Sprite;

	
	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

	virtual CopiableObjectInterface* CreateCopiableInstanceOfMatchingType () const override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) override;

	virtual SD::DString GetEditorObjName () const override;
	virtual void PopulateInspectorList (SD::InspectorList& outList) override;
	virtual void DeleteEditorObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SaveToDevAsset (SD::ConfigWriter* config);
	virtual void LoadFromDevAsset (SD::ConfigWriter* config);
	virtual void SaveToByteBuffer (SD::DataBuffer& outBuffer);
	virtual void LoadFromBuffer (const SD::DataBuffer& incomingData);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitSprite ();
	virtual void RegisterToSelectRegion ();
};
SD_TESTER_END
#endif