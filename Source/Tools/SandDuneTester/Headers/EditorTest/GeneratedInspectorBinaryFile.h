/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspectorBinaryFile.h
  Simple BinaryFile that serializes and deserializes a GeneratedInspectorTestEntity in binary format.

  This behaves the same as MockEditorTestEntity except for it goes through EditorUtils to handle
  saving/loading.
 =====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class GeneratedInspectorTestEntity;

class GeneratedInspectorBinaryFile : public SD::BinaryFile
{
	DECLARE_CLASS(GeneratedInspectorBinaryFile)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The Entity this object is saving and loading. This object will not take ownership of this object where it
	will not destroy it when this object is destroyed. */
	GeneratedInspectorTestEntity* TestEntity;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual bool ReadContents (const SD::DataBuffer& incomingData) override;
	virtual bool SaveContents (SD::DataBuffer& outBuffer) const override;
};
SD_END
#endif