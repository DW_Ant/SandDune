/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditorBinaryFile.h
  Simple BinaryFile that serializes and deserializes a MockEditorTestEntity in binary format.

  This is to simulate exporting an editor asset to production format.

  This object never takes ownership of other entities. The Mock Entity instances will not be destroyed.

  The format of this binary file is very simple. It assumes only one instance per file.
  And the data for that instance is sorted in order MockEditorTestEntity variables are defined in class.
  For example its:
  1. SimpleInt
  2. ReadOnlyInt
  3. PositiveInt
  4. NegativeInt
  ...
 =====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class MockEditorTestEntity;

class MockEditorBinaryFile : public SD::BinaryFile
{
	DECLARE_CLASS(MockEditorBinaryFile)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The Entity this object is saving and loading. This object will not take ownership of this object where it
	will not destroy it when this object is destroyed. */
	MockEditorTestEntity* TestEntity;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual bool ReadContents (const SD::DataBuffer& incomingData) override;
	virtual bool SaveContents (SD::DataBuffer& outBuffer) const override;
};
SD_END
#endif