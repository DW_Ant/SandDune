/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SelectObjectRegion.h
  This object is responsible for rendering ObjTest_ParentClass object instances.
  The purpose of this class is to provide a simple region where to select objects from.

  Although it's not required to have its own dedicated class for object selection since the 
  screen capture can run at a window-level instead of RenderTexture, this class is created
  so that it can be displayed in a scrollbar without using GuiComponents. In addition to that,
  the test objects need to be rendered somewhere.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ObjTest_ParentClass;

class SelectObjectRegion : public SD::Entity, public SD::PlanarTransform, public SD::ScrollableInterface
{
	DECLARE_CLASS(SelectObjectRegion)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::Float LabelSize;

	/* The number of horizontal pixels each object will occupy. */
	SD::Float IconSize;

	/* Spacing between the label and the object associated with that label. */
	SD::Float LabelSpacing;

	/* Horizontal spacing between the previous object and the next object. */
	SD::Float ObjSpacing;

	/* List of objects to display in this region. This Entity will not take ownership over these objects. */
	std::vector<ObjTest_ParentClass*> RegisteredTestObjs;
	std::vector<SD::LabelComponent*> LabelComps;

	SD::ScrollbarComponent* OwningScrollbar;
	SD::PlanarDrawLayer* DrawLayer;

	
	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void InitializeScrollableInterface (SD::ScrollbarComponent* scrollbarOwner) override;
	virtual SD::ScrollbarComponent* GetScrollbarOwner () const override;
	virtual void RegisterToDrawLayer (SD::ScrollbarComponent* scrollbar, SD::RenderTexture* drawLayerOwner) override;
	virtual void UnregisterFromDrawLayer (SD::RenderTexture* drawLayerOwner) override;
	virtual void GetTotalSize (SD::Vector2& outSize) const override;
	virtual void HandleScrollbarFrameSizeChange (const SD::Vector2& oldFrameSize, const SD::Vector2& newFrameSize) override;
	virtual void ClearScrollbarOwner (SD::ScrollbarComponent* oldScrollbarOwner) override;
	virtual void RemoveScrollableObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void RegisterTestObj (ObjTest_ParentClass* newTestObj);
	virtual void UnregisterTestObj (ObjTest_ParentClass* target);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetLabelSize (SD::Float newLabelSize);
	virtual void SetIconSize (SD::Float newIconSize);
	virtual void SetLabelSpacing (SD::Float newLabelSpacing);
	virtual void SetObjSpacing (SD::Float newObjSpacing);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::Float GetLabelSize () const
	{
		return LabelSize;
	}

	inline SD::Float GetIconSize () const
	{
		return IconSize;
	}

	inline SD::Float GetLabelSpacing () const
	{
		return LabelSpacing;
	}

	inline SD::Float GetObjSpacing () const
	{
		return ObjSpacing;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through each test object to update their transforms to be displayed horizontally.
	 * This also creates labels associated with each object.
	 */
	virtual void ReconstructObjectRegion (size_t startUpdateIdx);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleObjDestroyed (SD::DestroyNotifyComponent* destroyComp);
};
SD_TESTER_END
#endif