/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspector.h
  This Entity has similar functionality as the MockEditor with the exception that it
  leverages the EditorUtils to generate the Inspector.

  Unlike the MockEditor, this is a good demonstration in constructing an Inspector with minimal code
  since it uses the interface.

  The overlap in functionality between MockEditor and GeneratedInspector is intended to
  reveal the differences between the two cases.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class GeneratedInspectorTestEntity;

class GeneratedInspector : public SD::GuiEntity
{
	DECLARE_CLASS(GeneratedInspector)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SD::SDFunction<void> OnSwapEditors;

protected:
	/* The instance this editor is modifying. */
	GeneratedInspectorTestEntity* TestEntity;

	/* The file attributes that point to the file in the dev asset directory. */
	SD::FileAttributes DevAssetFileAttr;

	/* The file attributes that point to the file in the content directory. */
	SD::FileAttributes ContentFileAttr;

	SD::FrameComponent* EditorOperationsFrame;
	SD::LabelComponent* EditorDescription;
	SD::ButtonComponent* CreateEntityButton;
	SD::ButtonComponent* LoadFromDevButton;
	SD::ButtonComponent* SaveToDevButton;
	SD::ButtonComponent* LoadFromContentButton;
	SD::ButtonComponent* ExportToContentButton;
	SD::ButtonComponent* SwitchEditorButton;

	SD::ScrollbarComponent* InspectorScrollbar;
	SD::GuiEntity* InspectorEntity;

	/* Scrollbar that visually represents instantiated object tests. This also tests against selecting objects. */
	SD::ScrollbarComponent* SelectObjScrollbar;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reconstructs the inspector list using the Editor Utilities.
	 */
	virtual void InitializeInspector ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCreateEntityReleased (SD::ButtonComponent* button);
	virtual void HandleLoadFromDevReleased (SD::ButtonComponent* button);
	virtual void HandleSaveToDevReleased (SD::ButtonComponent* button);
	virtual void HandleLoadFromContentReleased (SD::ButtonComponent* button);
	virtual void HandleExportToContentReleased (SD::ButtonComponent* button);
	virtual void HandleSwitchEditors (SD::ButtonComponent* button);
};
SD_TESTER_END
#endif