/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FeatureTestLauncher.h
  Pops up a window that allows the user to launch individual unit tests that typically
  have their own display or something that requires attention to run.

  This is for tests that are not running in an automated way. Yet it prevents the
  SandDuneTester from launching dozens of windows at start.
=====================================================================
*/

#pragma once

#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class Box2dTester;
class CurveTester;
class EditorTester;
class GraphEditorTester;
class InputTester;
class PhysicsTester;
class QaUnitTester;
class ScreenCaptureTester;
class SceneTester;
class ShaderTester;
class SpriteInstanceTester;
class SpriteTester;

class FeatureTestLauncher : public WindowedEntityManager
{
	DECLARE_CLASS(FeatureTestLauncher)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various tests this object has launched. */
	Box2dTester* Box2dTester;
	CurveTester* CurveTester;
	EditorTester* EditorTest;
	GraphEditorTester* GraphEditorTest;
	InputTester* InputTest;
	PhysicsTester* PhysicsTest;
	SceneTester* SceneTester;
	ScreenCaptureTester* ScreenCaptureTester;
	ShaderTester* ShaderTester;
	SpriteInstanceTester* SpriteInstTester;
	SpriteTester* SpriteTester;

	/* Simple UI that has individual buttons for launching a single test.
	If this GuiEntity becomes complicated enough, may want to consider using a custom class. */
	SD::GuiEntity* LauncherUi;

	SD::ButtonComponent* Box2dTestButton;
	SD::ButtonComponent* CurveTestButton;
	SD::ButtonComponent* EditorTesterButton;
	SD::ButtonComponent* GraphEditorTesterButton;
	SD::ButtonComponent* InputTesterButton;
	SD::ButtonComponent* PhysicsTesterButton;
	SD::ButtonComponent* SceneTestButton;
	SD::ButtonComponent* ScreenCaptureTestButton;
	SD::ButtonComponent* ShaderTesterButton;
	SD::ButtonComponent* SpriteInstanceButton;
	SD::ButtonComponent* SpriteTestButton;

	/* Object instance that kicked off this object. It's read only since usually CDOs initialize this test. */
	const QaUnitTester* QaTester;

	/* Unit test flags that were used when initializing the launcher. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* If true, then this launcher will shutdown the local engine as soon as this is destroyed. */
	bool ShutdownEngineOnDestruction;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes all Entities that are needed to setup the launcher.
	 */
	virtual void InitializeLauncher (const QaUnitTester* launcher, SD::UnitTester::EUnitTestFlags testFlags, bool shutdownOnDestruction);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes all components for the LauncherUi.
	 */
	virtual void InitializeLauncherUi ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLaunchBox2dTester (SD::ButtonComponent* uiComponent);
	virtual void HandleBox2dTesterTerminated ();

	virtual void HandleLaunchCurveTester (SD::ButtonComponent* uiComponent);
	virtual void HandleCurveTesterTerminated ();

	virtual void HandleLaunchEditorTester (SD::ButtonComponent* uiComponent);
	virtual void HandleEditorTesterTerminated ();

	virtual void HandleLaunchGraphEditorTester (SD::ButtonComponent* uiComponent);
	virtual void HandleGraphEditorTesterTerminated ();

	virtual void HandleLaunchInputTester (SD::ButtonComponent* uiComponent);
	virtual void HandleInputTesterTerminated ();

	virtual void HandleLaunchPhysicsTester (SD::ButtonComponent* uiComponent);
	virtual void HandlePhysicsTesterTerminated ();

	virtual void HandleLaunchSceneTester (SD::ButtonComponent* uiComponent);
	virtual void HandleSceneTesterTerminated ();

	virtual void HandleLaunchScreenCaptureTester (SD::ButtonComponent* uiComponent);
	virtual void HandleScreenCaptureTesterTerminated ();

	virtual void HandleLaunchShaderTester (SD::ButtonComponent* uiComponent);
	virtual void HandleShaderTesterTerminated ();

	virtual void HandleLaunchSpriteInstanceTester (SD::ButtonComponent* uiComponent);
	virtual void HandleSpriteInstanceTesterTerminated ();

	virtual void HandleLaunchSpriteTester (SD::ButtonComponent* uiComponent);
	virtual void HandleSpriteTesterTerminated ();

	virtual void HandleCloseButtonReleased (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif