/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveTestPlotter.h
  A GuiEntity that allows the user to draw curves.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class CurveTester;

class CurveTestPlotter : public SD::GuiEntity
{
	DECLARE_CLASS(CurveTestPlotter)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum EDrawMode
	{
		DM_Free,
		DM_Bezier
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	EDrawMode DrawMode;

	/* Becomes true if the user is currently dragging the mouse within the DrawFrame. */
	bool bIsDragging;

	/* When drawing a bezier curve, these are the points used to generate the curve. */
	SD::Vector2 BezierStart;
	SD::Vector2 BezierEnd;
	SD::Vector2 BezierCtrl1;
	SD::Vector2 BezierCtrl2;

	/* The bezier point the user is currently dragging around. */
	SD::Vector2* CtrledPt;

	/* When controlling any of the control points, the bezier curve will generate at a low resolution. */
	SD::Float BezierPreviewSegment;
	SD::Float BezierPreviewStep;

	/* User specified values for the curve component. */
	SD::Float CurveThickness;
	SD::Float BezierSegment;
	SD::Float BezierStep;

	SD::FrameComponent* LeftPanel;

	SD::TextFieldComponent* CurveThickField;
	SD::CheckboxComponent* TextureCheckbox;
	SD::TextFieldComponent* RedField; //CHRIS!!!!!!!! -Wesker
	SD::TextFieldComponent* GreenField;
	SD::TextFieldComponent* BlueField;

	SD::ButtonComponent* FreeDrawButton;
	SD::ButtonComponent* BezierDrawButton;

	SD::TextFieldComponent* SegLengthField;
	SD::TextFieldComponent* StepField;

	SD::ButtonComponent* ClearLineButton;

	//Frame component where the user can draw the curves.
	SD::FrameComponent* DrawFrame;

	SD::CurveRenderComponent* CurveComp;

	/* Curve component used to indicate where the user is drawing. */
	SD::CurveRenderComponent* CurveDrawHelper;

private:
	/* Texture to apply to the CurveComponents if TextureCheckbox component is set. */
	SD::DPointer<const SD::Texture> CurveTexture;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove) override;
	virtual void HandlePassiveMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool HandleConsumableMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;


	/*
	=====================
	  Implementation
	=====================
	*/
	
protected:
	virtual void ConstructLeftPanel ();
	virtual void ConstructDrawPanel ();

	/**
	 * Returns a Vector relative to the CurveRenderComponent based on the given mouse position.
	 */
	virtual SD::Vector2 GetCurveCoordinates (SD::MousePointer* mouse);

	virtual void ResetLine ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleAllowNumericInput (const SD::DString& txt);

	virtual void HandleAcceptCurveThickness (SD::TextFieldComponent* uiComponent);
	virtual void HandleTextureChecked (SD::CheckboxComponent* checkbox);
	virtual void HandleAcceptRed (SD::TextFieldComponent* uiComponent);
	virtual void HandleAcceptGreen (SD::TextFieldComponent* uiComponent);
	virtual void HandleAcceptBlue (SD::TextFieldComponent* uiComponent);
	virtual void HandleAcceptSegLength (SD::TextFieldComponent* uiComponent);
	virtual void HandleAcceptStep (SD::TextFieldComponent* uiComponent);

	virtual void HandleFreeDrawReleased (SD::ButtonComponent* button);
	virtual void HandleBezierDrawReleased (SD::ButtonComponent* button);
	virtual void HandleClearLineReleased (SD::ButtonComponent* button);
};
SD_TESTER_END
#endif