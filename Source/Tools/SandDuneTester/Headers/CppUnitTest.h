/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CppUnitTest.h
  Constructs several simple C++ classes, and conducts unit tests to see if the current platform
  can reproduce the expected C++ behavior.
=====================================================================
*/

#pragma once

#include <cassert>

#define CHECK_TRUE(condition) \
	assert(condition);

#define CHECK_FALSE(condition) \
	assert(!(condition));

class CppUnitTestLauncher
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static void LaunchCppUnitTests ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	static void TestComparisons ();
	static void TestSizes ();
	static void TestOperations ();
	static void TestInheritance ();
	static void TestCasting ();
};

class CppUnitTest_BaseClass
{
public:
	virtual int GetNum () const;
};

class CppUnitTest_ProtectedInterface
{
protected:
	virtual int GetProtectedInterfaceNum () const = 0;
};

class CppUnitTest_PublicInterface
{
public:
	virtual int GetPublicInterfaceNum () const = 0;
};

class CppUnitTest_SubClass : public CppUnitTest_BaseClass, public CppUnitTest_PublicInterface, protected CppUnitTest_ProtectedInterface
{
private:
	int PrivateVar;

public:
	virtual int GetNum () const override;
	virtual int GetPublicInterfaceNum () const override;

protected:
	virtual int GetProtectedInterfaceNum () const override;
};