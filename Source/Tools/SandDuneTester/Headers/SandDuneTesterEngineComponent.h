/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDuneTesterEngineComponent.h
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

SD_TESTER_BEGIN
class SandDuneTesterEngineComponent : public SD::EngineComponent
{
	DECLARE_ENGINE_COMPONENT(SandDuneTesterEngineComponent)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool CanRunPreInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const override;
	virtual bool CanRunInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const override;
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports various utilized assets (like textures) to their corresponding asset pools.
	 */
	virtual void ImportAssets ();
};
SD_TESTER_END