/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTesterHud.h

  GuiEntity that's responsible for displaying text on the render target to notify the user
  of the controls.

  This also contains a few UI controls to manipulate objects and the test, itself.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SceneTester;
class Player;

class SceneTesterHud : public SD::GuiEntity
{
	DECLARE_CLASS(SceneTesterHud)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::TickComponent* HudTick;

	SD::FrameComponent* MenuFrame;
	SD::ButtonComponent* ToggleHelpButton;
	SD::ButtonComponent* ToggleAttributesButton;
	SD::ButtonComponent* ToggleTestDescriptionButton;

	/* Controls related to manipulating the player and camera. */
	SD::FrameComponent* ControlsFrame;
	SD::LabelComponent* PlayerLabel;
	SD::LabelComponent* PlayerMovementLabel;
	SD::LabelComponent* PlayerSizeLabel;
	SD::FrameComponent* MovePlayerFrame;
	SD::ButtonComponent* MovePlayerUpButton;
	SD::ButtonComponent* MovePlayerRightButton;
	SD::ButtonComponent* MovePlayerDownButton;
	SD::ButtonComponent* MovePlayerLeftButton;
	SD::ButtonComponent* IncreasePlayerSizeButton;
	SD::ButtonComponent* DecreasePlayerSizeButton;

	SD::LabelComponent* CameraLabel;
	SD::LabelComponent* CameraMovementLabel;
	SD::LabelComponent* CameraZoomLabel;
	SD::FrameComponent* MoveCameraFrame;
	SD::ButtonComponent* MoveCameraUpButton;
	SD::ButtonComponent* MoveCameraRightButton;
	SD::ButtonComponent* MoveCameraDownButton;
	SD::ButtonComponent* MoveCameraLeftButton;
	SD::ButtonComponent* ZoomInButton;
	SD::ButtonComponent* ZoomOutButton;

	/* Controls related to displaying Entity data. */
	SD::FrameComponent* EntityDataFrame;
	SD::LabelComponent* PlayerTransformLabel;
	SD::LabelComponent* CameraTransformLabel;

	/* Label that displays the purpose of this test. */
	SD::GuiEntity* TestDescriptionEntity;
	SD::ScrollbarComponent* TestDescriptionScrollbar;
	SD::LabelComponent* TestDescriptionLabel;

	/* The test responsible for creating this Hud Entity. */
	SceneTester* TestOwner;

	/* The Entity this UI element will display attributes for. */
	Player* PlayerTester;

	/* The camera this UI element may manipulate and display attributes for. */
	SD::TopDownCamera* SceneTesterCamera;

private:
	/* Cache the translated text for the Player and Camera transforms since these strings will be referenced frequently. */
	SD::DString TranslatedPlayerTransform;
	SD::DString TranslatedCameraTransform;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void LinkUpEntities (SceneTester* testOwner, Player* playerTester, SD::TopDownCamera* sceneTesterCamera);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Calculates the menu components' positions based on what's visible.
	 */
	virtual void RecomputePositions ();

	/**
	 * Updates the transform labels to reflect the absolute coordinates of the player and camera transforms.
	 */
	virtual void UpdateEntityTransforms ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleHudTick (SD::Float deltaSec);

	virtual void HandleToggleHelpButtonReleased (SD::ButtonComponent* button);
	virtual void HandleToggleAttributesButtonReleased (SD::ButtonComponent* button);
	virtual void HandleToggleDescriptionButtonReleased (SD::ButtonComponent* button);

	virtual void HandlePlayerMoveUpButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveUpButtonReleased (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveRightButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveRightButtonReleased (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveDownButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveDownButtonReleased (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveLeftButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerMoveLeftButtonReleased (SD::ButtonComponent* button);
	virtual void HandlePlayerIncreaseSizeButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerIncreaseSizeButtonReleased (SD::ButtonComponent* button);
	virtual void HandlePlayerDecreaseSizeButtonPressed (SD::ButtonComponent* button);
	virtual void HandlePlayerDecreaseSizeButtonReleased (SD::ButtonComponent* button);

	virtual void HandleCameraMoveUpButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraMoveUpButtonReleased (SD::ButtonComponent* button);
	virtual void HandleCameraMoveRightButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraMoveRightButtonReleased (SD::ButtonComponent* button);
	virtual void HandleCameraMoveDownButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraMoveDownButtonReleased (SD::ButtonComponent* button);
	virtual void HandleCameraMoveLeftButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraMoveLeftButtonReleased (SD::ButtonComponent* button);
	virtual void HandleCameraZoomInButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraZoomInButtonReleased (SD::ButtonComponent* button);
	virtual void HandleCameraZoomOutButtonPressed (SD::ButtonComponent* button);
	virtual void HandleCameraZoomOutButtonReleased (SD::ButtonComponent* button);
};
SD_TESTER_END

#endif