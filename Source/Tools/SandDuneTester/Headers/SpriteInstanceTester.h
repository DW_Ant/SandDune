/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteInstanceTester.h
  Simple object that manages all Entities that are part of the SpriteInstance test.
=====================================================================
*/

#pragma once

#include "WindowedEntityManager.h"
#include "SpriteInstanceTesterUi.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class QaUnitTester;

class SpriteInstanceTester : public WindowedEntityManager, public SD::PlanarTransform
{
	DECLARE_CLASS(SpriteInstanceTester)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPaintProperties
	{
	public:
		/* Determines how frequently the brush will update the Density Map texture while painting. */
		SD::Float UpdateRate;

		/* Determines the rate the colors on the density map (at the center of the brush) transitions
		from solid white to solid black and vise versa (in seconds). */
		SD::Float PaintPerSecond;

		/* The sprites' alpha channel value whenever the brush is painting the density map. */
		uint8 AlphaWhenPainting;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Number of texels that make up the Density Map texture. */
	static const SD::Int DENSITY_MAP_WIDTH;
	static const SD::Int DENSITY_MAP_HEIGHT;

	/* Delegate to invoke whenever this test is finished and destroyed. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	SD::DPointer<SpriteInstanceTesterUi> Ui;
	SD::DPointer<SD::InstancedSpriteComponent> SpriteInstance;
	SD::DPointer<SD::PlanarTransformComponent> BrushTransform;
	SD::DPointer<SD::SpriteComponent> BrushSprite;
	SD::DPointer<SD::Texture> DensityMap;
	SD::DPointer<SD::PlanarTransformComponent> DensityMapTransform;
	SD::DPointer<SD::SpriteComponent> DensityMapSprite;
	SD::DPointer<SD::InputComponent> BrushInput;

	/* Becomes true if the brush is actually manipulating the sprite instance components. */
	bool IsLPainting; //True when left mouse button held down
	bool IsRPainting; //True when right mouse button held down

	SPaintProperties PaintProperties;

	/* Becomes true if this Entity is overriding the mouse pointer icon. */
	bool IsOverwritingMouseIcon;

	/* Texture to use when overriding the mouse icon. */
	SD::Texture* MouseIconTexture;

	/* Object instance that kicked off this object. It's read only since usually CDOs initialize this test. */
	const QaUnitTester* QaTester;

	/* Unit test flags that were used when initializing the launcher. */
	SD::UnitTester::EUnitTestFlags TestFlags;

private:
	/* The image that contains the raw density map data. This is cached to prevent excessive image copying whenever the user is painting. */
	sf::Image DensityMapImage;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes all Entities that are needed to setup the launcher.
	 */
	virtual void InitializeTest (const QaUnitTester* launcher, SD::UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::InstancedSpriteComponent* GetSpriteInstance () const
	{
		return SpriteInstance.Get();
	}

	inline SD::PlanarTransformComponent* GetBrushTransform () const
	{
		return BrushTransform.Get();
	}

	inline SD::SpriteComponent* GetBrushSprite () const
	{
		return BrushSprite.Get();
	}

	inline SD::Texture* GetDensityMap () const
	{
		return DensityMap.Get();
	}

	inline SD::PlanarTransformComponent* GetDensityMapTransform () const
	{
		return DensityMapTransform.Get();
	}

	inline SD::SpriteComponent* GetDensityMapSprite () const
	{
		return DensityMapSprite.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Manipulates/draws on the Density Map based on the mouse click flags, position, and the draw rate.
	 */
	virtual void ManipulateDensityTexture (SD::Float deltaSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::Float deltaSec);
	virtual void HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt, const SD::Vector2& deltaMove);
	virtual bool HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& evnt, sf::Event::EventType evntType);
	virtual bool HandleMouseIconOverride (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt);
};
SD_TESTER_END

#endif