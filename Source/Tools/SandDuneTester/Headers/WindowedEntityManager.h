/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  WindowedEntityManager.h
  The SandDuneTester application has numerous unit tests that needs to launch in their
  own isolated windows with their own input handlers and draw layers.

  This Entity is responsible for managing these objects and properly shutting them down through
  destruction or through window closures.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class WindowedEntityManager : public SD::Entity
{
	DECLARE_CLASS(WindowedEntityManager)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::DPointer<SD::Window> WindowHandle;
	SD::DPointer<SD::GuiDrawLayer> GuiDrawLayer;
	SD::DPointer<SD::GuiDrawLayer> GuiForegroundLayer;
	SD::PlanarDrawLayer* MainDrawLayer;
	SD::MousePointer* Mouse;
	SD::DPointer<SD::InputBroadcaster> Input;

private:
	bool isInitialized;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes all base Entities for this manager.
	 */
	virtual void InitializeEntityManager (const SD::Vector2& windowSize, const SD::DString& windowName, SD::Float windowUpdateRate = 0.0333f /*~30 fps*/);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::Window* GetWindowHandle () const
	{
		return WindowHandle.Get();
	}

	inline SD::GuiDrawLayer* GetGuiDrawLayer () const
	{
		return GuiDrawLayer.Get();
	}

	inline SD::InputBroadcaster* GetInput () const
	{
		return Input.Get();
	}

	inline bool IsInitialized () const
	{
		return isInitialized;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWindowEvent (const sf::Event& windowEvent);
};
SD_TESTER_END

#endif