/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDuneTesterTheme.h

  Theme used to configure most of the UI elements the unit tests will utilize.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

SD_TESTER_BEGIN
class SandDuneTesterTheme : public SD::GuiTheme
{
	DECLARE_CLASS(SandDuneTesterTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const SD::DString SAND_DUNE_TESTER_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;
};
SD_TESTER_END