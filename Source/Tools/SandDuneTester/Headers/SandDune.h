/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDune.h
  This file includes all enabled modules that makes up the SandDune engine.
=====================================================================
*/

#pragma once

#include "Engine\Box2d\Headers\Box2dClasses.h"
#include "Engine\ContentLogic\Headers\ContentLogicClasses.h"
#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Editor\Headers\EditorClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#include "Engine\MultiThread\Headers\MultiThreadClasses.h"
#include "Engine\Physics\Headers\PhysicsClasses.h"
#include "Engine\Plugin\Headers\PluginClasses.h"
#include "Engine\PubSub\Headers\PubSubClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"
#include "Engine\Sound\Headers\SoundClasses.h"
#include "Engine\Time\Headers\TimeClasses.h"