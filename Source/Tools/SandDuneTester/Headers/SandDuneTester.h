/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDuneTester.h
=====================================================================
*/

#pragma once

#include "SandDune.h"

#define SD_TESTER_BEGIN namespace SDTester {
#define SD_TESTER_END }

#define TICK_GROUP_SD_TESTER "SdTester"
#define TICK_GROUP_PRIORITY_SD_TESTER 1200

#define FEATURE_ENGINE_IDX 4

SD_TESTER_BEGIN
extern SD::LogCategory SdTesterLog;
SD_TESTER_END

/**
 * Instantiates and returns a list of every Engine Component that'll be used for testing.
 * NOTE: This function actually creates engine components on the heap. If these aren't used to
 * initialize Engine instances, then these will have to be manually deleted.
 */
void GetEngineComponentList (std::vector<SD::EngineComponent*>& outEngineComponents);