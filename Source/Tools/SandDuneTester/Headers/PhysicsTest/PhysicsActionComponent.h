/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsActionComponent.h
  A component that allows the owning Entity to perform an action to test a physics utility.
  In addition to that, this component may create a hud element that corresponds with this action.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsTester;

class PhysicsActionComponent : public SD::EntityComponent
{
	DECLARE_CLASS(PhysicsActionComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines what collision channels are relevant with this action. */
	SD::Int CollisionChannels;

	/* The class of the GuiEntity to instantiate whenever this component initializes. */
	const SD::DClass* GuiClass;

	/* The GuiEntity instance created and associated with this component. */
	SD::GuiEntity* GuiInstance;

	PhysicsTester* Tester;
	SD::InputComponent* Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Associates this component with the given tester, and creates the GuiEntity with its information.
	 */
	virtual void InitializePhysicsAction (PhysicsTester* newTester);


	/*
	=====================
	  Mutator
	=====================
	*/

public:
	virtual void SetCollisionChannels (SD::Int newCollisionChannels);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::Int GetCollisionChannels () const
	{
		return CollisionChannels;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Attempts to create and initialize the GuiEntity instance.
	 */
	virtual void InitializeGuiInstance ();

	/**
	 * Perform the physics action.
	 * @param actionPos The position where the user clicked their mouse. Typically the action is performed around this spot.
	 */
	virtual void DoAction (const SD::Vector3& actionPos) = 0;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& evnt, sf::Event::EventType type);
};
SD_TESTER_END
#endif