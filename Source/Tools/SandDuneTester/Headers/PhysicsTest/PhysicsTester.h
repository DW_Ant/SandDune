/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsTester.h
  Object that acts like a manager for all Entities related to the Physics test.

  All Entities related to this test will be purged as soon as this Entity is destroyed.

  The PhysicsTester essentially runs a simulation where the user controls a character.
  The character only moves via Velocity and Acceleration.

  The scene they're placed is segmented in multiple rooms where each room tests various
  elements using the Physics module.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsMap;
class PhysicsPlayer;

class PhysicsTester : public WindowedEntityManager
{
	DECLARE_CLASS(PhysicsTester)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum ECollisionChannels
	{
		CC_Environment = 1,
		CC_Character = 2,
		CC_RedChannel = 4,
		CC_GreenChannel = 8,
		CC_BlueChannel = 16
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the WindowDisplay Entity. */
	static const SD::Vector2 WINDOW_SIZE;

	/* Function to invoke whenever this test terminates. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	/* The unit test flags used when this test was launched. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* Camera used to render the Entities in the scene. */
	SD::DPointer<SD::TopDownCamera> SceneCamera;

	SD::DPointer<SD::SceneDrawLayer> SceneLayer;

	/* Used to visualize the Physics' QuadTree. */
	SD::QuadTreeVisualizer* QTreeVisualizer;

	PhysicsMap* Map;
	SD::DPointer<PhysicsPlayer> Player;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a color that corresponds to the three color channels.
	 */
	static SD::Color CalcChannelColor (SD::Int collisionChannels);

	/**
	 * Creates all Entities and initializes them to begin this test.
	 */
	virtual void LaunchTest (SD::UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::TopDownCamera* GetSceneCamera () const
	{
		return SceneCamera.Get();
	}

	inline SD::SceneDrawLayer* GetSceneLayer () const
	{
		return SceneLayer.Get();
	}

	inline PhysicsPlayer* GetPlayer () const
	{
		return Player.Get();
	}
};

DEFINE_ENUM_FUNCTIONS(PhysicsTester::ECollisionChannels);
SD_TESTER_END

#endif