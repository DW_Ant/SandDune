/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SweepTester.h
  A component that allows the user to perform sweeps.
=====================================================================
*/

#pragma once

#include "PhysicsActionComponent.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SweepTester : public PhysicsActionComponent
{
	DECLARE_CLASS(SweepTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	
protected:
	virtual void InitializeGuiInstance () override;
	virtual void DoAction (const SD::Vector3& actionPos) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Performs the actual sweep and draw a line to visualize it.
	 */
	virtual void PerformSweep (const SD::Vector3& startSweep, const SD::Vector3& endSweep);
};
SD_TESTER_END
#endif