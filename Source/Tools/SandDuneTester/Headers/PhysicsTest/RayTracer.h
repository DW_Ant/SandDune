/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RayTracer.h
  A component that allows the user to perform ray traces.
=====================================================================
*/

#pragma once

#include "PhysicsActionComponent.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class RayTracer : public PhysicsActionComponent
{
	DECLARE_CLASS(RayTracer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines the maximum number of times these ray traces may reflect. */
	SD::Int MaxReflections;

	/* When colliding against Bouncing Shapes, this is the amount of force that is applied against the shape. */
	SD::Float ImpactForce;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	
protected:
	virtual void InitializeGuiInstance () override;
	virtual void DoAction (const SD::Vector3& actionPos) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Performs the ray traces and draw lines representing their results.
	 */
	virtual void PerformTraces (const SD::Vector3& startTrace, const SD::Vector3& endTrace);
};
SD_TESTER_END
#endif