/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsPlayer.h
  An entity the user can control. The input commands do not directly adjust this
  Entity's transform. Instead it uses its PhysicsComponent to adjust its velocity.

  This Entity acts like a roaming entity that can collide against others and detect
  when it's overlapping other Entities.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsVisualizer;

class PhysicsPlayer : public SD::SceneEntity
{
	DECLARE_CLASS(PhysicsPlayer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The acceleration magnitude when holding an input key down. */
	SD::Float MovementAcceleration;

	/* The maximum velocity the player can achieve through the movement keys. */
	SD::Float MaxVelocity;

	SD::DPointer<SD::PhysicsComponent> Physics;
	SD::DPointer<SD::InputComponent> Input;
	SD::TickComponent* Tick;
	SD::DPointer<PhysicsVisualizer> Visualizer;

private:
	/* Various input flags used to detect when a key is pressed and released. */
	bool bUpPressed : 2;
	bool bRightPressed : 2;
	bool bDownPressed : 2;
	bool bLeftPressed : 2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::PhysicsComponent* GetPhysics () const
	{
		return Physics.Get();
	}

	inline SD::InputComponent* GetInput () const
	{
		return Input.Get();
	}

	inline PhysicsVisualizer* GetVisualizer () const
	{
		return Visualizer.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& evnt);
	virtual void HandleTick (SD::Float deltaSec);
};
SD_TESTER_END

#endif