/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsVisualizer.h
  A component responsible for rendering shapes that mirror the owning PhysicsComponent's
  shape(s).

  This component assumes top-down projection due to the chosen camera selection for the PhysicsTest.

  This component must attach to a SceneTransform Entity that also contain at least one PhysicsComponent.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsVisualizer : public SD::RenderComponent
{
	DECLARE_CLASS(PhysicsVisualizer)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SRenderShape
	{
		sf::Shape* Shape;
		SD::Vector2 ShapeOffset;
		bool bIsAabb;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this component will stop ticking as soon as it updates. */
	bool bUpdateOnceOnly;

protected:
	static const SD::Color AABB_COLOR;

	/* If true, then this component will render the shape's Aabb. */
	static const bool bDEBUG_AABB;

	/* Determines how frequent the render shapes should update (in seconds). */
	SD::Float ShapeUpdateFrequency;

	std::vector<SRenderShape> RenderShapes;
	SD::TickComponent* Tick;

	/* If positive, then this is the time stamp when the visualizer flashed its colors (in seconds). */
	SD::Float FlashStartTime;

	/* The number of seconds it takes to lerp between colors when flashing. */
	SD::Float FlashDuration;

	sf::Color FlashFrom;
	sf::Color FlashTo;

	SD::TickComponent* FlashTick;

private:
	bool bColorOverride;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Render (SD::RenderTarget* renderTarget, const SD::Camera* camera) override;
	virtual SD::Aabb GetAabb () const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void OverrideColor (SD::Color newColor);
	virtual void FlashColor (SD::Float flashDuration);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetShapeUpdateFrequency (SD::Float newShapeUpdateFrequency);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates and destroys all render shapes.
	 */
	virtual void ClearShapes ();

	/**
	 * Iterates through the owner's PhysicsComponent to update its render shapes.
	 */
	virtual void UpdateShapes ();

	/**
	 * Modifies the given rectangle shape object so that its ends connect the two vertices. Vertices are coordinates relative to this component.
	 */
	virtual void ConnectRectToVerts (sf::RectangleShape& outRect, const SD::Vector2& ptA, const SD::Vector2& ptB) const;

	/**
	 * Returns a color value based on the color value.
	 */
	virtual sf::Color GetColor (SD::Int collisionChannel);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::Float deltaSec);
	virtual void HandleFlashTick (SD::Float deltaSec);
};
SD_TESTER_END
#endif