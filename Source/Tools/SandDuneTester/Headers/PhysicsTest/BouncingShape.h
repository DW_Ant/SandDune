/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BouncingShape.h
  A simple moving entity that essentially changes acceleration and velocity every time it collides
  against something.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class BouncingShape : public SD::SceneEntity
{
	DECLARE_CLASS(BouncingShape)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Velocity magnitude after a collision. */
	SD::Float BaseVelocityMagnitude;

	/* The list of acceleration vectors to cycle through after a collision event. */
	std::vector<SD::Vector3> AccelerationList;

	/* Index in the Acceleration list that is currently being applied. */
	size_t AccelerationIdx;

	SD::DPointer<SD::PhysicsComponent> PhysComp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetBaseVelocityMagnitude (SD::Float newBaseVelocityMagnitude);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::PhysicsComponent* GetPhysComp () const
	{
		return PhysComp.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBlocked (SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp);
};
SD_TESTER_END
#endif