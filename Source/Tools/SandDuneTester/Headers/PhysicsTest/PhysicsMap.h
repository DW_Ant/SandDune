/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsMap.h
  An Entity responsible for instantiating a series of objects that are to represent the
  physics test environment.

  When destroyed, all Entities spawned from this object will also be destroyed.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsTester;

class PhysicsMap : public SD::Entity
{
	DECLARE_CLASS(PhysicsMap)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::DString LocalizationFileName;
	SD::DString SectionName;

	/* For the complex collision test, this determines how frequently the moving shapes changes directions (in seconds). */
	SD::Float VerticalChangeDirectionFrequency;

	/* List of Entities to destroy when this entity is destroyed. */
	std::vector<Entity*> MapObjects;

	/* For the complex collisin test, this is the list of PhysicsComponents that should move vertically up and down every few seconds. */
	std::vector<SD::PhysicsComponent*> VertMovingComps;

	PhysicsTester* Tester;

private:
	/* List of PhysicsComponents that map to a PhysicsActionComponent. */
	std::vector<std::pair<SD::PhysicsComponent*, const SD::DClass*>> ActionComponents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Instantiates a collection of objects to represent the physics environment.
	 */
	virtual void InitializeMapObjects (PhysicsTester* tester);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick utility that creates an Entity responsible for rendering the given text.
	 */
	virtual SD::Entity* CreateTextEntity (const SD::Vector3& topLeftPos, const SD::DString& text, SD::Font* font);

	/**
	 * Creates an Entity that is responsible for changing the player's shape whenever they intersect with it.
	 * Returns the PhysicsComponent that's attached to the created Entity.
	 * @param newShapeInstance is the function callback responsible to instantiate a new shape.
	 */
	virtual SD::PhysicsComponent* CreateShapeChanger (const SD::Vector3& position, std::function<SD::CollisionShape*()> newShapeInstance);

	/**
	 * Creates a collection of bouncing spheres within the specified room.
	 */
	virtual void AddBouncingSpheres (const SD::Vector3& roomCenter, const SD::Vector3& roomDimensions, SD::Int numSpheres);

	/**
	 * Creates a SceneEntity that provides a PhysicsActionComponent only when the player overlaps with the room.
	 */
	virtual void CreatePhysActionObject (const SD::Vector3& roomCenter, const SD::Vector3& dimensions, const SD::DClass* componentClass);

	/**
	 * Create Entities that are responsible for the collision channel test room.
	 */
	virtual void CreateCollisionChannelRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);

	/**
	 * Create Entities that are responsible for the complex collision test room.
	 */
	virtual void CreateComplexCollisionRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);

	/**
	 * Creates Entities that are responsible for the AoE test room.
	 */
	virtual void CreateAoERoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);

	/**
	 * Create Entities that are responsible for the moving object test room.
	 */
	virtual void CreateMovingObjectRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);

	/**
	 * Creates Entities that are responsible for the ray tracing test room.
	 */
	virtual void CreateRayTraceRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);

	/**
	 * Creates Entities that make up the sweeping test room.
	 */
	virtual void CreateSweepRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBeginCollisionChannelOverlap (SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp);
	virtual void HandleTickMoveObjectVertically (SD::Float deltaSec);
};
SD_TESTER_END
#endif