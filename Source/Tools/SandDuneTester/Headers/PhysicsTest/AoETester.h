/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AoETester.h
  A component that allows the user to perform AoE tests. It can perform the following:
  * Find all PhysicsComponents that encompass a point in space.
  * Find all PhysicsComponents that overlap a spherical region.
=====================================================================
*/

#pragma once

#include "PhysicsActionComponent.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class AoETester : public PhysicsActionComponent
{
	DECLARE_CLASS(AoETester)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* Determines what kind of shape the AoE will take form. */
	enum EShapeType
	{
		ST_Point,
		ST_Sphere
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	EShapeType ShapeType;

	/* The radius of the spherical AoE test (the distance from the point). */
	SD::Float Radius;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	
protected:
	virtual void InitializeGuiInstance () override;
	virtual void DoAction (const SD::Vector3& actionPos) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Performs the actual AoE and draw on the scene to visualize it.
	 */
	virtual void PerformAoE (const SD::Vector3& position);
};
SD_TESTER_END
#endif