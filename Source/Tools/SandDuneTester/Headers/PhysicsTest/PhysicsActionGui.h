/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsActionGui.h
  A simple GuiEntity that allows the user to configure the PhysicsActionComponent's collision channels.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsActionComponent;

class PhysicsActionGui : public SD::GuiEntity
{
	DECLARE_CLASS(PhysicsActionGui)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Colors that map for each channel button. The button's color states will act like multipliers using this map as the base. */
	SD::Color BaseColors[3];

	/* Determines the vertical spacing of each option in the vertical list. */
	SD::Float OptionHeight;

	SD::VerticalList* ListComp;
	SD::LabelComponent* ChannelLabel;
	SD::DPointer<PhysicsActionComponent> PhysicsAction;
	SD::ButtonComponent* RedChannelButton;
	SD::ButtonComponent* GreenChannelButton;
	SD::ButtonComponent* BlueChannelButton;
	SD::LabelComponent* InstructionLabel;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetChannelPrompt (const SD::DString& newChannelPrompt);
	virtual void SetInstructions (const SD::DString& newInstructions);


	/*
	=====================
	  Mutator
	=====================
	*/

public:
	virtual void SetPhysicsAction (PhysicsActionComponent* newPhysicsAction);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the color state brightness based if the channel is active or not.
	 */
	virtual void UpdateColorStates (SD::ButtonComponent* button, size_t buttonIdx, bool bActive);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleRedChannelReleased (SD::ButtonComponent* button);
	virtual void HandleGreenChannelReleased (SD::ButtonComponent* button);
	virtual void HandleBlueChannelReleased (SD::ButtonComponent* button);
};
SD_TESTER_END
#endif