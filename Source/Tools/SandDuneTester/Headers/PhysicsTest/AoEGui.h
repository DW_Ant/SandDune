/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AoEGui.h
  A GuiEntity that allows the user to configure the AoETester's shape type and size.
=====================================================================
*/

#pragma once

#include "PhysicsActionGui.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class AoEGui : public PhysicsActionGui
{
	DECLARE_CLASS(AoEGui)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the amount to decrease/increase the tester's size every time the button is pressed. */
	SD::Float StepSize;

protected:
	SD::ButtonComponent* PointButton;
	SD::ButtonComponent* SphereButton;
	SD::ButtonComponent* IncreaseSizeButton;
	SD::LabelComponent* SizeLabel;
	SD::ButtonComponent* DecreaseSizeButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void SetPhysicsAction (PhysicsActionComponent* newPhysicsAction) override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes the color theme for the given button.
	 */
	virtual void InitButtonColors (SD::ButtonComponent* button);

	/**
	 * Refreshes button states and the size label that reflects the current state of the AoETester.
	 */
	virtual void EvaluateGuiStates ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePointReleased (SD::ButtonComponent* button);
	virtual void HandleSphereReleased (SD::ButtonComponent* button);
	virtual void HandleIncreaseSizeReleased (SD::ButtonComponent* button);
	virtual void HandleDecreaseSizeReleased (SD::ButtonComponent* button);
};
SD_TESTER_END
#endif