/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ShaderTester.h
  Object that'll be testing the ShaderComponent.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ShaderTester : public SD::GuiEntity
{
	DECLARE_CLASS(ShaderTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Called whenever the test is terminated/destroyed. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	SD::DPointer<SD::FrameComponent> Borders;
	SD::DPointer<SD::DropdownComponent> TextureDropdown;
	SD::DPointer<SD::DropdownComponent> SpriteShaderDropdown;
	SD::DPointer<SD::DropdownComponent> FontDropdown;
	SD::DPointer<SD::DropdownComponent> TextShaderDropdown;

	SD::DPointer<SD::LabelComponent> TextureDropdownLabel;
	SD::DPointer<SD::LabelComponent> SpriteShaderDropdownLabel;
	SD::DPointer<SD::LabelComponent> FontDropdownLabel;
	SD::DPointer<SD::LabelComponent> TextShaderDropdownLabel;

	SD::DPointer<SD::ButtonComponent> CloseButton;

	/* Various components that could be rendered with shaders. */
	SD::DPointer<SD::SpriteComponent> SpriteDemo;
	SD::DPointer<SD::LabelComponent> TextDemo;

	SD::DPointer<SD::GuiComponent> SpriteOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through the Shader directory, and reports a list of all available shaders.
	 */
	virtual void GetShaderList (std::vector<SD::DString>& outShaderList) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTextureSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx);
	virtual void HandleSpriteShaderSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx);
	virtual void HandleFontSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx);
	virtual void HandleTextShaderSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx);
	virtual void HandleCloseButtonPressed (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif