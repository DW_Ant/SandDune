/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteInstanceTesterUi.h
  Entity that displays the Gui controls for the SpriteInstanceTester.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class SpriteInstanceTester;

class SpriteInstanceTesterUi : public SD::GuiEntity
{
	DECLARE_CLASS(SpriteInstanceTesterUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::ButtonComponent* SettingsTab;
	SD::ButtonComponent* TransformTab;
	SD::ButtonComponent* ViewTab;

	SD::FrameComponent* SettingsFrame;
	SD::FrameComponent* TransformFrame;
	SD::FrameComponent* ViewFrame;
	SD::FrameComponent* StatsFrame;

	//Settings for the General tab
	SD::TextFieldComponent* NumInstancesField;
	SD::TextFieldComponent* CellSizeField;
	SD::TextFieldComponent* NumSubDivisionsFieldX;
	SD::TextFieldComponent* NumSubDivisionsFieldY;
	SD::TextFieldComponent* BrushSizeField;

	//Settings for the Sprite Transform tab
	SD::TextFieldComponent* MinSizeField;
	SD::TextFieldComponent* MaxSizeField;
	SD::CheckboxComponent* UniformSizeCheckbox;
	SD::CheckboxComponent* RandRotationCheckbox;

	//Settings for the View tab
	SD::CheckboxComponent* ShowDensityMapCheckbox;
	SD::CheckboxComponent* ShowBrushCheckbox;
	SD::CheckboxComponent* ShowStatsCheckbox;

	//Components for the Stats frame
	SD::LabelComponent* FrameRateLabel;
	SD::LabelComponent* GenerationTimeLabel;
	SD::LabelComponent* NumSpritesLabel;

	SD::DPointer<SpriteInstanceTester> OwningTester;
	SD::TickComponent* Tick;

	/* Small list of components to check to see if this UI is hovered. */
	std::vector<SD::GuiComponent*> HoverCheckComponents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/* Returns true if the given mouse coordinates are over any of the visible UI components. */
	virtual bool IsHovered (const SD::Vector2& mousePos) const;

	/**
	 * Reads the propertties of each Gui Component to figure out how to regenerate the sprite instances.
	 */
	virtual void RegenerateSpriteInstances ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningTester (SpriteInstanceTester* newOwningTester);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::Float deltaSec);
	virtual bool HandleAllowTextInput_NumericOnly (const SD::DString& txt);

	virtual void HandleSettingsTabReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleTransformTabReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleViewTabReleased (SD::ButtonComponent* uiComponent);

	virtual void HandleApplyNumInstances (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyCellSize (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyDimensionsX (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyDimensionsY (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyNumSubDivisionsX (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyNumSubDivisionsY (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyBrushSize (SD::TextFieldComponent* uiComponent);

	virtual void HandleApplyMinSize (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyMaxSize (SD::TextFieldComponent* uiComponent);

	virtual void HandleUniformSizeChecked (SD::CheckboxComponent* uiComponent);
	virtual void HandleRandRotationChecked (SD::CheckboxComponent* uiComponent);

	virtual void HandleDensityMapChecked (SD::CheckboxComponent* uiComponent);
	virtual void HandleShowBrushChecked (SD::CheckboxComponent* uiComponent);
	virtual void HandleShowStatsChecked (SD::CheckboxComponent* uiComponent);
};
SD_TESTER_END

#endif