/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ShaderTester.cpp
=====================================================================
*/

#include "ShaderTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::ShaderTester, SD::GuiEntity)
SD_TESTER_BEGIN

void ShaderTester::InitProps ()
{
	Super::InitProps();

	Borders = nullptr;
	TextureDropdown = nullptr;
	SpriteShaderDropdown = nullptr;
	FontDropdown = nullptr;
	TextShaderDropdown = nullptr;

	TextureDropdownLabel = nullptr;
	SpriteShaderDropdownLabel = nullptr;
	FontDropdownLabel = nullptr;
	TextShaderDropdownLabel = nullptr;

	CloseButton = nullptr;

	SpriteDemo = nullptr;
	TextDemo = nullptr;
	SpriteOwner = nullptr;
}

void ShaderTester::ConstructUI ()
{
	SetSize(SD::Vector2(512.f, 384.f));
	SetPosition(SD::Vector2(32.f, 64.f));

	Borders = SD::FrameComponent::CreateObject();
	if (!AddComponent(Borders))
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to construct ShaderTester UI since it could not attach the Borders component to GuiEntity."));
		return;
	}
	else
	{
		Borders->SetSize(ReadCachedAbsSize() - SD::Vector2(Borders->GetBorderThickness(), Borders->GetBorderThickness()));
		Borders->SetBorderThickness(1.f);
		Borders->SetLockedFrame(true);
	}

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	std::vector<SD::DString> shaderList;
	shaderList.push_back("None");
	GetShaderList(shaderList);

	std::vector<SD::GuiDataElement<SD::DString>> shaderDropdownList;
	for (size_t i = 0; i < shaderList.size(); i++)
	{
		shaderDropdownList.push_back(SD::GuiDataElement<SD::DString>(shaderList.at(i), shaderList.at(i)));
	}

	TextureDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(TextureDropdown))
	{
		std::vector<SD::Texture*> importedTextures;
		localTexturePool->GetAllTextures(OUT importedTextures);

		std::vector<SD::GuiDataElement<SD::Texture*>> textureList;
		for (size_t i = 0; i < importedTextures.size(); i++)
		{
			textureList.push_back(SD::GuiDataElement<SD::Texture*>(importedTextures.at(i), importedTextures.at(i)->GetFriendlyName()));
		}
		TextureDropdown->GetExpandMenuList()->SetList(textureList);
		TextureDropdown->SetSelectedItem(localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.DebuggingTexture")));
		TextureDropdown->SetSize(SD::Vector2(350, 256));
		TextureDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - TextureDropdown->ReadCachedAbsSize().X, 96));
		TextureDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, ShaderTester, HandleTextureSelected, void, SD::DropdownComponent*, SD::Int);
	}

	SpriteShaderDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(SpriteShaderDropdown))
	{
		SpriteShaderDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, ShaderTester, HandleSpriteShaderSelected, void, SD::DropdownComponent*, SD::Int);
		SpriteShaderDropdown->GetExpandMenuList()->SetList(shaderDropdownList);
		SpriteShaderDropdown->SetSize(SD::Vector2(350, 168));
		SpriteShaderDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - SpriteShaderDropdown->ReadCachedAbsSize().X, 24));
	}

	FontDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(FontDropdown))
	{
		FontDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, ShaderTester, HandleFontSelected, void, SD::DropdownComponent*, SD::Int);

		std::vector<SD::Font*> importedFonts;
		SD::FontPool::FindFontPool()->GetAllFonts(OUT importedFonts);

		std::vector<SD::GuiDataElement<SD::Font*>> fontList;
		for (UINT_TYPE i = 0; i < importedFonts.size(); i++)
		{
			fontList.push_back(SD::GuiDataElement<SD::Font*>(importedFonts.at(i), importedFonts.at(i)->GetFriendlyName()));
		}
		FontDropdown->GetExpandMenuList()->SetList(fontList);
		FontDropdown->SetSize(SD::Vector2(350, 256));
		FontDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - FontDropdown->ReadCachedAbsSize().X, 240));
	}

	TextShaderDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(TextShaderDropdown))
	{
		TextShaderDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, ShaderTester, HandleTextShaderSelected, void, SD::DropdownComponent*, SD::Int);
		TextShaderDropdown->GetExpandMenuList()->SetList(shaderDropdownList);
		TextShaderDropdown->SetSize(SD::Vector2(350, 256));
		TextShaderDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsPosition().X - TextShaderDropdown->ReadCachedAbsPosition().X, 312));
	}

	TextureDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextureDropdownLabel))
	{
		TextureDropdownLabel->SetCharacterSize(16);
		TextureDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextureDropdownLabel->SetWrapText(false);
		TextureDropdownLabel->SetText(TXT("Base Texture:"));
		TextureDropdownLabel->SetSize(SD::Vector2(TextureDropdown->ReadCachedAbsSize().X, 24));
		TextureDropdownLabel->SetPosition(TextureDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	SpriteOwner = SD::GuiComponent::CreateObject();
	if (Borders->AddComponent(SpriteOwner))
	{
		SpriteOwner->SetPosition(SD::Vector2(Borders->GetBorderThickness(), TextureDropdownLabel->ReadPosition().Y));
		SpriteOwner->SetSize(SD::Vector2(64, 64));

		SpriteDemo = SD::SpriteComponent::CreateObject();
		SpriteOwner->AddComponent(SpriteDemo);
	}

	SpriteShaderDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(SpriteShaderDropdownLabel))
	{
		SpriteShaderDropdownLabel->SetCharacterSize(16);
		SpriteShaderDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		SpriteShaderDropdownLabel->SetWrapText(false);
		SpriteShaderDropdownLabel->SetText(TXT("Sprite Shader:"));
		SpriteShaderDropdownLabel->SetSize(SD::Vector2(SpriteShaderDropdown->ReadCachedAbsSize().X, 24));
		SpriteShaderDropdownLabel->SetPosition(SpriteShaderDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	FontDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(FontDropdownLabel))
	{
		FontDropdownLabel->SetCharacterSize(16);
		FontDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		FontDropdownLabel->SetWrapText(false);
		FontDropdownLabel->SetText(TXT("Font:"));
		FontDropdownLabel->SetSize(SD::Vector2(FontDropdown->ReadCachedAbsSize().X, 24));
		FontDropdownLabel->SetPosition(FontDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	TextDemo = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextDemo))
	{
		TextDemo->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextDemo->SetWrapText(true);
		TextDemo->SetCharacterSize(24);
		TextDemo->SetText(SD::ProjectName);
		TextDemo->SetPosition(SpriteOwner->ReadPosition() + SD::Vector2(0, FontDropdownLabel->ReadPosition().Y));
		TextDemo->SetSize(SD::Vector2(128, 64));
	}

	TextShaderDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextShaderDropdownLabel))
	{
		TextShaderDropdownLabel->SetCharacterSize(16);
		TextShaderDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextShaderDropdownLabel->SetWrapText(false);
		TextShaderDropdownLabel->SetText(TXT("Text Shader:"));
		TextShaderDropdownLabel->SetSize(SD::Vector2(TextShaderDropdown->ReadCachedAbsSize().X, 24));
		TextShaderDropdownLabel->SetPosition(TextShaderDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	CloseButton = SD::ButtonComponent::CreateObject();
	if (Borders->AddComponent(CloseButton))
	{
		CloseButton->SetCaptionText(TXT("Close"));
		CloseButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ShaderTester, HandleCloseButtonPressed, void, SD::ButtonComponent*));
		SD::Texture* textureStates = SD::GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get();
		if (textureStates != nullptr)
		{
			if (SD::FrameComponent* background = CloseButton->GetBackground())
			{
				background->SetCenterTexture(textureStates);
			}
		}

		CloseButton->ReplaceStateComponent(SD::SingleSpriteButtonState::SStaticClass());
		CloseButton->SetSize(SD::Vector2(48, 24));
		CloseButton->SetPosition(SD::Vector2((Borders->ReadCachedAbsSize().X - (CloseButton->ReadCachedAbsSize().X/2)) / 2, 0.95f));
	}
}

void ShaderTester::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	CHECK(Focus.IsValid());

	Focus->TabOrder.push_back(TextureDropdown.Get());
	Focus->TabOrder.push_back(SpriteShaderDropdown.Get());
	Focus->TabOrder.push_back(FontDropdown.Get());
	Focus->TabOrder.push_back(TextShaderDropdown.Get());
}

void ShaderTester::Destroyed ()
{
	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void ShaderTester::GetShaderList (std::vector<SD::DString>& outShaderList) const
{
	for (SD::FileIterator iter(SD::Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), SD::FileIterator::SF_IncludeFiles | SD::FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); iter++)
	{
		SD::FileAttributes attributes = iter.GetSelectedAttributes();
		if (attributes.GetFileExtension().Compare(TXT(".frag"), SD::DString::CC_IgnoreCase) == 0)
		{
			outShaderList.push_back(attributes.GetName(false, true));
		}
	}
}

void ShaderTester::HandleTextureSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx)
{
	CHECK(SpriteDemo.IsValid())
	SpriteDemo->SetSpriteTexture(TextureDropdown->GetSelectedItem<SD::Texture*>());
}

void ShaderTester::HandleSpriteShaderSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx)
{
	CHECK(SpriteDemo.IsValid())
	if (dropdownIdx == 0) //None
	{
		SpriteDemo->SetSpriteShader(nullptr);
		return;
	}

	SD::DString fullFileName = SpriteShaderDropdown->GetSelectedItem<SD::DString>();
	SD::DString fileName;
	SD::DString fileExtension;
	SD::FileUtils::ExtractFileExtension(fullFileName, fileName, fileExtension);

	sf::Shader::Type shaderType = (fileExtension.Compare(FRAGMENT_SHADER_EXTENSION, SD::DString::CC_IgnoreCase) == 0) ? sf::Shader::Type::Fragment : sf::Shader::Type::Vertex;
	SD::Shader* newShader = SD::Shader::CreateShader(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), fullFileName), shaderType);
	if (newShader == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to apply shader %s to sprite since it failed to create and import a shader instance."), fullFileName);
		return;
	}

	SpriteDemo->SetSpriteShader(newShader);
}

void ShaderTester::HandleFontSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx)
{
	CHECK(TextDemo.IsValid())
	TextDemo->SetFont(FontDropdown->GetSelectedItem<SD::Font*>());
}

void ShaderTester::HandleTextShaderSelected (SD::DropdownComponent* comp, SD::Int dropdownIdx)
{
	CHECK(TextDemo.IsValid())
	if (dropdownIdx == 0) //None
	{
		TextDemo->GetRenderComponent()->SetTextShader(nullptr);
		return;
	}

	SD::DString fullFileName = TextShaderDropdown->GetSelectedItem<SD::DString>();
	SD::DString fileName;
	SD::DString fileExtension;
	SD::FileUtils::ExtractFileExtension(fullFileName, fileName, fileExtension);

	sf::Shader::Type shaderType = (fileExtension.Compare(FRAGMENT_SHADER_EXTENSION, SD::DString::CC_IgnoreCase) == 0) ? sf::Shader::Type::Fragment : sf::Shader::Type::Vertex;
	SD::Shader* newShader = SD::Shader::CreateShader(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Engine") / TXT("Graphics"), fullFileName), shaderType);
	if (newShader == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to apply shader %s to text since it failed to create and import a shader instance."), fullFileName);
		return;
	}

	TextDemo->GetRenderComponent()->SetTextShader(newShader);
}

void ShaderTester::HandleCloseButtonPressed (SD::ButtonComponent* uiComponent)
{
	Destroy();
}
SD_TESTER_END

#endif