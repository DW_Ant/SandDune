/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTesterUi.cpp
=====================================================================
*/

#include "Box2dTesterUi.h"
#include "Box2dTester.h"
#include "Box2dTesterTeleporter.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::Box2dTesterUi, SD::GuiEntity)
SD_TESTER_BEGIN

void Box2dTesterUi::InitProps ()
{
	Super::InitProps();

	MenuList = nullptr;
	LaunchButtonsUi = nullptr;
	OwningTester = nullptr;

	StaticColor = SD::Color::GREEN;
	DynamicColor = SD::Color::CYAN;
	OverlapColor = SD::Color::RED;
	TeleporterColor = SD::Color::MAGENTA;

	CollisionGroup = 29859; //Some hard-coded magic number that should be unique to any other collision systems.
}

void Box2dTesterUi::ConstructUI ()
{
	Super::ConstructUI();

	MenuList = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(MenuList))
	{
		MenuList->SetSize(SD::Vector2(0.15f, 0.33f));
		MenuList->SetHideControlsWhenFull(true);
		MenuList->SetZoomEnabled(false);
		CHECK(MenuList->GetFrameTexture() != nullptr)
		MenuList->GetFrameTexture()->ResetColor = SD::Color(32, 32, 32);

		LaunchButtonsUi = SD::GuiEntity::CreateObject();
		CHECK(LaunchButtonsUi != nullptr)

		LaunchButtonsUi->SetAutoSizeVertical(true);
		LaunchButtonsUi->SetEnableFractionScaling(false);
		LaunchButtonsUi->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
		MenuList->SetViewedObject(LaunchButtonsUi);

		SD::Float margins = 4.f;
		SD::Float buttonHeight = 16.f;
		SD::Float verticalButtonPos = margins;

		std::function<void(SD::ButtonComponent*)> initButton([&](SD::ButtonComponent* button)
		{
			button->SetPosition(SD::Vector2(0.f, verticalButtonPos));
			button->SetSize(SD::Vector2(1.f, buttonHeight));
			button->SetAnchorLeft(margins);
			button->SetAnchorRight(margins);
			LaunchButtons.push_back(button);
			verticalButtonPos += buttonHeight + margins;
		});

		SD::ButtonComponent* bouncingBallButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(bouncingBallButton))
		{
			initButton(bouncingBallButton);
			bouncingBallButton->SetCaptionText(TXT("Bouncy Ball Test"));
			bouncingBallButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, Box2dTesterUi, HandleLaunchBouncyBallTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* multiBallButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(multiBallButton))
		{
			initButton(multiBallButton);
			multiBallButton->SetCaptionText(TXT("Multi Balls Test"));
			multiBallButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, Box2dTesterUi, HandleLaunchMultiBallTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* overlapButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(overlapButton))
		{
			initButton(overlapButton);
			overlapButton->SetCaptionText(TXT("Overlap Test"));
			overlapButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, Box2dTesterUi, HandleLaunchOverlapTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* tumbleButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(tumbleButton))
		{
			initButton(tumbleButton);
			tumbleButton->SetCaptionText(TXT("Tumble Test"));
			tumbleButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, Box2dTesterUi, HandleLaunchTumbleTest, void, SD::ButtonComponent*));
		}
	}
}

void Box2dTesterUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())

	for (size_t i = 0; i < LaunchButtons.size(); ++i)
	{
		Focus->TabOrder.push_back(LaunchButtons.at(i));
	}
}

void Box2dTesterUi::InitializeRenderComponent (SD::SceneEntity* entity, SD::Color renderColor) const
{
	CHECK(entity != nullptr)

	if (OwningTester != nullptr && OwningTester->GetSceneDrawLayer() != nullptr)
	{
		SD::Box2dRenderer* renderer = SD::Box2dRenderer::CreateObject();
		if (entity->AddComponent(renderer))
		{
			renderer->FillColor = renderColor;
			OwningTester->GetSceneDrawLayer()->RegisterSingleComponent(renderer);
		}
	}
	else
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to initialize Render Entity for %s since either the owning tester or its draw layer is not initialized."), entity->DebugName);
	}
}

SD::SceneEntity* Box2dTesterUi::CreateStaticRectangle (std::vector<Entity*>& outEntityList, const SD::Vector3& position, const SD::Rotator& rotation, const SD::Rectangle& dimensions) const
{
	SD::SceneEntity* entity = SD::SceneEntity::CreateObject();
	entity->SetTranslation(position);
	entity->SetRotation(rotation);

	SD::Box2dComponent* comp = SD::Box2dComponent::CreateObject();
	if (entity->AddComponent(comp))
	{
		SD::Box2dComponent::SPhysicsInitData initData;
		initData.PhysicsType = SD::Box2dComponent::PT_Static;
		comp->InitializePhysics(initData);
		if (b2Body* body = comp->GetPhysicsBody())
		{
			b2FixtureDef fixtureInit;
			b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(dimensions);
			fixtureInit.shape = &shape;
			fixtureInit.density = 0.f;

			body->CreateFixture(&fixtureInit);
		}
		else
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to retrieve static rectangle's Box 2D component's body."));
		}

		comp->SetCollisionGroup(CollisionGroup);
		InitializeRenderComponent(entity, StaticColor);


	}
	outEntityList.push_back(entity);

	return entity;
}

void Box2dTesterUi::CreateStaticPerimeter (std::vector<Entity*>& outEntityList, SD::Float width, SD::Float height) const
{
	SD::Float halfWidth = width * 0.5f;
	SD::Float halfHeight = height * 0.5f;
	SD::Float rectThickness = 20.f;

	SD::SceneEntity* rightWall = CreateStaticRectangle(OUT outEntityList, SD::Vector3(halfWidth, 0.f, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle(rectThickness, height, SD::Vector2::ZERO_VECTOR));
	rightWall->DebugName = TXT("Right Wall");

	SD::SceneEntity* bottomWall = CreateStaticRectangle(OUT outEntityList, SD::Vector3(0.f, halfHeight, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle(width, rectThickness, SD::Vector2::ZERO_VECTOR));
	bottomWall->DebugName = TXT("Bottom Wall");

	SD::SceneEntity* leftWall = CreateStaticRectangle(OUT outEntityList, SD::Vector3(-halfWidth, 0.f, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle(rectThickness, height, SD::Vector2::ZERO_VECTOR));
	leftWall->DebugName = TXT("Left Wall");

	SD::SceneEntity* topWall = CreateStaticRectangle(OUT outEntityList, SD::Vector3(0.f, -halfHeight, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle(width, rectThickness, SD::Vector2::ZERO_VECTOR));
	topWall->DebugName = TXT("Top Wall");
}

void Box2dTesterUi::HandleLaunchBouncyBallTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	CreateStaticPerimeter(OUT entityList, 1000.f, 1000.f);

	SD::SceneEntity* tester = SD::SceneEntity::CreateObject();
	tester->DebugName = TXT("Bouncing Circle");
	entityList.push_back(tester);

	SD::Box2dComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::Box2dComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.StarterVelocity = SD::Vector3(500.f, 300.f, 0.f); //5 and 3 meters per second
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	circle.m_radius = 1.f; //1 meter radius
	compInit.CollisionShape = &circle;

	SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(tester, compInit);
	InitializeRenderComponent(tester, DynamicColor);

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void Box2dTesterUi::HandleLaunchMultiBallTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::Float arenaWidth = 2000.f;
	SD::Float arenaHeight = 1000.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::Box2dComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::Box2dComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	SD::Vector3 startPosition(arenaWidth * -0.5f, arenaHeight * -0.5f, 0.f);
	SD::Float maxCircleDiameter = 200.f;
	startPosition.Y += maxCircleDiameter;

	for (size_t i = 0; i < 32; ++i)
	{
		startPosition.X += maxCircleDiameter;
		if (startPosition.X + maxCircleDiameter >= arenaWidth * 0.5f)
		{
			startPosition.Y += maxCircleDiameter;
			startPosition.X = (arenaWidth * -0.5f) + maxCircleDiameter;
		}

		SD::SceneEntity* ball = SD::SceneEntity::CreateObject();
		ball->DebugName = TXT("Ball ") + SD::DString::MakeString(i);
		ball->SetTranslation(startPosition);
		entityList.push_back(ball);

		compInit.StarterVelocity = SD::Vector3(SD::RandomUtils::RandRange(-600.f, 600.f), SD::RandomUtils::RandRange(-600.f, 600.f), 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, maxCircleDiameter * 0.5f)); //10cm to 1 meter radii

		SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(ball, compInit);
		InitializeRenderComponent(ball, DynamicColor);
	}

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void Box2dTesterUi::HandleLaunchOverlapTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::Float arenaWidth = 1500.f;
	SD::Float arenaHeight = 1000.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::Box2dComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::Box2dComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	//Left ball
	{
		SD::SceneEntity* leftBall = SD::SceneEntity::CreateObject();
		leftBall->DebugName = TXT("Left Ball");
		leftBall->SetTranslation(SD::Vector3(-650.f, 0.f, 0.f));
		entityList.push_back(leftBall);

		compInit.StarterVelocity = SD::Vector3(600.f, 500.f, 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, 100.f)); //10cm to 1 meter radii

		SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(leftBall, compInit);
		InitializeRenderComponent(leftBall, DynamicColor);
	}

	//Right ball
	{
		SD::SceneEntity* rightBall = SD::SceneEntity::CreateObject();
		rightBall->DebugName = TXT("Right Ball");
		rightBall->SetTranslation(SD::Vector3(650.f, 0.f, 0.f));
		entityList.push_back(rightBall);

		compInit.StarterVelocity = SD::Vector3(-650.f, -400.f, 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, 100.f)); //10cm to 1 meter radii

		SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(rightBall, compInit);
		InitializeRenderComponent(rightBall, DynamicColor);
	}

	//Dynamic Overlap ball
	{
		SD::SceneEntity* overlapBall = SD::SceneEntity::CreateObject();
		overlapBall->DebugName = TXT("Moving Overlap Ball");
		overlapBall->SetTranslation(SD::Vector3(0.f, 0.f, -1));
		entityList.push_back(overlapBall);

		compInit.StarterVelocity = SD::Vector3(250.f, 0.f, 0.f);
		compInit.IsBlockingComponent = false;
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(150.f); //1.5 meter radius

		SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(overlapBall, compInit);
		InitializeRenderComponent(overlapBall, OverlapColor);
	}

	//Static Overlap ball
	{
		SD::SceneEntity* overlapBall = SD::SceneEntity::CreateObject();
		overlapBall->DebugName = TXT("Stationary Overlap Ball");
		overlapBall->SetTranslation(SD::Vector3(0.f, 0.f, -1.f));
		entityList.push_back(overlapBall);

		compInit.StarterVelocity = SD::Vector3(1.f, 1.f, 1.f);
		compInit.IsBlockingComponent = false;
		compInit.BodyInit.PhysicsType = SD::Box2dComponent::ePhysicsType::PT_Static;
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(150.f); //1.5 meter radius

		SD::Box2dComponent* comp = SD::Box2dComponent::CreateAndInitializeComponent<SD::Box2dComponent>(overlapBall, compInit);
		InitializeRenderComponent(overlapBall, OverlapColor);
	}

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void Box2dTesterUi::HandleLaunchTumbleTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::Float arenaWidth = 2000.f;
	SD::Float arenaHeight = 1250.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::Box2dComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::Box2dComponent::PT_Static;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	SD::Rectangle rectangleDimensions(400.f, 20.f, SD::Vector2::ZERO_VECTOR);
	SD::SceneEntity* topRight = CreateStaticRectangle(OUT entityList, SD::Vector3(512.f, -512.f, 0.f), SD::Rotator(30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* topCenter = CreateStaticRectangle(OUT entityList, SD::Vector3(128.f, -200.f, 0.f), SD::Rotator(-20.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomRight = CreateStaticRectangle(OUT entityList, SD::Vector3(512.f, 512.f, 0.f), SD::Rotator(-30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomCenter = CreateStaticRectangle(OUT entityList, SD::Vector3(128.f, 200.f, 0.f), SD::Rotator(20.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* centerTopHalf = CreateStaticRectangle(OUT entityList, SD::Vector3(-64.f, 96.f, 0.f), SD::Rotator(30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* centerBottomHalf = CreateStaticRectangle(OUT entityList, SD::Vector3(-64.f, -96.f, 0.f), SD::Rotator(-30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* topLeft = CreateStaticRectangle(OUT entityList, SD::Vector3(-512.f, -300.f, 0.f), SD::Rotator(45.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomLeft = CreateStaticRectangle(OUT entityList, SD::Vector3(-512.f, 300.f, 0.f), SD::Rotator(-45.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);

	Box2dTesterTeleporter* teleporter = Box2dTesterTeleporter::CreateObject();
	//Initialize teleporter
	{
		teleporter->SetTranslation(SD::Vector3(arenaWidth * -0.485f, 0.f, 0.f));
		teleporter->TeleportDestination = SD::Vector3(arenaWidth * 0.475f, 0.f, 0.f);
		teleporter->PreserveYCoordinates = true;

		SD::Box2dComponent* physComp = teleporter->GetPhysComp();
		CHECK(physComp != nullptr)
		SD::Box2dComponent::SPhysicsInitData teleportInit;
		teleportInit.PhysicsType = SD::Box2dComponent::PT_Static;
		physComp->InitializePhysics(teleportInit);
		if (b2Body* body = physComp->GetPhysicsBody())
		{
			b2FixtureDef fixtureInit;
			b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle(64.f, arenaHeight, SD::Vector2::ZERO_VECTOR));
			fixtureInit.shape = &shape;
			fixtureInit.isSensor = true;

			body->CreateFixture(&fixtureInit);
		}

		physComp->SetCollisionGroup(CollisionGroup);
		InitializeRenderComponent(teleporter, TeleporterColor);
		entityList.push_back(teleporter);
	}

	//Create numerous moving/tumbling crosses
	for (int i = 0; i < 32; ++i)
	{
		SD::SceneEntity* cross = SD::SceneEntity::CreateObject();
		cross->DebugName = TXT("Tumbling Cross");
		cross->SetTranslation(SD::Vector3(arenaWidth * 0.45f + SD::RandomUtils::RandRange(-64.f, 0.f), SD::RandomUtils::RandRange(arenaHeight * -0.45f, arenaHeight * 0.45f), SD::Float::MakeFloat(i)));
		entityList.push_back(cross);

		SD::Box2dComponent* crossPhys = SD::Box2dComponent::CreateObject();
		if (cross->AddComponent(crossPhys))
		{
			crossPhys->SetAcceleration(SD::Vector3(-32.f, 0.f, 0.f));

			SD::Box2dComponent::SPhysicsInitData crossInit;
			crossInit.AngularDamping = 0.f;
			crossInit.IsFixedRotation = false;
			crossInit.LinearDamping = 0.f;
			crossInit.PhysicsType = SD::Box2dComponent::PT_Dynamic;
			crossPhys->InitializePhysics(crossInit);
			if (b2Body* crossBody = crossPhys->GetPhysicsBody())
			{
				b2FixtureDef fixtureInit;
				b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle(8.f, 48.f, SD::Vector2::ZERO_VECTOR));
				shape.m_type = b2Shape::e_polygon;
				fixtureInit.shape = &shape;
				fixtureInit.isSensor = false;
				fixtureInit.restitution = 0.5f;
				fixtureInit.friction = 0.25f;
				fixtureInit.density = 1.f;
				crossBody->CreateFixture(&fixtureInit);

				shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle(48.f, 8.f, SD::Vector2::ZERO_VECTOR));
				crossBody->CreateFixture(&fixtureInit);
			}

			crossPhys->SetCollisionGroup(CollisionGroup);
			InitializeRenderComponent(cross, DynamicColor);
		}
		else
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to attached Box2dComponent to a SceneEntity when initializing the tumbling crosses."));
		}
	}

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}
SD_TESTER_END

#endif