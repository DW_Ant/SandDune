/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScreenCaptureTester.cpp
=====================================================================
*/

#include "ScreenCaptureTester.h"
#include "ScreenCaptureTestUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::ScreenCaptureTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 ScreenCaptureTester::WINDOW_SIZE(640.f, 480.f);

void ScreenCaptureTester::InitProps ()
{
	Super::InitProps();

	TestFlags = SD::UnitTester::UTF_None;
	MainTestUi = nullptr;
}

void ScreenCaptureTester::Destroyed ()
{
	if (MainTestUi != nullptr)
	{
		MainTestUi->Destroy();
		MainTestUi = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void ScreenCaptureTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the ScreenCaptureTest since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Screen Capture Test"), 0.033333f /*~30 fps*/);

	MainTestUi = ScreenCaptureTestUi::CreateObject();
	GuiDrawLayer->RegisterMenu(MainTestUi);
	MainTestUi->SetupInputComponent(Input.Get(), 100);

	SD::UnitTester::TestLog(TestFlags, TXT("The ScreenCaptureTest unit test has launched. It will terminate based on user input."));
}
SD_TESTER_END
#endif