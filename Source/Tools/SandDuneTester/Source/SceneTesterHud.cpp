/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTesterHud.cpp
=====================================================================
*/

#include "SceneTesterHud.h"
#include "SceneTester.h"
#include "Player.h"
#include "SandDuneTesterTheme.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::SceneTesterHud, SD::GuiEntity)
SD_TESTER_BEGIN

void SceneTesterHud::InitProps ()
{
	Super::InitProps();

	HudTick = nullptr;
	MenuFrame = nullptr;
	ToggleHelpButton = nullptr;
	ToggleAttributesButton = nullptr;
	ToggleTestDescriptionButton = nullptr;
	ControlsFrame = nullptr;
	PlayerLabel = nullptr;
	PlayerMovementLabel = nullptr;
	PlayerSizeLabel = nullptr;
	MovePlayerFrame = nullptr;
	MovePlayerUpButton = nullptr;
	MovePlayerRightButton = nullptr;
	MovePlayerDownButton = nullptr;
	MovePlayerLeftButton = nullptr;
	IncreasePlayerSizeButton = nullptr;
	DecreasePlayerSizeButton = nullptr;
	CameraLabel = nullptr;
	CameraMovementLabel = nullptr;
	CameraZoomLabel = nullptr;
	MoveCameraFrame = nullptr;
	MoveCameraUpButton = nullptr;
	MoveCameraRightButton = nullptr;
	MoveCameraDownButton = nullptr;
	MoveCameraLeftButton = nullptr;
	ZoomInButton = nullptr;
	ZoomOutButton = nullptr;
	EntityDataFrame = nullptr;
	PlayerTransformLabel = nullptr;
	CameraTransformLabel = nullptr;
	TestDescriptionEntity = nullptr;
	TestDescriptionScrollbar = nullptr;
	TestDescriptionLabel = nullptr;
	TestOwner = nullptr;
	PlayerTester = nullptr;
	SceneTesterCamera = nullptr;
}

void SceneTesterHud::BeginObject ()
{
	Super::BeginObject();

	HudTick = SD::TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(HudTick))
	{
		HudTick->SetTickInterval(0.2f); //five times a second
		HudTick->SetTickHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleHudTick, void, SD::Float));
	}
}

void SceneTesterHud::ConstructUI ()
{
	Super::ConstructUI();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::DString localizationFileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("SceneTesterHud");

	MenuFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(MenuFrame))
	{
		MenuFrame->SetLockedFrame(true);
		MenuFrame->SetAnchorTop(0.f);
		MenuFrame->SetAnchorRight(0.f);
		MenuFrame->SetSize(SD::Vector2(0.25f, 0.075f));
		MenuFrame->SetBorderThickness(1.f);
		MenuFrame->SetCenterColor(SD::Color(0, 0, 0, 200));

		SD::Int buttonFontSize = 14;
		SD::Float buttonPadding = 0.05f;
		SD::Float buttonWidth = 0.275f;
		SD::Float buttonAnchors = 8.f;

		ToggleHelpButton = SD::ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(ToggleHelpButton))
		{
			ToggleHelpButton->SetAnchorTop(buttonAnchors);
			ToggleHelpButton->SetAnchorBottom(buttonAnchors);
			ToggleHelpButton->SetSize(SD::Vector2(buttonWidth, 0.f));
			ToggleHelpButton->SetPosition(SD::Vector2(buttonPadding, 0.f));
			ToggleHelpButton->SetCaptionText(translator->TranslateText(TXT("HelpButton"), localizationFileName, sectionName));
			ToggleHelpButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleToggleHelpButtonReleased, void, SD::ButtonComponent*));

			if (ToggleHelpButton->GetCaptionComponent() != nullptr)
			{
				ToggleHelpButton->GetCaptionComponent()->SetCharacterSize(buttonFontSize);
			}
		}

		ToggleAttributesButton = SD::ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(ToggleAttributesButton))
		{
			ToggleAttributesButton->SetAnchorTop(buttonAnchors);
			ToggleAttributesButton->SetAnchorBottom(buttonAnchors);
			ToggleAttributesButton->SetSize(SD::Vector2(buttonWidth, 0.f));
			ToggleAttributesButton->SetPosition(SD::Vector2((buttonPadding * 2.f) + buttonWidth, 0.f));
			ToggleAttributesButton->SetCaptionText(translator->TranslateText(TXT("AttributesButton"), localizationFileName, sectionName));
			ToggleAttributesButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleToggleAttributesButtonReleased, void, SD::ButtonComponent*));

			if (ToggleAttributesButton->GetCaptionComponent() != nullptr)
			{
				ToggleAttributesButton->GetCaptionComponent()->SetCharacterSize(buttonFontSize);
			}
		}

		ToggleTestDescriptionButton = SD::ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(ToggleTestDescriptionButton))
		{
			ToggleTestDescriptionButton->SetAnchorTop(buttonAnchors);
			ToggleTestDescriptionButton->SetAnchorBottom(buttonAnchors);
			ToggleTestDescriptionButton->SetSize(SD::Vector2(buttonWidth, 0.f));
			ToggleTestDescriptionButton->SetPosition(SD::Vector2((buttonPadding * 3.f) + (buttonWidth * 2.f), 0.f));
			ToggleTestDescriptionButton->SetCaptionText(translator->TranslateText(TXT("DescriptionButton"), localizationFileName, sectionName));
			ToggleTestDescriptionButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleToggleDescriptionButtonReleased, void, SD::ButtonComponent*));

			if (ToggleTestDescriptionButton->GetCaptionComponent() != nullptr)
			{
				ToggleTestDescriptionButton->GetCaptionComponent()->SetCharacterSize(buttonFontSize);
			}
		}
	}

	ControlsFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(ControlsFrame))
	{
		ControlsFrame->SetLockedFrame(true);
		ControlsFrame->SetAnchorTop(0.f);
		ControlsFrame->SetSize(SD::Vector2(0.25f, 0.2f));
		ControlsFrame->SetBorderThickness(1.f);
		ControlsFrame->SetCenterColor(SD::Color(0, 0, 0, 200));

		SD::Int smallButtonCharSize = 12;
		std::vector<SD::Float> colPositions({0.01f, 0.3f, 0.7f});
		PlayerLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(PlayerLabel))
		{
			PlayerLabel->SetAutoRefresh(false);
			PlayerLabel->SetText(translator->TranslateText(TXT("PlayerLabel"), localizationFileName, sectionName));
			PlayerLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			PlayerLabel->SetWrapText(false);
			PlayerLabel->SetClampText(true);
			PlayerLabel->SetSize(SD::Vector2(0.25f, 8.f));
			PlayerLabel->SetPosition(SD::Vector2(colPositions.at(0), 0.1f));
			PlayerLabel->SetAutoRefresh(true);
		}

		PlayerMovementLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(PlayerMovementLabel))
		{
			PlayerMovementLabel->SetAutoRefresh(false);
			PlayerMovementLabel->SetText(translator->TranslateText(TXT("PlayerMovement"), localizationFileName, sectionName));
			PlayerMovementLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			PlayerMovementLabel->SetWrapText(false);
			PlayerMovementLabel->SetClampText(true);
			PlayerMovementLabel->SetCharacterSize(10);
			PlayerMovementLabel->SetSize(SD::Vector2(0.35f, 8.f));
			PlayerMovementLabel->SetPosition(SD::Vector2(colPositions.at(1), 0.f));
			PlayerMovementLabel->SetAutoRefresh(true);
		}

		PlayerSizeLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(PlayerSizeLabel))
		{
			PlayerSizeLabel->SetAutoRefresh(false);
			PlayerSizeLabel->SetText(translator->TranslateText(TXT("PlayerSize"), localizationFileName, sectionName));
			PlayerSizeLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			PlayerSizeLabel->SetWrapText(false);
			PlayerSizeLabel->SetClampText(true);
			PlayerSizeLabel->SetCharacterSize(10);
			PlayerSizeLabel->SetSize(SD::Vector2(0.25f, 8.f));
			PlayerSizeLabel->SetPosition(SD::Vector2(colPositions.at(2), 0.f));
			PlayerSizeLabel->SetAutoRefresh(true);
		}

		MovePlayerFrame = SD::FrameComponent::CreateObject();
		if (ControlsFrame->AddComponent(MovePlayerFrame))
		{
			MovePlayerFrame->SetLockedFrame(true);
			MovePlayerFrame->SetBorderThickness(0.f);
			MovePlayerFrame->SetSize(SD::Vector2(0.35f, 0.35f));
			MovePlayerFrame->SetPosition(SD::Vector2(colPositions.at(1), 16.f));

			SD::Float buttonSize = 0.3333f;
			MovePlayerUpButton = SD::ButtonComponent::CreateObject();
			if (MovePlayerFrame->AddComponent(MovePlayerUpButton))
			{
				MovePlayerUpButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MovePlayerUpButton->SetPosition(SD::Vector2(buttonSize, 0.f));
				MovePlayerUpButton->SetCaptionText(translator->TranslateText(TXT("MovePlayerUp"), localizationFileName, sectionName));
				MovePlayerUpButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveUpButtonPressed, void, SD::ButtonComponent*));
				MovePlayerUpButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveUpButtonReleased, void, SD::ButtonComponent*));
				if (MovePlayerUpButton->GetCaptionComponent() != nullptr)
				{
					MovePlayerUpButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MovePlayerRightButton = SD::ButtonComponent::CreateObject();
			if (MovePlayerFrame->AddComponent(MovePlayerRightButton))
			{
				MovePlayerRightButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MovePlayerRightButton->SetPosition(SD::Vector2((buttonSize * 2.f), buttonSize));
				MovePlayerRightButton->SetCaptionText(translator->TranslateText(TXT("MovePlayerRight"), localizationFileName, sectionName));
				MovePlayerRightButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveRightButtonPressed, void, SD::ButtonComponent*));
				MovePlayerRightButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveRightButtonReleased, void, SD::ButtonComponent*));
				if (MovePlayerRightButton->GetCaptionComponent() != nullptr)
				{
					MovePlayerRightButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MovePlayerDownButton = SD::ButtonComponent::CreateObject();
			if (MovePlayerFrame->AddComponent(MovePlayerDownButton))
			{
				MovePlayerDownButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MovePlayerDownButton->SetPosition(SD::Vector2(buttonSize, (buttonSize * 2.f)));
				MovePlayerDownButton->SetCaptionText(translator->TranslateText(TXT("MovePlayerDown"), localizationFileName, sectionName));
				MovePlayerDownButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveDownButtonPressed, void, SD::ButtonComponent*));
				MovePlayerDownButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveDownButtonReleased, void, SD::ButtonComponent*));
				if (MovePlayerDownButton->GetCaptionComponent() != nullptr)
				{
					MovePlayerDownButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MovePlayerLeftButton = SD::ButtonComponent::CreateObject();
			if (MovePlayerFrame->AddComponent(MovePlayerLeftButton))
			{
				MovePlayerLeftButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MovePlayerLeftButton->SetPosition(SD::Vector2(0.f, buttonSize));
				MovePlayerLeftButton->SetCaptionText(translator->TranslateText(TXT("MovePlayerLeft"), localizationFileName, sectionName));
				MovePlayerLeftButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveLeftButtonPressed, void, SD::ButtonComponent*));
				MovePlayerLeftButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerMoveLeftButtonReleased, void, SD::ButtonComponent*));
				if (MovePlayerLeftButton->GetCaptionComponent() != nullptr)
				{
					MovePlayerLeftButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}
		}

		SD::Float buttonSizeSize = 0.1f;
		IncreasePlayerSizeButton = SD::ButtonComponent::CreateObject();
		if (ControlsFrame->AddComponent(IncreasePlayerSizeButton))
		{
			IncreasePlayerSizeButton->SetSize(SD::Vector2(buttonSizeSize, buttonSizeSize * 1.5f));
			IncreasePlayerSizeButton->SetPosition(SD::Vector2(colPositions.at(2), 16.f));
			IncreasePlayerSizeButton->SetCaptionText(translator->TranslateText(TXT("IncreasePlayerSize"), localizationFileName, sectionName));
			IncreasePlayerSizeButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerIncreaseSizeButtonPressed, void, SD::ButtonComponent*));
			IncreasePlayerSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerIncreaseSizeButtonReleased, void, SD::ButtonComponent*));
			if (IncreasePlayerSizeButton->GetCaptionComponent() != nullptr)
			{
				IncreasePlayerSizeButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
			}
		}

		DecreasePlayerSizeButton = SD::ButtonComponent::CreateObject();
		if (ControlsFrame->AddComponent(DecreasePlayerSizeButton))
		{
			DecreasePlayerSizeButton->SetSize(SD::Vector2(buttonSizeSize, buttonSizeSize * 1.5f));
			DecreasePlayerSizeButton->SetPosition(SD::Vector2(colPositions.at(2) + buttonSizeSize + 0.01f, 16.f));
			DecreasePlayerSizeButton->SetCaptionText(translator->TranslateText(TXT("DecreasePlayerSize"), localizationFileName, sectionName));
			DecreasePlayerSizeButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerDecreaseSizeButtonPressed, void, SD::ButtonComponent*));
			DecreasePlayerSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandlePlayerDecreaseSizeButtonReleased, void, SD::ButtonComponent*));
			if (DecreasePlayerSizeButton->GetCaptionComponent() != nullptr)
			{
				DecreasePlayerSizeButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
			}
		}

		CameraLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(CameraLabel))
		{
			CameraLabel->SetAutoRefresh(false);
			CameraLabel->SetText(translator->TranslateText(TXT("Camera"), localizationFileName, sectionName));
			CameraLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			CameraLabel->SetWrapText(false);
			CameraLabel->SetClampText(true);
			CameraLabel->SetSize(SD::Vector2(0.25f, 8.f));
			CameraLabel->SetPosition(SD::Vector2(colPositions.at(0), 0.5));
			CameraLabel->SetAutoRefresh(true);
		}

		CameraMovementLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(CameraMovementLabel))
		{
			CameraMovementLabel->SetAutoRefresh(false);
			CameraMovementLabel->SetText(translator->TranslateText(TXT("CameraMovement"), localizationFileName, sectionName));
			CameraMovementLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			CameraMovementLabel->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
			CameraMovementLabel->SetAnchorBottom(8.f);
			CameraMovementLabel->SetWrapText(false);
			CameraMovementLabel->SetClampText(true);
			CameraMovementLabel->SetCharacterSize(10);
			CameraMovementLabel->SetSize(SD::Vector2(0.35f, 8.f));
			CameraMovementLabel->SetPosition(SD::Vector2(colPositions.at(1), 0.85f));
			CameraMovementLabel->SetAutoRefresh(true);
		}

		CameraZoomLabel = SD::LabelComponent::CreateObject();
		if (ControlsFrame->AddComponent(CameraZoomLabel))
		{
			CameraZoomLabel->SetAutoRefresh(false);
			CameraZoomLabel->SetText(translator->TranslateText(TXT("CameraSize"), localizationFileName, sectionName));
			CameraZoomLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			CameraZoomLabel->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
			CameraZoomLabel->SetAnchorBottom(8.f);
			CameraZoomLabel->SetWrapText(false);
			CameraZoomLabel->SetClampText(true);
			CameraZoomLabel->SetCharacterSize(10);
			CameraZoomLabel->SetSize(SD::Vector2(0.25f, 8.f));
			CameraZoomLabel->SetPosition(SD::Vector2(colPositions.at(2), 0.85f));
			CameraZoomLabel->SetAutoRefresh(true);
		}

		MoveCameraFrame = SD::FrameComponent::CreateObject();
		if (ControlsFrame->AddComponent(MoveCameraFrame))
		{
			MoveCameraFrame->SetLockedFrame(true);
			MoveCameraFrame->SetBorderThickness(0.f);
			MoveCameraFrame->SetSize(SD::Vector2(0.35f, 0.35f));
			MoveCameraFrame->SetPosition(SD::Vector2(colPositions.at(1), 0.5f));

			SD::Float buttonSize = 0.333333f;
			MoveCameraUpButton = SD::ButtonComponent::CreateObject();
			if (MoveCameraFrame->AddComponent(MoveCameraUpButton))
			{
				MoveCameraUpButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MoveCameraUpButton->SetPosition(SD::Vector2(buttonSize, 0.f));
				MoveCameraUpButton->SetCaptionText(translator->TranslateText(TXT("MoveCameraUp"), localizationFileName, sectionName));
				MoveCameraUpButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveUpButtonPressed, void, SD::ButtonComponent*));
				MoveCameraUpButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveUpButtonReleased, void, SD::ButtonComponent*));
				if (MoveCameraUpButton->GetCaptionComponent() != nullptr)
				{
					MoveCameraUpButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MoveCameraRightButton = SD::ButtonComponent::CreateObject();
			if (MoveCameraFrame->AddComponent(MoveCameraRightButton))
			{
				MoveCameraRightButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MoveCameraRightButton->SetPosition(SD::Vector2((buttonSize * 2.f), buttonSize));
				MoveCameraRightButton->SetCaptionText(translator->TranslateText(TXT("MoveCameraRight"), localizationFileName, sectionName));
				MoveCameraRightButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveRightButtonPressed, void, SD::ButtonComponent*));
				MoveCameraRightButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveRightButtonReleased, void, SD::ButtonComponent*));
				if (MoveCameraRightButton->GetCaptionComponent() != nullptr)
				{
					MoveCameraRightButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MoveCameraDownButton = SD::ButtonComponent::CreateObject();
			if (MoveCameraFrame->AddComponent(MoveCameraDownButton))
			{
				MoveCameraDownButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MoveCameraDownButton->SetPosition(SD::Vector2(buttonSize, (buttonSize * 2.f)));
				MoveCameraDownButton->SetCaptionText(translator->TranslateText(TXT("MoveCameraDown"), localizationFileName, sectionName));
				MoveCameraDownButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveDownButtonPressed, void, SD::ButtonComponent*));
				MoveCameraDownButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveDownButtonReleased, void, SD::ButtonComponent*));
				if (MoveCameraDownButton->GetCaptionComponent() != nullptr)
				{
					MoveCameraDownButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}

			MoveCameraLeftButton = SD::ButtonComponent::CreateObject();
			if (MoveCameraFrame->AddComponent(MoveCameraLeftButton))
			{
				MoveCameraLeftButton->SetSize(SD::Vector2(buttonSize, buttonSize));
				MoveCameraLeftButton->SetPosition(SD::Vector2(0.f, buttonSize));
				MoveCameraLeftButton->SetCaptionText(translator->TranslateText(TXT("MoveCameraLeft"), localizationFileName, sectionName));
				MoveCameraLeftButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveLeftButtonPressed, void, SD::ButtonComponent*));
				MoveCameraLeftButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraMoveLeftButtonReleased, void, SD::ButtonComponent*));
				if (MoveCameraLeftButton->GetCaptionComponent() != nullptr)
				{
					MoveCameraLeftButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize);
				}
			}
		}

		ZoomInButton = SD::ButtonComponent::CreateObject();
		if (ControlsFrame->AddComponent(ZoomInButton))
		{
			ZoomInButton->SetSize(SD::Vector2(buttonSizeSize, buttonSizeSize * 1.5f));
			ZoomInButton->SetPosition(SD::Vector2(colPositions.at(2), 0.7f));
			ZoomInButton->SetCaptionText(translator->TranslateText(TXT("ZoomIn"), localizationFileName, sectionName));
			ZoomInButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraZoomInButtonPressed, void, SD::ButtonComponent*));
			ZoomInButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraZoomInButtonReleased, void, SD::ButtonComponent*));
			if (ZoomInButton->GetCaptionComponent() != nullptr)
			{
				ZoomInButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize + 3);
			}
		}

		ZoomOutButton = SD::ButtonComponent::CreateObject();
		if (ControlsFrame->AddComponent(ZoomOutButton))
		{
			ZoomOutButton->SetSize(SD::Vector2(buttonSizeSize, buttonSizeSize * 1.5f));
			ZoomOutButton->SetPosition(SD::Vector2(colPositions.at(2) + buttonSizeSize + 0.01f, 0.7f));
			ZoomOutButton->SetCaptionText(translator->TranslateText(TXT("ZoomOut"), localizationFileName, sectionName));
			ZoomOutButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraZoomOutButtonPressed, void, SD::ButtonComponent*));
			ZoomOutButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SceneTesterHud, HandleCameraZoomOutButtonReleased, void, SD::ButtonComponent*));
			if (ZoomOutButton->GetCaptionComponent() != nullptr)
			{
				ZoomOutButton->GetCaptionComponent()->SetCharacterSize(smallButtonCharSize + 3);
			}
		}
	}

	EntityDataFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(EntityDataFrame))
	{
		EntityDataFrame->SetLockedFrame(true);
		EntityDataFrame->SetAnchorTop(0.f);
		EntityDataFrame->SetSize(SD::Vector2(0.25f, 0.2f));
		EntityDataFrame->SetBorderThickness(1.f);
		EntityDataFrame->SetCenterColor(SD::Color(0, 0, 0, 200));

		PlayerTransformLabel = SD::LabelComponent::CreateObject();
		if (EntityDataFrame->AddComponent(PlayerTransformLabel))
		{
			TranslatedPlayerTransform = translator->TranslateText(TXT("PlayerTransform"), localizationFileName, sectionName);
			PlayerTransformLabel->SetAutoRefresh(false);
			PlayerTransformLabel->SetVisibility(false); //Initially hide it since the entity is not yet linked.
			PlayerTransformLabel->SetWrapText(true);
			PlayerTransformLabel->SetClampText(true);
			PlayerTransformLabel->SetSize(SD::Vector2(1.f, 0.5));
			PlayerTransformLabel->SetPosition(SD::Vector2(0.f, 0.1f));
			PlayerTransformLabel->SetAutoRefresh(true);
		}

		CameraTransformLabel = SD::LabelComponent::CreateObject();
		if (EntityDataFrame->AddComponent(CameraTransformLabel))
		{
			TranslatedCameraTransform = translator->TranslateText(TXT("CameraTransform"), localizationFileName, sectionName);
			CameraTransformLabel->SetAutoRefresh(false);
			CameraTransformLabel->SetVisibility(false); //Initially hide it since the entity is not yet linked.
			CameraTransformLabel->SetWrapText(true);
			CameraTransformLabel->SetClampText(true);
			CameraTransformLabel->SetSize(SD::Vector2(1.f, 0.5));
			CameraTransformLabel->SetPosition(SD::Vector2(0.f, 0.5f));
			CameraTransformLabel->SetAutoRefresh(true);
		}
	}

	TestDescriptionScrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(TestDescriptionScrollbar))
	{
		TestDescriptionScrollbar->SetPosition(SD::Vector2(0.f, 0.f));
		TestDescriptionScrollbar->SetSize(SD::Vector2(0.25f, 0.2f));
		TestDescriptionScrollbar->SetHideControlsWhenFull(true);
		TestDescriptionScrollbar->SetZoomEnabled(false);
		if (TestDescriptionScrollbar->GetFrameTexture() != nullptr)
		{
			//Give it a slightly lighter background to differentiate this from the main test.
			TestDescriptionScrollbar->GetFrameTexture()->ResetColor = SD::Color(32, 32, 32);
		}

		SD::LabelComponent::CreateLabelWithinScrollbar(TestDescriptionScrollbar, OUT TestDescriptionEntity, OUT TestDescriptionLabel);
		CHECK(TestDescriptionEntity != nullptr && TestDescriptionLabel != nullptr)

		SD::DString translatedText = translator->TranslateText(TXT("TestDescription"), localizationFileName, sectionName);
		TestDescriptionLabel->SetText(translatedText);
	}

	RecomputePositions();
}

void SceneTesterHud::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(MovePlayerUpButton);
	Focus->TabOrder.push_back(MovePlayerRightButton);
	Focus->TabOrder.push_back(MovePlayerDownButton);
	Focus->TabOrder.push_back(MovePlayerLeftButton);
	Focus->TabOrder.push_back(IncreasePlayerSizeButton);
	Focus->TabOrder.push_back(DecreasePlayerSizeButton);

	Focus->TabOrder.push_back(MoveCameraUpButton);
	Focus->TabOrder.push_back(MoveCameraRightButton);
	Focus->TabOrder.push_back(MoveCameraDownButton);
	Focus->TabOrder.push_back(MoveCameraLeftButton);
	Focus->TabOrder.push_back(ZoomInButton);
	Focus->TabOrder.push_back(ZoomOutButton);

	Focus->TabOrder.push_back(ToggleHelpButton);
	Focus->TabOrder.push_back(ToggleAttributesButton);
	Focus->TabOrder.push_back(ToggleTestDescriptionButton);
}

void SceneTesterHud::LinkUpEntities (SceneTester* testOwner, Player* playerTester, SD::TopDownCamera* sceneTesterCamera)
{
	TestOwner = testOwner;
	PlayerTester = playerTester;
	SceneTesterCamera = sceneTesterCamera;

	UpdateEntityTransforms();
}


void SceneTesterHud::RecomputePositions ()
{
	SD::Float posX = 0.f;
	SD::Int numPanels = 0;
	if (ControlsFrame != nullptr && ControlsFrame->IsVisible())
	{
		ControlsFrame->SetPosition(SD::Vector2(posX, 0.f));
		posX += ControlsFrame->ReadSize().X;
		numPanels++;
	}

	if (EntityDataFrame != nullptr && EntityDataFrame->IsVisible())
	{
		EntityDataFrame->SetPosition(SD::Vector2(posX, 0.f));
		posX += EntityDataFrame->ReadSize().X;
		numPanels++;
	}

	if (TestDescriptionScrollbar != nullptr && TestDescriptionScrollbar->IsVisible())
	{
		SD::Float sizeX = (numPanels <= 1) ? 0.5f : 0.25f;
		TestDescriptionScrollbar->SetSize(sizeX, TestDescriptionScrollbar->ReadSize().Y);
		TestDescriptionScrollbar->SetPosition(SD::Vector2(posX, 0.f));

		posX += TestDescriptionScrollbar->ReadSize().X;
		numPanels++;
	}
}

void SceneTesterHud::UpdateEntityTransforms ()
{
	PlayerTransformLabel->SetVisibility(PlayerTester != nullptr);
	if (PlayerTester != nullptr)
	{
		SD::DString updatedText = TranslatedPlayerTransform;
		SD::DString::FormatString(OUT updatedText, PlayerTester->ReadAbsTranslation().X.ToInt(), PlayerTester->ReadAbsTranslation().Y.ToInt());
		PlayerTransformLabel->SetText(updatedText);
	}

	CameraTransformLabel->SetVisibility(SceneTesterCamera != nullptr);
	if (SceneTesterCamera)
	{
		SD::DString updatedText = TranslatedCameraTransform;
		SD::DString::FormatString(OUT updatedText, SceneTesterCamera->ReadAbsTranslation().X.ToInt(), SceneTesterCamera->ReadAbsTranslation().Y.ToInt());
		CameraTransformLabel->SetText(updatedText);
	}
}

void SceneTesterHud::HandleHudTick (SD::Float deltaSec)
{
	UpdateEntityTransforms();
}

void SceneTesterHud::HandleToggleHelpButtonReleased (SD::ButtonComponent* button)
{
	ControlsFrame->SetVisibility(!ControlsFrame->IsVisible());
	RecomputePositions();
}

void SceneTesterHud::HandleToggleAttributesButtonReleased (SD::ButtonComponent* button)
{
	EntityDataFrame->SetVisibility(!EntityDataFrame->IsVisible());
	RecomputePositions();
}

void SceneTesterHud::HandleToggleDescriptionButtonReleased (SD::ButtonComponent* button)
{
	TestDescriptionScrollbar->SetVisibility(!TestDescriptionScrollbar->IsVisible());
	RecomputePositions();
}

void SceneTesterHud::HandlePlayerMoveUpButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.Y = -1.f;
	}
}

void SceneTesterHud::HandlePlayerMoveUpButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.Y = 0.f;
	}
}

void SceneTesterHud::HandlePlayerMoveRightButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.X = 1.f;
	}
}

void SceneTesterHud::HandlePlayerMoveRightButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.X = 0.f;
	}
}

void SceneTesterHud::HandlePlayerMoveDownButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.Y = 1.f;
	}
}

void SceneTesterHud::HandlePlayerMoveDownButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.Y = 0.f;
	}
}

void SceneTesterHud::HandlePlayerMoveLeftButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.X = -1.f;
	}
}

void SceneTesterHud::HandlePlayerMoveLeftButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->VelocityInput.X = 0.f;
	}
}

void SceneTesterHud::HandlePlayerIncreaseSizeButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->GrowthInput.X = 1.f;
		PlayerTester->GrowthInput.Y = 1.f;
		PlayerTester->GrowthInput.Z = 1.f;
	}
}

void SceneTesterHud::HandlePlayerIncreaseSizeButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->GrowthInput = SD::Vector3::ZERO_VECTOR;
	}
}

void SceneTesterHud::HandlePlayerDecreaseSizeButtonPressed (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->GrowthInput.X = -1.f;
		PlayerTester->GrowthInput.Y = -1.f;
		PlayerTester->GrowthInput.Z = -1.f;
	}
}

void SceneTesterHud::HandlePlayerDecreaseSizeButtonReleased (SD::ButtonComponent* button)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->GrowthInput = SD::Vector3::ZERO_VECTOR;
	}
}

void SceneTesterHud::HandleCameraMoveUpButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.Y = -1.f;
	}
}

void SceneTesterHud::HandleCameraMoveUpButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.Y = 0.f;
	}
}

void SceneTesterHud::HandleCameraMoveRightButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.X = 1.f;
	}
}

void SceneTesterHud::HandleCameraMoveRightButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.X = 0.f;
	}
}

void SceneTesterHud::HandleCameraMoveDownButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.Y = 1.f;
	}
}

void SceneTesterHud::HandleCameraMoveDownButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.Y = 0.f;
	}
}

void SceneTesterHud::HandleCameraMoveLeftButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.X = -1.f;
	}
}

void SceneTesterHud::HandleCameraMoveLeftButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraVelocityInput.X = 0.f;
	}
}

void SceneTesterHud::HandleCameraZoomInButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraZoomInput = 1.f;
	}
}

void SceneTesterHud::HandleCameraZoomInButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraZoomInput = 0.f;
	}
}

void SceneTesterHud::HandleCameraZoomOutButtonPressed (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraZoomInput = -1.f;
	}
}

void SceneTesterHud::HandleCameraZoomOutButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->CameraZoomInput = 0.f;
	}
}

SD_TESTER_END

#endif