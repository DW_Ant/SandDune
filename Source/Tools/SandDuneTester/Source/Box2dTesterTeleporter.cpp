/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTesterTeleporter.cpp
=====================================================================
*/

#include "Box2dTesterTeleporter.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::Box2dTesterTeleporter, SD::SceneEntity)
SD_TESTER_BEGIN

void Box2dTesterTeleporter::InitProps ()
{
	Super::InitProps();

	PreserveXCoordinates = false;
	PreserveYCoordinates = false;
	TeleportDestination = SD::Vector3::ZERO_VECTOR;

	PhysComp = nullptr;
}

void Box2dTesterTeleporter::BeginObject ()
{
	Super::BeginObject();

	PhysComp = SD::Box2dComponent::CreateObject();
	if (AddComponent(PhysComp))
	{
		PhysComp->OnBeginContact = SDFUNCTION_2PARAM(this, Box2dTesterTeleporter, HandleBeginContact, void, SD::Box2dComponent*, b2Contact*);
	}
}

void Box2dTesterTeleporter::HandleBeginContact (SD::Box2dComponent* otherComp, b2Contact* contactData)
{
	SD::SceneTransform* transform = dynamic_cast<SD::SceneTransform*>(otherComp->GetOwner());
	CHECK(transform != nullptr) //Box2dComponent should always be attached to a SceneTransform.

	SD::Vector3 destination(TeleportDestination);
	if (PreserveXCoordinates)
	{
		destination.X = transform->ReadTranslation().X;
	}

	if (PreserveYCoordinates)
	{
		destination.Y = transform->ReadTranslation().Y;
	}

	transform->SetTranslation(destination);
	otherComp->OverwritePosition = true;
}

SD_TESTER_END

#endif