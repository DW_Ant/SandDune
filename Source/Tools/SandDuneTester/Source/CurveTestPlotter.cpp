/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveTestPlotter.cpp
=====================================================================
*/

#include "CurveTestPlotter.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::CurveTestPlotter, SD::GuiEntity)
SD_TESTER_BEGIN

void CurveTestPlotter::InitProps ()
{
	Super::InitProps();

	DrawMode = DM_Free;
	bIsDragging = false;
	BezierStart = SD::Vector2::ZERO_VECTOR;
	BezierEnd = SD::Vector2::ZERO_VECTOR;
	BezierCtrl1 = SD::Vector2::ZERO_VECTOR;
	BezierCtrl2 = SD::Vector2::ZERO_VECTOR;
	CtrledPt = nullptr;
	BezierPreviewSegment = 32.f;
	BezierPreviewStep = 0.075f;
	CurveThickness = 6.f;
	BezierSegment = 16.f;
	BezierStep = 0.01f;

	LeftPanel = nullptr;
	CurveThickField = nullptr;
	TextureCheckbox = nullptr;
	RedField = nullptr;
	GreenField = nullptr;
	BlueField = nullptr;
	FreeDrawButton = nullptr;
	BezierDrawButton = nullptr;
	SegLengthField = nullptr;
	StepField = nullptr;
	ClearLineButton = nullptr;
	DrawFrame = nullptr;
	CurveComp = nullptr;
	CurveDrawHelper = nullptr;
}

void CurveTestPlotter::ConstructUI ()
{
	Super::ConstructUI();

	ConstructLeftPanel();
	ConstructDrawPanel();
}

void CurveTestPlotter::HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove)
{
	Super::HandleMouseMove(mouse, sfmlEvent, deltaMove);

	if (bIsDragging && CurveDrawHelper != nullptr && DrawFrame != nullptr)
	{
		if (DrawFrame->IsWithinBounds(SD::Vector2(sfmlEvent.x, sfmlEvent.y)))
		{
			SD::Vector2 curvePos = GetCurveCoordinates(mouse);

			std::vector<SD::Vector2> drawPts = CurveDrawHelper->GetPoints();
			if (drawPts.size() < 2)
			{
				return;
			}

			//Move the draw helper's end point
			drawPts.at(1) = curvePos;
			CurveDrawHelper->GenerateCurve(drawPts, CurveDrawHelper->GetCurveThickness());

			if (DrawMode == DM_Bezier && CurveComp != nullptr && CtrledPt != nullptr && !BezierEnd.IsEmpty())
			{
				*CtrledPt = curvePos;

				SD::Float segmentLength = SD::Utils::Max(BezierSegment, BezierPreviewSegment);
				SD::Float stepAmount = SD::Utils::Max(BezierStep, BezierPreviewStep);
				CurveComp->GenerateBezierCurve(BezierStart, BezierEnd, BezierCtrl1, BezierCtrl2, segmentLength, CurveThickness, stepAmount);
			}
		}
	}
}

void CurveTestPlotter::HandlePassiveMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::HandlePassiveMouseClick(mouse, sfmlEvent, eventType);

	if (!bIsDragging || sfmlEvent.button != sf::Mouse::Left || eventType != sf::Event::MouseButtonReleased)
	{
		return;
	}

	bIsDragging = false;

	if (CurveComp != nullptr)
	{
		if (CtrledPt != nullptr)
		{
			//Update the controlled point (handles case where the mouse didn't move between pressed and released).
			*CtrledPt = GetCurveCoordinates(mouse);

			if (CtrledPt == &BezierEnd)
			{
				//When the second end point is set, default the control points to the end points.
				BezierCtrl1 = BezierStart;
				BezierCtrl2 = BezierEnd;
			}
		}

		if (DrawMode == DM_Free)
		{
			//Simply append the new point to the curve.
			std::vector<SD::Vector2> newPoints = CurveComp->GetPoints();
			newPoints.emplace_back(GetCurveCoordinates(mouse));
			CurveComp->GenerateCurve(newPoints, CurveThickness);
		}
		else if (!BezierEnd.IsEmpty()) //If both end points are placed for bezier curve
		{
			CurveComp->GenerateBezierCurve(BezierStart, BezierEnd, BezierCtrl1, BezierCtrl2, BezierSegment, CurveThickness, BezierStep);
		}

		if (CurveDrawHelper != nullptr)
		{
			CurveDrawHelper->SetVisibility(false);
		}
	}
}

bool CurveTestPlotter::HandleConsumableMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonPressed && CurveComp != nullptr)
	{
		if (DrawFrame != nullptr && DrawFrame->IsWithinBounds(SD::Vector2(sfmlEvent.x, sfmlEvent.y)))
		{
			bIsDragging = true;

			bool hasStartPt = false;
			SD::Vector2 startHelperPt;
			if (DrawMode == DM_Free)
			{
				if (!SD::ContainerUtils::IsEmpty(CurveComp->ReadPoints()))
				{
					startHelperPt = SD::ContainerUtils::GetLast(CurveComp->ReadPoints());
					hasStartPt = true;
				}
			}
			else
			{
				//Identify which point is being configured in the bezier curve
				if (CtrledPt == nullptr)
				{
					CtrledPt = &BezierStart;
				}
				else if (CtrledPt == &BezierStart)
				{
					//Control end point next
					CtrledPt = &BezierEnd;
					startHelperPt = BezierStart;
					hasStartPt = true;
				}
				else if (CtrledPt == &BezierEnd)
				{
					CtrledPt = &BezierCtrl1;
					startHelperPt = BezierStart;
					hasStartPt = true;
				}
				else if (CtrledPt == &BezierCtrl1)
				{
					CtrledPt = &BezierCtrl2;
					startHelperPt = BezierEnd;
					hasStartPt = true;
				}
				else
				{
					CtrledPt = nullptr;
					bIsDragging = false;
				}
			}

			if (hasStartPt && CurveDrawHelper != nullptr)
			{
				SD::Vector2 curvePos = GetCurveCoordinates(mouse);
				CurveDrawHelper->SetVisibility(true);
				std::vector<SD::Vector2> pts{startHelperPt, curvePos};
				CurveDrawHelper->GenerateCurve(pts, CurveThickness);
			}

			return true;
		}
	}

	//Relay input event to components
	return Super::HandleConsumableMouseClick(mouse, sfmlEvent, eventType);
}


void CurveTestPlotter::ConstructLeftPanel ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString transFileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("CurveTestPlotter");

	LeftPanel = SD::FrameComponent::CreateObject();
	if (AddComponent(LeftPanel))
	{
		LeftPanel->SetPosition(SD::Vector2::ZERO_VECTOR);
		LeftPanel->SetSize(SD::Vector2(0.25f, 1.f));
		LeftPanel->SetBorderThickness(2.f);

		SD::Float optionHeight = 0.05f;
		SD::Float vertSpacing = 0.01f;
		SD::Float sideMargin = 0.025f;
		SD::Float posY = vertSpacing;
		SD::Float promptWidth = 0.7f;
		SD::Float fieldWidth = 1.f - ((sideMargin * 2.f) + promptWidth + 0.01f); //0.01 to add a little buffer between prompt and field.

		std::function<bool(SD::LabelComponent*&, SD::FrameComponent*&, SD::TextFieldComponent*&)> createPrompt([&](SD::LabelComponent*& outPrompt, SD::FrameComponent*& outFieldBackground, SD::TextFieldComponent*& outTextField)
		{
			outPrompt = SD::LabelComponent::CreateObject();
			if (LeftPanel->AddComponent(outPrompt))
			{
				outPrompt->SetAutoRefresh(false);
				outPrompt->SetPosition(SD::Vector2(sideMargin, posY));
				outPrompt->SetSize(SD::Vector2(promptWidth, optionHeight));
				outPrompt->SetWrapText(false);
				outPrompt->SetClampText(false);
				outPrompt->SetVerticalAlignment(SD::LabelComponent::VA_Center);
				outPrompt->SetAutoRefresh(true);
			}

			outFieldBackground = SD::FrameComponent::CreateObject();
			if (LeftPanel->AddComponent(outFieldBackground))
			{
				outFieldBackground->SetPosition(SD::Vector2(0.f, posY));
				outFieldBackground->SetSize(SD::Vector2(fieldWidth, optionHeight));
				outFieldBackground->SetAnchorRight(sideMargin);
				outFieldBackground->SetCenterColor(SD::Color(48, 48, 48, 128));
				outFieldBackground->SetBorderThickness(0.f);
				if (SD::BorderRenderComponent* borderComp = outFieldBackground->GetBorderComp())
				{
					borderComp->Destroy();
				}

				outTextField = SD::TextFieldComponent::CreateObject();
				if (outFieldBackground->AddComponent(outTextField))
				{
					outTextField->SetAutoRefresh(false);
					outTextField->SetPosition(SD::Vector2::ZERO_VECTOR);
					outTextField->SetSize(SD::Vector2(1.f, 1.f));
					outTextField->SetWrapText(true);
					outTextField->SetClampText(false);
					outTextField->SetVerticalAlignment(SD::LabelComponent::VA_Center);
					outTextField->MaxNumCharacters = 8;
					outTextField->SetAutoRefresh(true);
				}
			}

			posY += (optionHeight + vertSpacing);

			return (outPrompt != nullptr && outTextField != nullptr);
		});

		//Sets the button state to a consistent theme for this GuiEntity (don't use GuiTheme since that theme's button uses the same texture as the frame component).
		std::function<void(SD::ButtonComponent*)> initButtonState([](SD::ButtonComponent* button)
		{
			if (SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(button->ReplaceStateComponent(SD::ColorButtonState::SStaticClass())))
			{
				colorState->SetDefaultColor(SD::Color(200, 150, 19));
				colorState->SetHoverColor(SD::Color(220, 160, 22));
				colorState->SetDownColor(SD::Color(188, 130, 15));
				colorState->SetDisabledColor(SD::Color(81, 56, 6));
			}
		});

		SD::LabelComponent* prompt = nullptr;
		SD::FrameComponent* background = nullptr;
		if (createPrompt(OUT prompt, OUT background, OUT CurveThickField))
		{
			prompt->SetText(translator->TranslateText(TXT("CurveThickness"), transFileName, sectionName));
			CurveThickField->SetText(CurveThickness.ToFormattedString(1, 2, SD::Float::TM_Round));
			CurveThickField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			CurveThickField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptCurveThickness, void, SD::TextFieldComponent*);
		}

		TextureCheckbox = SD::CheckboxComponent::CreateObject();
		if (LeftPanel->AddComponent(TextureCheckbox))
		{
			TextureCheckbox->SetPosition(SD::Vector2(sideMargin, posY));
			TextureCheckbox->SetSize(SD::Vector2(1.f - (sideMargin * 2.f), optionHeight));
			TextureCheckbox->SetChecked(true);
			TextureCheckbox->OnChecked = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleTextureChecked, void, SD::CheckboxComponent*);
			if (SD::LabelComponent* caption = TextureCheckbox->GetCaptionComponent())
			{
				caption->SetText(translator->TranslateText(TXT("TextureCheckbox"), transFileName, sectionName));
			}

			posY += vertSpacing + optionHeight;
		}

		if (createPrompt(OUT prompt, OUT background, OUT RedField))
		{
			prompt->SetText(translator->TranslateText(TXT("CurveColorR"), transFileName, sectionName));
			background->SetCenterColor(SD::Color(128, 24, 24, 96));

			RedField->SetText(TXT("255"));
			RedField->MaxNumCharacters = 3;
			RedField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			RedField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptRed, void, SD::TextFieldComponent*);
		}

		if (createPrompt(OUT prompt, OUT background, OUT GreenField))
		{
			prompt->SetText(translator->TranslateText(TXT("CurveColorG"), transFileName, sectionName));
			background->SetCenterColor(SD::Color(24, 128, 24, 96));

			GreenField->SetText(TXT("255"));
			GreenField->MaxNumCharacters = 3;
			GreenField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			GreenField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptGreen, void, SD::TextFieldComponent*);
		}

		if (createPrompt(OUT prompt, OUT background, OUT BlueField))
		{
			prompt->SetText(translator->TranslateText(TXT("CurveColorB"), transFileName, sectionName));
			background->SetCenterColor(SD::Color(24, 24, 128, 96));

			BlueField->SetText(TXT("255"));
			BlueField->MaxNumCharacters = 3;
			BlueField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			BlueField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptBlue, void, SD::TextFieldComponent*);
		}

		FreeDrawButton = SD::ButtonComponent::CreateObject();
		if (LeftPanel->AddComponent(FreeDrawButton))
		{
			FreeDrawButton->SetPosition(SD::Vector2(sideMargin, posY));
			FreeDrawButton->SetSize(SD::Vector2(0.45f, optionHeight * 2.f));
			FreeDrawButton->SetEnabled(DrawMode == DM_Bezier);
			FreeDrawButton->SetCaptionText(translator->TranslateText(TXT("FreeDrawButton"), transFileName, sectionName));
			FreeDrawButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleFreeDrawReleased, void, SD::ButtonComponent*));
			initButtonState(FreeDrawButton);
		}

		BezierDrawButton = SD::ButtonComponent::CreateObject();
		if (LeftPanel->AddComponent(BezierDrawButton))
		{
			BezierDrawButton->SetPosition(SD::Vector2(0.f, posY));
			BezierDrawButton->SetSize(SD::Vector2(0.45f, optionHeight * 2.f));
			BezierDrawButton->SetAnchorRight(sideMargin);
			BezierDrawButton->SetEnabled(DrawMode == DM_Free);
			BezierDrawButton->SetCaptionText(translator->TranslateText(TXT("BezierDrawButton"), transFileName, sectionName));
			BezierDrawButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleBezierDrawReleased, void, SD::ButtonComponent*));
			initButtonState(BezierDrawButton);
		}

		posY += ((optionHeight*2.f) + vertSpacing);

		if (createPrompt(OUT prompt, OUT background, OUT SegLengthField))
		{
			prompt->SetText(translator->TranslateText(TXT("SegmentLengthPrompt"), transFileName, sectionName));
			SegLengthField->SetEditable(DrawMode == DM_Bezier);
			SegLengthField->SetText(BezierSegment.ToFormattedString(1, 2, SD::Float::TM_Round));
			SegLengthField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			SegLengthField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptSegLength, void, SD::TextFieldComponent*);
		}

		if (createPrompt(OUT prompt, OUT background, OUT StepField))
		{
			prompt->SetText(translator->TranslateText(TXT("StepPrompt"), transFileName, sectionName));
			StepField->SetEditable(DrawMode == DM_Bezier);
			StepField->SetText(BezierStep.ToFormattedString(1, 2, SD::Float::TM_Round));
			StepField->OnAllowTextInput = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAllowNumericInput, bool, const SD::DString&);
			StepField->OnReturn = SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleAcceptStep, void, SD::TextFieldComponent*);
		}

		ClearLineButton = SD::ButtonComponent::CreateObject();
		if (LeftPanel->AddComponent(ClearLineButton))
		{
			ClearLineButton->SetSize(SD::Vector2(1.f, optionHeight));
			ClearLineButton->SetAnchorRight(sideMargin);
			ClearLineButton->SetAnchorLeft(sideMargin);
			ClearLineButton->SetAnchorBottom(vertSpacing * 2.f);
			ClearLineButton->SetCaptionText(translator->TranslateText(TXT("ClearLineButton"), transFileName, sectionName));
			ClearLineButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, CurveTestPlotter, HandleClearLineReleased, void, SD::ButtonComponent*));
			initButtonState(ClearLineButton);
		}
	}
}

void CurveTestPlotter::ConstructDrawPanel ()
{
	DrawFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(DrawFrame))
	{
		SD::Float leftPanelWidth = 0.f;
		if (LeftPanel != nullptr)
		{
			leftPanelWidth = LeftPanel->ReadSize().X;
			MoveComponentToBack(LeftPanel); //Move LeftPanel to the back in order to render it in front of the DrawFrame and the curves within it (for those really wide curves).
		}

		DrawFrame->SetPosition(SD::Vector2(leftPanelWidth, 0.f));
		DrawFrame->SetSize(SD::Vector2(1.f - leftPanelWidth, 1.f));
		DrawFrame->SetBorderThickness(3.f);
		
		if (SD::SpriteComponent* spriteComp = DrawFrame->GetCenterCompAs<SD::SpriteComponent>())
		{
			if (sf::Sprite* sprite = spriteComp->GetSprite())
			{
				sprite->setColor(sf::Color(64, 64, 64)); //Darken the background
			}
		}

		CurveComp = SD::CurveRenderComponent::CreateObject();
		if (DrawFrame->AddComponent(CurveComp))
		{
			CurveComp->SetCurveColor(sf::Color::White);
			CurveTexture = CurveComp->GetCurveTexture();
		}

		CurveDrawHelper = SD::CurveRenderComponent::CreateObject();
		if (DrawFrame->AddComponent(CurveDrawHelper))
		{
			CurveDrawHelper->SetCurveColor(sf::Color(96, 96, 96, 128));
			CurveDrawHelper->SetVisibility(false);
		}
	}
}

SD::Vector2 CurveTestPlotter::GetCurveCoordinates (SD::MousePointer* mouse)
{
	CHECK(mouse != nullptr && DrawFrame != nullptr)

	return (mouse->ReadPosition() - DrawFrame->ReadCachedAbsPosition());
}

void CurveTestPlotter::ResetLine ()
{
	CtrledPt = nullptr;
	BezierStart = SD::Vector2::ZERO_VECTOR;
	BezierEnd = SD::Vector2::ZERO_VECTOR;
	BezierCtrl1 = SD::Vector2::ZERO_VECTOR;
	BezierCtrl2 = SD::Vector2::ZERO_VECTOR;

	if (CurveComp != nullptr)
	{
		std::vector<SD::Vector2> emptyPts;
		CurveComp->GenerateCurve(emptyPts, CurveThickness);
	}
}

bool CurveTestPlotter::HandleAllowNumericInput (const SD::DString& txt)
{
	//Only allow positive numbers
	return txt.HasRegexMatch(TXT("[0-9.]"));
}

void CurveTestPlotter::HandleAcceptCurveThickness (SD::TextFieldComponent* uiComponent)
{
	CurveThickness = uiComponent->GetContent().Stof();
}

void CurveTestPlotter::HandleTextureChecked (SD::CheckboxComponent* checkbox)
{
	CHECK(checkbox != nullptr)
	if (CurveComp == nullptr || CurveDrawHelper == nullptr)
	{
		return;
	}

	if (checkbox->IsChecked())
	{
		CurveComp->SetCurveTexture(CurveTexture.Get());
		CurveDrawHelper->SetCurveTexture(CurveTexture.Get());
	}
	else
	{
		CurveComp->SetCurveTexture(nullptr);
		CurveDrawHelper->SetCurveTexture(nullptr);
	}
}

void CurveTestPlotter::HandleAcceptRed (SD::TextFieldComponent* uiComponent)
{
	SD::Int redValue = uiComponent->GetContent().Atoi();
	redValue = SD::Utils::Clamp<SD::Int>(redValue, 0, 255);

	if (CurveComp != nullptr)
	{
		sf::Color newColor = CurveComp->GetCurveColor();
		newColor.r = redValue.Value;
		CurveComp->SetCurveColor(newColor);
	}
}

void CurveTestPlotter::HandleAcceptGreen (SD::TextFieldComponent* uiComponent)
{
	SD::Int greenValue = uiComponent->GetContent().Atoi();
	greenValue = SD::Utils::Clamp<SD::Int>(greenValue, 0, 255);

	if (CurveComp != nullptr)
	{
		sf::Color newColor = CurveComp->GetCurveColor();
		newColor.g = greenValue.Value;
		CurveComp->SetCurveColor(newColor);
	}
}

void CurveTestPlotter::HandleAcceptBlue (SD::TextFieldComponent* uiComponent)
{
	SD::Int blueValue = uiComponent->GetContent().Atoi();
	blueValue = SD::Utils::Clamp<SD::Int>(blueValue, 0, 255);

	if (CurveComp != nullptr)
	{
		sf::Color newColor = CurveComp->GetCurveColor();
		newColor.b = blueValue.Value;
		CurveComp->SetCurveColor(newColor);
	}
}

void CurveTestPlotter::HandleAcceptSegLength (SD::TextFieldComponent* uiComponent)
{
	BezierSegment = uiComponent->GetContent().Stof();
	BezierSegment = SD::Utils::Max<SD::Float>(BezierSegment, 1.f);
}

void CurveTestPlotter::HandleAcceptStep (SD::TextFieldComponent* uiComponent)
{
	BezierStep = uiComponent->GetContent().Stof();
	BezierStep = SD::Utils::Clamp<SD::Float>(BezierStep, 0.0001f, 1.f);
}

void CurveTestPlotter::HandleFreeDrawReleased (SD::ButtonComponent* button)
{
	DrawMode = DM_Free;
	ResetLine();

	if (FreeDrawButton != nullptr)
	{
		FreeDrawButton->SetEnabled(false);
	}

	if (BezierDrawButton != nullptr)
	{
		BezierDrawButton->SetEnabled(true);
	}

	if (SegLengthField != nullptr)
	{
		SegLengthField->SetEditable(false);
	}

	if (StepField != nullptr)
	{
		StepField->SetEditable(false);
	}
}

void CurveTestPlotter::HandleBezierDrawReleased (SD::ButtonComponent* button)
{
	DrawMode = DM_Bezier;
	ResetLine();

	if (FreeDrawButton != nullptr)
	{
		FreeDrawButton->SetEnabled(true);
	}

	if (BezierDrawButton != nullptr)
	{
		BezierDrawButton->SetEnabled(false);
	}

	if (SegLengthField != nullptr)
	{
		SegLengthField->SetEditable(true);
	}

	if (StepField != nullptr)
	{
		StepField->SetEditable(true);
	}
}

void CurveTestPlotter::HandleClearLineReleased (SD::ButtonComponent* button)
{
	ResetLine();
}
SD_TESTER_END
#endif