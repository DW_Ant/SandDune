/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  FeatureTestLauncher.cpp
=====================================================================
*/

#include "Box2dTester.h"
#include "CurveTester.h"
#include "EditorTester.h"
#include "FeatureTestLauncher.h"
#include "GraphEditorTester.h"
#include "InputTester.h"
#include "PhysicsTester.h"
#include "QaUnitTester.h"
#include "SandDuneTester.h"
#include "SceneTester.h"
#include "ScreenCaptureTester.h"
#include "ShaderTester.h"
#include "SpriteInstanceTester.h"
#include "SpriteTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::FeatureTestLauncher, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

void FeatureTestLauncher::InitProps ()
{
	Super::InitProps();

	Box2dTester = nullptr;
	CurveTester = nullptr;
	EditorTest = nullptr;
	GraphEditorTest = nullptr;
	InputTest = nullptr;
	PhysicsTest = nullptr;
	SceneTester = nullptr;
	ScreenCaptureTester = nullptr;
	ShaderTester = nullptr;
	SpriteInstTester = nullptr;
	SpriteTester = nullptr;

	LauncherUi = nullptr;
	Box2dTestButton = nullptr;
	CurveTestButton = nullptr;
	EditorTesterButton = nullptr;
	GraphEditorTesterButton = nullptr;
	InputTesterButton = nullptr;
	PhysicsTesterButton = nullptr;
	SceneTestButton = nullptr;
	ScreenCaptureTestButton = nullptr;
	ShaderTesterButton = nullptr;
	SpriteInstanceButton = nullptr;
	SpriteTestButton = nullptr;
	ShutdownEngineOnDestruction = false;
}

void FeatureTestLauncher::Destroyed ()
{
	if (Box2dTester != nullptr)
	{
		Box2dTester->Destroy();
		Box2dTester = nullptr;
	}

	if (CurveTester != nullptr)
	{
		CurveTester->Destroy();
		CurveTester = nullptr;
	}

	if (EditorTest != nullptr)
	{
		EditorTest->Destroy();
		EditorTest = nullptr;
	}

	if (GraphEditorTest != nullptr)
	{
		GraphEditorTest->Destroy();
		GraphEditorTest = nullptr;
	}

	if (InputTest != nullptr)
	{
		InputTest->Destroy();
		InputTest = nullptr;
	}

	if (PhysicsTest != nullptr)
	{
		PhysicsTest->Destroy();
		PhysicsTest = nullptr;
	}

	if (SceneTester != nullptr)
	{
		SceneTester->Destroy();
		SceneTester = nullptr;
	}

	if (ScreenCaptureTester != nullptr)
	{
		ScreenCaptureTester->Destroy();
		ScreenCaptureTester = nullptr;
	}

	if (ShaderTester != nullptr)
	{
		ShaderTester->Destroy();
		ShaderTester = nullptr;
	}

	if (SpriteInstTester != nullptr)
	{
		SpriteInstTester->Destroy();
		SpriteInstTester = nullptr;
	}

	if (SpriteTester != nullptr)
	{
		SpriteTester->Destroy();
		SpriteTester = nullptr;
	}

	if (LauncherUi != nullptr)
	{
		LauncherUi->Destroy();
		LauncherUi = nullptr;

		//Components within a destroyed Entity are also destroyed. Simply set them to null.
		ShaderTesterButton = nullptr;
		SpriteInstanceButton = nullptr;
	}

	if (ShutdownEngineOnDestruction)
	{
		SD::Engine* localEngine = SD::Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
	}

	Super::Destroyed();
}

void FeatureTestLauncher::InitializeLauncher (const QaUnitTester* tester, SD::UnitTester::EUnitTestFlags testFlags, bool shutdownOnDestruction)
{
	CHECK(!IsInitialized())
	QaTester = tester;
	TestFlags = testFlags;
	ShutdownEngineOnDestruction = shutdownOnDestruction;

	const float numButtons = 12.f;
	const float buttonSpace = 36.f;
	const SD::Vector2 windowSize(256.f, (buttonSpace * numButtons) + 12.f);
	InitializeEntityManager(windowSize, TXT("Unit Test Launcher"));

	InitializeLauncherUi();
}

void FeatureTestLauncher::InitializeLauncherUi ()
{
	LauncherUi = SD::GuiEntity::CreateObject();
	GuiDrawLayer->RegisterMenu(LauncherUi);
	LauncherUi->SetupInputComponent(Input.Get(), 100);
	LauncherUi->SetPosition(SD::Vector2::ZERO_VECTOR);
	LauncherUi->SetSize(SD::Vector2(1.f, 1.f));

	SD::FocusComponent* focusComp = LauncherUi->GetFocus();
	SD::ContainerUtils::Empty(OUT focusComp->TabOrder);

	SD::Float buttonPadding = 12.f;
	SD::Float buttonHeight = 24.f;
	SD::Float buttonPos = buttonPadding;

	std::function<void(SD::ButtonComponent*)> initButton([&](SD::ButtonComponent* button)
	{
		button->SetSize(0.f, buttonHeight);
		button->SetPosition(0.f, buttonPos);
		button->SetAnchorLeft(buttonPadding);
		button->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		focusComp->TabOrder.push_back(button);
	});

	Box2dTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(Box2dTestButton))
	{
		initButton(Box2dTestButton);
		Box2dTestButton->SetCaptionText(TXT("Launch Box 2D Test"));
		Box2dTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchBox2dTester, void, SD::ButtonComponent*));
	}

	CurveTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(CurveTestButton))
	{
		initButton(CurveTestButton);
		CurveTestButton->SetCaptionText(TXT("Launch Curve Test"));
		CurveTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchCurveTester, void, SD::ButtonComponent*));
	}

	EditorTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(EditorTesterButton))
	{
		initButton(EditorTesterButton);
		EditorTesterButton->SetCaptionText(TXT("Launch Editor Test"));
		EditorTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchEditorTester, void, SD::ButtonComponent*));
	}

	GraphEditorTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(GraphEditorTesterButton))
	{
		initButton(GraphEditorTesterButton);
		GraphEditorTesterButton->SetCaptionText(TXT("Launch Graph Editor Test"));
		GraphEditorTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchGraphEditorTester, void, SD::ButtonComponent*));
	}

	InputTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(InputTesterButton))
	{
		initButton(InputTesterButton);
		InputTesterButton->SetCaptionText(TXT("Launch Input Test"));
		InputTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchInputTester, void, SD::ButtonComponent*));
	}

	PhysicsTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(PhysicsTesterButton))
	{
		initButton(PhysicsTesterButton);
		PhysicsTesterButton->SetCaptionText(TXT("Launch Physics Test"));
		PhysicsTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchPhysicsTester, void, SD::ButtonComponent*));
	}

	SceneTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(SceneTestButton))
	{
		initButton(SceneTestButton);
		SceneTestButton->SetCaptionText(TXT("Launch Scene Test"));
		SceneTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchSceneTester, void, SD::ButtonComponent*));
	}

	ScreenCaptureTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(ScreenCaptureTestButton))
	{
		initButton(ScreenCaptureTestButton);
		ScreenCaptureTestButton->SetCaptionText(TXT("Launch Screen Capture Test"));
		ScreenCaptureTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchScreenCaptureTester, void, SD::ButtonComponent*));
	}

	ShaderTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(ShaderTesterButton))
	{
		initButton(ShaderTesterButton);
		ShaderTesterButton->SetCaptionText(TXT("Launch Shader Test"));
		ShaderTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchShaderTester, void, SD::ButtonComponent*));
	}

	SpriteInstanceButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(SpriteInstanceButton))
	{
		initButton(SpriteInstanceButton);
		SpriteInstanceButton->SetCaptionText(TXT("Launch Sprite Instance Test"));
		SpriteInstanceButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchSpriteInstanceTester, void, SD::ButtonComponent*));
	}

	SpriteTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(SpriteTestButton))
	{
		initButton(SpriteTestButton);
		SpriteTestButton->SetCaptionText(TXT("Launch Sprite Test"));
		SpriteTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchSpriteTester, void, SD::ButtonComponent*));
	}

	SD::ButtonComponent* closeButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(closeButton))
	{
		initButton(closeButton);
		closeButton->SetCaptionText(TXT("Close"));
		closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleCloseButtonReleased, void, SD::ButtonComponent*));
	}
}

void FeatureTestLauncher::HandleLaunchBox2dTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	Box2dTester = Box2dTester::CreateObject();
	CHECK(Box2dTester != nullptr)
	Box2dTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleBox2dTesterTerminated, void);
	Box2dTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("Box 2D is created. This test will automatically terminate based on user input."));

	if (Box2dTestButton != nullptr)
	{
		Box2dTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleBox2dTesterTerminated ()
{
	Box2dTester = nullptr;
	if (Box2dTestButton != nullptr)
	{
		Box2dTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchCurveTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	CurveTester = CurveTester::CreateObject();
	CHECK(CurveTester != nullptr)
	CurveTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleCurveTesterTerminated, void);
	CurveTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("Curve Tester is created. This test will automatically terminate based on user input."));

	if (CurveTestButton != nullptr)
	{
		CurveTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleCurveTesterTerminated ()
{
	CurveTester = nullptr;
	if (CurveTestButton != nullptr)
	{
		CurveTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchEditorTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	EditorTest = EditorTester::CreateObject();
	CHECK(EditorTest != nullptr)
	EditorTest->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleEditorTesterTerminated, void);
	EditorTest->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("EditorTester is created. This test will automatically terminate based on user input."));

	if (EditorTesterButton != nullptr)
	{
		EditorTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleEditorTesterTerminated ()
{
	EditorTest = nullptr;
	if (EditorTesterButton != nullptr)
	{
		EditorTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchGraphEditorTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	GraphEditorTest = GraphEditorTester::CreateObject();
	CHECK(GraphEditorTest != nullptr)
	GraphEditorTest->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleGraphEditorTesterTerminated, void);
	GraphEditorTest->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("GraphEditorTester is created. This test will automatically terminate based on user input."));

	if (GraphEditorTesterButton != nullptr)
	{
		GraphEditorTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleGraphEditorTesterTerminated ()
{
	GraphEditorTest = nullptr;
	if (GraphEditorTesterButton != nullptr)
	{
		GraphEditorTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchInputTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	InputTest = InputTester::CreateObject();
	CHECK(InputTest != nullptr)
	InputTest->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleInputTesterTerminated, void);
	InputTest->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("InputTest is created. This test will automatically terminate based on user input."));

	if (InputTesterButton != nullptr)
	{
		InputTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleInputTesterTerminated ()
{
	InputTest = nullptr;
	if (InputTesterButton != nullptr)
	{
		InputTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchPhysicsTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	PhysicsTest = PhysicsTester::CreateObject();
	CHECK(PhysicsTest != nullptr)
	PhysicsTest->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandlePhysicsTesterTerminated, void);
	PhysicsTest->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("PhysicsTester is created. This test will automatically terminate based on user input."));

	if (PhysicsTesterButton != nullptr)
	{
		PhysicsTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandlePhysicsTesterTerminated ()
{
	PhysicsTest = nullptr;
	if (PhysicsTesterButton != nullptr)
	{
		PhysicsTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchSceneTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	SceneTester = SceneTester::CreateObject();
	CHECK(SceneTester != nullptr)
	SceneTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleSceneTesterTerminated, void);
	SceneTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("SceneTester is created. This test will automatically terminate based on user input."));

	if (SceneTestButton != nullptr)
	{
		SceneTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleSceneTesterTerminated ()
{
	SceneTester = nullptr;
	if (SceneTestButton != nullptr)
	{
		SceneTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchScreenCaptureTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	ScreenCaptureTester = ScreenCaptureTester::CreateObject();
	CHECK(ScreenCaptureTester != nullptr)
	ScreenCaptureTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleScreenCaptureTesterTerminated, void);
	ScreenCaptureTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("ScreenCaptureTester is created. This test will automatically terminate based on user input."));

	if (ScreenCaptureTestButton != nullptr)
	{
		ScreenCaptureTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleScreenCaptureTesterTerminated ()
{
	ScreenCaptureTester = nullptr;
	if (ScreenCaptureTestButton != nullptr)
	{
		ScreenCaptureTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchShaderTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	ShaderTester = ShaderTester::CreateObject();
	CHECK(ShaderTester != nullptr)
	ShaderTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleShaderTesterTerminated, void);
	QaTester->TestLog(TestFlags, TXT("ShaderTester created.  Entity will automatically perish when user clicks on close button."));

	if (ShaderTesterButton != nullptr)
	{
		ShaderTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleShaderTesterTerminated ()
{
	ShaderTester = nullptr;
	if (ShaderTesterButton != nullptr)
	{
		ShaderTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchSpriteInstanceTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	SpriteInstTester = SpriteInstanceTester::CreateObject();
	CHECK(SpriteInstTester != nullptr)
	SpriteInstTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleSpriteInstanceTesterTerminated, void);
	SpriteInstTester->InitializeTest(QaTester, TestFlags);
	QaTester->TestLog(TestFlags, TXT("Sprite Instance Tester created.  Entity will automatically perish when user clicks on close button."));

	if (SpriteInstanceButton != nullptr)
	{
		SpriteInstanceButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleSpriteInstanceTesterTerminated ()
{
	SpriteInstTester = nullptr;
	if (SpriteInstanceButton != nullptr)
	{
		SpriteInstanceButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchSpriteTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	SpriteTester = SpriteTester::CreateObject();
	CHECK(SpriteTester != nullptr)
	SpriteTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleSpriteTesterTerminated, void);
	SpriteTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("SpriteTester is created. This test will automatically terminate based on user input."));

	if (SpriteTestButton != nullptr)
	{
		SpriteTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleSpriteTesterTerminated ()
{
	SpriteTester = nullptr;
	if (SpriteTestButton != nullptr)
	{
		SpriteTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleCloseButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (WindowHandle.IsValid())
	{
		//No need to invoke closed event since this Entity is already aware of it.
		WindowHandle->InvokeCloseOnDestruction = false;
	}

	Destroy();
}
SD_TESTER_END
#endif