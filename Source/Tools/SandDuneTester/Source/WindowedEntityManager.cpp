/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  WindowedEntityManager.cpp
=====================================================================
*/

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::WindowedEntityManager, SD::Entity)
SD_TESTER_BEGIN

void WindowedEntityManager::InitProps ()
{
	Super::InitProps();

	MainDrawLayer = nullptr;
	Mouse = nullptr;

	isInitialized;
}

void WindowedEntityManager::Destroyed ()
{
	if (Mouse != nullptr)
	{
		Mouse->Destroy();
		Mouse = nullptr;
	}

	if (Input.IsValid())
	{
		Input->Destroy();
	}

	if (GuiDrawLayer.IsValid())
	{
		GuiDrawLayer->Destroy();
	}

	if (GuiForegroundLayer != nullptr)
	{
		GuiForegroundLayer->Destroy();
	}

	if (MainDrawLayer != nullptr)
	{
		MainDrawLayer->Destroy();
		MainDrawLayer = nullptr;
	}

	if (WindowHandle.IsValid())
	{
		WindowHandle->InvokeCloseOnDestruction = false;
		WindowHandle->Destroy();
	}

	Super::Destroyed();
}

void WindowedEntityManager::InitializeEntityManager (const SD::Vector2& windowSize, const SD::DString& windowName, SD::Float windowUpdateRate)
{
	if (isInitialized)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("%s is already initialized."), ToString());
		return;
	}
	isInitialized = true;

	WindowHandle = SD::Window::CreateObject();
	WindowHandle->SetResource(new sf::RenderWindow(sf::VideoMode(SD::Int(windowSize.X).ToUnsignedInt32(), SD::Int(windowSize.Y).ToUnsignedInt32()), windowName.ReadString()));
	WindowHandle->GetTick()->SetTickInterval(windowUpdateRate);
	WindowHandle->GetPollingTickComponent()->SetTickInterval(windowUpdateRate);
	WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, WindowedEntityManager, HandleWindowEvent, void, const sf::Event&));
	WindowHandle->DefaultColor = SD::Color(0, 0, 0);
	WindowHandle->SetSize(windowSize);

	MainDrawLayer = SD::PlanarDrawLayer::CreateObject();
	MainDrawLayer->SetDrawPriority(100);
	WindowHandle->RegisterDrawLayer(MainDrawLayer);

	GuiDrawLayer = SD::GuiDrawLayer::CreateObject();
	GuiDrawLayer->SetDrawPriority(SD::GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
	WindowHandle->RegisterDrawLayer(GuiDrawLayer.Get());

	Input = SD::InputBroadcaster::CreateObject();
	Mouse = SD::MousePointer::CreateObject();
	SD::InputUtils::SetupInputForWindow(WindowHandle.Get(), Input.Get(), Mouse);

	GuiForegroundLayer = SD::GuiDrawLayer::CreateObject();
	GuiForegroundLayer->SetDrawPriority(SD::GuiDrawLayer::FOREGROUND_PRIORITY);
	GuiForegroundLayer->InitCommonEntities(SD::GuiDrawLayer::SCommonGuiClasses(true), Input.Get());
	WindowHandle->RegisterDrawLayer(GuiForegroundLayer.Get());
	GuiForegroundLayer->RegisterPlanarObject(Mouse);
}

void WindowedEntityManager::HandleWindowEvent (const sf::Event& windowEvent)
{
	if (windowEvent.type == sf::Event::Closed)
	{
		//Window is already getting destroyed. Clear pointer to prevent destroying it twice.
		WindowHandle = nullptr;
		Destroy();
	}
}
SD_TESTER_END
#endif