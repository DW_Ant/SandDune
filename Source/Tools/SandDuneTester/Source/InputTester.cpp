/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputTester.cpp
=====================================================================
*/

#include "InputTester.h"
#include "InputTesterUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::InputTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 InputTester::WINDOW_SIZE(512.f, 256.f);

void InputTester::InitProps ()
{
	Super::InitProps();

	Ui = nullptr;
}

void InputTester::Destroyed ()
{
	if (Ui != nullptr)
	{
		Ui->Destroy();
		Ui = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void InputTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the InputTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Input Test"), 0.041667f /*~24 fps*/);

	Ui = InputTesterUi::CreateObject();
	Ui->SetupInputComponent(Input.Get(), 1000);
	GuiDrawLayer->RegisterMenu(Ui);
	Ui->InitializeInputTestUi(this, Input.Get());

	SD::UnitTester::TestLog(TestFlags, TXT("The InputTester unit test has launched. It will terminate when the window closes."));
}

SD_TESTER_END
#endif