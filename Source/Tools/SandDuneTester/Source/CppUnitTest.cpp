/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CppUnitTest.cpp
=====================================================================
*/

#include "CppUnitTest.h"
#include "Engine\Core\Headers\CoreClasses.h" //defines PLATFORM_32BIT macro

void CppUnitTestLauncher::LaunchCppUnitTests ()
{
	TestComparisons();
	TestSizes();
	TestOperations();
	TestCasting();
}

void CppUnitTestLauncher::TestComparisons ()
{
	CHECK_TRUE(true)
	CHECK_FALSE(false)

	CHECK_TRUE(4 == 4)
	CHECK_TRUE(2.5f != 2.f)

	CHECK_TRUE(true && (true || false))
	CHECK_FALSE(true && false && true)
	CHECK_TRUE(true || false || true)
}

void CppUnitTestLauncher::TestSizes ()
{
	CHECK_TRUE(sizeof(int) == 4)
	CHECK_TRUE(sizeof(float) == 4)

#ifdef PLATFORM_32BIT
	CHECK_TRUE(sizeof(CppUnitTest_BaseClass) == 4)
	CHECK_TRUE(sizeof(CppUnitTest_SubClass) == 16)
#else
	CHECK_TRUE(sizeof(CppUnitTest_BaseClass) == 8)
	CHECK_TRUE(sizeof(CppUnitTest_SubClass) == 32)
#endif
}

void CppUnitTestLauncher::TestOperations ()
{
	int a = 4;
	int b = 5;
	CHECK_TRUE((a+b) == 9)
	CHECK_TRUE((b-a) == 1)
	CHECK_TRUE((a-b) == -1)
	CHECK_TRUE((a*b) == 20)
	CHECK_TRUE((a&b) == 4)
	CHECK_TRUE((a|b) == 5)
	CHECK_TRUE((a^b) == 1)

	b = 6;
	CHECK_TRUE(b == 6)

	float c = 2.5f;
	float d = 2.f;
	CHECK_TRUE((c+d) == 4.5f)
	CHECK_TRUE((c-d) == 0.5f)
	CHECK_TRUE((c*d) == 5.f)
	CHECK_TRUE((c/d) == 1.25f)
}

void CppUnitTestLauncher::TestInheritance ()
{
	CppUnitTest_SubClass testObj;

	CHECK_TRUE(testObj.GetNum() == 15)
}

void CppUnitTestLauncher::TestCasting ()
{
	int a = 5;
	float b = 6.f;

	CHECK_TRUE((static_cast<int>(b) + a) == 11)
	CHECK_TRUE((static_cast<float>(a) - b) == -1.f)

	CppUnitTest_BaseClass* testObj = new CppUnitTest_SubClass();
	CHECK_TRUE(dynamic_cast<CppUnitTest_SubClass*>(testObj) != nullptr)
	CHECK_TRUE(dynamic_cast<CppUnitTest_ProtectedInterface*>(testObj) == nullptr)
	CHECK_TRUE(dynamic_cast<CppUnitTest_PublicInterface*>(testObj) != nullptr)

	delete testObj;
}


int CppUnitTest_BaseClass::GetNum () const
{
	return 1;
}

int CppUnitTest_SubClass::GetNum () const
{
	int result = 0;

	result += CppUnitTest_BaseClass::GetNum();
	result += GetProtectedInterfaceNum();
	result += GetPublicInterfaceNum();
	result += 8;

	return result;
}

int CppUnitTest_SubClass::GetPublicInterfaceNum () const
{
	return 2;
}

int CppUnitTest_SubClass::GetProtectedInterfaceNum () const
{
	return 4;
}