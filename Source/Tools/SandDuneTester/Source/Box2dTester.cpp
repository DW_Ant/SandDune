/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Box2dTester.cpp
=====================================================================
*/

#include "Box2dTester.h"
#include "Box2dTesterUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::Box2dTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 Box2dTester::WINDOW_SIZE(1024.f, 576.f);

void Box2dTester::InitProps ()
{
	Super::InitProps();

	SceneLayer = nullptr;
	SceneCamera = nullptr;
	Ui = nullptr;
}

void Box2dTester::Destroyed ()
{
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		ActivePhysicsEntities.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(ActivePhysicsEntities);

	if (SceneLayer != nullptr)
	{
		SceneLayer->Destroy();
		SceneLayer = nullptr;
	}

	if (SceneCamera != nullptr)
	{
		SceneCamera->Destroy();
		SceneCamera = nullptr;
	}

	if (Ui != nullptr)
	{
		Ui->Destroy();
		Ui = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void Box2dTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the Box2dTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Box 2D Test"), 0.0166f /*~60 fps*/);

	SceneCamera = SD::TopDownCamera::CreateObject();
	SceneCamera->SetTranslation(SD::Vector3(-196.f, 0.f, 100.f)); //Slightly shift the camera since the UI is going to take up the left side of the window.
	SceneLayer = SD::SceneDrawLayer::CreateObject();
	SceneLayer->SetDrawPriority(GuiDrawLayer->GetDrawPriority() - 100); //Lower priority compared to the GuiDrawLayer so it draws behind it.
	WindowHandle->RegisterDrawLayer(SceneLayer, SceneCamera);

	Ui = Box2dTesterUi::CreateObject();
	Ui->SetupInputComponent(Input.Get(), 1000);
	GuiDrawLayer->RegisterMenu(Ui);
	Ui->SetOwningTester(this);

	SD::UnitTester::TestLog(TestFlags, TXT("The Box2dTester unit test has launched. It will terminate when the window closes."));
}

void Box2dTester::LaunchNewPhysicsTest (const std::vector<Entity*>& newEntities)
{
	//Clear old test
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		ActivePhysicsEntities.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(ActivePhysicsEntities);

	//Add new Entities to the ActivePhysicsEntities vector.
	ActivePhysicsEntities.insert(ActivePhysicsEntities.end(), newEntities.begin(), newEntities.end());

	//Register PhysicsComponents' callbacks
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		SD::Box2dComponent* physComp = dynamic_cast<SD::Box2dComponent*>(ActivePhysicsEntities.at(i)->FindComponent(SD::Box2dComponent::SStaticClass()));
		if (physComp != nullptr)
		{
			if (!physComp->OnBeginContact.IsBounded())
			{
				physComp->OnBeginContact = SDFUNCTION_2PARAM(this, Box2dTester, HandleBeginContact, void, SD::Box2dComponent*, b2Contact*);
			}

			if (!physComp->OnEndContact.IsBounded())
			{
				physComp->OnEndContact = SDFUNCTION_2PARAM(this, Box2dTester, HandleEndContact, void, SD::Box2dComponent*, b2Contact*);
			}

			if (!physComp->OnCollision.IsBounded())
			{
				physComp->OnCollision = SDFUNCTION_3PARAM(this, Box2dTester, HandleCollision, void, SD::Box2dComponent*, b2Contact*, const b2ContactImpulse*);
			}
		}
	}
}

void Box2dTester::RefreshRenderCompColors (SD::Box2dComponent* physComp, bool incrementBrightness) const
{
	bool foundSensor = false;
	for (const b2Fixture* fixture = physComp->GetPhysicsBody()->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		if (fixture->IsSensor())
		{
			foundSensor = true;
			break;
		}
	}

	if (!foundSensor)
	{
		//Only apply if this component is a sensor (ignore blocking components)
		return;
	}

	Entity* owner = physComp->GetOwner();
	if (owner == nullptr)
	{
		return;
	}

	SD::Box2dRenderer* renderer = dynamic_cast<SD::Box2dRenderer*>(owner->FindComponent(SD::Box2dRenderer::SStaticClass(), true));
	if (renderer == nullptr)
	{
		//Nothing to do here if there's no render component
		return;
	}

	sf::Uint8 colorIncrement = 32;
	//Not protecting against number overload to hint there's an error with the sensor counter.
	if (incrementBrightness)
	{
		renderer->FillColor.Source.r += colorIncrement;
	}
	else
	{
		renderer->FillColor.Source.r -= colorIncrement;
	}
}

void Box2dTester::HandleBeginContact (SD::Box2dComponent* otherComp, b2Contact* contactData)
{
	RefreshRenderCompColors(otherComp, false);
}

void Box2dTester::HandleEndContact (SD::Box2dComponent* otherComp, b2Contact* contactData)
{
	RefreshRenderCompColors(otherComp, true);
}

void Box2dTester::HandleCollision (SD::Box2dComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse)
{
	Entity* owner = otherComp->GetOwner();
	CHECK(owner != nullptr)

	const sf::Uint8 minAlpha = 64;
	const sf::Uint8 alphaLossPerHit = 32;

	SD::Box2dRenderer* renderer = dynamic_cast<SD::Box2dRenderer*>(owner->FindComponent(SD::Box2dRenderer::SStaticClass(), false));
	if (renderer != nullptr && renderer->FillColor.Source.a > minAlpha)
	{
		//Darken the color
		renderer->FillColor.Source.a -= alphaLossPerHit;
	}
}
SD_TESTER_END

#endif