/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SceneTester.cpp
=====================================================================
*/

#include "SceneTester.h"
#include "SceneTesterHud.h"
#include "Player.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::SceneTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 SceneTester::WINDOW_SIZE(1024.f, 576.f);

const std::pair<SD::Vector3, SD::Vector3> SceneTester::PLAYER_POS_RANGE(SD::Vector3(-2048.f, -2048.f, -1000.f), SD::Vector3(2048.f, 2048.f, 1000.f));
const std::pair<SD::Vector3, SD::Vector3> SceneTester::PLAYER_SIZE_RANGE(SD::Vector3(0.1f, 0.1f, 0.1f), SD::Vector3(10.f, 10.f, 10.f)); //Note: This is scalar multipliers to the player's base size (1m). So the range is from 1cm to 10m. The center platform is 10 meters long.
const std::pair<SD::Vector3, SD::Vector3> SceneTester::CAMERA_POS_RANGE(SD::Vector3(-512.f, -288.f, -1000.f), SD::Vector3(512.f, 288.f, 1000.f));
const std::pair<SD::Float, SD::Float> SceneTester::CAMERA_ZOOM_RANGE(0.01f, 0.05f);

void SceneTester::InitProps ()
{
	Super::InitProps();

	SceneLayer = nullptr;
	SceneCamera = nullptr;
	PlayerTester = nullptr;
	Hud = nullptr;
	CameraMoveSpeed = 50.f;
	CameraZoomSpeed = 0.1f;

	InputCameraUp = false;
	InputCameraRight = false;
	InputCameraDown = false;
	InputCameraLeft = false;
	InputCameraZoomIn = false;
	InputCameraZoomOut = false;
}

void SceneTester::Destroyed ()
{
	if (SceneLayer != nullptr)
	{
		SceneLayer->Destroy();
		SceneLayer = nullptr;
	}

	if (SceneCamera != nullptr)
	{
		SceneCamera->Destroy();
		SceneCamera = nullptr;
	}

	if (PlayerTester != nullptr)
	{
		PlayerTester->Destroy();
		PlayerTester = nullptr;
	}

	if (Hud != nullptr)
	{
		Hud->Destroy();
		Hud = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void SceneTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the SceneTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Top Down Scene Test"), 0.0166f /*~60 fps*/);

	SceneCamera = SD::TopDownCamera::CreateObject();
	//Elevate it above the player
	SceneCamera->EditTranslation().Z = 50.f;

	PlayerTester = Player::CreateObject();
	CHECK(PlayerTester != nullptr)
	//Elevate the player so they'll render above other Entities.
	PlayerTester->EditTranslation().Z = 100.f;
	SceneCamera->SetRelativeTo(PlayerTester);
	Input->AddInputComponent(PlayerTester->GetInputComp(), 1000);

	SceneLayer = SD::SceneDrawLayer::CreateObject();
	SceneLayer->SetDrawPriority(GuiDrawLayer->GetDrawPriority() - 100); //Lower priority compared to the GuiDrawLayer so it draws behind it.
	WindowHandle->RegisterDrawLayer(SceneLayer, SceneCamera);
	SceneLayer->RegisterSingleComponent(PlayerTester->GetRenderComp());

	Hud = SceneTesterHud::CreateObject();
	Hud->LinkUpEntities(this, PlayerTester, SceneCamera);
	Hud->SetupInputComponent(Input.Get(), 1000);
	GuiDrawLayer->RegisterMenu(Hud);

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Create green grass background
	SD::SceneTransformComponent* grassInstanceTransform = SD::SceneTransformComponent::CreateObject();
	if (AddComponent(grassInstanceTransform))
	{
		SD::InstancedSpriteComponent* grassInstances = SD::InstancedSpriteComponent::CreateObject();
		if (grassInstanceTransform->AddComponent(grassInstances))
		{
			grassInstances->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.GreenGrass")));
			SceneLayer->RegisterSingleComponent(grassInstances);

			std::vector<SD::InstancedSpriteComponent::SSpriteInstance>& spriteInstances = grassInstances->EditSpriteInstances();
			SD::ContainerUtils::Empty(spriteInstances);
			SD::Vector2 position(PLAYER_POS_RANGE.first.ToVector2() * 2.f);
			SD::Vector2 spriteSize(1000.f, 1000.f); //1 tile is 10 meters
			SD::Float numSpritesPerRow = (PLAYER_POS_RANGE.second.X * 4.f) / spriteSize.X;
			SD::Float numSpritesPerCol = (PLAYER_POS_RANGE.second.Y * 4.f) / spriteSize.Y;

			for (SD::Float rowNum = 0.f; rowNum < numSpritesPerRow; rowNum += 1.f)
			{
				position.X = PLAYER_POS_RANGE.first.X * 2.f;

				for (SD::Float colNum = 0.f; colNum < numSpritesPerCol; colNum += 1.f)
				{
					spriteInstances.push_back(SD::InstancedSpriteComponent::SSpriteInstance());
					SD::InstancedSpriteComponent::SSpriteInstance& newInstance = spriteInstances[spriteInstances.size() - 1];
					newInstance.Position = position;
					newInstance.Size = spriteSize;

					position.X += spriteSize.X;
				}

				position.Y += spriteSize.Y;
			}

			grassInstances->RegenerateVertices();
		}

		SD::SceneTransformComponent* foliageTransform = SD::SceneTransformComponent::CreateObject();
		if (grassInstanceTransform->AddComponent(foliageTransform))
		{
			foliageTransform->SetTranslation(SD::Vector3(0.f, 0.f, 3.f));

			SD::InstancedSpriteComponent* foliageSprites = SD::InstancedSpriteComponent::CreateObject();
			if (foliageTransform->AddComponent(foliageSprites))
			{
				foliageSprites->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.TopDownFerns")));
				foliageSprites->SetNumSubDivisions(2, 3);
				foliageSprites->PopulateInstances(4096, 1, (PLAYER_POS_RANGE.second - PLAYER_POS_RANGE.first).ToVector2() * 1.5f, SD::Range<SD::Float>(100.f, 150.f), false, true);
				SceneLayer->RegisterSingleComponent(foliageSprites);
			}

			SD::SceneTransformComponent* middlePlatformTransform = SD::SceneTransformComponent::CreateObject();
			if (foliageTransform->AddComponent(middlePlatformTransform))
			{
				const SD::Vector2 platformSize = SD::Vector2(1000.f, 1000.f); //10 meters
				middlePlatformTransform->SetTranslation(platformSize.ToVector3() * -0.5f); //center it
				middlePlatformTransform->EditTranslation().Z = -1.5f;

				SD::SpriteComponent* middlePlatform = SD::SpriteComponent::CreateObject();
				if (middlePlatformTransform->AddComponent(middlePlatform))
				{
					middlePlatform->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.Tile4")));
					middlePlatform->SetDrawCoordinatesMultipliers(4.f, 4.f);
					middlePlatform->EditBaseSize() = platformSize;
					SceneLayer->RegisterSingleComponent(middlePlatform);
				}
			}
		}
	}

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(tick))
	{
		tick->SetTicking(true);
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, SceneTester, HandleTick, void, SD::Float));
	}

	SD::InputComponent* cameraInput = SD::InputComponent::CreateObject();
	if (SceneCamera->AddComponent(cameraInput))
	{
		cameraInput->SetInputEnabled(true);
		cameraInput->OnInput = SDFUNCTION_1PARAM(this, SceneTester, HandleKeyboardInput, bool, const sf::Event&);
		Input->AddInputComponent(cameraInput, 500);
	}

	SD::UnitTester::TestLog(TestFlags, TXT("The SceneTester unit test has launched. It will terminate based on user input."));
}

bool SceneTester::HandleKeyboardInput (const sf::Event& evnt)
{
	if (evnt.type != sf::Event::KeyPressed && evnt.type  != sf::Event::KeyReleased)
	{
		return false;
	}

	bool newInputFlag = (evnt.type == sf::Event::KeyPressed); //Move on true (pressed). Stop on false (released).
	bool consumeEvent = false;
	switch (evnt.key.code)
	{
		case(sf::Keyboard::I):
			InputCameraUp = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::L):
			InputCameraRight = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::K):
			InputCameraDown = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::J):
			InputCameraLeft = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Equal):
			InputCameraZoomIn = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Dash):
			InputCameraZoomOut = newInputFlag;
			consumeEvent = true;
			break;
	}

	CameraVelocityInput = SD::Vector3::ZERO_VECTOR;
	if (InputCameraUp)
	{
		CameraVelocityInput.Y -= 1.f;
	}

	if (InputCameraRight)
	{
		CameraVelocityInput.X += 1.f;
	}

	if (InputCameraDown)
	{
		CameraVelocityInput.Y += 1.f;
	}

	if (InputCameraLeft)
	{
		CameraVelocityInput.X -= 1.f;
	}

	CameraZoomInput = 0.f;
	if (InputCameraZoomIn)
	{
		CameraZoomInput += 1.f;
	}

	if (InputCameraZoomOut)
	{
		CameraZoomInput -= 1.f;
	}

	return consumeEvent;
}

void SceneTester::HandleTick (SD::Float deltaSec)
{
	if (PlayerTester != nullptr)
	{
		PlayerTester->EditTranslation().ClampInline(PLAYER_POS_RANGE.first, PLAYER_POS_RANGE.second);
		PlayerTester->EditScale().ClampInline(PLAYER_SIZE_RANGE.first, PLAYER_SIZE_RANGE.second);
	}

	if (!CameraVelocityInput.IsEmpty() && SceneCamera != nullptr)
	{
		SD::Vector3 newCamPos = SceneCamera->ReadTranslation() + (CameraVelocityInput * deltaSec * CameraMoveSpeed);
		newCamPos.ClampInline(CAMERA_POS_RANGE.first, CAMERA_POS_RANGE.second);
		SceneCamera->SetTranslation(newCamPos);
	}

	if (CameraZoomInput != 0.f && SceneCamera != nullptr)
	{
		SD::Float newCamZoom = SceneCamera->Zoom + (CameraZoomInput * deltaSec * CameraZoomSpeed);
		newCamZoom = SD::Utils::Clamp(newCamZoom, CAMERA_ZOOM_RANGE.first, CAMERA_ZOOM_RANGE.second);
		SceneCamera->Zoom = newCamZoom;
	}
}

SD_TESTER_END

#endif