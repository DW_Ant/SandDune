/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspector.cpp
=====================================================================
*/

#include "GeneratedInspector.h"
#include "GeneratedInspectorBinaryFile.h"
#include "GeneratedInspectorTestEntity.h"
#include "SelectObjectRegion.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::GeneratedInspector, SD::GuiEntity)
SD_TESTER_BEGIN

void GeneratedInspector::InitProps ()
{
	Super::InitProps();

	TestEntity = nullptr;

	DevAssetFileAttr = SD::FileAttributes(SD::Directory::DEV_ASSET_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("GeneratedInspector.ini"));

#ifdef PLATFORM_64BIT
	SD::DString dirSuffix = TXT("x64");
#else
	SD::DString dirSuffix = TXT("x86");
#endif
	ContentFileAttr = SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester") / dirSuffix, TXT("SecondMockAsset.bin"));

	EditorOperationsFrame = nullptr;
	EditorDescription = nullptr;
	CreateEntityButton = nullptr;
	LoadFromDevButton = nullptr;
	SaveToDevButton = nullptr;
	LoadFromContentButton = nullptr;
	ExportToContentButton = nullptr;
	SwitchEditorButton = nullptr;

	InspectorScrollbar = nullptr;
	InspectorEntity = nullptr;
	SelectObjScrollbar = nullptr;
}

void GeneratedInspector::Destroyed ()
{
	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
		TestEntity = nullptr;
	}

	Super::Destroyed();
}

void GeneratedInspector::ConstructUI ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString transFileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("MockEditor");

	EditorOperationsFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(EditorOperationsFrame))
	{
		EditorOperationsFrame->SetPosition(SD::Vector2::ZERO_VECTOR);
		EditorOperationsFrame->SetSize(SD::Vector2(0.475f, 0.825f));
		EditorOperationsFrame->SetLockedFrame(true);
		EditorOperationsFrame->SetBorderThickness(4.f);
		EditorOperationsFrame->SetCenterColor(SD::Color(48, 48, 48));

		SD::Float spacing = 0.025f;
		SD::Vector2 pos(spacing, spacing);
		SD::Vector2 buttonSize(1.f - (spacing * 2.f), 0.1f);

		EditorDescription = SD::LabelComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(EditorDescription))
		{
			EditorDescription->SetAutoRefresh(false);
			EditorDescription->SetPosition(pos);
			EditorDescription->SetSize(buttonSize);
			EditorDescription->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			EditorDescription->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			EditorDescription->SetWrapText(true);
			EditorDescription->SetClampText(true);
			EditorDescription->SetText(translator->TranslateText(TXT("InspectorEditorDescription"), transFileName, sectionName));
			EditorDescription->SetAutoRefresh(true);
			pos.Y += buttonSize.Y + spacing;
		}

		CreateEntityButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(CreateEntityButton))
		{
			CreateEntityButton->SetPosition(pos);
			CreateEntityButton->SetSize(buttonSize);
			CreateEntityButton->SetCaptionText(translator->TranslateText(TXT("CreateEntity"), transFileName, sectionName));
			CreateEntityButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleCreateEntityReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		LoadFromDevButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(LoadFromDevButton))
		{
			LoadFromDevButton->SetPosition(pos);
			LoadFromDevButton->SetSize(buttonSize);
			LoadFromDevButton->SetCaptionText(translator->TranslateText(TXT("LoadDev"), transFileName, sectionName));
			LoadFromDevButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleLoadFromDevReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		SaveToDevButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(SaveToDevButton))
		{
			SaveToDevButton->SetPosition(pos);
			SaveToDevButton->SetSize(buttonSize);
			SaveToDevButton->SetEnabled(false);
			SaveToDevButton->SetCaptionText(translator->TranslateText(TXT("SaveDev"), transFileName, sectionName));
			SaveToDevButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleSaveToDevReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		LoadFromContentButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(LoadFromContentButton))
		{
			LoadFromContentButton->SetPosition(pos);
			LoadFromContentButton->SetSize(buttonSize);
			LoadFromContentButton->SetCaptionText(translator->TranslateText(TXT("LoadContent"), transFileName, sectionName));
			LoadFromContentButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleLoadFromContentReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		ExportToContentButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(ExportToContentButton))
		{
			ExportToContentButton->SetPosition(pos);
			ExportToContentButton->SetSize(buttonSize);
			ExportToContentButton->SetEnabled(false);
			ExportToContentButton->SetCaptionText(translator->TranslateText(TXT("SaveContent"), transFileName, sectionName));
			ExportToContentButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleExportToContentReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		SwitchEditorButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(SwitchEditorButton))
		{
			SwitchEditorButton->SetPosition(pos);
			SwitchEditorButton->SetSize(buttonSize);
			SwitchEditorButton->SetCaptionText(translator->TranslateText(TXT("SwitchToManual"), transFileName, sectionName));
			SwitchEditorButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GeneratedInspector, HandleSwitchEditors, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}
	}

	InspectorScrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(InspectorScrollbar))
	{
		InspectorScrollbar->SetPosition(SD::Vector2(0.525f, 0.f));
		InspectorScrollbar->SetSize(SD::Vector2(1.f - InspectorScrollbar->ReadPosition().X, 0.825f));
		InspectorScrollbar->SetHideControlsWhenFull(true);
		InspectorScrollbar->SetVisibility(false); //Hide until we load an Entity
	}

	SD::LabelComponent* objRegionLabel = SD::LabelComponent::CreateObject();
	if (AddComponent(objRegionLabel))
	{
		objRegionLabel->SetAutoRefresh(false);
		objRegionLabel->SetPosition(SD::Vector2(0.f, EditorOperationsFrame->ReadSize().Y));
		objRegionLabel->SetSize(SD::Vector2(1.f, 0.03f));
		objRegionLabel->SetWrapText(false);
		objRegionLabel->SetClampText(false);
		objRegionLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
		objRegionLabel->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
		objRegionLabel->SetText(translator->TranslateText(TXT("ObjectRegionLabel"), transFileName, sectionName));
		objRegionLabel->SetAutoRefresh(true);

		SelectObjScrollbar = SD::ScrollbarComponent::CreateObject();
		if (AddComponent(SelectObjScrollbar))
		{
			SelectObjScrollbar->SetPosition(SD::Vector2(0.f, objRegionLabel->ReadPosition().Y + objRegionLabel->ReadSize().Y));
			SelectObjScrollbar->SetSize(SD::Vector2(1.f, 1.f - SelectObjScrollbar->ReadPosition().Y));
			SelectObjScrollbar->SetHideControlsWhenFull(true);
		
			SelectObjectRegion* selectObjEntity = SelectObjectRegion::CreateObject();
			selectObjEntity->SetLabelSize(12.f);
			selectObjEntity->SetIconSize(64.f);
			selectObjEntity->SetLabelSpacing(2.f);
			selectObjEntity->SetObjSpacing(128.f);
			SelectObjScrollbar->SetViewedObject(selectObjEntity);
		}
	}
}

void GeneratedInspector::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(CreateEntityButton);
	Focus->TabOrder.push_back(LoadFromDevButton);
	Focus->TabOrder.push_back(SaveToDevButton);
	Focus->TabOrder.push_back(ExportToContentButton);
}

void GeneratedInspector::InitializeInspector ()
{
	if (TestEntity == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot generate an inspector without initializing the TestEntity first."));
		return;
	}

	CHECK(InspectorScrollbar != nullptr)
	InspectorScrollbar->SetVisibility(true);
	
	if (InspectorEntity != nullptr)
	{
		InspectorEntity->Destroy();
	}

	InspectorEntity = SD::EditorUtils::CreateInspector(TestEntity, 16.f);
	InspectorScrollbar->SetViewedObject(InspectorEntity);

	if (SaveToDevButton != nullptr)
	{
		SaveToDevButton->SetEnabled(true);
	}

	if (ExportToContentButton != nullptr)
	{
		ExportToContentButton->SetEnabled(true);
	}
}

void GeneratedInspector::HandleCreateEntityReleased (SD::ButtonComponent* button)
{
	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}

	TestEntity = GeneratedInspectorTestEntity::CreateObject();
	InitializeInspector();
}

void GeneratedInspector::HandleLoadFromDevReleased (SD::ButtonComponent* button)
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(DevAssetFileAttr, false))
	{
		config->Destroy();
		return;
	}

	GeneratedInspectorTestEntity* loadedEntity = GeneratedInspectorTestEntity::CreateObject();
	SD::EditorUtils::LoadFromConfig(config, TXT("GeneratedInspector"), loadedEntity);
	config->Destroy();

	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}

	TestEntity = loadedEntity;
	InitializeInspector();
}

void GeneratedInspector::HandleSaveToDevReleased (SD::ButtonComponent* button)
{
	if (!OS_CheckIfDirectoryExists(DevAssetFileAttr.ReadPath()))
	{
		OS_CreateDirectory(DevAssetFileAttr.ReadPath());
	}

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (config->OpenFile(DevAssetFileAttr, false))
	{
		SD::EditorUtils::SaveToConfig(config, TXT("GeneratedInspector"), TestEntity);
	}

	config->Destroy();
}

void GeneratedInspector::HandleLoadFromContentReleased (SD::ButtonComponent* button)
{
	GeneratedInspectorTestEntity* loadedObj = GeneratedInspectorTestEntity::CreateObject();
	GeneratedInspectorBinaryFile* loadFile = GeneratedInspectorBinaryFile::CreateObject();
	loadFile->TestEntity = loadedObj;
	if (!loadFile->OpenFile(ContentFileAttr))
	{
		loadedObj->Destroy();
		loadFile->Destroy();
		return;
	}

	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}
	TestEntity = loadedObj;
	loadFile->Destroy();
	InitializeInspector();
}

void GeneratedInspector::HandleExportToContentReleased (SD::ButtonComponent* button)
{
	CHECK(TestEntity != nullptr)
	if (!OS_CheckIfDirectoryExists(ContentFileAttr.ReadPath()))
	{
		OS_CreateDirectory(ContentFileAttr.ReadPath());
	}

	GeneratedInspectorBinaryFile* saveFile = GeneratedInspectorBinaryFile::CreateObject();
	saveFile->TestEntity = TestEntity;
	saveFile->SaveFile(ContentFileAttr);
	saveFile->Destroy();
}

void GeneratedInspector::HandleSwitchEditors (SD::ButtonComponent* button)
{
	if (SD::TooltipGuiEntity* tooltipManager = SD::GuiUtils::FindEntityInSameWindowAs<SD::TooltipGuiEntity>(SwitchEditorButton))
	{
		tooltipManager->HideTooltip();
	}

	if (OnSwapEditors.IsBounded())
	{
		OnSwapEditors.Execute();
	}
}
SD_TESTER_END
#endif