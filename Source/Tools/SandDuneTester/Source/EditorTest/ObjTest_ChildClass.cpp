/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjTest_ChildClass.cpp
=====================================================================
*/

#include "ObjTest_ChildClass.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::ObjTest_ChildClass, SDTester::ObjTest_ParentClass)
SD_TESTER_BEGIN

void ObjTest_ChildClass::InitProps ()
{
	Super::InitProps();

	TestBool = false;
	OtherParentObj = nullptr;
	OtherChildObj = nullptr;
}

void ObjTest_ChildClass::CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate)
{
	Super::CopyPropertiesFrom(objectTemplate);

	if (const ObjTest_ChildClass* objTemplate = dynamic_cast<const ObjTest_ChildClass*>(objectTemplate))
	{
		TestBool = objTemplate->TestBool;
		OtherParentObj = objTemplate->OtherParentObj;
		OtherChildObj = objTemplate->OtherChildObj;
	}
}

void ObjTest_ChildClass::PopulateInspectorList (SD::InspectorList& outList)
{
	Super::PopulateInspectorList(OUT outList);

	outList.AddProp(new SD::EditableBool::SInspectorBool(TXT("TestBool"), SD::DString::EmptyString, &TestBool));
	outList.AddProp(new SD::EditableObjectComponent::SInspectorObject(TXT("OtherParentObj"), SD::DString::EmptyString, new SD::ObjBinder(&OtherParentObj), false, ObjTest_ParentClass::SStaticClass()));
	outList.AddProp(new SD::EditableObjectComponent::SInspectorObject(TXT("OtherChildObj"), SD::DString::EmptyString, new SD::ObjBinder(&OtherChildObj), false, ObjTest_ChildClass::SStaticClass()));
}

void ObjTest_ChildClass::SaveToDevAsset (SD::ConfigWriter* config)
{
	Super::SaveToDevAsset(config);

	SD::DString sectionName = GetCompleteEditorName();

	if (const ObjTest_ChildClass* cdo = dynamic_cast<const ObjTest_ChildClass*>(GetDefaultObject()))
	{
		if (cdo->TestBool != TestBool)
		{
			config->SaveProperty<SD::Bool>(sectionName, TXT("TestBool"), TestBool);
		}

		SD::DString objName;
		if (OtherParentObj != nullptr)
		{
			objName = OtherParentObj->GetCompleteEditorName();
		}
		config->SavePropertyText(sectionName, TXT("OtherParentObj"), objName);

		objName = SD::DString::EmptyString;
		if (OtherChildObj != nullptr)
		{
			objName = OtherChildObj->GetCompleteEditorName();
		}
		config->SavePropertyText(sectionName, TXT("OtherObject"), objName);
	}
}

void ObjTest_ChildClass::LoadFromDevAsset (SD::ConfigWriter* config)
{
	Super::LoadFromDevAsset(config);

	SD::DString sectionName = GetCompleteEditorName();

	if (config->ContainsProperty(sectionName, TXT("TestBool")))
	{
		TestBool = config->GetProperty<SD::Bool>(sectionName, TXT("TestBool"));
	}

	if (config->ContainsProperty(sectionName, TXT("OtherObject")))
	{
		//TODO: When packages are implemented, find object of matching name.
	}
}

void ObjTest_ChildClass::SaveToByteBuffer (SD::DataBuffer& outBuffer)
{
	Super::SaveToByteBuffer(OUT outBuffer);

	SD::DString parentName;
	if (OtherParentObj != nullptr)
	{
		parentName = OtherParentObj->GetEditorObjName();
	}

	SD::DString childName;
	if (OtherChildObj != nullptr)
	{
		childName = OtherChildObj->GetEditorObjName();
	}

	outBuffer << TestBool << parentName << childName;
}

void ObjTest_ChildClass::LoadFromBuffer (const SD::DataBuffer& incomingData)
{
	Super::LoadFromBuffer(incomingData);

	SD::DString parentName;
	SD::DString childName;
	incomingData >> TestBool >> parentName >> childName;

	OtherParentObj = nullptr;
	OtherChildObj = nullptr;

	if (!parentName.IsEmpty())
	{
		//TODO: When packages are implemented, find object of matching name.
	}

	if (!childName.IsEmpty())
	{
		//TODO: When packages are implemented, find object of matching name.
	}
}
SD_TESTER_END
#endif