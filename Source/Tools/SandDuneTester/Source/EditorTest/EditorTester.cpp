/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorTester.cpp
=====================================================================
*/

#include "EditorTester.h"
#include "GeneratedInspector.h"
#include "MockEditor.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::EditorTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 EditorTester::WINDOW_SIZE(768.f, 576.f);

void EditorTester::InitProps ()
{
	Super::InitProps();

	TestFlags = SD::UnitTester::EUnitTestFlags::UTF_None;
	Editor = nullptr;
	GeneratedEditor = nullptr;
}

void EditorTester::Destroyed ()
{
	if (Editor != nullptr)
	{
		Editor->Destroy();
		Editor = nullptr;
	}

	if (GeneratedEditor != nullptr)
	{
		GeneratedEditor->Destroy();
		GeneratedEditor = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated.Execute();
	}

	Super::Destroyed();
}

void EditorTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the EditorTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Editor Test"));

	//Swap themes for the mock editor. Typically this is set at an application level, but for a tester that has editors and scenes, swap it dynamically.
	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(guiEngine != nullptr && editorEngine != nullptr)
	guiEngine->SetGuiTheme(editorEngine->GetUiTheme());

	Editor = MockEditor::CreateObject();
	Editor->SetupInputComponent(Input.Get(), 100);
	Editor->OnSwapEditors = SDFUNCTION(this, EditorTester, HandleRevealGeneratedInspector, void);
	GuiDrawLayer->RegisterMenu(Editor);

	GeneratedEditor = GeneratedInspector::CreateObject();
	GeneratedEditor->SetupInputComponent(Input.Get(), 100);
	GeneratedEditor->OnSwapEditors = SDFUNCTION(this, EditorTester, HandleRevealMockEditor, void);
	GeneratedEditor->SetVisibility(false);
	GuiDrawLayer->RegisterMenu(GeneratedEditor);

	SD::UnitTester::TestLog(TestFlags, TXT("The EditorTester unit test has launched. It will terminate based on user input."));
}

void EditorTester::HandleRevealMockEditor ()
{
	CHECK(GeneratedEditor != nullptr && Editor != nullptr)
	Editor->SetVisibility(true);
	GeneratedEditor->SetVisibility(false);
}

void EditorTester::HandleRevealGeneratedInspector ()
{
	CHECK(GeneratedEditor != nullptr && Editor != nullptr)
	Editor->SetVisibility(false);
	GeneratedEditor->SetVisibility(true);
}
SD_TESTER_END
#endif