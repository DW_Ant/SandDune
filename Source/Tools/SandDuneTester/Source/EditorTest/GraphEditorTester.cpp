/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphEditorTester.cpp
=====================================================================
*/

#include "GraphEditorTester.h"
#include "GraphOverlay.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::GraphEditorTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 GraphEditorTester::WINDOW_SIZE(1280.f, 720.f);

void GraphEditorTester::InitProps ()
{
	Super::InitProps();

	TestFlags = SD::UnitTester::EUnitTestFlags::UTF_None;
}

void GraphEditorTester::Destroyed ()
{
	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated.Execute();
	}

	if (Overlay != nullptr)
	{
		Overlay->Destroy();
	}

	for (size_t i = 0; i < LoadedAssets.size(); ++i)
	{
		LoadedAssets.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(OUT LoadedAssets);


	Super::Destroyed();
}

void GraphEditorTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the GraphEditorTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Graph Editor Test"), 0.016667f);

	//Swap themes for the mock editor. Typically this is set at an application level, but for a tester that has editors and scenes, swap it dynamically.
	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(guiEngine != nullptr && editorEngine != nullptr)
	guiEngine->SetGuiTheme(editorEngine->GetUiTheme());

	Overlay = GraphOverlay::CreateObject();
	GuiDrawLayer->RegisterMenu(Overlay.Get());
	Overlay->SetupInputComponent(Input.Get(), 100);
	Overlay->SetGraphTester(this);

	SD::UnitTester::TestLog(TestFlags, TXT("The GraphEditorTester unit test has launched. It will terminate based on user input."));
}

void GraphEditorTester::SetEditedAsset (SD::GraphAsset* newEditedAsset)
{
	EditedAsset = newEditedAsset;
	if (EditedAsset != nullptr)
	{
		SD::ContainerUtils::AddUnique(OUT LoadedAssets, EditedAsset.Get());
		EditedAsset->SetLoadState(SD::GraphAsset::LS_Loaded);
	}
}
SD_TESTER_END
#endif