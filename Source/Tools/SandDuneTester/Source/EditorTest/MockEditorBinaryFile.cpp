/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditorBinaryFile.cpp
=====================================================================
*/

#include "MockEditorBinaryFile.h"
#include "MockEditorTestEntity.h"
#include "ObjTest_ChildClass.h"
#include "ObjTest_ParentClass.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::MockEditorBinaryFile, SD::BinaryFile)
SD_TESTER_BEGIN

void MockEditorBinaryFile::InitProps ()
{
	Super::InitProps();

	TestEntity = nullptr;
}

bool MockEditorBinaryFile::ReadContents (const SD::DataBuffer& incomingData)
{
	if (!Super::ReadContents(incomingData))
	{
		return false;
	}

	if (TestEntity == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot read from %s since the TestEntity reference is not yet assigned."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	bool success;
	//Int test
	{
		const SD::DString errorMsg = TXT("Failed to load %s since there wasn't enough bytes to read an Int.");

		TestEntity->SimpleInt = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}

		TestEntity->ReadOnlyInt = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}

		TestEntity->PositiveInt = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}

		TestEntity->NegativeInt = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}

		TestEntity->IntBetweenZeroAndOneHundred = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}

		TestEntity->CountingInt = ReadIntFromBuffer(incomingData, OUT success);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, errorMsg, LatestAccessedFile.GetName(false, true));
			return false;
		}
	}

	//Float test
	{
		std::function<bool(const SD::DString&, SD::Float&)> readFloat([&](const SD::DString& varName, SD::Float& outValue)
		{
			if (!incomingData.CanReadBytes(outValue.GetMinBytes()))
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s. There's not enough bytes to read for the %s variable."), LatestAccessedFile.GetName(false, true), varName);
				return false;
			}

			if ((incomingData >> outValue).HasReadError())
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s. The bytes is malformed, preventing it from reading the %s variable."), LatestAccessedFile.GetName(false, true), varName);
				return false;
			}

			return true;
		});

		if (!readFloat(TXT("SimpleFloat"), OUT TestEntity->SimpleFloat))
		{
			return false;
		}

		if (!readFloat(TXT("NoDecimal"), OUT TestEntity->NoDecimal))
		{
			return false;
		}

		if (!readFloat(TXT("MinNegHundred"), OUT TestEntity->MinNegHundred))
		{
			return false;
		}

		if (!readFloat(TXT("MaxHundred"), OUT TestEntity->MaxHundred))
		{
			return false;
		}

		if (!readFloat(TXT("ClampedFloat"), OUT TestEntity->ClampedFloat))
		{
			return false;
		}
	}

	//Vector test
	{
		std::function<bool(const SD::DString&, SD::Vector2&)> readVect2([&](const SD::DString& varName, SD::Vector2& outVect)
		{
			if (!incomingData.CanReadBytes(outVect.GetMinBytes()))
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since there are not enough bytes to read %s."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			if ((incomingData >> outVect).HasReadError())
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since the bytes are malformed, preventing it from reading the %s variable."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			return true;
		});

		if (!readVect2(TXT("SimpleVect2"), OUT TestEntity->SimpleVect2))
		{
			return false;
		}

		if (!readVect2(TXT("LockedYAxisVect2"), OUT TestEntity->LockedYAxisVect2))
		{
			return false;
		}

		if (!readVect2(TXT("ClampedXAxisVect2"), OUT TestEntity->ClampedXAxisVect2))
		{
			return false;
		}

		//Vector3
		std::function<bool(const SD::DString&, SD::Vector3&)> readVect3([&](const SD::DString& varName, SD::Vector3& outVect)
		{
			if (!incomingData.CanReadBytes(outVect.GetMinBytes()))
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since there are not enough bytes to read %s."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			if ((incomingData >> outVect).HasReadError())
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since the bytes are malformed, preventing it from reading the %s variable."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			return true;
		});

		if (!readVect3(TXT("SimpleVect3"), OUT TestEntity->SimpleVect3))
		{
			return false;
		}

		if (!readVect3(TXT("LockedZAxisVect3"), OUT TestEntity->LockedZAxisVect3))
		{
			return false;
		}
	}

	//DString test
	{
		bool bSuccess;
		TestEntity->SimpleStr = ReadStringFromBuffer(incomingData, OUT bSuccess);
		if (!bSuccess)
		{
			return false;
		}

		TestEntity->VowelsOnly = ReadStringFromBuffer(incomingData, OUT bSuccess);
		if (!bSuccess)
		{
			return false;
		}

		TestEntity->ShortStr = ReadStringFromBuffer(incomingData, OUT bSuccess);
		if (!bSuccess)
		{
			return false;
		}

		TestEntity->MultiLineStr = ReadStringFromBuffer(incomingData, OUT bSuccess);
		if (!bSuccess)
		{
			return false;
		}

		TestEntity->CaptionTestStr = ReadStringFromBuffer(incomingData, OUT bSuccess);
		if (!bSuccess)
		{
			return false;
		}
	}

	//Bool test
	{
		std::function<bool(const SD::DString&, bool&)> readBool([&](const SD::DString& varName, bool& outVar)
		{
			if (incomingData.IsReaderAtEnd())
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since there are not enough bytes to read %s."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			SD::Bool sdBool;
			if ((incomingData >> sdBool).HasReadError())
			{
				SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to read %s since the bytes are malformed, preventing it from reading the %s variable."), LatestAccessedFile.GetName(true, true), varName);
				return false;
			}

			outVar = sdBool;

			return true;
		});

		if (!readBool(TXT("SimpleBool"), OUT TestEntity->SimpleBool))
		{
			return false;
		}

		if (!readBool(TXT("ReadOnlyBool"), OUT TestEntity->ReadOnlyBool))
		{
			return false;
		}
	}

	//Object test
	{
		SD::DString parentName;
		incomingData >> parentName;
		if (incomingData.HasReadError())
		{
			return false;
		}

		if (TestEntity->ParentRef != nullptr)
		{
			TestEntity->ParentRef->Destroy();
			TestEntity->ParentRef = nullptr;
		}

		if (!parentName.IsEmpty())
		{
			//TODO: When packages are implemented, locate the Entity that matches the name.
		}

		SD::DString childName;
		incomingData >> childName;
		if (incomingData.HasReadError())
		{
			return false;
		}

		if (TestEntity->ChildRef != nullptr)
		{
			TestEntity->ChildRef->Destroy();
			TestEntity->ChildRef = nullptr;
		}

		if (!childName.IsEmpty())
		{
			//TODO: When packages are implemented, locate the Entity that matches the name.
		}
	}

	//Enum test
	{
		SD::Int enumIdx;
		incomingData >> enumIdx;
		if (incomingData.HasReadError())
		{
			return false;
		}

		TestEntity->TestEnum = static_cast<MockEditorTestEntity::ETestEnum>(enumIdx.Value);
	}

	//Caption struct test
	{
		incomingData >> TestEntity->CaptionStruct.TestStr >> TestEntity->CaptionStruct.TestInt;
		if (incomingData.HasReadError())
		{
			return false;
		}
	}

	//Nested struct test
	{
		SD::Bool sdBool(TestEntity->NestedStruct.OuterBool);
		incomingData >> sdBool >> TestEntity->NestedStruct.InnerStruct.InnerInt >> TestEntity->NestedStruct.InnerStruct.InnerFloat >> TestEntity->NestedStruct.InnerStruct.InnerString >> TestEntity->NestedStruct.OuterString;
		if (incomingData.HasReadError())
		{
			return false;
		}
	}

	//Array test
	{
		incomingData.DeserializeArray(OUT TestEntity->ArrayInt);
		incomingData.DeserializeArray(OUT TestEntity->ArrayInt);
		incomingData.DeserializeArray(OUT TestEntity->ArrayFloat);
		incomingData.DeserializeArray(OUT TestEntity->ArrayString);
		incomingData.DeserializeArray(OUT TestEntity->ArrayVector2);
		incomingData.DeserializeArray(OUT TestEntity->ArrayVector3);
		incomingData.DeserializeArray(OUT TestEntity->ArrayReadOnly);
		incomingData.DeserializeArray(OUT TestEntity->ArrayFixedSize);
		incomingData.DeserializeArray(OUT TestEntity->ArrayMaxSize);
	}

	return true;
}

bool MockEditorBinaryFile::SaveContents (SD::DataBuffer& outBuffer) const
{
	if (!Super::SaveContents(OUT outBuffer))
	{
		return false;
	}

	if (TestEntity == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to save %s since the Mocking Test Entity is not yet assigned."), LatestAccessedFile.GetName(false, true));
		return false;
	}

	outBuffer << TestEntity->SimpleInt;
	outBuffer << TestEntity->ReadOnlyInt;
	outBuffer << TestEntity->PositiveInt;
	outBuffer << TestEntity->NegativeInt;
	outBuffer << TestEntity->IntBetweenZeroAndOneHundred;
	outBuffer << TestEntity->CountingInt;

	outBuffer << TestEntity->SimpleFloat;
	outBuffer << TestEntity->NoDecimal;
	outBuffer << TestEntity->MinNegHundred;
	outBuffer << TestEntity->MaxHundred;
	outBuffer << TestEntity->ClampedFloat;

	outBuffer << TestEntity->SimpleVect2;
	outBuffer << TestEntity->LockedYAxisVect2;
	outBuffer << TestEntity->ClampedXAxisVect2;

	outBuffer << TestEntity->SimpleVect3;
	outBuffer << TestEntity->LockedZAxisVect3;

	outBuffer << TestEntity->SimpleStr;
	outBuffer << TestEntity->VowelsOnly;
	outBuffer << TestEntity->ShortStr;
	outBuffer << TestEntity->MultiLineStr;
	outBuffer << TestEntity->CaptionTestStr;

	outBuffer << SD::Bool(TestEntity->SimpleBool);
	outBuffer << SD::Bool(TestEntity->ReadOnlyBool);

	{
		SD::DString parentRefName;
		if (TestEntity->ParentRef != nullptr)
		{
			parentRefName = TestEntity->ParentRef->GetCompleteEditorName();
		}

		SD::DString childRefName;
		if (TestEntity->ChildRef != nullptr)
		{
			childRefName = TestEntity->ChildRef->GetCompleteEditorName();
		}

		outBuffer << parentRefName << childRefName;
	}

	outBuffer << SD::Int(TestEntity->TestEnum);

	outBuffer << TestEntity->CaptionStruct.TestStr << TestEntity->CaptionStruct.TestInt;
	outBuffer << SD::Bool(TestEntity->NestedStruct.OuterBool) << TestEntity->NestedStruct.InnerStruct.InnerInt << TestEntity->NestedStruct.InnerStruct.InnerFloat << TestEntity->NestedStruct.InnerStruct.InnerString << TestEntity->NestedStruct.OuterString;

	outBuffer.SerializeArray(TestEntity->ArrayInt);
	outBuffer.SerializeArray(TestEntity->ArrayInt);
	outBuffer.SerializeArray(TestEntity->ArrayFloat);
	outBuffer.SerializeArray(TestEntity->ArrayString);
	outBuffer.SerializeArray(TestEntity->ArrayVector2);
	outBuffer.SerializeArray(TestEntity->ArrayVector3);
	outBuffer.SerializeArray(TestEntity->ArrayReadOnly);
	outBuffer.SerializeArray(TestEntity->ArrayFixedSize);
	outBuffer.SerializeArray(TestEntity->ArrayMaxSize);

	return true;
}
SD_END
#endif