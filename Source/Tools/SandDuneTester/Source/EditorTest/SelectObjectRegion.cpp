/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SelectObjectRegion.cpp
=====================================================================
*/

#include "ObjTest_ParentClass.h"
#include "SelectObjectRegion.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::SelectObjectRegion, SD::Entity)
SD_TESTER_BEGIN

void SelectObjectRegion::InitProps ()
{
	Super::InitProps();

	LabelSize = 12.f;
	IconSize = 48.f;
	LabelSpacing = 4.f;
	ObjSpacing = 16.f;

	OwningScrollbar = nullptr;
	DrawLayer = nullptr;
}

void SelectObjectRegion::InitializeScrollableInterface (SD::ScrollbarComponent* scrollbarOwner)
{
	SD::ScrollableInterface::InitializeScrollableInterface(scrollbarOwner);

	OwningScrollbar = scrollbarOwner;
}

SD::ScrollbarComponent* SelectObjectRegion::GetScrollbarOwner () const
{
	return OwningScrollbar;
}

void SelectObjectRegion::RegisterToDrawLayer (SD::ScrollbarComponent* scrollbar, SD::RenderTexture* drawLayerOwner)
{
	std::vector<SD::RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	for (SD::RenderTarget::SDrawLayerCamera& drawLayer : drawLayers)
	{
		if (SD::PlanarDrawLayer* planarDrawLayer = dynamic_cast<SD::PlanarDrawLayer*>(drawLayer.Layer))
		{
			DrawLayer = planarDrawLayer;
			planarDrawLayer->RegisterPlanarObject(this);
			break;
		}
	}

	if (DrawLayer == nullptr)
	{
		SD::PlanarDrawLayer* planarDrawLayer = SD::PlanarDrawLayer::CreateObject();
		SD::RenderTarget::SDrawLayerCamera& newLayer = drawLayers.emplace_back(planarDrawLayer, scrollbar->GetFrameCamera());
		DrawLayer = planarDrawLayer;
		planarDrawLayer->RegisterPlanarObject(this);
	}

	CHECK(DrawLayer != nullptr)
	for (ObjTest_ParentClass* testObj : RegisteredTestObjs)
	{
		DrawLayer->RegisterPlanarObject(testObj);
	}
}

void SelectObjectRegion::UnregisterFromDrawLayer (SD::RenderTexture* drawLayerOwner)
{
	if (DrawLayer != nullptr)
	{
		for (ObjTest_ParentClass* testObj : RegisteredTestObjs)
		{
			DrawLayer->UnregisterPlanarObject(testObj);
		}

		DrawLayer = nullptr;
	}

	std::vector<SD::RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	for (UINT_TYPE i = 0; i < drawLayers.size(); ++i)
	{
		SD::PlanarDrawLayer* drawLayer = dynamic_cast<SD::PlanarDrawLayer*>(drawLayers.at(i).Layer);
		if (drawLayer != nullptr)
		{
			drawLayer->UnregisterPlanarObject(this);
			return;
		}
	}
}

void SelectObjectRegion::GetTotalSize (SD::Vector2& outSize) const
{
	outSize = GetSize();
}

void SelectObjectRegion::HandleScrollbarFrameSizeChange (const SD::Vector2& oldFrameSize, const SD::Vector2& newFrameSize)
{
	if (ReadSize().Y != newFrameSize.Y)
	{
		EditSize().Y = newFrameSize.Y;
		ReconstructObjectRegion(0);
	}
}

void SelectObjectRegion::ClearScrollbarOwner (SD::ScrollbarComponent* oldScrollbarOwner)
{
	OwningScrollbar = nullptr;
	ScrollableInterface::ClearScrollbarOwner(oldScrollbarOwner);
}

void SelectObjectRegion::RemoveScrollableObject ()
{
	Destroy();
}

void SelectObjectRegion::RegisterTestObj (ObjTest_ParentClass* newTestObj)
{
	if (SD::ContainerUtils::FindInVector(RegisteredTestObjs, newTestObj) == UINT_INDEX_NONE)
	{
		if (DrawLayer != nullptr)
		{
			DrawLayer->RegisterPlanarObject(newTestObj);
		}

		RegisteredTestObjs.push_back(newTestObj);
		ReconstructObjectRegion(RegisteredTestObjs.size() - 1);

		SD::DestroyNotifyComponent* destroyComp = SD::DestroyNotifyComponent::CreateObject();
		if (newTestObj->AddComponent(destroyComp))
		{
			destroyComp->OnDestroyed = SDFUNCTION_1PARAM(this, SelectObjectRegion, HandleObjDestroyed, void, SD::DestroyNotifyComponent*);
		}
	}
}

void SelectObjectRegion::UnregisterTestObj (ObjTest_ParentClass* target)
{
	SD::Int removedIdx = SD::ContainerUtils::RemoveItem(OUT RegisteredTestObjs, target);
	if (removedIdx != INT_INDEX_NONE)
	{
		if (DrawLayer != nullptr)
		{
			DrawLayer->UnregisterPlanarObject(target);
		}

		ReconstructObjectRegion(removedIdx.ToUnsignedInt());
	}
}

void SelectObjectRegion::SetLabelSize (SD::Float newLabelSize)
{
	if (newLabelSize != LabelSize)
	{
		LabelSize = newLabelSize;
		ReconstructObjectRegion(0);
	}
}

void SelectObjectRegion::SetIconSize (SD::Float newIconSize)
{
	if (IconSize != newIconSize)
	{
		IconSize = newIconSize;
		ReconstructObjectRegion(0);
	}
}

void SelectObjectRegion::SetLabelSpacing (SD::Float newLabelSpacing)
{
	if (newLabelSpacing != LabelSpacing)
	{
		LabelSpacing = newLabelSpacing;
		ReconstructObjectRegion(0);
	}
}

void SelectObjectRegion::SetObjSpacing (SD::Float newObjSpacing)
{
	if (newObjSpacing != ObjSpacing)
	{
		ObjSpacing = newObjSpacing;
		ReconstructObjectRegion(0);
	}
}

void SelectObjectRegion::ReconstructObjectRegion (size_t startUpdateIdx)
{
	if (GetPendingDelete())
	{
		//Don't bother updating this since it's about to be destroyed
		return;
	}

	if (startUpdateIdx == 0)
	{
		for (SD::LabelComponent* label : LabelComps)
		{
			label->Destroy();
		}

		SD::ContainerUtils::Empty(OUT LabelComps);
	}
	else if (!SD::ContainerUtils::IsEmpty(LabelComps))
	{
		while (LabelComps.size() > startUpdateIdx)
		{
			LabelComps.at(LabelComps.size() - 1)->Destroy();
			LabelComps.pop_back();
		}
	}

	SD::FontPool* localFontPool = SD::FontPool::FindFontPool();
	CHECK(localFontPool != nullptr)

	SD::Float posX = (IconSize + ObjSpacing) * SD::Float(startUpdateIdx);
	for (size_t i = startUpdateIdx; i < RegisteredTestObjs.size(); ++i)
	{
		SD::LabelComponent* label = SD::LabelComponent::CreateObject();
		if (AddComponent(label))
		{
			label->SetAutoRefresh(false);
			label->SetPosition(SD::Vector2(posX, 0.f));
			label->SetSize(SD::Vector2(IconSize + ObjSpacing, LabelSize));
			label->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			label->SetWrapText(true);
			label->SetClampText(false);
			label->SetText(RegisteredTestObjs.at(i)->GetCompleteEditorName());
			label->SetAutoRefresh(true);
			LabelComps.push_back(label);
		}

		RegisteredTestObjs.at(i)->SetPosition(SD::Vector2(posX + ((IconSize + ObjSpacing) * 0.5f) - (IconSize * 0.5f), LabelSize + LabelSpacing));
		RegisteredTestObjs.at(i)->SetSize(SD::Vector2(IconSize, IconSize));
		posX += IconSize + ObjSpacing;
	}

	EditSize().X = posX;
}

void SelectObjectRegion::HandleObjDestroyed (SD::DestroyNotifyComponent* destroyComp)
{
	if (ObjTest_ParentClass* parentObj = dynamic_cast<ObjTest_ParentClass*>(destroyComp->GetMostRecentOwner()))
	{
		UnregisterTestObj(parentObj);
	}
}
SD_TESTER_END
#endif