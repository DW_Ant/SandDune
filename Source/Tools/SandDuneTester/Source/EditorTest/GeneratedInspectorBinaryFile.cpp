/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspectorBinaryFile.cpp
=====================================================================
*/

#include "GeneratedInspectorBinaryFile.h"
#include "GeneratedInspectorTestEntity.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::GeneratedInspectorBinaryFile, SD::BinaryFile)
SD_TESTER_BEGIN

void GeneratedInspectorBinaryFile::InitProps ()
{
	Super::InitProps();

	TestEntity = nullptr;
}

bool GeneratedInspectorBinaryFile::ReadContents (const SD::DataBuffer& incomingData)
{
	if (!Super::ReadContents(incomingData))
	{
		return false;
	}

	if (TestEntity == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot read from %s since the TestEntity reference is not yet assigned."), LatestAccessedFile.GetName(true, true));
		return false;
	}

	return SD::EditorUtils::LoadFromBuffer(incomingData, TestEntity);
}

bool GeneratedInspectorBinaryFile::SaveContents (SD::DataBuffer& outBuffer) const
{
	if (!Super::SaveContents(OUT outBuffer))
	{
		return false;
	}

	if (TestEntity == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to save %s since the Test Entity is not yet assigned."), LatestAccessedFile.GetName(false, true));
		return false;
	}

	SD::EditorUtils::SaveToBuffer(OUT outBuffer, TestEntity);
	return true;
}
SD_END
#endif