/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditor.cpp
=====================================================================
*/

#include "MockEditor.h"
#include "MockEditorBinaryFile.h"
#include "MockEditorTestEntity.h"
#include "ObjTest_ChildClass.h"
#include "ObjTest_ParentClass.h"
#include "SelectObjectRegion.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::MockEditor, SD::GuiEntity)
SD_TESTER_BEGIN

void MockEditor::InitProps ()
{
	Super::InitProps();

	TestEntity = nullptr;

	DevAssetFileAttr = SD::FileAttributes(SD::Directory::DEV_ASSET_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("MockEditor.ini"));

#ifdef PLATFORM_64BIT
	SD::DString dirSuffix = TXT("x64");
#else
	SD::DString dirSuffix = TXT("x86");
#endif
	ContentFileAttr = SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester") / dirSuffix, TXT("MockAsset.bin"));

	HideStructCaptionTick = nullptr;

	EditorOperationsFrame = nullptr;
	EditorDescription = nullptr;
	CreateEntityButton = nullptr;
	LoadFromDevButton = nullptr;
	SaveToDevButton = nullptr;
	LoadFromContentButton = nullptr;
	ExportToContentButton = nullptr;
	SwitchEditorButton = nullptr;

	InspectorScrollbar = nullptr;
	InspectorEntity = nullptr;
	InspectorList = nullptr;

	SimpleIntField = nullptr;
	ReadOnlyIntField = nullptr;
	PositiveIntField = nullptr;
	NegativeIntField = nullptr;
	IntBetweenZeroAndOneHundredField = nullptr;
	CountingIntField = nullptr;

	SimpleFloatField = nullptr;
	NoDecimalField = nullptr;
	MinNegHundredField = nullptr;
	MaxHundredField = nullptr;
	ClampedFloatField = nullptr;

	SimpleVectorField = nullptr;
	LockedYAxisVect2Field = nullptr;
	ClampedXAxisVect2Field = nullptr;

	SimpleVector3Field = nullptr;
	LockedZAxisVect3Field = nullptr;

	SimpleStrField = nullptr;
	VowelsOnlyField = nullptr;
	ShortStrField = nullptr;
	MultiLineStrField = nullptr;
	CaptionTestField = nullptr;

	SimpleBoolField = nullptr;
	ReadOnlyBoolField = nullptr;

	CreateObjComp = nullptr;
	BoundParentComp = nullptr;
	BoundChildComp = nullptr;

	TestEnumDropdown = nullptr;

	CaptionStructUi = nullptr;
	NestedStructUi = nullptr;

	ArrayInt = nullptr;
	ArrayFloat = nullptr;
	ArrayString = nullptr;
	ArrayVector2 = nullptr;
	ArrayVector3 = nullptr;
	ArrayReadOnly = nullptr;
	ArrayFixedSize = nullptr;
	ArrayMaxSize = nullptr;
	ArrayOfObjs = nullptr;

	SelectObjScrollbar = nullptr;
}

void MockEditor::Destroyed ()
{
	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
		TestEntity = nullptr;
	}

	Super::Destroyed();
}

void MockEditor::ConstructUI ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString transFileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("MockEditor");

	EditorOperationsFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(EditorOperationsFrame))
	{
		EditorOperationsFrame->SetPosition(SD::Vector2::ZERO_VECTOR);
		EditorOperationsFrame->SetSize(SD::Vector2(0.475f, 0.825f));
		EditorOperationsFrame->SetLockedFrame(true);
		EditorOperationsFrame->SetBorderThickness(4.f);
		EditorOperationsFrame->SetCenterColor(SD::Color(48, 48, 48));

		SD::Float spacing = 0.025f;
		SD::Vector2 pos(spacing, spacing);
		SD::Vector2 buttonSize(1.f - (spacing * 2.f), 0.1f);

		EditorDescription = SD::LabelComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(EditorDescription))
		{
			EditorDescription->SetAutoRefresh(false);
			EditorDescription->SetPosition(pos);
			EditorDescription->SetSize(buttonSize);
			EditorDescription->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			EditorDescription->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			EditorDescription->SetWrapText(true);
			EditorDescription->SetClampText(true);
			EditorDescription->SetText(translator->TranslateText(TXT("MockEditorDescription"), transFileName, sectionName));
			EditorDescription->SetAutoRefresh(true);
			pos.Y += buttonSize.Y + spacing;
		}

		CreateEntityButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(CreateEntityButton))
		{
			CreateEntityButton->SetPosition(pos);
			CreateEntityButton->SetSize(buttonSize);
			CreateEntityButton->SetCaptionText(translator->TranslateText(TXT("CreateEntity"), transFileName, sectionName));
			CreateEntityButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleCreateEntityReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		LoadFromDevButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(LoadFromDevButton))
		{
			LoadFromDevButton->SetPosition(pos);
			LoadFromDevButton->SetSize(buttonSize);
			LoadFromDevButton->SetCaptionText(translator->TranslateText(TXT("LoadDev"), transFileName, sectionName));
			LoadFromDevButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleLoadFromDevReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		SaveToDevButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(SaveToDevButton))
		{
			SaveToDevButton->SetPosition(pos);
			SaveToDevButton->SetSize(buttonSize);
			SaveToDevButton->SetEnabled(false);
			SaveToDevButton->SetCaptionText(translator->TranslateText(TXT("SaveDev"), transFileName, sectionName));
			SaveToDevButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleSaveToDevReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		LoadFromContentButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(LoadFromContentButton))
		{
			LoadFromContentButton->SetPosition(pos);
			LoadFromContentButton->SetSize(buttonSize);
			LoadFromContentButton->SetCaptionText(translator->TranslateText(TXT("LoadContent"), transFileName, sectionName));
			LoadFromContentButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleLoadFromContentReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		ExportToContentButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(ExportToContentButton))
		{
			ExportToContentButton->SetPosition(pos);
			ExportToContentButton->SetSize(buttonSize);
			ExportToContentButton->SetEnabled(false);
			ExportToContentButton->SetCaptionText(translator->TranslateText(TXT("SaveContent"), transFileName, sectionName));
			ExportToContentButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleExportToContentReleased, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}

		SwitchEditorButton = SD::ButtonComponent::CreateObject();
		if (EditorOperationsFrame->AddComponent(SwitchEditorButton))
		{
			SwitchEditorButton->SetPosition(pos);
			SwitchEditorButton->SetSize(buttonSize);
			SwitchEditorButton->SetCaptionText(translator->TranslateText(TXT("SwitchToGenerated"), transFileName, sectionName));
			SwitchEditorButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleSwitchEditors, void, SD::ButtonComponent*));
			pos.Y += buttonSize.Y + spacing;
		}
	}

	InspectorScrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(InspectorScrollbar))
	{
		InspectorScrollbar->SetPosition(SD::Vector2(0.525f, 0.f));
		InspectorScrollbar->SetSize(SD::Vector2(1.f - InspectorScrollbar->ReadPosition().X, 0.825f));
		InspectorScrollbar->SetHideControlsWhenFull(true);
		InspectorScrollbar->SetVisibility(false); //Hide until we load an Entity

		InspectorEntity = GuiEntity::CreateObject();
		InspectorEntity->SetAutoSizeVertical(true);
		InspectorEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));

		InspectorScrollbar->SetViewedObject(InspectorEntity);

		InspectorList = SD::VerticalList::CreateObject();
		if (InspectorEntity->AddComponent(InspectorList))
		{
			InspectorList->SetPosition(SD::Vector2::ZERO_VECTOR);
			InspectorList->SetSize(SD::Vector2(1.f, 1.f));
			InspectorList->ComponentSpacing = 2.f;

			SD::Float lineHeight = 26.f;
			std::function<bool(SD::EditableField*, const SD::DString&)> initPropLine([&](SD::EditableField* fieldToInit, const SD::DString& translationKey)
			{
				if (InspectorList->AddComponent(fieldToInit))
				{
					fieldToInit->SetPosition(SD::Vector2::ZERO_VECTOR);
					fieldToInit->SetSize(SD::Vector2(1.f, lineHeight));
					if (fieldToInit->GetPropertyName())
					{
						fieldToInit->GetPropertyName()->SetText(translator->TranslateText(translationKey, transFileName, sectionName));
					}

					return true;
				}
			
				return false;
			});

			//Ints
			{
				SimpleIntField = SD::EditableInt::CreateObject();
				initPropLine(SimpleIntField, TXT("IntSimple"));
		
				ReadOnlyIntField = SD::EditableInt::CreateObject();
				if (initPropLine(ReadOnlyIntField, TXT("IntReadOnly")))
				{
					ReadOnlyIntField->SetReadOnly(true);
				}

				PositiveIntField = SD::EditableInt::CreateObject();
				if (initPropLine(PositiveIntField, TXT("IntPositive")))
				{
					PositiveIntField->SetMinValue(0);
				}

				NegativeIntField = SD::EditableInt::CreateObject();
				if (initPropLine(NegativeIntField, TXT("IntNegative")))
				{
					NegativeIntField->SetMaxValue(0);
				}

				IntBetweenZeroAndOneHundredField = SD::EditableInt::CreateObject();
				if (initPropLine(IntBetweenZeroAndOneHundredField, TXT("IntRanged")))
				{
					IntBetweenZeroAndOneHundredField->SetMinValue(0);
					IntBetweenZeroAndOneHundredField->SetMaxValue(100);
				}

				CountingIntField = SD::EditableInt::CreateObject();
				initPropLine(CountingIntField, TXT("IntCounting"));
			}

			//Floats
			{
				SimpleFloatField = SD::EditableFloat::CreateObject();
				initPropLine(SimpleFloatField, TXT("FloatSimple"));

				NoDecimalField = SD::EditableFloat::CreateObject();
				if (initPropLine(NoDecimalField, TXT("FloatNoDecimal")))
				{
					NoDecimalField->SetEnableDecimal(false);
				}

				MinNegHundredField = SD::EditableFloat::CreateObject();
				if (initPropLine(MinNegHundredField, TXT("FloatMinNegHundred")))
				{
					MinNegHundredField->SetHasMinValue(true);
					MinNegHundredField->SetMinValue(-100.f);
				}

				MaxHundredField = SD::EditableFloat::CreateObject();
				if (initPropLine(MaxHundredField, TXT("FloatMaxHundred")))
				{
					MaxHundredField->SetHasMaxValue(true);
					MaxHundredField->SetMaxValue(100.f);
				}

				ClampedFloatField = SD::EditableFloat::CreateObject();
				if (initPropLine(ClampedFloatField, TXT("FloatClamped")))
				{
					ClampedFloatField->SetHasMinValue(true);
					ClampedFloatField->SetMinValue(-1000.f);
					ClampedFloatField->SetHasMaxValue(true);
					ClampedFloatField->SetMaxValue(256.f);
				}
			}

			//Vector2
			{
				SimpleVectorField = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(SimpleVectorField))
				{
					SimpleVectorField->SetPosition(SD::Vector2::ZERO_VECTOR);
					SimpleVectorField->SetSize(SD::Vector2(1.f, lineHeight));
					SimpleVectorField->BindVector2(nullptr, SD::Vector2::ZERO_VECTOR);
					if (SimpleVectorField->GetPropertyName())
					{
						SimpleVectorField->GetPropertyName()->SetText(translator->TranslateText(TXT("Vector2Simple"), transFileName, sectionName));
					}
				}

				LockedYAxisVect2Field = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(LockedYAxisVect2Field))
				{
					LockedYAxisVect2Field->SetPosition(SD::Vector2::ZERO_VECTOR);
					LockedYAxisVect2Field->SetSize(SD::Vector2(1.f, lineHeight));

					//Bind to a nullptr Vector2 early to instantiate the SubPropertyComponents
					LockedYAxisVect2Field->BindVector2(nullptr, SD::Vector2::ZERO_VECTOR);
					if (SD::EditPropertyComponent* yField = LockedYAxisVect2Field->ReadSubPropertyComponents().at(1))
					{
						yField->SetReadOnly(true);
					}

					if (LockedYAxisVect2Field->GetPropertyName())
					{
						LockedYAxisVect2Field->GetPropertyName()->SetText(translator->TranslateText(TXT("Vector2LockedYAxis"), transFileName, sectionName));
					}
				}

				ClampedXAxisVect2Field = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(ClampedXAxisVect2Field))
				{
					ClampedXAxisVect2Field->SetPosition(SD::Vector2::ZERO_VECTOR);
					ClampedXAxisVect2Field->SetSize(SD::Vector2(1.f, lineHeight));

					//Bind to a nullptr Vector2 early to instantiate the SubPropertyComponents
					ClampedXAxisVect2Field->BindVector2(nullptr, SD::Vector2::ZERO_VECTOR);
					if (SD::EditableFloat* xField = dynamic_cast<SD::EditableFloat*>(ClampedXAxisVect2Field->ReadSubPropertyComponents().at(0)))
					{
						xField->SetHasMinValue(true);
						xField->SetHasMaxValue(true);
						xField->SetMinValue(0.f);
						xField->SetMaxValue(1000.f);
					}

					if (ClampedXAxisVect2Field->GetPropertyName())
					{
						ClampedXAxisVect2Field->GetPropertyName()->SetText(translator->TranslateText(TXT("Vector2ClampedXAxis"), transFileName, sectionName));
					}
				}
			}

			//Vector3
			{
				SimpleVector3Field = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(SimpleVector3Field))
				{
					SimpleVector3Field->SetPosition(SD::Vector2::ZERO_VECTOR);
					SimpleVector3Field->SetSize(SD::Vector2(1.f, lineHeight));
					SimpleVector3Field->BindVector3(nullptr, SD::Vector3::ZERO_VECTOR);
					if (SimpleVector3Field->GetPropertyName())
					{
						SimpleVector3Field->GetPropertyName()->SetText(translator->TranslateText(TXT("Vector3Simple"), transFileName, sectionName));
					}
				}

				LockedZAxisVect3Field = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(LockedZAxisVect3Field))
				{
					LockedZAxisVect3Field->SetPosition(SD::Vector2::ZERO_VECTOR);
					LockedZAxisVect3Field->SetSize(SD::Vector2(1.f, lineHeight));

					//Bind to a nullptr Vector3 early to instantiate the SubPropertyComponents
					LockedZAxisVect3Field->BindVector3(nullptr, SD::Vector3::ZERO_VECTOR);
					if (SD::EditPropertyComponent* zField = LockedZAxisVect3Field->ReadSubPropertyComponents().at(2))
					{
						zField->SetReadOnly(true);
					}
					
					if (LockedZAxisVect3Field->GetPropertyName())
					{
						LockedZAxisVect3Field->GetPropertyName()->SetText(translator->TranslateText(TXT("Vector3LockedZAxis"), transFileName, sectionName));
					}
				}
			}

			//DString
			{
				 SimpleStrField = SD::EditableDString::CreateObject();
				 initPropLine(SimpleStrField, TXT("StringSimple"));

				 VowelsOnlyField = SD::EditableDString::CreateObject();
				 if (initPropLine(VowelsOnlyField, TXT("StringVowels")))
				 {
					VowelsOnlyField->SetPermittedChars(TXT("[aeiouAEIOU]"));
				 }

				 ShortStrField = SD::EditableDString::CreateObject();
				 if (initPropLine(ShortStrField, TXT("StringShort")) && ShortStrField->GetPropValueField() != nullptr)
				 {
					ShortStrField->GetPropValueField()->MaxNumCharacters = 8;
				 }

				 MultiLineStrField = SD::EditableDString::CreateObject();
				 if (initPropLine(MultiLineStrField, TXT("StringMultiLine")))
				 {
					MultiLineStrField->SetLineType(SD::EditableDString::LT_MultiLine);
					MultiLineStrField->SetLineHeight(128.f);
				 }

				 CaptionTestField = SD::EditableDString::CreateObject();
				 if (initPropLine(CaptionTestField, TXT("StringCaptionTest")))
				 {
					CaptionTestField->OnApplyEdit = SDFUNCTION_1PARAM(this, MockEditor, HandleCaptionFieldEdit, void, SD::EditPropertyComponent*);
				 }
			}

			//Bool
			{
				SimpleBoolField = SD::EditableBool::CreateObject();
				if (InspectorList->AddComponent(SimpleBoolField))
				{
					SimpleBoolField->SetPosition(SD::Vector2::ZERO_VECTOR);
					SimpleBoolField->SetSize(SD::Vector2(1.f, lineHeight));
					if (SimpleBoolField->GetCheckbox()->GetCaptionComponent() != nullptr)
					{
						SimpleBoolField->GetCheckbox()->GetCaptionComponent()->SetText(translator->TranslateText(TXT("BoolSimple"), transFileName, sectionName));
					}
				}

				ReadOnlyBoolField = SD::EditableBool::CreateObject();
				if (InspectorList->AddComponent(ReadOnlyBoolField))
				{
					ReadOnlyBoolField->SetPosition(SD::Vector2::ZERO_VECTOR);
					ReadOnlyBoolField->SetSize(SD::Vector2(1.f, lineHeight));
					ReadOnlyBoolField->SetReadOnly(true);
					if (ReadOnlyBoolField->GetCheckbox()->GetCaptionComponent() != nullptr)
					{
						ReadOnlyBoolField->GetCheckbox()->GetCaptionComponent()->SetText(translator->TranslateText(TXT("BoolReadOnly"), transFileName, sectionName));
					}
				}
			}

			//Object
			{
				CreateObjComp = SD::EditableObjectComponent::CreateObject();
				if (InspectorList->AddComponent(CreateObjComp))
				{
					CreateObjComp->SetPosition(SD::Vector2::ZERO_VECTOR);
					CreateObjComp->SetSize(SD::Vector2(1.f, lineHeight));
					CreateObjComp->SetCreateNewObject(true);
					CreateObjComp->SetBaseClass(ObjTest_ChildClass::SStaticClass());

					if (SD::LabelComponent* propName = CreateObjComp->GetPropertyName())
					{
						propName->SetText(translator->TranslateText(TXT("ObjectCreateObj"), transFileName, sectionName));
					}
				}

				BoundParentComp = SD::EditableObjectComponent::CreateObject();
				if (InspectorList->AddComponent(BoundParentComp))
				{
					BoundParentComp->SetPosition(SD::Vector2::ZERO_VECTOR);
					BoundParentComp->SetSize(SD::Vector2(1.f, lineHeight));
					BoundParentComp->SetCreateNewObject(true);
					BoundParentComp->SetBaseClass(ObjTest_ParentClass::SStaticClass());

					if (SD::LabelComponent* propName = BoundParentComp->GetPropertyName())
					{
						propName->SetText(translator->TranslateText(TXT("ObjectBoundParent"), transFileName, sectionName));
					}
				}

				BoundChildComp = SD::EditableObjectComponent::CreateObject();
				if (InspectorList->AddComponent(BoundChildComp))
				{
					BoundChildComp->SetPosition(SD::Vector2::ZERO_VECTOR);
					BoundChildComp->SetSize(SD::Vector2(1.f, lineHeight));
					BoundChildComp->SetCreateNewObject(true);
					BoundChildComp->SetBaseClass(ObjTest_ChildClass::SStaticClass());

					if (SD::LabelComponent* propName = BoundChildComp->GetPropertyName())
					{
						propName->SetText(translator->TranslateText(TXT("ObjectBoundChild"), transFileName, sectionName));
					}
				}
			}

			//Enums
			{
				TestEnumDropdown = SD::EditableEnum::CreateObject();
				if (InspectorList->AddComponent(TestEnumDropdown))
				{
					TestEnumDropdown->SetPosition(SD::Vector2::ZERO_VECTOR);
					TestEnumDropdown->SetSize(SD::Vector2(1.f, lineHeight));

					if (SD::LabelComponent* propName = TestEnumDropdown->GetPropertyName())
					{
						propName->SetText(translator->TranslateText(TXT("TestEnum"), transFileName, sectionName));
					}
				}
			}

			//Structs
			{
				CaptionStructUi = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(CaptionStructUi))
				{
					CaptionStructUi->SetPosition(SD::Vector2::ZERO_VECTOR);
					CaptionStructUi->SetSize(SD::Vector2(1.f, lineHeight));

					//Populate the struct sub properties when binding the TestEntity
					if (CaptionStructUi->GetPropertyName() != nullptr)
					{
						CaptionStructUi->GetPropertyName()->SetText(translator->TranslateText(TXT("StructCaption"), transFileName, sectionName));
					}
				}

				NestedStructUi = SD::EditableStruct::CreateObject();
				if (InspectorList->AddComponent(NestedStructUi))
				{
					NestedStructUi->SetPosition(SD::Vector2::ZERO_VECTOR);
					NestedStructUi->SetSize(SD::Vector2(1.f, lineHeight));

					//Populate the struct sub properties when binding the TestEntity

					if (NestedStructUi->GetPropertyName() != nullptr)
					{
						NestedStructUi->GetPropertyName()->SetText(translator->TranslateText(TXT("StructNested"), transFileName, sectionName));
					}
				}
			}

			//Arrays
			{
				ArrayInt = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayInt))
				{
					ArrayInt->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayInt->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayInt->SetElementHeight(lineHeight);
					ArrayInt->EditItemTemplate().AddProp(new SD::EditableInt::SInspectorInt(TXT("Default"), SD::DString::EmptyString, nullptr));
					if (ArrayInt->GetPropertyName() != nullptr)
					{
						ArrayInt->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayInt"), transFileName, sectionName));
					}
				}

				ArrayFloat = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayFloat))
				{
					ArrayFloat->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayFloat->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayFloat->SetElementHeight(lineHeight);
					ArrayFloat->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Default"), SD::DString::EmptyString, nullptr));
					if (ArrayFloat->GetPropertyName() != nullptr)
					{
						ArrayFloat->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayFloat"), transFileName, sectionName));
					}
				}

				ArrayString = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayString))
				{
					ArrayString->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayString->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayString->SetElementHeight(lineHeight);
					ArrayString->EditItemTemplate().AddProp(new SD::EditableDString::SInspectorString(TXT("Default"), SD::DString::EmptyString, nullptr));
					if (ArrayString->GetPropertyName() != nullptr)
					{
						ArrayString->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayString"), transFileName, sectionName));
					}
				}

				ArrayVector2 = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayVector2))
				{
					ArrayVector2->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayVector2->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayVector2->SetElementHeight(lineHeight);
					ArrayVector2->EditItemTemplate().PushStruct(new SD::EditableStruct::SInspectorStruct(TXT("Default"), SD::DString::EmptyString));
					ArrayVector2->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("X"), SD::DString::EmptyString, nullptr));
					ArrayVector2->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Y"), SD::DString::EmptyString, nullptr));
					ArrayVector2->EditItemTemplate().PopStruct();
					if (ArrayVector2->GetPropertyName() != nullptr)
					{
						ArrayVector2->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayVector2"), transFileName, sectionName));
					}
				}

				ArrayVector3 = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayVector3))
				{
					ArrayVector3->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayVector3->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayVector3->SetElementHeight(lineHeight);
					ArrayVector3->EditItemTemplate().PushStruct(new SD::EditableStruct::SInspectorStruct(TXT("Default"), SD::DString::EmptyString));
					ArrayVector3->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("X"), SD::DString::EmptyString, nullptr));
					ArrayVector3->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Y"), SD::DString::EmptyString, nullptr));
					ArrayVector3->EditItemTemplate().AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Z"), SD::DString::EmptyString, nullptr));
					ArrayVector3->EditItemTemplate().PopStruct();
					if (ArrayVector3->GetPropertyName() != nullptr)
					{
						ArrayVector3->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayVector3"), transFileName, sectionName));
					}
				}

				ArrayReadOnly = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayReadOnly))
				{
					ArrayReadOnly->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayReadOnly->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayReadOnly->SetElementHeight(lineHeight);
					ArrayReadOnly->SetReadOnly(true);
					ArrayReadOnly->EditItemTemplate().AddProp(new SD::EditableInt::SInspectorInt(TXT("Default"), translator->TranslateText(TXT("ArrayReadOnlyTooltip"), transFileName, sectionName), nullptr));
					if (ArrayReadOnly->GetPropertyName() != nullptr)
					{
						ArrayReadOnly->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayReadOnly"), transFileName, sectionName));
					}
				}

				ArrayFixedSize = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayFixedSize))
				{
					ArrayFixedSize->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayFixedSize->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayFixedSize->SetElementHeight(lineHeight);
					ArrayFixedSize->SetFixedSize(true);
					ArrayFixedSize->EditItemTemplate().AddProp(new SD::EditableInt::SInspectorInt(TXT("Default"), translator->TranslateText(TXT("ArrayFixedSizeTooltip"), transFileName, sectionName), nullptr));
					if (ArrayFixedSize->GetPropertyName() != nullptr)
					{
						ArrayFixedSize->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayFixedSize"), transFileName, sectionName));
					}
				}

				ArrayMaxSize = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayMaxSize))
				{
					ArrayMaxSize->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayMaxSize->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayMaxSize->SetElementHeight(lineHeight);
					ArrayMaxSize->SetMaxItems(8);
					ArrayMaxSize->EditItemTemplate().AddProp(new SD::EditableInt::SInspectorInt(TXT("Default"), translator->TranslateText(TXT("ArrayMaxSizeTooltip"), transFileName, sectionName), nullptr, 2));
					if (ArrayMaxSize->GetPropertyName() != nullptr)
					{
						ArrayMaxSize->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayMaxSize"), transFileName, sectionName));
					}
				}

				ArrayOfObjs = SD::EditableArray::CreateObject();
				if (InspectorList->AddComponent(ArrayOfObjs))
				{
					ArrayOfObjs->SetPosition(SD::Vector2::ZERO_VECTOR);
					ArrayOfObjs->SetSize(SD::Vector2(1.f, lineHeight));
					ArrayOfObjs->SetElementHeight(lineHeight);
					ArrayOfObjs->EditItemTemplate().AddProp(new SD::EditableObjectComponent::SInspectorObject(TXT("Object"), translator->TranslateText(TXT("ArrayOfObjsTooltip"), transFileName, sectionName), nullptr, true, ObjTest_ParentClass::SStaticClass()));
					if (ArrayOfObjs->GetPropertyName() != nullptr)
					{
						ArrayOfObjs->GetPropertyName()->SetText(translator->TranslateText(TXT("ArrayOfObjs"), transFileName, sectionName));
					}
				}
			}
		}
	}

	SD::LabelComponent* objRegionLabel = SD::LabelComponent::CreateObject();
	if (AddComponent(objRegionLabel))
	{
		objRegionLabel->SetAutoRefresh(false);
		objRegionLabel->SetPosition(SD::Vector2(0.f, EditorOperationsFrame->ReadSize().Y));
		objRegionLabel->SetSize(SD::Vector2(1.f, 0.03f));
		objRegionLabel->SetWrapText(false);
		objRegionLabel->SetClampText(false);
		objRegionLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
		objRegionLabel->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
		objRegionLabel->SetText(translator->TranslateText(TXT("ObjectRegionLabel"), transFileName, sectionName));
		objRegionLabel->SetAutoRefresh(true);

		SelectObjScrollbar = SD::ScrollbarComponent::CreateObject();
		if (AddComponent(SelectObjScrollbar))
		{
			SelectObjScrollbar->SetPosition(SD::Vector2(0.f, objRegionLabel->ReadPosition().Y + objRegionLabel->ReadSize().Y));
			SelectObjScrollbar->SetSize(SD::Vector2(1.f, 1.f - SelectObjScrollbar->ReadPosition().Y));
			SelectObjScrollbar->SetHideControlsWhenFull(true);
		
			SelectObjectRegion* selectObjEntity = SelectObjectRegion::CreateObject();
			selectObjEntity->SetLabelSize(12.f);
			selectObjEntity->SetIconSize(64.f);
			selectObjEntity->SetLabelSpacing(2.f);
			selectObjEntity->SetObjSpacing(128.f);
			SelectObjScrollbar->SetViewedObject(selectObjEntity);
		}
	}
}

void MockEditor::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(CreateEntityButton);
	Focus->TabOrder.push_back(LoadFromDevButton);
	Focus->TabOrder.push_back(SaveToDevButton);
	Focus->TabOrder.push_back(ExportToContentButton);

	Focus->TabOrder.push_back(SimpleIntField->GetPropValueField());
	Focus->TabOrder.push_back(ReadOnlyIntField->GetPropValueField());
	Focus->TabOrder.push_back(PositiveIntField->GetPropValueField());
	Focus->TabOrder.push_back(NegativeIntField->GetPropValueField());
	Focus->TabOrder.push_back(IntBetweenZeroAndOneHundredField->GetPropValueField());
	Focus->TabOrder.push_back(CountingIntField->GetPropValueField());

	Focus->TabOrder.push_back(SimpleFloatField->GetPropValueField());
	Focus->TabOrder.push_back(NoDecimalField->GetPropValueField());
	Focus->TabOrder.push_back(MinNegHundredField->GetPropValueField());
	Focus->TabOrder.push_back(MaxHundredField->GetPropValueField());
	Focus->TabOrder.push_back(ClampedFloatField->GetPropValueField());

	Focus->TabOrder.push_back(SimpleStrField->GetPropValueField());
	Focus->TabOrder.push_back(VowelsOnlyField->GetPropValueField());
	Focus->TabOrder.push_back(ShortStrField->GetPropValueField());
	Focus->TabOrder.push_back(MultiLineStrField->GetPropValueField());
	Focus->TabOrder.push_back(CaptionTestField->GetPropValueField());

	Focus->TabOrder.push_back(SimpleBoolField->GetCheckbox());
	Focus->TabOrder.push_back(ReadOnlyBoolField->GetCheckbox());

	Focus->TabOrder.push_back(TestEnumDropdown->GetDropdown());
}

void MockEditor::PairInspectorToTestEntity ()
{
	CHECK(TestEntity != nullptr)

	SaveToDevButton->SetEnabled(true);
	ExportToContentButton->SetEnabled(true);

	InspectorScrollbar->SetVisibility(true);

	SimpleIntField->BindVariable(&TestEntity->SimpleInt);
	ReadOnlyIntField->BindVariable(&TestEntity->ReadOnlyInt);
	PositiveIntField->BindVariable(&TestEntity->PositiveInt);
	NegativeIntField->BindVariable(&TestEntity->NegativeInt);
	IntBetweenZeroAndOneHundredField->BindVariable(&TestEntity->IntBetweenZeroAndOneHundred);
	CountingIntField->BindVariable(&TestEntity->CountingInt);

	SimpleFloatField->BindVariable(&TestEntity->SimpleFloat);
	NoDecimalField->BindVariable(&TestEntity->NoDecimal);
	MinNegHundredField->BindVariable(&TestEntity->MinNegHundred);
	MaxHundredField->BindVariable(&TestEntity->MaxHundred);
	ClampedFloatField->BindVariable(&TestEntity->ClampedFloat);

	SimpleVectorField->BindVector2(&TestEntity->SimpleVect2, SD::Vector2::ZERO_VECTOR);
	LockedYAxisVect2Field->BindVector2(&TestEntity->LockedYAxisVect2, SD::Vector2::ZERO_VECTOR);
	ClampedXAxisVect2Field->BindVector2(&TestEntity->ClampedXAxisVect2, SD::Vector2::ZERO_VECTOR);

	SimpleVector3Field->BindVector3(&TestEntity->SimpleVect3, SD::Vector3::ZERO_VECTOR);
	LockedZAxisVect3Field->BindVector3(&TestEntity->LockedZAxisVect3, SD::Vector3::ZERO_VECTOR);

	SimpleStrField->BindVariable(&TestEntity->SimpleStr);
	VowelsOnlyField->BindVariable(&TestEntity->VowelsOnly);
	ShortStrField->BindVariable(&TestEntity->ShortStr);
	MultiLineStrField->BindVariable(&TestEntity->MultiLineStr);
	CaptionTestField->BindVariable(&TestEntity->CaptionTestStr);

	SimpleBoolField->BindVariable(&TestEntity->SimpleBool);
	ReadOnlyBoolField->BindVariable(&TestEntity->ReadOnlyBool);

	BoundParentComp->BindVariable(new SD::ObjBinder(&TestEntity->ParentRef));
	BoundChildComp->BindVariable(new SD::ObjBinder(&TestEntity->ChildRef));

	TestEnumDropdown->BindVariable<MockEditorTestEntity::Dynamic_Enum_ETestEnum, MockEditorTestEntity::ETestEnum>(&TestEntity->TestEnum);

	//Bind the nested structs
	if (CaptionStructUi != nullptr)
	{
		CaptionStructUi->DestroyStructElements();

		if (SD::EditPropertyComponent* structComp = CaptionStructUi->AddStructElement(new SD::StructElement<SD::DString, SD::EditableDString>(TXT("TestStr"), &TestEntity->CaptionStruct.TestStr, SD::DString::EmptyString)))
		{
			structComp->OnApplyEdit = SDFUNCTION_1PARAM(this, MockEditor, HandleCaptionFieldEdit, void, SD::EditPropertyComponent*);
		}

		if (SD::EditPropertyComponent* structComp = CaptionStructUi->AddStructElement(new SD::StructElement<SD::Int, SD::EditableInt>(TXT("TestInt"), &TestEntity->CaptionStruct.TestInt, 0)))
		{
			structComp->OnApplyEdit = SDFUNCTION_1PARAM(this, MockEditor, HandleCaptionFieldEdit, void, SD::EditPropertyComponent*);
		}
	}

	if (NestedStructUi != nullptr)
	{
		NestedStructUi->DestroyStructElements();

		NestedStructUi->AddStructElement(new SD::StructElement<bool, SD::EditableBool>(TXT("OuterBool"), &TestEntity->NestedStruct.OuterBool, false));
		SD::EditableStruct* structComp = dynamic_cast<SD::EditableStruct*>(NestedStructUi->AddStructElement(new SD::StructElement<MockEditorTestEntity::SNestedStruct, SD::EditableStruct>(TXT("InnerStruct"), nullptr, MockEditorTestEntity::SNestedStruct())));
		CHECK(structComp != nullptr)
		{
			structComp->AddStructElement(new SD::StructElement<SD::Int, SD::EditableInt>(TXT("InnerInt"), &TestEntity->NestedStruct.InnerStruct.InnerInt, 0));
			structComp->AddStructElement(new SD::StructElement<SD::Float, SD::EditableFloat>(TXT("InnerFloat"), &TestEntity->NestedStruct.InnerStruct.InnerFloat, -1.f));
			structComp->AddStructElement(new SD::StructElement<SD::DString, SD::EditableDString>(TXT("InnerString"), &TestEntity->NestedStruct.InnerStruct.InnerString, SD::DString::EmptyString));
		}
		NestedStructUi->AddStructElement(new SD::StructElement<SD::DString, SD::EditableDString>(TXT("OuterString"), &TestEntity->NestedStruct.OuterString, SD::DString::EmptyString));
	}

	ArrayInt->BindVariable<SD::Int>(&TestEntity->ArrayInt, 0);
	ArrayFloat->BindVariable<SD::Float>(&TestEntity->ArrayFloat, 0.f);
	ArrayString->BindVariable(&TestEntity->ArrayString, SD::DString::EmptyString);
	ArrayVector2->BindVariable(&TestEntity->ArrayVector2, SD::Vector2::ZERO_VECTOR);
	ArrayVector3->BindVariable(&TestEntity->ArrayVector3, SD::Vector3::ZERO_VECTOR);
	ArrayReadOnly->BindVariable<SD::Int>(&TestEntity->ArrayReadOnly, 0);
	ArrayFixedSize->BindVariable<SD::Int>(&TestEntity->ArrayFixedSize, 0);
	ArrayMaxSize->BindVariable<SD::Int>(&TestEntity->ArrayMaxSize, 0);

	//Serializing/Deserializing objects are not yet supported until asset packages are implemented.

	//Handle defaults
	const MockEditorTestEntity* cdo = dynamic_cast<const MockEditorTestEntity*>(TestEntity->GetDefaultObject());
	if (cdo != nullptr)
	{
		SimpleIntField->SetDefaultValue(cdo->SimpleInt);
		ReadOnlyIntField->SetDefaultValue(cdo->ReadOnlyInt);
		PositiveIntField->SetDefaultValue(cdo->PositiveInt);
		NegativeIntField->SetDefaultValue(cdo->NegativeInt);
		IntBetweenZeroAndOneHundredField->SetDefaultValue(cdo->IntBetweenZeroAndOneHundred);
		CountingIntField->SetDefaultValue(cdo->CountingInt);

		SimpleFloatField->SetDefaultValue(cdo->SimpleFloat);
		NoDecimalField->SetDefaultValue(cdo->NoDecimal);
		MinNegHundredField->SetDefaultValue(cdo->MinNegHundred);
		MaxHundredField->SetDefaultValue(cdo->MaxHundred);
		ClampedFloatField->SetDefaultValue(cdo->ClampedFloat);

		SimpleVectorField->SetDefaultVector2(cdo->SimpleVect2);
		LockedYAxisVect2Field->SetDefaultVector2(cdo->LockedYAxisVect2);
		ClampedXAxisVect2Field->SetDefaultVector2(cdo->ClampedXAxisVect2);

		SimpleVector3Field->SetDefaultVector3(cdo->SimpleVect3);
		LockedZAxisVect3Field->SetDefaultVector3(cdo->LockedZAxisVect3);

		SimpleStrField->SetDefaultValue(cdo->SimpleStr);
		VowelsOnlyField->SetDefaultValue(cdo->VowelsOnly);
		ShortStrField->SetDefaultValue(cdo->ShortStr);
		MultiLineStrField->SetDefaultValue(cdo->MultiLineStr);
		CaptionTestField->SetDefaultValue(cdo->CaptionTestStr);

		SimpleBoolField->SetDefaultValue(&cdo->SimpleBool);
		ReadOnlyBoolField->SetDefaultValue(&cdo->ReadOnlyBool);

		TestEnumDropdown->SetDefaultIndex(0);
	}

	//Construct arrays
	ArrayInt->ConstructArray();
	ArrayFloat->ConstructArray();
	ArrayString->ConstructArray();
	ArrayVector2->ConstructArray();
	ArrayVector3->ConstructArray();
	ArrayReadOnly->ConstructArray();
	ArrayFixedSize->ConstructArray();
	ArrayMaxSize->ConstructArray();
	ArrayOfObjs->ConstructArray();
}

void MockEditor::HandleCreateEntityReleased (SD::ButtonComponent* button)
{
	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}

	TestEntity = MockEditorTestEntity::CreateObject();
	PairInspectorToTestEntity();
}

void MockEditor::HandleLoadFromDevReleased (SD::ButtonComponent* button)
{
	MockEditorTestEntity* loadedEntity = MockEditorTestEntity::CreateObject();
	loadedEntity->LoadFromDevAsset(DevAssetFileAttr);

	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}

	TestEntity = loadedEntity;
	PairInspectorToTestEntity();
}

void MockEditor::HandleSaveToDevReleased (SD::ButtonComponent* button)
{
	if (!OS_CheckIfDirectoryExists(DevAssetFileAttr.ReadPath()))
	{
		OS_CreateDirectory(DevAssetFileAttr.ReadPath());
	}

	if (TestEntity != nullptr)
	{
		TestEntity->SaveAsDevAsset(DevAssetFileAttr);
	}
}

void MockEditor::HandleLoadFromContentReleased (SD::ButtonComponent* button)
{
	MockEditorTestEntity* loadedEntity = MockEditorTestEntity::CreateObject();
	MockEditorBinaryFile* binaryFile = MockEditorBinaryFile::CreateObject();
	binaryFile->TestEntity = loadedEntity;

	if (!binaryFile->OpenFile(ContentFileAttr))
	{
		loadedEntity->Destroy();
		binaryFile->Destroy();
		return;
	}

	binaryFile->Destroy();
	if (TestEntity != nullptr)
	{
		TestEntity->Destroy();
	}

	TestEntity = loadedEntity;
	PairInspectorToTestEntity();
}

void MockEditor::HandleExportToContentReleased (SD::ButtonComponent* button)
{
	if (TestEntity != nullptr)
	{
		if (!OS_CheckIfDirectoryExists(ContentFileAttr.ReadPath()))
		{
			OS_CreateDirectory(ContentFileAttr.ReadPath());
		}

		MockEditorBinaryFile* binaryFile = MockEditorBinaryFile::CreateObject();
		binaryFile->TestEntity = TestEntity;

		binaryFile->SaveFile(ContentFileAttr);
		binaryFile->Destroy();
	}
}

void MockEditor::HandleSwitchEditors (SD::ButtonComponent* button)
{
	if (SD::TooltipGuiEntity* tooltipManager = SD::GuiUtils::FindEntityInSameWindowAs<SD::TooltipGuiEntity>(SwitchEditorButton))
	{
		tooltipManager->HideTooltip();
	}

	if (OnSwapEditors.IsBounded())
	{
		OnSwapEditors.Execute();
	}
}

void MockEditor::HandleCaptionFieldEdit (SD::EditPropertyComponent* editedComp)
{
	if (SD::EditableField* editedField = dynamic_cast<SD::EditableField*>(editedComp))
	{
		CHECK(editedField->GetPropValueField() != nullptr)

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		SD::DString fileName = TXT("SandDuneTester");
		SD::DString sectionName = TXT("MockEditor");

		SD::DString captionText = SD::DString::CreateFormattedString(translator->TranslateText(TXT("CaptionTranslation"), fileName, sectionName), editedField->GetPropValueField()->GetContent());
		editedField->ShowCaption(captionText, SD::Color(200, 200, 64), SD::Color(96, 96, 128));

		if (SD::EditableStruct* editedStruct = dynamic_cast<SD::EditableStruct*>(editedComp->GetOwner()))
		{
			editedStruct->ShowCaption(translator->TranslateText(TXT("StructCaptionText"), fileName, sectionName), SD::Color(200, 225, 225), SD::Color(64, 36, 36));

			if (HideStructCaptionTick == nullptr)
			{
				HideStructCaptionTick = SD::TickComponent::CreateObject(TICK_GROUP_EDITOR);
				if (AddComponent(HideStructCaptionTick))
				{
					HideStructCaptionTick->SetTickHandler(SDFUNCTION_1PARAM(this, MockEditor, HandleHideStructCaption, void, SD::Float));
					HideStructCaptionTick->SetTickInterval(8.f);
				}
			}
			else
			{
				HideStructCaptionTick->ResetAccumulatedTickTime();
			}
		}
	}
}

void MockEditor::HandleHideStructCaption (SD::Float deltaSec)
{
	if (CaptionStructUi != nullptr)
	{
		CaptionStructUi->HideCaption();
	}

	if (HideStructCaptionTick != nullptr)
	{
		HideStructCaptionTick->Destroy();
		HideStructCaptionTick = nullptr;
	}
}
SD_TESTER_END
#endif