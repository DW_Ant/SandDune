/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  MockEditorTestEntity.cpp
=====================================================================
*/

#include "MockEditorTestEntity.h"
#include "ObjTest_ChildClass.h"
#include "ObjTest_ParentClass.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::MockEditorTestEntity, SD::Entity)
SD_TESTER_BEGIN

void MockEditorTestEntity::InitProps ()
{
	Super::InitProps();

	SimpleInt = 0;
	ReadOnlyInt = 10;
	PositiveInt = 0;
	NegativeInt = 0;
	IntBetweenZeroAndOneHundred = 0;
	CountingInt = 0;

	SimpleFloat = 0.f;
	NoDecimal = 0.f;
	MinNegHundred = 0.f;
	MaxHundred = 0.f;
	ClampedFloat = 0.f;

	SimpleVect2 = SD::Vector2::ZERO_VECTOR;
	LockedYAxisVect2 = SD::Vector2::ZERO_VECTOR;
	ClampedXAxisVect2 = SD::Vector2::ZERO_VECTOR;

	SimpleVect3 = SD::Vector3::ZERO_VECTOR;
	LockedZAxisVect3 = SD::Vector3::ZERO_VECTOR;

	SimpleBool = false;
	ReadOnlyBool = true;

	ParentRef = nullptr;
	ChildRef = nullptr;

	TestEnum = TE_FirstValue;

	//Add some elements to the fixed size and read only array since the end user won't be able to.
	for (SD::Int i = 0; i < 8; ++i)
	{
		ArrayReadOnly.push_back(i);
		ArrayFixedSize.push_back(i);
	}

	CountingIntTick = nullptr;
}

void MockEditorTestEntity::BeginObject ()
{
	Super::BeginObject();

	CountingIntTick = SD::TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(CountingIntTick))
	{
		CountingIntTick->SetTickInterval(1.f);
		CountingIntTick->SetTickHandler(SDFUNCTION_1PARAM(this, MockEditorTestEntity, HandleCountingIntTick, void, SD::Float));
	}
}

void MockEditorTestEntity::Destroyed ()
{
	if (ParentRef != nullptr)
	{
		ParentRef->Destroy();
		ParentRef = nullptr;
	}

	if (ChildRef != nullptr)
	{
		ChildRef->Destroy();
		ChildRef = nullptr;
	}

	Super::Destroyed();
}

void MockEditorTestEntity::SaveAsDevAsset (const SD::FileAttributes& configFile) const
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(configFile, true))
	{
		config->Destroy();
		return;
	}

	SD::DString sectionName = TXT("Mock Editor Test Entity");

	//This boiler plate code should be data driven or wrapped in a macro, but it's spelled out here for example/readability purposes.
	config->SaveProperty<SD::Int>(sectionName, TXT("SimpleInt"), SimpleInt);
	config->SaveProperty<SD::Int>(sectionName, TXT("ReadOnlyInt"), ReadOnlyInt);
	config->SaveProperty<SD::Int>(sectionName, TXT("PositiveInt"), PositiveInt);
	config->SaveProperty<SD::Int>(sectionName, TXT("NegativeInt"), NegativeInt);
	config->SaveProperty<SD::Int>(sectionName, TXT("IntBetweenZeroAndOneHundred"), IntBetweenZeroAndOneHundred);
	config->SaveProperty<SD::Int>(sectionName, TXT("CountingInt"), CountingInt);

	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleFloat"), SimpleFloat);
	config->SaveProperty<SD::Float>(sectionName, TXT("NoDecimal"), NoDecimal);
	config->SaveProperty<SD::Float>(sectionName, TXT("MinNegHundred"), MinNegHundred);
	config->SaveProperty<SD::Float>(sectionName, TXT("MaxHundred"), MaxHundred);
	config->SaveProperty<SD::Float>(sectionName, TXT("ClampedFloat"), ClampedFloat);

	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleVect2.X"), SimpleVect2.X);
	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleVect2.Y"), SimpleVect2.Y);
	config->SaveProperty<SD::Float>(sectionName, TXT("LockedYAxisVect2.X"), LockedYAxisVect2.X);
	config->SaveProperty<SD::Float>(sectionName, TXT("LockedYAxisVect2.Y"), LockedYAxisVect2.Y);
	config->SaveProperty<SD::Float>(sectionName, TXT("ClampedXAxisVect2.X"), ClampedXAxisVect2.X);
	config->SaveProperty<SD::Float>(sectionName, TXT("ClampedXAxisVect2.Y"), ClampedXAxisVect2.Y);

	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleVect3.X"), SimpleVect3.X);
	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleVect3.Y"), SimpleVect3.Y);
	config->SaveProperty<SD::Float>(sectionName, TXT("SimpleVect3.Z"), SimpleVect3.Z);

	config->SaveProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.X"), LockedZAxisVect3.X);
	config->SaveProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.Y"), LockedZAxisVect3.Y);
	config->SaveProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.Z"), LockedZAxisVect3.Z);

	config->SaveProperty(sectionName, TXT("SimpleStr"), SimpleStr);
	config->SaveProperty(sectionName, TXT("VowelsOnly"), VowelsOnly);
	config->SaveProperty(sectionName, TXT("ShortStr"), ShortStr);
	config->SaveProperty(sectionName, TXT("MultiLineStr"), MultiLineStr);
	config->SaveProperty(sectionName, TXT("CaptionTestStr"), CaptionTestStr);

	config->SaveProperty<SD::Bool>(sectionName, TXT("SimpleBool"), SimpleBool);
	config->SaveProperty<SD::Bool>(sectionName, TXT("ReadOnlyBool"), ReadOnlyBool);

	SD::DString parentObjStr;
	if (ParentRef != nullptr)
	{
		parentObjStr = ParentRef->GetCompleteEditorName();
		ParentRef->SaveToDevAsset(config);
	}
	config->SavePropertyText(sectionName, TXT("ParentRef"), parentObjStr);

	SD::DString childObjStr;
	if (ChildRef != nullptr)
	{
		childObjStr = ChildRef->GetCompleteEditorName();
		ChildRef->SaveToDevAsset(config);
	}
	config->SavePropertyText(sectionName, TXT("ChildRef"), childObjStr);

	config->SavePropertyText(sectionName, TXT("TestEnum"), EnumToString(TestEnum));

	config->SaveProperty<SD::DString>(sectionName, TXT("CaptionStruct.TestStr"), CaptionStruct.TestStr);
	config->SaveProperty<SD::Int>(sectionName, TXT("CaptionStruct.TestInt"), CaptionStruct.TestInt);

	config->SaveProperty<SD::Bool>(sectionName, TXT("NestedStruct.OuterBool"), NestedStruct.OuterBool);
	config->SaveProperty<SD::Int>(sectionName, TXT("NestedStruct.InnerStruct.InnerInt"), NestedStruct.InnerStruct.InnerInt);
	config->SaveProperty<SD::Float>(sectionName, TXT("NestedStruct.InnerStruct.InnerFloat"), NestedStruct.InnerStruct.InnerFloat);
	config->SaveProperty<SD::DString>(sectionName, TXT("NestedStruct.InnerStruct.InnerString"), NestedStruct.InnerStruct.InnerString);
	config->SaveProperty<SD::DString>(sectionName, TXT("NestedStruct.OuterString"), NestedStruct.OuterString);

	config->SaveArray(sectionName, TXT("ArrayInt"), ArrayInt);
	config->SaveArray(sectionName, TXT("ArrayFloat"), ArrayFloat);
	config->SaveArray(sectionName, TXT("ArrayString"), ArrayString);
	config->SaveArray(sectionName, TXT("ArrayVector2"), ArrayVector2);
	config->SaveArray(sectionName, TXT("ArrayVector3"), ArrayVector3);
	config->SaveArray(sectionName, TXT("ArrayReadOnly"), ArrayReadOnly);
	config->SaveArray(sectionName, TXT("ArrayFixedSize"), ArrayFixedSize);
	config->SaveArray(sectionName, TXT("ArrayMaxSize"), ArrayMaxSize);

	config->Destroy();
}

void MockEditorTestEntity::LoadFromDevAsset (const SD::FileAttributes& configFile)
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(configFile, false))
	{
		config->Destroy();
		return;
	}

	SD::DString sectionName = TXT("Mock Editor Test Entity");

	SimpleInt = config->GetProperty<SD::Int>(sectionName, TXT("SimpleInt"));
	ReadOnlyInt = config->GetProperty<SD::Int>(sectionName, TXT("ReadOnlyInt"));
	PositiveInt = config->GetProperty<SD::Int>(sectionName, TXT("PositiveInt"));
	NegativeInt = config->GetProperty<SD::Int>(sectionName, TXT("NegativeInt"));
	IntBetweenZeroAndOneHundred = config->GetProperty<SD::Int>(sectionName, TXT("IntBetweenZeroAndOneHundred"));
	CountingInt = config->GetProperty<SD::Int>(sectionName, TXT("CountingInt"));

	SimpleFloat = config->GetProperty<SD::Float>(sectionName, TXT("SimpleFloat"));
	NoDecimal = config->GetProperty<SD::Float>(sectionName, TXT("NoDecimal"));
	MinNegHundred = config->GetProperty<SD::Float>(sectionName, TXT("MinNegHundred"));
	MaxHundred = config->GetProperty<SD::Float>(sectionName, TXT("MaxHundred"));
	ClampedFloat = config->GetProperty<SD::Float>(sectionName, TXT("ClampedFloat"));

	SimpleVect2.X = config->GetProperty<SD::Float>(sectionName, TXT("SimpleVect2.X"));
	SimpleVect2.Y = config->GetProperty<SD::Float>(sectionName, TXT("SimpleVect2.Y"));
	LockedYAxisVect2.X = config->GetProperty<SD::Float>(sectionName, TXT("LockedYAxisVect2.X"));
	LockedYAxisVect2.Y = config->GetProperty<SD::Float>(sectionName, TXT("LockedYAxisVect2.Y"));
	ClampedXAxisVect2.X = config->GetProperty<SD::Float>(sectionName, TXT("ClampedXAxisVect2.X"));
	ClampedXAxisVect2.Y = config->GetProperty<SD::Float>(sectionName, TXT("ClampedXAxisVect2.Y"));

	SimpleVect3.X = config->GetProperty<SD::Float>(sectionName, TXT("SimpleVect3.X"));
	SimpleVect3.Y = config->GetProperty<SD::Float>(sectionName, TXT("SimpleVect3.Y"));
	SimpleVect3.Z = config->GetProperty<SD::Float>(sectionName, TXT("SimpleVect3.Z"));
	LockedZAxisVect3.X = config->GetProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.X"));
	LockedZAxisVect3.Y = config->GetProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.Y"));
	LockedZAxisVect3.Z = config->GetProperty<SD::Float>(sectionName, TXT("LockedZAxisVect3.Z"));

	SimpleStr = config->GetPropertyText(sectionName, TXT("SimpleStr"));
	VowelsOnly = config->GetPropertyText(sectionName, TXT("VowelsOnly"));
	ShortStr = config->GetPropertyText(sectionName, TXT("ShortStr"));
	MultiLineStr = config->GetPropertyText(sectionName, TXT("MultiLineStr"));
	CaptionTestStr = config->GetPropertyText(sectionName, TXT("CaptionTestStr"));

	SimpleBool = config->GetProperty<SD::Bool>(sectionName, TXT("SimpleBool"));
	ReadOnlyBool = config->GetProperty<SD::Bool>(sectionName, TXT("ReadOnlyBool"));

	SD::DString parentStr = config->GetPropertyText(sectionName, TXT("ParentRef"));
	if (!parentStr.IsEmpty())
	{
		if (ParentRef != nullptr)
		{
			ParentRef->Destroy();
		}

		ParentRef = ObjTest_ParentClass::CreateObject();
		//TODO: Set the ParentRef name equal to parentStr whenever packages are implemented.
		ParentRef->LoadFromDevAsset(config);
	}
	else if (ParentRef != nullptr)
	{
		ParentRef->Destroy();
		ParentRef = nullptr;
	}

	SD::DString childStr = config->GetPropertyText(sectionName, TXT("ChildRef"));
	if (!childStr.IsEmpty())
	{
		if (ChildRef != nullptr)
		{
			ChildRef->Destroy();
		}

		ChildRef = ObjTest_ChildClass::CreateObject();
		//TODO: Set the ChildRef name equal to childStr whenever packages are implemented.
		ChildRef->LoadFromDevAsset(config);
	}
	else if (ChildRef != nullptr)
	{
		ChildRef->Destroy();
		ChildRef = nullptr;
	}

	SD::DString propText = config->GetPropertyText(sectionName, TXT("TestEnum"));
	if (!propText.IsEmpty())
	{
		StringToEnum(propText, OUT TestEnum);
	}

	CaptionStruct.TestStr = config->GetPropertyText(sectionName, TXT("CaptionStruct.TestStr"));
	CaptionStruct.TestInt = config->GetProperty<SD::Int>(sectionName, TXT("CaptionStruct.TestInt"));

	NestedStruct.OuterBool = config->GetProperty<SD::Bool>(sectionName, TXT("NestedStruct.OuterBool"));
	NestedStruct.InnerStruct.InnerInt = config->GetProperty<SD::Int>(sectionName, TXT("NestedStruct.InnerStruct.InnerInt"));
	NestedStruct.InnerStruct.InnerFloat = config->GetProperty<SD::Float>(sectionName, TXT("NestedStruct.InnerStruct.InnerFloat"));
	NestedStruct.InnerStruct.InnerString = config->GetProperty<SD::DString>(sectionName, TXT("NestedStruct.InnerStruct.InnerString"));
	NestedStruct.OuterString = config->GetProperty<SD::DString>(sectionName, TXT("NestedStruct.OuterString"));

	config->GetArrayValues<SD::Int>(sectionName, TXT("ArrayInt"), OUT ArrayInt);
	config->GetArrayValues<SD::Float>(sectionName, TXT("ArrayFloat"), OUT ArrayFloat);
	config->GetArrayValues<SD::DString>(sectionName, TXT("ArrayString"), OUT ArrayString);
	config->GetArrayValues<SD::Vector2>(sectionName, TXT("ArrayVector2"), OUT ArrayVector2);
	config->GetArrayValues<SD::Vector3>(sectionName, TXT("ArrayVector3"), OUT ArrayVector3);
	config->GetArrayValues<SD::Int>(sectionName, TXT("ArrayReadOnly"), OUT ArrayReadOnly);
	config->GetArrayValues<SD::Int>(sectionName, TXT("ArrayFixedSize"), OUT ArrayFixedSize);
	config->GetArrayValues<SD::Int>(sectionName, TXT("ArrayMaxSize"), OUT ArrayMaxSize);

	config->Destroy();
}

void MockEditorTestEntity::HandleCountingIntTick (SD::Float deltaSec)
{
	++CountingInt;
}

SD_TESTER_END
#endif