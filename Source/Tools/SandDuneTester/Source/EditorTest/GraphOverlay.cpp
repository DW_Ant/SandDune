/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GraphOverlay.cpp
=====================================================================
*/

#include "GraphOverlay.h"
#include "GraphEditorTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::GraphOverlay, SD::GuiEntity)
SD_TESTER_BEGIN

const SD::DString GraphOverlay::ASSET_TYPE(TXT("GraphEditorTest"));

void GraphOverlay::InitProps ()
{
	Super::InitProps();

	TopPanel = nullptr;
	AssetCatalogueStatusLabel = nullptr;
	RefUpdaterStatusLabel = nullptr;
	GraphPanel = nullptr;

	InspectorPanel = nullptr;
	InspectorList = nullptr;

	GraphTester = nullptr;

	StatusBarSpacing = 0.075f;
	StatusBarSize = 0.05f;
}

void GraphOverlay::BeginObject ()
{
	Super::BeginObject();

	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	editorEngine->OnInvokeRefUpdater.RegisterHandler(SDFUNCTION_2PARAM(this, GraphOverlay, HandleInvokeRefUpdater, void, const SD::DString&, const SD::DString&));
	editorEngine->OnOpenFunction.RegisterHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleOpenFunction, void, SD::GraphFunction*));

	if (SD::TaskMessenger* taskMessage = editorEngine->GetWorkerMessenger())
	{
		std::vector<SD::DString> relevantTypes = {ASSET_TYPE};
		taskMessage->LaunchAssetCatalogue(SD::Directory::DEV_ASSET_DIRECTORY, SD::GraphAsset::DEV_ASSET_EXTENSION, relevantTypes, SDFUNCTION_1PARAM(this, GraphOverlay, HandleAssetCatalogueUpdate, void, SD::Float), SDFUNCTION_1PARAM(this, GraphOverlay, HandleAssetCatalogueComplete, void, const std::vector<SD::FileAttributes>&));
	}
}

void GraphOverlay::ConstructUI ()
{
	Super::ConstructUI();

	ConstructTopPanel();
	ConstructInspectorPanel();
	ConstructGraphPanel();
	ConstructStatusPanels();
}

void GraphOverlay::Destroyed ()
{
	if (SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find())
	{
		editorEngine->OnInvokeRefUpdater.UnregisterHandler(SDFUNCTION_2PARAM(this, GraphOverlay, HandleInvokeRefUpdater, void, const SD::DString&, const SD::DString&));
		editorEngine->OnOpenFunction.UnregisterHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleOpenFunction, void, SD::GraphFunction*));

		if (AssetCatalogueStatusLabel != nullptr) //If the asset catalogue is possibly running
		{
			if (SD::TaskMessenger* taskMessage = editorEngine->GetWorkerMessenger())
			{
				taskMessage->CancelAssetCatalogue();
			}
		}
	}

	Super::Destroyed();
}

void GraphOverlay::SetGraphTester (GraphEditorTester* newGraphTester)
{
	GraphTester = newGraphTester;

	if (GraphTester != nullptr && GraphPanel != nullptr)
	{
		if (GraphTester->GetEditedAsset() != nullptr)
		{
			RefreshInspector();
		}
	}
}

void GraphOverlay::ConstructTopPanel ()
{
	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString transFileName(TXT("SandDuneTester"));
	SD::DString sectionName(TXT("GraphOverlay"));

	SD::Vector2 buttonSize(0.05f, 0.05f);
	SD::Float spacing = 0.01f;
	SD::Float horPos = spacing;

	std::function<void(SD::ButtonComponent*)> initButton([&](SD::ButtonComponent* button)
	{
		button->SetPosition(horPos, spacing);
		button->SetSize(buttonSize.X, 1.f - (spacing * 2.f));
		horPos += buttonSize.X + spacing;

		if (button->GetCaptionComponent() != nullptr)
		{
			button->GetCaptionComponent()->Destroy();
		}
	});

	TopPanel = SD::FrameComponent::CreateObject();
	if (AddComponent(TopPanel))
	{
		TopPanel->SetPosition(SD::Vector2::ZERO_VECTOR);
		TopPanel->SetSize(SD::Vector2(1.f, buttonSize.Y + (spacing * 2.f)));
		TopPanel->SetBorderThickness(2.f);

		SD::ButtonComponent* newButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(newButton))
		{
			initButton(newButton);
			if (SD::FrameComponent* background = newButton->GetBackground())
			{
				if (const SD::Texture* newButtonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.NewButton")))
				{
					background->SetCenterTexture(newButtonTexture);
				}
			}

			newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleNewReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (newButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("NewButton"), transFileName, sectionName));
			}
		}

		SD::ButtonComponent* loadButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(loadButton))
		{
			initButton(loadButton);
			if (SD::FrameComponent* background = loadButton->GetBackground())
			{
				if (const SD::Texture* buttonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.OpenButton")))
				{
					background->SetCenterTexture(buttonTexture);
				}
			}

			loadButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleLoadReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (loadButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("LoadButton"), transFileName, sectionName));
			}
		}

		SD::ButtonComponent* loadSampleButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(loadSampleButton))
		{
			initButton(loadSampleButton);
			if (SD::FrameComponent* background = loadSampleButton->GetBackground())
			{
				if (const SD::Texture* buttonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.EditButton")))
				{
					background->SetCenterTexture(buttonTexture);
				}
			}

			loadSampleButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleLoadSampleReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (loadSampleButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("LoadSampleButton"), transFileName, sectionName));
			}
		}

		SD::ButtonComponent* saveButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(saveButton))
		{
			initButton(saveButton);
			if (SD::FrameComponent* background = saveButton->GetBackground())
			{
				if (const SD::Texture* buttonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.SaveButton")))
				{
					background->SetCenterTexture(buttonTexture);
				}
			}

			saveButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleSaveReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (saveButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("SaveButton"), transFileName, sectionName));
			}
		}

		SD::ButtonComponent* compileButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(compileButton))
		{
			initButton(compileButton);
			if (SD::FrameComponent* background = compileButton->GetBackground())
			{
				if (const SD::Texture* buttonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.ExportButton")))
				{
					background->SetCenterTexture(buttonTexture);
				}
			}

			compileButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleCompileReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (compileButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("CompileButton"), transFileName, sectionName));
			}
		}

		SD::ButtonComponent* runButton = SD::ButtonComponent::CreateObject();
		if (TopPanel->AddComponent(runButton))
		{
			initButton(runButton);
			if (SD::FrameComponent* background = runButton->GetBackground())
			{
				if (const SD::Texture* buttonTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Editor.ViewButton")))
				{
					background->SetCenterTexture(buttonTexture);
				}
			}

			runButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GraphOverlay, HandleRunReleased, void, SD::ButtonComponent*));

			SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
			if (runButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("RunButton"), transFileName, sectionName));
			}
		}
	}
}

void GraphOverlay::ConstructInspectorPanel ()
{
	CHECK(TopPanel != nullptr)

	InspectorPanel = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(InspectorPanel))
	{
		InspectorPanel->SetPosition(SD::Vector2(0.f, TopPanel->ReadSize().Y));
		InspectorPanel->SetAnchorTop(TopPanel->ReadSize().Y);
		InspectorPanel->SetAnchorBottom(0.f);
		InspectorPanel->SetSize(SD::Vector2(0.25f, 1.f));

		SD::GuiEntity* panelEntity = SD::GuiEntity::CreateObject();
		panelEntity->SetAutoSizeVertical(true);
		panelEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
		InspectorPanel->SetViewedObject(panelEntity);

		InspectorList = SD::VerticalList::CreateObject();
		if (panelEntity->AddComponent(InspectorList))
		{
			InspectorList->SetPosition(SD::Vector2::ZERO_VECTOR);
			InspectorList->SetSize(SD::Vector2(1.f, 1.f));
		}
	}
}

void GraphOverlay::ConstructGraphPanel ()
{
	CHECK(TopPanel != nullptr && InspectorPanel != nullptr)

	GraphPanel = SD::GraphViewport::CreateObject();
	if (AddComponent(GraphPanel))
	{
		GraphPanel->SetAnchorTop(TopPanel->ReadSize().Y);
		GraphPanel->SetAnchorRight(0.f);
		GraphPanel->SetAnchorBottom(0.f);
		GraphPanel->SetAnchorLeft(InspectorPanel->ReadSize().X);
	}
}

void GraphOverlay::ConstructStatusPanels ()
{
	//Construct the asset catalogue status bar
	SD::FrameComponent* catalogueFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(catalogueFrame))
	{
		catalogueFrame->SetAnchorTop(StatusBarSpacing);
		catalogueFrame->SetAnchorRight(0.01f);
		catalogueFrame->SetSize(SD::Vector2(0.25f, StatusBarSize));
		catalogueFrame->SetBorderThickness(1.f);
		StatusComponents.push_back(catalogueFrame);
		
		AssetCatalogueStatusLabel = SD::LabelComponent::CreateObject();
		if (catalogueFrame->AddComponent(AssetCatalogueStatusLabel))
		{
			AssetCatalogueStatusLabel->SetAutoRefresh(false);
			AssetCatalogueStatusLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
			AssetCatalogueStatusLabel->SetSize(SD::Vector2(1.f, 1.f));
			AssetCatalogueStatusLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			AssetCatalogueStatusLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			AssetCatalogueStatusLabel->SetWrapText(true);
			AssetCatalogueStatusLabel->SetClampText(true);
			AssetCatalogueStatusLabel->SetText(TXT("Launching Asset Catalogue"));
			AssetCatalogueStatusLabel->SetAutoRefresh(true);
		}
	}
}

void GraphOverlay::InitializeRefUpdaterStatusBar ()
{
	SD::FrameComponent* refUpdaterFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(refUpdaterFrame))
	{
		SD::Float spacePerElement = StatusBarSpacing + StatusBarSize;
		refUpdaterFrame->SetAnchorTop(StatusBarSpacing + (spacePerElement * SD::Int(StatusComponents.size()).ToFloat()));
		refUpdaterFrame->SetAnchorRight(0.01f);
		refUpdaterFrame->SetSize(SD::Vector2(0.25f, StatusBarSize));
		refUpdaterFrame->SetBorderThickness(1.f);
		StatusComponents.push_back(refUpdaterFrame);
		
		RefUpdaterStatusLabel = SD::LabelComponent::CreateObject();
		if (refUpdaterFrame->AddComponent(RefUpdaterStatusLabel))
		{
			RefUpdaterStatusLabel->SetAutoRefresh(false);
			RefUpdaterStatusLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
			RefUpdaterStatusLabel->SetSize(SD::Vector2(1.f, 1.f));
			RefUpdaterStatusLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			RefUpdaterStatusLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			RefUpdaterStatusLabel->SetWrapText(true);
			RefUpdaterStatusLabel->SetClampText(true);
			RefUpdaterStatusLabel->SetText(TXT("Updating Asset References"));
			RefUpdaterStatusLabel->SetAutoRefresh(true);
		}
	}
}

void GraphOverlay::RefreshInspector ()
{
	if (GraphTester != nullptr && InspectorList != nullptr && GraphTester->GetEditedAsset() != nullptr)
	{
		std::vector<SD::EntityComponent*> components;
		for (const SD::DPointer<SD::EntityComponent>& comp : InspectorList->ReadComponents())
		{
			if (dynamic_cast<SD::EditPropertyComponent*>(comp.Get()) != nullptr)
			{
				components.push_back(comp.Get());
			}
		}

		//The VerticalList would remove elements, but might as well empty the list now for performance reasons.
		SD::ContainerUtils::Empty(OUT InspectorList->EditListOrder());
		for (size_t i = 0; i < components.size(); ++i)
		{
			components.at(i)->Destroy();
		}
		SD::ContainerUtils::Empty(OUT components);

		SD::InspectorList propList;
		GraphTester->GetEditedAsset()->PopulateInspectorList(OUT propList);

		for (SD::EditPropertyComponent::SInspectorProperty* prop : propList.ReadPropList())
		{
			SD::EditPropertyComponent* propComp = prop->CreateBlankEditableComponent();
			if (propComp == nullptr)
			{
				continue;
			}

			if (InspectorList->AddComponent(propComp))
			{
				propComp->SetPosition(SD::Vector2::ZERO_VECTOR);
				propComp->SetSize(SD::Vector2(1.f, 0.f));
				prop->AssociatedComp = propComp;
				prop->EditPropComponent();
			}
		}
	}
}

void GraphOverlay::HandleAssetNameChanged (SD::GraphAsset* asset, const SD::DString& oldName, const SD::DString& newName)
{
	if (OldAssetName.IsEmpty())
	{
		OldAssetName = oldName;
	}
}

void GraphOverlay::HandleInvokeRefUpdater (const SD::DString& oldName, const SD::DString& newName)
{
	if (RefUpdaterStatusLabel == nullptr)
	{
		InitializeRefUpdaterStatusBar();
	}

	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	if (SD::TaskMessenger* messenger = editorEngine->GetWorkerMessenger())
	{
		messenger->LaunchReferenceUpdater(oldName, newName, SDFUNCTION_1PARAM(this, GraphOverlay, HandleReferenceUpdateProgress, void, SD::Float), SDFUNCTION(this, GraphOverlay, HandleReferenceUpdateComplete, void));
	}
}

void GraphOverlay::HandleOpenFunction (SD::GraphFunction* func)
{
	if (GraphPanel != nullptr)
	{
		GraphPanel->SetGraph(func->GetFunctionDefinition());
	}
}

void GraphOverlay::HandleNewReleased (SD::ButtonComponent* uiComponent)
{
	if (GraphTester == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to process new event since the GraphOverlay is not associated with a GraphTester."));
		return;
	}

	if (SD::GraphAsset* oldAsset = GraphTester->GetEditedAsset())
	{
		oldAsset->OnAssetNameChanged.ClearFunction();
	}

	SD::DString availAssetName = TXT("MyNewAsset");
	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(editorEngine != nullptr)
	SD::Int counter = 0;
	while (editorEngine->ReadLoadedAssets().contains(SD::HashedString(availAssetName)))
	{
		++counter;
		availAssetName = SD::DString::CreateFormattedString(TXT("MyNewAsset_%s"), counter);
	}

	SD::GraphAsset* newAsset = SD::GraphAsset::CreateObject();
	newAsset->SetLoadState(SD::GraphAsset::LS_Loaded);
	newAsset->InitializeAsset(availAssetName, true);
	newAsset->SetAssetType(GraphOverlay::ASSET_TYPE);
	newAsset->OnAssetNameChanged = SDFUNCTION_3PARAM(this, GraphOverlay, HandleAssetNameChanged, void, SD::GraphAsset*, const SD::DString&, const SD::DString&);
	GraphTester->SetEditedAsset(newAsset);
	RefreshInspector();
}

void GraphOverlay::HandleLoadReleased (SD::ButtonComponent* uiComponent)
{
	if (GraphTester != nullptr)
	{
		SD::FileDialogueOptions options(SD::FileDialogueOptions::DT_Open);
		options.bMultiSelect = false;
		options.bStrictFileTypesOnly = true;
		options.DefaultFolder = SD::Directory::DEV_ASSET_DIRECTORY;
		options.FileTypes.push_back(SD::GraphAsset::DEV_ASSET_EXTENSION);
		options.PersistenceId = SD::Int(SD::DString(TXT("GraphEditorTester")).GenerateHash());

		std::vector<SD::FileAttributes> selectedFiles;
		if (SD::OS_BrowseFiles(options, OUT selectedFiles))
		{
			if (selectedFiles.size() > 0)
			{
				SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
				CHECK(editorEngine != nullptr)

				SD::GraphAsset* newAsset = nullptr;

				SD::HashedString assetName(selectedFiles.at(0).GetFileName());
				if (editorEngine->ReadLoadedAssets().contains(assetName))
				{
					newAsset = editorEngine->ReadLoadedAssets().at(assetName);
				}

				if (newAsset == nullptr)
				{
					newAsset = SD::GraphAsset::CreateObject();
					newAsset->InitializeAsset(assetName.ToString(), true);
				}

				newAsset->SetSaveLocation(selectedFiles.at(0).ReadPath());
				newAsset->LoadFromFile();

				if (newAsset->ReadAssetType().Compare(GraphOverlay::ASSET_TYPE, SD::DString::CC_CaseSensitive) != 0)
				{
					SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot open asset %s since the GraphEditorTester will only open assets of type \"%s\". The specified asset is type \"%s\" instead."), assetName, GraphOverlay::ASSET_TYPE, newAsset->ReadAssetType());
					newAsset->Destroy();
					return;
				}

				if (SD::GraphAsset* oldAsset = GraphTester->GetEditedAsset())
				{
					oldAsset->OnAssetNameChanged.ClearFunction();
				}

				newAsset->OnAssetNameChanged = SDFUNCTION_3PARAM(this, GraphOverlay, HandleAssetNameChanged, void, SD::GraphAsset*, const SD::DString&, const SD::DString&);
				GraphTester->SetEditedAsset(newAsset);
				RefreshInspector();
			}
		}
	}
}

void GraphOverlay::HandleLoadSampleReleased (SD::ButtonComponent* uiComponent)
{
	//TODO: Implement me
}

void GraphOverlay::HandleSaveReleased (SD::ButtonComponent* uiComponent)
{
	if (GraphTester != nullptr)
	{
		if (SD::GraphAsset* editAsset = GraphTester->GetEditedAsset())
		{
			if (editAsset->ReadSaveLocation() == SD::Directory::INVALID_DIRECTORY)
			{
				SD::Directory selectedDirectory;
				SD::Int persistenceId(SD::DString(TXT("GraphEditorTester")).GenerateHash());
				if (SD::OS_BrowseDirectory(SD::Directory::DEV_ASSET_DIRECTORY, OUT selectedDirectory, persistenceId))
				{
					if (selectedDirectory != SD::Directory::INVALID_DIRECTORY)
					{
						editAsset->SetSaveLocation(selectedDirectory);
					}
				}
			}

			if (!OldAssetName.IsEmpty())
			{
				if (RefUpdaterStatusLabel == nullptr)
				{
					InitializeRefUpdaterStatusBar();
				}

				SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
				CHECK(editorEngine != nullptr)
				if (SD::TaskMessenger* messenger = editorEngine->GetWorkerMessenger())
				{
					messenger->LaunchReferenceUpdater(OldAssetName, editAsset->GetFullAssetName(), SDFUNCTION_1PARAM(this, GraphOverlay, HandleReferenceUpdateProgress, void, SD::Float), SDFUNCTION(this, GraphOverlay, HandleReferenceUpdateComplete, void));
				}

				OldAssetName = SD::DString::EmptyString;
			}

			editAsset->SaveToDevAsset();
		}
	}
}

void GraphOverlay::HandleCompileReleased (SD::ButtonComponent* uiComponent)
{
	//TODO: Implement me
}

void GraphOverlay::HandleRunReleased (SD::ButtonComponent* uiComponent)
{
	//TODO: Implement me
}

void GraphOverlay::HandleAssetCatalogueUpdate (SD::Float percentComplete)
{
	if (AssetCatalogueStatusLabel != nullptr)
	{
		AssetCatalogueStatusLabel->SetText(SD::DString::CreateFormattedString(TXT("Asset Catalogue Progress: %s%"), (percentComplete * 100.f).ToInt()));
	}
}

void GraphOverlay::HandleAssetCatalogueComplete (const std::vector<SD::FileAttributes>& completeCatalogue)
{
	SdTesterLog.Log(SD::LogCategory::LL_Log, TXT("The Asset Catalogue found the following assets. . ."));
	for (const SD::FileAttributes& foundAsset : completeCatalogue)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Log, TXT("    %s"), foundAsset.ReadFileName());
	}

	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	if (editorEngine != nullptr)
	{
		editorEngine->ReplaceAssetCatalogue(completeCatalogue);
	}

	if (AssetCatalogueStatusLabel != nullptr)
	{
		if (SD::FrameComponent* owningFrame = dynamic_cast<SD::FrameComponent*>(AssetCatalogueStatusLabel->GetOwner()))
		{
			SD::ContainerUtils::RemoveItem(OUT StatusComponents, owningFrame);
			owningFrame->Destroy();
		}
	}
}

void GraphOverlay::HandleReferenceUpdateProgress (SD::Float percentComplete)
{
	if (RefUpdaterStatusLabel != nullptr)
	{
		RefUpdaterStatusLabel->SetText(SD::DString::CreateFormattedString(TXT("Reference Updater Progress: %s%"), percentComplete));
	}
}

void GraphOverlay::HandleReferenceUpdateComplete ()
{
	if (RefUpdaterStatusLabel != nullptr)
	{
		if (SD::FrameComponent* owningFrame = dynamic_cast<SD::FrameComponent*>(RefUpdaterStatusLabel->GetOwner()))
		{
			SD::ContainerUtils::RemoveItem(OUT StatusComponents, owningFrame);
			owningFrame->Destroy();
			RefUpdaterStatusLabel = nullptr;
		}
	}
}
SD_TESTER_END
#endif