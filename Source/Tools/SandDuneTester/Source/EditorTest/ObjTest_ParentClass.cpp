/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ObjTest_ParentClass.cpp
=====================================================================
*/

#include "ObjTest_ParentClass.h"
#include "SelectObjectRegion.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::ObjTest_ParentClass, SD::Entity)
SD_TESTER_BEGIN

void ObjTest_ParentClass::InitProps ()
{
	Super::InitProps();

	TestInt = 0;
	TestFloat = 0.f;
	TestString = SD::DString::EmptyString;

	Sprite = nullptr;
}

void ObjTest_ParentClass::BeginObject ()
{
	Super::BeginObject();

	RegisterEditorInterface();
	InitSprite();
	RegisterToSelectRegion();
}

SD::CopiableObjectInterface* ObjTest_ParentClass::CreateCopiableInstanceOfMatchingType () const
{
	if (const SD::DClass* duneClass = StaticClass())
	{
		if (const ObjTest_ParentClass* cdo = dynamic_cast<const ObjTest_ParentClass*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void ObjTest_ParentClass::CopyPropertiesFrom (const SD::CopiableObjectInterface* objectTemplate)
{
	if (const ObjTest_ParentClass* objTemplate = dynamic_cast<const ObjTest_ParentClass*>(objectTemplate))
	{
		TestInt = objTemplate->TestInt;
		TestFloat = objTemplate->TestFloat;
		TestString = objTemplate->TestString;
	}
}

SD::DString ObjTest_ParentClass::GetEditorObjName () const
{
	CHECK(StaticClass() != nullptr)
	return StaticClass()->GetClassNameWithoutNamespace();
}

void ObjTest_ParentClass::PopulateInspectorList (SD::InspectorList& outList)
{
	outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("TestInt"), SD::DString::EmptyString, &TestInt));
	outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("TestFloat"), SD::DString::EmptyString, &TestFloat));
	outList.AddProp(new SD::EditableDString::SInspectorString(TXT("TestString"), SD::DString::EmptyString, &TestString));
}

void ObjTest_ParentClass::DeleteEditorObject ()
{
	Destroy();
}

void ObjTest_ParentClass::Destroyed ()
{
	UnregisterEditorInterface();

	Super::Destroyed();
}

void ObjTest_ParentClass::SaveToDevAsset (SD::ConfigWriter* config)
{
	CHECK(config != nullptr)

	//TODO: When packages are implemented, update this to include package name. The issue with this is that the EditorId is determined at runtime. Packages will be used to reserve names.
	SD::DString sectionName = GetCompleteEditorName();
	config->ClearSection(sectionName, true);

	if (const ObjTest_ParentClass* cdo = dynamic_cast<const ObjTest_ParentClass*>(GetDefaultObject()))
	{
		if (TestInt != cdo->TestInt)
		{
			config->SaveProperty<SD::Int>(sectionName, TXT("TestInt"), TestInt);
		}

		if (TestFloat != cdo->TestFloat)
		{
			config->SaveProperty<SD::Float>(sectionName, TXT("TestFloat"), TestFloat);
		}

		if (TestString.Compare(cdo->TestString, SD::DString::CC_CaseSensitive) != 0)
		{
			config->SavePropertyText(sectionName, TXT("TestString"), TestString);
		}
	}
}

void ObjTest_ParentClass::LoadFromDevAsset (SD::ConfigWriter* config)
{
	CHECK(config != nullptr)

	SD::DString sectionName = GetCompleteEditorName();

	if (config->ContainsProperty(sectionName, TXT("TestInt")))
	{
		TestInt = config->GetProperty<SD::Int>(sectionName, TXT("TestInt"));
	}

	if (config->ContainsProperty(sectionName, TXT("TestFloat")))
	{
		TestFloat = config->GetProperty<SD::Float>(sectionName, TXT("TestFloat"));
	}

	if (config->ContainsProperty(sectionName, TXT("TestString")))
	{
		TestString = config->GetPropertyText(sectionName, TXT("TestString"));
	}
}

void ObjTest_ParentClass::SaveToByteBuffer (SD::DataBuffer& outBuffer)
{
	outBuffer << TestInt << TestFloat << TestString;
}

void ObjTest_ParentClass::LoadFromBuffer (const SD::DataBuffer& incomingData)
{
	incomingData >> TestInt >> TestFloat >> TestString;
}

void ObjTest_ParentClass::InitSprite ()
{
	CHECK(Sprite == nullptr)

	Sprite = SD::SpriteComponent::CreateObject();
	if (AddComponent(Sprite))
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		Sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.TopDownFerns")));
		Sprite->SetSubDivision(2, 3, SD::RandomUtils::Rand(2), SD::RandomUtils::Rand(3));
	}
}

void ObjTest_ParentClass::RegisterToSelectRegion ()
{
	/*
	Ugly and slow solution. This simply finds a SelectObjectRegion and registers this instance to that region.
	Real cases wouldn't bother registering created objects (from EditableObjectComponent) to some viewport since the created objects are typically invisible to support a parent Entity.
	If the object is visible, then some other tool should be responsible for object placement such as placing things down in a map editor.
	I didn't want to create a separate ObjTest class to test against selection. When selecting objects, typically the user would be selecting objects that was placed externally.
	*/

	//Since this class is merely a test class to test against object selection and creation, the simplest approach is to have objects find and auto register themselves to the SelectObjectRegion.

	for (SD::ObjectIterator iter; iter.SelectedObject != nullptr; ++iter)
	{
		if (SelectObjectRegion* objRegion = dynamic_cast<SelectObjectRegion*>(iter.SelectedObject.Get()))
		{
			if (objRegion->IsVisible() && objRegion->GetScrollbarOwner() != nullptr && objRegion->GetScrollbarOwner()->IsVisible())
			{
				objRegion->RegisterTestObj(this);
				break;
			}
		}
	}
}
SD_TESTER_END
#endif