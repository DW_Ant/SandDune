/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  GeneratedInspectorTestEntity.cpp
=====================================================================
*/

#include "GeneratedInspectorTestEntity.h"
#include "ObjTest_ParentClass.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::GeneratedInspectorTestEntity, SD::Entity)
SD_TESTER_BEGIN

void GeneratedInspectorTestEntity::InitProps ()
{
	Super::InitProps();

	SimpleInt = 0;
	ReadOnlyInt = 10;
	PositiveInt = 0;
	NegativeInt = 0;
	IntBetweenZeroAndOneHundred = 0;
	CountingInt = 0;

	SimpleFloat = 0.f;
	NoDecimal = 0.f;
	MinNegHundred = 0.f;
	MaxHundred = 0.f;
	ClampedFloat = 0.f;

	SimpleVect2 = SD::Vector2::ZERO_VECTOR;

	SimpleVect3 = SD::Vector3::ZERO_VECTOR;

	SimpleBool = false;
	ReadOnlyBool = true;

	CreateObjInstance = nullptr;
	SelectObjInstance = nullptr;

	InspectorEnum = ITE_DefaultValue;

	//Add some elements to the fixed size and read only array since the end user won't be able to.
	for (SD::Int i = 0; i < 8; ++i)
	{
		ArrayReadOnly.push_back(i);
		ArrayFixedSize.push_back(i);
	}

	CountingIntTick = nullptr;
}

void GeneratedInspectorTestEntity::BeginObject ()
{
	Super::BeginObject();

	RegisterEditorInterface();

	CountingIntTick = SD::TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(CountingIntTick))
	{
		CountingIntTick->SetTickInterval(1.f);
		CountingIntTick->SetTickHandler(SDFUNCTION_1PARAM(this, GeneratedInspectorTestEntity, HandleCountingIntTick, void, SD::Float));
	}
}

SD::CopiableObjectInterface* GeneratedInspectorTestEntity::CreateCopiableInstanceOfMatchingType () const
{
	if (const SD::DClass* duneClass = StaticClass())
	{
		if (const GeneratedInspectorTestEntity* cdo = dynamic_cast<const GeneratedInspectorTestEntity*>(duneClass->GetDefaultObject()))
		{
			return cdo->CreateObjectOfMatchingClass();
		}
	}

	return nullptr;
}

void GeneratedInspectorTestEntity::CopyPropertiesFrom (const SD::CopiableObjectInterface* objectTemplate)
{
	if (const GeneratedInspectorTestEntity* inspectorTemplate = dynamic_cast<const GeneratedInspectorTestEntity*>(objectTemplate))
	{
		SimpleInt = inspectorTemplate->SimpleInt;
		ReadOnlyInt = inspectorTemplate->ReadOnlyInt;
		PositiveInt = inspectorTemplate->PositiveInt;
		NegativeInt = inspectorTemplate->NegativeInt;
		IntBetweenZeroAndOneHundred = inspectorTemplate->IntBetweenZeroAndOneHundred;
		CountingInt = inspectorTemplate->CountingInt;

		SimpleFloat = inspectorTemplate->SimpleFloat;
		NoDecimal = inspectorTemplate->NoDecimal;
		MinNegHundred = inspectorTemplate->MinNegHundred;
		MaxHundred = inspectorTemplate->MaxHundred;
		ClampedFloat = inspectorTemplate->ClampedFloat;

		SimpleVect2 = inspectorTemplate->SimpleVect2;
		SimpleVect3 = inspectorTemplate->SimpleVect3;

		SimpleStr = inspectorTemplate->SimpleStr;
		VowelsOnly = inspectorTemplate->VowelsOnly;
		ShortStr = inspectorTemplate->ShortStr;
		MultiLineStr = inspectorTemplate->MultiLineStr;

		SimpleBool = inspectorTemplate->SimpleBool;
		ReadOnlyBool = inspectorTemplate->ReadOnlyBool;

		CreateObjInstance = inspectorTemplate->CreateObjInstance;
		SelectObjInstance = inspectorTemplate->SelectObjInstance;

		InspectorEnum = inspectorTemplate->InspectorEnum;


		ArrayInt.resize(inspectorTemplate->ArrayInt.size());
		for (size_t i = 0; i < ArrayInt.size(); ++i)
		{
			ArrayInt.at(i) = inspectorTemplate->ArrayInt.at(i);
		}

		ArrayFloat.resize(inspectorTemplate->ArrayFloat.size());
		for (size_t i = 0; i < ArrayFloat.size(); ++i)
		{
			ArrayFloat.at(i) = inspectorTemplate->ArrayFloat.at(i);
		}

		ArrayString.resize(inspectorTemplate->ArrayString.size());
		for (size_t i = 0; i < ArrayString.size(); ++i)
		{
			ArrayString.at(i) = inspectorTemplate->ArrayString.at(i);
		}

		ArrayVector2.resize(inspectorTemplate->ArrayVector2.size());
		for (size_t i = 0; i < ArrayVector2.size(); ++i)
		{
			ArrayVector2.at(i) = inspectorTemplate->ArrayVector2.at(i);
		}

		ArrayVector3.resize(inspectorTemplate->ArrayVector3.size());
		for (size_t i = 0; i < ArrayVector3.size(); ++i)
		{
			ArrayVector3.at(i) = inspectorTemplate->ArrayVector3.at(i);
		}

		ArrayReadOnly.resize(inspectorTemplate->ArrayReadOnly.size());
		for (size_t i = 0; i < ArrayReadOnly.size(); ++i)
		{
			ArrayReadOnly.at(i) = inspectorTemplate->ArrayReadOnly.at(i);
		}

		ArrayFixedSize.resize(inspectorTemplate->ArrayFixedSize.size());
		for (size_t i = 0; i < ArrayFixedSize.size(); ++i)
		{
			ArrayFixedSize.at(i) = inspectorTemplate->ArrayFixedSize.at(i);
		}
		
		ArrayMaxSize.resize(inspectorTemplate->ArrayMaxSize.size());
		for (size_t i = 0; i < ArrayMaxSize.size(); ++i)
		{
			ArrayMaxSize.at(i) = inspectorTemplate->ArrayMaxSize.at(i);
		}
	}
}

SD::DString GeneratedInspectorTestEntity::GetEditorObjName () const
{
	CHECK(StaticClass() != nullptr)
	return StaticClass()->GetClassNameWithoutNamespace();
}

void GeneratedInspectorTestEntity::PopulateInspectorList (SD::InspectorList& outList)
{
	const GeneratedInspectorTestEntity* cdo = dynamic_cast<const GeneratedInspectorTestEntity*>(StaticClass()->GetDefaultObject());
	CHECK(cdo != nullptr)

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString fileName(TXT("SandDuneTester"));
	SD::DString sectionName(TXT("GeneratedInspectorTestEntity"));

	outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("SimpleInt"), SD::DString::EmptyString, &SimpleInt, cdo->SimpleInt));

	{
		SD::EditableInt::SInspectorInt* newProp = outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("ReadOnlyInt"), SD::DString::EmptyString, &ReadOnlyInt, cdo->ReadOnlyInt));
		newProp->bReadOnly = true;
	}

	{
		SD::EditableInt::SInspectorInt* newProp = outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("PositiveInt"), translator->TranslateText(TXT("PositiveIntTooltip"), fileName, sectionName), &PositiveInt, cdo->PositiveInt));
		newProp->bHasMinValue = true;
		newProp->ValueRange.Min = 0;
	}

	{
		SD::EditableInt::SInspectorInt* newProp = outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("NegativeInt"), translator->TranslateText(TXT("NegativeIntTooltip"), fileName, sectionName), &NegativeInt, cdo->NegativeInt));
		newProp->bHasMaxValue = true;
		newProp->ValueRange.Max = 0;
	}

	{
		SD::EditableInt::SInspectorInt* newProp = outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("RangedInt"), translator->TranslateText(TXT("RangedIntTooltip"), fileName, sectionName), &IntBetweenZeroAndOneHundred, cdo->IntBetweenZeroAndOneHundred));
		newProp->bHasMinValue = true;
		newProp->bHasMaxValue = true;
		newProp->ValueRange = SD::Range<SD::Int>(0, 100);
	}

	outList.AddProp(new SD::EditableInt::SInspectorInt(TXT("CountingInt"), translator->TranslateText(TXT("CountingIntTooltip"), fileName, sectionName), &CountingInt, cdo->CountingInt));
	outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("SimpleFloat"), SD::DString::EmptyString, &SimpleFloat, cdo->SimpleFloat));

	{
		SD::EditableFloat::SInspectorFloat* newProp = outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("NoDecimal"), translator->TranslateText(TXT("NoDecimalTooltip"), fileName, sectionName), &NoDecimal, cdo->NoDecimal));
		newProp->bEnableDecimal = false;
	}

	{
		SD::EditableFloat::SInspectorFloat* newProp = outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("MinNegHundred"), translator->TranslateText(TXT("MinNegHundredTooltip"), fileName, sectionName), &MinNegHundred, cdo->MinNegHundred));
		newProp->bHasMinValue = true;
		newProp->ValueRange.Min = -100.f;
	}

	{
		SD::EditableFloat::SInspectorFloat* newProp = outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("MaxHundred"), translator->TranslateText(TXT("MaxHundredTooltip"), fileName, sectionName), &MaxHundred, cdo->MaxHundred));
		newProp->bHasMaxValue = true;
		newProp->ValueRange.Max = 100.f;
	}

	{
		SD::EditableFloat::SInspectorFloat* newProp = outList.AddProp(new SD::EditableFloat::SInspectorFloat(TXT("ClampedFloat"), translator->TranslateText(TXT("ClampedFloatTooltip"), fileName, sectionName), &ClampedFloat, cdo->ClampedFloat));
		newProp->bHasMinValue = true;
		newProp->bHasMaxValue = true;
		newProp->ValueRange = SD::Range<SD::Float>(-1000.f, 256.f);
	}

	AddVector2ToInspectorList(TXT("SimpleVect2"), SimpleVect2, OUT outList, cdo->SimpleVect2);
	AddVector3ToInspectorList(TXT("SimpleVect3"), SimpleVect3, OUT outList, cdo->SimpleVect3);
	outList.AddProp(new SD::EditableDString::SInspectorString(TXT("SimpleStr"), SD::DString::EmptyString, &SimpleStr, cdo->SimpleStr));

	{
		SD::EditableDString::SInspectorString* newProp = outList.AddProp(new SD::EditableDString::SInspectorString(TXT("VowelsOnly"), translator->TranslateText(TXT("VowelsOnlyTooltip"), fileName, sectionName), &VowelsOnly, cdo->VowelsOnly));
		newProp->PermittedChars = TXT("[aeiouAEIOU]");
	}

	{
		SD::EditableDString::SInspectorString* newProp = outList.AddProp(new SD::EditableDString::SInspectorString(TXT("ShortStr"), translator->TranslateText(TXT("ShortStrTooltip"), fileName, sectionName), &ShortStr, cdo->ShortStr));
		newProp->MaxCharacters = 8;
	}

	{
		SD::EditableDString::SInspectorString* newProp = outList.AddProp(new SD::EditableDString::SInspectorString(TXT("MultiLineStr"), SD::DString::EmptyString, &MultiLineStr, cdo->MultiLineStr));
		newProp->bMultiLine = true;
	}

	outList.AddProp(new SD::EditableBool::SInspectorBool(TXT("SimpleBool"), SD::DString::EmptyString, &SimpleBool, cdo->SimpleBool));

	{
		SD::EditableBool::SInspectorBool* newProp = outList.AddProp(new SD::EditableBool::SInspectorBool(TXT("ReadOnlyBool"), SD::DString::EmptyString, &ReadOnlyBool, cdo->ReadOnlyBool));
		newProp->bReadOnly = true;
	}

	{
		outList.AddProp(new SD::EditableObjectComponent::SInspectorObject(TXT("CreateObjInstance"), SD::DString::EmptyString, new SD::ObjBinder(&CreateObjInstance), true, ObjTest_ParentClass::SStaticClass()));
		outList.AddProp(new SD::EditableObjectComponent::SInspectorObject(TXT("SelectObjInstance"), SD::DString::EmptyString, new SD::ObjBinder(&SelectObjInstance), false, ObjTest_ParentClass::SStaticClass()));
	}

	outList.AddProp(new SD::EditableEnum::SInspectorEnum(TXT("InspectorEnum"), SD::DString::EmptyString, new Dynamic_Enum_EInspectorTestEnum(&InspectorEnum), SD::Int(ITE_DefaultValue)));

	outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayInt"), SD::DString::EmptyString, SD::EditableInt::SStaticClass(), SD::EditableArray::SInspectorArray::CreateBasicBinder<SD::Int>(&ArrayInt, 0)));
	outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayFloat"), SD::DString::EmptyString, SD::EditableFloat::SStaticClass(), SD::EditableArray::SInspectorArray::CreateBasicBinder<SD::Float>(&ArrayFloat, 0.f)));
	outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayString"), SD::DString::EmptyString, SD::EditableDString::SStaticClass(), SD::EditableArray::SInspectorArray::CreateBasicBinder(&ArrayString, SD::DString::EmptyString)));

	{
		SD::InspectorList* arrayTemplate = new SD::InspectorList();
		arrayTemplate->PushStruct(new SD::EditableStruct::SInspectorStruct(TXT("Default"), SD::DString::EmptyString));
		{
			arrayTemplate->AddProp(new SD::EditableFloat::SInspectorFloat(TXT("X"), SD::DString::EmptyString, nullptr));
			arrayTemplate->AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Y"), SD::DString::EmptyString, nullptr));
		}
		arrayTemplate->PopStruct();

		outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayVector2"), SD::DString::EmptyString, arrayTemplate, SD::EditableArray::SInspectorArray::CreateBasicBinder(&ArrayVector2, SD::Vector2::ZERO_VECTOR)));
	}

	{
		SD::InspectorList* arrayTemplate = new SD::InspectorList();
		arrayTemplate->PushStruct(new SD::EditableStruct::SInspectorStruct(TXT("Default"), SD::DString::EmptyString));
		{
			arrayTemplate->AddProp(new SD::EditableFloat::SInspectorFloat(TXT("X"), SD::DString::EmptyString, nullptr));
			arrayTemplate->AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Y"), SD::DString::EmptyString, nullptr));
			arrayTemplate->AddProp(new SD::EditableFloat::SInspectorFloat(TXT("Z"), SD::DString::EmptyString, nullptr));
		}
		arrayTemplate->PopStruct();

		outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayVector3"), SD::DString::EmptyString, arrayTemplate, SD::EditableArray::SInspectorArray::CreateBasicBinder(&ArrayVector3, SD::Vector3::ZERO_VECTOR)));
	}

	const SD::EditableInt* defaultEditInt = dynamic_cast<const SD::EditableInt*>(SD::EditableInt::SStaticClass()->GetDefaultObject());
	{
		SD::EditableArray::SInspectorArray* newProp = outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayReadOnly"), SD::DString::EmptyString, defaultEditInt, SD::EditableArray::SInspectorArray::CreateBasicBinder<SD::Int>(&ArrayReadOnly, 0)));
		newProp->bReadOnly = true;
	}

	{
		SD::EditableArray::SInspectorArray* newProp = outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayFixedSize"), translator->TranslateText(TXT("ArrayFixedSizeTooltip"), fileName, sectionName), defaultEditInt, SD::EditableArray::SInspectorArray::CreateBasicBinder<SD::Int>(&ArrayFixedSize, 0)));
		newProp->bFixedSize = true;
	}

	{
		SD::EditableArray::SInspectorArray* newProp = outList.AddProp(new SD::EditableArray::SInspectorArray(TXT("ArrayMaxSize"), translator->TranslateText(TXT("ArrayMaxSizeTooltip"), fileName, sectionName), defaultEditInt, SD::EditableArray::SInspectorArray::CreateBasicBinder<SD::Int>(&ArrayMaxSize, 2)));
		newProp->MaxItems = 8;
	}
}

void GeneratedInspectorTestEntity::DeleteEditorObject ()
{
	Destroy();
}

void GeneratedInspectorTestEntity::Destroyed ()
{
	UnregisterEditorInterface();

	Super::Destroyed();
}

void GeneratedInspectorTestEntity::HandleCountingIntTick (SD::Float deltaSec)
{
	++CountingInt;
}
SD_TESTER_END
#endif