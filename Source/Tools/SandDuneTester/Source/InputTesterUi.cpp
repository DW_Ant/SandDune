/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  InputTesterUi.cpp
=====================================================================
*/

#include "InputTester.h"
#include "InputTesterUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::InputTesterUi, SD::GuiEntity)
SD_TESTER_BEGIN

void InputTesterUi::InitProps ()
{
	Super::InitProps();

	TestOwner = nullptr;

	Input = nullptr;
	Circle = nullptr;

	RedBind = nullptr;
	GreenBind = nullptr;
	BlueBind = nullptr;
	ExitButton = nullptr;
}

void InputTesterUi::BeginObject ()
{
	Super::BeginObject();

	Input = SD::InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		//Use convenience keybind methods
		Input->AddKeybind(sf::Keyboard::R, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddRedBind, bool));
		Input->AddKeybind(sf::Keyboard::G, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddGreenBind, bool));
		Input->AddKeybind(sf::Keyboard::B, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddBlueBind, bool));

		Input->AddKeybind(sf::Keyboard::R, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveRedBind, bool));
		Input->AddKeybind(sf::Keyboard::G, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveGreenBind, bool));
		Input->AddKeybind(sf::Keyboard::B, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveBlueBind, bool));

		Input->AddKeybind(sf::Keyboard::R, false, SDFUNCTION(this, InputTesterUi, HandleRedBind, bool));
		Input->AddKeybind(sf::Keyboard::G, false, SDFUNCTION(this, InputTesterUi, HandleGreenBind, bool));
		Input->AddKeybind(sf::Keyboard::B, false, SDFUNCTION(this, InputTesterUi, HandleBlueBind, bool));
	}
}

void InputTesterUi::ConstructUI ()
{
	Super::ConstructUI();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::Float lineHeight = 0.125f;
	SD::Float margin = 0.03f;

	SD::FrameComponent* menuFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(menuFrame))
	{
		menuFrame->SetPosition(SD::Vector2::ZERO_VECTOR);
		menuFrame->SetSize(SD::Vector2(0.5f, 1.f));
		menuFrame->SetBorderThickness(2.f);
		menuFrame->SetCenterColor(SD::Color(64, 64, 64));

		SD::Float vertPos = margin;

		SD::LabelComponent* redPrompt = SD::LabelComponent::CreateObject();
		if (menuFrame->AddComponent(redPrompt))
		{
			redPrompt->SetAutoRefresh(false);
			redPrompt->SetAnchorTop(vertPos);
			redPrompt->SetAnchorLeft(margin);
			redPrompt->SetSize(SD::Vector2(0.45f, lineHeight));
			redPrompt->SetWrapText(false);
			redPrompt->SetClampText(true);
			redPrompt->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			redPrompt->SetText(translator->TranslateText(TXT("RedKeybind"), TXT("SandDuneTester"), TXT("InputTester")));
			redPrompt->SetAutoRefresh(true);
		}

		RedBind = SD::TextFieldComponent::CreateObject();
		if (menuFrame->AddComponent(RedBind))
		{
			RedBind->SetAutoRefresh(false);
			RedBind->SetAnchorTop(vertPos);
			RedBind->SetAnchorRight(margin);
			RedBind->SetSize(SD::Vector2(0.45f, lineHeight));
			RedBind->SetWrapText(false);
			RedBind->SetClampText(true);
			RedBind->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			RedBind->SetText(SD::InputUtils::TranslateToHumanReadableText(sf::Keyboard::R));
			RedBind->OnEdit = SDFUNCTION_1PARAM(this, InputTesterUi, HandleRedEdit, bool, const sf::Event&);
			RedBind->SetAutoRefresh(true);
		}

		vertPos += lineHeight + margin;

		SD::LabelComponent* greenPrompt = SD::LabelComponent::CreateObject();
		if (menuFrame->AddComponent(greenPrompt))
		{
			greenPrompt->SetAutoRefresh(false);
			greenPrompt->SetAnchorTop(vertPos);
			greenPrompt->SetAnchorLeft(margin);
			greenPrompt->SetSize(SD::Vector2(0.45f, lineHeight));
			greenPrompt->SetWrapText(false);
			greenPrompt->SetClampText(true);
			greenPrompt->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			greenPrompt->SetText(translator->TranslateText(TXT("GreenKeybind"), TXT("SandDuneTester"), TXT("InputTester")));
			greenPrompt->SetAutoRefresh(true);
		}

		GreenBind = SD::TextFieldComponent::CreateObject();
		if (menuFrame->AddComponent(GreenBind))
		{
			GreenBind->SetAutoRefresh(false);
			GreenBind->SetAnchorTop(vertPos);
			GreenBind->SetAnchorRight(margin);
			GreenBind->SetSize(SD::Vector2(0.45f, lineHeight));
			GreenBind->SetWrapText(false);
			GreenBind->SetClampText(true);
			GreenBind->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			GreenBind->SetText(SD::InputUtils::TranslateToHumanReadableText(sf::Keyboard::G));
			GreenBind->OnEdit = SDFUNCTION_1PARAM(this, InputTesterUi, HandleGreenEdit, bool, const sf::Event&);
			GreenBind->SetAutoRefresh(true);
		}

		vertPos += lineHeight + margin;

		SD::LabelComponent* bluePrompt = SD::LabelComponent::CreateObject();
		if (menuFrame->AddComponent(bluePrompt))
		{
			bluePrompt->SetAutoRefresh(false);
			bluePrompt->SetAnchorTop(vertPos);
			bluePrompt->SetAnchorLeft(margin);
			bluePrompt->SetSize(SD::Vector2(0.45f, lineHeight));
			bluePrompt->SetWrapText(false);
			bluePrompt->SetClampText(true);
			bluePrompt->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			bluePrompt->SetText(translator->TranslateText(TXT("BlueKeybind"), TXT("SandDuneTester"), TXT("InputTester")));
			bluePrompt->SetAutoRefresh(true);
		}

		BlueBind = SD::TextFieldComponent::CreateObject();
		if (menuFrame->AddComponent(BlueBind))
		{
			BlueBind->SetAutoRefresh(false);
			BlueBind->SetAnchorTop(vertPos);
			BlueBind->SetAnchorRight(margin);
			BlueBind->SetSize(SD::Vector2(0.45f, lineHeight));
			BlueBind->SetWrapText(false);
			BlueBind->SetClampText(true);
			BlueBind->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			BlueBind->SetText(SD::InputUtils::TranslateToHumanReadableText(sf::Keyboard::B));
			BlueBind->OnEdit = SDFUNCTION_1PARAM(this, InputTesterUi, HandleBlueEdit, bool, const sf::Event&);
			BlueBind->SetAutoRefresh(true);
		}

		vertPos += lineHeight + margin;

		SD::LabelComponent* modifierText = SD::LabelComponent::CreateObject();
		if (menuFrame->AddComponent(modifierText))
		{
			modifierText->SetAutoRefresh(false);
			modifierText->SetAnchorLeft(0.05f);
			modifierText->SetAnchorRight(0.05f);
			modifierText->SetPosition(SD::Vector2(0.f, vertPos));
			modifierText->SetSize(SD::Vector2(1.f, lineHeight));
			modifierText->SetWrapText(true);
			modifierText->SetClampText(true);
			modifierText->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			modifierText->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			modifierText->SetText(translator->TranslateText(TXT("KeyboardModifierText"), TXT("SandDuneTester"), TXT("InputTester")));
			modifierText->SetAutoRefresh(true);
		}

		ExitButton = SD::ButtonComponent::CreateObject();
		if (menuFrame->AddComponent(ExitButton))
		{
			ExitButton->SetAnchorLeft(0.25f);
			ExitButton->SetAnchorRight(0.25f);
			ExitButton->SetAnchorBottom(margin);
			ExitButton->SetSize(SD::Vector2(0.5f, lineHeight));
			ExitButton->SetCaptionText(translator->TranslateText(TXT("ExitButton"), TXT("SandDuneTester"), TXT("InputTester")));
			ExitButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, InputTesterUi, HandleExitButtonReleased, void, SD::ButtonComponent*));
		}
	}

	SD::PlanarTransformComponent* circleTransform = SD::PlanarTransformComponent::CreateObject();
	if (AddComponent(circleTransform))
	{
		circleTransform->SetPosition(SD::Vector2(0.5f, 0.f));
		circleTransform->SetSize(SD::Vector2(0.5f, 1.f));

		Circle = SD::ColorRenderComponent::CreateObject();
		if (circleTransform->AddComponent(Circle))
		{
			Circle->SetShape(SD::ColorRenderComponent::ST_Circle);
			Circle->SolidColor = SD::Color::RED;
		}
	}
}

void InputTesterUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())
	Focus->TabOrder.push_back(ExitButton);
}

void InputTesterUi::InitializeInputTestUi (InputTester* newTestOwner, SD::InputBroadcaster* broadcaster)
{
	if (TestOwner != nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot reassign InputTesterUi's TestOwner if it's already asigned."));
		return;
	}

	if (newTestOwner == nullptr || broadcaster == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot initialize the InputTestUi with null attributes."));
		return;
	}

	TestOwner = newTestOwner;
	CHECK(Input != nullptr)
	broadcaster->AddInputComponent(Input, 100);
}

bool InputTesterUi::IsConfiguringKeybind () const
{
	return ((RedBind != nullptr && RedBind->GetCursorPosition() >= 0) ||
		(BlueBind != nullptr && BlueBind->GetCursorPosition() >= 0) ||
		(GreenBind != nullptr && GreenBind->GetCursorPosition() >= 0));
}

void InputTesterUi::UpdateTextField (SD::TextFieldComponent* textField, sf::Keyboard::Key newBind)
{
	CHECK(textField != nullptr)
	textField->SetText(SD::InputUtils::TranslateToHumanReadableText(newBind));
	textField->SetCursorPosition(-1);
}

bool InputTesterUi::HandleRedEdit (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::KeyReleased)
	{
		UpdateTextField(RedBind, evnt.key.code);
		Input->AddKeybind(evnt.key.code, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddRedBind, bool));
		Input->AddKeybind(evnt.key.code, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveRedBind, bool));
		Input->AddKeybind(evnt.key.code, false, SDFUNCTION(this, InputTesterUi, HandleRedBind, bool));
	}

	return (evnt.type == sf::Event::KeyPressed || evnt.type == sf::Event::KeyReleased);
}

bool InputTesterUi::HandleGreenEdit (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::KeyReleased)
	{
		UpdateTextField(GreenBind, evnt.key.code);
		Input->AddKeybind(evnt.key.code, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddGreenBind, bool));
		Input->AddKeybind(evnt.key.code, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveGreenBind, bool));
		Input->AddKeybind(evnt.key.code, false, SDFUNCTION(this, InputTesterUi, HandleGreenBind, bool));
	}

	return (evnt.type == sf::Event::KeyPressed || evnt.type == sf::Event::KeyReleased);
}

bool InputTesterUi::HandleBlueEdit (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::KeyReleased)
	{
		UpdateTextField(BlueBind, evnt.key.code);
		Input->AddKeybind(evnt.key.code, false, SD::KS_Ctrl, SDFUNCTION(this, InputTesterUi, HandleAddBlueBind, bool));
		Input->AddKeybind(evnt.key.code, false, SD::KS_Alt, SDFUNCTION(this, InputTesterUi, HandleRemoveBlueBind, bool));
		Input->AddKeybind(evnt.key.code, false, SDFUNCTION(this, InputTesterUi, HandleBlueBind, bool));
	}

	return (evnt.type == sf::Event::KeyPressed || evnt.type == sf::Event::KeyReleased);
}

bool InputTesterUi::HandleRedBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor = SD::Color::RED;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleGreenBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor = SD::Color::GREEN;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleBlueBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor = SD::Color::BLUE;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleAddRedBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.r = 255;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleAddGreenBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.g = 255;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleAddBlueBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.b = 255;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleRemoveRedBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.r = 0;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleRemoveGreenBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.g = 0;
		return true;
	}

	return false;
}

bool InputTesterUi::HandleRemoveBlueBind ()
{
	if (Circle != nullptr && !IsConfiguringKeybind())
	{
		Circle->SolidColor.Source.b = 0;
		return true;
	}

	return false;
}

void InputTesterUi::HandleExitButtonReleased (SD::ButtonComponent* button)
{
	if (TestOwner != nullptr)
	{
		TestOwner->Destroy();
	}
}
SD_TESTER_END
#endif