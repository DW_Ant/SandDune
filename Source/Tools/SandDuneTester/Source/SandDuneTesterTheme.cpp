/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDuneTesterTheme.cpp
=====================================================================
*/

#include "SandDuneTesterTheme.h"

IMPLEMENT_CLASS(SDTester::SandDuneTesterTheme, SD::GuiTheme)
SD_TESTER_BEGIN

const SD::DString SandDuneTesterTheme::SAND_DUNE_TESTER_STYLE_NAME = TXT("SandDuneTesterTheme");

void SandDuneTesterTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo testerTheme = CreateStyleFrom(*defaultStyle);
		testerTheme.Name = SAND_DUNE_TESTER_STYLE_NAME;
		Styles.push_back(testerTheme);

		SD::ButtonComponent* buttonTemplate = dynamic_cast<SD::ButtonComponent*>(FindTemplate(&testerTheme, SD::ButtonComponent::SStaticClass()));
		CHECK(buttonTemplate != nullptr)

		SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(buttonTemplate->ReplaceStateComponent(SD::ColorButtonState::SStaticClass()));
		CHECK(colorState != nullptr)
		colorState->SetDefaultColor(SD::Color(128, 128, 128, 200));
		colorState->SetHoverColor(SD::Color(145, 145, 145, 200));
		colorState->SetDownColor(SD::Color(96, 96, 96, 200));
		colorState->SetDisabledColor(SD::Color(32, 32, 32, 200));
	}
}

SD_TESTER_END