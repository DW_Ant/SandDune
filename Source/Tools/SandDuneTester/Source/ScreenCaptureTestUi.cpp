/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  ScreenCaptureTestUi.cpp
=====================================================================
*/

#include "ScreenCaptureTestUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::ScreenCaptureTestUi, SD::GuiEntity)
SD_TESTER_BEGIN
void ScreenCaptureTestUi::InitProps ()
{
	Super::InitProps();

	SceneScrollbar = nullptr;
	NumCompsLabel = nullptr;
	GeneratedTextureFrame = nullptr;
	GeneratedTextureLabel = nullptr;
	HoveredPixelLabel = nullptr;
	ConfigurationScrollbar = nullptr;
	PixelTypeLabel = nullptr;
	PixelTypeDropdown = nullptr;
	AlphaThresholdLabel = nullptr;
	AlphaThresholdField = nullptr;
	IncludeColorComps = nullptr;
	IncludeSpriteComps = nullptr;
	RecursiveCaptureDescription = nullptr;
	RecursiveCaptureComps = nullptr;
}

void ScreenCaptureTestUi::ConstructUI ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr);
	SD::DString fileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("ScreenCaptureTestUi");

	SD::Float col2 = 0.5f;

	SceneScrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(SceneScrollbar))
	{
		SceneScrollbar->SetPosition(SD::Vector2::ZERO_VECTOR);
		SceneScrollbar->SetSize(SD::Vector2(col2, 1.f));

		SD::GuiEntity* viewedObj = SD::GuiEntity::CreateObject();
		viewedObj->SetGuiSizeToOwningScrollbar(SD::Vector2(-1.f, -1.f));
		viewedObj->SetSize(SD::Vector2(1024.f, 1024.f));
		SceneScrollbar->SetViewedObject(viewedObj);

		CreateCompScene(viewedObj);
	}

	SD::Float posY = 0.f;

	GeneratedTextureFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(GeneratedTextureFrame))
	{
		GeneratedTextureFrame->SetPosition(SD::Vector2(col2, 0.f));
		GeneratedTextureFrame->SetSize(SD::Vector2(1.f - col2, 0.35f));
		GeneratedTextureFrame->SetLockedFrame(true);
		GeneratedTextureFrame->SetBorderThickness(2.f);
		posY += GeneratedTextureFrame->ReadSize().Y;

		GeneratedTextureLabel = SD::LabelComponent::CreateObject();
		if (GeneratedTextureFrame->AddComponent(GeneratedTextureLabel))
		{
			GeneratedTextureLabel->SetAutoRefresh(false);
			GeneratedTextureLabel->SetAnchorTop(0.1f);
			GeneratedTextureLabel->SetAnchorRight(0.1f);
			GeneratedTextureLabel->SetAnchorBottom(0.1f);
			GeneratedTextureLabel->SetAnchorLeft(0.1f);
			GeneratedTextureLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			GeneratedTextureLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			GeneratedTextureLabel->SetWrapText(true);
			GeneratedTextureLabel->SetText(translator->TranslateText(TXT("GeneratedTextureLabel"), fileName, sectionName));
			GeneratedTextureLabel->SetAutoRefresh(true);
		}
	}

	HoveredPixelLabel = SD::LabelComponent::CreateObject();
	if (AddComponent(HoveredPixelLabel))
	{
		HoveredPixelLabel->SetAutoRefresh(false);
		HoveredPixelLabel->SetPosition(SD::Vector2(col2, posY));
		HoveredPixelLabel->SetSize(SD::Vector2(1.f - col2, 0.1f));
		posY += HoveredPixelLabel->ReadSize().Y;
		HoveredPixelLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
		HoveredPixelLabel->SetWrapText(true);
		HoveredPixelLabel->SetVisibility(false);
		HoveredPixelLabel->SetAutoRefresh(true);
	}

	ConfigurationScrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(ConfigurationScrollbar))
	{
		ConfigurationScrollbar->SetPosition(SD::Vector2(col2, posY));
		ConfigurationScrollbar->SetSize(SD::Vector2(1.f - col2, 1.f - posY));
		ConfigurationScrollbar->SetHideControlsWhenFull(true);

		SD::GuiEntity* configEntity = SD::GuiEntity::CreateObject();
		configEntity->SetAutoSizeVertical(true);
		configEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
		ConfigurationScrollbar->SetViewedObject(configEntity);

		SD::VerticalList* vertList = SD::VerticalList::CreateObject();
		if (configEntity->AddComponent(vertList))
		{
			vertList->SetPosition(SD::Vector2::ZERO_VECTOR);
			vertList->SetSize(SD::Vector2(1.f, 8.f));
			vertList->ComponentSpacing = 8.f;
			SD::Float lineHeight = 24.f;
			
			PixelTypeLabel = SD::LabelComponent::CreateObject();
			if (vertList->AddComponent(PixelTypeLabel))
			{
				PixelTypeLabel->SetAutoRefresh(false);
				PixelTypeLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
				PixelTypeLabel->SetSize(SD::Vector2(1.f, lineHeight));
				PixelTypeLabel->SetWrapText(false);
				PixelTypeLabel->SetText(translator->TranslateText(TXT("PixelType"), fileName, sectionName));
				PixelTypeLabel->SetAutoRefresh(true);
			}

			PixelTypeDropdown = SD::DropdownComponent::CreateObject();
			if (vertList->AddComponent(PixelTypeDropdown))
			{
				PixelTypeDropdown->SetPosition(SD::Vector2::ZERO_VECTOR);
				PixelTypeDropdown->SetSize(SD::Vector2(1.f, lineHeight * 6.f));
				PixelTypeDropdown->SetItemSelectionHeight(lineHeight);
				if (SD::ListBoxComponent* listBox = PixelTypeDropdown->GetExpandMenuList())
				{
					std::vector<SD::BaseGuiDataElement*> items;

					items.push_back(new SD::GuiDataElement<SD::ScreenCapture::EPixelMapType>(SD::ScreenCapture::PMT_PerPixel, TXT("Per Pixel - Alpha detection")));
					items.push_back(new SD::GuiDataElement<SD::ScreenCapture::EPixelMapType>(SD::ScreenCapture::PMT_AABB, TXT("AABB - Hover based on bounding box")));
					items.push_back(new SD::GuiDataElement<SD::ScreenCapture::EPixelMapType>(SD::ScreenCapture::PMT_None, TXT("None - Nothing can be hovered")));
					listBox->SetList(items);
				}
				
				PixelTypeDropdown->SetSelectedItem(SD::ScreenCapture::PMT_PerPixel);
			}

			AlphaThresholdLabel = SD::LabelComponent::CreateObject();
			if (vertList->AddComponent(AlphaThresholdLabel))
			{
				AlphaThresholdLabel->SetAutoRefresh(false);
				AlphaThresholdLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
				AlphaThresholdLabel->SetSize(SD::Vector2(1.f, lineHeight));
				AlphaThresholdLabel->SetWrapText(false);
				AlphaThresholdLabel->SetText(translator->TranslateText(TXT("AlphaThreshold"), fileName, sectionName));
				AlphaThresholdLabel->SetAutoRefresh(true);
			}

			AlphaThresholdField = SD::TextFieldComponent::CreateObject();
			if (vertList->AddComponent(AlphaThresholdField))
			{
				AlphaThresholdField->SetAutoRefresh(false);
				AlphaThresholdField->SetPosition(SD::Vector2::ZERO_VECTOR);
				AlphaThresholdField->SetSize(SD::Vector2(1.f, lineHeight));
				AlphaThresholdField->SetSingleLine(true);
				AlphaThresholdField->SetText(TXT("0.25"));
				AlphaThresholdField->OnAllowTextInput = SDFUNCTION_1PARAM(this, ScreenCaptureTestUi, HandleAlphaThresholdAllowText, bool, const SD::DString&);
				AlphaThresholdField->OnReturn = SDFUNCTION_1PARAM(this, ScreenCaptureTestUi, HandleAlphaThresholdReturn, void, SD::TextFieldComponent*);
				AlphaThresholdField->SetAutoRefresh(true);
			}

			IncludeColorComps = SD::CheckboxComponent::CreateObject();
			if (vertList->AddComponent(IncludeColorComps))
			{
				IncludeColorComps->SetChecked(true);
				IncludeColorComps->SetPosition(SD::Vector2::ZERO_VECTOR);
				IncludeColorComps->EditSize().Y = lineHeight;
				if (SD::LabelComponent* caption = IncludeColorComps->GetCaptionComponent())
				{
					caption->SetText(translator->TranslateText(TXT("IncludeColorComps"), fileName, sectionName));
				}
			}

			IncludeSpriteComps = SD::CheckboxComponent::CreateObject();
			if (vertList->AddComponent(IncludeSpriteComps))
			{
				IncludeSpriteComps->SetChecked(true);
				IncludeSpriteComps->SetPosition(SD::Vector2::ZERO_VECTOR);
				IncludeSpriteComps->EditSize().Y = lineHeight;
				if (SD::LabelComponent* caption = IncludeSpriteComps->GetCaptionComponent())
				{
					caption->SetText(translator->TranslateText(TXT("IncludeSpriteComps"), fileName, sectionName));
				}
			}

			RecursiveCaptureDescription = SD::LabelComponent::CreateObject();
			if (vertList->AddComponent(RecursiveCaptureDescription))
			{
				RecursiveCaptureDescription->SetAutoRefresh(false);
				RecursiveCaptureDescription->SetPosition(SD::Vector2::ZERO_VECTOR);
				RecursiveCaptureDescription->SetSize(SD::Vector2(1.f, lineHeight));
				RecursiveCaptureDescription->SetWrapText(true);
				RecursiveCaptureDescription->SetAutoSizeVertical(true);
				RecursiveCaptureDescription->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				RecursiveCaptureDescription->SetText(translator->TranslateText(TXT("RecursiveCaptureDescription"), fileName, sectionName));
				RecursiveCaptureDescription->SetAutoRefresh(true);
			}

			RecursiveCaptureComps = SD::CheckboxComponent::CreateObject();
			if (vertList->AddComponent(RecursiveCaptureComps))
			{
				RecursiveCaptureComps->SetChecked(true);
				RecursiveCaptureComps->SetPosition(SD::Vector2::ZERO_VECTOR);
				RecursiveCaptureComps->SetSize(SD::Vector2(1.f, lineHeight));
				if (SD::LabelComponent* caption = RecursiveCaptureComps->GetCaptionComponent())
				{
					caption->SetText(translator->TranslateText(TXT("RecursiveCaptureComps"), fileName, sectionName));
				}
			}

			SD::ButtonComponent* button = SD::ButtonComponent::CreateObject();
			if (vertList->AddComponent(button))
			{
				button->SetPosition(SD::Vector2::ZERO_VECTOR);
				button->SetSize(SD::Vector2(1.f, lineHeight));
				button->SetCaptionText(translator->TranslateText(TXT("GenerateTextureButton"), fileName, sectionName));
				button->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScreenCaptureTestUi, HandleRunCaptureButtonReleased, void, SD::ButtonComponent*));
			}

			NumCompsLabel = SD::LabelComponent::CreateObject();
			if (vertList->AddComponent(NumCompsLabel))
			{
				NumCompsLabel->SetAutoRefresh(false);
				NumCompsLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
				NumCompsLabel->SetSize(SD::Vector2(1.f, lineHeight));
				NumCompsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				NumCompsLabel->SetWrapText(true);
				NumCompsLabel->SetClampText(false);
				NumCompsLabel->SetVisibility(false);
				NumCompsLabel->SetAutoRefresh(true);
			}
		}
	}
}

void ScreenCaptureTestUi::Destroyed ()
{
	if (Capture != nullptr)
	{
		Capture->Destroy();
		Capture = nullptr;
	}

	bOverridingMouseIcon = false;

	Super::Destroyed();
}

void ScreenCaptureTestUi::HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove)
{
	Super::HandleMouseMove(mouse, sfmlEvent, deltaMove);

	if (HoveredPixelLabel == nullptr)
	{
		return;
	}

	if (GeneratedTextureFrame != nullptr && Capture != nullptr && Capture->GetCapturedTexture() != nullptr && GeneratedTextureFrame->IsWithinBounds(mouse->ReadPosition()))
	{
		SD::Vector2 deltaPos = mouse->ReadPosition() - GeneratedTextureFrame->ReadCachedAbsPosition();

		SD::Vector2 sizeRatio = Capture->GetCapturedTexture()->GetSize();
		sizeRatio /= GeneratedTextureFrame->ReadCachedAbsSize();
		deltaPos *= sizeRatio;
		SD::RenderComponent* hoveredRendComp = Capture->GetMappedPixel(deltaPos.X.ToInt(), deltaPos.Y.ToInt());

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		if (hoveredRendComp != nullptr)
		{
			HoveredPixelLabel->SetText(SD::DString::CreateFormattedString(translator->TranslateText(TXT("HoveredComp"), TXT("SandDuneTester"), TXT("ScreenCaptureTestUi")), hoveredRendComp->DebugName));
		}
		else
		{
			HoveredPixelLabel->SetText(translator->TranslateText(TXT("NothingHovered"), TXT("SandDuneTester"), TXT("ScreenCaptureTestUi")));
		}

		if (!bOverridingMouseIcon)
		{
			SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
			CHECK(localTexturePool != nullptr);
			const SD::Texture* newIcon = localTexturePool->GetTexture(SD::HashedString("Engine.Input.CursorPrecision"));
			if (newIcon != nullptr)
			{
				mouse->PushMouseIconOverride(newIcon, SDFUNCTION_2PARAM(this, ScreenCaptureTestUi, HandleMouseIconOverride, bool, SD::MousePointer*, const sf::Event::MouseMoveEvent&));
				bOverridingMouseIcon = true;
			}
		}
	}
	else
	{
		HoveredPixelLabel->SetText(SD::DString::EmptyString);
		bOverridingMouseIcon = false;
	}
}

void ScreenCaptureTestUi::CreateCompScene (SD::GuiEntity* sceneOwner)
{
	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	{
		SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(transform))
		{
			transform->SetPosition(SD::Vector2(8.f, 8.f));
			transform->SetSize(SD::Vector2(64.f, 48.f));

			SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
			if (transform->AddComponent(sprite))
			{
				sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.GreenGrass")));
				sprite->DebugName = TXT("Grass Sprite");
			}
		}
	}

	{
		SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(transform))
		{
			transform->SetPosition(SD::Vector2(96.f, 8.f));
			transform->SetSize(SD::Vector2(64.f, 48.f));

			SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = SD::Color(150, 0, 0);
				color->DebugName = TXT("Solid Red");
			}
		}
	}

	{
		SD::PlanarTransformComponent* rootTransform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(rootTransform))
		{
			rootTransform->SetPosition(SD::Vector2(196.f, 8.f));
			rootTransform->SetSize(SD::Vector2(96.f, 128.f));

			SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
			if (rootTransform->AddComponent(sprite))
			{
				sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.DesertWet")));
				sprite->DebugName = TXT("Desert Sprite");
			}

			SD::PlanarTransformComponent* relativeTransform = SD::PlanarTransformComponent::CreateObject();
			if (rootTransform->AddComponent(relativeTransform))
			{
				relativeTransform->SetPosition(SD::Vector2(0.33f, 0.33f));
				relativeTransform->SetSize(SD::Vector2(0.25f, 0.25f));

				SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
				if (relativeTransform->AddComponent(color))
				{
					color->SolidColor = SD::Color(0, 128, 0);
					color->DebugName = TXT("Solid Green");
				}
			}
		}
	}

	//Test against nested screen captures
	{
		SD::ScrollbarComponent* scrollbar = SD::ScrollbarComponent::CreateObject();
		if (sceneOwner->AddComponent(scrollbar))
		{
			scrollbar->SetPosition(SD::Vector2(8.f, 135.f));
			scrollbar->SetSize(SD::Vector2(128.f, 165.f));

			for (SD::ComponentIterator iter(scrollbar, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				if (SD::RenderComponent* renderComp = dynamic_cast<SD::RenderComponent*>(iter.GetSelectedComponent()))
				{
					renderComp->DebugName = TXT("Scrollbar Render Component");
				}
			}

			SD::GuiEntity* scrollbarObj = SD::GuiEntity::CreateObject();
			scrollbarObj->SetGuiSizeToOwningScrollbar(SD::Vector2(-1.f, -1.f));
			scrollbarObj->SetSize(SD::Vector2(128.f, 256.f));
			scrollbar->SetViewedObject(scrollbarObj);

			{
				SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
				if (scrollbarObj->AddComponent(transform))
				{
					transform->SetPosition(SD::Vector2(8.f, 8.f));
					transform->SetSize(SD::Vector2(64.f, 64.f));

					SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
					if (transform->AddComponent(sprite))
					{
						sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.GreenGrass")));
						sprite->DebugName = TXT("Grass Inside Scrollbar");
					}
				}
			}

			{
				SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
				if (scrollbarObj->AddComponent(transform))
				{
					transform->SetPosition(SD::Vector2(32.f, 96.f));
					transform->SetSize(SD::Vector2(64.f, 64.f));

					SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
					if (transform->AddComponent(color))
					{
						color->SolidColor = SD::Color(0, 64, 255);
						color->DebugName = TXT("Blue Inside Scrollbar");
					}

					SD::PlanarTransformComponent* fernTransform = SD::PlanarTransformComponent::CreateObject();
					if (transform->AddComponent(fernTransform))
					{
						fernTransform->SetPosition(SD::Vector2(0.25f, 0.25f));
						fernTransform->SetSize(SD::Vector2(0.5f, 0.5f));

						SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
						if (fernTransform->AddComponent(sprite))
						{
							sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.TopDownFerns")));
							sprite->DebugName = TXT("Ferns Inside Scrollbar");
						}
					}
				}
			}
		}
	}

	{
		SD::PlanarTransformComponent* rootTransform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(rootTransform))
		{
			rootTransform->SetPosition(SD::Vector2(8.f, 384.f));
			rootTransform->SetSize(SD::Vector2(256.f, 256.f));

			SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
			if (rootTransform->AddComponent(color))
			{
				color->SolidColor = SD::Color(0, 128, 125);
				color->DebugName = TXT("Solid Cyan");
			}

			SD::PlanarTransformComponent* childTransform = SD::PlanarTransformComponent::CreateObject();
			if (rootTransform->AddComponent(childTransform))
			{
				childTransform->SetPosition(SD::Vector2(0.33f, 0.33f));
				childTransform->SetSize(SD::Vector2(0.25f, 0.25f));

				SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
				if (childTransform->AddComponent(sprite))
				{
					sprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.TopDownFerns")));
					sprite->DebugName = TXT("Fern Sprite");
				}
			}
		}
	}

	{
		SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(transform))
		{
			transform->SetPosition(SD::Vector2(300.f, 384.f));
			transform->SetSize(SD::Vector2(64.f, 64.f));

			SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
			if (transform->AddComponent(color))
			{
				color->SolidColor = SD::Color(255, 255, 255, 64);
				color->DebugName = TXT("Transparent Color");
			}
		}
	}

	{
		std::vector<SD::Color> colors
		{
			SD::Color(255, 0, 0),
			SD::Color(200, 50, 0, 225),
			SD::Color(150, 100, 0, 200),
			SD::Color(64, 200, 0, 175),
			SD::Color(0, 255, 0, 150),
			SD::Color(0, 128, 128, 125),
			SD::Color(128, 128, 128, 100),
			SD::Color(255, 128, 128, 75),
			SD::Color(0, 255, 128, 50),
			SD::Color(0, 128, 255, 25)
		};

		SD::PlanarTransformComponent* rootTransform = SD::PlanarTransformComponent::CreateObject();
		if (sceneOwner->AddComponent(rootTransform))
		{
			rootTransform->SetPosition(SD::Vector2(450.f, 384.f));
			rootTransform->SetSize(SD::Vector2(512.f, 512.f));
		}

		SD::Entity* compOwner = rootTransform;
		SD::Vector2 compSize(600.f, 600.f);
		for (size_t i = 0; i < colors.size(); ++i)
		{
			SD::PlanarTransformComponent* transform = SD::PlanarTransformComponent::CreateObject();
			if (compOwner->AddComponent(transform))
			{
				transform->SetSize(compSize);
				transform->SetAnchorRight(0.f);
				transform->SetAnchorBottom(0.f);
				compOwner = transform;
				compSize -= SD::Vector2(48.f, 48.f);

				SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
				if (transform->AddComponent(color))
				{
					color->SetShape(SD::ColorRenderComponent::ST_Circle);
					color->SolidColor = colors.at(i);
					color->DebugName = SD::DString::CreateFormattedString(TXT("Color: %s"), colors.at(i).ToString());
				}
			}
		}
	}
}

void ScreenCaptureTestUi::RunScreenCapture ()
{
	if (Capture == nullptr)
	{
		Capture = SD::ScreenCapture::CreateObject();
		Capture->OnIsRelevant = SDFUNCTION_1PARAM(this, ScreenCaptureTestUi, HandleRenderCompRelevant, bool, SD::RenderComponent*);
	}

	if (PixelTypeDropdown != nullptr && PixelTypeDropdown->IsItemSelected())
	{
		SD::ScreenCapture::EPixelMapType mapType = PixelTypeDropdown->GetSelectedItem<SD::ScreenCapture::EPixelMapType>();
		Capture->SetPixelMapType(mapType);
	}

	if (AlphaThresholdField != nullptr)
	{
		SD::DString alphaThresholdTxt = AlphaThresholdField->GetContent();
		SD::Float alphaThreshold(alphaThresholdTxt);
		Capture->SetAlphaThreshold(alphaThreshold);
	}

	if (RecursiveCaptureComps != nullptr)
	{
		Capture->SetDetectCompsWithinRenderTextures(RecursiveCaptureComps->IsChecked());
	}

	if (SceneScrollbar != nullptr && SceneScrollbar->GetFrameTexture() != nullptr)
	{
		bool success = Capture->GenerateCaptureTexture(SceneScrollbar->GetFrameTexture(), SD::Color::INVISIBLE);
		if (!success)
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Scene Capture Test failed. Unable to generate a capture texture."));
			return;
		}

		if (GeneratedTextureFrame != nullptr)
		{
			GeneratedTextureFrame->SetCenterTexture(Capture->GetCapturedTexture());

			if (GeneratedTextureLabel != nullptr)
			{
				GeneratedTextureLabel->SetVisibility(false);
			}

			if (NumCompsLabel != nullptr)
			{
				SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
				CHECK(translator != nullptr)

				NumCompsLabel->SetText(SD::DString::CreateFormattedString(translator->TranslateText(TXT("NumCompsLabel"), TXT("SandDuneTester"), TXT("ScreenCaptureTestUi")), Capture->GetNumCapturedComponents()));
				NumCompsLabel->SetVisibility(true);
			}

			if (HoveredPixelLabel != nullptr)
			{
				HoveredPixelLabel->SetVisibility(true);
				HoveredPixelLabel->SetText(SD::DString::EmptyString);
			}
		}
	}
}

bool ScreenCaptureTestUi::HandleMouseIconOverride (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& moveEvent)
{
	return bOverridingMouseIcon;
}

bool ScreenCaptureTestUi::HandleAlphaThresholdAllowText (const SD::DString& txt)
{
	if (txt.Compare(TXT("."), SD::DString::CC_CaseSensitive) == 0 && AlphaThresholdField != nullptr)
	{
		SD::DString fieldText = AlphaThresholdField->GetContent();
		return (fieldText.Find('.', 0) == INT_INDEX_NONE);
	}

	return txt.HasRegexMatch(TXT("[0-9]"));
}

void ScreenCaptureTestUi::HandleAlphaThresholdReturn (SD::TextFieldComponent* uiComponent)
{
	if (AlphaThresholdField != nullptr)
	{
		SD::Float clampedValue(AlphaThresholdField->GetContent());
		clampedValue = SD::Utils::Clamp<SD::Float>(clampedValue, 0.f, 1.f);
		AlphaThresholdField->SetText(clampedValue.ToString());
	}
}

void ScreenCaptureTestUi::HandleRunCaptureButtonReleased (SD::ButtonComponent* button)
{
	RunScreenCapture();
}

bool ScreenCaptureTestUi::HandleRenderCompRelevant (SD::RenderComponent* renderComp)
{
	if (SD::ColorRenderComponent* colorComp = dynamic_cast<SD::ColorRenderComponent*>(renderComp))
	{
		if (IncludeColorComps != nullptr && !IncludeColorComps->IsChecked())
		{
			return false;
		}
	}
	else if (SD::SpriteComponent* spriteComp = dynamic_cast<SD::SpriteComponent*>(renderComp))
	{
		if (IncludeSpriteComps && !IncludeSpriteComps->IsChecked())
		{
			return false;
		}
	}

	return true;
}
SD_TESTER_END
#endif