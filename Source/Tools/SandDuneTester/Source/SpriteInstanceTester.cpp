/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteInstanceTester.cpp
=====================================================================
*/

#include "SandDuneTester.h"
#include "SpriteInstanceTester.h"
#include "SpriteInstanceTesterUi.h"
#include "QaUnitTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::SpriteInstanceTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Int SpriteInstanceTester::DENSITY_MAP_WIDTH(256);
const SD::Int SpriteInstanceTester::DENSITY_MAP_HEIGHT(256);

void SpriteInstanceTester::InitProps ()
{
	Super::InitProps();

	Ui = nullptr;
	SpriteInstance = nullptr;
	BrushTransform = nullptr;
	BrushSprite = nullptr;

	IsLPainting = false;
	IsRPainting = false;
	PaintProperties.PaintPerSecond = 255.f;
	PaintProperties.UpdateRate = 0.1f;
	PaintProperties.AlphaWhenPainting = 32;
	IsOverwritingMouseIcon = false;
	MouseIconTexture = nullptr;

	QaTester = nullptr;
	TestFlags = SD::UnitTester::UTF_None;
}

void SpriteInstanceTester::Destroyed ()
{
	if (Ui.IsValid())
	{
		Ui->Destroy();
	}

	if (DensityMap.IsValid())
	{
		DensityMap->Destroy();
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void SpriteInstanceTester::InitializeTest (const QaUnitTester* launcher, SD::UnitTester::EUnitTestFlags testFlags)
{
	CHECK(!IsInitialized())
	InitializeEntityManager(SD::Vector2(768.f, 768.f), TXT("Sprite Instance Test"), -1.f);
	QaTester = launcher;
	TestFlags = testFlags;

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (AddComponent(tick))
	{
		tick->SetTickInterval(PaintProperties.UpdateRate);
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, SpriteInstanceTester, HandleTick, void, SD::Float));
	}

	MainDrawLayer->RegisterPlanarObject(this);

	//Create a blank texture. We're setting each individual texel.
	sf::Texture* densityMapResource = new sf::Texture();
	densityMapResource->create(DENSITY_MAP_WIDTH.ToUnsignedInt32(), DENSITY_MAP_HEIGHT.ToUnsignedInt32());
	densityMapResource->setSmooth(false);

	DensityMap = SD::Texture::CreateObject();
	DensityMap->SetResource(densityMapResource);
	DensityMap->SetTextureName(TXT("SpriteInstanceTester_GeneratedDensityMap"));

	std::vector<sf::Uint8> texelData;
	texelData.resize(SD::Int(DENSITY_MAP_WIDTH * DENSITY_MAP_HEIGHT * 4).ToUnsignedInt());

	//Make each texel solid white
	for (size_t i = 0; i < texelData.size(); i += 4)
	{
		texelData.at(i) = 255;
		texelData.at(i+1) = 255;
		texelData.at(i+2) = 255;

		//Set alpha channel to about 50% translucent so it's not burning bright white at max density areas.
		texelData.at(i+3) = 128;
	}
	DensityMap->SetTexelData(texelData);
	if (!DensityMap->GetImage(OUT DensityMapImage))
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to retrieve Density Map's image data. The SpriteInstanceTester will initialize the density map incorrectly whenever the user paints it for the first time."));
		CHECK_INFO(false, "Failed to get Density Map's image data.")
	}

	DensityMapTransform = SD::PlanarTransformComponent::CreateObject();
	if (AddComponent(DensityMapTransform))
	{
		DensityMapTransform->SetSize(SD::Vector2(DENSITY_MAP_WIDTH.ToFloat() * 3.f, DENSITY_MAP_WIDTH.ToFloat() * 3.f));

		DensityMapSprite = SD::SpriteComponent::CreateObject();
		if (DensityMapTransform->AddComponent(DensityMapSprite))
		{
			DensityMapSprite->SetSpriteTexture(DensityMap.Get());
		}
	}

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	MouseIconTexture = localTexturePool->EditTexture(SD::HashedString("Engine.Input.CursorPrecision"));

	SpriteInstance = SD::InstancedSpriteComponent::CreateObject();
	CHECK(SpriteInstance.IsValid())
	if (AddComponent(SpriteInstance))
	{
		SpriteInstance->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.64-Grid")));
	}

	BrushTransform = SD::PlanarTransformComponent::CreateObject();
	if (AddComponent(BrushTransform))
	{
		BrushTransform->SetEnableFractionScaling(false);
		BrushTransform->SetSize(SD::Vector2(96.f, 96.f));

		BrushSprite = SD::SpriteComponent::CreateObject();
		if (BrushTransform->AddComponent(BrushSprite))
		{
			BrushSprite->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.DottedCircle")));
		}
	}

	BrushInput = SD::InputComponent::CreateObject();
	if (AddComponent(BrushInput))
	{
		Input->AddInputComponent(BrushInput.Get(), 50);
		BrushInput->OnMouseMove = SDFUNCTION_3PARAM(this, SpriteInstanceTester, HandleMouseMove, void, SD::MousePointer*, const sf::Event::MouseMoveEvent&, const SD::Vector2&);
		BrushInput->OnMouseClick = SDFUNCTION_3PARAM(this, SpriteInstanceTester, HandleMouseClick, bool, SD::MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	Ui = SpriteInstanceTesterUi::CreateObject();
	CHECK(Ui.IsValid())
	GuiDrawLayer->RegisterMenu(Ui.Get());
	Ui->SetupInputComponent(Input.Get(), 100);
	Ui->SetOwningTester(this);
}

void SpriteInstanceTester::ManipulateDensityTexture (SD::Float deltaSec)
{
	CHECK(DensityMap.IsValid() && Mouse != nullptr && BrushTransform.IsValid())

	SD::Float multiplier = (IsLPainting) ? -1.f : 1.f; //If left clicking, make it darker. If right clicking, make it brighter.

	//Note: We use screen projection data instead of transformation properties since we're doing a reverse projection from mouse to rendered object.
	const SD::Transformation::SScreenProjectionData& screenProjection = DensityMapTransform->ReadProjectionData();
	SD::Vector2 finalProjectedSize = SD::Vector2::SFMLtoSD(screenProjection.GetFinalSize());

	//Compute the texel the mouse is over - out of bound texels are okay due to brush size.
	SD::Vector2 pixelsPerTexel(finalProjectedSize.X / DENSITY_MAP_WIDTH.ToFloat(), finalProjectedSize.Y / DENSITY_MAP_HEIGHT.ToFloat());
	SD::Float centerTexelX = (Mouse->ReadPosition().X - SD::Float(screenProjection.Position.x)) / pixelsPerTexel.X;
	SD::Float centerTexelY = (Mouse->ReadPosition().Y - SD::Float(screenProjection.Position.y)) / pixelsPerTexel.Y;

	//Figure out the min/max bounding extents to identify which texels are affected based on brush size.
	SD::Vector2 numTexelRadius((BrushTransform->ReadCachedAbsSize() * 0.5f) / pixelsPerTexel);
	SD::Int top = (centerTexelY - numTexelRadius.Y).ToInt();
	SD::Int right = (centerTexelX + numTexelRadius.X).ToInt();
	SD::Int bottom = (centerTexelY + numTexelRadius.Y).ToInt();
	SD::Int left = (centerTexelX - numTexelRadius.X).ToInt();

	//Iterate through each texel withing texelRange
	for (SD::Int rowIdx = top; rowIdx <= bottom && rowIdx < DENSITY_MAP_HEIGHT; rowIdx++)
	{
		//Started off out of range. Ignore but continue through the loop to check other rows within the bounds.
		if (rowIdx < 0)
		{
			continue;
		}

		for (SD::Int colIdx = left; colIdx <= right && colIdx < DENSITY_MAP_WIDTH; colIdx++)
		{
			if (colIdx < 0)
			{
				continue;
			}

			//Find the texel position in planar space
			SD::Vector2 texelPosition(colIdx.ToFloat() * pixelsPerTexel.X, rowIdx.ToFloat() * pixelsPerTexel.Y);
			texelPosition += ReadPosition();

			//Compute how much this texel should be affected for this frame.
			SD::Float alpha = (Mouse->ReadPosition() - texelPosition).VSize();
			alpha = SD::Utils::Lerp<SD::Float>(1 - (alpha / (BrushTransform->ReadCachedAbsSize().X * 0.5f)), 0.f, 1.f);
			alpha *= (deltaSec * PaintProperties.PaintPerSecond);
			if (alpha <= 0.f)
			{
				continue; //out of range of circle (at the corner of the texelRange)
			}

			sf::Color texelColor = DensityMapImage.getPixel(colIdx.ToUnsignedInt32(), rowIdx.ToUnsignedInt32());

			//Change the color
			SD::Int deltaColorChange = static_cast<SD::Int>((alpha * multiplier).ToInt().Value); //Use Ints instead of unsigned ints since it's possible to get number overflow in the clamp functions below.
			texelColor.r = static_cast<sf::Uint8>(SD::Utils::Clamp<SD::Int>(SD::Int(texelColor.r) + deltaColorChange, 0, 255).Value);
			texelColor.g = static_cast<sf::Uint8>(SD::Utils::Clamp<SD::Int>(SD::Int(texelColor.g) + deltaColorChange, 0, 255).Value);
			texelColor.b = static_cast<sf::Uint8>(SD::Utils::Clamp<SD::Int>(SD::Int(texelColor.b) + deltaColorChange, 0, 255).Value);

			DensityMapImage.setPixel(colIdx.ToUnsignedInt32(), rowIdx.ToUnsignedInt32(), texelColor);
		}
	}

	if (!DensityMap->EditTextureResource()->loadFromImage(DensityMapImage))
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to update Density Map's data in the sprite instance tester."));
	}
}

void SpriteInstanceTester::HandleTick (SD::Float deltaSec)
{
	if ((IsLPainting || IsRPainting) && DensityMap.IsValid() && Mouse != nullptr && BrushTransform.IsValid())
	{
		ManipulateDensityTexture(deltaSec);
	}
}

void SpriteInstanceTester::HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt, const SD::Vector2& deltaMove)
{
	SD::Vector2 mousePos(static_cast<float>(evnt.x), static_cast<float>(evnt.y));

	if (BrushTransform.IsValid() && BrushSprite.IsValid() && BrushSprite->IsVisible())
	{
		//Center the brush transform over the mouse cursor
		//Note: MousePosition is in absolute coordinates. Offset it by this Entity's position to convert it from abs to local space.
		BrushTransform->SetPosition((mousePos - ReadPosition()) - (BrushTransform->ReadCachedAbsSize() * 0.5f));
	}

	if (!IsOverwritingMouseIcon && mouse != nullptr && Ui.IsValid() && !Ui->IsHovered(mousePos))
	{
		IsOverwritingMouseIcon = true;
		mouse->PushMouseIconOverride(MouseIconTexture, SDFUNCTION_2PARAM(this, SpriteInstanceTester, HandleMouseIconOverride, bool, SD::MousePointer*, const sf::Event::MouseMoveEvent&));
	}
}

bool SpriteInstanceTester::HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& evnt, sf::Event::EventType evntType)
{
	if (SpriteInstance.IsValid())
	{
		bool oldIsPainting = IsLPainting || IsRPainting;

		sf::VertexArray& vertices = SpriteInstance->EditVertices();
		if (evntType == sf::Event::MouseButtonPressed && Ui.IsValid() && !Ui->IsHovered(SD::Vector2(SD::Float::MakeFloat(evnt.x), SD::Float::MakeFloat(evnt.y))))
		{
			IsLPainting |= (evnt.button == sf::Mouse::Left);
			IsRPainting |= (evnt.button == sf::Mouse::Right);

			if (!oldIsPainting && (IsLPainting || IsRPainting)) //If we weren't painting before but we are now painting
			{
				for (size_t i = 0; i < vertices.getVertexCount(); ++i)
				{
					//Make nearly invisible so the user can see the changes to the density map texture.
					vertices[i].color.a = PaintProperties.AlphaWhenPainting;
				}
			}
		}
		else if (evntType == sf::Event::MouseButtonReleased)
		{
			//Check which flag needs to be removed.
			IsLPainting = (evnt.button == sf::Mouse::Left) ? false : IsLPainting;
			IsRPainting = (evnt.button == sf::Mouse::Right) ? false : IsRPainting;

			if (oldIsPainting && !IsLPainting && !IsRPainting) //If was painting before but no longer painting now
			{
				for (size_t i = 0; i < vertices.getVertexCount(); ++i)
				{
					vertices[i].color.a = 255;
				}

				//Apply new Density Map
				if (Ui.IsValid())
				{
					Ui->RegenerateSpriteInstances();
				}
			}
		}
	}

	return false;
}

bool SpriteInstanceTester::HandleMouseIconOverride (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& evnt)
{
	const SD::Vector2 mousePos(SD::Float::MakeFloat(evnt.x), SD::Float::MakeFloat(evnt.y));

	//Stop overriding icon if it's hovering over the UI or the UI, itself, is destroyed.
	if (!IsOverwritingMouseIcon || Ui.IsNullptr() || Ui->IsHovered(mousePos))
	{
		IsOverwritingMouseIcon = false;
	}

	return IsOverwritingMouseIcon;
}
SD_TESTER_END
#endif