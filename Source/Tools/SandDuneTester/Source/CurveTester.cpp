/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  CurveTester.cpp
=====================================================================
*/

#include "CurveTester.h"
#include "CurveTestPlotter.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::CurveTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 CurveTester::WINDOW_SIZE(1024.f, 576.f);

void CurveTester::InitProps ()
{
	Super::InitProps();

	TestFlags = SD::UnitTester::EUnitTestFlags::UTF_None;
	Plotter = nullptr;
}

void CurveTester::Destroyed ()
{
	if (Plotter != nullptr)
	{
		Plotter->Destroy();
		Plotter = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated.Execute();
	}

	Super::Destroyed();
}

void CurveTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the CurveTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Curve Test"));

	Plotter = CurveTestPlotter::CreateObject();
	Plotter->SetupInputComponent(Input.Get(), 100);
	GuiDrawLayer->RegisterMenu(Plotter);

	SD::UnitTester::TestLog(TestFlags, TXT("The CurveTest unit test has launched. It will terminate when the user closes the window."));
}
SD_TESTER_END
#endif