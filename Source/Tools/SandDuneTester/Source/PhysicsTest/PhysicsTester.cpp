/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsTester.cpp
=====================================================================
*/

#include "PhysicsMap.h"
#include "PhysicsPlayer.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::PhysicsTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 PhysicsTester::WINDOW_SIZE(1024.f, 576.f);

void PhysicsTester::InitProps ()
{
	Super::InitProps();

	SceneLayer = nullptr;
	QTreeVisualizer = nullptr;
	Map = nullptr;
}

void PhysicsTester::Destroyed ()
{
	if (Map != nullptr)
	{
		Map->Destroy();
		Map = nullptr;
	}

	if (Player.IsValid())
	{
		Player->Destroy();
	}

	if (QTreeVisualizer != nullptr)
	{
		QTreeVisualizer->Destroy();
		QTreeVisualizer = nullptr;
	}

	if (SceneCamera.IsValid())
	{
		SceneCamera->Destroy();
	}

	if (SceneLayer.IsValid())
	{
		SceneLayer->Destroy();
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

SD::Color PhysicsTester::CalcChannelColor (SD::Int collisionChannels)
{
	SD::Color results(0, 0, 0, 255);
	sf::Uint8 colorIntensity(200);

	if ((collisionChannels & CC_RedChannel) > 0)
	{
		results.Source.r = colorIntensity;
	}

	if ((collisionChannels & CC_GreenChannel) > 0)
	{
		results.Source.g = colorIntensity;
	}

	if ((collisionChannels & CC_BlueChannel) > 0)
	{
		results.Source.b = colorIntensity;
	}

	return results;
}

void PhysicsTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the PhysicsTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Physics Test"), 0.0166f /*~60 fps*/);

	SceneCamera = SD::TopDownCamera::CreateObject();
	//Elevate it above the player
	SceneCamera->EditTranslation().Z = 50.f;
	SceneLayer = SD::SceneDrawLayer::CreateObject();
	WindowHandle->RegisterDrawLayer(SceneLayer.Get(), SceneCamera.Get());

	SD::PhysicsEngineComponent* physicsEngine = SD::PhysicsEngineComponent::Find();
	CHECK(physicsEngine != nullptr && physicsEngine->GetRootPhysTree() != nullptr)
	QTreeVisualizer = SD::QuadTreeVisualizer::CreateObject();
	QTreeVisualizer->EditTranslation().Z = 100.f;
	QTreeVisualizer->SetupVisualizer(physicsEngine->GetRootPhysTree(), SceneLayer.Get(), SD::Color(32, 32, 200, 64), 12.f);

	Map = PhysicsMap::CreateObject();
	Map->InitializeMapObjects(this);

	Player = PhysicsPlayer::CreateObject();
	SceneCamera->SetRelativeTo(Player.Get());
	Input->AddInputComponent(Player->GetInput()); //Register to broadcaster
	SceneLayer->RegisterSingleComponent(Player->GetVisualizer());

	SD::UnitTester::TestLog(TestFlags, TXT("The PhysicsTester unit test has launched. It will terminate based on user input."));
}
SD_TESTER_END
#endif