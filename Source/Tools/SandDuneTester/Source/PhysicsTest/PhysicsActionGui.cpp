/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsActionGui.cpp
=====================================================================
*/

#include "PhysicsTester.h"
#include "PhysicsActionComponent.h"
#include "PhysicsActionGui.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::PhysicsActionGui, SD::GuiEntity)
SD_TESTER_BEGIN

void PhysicsActionGui::InitProps ()
{
	Super::InitProps();

	BaseColors[0] = SD::Color(255, 0, 0, 255);
	BaseColors[1] = SD::Color(0, 255, 0, 255);
	BaseColors[2] = SD::Color(0, 0, 255, 255);

	OptionHeight = 32.f;

	ListComp = nullptr;
	ChannelLabel = nullptr;
	PhysicsAction = nullptr;
	RedChannelButton = nullptr;
	GreenChannelButton = nullptr;
	BlueChannelButton = nullptr;
	InstructionLabel = nullptr;
}

void PhysicsActionGui::ConstructUI ()
{
	Super::ConstructUI();

	SetAutoSizeVertical(true);

	ListComp = SD::VerticalList::CreateObject();
	if (AddComponent(ListComp))
	{
		ListComp->SetPosition(SD::Vector2::ZERO_VECTOR);
		ListComp->SetSize(SD::Vector2(1.f, 1.f));
		if (SD::TickComponent* tick = ListComp->GetRefreshingTick())
		{
			tick->Destroy();
		}

		SD::FrameComponent* background = SD::FrameComponent::CreateObject();
		if (ListComp->AddComponent(background))
		{
			background->SetPosition(SD::Vector2::ZERO_VECTOR);
			background->SetSize(SD::Vector2(1.f, 1.f));
			background->SetCenterColor(SD::Color(0, 0, 0, 96));
			background->SetBorderThickness(0.f);

			if (SD::BorderRenderComponent* borderComp = background->GetBorderComp())
			{
				borderComp->Destroy();
			}
		}

		ChannelLabel = SD::LabelComponent::CreateObject();
		if (ListComp->AddComponent(ChannelLabel))
		{
			ChannelLabel->SetAutoRefresh(false);
			ChannelLabel->SetPosition(SD::Vector2::ZERO_VECTOR);
			ChannelLabel->SetSize(SD::Vector2(1.f, OptionHeight));
			ChannelLabel->SetWrapText(false);
			ChannelLabel->SetClampText(true);
			ChannelLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			ChannelLabel->SetAutoRefresh(true);
		}

		SD::FrameComponent* channelFrame = SD::FrameComponent::CreateObject();
		if (ListComp->AddComponent(channelFrame))
		{
			channelFrame->SetPosition(SD::Vector2(0.f, OptionHeight));
			channelFrame->SetSize(SD::Vector2(1.f, OptionHeight));
			channelFrame->SetCenterColor(SD::Color(0, 0, 0, 24));
			channelFrame->SetBorderThickness(0.f);

			if (SD::BorderRenderComponent* borderComp = channelFrame->GetBorderComp())
			{
				borderComp->Destroy();
			}

			RedChannelButton = SD::ButtonComponent::CreateObject();
			if (channelFrame->AddComponent(RedChannelButton))
			{
				RedChannelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsActionGui, HandleRedChannelReleased, void, SD::ButtonComponent*));
			}

			GreenChannelButton = SD::ButtonComponent::CreateObject();
			if (channelFrame->AddComponent(GreenChannelButton))
			{
				GreenChannelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsActionGui, HandleGreenChannelReleased, void, SD::ButtonComponent*));
			}

			BlueChannelButton = SD::ButtonComponent::CreateObject();
			if (channelFrame->AddComponent(BlueChannelButton))
			{
				BlueChannelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsActionGui, HandleBlueChannelReleased, void, SD::ButtonComponent*));
			}
		}

		SD::Float buttonWidth = 0.25f;
		SD::Float spacing = (1 - (buttonWidth * 3.f)) / 4.f;
		SD::Float posX = spacing;
		std::vector<SD::ButtonComponent*> buttons
		{
			RedChannelButton,
			GreenChannelButton,
			BlueChannelButton
		};

		for (size_t i = 0; i < buttons.size(); ++i)
		{
			if (buttons.at(i) != nullptr)
			{
				buttons.at(i)->SetPosition(posX, 0.f);
				buttons.at(i)->SetSize(buttonWidth, 0.25f);
				buttons.at(i)->SetAnchorTop(0.05f);
				buttons.at(i)->SetAnchorBottom(0.05f);
				if (SD::LabelComponent* label = buttons.at(i)->GetCaptionComponent())
				{
					label->Destroy();
				}

				buttons.at(i)->ReplaceStateComponent(SD::ColorButtonState::SStaticClass());
				UpdateColorStates(buttons.at(i), i, true);
			}

			posX += (spacing + buttonWidth);
		}

		InstructionLabel = SD::LabelComponent::CreateObject();
		if (ListComp->AddComponent(InstructionLabel))
		{
			InstructionLabel->SetAutoRefresh(false);
			InstructionLabel->SetPosition(SD::Vector2(0.f, OptionHeight));
			InstructionLabel->SetSize(SD::Vector2(1.f, OptionHeight));
			InstructionLabel->SetWrapText(true);
			InstructionLabel->SetClampText(true);
			InstructionLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			InstructionLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			InstructionLabel->SetAutoRefresh(true);
		}

		ListComp->RefreshComponentTransforms();
	}
}

void PhysicsActionGui::SetChannelPrompt (const SD::DString& newChannelPrompt)
{
	if (ChannelLabel != nullptr)
	{
		ChannelLabel->SetText(newChannelPrompt);
	}
}

void PhysicsActionGui::SetInstructions (const SD::DString& newInstructions)
{
	if (InstructionLabel != nullptr)
	{
		InstructionLabel->SetText(newInstructions);
	}
}

void PhysicsActionGui::SetPhysicsAction (PhysicsActionComponent* newPhysicsAction)
{
	PhysicsAction = newPhysicsAction;
}

void PhysicsActionGui::UpdateColorStates (SD::ButtonComponent* button, size_t buttonIdx, bool bActive)
{
	//TODO: Make color function?
	std::function<SD::Color(SD::Color, SD::Float)> multiplyColor([](SD::Color originalColor, SD::Float multiplier)
	{
		SD::Color results(originalColor);

		SD::Float modifiedColor = originalColor.Source.r;
		modifiedColor *= multiplier;
		modifiedColor = SD::Utils::Clamp<SD::Float>(modifiedColor, 0.f, 255.f);
		results.Source.r = static_cast<sf::Uint8>(SD::Int(modifiedColor).ToUnsignedInt32());

		modifiedColor = originalColor.Source.g;
		modifiedColor *= multiplier;
		modifiedColor = SD::Utils::Clamp<SD::Float>(modifiedColor, 0.f, 255.f);
		results.Source.g = static_cast<sf::Uint8>(SD::Int(modifiedColor).ToUnsignedInt32());

		modifiedColor = originalColor.Source.b;
		modifiedColor *= multiplier;
		modifiedColor = SD::Utils::Clamp<SD::Float>(modifiedColor, 0.f, 255.f);
		results.Source.b = static_cast<sf::Uint8>(SD::Int(modifiedColor).ToUnsignedInt32());

		return results;
	});

	if (SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(button->GetStateComponent()))
	{
		CHECK(buttonIdx < 3)
		SD::Float multiplier = (bActive) ? 1.f : 0.33f;
		
		SD::Color color = multiplyColor(BaseColors[buttonIdx], multiplier * 0.65f);
		colorState->SetDefaultColor(color);

		color = multiplyColor(BaseColors[buttonIdx], multiplier);
		colorState->SetHoverColor(color);

		color = multiplyColor(BaseColors[buttonIdx], multiplier * 0.4f);
		colorState->SetDownColor(color);

		color = multiplyColor(BaseColors[buttonIdx], multiplier * 0.25f);
		colorState->SetDisabledColor(color);
	}
}

void PhysicsActionGui::HandleRedChannelReleased (SD::ButtonComponent* button)
{
	if (PhysicsAction.IsValid())
	{
		SD::Int newChannels = PhysicsAction->GetCollisionChannels();
		newChannels = (newChannels ^ PhysicsTester::CC_RedChannel); //Toggle red
		bool bActive = ((newChannels & PhysicsTester::CC_RedChannel) > 0);
		PhysicsAction->SetCollisionChannels(newChannels);
		UpdateColorStates(button, 0, bActive);
	}
}

void PhysicsActionGui::HandleGreenChannelReleased (SD::ButtonComponent* button)
{
	if (PhysicsAction.IsValid())
	{
		SD::Int newChannels = PhysicsAction->GetCollisionChannels();
		newChannels = (newChannels ^ PhysicsTester::CC_GreenChannel); //Toggle green
		bool bActive = ((newChannels & PhysicsTester::CC_GreenChannel) > 0);
		PhysicsAction->SetCollisionChannels(newChannels);
		UpdateColorStates(button, 1, bActive);
	}
}

void PhysicsActionGui::HandleBlueChannelReleased (SD::ButtonComponent* button)
{
	if (PhysicsAction.IsValid())
	{
		SD::Int newChannels = PhysicsAction->GetCollisionChannels();
		newChannels = (newChannels ^ PhysicsTester::CC_BlueChannel); //Toggle blue
		bool bActive = ((newChannels & PhysicsTester::CC_BlueChannel) > 0);
		PhysicsAction->SetCollisionChannels(newChannels);
		UpdateColorStates(button, 2, bActive);
	}
}
SD_TESTER_END
#endif