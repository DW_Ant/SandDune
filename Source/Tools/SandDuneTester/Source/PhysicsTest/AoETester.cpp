/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AoETester.cpp
=====================================================================
*/

#include "AoEGui.h"
#include "AoETester.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::AoETester, SDTester::PhysicsActionComponent)
SD_TESTER_BEGIN

void AoETester::InitProps ()
{
	Super::InitProps();

	GuiClass = AoEGui::SStaticClass();

	ShapeType = ST_Point;
	Radius = 32.f;
}

void AoETester::InitializeGuiInstance ()
{
	Super::InitializeGuiInstance();

	if (PhysicsActionGui* actionGui = dynamic_cast<PhysicsActionGui*>(GuiInstance))
	{
		actionGui->SetPhysicsAction(this);

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		SD::DString fileName = TXT("SandDuneTester");
		SD::DString section = TXT("AoETester");
		actionGui->SetChannelPrompt(translator->TranslateText(TXT("ChannelPrompt"), fileName, section));
		actionGui->SetInstructions(translator->TranslateText(TXT("Instructions"), fileName, section));
	}
}

void AoETester::DoAction (const SD::Vector3& actionPos)
{
	PerformAoE(actionPos);
}

void AoETester::PerformAoE (const SD::Vector3& position)
{
	SD::PhysicsUtils::SRelevanceParams params;
	params.CollisionChannels = CollisionChannels;
	SD::Color aoeColor = PhysicsTester::CalcChannelColor(CollisionChannels);

	SD::PhysicsEngineComponent* localPhysEngine = SD::PhysicsEngineComponent::Find();
	CHECK(localPhysEngine != nullptr)
	std::vector<SD::PhysicsComponent*> results;
	
	switch (ShapeType)
	{
		case(ST_Point):
			results = SD::PhysicsUtils::FindEncompassingComps(localPhysEngine->GetRootPhysTree(), position, params);
			break;

		case(ST_Sphere):
		{
			results = SD::PhysicsUtils::FindNearbyComps(localPhysEngine->GetRootPhysTree(), position, Radius, params);

			SD::GraphicsEngineComponent* localGraphics = SD::GraphicsEngineComponent::Find();
			CHECK(localGraphics != nullptr)
			SD::Color drawColor = Tester->CalcChannelColor(params.CollisionChannels);
			drawColor.Source.a = 128;
			localGraphics->DrawCircle(position + SD::Vector3(0.f, 0.f, -5.f), Radius, drawColor, 3.f, Tester->GetSceneLayer());
			break;
		}
	}

	for (SD::PhysicsComponent* result : results)
	{
		//Flash the physics visualizer component.
		if (PhysicsVisualizer* visualizer = dynamic_cast<PhysicsVisualizer*>(result->GetOwner()->FindComponent(PhysicsVisualizer::SStaticClass(), true)))
		{
			visualizer->FlashColor(1.f);
		}
	}
}
SD_TESTER_END
#endif