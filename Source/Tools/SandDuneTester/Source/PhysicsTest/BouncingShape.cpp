/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  BouncingShape.cpp
=====================================================================
*/

#include "BouncingShape.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::BouncingShape, SD::SceneEntity)
SD_TESTER_BEGIN

void BouncingShape::InitProps ()
{
	Super::InitProps();

	BaseVelocityMagnitude = 64.f;
	SD::Float accelMag(64.f);
	AccelerationList =
	{
		SD::Vector3(0.f, -accelMag, 0.f),
		SD::Vector3(accelMag, 0.f, 0.f),
		SD::Vector3(0.f, accelMag, 0.f),
		SD::Vector3(-accelMag, 0.f, 0.f)
	};
	AccelerationIdx = 0;
	PhysComp = nullptr;
}

void BouncingShape::BeginObject ()
{
	Super::BeginObject();

	PhysComp = SD::PhysicsComponent::CreateObject();
	if (AddComponent(PhysComp))
	{
		PhysComp->SetBlocked(SDFUNCTION_2PARAM(this, BouncingShape, HandleBlocked, void, SD::PhysicsComponent*, SD::PhysicsComponent*));
		PhysComp->SetAcceleration(AccelerationList.at(AccelerationIdx));
	}
}

void BouncingShape::SetBaseVelocityMagnitude (SD::Float newBaseVelocityMagnitude)
{
	BaseVelocityMagnitude = newBaseVelocityMagnitude;
}

void BouncingShape::HandleBlocked (SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp)
{
	//When normals are added, the better solution is to reflect the velocity by the normal. Until then, use a directional vector based on Entity positions
	SD::Rotator rotation(delegateOwner->GetTransformOwner()->ReadAbsTranslation() - otherComp->GetTransformOwner()->ReadAbsTranslation());
	rotation.Pitch = 0;

	delegateOwner->SetVelocity(rotation.GetDirectionalVector() * BaseVelocityMagnitude);

	++AccelerationIdx;
	AccelerationIdx %= AccelerationList.size();
	delegateOwner->SetAcceleration(AccelerationList.at(AccelerationIdx));
}
SD_TESTER_END
#endif