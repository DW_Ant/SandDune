/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsActionComponent.cpp
=====================================================================
*/

#include "PhysicsActionComponent.h"
#include "PhysicsTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_ABSTRACT_CLASS(SDTester::PhysicsActionComponent, SD::EntityComponent)
SD_TESTER_BEGIN

void PhysicsActionComponent::InitProps ()
{
	Super::InitProps();

	CollisionChannels = (PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);

	GuiClass = nullptr;
	GuiInstance = nullptr;
	Tester = nullptr;
	Input = nullptr;
}

void PhysicsActionComponent::BeginObject ()
{
	Super::BeginObject();

	Input = SD::InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->OnMouseClick = SDFUNCTION_3PARAM(this, PhysicsActionComponent, HandleMouseClick, bool, SD::MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}
}

void PhysicsActionComponent::Destroyed ()
{
	if (GuiInstance != nullptr)
	{
		GuiInstance->Destroy();
		GuiInstance = nullptr;
	}

	Super::Destroyed();
}

void PhysicsActionComponent::InitializePhysicsAction (PhysicsTester* newTester)
{
	if (GuiInstance != nullptr)
	{
		GuiInstance->Destroy();
		GuiInstance = nullptr;
	}

	Tester = newTester;
	if (Tester != nullptr && Tester->GetInput() != nullptr)
	{
		Tester->GetInput()->AddInputComponent(Input);
		InitializeGuiInstance();
	}
}

void PhysicsActionComponent::SetCollisionChannels (SD::Int newCollisionChannels)
{
	CollisionChannels = newCollisionChannels;
}

void PhysicsActionComponent::InitializeGuiInstance ()
{
	if (GuiClass != nullptr && GuiClass->IsA(SD::GuiEntity::SStaticClass()) && !GuiClass->IsAbstract())
	{
		if (SD::GuiDrawLayer* guiLayer = Tester->GetGuiDrawLayer())
		{
			GuiInstance = dynamic_cast<SD::GuiEntity*>(GuiClass->GetDefaultObject()->CreateObjectOfMatchingClass());
			CHECK(GuiInstance != nullptr)
			guiLayer->RegisterMenu(GuiInstance);

			GuiInstance->SetupInputComponent(Tester->GetInput(), 100);
			GuiInstance->SetPosition(SD::Vector2::ZERO_VECTOR);
			GuiInstance->SetSize(SD::Vector2(0.25f, 0.2f));
		}
	}
}

bool PhysicsActionComponent::HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& evnt, sf::Event::EventType type)
{
	if (evnt.button == sf::Mouse::Left && type == sf::Event::MouseButtonReleased)
	{
		if (Tester != nullptr && Tester->GetWindowHandle() != nullptr && Tester->GetSceneCamera() != nullptr)
		{
			SD::Rotator rotation;
			SD::Vector3 actionPos;
			Tester->GetSceneCamera()->CalculatePixelProjection(Tester->GetWindowHandle()->GetSize(), SD::Vector2(evnt.x, evnt.y), OUT rotation, OUT actionPos);
			actionPos.Z = 0.f;
			DoAction(actionPos);
			
			return true;
		}
	}

	return false;
}
SD_TESTER_END
#endif