/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  RayTracer.cpp
=====================================================================
*/

#include "BouncingShape.h"
#include "PhysicsActionGui.h"
#include "PhysicsPlayer.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"
#include "RayTracer.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::RayTracer, SDTester::PhysicsActionComponent)
SD_TESTER_BEGIN

void RayTracer::InitProps ()
{
	Super::InitProps();

	GuiClass = PhysicsActionGui::SStaticClass();

	MaxReflections = 3;
	ImpactForce = 100.f;
}

void RayTracer::InitializeGuiInstance ()
{
	Super::InitializeGuiInstance();

	if (PhysicsActionGui* actionGui = dynamic_cast<PhysicsActionGui*>(GuiInstance))
	{
		actionGui->SetPhysicsAction(this);

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		SD::DString fileName = TXT("SandDuneTester");
		SD::DString section = TXT("RayTracer");
		actionGui->SetChannelPrompt(translator->TranslateText(TXT("ChannelPrompt"), fileName, section));
		actionGui->SetInstructions(translator->TranslateText(TXT("Instructions"), fileName, section));
	}
}

void RayTracer::DoAction (const SD::Vector3& actionPos)
{
	if (Tester != nullptr && Tester->GetPlayer() != nullptr)
	{
		SD::Vector3 startTrace = Tester->GetPlayer()->GetTranslation();
		PerformTraces(startTrace, actionPos);
	}
}

void RayTracer::PerformTraces (const SD::Vector3& startTrace, const SD::Vector3& endTrace)
{
	CHECK(Tester != nullptr && Tester->GetPlayer() != nullptr)
	SD::PhysicsComponent* playerPhys = Tester->GetPlayer()->GetPhysics();
	SD::PhysicsUtils::STraceParams traceParams;
	traceParams.bCanCollideAtStart = true;
	traceParams.CollisionChannels = CollisionChannels;
	traceParams.ExceptionList.push_back(playerPhys);
	SD::Color traceColor = PhysicsTester::CalcChannelColor(CollisionChannels);

	SD::PhysicsEngineComponent* localPhysEngine = SD::PhysicsEngineComponent::Find();
	CHECK(localPhysEngine != nullptr)

	SD::Vector3 curStart(startTrace);
	SD::Vector3 curEnd(endTrace);
	SD::Float traceLength = (curStart - curEnd).VSize();
	for (SD::Int i = 0; i <= MaxReflections; ++i)
	{
		SD::PhysicsUtils::STraceResults results = SD::PhysicsUtils::RayTrace(localPhysEngine->GetRootPhysTree(), curStart, curEnd, traceParams);

		//Draw trace
		SD::GraphicsEngineComponent::DrawLine(curStart, results.EndPoint, 6.f, traceColor, 3.f, Tester->GetSceneLayer());

		if (results.HasImpact())
		{
			//Draw normal
			SD::GraphicsEngineComponent::DrawLine(results.EndPoint, results.EndPoint + (results.ReadNormal() * 50.f), 5.f, SD::Color::WHITE, 3.f, Tester->GetSceneLayer());

			//If it collided against a bouncing shape, push it back
			if (BouncingShape* shape = dynamic_cast<BouncingShape*>(results.CollidedAgainst->GetOwner()))
			{
				//TODO: When mass is implemented, replace SetVelocity with AddForce.
				results.CollidedAgainst->SetVelocity(results.CollidedAgainst->ReadVelocity() + (SD::Vector3::Normalize(curStart - curEnd) * -ImpactForce));
			}

			//Flash the physics visualizer component.
			if (PhysicsVisualizer* visualizer = dynamic_cast<PhysicsVisualizer*>(results.CollidedAgainst->GetOwner()->FindComponent(PhysicsVisualizer::SStaticClass(), true)))
			{
				visualizer->FlashColor(1.f);
			}

			SD::Vector3 delta = (curStart - curEnd);
			curEnd = delta.CalcReflection(results.ReadNormal());
			curEnd.SetLengthTo(traceLength);
			curEnd += results.EndPoint;
			curStart = results.EndPoint;

			SD::ContainerUtils::Empty(OUT traceParams.ExceptionList);
			traceParams.ExceptionList.push_back(playerPhys);
			traceParams.ExceptionList.push_back(results.CollidedAgainst);
		}
	}
}
SD_TESTER_END
#endif