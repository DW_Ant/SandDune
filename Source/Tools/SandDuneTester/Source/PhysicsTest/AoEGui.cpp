/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  AoEGui.cpp
=====================================================================
*/

#include "AoEGui.h"
#include "AoETester.h"
#include "PhysicsActionComponent.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::AoEGui, SDTester::PhysicsActionGui)
SD_TESTER_BEGIN

void AoEGui::InitProps ()
{
	Super::InitProps();

	StepSize = 32.f;

	PointButton = nullptr;
	SphereButton = nullptr;
	IncreaseSizeButton = nullptr;
	SizeLabel = nullptr;
	DecreaseSizeButton = nullptr;
}

void AoEGui::SetPhysicsAction (PhysicsActionComponent* newPhysicsAction)
{
	Super::SetPhysicsAction(newPhysicsAction);

	EvaluateGuiStates();
}

void AoEGui::ConstructUI ()
{
	Super::ConstructUI();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SD::DString fileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("AoEGui");

	CHECK(ListComp != nullptr)
	SD::FrameComponent* shapeFrame = SD::FrameComponent::CreateObject();
	if (ListComp->AddComponent(shapeFrame))
	{
		shapeFrame->SetPosition(SD::Vector2::ZERO_VECTOR);
		shapeFrame->SetSize(SD::Vector2(1.f, OptionHeight));
		shapeFrame->SetCenterColor(SD::Color(0, 0, 0, 24));
		shapeFrame->SetBorderThickness(0.f);

		if (SD::BorderRenderComponent* borderComp = shapeFrame->GetBorderComp())
		{
			borderComp->Destroy();
		}

		std::vector<SD::ButtonComponent*> buttons;

		PointButton = SD::ButtonComponent::CreateObject();
		if (shapeFrame->AddComponent(PointButton))
		{
			PointButton->SetCaptionText(translator->TranslateText(TXT("PointButton"), fileName, sectionName));
			PointButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, AoEGui, HandlePointReleased, void, SD::ButtonComponent*));
			buttons.push_back(PointButton);
		}

		SphereButton = SD::ButtonComponent::CreateObject();
		if (shapeFrame->AddComponent(SphereButton))
		{
			SphereButton->SetCaptionText(translator->TranslateText(TXT("SphereButton"), fileName, sectionName));
			SphereButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, AoEGui, HandleSphereReleased, void, SD::ButtonComponent*));
			buttons.push_back(SphereButton);
		}
		
		SD::Float buttonWidth = 0.25f;
		SD::Float spacing = (1 - (buttonWidth * 2.f)) / 3.f;
		SD::Float posX = spacing;
		for (SD::ButtonComponent* button : buttons)
		{
			button->SetPosition(SD::Vector2(posX, 0.01f));
			button->SetSize(SD::Vector2(buttonWidth, 0.98f));
			InitButtonColors(button);

			posX += (buttonWidth + spacing);
		}
	}

	SD::FrameComponent* sizeFrame = SD::FrameComponent::CreateObject();
	if (ListComp->AddComponent(sizeFrame))
	{
		sizeFrame->SetPosition(SD::Vector2(0.f, 0.f));
		sizeFrame->SetSize(SD::Vector2(1.f, OptionHeight));
		sizeFrame->SetCenterColor(SD::Color(0, 0, 0, 24));
		sizeFrame->SetBorderThickness(0.f);

		if (SD::BorderRenderComponent* borderComp = sizeFrame->GetBorderComp())
		{
			borderComp->Destroy();
		}

		SD::Float posX = 0.f;

		SD::LabelComponent* label = SD::LabelComponent::CreateObject();
		if (sizeFrame->AddComponent(label))
		{
			label->SetAutoRefresh(false);
			label->SetPosition(SD::Vector2(posX, 0.f));
			label->SetSize(SD::Vector2(0.25f, 1.f));
			label->SetWrapText(false);
			label->SetClampText(false);
			label->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			label->SetText(translator->TranslateText(TXT("SizeLabel"), fileName, sectionName));
			label->SetAutoRefresh(true);
			posX += label->ReadSize().X;
		}

		IncreaseSizeButton = SD::ButtonComponent::CreateObject();
		if (sizeFrame->AddComponent(IncreaseSizeButton))
		{
			IncreaseSizeButton->SetPosition(SD::Vector2(posX, 0.f));
			IncreaseSizeButton->SetSize(SD::Vector2(0.2f, 1.f));
			IncreaseSizeButton->SetCaptionText(TXT("+"));
			InitButtonColors(IncreaseSizeButton);
			IncreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, AoEGui, HandleIncreaseSizeReleased, void, SD::ButtonComponent*));
			posX += IncreaseSizeButton->ReadSize().X;
		}

		SizeLabel = SD::LabelComponent::CreateObject();
		if (sizeFrame->AddComponent(SizeLabel))
		{
			SizeLabel->SetAutoRefresh(false);
			SizeLabel->SetPosition(SD::Vector2(posX, 0.f));
			SizeLabel->SetSize(SD::Vector2(0.25f, 1.f));
			SizeLabel->SetWrapText(false);
			SizeLabel->SetClampText(false);
			SizeLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			SizeLabel->SetAutoRefresh(true);
			posX += SizeLabel->ReadSize().X;
		}

		DecreaseSizeButton = SD::ButtonComponent::CreateObject();
		if (sizeFrame->AddComponent(DecreaseSizeButton))
		{
			DecreaseSizeButton->SetPosition(SD::Vector2(posX, 0.f));
			DecreaseSizeButton->SetSize(SD::Vector2(0.2f, 1.f));
			DecreaseSizeButton->SetCaptionText(TXT("-"));
			InitButtonColors(DecreaseSizeButton);
			DecreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, AoEGui, HandleDecreaseSizeReleased, void, SD::ButtonComponent*));
			posX += DecreaseSizeButton->ReadSize().X;
		}
	}

	ListComp->MoveComponentToBottom(InstructionLabel);
	ListComp->RefreshComponentTransforms();
}

void AoEGui::InitButtonColors (SD::ButtonComponent* button)
{
	if (SD::ColorButtonState* state = dynamic_cast<SD::ColorButtonState*>(button->ReplaceStateComponent(SD::ColorButtonState::SStaticClass())))
	{
		state->SetDefaultColor(SD::Color(128, 128, 128));
		state->SetDisabledColor(SD::Color(64, 48, 48));
		state->SetHoverColor(SD::Color(128, 128, 175));
		state->SetDownColor(SD::Color(96, 96, 128));
	}
}

void AoEGui::EvaluateGuiStates ()
{
	if (AoETester* tester = dynamic_cast<AoETester*>(PhysicsAction.Get()))
	{
		if (PointButton != nullptr)
		{
			PointButton->SetEnabled(tester->ShapeType != AoETester::ST_Point);
		}

		if (SphereButton != nullptr)
		{
			SphereButton->SetEnabled(tester->ShapeType != AoETester::ST_Sphere);
		}

		if (SizeLabel != nullptr)
		{
			SizeLabel->SetText(tester->Radius.ToInt().ToString());
		}
	}
}

void AoEGui::HandlePointReleased (SD::ButtonComponent* button)
{
	if (AoETester* tester = dynamic_cast<AoETester*>(PhysicsAction.Get()))
	{
		tester->ShapeType = AoETester::ST_Point;
	}

	EvaluateGuiStates();
}

void AoEGui::HandleSphereReleased (SD::ButtonComponent* button)
{
	if (AoETester* tester = dynamic_cast<AoETester*>(PhysicsAction.Get()))
	{
		tester->ShapeType = AoETester::ST_Sphere;
	}

	EvaluateGuiStates();
}

void AoEGui::HandleIncreaseSizeReleased (SD::ButtonComponent* button)
{
	if (AoETester* tester = dynamic_cast<AoETester*>(PhysicsAction.Get()))
	{
		tester->Radius += StepSize;
	}

	EvaluateGuiStates();
}

void AoEGui::HandleDecreaseSizeReleased (SD::ButtonComponent* button)
{
	if (AoETester* tester = dynamic_cast<AoETester*>(PhysicsAction.Get()))
	{
		tester->Radius = SD::Utils::Max<SD::Float>(tester->Radius - StepSize, StepSize);
	}

	EvaluateGuiStates();
}
SD_TESTER_END
#endif