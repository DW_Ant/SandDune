/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SweepTester.cpp
=====================================================================
*/

#include "PhysicsActionGui.h"
#include "PhysicsPlayer.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"
#include "SweepTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::SweepTester, SDTester::PhysicsActionComponent)
SD_TESTER_BEGIN

void SweepTester::InitProps ()
{
	Super::InitProps();

	GuiClass = PhysicsActionGui::SStaticClass();
}

void SweepTester::InitializeGuiInstance ()
{
	Super::InitializeGuiInstance();

	if (PhysicsActionGui* actionGui = dynamic_cast<PhysicsActionGui*>(GuiInstance))
	{
		actionGui->SetPhysicsAction(this);

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		SD::DString fileName = TXT("SandDuneTester");
		SD::DString section = TXT("SweepTester");
		actionGui->SetChannelPrompt(translator->TranslateText(TXT("ChannelPrompt"), fileName, section));
		actionGui->SetInstructions(translator->TranslateText(TXT("Instructions"), fileName, section));
	}
}

void SweepTester::DoAction (const SD::Vector3& actionPos)
{
	if (Tester != nullptr && Tester->GetPlayer() != nullptr)
	{
		SD::Vector3 startSweep = Tester->GetPlayer()->GetTranslation();
		PerformSweep(startSweep, actionPos);
	}
}

void SweepTester::PerformSweep (const SD::Vector3& startSweep, const SD::Vector3& endSweep)
{
	CHECK(Tester != nullptr && Tester->GetPlayer() != nullptr)
	SD::PhysicsComponent* playerPhys = Tester->GetPlayer()->GetPhysics();
	SD::PhysicsUtils::SRelevanceParams sweepParam;
	sweepParam.CollisionChannels = CollisionChannels;
	sweepParam.ExceptionList.push_back(playerPhys);
	SD::Color sweepColor = PhysicsTester::CalcChannelColor(CollisionChannels);

	SD::PhysicsEngineComponent* localPhysEngine = SD::PhysicsEngineComponent::Find();
	CHECK(localPhysEngine != nullptr)
	std::vector<SD::PhysicsComponent*> results = SD::PhysicsUtils::Sweep(localPhysEngine->GetRootPhysTree(), startSweep, endSweep, sweepParam);

	//Draw sweep
	SD::GraphicsEngineComponent::DrawLine(startSweep, endSweep, 6.f, sweepColor, 3.f, Tester->GetSceneLayer());

	for (SD::PhysicsComponent* result : results)
	{
		//Flash the physics visualizer component.
		if (PhysicsVisualizer* visualizer = dynamic_cast<PhysicsVisualizer*>(result->GetOwner()->FindComponent(PhysicsVisualizer::SStaticClass(), true)))
		{
			visualizer->FlashColor(1.f);
		}
	}
}
SD_TESTER_END
#endif