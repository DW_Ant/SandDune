/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsMap.cpp
=====================================================================
*/

#include "AoETester.h"
#include "BouncingShape.h"
#include "PhysicsMap.h"
#include "PhysicsPlayer.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"
#include "RayTracer.h"
#include "SweepTester.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::PhysicsMap, SD::Entity)
SD_TESTER_BEGIN

void PhysicsMap::InitProps ()
{
	Super::InitProps();

	LocalizationFileName = TXT("SandDuneTester");
	SectionName = TXT("PhysicsMap");
	VerticalChangeDirectionFrequency = 10.f;
	Tester = nullptr;
}

void PhysicsMap::Destroyed ()
{
	for (Entity* mapObj : MapObjects)
	{
		CHECK(mapObj != nullptr) //None of the map objects should have been destroyed externally.
		mapObj->Destroy();
	}
	SD::ContainerUtils::Empty(OUT MapObjects);

	Super::Destroyed();
}

void PhysicsMap::InitializeMapObjects (PhysicsTester* tester)
{
	CHECK(tester != nullptr)
	Tester = tester;

	//Creates a series a objects that encompasses an area of specified dimensions. The parameters specifies which directions should be open for a doorway.
	std::function<void(const SD::Vector3&, const SD::Vector3&, bool, bool, bool, bool)> createRoom([&](const SD::Vector3& centerPos, const SD::Vector3& dimensions, bool bOpenNorth, bool bOpenEast, bool bOpenSouth, bool bOpenWest)
	{
		const SD::Float doorwayWidth = 400.f;
		const SD::Float wallThickness = 500.f;
		SD::Int worldCollisionChannels = PhysicsTester::CC_Environment;
		SD::Int worldBlockingChannels = ~0; //block everything

		/*
		Top right corner
		0--------------1
		|              |
		|              |
		4-------P      |
				|      |
				|      |
				3------2
		*/
		{
			SD::SceneEntity* trCorner = SD::SceneEntity::CreateObject();
			trCorner->SetTranslation(centerPos + (dimensions * SD::Vector3(0.5f, -0.5f, 1.f)));
			MapObjects.push_back(trCorner);

			SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
			if (trCorner->AddComponent(physComp))
			{
				physComp->SetCollisionChannels(worldCollisionChannels);
				physComp->SetBlockingChannels(worldBlockingChannels);

				SD::CollisionExtrudePolygon* shape = new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
				{
					SD::Vector2(((dimensions.X * 0.5f) - (doorwayWidth * 0.5f)) * -1.f, -wallThickness),
					SD::Vector2(wallThickness, -wallThickness),
					SD::Vector2(wallThickness, (dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)),
					SD::Vector2(0.f, (dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)),
					SD::Vector2(0.f, 0.f),
					SD::Vector2(((dimensions.X * 0.5f) - (doorwayWidth * 0.5f)) * -1.f, 0.f)
				});
				physComp->AddShape(shape);
			}

			PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
			if (trCorner->AddComponent(visualizer))
			{
				visualizer->bUpdateOnceOnly = true;
				Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
			}
		}

		/*
		Bottom right corner
				4------0
				|      |
				|      |
		3-------P      |
		|              |
		|              |
		2--------------1
		*/
		{
			SD::SceneEntity* brCorner = SD::SceneEntity::CreateObject();
			brCorner->SetTranslation(centerPos + (dimensions * 0.5f));
			MapObjects.push_back(brCorner);

			SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
			if (brCorner->AddComponent(physComp))
			{
				physComp->SetCollisionChannels(worldCollisionChannels);
				physComp->SetBlockingChannels(worldBlockingChannels);

				SD::CollisionExtrudePolygon* shape = new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
				{
					SD::Vector2(wallThickness, ((dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)) * -1.f),
					SD::Vector2(wallThickness, wallThickness),
					SD::Vector2(((dimensions.X * 0.5f) - (doorwayWidth * 0.5f)) * -1.f, wallThickness),
					SD::Vector2(((dimensions.X * 0.5f) - (doorwayWidth * 0.5f)) * -1.f, 0.f),
					SD::Vector2::ZERO_VECTOR,
					SD::Vector2(0.f, ((dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)) * -1.f)
				});
				physComp->AddShape(shape);
			}

			PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
			if (brCorner->AddComponent(visualizer))
			{
				visualizer->bUpdateOnceOnly = true;
				Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
			}
		}

		/*
		Bottom left corner
		2-----3
		|     |
		|     |
		|     P------4
		|            |
		|            |
		1------------0
		*/
		{
			SD::SceneEntity* blCorner = SD::SceneEntity::CreateObject();
			blCorner->SetTranslation(centerPos + (dimensions * SD::Vector3(-0.5f, 0.5f, 1.f)));
			MapObjects.push_back(blCorner);

			SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
			if (blCorner->AddComponent(physComp))
			{
				physComp->SetCollisionChannels(worldCollisionChannels);
				physComp->SetBlockingChannels(worldBlockingChannels);

				SD::CollisionExtrudePolygon* shape = new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
				{
					SD::Vector2((dimensions.X * 0.5f) - (doorwayWidth * 0.5f), wallThickness),
					SD::Vector2(-wallThickness, wallThickness),
					SD::Vector2(-wallThickness, ((dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)) * -1.f),
					SD::Vector2(0.f, ((dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)) * -1.f),
					SD::Vector2::ZERO_VECTOR,
					SD::Vector2((dimensions.X * 0.5f) - (doorwayWidth * 0.5f), 0.f)
				});
				physComp->AddShape(shape);
			}

			PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
			if (blCorner->AddComponent(visualizer))
			{
				visualizer->bUpdateOnceOnly = true;
				Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
			}
		}

		/*
		Top left corner
		1-------------2
		|             |
		|             |
		|     P-------3
		|     |
		|     |
		0-----4
		*/
		{
			SD::SceneEntity* tlCorner = SD::SceneEntity::CreateObject();
			tlCorner->SetTranslation(centerPos + (dimensions * SD::Vector3(-0.5f, -0.5f, 1.f)));
			MapObjects.push_back(tlCorner);

			SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
			if (tlCorner->AddComponent(physComp))
			{
				physComp->SetCollisionChannels(worldCollisionChannels);
				physComp->SetBlockingChannels(worldBlockingChannels);

				SD::CollisionExtrudePolygon* shape = new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
				{
					SD::Vector2(-wallThickness, (dimensions.Y * 0.5f) - (doorwayWidth * 0.5f)),
					SD::Vector2(-wallThickness, -wallThickness),
					SD::Vector2((dimensions.X * 0.5f) - (doorwayWidth * 0.5f), -wallThickness),
					SD::Vector2((dimensions.X * 0.5f) - (doorwayWidth * 0.5f), 0.f),
					SD::Vector2::ZERO_VECTOR,
					SD::Vector2(0.f, (dimensions.Y * 0.5f) - (doorwayWidth * 0.5f))
				});
				physComp->AddShape(shape);
			}

			PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
			if (tlCorner->AddComponent(visualizer))
			{
				visualizer->bUpdateOnceOnly = true;
				Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
			}
		}

		//Create walls that blocks doorways
		std::vector<SD::Vector3> doorwayCenters;
		if (!bOpenNorth)
		{
			doorwayCenters.push_back(centerPos - SD::Vector3(0.f, (dimensions.Y * 0.5f) + (doorwayWidth * 0.5f), 0.f));
		}

		if (!bOpenEast)
		{
			doorwayCenters.push_back(centerPos + SD::Vector3((dimensions.X * 0.5f) + (doorwayWidth * 0.5f), 0.f, 0.f));
		}

		if (!bOpenSouth)
		{
			doorwayCenters.push_back(centerPos + SD::Vector3(0.f, (dimensions.Y * 0.5f) + (doorwayWidth * 0.5f), 0.f));
		}

		if (!bOpenWest)
		{
			doorwayCenters.push_back(centerPos - SD::Vector3((dimensions.X * 0.5f) + (doorwayWidth * 0.5f), 0.f, 0.f));
		}

		for (const SD::Vector3& doorCenter : doorwayCenters)
		{
			SD::SceneEntity* obj = SD::SceneEntity::CreateObject();
			obj->SetTranslation(doorCenter);
			MapObjects.push_back(obj);

			SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
			if (obj->AddComponent(physComp))
			{
				physComp->SetCollisionChannels(worldCollisionChannels);
				physComp->SetBlockingChannels(worldBlockingChannels);

				SD::CollisionExtrudePolygon* shape = new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
				{
					SD::Vector2(doorwayWidth * 0.5f, -doorwayWidth * 0.5f),
					SD::Vector2(doorwayWidth * 0.5f, doorwayWidth * 0.5f),
					SD::Vector2(-doorwayWidth * 0.5f, doorwayWidth * 0.5f),
					SD::Vector2(-doorwayWidth * 0.5f, -doorwayWidth * 0.5f)
				});
				physComp->AddShape(shape);
			}

			PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
			if (obj->AddComponent(visualizer))
			{
				visualizer->bUpdateOnceOnly = true;
				Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
			}
		}
	});

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::FontPool* localFontPool = SD::FontPool::FindFontPool();
	CHECK(localFontPool != nullptr)

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::SceneTransformComponent* floorTransform = SD::SceneTransformComponent::CreateObject();
	if (AddComponent(floorTransform))
	{
		const SD::Vector2 platformSize = SD::Vector2(100000.f, 100000.f); //1000 meters
		floorTransform->SetTranslation(platformSize.ToVector3() * -0.5f); //center it
		floorTransform->EditTranslation().Z = -100.f; //Render under everything

		SD::SpriteComponent* floor = SD::SpriteComponent::CreateObject();
		if (floorTransform->AddComponent(floor))
		{
			floor->SetSpriteTexture(localTexturePool->GetTexture(SD::HashedString("Tools.SandDuneTester.DesertWet")));
			floor->SetDrawCoordinatesMultipliers(100.f, 100.f);
			floor->EditBaseSize() = platformSize;
			Tester->GetSceneLayer()->RegisterSingleComponent(floor);
		}
	}

	//Create main floor
	createRoom(SD::Vector3::ZERO_VECTOR, SD::Vector3(2000.f, 2000.f, 0.f), false, true, true, true);
	CreateTextEntity(SD::Vector3(-1000.f, 0.f, 0.f), translator->TranslateText(TXT("CollisionChannelTest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());
	CreateTextEntity(SD::Vector3(500.f, 0.f, 0.f), translator->TranslateText(TXT("ComplexCollisionTest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());

	//Create an object with a render component to test if the physics coordinate system is the same as the SceneDrawLayer.
	{
		SD::SceneEntity* renderTest = SD::SceneEntity::CreateObject();
		renderTest->SetTranslation(SD::Vector3(250.f, -950.f, 1.f));
		MapObjects.push_back(renderTest);

		SD::Float radius = 48.f;

		SD::ColorRenderComponent* color = SD::ColorRenderComponent::CreateObject();
		if (renderTest->AddComponent(color))
		{
			color->SetBaseSize(radius * 2.f, radius * 2.f);
			color->SolidColor = SD::Color(64, 64, 64, 64);
			color->SetPivot(0.5f, 0.5f);
			color->SetShape(SD::ColorRenderComponent::ST_Circle);
			Tester->GetSceneLayer()->RegisterSingleComponent(color);
		}

		SD::PhysicsComponent* phys = SD::PhysicsComponent::CreateObject();
		if (renderTest->AddComponent(phys))
		{
			phys->SetCollisionChannels(PhysicsTester::CC_Environment);
			phys->SetOverlappingChannels(PhysicsTester::CC_Character);
			phys->AddShape(new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, radius));
			phys->SetBeginOverlap(SD::SDFunction<void, SD::PhysicsComponent*, SD::PhysicsComponent*>([](SD::PhysicsComponent* thisComp, SD::PhysicsComponent* otherComp)
			{
				CHECK(thisComp != nullptr && thisComp->GetOwner() != nullptr)
				SD::ColorRenderComponent* color = dynamic_cast<SD::ColorRenderComponent*>(thisComp->GetOwner()->FindComponent(SD::ColorRenderComponent::SStaticClass()));
				if (color != nullptr)
				{
					color->SolidColor.Source.a = 128;
				}
			}));

			phys->SetEndOverlap(SD::SDFunction<void, SD::PhysicsComponent*, SD::PhysicsComponent*>([](SD::PhysicsComponent* thisComp, SD::PhysicsComponent* otherComp)
			{
				CHECK(thisComp != nullptr && thisComp->GetOwner() != nullptr)
				SD::ColorRenderComponent* color = dynamic_cast<SD::ColorRenderComponent*>(thisComp->GetOwner()->FindComponent(SD::ColorRenderComponent::SStaticClass()));
				if (color != nullptr)
				{
					color->SolidColor.Source.a = 64;
				}
			}));
		}
	}
	

	//Create the shape changers in the starter room
	{
		CreateShapeChanger(SD::Vector3(600.f, -750.f, 0.f), ([]()
		{
			return new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 50.f);
		}));

		CreateShapeChanger(SD::Vector3(800.f, -750.f, 0.f), ([]()
		{
			return new SD::CollisionCapsule(1.f, SD::Vector3::ZERO_VECTOR, 200.f, 50.f);
		}));

		CreateTextEntity(SD::Vector3(450.f, -900.f, 0.f), translator->TranslateText(TXT("ShapeChanger"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());

		//triangle
		CreateShapeChanger(SD::Vector3(750.f, 750.f, 0.f), ([]()
		{
			return new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(0.f, -128.f),
				SD::Vector2(128.f, 128.f),
				SD::Vector2(-128.f, 128.f)
			});
		}));

		//four pointed star
		CreateShapeChanger(SD::Vector3(-750.f, 750.f, 0.f), ([]()
		{
			return new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(0.f, -128.f),
				SD::Vector2(16.f, -16.f),
				SD::Vector2(128.f, 0.f),
				SD::Vector2(16.f, 16.f),
				SD::Vector2(0.f, 128.f),
				SD::Vector2(-16.f, 16.f),
				SD::Vector2(-128.f, 0.f),
				SD::Vector2(-16.f, -16.f)
			});
		}));

		/*
		Giant U
		0----1         7----8
		|    |         |    |
		|    3         6    |
		|     4---P---5     |
		C                   9
		 \                 /
		  B---------------A
		*/
		CreateShapeChanger(SD::Vector3(-750.f, -750.f, 0.f), ([]()
		{
			return new SD::CollisionExtrudePolygon(0.67f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(-256.f, -256.f),
				SD::Vector2(-128.f, -256.f),
				SD::Vector2(-128.f, -64.f),
				SD::Vector2(-64.f, 0.f),
				SD::Vector2::ZERO_VECTOR,
				SD::Vector2(64.f, 0.f),
				SD::Vector2(128.f, -64.f),
				SD::Vector2(128.f, -256.f),
				SD::Vector2(256.f, -256.f),
				SD::Vector2(256.f, 128.f),
				SD::Vector2(128.f, 256.f),
				SD::Vector2(-128.f, 256.f),
				SD::Vector2(-256.f, 128.f)
			});
		}));
	}

	SD::Vector3 roomCenter(-3000.f, 0.f, 0.f);
	SD::Vector3 roomDimensions(2000.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, false, true, false, false);
	CreateCollisionChannelRoom(roomCenter, roomDimensions);

	roomCenter = SD::Vector3(4000.f, 0.f, 0.f);
	roomDimensions.X *= 2.f;
	createRoom(roomCenter, roomDimensions, false, false, false, true);
	CreateComplexCollisionRoom(roomCenter, roomDimensions);

	//Create 2nd floor
	roomCenter = SD::Vector3(0.f, 3000.f, 0.f);
	roomDimensions = SD::Vector3(2000.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, true, true, true, true);
	CreateTextEntity(roomCenter + SD::Vector3(-1000.f, 0.f, 0.f), translator->TranslateText(TXT("AoETest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());
	CreateTextEntity(roomCenter + SD::Vector3(500.f, 0.f, 0.f), translator->TranslateText(TXT("MovingObjTest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());

	//Create shape changers in 2nd floor
	{
		CreateShapeChanger(roomCenter + SD::Vector3(400.f, -750.f, 0.f), ([]()
		{
			return new SD::CollisionSegment(2.f, SD::Vector3(-50.f, 0.f, 0.f), SD::Vector3(100.f, 0.f, 0.f));
		}));

		CreateShapeChanger(roomCenter + SD::Vector3(800.f, -600.f, 0.f), ([]()
		{
			return new SD::CollisionSegment(2.f, SD::Vector3(0.f, -50.f, 0.f), SD::Vector3(0.f, 100.f, 0.f));
		}));

		CreateShapeChanger(roomCenter + SD::Vector3(-400.f, -750.f, 0.f), ([]()
		{
			return new SD::CollisionSegment(1.f, SD::Vector3(-100.f, 100.f, 0.f), SD::Vector3(200.f, -200.f, 0.f));
		}));

		CreateShapeChanger(roomCenter + SD::Vector3(-800.f, -600.f, 0.f), ([]()
		{
			return new SD::CollisionSegment(1.f, SD::Vector3(-100.f, -100.f, 0.f), SD::Vector3(200.f, 200.f, 0.f));
		}));
	}

	//Create AoE test room
	roomCenter = SD::Vector3(-3000.f, 3000.f, 0.f);
	roomDimensions = SD::Vector3(2000.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, false, true, false, false);
	CreateAoERoom(roomCenter, roomDimensions);

	//Create moving object room
	roomCenter = SD::Vector3(5000.f, 3000.f, 0.f);
	roomDimensions = SD::Vector3(6000.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, false, false, false, true);
	CreateMovingObjectRoom(roomCenter, roomDimensions);

	//Create 3rd floor
	roomCenter = SD::Vector3(0.f, 6000.f, 0.f);
	roomDimensions = SD::Vector3(2000.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, true, true, false, true);

	{
		//Add "Ray Trace Test" label in the left of this chamber.
		SD::SceneEntity* textEntity = SD::SceneEntity::CreateObject();
		SD::Vector3 pos(roomCenter);
		pos.X -= (roomDimensions.X * 0.45f);
		CreateTextEntity(pos, translator->TranslateText(TXT("RayTraceTest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());
		textEntity->SetTranslation(pos);
		MapObjects.push_back(textEntity);
	}

	{
		//Add "Sweep Test" label in the right of this chamber.
		SD::Vector3 pos(roomCenter);
		pos.X += (roomDimensions.X * 0.25f);
		CreateTextEntity(pos, translator->TranslateText(TXT("SweepTest"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());
	}
	

	//Create ray trace room
	roomCenter = SD::Vector3(-3250.f, 6000.f, 0.f);
	roomDimensions = SD::Vector3(2500.f, 2000.f, 0.f);
	createRoom(roomCenter, roomDimensions, false, true, false, false);
	CreateRayTraceRoom(roomCenter, roomDimensions);

	//Create sweep room
	roomCenter.X *= -1.f;
	createRoom(roomCenter, roomDimensions, false, false, false, true);
	CreateSweepRoom(roomCenter, roomDimensions);
}

SD::Entity* PhysicsMap::CreateTextEntity (const SD::Vector3& topLeftPos, const SD::DString& text, SD::Font* font)
{
	SD::SceneEntity* textEntity = SD::SceneEntity::CreateObject();
	textEntity->SetTranslation(topLeftPos);
	MapObjects.push_back(textEntity);

	SD::TextRenderComponent* textComp = SD::TextRenderComponent::CreateObject();
	if (textEntity->AddComponent(textComp))
	{
		textComp->SetFontColor(SD::Color::BLUE.Source);
		textComp->CreateTextInstance(text, font, 64);
		Tester->GetSceneLayer()->RegisterSingleComponent(textComp);
	}

	return textEntity;
}

SD::PhysicsComponent* PhysicsMap::CreateShapeChanger (const SD::Vector3& position, std::function<SD::CollisionShape*(void)> newShapeInstance)
{
	SD::SceneEntity* newObj = SD::SceneEntity::CreateObject();
	newObj->SetTranslation(position);
	MapObjects.push_back(newObj);

	SD::PhysicsComponent* physics = SD::PhysicsComponent::CreateObject();
	if (newObj->AddComponent(physics))
	{
		physics->SetCollisionChannels(PhysicsTester::CC_Environment | PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
		physics->SetOverlappingChannels(PhysicsTester::CC_Character);

		std::function<void(SD::PhysicsComponent*, SD::PhysicsComponent*)> handleBeginOverlap([newShapeInstance](SD::PhysicsComponent* thisComp, SD::PhysicsComponent* comp)
		{
			if (comp != nullptr)
			{
				//Remove old shapes
				std::vector<SD::CollisionShape*> shapes;
				comp->GetShapes(OUT shapes);
				for (SD::CollisionShape* shape : shapes)
				{
					if (comp->RemoveShape(shape))
					{
						delete shape;
					}
				}

				//Add new shape
				comp->AddShape(newShapeInstance());
			}
		});
		physics->SetBeginOverlap(SD::SDFunction<void, SD::PhysicsComponent*, SD::PhysicsComponent*>(handleBeginOverlap, newObj, TXT("SceneEntity::HandleReplaceShape")));

		//Have this PhysicsComponent look like the shape it would transform the player into.
		physics->AddShape(newShapeInstance());
	}

	PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
	if (newObj->AddComponent(visualizer))
	{
		visualizer->bUpdateOnceOnly = true;

		if (Tester != nullptr && Tester->GetSceneLayer() != nullptr)
		{
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	}

	return physics;
}

void PhysicsMap::AddBouncingSpheres (const SD::Vector3& roomCenter, const SD::Vector3& roomDimensions, SD::Int numSpheres)
{
	std::vector<SD::Int> collisionChannels
	{
		PhysicsTester::CC_RedChannel,
		PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel,
		PhysicsTester::CC_GreenChannel,
		PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel,
		PhysicsTester::CC_BlueChannel,
		PhysicsTester::CC_BlueChannel | PhysicsTester::CC_RedChannel,
		PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel
	};

	std::vector<SD::Vector3> positions
	{
		SD::Vector3::ZERO_VECTOR,
		SD::Vector3(0.25f, 0.f, 0.f),
		SD::Vector3(0.25f, 0.25f, 0.f),
		SD::Vector3(0.f, 0.25f, 0.f),
		SD::Vector3(-0.25f, 0.25f, 0.f),
		SD::Vector3(-0.25f, 0.f, 0.f),
		SD::Vector3(-0.25f, -0.25f, 0.f),
		SD::Vector3(0.f, -0.25f, 0.f),
		SD::Vector3(0.25f, -0.25f, 0.f),
		SD::Vector3(0.5f, 0.f, 0.f),
		SD::Vector3(0.5f, 0.5f, 0.f),
		SD::Vector3(0.f, 0.5f, 0.f),
		SD::Vector3(-0.5f, 0.5f, 0.f),
		SD::Vector3(-0.5f, 0.f, 0.f),
		SD::Vector3(-0.5f, -0.5f, 0.f),
		SD::Vector3(0.f, -0.5f, 0.f),
		SD::Vector3(0.5f, -0.5f, 0.f),
		SD::Vector3(0.75f, 0.f, 0.f),
		SD::Vector3(0.75f, 0.75f, 0.f),
		SD::Vector3(0.f, 0.75f, 0.f),
		SD::Vector3(-0.75f, 0.75f, 0.f),
		SD::Vector3(-0.75f, 0.f, 0.f),
		SD::Vector3(-0.75f, -0.75f, 0.f),
		SD::Vector3(0.f, -0.75f, 0.f),
		SD::Vector3(0.75f, -0.75f, 0.f)
	};

	for (SD::Int i = 0; i < numSpheres; ++i)
	{
		BouncingShape* shape = BouncingShape::CreateObject();

		size_t posIdx = (i.ToUnsignedInt() % positions.size());
		shape->SetTranslation((roomDimensions * 0.5f * positions.at(posIdx)) + roomCenter);
		MapObjects.push_back(shape);

		if (SD::PhysicsComponent* phys = shape->GetPhysComp())
		{
			size_t idx = (i.ToUnsignedInt() % collisionChannels.size());
			phys->SetCollisionChannels(PhysicsTester::CC_Environment | collisionChannels.at(idx));
			phys->SetBlockingChannels(collisionChannels.at(idx));
			phys->AddShape(new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 64.f));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (shape->AddComponent(visualizer))
		{
			visualizer->SetShapeUpdateFrequency(1.f);
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	}
}

void PhysicsMap::CreatePhysActionObject (const SD::Vector3& roomCenter, const SD::Vector3& dimensions, const SD::DClass* componentClass)
{
	CHECK(componentClass != nullptr && PhysicsActionComponent::SStaticClass()->IsParentOf(componentClass) && !componentClass->IsAbstract())

	SD::SceneEntity* obj = SD::SceneEntity::CreateObject();
	obj->SetTranslation(roomCenter);
	MapObjects.push_back(obj);

	SD::PhysicsComponent* phys = SD::PhysicsComponent::CreateObject();
	if (obj->AddComponent(phys))
	{
		phys->SetOverlappingChannels(PhysicsTester::CC_Character);
		phys->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
		{
			SD::Vector2(dimensions.X, -dimensions.Y) * 0.5f,
			SD::Vector2(dimensions.X, dimensions.Y) * 0.5f,
			SD::Vector2(-dimensions.X, dimensions.Y) * 0.5f,
			SD::Vector2(-dimensions.X, -dimensions.Y) * 0.5f
		}));

		//Add the component cache to the ActionComponents map since componentClass pointer will go out of scope by the time Begin/End Overlap is executed.
		ActionComponents.push_back({phys, componentClass});

		phys->SetBeginOverlap(SD::SDFunction<void, SD::PhysicsComponent*, SD::PhysicsComponent*>([&](SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp)
		{
			const SD::DClass* associatedAction = nullptr;
			for (size_t i = 0; i < ActionComponents.size(); ++i)
			{
				if (ActionComponents.at(i).first == delegateOwner)
				{
					associatedAction = ActionComponents.at(i).second;
					break;
				}
			}

			if (associatedAction == nullptr)
			{
				return;
			}
			CHECK(!associatedAction->IsAbstract())

			SD::Entity* owner = otherComp->GetRootEntity();
			if (!owner->HasComponent(componentClass, false))
			{
				//Find the component associated 
				PhysicsActionComponent* actionComp = dynamic_cast<PhysicsActionComponent*>(associatedAction->GetDefaultObject()->CreateObjectOfMatchingClass());
				if (owner->AddComponent(actionComp))
				{
					actionComp->InitializePhysicsAction(Tester);
				}
			}
		}));

		phys->SetEndOverlap(SD::SDFunction<void, SD::PhysicsComponent*, SD::PhysicsComponent*>([&](SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp)
		{
			const SD::DClass* associatedAction = nullptr;
			for (size_t i = 0; i < ActionComponents.size(); ++i)
			{
				if (ActionComponents.at(i).first == delegateOwner)
				{
					associatedAction = ActionComponents.at(i).second;
					break;
				}
			}

			if (associatedAction == nullptr)
			{
				return;
			}

			SD::Entity* owner = otherComp->GetRootEntity();
			if (owner != nullptr)
			{
				if (SD::EntityComponent* physAction = (owner->FindComponent(associatedAction, false)))
				{
					physAction->Destroy();
				}
			}
		}));
	}
}

void PhysicsMap::CreateCollisionChannelRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	SD::FontPool* localFontPool = SD::FontPool::FindFontPool();
	CHECK(localFontPool != nullptr && localFontPool->GetDefaultFont() != nullptr)

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::Vector3 pos(roomCenter);
	pos.X += (dimensions.X * 0.25f);
	CreateTextEntity(pos, translator->TranslateText(TXT("CollisionChannelDescription"), LocalizationFileName, SectionName), localFontPool->GetDefaultFont());

	std::function<void(const SD::Vector3&, SD::Int)> createTriangle([&](const SD::Vector3& trianglePos, SD::Int collisionChannel)
	{
		SD::SceneEntity* triangle = SD::SceneEntity::CreateObject();
		triangle->SetTranslation(trianglePos);
		MapObjects.push_back(triangle);

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateObject();
		if (triangle->AddComponent(comp))
		{
			comp->SetCollisionChannels(collisionChannel);
			comp->SetOverlappingChannels(PhysicsTester::CC_Character);
			comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(0.f, 64.f),
				SD::Vector2(-64.f, -64.f),
				SD::Vector2(64.f, -64.f)
			}));

			comp->SetBeginOverlap(SDFUNCTION_2PARAM(this, PhysicsMap, HandleBeginCollisionChannelOverlap, void, SD::PhysicsComponent*, SD::PhysicsComponent*));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (triangle->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	});

	std::function<void(const SD::Vector3&, SD::Int)> createSphere([&](const SD::Vector3& circlePos, SD::Int collisionChannel)
	{
		SD::SceneEntity* sphere = SD::SceneEntity::CreateObject();
		sphere->SetTranslation(circlePos);
		MapObjects.push_back(sphere);

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateObject();
		if (sphere->AddComponent(comp))
		{
			comp->SetCollisionChannels(collisionChannel);
			comp->SetBlockingChannels(collisionChannel);
			comp->AddShape(new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 75.f));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (sphere->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	});

	//Red triangle
	pos = roomCenter;
	pos.Y -= 64.f;
	createTriangle(pos, PhysicsTester::CC_RedChannel);

	//Green triangle
	pos = roomCenter + SD::Vector3(-64.f, 64.f, 0.f);
	createTriangle(pos, PhysicsTester::CC_GreenChannel);

	//Blue triangle
	pos = roomCenter + SD::Vector3(64.f, 64.f, 0.f);
	createTriangle(pos, PhysicsTester::CC_BlueChannel);

	SD::Float roomMultiplier = 0.4f;
	pos = roomCenter + SD::Vector3(dimensions.X * roomMultiplier, -dimensions.Y * roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_RedChannel);

	pos = roomCenter + SD::Vector3(0.f, dimensions.Y * -roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel);

	pos = roomCenter + SD::Vector3(dimensions.X * -roomMultiplier, dimensions.Y * -roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_GreenChannel);

	pos = roomCenter + SD::Vector3(dimensions.X * -roomMultiplier, 0.f, 0.f);
	createSphere(pos, PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);

	pos = roomCenter + SD::Vector3(dimensions.X * -roomMultiplier, dimensions.Y * roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_BlueChannel);

	pos = roomCenter + SD::Vector3(0.f, dimensions.Y * roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_BlueChannel | PhysicsTester::CC_RedChannel);

	pos = roomCenter + SD::Vector3(dimensions.X * roomMultiplier, dimensions.Y * roomMultiplier, 0.f);
	createSphere(pos, PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
}

void PhysicsMap::CreateComplexCollisionRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	std::function<SD::PhysicsComponent*(const SD::Vector3&)> createObj([&](const SD::Vector3& pos)
	{
		SD::SceneEntity* obj = SD::SceneEntity::CreateObject();
		obj->SetTranslation(pos);
		MapObjects.push_back(obj);

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateObject();
		if (obj->AddComponent(comp))
		{
			comp->SetCollisionChannels(PhysicsTester::CC_Environment | PhysicsTester::CC_RedChannel);
			comp->SetBlockingChannels(PhysicsTester::CC_Environment | PhysicsTester::CC_Character | PhysicsTester::CC_RedChannel);
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (obj->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}

		return comp;
	});

	/*
	  0-----1
	 /       \
	5    P    2
	|   / \   |
	|  /   \  |
	| /     \ |
	|/       \|
	4         3
	*/
	SD::Vector3 pos = roomCenter;
	pos.X += (dimensions.X * -0.25f);
	SD::PhysicsComponent* comp = createObj(pos);
	comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
	{
		SD::Vector2(-200.f, -256.f),
		SD::Vector2(200.f, -256.f),
		SD::Vector2(256.f, -64.f),
		SD::Vector2(256.f, 256.f),
		SD::Vector2::ZERO_VECTOR,
		SD::Vector2(-256.f, 256.f),
		SD::Vector2(-256.f, -64.f)
	}));

	/*
	Multiple shape object
	O = spheres

	O     0
		 / \
		3 P 1
		 \ /
		  2   O
	*/
	pos.X += (dimensions.X * 0.33f);
	comp = createObj(pos);
	VertMovingComps.push_back(comp);
	comp->AddShape(new SD::CollisionSphere(1.f, SD::Vector3(-128.f, -128.f, 0.f), 64.f));
	comp->AddShape(new SD::CollisionSphere(1.f, SD::Vector3(128.f, 128.f, 0.f), 64.f));
	comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
	{
		SD::Vector2(0.f, -128.f),
		SD::Vector2(128.f, 0.f),
		SD::Vector2(0.f, 128.f),
		SD::Vector2(-128.f, 0.f)
	}));


	/*
	Multiple shapes
		  1---2         F---0
		 /   /         /    |
	7---0   /    C----D     |
	|   P  /    /           1
	6  4--3    /     4---3 /
	 \ |      /      |   |/
	  \|     /       |   2
	   5    /    P   5
		   /        /     0
	   A--B    7---6     / \
		\     /         /   \
		 9---8         /  P  \
					  / .- -. \
					 2-'     '-1
	*/
	pos.X += (dimensions.X * 0.25f);
	comp = createObj(pos);
	VertMovingComps.push_back(comp);
	//Top left shape
	comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3(-384.f , -384.f, 0.f), 100.f,
	{
		SD::Vector2(0.f, -64.f),
		SD::Vector2(64.f, -128.f),
		SD::Vector2(128.f, -128.f),
		SD::Vector2(64.f, 96.f),
		SD::Vector2(-32.f, 96.f),
		SD::Vector2(-32.f, 128.f),
		SD::Vector2(-128.f, 64.f),
		SD::Vector2(-128.f, -64.f)
	}));

	//Center shape
	comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
	{
		/*[0]*/ SD::Vector2(512.f, -512.f), SD::Vector2(512.f, -384.f),	SD::Vector2(384.f, -256.f), SD::Vector2(384.f, -384.f),
		/*[4]*/ SD::Vector2(256.f, -384.f), SD::Vector2(256.f, 0.f), SD::Vector2(128.f, 128.f), SD::Vector2(-128.f, 128.f),
		/*[8]*/ SD::Vector2(-256.f, 256.f), SD::Vector2(-384.f, 256.f), SD::Vector2(-512.f, 128.f), SD::Vector2(-256.f, 128.f),
		/*[C]*/ SD::Vector2(0.f, -400.f), SD::Vector2(300.f, -400.f), SD::Vector2(400.f, -512.f)
	}));

	//Bottom left shape
	comp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3(384.f, 256.f, 0.f), 100.f,
	{
		SD::Vector2(0.f, -128.f),
		SD::Vector2(128.f, 128.f),
		SD::Vector2::ZERO_VECTOR,
		SD::Vector2(-128.f, 128.f)
	}));

	//Add movement component to show these shapes are paired together.
	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (comp->AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsMap, HandleTickMoveObjectVertically, void, SD::Float));
	}
}

void PhysicsMap::CreateAoERoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	CreatePhysActionObject(roomCenter, dimensions, AoETester::SStaticClass());

	std::vector<SD::Int> channels
	{
		PhysicsTester::CC_RedChannel,
		PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel,
		PhysicsTester::CC_GreenChannel,
		PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel,
		PhysicsTester::CC_BlueChannel,
		PhysicsTester::CC_BlueChannel | PhysicsTester::CC_RedChannel,
		PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel
	};

	//Create spheres at various locations. Some of them overlapping is intended.
	std::vector<SD::Vector3> positions
	{
		SD::Vector3(0.1f, 0.1f, 0.f),
		SD::Vector3(0.15f, 0.1f, 0.f),
		SD::Vector3(0.1f, 0.15f, 0.f),
		SD::Vector3(0.2f, 0.25f, 0.f),
		SD::Vector3(0.2f, 0.2f, 0.f),
		SD::Vector3(0.2f, 0.3f, 0.f),
		SD::Vector3(0.25f, 0.f, 0.f),
		SD::Vector3(0.25f, 0.25f, 0.f),
		SD::Vector3(-0.25f, 0.25f, 0.f),
		SD::Vector3(-0.25f, -0.25f, 0.f),
		SD::Vector3(-0.25f, 0.f, 0.f)
	};

	size_t channelIdx = 0;

	std::function<void(const SD::Vector3&)> spawnSphere([&](const SD::Vector3& pos)
	{
		SD::SceneEntity* sphere = SD::SceneEntity::CreateObject();
		sphere->SetTranslation(pos);
		MapObjects.push_back(sphere);

		SD::PhysicsComponent* phys = SD::PhysicsComponent::CreateObject();
		if (sphere->AddComponent(phys))
		{
			phys->SetCollisionChannels(channels.at(channelIdx));
			++channelIdx;
			channelIdx %= channels.size();

			phys->AddShape(new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 64.f));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (sphere->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	});

	for (const SD::Vector3& pos : positions)
	{
		spawnSphere(roomCenter + (dimensions * pos));
	}

	//Create a concave polygon
	SD::SceneEntity* concave = SD::SceneEntity::CreateObject();
	concave->SetTranslation(roomCenter);
	MapObjects.push_back(concave);

	SD::PhysicsComponent* phys = SD::PhysicsComponent::CreateObject();
	if (concave->AddComponent(phys))
	{
		phys->SetCollisionChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);

		/*
		A-B        G-H
		| |        | |
		| C        F |
		|  \   P  /  |
		L   D----E   I
		  K--------J
		*/
		phys->AddShape(new SD::CollisionExtrudePolygon(1.75f, SD::Vector3::ZERO_VECTOR, 10.f,
		{
			SD::Vector2(-150.f, -150.f), //A
			SD::Vector2(-100.f, -150.f),
			SD::Vector2(-100.f, -50.f), //C
			SD::Vector2(-50.f, 50.f),
			SD::Vector2(50.f, 50.f), //E
			SD::Vector2(100.f, -50.f),
			SD::Vector2(100.f, -150.f), //G
			SD::Vector2(150.f, -150.f),
			SD::Vector2(150.f, 75.f), //I
			SD::Vector2(100.f, 150.f),
			SD::Vector2(-100.f, 150.f), //K
			SD::Vector2(-150.f, 75.f)
		}));

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (concave->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	}
}

void PhysicsMap::CreateMovingObjectRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	SD::Int collisionChannels;
	SD::Int blockingChannels;
	std::function<void(const SD::Vector3&)> createSphere([&](const SD::Vector3& sphereCenter)
	{
		BouncingShape* newObj = BouncingShape::CreateObject();
		newObj->SetTranslation(sphereCenter);
		MapObjects.push_back(newObj);

		if (newObj->GetPhysComp() != nullptr)
		{
			newObj->GetPhysComp()->SetCollisionChannels(collisionChannels);
			newObj->GetPhysComp()->SetBlockingChannels(blockingChannels);
			newObj->GetPhysComp()->AddShape(new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 64.f));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (newObj->AddComponent(visualizer))
		{
			visualizer->SetShapeUpdateFrequency(1.f);
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	});

	std::function<void(const SD::Vector3&, SD::Int)> createChannelChanger([&](const SD::Vector3& center, SD::Int collisionChannel)
	{
		SD::SceneEntity* newObj = SD::SceneEntity::CreateObject();
		newObj->SetTranslation(center);
		MapObjects.push_back(newObj);

		SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
		if (newObj->AddComponent(physComp))
		{
			physComp->SetCollisionChannels(collisionChannel);
			physComp->SetOverlappingChannels(PhysicsTester::CC_Character);
			physComp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(0.f, -64.f),
				SD::Vector2(64.f, 64.f),
				SD::Vector2(-64.f, 64.f)
			}));

			physComp->SetBeginOverlap(SDFUNCTION_2PARAM(this, PhysicsMap, HandleBeginCollisionChannelOverlap, void, SD::PhysicsComponent*, SD::PhysicsComponent*));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (newObj->AddComponent(visualizer))
		{
			visualizer->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	});

	collisionChannels = (PhysicsTester::CC_Environment | PhysicsTester::CC_RedChannel);
	blockingChannels = (PhysicsTester::CC_RedChannel);
	SD::Vector3 position;
	for (int rowNum = 0; rowNum < 2; ++rowNum)
	{
		position = roomCenter;
		position.X -= (dimensions.X * 0.45f);
		SD::Float multiplier = (rowNum == 1) ? 1.f : -1.f;
		position.Y += (dimensions.Y * 0.4f * multiplier);

		if (rowNum == 1)
		{
			collisionChannels = (PhysicsTester::CC_Environment | PhysicsTester::CC_BlueChannel);
			blockingChannels = (PhysicsTester::CC_BlueChannel);
		}

		for (int colNum = 0; colNum < 19; ++colNum)
		{
			createSphere(position);
			position.X += (dimensions.X * 0.05f);
		}
	}

	//Create the channel changers
	position = roomCenter;
	position.X -= (dimensions.X * 0.25f);
	createChannelChanger(position, PhysicsTester::CC_RedChannel);

	position.X += (dimensions.X * 0.5f);
	createChannelChanger(position, PhysicsTester::CC_BlueChannel);

	//Create an object that will block the spheres from escaping the room.
	SD::SceneEntity* exitBlocker = SD::SceneEntity::CreateObject();
	position = roomCenter;
	position.X += (dimensions.X * -0.5f);
	position.X -= 16.f;
	exitBlocker->SetTranslation(position);

	SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
	if (exitBlocker->AddComponent(physComp))
	{
		physComp->SetCollisionChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_BlueChannel);
		physComp->SetBlockingChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_BlueChannel);
		physComp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
		{
			SD::Vector2(16.f, -200.f),
			SD::Vector2(16.f, 200.f),
			SD::Vector2(-16.f, 200.f),
			SD::Vector2(-16.f, -200.f)
		}));
	}

	PhysicsVisualizer* comp = PhysicsVisualizer::CreateObject();
	if (exitBlocker->AddComponent(comp))
	{
		comp->bUpdateOnceOnly = true;
		Tester->GetSceneLayer()->RegisterSingleComponent(comp);
	}
}

void PhysicsMap::CreateRayTraceRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	//Create an object that will block the shapes from escaping the room.
	{
		SD::SceneEntity* exitBlocker = SD::SceneEntity::CreateObject();
		SD::Vector3 position(roomCenter);
		position.X += (dimensions.X * 0.5f);
		position.X += 16.f;
		exitBlocker->SetTranslation(position);

		SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
		if (exitBlocker->AddComponent(physComp))
		{
			physComp->SetCollisionChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			physComp->SetBlockingChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			physComp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(16.f, -200.f),
				SD::Vector2(16.f, 200.f),
				SD::Vector2(-16.f, 200.f),
				SD::Vector2(-16.f, -200.f)
			}));
		}

		PhysicsVisualizer* comp = PhysicsVisualizer::CreateObject();
		if (exitBlocker->AddComponent(comp))
		{
			comp->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(comp);
		}
	}

	//Create objects that adds/removes the RayTracer component.
	CreatePhysActionObject(roomCenter, dimensions, RayTracer::SStaticClass());

	//Create spheres that move around
	AddBouncingSpheres(roomCenter, dimensions, 10);

	//Create concave shape that moves around.
	{
		BouncingShape* shape = BouncingShape::CreateObject();
		shape->SetTranslation(roomCenter);
		MapObjects.push_back(shape);

		if (SD::PhysicsComponent* phys = shape->GetPhysComp())
		{
			SD::Int channels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			phys->SetCollisionChannels(PhysicsTester::CC_Environment | channels);
			phys->SetBlockingChannels(channels);
			/*
			   A------B
			  /  E---D\\
			 M  /       C
			 | F   P
			 L  \       I
			  \  G---H//
			   K------J
			*/
			phys->AddShape(new SD::CollisionExtrudePolygon(1.5f, SD::Vector3::ZERO_VECTOR, 10.f,
			{
				SD::Vector2(-100.f, -200.f), //A
				SD::Vector2(100.f, -200.f), //B
				SD::Vector2(125.f, -50.f), //C
				SD::Vector2(87.5f, -100.f), //D
				SD::Vector2(-87.5f, -75.f), //E
				SD::Vector2(-100.f, 0.f), //F
				SD::Vector2(-87.5f, 75.f), //G
				SD::Vector2(87.5f, 100.f), //H
				SD::Vector2(125.f, 50.f), //I
				SD::Vector2(100.f, 200.f), //J
				SD::Vector2(-100.f, 200.f), //K
				SD::Vector2(-125.f, 100.f), //L
				SD::Vector2(-125.f, -100.f) //M
			}));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (shape->AddComponent(visualizer))
		{
			visualizer->SetShapeUpdateFrequency(1.f);
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	}
}

void PhysicsMap::CreateSweepRoom (const SD::Vector3& roomCenter, const SD::Vector3& dimensions)
{
	//Create an object that will block the shapes from escaping the room.
	{
		SD::SceneEntity* exitBlocker = SD::SceneEntity::CreateObject();
		SD::Vector3 position(roomCenter);
		position.X -= (dimensions.X * 0.5f);
		position.X -= 16.f;
		exitBlocker->SetTranslation(position);

		SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateObject();
		if (exitBlocker->AddComponent(physComp))
		{
			physComp->SetCollisionChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			physComp->SetBlockingChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			physComp->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(16.f, -200.f),
				SD::Vector2(16.f, 200.f),
				SD::Vector2(-16.f, 200.f),
				SD::Vector2(-16.f, -200.f)
			}));
		}

		PhysicsVisualizer* comp = PhysicsVisualizer::CreateObject();
		if (exitBlocker->AddComponent(comp))
		{
			comp->bUpdateOnceOnly = true;
			Tester->GetSceneLayer()->RegisterSingleComponent(comp);
		}
	}

	//Create objects that adds/removes the SweepTester component.
	CreatePhysActionObject(roomCenter, dimensions, SweepTester::SStaticClass());
	
	//Create spheres that move around
	AddBouncingSpheres(roomCenter, dimensions, 24);

	//Create a L shape
	{
		BouncingShape* testObj = BouncingShape::CreateObject();
		testObj->SetTranslation(roomCenter);
		MapObjects.push_back(testObj);

		if (SD::PhysicsComponent* phys = testObj->GetPhysComp())
		{
			phys->SetCollisionChannels(PhysicsTester::CC_Environment | PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			phys->SetBlockingChannels(PhysicsTester::CC_RedChannel | PhysicsTester::CC_GreenChannel | PhysicsTester::CC_BlueChannel);
			
			/*
			A
			|\
			| \
			|  \
			|   B---
			| P      ---
			D------------C
			*/
			phys->AddShape(new SD::CollisionExtrudePolygon(1.f, SD::Vector3::ZERO_VECTOR, 100.f,
			{
				SD::Vector2(-64.f, -400.f),
				SD::Vector2(64.f, -64.f),
				SD::Vector2(400.f, 64.f),
				SD::Vector2(-32.f, 32.f)
			}));
		}

		PhysicsVisualizer* visualizer = PhysicsVisualizer::CreateObject();
		if (testObj->AddComponent(visualizer))
		{
			visualizer->SetShapeUpdateFrequency(1.f);
			Tester->GetSceneLayer()->RegisterSingleComponent(visualizer);
		}
	}
}

void PhysicsMap::HandleBeginCollisionChannelOverlap (SD::PhysicsComponent* delegateOwner, SD::PhysicsComponent* otherComp)
{
	//Toggle the bit corresponding to the delegate owner's collision channel.
	otherComp->SetCollisionChannels(otherComp->GetCollisionChannels() ^ delegateOwner->GetCollisionChannels());
	otherComp->SetBlockingChannels(otherComp->GetBlockingChannels() ^ delegateOwner->GetCollisionChannels());
}

void PhysicsMap::HandleTickMoveObjectVertically (SD::Float deltaSec)
{
	for (SD::PhysicsComponent* comp : VertMovingComps)
	{
		SD::Engine* localEngine = SD::Engine::FindEngine();
		CHECK(localEngine != nullptr)

		//Every ten seconds, it should alternate between moving down and moving up.
		SD::Float time = localEngine->GetElapsedTime();
		SD::Vector3 newVelocity(0.f, 256.f, 0.f);
		newVelocity.Y *= std::sin(((time * 2.f * PI_FLOAT) / VerticalChangeDirectionFrequency).Value);

		comp->SetVelocity(newVelocity);
	}
}
SD_TESTER_END
#endif