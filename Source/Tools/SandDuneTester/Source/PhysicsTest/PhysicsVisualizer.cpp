/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsVisualizer.cpp
=====================================================================
*/

#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"

using namespace SD; //For the global operator (sf::Vector2f * sf::Vector2f)

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::PhysicsVisualizer, SD::RenderComponent)
SD_TESTER_BEGIN

const SD::Color PhysicsVisualizer::AABB_COLOR(64, 64, 64, 48);
const bool PhysicsVisualizer::bDEBUG_AABB = true;

void PhysicsVisualizer::InitProps ()
{
	Super::InitProps();

	bUpdateOnceOnly = false;

	ShapeUpdateFrequency = 1.f;
	Tick = nullptr;

	FlashStartTime = -1.f;
	FlashDuration = 1.f;

	FlashFrom = sf::Color::White;
	FlashTo = sf::Color::White;

	FlashTick = nullptr;

	bColorOverride = false;
}

void PhysicsVisualizer::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsVisualizer, HandleTick, void, SD::Float));
		Tick->SetTickInterval(ShapeUpdateFrequency);
		Tick->SetTicking(false);
	}
}

void PhysicsVisualizer::Render (SD::RenderTarget* renderTarget, const SD::Camera* camera)
{
	SD::Transformation::SScreenProjectionData projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	for (SRenderShape& renderShape : RenderShapes)
	{
		CHECK(renderShape.Shape != nullptr)
		renderShape.Shape->setPosition(projectionData.Position + (SD::Vector2::SDtoSFML(renderShape.ShapeOffset) * projectionData.GetFinalSize()));
		renderShape.Shape->setScale(projectionData.GetFinalSize());
		renderTarget->Draw(this, camera, *renderShape.Shape);
	}
}

SD::Aabb PhysicsVisualizer::GetAabb () const
{
	SD::Aabb result;
	for (const SRenderShape& renderShape : RenderShapes)
	{
		sf::FloatRect rect = renderShape.Shape->getGlobalBounds();

		//Ugh this feels wrong to assume top down projection.
		SD::Aabb sdRect(rect.height, 1.f, rect.width, SD::Vector3(rect.left + (rect.width * 0.5f), rect.top + (rect.height * 0.5f), 0.f));
		sdRect.Center += renderShape.ShapeOffset.ToVector3();
		result.CombineAabb(sdRect);
	}

	return result;
}

void PhysicsVisualizer::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	if (Tick != nullptr)
	{
		Tick->SetTicking(true);
	}

	UpdateShapes();
}

void PhysicsVisualizer::ComponentDetached ()
{
	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}

	ClearShapes();

	Super::ComponentDetached();
}

void PhysicsVisualizer::Destroyed ()
{
	ClearShapes();

	Super::Destroyed();
}

void PhysicsVisualizer::OverrideColor (SD::Color newColor)
{
	bColorOverride = true;
	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}

	for (SRenderShape& renderShape : RenderShapes)
	{
		renderShape.Shape->setFillColor(newColor.Source);
	}
}

void PhysicsVisualizer::FlashColor (SD::Float flashDuration)
{
	FlashDuration = flashDuration;
	bool bAlreadyFlashing = (FlashStartTime >= 0.f);

	SD::Engine* localEngine = SD::Engine::FindEngine();
	FlashStartTime = localEngine->GetElapsedTime();

	if (bAlreadyFlashing)
	{
		return;
	}
	
	CHECK(FlashTick == nullptr)
	
	if (bColorOverride && RenderShapes.size() > 0)
	{
		FlashTo = RenderShapes.at(0).Shape->getFillColor();
	}
	else
	{
		for (SD::ComponentIterator iter(GetOwner(), false); iter.GetSelectedComponent() != nullptr; ++iter)
		{
			if (SD::PhysicsComponent* physComp = dynamic_cast<SD::PhysicsComponent*>(iter.GetSelectedComponent()))
			{
				FlashTo = GetColor(physComp->GetCollisionChannels());
				break;
			}
		}
	}

	Int totalColor = FlashTo.r;
	totalColor += FlashTo.g;
	totalColor += FlashTo.b;

	//If the color is bright enough, flash from black instead of white.
	if (totalColor > 650)
	{
		FlashFrom = sf::Color::Black;
	}
	else
	{
		FlashFrom = sf::Color::White;
	}

	FlashTick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(FlashTick))
	{
		FlashTick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsVisualizer, HandleFlashTick, void, SD::Float));
	}
}

void PhysicsVisualizer::SetShapeUpdateFrequency (SD::Float newShapeUpdateFrequency)
{
	ShapeUpdateFrequency = newShapeUpdateFrequency;
	if (Tick != nullptr)
	{
		Tick->SetTickInterval(ShapeUpdateFrequency);
	}
}

void PhysicsVisualizer::ClearShapes ()
{
	for (SRenderShape& renderShape : RenderShapes)
	{
		if (renderShape.Shape != nullptr)
		{
			delete renderShape.Shape;
			renderShape.Shape = nullptr;
		}
	}
	SD::ContainerUtils::Empty(OUT RenderShapes);
}

void PhysicsVisualizer::UpdateShapes ()
{
	if (!IsVisible())
	{
		return;
	}

	ClearShapes();

	if (GetOwner() == nullptr)
	{
		return;
	}

	for (SD::ComponentIterator iter(GetOwner(), false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (SD::PhysicsComponent* physComp = dynamic_cast<SD::PhysicsComponent*>(iter.GetSelectedComponent()))
		{
			const std::vector<SD::PhysicsComponent::SCollisionShape>& compShapes = physComp->ReadCollisionShapes();
			for (const SD::PhysicsComponent::SCollisionShape& compShape : compShapes)
			{
				switch (compShape.Shape->GetShape())
				{
					case(SD::CollisionShape::ST_Segment):
					{
						if (SD::CollisionSegment* segment = dynamic_cast<SD::CollisionSegment*>(compShape.Shape))
						{
							SRenderShape newRenderShape;
							sf::RectangleShape* rect = new sf::RectangleShape();
							ConnectRectToVerts(OUT *rect, segment->ReadPosition().ToVector2(), segment->GetAbsEndPoint().ToVector2());
							newRenderShape.Shape = rect;

							newRenderShape.ShapeOffset = compShape.ShapeDisplacement.ToVector2();
							newRenderShape.bIsAabb = false;
							RenderShapes.push_back(newRenderShape);
						}

						break;
					}

					case(SD::CollisionShape::ST_Sphere):
					{
						if (SD::CollisionSphere* sphere = dynamic_cast<SD::CollisionSphere*>(compShape.Shape))
						{
							SRenderShape newRenderShape;
							newRenderShape.Shape = new sf::CircleShape((sphere->GetRadius() * sphere->GetScale()).Value);
							newRenderShape.ShapeOffset = compShape.ShapeDisplacement.ToVector2() - (SD::Vector2(1.f, 1.f) * (sphere->GetRadius() * sphere->GetScale()));
							newRenderShape.bIsAabb = false;
							RenderShapes.push_back(newRenderShape);
						}

						break;
					}
					case(SD::CollisionShape::ST_Capsule):
					{
						if (SD::CollisionCapsule* capsule = dynamic_cast<SD::CollisionCapsule*>(compShape.Shape))
						{
							SRenderShape newRenderShape;
							newRenderShape.Shape = new sf::CircleShape((capsule->GetRadius() * capsule->GetScale()).Value);
							newRenderShape.ShapeOffset = compShape.ShapeDisplacement.ToVector2() - (SD::Vector2(1.f, 1.f) * (capsule->GetRadius() * capsule->GetScale()));
							newRenderShape.bIsAabb = false;
							RenderShapes.push_back(newRenderShape);
						}

						break;
					}
					case(SD::CollisionShape::ST_ExtrudePolygon):
					{
						if (SD::CollisionExtrudePolygon* polygon = dynamic_cast<SD::CollisionExtrudePolygon*>(compShape.Shape))
						{
							//SFML doesn't support concave polygons. Draw a series of segments instead.
							for (size_t i = 0; i < polygon->ReadVertices().size(); ++i)
							{
								SD::Vector2 nextVertex = (i < polygon->ReadVertices().size() - 1) ? polygon->ReadVertices().at(i+1) : polygon->ReadVertices().at(0);

								SRenderShape newRenderShape;
								sf::RectangleShape* rect = new sf::RectangleShape();
								ConnectRectToVerts(OUT *rect, polygon->ReadVertices().at(i) * polygon->GetScale(), nextVertex * polygon->GetScale());
								newRenderShape.Shape = rect;

								newRenderShape.ShapeOffset = SD::Vector2(compShape.ShapeDisplacement.ToVector2() + (polygon->ReadVertices().at(i) * polygon->GetScale()));
								newRenderShape.bIsAabb = false;
								RenderShapes.push_back(newRenderShape);
							}
						}

						break;
					}
				}

				if (bDEBUG_AABB)
				{
					const SD::Aabb& box = compShape.Shape->ReadBoundingBox();
					SRenderShape newRenderShape;
					newRenderShape.Shape = new sf::RectangleShape(sf::Vector2f(box.Depth.Value, box.Width.Value));
					newRenderShape.Shape->setFillColor(AABB_COLOR.Source);

					//Center the aabb over the Position
					newRenderShape.ShapeOffset = SD::Vector2(box.Depth, box.Width) * -0.5f;

					//Displace the Aabb based on the shape's relative position to the Aabb.
					newRenderShape.ShapeOffset += (box.Center - (compShape.Shape->ReadPosition() - compShape.ShapeDisplacement)).ToVector2();
					newRenderShape.bIsAabb = true;
					RenderShapes.push_back(newRenderShape);
				}
			}

			sf::Color shapeColor = GetColor(physComp->GetCollisionChannels());
			for (SRenderShape& renderShape : RenderShapes)
			{
				if (!renderShape.bIsAabb)
				{
					renderShape.Shape->setFillColor(shapeColor);
				}
			}
		}
	}

	if (bUpdateOnceOnly && Tick != nullptr)
	{
		Tick->SetTicking(false);
	}
}

void PhysicsVisualizer::ConnectRectToVerts (sf::RectangleShape& outRect, const SD::Vector2& ptA, const SD::Vector2& ptB) const
{
	SD::Float length = (ptA - ptB).VSize();
	outRect.setSize(sf::Vector2f(length.Value, 8.f));
	SD::Rotator rot((ptB - ptA).ToVector3());
	outRect.setRotation(-rot.GetYaw(SD::Rotator::RU_Degrees).Value);
}

sf::Color PhysicsVisualizer::GetColor (SD::Int collisionChannel)
{
	sf::Color result(0, 0, 0, 225);
	const sf::Uint8 colorIntensity = 200;
	if ((collisionChannel & PhysicsTester::CC_RedChannel) > 0)
	{
		result.r = colorIntensity;
	}

	if ((collisionChannel & PhysicsTester::CC_GreenChannel) > 0)
	{
		result.g = colorIntensity;
	}

	if ((collisionChannel & PhysicsTester::CC_BlueChannel) > 0)
	{
		result.b = colorIntensity;
	}

	return result;
}

void PhysicsVisualizer::HandleTick (SD::Float deltaSec)
{
	if (FlashStartTime < 0.f)
	{
		UpdateShapes();
	}
}

void PhysicsVisualizer::HandleFlashTick (SD::Float deltaSec)
{
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	SD::Float elapsedTime = localEngine->GetElapsedTime() - FlashStartTime;
	sf::Color newColor;
	if (elapsedTime >= FlashDuration)
	{
		newColor = FlashTo;
		FlashTick->Destroy();
		FlashTick = nullptr;
		FlashStartTime = -1.f;
	}
	else
	{
		SD::Float ratio = elapsedTime / FlashDuration;
		newColor = SD::Color::Lerp(ratio, FlashFrom, FlashTo).Source;
	}

	//Update colors
	for (SRenderShape& renderShape : RenderShapes)
	{
		if (!renderShape.bIsAabb)
		{
			renderShape.Shape->setFillColor(newColor);
		}
	}
}
SD_TESTER_END
#endif