/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PhysicsPlayer.cpp
=====================================================================
*/

#include "PhysicsPlayer.h"
#include "PhysicsTester.h"
#include "PhysicsVisualizer.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::PhysicsPlayer, SD::SceneEntity)
SD_TESTER_BEGIN

void PhysicsPlayer::InitProps ()
{
	Super::InitProps();

	MovementAcceleration = 1000.f; //10 meters per second per second
	MaxVelocity = 1000.f; //10 meters per second

	Tick = nullptr;

	bUpPressed = false;
	bRightPressed = false;
	bDownPressed = false;
	bLeftPressed = false;
}

void PhysicsPlayer::BeginObject ()
{
	Super::BeginObject();

	Physics = SD::PhysicsComponent::CreateObject();
	if (AddComponent(Physics))
	{
		Physics->SetCollisionChannels(PhysicsTester::CC_Character);
		Physics->SetBlockingChannels(PhysicsTester::CC_Environment | PhysicsTester::CC_Character);

		SD::CollisionSphere* sphere = new SD::CollisionSphere(1.f, SD::Vector3::ZERO_VECTOR, 50.f); //1 meter diameter
		Physics->AddShape(sphere);
	}

	Input = SD::InputComponent::CreateObject();
	if (AddComponent(Input))
	{
		Input->OnInput = SDFUNCTION_1PARAM(this, PhysicsPlayer, HandleInput, bool, const sf::Event&);
	}

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, PhysicsPlayer, HandleTick, void, SD::Float));
	}

	Visualizer = PhysicsVisualizer::CreateObject();
	if (AddComponent(Visualizer))
	{
		Visualizer->SetShapeUpdateFrequency(0.2f);
	}
}

bool PhysicsPlayer::HandleInput (const sf::Event& evnt)
{
	bool bConsumeEvent = false;
	if (evnt.type == sf::Event::KeyPressed || evnt.type == sf::Event::KeyReleased)
	{
		switch (evnt.key.code)
		{
			case(sf::Keyboard::W):
			case(sf::Keyboard::Up):
				bUpPressed = (evnt.type == sf::Event::KeyPressed);
				bConsumeEvent = true;
				break;

			case(sf::Keyboard::D):
			case(sf::Keyboard::Right):
				bRightPressed = (evnt.type == sf::Event::KeyPressed);
				bConsumeEvent = true;
				break;

			case(sf::Keyboard::S):
			case(sf::Keyboard::Down):
				bDownPressed = (evnt.type == sf::Event::KeyPressed);
				bConsumeEvent = true;
				break;

			case(sf::Keyboard::A):
			case(sf::Keyboard::Left):
				bLeftPressed = (evnt.type == sf::Event::KeyPressed);
				bConsumeEvent = true;
				break;
		}
	}

	if (bConsumeEvent && Physics.IsValid())
	{
		SD::Vector3 newAcceleration(SD::Vector3::ZERO_VECTOR);
		if (bUpPressed)
		{
			newAcceleration.Y = -1.f;
		}

		if (bRightPressed)
		{
			newAcceleration.X = 1.f;
		}

		if (bDownPressed)
		{
			newAcceleration.Y += 1.f;
		}

		if (bLeftPressed)
		{
			newAcceleration.X += -1.f;
		}

		newAcceleration *= MovementAcceleration;
		Physics->SetAcceleration(newAcceleration);
	}

	return bConsumeEvent;
}

void PhysicsPlayer::HandleTick (SD::Float deltaSec)
{
	//Check if the player needs to slow down.
	if (Physics.IsValid() && !bUpPressed && !bRightPressed && !bDownPressed && !bLeftPressed)
	{
		if (!Physics->ReadVelocity().IsNearlyEqual(SD::Vector3::ZERO_VECTOR, 1.f))
		{
			Physics->SetAcceleration(SD::Vector3::Normalize(Physics->ReadVelocity()) * -MovementAcceleration);
		}
		else
		{
			//Ensure the negated acceleration will not steer the other player in the other direction. Stop moving.
			Physics->SetVelocity(SD::Vector3::ZERO_VECTOR);
			Physics->SetAcceleration(SD::Vector3::ZERO_VECTOR);
		}
	}

	//Clamp velocity
	if (Physics.IsValid() && Physics->ReadVelocity().CalcDistSquared() > std::powf(MaxVelocity.Value, 2.f))
	{
		SD::Vector3 newVelocity = Physics->GetVelocity();
		newVelocity.SetLengthTo(MaxVelocity);
		Physics->SetVelocity(newVelocity);
	}
}

SD_TESTER_END
#endif