/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SandDuneTesterEngineComponent.cpp
=====================================================================
*/

#include "SandDuneTesterEngineComponent.h"
#include "SandDuneTesterTheme.h"

IMPLEMENT_ENGINE_COMPONENT(SDTester::SandDuneTesterEngineComponent)
SD_TESTER_BEGIN

bool SandDuneTesterEngineComponent::CanRunPreInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const
{
	if (!Super::CanRunPreInitializeComponent(executedEngineComponents))
	{
		return false;
	}

	for (size_t i = 0; i < executedEngineComponents.size(); ++i)
	{
		if (executedEngineComponents.at(i) == SD::GuiEngineComponent::SStaticClass())
		{
			return true;
		}
	}

	return false;
}

bool SandDuneTesterEngineComponent::CanRunInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const
{
	if (!Super::CanRunInitializeComponent(executedEngineComponents))
	{
		return false;
	}

	for (size_t i = 0; i < executedEngineComponents.size(); ++i)
	{
		if (executedEngineComponents.at(i) == SD::GuiEngineComponent::SStaticClass())
		{
			return true;
		}
	}

	return false;
}

void SandDuneTesterEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(SandDuneTesterTheme::SStaticClass());
}

void SandDuneTesterEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_SD_TESTER, TICK_GROUP_PRIORITY_SD_TESTER);

	ImportAssets();
}

void SandDuneTesterEngineComponent::ImportAssets ()
{
	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->ImportTexture(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("DesertWet.txtr")));
	localTexturePool->ImportTexture(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("GreenGrass.txtr")));
	localTexturePool->ImportTexture(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("TopDownFerns.txtr")));
	localTexturePool->ImportTexture(SD::FileAttributes(SD::Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("SandDuneTester"), TXT("Tile4.txtr")));
}

SD_TESTER_END