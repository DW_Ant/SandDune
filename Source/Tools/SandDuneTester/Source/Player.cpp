/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  Player.cpp
=====================================================================
*/

#include "Player.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::Player, SD::Entity)
SD_TESTER_BEGIN

void Player::InitProps ()
{
	Super::InitProps();

	VelocityInput = SD::Vector3::ZERO_VECTOR;
	GrowthInput = SD::Vector3::ZERO_VECTOR;

	RenderComp = nullptr;
	InputComp = nullptr;
	Tick = nullptr;

	MoveSpeed = 500.f; //5 meters a second
	GrowSpeed = 0.5f;

	InputLeft = false;
	InputUp = false;
	InputRight = false;
	InputDown = false;
	InputGrow = false;
	InputShrink = false;
}

void Player::BeginObject ()
{
	Super::BeginObject();

	RenderComp = SD::ColorRenderComponent::CreateObject();
	if (AddComponent(RenderComp))
	{
		RenderComp->SolidColor = SD::Color(255, 128, 64);
		RenderComp->SetShape(SD::ColorRenderComponent::ST_Circle);
		RenderComp->SetBaseSize(100.f, 100.f); //1 meter wide. Increase the player size without affecting Entity positions relative to this Entity.
		RenderComp->SetPivot(0.5f, 0.5f);
	}

	InputComp = SD::InputComponent::CreateObject();
	if (AddComponent(InputComp))
	{
		InputComp->SetInputEnabled(true);
		InputComp->OnInput = SDFUNCTION_1PARAM(this, Player, HandleKeyboardInput, bool, const sf::Event&);
	}

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, Player, HandleTick, void, SD::Float));
	}
}

bool Player::HandleKeyboardInput (const sf::Event& evnt)
{
	if (evnt.type != sf::Event::KeyPressed && evnt.type  != sf::Event::KeyReleased)
	{
		return false;
	}

	bool newInputFlag = (evnt.type == sf::Event::KeyPressed); //Move on true (pressed). Stop on false (released).
	bool consumeEvent = false;
	switch (evnt.key.code)
	{
		case(sf::Keyboard::Up):
		case(sf::Keyboard::W):
			InputUp = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Right):
		case(sf::Keyboard::D):
			InputRight = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Down):
		case(sf::Keyboard::S):
			InputDown = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Left):
		case(sf::Keyboard::A):
			InputLeft = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Z):
			InputGrow = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::X):
			InputShrink = newInputFlag;
			consumeEvent = true;
			break;
	}

	VelocityInput = SD::Vector3::ZERO_VECTOR;
	if (InputUp)
	{
		VelocityInput.Y -= 1.f;
	}

	if (InputRight)
	{
		VelocityInput.X += 1.f;
	}

	if (InputDown)
	{
		VelocityInput.Y += 1.f;
	}

	if (InputLeft)
	{
		VelocityInput.X -= 1.f;
	}

	GrowthInput = SD::Vector3::ZERO_VECTOR;
	if (InputGrow)
	{
		GrowthInput += SD::Vector3(1.f, 1.f, 1.f);
	}

	if (InputShrink)
	{
		GrowthInput -= SD::Vector3(1.f, 1.f, 1.f);
	}

	return consumeEvent;
}

void Player::HandleTick (SD::Float deltaSec)
{
	if (!VelocityInput.IsEmpty())
	{
		SD::Vector3 newPos = ReadTranslation() + (VelocityInput * deltaSec * MoveSpeed);
		SetTranslation(newPos);
	}

	if (!GrowthInput.IsEmpty())
	{
		SD::Vector3 newSize = ReadScale() + (GrowthInput * deltaSec * GrowSpeed);
		SetScale(newSize);
	}
}
SD_TESTER_END

#endif