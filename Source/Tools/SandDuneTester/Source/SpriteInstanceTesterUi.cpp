/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteInstanceTesterUi.cpp
=====================================================================
*/

#include "SpriteInstanceTesterUi.h"
#include "SpriteInstanceTester.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::SpriteInstanceTesterUi, SD::GuiEntity)
SD_TESTER_BEGIN

void SpriteInstanceTesterUi::InitProps ()
{
	Super::InitProps();

	SettingsTab = nullptr;
	TransformTab = nullptr;
	ViewTab = nullptr;
	SettingsFrame = nullptr;
	TransformFrame = nullptr;
	ViewFrame = nullptr;
	StatsFrame = nullptr;

	NumInstancesField = nullptr;
	CellSizeField = nullptr;
	NumSubDivisionsFieldX = nullptr;
	NumSubDivisionsFieldY = nullptr;
	BrushSizeField = nullptr;

	MinSizeField = nullptr;
	MaxSizeField = nullptr;
	UniformSizeCheckbox = nullptr;
	RandRotationCheckbox = nullptr;

	ShowDensityMapCheckbox = nullptr;
	ShowBrushCheckbox = nullptr;
	ShowStatsCheckbox = nullptr;

	FrameRateLabel = nullptr;
	GenerationTimeLabel = nullptr;
	NumSpritesLabel = nullptr;

	OwningTester = nullptr;
	Tick = nullptr;
}

void SpriteInstanceTesterUi::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick))
	{
		Tick->SetTicking(false);
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleTick, void, SD::Float));
	}
}

void SpriteInstanceTesterUi::ConstructUI ()
{
	Super::ConstructUI();

	const SD::Vector2 tabButtonSize(128.f, 24.f);
	SD::Float tabMargin(6.f);
	SD::Float tabHeight(tabButtonSize.Y + (tabMargin * 2.f));

	const std::function<void(SD::FrameComponent*)> initTextFieldBackground([](SD::FrameComponent* frameComp)
	{
		if (SD::BorderRenderComponent* borderComp = frameComp->GetBorderComp())
		{
			borderComp->Destroy();
		}

		frameComp->SetBorderThickness(1.f);
		frameComp->SetLockedFrame(true);
		frameComp->SetCenterColor(SD::Color(64, 64, 64));
	});

	//Tabs
	SettingsTab = SD::ButtonComponent::CreateObject();
	if (AddComponent(SettingsTab))
	{
		SettingsTab->SetSize(tabButtonSize);
		SettingsTab->SetPosition(tabMargin, tabMargin);
		SettingsTab->SetCaptionText(TXT("Settings"));
		SettingsTab->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleSettingsTabReleased, void, SD::ButtonComponent*));
		HoverCheckComponents.push_back(SettingsTab);
	}

	TransformTab = SD::ButtonComponent::CreateObject();
	if (AddComponent(TransformTab))
	{
		TransformTab->SetSize(tabButtonSize);
		TransformTab->SetPosition((tabMargin * 2.f) + tabButtonSize.X, tabMargin);
		TransformTab->SetCaptionText(TXT("Transformation"));
		TransformTab->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleTransformTabReleased, void, SD::ButtonComponent*));
		HoverCheckComponents.push_back(TransformTab);
	}

	ViewTab = SD::ButtonComponent::CreateObject();
	if (AddComponent(ViewTab))
	{
		ViewTab->SetSize(tabButtonSize);
		ViewTab->SetPosition((tabMargin * 3.f) + (tabButtonSize.X * 2.f), tabMargin);
		ViewTab->SetCaptionText(TXT("View"));
		ViewTab->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleViewTabReleased, void, SD::ButtonComponent*));
		HoverCheckComponents.push_back(ViewTab);
	}

	const SD::Float spacing = 0.025f;
	const SD::Float rowHeight = 0.15f;
	const SD::Float colWidth = 0.45f;
	const SD::Float rowSpacing = 0.02f;
	const SD::Vector2 frameSize(0.35f, 0.2f);

	SettingsFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(SettingsFrame))
	{
		SettingsFrame->SetPosition(tabMargin, tabHeight);
		SettingsFrame->SetSize(frameSize);
		SettingsFrame->SetVisibility(false);
		SettingsFrame->SetLockedFrame(true);
		SettingsFrame->SetBorderThickness(2.f);
		HoverCheckComponents.push_back(SettingsFrame);

		SD::Float rowNum = 0;
		SD::LabelComponent* numInstancesLabel = SD::LabelComponent::CreateObject();
		if (SettingsFrame->AddComponent(numInstancesLabel))
		{
			numInstancesLabel->SetAutoRefresh(false);
			numInstancesLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			numInstancesLabel->SetSize(colWidth, rowHeight);
			numInstancesLabel->SetClampText(true);
			numInstancesLabel->SetWrapText(false);
			numInstancesLabel->SetAutoRefresh(true);
			numInstancesLabel->SetText(TXT("Num Sprites per Cell"));
			numInstancesLabel->SetCharacterSize(12);
		}

		SD::FrameComponent* numInstancesBackground = SD::FrameComponent::CreateObject();
		if (SettingsFrame->AddComponent(numInstancesBackground))
		{
			initTextFieldBackground(numInstancesBackground);
			numInstancesBackground->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			numInstancesBackground->SetSize(colWidth, rowHeight);

			NumInstancesField = SD::TextFieldComponent::CreateObject();
			if (numInstancesBackground->AddComponent(NumInstancesField))
			{
				NumInstancesField->SetPosition(SD::Vector2::ZERO_VECTOR);
				NumInstancesField->SetSize(SD::Vector2(1.f, 1.f));
				NumInstancesField->SetText(TXT("8"));
				NumInstancesField->MaxNumCharacters = 4;
				NumInstancesField->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				NumInstancesField->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyNumInstances, void, SD::TextFieldComponent*);
				NumInstancesField->SetEditable(true);
				NumInstancesField->SetSelectable(true);
			}
		}

		rowNum++;
		SD::LabelComponent* cellSizeLabel = SD::LabelComponent::CreateObject();
		if (SettingsFrame->AddComponent(cellSizeLabel))
		{
			cellSizeLabel->SetAutoRefresh(false);
			cellSizeLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			cellSizeLabel->SetSize(colWidth, rowHeight);
			cellSizeLabel->SetClampText(true);
			cellSizeLabel->SetWrapText(false);
			cellSizeLabel->SetAutoRefresh(true);
			cellSizeLabel->SetText(TXT("Cell Size"));
		}

		SD::FrameComponent* cellSizeBackground = SD::FrameComponent::CreateObject();
		if (SettingsFrame->AddComponent(cellSizeBackground))
		{
			initTextFieldBackground(cellSizeBackground);
			cellSizeBackground->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			cellSizeBackground->SetSize(colWidth, rowHeight);

			CellSizeField = SD::TextFieldComponent::CreateObject();
			if (cellSizeBackground->AddComponent(CellSizeField))
			{
				CellSizeField->SetPosition(SD::Vector2::ZERO_VECTOR);
				CellSizeField->SetSize(SD::Vector2(1.f, 1.f));
				CellSizeField->SetText(TXT("16"));
				CellSizeField->MaxNumCharacters = 4;
				CellSizeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				CellSizeField->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyCellSize, void, SD::TextFieldComponent*);
				CellSizeField->SetEditable(true);
				CellSizeField->SetSelectable(true);
			}
		}

		rowNum++;
		SD::LabelComponent* numSubDivisionsLabelX = SD::LabelComponent::CreateObject();
		if (SettingsFrame->AddComponent(numSubDivisionsLabelX))
		{
			numSubDivisionsLabelX->SetAutoRefresh(false);
			numSubDivisionsLabelX->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			numSubDivisionsLabelX->SetSize(colWidth, rowHeight);
			numSubDivisionsLabelX->SetClampText(true);
			numSubDivisionsLabelX->SetWrapText(false);
			numSubDivisionsLabelX->SetAutoRefresh(true);
			numSubDivisionsLabelX->SetText(TXT("Num X Divisions"));
		}

		SD::FrameComponent* subDivisionsBackgroundX = SD::FrameComponent::CreateObject();
		if (SettingsFrame->AddComponent(subDivisionsBackgroundX))
		{
			initTextFieldBackground(subDivisionsBackgroundX);
			subDivisionsBackgroundX->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			subDivisionsBackgroundX->SetSize(colWidth, rowHeight);

			NumSubDivisionsFieldX = SD::TextFieldComponent::CreateObject();
			if (subDivisionsBackgroundX->AddComponent(NumSubDivisionsFieldX))
			{
				NumSubDivisionsFieldX->SetPosition(SD::Vector2::ZERO_VECTOR);
				NumSubDivisionsFieldX->SetSize(SD::Vector2(1.f, 1.f));
				NumSubDivisionsFieldX->SetText(TXT("1"));
				NumSubDivisionsFieldX->MaxNumCharacters = 3;
				NumSubDivisionsFieldX->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				NumSubDivisionsFieldX->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyNumSubDivisionsX, void, SD::TextFieldComponent*);
				NumSubDivisionsFieldX->SetEditable(true);
				NumSubDivisionsFieldX->SetSelectable(true);
			}
		}

		rowNum++;
		SD::LabelComponent* numSubDivisionsLabelY = SD::LabelComponent::CreateObject();
		if (SettingsFrame->AddComponent(numSubDivisionsLabelY))
		{
			numSubDivisionsLabelY->SetAutoRefresh(false);
			numSubDivisionsLabelY->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			numSubDivisionsLabelY->SetSize(colWidth, rowHeight);
			numSubDivisionsLabelY->SetClampText(true);
			numSubDivisionsLabelY->SetWrapText(false);
			numSubDivisionsLabelY->SetAutoRefresh(true);
			numSubDivisionsLabelY->SetText(TXT("Num Y Divisions"));
		}

		SD::FrameComponent* subDivisionsBackgroundY = SD::FrameComponent::CreateObject();
		if (SettingsFrame->AddComponent(subDivisionsBackgroundY))
		{
			initTextFieldBackground(subDivisionsBackgroundY);
			subDivisionsBackgroundY->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			subDivisionsBackgroundY->SetSize(colWidth, rowHeight);

			NumSubDivisionsFieldY = SD::TextFieldComponent::CreateObject();
			if (subDivisionsBackgroundY->AddComponent(NumSubDivisionsFieldY))
			{
				NumSubDivisionsFieldY->SetPosition(SD::Vector2::ZERO_VECTOR);
				NumSubDivisionsFieldY->SetSize(SD::Vector2(1.f, 1.f));
				NumSubDivisionsFieldY->SetText(TXT("1"));
				NumSubDivisionsFieldY->MaxNumCharacters = 3;
				NumSubDivisionsFieldY->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				NumSubDivisionsFieldY->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyNumSubDivisionsY, void, SD::TextFieldComponent*);
				NumSubDivisionsFieldY->SetEditable(true);
				NumSubDivisionsFieldY->SetSelectable(true);
			}
		}

		rowNum++;
		SD::LabelComponent* brushSizeLabel = SD::LabelComponent::CreateObject();
		if (SettingsFrame->AddComponent(brushSizeLabel))
		{
			brushSizeLabel->SetAutoRefresh(false);
			brushSizeLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			brushSizeLabel->SetSize(colWidth, rowHeight);
			brushSizeLabel->SetClampText(true);
			brushSizeLabel->SetWrapText(false);
			brushSizeLabel->SetAutoRefresh(true);
			brushSizeLabel->SetText(TXT("Brush Size"));
		}

		SD::FrameComponent* brushSizeBackground = SD::FrameComponent::CreateObject();
		if (SettingsFrame->AddComponent(brushSizeBackground))
		{
			initTextFieldBackground(brushSizeBackground);
			brushSizeBackground->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			brushSizeBackground->SetSize(colWidth, rowHeight);

			BrushSizeField = SD::TextFieldComponent::CreateObject();
			if (brushSizeBackground->AddComponent(BrushSizeField))
			{
				BrushSizeField->SetPosition(SD::Vector2::ZERO_VECTOR);
				BrushSizeField->SetSize(SD::Vector2(1.f, 1.f));
				BrushSizeField->SetText(TXT("1"));
				BrushSizeField->MaxNumCharacters = 4;
				BrushSizeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				BrushSizeField->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyBrushSize, void, SD::TextFieldComponent*);
				BrushSizeField->SetEditable(true);
				BrushSizeField->SetSelectable(true);
			}
		}
	}

	TransformFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(TransformFrame))
	{
		TransformFrame->SetPosition(tabMargin, tabHeight);
		TransformFrame->SetSize(frameSize);
		TransformFrame->SetVisibility(false);
		TransformFrame->SetLockedFrame(true);
		TransformFrame->SetBorderThickness(2.f);
		HoverCheckComponents.push_back(TransformFrame);
		SD::Float rowNum = 0.f;

		SD::LabelComponent* minSizeLabel = SD::LabelComponent::CreateObject();
		if (TransformFrame->AddComponent(minSizeLabel))
		{
			minSizeLabel->SetAutoRefresh(false);
			minSizeLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			minSizeLabel->SetSize(colWidth, rowHeight);
			minSizeLabel->SetClampText(true);
			minSizeLabel->SetWrapText(false);
			minSizeLabel->SetAutoRefresh(true);
			minSizeLabel->SetText(TXT("Min Sprite Size"));
		}

		SD::FrameComponent* minSizeBackground = SD::FrameComponent::CreateObject();
		if (TransformFrame->AddComponent(minSizeBackground))
		{
			initTextFieldBackground(minSizeBackground);
			minSizeBackground->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			minSizeBackground->SetSize(colWidth, rowHeight);

			MinSizeField = SD::TextFieldComponent::CreateObject();
			if (minSizeBackground->AddComponent(MinSizeField))
			{
				MinSizeField->SetPosition(SD::Vector2::ZERO_VECTOR);
				MinSizeField->SetSize(SD::Vector2(1.f, 1.f));
				MinSizeField->SetText(TXT("16"));
				MinSizeField->MaxNumCharacters = 4;
				MinSizeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				MinSizeField->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyMinSize, void, SD::TextFieldComponent*);
				MinSizeField->SetEditable(true);
				MinSizeField->SetSelectable(true);
			}
		}

		rowNum++;
		SD::LabelComponent* maxSizeLabel = SD::LabelComponent::CreateObject();
		if (TransformFrame->AddComponent(maxSizeLabel))
		{
			maxSizeLabel->SetAutoRefresh(false);
			maxSizeLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			maxSizeLabel->SetSize(colWidth, rowHeight);
			maxSizeLabel->SetClampText(true);
			maxSizeLabel->SetWrapText(false);
			maxSizeLabel->SetAutoRefresh(true);
			maxSizeLabel->SetText(TXT("Max Sprite Size"));
		}

		SD::FrameComponent* maxSizeBackground = SD::FrameComponent::CreateObject();
		if (TransformFrame->AddComponent(maxSizeBackground))
		{
			initTextFieldBackground(maxSizeBackground);
			maxSizeBackground->SetPosition((spacing*2.f) + colWidth, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			maxSizeBackground->SetSize(colWidth, rowHeight);

			MaxSizeField = SD::TextFieldComponent::CreateObject();
			if (maxSizeBackground->AddComponent(MaxSizeField))
			{
				MaxSizeField->SetPosition(SD::Vector2::ZERO_VECTOR);
				MaxSizeField->SetSize(SD::Vector2(1.f, 1.f));
				MaxSizeField->SetText(TXT("32"));
				MaxSizeField->MaxNumCharacters = 4;
				MaxSizeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleAllowTextInput_NumericOnly, bool, const SD::DString&);
				MaxSizeField->OnReturn = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleApplyMaxSize, void, SD::TextFieldComponent*);
				MaxSizeField->SetEditable(true);
				MaxSizeField->SetSelectable(true);
			}
		}

		rowNum++;
		UniformSizeCheckbox = SD::CheckboxComponent::CreateObject();
		if (TransformFrame->AddComponent(UniformSizeCheckbox))
		{
			UniformSizeCheckbox->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			UniformSizeCheckbox->SetSize((colWidth*2) + rowSpacing, rowHeight);
			UniformSizeCheckbox->SetChecked(true);
			UniformSizeCheckbox->SetCheckboxRightSide(true);
			if (UniformSizeCheckbox->GetCaptionComponent() != nullptr)
			{
				UniformSizeCheckbox->GetCaptionComponent()->SetText(TXT("Uniform Sprite Size"));
			}

			UniformSizeCheckbox->OnChecked = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleUniformSizeChecked, void, SD::CheckboxComponent*);
		}

		rowNum++;
		RandRotationCheckbox = SD::CheckboxComponent::CreateObject();
		if (TransformFrame->AddComponent(RandRotationCheckbox))
		{
			RandRotationCheckbox->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			RandRotationCheckbox->SetSize((colWidth*2) + rowSpacing, rowHeight);
			RandRotationCheckbox->SetChecked(true);
			RandRotationCheckbox->SetCheckboxRightSide(true);
			if (RandRotationCheckbox->GetCaptionComponent() != nullptr)
			{
				RandRotationCheckbox->GetCaptionComponent()->SetText(TXT("Random Rotation"));
			}

			RandRotationCheckbox->OnChecked = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleRandRotationChecked, void, SD::CheckboxComponent*);
		}
	}

	ViewFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(ViewFrame))
	{
		ViewFrame->SetPosition(tabMargin, tabHeight);
		ViewFrame->SetSize(frameSize);
		ViewFrame->SetVisibility(false);
		ViewFrame->SetLockedFrame(true);
		ViewFrame->SetBorderThickness(2.f);
		HoverCheckComponents.push_back(ViewFrame);

		SD::Float rowNum = 0.f;
		ShowDensityMapCheckbox = SD::CheckboxComponent::CreateObject();
		if (ViewFrame->AddComponent(ShowDensityMapCheckbox))
		{
			ShowDensityMapCheckbox->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			ShowDensityMapCheckbox->SetSize((colWidth*2) + rowSpacing, rowHeight);
			ShowDensityMapCheckbox->SetChecked(false);
			ShowDensityMapCheckbox->SetCheckboxRightSide(true);
			if (ShowDensityMapCheckbox->GetCaptionComponent() != nullptr)
			{
				ShowDensityMapCheckbox->GetCaptionComponent()->SetText(TXT("Show Density Map"));
			}

			ShowDensityMapCheckbox->OnChecked = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleDensityMapChecked, void, SD::CheckboxComponent*);
		}

		rowNum++;
		ShowBrushCheckbox = SD::CheckboxComponent::CreateObject();
		if (ViewFrame->AddComponent(ShowBrushCheckbox))
		{
			ShowBrushCheckbox->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			ShowBrushCheckbox->SetSize((colWidth*2) + rowSpacing, rowHeight);
			ShowBrushCheckbox->SetChecked(false);
			ShowBrushCheckbox->SetCheckboxRightSide(true);
			if (ShowBrushCheckbox->GetCaptionComponent() != nullptr)
			{
				ShowBrushCheckbox->GetCaptionComponent()->SetText(TXT("Show Brush"));
			}

			ShowBrushCheckbox->OnChecked = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleShowBrushChecked, void, SD::CheckboxComponent*);
		}

		rowNum++;
		ShowStatsCheckbox = SD::CheckboxComponent::CreateObject();
		if (ViewFrame->AddComponent(ShowStatsCheckbox))
		{
			ShowStatsCheckbox->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			ShowStatsCheckbox->SetSize((colWidth*2) + rowSpacing, rowHeight);
			ShowStatsCheckbox->SetChecked(false);
			ShowStatsCheckbox->SetCheckboxRightSide(true);
			if (ShowStatsCheckbox->GetCaptionComponent() != nullptr)
			{
				ShowStatsCheckbox->GetCaptionComponent()->SetText(TXT("Show Stats"));
			}

			ShowStatsCheckbox->OnChecked = SDFUNCTION_1PARAM(this, SpriteInstanceTesterUi, HandleShowStatsChecked, void, SD::CheckboxComponent*);
		}
	}

	StatsFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(StatsFrame))
	{
		StatsFrame->SetAnchorTop(2.f);
		StatsFrame->SetAnchorRight(2.f);
		StatsFrame->SetSize(0.35f, 0.125f);
		StatsFrame->SetVisibility(false);
		StatsFrame->SetLockedFrame(true);
		StatsFrame->SetBorderThickness(2.f);
		StatsFrame->SetCenterColor(SD::Color(32, 32, 32));
		HoverCheckComponents.push_back(StatsFrame);

		SD::Float rowNum = 0.f;
		FrameRateLabel = SD::LabelComponent::CreateObject();
		if (StatsFrame->AddComponent(FrameRateLabel))
		{
			FrameRateLabel->SetAutoRefresh(false);
			FrameRateLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			FrameRateLabel->SetSize(colWidth * 2.f, rowHeight);
			FrameRateLabel->SetClampText(true);
			FrameRateLabel->SetWrapText(false);
			FrameRateLabel->SetAutoRefresh(true);
		}

		++rowNum;
		GenerationTimeLabel = SD::LabelComponent::CreateObject();
		if (StatsFrame->AddComponent(GenerationTimeLabel))
		{
			GenerationTimeLabel->SetAutoRefresh(false);
			GenerationTimeLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			GenerationTimeLabel->SetSize(colWidth * 2.f, rowHeight);
			GenerationTimeLabel->SetClampText(true);
			GenerationTimeLabel->SetWrapText(false);
			GenerationTimeLabel->SetAutoRefresh(true);
		}

		++rowNum;
		NumSpritesLabel = SD::LabelComponent::CreateObject();
		if (StatsFrame->AddComponent(NumSpritesLabel))
		{
			NumSpritesLabel->SetAutoRefresh(false);
			NumSpritesLabel->SetPosition(spacing, rowSpacing + (rowNum * (rowSpacing+rowHeight)));
			NumSpritesLabel->SetSize(colWidth * 2.f, rowHeight);
			NumSpritesLabel->SetClampText(true);
			NumSpritesLabel->SetWrapText(false);
			NumSpritesLabel->SetAutoRefresh(true);
		}
	}
}

void SpriteInstanceTesterUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(SettingsTab);
	Focus->TabOrder.push_back(TransformTab);
	Focus->TabOrder.push_back(ViewTab);

	Focus->TabOrder.push_back(NumInstancesField);
	Focus->TabOrder.push_back(CellSizeField);
	Focus->TabOrder.push_back(NumSubDivisionsFieldX);
	Focus->TabOrder.push_back(NumSubDivisionsFieldY);
	Focus->TabOrder.push_back(BrushSizeField);

	Focus->TabOrder.push_back(MinSizeField);
	Focus->TabOrder.push_back(MaxSizeField);
	Focus->TabOrder.push_back(UniformSizeCheckbox);
	Focus->TabOrder.push_back(RandRotationCheckbox);

	Focus->TabOrder.push_back(ShowDensityMapCheckbox);
	Focus->TabOrder.push_back(ShowBrushCheckbox);
}

bool SpriteInstanceTesterUi::IsHovered (const SD::Vector2& mousePos) const
{
	for (SD::GuiComponent* comp : HoverCheckComponents)
	{
		if (comp->IsVisible() && comp->IsWithinBounds(mousePos))
		{
			return true;
		}
	}

	return false;
}

void SpriteInstanceTesterUi::RegenerateSpriteInstances ()
{
	if (OwningTester.IsNullptr())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot regenerate sprite instances withut specifying a test owner first."));
		return;
	}

	if (OwningTester->GetSpriteInstance() == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot regenerate sprite instance since a SpriteInstance component is not initialized."));
		return;
	}

	CHECK(NumSubDivisionsFieldX != nullptr && NumSubDivisionsFieldY != nullptr)
	SD::Int numSubDivisionsX(NumSubDivisionsFieldX->GetContent());
	SD::Int numSubDivisionsY(NumSubDivisionsFieldY->GetContent());
	OwningTester->GetSpriteInstance()->SetNumSubDivisions(numSubDivisionsX, numSubDivisionsY);

	CHECK(NumInstancesField != nullptr && CellSizeField != nullptr && 	MinSizeField != nullptr && MaxSizeField != nullptr &&
		UniformSizeCheckbox != nullptr && RandRotationCheckbox != nullptr)
	SD::Int numInstances(NumInstancesField->GetContent());
	SD::Int cellSize(CellSizeField->GetContent());
	SD::Float minSize(MinSizeField->GetContent());
	SD::Float maxSize(MaxSizeField->GetContent());

	SD::Stopwatch stopWatch(TXT("SpriteInstanceGenerateSprites"), false);
	OwningTester->GetSpriteInstance()->PopulateInstances(numInstances, cellSize, OwningTester->GetDensityMapTransform()->ReadSize(), SD::Range<SD::Float>(minSize, maxSize), UniformSizeCheckbox->IsChecked(), RandRotationCheckbox->IsChecked(), OwningTester->GetDensityMap());

	if (GenerationTimeLabel != nullptr)
	{
		GenerationTimeLabel->SetText(SD::DString::CreateFormattedString(TXT("Generation time: %s ms"), stopWatch.GetElapsedTime().ToFormattedString(4, 2, SD::Float::TM_RoundDown)));
	}

	if (NumSpritesLabel != nullptr)
	{
		NumSpritesLabel->SetText(SD::DString::CreateFormattedString(TXT("Number of sprites: %s"), SD::Int(OwningTester->GetSpriteInstance()->ReadSpriteInstances().size())));
	}
}

void SpriteInstanceTesterUi::SetOwningTester (SpriteInstanceTester* newOwningTester)
{
	OwningTester = newOwningTester;

	if (OwningTester.IsValid())
	{
		if (BrushSizeField != nullptr && OwningTester->GetBrushTransform() != nullptr)
		{
			BrushSizeField->SetText(OwningTester->GetBrushTransform()->GetSize().X.ToInt().ToString());
		}

		if (ShowBrushCheckbox != nullptr && OwningTester->GetBrushSprite() != nullptr)
		{
			ShowBrushCheckbox->SetChecked(OwningTester->GetBrushSprite()->IsVisible());
		}

		if (ShowDensityMapCheckbox != nullptr && OwningTester->GetDensityMapSprite() != nullptr)
		{
			ShowDensityMapCheckbox->SetChecked(OwningTester->GetDensityMapSprite()->IsVisible());
		}
	}

	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleTick (SD::Float deltaSec)
{
	if (StatsFrame->IsVisible() && FrameRateLabel != nullptr)
	{
		FrameRateLabel->SetText(SD::DString::CreateFormattedString(TXT("FPS: %s"), (1.f/deltaSec).ToFormattedString(4, 2, SD::Float::TM_RoundDown)));
	}
}

bool SpriteInstanceTesterUi::HandleAllowTextInput_NumericOnly (const SD::DString& txt)
{
	return txt.HasRegexMatch(TXT("[0-9]")); //Only accept numbers (not even decimals or negative values)
}

void SpriteInstanceTesterUi::HandleSettingsTabReleased (SD::ButtonComponent* uiComponent)
{
	CHECK(SettingsFrame != nullptr && TransformFrame != nullptr && ViewFrame != nullptr)

	SettingsFrame->SetVisibility(!SettingsFrame->IsVisible());
	TransformFrame->SetVisibility(false);
	ViewFrame->SetVisibility(false);
}

void SpriteInstanceTesterUi::HandleTransformTabReleased (SD::ButtonComponent* uiComponent)
{
	CHECK(SettingsFrame != nullptr && TransformFrame != nullptr && ViewFrame != nullptr)

	SettingsFrame->SetVisibility(false);
	TransformFrame->SetVisibility(!TransformFrame->IsVisible());
	ViewFrame->SetVisibility(false);
}

void SpriteInstanceTesterUi::HandleViewTabReleased (SD::ButtonComponent* uiComponent)
{
	CHECK(SettingsFrame != nullptr && TransformFrame != nullptr && ViewFrame != nullptr)

	SettingsFrame->SetVisibility(false);
	TransformFrame->SetVisibility(false);
	ViewFrame->SetVisibility(!ViewFrame->IsVisible());
}

void SpriteInstanceTesterUi::HandleApplyNumInstances (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyCellSize (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyDimensionsX (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyDimensionsY (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyNumSubDivisionsX (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyNumSubDivisionsY (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyBrushSize (SD::TextFieldComponent* uiComponent)
{
	if (OwningTester.IsValid() && OwningTester->GetBrushTransform() != nullptr)
	{
		SD::Float newDiameter(uiComponent->GetContent());
		OwningTester->GetBrushTransform()->SetSize(newDiameter, newDiameter);
	}
}

void SpriteInstanceTesterUi::HandleApplyMinSize (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleApplyMaxSize (SD::TextFieldComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleUniformSizeChecked (SD::CheckboxComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleRandRotationChecked (SD::CheckboxComponent* uiComponent)
{
	RegenerateSpriteInstances();
}

void SpriteInstanceTesterUi::HandleDensityMapChecked (SD::CheckboxComponent* uiComponent)
{
	if (OwningTester.IsValid() && OwningTester->GetDensityMapSprite() != nullptr)
	{
		OwningTester->GetDensityMapSprite()->SetVisibility(uiComponent->IsChecked());
	}
}

void SpriteInstanceTesterUi::HandleShowBrushChecked (SD::CheckboxComponent* uiComponent)
{
	if (OwningTester.IsValid() && OwningTester->GetBrushSprite() != nullptr)
	{
		OwningTester->GetBrushSprite()->SetVisibility(uiComponent->IsChecked());
	}
}

void SpriteInstanceTesterUi::HandleShowStatsChecked (SD::CheckboxComponent* uiComponent)
{
	if (StatsFrame != nullptr)
	{
		StatsFrame->SetVisibility(uiComponent->IsChecked());
		if (Tick != nullptr)
		{
			Tick->SetTicking(uiComponent->IsChecked());
		}
	}
}

SD_TESTER_END
#endif