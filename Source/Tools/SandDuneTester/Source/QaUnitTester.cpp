/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  QaUnitTester.cpp
=====================================================================
*/

#include "QaUnitTester.h"
#include "FeatureTestLauncher.h"

#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SDTester::QaUnitTester, SD::UnitTester)
SD_TESTER_BEGIN

SD_THREAD_FUNCTION(LaunchFeatureTester);

bool QaUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_Manual) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		FeatureTestLauncher* launcher = FeatureTestLauncher::CreateObject();
		launcher->InitializeLauncher(this, testFlags, false);
	}

	return true;
}

SD_TESTER_END
#endif