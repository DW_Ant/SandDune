/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteTesterUi.cpp
=====================================================================
*/

#include "SpriteTesterUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::SpriteTesterUi, SD::GuiEntity)
SD_TESTER_BEGIN

void SpriteTesterUi::InitProps ()
{
	Super::InitProps();

	OwningTester = nullptr;

	PreviewFrame = nullptr;
	SpriteFrame = nullptr;
	TestBorderComp = nullptr;
	TestSpriteTransform = nullptr;
	TestSprite = nullptr;
	DrawModeDropdown = nullptr;

	NumHorizontalSegField = nullptr;
	NumVerticalSegField = nullptr;
	HorizontalIdxField = nullptr;
	VerticalIdxField = nullptr;
	ApplySubDivisionButton = nullptr;

	XScaleMultField = nullptr;
	YScaleMultField = nullptr;
	XPosMultField = nullptr;
	YPosMultField = nullptr;
	ApplyDrawCoordMultButton = nullptr;

	UField = nullptr;
	VField = nullptr;
	UlField = nullptr;
	VlField = nullptr;
	ApplyDrawCoordButton = nullptr;

	BorderSetTextureCheckbox = nullptr;
	BorderThicknessField = nullptr;
	BorderColorR = nullptr;
	BorderColorG = nullptr;
	BorderColorB = nullptr;
	BorderTileTextureField = nullptr;
	BorderStretchTextureField = nullptr;
}

void SpriteTesterUi::ConstructUI ()
{
	Super::ConstructUI();

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	const SD::Texture* testTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.64-Grid"));
	const SD::Texture* borderTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.BorderTexture"));
	if (testTexture == nullptr || borderTexture == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to launch Sprite Tester Ui since the texture 'Engine.Graphics.64-Grid' or 'Engine.Graphics.BorderTexture' does not exist."));
		return;
	}

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::DString transFileName = TXT("SandDuneTester");
	SD::DString sectionName = TXT("SpriteTest");

	PreviewFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(PreviewFrame))
	{
		PreviewFrame->SetPosition(SD::Vector2::ZERO_VECTOR);
		PreviewFrame->SetSize(SD::Vector2(0.5f, 1.f));
		PreviewFrame->SetLockedFrame(true);
		PreviewFrame->SetBorderThickness(2.f);
		PreviewFrame->SetCenterColor(SD::Color::BLACK);

		SpriteFrame = SD::FrameComponent::CreateObject();
		if (PreviewFrame->AddComponent(SpriteFrame))
		{
			SpriteFrame->SetPosition(SD::Vector2(0.25f, 0.25f));
			SpriteFrame->SetSize(SD::Vector2(0.5f, 0.5f));
			SpriteFrame->SetBorderThickness(16.f);
			SpriteFrame->SetLockedFrame(false);

			TestBorderComp = SD::BorderRenderComponent::CreateObject();
			if (SpriteFrame->AddComponent(TestBorderComp))
			{
				TestBorderComp->SetBorderTexture(borderTexture);
				TestBorderComp->SetBorderThickness(SpriteFrame->GetBorderThickness());
				TestBorderComp->ApplyDrawModeStretched(SD::Vector2(1.f, 1.f));
			}

			TestSpriteTransform = SD::PlanarTransformComponent::CreateObject();
			if (SpriteFrame->AddComponent(TestSpriteTransform))
			{
				SD::Float anchorDist = (TestBorderComp != nullptr) ? TestBorderComp->GetBorderThickness() : 0.f;
				TestSpriteTransform->SetAnchorTop(anchorDist);
				TestSpriteTransform->SetAnchorRight(anchorDist);
				TestSpriteTransform->SetAnchorBottom(anchorDist);
				TestSpriteTransform->SetAnchorLeft(anchorDist);

				TestSprite = SD::SpriteComponent::CreateObject();
				if (TestSpriteTransform->AddComponent(TestSprite))
				{
					TestSprite->SetSpriteTexture(testTexture);
				}
			}
		}
	}

	SD::ScrollbarComponent* scrollbar = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(scrollbar))
	{
		scrollbar->SetPosition(SD::Vector2(0.f, 0.f));
		scrollbar->SetSize(SD::Vector2(0.5f, 1.f));
		scrollbar->SetAnchorRight(0.f);
		scrollbar->SetHideControlsWhenFull(true);

		SD::GuiEntity* propPanel = SD::GuiEntity::CreateObject();
		propPanel->SetAutoSizeVertical(true);
		propPanel->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
		scrollbar->SetViewedObject(propPanel);

		SD::VerticalList* propList = SD::VerticalList::CreateObject();
		if (propPanel->AddComponent(propList))
		{
			propList->SetPosition(SD::Vector2::ZERO_VECTOR);
			propList->SetSize(SD::Vector2(1.f, 1.f));
			propList->ComponentSpacing = 4.f;

			SD::Vector2 compPos(0.1f, 0.f);
			SD::Vector2 compSize(0.8f, 24.f);

			//SubDivision Section
			{
				SD::FrameComponent* border = SD::FrameComponent::CreateObject();
				if (propList->AddComponent(border))
				{
					border->SetPosition(compPos);
					border->SetSize(compSize * SD::Vector2(1.f, 0.25f));
					border->SetLockedFrame(true);
					border->SetBorderThickness(1.f);
					border->SetCenterColor(SD::Color::WHITE);
				}

				SD::LabelComponent* subDivisionSection = SD::LabelComponent::CreateObject();
				if (propList->AddComponent(subDivisionSection))
				{
					subDivisionSection->SetAutoRefresh(false);
					subDivisionSection->SetPosition(compPos);
					subDivisionSection->SetSize(compSize);
					subDivisionSection->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
					subDivisionSection->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
					subDivisionSection->SetWrapText(false);
					subDivisionSection->SetClampText(false);
					subDivisionSection->SetText(translator->TranslateText(TXT("SubDivisionSection"), transFileName, sectionName));
					subDivisionSection->SetAutoRefresh(true);
				}

				NumHorizontalSegField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(NumHorizontalSegField))
				{
					NumHorizontalSegField->SetPosition(compPos);
					NumHorizontalSegField->SetSize(compSize);
					NumHorizontalSegField->SetDefaultValue(1);
					NumHorizontalSegField->ResetToDefaults();
					NumHorizontalSegField->SetMinValue(1);
					if (NumHorizontalSegField->GetPropertyName() != nullptr)
					{
						NumHorizontalSegField->GetPropertyName()->SetText(translator->TranslateText(TXT("NumHorizontalSegField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (NumHorizontalSegField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("NumHorizontalSegFieldTooltip"), transFileName, sectionName));
					}
				}

				NumVerticalSegField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(NumVerticalSegField))
				{
					NumVerticalSegField->SetPosition(compPos);
					NumVerticalSegField->SetSize(compSize);
					NumVerticalSegField->SetDefaultValue(1);
					NumVerticalSegField->ResetToDefaults();
					NumVerticalSegField->SetMinValue(1);
					if (NumVerticalSegField->GetPropertyName() != nullptr)
					{
						NumVerticalSegField->GetPropertyName()->SetText(translator->TranslateText(TXT("NumVerticalSegField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (NumVerticalSegField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("NumVerticalSegFieldTooltip"), transFileName, sectionName));
					}
				}

				HorizontalIdxField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(HorizontalIdxField))
				{
					HorizontalIdxField->SetPosition(compPos);
					HorizontalIdxField->SetSize(compSize);
					HorizontalIdxField->SetDefaultValue(0);
					HorizontalIdxField->ResetToDefaults();
					HorizontalIdxField->SetMinValue(0);
					if (HorizontalIdxField->GetPropertyName() != nullptr)
					{
						HorizontalIdxField->GetPropertyName()->SetText(translator->TranslateText(TXT("HorizontalIdxField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (HorizontalIdxField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("HorizontalIdxFieldTooltip"), transFileName, sectionName));
					}
				}

				VerticalIdxField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(VerticalIdxField))
				{
					VerticalIdxField->SetPosition(compPos);
					VerticalIdxField->SetSize(compSize);
					VerticalIdxField->SetDefaultValue(0);
					VerticalIdxField->ResetToDefaults();
					VerticalIdxField->SetMinValue(0);
					if (VerticalIdxField->GetPropertyName() != nullptr)
					{
						VerticalIdxField->GetPropertyName()->SetText(translator->TranslateText(TXT("VerticalIdxField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (VerticalIdxField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("VerticalIdxFieldTooltip"), transFileName, sectionName));
					}
				}

				ApplySubDivisionButton = SD::ButtonComponent::CreateObject();
				if (propList->AddComponent(ApplySubDivisionButton))
				{
					ApplySubDivisionButton->SetPosition(compPos);
					ApplySubDivisionButton->SetSize(compSize);
					ApplySubDivisionButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleApplySubDivisionReleased, void, SD::ButtonComponent*));
					ApplySubDivisionButton->SetCaptionText(translator->TranslateText(TXT("ApplySubDivisionButton"), transFileName, sectionName));
				}
			}

			//SetDrawCoordinatesMultipliers section
			{
				SD::FrameComponent* border = SD::FrameComponent::CreateObject();
				if (propList->AddComponent(border))
				{
					border->SetPosition(compPos);
					border->SetSize(compSize * SD::Vector2(1.f, 0.25f));
					border->SetLockedFrame(true);
					border->SetBorderThickness(1.f);
					border->SetCenterColor(SD::Color::WHITE);
				}

				SD::LabelComponent* drawCoordMultSection = SD::LabelComponent::CreateObject();
				if (propList->AddComponent(drawCoordMultSection))
				{
					drawCoordMultSection->SetAutoRefresh(false);
					drawCoordMultSection->SetPosition(compPos);
					drawCoordMultSection->SetSize(compSize);
					drawCoordMultSection->SetWrapText(false);
					drawCoordMultSection->SetClampText(false);
					drawCoordMultSection->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
					drawCoordMultSection->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
					drawCoordMultSection->SetText(translator->TranslateText(TXT("DrawCoordMultSection"), transFileName, sectionName));
					drawCoordMultSection->SetAutoRefresh(true);
				}

				XScaleMultField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(XScaleMultField))
				{
					XScaleMultField->SetPosition(compPos);
					XScaleMultField->SetSize(compSize);
					XScaleMultField->SetDefaultValue(1.f);
					XScaleMultField->ResetToDefaults();
					XScaleMultField->SetMinValue(0.01f);
					XScaleMultField->SetHasMinValue(true);
					if (XScaleMultField->GetPropertyName() != nullptr)
					{
						XScaleMultField->GetPropertyName()->SetText(translator->TranslateText(TXT("XScaleMultField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (XScaleMultField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("XScaleMultFieldTooltip"), transFileName, sectionName));
					}
				}

				YScaleMultField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(YScaleMultField))
				{
					YScaleMultField->SetPosition(compPos);
					YScaleMultField->SetSize(compSize);
					YScaleMultField->SetDefaultValue(1.f);
					YScaleMultField->ResetToDefaults();
					YScaleMultField->SetMinValue(0.01f);
					YScaleMultField->SetHasMinValue(true);
					if (YScaleMultField->GetPropertyName() != nullptr)
					{
						YScaleMultField->GetPropertyName()->SetText(translator->TranslateText(TXT("YScaleMultField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (YScaleMultField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("YScaleMultFieldTooltip"), transFileName, sectionName));
					}
				}

				XPosMultField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(XPosMultField))
				{
					XPosMultField->SetPosition(compPos);
					XPosMultField->SetSize(compSize);
					XPosMultField->SetDefaultValue(0.f);
					XPosMultField->ResetToDefaults();
					if (XPosMultField->GetPropertyName() != nullptr)
					{
						XPosMultField->GetPropertyName()->SetText(translator->TranslateText(TXT("XPosMultField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (XPosMultField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("XPosMultFieldTooltip"), transFileName, sectionName));
					}
				}

				YPosMultField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(YPosMultField))
				{
					YPosMultField->SetPosition(compPos);
					YPosMultField->SetSize(compSize);
					YPosMultField->SetDefaultValue(0.f);
					YPosMultField->ResetToDefaults();
					if (YPosMultField->GetPropertyName() != nullptr)
					{
						YPosMultField->GetPropertyName()->SetText(translator->TranslateText(TXT("YPosMultField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (YPosMultField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("YPosMultFieldTooltip"), transFileName, sectionName));
					}
				}

				ApplyDrawCoordMultButton = SD::ButtonComponent::CreateObject();
				if (propList->AddComponent(ApplyDrawCoordMultButton))
				{
					ApplyDrawCoordMultButton->SetPosition(compPos);
					ApplyDrawCoordMultButton->SetSize(compSize);
					ApplyDrawCoordMultButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleApplyDrawCoordMultReleased, void, SD::ButtonComponent*));
					ApplyDrawCoordMultButton->SetCaptionText(translator->TranslateText(TXT("ApplyDrawCoordMultButton"), transFileName, sectionName));
				}
			}

			//SetDrawCoordinates section
			{
				SD::FrameComponent* border = SD::FrameComponent::CreateObject();
				if (propList->AddComponent(border))
				{
					border->SetPosition(compPos);
					border->SetSize(compSize * SD::Vector2(1.f, 0.25f));
					border->SetLockedFrame(true);
					border->SetBorderThickness(1.f);
					border->SetCenterColor(SD::Color::WHITE);
				}

				SD::LabelComponent* drawCoordSection = SD::LabelComponent::CreateObject();
				if (propList->AddComponent(drawCoordSection))
				{
					drawCoordSection->SetAutoRefresh(false);
					drawCoordSection->SetPosition(compPos);
					drawCoordSection->SetSize(compSize);
					drawCoordSection->SetWrapText(false);
					drawCoordSection->SetClampText(false);
					drawCoordSection->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
					drawCoordSection->SetVerticalAlignment(SD::LabelComponent::VA_Bottom);
					drawCoordSection->SetText(translator->TranslateText(TXT("DrawCoordSection"), transFileName, sectionName));
					drawCoordSection->SetAutoRefresh(true);
				}

				UField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(UField))
				{
					UField->SetPosition(compPos);
					UField->SetSize(compSize);
					UField->SetDefaultValue(0);
					UField->ResetToDefaults();
					if (UField->GetPropertyName() != nullptr)
					{
						UField->GetPropertyName()->SetText(translator->TranslateText(TXT("UField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (UField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("UFieldTooltip"), transFileName, sectionName));
					}
				}

				VField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(VField))
				{
					VField->SetPosition(compPos);
					VField->SetSize(compSize);
					VField->SetDefaultValue(0);
					VField->ResetToDefaults();
					if (VField->GetPropertyName() != nullptr)
					{
						VField->GetPropertyName()->SetText(translator->TranslateText(TXT("VField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (VField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("VFieldTooltip"), transFileName, sectionName));
					}
				}

				UlField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(UlField))
				{
					UlField->SetPosition(compPos);
					UlField->SetSize(compSize);
					UlField->SetDefaultValue(testTexture->GetWidth());
					UlField->ResetToDefaults();
					UlField->SetMinValue(0);
					if (UlField->GetPropertyName() != nullptr)
					{
						UlField->GetPropertyName()->SetText(translator->TranslateText(TXT("UlField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (UlField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("UlFieldTooltip"), transFileName, sectionName));
					}
				}

				VlField = SD::EditableInt::CreateObject();
				if (propList->AddComponent(VlField))
				{
					VlField->SetPosition(compPos);
					VlField->SetSize(compSize);
					VlField->SetDefaultValue(testTexture->GetHeight());
					VlField->ResetToDefaults();
					VlField->SetMinValue(0);
					if (VlField->GetPropertyName() != nullptr)
					{
						VlField->GetPropertyName()->SetText(translator->TranslateText(TXT("VlField"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (VlField->GetPropertyName()->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("VlFieldTooltip"), transFileName, sectionName));
					}
				}

				ApplyDrawCoordButton = SD::ButtonComponent::CreateObject();
				if (propList->AddComponent(ApplyDrawCoordButton))
				{
					ApplyDrawCoordButton->SetPosition(compPos);
					ApplyDrawCoordButton->SetSize(compSize);
					ApplyDrawCoordButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleApplyDrawCoordReleased, void, SD::ButtonComponent*));
					ApplyDrawCoordButton->SetCaptionText(translator->TranslateText(TXT("ApplyDrawCoordButton"), transFileName, sectionName));
				}
			}

			//BorderRenderComponent options
			{
				SD::FrameComponent* border = SD::FrameComponent::CreateObject();
				if (propList->AddComponent(border))
				{
					border->SetPosition(compPos);
					border->SetSize(compSize * SD::Vector2(1.f, 0.25f));
					border->SetLockedFrame(true);
					border->SetBorderThickness(1.f);
					border->SetCenterColor(SD::Color::WHITE);
				}

				BorderSetTextureCheckbox = SD::EditableBool::CreateObject();
				if (propList->AddComponent(BorderSetTextureCheckbox))
				{
					BorderSetTextureCheckbox->SetPosition(compPos);
					BorderSetTextureCheckbox->SetSize(compSize);
					BorderSetTextureCheckbox->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderTexture, void, SD::EditPropertyComponent*);

					if (SD::CheckboxComponent* checkbox = BorderSetTextureCheckbox->GetCheckbox())
					{
						if (TestBorderComp != nullptr)
						{
							checkbox->SetChecked(TestBorderComp->GetBorderTexture() != nullptr);
						}

						if (SD::LabelComponent* caption = checkbox->GetCaptionComponent())
						{
							caption->SetText(translator->TranslateText(TXT("BorderSetTextureCheckbox"), transFileName, sectionName));
						}
					}
				}

				BorderThicknessField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(BorderThicknessField))
				{
					BorderThicknessField->SetPosition(compPos);
					BorderThicknessField->SetSize(compSize);
					BorderThicknessField->SetMinValue(0.f);
					BorderThicknessField->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderThickness, void, SD::EditPropertyComponent*);

					if (TestBorderComp != nullptr)
					{
						BorderThicknessField->SetPropValue(TestBorderComp->GetBorderThickness().ToFormattedString(1, 2, SD::Float::TM_Round), false);
						BorderThicknessField->SetDefaultValue(TestBorderComp->GetBorderThickness());
					}

					if (SD::LabelComponent* caption = BorderThicknessField->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderThickness"), transFileName, sectionName));
					}
				}

				BorderColorR = SD::EditableInt::CreateObject();
				if (propList->AddComponent(BorderColorR))
				{
					BorderColorR->SetPosition(compPos);
					BorderColorR->SetSize(compSize);
					BorderColorR->SetMinValue(0);
					BorderColorR->SetMaxValue(255);
					BorderColorR->SetPropValue(TXT("255"), false);
					BorderColorR->SetDefaultValue(255);
					BorderColorR->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderColor, void, SD::EditPropertyComponent*);

					if (SD::LabelComponent* caption = BorderColorR->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderColorR"), transFileName, sectionName));
					}
				}

				BorderColorG = SD::EditableInt::CreateObject();
				if (propList->AddComponent(BorderColorG))
				{
					BorderColorG->SetPosition(compPos);
					BorderColorG->SetSize(compSize);
					BorderColorG->SetMinValue(0);
					BorderColorG->SetMaxValue(255);
					BorderColorG->SetPropValue(TXT("255"), false);
					BorderColorG->SetDefaultValue(255);
					BorderColorG->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderColor, void, SD::EditPropertyComponent*);

					if (SD::LabelComponent* caption = BorderColorG->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderColorG"), transFileName, sectionName));
					}
				}

				BorderColorB = SD::EditableInt::CreateObject();
				if (propList->AddComponent(BorderColorB))
				{
					BorderColorB->SetPosition(compPos);
					BorderColorB->SetSize(compSize);
					BorderColorB->SetMinValue(0);
					BorderColorB->SetMaxValue(255);
					BorderColorB->SetPropValue(TXT("255"), false);
					BorderColorB->SetDefaultValue(255);
					BorderColorB->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderColor, void, SD::EditPropertyComponent*);

					if (SD::LabelComponent* caption = BorderColorB->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderColorB"), transFileName, sectionName));
					}
				}

				BorderTileTextureField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(BorderTileTextureField))
				{
					BorderTileTextureField->SetPosition(compPos);
					BorderTileTextureField->SetSize(compSize);
					BorderTileTextureField->SetMinValue(0.0001f);
					BorderTileTextureField->SetPropValue(TXT("1.0"), false);
					BorderTileTextureField->SetDefaultValue(1.f);
					BorderTileTextureField->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderTiledTexture, void, SD::EditPropertyComponent*);

					if (SD::LabelComponent* caption = BorderTileTextureField->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderTileTexture"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (BorderTileTextureField->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("BorderTileTextureTooltip"), transFileName, sectionName));
					}
				}
				
				BorderStretchTextureField = SD::EditableFloat::CreateObject();
				if (propList->AddComponent(BorderStretchTextureField))
				{
					BorderStretchTextureField->SetPosition(compPos);
					BorderStretchTextureField->SetSize(compSize);
					BorderStretchTextureField->SetMinValue(0.0001f);
					BorderStretchTextureField->SetPropValue(TXT("1.0"), false);
					BorderStretchTextureField->SetDefaultValue(1.f);
					BorderStretchTextureField->OnApplyEdit = SDFUNCTION_1PARAM(this, SpriteTesterUi, HandleSetBorderStretchedTexture, void, SD::EditPropertyComponent*);

					if (SD::LabelComponent* caption = BorderStretchTextureField->GetPropertyName())
					{
						caption->SetText(translator->TranslateText(TXT("BorderStretchTexture"), transFileName, sectionName));
					}

					SD::TooltipComponent* tooltip = SD::TooltipComponent::CreateObject();
					if (BorderStretchTextureField->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("BorderStretchTextureTooltip"), transFileName, sectionName));
					}
				}
			}

			SD::LabelComponent* drawModeText = SD::LabelComponent::CreateObject();
			if (propList->AddComponent(drawModeText))
			{
				drawModeText->SetAutoRefresh(false);
				drawModeText->SetPosition(compPos);
				drawModeText->SetSize(compSize);
				drawModeText->SetWrapText(false);
				drawModeText->SetClampText(false);
				drawModeText->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				drawModeText->SetVerticalAlignment(SD::LabelComponent::VA_Center);
				drawModeText->SetText(translator->TranslateText(TXT("DrawMode"), transFileName, sectionName));
				drawModeText->SetAutoRefresh(true);
			}

			DrawModeDropdown = SD::DropdownComponent::CreateObject();
			if (propList->AddComponent(DrawModeDropdown))
			{
				//Add DrawMode option at the top. Added at the end of component list for render order purposes. The Dropdown component should render above the options below.
				propList->MoveComponentToTop(DrawModeDropdown);
				CHECK(drawModeText != nullptr)
				propList->MoveComponentToTop(drawModeText);

				DrawModeDropdown->SetPosition(compPos);
				DrawModeDropdown->SetSize(SD::Vector2(1.f, 4.f) * compSize);
				DrawModeDropdown->SetItemSelectionHeight(compSize.Y);
				DrawModeDropdown->OnOptionSelected = SDFUNCTION_2PARAM(this, SpriteTesterUi, HandleDrawModeOptionSelected, void, SD::DropdownComponent*, SD::Int);

				SD::GuiDataElement<SD::SpriteComponent::EDrawMode> stretchOption;
				stretchOption.Data = SD::SpriteComponent::DM_Stretch;
				stretchOption.LabelText = translator->TranslateText(TXT("StretchMode"), transFileName, sectionName);
				DrawModeDropdown->GetExpandMenuList()->AddOption(stretchOption);

				SD::GuiDataElement<SD::SpriteComponent::EDrawMode> tiledOption;
				tiledOption.Data = SD::SpriteComponent::DM_Tiled;
				tiledOption.LabelText = translator->TranslateText(TXT("TiledMode"), transFileName, sectionName);
				DrawModeDropdown->GetExpandMenuList()->AddOption(tiledOption);

				DrawModeDropdown->SetSelectedItem(stretchOption.Data);
			}
		}
	}
}

void SpriteTesterUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	Focus->TabOrder.push_back(DrawModeDropdown);
	Focus->TabOrder.push_back(NumHorizontalSegField->GetPropValueField());
	Focus->TabOrder.push_back(NumVerticalSegField->GetPropValueField());
	Focus->TabOrder.push_back(HorizontalIdxField->GetPropValueField());
	Focus->TabOrder.push_back(VerticalIdxField->GetPropValueField());
	Focus->TabOrder.push_back(ApplySubDivisionButton);

	Focus->TabOrder.push_back(XScaleMultField->GetPropValueField());
	Focus->TabOrder.push_back(YScaleMultField->GetPropValueField());
	Focus->TabOrder.push_back(XPosMultField->GetPropValueField());
	Focus->TabOrder.push_back(YPosMultField->GetPropValueField());
	Focus->TabOrder.push_back(ApplyDrawCoordMultButton);

	Focus->TabOrder.push_back(UField->GetPropValueField());
	Focus->TabOrder.push_back(VField->GetPropValueField());
	Focus->TabOrder.push_back(UlField->GetPropValueField());
	Focus->TabOrder.push_back(VlField->GetPropValueField());
	Focus->TabOrder.push_back(ApplyDrawCoordButton);
}

void SpriteTesterUi::SetOwningTester (SpriteTester* newOwningTester)
{
	OwningTester = newOwningTester;
}

void SpriteTesterUi::HandleDrawModeOptionSelected (SD::DropdownComponent* comp, SD::Int optionIdx)
{
	switch (optionIdx.Value)
	{
		case(0) :
			if (TestSprite != nullptr)
			{
				TestSprite->SetDrawMode(SD::SpriteComponent::DM_Stretch);
			}

			if (ApplySubDivisionButton != nullptr)
			{
				ApplySubDivisionButton->SetEnabled(true);
			}

			break;

		case(1) :
			if (TestSprite != nullptr)
			{
				TestSprite->SetDrawMode(SD::SpriteComponent::DM_Tiled);
			}

			if (ApplySubDivisionButton != nullptr)
			{
				ApplySubDivisionButton->SetEnabled(false);
			}

			break;
	}
}

void SpriteTesterUi::HandleApplySubDivisionReleased (SD::ButtonComponent* uiComp)
{
	if (TestSprite != nullptr)
	{
		CHECK(NumHorizontalSegField != nullptr && NumVerticalSegField != nullptr && HorizontalIdxField != nullptr && VerticalIdxField != nullptr)
		SD::Int numHorSeg;
		SD::Int numVertSeg;
		SD::Int horIdx;
		SD::Int vertIdx;
		if (NumHorizontalSegField->GetPropValue(OUT numHorSeg) && NumVerticalSegField->GetPropValue(OUT numVertSeg) && HorizontalIdxField->GetPropValue(OUT horIdx) && VerticalIdxField->GetPropValue(OUT vertIdx))
		{
			TestSprite->SetSubDivision(numHorSeg, numVertSeg, horIdx, vertIdx);
		}
	}
}

void SpriteTesterUi::HandleApplyDrawCoordMultReleased (SD::ButtonComponent* uiComp)
{
	if (TestSprite != nullptr)
	{
		CHECK(XScaleMultField != nullptr && YScaleMultField != nullptr && XPosMultField != nullptr && YPosMultField != nullptr)
	
		SD::Float xScale;
		SD::Float yScale;
		SD::Float xPos;
		SD::Float yPos;
		if (XScaleMultField->GetPropValue(OUT xScale) && YScaleMultField->GetPropValue(OUT yScale) && XPosMultField->GetPropValue(OUT xPos) && YPosMultField->GetPropValue(OUT yPos))
		{
			TestSprite->SetDrawCoordinatesMultipliers(xScale, yScale, xPos, yPos);
		}
	}
}

void SpriteTesterUi::HandleApplyDrawCoordReleased (SD::ButtonComponent* uiComp)
{
	if (TestSprite != nullptr)
	{
		CHECK(UField != nullptr && VField != nullptr && UlField != nullptr && VlField != nullptr)
	
		SD::Int u;
		SD::Int v;
		SD::Int ul;
		SD::Int vl;
		if (UField->GetPropValue(OUT u) && VField->GetPropValue(OUT v) && UlField->GetPropValue(OUT ul) && VlField->GetPropValue(OUT vl))
		{
			TestSprite->SetDrawCoordinates(u, v, ul, vl);
		}
	}
}

void SpriteTesterUi::HandleSetBorderTexture (SD::EditPropertyComponent* editProp)
{
	if (TestBorderComp != nullptr && BorderSetTextureCheckbox != nullptr)
	{
		bool bApplyTexture = true;
		if (SD::CheckboxComponent* checkbox = BorderSetTextureCheckbox->GetCheckbox())
		{
			bApplyTexture = checkbox->IsChecked();
		}

		if (bApplyTexture)
		{
			SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
			CHECK(localTexturePool != nullptr)
			if (const SD::Texture* borderTexture = localTexturePool->GetTexture(SD::HashedString("Engine.Graphics.BorderTexture")))
			{
				TestBorderComp->SetBorderTexture(borderTexture);
			}
		}
		else
		{
			TestBorderComp->SetBorderTexture(nullptr);
		}
	}
}

void SpriteTesterUi::HandleSetBorderThickness (SD::EditPropertyComponent* editProp)
{
	if (TestBorderComp != nullptr && TestSpriteTransform != nullptr && BorderThicknessField != nullptr)
	{
		SD::Float newThickness;
		if (BorderThicknessField->GetPropValue<SD::Float>(OUT newThickness))
		{
			TestBorderComp->SetBorderThickness(newThickness);
			TestSpriteTransform->SetAnchorTop(newThickness);
			TestSpriteTransform->SetAnchorRight(newThickness);
			TestSpriteTransform->SetAnchorBottom(newThickness);
			TestSpriteTransform->SetAnchorLeft(newThickness);
		}

		if (SpriteFrame != nullptr)
		{
			SpriteFrame->SetBorderThickness(SD::Utils::Max<SD::Float>(newThickness, 4.f));
		}
	}
}

void SpriteTesterUi::HandleSetBorderColor (SD::EditPropertyComponent* editProp)
{
	SD::Color newColor = SD::Color::WHITE;

	if (BorderColorR != nullptr)
	{
		SD::Int newR;
		if (BorderColorR->GetPropValue<SD::Int>(OUT newR))
		{
			newColor.Source.r = newR.Value;
		}
	}

	if (BorderColorG != nullptr)
	{
		SD::Int newG;
		if (BorderColorG->GetPropValue<SD::Int>(OUT newG))
		{
			newColor.Source.g = newG.Value;
		}
	}

	if (BorderColorB != nullptr)
	{
		SD::Int newB;
		if (BorderColorB->GetPropValue<SD::Int>(OUT newB))
		{
			newColor.Source.b = newB.Value;
		}
	}

	if (TestBorderComp != nullptr)
	{
		TestBorderComp->SetBorderColor(newColor);
	}
}

void SpriteTesterUi::HandleSetBorderTiledTexture (SD::EditPropertyComponent* uiComp)
{
	if (TestBorderComp != nullptr && BorderTileTextureField != nullptr)
	{
		SD::Float textureScale = 1.f;
		BorderTileTextureField->GetPropValue<SD::Float>(OUT textureScale);

		SD::Float textureHeight = 1.f; //For this test, adjust the texture size so that it fits exactly within the border thickness.
		if (const SD::Texture* borderTexture = TestBorderComp->GetBorderTexture())
		{
			textureHeight = TestBorderComp->GetBorderThickness() / borderTexture->GetHeight().ToFloat();
		}

		TestBorderComp->ApplyDrawModeTiled(SD::Vector2(textureScale, textureHeight));
	}
}

void SpriteTesterUi::HandleSetBorderStretchedTexture (SD::EditPropertyComponent* uiComp)
{
	if (TestBorderComp != nullptr && BorderStretchTextureField != nullptr)
	{
		SD::Float textureScale = 1.f;
		BorderStretchTextureField->GetPropValue<SD::Float>(OUT textureScale);
		TestBorderComp->ApplyDrawModeStretched(SD::Vector2(textureScale, 1.f));
	}
}
SD_TESTER_END
#endif