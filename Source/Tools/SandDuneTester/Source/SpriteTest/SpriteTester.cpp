/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  SpriteTester.cpp
=====================================================================
*/

#include "SpriteTester.h"
#include "SpriteTesterUi.h"

#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SDTester::SpriteTester, SDTester::WindowedEntityManager)
SD_TESTER_BEGIN

const SD::Vector2 SpriteTester::WINDOW_SIZE(1024.f, 576.f);

void SpriteTester::InitProps ()
{
	Super::InitProps();

	TestFlags = SD::UnitTester::UTF_None;
	MainTestUi = nullptr;
}

void SpriteTester::Destroyed ()
{
	if (MainTestUi != nullptr)
	{
		MainTestUi->Destroy();
		MainTestUi = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void SpriteTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the SpriteTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Sprite Test"), 0.033333f /*~30 fps*/);

	//Swap themes for the mock editor. Typically this is set at an application level, but for a tester that has editors and scenes, swap it dynamically.
	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	SD::EditorEngineComponent* editorEngine = SD::EditorEngineComponent::Find();
	CHECK(guiEngine != nullptr && editorEngine != nullptr)
	guiEngine->SetGuiTheme(editorEngine->GetUiTheme());

	MainTestUi = SpriteTesterUi::CreateObject();
	GuiDrawLayer->RegisterMenu(MainTestUi);
	MainTestUi->SetupInputComponent(Input.Get(), 100);
	MainTestUi->SetOwningTester(this);

	SD::UnitTester::TestLog(TestFlags, TXT("The SprteTester unit test has launched. It will terminate based on user input."));
}
SD_TESTER_END
#endif