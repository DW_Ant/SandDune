/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditorTheme.h

  Theme used to configure GuiComponents' appearance for this application.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class TextureEditorTheme : public EditorTheme
{
	DECLARE_CLASS(TextureEditorTheme)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString TEXTURE_EDITOR_STYLE_NAME;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeStyles () override;
};
TE_END