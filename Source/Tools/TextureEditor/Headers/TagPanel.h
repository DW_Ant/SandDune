/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TagPanel.h
  The GuiEntity that allows the user to add and select texture tags.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class TagPanel : public GuiEntity
{
	DECLARE_CLASS(TagPanel)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever the selected tags are applied to the edited texture instance. */
	SDFunction<void> OnApply;

	/* Invoked whenever this panel closes. */
	SDFunction<void> OnClose;
	
protected:
	/* Name of the texture instance this panel is editing. */
	DString TextureName;

	/* List of all tags that was pulled from the engine component (cached for performance). */
	std::vector<DString> AllTags;

	/* List of tags the user selected. This may not always be the same as the ListBoxComponent since the ListBoxComponent could display only filtered tags. */
	std::vector<DString> SelectedTags;

	//Tagging frame
	FrameComponent* TagFrame;
	TextFieldComponent* TagNameField;
	ButtonComponent* AddTagButton;
	ButtonComponent* SearchTagButton;
	FrameComponent* TagReferenceViewerFrame;
	LabelComponent* TagReferenceViewerHeader;
	ButtonComponent* CollapseTagReferenceViewerButton;
	ScrollbarComponent* ReferenceContentScrollbar;
	LabelComponent* ReferenceViewerContent;
	ScrollbarComponent* TagListScrollbar;
	ListBoxComponent* TagList;
	ButtonComponent* AcceptTagsButton;
	ButtonComponent* CancelTagsButton;

private:
	/* If true, then this Entity will ignore any selection toggle callbacks from the TagList ListBoxComponent. */
	bool bIgnoreTagSelectionToggle;

	/* The size of the ListBox owner at the time was added to the list. This is used to detect changes in size so this panel
	knows when it can shift the scrollbar camera to the bottom. */
	Vector2 TagListPrevSize;

	/* Tick Component that's only running when it's waiting for the scrollbar's viewed object to change dimensions. */
	TickComponent* BottomScrollTick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual bool HandleConsumableInput (const sf::Event& evnt) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Applies the selected tags to the edited texture.
	 */
	virtual void ApplyTags ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTextureName (const DString& newTextureName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<DString>& ReadSelectedTags () const
	{
		return SelectedTags;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Populates the tag list box based on the search filters (if any).
	 * This also auto selects tags based on the Tags vector.
	 */
	virtual void PopulateTagList ();

	/**
	 * Iterates through all imported textures to figure out which textures reference what tags.
	 * It'll populate the ReferenceViewerContent with what the search found.
	 */
	virtual void PopulateTagReferences (const DString& tagSearch);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleSearchTagEdit (const sf::Event& evnt);
	virtual void HandleTagNameReturn (TextFieldComponent* uiComp);
	virtual void HandleAddTagReleased (ButtonComponent* uiComp);
	virtual void HandleSearchTagReleased (ButtonComponent* uiComp);
	virtual void HandleCollapseRefViewerReleased (ButtonComponent* uiComp);
	virtual void HandleTagSelected (Int selectionIdx);
	virtual void HandleTagDeselected (Int selectionIdx);
	virtual void HandleScrollToBottomTick (Float deltaSec);
	virtual void HandleAcceptTagsReleased (ButtonComponent* uiComp);
	virtual void HandleCancelTagsReleased (ButtonComponent* uiComp);
};
TE_END