/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PromptPopup.h
  A simple GuiEntity that prompts the user with a few options.

  This popup, as the name implies, should appear above other GuiEntities to render
  last and consume input.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class PromptPopup : public GuiEntity
{
	DECLARE_CLASS(PromptPopup)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	SDFunction<void> OnAccept;
	SDFunction<void> OnNo;
	SDFunction<void> OnCancel;

protected:
	FrameComponent* Blackout;
	LabelComponent* PromptLabel;
	ButtonComponent* YesButton;
	ButtonComponent* NoButton;
	ButtonComponent* CancelButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual bool HandleConsumableInput (const sf::Event& evnt) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Replaces the text content with the passed in string.
	 */
	virtual void SetPromptText (const DString& newText);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleYesReleased (ButtonComponent* uiComp);
	virtual void HandleNoReleased (ButtonComponent* uiComp);
	virtual void HandleCancelReleased (ButtonComponent* uiComp);
};
TE_END