/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditorEngineComponent.h
  The engine component that owns the editor entity.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class EditorUi;

class TextureEditorEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(TextureEditorEngineComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SDevAssetTags
	{
		/* Full file name including path and extension. */
		DString DevAssetName;
		std::vector<DString> Tags;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The main UI Entity that is the texture editor, itself. */
	EditorUi* Editor;

	/* List of DevAssets and their associated tags. The editor will reference this list instead of TexturePool's tags because
	not all DevAssets are exported to the Content directory. For runtime updates, it'll reference this list instead. */
	std::vector<SDevAssetTags> DevAssetTags;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TextureEditorEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual void GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the dev asset with its new tag information.
	 * If the asset name is not found, one will be created.
	 */
	virtual void UpdateDevAsset (const DString& assetName, const std::vector<DString>& newTagList);

	/**
	 * Retrieves a list of tags that are found from dev texture assets.
	 * The vector is not in any particular order.
	 */
	virtual void GetTags (std::vector<DString>& outTags) const;

	virtual void GetTagsFromTexture (const DString& textureName, std::vector<DString>& outTags) const;

	/**
	 * Returns a list of file assets that references the given tag.
	 */
	virtual void FindFilesReferencingTag (const DString& tagName, std::vector<DString>& outReferences) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportEditorAssets ();

	/**
	 * Iterates through each texture file in the DevAsset directory, and populates the DevAssetTags vector with the information it finds.
	 */
	virtual void PopulateDevAssetTags ();
};
TE_END