/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditor.h
  Contains important file includes and definitions for the Texture Editor application.

  The Texture Editor presents a Gui that allows the user to edit and export
  texture assets.
=====================================================================
*/

#pragma once

#include "Engine/Core/Headers/CoreClasses.h"
#include "Engine/Editor/Headers/EditorClasses.h"
#include "Engine/File/Headers/FileClasses.h"
#include "Engine/Graphics/Headers/GraphicsClasses.h"
#include "Engine/Gui/Headers/GuiClasses.h"
#include "Engine/Input/Headers/InputClasses.h"
#include "Engine/Localization/Headers/LocalizationClasses.h"
#include "Engine/Logger/Headers/LoggerClasses.h"
#include "Engine/Plugin/Headers/PluginClasses.h"

#define TE_BEGIN namespace SD { namespace TE {
#define TE_END }}

TE_BEGIN
extern LogCategory TextureEditorLog;
extern const DString LOCALIZATION_FILE;
TE_END