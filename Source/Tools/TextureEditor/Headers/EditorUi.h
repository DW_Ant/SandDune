/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorUi.h
  The main GuiEntity that defines the Texture Editor panels.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class PromptPopup;
class TagPanel;

class EditorUi : public GuiEntity
{
	DECLARE_CLASS(EditorUi)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* File extension for all dev texture assets. */
	static const DString DEV_EXTENSION;

	/* String used to identify this dev asset types from others (in case there's a need to merge the texture's dev assets as a standard dasset). */
	static const DString TEXTURE_ASSET_TYPE;

	/* Unique identifies the OS will use to remember file browser options. */
	static const Int NEW_DIALOGUE_ID;
	static const Int OPEN_DIALOGUE_ID;
	static const Int SAVE_DIALOGUE_ID;
	static const Int EXPORT_DIALOGUE_ID;

	/* Colors to use when the feedback text is displaying. */
	Color FeedbackPassedColor;
	Color FeedbackFailedColor;

protected:
	/* The texture this editor is ... editing. */
	Texture* EditedTexture;

	/* The HashedString used in the texture pool that leads to the EditedTexture. */
	HashedString TexturePath;

	/* The texture dev asset file that was used to open the current edited texture.
	Becomes empty and invalid if this was a created texture from New. */
	FileAttributes OpenedDevFile;

	/* The texture asset file that was most recently exported to for the current texture. */
	FileAttributes ExportedFile;

	/* Becomes true if the edited texture has been modified but not saved. */
	bool bDirty;

	/* The preview color the user has configured. */
	Color BackgroundColor;

	/* Timestamp when the feedback text was displayed (in seconds). */
	Float FeedbackTimestamp;

	/* Number of seconds does it take for the feedback text to completely fade out. */
	Float FeedbackDisplayDuration;

	/* List of tags the user has selected. This is different from EditedTexture tags when the user toggled a selection without applying the changes. */
	std::vector<DString> Tags;

	/* The UI Entity that's responsible for rendering over the editor. This panel will allow the user to edit the tag list. */
	TagPanel* CurrentTagPanel;

	PromptPopup* Popup;

	//Menu bar
	FrameComponent* MenuFrame;
	ButtonComponent* NewButton;
	ButtonComponent* LoadButton;
	ButtonComponent* SaveButton;
	ButtonComponent* ExportButton;

	//Feedback Icon
	FrameComponent* FeedbackFrame;
	TickComponent* FeedbackTick;
	PlanarTransformComponent* FeedbackIconTransform;
	SpriteComponent* FeedbackIcon;
	LabelComponent* FeedbackText;

	//Viewport
	FrameComponent* PreviewFrame;
	PlanarTransformComponent* PreviewTransform;
	SpriteComponent* TexturePreview;
	ButtonComponent* ViewOptions;
	FrameComponent* InfoBackground;
	LabelComponent* SourceLabel;
	LabelComponent* DimensionsLabel;

	//View options
	VerticalList* ViewOptionsFrame;
	EditableBool* TiledCheckbox;
	EditableBool* CheckerboardBackground;
	EditableBool* ShowInfoCheckbox;
	EditableInt* BackgroundColorR;
	EditableInt* BackgroundColorG;
	EditableInt* BackgroundColorB;

	//Texture editable properties
	VerticalList* TextureInspector;
	EditableDString* AuthorField;
	EditableDString* LicenseField;
	EditableDString* ExportDestinationField;
	EditableArray* TagList;
	ButtonComponent* EditTagListButton;
	EditableBool* WrappingBool;
	EditableBool* SmoothEdgesBool;
	EditableBool* AllowMipMapBool;
	EditableDString* CommentField;

private:
	/* If true then the editor can set the dirty flag to true when there are edits to its components. */
	bool bCanBecomeDirty;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void PostAbsTransformUpdate () override;

protected:
	virtual void Destroyed () override;
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual bool HandleConsumableInput (const sf::Event& evnt) override;
	virtual bool HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Saves the current texture to the specified file.
	 */
	virtual void SaveCurrentTexture (const FileAttributes& file);

	/**
	 * Displays the feedback notification with the specified text.
	 * @param bSuccess - If true, then the feedback text will be greenish with a checkmark. Otherwise it would be red X.
	 */
	virtual void DisplayFeedback (const DString& feedbackText, bool bSuccess);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Marks the editor that there are unsaved edits.
	 */
	virtual void MarkDirty ();

	/**
	 * Notifies the editor that it's no longer dirty.
	 */
	virtual void UnmarkDirty ();

	virtual void DisplayPrompt ();

	/**
	 * Accesses the primary window handle to update its title bar text to the specified string.
	 */
	virtual void SetWindowTitle (const DString& newTitle);

	/**
	 * Calculates the absolute size and position for each menu component.
	 * The menu components are squared buttons that also auto adjusts their vertical size based on the frame's size.
	 * The buttons are also displaced based on the horizontal size.
	 */
	virtual void CalcMenuTransforms ();

	/**
	 * Updates the texture preview to show the current texture, and its transform so that its aspect ratio is preserved.
	 */
	virtual void RefreshTexturePreview ();

	/**
	 * Formats the given string to be in the correct format for ExportDestination. Essentially it returns the string that is
	 * relative to the Engine directory rather than the absolute path.
	 * This also ensures the extension is txtr.
	 */
	virtual DString FormatExportDestination (const DString& exportDest) const;

	virtual void ShowViewOptions ();
	virtual void HideViewOptions ();

	virtual void ShowTagFrame ();

	virtual void CreateNewTexture ();
	virtual void LoadTexture ();
	virtual void SaveTexture ();

	/**
	 * Updates the Export Destination text field based on where the dev asset is saved.
	 * This will not overwrite the text field if there is data residing in it already.
	 * The TextureEditor will only set it to corresponding the content directory if the dev asset resides in the DevAsset folder.
	 * Otherwise it's left alone.
	 */
	virtual void UpdateExportDestBasedOnSaveDest (const FileAttributes& devAssetDest);

	/**
	 * Opens the file dialogue to allow the user to browse to the file they want to export this texture to.
	 */
	virtual bool ChooseExportDestination ();
	virtual void ExportTexture ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNewButtonReleased (ButtonComponent* uiComp);
	virtual void HandleLoadButtonReleased (ButtonComponent* uiComp);
	virtual void HandleSaveButtonReleased (ButtonComponent* uiComp);
	virtual void HandleExportButtonReleased (ButtonComponent* uiComp);
	virtual void HandleFeedbackTick (Float deltaSec);

	virtual void HandleViewOptionsReleased (ButtonComponent* uiComp);
	virtual void HandleTiledCheckboxToggle (EditPropertyComponent* comp);
	virtual void HandleCheckerboardToggle (EditPropertyComponent* comp);
	virtual void HandleShowInfoToggle (EditPropertyComponent* comp);
	virtual void HandleBackgroundColorEdit (EditPropertyComponent* comp);

	virtual void HandleSourceLabelTransformChanged ();

	virtual void HandleEditExportDestinationReleased (ButtonComponent* uiComp);
	virtual void HandleExportDestinationEdit (EditPropertyComponent* comp);
	virtual void HandleEditTagsReleased (ButtonComponent* uiComp);
	virtual void HandleToggleWrapping (EditPropertyComponent* comp);
	virtual void HandleToggleSmoothEdges (EditPropertyComponent* comp);
	virtual void HandleToggleMipMap (EditPropertyComponent* comp);

	virtual void HandleTagPanelApply ();
	virtual void HandleTagPanelClose ();

	virtual void HandlePropertyEdit (EditPropertyComponent* uiComp);

	virtual void HandleSaveToNewReleased ();
	virtual void HandleSkipSaveToNewReleased ();
	virtual void HandleCancelNewReleased ();

	virtual void HandleSaveToLoadReleased ();
	virtual void HandleSkipSaveToLoadReleased ();
	virtual void HandleCancelLoadReleased ();

	virtual void HandleSaveToQuitReleased ();
	virtual void HandleSkipSaveToQuitReleased ();
	virtual void HandleCancelQuitReleased ();

	virtual bool HandleCanClose ();
};
TE_END