/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureExporter.h
  An object that iterates through each DevAsset to export them to the Content directory.

  This is executed when the user passes '-AutoExport' in the command line arguments.

  If '-Clean' is passed in the command line arguments, it'll delete every txtr file from
  the Content directory before running the exporter.

  If '-CleanOnly' is passed in the command line arguments, it'll only delete every txtr file
  from the Content directory.

  The Editor, itself, is not instantiated when the TextureEditor is running in export mode.
=====================================================================
*/

#pragma once

#include "TextureEditor.h"

TE_BEGIN
class TextureExporter : public Object
{
	DECLARE_CLASS(TextureExporter)


	/*
	=====================
	  Properties
	=====================
	*/
	
protected:
	/* The number of files that was deleted from a clean command. */
	Int NumCleaned;

	/* The number of files that was successfully exported. */
	Int NumExported;

	/* The number of files that failed to delete or export. */
	Int NumErrors;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Launches the process to manage texture assets.
	 * @param bRunClean - If true, then the exporter will delete every txtr file from the Content directory.
	 * @param bRunExport - If true, then the exporter will generate a txtr file found from the DevAssets directory.
	 */
	virtual void RunExporter (bool bRunClean, bool bRunExport);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Int GetNumCleaned () const
	{
		return NumCleaned;
	}

	inline Int GetNumExported () const
	{
		return NumExported;
	}

	inline Int GetNumErrors () const
	{
		return NumErrors;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Actual implementation of the clean command.
	 */
	virtual void ExecuteClean ();

	/**
	 * Iterates through each dev asset file to produce a txtr file.
	 */
	virtual void ExecuteExport ();
};
TE_END