/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditor.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "TextureEditor.h"
#include "TextureEditorEngineComponent.h"
#include "TextureExporter.h"

TE_BEGIN
LogCategory TextureEditorLog(TXT("Texture Editor"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);

const DString LOCALIZATION_FILE(TXT("TextureEditor"));
TE_END

//Forward function declarations
int BeginSandDune (TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	TCHAR* args = GetCommandLine();
	return BeginSandDune(&args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (TCHAR* args[])
{
	SD::ProjectName = TXT("Texture Editor");

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(args[0]);

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	bool bAutoExport = engine->HasCmdLineSwitch(TXT("-AutoExport"), SD::DString::CC_IgnoreCase);

	if (!bAutoExport) //Run normally (with the editor)
	{
		//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
		std::vector<SD::EngineComponent*> engineComponents;
		engineComponents.push_back(new SD::EditorEngineComponent());
		engineComponents.push_back(new SD::GraphicsEngineComponent());
		engineComponents.push_back(new SD::GuiEngineComponent());
		engineComponents.push_back(new SD::InputEngineComponent());
		engineComponents.push_back(new SD::LocalizationEngineComponent());
		engineComponents.push_back(new SD::LoggerEngineComponent());
		engineComponents.push_back(new SD::PluginEngineComponent());
		engineComponents.push_back(new SD::TE::TextureEditorEngineComponent());

		engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

		return RunMainLoop();
	}
	else
	{
		std::vector<SD::EngineComponent*> engineComponents;
		engineComponents.push_back(new SD::LoggerEngineComponent());
		engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents);

		SD::TE::TextureExporter* exporter = SD::TE::TextureExporter::CreateObject();
		
		bool bCleanOnly = engine->HasCmdLineSwitch(TXT("-CleanOnly"), SD::DString::CC_IgnoreCase);
		bool bClean = (bCleanOnly || engine->HasCmdLineSwitch(TXT("-Clean"), SD::DString::CC_IgnoreCase));
		exporter->RunExporter(bClean, !bCleanOnly);
		SD::Int numErrors = exporter->GetNumErrors();
		exporter->Destroy();

		engine->Shutdown();
		engine->Tick();
		delete engine;
		engine = nullptr;

		return numErrors.Value;
	}
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}