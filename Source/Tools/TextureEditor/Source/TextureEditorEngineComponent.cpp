/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditorEngineComponent.cpp
=====================================================================
*/

#include "EditorUi.h"
#include "TextureEditorEngineComponent.h"
#include "TextureEditorTheme.h"

IMPLEMENT_ENGINE_COMPONENT(SD::TE::TextureEditorEngineComponent)
TE_BEGIN

TextureEditorEngineComponent::TextureEditorEngineComponent () : Super(),
	Editor(nullptr)
{
	//Noop
}

void TextureEditorEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(TextureEditorTheme::SStaticClass());

	//Textures must be imported in pre before the GuiEngineComponent initializes the theme in init.
	ImportEditorAssets();
}

void TextureEditorEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	PopulateDevAssetTags();
}

void TextureEditorEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	Editor = EditorUi::CreateObject();
	Editor->RegisterToMainWindow(true, true);
}

void TextureEditorEngineComponent::ShutdownComponent ()
{
	if (Editor != nullptr)
	{
		Editor->Destroy();
		Editor = nullptr;
	}

	Super::ShutdownComponent();
}

void TextureEditorEngineComponent::GetPreInitializeDependencies (std::vector<const DClass*>& outDependencies) const
{
	Super::GetPreInitializeDependencies(OUT outDependencies);

	outDependencies.push_back(GraphicsEngineComponent::SStaticClass()); //for the TexturePool
	outDependencies.push_back(GuiEngineComponent::SStaticClass()); //for setting the gui theme
}

void TextureEditorEngineComponent::UpdateDevAsset (const DString& assetName, const std::vector<DString>& newTagList)
{
	for (size_t i = 0; i < DevAssetTags.size(); ++i)
	{
		if (DevAssetTags.at(i).DevAssetName.Compare(assetName, DString::CC_IgnoreCase) == 0)
		{
			DevAssetTags.at(i).Tags = newTagList;
			return;
		}
	}

	//File is not found. Add a new one
	SDevAssetTags newEntry;
	newEntry.DevAssetName = assetName;
	newEntry.Tags = newTagList;
	DevAssetTags.emplace_back(newEntry);
}

void TextureEditorEngineComponent::GetTags (std::vector<DString>& outTags) const
{
	//This is a very slow function! Strongly considering switching to case sensitive comparisons and caching the merged tag list.
	for (const SDevAssetTags& devAsset : DevAssetTags)
	{
		for (const DString& devTag : devAsset.Tags)
		{
			bool bFoundTag = false;
			for (const DString& outTag : outTags)
			{
				if (devTag.Compare(outTag, DString::CC_IgnoreCase) == 0)
				{
					bFoundTag = true;
					break;
				}
			}

			if (!bFoundTag)
			{
				outTags.push_back(devTag);
			}
		}
	}
}

void TextureEditorEngineComponent::GetTagsFromTexture (const DString& textureName, std::vector<DString>& outTags) const
{
	ContainerUtils::Empty(OUT outTags);

	for (const SDevAssetTags& devAsset : DevAssetTags)
	{
		if (devAsset.DevAssetName.Compare(textureName, DString::CC_IgnoreCase) == 0)
		{
			outTags = devAsset.Tags;
			break;
		}
	}
}

void TextureEditorEngineComponent::FindFilesReferencingTag (const DString& tagName, std::vector<DString>& outReferences) const
{
	for (const SDevAssetTags& devAsset : DevAssetTags)
	{
		for (const DString& tag : devAsset.Tags)
		{
			if (tag.Compare(tagName, DString::CC_IgnoreCase) == 0)
			{
				outReferences.push_back(devAsset.DevAssetName);
				break; //only break inner loop
			}
		}
	}
}

void TextureEditorEngineComponent::ImportEditorAssets ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->ImportTexture(FileAttributes(Directory::CONTENT_DIRECTORY / TXT("Tools") / TXT("TextureEditor"), TXT("Gradient.txtr")));
}

void TextureEditorEngineComponent::PopulateDevAssetTags ()
{
	ContainerUtils::Empty(OUT DevAssetTags);

	TextureEditorLog.Log(LogCategory::LL_Log, TXT("Importing texture tags . . ."));
	ConfigWriter* config = ConfigWriter::CreateObject();

	for (FileIterator iter(Directory::DEV_ASSET_DIRECTORY, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		FileAttributes file = iter.GetSelectedAttributes();
		DString fullName = file.GetName(true, true);
		if (!fullName.EndsWith(EditorUi::DEV_EXTENSION, DString::CC_IgnoreCase))
		{
			//Not a texture asset
			continue;
		}

		SDevAssetTags newEntry;
		newEntry.DevAssetName = fullName;

		if (config->OpenFile(file, false))
		{
			config->GetArrayValues<DString>(TXT("EditorUi"), TXT("Tags"), OUT newEntry.Tags);
		}

		DevAssetTags.push_back(newEntry);
	}

	config->Destroy();
	TextureEditorLog.Log(LogCategory::LL_Log, TXT("Found %s dev texture files."), Int(DevAssetTags.size()));
}
TE_END