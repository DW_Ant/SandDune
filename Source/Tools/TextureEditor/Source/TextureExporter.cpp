/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureExporter.cpp
=====================================================================
*/

#include "EditorUi.h"
#include "TextureExporter.h"

IMPLEMENT_CLASS(SD::TE::TextureExporter, SD::Object)
TE_BEGIN

void TextureExporter::InitProps ()
{
	Super::InitProps();

	NumCleaned = 0;
	NumExported = 0;
	NumErrors = 0;
}

void TextureExporter::RunExporter (bool bRunClean, bool bRunExport)
{
	NumCleaned = 0;
	NumExported = 0;
	NumErrors = 0;

	if (!bRunClean && !bRunExport)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Texture Exporter didn't do anything since the exporter was instructed to run without cleaning and without running the exporter."));
		return;
	}

	TextureEditorLog.Log(LogCategory::LL_Log, TXT("Running Texture Exporter with Clean=%s and Export=%s."), Bool(bRunClean), Bool(bRunExport));
	if (bRunClean)
	{
		ExecuteClean();
	}

	if (bRunExport)
	{
		ExecuteExport();
	}

	DString failedSuffix = (NumErrors > 0) ? DString::CreateFormattedString(TXT("%s file(s) failed to export!"), NumErrors) : TXT("No errors detected!");
	TextureEditorLog.Log(LogCategory::LL_Log, TXT("Texture Exporter finished! Found and removed %s texture file(s). Successfully exported %s file(s). %s"), NumCleaned, NumExported, failedSuffix);
}

void TextureExporter::ExecuteClean ()
{
	std::vector<FileAttributes> deleteList;

	for (FileIterator iter(Directory::CONTENT_DIRECTORY, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		if (iter.GetSelectedAttributes().GetFileExtension().Compare(TextureFile::FILE_EXTENSION, DString::CC_IgnoreCase) == 0)
		{
			deleteList.push_back(iter.GetSelectedAttributes());
		}
	}

	for (const FileAttributes& txtrFile : deleteList)
	{
		if (!OS_CheckIfFileExists(txtrFile))
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to delete %s since it doesn't exist on the OS despite that the filte iterator found it earlier."), txtrFile.GetName(true, true));
			continue;
		}

		if (OS_DeleteFile(txtrFile))
		{
			++NumCleaned;
			TextureEditorLog.Log(LogCategory::LL_Log, TXT("Successfully Cleaned %s"), txtrFile.GetName(true, true));
		}
		else
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to clean %s"), txtrFile.GetName(true, true));
		}
	}
}

void TextureExporter::ExecuteExport ()
{
	std::vector<FileAttributes> exportList;

	for (FileIterator iter(Directory::DEV_ASSET_DIRECTORY, FileIterator::SF_IncludeFiles | FileIterator::SF_IncludeSubDirs); !iter.FinishedIterating(); ++iter)
	{
		DString fileName = iter.GetSelectedAttributes().GetName(false, true);
		if (fileName.EndsWith(EditorUi::DEV_EXTENSION, DString::CC_IgnoreCase))
		{
			exportList.push_back(iter.GetSelectedAttributes());
		}
	}

	DString sectionName = TXT("EditorUi");
	ConfigWriter* config = ConfigWriter::CreateObject();
	TextureFile* output = TextureFile::CreateObject();

	//The GraphicsEngineComponent doesn't exist when exporting textures. Create a blank one for the exporter.
	HashedString texturePath("ExportedTexture");
	TexturePool* texturePool = TexturePool::CreateObject();

	for (const FileAttributes& devAsset : exportList)
	{
		if (!config->OpenFile(devAsset, false))
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s. Unable to open config file."), devAsset.GetName(true, true));
			continue;
		}

		//Ensure the source still exists.
		DString source = config->GetPropertyText(sectionName, TXT("Source"));
		if (source.IsEmpty())
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s since the config file does not specify the image source."), devAsset.GetName(true, true));
			continue;
		}

		source = Directory::BASE_DIRECTORY.ToString() + source; //convert to abs path
		DString sourcePath;
		DString sourceName;
		FileUtils::ExtractFileName(source, OUT sourcePath, OUT sourceName);
		FileAttributes sourceFile(sourcePath, sourceName);
		if (!OS_CheckIfFileExists(sourceFile))
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s since the source file \"%s\" does not exist. Ensure the source is relative to the Engine directory."), devAsset.GetName(true, true), source);
			continue;
		}

		DString exportDest = config->GetPropertyText(sectionName, TXT("ExportDestination"));
		if (exportDest.IsEmpty())
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s since the config file does not specify the export destination."), devAsset.GetName(true, true));
			continue;
		}

		//Export destination is relative to the Base directory. Prepend the base directory to convert to abs path.
		exportDest = Directory::BASE_DIRECTORY.ToString() + exportDest;

		DString exportPath;
		DString exportName;
		FileUtils::ExtractFileName(exportDest, OUT exportPath, OUT exportName);
		Directory exportDir(exportPath, true);
		FileAttributes outputFile = FileAttributes(exportDir, exportName);

		//If file already exists, ignore.
		if (OS_CheckIfFileExists(outputFile))
		{
			TextureEditorLog.Log(LogCategory::LL_Log, TXT("%s already exists. Skipping . . ."), outputFile.GetName(true, true));
			continue;
		}
		else if (!OS_CheckIfDirectoryExists(exportDir))
		{
			TextureEditorLog.Log(LogCategory::LL_Log, TXT("Creating directory: %s"), exportPath);
			if (!OS_CreateDirectory(exportDir))
			{
				++NumErrors;
				TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s. Could not create directory %s. Ensure the ExportDestination is relative to the Engine directory."), devAsset.GetName(true, true), exportPath);
				continue;
			}
		}

		sf::Texture* sfTexture = new sf::Texture();
		{
			SfmlOutputStream sfOutput;
			if (!sfTexture->loadFromFile(sourceFile.GetName(true, true).ToCString()))
			{
				++NumErrors;
				TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s. Could not load source file %s. %s"), devAsset.GetName(true, true), sourceFile.GetName(true, true), sfOutput.ReadOutput());
				delete sfTexture;
				continue;
			}
		}

		Texture* texture = Texture::CreateObject(); //Must create a new texture for each file instead of reusing since the GenerateMipMap cannot be undone.
		texture->SetResource(sfTexture); //Takes ownership of the sfml resource
		texturePool->AddTexture(texture, texturePath);
		texture->SetOwningTexturePool(texturePool);

		std::vector<DString> tags;
		config->GetArrayValues(sectionName, TXT("Tags"), OUT tags);

		for (const DString& tag : tags)
		{
			const HashedString& newTag = texture->EditTags().emplace_back(tag);
			ResourceTag::AddTag(newTag, OUT texturePool->Tags);
		}

		Bool bWrapping = config->GetProperty<Bool>(sectionName, TXT("Wrapping"));
		texture->SetRepeat(bWrapping);

		Bool bSmooth = config->GetProperty<Bool>(sectionName, TXT("SmoothEdges"));
		texture->SetSmooth(bSmooth);

		Bool bMipMap = config->GetProperty<Bool>(sectionName, TXT("AllowMipMap"));
		if (bMipMap)
		{
			if (!texture->GenerateMipMap())
			{
				++NumErrors;
				TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s. Could not generate mipmaps for that textures."), devAsset.GetName(true, true));
				texturePool->DestroyTexture(texturePath);
				texture->Destroy(); //deletes the sfml texture resource
				continue;
			}
		}

		//Do the actual export now!
		output->SetTargetTexture(texture);
		bool bSuccess = output->SaveFile(outputFile);
		if (!bSuccess)
		{
			++NumErrors;
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to export %s. Failed to save texture to binary file %s."), devAsset.GetName(true, true), outputFile.GetName(true, true));
		}
		else
		{
			++NumExported;
			TextureEditorLog.Log(LogCategory::LL_Log, TXT("Successfully exported %s to %s."), devAsset.GetName(true, true), outputFile.GetName(true, true));
		}

		texturePool->DestroyTexture(texturePath);
		texture->Destroy();
		output->SetTargetTexture(nullptr);
	}

	//General cleanup
	config->Destroy();
	output->Destroy();
	texturePool->Destroy();
}
TE_END