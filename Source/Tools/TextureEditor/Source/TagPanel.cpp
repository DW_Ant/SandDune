/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TagPanel.cpp
=====================================================================
*/

#include "TagPanel.h"
#include "TextureEditorEngineComponent.h"
#include "TextureEditorTheme.h"

IMPLEMENT_CLASS(SD::TE::TagPanel, SD::GuiEntity)
TE_BEGIN

void TagPanel::InitProps ()
{
	Super::InitProps();

	TextureName = DString::EmptyString;

	TagFrame = nullptr;
	TagNameField = nullptr;
	AddTagButton = nullptr;
	SearchTagButton = nullptr;
	TagReferenceViewerFrame = nullptr;
	TagReferenceViewerHeader = nullptr;
	CollapseTagReferenceViewerButton = nullptr;
	ReferenceContentScrollbar = nullptr;
	ReferenceViewerContent = nullptr;
	TagListScrollbar = nullptr;
	TagList = nullptr;
	AcceptTagsButton = nullptr;
	CancelTagsButton = nullptr;

	bIgnoreTagSelectionToggle = false;
	TagListPrevSize = Vector2::ZERO_VECTOR;
	BottomScrollTick = nullptr;
}

void TagPanel::BeginObject ()
{
	TextureEditorEngineComponent* localTextureEngine = TextureEditorEngineComponent::Find();
	CHECK(localTextureEngine != nullptr)
	localTextureEngine->GetTags(OUT AllTags);

	Super::BeginObject();

	BottomScrollTick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(BottomScrollTick))
	{
		BottomScrollTick->SetTickHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleScrollToBottomTick, void, Float));
		BottomScrollTick->SetTicking(false);
	}
}

void TagPanel::Destroyed ()
{
	if (OnClose.IsBounded())
	{
		OnClose();
	}

	Super::Destroyed();
}

void TagPanel::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SetStyleName(TextureEditorTheme::TEXTURE_EDITOR_STYLE_NAME);

	//Tagging frame
	FrameComponent* blackout = FrameComponent::CreateObject();
	if (AddComponent(blackout))
	{
		blackout->SetBorderThickness(0.f);
		blackout->SetLockedFrame(true);
		blackout->SetAlwaysConsumeMouseEvents(true);
		blackout->SetCenterColor(Color(0, 0, 0, 128));
	
		TagFrame = FrameComponent::CreateObject();
		if (blackout->AddComponent(TagFrame))
		{
			TagFrame->SetAnchorTop(0.2f);
			TagFrame->SetAnchorRight(0.2f);
			TagFrame->SetAnchorBottom(0.2f);
			TagFrame->SetAnchorLeft(0.2f);
			TagFrame->SetLockedFrame(true);

			LabelComponent* tagNamePrompt = LabelComponent::CreateObject();
			if (TagFrame->AddComponent(tagNamePrompt))
			{
				tagNamePrompt->SetAutoRefresh(false);
				tagNamePrompt->SetPosition(Vector2(0.05f, 0.05f));
				tagNamePrompt->SetSize(Vector2(0.6f, 0.1f));
				tagNamePrompt->SetWrapText(false);
				tagNamePrompt->SetClampText(true);
				tagNamePrompt->SetText(translator->TranslateText(TXT("TagNamePrompt"), LOCALIZATION_FILE, TXT("TagPanel")));
				tagNamePrompt->SetAutoRefresh(true);
			}

			FrameComponent* tagNameBackground = FrameComponent::CreateObject();
			if (TagFrame->AddComponent(tagNameBackground))
			{
				tagNameBackground->SetPosition(Vector2(0.05f, 0.15f));
				tagNameBackground->SetSize(Vector2(0.6f, 0.1f));
				tagNameBackground->SetLockedFrame(true);
				tagNameBackground->SetBorderThickness(1.5f);
				tagNameBackground->SetCenterColor(Color(48, 48, 48, 255));

				TagNameField = TextFieldComponent::CreateObject();
				if (tagNameBackground->AddComponent(TagNameField))
				{
					TagNameField->SetAutoRefresh(false);
					TagNameField->OnEdit = SDFUNCTION_1PARAM(this, TagPanel, HandleSearchTagEdit, bool, const sf::Event&);
					TagNameField->OnReturn = SDFUNCTION_1PARAM(this, TagPanel, HandleTagNameReturn, void, TextFieldComponent*);
					TagNameField->bAutoSelect = true;
					TagNameField->SetHorizontalAlignment(LabelComponent::HA_Center);
					TagNameField->SetAutoRefresh(true);

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (TagNameField->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("TagNameFieldTooltip"), LOCALIZATION_FILE, TXT("TagPanel")));
					}
				}
			}

			AddTagButton = ButtonComponent::CreateObject();
			if (TagFrame->AddComponent(AddTagButton))
			{
				AddTagButton->SetPosition(Vector2(0.7f, 0.15f));
				AddTagButton->SetSize(Vector2(0.1f, 0.1f));
				AddTagButton->SetEnabled(false);
				AddTagButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleAddTagReleased, void, ButtonComponent*));
				if (AddTagButton->GetCaptionComponent() != nullptr)
				{
					AddTagButton->GetCaptionComponent()->Destroy();
				}

				if (FrameComponent* background = AddTagButton->GetBackground())
				{
					background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.AddButtonSquared")));
					AddTagButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (AddTagButton->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("AddTagTooltip"), LOCALIZATION_FILE, TXT("TagPanel")));
				}
			}

			SearchTagButton = ButtonComponent::CreateObject();
			if (TagFrame->AddComponent(SearchTagButton))
			{
				SearchTagButton->SetPosition(Vector2(0.825f, 0.15f));
				SearchTagButton->SetSize(Vector2(0.1f, 0.1f));
				SearchTagButton->SetEnabled(false);
				SearchTagButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleSearchTagReleased, void, ButtonComponent*));
				if (SearchTagButton->GetCaptionComponent() != nullptr)
				{
					SearchTagButton->GetCaptionComponent()->Destroy();
				}

				if (FrameComponent* background = SearchTagButton->GetBackground())
				{
					background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Gui.ScrollZoomButton")));
					SearchTagButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (SearchTagButton->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("SearchTagTooltip"), LOCALIZATION_FILE, TXT("TagPanel")));
				}
			}

			TagReferenceViewerFrame = FrameComponent::CreateObject();
			if (TagFrame->AddComponent(TagReferenceViewerFrame))
			{
				TagReferenceViewerFrame->SetPosition(Vector2(0.05f, 0.25f));
				TagReferenceViewerFrame->SetSize(Vector2(0.9f, 0.4f));
				TagReferenceViewerFrame->SetBorderThickness(2.f);
				TagReferenceViewerFrame->SetLockedFrame(true);
				TagReferenceViewerFrame->SetVisibility(false);

				TagReferenceViewerHeader = LabelComponent::CreateObject();
				if (TagReferenceViewerFrame->AddComponent(TagReferenceViewerHeader))
				{
					TagReferenceViewerHeader->SetAutoRefresh(false);
					TagReferenceViewerHeader->SetPosition(Vector2(0.05f, 0.05f));
					TagReferenceViewerHeader->SetSize(Vector2(0.8f, 0.175f));
					TagReferenceViewerHeader->SetWrapText(false);
					TagReferenceViewerHeader->SetClampText(true);
					TagReferenceViewerHeader->SetAutoRefresh(true);
				}

				CollapseTagReferenceViewerButton = ButtonComponent::CreateObject();
				if (TagReferenceViewerFrame->AddComponent(CollapseTagReferenceViewerButton))
				{
					CollapseTagReferenceViewerButton->SetAnchorTop(0.05f);
					CollapseTagReferenceViewerButton->SetAnchorRight(0.05f);
					CollapseTagReferenceViewerButton->SetSize(Vector2(0.075f, 0.175f));
					CollapseTagReferenceViewerButton->SetCaptionText(TXT("X"));
					CollapseTagReferenceViewerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleCollapseRefViewerReleased, void, ButtonComponent*));

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (CollapseTagReferenceViewerButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("CollapseTagReferenceViewerTooltip"), LOCALIZATION_FILE, TXT("TagPanel")));
					}
				}

				ReferenceContentScrollbar = ScrollbarComponent::CreateObject();
				if (TagReferenceViewerFrame->AddComponent(ReferenceContentScrollbar))
				{
					ReferenceContentScrollbar->SetPosition(Vector2(0.05f, 0.2f));
					ReferenceContentScrollbar->SetSize(Vector2(0.9f, 0.75f));
					ReferenceContentScrollbar->SetHideControlsWhenFull(true);

					GuiEntity* contentOwner = GuiEntity::CreateObject();
					contentOwner->SetAutoSizeVertical(true);
					contentOwner->SetAutoSizeHorizontal(true);
					ReferenceContentScrollbar->SetViewedObject(contentOwner);

					ReferenceViewerContent = LabelComponent::CreateObject();
					if (contentOwner->AddComponent(ReferenceViewerContent))
					{
						ReferenceViewerContent->SetAutoRefresh(false);
						ReferenceViewerContent->SetPosition(Vector2::ZERO_VECTOR);
						ReferenceViewerContent->SetSize(Vector2(1.f, 1.f));
						ReferenceViewerContent->SetAutoSizeVertical(true);
						ReferenceViewerContent->SetAutoSizeHorizontal(true);
						ReferenceViewerContent->SetWrapText(false);
						ReferenceViewerContent->SetClampText(false);
						ReferenceViewerContent->SetHorizontalAlignment(LabelComponent::HA_Left);
						ReferenceViewerContent->SetAutoRefresh(true);
					}
				}
			}

			TagListScrollbar = ScrollbarComponent::CreateObject();
			if (TagFrame->AddComponent(TagListScrollbar))
			{
				TagListScrollbar->SetAnchorTop(0.25f);
				TagListScrollbar->SetAnchorRight(0.05f);
				TagListScrollbar->SetAnchorBottom(0.15f);
				TagListScrollbar->SetAnchorLeft(0.05f);
				TagListScrollbar->SetHideControlsWhenFull(false);

				GuiEntity* tagListEntity = GuiEntity::CreateObject();
				tagListEntity->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
				tagListEntity->SetAutoSizeVertical(true);
				tagListEntity->SetAutoSizeCheckFrequency(0.5f);
				TagListScrollbar->SetViewedObject(tagListEntity);

				TagList = ListBoxComponent::CreateObject();
				if (tagListEntity->AddComponent(TagList))
				{
					TagList->SetPosition(Vector2::ZERO_VECTOR);
					TagList->SetSize(Vector2(1.f, 1.f));
					TagList->SetAutoDeselect(false);
					TagList->OnItemSelected = SDFUNCTION_1PARAM(this, TagPanel, HandleTagSelected, void, Int);
					TagList->OnItemDeselected = SDFUNCTION_1PARAM(this, TagPanel, HandleTagDeselected, void, Int);

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (TagList->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("TagListTooltip"), LOCALIZATION_FILE, TXT("TagPanel")));
					}
				}
			}

			AcceptTagsButton = ButtonComponent::CreateObject();
			if (TagFrame->AddComponent(AcceptTagsButton))
			{
				AcceptTagsButton->SetAnchorLeft(0.05f);
				AcceptTagsButton->SetAnchorBottom(0.04f);
				AcceptTagsButton->SetSize(Vector2(0.4f, 0.1f));
				AcceptTagsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleAcceptTagsReleased, void, ButtonComponent*));
				AcceptTagsButton->SetCaptionText(translator->TranslateText(TXT("AcceptTags"), LOCALIZATION_FILE, TXT("TagPanel")));
			}

			CancelTagsButton = ButtonComponent::CreateObject();
			if (TagFrame->AddComponent(CancelTagsButton))
			{
				CancelTagsButton->SetAnchorRight(0.05f);
				CancelTagsButton->SetAnchorBottom(0.04f);
				CancelTagsButton->SetSize(Vector2(0.4f, 0.1f));
				CancelTagsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TagPanel, HandleCancelTagsReleased, void, ButtonComponent*));
				CancelTagsButton->SetCaptionText(translator->TranslateText(TXT("CancelTags"), LOCALIZATION_FILE, TXT("TagPanel")));
			}
		}

		//Add tooltip at the end of this block to ensure this has lesser priority compared to its inner components.
		TooltipComponent* tooltipBlocker = TooltipComponent::CreateObject();
		if (blackout->AddComponent(tooltipBlocker))
		{
			//This component is used to disable tooltips behind this menu.
			tooltipBlocker->bTooltipBlocker = true;
		}
	}
}

void TagPanel::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	
	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(TagNameField);
	Focus->TabOrder.push_back(AddTagButton);
	Focus->TabOrder.push_back(SearchTagButton);
	Focus->TabOrder.push_back(CollapseTagReferenceViewerButton);
	Focus->TabOrder.push_back(TagList);
	Focus->TabOrder.push_back(AcceptTagsButton);
	Focus->TabOrder.push_back(CancelTagsButton);
}

bool TagPanel::HandleConsumableInput (const sf::Event& evnt)
{
	if (Super::HandleConsumableInput(evnt))
	{
		return true;
	}

	//If the user pressed escape while the view options or tagging popup is visible, remove popups.
	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		//Make sure the user is not typing in the tag name field.
		if (TagNameField != nullptr && TagNameField->GetCursorPosition() >= 0)
		{
			return false;
		}

		Destroy();
		return true;
	}

	return false;
}

void TagPanel::ApplyTags ()
{
	//Sort alphabetically
	std::sort(SelectedTags.begin(), SelectedTags.end(), [](const DString& a, const DString& b)
	{
		return (a.Compare(b, DString::CC_CaseSensitive) < 0);
	});

	TextureEditorEngineComponent* localTextureEngine = TextureEditorEngineComponent::Find();
	CHECK(localTextureEngine != nullptr)
	localTextureEngine->UpdateDevAsset(TextureName, SelectedTags);

	if (OnApply.IsBounded())
	{
		OnApply();
	}
}

void TagPanel::SetTextureName (const DString& newTextureName)
{
	TextureName = newTextureName;

	TextureEditorEngineComponent* localTextureEngine = TextureEditorEngineComponent::Find();
	CHECK(localTextureEngine != nullptr)
	localTextureEngine->GetTagsFromTexture(TextureName, OUT SelectedTags);

	PopulateTagList();
}

void TagPanel::PopulateTagList ()
{
	if (TagList != nullptr && TagNameField != nullptr)
	{
		std::vector<GuiDataElement<DString>> tagList;

		DString filter(DString::EmptyString);
		if (TagNameField != nullptr && !ContainerUtils::IsEmpty(TagNameField->ReadContent()) && !TagNameField->ReadContent().at(0).IsEmpty())
		{
			filter = TagNameField->GetContent();
			filter.TrimSpaces();
		}

		for (const DString& tag : AllTags)
		{
			if (!filter.IsEmpty())
			{
				const DString& searchStr = TagNameField->ReadContent().at(0);
				if (tag.Find(filter, 0, DString::CC_IgnoreCase) < 0)
				{
					continue; //Not in search string
				}
			}

			tagList.emplace_back(GuiDataElement<DString>(tag, tag));
		}

		//Sort alphabetically
		std::sort(tagList.begin(), tagList.end(), [](GuiDataElement<DString> a, GuiDataElement<DString> b)
		{
			return (a.Data.Compare(b.Data, DString::CC_CaseSensitive) < 0);
		});

		bIgnoreTagSelectionToggle = true;
		TagList->SetList(tagList);

		//Select relevant tags.
		std::vector<Int> newSelections;
		for (size_t i = 0; i < TagList->ReadList().size(); ++i)
		{
			for (const DString& selectedTag : SelectedTags)
			{
				if (TagList->ReadList().at(i)->ReadLabelText().Compare(selectedTag, DString::CC_IgnoreCase) == 0)
				{
					newSelections.push_back(Int(i));
					break;
				}
			}
		}

		TagList->SelectItemsByIdx(newSelections);
		bIgnoreTagSelectionToggle = false;
	}
}

void TagPanel::PopulateTagReferences (const DString& tagSearch)
{
	CHECK(ReferenceViewerContent != nullptr && TagReferenceViewerHeader != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	TextureEditorEngineComponent* localTextureEngine = TextureEditorEngineComponent::Find();
	CHECK(localTextureEngine != nullptr)

	std::vector<DString> tagReferences;
	localTextureEngine->FindFilesReferencingTag(tagSearch, OUT tagReferences);

	if (ContainerUtils::IsEmpty(tagReferences))
	{
		TagReferenceViewerHeader->SetText(DString::CreateFormattedString(translator->TranslateText(TXT("TagReferencesEmpty"), LOCALIZATION_FILE, TXT("TagPanel")), tagSearch));
		ReferenceViewerContent->SetText(DString::EmptyString);
	}
	else
	{
		TagReferenceViewerHeader->SetText(DString::CreateFormattedString(translator->TranslateText(TXT("TagReferences"), LOCALIZATION_FILE, TXT("TagPanel")), Int(tagReferences.size()), tagSearch));

		DString refList = DString::EmptyString;
		for (const DString& reference : tagReferences)
		{
			refList += reference + TXT("\n");
		}
	
		ReferenceViewerContent->SetText(refList);
	}
}

bool TagPanel::HandleSearchTagEdit (const sf::Event& evnt)
{
	CHECK(evnt.type == sf::Event::KeyPressed || evnt.type == sf::Event::KeyReleased)
	if (evnt.key.code == sf::Keyboard::Enter)
	{
		//Don't bother searching when the user pressed the enter key since that's going to add or toggle a tag. The HandleTagNameReturn function will handle this event.
		return false;
	}

	PopulateTagList();

	if (TagNameField != nullptr)
	{
		bool enableButtons = TagNameField->ReadContent().size() > 0 && !TagNameField->ReadContent().at(0).IsEmpty();
		if (AddTagButton != nullptr)
		{
			AddTagButton->SetEnabled(enableButtons);
		}

		if (SearchTagButton != nullptr)
		{
			SearchTagButton->SetEnabled(enableButtons);
		}
	}

	return false;
}

void TagPanel::HandleTagNameReturn (TextFieldComponent* uiComp)
{
	//Same as clicking the add button
	HandleAddTagReleased(AddTagButton);

	CHECK(uiComp != nullptr)
	uiComp->JumpCursorToBeginning(false); //refocus back to the text field in case the user is interested in adding multiple tags.
}

void TagPanel::HandleAddTagReleased (ButtonComponent* uiComp)
{
	if (TagList == nullptr)
	{
		return;
	}

	DString newTag = DString::EmptyString;
	if (TagNameField != nullptr && !ContainerUtils::IsEmpty(TagNameField->ReadContent()))
	{
		newTag = TagNameField->GetContent();
		newTag.TrimSpaces();
		TagNameField->SetText(DString::EmptyString);
		PopulateTagList();

		if (AddTagButton != nullptr)
		{
			AddTagButton->SetEnabled(false);
		}

		if (SearchTagButton != nullptr)
		{
			SearchTagButton->SetEnabled(false);
		}
	}

	if (newTag.IsEmpty())
	{
		return;
	}

	//If there's an exact match in TagList, toggle it. And correct casing.
	if (!ContainerUtils::IsEmpty(TagList->ReadList()))
	{
		//Search for exact match (ignore case)
		std::vector<BaseGuiDataElement*> tags = TagList->GetList();
		bool bFoundMatch = false;
		for (size_t i = 0; i < tags.size(); ++i)
		{
			if (GuiDataElement<DString>* tagResource = dynamic_cast<GuiDataElement<DString>*>(tags.at(i)))
			{
				if (tagResource->GetLabelText().Compare(newTag, DString::CC_IgnoreCase) == 0)
				{
					bFoundMatch = true;
					bool bUpdateCasing = (tagResource->GetLabelText().Compare(newTag, DString::CC_CaseSensitive) != 0); //insensitive passed, but sensitive did not. Update casing

					//toggle selection
					bool newSelected = !TagList->IsItemSelectedByIndex(Int(i));
					TagList->SetItemSelectionByIdx(Int(i), newSelected);
					if (bUpdateCasing)
					{
						tagResource->LabelText = newTag;
						tagResource->Data = newTag;
						TagList->UpdateTextContent();
					}

					//Update selected list
					if (!newSelected) //Remove old selection
					{
						DString::ECaseComparison caseComparison = (bUpdateCasing) ? DString::CC_IgnoreCase : DString::CC_CaseSensitive; //If there's a change in comparison, use the slower ignore compare case. Otherwise, use the faster exact match.
						for (size_t i = 0; i < SelectedTags.size(); ++i)
						{
							if (SelectedTags.at(i).Compare(newTag, caseComparison) == 0)
							{
								SelectedTags.erase(SelectedTags.begin() + i);
								break;
							}
						}
					}
					else //Add selection
					{
						SelectedTags.push_back(newTag);
					}

					break;
				}
			}
		}

		if (bFoundMatch)
		{
			return;
		}
	}

	//No matches found. Add the tag to the list.
	AllTags.push_back(newTag);
	GuiDataElement<DString> guiTag(newTag, newTag);
	size_t listIdx = TagList->AddOption(guiTag); //Don't bother sorting. All new items should appear at the end.
	TagList->AddItemSelectionByIdx(Int(listIdx));
	SelectedTags.push_back(newTag);

	//When the object inevitably changes size, have the scrollbar snap to the bottom.
	if (BottomScrollTick != nullptr && TagListScrollbar != nullptr && TagListScrollbar->GetViewedObject() != nullptr)
	{
		TagListScrollbar->GetViewedObject()->GetTotalSize(OUT TagListPrevSize);
		BottomScrollTick->SetTicking(true);
	}
}

void TagPanel::HandleSearchTagReleased (ButtonComponent* uiComp)
{
	if (TagNameField == nullptr)
	{
		return;
	}

	DString tagSearch = TagNameField->GetContent();
	if (tagSearch.IsEmpty())
	{
		return;
	}

	if (TagReferenceViewerFrame != nullptr)
	{
		TagReferenceViewerFrame->SetVisibility(true);
		PopulateTagReferences(tagSearch);
	}

	if (TagListScrollbar != nullptr)
	{
		TagListScrollbar->SetAnchorTop(0.65f);
	}
}

void TagPanel::HandleCollapseRefViewerReleased (ButtonComponent* uiComp)
{
	if (TagReferenceViewerFrame != nullptr)
	{
		TagReferenceViewerFrame->SetVisibility(false);
	}

	if (TagListScrollbar != nullptr)
	{
		TagListScrollbar->SetAnchorTop(0.25f);
	}
}

void TagPanel::HandleTagSelected (Int selectionIdx)
{
	if (bIgnoreTagSelectionToggle)
	{
		return;
	}

	if (TagList != nullptr)
	{
		const DString& selectionStr = TagList->ReadList().at(selectionIdx.ToUnsignedInt())->ReadLabelText();
		for (const DString& curTag : SelectedTags)
		{
			if (curTag.Compare(selectionStr, DString::CC_IgnoreCase) == 0)
			{
				return; //Already selected do nothing.
			}
		}

		SelectedTags.push_back(selectionStr);
	}
}

void TagPanel::HandleTagDeselected (Int selectionIdx)
{
	if (bIgnoreTagSelectionToggle)
	{
		return;
	}

	if (TagList != nullptr)
	{
		const DString& selectionStr = TagList->ReadList().at(selectionIdx.ToUnsignedInt())->ReadLabelText();
		for (size_t i = 0; i < SelectedTags.size(); ++i)
		{
			if (SelectedTags.at(i).Compare(selectionStr, DString::CC_IgnoreCase) == 0)
			{
				SelectedTags.erase(SelectedTags.begin() + i);
				break;
			}
		}
	}
}

void TagPanel::HandleScrollToBottomTick (Float deltaSec)
{
	if (TagListScrollbar != nullptr && TagListScrollbar->GetViewedObject() != nullptr && TagListScrollbar->GetFrameCamera() != nullptr)
	{
		Vector2 curSize;
		TagListScrollbar->GetViewedObject()->GetTotalSize(OUT curSize);
		if (curSize != TagListPrevSize)
		{
			TagListScrollbar->GetFrameCamera()->SetPosition(Vector2(0.f, MAX_FLOAT)); //snap to bottom

			CHECK(BottomScrollTick != nullptr)
			BottomScrollTick->SetTicking(false);
		}
	}
}

void TagPanel::HandleAcceptTagsReleased (ButtonComponent* uiComp)
{
	ApplyTags();
	Destroy();
}

void TagPanel::HandleCancelTagsReleased (ButtonComponent* uiComp)
{
	Destroy();
}
TE_END