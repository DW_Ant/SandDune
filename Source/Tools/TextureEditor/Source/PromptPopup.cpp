/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  PromptPopup.cpp
=====================================================================
*/

#include "PromptPopup.h"
#include "TextureEditorTheme.h"

IMPLEMENT_CLASS(SD::TE::PromptPopup, SD::GuiEntity)
TE_BEGIN

void PromptPopup::InitProps ()
{
	Super::InitProps();

	Blackout = nullptr;
	PromptLabel = nullptr;
	YesButton = nullptr;
	NoButton = nullptr;
	CancelButton = nullptr;
}

void PromptPopup::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SetStyleName(TextureEditorTheme::TEXTURE_EDITOR_STYLE_NAME);

	Blackout = FrameComponent::CreateObject();
	if (AddComponent(Blackout))
	{
		Blackout->SetPosition(Vector2::ZERO_VECTOR);
		Blackout->SetSize(Vector2(1.f, 1.f));
		Blackout->SetLockedFrame(true);
		Blackout->SetAlwaysConsumeMouseEvents(true);
		Blackout->SetCenterColor(Color(0, 0, 0, 128));
		Blackout->SetBorderThickness(0.f);

		if (BorderRenderComponent* borderComp = Blackout->GetBorderComp())
		{
			borderComp->Destroy();
		}

		FrameComponent* background = FrameComponent::CreateObject();
		if (Blackout->AddComponent(background))
		{
			background->SetAnchorTop(0.35f);
			background->SetAnchorRight(0.25f);
			background->SetAnchorBottom(0.35f);
			background->SetAnchorLeft(0.25f);
			background->SetLockedFrame(true);

			PromptLabel = LabelComponent::CreateObject();
			if (background->AddComponent(PromptLabel))
			{
				PromptLabel->SetAutoRefresh(false);
				Float margin = 0.02f;
				PromptLabel->SetAnchorTop(margin);
				PromptLabel->SetAnchorRight(margin);
				PromptLabel->SetAnchorBottom(0.25f + margin);
				PromptLabel->SetAnchorLeft(margin);
				PromptLabel->SetWrapText(true);
				PromptLabel->SetClampText(true);
				PromptLabel->SetCharacterSize(28);
				PromptLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
				PromptLabel->SetVerticalAlignment(LabelComponent::VA_Center);
				PromptLabel->SetAutoRefresh(true);
			}

			YesButton = ButtonComponent::CreateObject();
			if (background->AddComponent(YesButton))
			{
				YesButton->SetPosition(Vector2(0.05f, 0.75f));
				YesButton->SetSize(Vector2(0.25f, 0.15f));
				YesButton->SetCaptionText(translator->TranslateText(TXT("Yes"), LOCALIZATION_FILE, TXT("PromptPopup")));
				YesButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PromptPopup, HandleYesReleased, void, ButtonComponent*));
			}

			NoButton = ButtonComponent::CreateObject();
			if (background->AddComponent(NoButton))
			{
				NoButton->SetPosition(Vector2(0.375f, 0.75f));
				NoButton->SetSize(Vector2(0.25f, 0.15f));
				NoButton->SetCaptionText(translator->TranslateText(TXT("No"), LOCALIZATION_FILE, TXT("PromptPopup")));
				NoButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PromptPopup, HandleNoReleased, void, ButtonComponent*));
			}

			CancelButton = ButtonComponent::CreateObject();
			if (background->AddComponent(CancelButton))
			{
				CancelButton->SetPosition(Vector2(1.f, 0.75f));
				CancelButton->SetSize(Vector2(0.25f, 0.15f));
				CancelButton->SetAnchorRight(0.05f);
				CancelButton->SetCaptionText(translator->TranslateText(TXT("Cancel"), LOCALIZATION_FILE, TXT("PromptPopup")));
				CancelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PromptPopup, HandleCancelReleased, void, ButtonComponent*));
			}
		}

		//block tooltips behind this entity
		TooltipComponent* tooltipBlocker = TooltipComponent::CreateObject();
		if (Blackout->AddComponent(tooltipBlocker))
		{
			tooltipBlocker->bTooltipBlocker = true;
		}
	}
}

void PromptPopup::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	
	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(YesButton);
	Focus->TabOrder.push_back(NoButton);
	Focus->TabOrder.push_back(CancelButton);
}

bool PromptPopup::HandleConsumableInput (const sf::Event& evnt)
{
	if (Super::HandleConsumableInput(evnt))
	{
		return true;
	}

	//If the user pressed escape while the view options or tagging popup is visible, remove popups.
	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		if (CancelButton != nullptr)
		{
			CancelButton->SetButtonUp(true, true);
			return true;
		}
	}

	return false;
}

void PromptPopup::SetPromptText (const DString& newText)
{
	if (PromptLabel != nullptr)
	{
		PromptLabel->SetText(newText);
	}
}

void PromptPopup::HandleYesReleased (ButtonComponent* uiComp)
{
	if (OnAccept.IsBounded())
	{
		OnAccept();
	}

	Destroy();
}

void PromptPopup::HandleNoReleased (ButtonComponent* uiComp)
{
	if (OnNo.IsBounded())
	{
		OnNo();
	}

	Destroy();
}

void PromptPopup::HandleCancelReleased (ButtonComponent* uiComp)
{
	if (OnCancel.IsBounded())
	{
		OnCancel();
	}

	Destroy();
}
TE_END