/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  EditorUi.cpp
=====================================================================
*/

#include "EditorUi.h"
#include "PromptPopup.h"
#include "TagPanel.h"
#include "TextureEditorTheme.h"

IMPLEMENT_CLASS(SD::TE::EditorUi, SD::GuiEntity)
TE_BEGIN

const DString EditorUi::DEV_EXTENSION(TXT("txtr.ini"));
const DString EditorUi::TEXTURE_ASSET_TYPE(TXT("Texture"));
const Int EditorUi::NEW_DIALOGUE_ID(0x000000FF);
const Int EditorUi::OPEN_DIALOGUE_ID(0x0000FF00);
const Int EditorUi::SAVE_DIALOGUE_ID(0x00FF0000);
const Int EditorUi::EXPORT_DIALOGUE_ID(0xFF000000);

void EditorUi::InitProps ()
{
	Super::InitProps();

	FeedbackPassedColor = Color(96, 128, 96);
	FeedbackFailedColor = Color(128, 96, 96);

	EditedTexture = nullptr;
	TexturePath = HashedString("Tools.TextureEditor.EditedTexture");
	bDirty = false;
	BackgroundColor = Color::BLACK;

	FeedbackTimestamp = 0.f;
	FeedbackDisplayDuration = 6.f;

	CurrentTagPanel = nullptr;
	Popup = nullptr;

	MenuFrame = nullptr;
	NewButton = nullptr;
	LoadButton = nullptr;
	SaveButton = nullptr;
	ExportButton = nullptr;

	FeedbackFrame = nullptr;
	FeedbackTick = nullptr;
	FeedbackIconTransform = nullptr;
	FeedbackIcon = nullptr;
	FeedbackText = nullptr;

	PreviewFrame = nullptr;
	PreviewTransform = nullptr;
	TexturePreview = nullptr;
	ViewOptions = nullptr;
	InfoBackground = nullptr;
	SourceLabel = nullptr;
	DimensionsLabel = nullptr;

	ViewOptionsFrame = nullptr;
	TiledCheckbox = nullptr;
	CheckerboardBackground = nullptr;
	ShowInfoCheckbox = nullptr;
	BackgroundColorR = nullptr;
	BackgroundColorG = nullptr;
	BackgroundColorB = nullptr;

	TextureInspector = nullptr;
	AuthorField = nullptr;
	LicenseField = nullptr;
	ExportDestinationField = nullptr;
	TagList = nullptr;
	EditTagListButton = nullptr;
	WrappingBool = nullptr;
	SmoothEdgesBool = nullptr;
	AllowMipMapBool = nullptr;
	CommentField = nullptr;

	bCanBecomeDirty = true;
}

void EditorUi::BeginObject ()
{
	Super::BeginObject();

	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	if (localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr)
	{
		localGraphics->GetPrimaryWindow()->RegisterCanCloseDelegate(SDFUNCTION(this, EditorUi, HandleCanClose, bool));
	}
}

void EditorUi::PostAbsTransformUpdate ()
{
	Super::PostAbsTransformUpdate();

	CalcMenuTransforms();
	RefreshTexturePreview();
}

void EditorUi::Destroyed ()
{
	if (SourceLabel != nullptr)
	{
		SourceLabel->OnTransformChanged.UnregisterHandler(SDFUNCTION(this, EditorUi, HandleSourceLabelTransformChanged, void));
	}

	if (CurrentTagPanel != nullptr)
	{
		CurrentTagPanel->Destroy();
		CurrentTagPanel = nullptr;
	}

	if (Popup != nullptr)
	{
		Popup->Destroy();
		Popup = nullptr;
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (EditedTexture != nullptr && localTexturePool != nullptr)
	{
		localTexturePool->DestroyTexture(TexturePath);
		EditedTexture = nullptr;
	}

	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	if (localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr)
	{
		localGraphics->GetPrimaryWindow()->UnregisterCanCloseDelegate(SDFUNCTION(this, EditorUi, HandleCanClose, bool));
	}

	Super::Destroyed();
}

void EditorUi::ConstructUI ()
{
	Super::ConstructUI();

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SetStyleName(TextureEditorTheme::TEXTURE_EDITOR_STYLE_NAME);

	//Menu bar
	MenuFrame = FrameComponent::CreateObject();
	if (AddComponent(MenuFrame))
	{
		MenuFrame->SetPosition(Vector2::ZERO_VECTOR);
		MenuFrame->SetSize(Vector2(1.f, 0.1f));
		MenuFrame->SetLockedFrame(true);
		MenuFrame->SetBorderThickness(4.f);
		MenuFrame->SetCenterColor(Color(48, 48, 48, 255));

		if (BorderRenderComponent* borderComp = MenuFrame->GetBorderComp())
		{
			borderComp->SetBorderTexture(nullptr);
			borderComp->SetBorderColor(Color(64, 64, 64, 255));
		}

		NewButton = ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(NewButton))
		{
			NewButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleNewButtonReleased, void, ButtonComponent*));
			if (NewButton->GetCaptionComponent() != nullptr)
			{
				NewButton->GetCaptionComponent()->Destroy();
			}

			if (FrameComponent* background = NewButton->GetBackground())
			{
				background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.NewButton")));
				NewButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (NewButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("NewButton"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		LoadButton = ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(LoadButton))
		{
			LoadButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleLoadButtonReleased, void, ButtonComponent*));
			if (LoadButton->GetCaptionComponent() != nullptr)
			{
				LoadButton->GetCaptionComponent()->Destroy();
			}

			if (FrameComponent* background = LoadButton->GetBackground())
			{
				background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.OpenButton")));
				LoadButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (LoadButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("LoadButton"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		SaveButton = ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(SaveButton))
		{
			SaveButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleSaveButtonReleased, void, ButtonComponent*));
			if (SaveButton->GetCaptionComponent() != nullptr)
			{
				SaveButton->GetCaptionComponent()->Destroy();
			}

			if (FrameComponent* background = SaveButton->GetBackground())
			{
				background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.SaveButton")));
				SaveButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (SaveButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("SaveButton"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		ExportButton = ButtonComponent::CreateObject();
		if (MenuFrame->AddComponent(ExportButton))
		{
			ExportButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleExportButtonReleased, void, ButtonComponent*));
			if (ExportButton->GetCaptionComponent() != nullptr)
			{
				ExportButton->GetCaptionComponent()->Destroy();
			}

			if (FrameComponent* background = ExportButton->GetBackground())
			{
				background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.ExportButton")));
				ExportButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (ExportButton->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("ExportButton"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		//Feedback icon
		FeedbackFrame = FrameComponent::CreateObject();
		if (MenuFrame->AddComponent(FeedbackFrame))
		{
			FeedbackFrame->SetAnchorTop(0.f);
			FeedbackFrame->SetAnchorRight(0.f);
			FeedbackFrame->SetSize(Vector2(0.25f, 1.f));
			FeedbackFrame->SetBorderThickness(0.f);
			FeedbackFrame->SetLockedFrame(true);
			FeedbackFrame->SetVisibility(false);

			if (BorderRenderComponent* borderComp = FeedbackFrame->GetBorderComp())
			{
				borderComp->Destroy();
			}

			FeedbackTick = TickComponent::CreateObject(TICK_GROUP_GUI);
			if (FeedbackFrame->AddComponent(FeedbackTick))
			{
				FeedbackTick->SetTickHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleFeedbackTick, void, Float));
				FeedbackTick->SetTicking(false);
			}

			FeedbackIconTransform = PlanarTransformComponent::CreateObject();
			if (FeedbackFrame->AddComponent(FeedbackIconTransform))
			{
				FeedbackIconTransform->SetAnchorTop(0.f);
				FeedbackIconTransform->SetAnchorLeft(0.f);
				//Size will be set when menu bar computes its transform to ensure it's squared.

				FeedbackIcon = SpriteComponent::CreateObject();
				if (FeedbackIconTransform->AddComponent(FeedbackIcon))
				{
					FeedbackIcon->SetSpriteTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.PassFail")));
				}
			}

			FeedbackText = LabelComponent::CreateObject();
			if (FeedbackFrame->AddComponent(FeedbackText))
			{
				FeedbackText->SetAutoRefresh(false);
				FeedbackText->SetAnchorTop(0.01f);
				FeedbackText->SetAnchorRight(0.05f);
				FeedbackText->SetAnchorBottom(0.01f);
				FeedbackText->SetAnchorLeft(0.25f);
				FeedbackText->SetVerticalAlignment(LabelComponent::VA_Center);
				FeedbackText->SetHorizontalAlignment(LabelComponent::HA_Right);
				FeedbackText->SetWrapText(true);
				FeedbackText->SetClampText(true);
				FeedbackText->SetCharacterSize(16);
				FeedbackText->SetAutoRefresh(true);
			}
		}
	}
	
	//Viewport
	PreviewFrame = FrameComponent::CreateObject();
	if (AddComponent(PreviewFrame))
	{
		PreviewFrame->SetPosition(Vector2(0.f, 0.1f));
		PreviewFrame->SetSize(Vector2(1.f, 0.45f));
		PreviewFrame->SetLockedFrame(true);
		PreviewFrame->SetCenterColor(Color::BLACK);

		if (BorderRenderComponent* borderComp = PreviewFrame->GetBorderComp())
		{
			borderComp->SetBorderTexture(nullptr);
			borderComp->SetBorderColor(Color(32, 32, 32));
		}

		PreviewTransform = PlanarTransformComponent::CreateObject();
		if (PreviewFrame->AddComponent(PreviewTransform))
		{
			PreviewTransform->SetSize(Vector2(0.5f, 0.5f));
			PreviewTransform->SetPosition(Vector2(0.25f, 0.25f));

			TexturePreview = SpriteComponent::CreateObject();
			if (PreviewTransform->AddComponent(TexturePreview))
			{
				//Noop
			}
		}

		ViewOptions = ButtonComponent::CreateObject();
		if (PreviewFrame->AddComponent(ViewOptions))
		{
			ViewOptions->SetPosition(Vector2(0.01f, 0.01f));
			ViewOptions->SetSize(Vector2(32.f, 32.f));
			ViewOptions->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleViewOptionsReleased, void, ButtonComponent*));
			if (ViewOptions->GetCaptionComponent() != nullptr)
			{
				ViewOptions->GetCaptionComponent()->Destroy();
			}

			if (FrameComponent* background = ViewOptions->GetBackground())
			{
				background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.ViewButton")));
				ViewOptions->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			TooltipComponent* tooltip = TooltipComponent::CreateObject();
			if (ViewOptions->AddComponent(tooltip))
			{
				tooltip->SetTooltipText(translator->TranslateText(TXT("ViewOptions"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		InfoBackground = FrameComponent::CreateObject();
		if (PreviewFrame->AddComponent(InfoBackground))
		{
			InfoBackground->SetAnchorTop(0.f);
			InfoBackground->SetAnchorRight(0.f);
			InfoBackground->SetSize(Vector2(0.1f, 0.15f));
			InfoBackground->SetBorderThickness(2.f);
			InfoBackground->SetLockedFrame(true);
			InfoBackground->SetCenterColor(Color(24, 24, 24, 192));

			if (BorderRenderComponent* borderComp = InfoBackground->GetBorderComp())
			{
				borderComp->SetBorderTexture(nullptr);
				borderComp->SetBorderColor(Color(24, 24, 24));
			}

			SourceLabel = LabelComponent::CreateObject();
			if (InfoBackground->AddComponent(SourceLabel))
			{
				SourceLabel->SetAutoRefresh(false);
				SourceLabel->SetAnchorTop(0.005f);
				SourceLabel->SetAnchorRight(8.f);
				SourceLabel->SetSize(Vector2(1.f, 0.5f));
				SourceLabel->SetAutoSizeHorizontal(true);
				SourceLabel->SetWrapText(false);
				SourceLabel->SetClampText(false);
				SourceLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
				SourceLabel->SetAutoRefresh(true);
				SourceLabel->OnTransformChanged.RegisterHandler(SDFUNCTION(this, EditorUi, HandleSourceLabelTransformChanged, void));
			}

			DimensionsLabel = LabelComponent::CreateObject();
			if (InfoBackground->AddComponent(DimensionsLabel))
			{
				DimensionsLabel->SetAutoRefresh(false);
				DimensionsLabel->SetAnchorRight(8.f);
				DimensionsLabel->SetAnchorBottom(0.005f);
				DimensionsLabel->SetSize(Vector2(0.8f, 0.5f));
				DimensionsLabel->SetAutoSizeHorizontal(true);
				DimensionsLabel->SetWrapText(false);
				DimensionsLabel->SetClampText(false);
				DimensionsLabel->SetHorizontalAlignment(LabelComponent::HA_Right);
				DimensionsLabel->SetAutoRefresh(true);
			}
		}
	}

	//View options
	ViewOptionsFrame = VerticalList::CreateObject();
	if (AddComponent(ViewOptionsFrame))
	{
		ViewOptionsFrame->SetPosition(Vector2(0.f, 0.15f));
		ViewOptionsFrame->SetSize(Vector2(0.45f, 0.25f));
		ViewOptionsFrame->SetVisibility(false);
		if (ViewOptionsFrame->GetRefreshingTick() != nullptr)
		{
			ViewOptionsFrame->GetRefreshingTick()->Destroy();
		}

		ColorRenderComponent* frameBackground = ColorRenderComponent::CreateObject();
		if (ViewOptionsFrame->AddComponent(frameBackground))
		{
			frameBackground->SolidColor = Color(32, 32, 32, 200);
		}

		Vector2 optionSize(1.f, 24.f);
		Vector2 optionPos(Vector2::ZERO_VECTOR);

		TiledCheckbox = EditableBool::CreateObject();
		if (ViewOptionsFrame->AddComponent(TiledCheckbox))
		{
			TiledCheckbox->SetPosition(optionPos);
			TiledCheckbox->SetSize(optionSize);
			TiledCheckbox->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleTiledCheckboxToggle, void, EditPropertyComponent*);
			if (ButtonComponent* resetDefaults = TiledCheckbox->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (TiledCheckbox->GetCheckbox() != nullptr && TiledCheckbox->GetCheckbox()->GetCaptionComponent() != nullptr)
			{
				TiledCheckbox->GetCheckbox()->GetCaptionComponent()->SetText(translator->TranslateText(TXT("TiledCheckbox"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		CheckerboardBackground = EditableBool::CreateObject();
		if (ViewOptionsFrame->AddComponent(CheckerboardBackground))
		{
			CheckerboardBackground->SetPosition(optionPos);
			CheckerboardBackground->SetSize(optionSize);
			CheckerboardBackground->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleCheckerboardToggle, void, EditPropertyComponent*);
			if (ButtonComponent* resetDefaults = CheckerboardBackground->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (CheckerboardBackground->GetCheckbox() != nullptr && CheckerboardBackground->GetCheckbox()->GetCaptionComponent() != nullptr)
			{
				CheckerboardBackground->GetCheckbox()->GetCaptionComponent()->SetText(translator->TranslateText(TXT("CheckerboardBackground"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		ShowInfoCheckbox = EditableBool::CreateObject();
		if (ViewOptionsFrame->AddComponent(ShowInfoCheckbox))
		{
			ShowInfoCheckbox->SetPosition(optionPos);
			ShowInfoCheckbox->SetSize(optionSize);
			ShowInfoCheckbox->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleShowInfoToggle, void, EditPropertyComponent*);
			if (ButtonComponent* resetDefaults = ShowInfoCheckbox->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (ShowInfoCheckbox->GetCheckbox() != nullptr && ShowInfoCheckbox->GetCheckbox()->GetCaptionComponent() != nullptr)
			{
				ShowInfoCheckbox->GetCheckbox()->SetChecked(true);
				ShowInfoCheckbox->GetCheckbox()->GetCaptionComponent()->SetText(translator->TranslateText(TXT("ShowInfoCheckbox"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		BackgroundColorR = EditableInt::CreateObject();
		if (ViewOptionsFrame->AddComponent(BackgroundColorR))
		{
			BackgroundColorR->SetPosition(optionPos);
			BackgroundColorR->SetSize(optionSize);
			BackgroundColorR->SetMinValue(0);
			BackgroundColorR->SetMaxValue(255);
			BackgroundColorR->SetDefaultValue(0);
			BackgroundColorR->ResetToDefaults();
			BackgroundColorR->OnApplyEdit  = SDFUNCTION_1PARAM(this, EditorUi, HandleBackgroundColorEdit, void, EditPropertyComponent*);

			if (ButtonComponent* resetDefaults = BackgroundColorR->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (BackgroundColorR->GetPropertyName() != nullptr)
			{
				BackgroundColorR->GetPropertyName()->SetText(translator->TranslateText(TXT("BackgroundColorR"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		BackgroundColorG = EditableInt::CreateObject();
		if (ViewOptionsFrame->AddComponent(BackgroundColorG))
		{
			BackgroundColorG->SetPosition(optionPos);
			BackgroundColorG->SetSize(optionSize);
			BackgroundColorG->SetMinValue(0);
			BackgroundColorG->SetMaxValue(255);
			BackgroundColorG->SetDefaultValue(0);
			BackgroundColorG->ResetToDefaults();
			BackgroundColorG->OnApplyEdit  = SDFUNCTION_1PARAM(this, EditorUi, HandleBackgroundColorEdit, void, EditPropertyComponent*);
			
			if (ButtonComponent* resetDefaults = BackgroundColorG->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (BackgroundColorG->GetPropertyName() != nullptr)
			{
				BackgroundColorG->GetPropertyName()->SetText(translator->TranslateText(TXT("BackgroundColorG"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		BackgroundColorB = EditableInt::CreateObject();
		if (ViewOptionsFrame->AddComponent(BackgroundColorB))
		{
			BackgroundColorB->SetPosition(optionPos);
			BackgroundColorB->SetSize(optionSize);
			BackgroundColorB->SetMinValue(0);
			BackgroundColorB->SetMaxValue(255);
			BackgroundColorB->SetDefaultValue(0);
			BackgroundColorB->ResetToDefaults();
			BackgroundColorB->OnApplyEdit  = SDFUNCTION_1PARAM(this, EditorUi, HandleBackgroundColorEdit, void, EditPropertyComponent*);
			
			if (ButtonComponent* resetDefaults = BackgroundColorB->GetResetDefaultsButton())
			{
				resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			}

			if (BackgroundColorB->GetPropertyName() != nullptr)
			{
				BackgroundColorB->GetPropertyName()->SetText(translator->TranslateText(TXT("BackgroundColorB"), LOCALIZATION_FILE, TXT("EditorUi")));
			}
		}

		ViewOptionsFrame->RefreshComponentTransforms();
	}

	//Texture editable properties
	ScrollbarComponent* inspectorScrollbar = ScrollbarComponent::CreateObject();
	if (AddComponent(inspectorScrollbar))
	{
		inspectorScrollbar->SetPosition(Vector2(0.f, 0.55f));
		inspectorScrollbar->SetSize(Vector2(1.f, 0.45f));
		inspectorScrollbar->SetHideControlsWhenFull(true);

		GuiEntity* inspector = GuiEntity::CreateObject();
		inspector->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));
		inspector->SetAutoSizeVertical(true);
		inspectorScrollbar->SetViewedObject(inspector);

		TextureInspector = VerticalList::CreateObject();
		if (inspector->AddComponent(TextureInspector))
		{
			TextureInspector->SetPosition(Vector2::ZERO_VECTOR);
			TextureInspector->SetSize(Vector2(1.f, 10.f));

			Vector2 fieldSize(1.f, 24.f);
			Vector2 fieldPos(Vector2::ZERO_VECTOR);

			AuthorField = EditableDString::CreateObject();
			if (TextureInspector->AddComponent(AuthorField))
			{
				AuthorField->SetSize(fieldSize);
				AuthorField->SetPosition(fieldPos);
				AuthorField->SetBarrierPosition(0.2f);
				AuthorField->SetDefaultValue(TXT("Anonymous"));
				AuthorField->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandlePropertyEdit, void, EditPropertyComponent*);
				
				if (ButtonComponent* resetDefaults = AuthorField->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (AuthorField->GetPropertyName() != nullptr)
				{
					AuthorField->GetPropertyName()->SetText(translator->TranslateText(TXT("AuthorField"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (AuthorField->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("AuthorFieldTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			LicenseField = EditableDString::CreateObject();
			if (TextureInspector->AddComponent(LicenseField))
			{
				LicenseField->SetLineHeight(LicenseField->GetLineHeight() * 3.f);
				LicenseField->SetPosition(fieldPos);
				LicenseField->SetLineType(EditableDString::LT_MultiLine);
				LicenseField->SetBarrierPosition(0.2f);
				LicenseField->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandlePropertyEdit, void, EditPropertyComponent*);
				
				if (ButtonComponent* resetDefaults = LicenseField->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (LicenseField->GetPropertyName() != nullptr)
				{
					LicenseField->GetPropertyName()->SetText(translator->TranslateText(TXT("LicenseField"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (LicenseField->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("LicenseFieldTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			ExportDestinationField = EditableDString::CreateObject();
			if (TextureInspector->AddComponent(ExportDestinationField))
			{
				ExportDestinationField->SetSize(fieldSize);
				ExportDestinationField->SetPosition(fieldPos);
				ExportDestinationField->SetBarrierPosition(0.2f);
				ExportDestinationField->SetDefaultValue(DString::EmptyString);
				ExportDestinationField->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleExportDestinationEdit, void, EditPropertyComponent*);
				
				if (ButtonComponent* resetDefaults = ExportDestinationField->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (ExportDestinationField->GetPropertyName() != nullptr)
				{
					ExportDestinationField->GetPropertyName()->SetText(translator->TranslateText(TXT("ExportDestinationField"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				ButtonComponent* editExportDestButton = ExportDestinationField->AddFieldControl();
				if (editExportDestButton != nullptr)
				{
					editExportDestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleEditExportDestinationReleased, void, ButtonComponent*));
					if (FrameComponent* background = editExportDestButton->GetBackground())
					{
						background->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.ElipsesButton")));
						editExportDestButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
					}

					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (editExportDestButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("EditExportDestButtonTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
					}
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (ExportDestinationField->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("ExportDestinationFieldTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			TagList = EditableArray::CreateObject();
			if (TextureInspector->AddComponent(TagList))
			{
				TagList->SetSize(fieldSize);
				TagList->SetPosition(fieldPos);
				TagList->SetBarrierPosition(0.2f);
				TagList->SetElementHeight(fieldSize.Y);
				TagList->SetReadOnly(true);
				TagList->EditItemTemplate().AddProp(new EditableDString::SInspectorString(TXT("Default"), DString::EmptyString, nullptr));
				TagList->BindVariable(&Tags, DString::EmptyString);
				EditTagListButton = TagList->AddFieldControl();
				if (EditTagListButton != nullptr)
				{
					EditTagListButton->GetBackground()->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Editor.EditButton")));
					EditTagListButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, EditorUi, HandleEditTagsReleased, void, ButtonComponent*));
					EditTagListButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
					TooltipComponent* tooltip = TooltipComponent::CreateObject();
					if (EditTagListButton->AddComponent(tooltip))
					{
						tooltip->SetTooltipText(translator->TranslateText(TXT("EditTagsTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
					}
				}

				if (TagList->GetPropertyName() != nullptr)
				{
					TagList->GetPropertyName()->SetText(translator->TranslateText(TXT("TagList"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TagList->ConstructArray();
			}

			WrappingBool = EditableBool::CreateObject();
			if (TextureInspector->AddComponent(WrappingBool))
			{
				WrappingBool->SetSize(fieldSize);
				WrappingBool->SetPosition(fieldPos);
				WrappingBool->SetDefaultValue(true);
				WrappingBool->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleToggleWrapping, void, EditPropertyComponent*);

				if (ButtonComponent* resetDefaults = WrappingBool->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (WrappingBool->GetPropertyName() != nullptr)
				{
					WrappingBool->GetPropertyName()->SetText(translator->TranslateText(TXT("WrappingBool"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (WrappingBool->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("WrappingBoolTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			SmoothEdgesBool = EditableBool::CreateObject();
			if (TextureInspector->AddComponent(SmoothEdgesBool))
			{
				SmoothEdgesBool->SetSize(fieldSize);
				SmoothEdgesBool->SetPosition(fieldPos);
				SmoothEdgesBool->SetDefaultValue(true);
				SmoothEdgesBool->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleToggleSmoothEdges, void, EditPropertyComponent*);
				if (ButtonComponent* resetDefaults = SmoothEdgesBool->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (SmoothEdgesBool->GetPropertyName() != nullptr)
				{
					SmoothEdgesBool->GetPropertyName()->SetText(translator->TranslateText(TXT("SmoothEdgesBool"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (SmoothEdgesBool->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("SmoothEdgesBoolTooltip"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			AllowMipMapBool = EditableBool::CreateObject();
			if (TextureInspector->AddComponent(AllowMipMapBool))
			{
				AllowMipMapBool->SetSize(fieldSize);
				AllowMipMapBool->SetPosition(fieldPos);
				AllowMipMapBool->SetDefaultValue(true);
				AllowMipMapBool->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandleToggleMipMap, void, EditPropertyComponent*);

				if (ButtonComponent* resetDefaults = AllowMipMapBool->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (AllowMipMapBool->GetPropertyName() != nullptr)
				{
					AllowMipMapBool->GetPropertyName()->SetText(translator->TranslateText(TXT("AllowMipMapBool"), LOCALIZATION_FILE, TXT("EditorUi")));
				}

				TooltipComponent* tooltip = TooltipComponent::CreateObject();
				if (AllowMipMapBool->AddComponent(tooltip))
				{
					tooltip->SetTooltipText(translator->TranslateText(TXT("AllowMipMapBoolTooltips"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}

			CommentField = EditableDString::CreateObject();
			if (TextureInspector->AddComponent(CommentField))
			{
				CommentField->SetLineHeight(CommentField->GetLineHeight() * 4.f);
				CommentField->SetPosition(fieldPos);
				CommentField->SetLineType(EditableDString::LT_MultiLine);
				CommentField->SetBarrierPosition(0.2f);
				CommentField->OnApplyEdit = SDFUNCTION_1PARAM(this, EditorUi, HandlePropertyEdit, void, EditPropertyComponent*);
				
				if (ButtonComponent* resetDefaults = CommentField->GetResetDefaultsButton())
				{
					resetDefaults->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				}

				if (CommentField->GetPropertyName() != nullptr)
				{
					CommentField->GetPropertyName()->SetText(translator->TranslateText(TXT("CommentField"), LOCALIZATION_FILE, TXT("EditorUi")));
				}
			}
		}
	}
}

void EditorUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	
	CHECK(Focus.IsValid())

	Focus->TabOrder.push_back(NewButton);
	Focus->TabOrder.push_back(LoadButton);
	Focus->TabOrder.push_back(SaveButton);
	Focus->TabOrder.push_back(ExportButton);

	Focus->TabOrder.push_back(ViewOptions);

	Focus->TabOrder.push_back(TiledCheckbox->GetCheckbox());
	Focus->TabOrder.push_back(CheckerboardBackground->GetCheckbox());
	Focus->TabOrder.push_back(BackgroundColorR->GetPropValueField());
	Focus->TabOrder.push_back(BackgroundColorG->GetPropValueField());
	Focus->TabOrder.push_back(BackgroundColorB->GetPropValueField());

	Focus->TabOrder.push_back(AuthorField->GetPropValueField());
	Focus->TabOrder.push_back(LicenseField->GetPropValueField());
	Focus->TabOrder.push_back(EditTagListButton);
	Focus->TabOrder.push_back(WrappingBool->GetCheckbox());
	Focus->TabOrder.push_back(SmoothEdgesBool->GetCheckbox());
	Focus->TabOrder.push_back(AllowMipMapBool->GetCheckbox());
	Focus->TabOrder.push_back(CommentField->GetPropValueField());
}

bool EditorUi::HandleConsumableInput (const sf::Event& evnt)
{
	if (Super::HandleConsumableInput(evnt))
	{
		return true;
	}

	//If the user pressed escape while the view options or tagging popup is visible, remove popups.
	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Escape)
	{
		if (ViewOptionsFrame != nullptr && ViewOptionsFrame->IsVisible())
		{
			HideViewOptions();
			return true;
		}
	}

	//Handle ctrl commands
	if (InputBroadcaster::GetCtrlHeld() && Popup == nullptr && CurrentTagPanel == nullptr)
	{
		switch(evnt.key.code)
		{
			case(sf::Keyboard::N):
				HandleNewButtonReleased(NewButton);
				return true;

			case(sf::Keyboard::O):
				HandleLoadButtonReleased(LoadButton);
				return true;

			case(sf::Keyboard::S):
				HandleSaveButtonReleased(SaveButton);
				return true;

			case(sf::Keyboard::E):
				HandleExportButtonReleased(ExportButton);
				return true;
		}
	}

	return false;
}

bool EditorUi::HandleConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Vector2 mousePos(sfmlEvent.x, sfmlEvent.y);

	//Collapse ViewOptions if the user clicked outside of it.
	if (ViewOptionsFrame != nullptr && ViewOptionsFrame->IsVisible())
	{
		if (!ViewOptionsFrame->IsWithinBounds(mousePos))
		{
			if (eventType == sf::Event::MouseButtonReleased)
			{
				HideViewOptions();
			}

			return true;
		}
	}

	return Super::HandleConsumableMouseClick(mouse, sfmlEvent, eventType);
}

void EditorUi::SaveCurrentTexture (const FileAttributes& file)
{
	if (EditedTexture == nullptr)
	{
		return;
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	ConfigWriter* writer = ConfigWriter::CreateObject();
	if (!writer->OpenFile(file, false))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackSaveFailed"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		writer->Destroy();
		return;
	}

	OpenedDevFile = file;
	SetWindowTitle(OpenedDevFile.GetName(false, true));
	const DString sectionName = TXT("EditorUi");

	//Save the type to warn other editors what type of data resides here, and it permits the texture editor to open this file.
	writer->SavePropertyText(DString::EmptyString, ASSET_TYPE, TEXTURE_ASSET_TYPE);

	if (SourceLabel != nullptr)
	{
		writer->SavePropertyText(sectionName, TXT("Source"), SourceLabel->GetContent());
	}

	if (AuthorField != nullptr && AuthorField->GetPropValueField() != nullptr)
	{
		writer->SavePropertyText(sectionName, TXT("Author"), AuthorField->GetPropValueField()->GetContent());
	}

	if (LicenseField != nullptr && LicenseField->GetPropValueField() != nullptr)
	{
		writer->SavePropertyText(sectionName, TXT("License"), LicenseField->GetPropValueField()->GetContent());
	}

	if (ExportDestinationField != nullptr && ExportDestinationField->GetPropValueField() != nullptr)
	{
		writer->SavePropertyText(sectionName, TXT("ExportDestination"), ExportDestinationField->GetPropValueField()->GetContent());
	}

	writer->SaveArray(sectionName, TXT("Tags"), Tags);

	if (WrappingBool != nullptr && WrappingBool->GetCheckbox() != nullptr)
	{
		writer->SaveProperty<Bool>(sectionName, TXT("Wrapping"), WrappingBool->GetCheckbox()->IsChecked());
	}

	if (SmoothEdgesBool != nullptr && SmoothEdgesBool->GetCheckbox() != nullptr)
	{
		writer->SaveProperty<Bool>(sectionName, TXT("SmoothEdges"), SmoothEdgesBool->GetCheckbox()->IsChecked());
	}

	if (AllowMipMapBool != nullptr && AllowMipMapBool->GetCheckbox() != nullptr)
	{
		writer->SaveProperty<Bool>(sectionName, TXT("AllowMipMap"), AllowMipMapBool->GetCheckbox()->IsChecked());
	}

	if (CommentField != nullptr && CommentField->GetPropValueField() != nullptr)
	{
		writer->SavePropertyText(sectionName, TXT("Comment"), CommentField->GetPropValueField()->GetContent());
	}

	writer->SaveConfig();
	writer->Destroy();
	UnmarkDirty();
	DisplayFeedback(translator->TranslateText(TXT("FeedbackSaveSuccess"), LOCALIZATION_FILE, TXT("EditorUi")), true);
}

void EditorUi::DisplayFeedback (const DString& feedbackText, bool bSuccess)
{
	if (FeedbackFrame == nullptr || FeedbackTick == nullptr || FeedbackIcon == nullptr || FeedbackText == nullptr)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to display feedback since the editor is missing some of its feedback components."));
		return;
	}

	FeedbackFrame->SetVisibility(true);
	FeedbackFrame->SetCenterColor((bSuccess) ? FeedbackPassedColor : FeedbackFailedColor);

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	FeedbackTimestamp = localEngine->GetElapsedTime();
	FeedbackTick->SetTicking(true);

	Int vertIdx = (bSuccess) ? 0 : 1;
	FeedbackIcon->SetSubDivision(1, 2, 0, vertIdx);

	FeedbackText->SetText(feedbackText);
}

void EditorUi::MarkDirty ()
{
	if (bCanBecomeDirty && !bDirty)
	{
		bDirty = true;

		if (!OpenedDevFile.ReadFileName().IsEmpty())
		{
			//Add a * in the title
			SetWindowTitle(TXT("*") + OpenedDevFile.GetName(false, true));
		}
	}
}

void EditorUi::UnmarkDirty ()
{
	if (bDirty)
	{
		bDirty = false;

		if (!OpenedDevFile.ReadFileName().IsEmpty())
		{
			//Remove the * from its title
			SetWindowTitle(OpenedDevFile.GetName(false, true));
		}
	}
}

void EditorUi::DisplayPrompt ()
{
	if (Popup != nullptr)
	{
		return; //Popup is already up
	}

	Popup = PromptPopup::CreateObject();
	Popup->RegisterToMainWindow(true, true, 1000);
}

void EditorUi::SetWindowTitle (const DString& newTitle)
{
	GraphicsEngineComponent* localGraphics = GraphicsEngineComponent::Find();
	if (localGraphics != nullptr)
	{
		if (Window* primaryWindow = localGraphics->GetPrimaryWindow())
		{
			if (sf::Window* resource = primaryWindow->GetResource())
			{
				static DString titleSuffix = TXT(" - ") + ProjectName; //Project name should always appear at the end.
				resource->setTitle((newTitle + titleSuffix).ToSfmlString());
			}
		}
	}
}

void EditorUi::CalcMenuTransforms ()
{
	if (MenuFrame == nullptr)
	{
		return;
	}

	Float buttonSpacing = 8.f;
	Vector2 buttonSize;
	buttonSize.Y = (MenuFrame->ReadCachedAbsSize().Y - (buttonSpacing * 2.f));
	buttonSize.X = buttonSize.Y;
	Vector2 buttonPos(buttonSpacing, buttonSpacing);

	if (NewButton != nullptr)
	{
		NewButton->SetSize(buttonSize);
		NewButton->SetPosition(buttonPos);
		buttonPos.X += buttonSpacing + buttonSize.X;
	}

	if (LoadButton != nullptr)
	{
		LoadButton->SetSize(buttonSize);
		LoadButton->SetPosition(buttonPos);
		buttonPos.X += buttonSpacing + buttonSize.X;
	}

	if (SaveButton != nullptr)
	{
		SaveButton->SetSize(buttonSize);
		SaveButton->SetPosition(buttonPos);
		buttonPos.X += buttonSpacing + buttonSize.X;
	}

	if (ExportButton != nullptr)
	{
		ExportButton->SetSize(buttonSize);
		ExportButton->SetPosition(buttonPos);
		buttonPos.X += buttonSpacing + buttonSize.X;
	}

	if (FeedbackIconTransform != nullptr)
	{
		FeedbackIconTransform->SetSize(buttonSize);
		FeedbackIconTransform->SetAnchorTop(buttonSpacing);
		FeedbackIconTransform->SetAnchorLeft(buttonSpacing);
	}
}

void EditorUi::RefreshTexturePreview ()
{
	if (EditedTexture == nullptr || TexturePreview == nullptr || PreviewTransform == nullptr || PreviewFrame == nullptr)
	{
		return;
	}

	if (TexturePreview->GetDrawMode() == SpriteComponent::DM_Stretch)
	{
		Vector2 textureDimensions;
		EditedTexture->GetDimensions(OUT textureDimensions);

		//Maximum number of tiles this texture can fit in the viewport along the X/Y axis.
		Float maxXTiles = PreviewFrame->ReadCachedAbsSize().X / textureDimensions.X;
		Float maxYTiles = PreviewFrame->ReadCachedAbsSize().Y / textureDimensions.Y;

		//Take the lesser multiplier of the two dimensions to clamp by border collision
		Float lesserMultiplier = Utils::Min(maxXTiles, maxYTiles);

		PreviewTransform->SetSize(textureDimensions.X * lesserMultiplier, textureDimensions.Y * lesserMultiplier);

		//Center the preview
		Vector2 deltaSize = PreviewFrame->ReadCachedAbsSize() - PreviewTransform->ReadSize();
		PreviewTransform->SetPosition(deltaSize * 0.5f);
		TexturePreview->SetSubDivision(1, 1, 0, 0); //Reset texture region to draw one tile.
	}
	else //tiled mode
	{
		PreviewTransform->SetSize(Vector2(1.f, 1.f));
		PreviewTransform->SetPosition(Vector2::ZERO_VECTOR);
	}
}

DString EditorUi::FormatExportDestination (const DString& exportDest) const
{
	if (exportDest.IsEmpty())
	{
		//Leave it blank since the user may want to use the auto populate option whenever they save the dev asset.
		return DString::EmptyString;
	}

	DString result = exportDest;

	//Swap character slashes to make the comparisons easier (and since they can be used interchangeably
	if (Directory::DIRECTORY_SEPARATOR == '\\' || Directory::DIRECTORY_SEPARATOR == '/')
	{
		DString wrongCharSlash = (Directory::DIRECTORY_SEPARATOR == '\\') ? '/' : '\\';
		result.ReplaceInline(wrongCharSlash, Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
	}

	DString engineDir = Directory::BASE_DIRECTORY.ToString();

	if (engineDir.Length() < result.Length() && result.StartsWith(engineDir, DString::CC_IgnoreCase))
	{
		//Cut out the paths before the engine path
		result = result.SubString(engineDir.Length());
	}

	//Ensure it's txtr extension
	if (!result.EndsWith(TextureFile::FILE_EXTENSION, DString::CC_IgnoreCase))
	{
		//check if there is an extension specified
		Int periodIdx = result.Find(TXT("."), 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		if (periodIdx > 0)
		{
			//Drop everything after the period.
			result = result.SubString(0, periodIdx);
		}
		else
		{
			//Extension wasn't specified. Need to add the period.
			result += TXT(".");
		}

		//Add the txtr
		result += TextureFile::FILE_EXTENSION;
	}

	return result;
}

void EditorUi::ShowViewOptions ()
{
	ViewOptionsFrame->SetVisibility(true);
	
	NewButton->SetCanBeFocused(false);
	LoadButton->SetCanBeFocused(false);
	SaveButton->SetCanBeFocused(false);
	ExportButton->SetCanBeFocused(false);
	ViewOptions->SetCanBeFocused(false);
	AuthorField->GetPropValueField()->SetCanBeFocused(false);
	LicenseField->GetPropValueField()->SetCanBeFocused(false);
	EditTagListButton->SetCanBeFocused(false);
	WrappingBool->GetCheckbox()->SetCanBeFocused(false);
	SmoothEdgesBool->GetCheckbox()->SetCanBeFocused(false);
	AllowMipMapBool->GetCheckbox()->SetCanBeFocused(false);
	CommentField->GetPropValueField()->SetCanBeFocused(false);

	ViewOptionsFrame->RefreshComponentTransforms();
}

void EditorUi::HideViewOptions ()
{
	ViewOptionsFrame->SetVisibility(false);
	
	NewButton->SetCanBeFocused(true);
	LoadButton->SetCanBeFocused(true);
	SaveButton->SetCanBeFocused(true);
	ExportButton->SetCanBeFocused(true);
	ViewOptions->SetCanBeFocused(true);
	AuthorField->GetPropValueField()->SetCanBeFocused(true);
	LicenseField->GetPropValueField()->SetCanBeFocused(true);
	EditTagListButton->SetCanBeFocused(true);
	WrappingBool->GetCheckbox()->SetCanBeFocused(true);
	SmoothEdgesBool->GetCheckbox()->SetCanBeFocused(true);
	AllowMipMapBool->GetCheckbox()->SetCanBeFocused(true);
	CommentField->GetPropValueField()->SetCanBeFocused(true);
}

void EditorUi::ShowTagFrame ()
{
	if (CurrentTagPanel != nullptr)
	{
		return; //panel is already up
	}

	CurrentTagPanel = TagPanel::CreateObject();
	CurrentTagPanel->SetTextureName(OpenedDevFile.GetName(true, true));
	CurrentTagPanel->RegisterToMainWindow(true, true, 500);
	CurrentTagPanel->OnApply = SDFUNCTION(this, EditorUi, HandleTagPanelApply, void);
	CurrentTagPanel->OnClose = SDFUNCTION(this, EditorUi, HandleTagPanelClose, void);
}

void EditorUi::CreateNewTexture ()
{
	FileDialogueOptions dialogueOptions(FileDialogueOptions::DT_Open);
	dialogueOptions.PersistenceId = NEW_DIALOGUE_ID;
	dialogueOptions.bMultiSelect = false;
	dialogueOptions.bStrictFileTypesOnly = true;
	dialogueOptions.DefaultFolder = Directory::DEV_ASSET_DIRECTORY;

	//List is based on sf::Image::loadFromFile.
	dialogueOptions.FileTypes =
	{
		TXT("bmp"),
		TXT("png"),
		TXT("tga"),
		TXT("jpg"),
		TXT("gif"),
		TXT("psd"),
		TXT("hdr"),
		TXT("pic")
	};

	std::vector<FileAttributes> selectedFiles;
	if (!OS_BrowseFiles(dialogueOptions, OUT selectedFiles))
	{
		return;
	}

	if (ContainerUtils::IsEmpty(selectedFiles))
	{
		return; //User cancelled from window dialogue
	}

	//Ensure the selected file resides in the engine.
	DString filePath = selectedFiles.at(0).GetPath().ToString();
	if (filePath.Find(Directory::BASE_DIRECTORY.ToString(), 0, DString::CC_IgnoreCase) != 0)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Unable to create a texture using file %s since all files must reside in %s."), selectedFiles.at(0).GetName(true, true), Directory::BASE_DIRECTORY.ToString());
		return;
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	if (EditedTexture != nullptr)
	{
		localTexturePool->DestroyTexture(TexturePath);
	}

	EditedTexture = localTexturePool->CreateAndImportTexture(selectedFiles.at(0), TexturePath);
	if (EditedTexture == nullptr || EditedTexture->GetTextureResource() == nullptr)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Unable to create a new texture using %s since the engine was unable to open the specified file."), selectedFiles.at(0).GetFileName());
		return;
	}

	if (TexturePreview != nullptr)
	{
		TexturePreview->SetSpriteTexture(EditedTexture);
	}

	RefreshTexturePreview();

	OpenedDevFile.SetFileName(DString::EmptyString, DString::EmptyString);
	ExportedFile.SetFileName(DString::EmptyString, DString::EmptyString);

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	SetWindowTitle(translator->TranslateText(TXT("UnnamedTexture"), LOCALIZATION_FILE, TXT("EditorUi")));

	UnmarkDirty();
	bCanBecomeDirty = false;

	if (SourceLabel != nullptr)
	{
		DString sourceTxt = selectedFiles.at(0).GetName(true, true);

		//Need to have the source label be relative to the engine directory instead of absolute path. Simply cut the front text.
		DString baseDir = Directory::BASE_DIRECTORY.ToString();
		sourceTxt = sourceTxt.SubString(Directory::BASE_DIRECTORY.ToString().Length());
		SourceLabel->SetText(sourceTxt);
	}

	if (DimensionsLabel != nullptr)
	{
		Int width;
		Int height;
		EditedTexture->GetDimensions(OUT width, OUT height);
		DimensionsLabel->SetText(width.ToString() + TXT(" x ") + height.ToString());
	}

	if (AuthorField != nullptr)
	{
		AuthorField->ResetToDefaults();
	}

	if (LicenseField != nullptr)
	{
		LicenseField->ResetToDefaults();
	}

	if (ExportDestinationField != nullptr)
	{
		ExportDestinationField->ResetToDefaults();
	}

	ContainerUtils::Empty(OUT Tags);
	if (TagList != nullptr)
	{
		TagList->SyncVector();
	}

	if (WrappingBool != nullptr && WrappingBool->GetCheckbox() != nullptr)
	{
		WrappingBool->GetCheckbox()->SetChecked(EditedTexture->GetTextureResource()->isRepeated());
	}

	if (SmoothEdgesBool != nullptr && SmoothEdgesBool->GetCheckbox() != nullptr)
	{
		SmoothEdgesBool->GetCheckbox()->SetChecked(EditedTexture->GetTextureResource()->isSmooth());
	}

	if (AllowMipMapBool != nullptr && AllowMipMapBool->GetCheckbox() != nullptr)
	{
		AllowMipMapBool->ResetToDefaults();
		if (AllowMipMapBool->GetCheckbox()->IsChecked())
		{
			if (!EditedTexture->GenerateMipMap())
			{
				TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to generate mipmap for %s."), selectedFiles.at(0).GetName(true, true));
				AllowMipMapBool->GetCheckbox()->SetChecked(false);
			}
		}
	}

	if (CommentField != nullptr)
	{
		CommentField->ResetToDefaults();
	}

	if (SaveButton != nullptr)
	{
		SaveButton->SetEnabled(true);
	}

	if (ExportButton != nullptr)
	{
		ExportButton->SetEnabled(true);
	}

	bCanBecomeDirty = true;
}

void EditorUi::LoadTexture ()
{
	FileDialogueOptions dialogueOptions(FileDialogueOptions::DT_Open);
	dialogueOptions.PersistenceId = OPEN_DIALOGUE_ID;
	dialogueOptions.bStrictFileTypesOnly = true;
	dialogueOptions.bMultiSelect = false;
	dialogueOptions.FileTypes = {DEV_EXTENSION};
	dialogueOptions.DefaultFolder = Directory::DEV_ASSET_DIRECTORY;

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	std::vector<FileAttributes> selectedFiles;
	if (!OS_BrowseFiles(dialogueOptions, OUT selectedFiles))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadDialogueError"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return;
	}

	if (ContainerUtils::IsEmpty(selectedFiles))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadUserCancelled"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return; //User cancelled
	}

	const DString sectionName = TXT("EditorUi");
	ConfigWriter* writer = ConfigWriter::CreateObject();
	if (!writer->OpenFile(selectedFiles.at(0), false))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadFileError"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		writer->Destroy();
		return;
	}

	//Check the type
	DString ignoreTypeSwitch = TXT("-IgnoreTypeChecking");
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	if (!localEngine->HasCmdLineSwitch(ignoreTypeSwitch, DString::CC_IgnoreCase))
	{
		DString assetType = writer->GetPropertyText(DString::EmptyString, ASSET_TYPE);
		if (!assetType.IsEmpty() && assetType.Compare(TEXTURE_ASSET_TYPE, DString::CC_CaseSensitive) != 0)
		{
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to open texture %s since the asset type for that file \"%s\" does not match the expected asset type \"%s\"."), selectedFiles.at(0).GetFileName(), assetType, TEXTURE_ASSET_TYPE);
			TextureEditorLog.Log(LogCategory::LL_Warning, TXT("To ignore this warning, pass \"%s\" in the command line arguments. Doing so would cause this editor to try to open the file anyways despite the type mismatch."), ignoreTypeSwitch);
			writer->Destroy();
			DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadTypeMismatch"), LOCALIZATION_FILE, TXT("EditorUi")), false);
			return;
		}
	}

	DString source = writer->GetPropertyText(sectionName, TXT("Source"));
	if (source.IsEmpty())
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to open texture %s since it does not specify a file source."), selectedFiles.at(0).GetFileName());
		writer->Destroy();
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadMissingSource"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return;
	}

	//Source is relative to the engine directory. Prepend the engine path.
	DString fullSource = Directory::BASE_DIRECTORY.ToString() + source;

	DString sourcePath;
	DString sourceName;
	FileUtils::ExtractFileName(fullSource, OUT sourcePath, OUT sourceName);
	FileAttributes sourceFile(sourcePath, sourceName);
	if (!OS_CheckIfFileExists(sourceFile))
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to open texture %s since its file source (%s) is either invalid or doesn't exist."), selectedFiles.at(0).GetFileName(), fullSource);
		writer->Destroy();
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadMissingSource"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return;
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	if (EditedTexture != nullptr)
	{
		localTexturePool->DestroyTexture(TexturePath);
	}

	EditedTexture = localTexturePool->CreateAndImportTexture(sourceFile, TexturePath);
	if (EditedTexture == nullptr || EditedTexture->GetTextureResource() == nullptr)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Unable to open texture %s since it could not import %s"), selectedFiles.at(0), source);
		writer->Destroy();
		DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadImportFailed"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return;
	}

	if (TexturePreview != nullptr)
	{
		TexturePreview->SetSpriteTexture(EditedTexture);
	}

	RefreshTexturePreview();

	OpenedDevFile = selectedFiles.at(0);
	SetWindowTitle(OpenedDevFile.GetName(false, true));
	UnmarkDirty();
	bCanBecomeDirty = false;

	if (SourceLabel != nullptr)
	{
		SourceLabel->SetText(source);
	}

	if (DimensionsLabel != nullptr)
	{
		Int width;
		Int height;
		EditedTexture->GetDimensions(OUT width, OUT height);
		DimensionsLabel->SetText(width.ToString() + TXT(" x ") + height.ToString());
	}

	if (AuthorField != nullptr && AuthorField->GetPropValueField() != nullptr)
	{
		AuthorField->GetPropValueField()->SetText(writer->GetPropertyText(sectionName, TXT("Author")));
	}

	if (LicenseField != nullptr && LicenseField->GetPropValueField() != nullptr)
	{
		LicenseField->GetPropValueField()->SetText(writer->GetPropertyText(sectionName, TXT("License")));
	}

	ExportedFile.SetFileName(DString::EmptyString, DString::EmptyString);
	DString exportDestStr = writer->GetPropertyText(sectionName, TXT("ExportDestination"));
	if (ExportDestinationField != nullptr && ExportDestinationField->GetPropValueField() != nullptr)
	{
		ExportDestinationField->GetPropValueField()->SetText(exportDestStr);
	}

	if (!exportDestStr.IsEmpty())
	{
		DString exportDestPath;
		DString exportDestFile;
		FileUtils::ExtractFileName(Directory::BASE_DIRECTORY.ToString() + Directory::DIRECTORY_SEPARATOR + exportDestStr, OUT exportDestPath, OUT exportDestFile);
		ExportedFile.SetFileName(exportDestPath, exportDestFile);
	}

	writer->GetArrayValues(sectionName, TXT("Tags"), OUT Tags);
	if (TagList != nullptr)
	{
		TagList->SyncVector();
	}

	if (WrappingBool != nullptr && WrappingBool->GetCheckbox() != nullptr)
	{
		Bool isWrapping = writer->GetProperty<Bool>(sectionName, TXT("Wrapping"));
		WrappingBool->GetCheckbox()->SetChecked(isWrapping);
		EditedTexture->EditTextureResource()->setRepeated(isWrapping);
	}

	if (SmoothEdgesBool != nullptr && SmoothEdgesBool->GetCheckbox() != nullptr)
	{
		Bool isSmooth = writer->GetProperty<Bool>(sectionName, TXT("SmoothEdges"));
		SmoothEdgesBool->GetCheckbox()->SetChecked(isSmooth);
		EditedTexture->EditTextureResource()->setSmooth(isSmooth);
	}

	if (AllowMipMapBool != nullptr && AllowMipMapBool->GetCheckbox() != nullptr)
	{
		Bool isAllowMipMap = writer->GetProperty<Bool>(sectionName, TXT("AllowMipMap"));
		AllowMipMapBool->GetCheckbox()->SetChecked(isAllowMipMap);
		if (isAllowMipMap)
		{
			if (!EditedTexture->GenerateMipMap())
			{
				TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to generate mipmap for %s."), fullSource);
				AllowMipMapBool->GetCheckbox()->SetChecked(false);
			}
		}
	}

	if (CommentField != nullptr && CommentField->GetPropValueField() != nullptr)
	{
		CommentField->GetPropValueField()->SetText(writer->GetPropertyText(sectionName, TXT("Comment")));
	}
	writer->Destroy();

	if (SaveButton != nullptr)
	{
		SaveButton->SetEnabled(true);
	}

	if (ExportButton != nullptr)
	{
		ExportButton->SetEnabled(true);
	}

	bCanBecomeDirty = true;

	DisplayFeedback(translator->TranslateText(TXT("FeedbackLoadSuccess"), LOCALIZATION_FILE, TXT("EditorUi")), true);
}

void EditorUi::SaveTexture ()
{
	if (OpenedDevFile.GetFileName().IsEmpty())
	{
		FileDialogueOptions dialogueOptions(FileDialogueOptions::DT_Save);
		dialogueOptions.PersistenceId = SAVE_DIALOGUE_ID;
		dialogueOptions.bStrictFileTypesOnly = true;
		dialogueOptions.FileTypes = {DEV_EXTENSION};
		dialogueOptions.DefaultFolder = Directory::DEV_ASSET_DIRECTORY;

		std::vector<FileAttributes> selectedFiles;
		if (!OS_BrowseFiles(dialogueOptions, OUT selectedFiles))
		{
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			DisplayFeedback(translator->TranslateText(TXT("FeedbackSaveUserCancelled"), LOCALIZATION_FILE, TXT("EditorUi")), false);
			return;
		}

		if (ContainerUtils::IsEmpty(selectedFiles))
		{
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			DisplayFeedback(translator->TranslateText(TXT("FeedbackSaveUserCancelled"), LOCALIZATION_FILE, TXT("EditorUi")), false);
			return; //user cancelled
		}

		if (selectedFiles.at(0).ReadFileExtension().IsEmpty())
		{
			//Append file extension if the user didn't specify one.
			selectedFiles.at(0).SetFileName(selectedFiles.at(0).ReadPath().ToString(), selectedFiles.at(0).GetFileName() + TXT(".") + DEV_EXTENSION);
		}

		if (!selectedFiles.at(0).GetName(false, true).EndsWith(DEV_EXTENSION, DString::CC_IgnoreCase))
		{
			TextTranslator* translator = TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			DisplayFeedback(DString::CreateFormattedString(translator->TranslateText(TXT("FeedbackSaveIncorrectExtension"), LOCALIZATION_FILE, TXT("EditorUi")), DEV_EXTENSION), false);
			return;
		}

		UpdateExportDestBasedOnSaveDest(selectedFiles.at(0));
		SaveCurrentTexture(selectedFiles.at(0));
	}
	else
	{
		UpdateExportDestBasedOnSaveDest(OpenedDevFile);
		SaveCurrentTexture(OpenedDevFile);
	}
}

void EditorUi::UpdateExportDestBasedOnSaveDest (const FileAttributes& devAssetDest)
{
	//Update ExportDestination if it's an empty string
	if (ExportDestinationField != nullptr)
	{
		if (TextFieldComponent* exportField = ExportDestinationField->GetPropValueField())
		{
			if (exportField->GetContent().IsEmpty())
			{
				//Set to relative to Content directory instead of DevAsset (only if the file saved resides in dev asset directory)
				DString exportDirStr = devAssetDest.ReadPath().ToString();

				if (exportDirStr.StartsWith(Directory::DEV_ASSET_DIRECTORY.ToString(), DString::CC_IgnoreCase))
				{
					exportDirStr = exportDirStr.SubString(Directory::DEV_ASSET_DIRECTORY.ToString().Length());
					exportDirStr = TXT("Content") + Directory::DIRECTORY_SEPARATOR + exportDirStr;

					DString fileName = devAssetDest.GetName(false, true);

					//Replace DevAsset extension with TextureFile extension
					if (fileName.EndsWith(DEV_EXTENSION, DString::CC_CaseSensitive))
					{
						fileName = fileName.SubString(0, fileName.Length() - (DEV_EXTENSION.Length() + 1));
						fileName += TextureFile::FILE_EXTENSION;
					}

					exportField->SetText(exportDirStr + fileName);
				}
			}
		}
	}
}

bool EditorUi::ChooseExportDestination ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	FileDialogueOptions dialogueOptions(FileDialogueOptions::DT_Save);
	dialogueOptions.PersistenceId = EXPORT_DIALOGUE_ID;
	dialogueOptions.bMultiSelect = false;
	dialogueOptions.bStrictFileTypesOnly = true;
	dialogueOptions.DefaultFolder = Directory::CONTENT_DIRECTORY;
	dialogueOptions.FileTypes = {TextureFile::FILE_EXTENSION};

	std::vector<FileAttributes> selectedFiles;
	if (!OS_BrowseFiles(dialogueOptions, OUT selectedFiles))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackExportUserCancelled"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return false;
	}

	if (ContainerUtils::IsEmpty(selectedFiles))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackExportUserCancelled"), LOCALIZATION_FILE, TXT("EditorUi")), false);
		return false; //user cancelled
	}

	if (selectedFiles.at(0).ReadFileExtension().IsEmpty())
	{
		//Append file extension if the user didn't specify one.
		selectedFiles.at(0).SetFileName(selectedFiles.at(0).ReadPath().ToString(), selectedFiles.at(0).GetFileName() + TXT(".") + TextureFile::FILE_EXTENSION);
	}

	if (selectedFiles.at(0).ReadFileExtension().Compare(TextureFile::FILE_EXTENSION, DString::CC_IgnoreCase) != 0)
	{
		DisplayFeedback(DString::CreateFormattedString(translator->TranslateText(TXT("FeedbackExportIncorrectExtension"), LOCALIZATION_FILE, TXT("EditorUi")), TextureFile::FILE_EXTENSION), false);
		return false;
	}

	ExportedFile = selectedFiles.at(0);

	//Update the destination field
	if (ExportDestinationField != nullptr && ExportDestinationField->GetPropValueField() != nullptr)
	{
		ExportDestinationField->GetPropValueField()->SetText(FormatExportDestination(ExportedFile.GetName(true, true)));
	}

	return true;
}

void EditorUi::ExportTexture ()
{
	if (EditedTexture == nullptr)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Cannot export a texture if one hasn't been loaded yet."));
		return;
	}

	if (ExportedFile.GetFileName().IsEmpty())
	{
		if (!ChooseExportDestination())
		{
			return;
		}
	}

	//The TextureFile will be reading the tags from the texture pool. Simply replace its tag list with the editor's cached tag list.
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Remove old tags if there are any.
	for (const HashedString& oldTag : EditedTexture->ReadTags())
	{
		ResourceTag::RemoveTag(oldTag, localTexturePool->Tags);
	}
	ContainerUtils::Empty(OUT EditedTexture->EditTags());

	//Add new tags based on user selection
	for (size_t i = 0; i < Tags.size(); ++i)
	{
		const HashedString& newTag = EditedTexture->EditTags().emplace_back(Tags.at(i));
		ResourceTag::AddTag(newTag, localTexturePool->Tags);
	}

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	TextureFile* exportFile = TextureFile::CreateObject();
	exportFile->SetTargetTexture(EditedTexture);
	
	exportFile->SetTargetTexturePool(localTexturePool);
	if (exportFile->SaveFile(ExportedFile))
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackExportSuccess"), LOCALIZATION_FILE, TXT("EditorUi")), true);
	}
	else
	{
		DisplayFeedback(translator->TranslateText(TXT("FeedbackExportFailed"), LOCALIZATION_FILE, TXT("EditorUi")), false);
	}
}

void EditorUi::HandleNewButtonReleased (ButtonComponent* uiComp)
{
	if (EditedTexture != nullptr && bDirty)
	{
		DisplayPrompt();
		CHECK(Popup != nullptr)
		
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		Popup->SetPromptText(translator->TranslateText(TXT("PromptSaveChanges"), LOCALIZATION_FILE, TXT("EditorUi")));

		Popup->OnAccept = SDFUNCTION(this, EditorUi, HandleSaveToNewReleased, void);
		Popup->OnNo = SDFUNCTION(this, EditorUi, HandleSkipSaveToNewReleased, void);
		Popup->OnCancel = SDFUNCTION(this, EditorUi, HandleCancelNewReleased, void);
	}
	else
	{
		CreateNewTexture();
	}
}

void EditorUi::HandleLoadButtonReleased (ButtonComponent* uiComp)
{
	if (EditedTexture != nullptr && bDirty)
	{
		DisplayPrompt();
		CHECK(Popup != nullptr)
		
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		Popup->SetPromptText(translator->TranslateText(TXT("PromptSaveChanges"), LOCALIZATION_FILE, TXT("EditorUi")));

		Popup->OnAccept = SDFUNCTION(this, EditorUi, HandleSaveToLoadReleased, void);
		Popup->OnNo = SDFUNCTION(this, EditorUi, HandleSkipSaveToLoadReleased, void);
		Popup->OnCancel = SDFUNCTION(this, EditorUi, HandleCancelLoadReleased, void);
	}
	else
	{
		LoadTexture();
	}
}

void EditorUi::HandleSaveButtonReleased (ButtonComponent* uiComp)
{
	SaveTexture();
}

void EditorUi::HandleExportButtonReleased (ButtonComponent* uiComp)
{
	ExportTexture();
}

void EditorUi::HandleFeedbackTick (Float deltaSec)
{
	CHECK(FeedbackTick != nullptr) //This function shouldn't be running if the tick was destroyed
	if (FeedbackFrame == nullptr || FeedbackIcon == nullptr || FeedbackText == nullptr)
	{
		FeedbackTick->SetTicking(false);
		return;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	Float ratio = (localEngine->GetElapsedTime() - FeedbackTimestamp) / FeedbackDisplayDuration;
	if (ratio >= 1.f) //Finished displaying
	{
		FeedbackFrame->SetVisibility(false);
		FeedbackTick->SetTicking(false);
		return;
	}

	sf::Uint8 alpha = Utils::Lerp<sf::Uint8>(ratio.Value, 255, 0);
	if (ColorRenderComponent* colorComp = FeedbackFrame->GetCenterCompAs<ColorRenderComponent>())
	{
		colorComp->SolidColor.Source.a = alpha;
	}

	if (sf::Sprite* sprite = FeedbackIcon->GetSprite())
	{
		sf::Color newColor = sprite->getColor();
		newColor.a = alpha;
		sprite->setColor(newColor);
	}

	if (TextRenderComponent* textComp = FeedbackText->GetRenderComponent())
	{
		sf::Color newColor = textComp->GetFontColor();
		newColor.a = alpha;
		textComp->SetFontColor(newColor);
	}
}

void EditorUi::HandleViewOptionsReleased (ButtonComponent* uiComp)
{
	ShowViewOptions();
}

void EditorUi::HandleTiledCheckboxToggle (EditPropertyComponent* comp)
{
	if (PreviewTransform == nullptr || TexturePreview == nullptr || EditedTexture == nullptr)
	{
		TextureEditorLog.Log(LogCategory::LL_Warning, TXT("Unable to toggle tiled or not since the editor did not initialize the texture preview."));
		return;
	}

	if (EditableBool* boolComp = dynamic_cast<EditableBool*>(comp))
	{
		if (boolComp->GetCheckbox() != nullptr && boolComp->GetCheckbox()->IsChecked())
		{
			TexturePreview->SetDrawMode(SpriteComponent::DM_Tiled);
		}
		else
		{
			TexturePreview->SetDrawMode(SpriteComponent::DM_Stretch);
		}

		RefreshTexturePreview();
	}
}

void EditorUi::HandleCheckerboardToggle (EditPropertyComponent* comp)
{
	if (EditableBool* boolComp = dynamic_cast<EditableBool*>(comp))
	{
		if (boolComp->GetCheckbox() != nullptr && PreviewFrame != nullptr)
		{
			bool showCheckerboard = boolComp->GetCheckbox()->IsChecked();
			if (showCheckerboard)
			{
				TexturePool* localTexturePool = TexturePool::FindTexturePool();
				CHECK(localTexturePool != nullptr)
				PreviewFrame->SetCenterTexture(localTexturePool->GetTexture(HashedString("Engine.Graphics.DebuggingTexture")));
				if (SpriteComponent* spriteComp = PreviewFrame->GetCenterCompAs<SpriteComponent>())
				{
					spriteComp->SetDrawMode(SpriteComponent::DM_Tiled);
				}
			}
			else
			{
				PreviewFrame->SetCenterColor(BackgroundColor);
			}
		}
	}
}

void EditorUi::HandleShowInfoToggle (EditPropertyComponent* comp)
{
	if (ShowInfoCheckbox != nullptr && ShowInfoCheckbox->GetCheckbox() != nullptr && InfoBackground != nullptr)
	{
		InfoBackground->SetVisibility(ShowInfoCheckbox->GetCheckbox()->IsChecked());
	}
}

void EditorUi::HandleBackgroundColorEdit (EditPropertyComponent* comp)
{
	Int colorComposition = 0;
	if (BackgroundColorR->GetPropValueField() != nullptr)
	{
		Int propValue = Int(BackgroundColorR->GetPropValueField()->GetContent());
		colorComposition |= (propValue << 24);
	}

	if (BackgroundColorG->GetPropValueField() != nullptr)
	{
		Int propValue = Int(BackgroundColorG->GetPropValueField()->GetContent());
		colorComposition |= (propValue << 16);
	}

	if (BackgroundColorB->GetPropValueField() != nullptr)
	{
		Int propValue = Int(BackgroundColorB->GetPropValueField()->GetContent());
		colorComposition |= (propValue << 8);
	}

	//Alpha is always maxed
	colorComposition |= 255;
	BackgroundColor = Color(colorComposition.ToInt32());
	if (PreviewFrame != nullptr)
	{
		//If the user is not viewing the checkerboard texture, then update the center color
		if (ColorRenderComponent* colorComp = PreviewFrame->GetCenterCompAs<ColorRenderComponent>())
		{
			colorComp->SolidColor = BackgroundColor;
		}
	}
}

void EditorUi::HandleSourceLabelTransformChanged ()
{
	if (SourceLabel != nullptr && DimensionsLabel != nullptr && InfoBackground != nullptr)
	{
		Float maxSize = Utils::Max(SourceLabel->ReadCachedAbsSize().X, DimensionsLabel->ReadCachedAbsSize().X);
		maxSize += SourceLabel->GetAnchorRightDist() * 2.f; //add margins
		InfoBackground->SetSize(Vector2(maxSize, InfoBackground->ReadSize().Y));
	}
}

void EditorUi::HandleEditExportDestinationReleased (ButtonComponent* uiComp)
{
	ChooseExportDestination();
}

void EditorUi::HandleExportDestinationEdit (EditPropertyComponent* comp)
{
	if (EditableDString* editableStr = dynamic_cast<EditableDString*>(comp))
	{
		if (editableStr->GetPropValueField() != nullptr)
		{
			editableStr->GetPropValueField()->SetText(FormatExportDestination(editableStr->GetPropValueField()->GetContent()));
		}
	}
}

void EditorUi::HandleEditTagsReleased (ButtonComponent* uiComp)
{
	ShowTagFrame();
}

void EditorUi::HandleToggleWrapping (EditPropertyComponent* comp)
{
	if (WrappingBool != nullptr && WrappingBool->GetCheckbox() != nullptr && EditedTexture != nullptr)
	{
		EditedTexture->EditTextureResource()->setRepeated(WrappingBool->GetCheckbox()->IsChecked());
		MarkDirty();
	}
}

void EditorUi::HandleToggleSmoothEdges (EditPropertyComponent* comp)
{
	if (SmoothEdgesBool != nullptr && SmoothEdgesBool->GetCheckbox() != nullptr && EditedTexture != nullptr)
	{
		EditedTexture->EditTextureResource()->setSmooth(SmoothEdgesBool->GetCheckbox()->IsChecked());
		MarkDirty();
	}
}

void EditorUi::HandleToggleMipMap (EditPropertyComponent* comp)
{
	if (AllowMipMapBool != nullptr && AllowMipMapBool->GetCheckbox() != nullptr && EditedTexture != nullptr)
	{
		if (AllowMipMapBool->GetCheckbox()->IsChecked())
		{
			EditedTexture->GenerateMipMap();
		}

		MarkDirty();
	}
}

void EditorUi::HandleTagPanelApply ()
{
	if (CurrentTagPanel != nullptr)
	{
		Tags = CurrentTagPanel->ReadSelectedTags();
		if (TagList != nullptr)
		{
			TagList->SyncVector();
		}

		MarkDirty();
	}
}

void EditorUi::HandleTagPanelClose ()
{
	CurrentTagPanel = nullptr;
}

void EditorUi::HandlePropertyEdit (EditPropertyComponent* uiComp)
{
	MarkDirty();
}

void EditorUi::HandleSaveToNewReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
	SaveTexture();
	CreateNewTexture();
}

void EditorUi::HandleSkipSaveToNewReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
	CreateNewTexture();
}

void EditorUi::HandleCancelNewReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
}

void EditorUi::HandleSaveToLoadReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
	SaveTexture();
	LoadTexture();
}

void EditorUi::HandleSkipSaveToLoadReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
	LoadTexture();
}

void EditorUi::HandleCancelLoadReleased ()
{
	Popup = nullptr; //Popup is going to destroy itself. Clear pointer.
}

void EditorUi::HandleSaveToQuitReleased ()
{
	Popup = nullptr;
	SaveTexture();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->Shutdown();
}

void EditorUi::HandleSkipSaveToQuitReleased ()
{
	Popup = nullptr;

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->Shutdown();
}

void EditorUi::HandleCancelQuitReleased ()
{
	Popup = nullptr;
}

bool EditorUi::HandleCanClose ()
{
	if (bDirty && Popup == nullptr)
	{
		DisplayPrompt();
		CHECK(Popup != nullptr)
		
		TextTranslator* translator = TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		Popup->SetPromptText(translator->TranslateText(TXT("PromptSaveChanges"), LOCALIZATION_FILE, TXT("EditorUi")));

		Popup->OnAccept = SDFUNCTION(this, EditorUi, HandleSaveToQuitReleased, void);
		Popup->OnNo = SDFUNCTION(this, EditorUi, HandleSkipSaveToQuitReleased, void);
		Popup->OnCancel = SDFUNCTION(this, EditorUi, HandleCancelQuitReleased, void);
	}

	return !bDirty;
}
TE_END