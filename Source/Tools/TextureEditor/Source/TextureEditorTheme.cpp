/*
=====================================================================
  
  Copyright (c) 2016-2024 Translucent Games LLC

  -------------------------------------------------------------------

  TextureEditorTheme.cpp
=====================================================================
*/

#include "TextureEditorTheme.h"

IMPLEMENT_CLASS(SD::TE::TextureEditorTheme, SD::EditorTheme)
TE_BEGIN

const DString TextureEditorTheme::TEXTURE_EDITOR_STYLE_NAME = TXT("TextureEditorTheme");

void TextureEditorTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const SStyleInfo* defaultStyle = FindStyle(EDITOR_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo testerTheme = CreateStyleFrom(*defaultStyle);
		testerTheme.Name = TEXTURE_EDITOR_STYLE_NAME;
		Styles.push_back(testerTheme);

		ButtonComponent* buttonTemplate = dynamic_cast<ButtonComponent*>(FindTemplate(&testerTheme, ButtonComponent::SStaticClass()));
		CHECK(buttonTemplate != nullptr)
		{
			ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(buttonTemplate->ReplaceStateComponent(ColorButtonState::SStaticClass()));
			CHECK(colorState != nullptr)
			colorState->SetDefaultColor(Color(128, 128, 128, 200));
			colorState->SetHoverColor(Color(145, 145, 145, 200));
			colorState->SetDownColor(Color(96, 96, 96, 200));
			colorState->SetDisabledColor(Color(32, 32, 32, 200));
		}

		ListBoxComponent* boxTemplate = dynamic_cast<ListBoxComponent*>(FindTemplate(&testerTheme, ListBoxComponent::SStaticClass()));
		CHECK(boxTemplate != nullptr)
		{
			if (FrameComponent* background = boxTemplate->GetBackground())
			{
				if (const Texture* backgroundTexture = localTexturePool->GetTexture(HashedString("Tools.TextureEditor.Gradient")))
				{
					background->SetCenterTexture(backgroundTexture);
				}
			}
		}
	}
}
TE_END